﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean ObiContactGrabber::get_grabbed()
extern void ObiContactGrabber_get_grabbed_mB390126778E0A789B441B372D5F525DB318BC301 (void);
// 0x00000002 System.Void ObiContactGrabber::Awake()
extern void ObiContactGrabber_Awake_m81FAB1A7D0894DFF410B881A4BD34C377CCDF33D (void);
// 0x00000003 System.Void ObiContactGrabber::OnEnable()
extern void ObiContactGrabber_OnEnable_m0CDF84CEFA0997E0C363D99406A72B7F6617015D (void);
// 0x00000004 System.Void ObiContactGrabber::OnDisable()
extern void ObiContactGrabber_OnDisable_m70E63109356461451716F2656B1F703ABDF30B67 (void);
// 0x00000005 System.Void ObiContactGrabber::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ObiContactGrabber_Solver_OnCollision_m02CB781C94F8F518ADC1B61E01487BBDC62BA026 (void);
// 0x00000006 System.Void ObiContactGrabber::UpdateParticleProperties()
extern void ObiContactGrabber_UpdateParticleProperties_m874B590742CF4C376FDD13678A387918D098B615 (void);
// 0x00000007 System.Boolean ObiContactGrabber::GrabParticle(Obi.ObiSolver,System.Int32)
extern void ObiContactGrabber_GrabParticle_mA112EAB2920DB3B64AA31FCF9455C6F4702858B5 (void);
// 0x00000008 System.Void ObiContactGrabber::Grab()
extern void ObiContactGrabber_Grab_mF88B7A50127D874AC0295C6D7D98FFEBE6D8F618 (void);
// 0x00000009 System.Void ObiContactGrabber::Release()
extern void ObiContactGrabber_Release_m82B5D7B992BC9423026E8CE0BD6DE0884D72BA68 (void);
// 0x0000000A System.Void ObiContactGrabber::FixedUpdate()
extern void ObiContactGrabber_FixedUpdate_mCAD4BA24875E897F95967F4540B06AC26800BF01 (void);
// 0x0000000B System.Void ObiContactGrabber::.ctor()
extern void ObiContactGrabber__ctor_m9C1D048C7D8633509DB48156260CB765A7E6E364 (void);
// 0x0000000C System.Void ObiContactGrabber/GrabbedParticle::.ctor(Obi.ObiSolver,System.Int32,System.Single)
extern void GrabbedParticle__ctor_m29866A846BC99D62A7E53A7F55FD6234B7DB7567 (void);
// 0x0000000D System.Boolean ObiContactGrabber/GrabbedParticle::Equals(ObiContactGrabber/GrabbedParticle,ObiContactGrabber/GrabbedParticle)
extern void GrabbedParticle_Equals_mFF2582C3BFDDE99366B35AC5DF73C20ECE8E430D (void);
// 0x0000000E System.Int32 ObiContactGrabber/GrabbedParticle::GetHashCode(ObiContactGrabber/GrabbedParticle)
extern void GrabbedParticle_GetHashCode_m37D33E44CE7086BE8CD4AF64B4270BE0FADDACC9 (void);
// 0x0000000F System.Runtime.InteropServices.GCHandle Oni::PinMemory(System.Object)
extern void Oni_PinMemory_m03E1EA6D2AD2B2501D052E52FDE072606EDEB6EF (void);
// 0x00000010 System.Void Oni::UnpinMemory(System.Runtime.InteropServices.GCHandle)
extern void Oni_UnpinMemory_m029F0BA710E2DBE187A2300ED9F5CFA9DE37B62F (void);
// 0x00000011 System.Void Oni::UpdateColliderGrid(System.Single)
extern void Oni_UpdateColliderGrid_m75034617DEA373FD3AAC6D3F57A9530AA426895D (void);
// 0x00000012 System.Void Oni::SetColliders(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetColliders_m901960CF23BF96A6DCE6751F09B4425431091A65 (void);
// 0x00000013 System.Void Oni::SetRigidbodies(System.IntPtr)
extern void Oni_SetRigidbodies_mF84FEC7A0C4A850D6CD1F9CF39A29C2D6AFEAECF (void);
// 0x00000014 System.Void Oni::SetCollisionMaterials(System.IntPtr)
extern void Oni_SetCollisionMaterials_mAAE3428F89A91AF30BB753E728556AD1F592F3B1 (void);
// 0x00000015 System.Void Oni::SetTriangleMeshData(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern void Oni_SetTriangleMeshData_mFFF4366C8A8B13EDA5301EDE1CED9A2CD92AE43D (void);
// 0x00000016 System.Void Oni::SetEdgeMeshData(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern void Oni_SetEdgeMeshData_m7D6EBD72BA2169E3C697F341B75BC716161524D0 (void);
// 0x00000017 System.Void Oni::SetDistanceFieldData(System.IntPtr,System.IntPtr)
extern void Oni_SetDistanceFieldData_mED7CC8724B49A2AF36F817A57152CD95AA6E0200 (void);
// 0x00000018 System.Void Oni::SetHeightFieldData(System.IntPtr,System.IntPtr)
extern void Oni_SetHeightFieldData_m3890F2905A2990080378C1C2F28A9AD2DA077BEE (void);
// 0x00000019 System.IntPtr Oni::CreateSolver(System.Int32)
extern void Oni_CreateSolver_m95484CA4C8E4048EB871BB224918540A317C885A (void);
// 0x0000001A System.Void Oni::DestroySolver(System.IntPtr)
extern void Oni_DestroySolver_m0ADEE02261B34DC1C348911FCDC53E4471B37D0B (void);
// 0x0000001B System.Void Oni::SetCapacity(System.IntPtr,System.Int32)
extern void Oni_SetCapacity_m4804EBFCAF115506C8DD5332214C6A2E0CB4810B (void);
// 0x0000001C System.Void Oni::InitializeFrame(System.IntPtr,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Quaternion&)
extern void Oni_InitializeFrame_mB306333E4A867678C088DACC72B039EF58E80D31 (void);
// 0x0000001D System.Void Oni::UpdateFrame(System.IntPtr,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Quaternion&,System.Single)
extern void Oni_UpdateFrame_m680BE80814872FABDBFDC7ABBE5FF1E740C5773D (void);
// 0x0000001E System.Void Oni::ApplyFrame(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void Oni_ApplyFrame_mD136F376FE68D93651459FB1B946CE18BFAA8D10 (void);
// 0x0000001F System.Void Oni::RecalculateInertiaTensors(System.IntPtr)
extern void Oni_RecalculateInertiaTensors_m5D778943EF3E20DCF00A85175E39DD691A8AFA16 (void);
// 0x00000020 System.Void Oni::ResetForces(System.IntPtr)
extern void Oni_ResetForces_m38A179189C49CCAC6142D9F38F234BA8D03C5F64 (void);
// 0x00000021 System.Void Oni::SetRigidbodyLinearDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetRigidbodyLinearDeltas_m3D5B23B5969BC13392B4E7E019D03730E61E3639 (void);
// 0x00000022 System.Void Oni::SetRigidbodyAngularDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetRigidbodyAngularDeltas_m4A436C37D1F6F9FE39F1184279883463F586CBF0 (void);
// 0x00000023 System.Void Oni::GetBounds(System.IntPtr,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Oni_GetBounds_m363A96EE8BAB3FB760C636B8C5F361B8EE1E53B5 (void);
// 0x00000024 System.Int32 Oni::GetParticleGridSize(System.IntPtr)
extern void Oni_GetParticleGridSize_m2BD9A7939A0F7733323CA9C9EDE32C53FC7B6ABD (void);
// 0x00000025 System.Void Oni::GetParticleGrid(System.IntPtr,Oni/GridCell[])
extern void Oni_GetParticleGrid_m21D4AA3548CF77E25C5E456096E16162C1BAD4C0 (void);
// 0x00000026 System.Int32 Oni::SpatialQuery(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SpatialQuery_m526F90093505F1A4FB3E31A2AC6EC6B8C1F850A5 (void);
// 0x00000027 System.Void Oni::GetQueryResults(System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_GetQueryResults_mB69A3B8645A8218700E75C49956E15AC27B125B7 (void);
// 0x00000028 System.Void Oni::SetSolverParameters(System.IntPtr,Oni/SolverParameters&)
extern void Oni_SetSolverParameters_mA78FF4C834E23D50A56D989521B278AA6B11BD48 (void);
// 0x00000029 System.Void Oni::GetSolverParameters(System.IntPtr,Oni/SolverParameters&)
extern void Oni_GetSolverParameters_mD299B385DF9770D77C9E0D4F4781EFADE20C30E4 (void);
// 0x0000002A System.Int32 Oni::SetActiveParticles(System.IntPtr,System.Int32[],System.Int32)
extern void Oni_SetActiveParticles_mA8361CFE331459188A7701673465FCEC0770F84B (void);
// 0x0000002B System.IntPtr Oni::CollisionDetection(System.IntPtr,System.Single)
extern void Oni_CollisionDetection_mD890F2AED814E8B7F3766DDD603916EDA4B1B861 (void);
// 0x0000002C System.IntPtr Oni::Step(System.IntPtr,System.Single,System.Single,System.Int32)
extern void Oni_Step_m73AE7B946E5053FC14C8061FCD6508D4824213B7 (void);
// 0x0000002D System.Void Oni::ApplyPositionInterpolation(System.IntPtr,System.IntPtr,System.IntPtr,System.Single,System.Single)
extern void Oni_ApplyPositionInterpolation_mA7A631314B43D3A62FAF81D209F1CCB00A3A901D (void);
// 0x0000002E System.Void Oni::UpdateSkeletalAnimation(System.IntPtr)
extern void Oni_UpdateSkeletalAnimation_m0F0F49630F0ABF1B6C742D13FEDB8A69B3C71A31 (void);
// 0x0000002F System.Int32 Oni::GetConstraintCount(System.IntPtr,System.Int32)
extern void Oni_GetConstraintCount_m046D7B8D04B0EDDC24A336FA1AD6F93B181B143A (void);
// 0x00000030 System.Void Oni::SetRenderableParticlePositions(System.IntPtr,System.IntPtr)
extern void Oni_SetRenderableParticlePositions_mF10457D7F9724C687D3AB6E83DEEBB4C9937F138 (void);
// 0x00000031 System.Void Oni::SetParticlePhases(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePhases_m330FC442852B31A851FB05486D9D5D4A9E85080C (void);
// 0x00000032 System.Void Oni::SetParticleFilters(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleFilters_m4689F2DCACACC85971902111BF779817074E976C (void);
// 0x00000033 System.Void Oni::SetParticleCollisionMaterials(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleCollisionMaterials_m64562F4539FF5805E44B181ED119B61FB636DB91 (void);
// 0x00000034 System.Void Oni::SetParticlePositions(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePositions_mBF4B08453E99AACC5D3C934F3B7F1EDB2AA3934A (void);
// 0x00000035 System.Void Oni::SetParticlePreviousPositions(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePreviousPositions_mF7643A2DD9F3D10409DF62601E0D8EE2D0DBD7E4 (void);
// 0x00000036 System.Void Oni::SetParticleOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleOrientations_m92BF1084EB29939C992A0666D1E75C6AAB14801D (void);
// 0x00000037 System.Void Oni::SetParticlePreviousOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePreviousOrientations_m08E1313630723606D179B6FB44025E49B838D2BB (void);
// 0x00000038 System.Void Oni::SetRenderableParticleOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetRenderableParticleOrientations_mE90A2228D4CDC6641661C730F1164F65609788B8 (void);
// 0x00000039 System.Void Oni::SetParticleInverseMasses(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleInverseMasses_mA5F3998095FBD44B558C75B634E18628520D2385 (void);
// 0x0000003A System.Void Oni::SetParticleInverseRotationalMasses(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleInverseRotationalMasses_mBB6281669729C39BEF623DDDEDCC12EC93C31FBB (void);
// 0x0000003B System.Void Oni::SetParticlePrincipalRadii(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePrincipalRadii_mCE141D972ABA98C2CCC7B66419BC0009E227F8CE (void);
// 0x0000003C System.Void Oni::SetParticleVelocities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleVelocities_m806F66F28AB0D4E632921B7E0CF3464C777B00C7 (void);
// 0x0000003D System.Void Oni::SetParticleAngularVelocities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleAngularVelocities_mD376C89032B153B1C2EB36FAF673C90600871052 (void);
// 0x0000003E System.Void Oni::SetParticleExternalForces(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleExternalForces_mABE204855B05F30D980984785EFC178E8178657B (void);
// 0x0000003F System.Void Oni::SetParticleExternalTorques(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleExternalTorques_m52F453DFC994BB2CB946C0ECFCA90755E8CE81BF (void);
// 0x00000040 System.Void Oni::SetParticleWinds(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleWinds_m4D8B34B1F691B0D06B66B5AC4A9CE7A47C438D7A (void);
// 0x00000041 System.Void Oni::SetParticlePositionDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePositionDeltas_m44290488FA81D82A7B5E34C6C419183E4AC4C7F3 (void);
// 0x00000042 System.Void Oni::SetParticleOrientationDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleOrientationDeltas_m80BA163C8E315CDE4D535070E044140582F4FB75 (void);
// 0x00000043 System.Void Oni::SetParticlePositionConstraintCounts(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePositionConstraintCounts_mC6C3DF0767ABFE55B7D56664BD2B85CACC2F2AF3 (void);
// 0x00000044 System.Void Oni::SetParticleOrientationConstraintCounts(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleOrientationConstraintCounts_mE8DE30A217492D6C2961351F94471600E84DDF33 (void);
// 0x00000045 System.Void Oni::SetParticleNormals(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleNormals_mC7DB3812E3D8DA4944D5783D612327C14DAA1407 (void);
// 0x00000046 System.Void Oni::SetParticleInverseInertiaTensors(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleInverseInertiaTensors_m107AC92B3D36DA0EC84A2BC81EAAFDE1A87645F3 (void);
// 0x00000047 System.Void Oni::SetParticleSmoothingRadii(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleSmoothingRadii_m23C24AAADB64BB87F5AB6C0F29146E045BD15F32 (void);
// 0x00000048 System.Void Oni::SetParticleBuoyancy(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleBuoyancy_m1D5C0C42C6E1F7F9F0009907C3C47777493BFFFD (void);
// 0x00000049 System.Void Oni::SetParticleRestDensities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleRestDensities_mC2C5BF07F3F31A9A9BD30F1E0FD65FA90DAB8668 (void);
// 0x0000004A System.Void Oni::SetParticleViscosities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleViscosities_m40D16C05777FCCBA236F8812E90FCE61732F8F65 (void);
// 0x0000004B System.Void Oni::SetParticleSurfaceTension(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleSurfaceTension_mEB650D756ACFD8AFCE0B0CA1FA516B8A04166803 (void);
// 0x0000004C System.Void Oni::SetParticleVorticityConfinement(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleVorticityConfinement_m490207BCEB92C2090FA6F812903E9DC720C542CD (void);
// 0x0000004D System.Void Oni::SetParticleAtmosphericDragPressure(System.IntPtr,System.IntPtr,System.IntPtr)
extern void Oni_SetParticleAtmosphericDragPressure_m0DE4F75155DE8255D542463CED62BBA10DE95991 (void);
// 0x0000004E System.Void Oni::SetParticleDiffusion(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleDiffusion_m2E7A56B1429ABF2C68BCBEAF9A8B903BCFBBEB97 (void);
// 0x0000004F System.Void Oni::SetParticleVorticities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleVorticities_mBFAFD13B80D055251ECE3BB09331FEE93EC096CE (void);
// 0x00000050 System.Void Oni::SetParticleFluidData(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleFluidData_m5475F465341FBD9D3646CF21F640D31DBBDA52BC (void);
// 0x00000051 System.Void Oni::SetParticleUserData(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleUserData_m1C95E2860A342CC2408369BE71453D688A4940E2 (void);
// 0x00000052 System.Void Oni::SetParticleAnisotropies(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleAnisotropies_m4F40683E247F34D06C6FAC46E7BB2D100699D943 (void);
// 0x00000053 System.Void Oni::SetSimplices(System.IntPtr,System.Int32[],System.Int32,System.Int32,System.Int32)
extern void Oni_SetSimplices_mFA1EA60EF9F96B8AD384993D7AC15B73A2B08D95 (void);
// 0x00000054 System.Int32 Oni::GetDeformableTriangleCount(System.IntPtr)
extern void Oni_GetDeformableTriangleCount_m3D720CBB04BADD1E4AAE5D0F630E7E8A1AB8D398 (void);
// 0x00000055 System.Void Oni::SetDeformableTriangles(System.IntPtr,System.Int32[],System.Int32,System.Int32)
extern void Oni_SetDeformableTriangles_m93D02F1E05CEF46D51296532191387392C8885C6 (void);
// 0x00000056 System.Int32 Oni::RemoveDeformableTriangles(System.IntPtr,System.Int32,System.Int32)
extern void Oni_RemoveDeformableTriangles_mA0A4A28EA77A35743C37CF80A333559E7C8CC8AF (void);
// 0x00000057 System.Void Oni::SetConstraintGroupParameters(System.IntPtr,System.Int32,Oni/ConstraintParameters&)
extern void Oni_SetConstraintGroupParameters_m7402C64CA0198FD7D07E4F2BBFC81F8C9DB926B3 (void);
// 0x00000058 System.Void Oni::GetConstraintGroupParameters(System.IntPtr,System.Int32,Oni/ConstraintParameters&)
extern void Oni_GetConstraintGroupParameters_m841DD84B6D78A0B694506D815E4756313934B29F (void);
// 0x00000059 System.Void Oni::SetRestPositions(System.IntPtr,System.IntPtr)
extern void Oni_SetRestPositions_m819342E902C330762AEB5BE9D4C68F31DA6CA8A1 (void);
// 0x0000005A System.Void Oni::SetRestOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetRestOrientations_m87B3C65B2FA89627A6526A5C22B674280DC3C577 (void);
// 0x0000005B System.IntPtr Oni::CreateBatch(System.Int32)
extern void Oni_CreateBatch_m289C846274F339F1F3E4A026FE857DAAAC56ABE8 (void);
// 0x0000005C System.Void Oni::DestroyBatch(System.IntPtr)
extern void Oni_DestroyBatch_m0C065F052A4E0B16A426180BF8EC84E74845007E (void);
// 0x0000005D System.IntPtr Oni::AddBatch(System.IntPtr,System.IntPtr)
extern void Oni_AddBatch_m22AFEB35DB5CD69377763213EEED12183545A8DE (void);
// 0x0000005E System.Void Oni::RemoveBatch(System.IntPtr,System.IntPtr)
extern void Oni_RemoveBatch_m62A5EE513FF64B56579A035F1463C0308DD01A48 (void);
// 0x0000005F System.Boolean Oni::EnableBatch(System.IntPtr,System.Boolean)
extern void Oni_EnableBatch_mE5AAEE57C0386681A3016600946C205D90CCBD0B (void);
// 0x00000060 System.Int32 Oni::GetBatchConstraintForces(System.IntPtr,System.Single[],System.Int32,System.Int32)
extern void Oni_GetBatchConstraintForces_m2F4FB4C6732CFC54C859D7DB5C4A3AC75E4400FE (void);
// 0x00000061 System.Void Oni::SetBatchConstraintCount(System.IntPtr,System.Int32)
extern void Oni_SetBatchConstraintCount_m31FD93985340AF90A5D98742034D010888144B41 (void);
// 0x00000062 System.Int32 Oni::GetBatchConstraintCount(System.IntPtr)
extern void Oni_GetBatchConstraintCount_mCDA7F7C7B72B04711AAD3435FFE3AB6FDF657966 (void);
// 0x00000063 System.Void Oni::SetDistanceConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetDistanceConstraints_m6BAF3B1FE95B48E8E681121385077A369140BA88 (void);
// 0x00000064 System.Void Oni::SetBendingConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetBendingConstraints_m4DD1607554632126E50CA62B31DE223000DEE579 (void);
// 0x00000065 System.Void Oni::SetSkinConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetSkinConstraints_m980F260DC4107F86B16555CB40B9B63A196F558C (void);
// 0x00000066 System.Void Oni::SetAerodynamicConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetAerodynamicConstraints_m97BED4EFAF9B73A677757503EF1AF7EA59CB712C (void);
// 0x00000067 System.Void Oni::SetVolumeConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetVolumeConstraints_m7914A0DFEEF418BE4C6C81DE6A27EEC21701F511 (void);
// 0x00000068 System.Void Oni::SetShapeMatchingConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetShapeMatchingConstraints_mE2D86779FF689D2FBEB8F39FF1683CEAA9D7547E (void);
// 0x00000069 System.Void Oni::CalculateRestShapeMatching(System.IntPtr,System.IntPtr)
extern void Oni_CalculateRestShapeMatching_mD268677C88F2E8ABB2C0E8149F0683FFF9AF3BDD (void);
// 0x0000006A System.Void Oni::SetStretchShearConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetStretchShearConstraints_m49F7345551857151AE2572A3F59A90FCA236CDA8 (void);
// 0x0000006B System.Void Oni::SetBendTwistConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetBendTwistConstraints_mE4976251409BFB7882CC39AD1F21D4A97B7F4D03 (void);
// 0x0000006C System.Void Oni::SetTetherConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetTetherConstraints_m10810C34086C7751FB8A5600EFBBC9C70887449E (void);
// 0x0000006D System.Void Oni::SetPinConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetPinConstraints_mC5929BC4A3B8861A32C47072223BD7C88CAC8843 (void);
// 0x0000006E System.Void Oni::SetStitchConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetStitchConstraints_m8576B5213542C38E501BD55E1C0936DF69506253 (void);
// 0x0000006F System.Void Oni::SetChainConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetChainConstraints_m8F08E1B952B69C7A6D3F1957CA4511BE3D95D25D (void);
// 0x00000070 System.Void Oni::GetCollisionContacts(System.IntPtr,Oni/Contact[],System.Int32)
extern void Oni_GetCollisionContacts_mD80B55291B76FB42F97EE2C75EB2E5089540AE63 (void);
// 0x00000071 System.Void Oni::GetParticleCollisionContacts(System.IntPtr,Oni/Contact[],System.Int32)
extern void Oni_GetParticleCollisionContacts_mCDAC4DB2F8B1A798CEC15191044C4FE761AF69D1 (void);
// 0x00000072 System.Int32 Oni::InterpolateDiffuseParticles(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_InterpolateDiffuseParticles_m4A3F61BFA0B073204F17E45D12E58B292B3C5D44 (void);
// 0x00000073 System.Int32 Oni::MakePhase(System.Int32,Obi.ObiUtils/ParticleFlags)
extern void Oni_MakePhase_m9FFA8D86C974D669797A0DF8FF1E6A6270554D71 (void);
// 0x00000074 System.Int32 Oni::GetGroupFromPhase(System.Int32)
extern void Oni_GetGroupFromPhase_m1E9E33EEBF211AD24F224C4DDF044F55DEC4E6CF (void);
// 0x00000075 System.Int32 Oni::GetFlagsFromPhase(System.Int32)
extern void Oni_GetFlagsFromPhase_m4EC4AEB3B15B30111080A14FB54D7939A4924F0B (void);
// 0x00000076 System.Single Oni::BendingConstraintRest(System.Single[])
extern void Oni_BendingConstraintRest_mBF9E6E4CB4E998E291B956B4304FFB31A1ED6058 (void);
// 0x00000077 System.Void Oni::CompleteAll()
extern void Oni_CompleteAll_m24DA6FE9992164ABDA19A2E0E5BB58280FD02582 (void);
// 0x00000078 System.Void Oni::Complete(System.IntPtr)
extern void Oni_Complete_m4909573C5EF85056474F9EEB3A8C91F6F62050B5 (void);
// 0x00000079 System.IntPtr Oni::CreateEmpty()
extern void Oni_CreateEmpty_mEEB6A72C1C211426318B7A3164BD1697F1C0859C (void);
// 0x0000007A System.Void Oni::Schedule(System.IntPtr)
extern void Oni_Schedule_mF1BBBF5D226699D18E8EB55A8F31A08BE1DE528B (void);
// 0x0000007B System.Void Oni::AddChild(System.IntPtr,System.IntPtr)
extern void Oni_AddChild_m2AA3FE95D394A0AB211C7FAF46DA6DDA34645B49 (void);
// 0x0000007C System.Int32 Oni::GetMaxSystemConcurrency()
extern void Oni_GetMaxSystemConcurrency_mA467F4FF67759D65BBACC64B265BBD53DBEE3177 (void);
// 0x0000007D System.Void Oni::ClearProfiler()
extern void Oni_ClearProfiler_m3CE1E415966CFAA8CBF71A57B412B980CE3B5E11 (void);
// 0x0000007E System.Void Oni::EnableProfiler(System.Boolean)
extern void Oni_EnableProfiler_m50B45E5B063E18108D99A6851C2C441A8FEDCFD4 (void);
// 0x0000007F System.Void Oni::BeginSample(System.String,System.Byte)
extern void Oni_BeginSample_m584963073106DEA213EA5679DA42F89AB29B0931 (void);
// 0x00000080 System.Void Oni::EndSample()
extern void Oni_EndSample_mE3D8141E3F15F6DEEB30C16BDA9BC9E82368F8EE (void);
// 0x00000081 System.Int32 Oni::GetProfilingInfoCount()
extern void Oni_GetProfilingInfoCount_m89525656BF980E088FBF35150F29DC2BCAF974EB (void);
// 0x00000082 System.Void Oni::GetProfilingInfo(Oni/ProfileInfo[],System.Int32)
extern void Oni_GetProfilingInfo_m5450B203396A0BD0042264E198B358D3FA41BA23 (void);
// 0x00000083 System.Void Oni/SolverParameters::.ctor(Oni/SolverParameters/Interpolation,UnityEngine.Vector4)
extern void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3 (void);
// 0x00000084 System.Void Oni/ConstraintParameters::.ctor(System.Boolean,Oni/ConstraintParameters/EvaluationOrder,System.Int32)
extern void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566 (void);
// 0x00000085 System.Int32 Obi.IObiParticleCollection::get_particleCount()
// 0x00000086 System.Int32 Obi.IObiParticleCollection::get_activeParticleCount()
// 0x00000087 System.Boolean Obi.IObiParticleCollection::get_usesOrientedParticles()
// 0x00000088 System.Int32 Obi.IObiParticleCollection::GetParticleRuntimeIndex(System.Int32)
// 0x00000089 UnityEngine.Vector3 Obi.IObiParticleCollection::GetParticlePosition(System.Int32)
// 0x0000008A UnityEngine.Quaternion Obi.IObiParticleCollection::GetParticleOrientation(System.Int32)
// 0x0000008B System.Void Obi.IObiParticleCollection::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
// 0x0000008C System.Single Obi.IObiParticleCollection::GetParticleMaxRadius(System.Int32)
// 0x0000008D UnityEngine.Color Obi.IObiParticleCollection::GetParticleColor(System.Int32)
// 0x0000008E System.Void Obi.ObiActor::add_OnBlueprintLoaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_add_OnBlueprintLoaded_m04B2952178ABFA72F0A7E7219EB78A4ECEAE156E (void);
// 0x0000008F System.Void Obi.ObiActor::remove_OnBlueprintLoaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_remove_OnBlueprintLoaded_m998C17DB7FED249637AC6D40775D9D50ED1CA3F8 (void);
// 0x00000090 System.Void Obi.ObiActor::add_OnBlueprintUnloaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_add_OnBlueprintUnloaded_mB1182B14609EA8CCD5A9BD7C2F3CEDDAFF78C601 (void);
// 0x00000091 System.Void Obi.ObiActor::remove_OnBlueprintUnloaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_remove_OnBlueprintUnloaded_mB1AB8C2B322EB4633B0CD45EF5AAF7448BAD68BB (void);
// 0x00000092 System.Void Obi.ObiActor::add_OnPrepareFrame(Obi.ObiActor/ActorCallback)
extern void ObiActor_add_OnPrepareFrame_m3014BCA91C860098AA7A396A7FFCE7AB7CA1049E (void);
// 0x00000093 System.Void Obi.ObiActor::remove_OnPrepareFrame(Obi.ObiActor/ActorCallback)
extern void ObiActor_remove_OnPrepareFrame_m0D90685372167486999B646FC83D4E2C36C570AB (void);
// 0x00000094 System.Void Obi.ObiActor::add_OnPrepareStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnPrepareStep_mC586B960142DDDF81A5B1C87D393C7CA379E743E (void);
// 0x00000095 System.Void Obi.ObiActor::remove_OnPrepareStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnPrepareStep_mCB8B15D01E8DA343CF2DD8E655932C886DD863CC (void);
// 0x00000096 System.Void Obi.ObiActor::add_OnBeginStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnBeginStep_m7A401277ED988AF4C6388B1F768B4E613BA02E10 (void);
// 0x00000097 System.Void Obi.ObiActor::remove_OnBeginStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnBeginStep_m80E4C1C9DE59FB6075BFB27E9B8D885ADC413C13 (void);
// 0x00000098 System.Void Obi.ObiActor::add_OnSubstep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnSubstep_m4E244BDD4F0DA8AFBEF75036632A65CF4EC1286F (void);
// 0x00000099 System.Void Obi.ObiActor::remove_OnSubstep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnSubstep_mABFE386CEC0194553AEE608F465F311B21360592 (void);
// 0x0000009A System.Void Obi.ObiActor::add_OnEndStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnEndStep_mE226631AC3F2AA3BE52CCB4AB9972A0620E76208 (void);
// 0x0000009B System.Void Obi.ObiActor::remove_OnEndStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnEndStep_m18354446434FA0FE622CFA9F09B7D0D98B7365F9 (void);
// 0x0000009C System.Void Obi.ObiActor::add_OnInterpolate(Obi.ObiActor/ActorCallback)
extern void ObiActor_add_OnInterpolate_mA39BB43FF56C2FF95981971698C30856DC25ED65 (void);
// 0x0000009D System.Void Obi.ObiActor::remove_OnInterpolate(Obi.ObiActor/ActorCallback)
extern void ObiActor_remove_OnInterpolate_mE2B57508C1595198B4E27E10DA174A0B3792AD8D (void);
// 0x0000009E Obi.ObiSolver Obi.ObiActor::get_solver()
extern void ObiActor_get_solver_mDE668FD9EA16553E2252B51FC4AA0EE5F2476940 (void);
// 0x0000009F System.Boolean Obi.ObiActor::get_isLoaded()
extern void ObiActor_get_isLoaded_mE37E960D0EE5EF278241FE730E4E6D56FBBE217E (void);
// 0x000000A0 Obi.ObiCollisionMaterial Obi.ObiActor::get_collisionMaterial()
extern void ObiActor_get_collisionMaterial_mFEBA9610A92672D379939CCF56E5A07800EF7AB7 (void);
// 0x000000A1 System.Void Obi.ObiActor::set_collisionMaterial(Obi.ObiCollisionMaterial)
extern void ObiActor_set_collisionMaterial_m04C98A6B84C31070613652C4D12C3EFC0D98A0C4 (void);
// 0x000000A2 System.Boolean Obi.ObiActor::get_surfaceCollisions()
extern void ObiActor_get_surfaceCollisions_m280DE5273E14E28B1037C4CEA893F3BFFBEB2CB8 (void);
// 0x000000A3 System.Void Obi.ObiActor::set_surfaceCollisions(System.Boolean)
extern void ObiActor_set_surfaceCollisions_m40EE79424632CAE1EBA8FB1C3087D54FD9FC5046 (void);
// 0x000000A4 System.Int32 Obi.ObiActor::get_particleCount()
extern void ObiActor_get_particleCount_m8A511B9B8EE21FE09B45EA484ADE54CE1A469C24 (void);
// 0x000000A5 System.Int32 Obi.ObiActor::get_activeParticleCount()
extern void ObiActor_get_activeParticleCount_m330540C2CFF296103F759A43A156960A14EF40E3 (void);
// 0x000000A6 System.Boolean Obi.ObiActor::get_usesOrientedParticles()
extern void ObiActor_get_usesOrientedParticles_m1F5AC23A705419FFA18109ACE2CD45861FFCADDE (void);
// 0x000000A7 System.Boolean Obi.ObiActor::get_usesAnisotropicParticles()
extern void ObiActor_get_usesAnisotropicParticles_m54157FE27C8DBE561125BC87EA21BA034F246B69 (void);
// 0x000000A8 System.Boolean Obi.ObiActor::get_usesCustomExternalForces()
extern void ObiActor_get_usesCustomExternalForces_m63A0FBED9893C8176AA7DB6B5567978FB1D71DCA (void);
// 0x000000A9 UnityEngine.Matrix4x4 Obi.ObiActor::get_actorLocalToSolverMatrix()
extern void ObiActor_get_actorLocalToSolverMatrix_mCF3080C74BC381BF19F21BB6204A19DBDDFB82DE (void);
// 0x000000AA UnityEngine.Matrix4x4 Obi.ObiActor::get_actorSolverToLocalMatrix()
extern void ObiActor_get_actorSolverToLocalMatrix_m467C1F4C003B591FBE2C93BA9BDB33C0371962CE (void);
// 0x000000AB Obi.ObiActorBlueprint Obi.ObiActor::get_sourceBlueprint()
// 0x000000AC Obi.ObiActorBlueprint Obi.ObiActor::get_sharedBlueprint()
extern void ObiActor_get_sharedBlueprint_mE4DB059D49A26A1EF8D5AFD8CECEDB63CDACD863 (void);
// 0x000000AD Obi.ObiActorBlueprint Obi.ObiActor::get_blueprint()
extern void ObiActor_get_blueprint_m47A228582F43BE1A2AF98A4CFBF052C0DCF852F5 (void);
// 0x000000AE System.Void Obi.ObiActor::Awake()
extern void ObiActor_Awake_m241F3014CB9150A6234C8BBFE5F7F1506CC4A4A8 (void);
// 0x000000AF System.Void Obi.ObiActor::OnDestroy()
extern void ObiActor_OnDestroy_m2F3D4A01290E0E5EB2F3753AB9913AC96249A655 (void);
// 0x000000B0 System.Void Obi.ObiActor::OnEnable()
extern void ObiActor_OnEnable_m971216E2924B472ED6E1EF5CA46C20711919C967 (void);
// 0x000000B1 System.Void Obi.ObiActor::OnDisable()
extern void ObiActor_OnDisable_mCD8A0D978C2AD361EA37D62139DD083606463FD7 (void);
// 0x000000B2 System.Void Obi.ObiActor::OnValidate()
extern void ObiActor_OnValidate_m52B606D1428A6597252E13B0A7664A6A2A9AC196 (void);
// 0x000000B3 System.Void Obi.ObiActor::OnTransformParentChanged()
extern void ObiActor_OnTransformParentChanged_mE56EB6C5787A9B68CC2DCBF59A69FC5DBAC340B7 (void);
// 0x000000B4 System.Void Obi.ObiActor::AddToSolver()
extern void ObiActor_AddToSolver_m4612A91281D3B2EEA44B2654DF515E757FA4D3FF (void);
// 0x000000B5 System.Void Obi.ObiActor::RemoveFromSolver()
extern void ObiActor_RemoveFromSolver_m6E84D3DEBFE722DD4EC517EB2622FC80350D4FEA (void);
// 0x000000B6 System.Void Obi.ObiActor::SetSolver(Obi.ObiSolver)
extern void ObiActor_SetSolver_m6163A3076A049EAC233DCD2BB28B05C282DA55B6 (void);
// 0x000000B7 System.Void Obi.ObiActor::OnBlueprintRegenerate(Obi.ObiActorBlueprint)
extern void ObiActor_OnBlueprintRegenerate_mC7B6851CA42C438CCA449E6F6B675C88F4515C71 (void);
// 0x000000B8 System.Void Obi.ObiActor::UpdateCollisionMaterials()
extern void ObiActor_UpdateCollisionMaterials_m5E96C2AE2B9F7811085F85D0D25A5621C46D6817 (void);
// 0x000000B9 System.Boolean Obi.ObiActor::CopyParticle(System.Int32,System.Int32)
extern void ObiActor_CopyParticle_m8AA520BF621655A1D7FD60BE63769B097E1996D6 (void);
// 0x000000BA System.Void Obi.ObiActor::TeleportParticle(System.Int32,UnityEngine.Vector3)
extern void ObiActor_TeleportParticle_mD6150ED98C32F82A0ADB734DDE58A37BA0B78E39 (void);
// 0x000000BB System.Void Obi.ObiActor::Teleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ObiActor_Teleport_mE63A190E1F4170CF5E1BE19522E3B6061AE96A04 (void);
// 0x000000BC System.Void Obi.ObiActor::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiActor_SwapWithFirstInactiveParticle_mC6593AA19F1C4AF00040F46D4EC723008B97B52B (void);
// 0x000000BD System.Boolean Obi.ObiActor::ActivateParticle(System.Int32)
extern void ObiActor_ActivateParticle_m45FF9EA8B2788D3C9EAE0EDE77DA4FA2133BF9BA (void);
// 0x000000BE System.Boolean Obi.ObiActor::DeactivateParticle(System.Int32)
extern void ObiActor_DeactivateParticle_m04DDF95A73A8AE09950848D4A8DBB2A57EC42C59 (void);
// 0x000000BF System.Boolean Obi.ObiActor::IsParticleActive(System.Int32)
extern void ObiActor_IsParticleActive_mFD14C2BCE7E86131404B2EEDA413555B7288C0E3 (void);
// 0x000000C0 System.Void Obi.ObiActor::SetSelfCollisions(System.Boolean)
extern void ObiActor_SetSelfCollisions_m45F0B0F5D1A4306D161C7EEC3C1435D9D52E3686 (void);
// 0x000000C1 System.Void Obi.ObiActor::SetOneSided(System.Boolean)
extern void ObiActor_SetOneSided_m99DA6DD46C17B2357C1815E76EA6FB70B6525D33 (void);
// 0x000000C2 System.Void Obi.ObiActor::SetSimplicesDirty()
extern void ObiActor_SetSimplicesDirty_m1FA63CB84FD2F2219BB54CB69CC5E8473FE2167A (void);
// 0x000000C3 System.Void Obi.ObiActor::SetConstraintsDirty(Oni/ConstraintType)
extern void ObiActor_SetConstraintsDirty_mF1D5DEE2405AE6F94AE264FC9CD0A63C31189D0F (void);
// 0x000000C4 Obi.IObiConstraints Obi.ObiActor::GetConstraintsByType(Oni/ConstraintType)
extern void ObiActor_GetConstraintsByType_mA485A63E65B2E5A863562BA515B15F6DE11138E7 (void);
// 0x000000C5 System.Void Obi.ObiActor::UpdateParticleProperties()
extern void ObiActor_UpdateParticleProperties_m9B2C616C0275DB62285F9E0CB0F1FF615D9FFAFF (void);
// 0x000000C6 System.Int32 Obi.ObiActor::GetParticleRuntimeIndex(System.Int32)
extern void ObiActor_GetParticleRuntimeIndex_m4AECF54626524359EE8CA930022DDFA864946B0D (void);
// 0x000000C7 UnityEngine.Vector3 Obi.ObiActor::GetParticlePosition(System.Int32)
extern void ObiActor_GetParticlePosition_m02493A97C489BA4E448F1B3BE3AAB59DB1E9039A (void);
// 0x000000C8 UnityEngine.Quaternion Obi.ObiActor::GetParticleOrientation(System.Int32)
extern void ObiActor_GetParticleOrientation_mC35CD07B08A81AC2159E02DF583118B7ACF8E9C3 (void);
// 0x000000C9 System.Void Obi.ObiActor::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
extern void ObiActor_GetParticleAnisotropy_mC8BAD8C2ACCEB1DF3284AEF13CC5E12ABEFF727A (void);
// 0x000000CA System.Single Obi.ObiActor::GetParticleMaxRadius(System.Int32)
extern void ObiActor_GetParticleMaxRadius_m834AF87A1D3204613AC0A6658EDBAF54871C13BD (void);
// 0x000000CB UnityEngine.Color Obi.ObiActor::GetParticleColor(System.Int32)
extern void ObiActor_GetParticleColor_m08A638A989F579EFADEA293B32C74635EECD25EA (void);
// 0x000000CC System.Void Obi.ObiActor::SetFilterCategory(System.Int32)
extern void ObiActor_SetFilterCategory_mAE60934D51EBD397225139EE05F2E80F08118D7D (void);
// 0x000000CD System.Void Obi.ObiActor::SetFilterMask(System.Int32)
extern void ObiActor_SetFilterMask_m7FED73FD99F34632090C9174E5EB278DC5D76F08 (void);
// 0x000000CE System.Void Obi.ObiActor::SetMass(System.Single)
extern void ObiActor_SetMass_m8D79A26F4FBEF1A895C6F482808A23A02B8BFAC4 (void);
// 0x000000CF System.Single Obi.ObiActor::GetMass(UnityEngine.Vector3&)
extern void ObiActor_GetMass_m2E9973F1EE3CBB300948F36489B7A7637B768F98 (void);
// 0x000000D0 System.Void Obi.ObiActor::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void ObiActor_AddForce_mE87CF6C0A21C03ED45209758E7BFE0E1AB2B1A00 (void);
// 0x000000D1 System.Void Obi.ObiActor::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void ObiActor_AddTorque_m43A494B40EA87E0EC7A6274A0931F9CF27B5D77D (void);
// 0x000000D2 System.Void Obi.ObiActor::LoadBlueprintParticles(Obi.ObiActorBlueprint,System.Int32)
extern void ObiActor_LoadBlueprintParticles_m11D7669681BB9F35863E2BE6B9B50F9C7F8CFE88 (void);
// 0x000000D3 System.Void Obi.ObiActor::UnloadBlueprintParticles()
extern void ObiActor_UnloadBlueprintParticles_mAFE44CA41C0A763A6142420F2145AAEDCFD296A3 (void);
// 0x000000D4 System.Void Obi.ObiActor::ResetParticles()
extern void ObiActor_ResetParticles_m081DAAF528B4EF4F9E79AA17ECBEFC2C0BCB03D2 (void);
// 0x000000D5 System.Void Obi.ObiActor::SaveStateToBlueprint(Obi.ObiActorBlueprint)
extern void ObiActor_SaveStateToBlueprint_m86BB211764276B407A42BF4C603D75AEBD260F84 (void);
// 0x000000D6 System.Void Obi.ObiActor::StoreState()
extern void ObiActor_StoreState_mDF522A37AED6A366CD98C42E6D12EB524E5A6B67 (void);
// 0x000000D7 System.Void Obi.ObiActor::ClearState()
extern void ObiActor_ClearState_m2708E40E0CC1259F0A4A23A134D7C08A34D64D9E (void);
// 0x000000D8 System.Void Obi.ObiActor::LoadBlueprint(Obi.ObiSolver)
extern void ObiActor_LoadBlueprint_mD8204B2EFE860EADB85ABAAEA380D0EB7DE52966 (void);
// 0x000000D9 System.Void Obi.ObiActor::UnloadBlueprint(Obi.ObiSolver)
extern void ObiActor_UnloadBlueprint_m0380FCD5E41E56876B933D34F94EE112FE49D14E (void);
// 0x000000DA System.Void Obi.ObiActor::PrepareFrame()
extern void ObiActor_PrepareFrame_mB2B1C5E2FB1A0570C0B7527B5654C17ADE677A53 (void);
// 0x000000DB System.Void Obi.ObiActor::PrepareStep(System.Single)
extern void ObiActor_PrepareStep_mB06CED03B998A63A4AC43A5318D99D1AE8ABA400 (void);
// 0x000000DC System.Void Obi.ObiActor::BeginStep(System.Single)
extern void ObiActor_BeginStep_m2CB8338B9754937B0F151D9F06AF97263B2D313F (void);
// 0x000000DD System.Void Obi.ObiActor::Substep(System.Single)
extern void ObiActor_Substep_m784F85C0BD4464D6E4B1BCE24E44047956C78486 (void);
// 0x000000DE System.Void Obi.ObiActor::EndStep(System.Single)
extern void ObiActor_EndStep_m5076BADB8970EC68040EA131738946988F140513 (void);
// 0x000000DF System.Void Obi.ObiActor::Interpolate()
extern void ObiActor_Interpolate_m589CB41AB063846C5C621944F1B14EACB90C3683 (void);
// 0x000000E0 System.Void Obi.ObiActor::OnSolverVisibilityChanged(System.Boolean)
extern void ObiActor_OnSolverVisibilityChanged_mA9B03F298B37CD3BB66DD5527F9E3CDB43A728EB (void);
// 0x000000E1 System.Void Obi.ObiActor::.ctor()
extern void ObiActor__ctor_mE992775E0773F687C886A5C462E2EAB7FA5D740E (void);
// 0x000000E2 Obi.ObiSolver Obi.ObiActor/ObiActorSolverArgs::get_solver()
extern void ObiActorSolverArgs_get_solver_m2C65EFA043F915BB25BF5EE5F467EA1119C55BDF (void);
// 0x000000E3 System.Void Obi.ObiActor/ObiActorSolverArgs::.ctor(Obi.ObiSolver)
extern void ObiActorSolverArgs__ctor_m645D2D61FF1A0780D4B70C2494C8C90607C7FA1B (void);
// 0x000000E4 System.Void Obi.ObiActor/ActorCallback::.ctor(System.Object,System.IntPtr)
extern void ActorCallback__ctor_mA6CF95A70DAB4FFF21823741E58A598FD0A3CC61 (void);
// 0x000000E5 System.Void Obi.ObiActor/ActorCallback::Invoke(Obi.ObiActor)
extern void ActorCallback_Invoke_m44385DCB93F7AC483E18B7477BDEB1C44993D508 (void);
// 0x000000E6 System.IAsyncResult Obi.ObiActor/ActorCallback::BeginInvoke(Obi.ObiActor,System.AsyncCallback,System.Object)
extern void ActorCallback_BeginInvoke_m511CEA8353354A1869B3548D98C3EA166C757E30 (void);
// 0x000000E7 System.Void Obi.ObiActor/ActorCallback::EndInvoke(System.IAsyncResult)
extern void ActorCallback_EndInvoke_m44778EF05F7C946EE7ACD0315B4F14F101980024 (void);
// 0x000000E8 System.Void Obi.ObiActor/ActorStepCallback::.ctor(System.Object,System.IntPtr)
extern void ActorStepCallback__ctor_m48119AF13D841B3D5476543A1C79108F94D53E00 (void);
// 0x000000E9 System.Void Obi.ObiActor/ActorStepCallback::Invoke(Obi.ObiActor,System.Single)
extern void ActorStepCallback_Invoke_m25120AB25E0465B7AF5976D51946EED8935E2A38 (void);
// 0x000000EA System.IAsyncResult Obi.ObiActor/ActorStepCallback::BeginInvoke(Obi.ObiActor,System.Single,System.AsyncCallback,System.Object)
extern void ActorStepCallback_BeginInvoke_mE4197C4E139BFCC95434E2C3E20BE882EB5F2989 (void);
// 0x000000EB System.Void Obi.ObiActor/ActorStepCallback::EndInvoke(System.IAsyncResult)
extern void ActorStepCallback_EndInvoke_m8AA8AAD3C0E68BB1AF5F7E6A61EA2269F3A57AFB (void);
// 0x000000EC System.Void Obi.ObiActor/ActorBlueprintCallback::.ctor(System.Object,System.IntPtr)
extern void ActorBlueprintCallback__ctor_mADC829707A4F631EADEE895A25D2AAEA59A4D64D (void);
// 0x000000ED System.Void Obi.ObiActor/ActorBlueprintCallback::Invoke(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ActorBlueprintCallback_Invoke_m7BFF8526B4E3B31969016C044E63EA9AF3FFE7AD (void);
// 0x000000EE System.IAsyncResult Obi.ObiActor/ActorBlueprintCallback::BeginInvoke(Obi.ObiActor,Obi.ObiActorBlueprint,System.AsyncCallback,System.Object)
extern void ActorBlueprintCallback_BeginInvoke_m32D286D241BEC1F0C4FD9CF6C04C5E7BF1823485 (void);
// 0x000000EF System.Void Obi.ObiActor/ActorBlueprintCallback::EndInvoke(System.IAsyncResult)
extern void ActorBlueprintCallback_EndInvoke_m7B651D23DA455A93DCC87B95CCE21A5789433013 (void);
// 0x000000F0 System.Int32 Obi.IColliderWorldImpl::get_referenceCount()
// 0x000000F1 System.Void Obi.IColliderWorldImpl::UpdateWorld(System.Single)
// 0x000000F2 System.Void Obi.IColliderWorldImpl::SetColliders(Obi.ObiNativeColliderShapeList,Obi.ObiNativeAabbList,Obi.ObiNativeAffineTransformList,System.Int32)
// 0x000000F3 System.Void Obi.IColliderWorldImpl::SetRigidbodies(Obi.ObiNativeRigidbodyList)
// 0x000000F4 System.Void Obi.IColliderWorldImpl::SetCollisionMaterials(Obi.ObiNativeCollisionMaterialList)
// 0x000000F5 System.Void Obi.IColliderWorldImpl::SetTriangleMeshData(Obi.ObiNativeTriangleMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeTriangleList,Obi.ObiNativeVector3List)
// 0x000000F6 System.Void Obi.IColliderWorldImpl::SetEdgeMeshData(Obi.ObiNativeEdgeMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeEdgeList,Obi.ObiNativeVector2List)
// 0x000000F7 System.Void Obi.IColliderWorldImpl::SetDistanceFieldData(Obi.ObiNativeDistanceFieldHeaderList,Obi.ObiNativeDFNodeList)
// 0x000000F8 System.Void Obi.IColliderWorldImpl::SetHeightFieldData(Obi.ObiNativeHeightFieldHeaderList,Obi.ObiNativeFloatList)
// 0x000000F9 System.Void Obi.IAerodynamicConstraintsBatchImpl::SetAerodynamicConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,System.Int32)
// 0x000000FA System.Void Obi.IBendConstraintsBatchImpl::SetBendConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x000000FB System.Void Obi.IBendTwistConstraintsBatchImpl::SetBendTwistConstraints(Obi.ObiNativeIntList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x000000FC System.Void Obi.IChainConstraintsBatchImpl::SetChainConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeIntList,Obi.ObiNativeIntList,System.Int32)
// 0x000000FD Oni/ConstraintType Obi.IConstraintsBatchImpl::get_constraintType()
// 0x000000FE Obi.IConstraints Obi.IConstraintsBatchImpl::get_constraints()
// 0x000000FF System.Void Obi.IConstraintsBatchImpl::set_enabled(System.Boolean)
// 0x00000100 System.Boolean Obi.IConstraintsBatchImpl::get_enabled()
// 0x00000101 System.Void Obi.IConstraintsBatchImpl::Destroy()
// 0x00000102 System.Void Obi.IConstraintsBatchImpl::SetConstraintCount(System.Int32)
// 0x00000103 System.Int32 Obi.IConstraintsBatchImpl::GetConstraintCount()
// 0x00000104 Oni/ConstraintType Obi.IConstraints::get_constraintType()
// 0x00000105 Obi.ISolverImpl Obi.IConstraints::get_solver()
// 0x00000106 System.Int32 Obi.IConstraints::GetConstraintCount()
// 0x00000107 System.Void Obi.IDistanceConstraintsBatchImpl::SetDistanceConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x00000108 System.Void Obi.IPinConstraintsBatchImpl::SetPinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x00000109 System.Void Obi.IShapeMatchingConstraintsBatchImpl::SetShapeMatchingConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeFloatList,System.Int32)
// 0x0000010A System.Void Obi.IShapeMatchingConstraintsBatchImpl::CalculateRestShapeMatching()
// 0x0000010B System.Void Obi.ISkinConstraintsBatchImpl::SetSkinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x0000010C System.Void Obi.IStitchConstraintsBatchImpl::SetStitchConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x0000010D System.Void Obi.IStretchShearConstraintsBatchImpl::SetStretchShearConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeFloatList,System.Int32)
// 0x0000010E System.Void Obi.ITetherConstraintsBatchImpl::SetTetherConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x0000010F System.Void Obi.IVolumeConstraintsBatchImpl::SetVolumeConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x00000110 Obi.ISolverImpl Obi.IObiBackend::CreateSolver(Obi.ObiSolver,System.Int32)
// 0x00000111 System.Void Obi.IObiBackend::DestroySolver(Obi.ISolverImpl)
// 0x00000112 System.Void Obi.IObiJobHandle::Complete()
// 0x00000113 System.Void Obi.ISolverImpl::Destroy()
// 0x00000114 System.Void Obi.ISolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
// 0x00000115 System.Void Obi.ISolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
// 0x00000116 System.Void Obi.ISolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
// 0x00000117 System.Void Obi.ISolverImpl::ParticleCountChanged(Obi.ObiSolver)
// 0x00000118 System.Void Obi.ISolverImpl::SetActiveParticles(System.Int32[],System.Int32)
// 0x00000119 System.Void Obi.ISolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
// 0x0000011A System.Void Obi.ISolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
// 0x0000011B Obi.IConstraintsBatchImpl Obi.ISolverImpl::CreateConstraintsBatch(Oni/ConstraintType)
// 0x0000011C System.Void Obi.ISolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
// 0x0000011D System.Int32 Obi.ISolverImpl::GetConstraintCount(Oni/ConstraintType)
// 0x0000011E System.Void Obi.ISolverImpl::GetCollisionContacts(Oni/Contact[],System.Int32)
// 0x0000011F System.Void Obi.ISolverImpl::GetParticleCollisionContacts(Oni/Contact[],System.Int32)
// 0x00000120 System.Void Obi.ISolverImpl::SetConstraintGroupParameters(Oni/ConstraintType,Oni/ConstraintParameters&)
// 0x00000121 Obi.IObiJobHandle Obi.ISolverImpl::CollisionDetection(System.Single)
// 0x00000122 Obi.IObiJobHandle Obi.ISolverImpl::Substep(System.Single,System.Single,System.Int32)
// 0x00000123 System.Void Obi.ISolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
// 0x00000124 System.Int32 Obi.ISolverImpl::GetDeformableTriangleCount()
// 0x00000125 System.Void Obi.ISolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
// 0x00000126 System.Int32 Obi.ISolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
// 0x00000127 System.Void Obi.ISolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
// 0x00000128 System.Void Obi.ISolverImpl::SetParameters(Oni/SolverParameters)
// 0x00000129 System.Void Obi.ISolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
// 0x0000012A System.Void Obi.ISolverImpl::ResetForces()
// 0x0000012B System.Int32 Obi.ISolverImpl::GetParticleGridSize()
// 0x0000012C System.Void Obi.ISolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
// 0x0000012D System.Void Obi.ISolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
// 0x0000012E Obi.ISolverImpl Obi.NullBackend::CreateSolver(Obi.ObiSolver,System.Int32)
extern void NullBackend_CreateSolver_m149A74012C4CF9DEC9EC68F1C16EE99F3D6E88AF (void);
// 0x0000012F System.Void Obi.NullBackend::DestroySolver(Obi.ISolverImpl)
extern void NullBackend_DestroySolver_m5E51947F4BEC3D57534FD796732535ED2B83D3F6 (void);
// 0x00000130 System.Void Obi.NullBackend::.ctor()
extern void NullBackend__ctor_m7C4DE499807A5119E6914F1A7B6666E7BBFE44A9 (void);
// 0x00000131 System.Void Obi.NullSolverImpl::Destroy()
extern void NullSolverImpl_Destroy_mCEF74F887CB2BF0E13C8176A052B1539F51BD6AB (void);
// 0x00000132 System.Void Obi.NullSolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
extern void NullSolverImpl_InitializeFrame_m1A9943BBB1200D17394A300365060CC1C180C333 (void);
// 0x00000133 System.Void Obi.NullSolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
extern void NullSolverImpl_UpdateFrame_mE9842C5421EB8BCEF09BEF66B7ECE4C6BBF68453 (void);
// 0x00000134 System.Void Obi.NullSolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
extern void NullSolverImpl_ApplyFrame_m6E1688FD679EF4B17A6C885CD116D47885BA9A6D (void);
// 0x00000135 System.Int32 Obi.NullSolverImpl::GetDeformableTriangleCount()
extern void NullSolverImpl_GetDeformableTriangleCount_m5B5658F9B5D43BC6CF7131229FE97D16E931E5AB (void);
// 0x00000136 System.Void Obi.NullSolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
extern void NullSolverImpl_SetDeformableTriangles_m3555C196B17530814FC47EB0D65B0B19428AE73E (void);
// 0x00000137 System.Int32 Obi.NullSolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
extern void NullSolverImpl_RemoveDeformableTriangles_m327A23C82D523DB2799DC060C88746468C9CB012 (void);
// 0x00000138 System.Void Obi.NullSolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
extern void NullSolverImpl_SetSimplices_m31A164287A45DD5E59CD0C787A24C85DAB4C3D40 (void);
// 0x00000139 System.Void Obi.NullSolverImpl::ParticleCountChanged(Obi.ObiSolver)
extern void NullSolverImpl_ParticleCountChanged_m8557EBD352BB202123699487E00B843F3CA0DC0B (void);
// 0x0000013A System.Void Obi.NullSolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
extern void NullSolverImpl_SetRigidbodyArrays_mEC64908F48D6E3EE3F03975F21201DAD33E36848 (void);
// 0x0000013B System.Void Obi.NullSolverImpl::SetActiveParticles(System.Int32[],System.Int32)
extern void NullSolverImpl_SetActiveParticles_m270CC166EEFCBB7494E52DFD1F7385B091AFB05B (void);
// 0x0000013C System.Void Obi.NullSolverImpl::ResetForces()
extern void NullSolverImpl_ResetForces_m02F4803EE6AB50D5563B60A00E5535B12F8E3B11 (void);
// 0x0000013D System.Void Obi.NullSolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void NullSolverImpl_GetBounds_m4D24F01134C67A078F93680525FB8DC5804A3DBB (void);
// 0x0000013E System.Void Obi.NullSolverImpl::SetParameters(Oni/SolverParameters)
extern void NullSolverImpl_SetParameters_mDCC55383F8800DB49278298ABF8A176E6F5B3CAA (void);
// 0x0000013F System.Int32 Obi.NullSolverImpl::GetConstraintCount(Oni/ConstraintType)
extern void NullSolverImpl_GetConstraintCount_mA5540FE96215F61ADEFE829931F4D927E70019E0 (void);
// 0x00000140 System.Void Obi.NullSolverImpl::GetCollisionContacts(Oni/Contact[],System.Int32)
extern void NullSolverImpl_GetCollisionContacts_mD2C3B675F93A95F33D2472E11AD4F48C503B2B54 (void);
// 0x00000141 System.Void Obi.NullSolverImpl::GetParticleCollisionContacts(Oni/Contact[],System.Int32)
extern void NullSolverImpl_GetParticleCollisionContacts_m55885C928BC24A887BD63F783CD979F085E07381 (void);
// 0x00000142 System.Void Obi.NullSolverImpl::SetConstraintGroupParameters(Oni/ConstraintType,Oni/ConstraintParameters&)
extern void NullSolverImpl_SetConstraintGroupParameters_m3A8213C3F85B7A2187B6030CC6EDC4DE5CB505BE (void);
// 0x00000143 Obi.IConstraintsBatchImpl Obi.NullSolverImpl::CreateConstraintsBatch(Oni/ConstraintType)
extern void NullSolverImpl_CreateConstraintsBatch_mB98D773275819E868529278DC289D6E084CC913D (void);
// 0x00000144 System.Void Obi.NullSolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
extern void NullSolverImpl_DestroyConstraintsBatch_m42D46BA79047F3A0DBD2B305549E97FA654EF2B7 (void);
// 0x00000145 Obi.IObiJobHandle Obi.NullSolverImpl::CollisionDetection(System.Single)
extern void NullSolverImpl_CollisionDetection_m3BE61763707C061EB9912C7E9DCD44C3F135C0FB (void);
// 0x00000146 Obi.IObiJobHandle Obi.NullSolverImpl::Substep(System.Single,System.Single,System.Int32)
extern void NullSolverImpl_Substep_m46BCCC5D64A69DD461D4EF42A3D227A2F53A6A08 (void);
// 0x00000147 System.Void Obi.NullSolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
extern void NullSolverImpl_ApplyInterpolation_mB63837B2AE0542F306D50ABF7A05868F2A9A75BC (void);
// 0x00000148 System.Void Obi.NullSolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
extern void NullSolverImpl_InterpolateDiffuseProperties_m5D4052E47F4C0CE896A55E4932A7AB35F0001133 (void);
// 0x00000149 System.Int32 Obi.NullSolverImpl::GetParticleGridSize()
extern void NullSolverImpl_GetParticleGridSize_mD74EA2D59BC1B4472754D76EFF3B4FBC969B2A66 (void);
// 0x0000014A System.Void Obi.NullSolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
extern void NullSolverImpl_GetParticleGrid_mCBCC36302C00CB01775017E21F63754021E04BEC (void);
// 0x0000014B System.Void Obi.NullSolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void NullSolverImpl_SpatialQuery_m252FFC078850F330BB8276D6996CD2303F90D6F1 (void);
// 0x0000014C System.Void Obi.NullSolverImpl::.ctor()
extern void NullSolverImpl__ctor_m835D73EC52194B7861F10BCEC2C8F3D30E98D6EF (void);
// 0x0000014D System.Void Obi.OniAerodynamicConstraintsBatchImpl::.ctor(Obi.OniAerodynamicConstraintsImpl)
extern void OniAerodynamicConstraintsBatchImpl__ctor_m08CF6D0353900E2F5161D01B30D6D4FD181EAF67 (void);
// 0x0000014E System.Void Obi.OniAerodynamicConstraintsBatchImpl::SetAerodynamicConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,System.Int32)
extern void OniAerodynamicConstraintsBatchImpl_SetAerodynamicConstraints_m78BC6505E02E608B6E7E64C31C785C0653DCCDD5 (void);
// 0x0000014F System.Void Obi.OniAerodynamicConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniAerodynamicConstraintsImpl__ctor_m525B8236B34A2BEB8A129BC2DFCAD6433E10673D (void);
// 0x00000150 Obi.IConstraintsBatchImpl Obi.OniAerodynamicConstraintsImpl::CreateConstraintsBatch()
extern void OniAerodynamicConstraintsImpl_CreateConstraintsBatch_m3C15118FE42A98BE00C420EFF5CC108133E8C9D1 (void);
// 0x00000151 System.Void Obi.OniAerodynamicConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniAerodynamicConstraintsImpl_RemoveBatch_m7312BFD8301F75F44EF449DAB1CA208CDF256820 (void);
// 0x00000152 System.Void Obi.OniBendConstraintsBatchImpl::.ctor(Obi.OniBendConstraintsImpl)
extern void OniBendConstraintsBatchImpl__ctor_mD888A9E984F69E1F75358F7C87F224E5BBFEF9C1 (void);
// 0x00000153 System.Void Obi.OniBendConstraintsBatchImpl::SetBendConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniBendConstraintsBatchImpl_SetBendConstraints_mACD9168310968682EC7F9FAF4FAF01D439561251 (void);
// 0x00000154 System.Void Obi.OniBendConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniBendConstraintsImpl__ctor_mDF7E828956758CC442DFCE530D6ACB3DA28C7520 (void);
// 0x00000155 Obi.IConstraintsBatchImpl Obi.OniBendConstraintsImpl::CreateConstraintsBatch()
extern void OniBendConstraintsImpl_CreateConstraintsBatch_mE943BA310CF0508FA4B457AE9C4CEDE436C0256C (void);
// 0x00000156 System.Void Obi.OniBendConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniBendConstraintsImpl_RemoveBatch_mE65A138E34D866312EA8D6811377B0D2A5A8F759 (void);
// 0x00000157 System.Void Obi.OniBendTwistConstraintsBatchImpl::.ctor(Obi.OniBendTwistConstraintsImpl)
extern void OniBendTwistConstraintsBatchImpl__ctor_m8F131CD09F6F7F09BBDA55AF3E953E5EA2D1F52F (void);
// 0x00000158 System.Void Obi.OniBendTwistConstraintsBatchImpl::SetBendTwistConstraints(Obi.ObiNativeIntList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniBendTwistConstraintsBatchImpl_SetBendTwistConstraints_m93208190B4C9F21B66804826F3F388365464C7E6 (void);
// 0x00000159 System.Void Obi.OniBendTwistConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniBendTwistConstraintsImpl__ctor_m2D810DA4120C0AB84298B4A542CA5B932E774DCB (void);
// 0x0000015A Obi.IConstraintsBatchImpl Obi.OniBendTwistConstraintsImpl::CreateConstraintsBatch()
extern void OniBendTwistConstraintsImpl_CreateConstraintsBatch_m5F1FC4850DB914A408732085F9AAEB1FE7D9DEF9 (void);
// 0x0000015B System.Void Obi.OniBendTwistConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniBendTwistConstraintsImpl_RemoveBatch_m571856C1E93A0B2507FD9309A2984CAD3143BB55 (void);
// 0x0000015C System.Void Obi.OniChainConstraintsBatchImpl::.ctor(Obi.OniChainConstraintsImpl)
extern void OniChainConstraintsBatchImpl__ctor_m11A76775D8DA7D09B7DACBF1D1168D4FEF4F1694 (void);
// 0x0000015D System.Void Obi.OniChainConstraintsBatchImpl::SetChainConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeIntList,Obi.ObiNativeIntList,System.Int32)
extern void OniChainConstraintsBatchImpl_SetChainConstraints_mA704113F26DE40220400DE5B85B1475C2B3D0A82 (void);
// 0x0000015E System.Void Obi.OniChainConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniChainConstraintsImpl__ctor_mAE3D234A2597D468A4621529AECFC4CD2D1BB328 (void);
// 0x0000015F Obi.IConstraintsBatchImpl Obi.OniChainConstraintsImpl::CreateConstraintsBatch()
extern void OniChainConstraintsImpl_CreateConstraintsBatch_m84FCCD1FC3FF8E6DE2FF68D3B259A0AF6E595B9D (void);
// 0x00000160 System.Void Obi.OniChainConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniChainConstraintsImpl_RemoveBatch_mD1FF35D0BD359EB12FC8A74DE8A560E487A9057B (void);
// 0x00000161 System.Void Obi.OniDistanceConstraintsBatchImpl::.ctor(Obi.OniDistanceConstraintsImpl)
extern void OniDistanceConstraintsBatchImpl__ctor_m9005CD0907A9E6BACBBDA3777B6717B8611AFA18 (void);
// 0x00000162 System.Void Obi.OniDistanceConstraintsBatchImpl::SetDistanceConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniDistanceConstraintsBatchImpl_SetDistanceConstraints_m2305BC0CB077A2F81D031356D983305E8344B34B (void);
// 0x00000163 System.Void Obi.OniDistanceConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniDistanceConstraintsImpl__ctor_m46EF17FABD8A1BFB1EDE24EDE0F6EB61473907F8 (void);
// 0x00000164 Obi.IConstraintsBatchImpl Obi.OniDistanceConstraintsImpl::CreateConstraintsBatch()
extern void OniDistanceConstraintsImpl_CreateConstraintsBatch_m68ACFFA63F7B3ACF12200ACCDD9796BC1E00453D (void);
// 0x00000165 System.Void Obi.OniDistanceConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniDistanceConstraintsImpl_RemoveBatch_m4F114348DEFF33909A2AFCF15D10D98187F7912C (void);
// 0x00000166 System.IntPtr Obi.OniConstraintsBatchImpl::get_oniBatch()
extern void OniConstraintsBatchImpl_get_oniBatch_mD3068ADFBFA72D2C5AEC1E4493AF44A61BAAB3D6 (void);
// 0x00000167 Oni/ConstraintType Obi.OniConstraintsBatchImpl::get_constraintType()
extern void OniConstraintsBatchImpl_get_constraintType_mA4D89C531D6E50262AAB808ED481F01FCBC4554F (void);
// 0x00000168 Obi.IConstraints Obi.OniConstraintsBatchImpl::get_constraints()
extern void OniConstraintsBatchImpl_get_constraints_mC86F66AEBA1CCFE2EE1484C2CA0844257A7A0CCD (void);
// 0x00000169 System.Void Obi.OniConstraintsBatchImpl::set_enabled(System.Boolean)
extern void OniConstraintsBatchImpl_set_enabled_mE3FE79E17C8786437000CF9DF7ED7CC24C58DD1F (void);
// 0x0000016A System.Boolean Obi.OniConstraintsBatchImpl::get_enabled()
extern void OniConstraintsBatchImpl_get_enabled_m74C3931C9C466399F511D1258F6DF49FAF007D8F (void);
// 0x0000016B System.Void Obi.OniConstraintsBatchImpl::.ctor(Obi.IConstraints,Oni/ConstraintType)
extern void OniConstraintsBatchImpl__ctor_m7E856D436221DA306E7EFF8409F799DC8F926C38 (void);
// 0x0000016C System.Void Obi.OniConstraintsBatchImpl::Destroy()
extern void OniConstraintsBatchImpl_Destroy_mB934F5194DA3A35DAC45581D183BE8C1BB056A1A (void);
// 0x0000016D System.Void Obi.OniConstraintsBatchImpl::SetConstraintCount(System.Int32)
extern void OniConstraintsBatchImpl_SetConstraintCount_m9A9C7B081DBCAFE727F2682F742B32A0A5CD1FC7 (void);
// 0x0000016E System.Int32 Obi.OniConstraintsBatchImpl::GetConstraintCount()
extern void OniConstraintsBatchImpl_GetConstraintCount_mBC3CD543643EBE713BEDBCEC5266DE52F0E6E53E (void);
// 0x0000016F Obi.IConstraintsBatchImpl Obi.IOniConstraintsImpl::CreateConstraintsBatch()
// 0x00000170 System.Void Obi.IOniConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
// 0x00000171 Obi.ISolverImpl Obi.OniConstraintsImpl::get_solver()
extern void OniConstraintsImpl_get_solver_m44585FB563C805AF280120F7FDE3B5737F4C0411 (void);
// 0x00000172 Oni/ConstraintType Obi.OniConstraintsImpl::get_constraintType()
extern void OniConstraintsImpl_get_constraintType_mF0F59F28976DF29B32C94C4156BD7752105087CC (void);
// 0x00000173 System.Void Obi.OniConstraintsImpl::.ctor(Obi.OniSolverImpl,Oni/ConstraintType)
extern void OniConstraintsImpl__ctor_m9A0E8C6A195C96B8CB7EF27D99B7A0988672BF61 (void);
// 0x00000174 Obi.IConstraintsBatchImpl Obi.OniConstraintsImpl::CreateConstraintsBatch()
// 0x00000175 System.Void Obi.OniConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
// 0x00000176 System.Int32 Obi.OniConstraintsImpl::GetConstraintCount()
extern void OniConstraintsImpl_GetConstraintCount_mDE65EE94DCFAB8905A9AEF5543E7119928F2900A (void);
// 0x00000177 System.Void Obi.OniPinConstraintsBatchImpl::.ctor(Obi.OniPinConstraintsImpl)
extern void OniPinConstraintsBatchImpl__ctor_mA95003D5E91633FA72AA1F912FFCC947CBFC25CF (void);
// 0x00000178 System.Void Obi.OniPinConstraintsBatchImpl::SetPinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniPinConstraintsBatchImpl_SetPinConstraints_mB28FD52B804E30D4B06AADE5A8764C9F3F4DC223 (void);
// 0x00000179 System.Void Obi.OniPinConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniPinConstraintsImpl__ctor_m2F0E0AF0C30CE5AA9BBA5B0F2C170582A8442ABE (void);
// 0x0000017A Obi.IConstraintsBatchImpl Obi.OniPinConstraintsImpl::CreateConstraintsBatch()
extern void OniPinConstraintsImpl_CreateConstraintsBatch_mF0305992D09B680091BD1EEF5519A801F19F69D0 (void);
// 0x0000017B System.Void Obi.OniPinConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniPinConstraintsImpl_RemoveBatch_m1EA7BE417BEF16951FAD0CA1106DB5BC257F38E1 (void);
// 0x0000017C System.Void Obi.OniShapeMatchingConstraintsBatchImpl::.ctor(Obi.OniShapeMatchingConstraintsImpl)
extern void OniShapeMatchingConstraintsBatchImpl__ctor_mA35A5ADBE7591DFAFCD6775D89CD7C7BBEB7E25D (void);
// 0x0000017D System.Void Obi.OniShapeMatchingConstraintsBatchImpl::SetShapeMatchingConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeFloatList,System.Int32)
extern void OniShapeMatchingConstraintsBatchImpl_SetShapeMatchingConstraints_mDDBCB5CFAB68DDF0E19581524119A7981D214588 (void);
// 0x0000017E System.Void Obi.OniShapeMatchingConstraintsBatchImpl::CalculateRestShapeMatching()
extern void OniShapeMatchingConstraintsBatchImpl_CalculateRestShapeMatching_m6E5073835BB3D1CED559A45D987CC3FCD9D4C21D (void);
// 0x0000017F System.Void Obi.OniShapeMatchingConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniShapeMatchingConstraintsImpl__ctor_m945AA881838FE9644266E98CA531247FE004C1D7 (void);
// 0x00000180 Obi.IConstraintsBatchImpl Obi.OniShapeMatchingConstraintsImpl::CreateConstraintsBatch()
extern void OniShapeMatchingConstraintsImpl_CreateConstraintsBatch_mB055C2DDEB21A68DC4E3A56E2A989231D1109BBD (void);
// 0x00000181 System.Void Obi.OniShapeMatchingConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniShapeMatchingConstraintsImpl_RemoveBatch_m0C9FC7FFB2680541A78037980C49C907F3598756 (void);
// 0x00000182 System.Void Obi.OniSkinConstraintsBatchImpl::.ctor(Obi.OniSkinConstraintsImpl)
extern void OniSkinConstraintsBatchImpl__ctor_m5DDA5D3F347C6EC9FF501C52A4374AD88A10AABD (void);
// 0x00000183 System.Void Obi.OniSkinConstraintsBatchImpl::SetSkinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniSkinConstraintsBatchImpl_SetSkinConstraints_m8065C2605A9D036F95C2F1B234883144F9C84CD3 (void);
// 0x00000184 System.Void Obi.OniSkinConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniSkinConstraintsImpl__ctor_m9A29E8FD8F9F99978DFF2EB7149829624A7939A5 (void);
// 0x00000185 Obi.IConstraintsBatchImpl Obi.OniSkinConstraintsImpl::CreateConstraintsBatch()
extern void OniSkinConstraintsImpl_CreateConstraintsBatch_m9FB2EAD640C26270C35A54351F354F8AA1D7E043 (void);
// 0x00000186 System.Void Obi.OniSkinConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniSkinConstraintsImpl_RemoveBatch_mC0102FC9F40941F1DDEBCDA28632CBDD43660523 (void);
// 0x00000187 System.Void Obi.OniStitchConstraintsBatchImpl::.ctor(Obi.OniStitchConstraintsImpl)
extern void OniStitchConstraintsBatchImpl__ctor_mC565125A40106134B8A526F21E536F819AACDB34 (void);
// 0x00000188 System.Void Obi.OniStitchConstraintsBatchImpl::SetStitchConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniStitchConstraintsBatchImpl_SetStitchConstraints_m850BB5F12FBB10D113073FFD360406906FE84F55 (void);
// 0x00000189 System.Void Obi.OniStitchConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniStitchConstraintsImpl__ctor_m5A0F9B6CD43ECC7D7AE65AD107B559178559308C (void);
// 0x0000018A Obi.IConstraintsBatchImpl Obi.OniStitchConstraintsImpl::CreateConstraintsBatch()
extern void OniStitchConstraintsImpl_CreateConstraintsBatch_m94119E21DE281CD292B556C5077176B4D9C9284F (void);
// 0x0000018B System.Void Obi.OniStitchConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniStitchConstraintsImpl_RemoveBatch_m7B1ED49DA5FB278734D0C9F8597E820B3E284FAC (void);
// 0x0000018C System.Void Obi.OniStretchShearConstraintsBatchImpl::.ctor(Obi.OniStretchShearConstraintsImpl)
extern void OniStretchShearConstraintsBatchImpl__ctor_m06A2F02D467608368981ECD3800852E4B1229908 (void);
// 0x0000018D System.Void Obi.OniStretchShearConstraintsBatchImpl::SetStretchShearConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeFloatList,System.Int32)
extern void OniStretchShearConstraintsBatchImpl_SetStretchShearConstraints_mACC9CB76D4FDD53A4FDD96FA2031EBD5F893B6AA (void);
// 0x0000018E System.Void Obi.OniStretchShearConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniStretchShearConstraintsImpl__ctor_m743767DB7C25C334E9584FACAF87392343A39ACC (void);
// 0x0000018F Obi.IConstraintsBatchImpl Obi.OniStretchShearConstraintsImpl::CreateConstraintsBatch()
extern void OniStretchShearConstraintsImpl_CreateConstraintsBatch_m78706F1FEE7FEAA5EF471FAB390E0E0D71861127 (void);
// 0x00000190 System.Void Obi.OniStretchShearConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniStretchShearConstraintsImpl_RemoveBatch_mF8F67B57D02D49DEC0DC0197146B243A44090457 (void);
// 0x00000191 System.Void Obi.OniTetherConstraintsBatchImpl::.ctor(Obi.OniTetherConstraintsImpl)
extern void OniTetherConstraintsBatchImpl__ctor_mA18D6CEBB82288A3B578C8C4C52F4E0119DAB0B4 (void);
// 0x00000192 System.Void Obi.OniTetherConstraintsBatchImpl::SetTetherConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniTetherConstraintsBatchImpl_SetTetherConstraints_mCA9A516027A94559796B9C29CD716F10CB4BEFD7 (void);
// 0x00000193 System.Void Obi.OniTetherConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniTetherConstraintsImpl__ctor_m79D07270D5F4E7E62395AA8BE2B0F5B2C5B9AD25 (void);
// 0x00000194 Obi.IConstraintsBatchImpl Obi.OniTetherConstraintsImpl::CreateConstraintsBatch()
extern void OniTetherConstraintsImpl_CreateConstraintsBatch_mD49AD0DA234A2CBAF481401273DC88CF16D87AD7 (void);
// 0x00000195 System.Void Obi.OniTetherConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniTetherConstraintsImpl_RemoveBatch_m4B4909C5425342565704DFD35EDD47C5A1E2FB20 (void);
// 0x00000196 System.Void Obi.OniVolumeConstraintsBatchImpl::.ctor(Obi.OniVolumeConstraintsImpl)
extern void OniVolumeConstraintsBatchImpl__ctor_m17E79E7DCFE92922A8861BE569C3FF460D175CAE (void);
// 0x00000197 System.Void Obi.OniVolumeConstraintsBatchImpl::SetVolumeConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniVolumeConstraintsBatchImpl_SetVolumeConstraints_m9C580C7D330A95E2032129920D22CC13D70E8AFC (void);
// 0x00000198 System.Void Obi.OniVolumeConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniVolumeConstraintsImpl__ctor_m5147F9E64F13F4D205515CE6F1906887F787BA74 (void);
// 0x00000199 Obi.IConstraintsBatchImpl Obi.OniVolumeConstraintsImpl::CreateConstraintsBatch()
extern void OniVolumeConstraintsImpl_CreateConstraintsBatch_mF7765774D05626A213C729351AE71CF664A0A8B2 (void);
// 0x0000019A System.Void Obi.OniVolumeConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniVolumeConstraintsImpl_RemoveBatch_m726F19C3F55138E1BB32C8D3C97804E44013547C (void);
// 0x0000019B Obi.ISolverImpl Obi.OniBackend::CreateSolver(Obi.ObiSolver,System.Int32)
extern void OniBackend_CreateSolver_m5E3C44A4EA9BCDD470E7E10E684F0AF897C8AF5F (void);
// 0x0000019C System.Void Obi.OniBackend::DestroySolver(Obi.ISolverImpl)
extern void OniBackend_DestroySolver_mC6AFB023EC9F9532FF2B840DDF955E89C71A0D01 (void);
// 0x0000019D System.Void Obi.OniBackend::GetOrCreateColliderWorld()
extern void OniBackend_GetOrCreateColliderWorld_m92C5F279126E62C80B84C7ACAC6FEB209751EF1D (void);
// 0x0000019E System.Void Obi.OniBackend::.ctor()
extern void OniBackend__ctor_m45B8DE94A53B0740C6704A51A4BC4FDEB7C7D4F7 (void);
// 0x0000019F System.Int32 Obi.OniColliderWorld::get_referenceCount()
extern void OniColliderWorld_get_referenceCount_m2E1D878672ADF99B997AA9EF3050BD41B27DC9FD (void);
// 0x000001A0 System.Void Obi.OniColliderWorld::Awake()
extern void OniColliderWorld_Awake_mBB4F8B9A43CB88C337CD9DB344E0B7169D47E923 (void);
// 0x000001A1 System.Void Obi.OniColliderWorld::OnDestroy()
extern void OniColliderWorld_OnDestroy_mAF83AA5D931A38B0E55E2226B94527B592FD9D83 (void);
// 0x000001A2 System.Void Obi.OniColliderWorld::IncreaseReferenceCount()
extern void OniColliderWorld_IncreaseReferenceCount_m78FCC3BFF315F84942D3994F5092A54A8D7F678E (void);
// 0x000001A3 System.Void Obi.OniColliderWorld::DecreaseReferenceCount()
extern void OniColliderWorld_DecreaseReferenceCount_m35AE9297895C720AC03E0D0F320DDAD1366BF64C (void);
// 0x000001A4 System.Void Obi.OniColliderWorld::UpdateWorld(System.Single)
extern void OniColliderWorld_UpdateWorld_m07BB5250B08FE6F9B66FBDC04C2B54A98C4B4251 (void);
// 0x000001A5 System.Void Obi.OniColliderWorld::SetColliders(Obi.ObiNativeColliderShapeList,Obi.ObiNativeAabbList,Obi.ObiNativeAffineTransformList,System.Int32)
extern void OniColliderWorld_SetColliders_mBC49AA3ED5E83E70CE733DCEC0683FF672872263 (void);
// 0x000001A6 System.Void Obi.OniColliderWorld::SetRigidbodies(Obi.ObiNativeRigidbodyList)
extern void OniColliderWorld_SetRigidbodies_m28F9E1B58302DB5A756C243EF52E788E708DA85F (void);
// 0x000001A7 System.Void Obi.OniColliderWorld::SetCollisionMaterials(Obi.ObiNativeCollisionMaterialList)
extern void OniColliderWorld_SetCollisionMaterials_mEB26C8AC30F1B5CE27D8F19A77ABB7B252115E0D (void);
// 0x000001A8 System.Void Obi.OniColliderWorld::SetTriangleMeshData(Obi.ObiNativeTriangleMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeTriangleList,Obi.ObiNativeVector3List)
extern void OniColliderWorld_SetTriangleMeshData_m4965F3D38925BFCACB96B6B3479DD4F232ED7104 (void);
// 0x000001A9 System.Void Obi.OniColliderWorld::SetEdgeMeshData(Obi.ObiNativeEdgeMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeEdgeList,Obi.ObiNativeVector2List)
extern void OniColliderWorld_SetEdgeMeshData_mD9F54F0603E44D518BC9243FF1979EC24E25FD44 (void);
// 0x000001AA System.Void Obi.OniColliderWorld::SetDistanceFieldData(Obi.ObiNativeDistanceFieldHeaderList,Obi.ObiNativeDFNodeList)
extern void OniColliderWorld_SetDistanceFieldData_m2C1ADF6E94752BC41CE632A804CBAE1AC19A9EDF (void);
// 0x000001AB System.Void Obi.OniColliderWorld::SetHeightFieldData(Obi.ObiNativeHeightFieldHeaderList,Obi.ObiNativeFloatList)
extern void OniColliderWorld_SetHeightFieldData_m4C593F78531A7AE191FA9F8D4A8BBFC23E84F68A (void);
// 0x000001AC System.Void Obi.OniColliderWorld::.ctor()
extern void OniColliderWorld__ctor_m4DE663B94B5C376427CEB42288BA88428E549A52 (void);
// 0x000001AD System.Void Obi.OniJobHandle::.ctor(System.IntPtr)
extern void OniJobHandle__ctor_m3C938A79AA35165ECEC272FDB9E229430994231B (void);
// 0x000001AE System.Void Obi.OniJobHandle::Complete()
extern void OniJobHandle_Complete_mB9E3D94D50CF207A0F961C9F4ADEB30B2FA8A6DE (void);
// 0x000001AF System.IntPtr Obi.OniSolverImpl::get_oniSolver()
extern void OniSolverImpl_get_oniSolver_mA021288BE5461FA7F14399158965ECDD9ECA1706 (void);
// 0x000001B0 System.Void Obi.OniSolverImpl::.ctor(System.IntPtr)
extern void OniSolverImpl__ctor_m2FE0413D6AF7939DBE480B789D43360A39DDFAD4 (void);
// 0x000001B1 System.Void Obi.OniSolverImpl::Destroy()
extern void OniSolverImpl_Destroy_m317E88AD3A919043461CE59F399DF3670A904961 (void);
// 0x000001B2 System.Void Obi.OniSolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
extern void OniSolverImpl_InitializeFrame_mC8E921CBD6A70A2F9F46310EB5556586B6749DC7 (void);
// 0x000001B3 System.Void Obi.OniSolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
extern void OniSolverImpl_UpdateFrame_m52DA894215FDD134640AD29208E780EEBD65180A (void);
// 0x000001B4 System.Void Obi.OniSolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
extern void OniSolverImpl_ApplyFrame_m67E28FA4B4FBE5C072E359F3574F562C59820DB4 (void);
// 0x000001B5 System.Int32 Obi.OniSolverImpl::GetDeformableTriangleCount()
extern void OniSolverImpl_GetDeformableTriangleCount_m230CCC63F4275668FF4EF1B34F56859D5C70E68F (void);
// 0x000001B6 System.Void Obi.OniSolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
extern void OniSolverImpl_SetDeformableTriangles_mE67CB6225F084DE826F48AA4D27E43FA43BE9827 (void);
// 0x000001B7 System.Int32 Obi.OniSolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
extern void OniSolverImpl_RemoveDeformableTriangles_m3B80FB7C6FA7DB0890B5BC37670151643F54C650 (void);
// 0x000001B8 System.Void Obi.OniSolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
extern void OniSolverImpl_SetSimplices_mD571A7AFB950F771AE96A6A30743D2E7CE5BAE5B (void);
// 0x000001B9 System.Void Obi.OniSolverImpl::ParticleCountChanged(Obi.ObiSolver)
extern void OniSolverImpl_ParticleCountChanged_mC72028DD674A3E54C4E08E758A3CE3D26E3E7577 (void);
// 0x000001BA System.Void Obi.OniSolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
extern void OniSolverImpl_SetRigidbodyArrays_mFA7655D4B61E7E1C65C4E42C8B77EA55DFC70AEE (void);
// 0x000001BB System.Void Obi.OniSolverImpl::SetActiveParticles(System.Int32[],System.Int32)
extern void OniSolverImpl_SetActiveParticles_m850250C4A4DE24AB05AB59AADCD43D6F232077E8 (void);
// 0x000001BC System.Void Obi.OniSolverImpl::ResetForces()
extern void OniSolverImpl_ResetForces_m5E8F36B6FCA63BD1E720DBFD5B8764863E3CE626 (void);
// 0x000001BD System.Void Obi.OniSolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void OniSolverImpl_GetBounds_m478CF8A2FAB372FE347E9899D262869163ACCB57 (void);
// 0x000001BE System.Void Obi.OniSolverImpl::SetParameters(Oni/SolverParameters)
extern void OniSolverImpl_SetParameters_mF506A029DB22FF674610C76D892D387BAD7178A5 (void);
// 0x000001BF System.Int32 Obi.OniSolverImpl::GetConstraintCount(Oni/ConstraintType)
extern void OniSolverImpl_GetConstraintCount_m7C95C3E195679C7076DEBAB096213874408B03EF (void);
// 0x000001C0 System.Void Obi.OniSolverImpl::GetCollisionContacts(Oni/Contact[],System.Int32)
extern void OniSolverImpl_GetCollisionContacts_m3179A5E075C13134C753E47D58F150C004D15906 (void);
// 0x000001C1 System.Void Obi.OniSolverImpl::GetParticleCollisionContacts(Oni/Contact[],System.Int32)
extern void OniSolverImpl_GetParticleCollisionContacts_m4B99957B57A3E6BAF05C9E3D64DF28B5E0748259 (void);
// 0x000001C2 System.Void Obi.OniSolverImpl::SetConstraintGroupParameters(Oni/ConstraintType,Oni/ConstraintParameters&)
extern void OniSolverImpl_SetConstraintGroupParameters_mB58C6320D75A44B682B4AEA82214F8ACA62E53DA (void);
// 0x000001C3 Obi.IConstraintsBatchImpl Obi.OniSolverImpl::CreateConstraintsBatch(Oni/ConstraintType)
extern void OniSolverImpl_CreateConstraintsBatch_m388EEDF658EF8F63CD78DC8DC33F0DC642688911 (void);
// 0x000001C4 System.Void Obi.OniSolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
extern void OniSolverImpl_DestroyConstraintsBatch_m7769244161551FB17B31600D453AC2F68F156860 (void);
// 0x000001C5 Obi.IObiJobHandle Obi.OniSolverImpl::CollisionDetection(System.Single)
extern void OniSolverImpl_CollisionDetection_m56F7F7DC6C224E1DEE5CF8A1437ED6FE02BAB5E2 (void);
// 0x000001C6 Obi.IObiJobHandle Obi.OniSolverImpl::Substep(System.Single,System.Single,System.Int32)
extern void OniSolverImpl_Substep_mCA3F4A59045D0A506692A48C78ACE6BB4E884D93 (void);
// 0x000001C7 System.Void Obi.OniSolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
extern void OniSolverImpl_ApplyInterpolation_mAD4820F8A797FF35107C1C920D5B50D4415C67AD (void);
// 0x000001C8 System.Void Obi.OniSolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
extern void OniSolverImpl_InterpolateDiffuseProperties_m0678F52DF465E6E9B34DECE91C6C3D48F96E8821 (void);
// 0x000001C9 System.Int32 Obi.OniSolverImpl::GetParticleGridSize()
extern void OniSolverImpl_GetParticleGridSize_mC858C1BFF95D86B1B0841E40378F882A014E8E78 (void);
// 0x000001CA System.Void Obi.OniSolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
extern void OniSolverImpl_GetParticleGrid_m3A454F1EA5EDCF6A590F99A29EEAC84F0D65B790 (void);
// 0x000001CB System.Void Obi.OniSolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void OniSolverImpl_SpatialQuery_m6BD4BCC1AACE17D8D66365F9FE7F16012A1E8F7F (void);
// 0x000001CC System.Single Obi.IStructuralConstraintBatch::GetRestLength(System.Int32)
// 0x000001CD System.Void Obi.IStructuralConstraintBatch::SetRestLength(System.Int32,System.Single)
// 0x000001CE Obi.ParticlePair Obi.IStructuralConstraintBatch::GetParticleIndices(System.Int32)
// 0x000001CF Oni/ConstraintType Obi.ObiAerodynamicConstraintsBatch::get_constraintType()
extern void ObiAerodynamicConstraintsBatch_get_constraintType_mBC6B3C472BCF899A37C3C9F15B61F0B22DB160F4 (void);
// 0x000001D0 Obi.IConstraintsBatchImpl Obi.ObiAerodynamicConstraintsBatch::get_implementation()
extern void ObiAerodynamicConstraintsBatch_get_implementation_mD6185053ABF60610B561CE15B51770F6E2556E08 (void);
// 0x000001D1 System.Void Obi.ObiAerodynamicConstraintsBatch::.ctor(Obi.ObiAerodynamicConstraintsData)
extern void ObiAerodynamicConstraintsBatch__ctor_m9DE858EA294B273AEED18201A289AEC50F28F02E (void);
// 0x000001D2 System.Void Obi.ObiAerodynamicConstraintsBatch::AddConstraint(System.Int32,System.Single,System.Single,System.Single)
extern void ObiAerodynamicConstraintsBatch_AddConstraint_mB9A927126FB700DC1E84DDA91D9132310EFEE124 (void);
// 0x000001D3 System.Void Obi.ObiAerodynamicConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiAerodynamicConstraintsBatch_GetParticlesInvolved_m430311A6B436224B845F2F196525AD41A67EB605 (void);
// 0x000001D4 System.Void Obi.ObiAerodynamicConstraintsBatch::Clear()
extern void ObiAerodynamicConstraintsBatch_Clear_m5ACDB0CA34591C9F454153521B67AC2478467EA2 (void);
// 0x000001D5 System.Void Obi.ObiAerodynamicConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiAerodynamicConstraintsBatch_SwapConstraints_m3F24E8A02E9F7E06130C7E27A8438623CBC36B99 (void);
// 0x000001D6 System.Void Obi.ObiAerodynamicConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiAerodynamicConstraintsBatch_Merge_m4CEBEA6CE4426002B0326CFCAE2C25E3F4954CD0 (void);
// 0x000001D7 System.Void Obi.ObiAerodynamicConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiAerodynamicConstraintsBatch_AddToSolver_mF2BA5925F8D160264B0EC1995C4EF9856E4E48FB (void);
// 0x000001D8 System.Void Obi.ObiAerodynamicConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiAerodynamicConstraintsBatch_RemoveFromSolver_m3288141E2907B24EF4337F9C513C6C69D68D8684 (void);
// 0x000001D9 Oni/ConstraintType Obi.ObiBendConstraintsBatch::get_constraintType()
extern void ObiBendConstraintsBatch_get_constraintType_mA302627388787213E045E481476B7C9EB7F43D92 (void);
// 0x000001DA Obi.IConstraintsBatchImpl Obi.ObiBendConstraintsBatch::get_implementation()
extern void ObiBendConstraintsBatch_get_implementation_mE6CC10221A772592842A23E0EE91871FAA8ED5FA (void);
// 0x000001DB System.Void Obi.ObiBendConstraintsBatch::.ctor(Obi.ObiBendConstraintsData)
extern void ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9 (void);
// 0x000001DC System.Void Obi.ObiBendConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiBendConstraintsBatch_Merge_mC7EBF91A7220B4C03792E048ABB718337C6F03AA (void);
// 0x000001DD System.Void Obi.ObiBendConstraintsBatch::AddConstraint(UnityEngine.Vector3Int,System.Single)
extern void ObiBendConstraintsBatch_AddConstraint_m59872A1ADDD39CF3FB2C303EA08FD4E6E9AFAA0C (void);
// 0x000001DE System.Void Obi.ObiBendConstraintsBatch::Clear()
extern void ObiBendConstraintsBatch_Clear_mA0DC32AE2AE98E09AF547DE49FB25D8CDFF0DB97 (void);
// 0x000001DF System.Void Obi.ObiBendConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiBendConstraintsBatch_GetParticlesInvolved_mA9C31748143F96BC5E341BE711B68E9F78204C4A (void);
// 0x000001E0 System.Void Obi.ObiBendConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiBendConstraintsBatch_SwapConstraints_mD42CD848B36CEB0540B9F211A1C7592348949487 (void);
// 0x000001E1 System.Void Obi.ObiBendConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiBendConstraintsBatch_AddToSolver_m65C74A506765E2DFFD6575EE0E9D8AF1F723EBC1 (void);
// 0x000001E2 System.Void Obi.ObiBendConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiBendConstraintsBatch_RemoveFromSolver_m771E799A2D261FFC3AB2686EB70676ABEBCE21F7 (void);
// 0x000001E3 Oni/ConstraintType Obi.ObiBendTwistConstraintsBatch::get_constraintType()
extern void ObiBendTwistConstraintsBatch_get_constraintType_mAC26F20B9CE45F7ACE966652B0ACAB8337A9703D (void);
// 0x000001E4 Obi.IConstraintsBatchImpl Obi.ObiBendTwistConstraintsBatch::get_implementation()
extern void ObiBendTwistConstraintsBatch_get_implementation_m293D659D51624728D74EB2EC1BEF2EBA93A301D2 (void);
// 0x000001E5 System.Void Obi.ObiBendTwistConstraintsBatch::.ctor(Obi.ObiBendTwistConstraintsData)
extern void ObiBendTwistConstraintsBatch__ctor_mA84FE1A64BC385B93B82ACDB515147D1BFD9D8E9 (void);
// 0x000001E6 System.Void Obi.ObiBendTwistConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,UnityEngine.Quaternion)
extern void ObiBendTwistConstraintsBatch_AddConstraint_m04DEFCAA48B91E8849ECC4FB1F151C17740B3E78 (void);
// 0x000001E7 System.Void Obi.ObiBendTwistConstraintsBatch::Clear()
extern void ObiBendTwistConstraintsBatch_Clear_mEA7D8BB6BCCA8C72D2CB42CF1D9C197F3C3FB905 (void);
// 0x000001E8 System.Void Obi.ObiBendTwistConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiBendTwistConstraintsBatch_GetParticlesInvolved_m7F039D1FBFA40A6E0EFFB3F66FAA2A5D4D621393 (void);
// 0x000001E9 System.Void Obi.ObiBendTwistConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiBendTwistConstraintsBatch_SwapConstraints_m19754F194196E8509DE60D4C6A4E01CDE61951CC (void);
// 0x000001EA System.Void Obi.ObiBendTwistConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiBendTwistConstraintsBatch_Merge_m65D2EB86911176AE0DBCFC755950330B0FF4D9C4 (void);
// 0x000001EB System.Void Obi.ObiBendTwistConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiBendTwistConstraintsBatch_AddToSolver_m00517D42056B5BD7E11AF70F1C617FD33E313B35 (void);
// 0x000001EC System.Void Obi.ObiBendTwistConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiBendTwistConstraintsBatch_RemoveFromSolver_m89781A48762FBA02AFF8BE9423981220F26A147B (void);
// 0x000001ED Oni/ConstraintType Obi.ObiChainConstraintsBatch::get_constraintType()
extern void ObiChainConstraintsBatch_get_constraintType_mBAD7C6C6A0862F3A03015BCA2BA2B969706672C7 (void);
// 0x000001EE Obi.IConstraintsBatchImpl Obi.ObiChainConstraintsBatch::get_implementation()
extern void ObiChainConstraintsBatch_get_implementation_m2883B97D7E6175B2C1FE458CD3E667789C1FEC92 (void);
// 0x000001EF System.Void Obi.ObiChainConstraintsBatch::.ctor(Obi.ObiChainConstraintsData)
extern void ObiChainConstraintsBatch__ctor_m8E8E6310A3DDA3527AFF7F0DF6A851F75B52AEDC (void);
// 0x000001F0 System.Void Obi.ObiChainConstraintsBatch::AddConstraint(System.Int32[],System.Single,System.Single,System.Single)
extern void ObiChainConstraintsBatch_AddConstraint_m8615D25C5C8628CEE00BD28F9D2D465A635813CB (void);
// 0x000001F1 System.Void Obi.ObiChainConstraintsBatch::Clear()
extern void ObiChainConstraintsBatch_Clear_mC5BA1FD8A165468C43063493EF8BFB93A3D2CE21 (void);
// 0x000001F2 System.Void Obi.ObiChainConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiChainConstraintsBatch_GetParticlesInvolved_m1B0852EE90B8C0089C7793F4915B9D5C5E739187 (void);
// 0x000001F3 System.Void Obi.ObiChainConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiChainConstraintsBatch_SwapConstraints_m56826B5809EDF1D888AC022EC80813AB5E8A8029 (void);
// 0x000001F4 System.Void Obi.ObiChainConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiChainConstraintsBatch_Merge_m7C8E59CC49A14B6A3F20B923CB9983A18709369E (void);
// 0x000001F5 System.Void Obi.ObiChainConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiChainConstraintsBatch_AddToSolver_m8E12FD5F4ACAA95E5FFA4E57A15C6D01796354AA (void);
// 0x000001F6 System.Void Obi.ObiChainConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiChainConstraintsBatch_RemoveFromSolver_m97FD3AAF49F7296DC655C1BE2B0C866D40330AAB (void);
// 0x000001F7 System.Int32 Obi.IObiConstraintsBatch::get_constraintCount()
// 0x000001F8 System.Int32 Obi.IObiConstraintsBatch::get_activeConstraintCount()
// 0x000001F9 System.Void Obi.IObiConstraintsBatch::set_activeConstraintCount(System.Int32)
// 0x000001FA System.Int32 Obi.IObiConstraintsBatch::get_initialActiveConstraintCount()
// 0x000001FB System.Void Obi.IObiConstraintsBatch::set_initialActiveConstraintCount(System.Int32)
// 0x000001FC Oni/ConstraintType Obi.IObiConstraintsBatch::get_constraintType()
// 0x000001FD Obi.IConstraintsBatchImpl Obi.IObiConstraintsBatch::get_implementation()
// 0x000001FE System.Void Obi.IObiConstraintsBatch::AddToSolver(Obi.ObiSolver)
// 0x000001FF System.Void Obi.IObiConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
// 0x00000200 System.Void Obi.IObiConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
// 0x00000201 System.Boolean Obi.IObiConstraintsBatch::DeactivateConstraint(System.Int32)
// 0x00000202 System.Boolean Obi.IObiConstraintsBatch::ActivateConstraint(System.Int32)
// 0x00000203 System.Void Obi.IObiConstraintsBatch::DeactivateAllConstraints()
// 0x00000204 System.Void Obi.IObiConstraintsBatch::Clear()
// 0x00000205 System.Void Obi.IObiConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
// 0x00000206 System.Void Obi.IObiConstraintsBatch::ParticlesSwapped(System.Int32,System.Int32)
// 0x00000207 System.Int32 Obi.ObiConstraintsBatch::get_constraintCount()
extern void ObiConstraintsBatch_get_constraintCount_mA8FC55D7B67C6DE94D5B7383C39D4330CEC3C4A8 (void);
// 0x00000208 System.Int32 Obi.ObiConstraintsBatch::get_activeConstraintCount()
extern void ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02 (void);
// 0x00000209 System.Void Obi.ObiConstraintsBatch::set_activeConstraintCount(System.Int32)
extern void ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A (void);
// 0x0000020A System.Int32 Obi.ObiConstraintsBatch::get_initialActiveConstraintCount()
extern void ObiConstraintsBatch_get_initialActiveConstraintCount_m5F0841BDCCB934AB148E2A870F3EAA2F8552189B (void);
// 0x0000020B System.Void Obi.ObiConstraintsBatch::set_initialActiveConstraintCount(System.Int32)
extern void ObiConstraintsBatch_set_initialActiveConstraintCount_mF8081C7DCFDC4CF1B2A306517632F6E9C026912C (void);
// 0x0000020C Oni/ConstraintType Obi.ObiConstraintsBatch::get_constraintType()
// 0x0000020D Obi.IConstraintsBatchImpl Obi.ObiConstraintsBatch::get_implementation()
// 0x0000020E System.Void Obi.ObiConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiConstraintsBatch_Merge_m0157340350A52C6574BBB2D68D612AA6852ADEC3 (void);
// 0x0000020F System.Void Obi.ObiConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
// 0x00000210 System.Void Obi.ObiConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
// 0x00000211 System.Void Obi.ObiConstraintsBatch::AddToSolver(Obi.ObiSolver)
// 0x00000212 System.Void Obi.ObiConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
// 0x00000213 System.Void Obi.ObiConstraintsBatch::CopyConstraint(Obi.ObiConstraintsBatch,System.Int32)
extern void ObiConstraintsBatch_CopyConstraint_m204FC07814B69BA73E78A0E743D059F565B85874 (void);
// 0x00000214 System.Void Obi.ObiConstraintsBatch::InnerSwapConstraints(System.Int32,System.Int32)
extern void ObiConstraintsBatch_InnerSwapConstraints_m72121F6E08F53AA93A94891FB98F3B6FF2D23A91 (void);
// 0x00000215 System.Void Obi.ObiConstraintsBatch::RegisterConstraint()
extern void ObiConstraintsBatch_RegisterConstraint_m8EABAFCE01074053D942CE05D9CF5CEC9381F5FB (void);
// 0x00000216 System.Void Obi.ObiConstraintsBatch::Clear()
extern void ObiConstraintsBatch_Clear_mC782D142E91B8F81F511A011339846EC1310C2D5 (void);
// 0x00000217 System.Int32 Obi.ObiConstraintsBatch::GetConstraintIndex(System.Int32)
extern void ObiConstraintsBatch_GetConstraintIndex_mA7193601AF174D3A9323DCBC41B86CF2F9EDF24B (void);
// 0x00000218 System.Boolean Obi.ObiConstraintsBatch::IsConstraintActive(System.Int32)
extern void ObiConstraintsBatch_IsConstraintActive_m86CFCB36A5A1AEBBE1FA877DFEE386BB244A7C65 (void);
// 0x00000219 System.Boolean Obi.ObiConstraintsBatch::ActivateConstraint(System.Int32)
extern void ObiConstraintsBatch_ActivateConstraint_m715C1DA819EA321555CC3E50E102E4130B0DF12E (void);
// 0x0000021A System.Boolean Obi.ObiConstraintsBatch::DeactivateConstraint(System.Int32)
extern void ObiConstraintsBatch_DeactivateConstraint_m75EE8E2E70006545791D2BA14E1E4513F9193025 (void);
// 0x0000021B System.Void Obi.ObiConstraintsBatch::DeactivateAllConstraints()
extern void ObiConstraintsBatch_DeactivateAllConstraints_mC7259C1E725CF3BFB8EC6ECFD548E6C70C2F63C5 (void);
// 0x0000021C System.Void Obi.ObiConstraintsBatch::RemoveConstraint(System.Int32)
extern void ObiConstraintsBatch_RemoveConstraint_m5AAEA90963297F57C4BFF0C6858AA1E13AE6EC46 (void);
// 0x0000021D System.Void Obi.ObiConstraintsBatch::ParticlesSwapped(System.Int32,System.Int32)
extern void ObiConstraintsBatch_ParticlesSwapped_mFA21282517AF5D3D422315E8748273AC39BC7ED1 (void);
// 0x0000021E System.Void Obi.ObiConstraintsBatch::.ctor()
extern void ObiConstraintsBatch__ctor_m2B0D34B60AB411AA8D929BA04F3BF98231678A81 (void);
// 0x0000021F Oni/ConstraintType Obi.ObiDistanceConstraintsBatch::get_constraintType()
extern void ObiDistanceConstraintsBatch_get_constraintType_m738D4B915ECC57433D1FBE3736930F510797A6D5 (void);
// 0x00000220 Obi.IConstraintsBatchImpl Obi.ObiDistanceConstraintsBatch::get_implementation()
extern void ObiDistanceConstraintsBatch_get_implementation_mB2318A9886BF7C3D0513E65A18526B4F28740515 (void);
// 0x00000221 System.Void Obi.ObiDistanceConstraintsBatch::.ctor(System.Int32)
extern void ObiDistanceConstraintsBatch__ctor_mE47DA7819812295CB5E274C6A916E54E705A0EC3 (void);
// 0x00000222 System.Void Obi.ObiDistanceConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Single)
extern void ObiDistanceConstraintsBatch_AddConstraint_m118C1575A23FFAC933204048E9DD894F44CDD021 (void);
// 0x00000223 System.Void Obi.ObiDistanceConstraintsBatch::Clear()
extern void ObiDistanceConstraintsBatch_Clear_mEE9F3DDD52ECC58C3A9B4391667F88D3FF4B48C4 (void);
// 0x00000224 System.Single Obi.ObiDistanceConstraintsBatch::GetRestLength(System.Int32)
extern void ObiDistanceConstraintsBatch_GetRestLength_m5E1C408F549700872BADA871F7301C1A368E0F12 (void);
// 0x00000225 System.Void Obi.ObiDistanceConstraintsBatch::SetRestLength(System.Int32,System.Single)
extern void ObiDistanceConstraintsBatch_SetRestLength_m1CA836FD76DCC1AC15BECCF86DD92E487F71397E (void);
// 0x00000226 Obi.ParticlePair Obi.ObiDistanceConstraintsBatch::GetParticleIndices(System.Int32)
extern void ObiDistanceConstraintsBatch_GetParticleIndices_mB6664FFE2333714B74F7BFEE6D915DC49C3812C6 (void);
// 0x00000227 System.Void Obi.ObiDistanceConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiDistanceConstraintsBatch_GetParticlesInvolved_mC73EBE0B6DB236EC4BE938CB43ACE50F2AE65EFE (void);
// 0x00000228 System.Void Obi.ObiDistanceConstraintsBatch::CopyConstraint(Obi.ObiConstraintsBatch,System.Int32)
extern void ObiDistanceConstraintsBatch_CopyConstraint_m0EBF810160015C8A19A39367C329D343DBD1BF44 (void);
// 0x00000229 System.Void Obi.ObiDistanceConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiDistanceConstraintsBatch_SwapConstraints_mFC45320687BA195BF90408368D14ED2B804DC370 (void);
// 0x0000022A System.Void Obi.ObiDistanceConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiDistanceConstraintsBatch_Merge_m4D30F883821CC7CF022584BEC90B548D14C6DC15 (void);
// 0x0000022B System.Void Obi.ObiDistanceConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiDistanceConstraintsBatch_AddToSolver_m3308FD903CF4BC955B8D2F2E0B387D2C8A79B7B8 (void);
// 0x0000022C System.Void Obi.ObiDistanceConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiDistanceConstraintsBatch_RemoveFromSolver_m6BC9C6E426F431C44B10DCA8CBFFEF4F67E0936E (void);
// 0x0000022D Oni/ConstraintType Obi.ObiPinConstraintsBatch::get_constraintType()
extern void ObiPinConstraintsBatch_get_constraintType_m41C00D52B2D27084965D72E1F35D4E075444E482 (void);
// 0x0000022E Obi.IConstraintsBatchImpl Obi.ObiPinConstraintsBatch::get_implementation()
extern void ObiPinConstraintsBatch_get_implementation_m4578AAC572F0C5466E210AC1BC23930F8C09C0E6 (void);
// 0x0000022F System.Void Obi.ObiPinConstraintsBatch::.ctor(Obi.ObiPinConstraintsData)
extern void ObiPinConstraintsBatch__ctor_mEE99790C9B386AE46D24F129CDDCF2C1C5B5F5FC (void);
// 0x00000230 System.Void Obi.ObiPinConstraintsBatch::AddConstraint(System.Int32,Obi.ObiColliderBase,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,System.Single,System.Single)
extern void ObiPinConstraintsBatch_AddConstraint_mF22DD07678C73491928469A7741BD8B076071DCC (void);
// 0x00000231 System.Void Obi.ObiPinConstraintsBatch::Clear()
extern void ObiPinConstraintsBatch_Clear_mB0A07E7DF21B6C3CF9C5E054F58510FFDAA522AE (void);
// 0x00000232 System.Void Obi.ObiPinConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiPinConstraintsBatch_GetParticlesInvolved_m91C044A47CCA58E91BD3E72C7DE0ECE74BC37DA8 (void);
// 0x00000233 System.Void Obi.ObiPinConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiPinConstraintsBatch_SwapConstraints_mAE0389E323855EC02CB78CE15E73076B72CB1D85 (void);
// 0x00000234 System.Void Obi.ObiPinConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiPinConstraintsBatch_Merge_m2E0E93A262BDA66314A1CF7125B02B0E3D3AA3D7 (void);
// 0x00000235 System.Void Obi.ObiPinConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiPinConstraintsBatch_AddToSolver_mDAB05B2150F51AA80C2911CEF1BCE5E104D37E86 (void);
// 0x00000236 System.Void Obi.ObiPinConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiPinConstraintsBatch_RemoveFromSolver_mC766AF925EE815328507994A26FC0DA6399A473C (void);
// 0x00000237 Oni/ConstraintType Obi.ObiShapeMatchingConstraintsBatch::get_constraintType()
extern void ObiShapeMatchingConstraintsBatch_get_constraintType_m1D1B013631E72B441D2F440FABC9A47260BC1EFC (void);
// 0x00000238 Obi.IConstraintsBatchImpl Obi.ObiShapeMatchingConstraintsBatch::get_implementation()
extern void ObiShapeMatchingConstraintsBatch_get_implementation_mDE72A361D520AEF2417D5FA3D99265B24AE622BE (void);
// 0x00000239 System.Void Obi.ObiShapeMatchingConstraintsBatch::.ctor(Obi.ObiShapeMatchingConstraintsData)
extern void ObiShapeMatchingConstraintsBatch__ctor_m07C5E1D77AD00968700D64371442119BD0EFD860 (void);
// 0x0000023A System.Void Obi.ObiShapeMatchingConstraintsBatch::AddConstraint(System.Int32[],System.Boolean)
extern void ObiShapeMatchingConstraintsBatch_AddConstraint_m36DB2368774B955435601E74A559F9194797B73E (void);
// 0x0000023B System.Void Obi.ObiShapeMatchingConstraintsBatch::Clear()
extern void ObiShapeMatchingConstraintsBatch_Clear_m7613DFE3A4A449971A4C9C0E7F78D8ED7D99B7CF (void);
// 0x0000023C System.Void Obi.ObiShapeMatchingConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiShapeMatchingConstraintsBatch_GetParticlesInvolved_m460EACD5CA047B22B48866E7980990188EF04B88 (void);
// 0x0000023D System.Void Obi.ObiShapeMatchingConstraintsBatch::RemoveParticleFromConstraint(System.Int32,System.Int32)
extern void ObiShapeMatchingConstraintsBatch_RemoveParticleFromConstraint_mD284790EE70116086A1EF3AF9565322E629039FF (void);
// 0x0000023E System.Void Obi.ObiShapeMatchingConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiShapeMatchingConstraintsBatch_SwapConstraints_m203CA70E418F8DF2BF506ED333695247E286BA2B (void);
// 0x0000023F System.Void Obi.ObiShapeMatchingConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiShapeMatchingConstraintsBatch_Merge_m5A9DEA535310B3863D91C0FBE5A52C051E0357A7 (void);
// 0x00000240 System.Void Obi.ObiShapeMatchingConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiShapeMatchingConstraintsBatch_AddToSolver_mA64BACE29C81AC0D914642A2326C5A768BCAD690 (void);
// 0x00000241 System.Void Obi.ObiShapeMatchingConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiShapeMatchingConstraintsBatch_RemoveFromSolver_m64F2CB468C81ED844CEE845FC280A5497A18AD33 (void);
// 0x00000242 System.Void Obi.ObiShapeMatchingConstraintsBatch::RecalculateRestShapeMatching()
extern void ObiShapeMatchingConstraintsBatch_RecalculateRestShapeMatching_m49C8363C8157E30089548C4BDF8BDA633D26DDB7 (void);
// 0x00000243 Oni/ConstraintType Obi.ObiSkinConstraintsBatch::get_constraintType()
extern void ObiSkinConstraintsBatch_get_constraintType_mB8336F19B6DBE7777042FA4814EA12115D899223 (void);
// 0x00000244 Obi.IConstraintsBatchImpl Obi.ObiSkinConstraintsBatch::get_implementation()
extern void ObiSkinConstraintsBatch_get_implementation_m8EFE8C0407125BE90F4A544FD0BBD6801F606924 (void);
// 0x00000245 System.Void Obi.ObiSkinConstraintsBatch::.ctor(Obi.ObiSkinConstraintsData)
extern void ObiSkinConstraintsBatch__ctor_m1C9F633F90BE267DDA66B580DED05A7F19BD61C5 (void);
// 0x00000246 System.Void Obi.ObiSkinConstraintsBatch::AddConstraint(System.Int32,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,System.Single,System.Single,System.Single)
extern void ObiSkinConstraintsBatch_AddConstraint_m5D3D3C2490F09B0C7B72F763AA56E3AE9D485134 (void);
// 0x00000247 System.Void Obi.ObiSkinConstraintsBatch::Clear()
extern void ObiSkinConstraintsBatch_Clear_m9760EF8B82970CC9CDE9A840BD9EAF78BD982393 (void);
// 0x00000248 System.Void Obi.ObiSkinConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiSkinConstraintsBatch_GetParticlesInvolved_m77E2085BC3B66F68FE4B9453F0D76A9566718597 (void);
// 0x00000249 System.Void Obi.ObiSkinConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiSkinConstraintsBatch_SwapConstraints_m90010A1D7C64381F5575F0F393B7030C2201DF8F (void);
// 0x0000024A System.Void Obi.ObiSkinConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiSkinConstraintsBatch_Merge_m5AC677F66016C8CC6DCC768E4A60ECEE633075A6 (void);
// 0x0000024B System.Void Obi.ObiSkinConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiSkinConstraintsBatch_AddToSolver_mF9FF0C39D2BA6C21C642F2F0880004B3D719DC2C (void);
// 0x0000024C System.Void Obi.ObiSkinConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiSkinConstraintsBatch_RemoveFromSolver_m146DF16BA35D6BBBAE5781504D744C59D61D80FE (void);
// 0x0000024D Oni/ConstraintType Obi.ObiStretchShearConstraintsBatch::get_constraintType()
extern void ObiStretchShearConstraintsBatch_get_constraintType_mC6DEA77B39D02D597E2CA898182A9BD560B545C6 (void);
// 0x0000024E Obi.IConstraintsBatchImpl Obi.ObiStretchShearConstraintsBatch::get_implementation()
extern void ObiStretchShearConstraintsBatch_get_implementation_m0DAC376106BC35C967BE578A40049E3C35E27024 (void);
// 0x0000024F System.Void Obi.ObiStretchShearConstraintsBatch::.ctor(Obi.ObiStretchShearConstraintsData)
extern void ObiStretchShearConstraintsBatch__ctor_m8927550DA897869840F27FAE27254EF2961AEFB7 (void);
// 0x00000250 System.Void Obi.ObiStretchShearConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Int32,System.Single,UnityEngine.Quaternion)
extern void ObiStretchShearConstraintsBatch_AddConstraint_m3EDB080395E5167A460038CE0B00CEEC306C19D5 (void);
// 0x00000251 System.Void Obi.ObiStretchShearConstraintsBatch::Clear()
extern void ObiStretchShearConstraintsBatch_Clear_m827C6A5985D286459089DC4635044609BC6B098C (void);
// 0x00000252 System.Single Obi.ObiStretchShearConstraintsBatch::GetRestLength(System.Int32)
extern void ObiStretchShearConstraintsBatch_GetRestLength_mC0368700D80E14C1F758085E1051265DB7E6F140 (void);
// 0x00000253 System.Void Obi.ObiStretchShearConstraintsBatch::SetRestLength(System.Int32,System.Single)
extern void ObiStretchShearConstraintsBatch_SetRestLength_m4401CCB79E8593DE77C2F66191AE967C6DC7E5DC (void);
// 0x00000254 Obi.ParticlePair Obi.ObiStretchShearConstraintsBatch::GetParticleIndices(System.Int32)
extern void ObiStretchShearConstraintsBatch_GetParticleIndices_mC265643A55371F01BD13A0C3FC34A11D9FFE838C (void);
// 0x00000255 System.Void Obi.ObiStretchShearConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiStretchShearConstraintsBatch_GetParticlesInvolved_m304EEFB3092A0112EAE84B4CECF3F6A948D9833E (void);
// 0x00000256 System.Void Obi.ObiStretchShearConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiStretchShearConstraintsBatch_SwapConstraints_m2C8677D96612ADAE5BFCC4825A9BA4E42685609E (void);
// 0x00000257 System.Void Obi.ObiStretchShearConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiStretchShearConstraintsBatch_Merge_mC487E3A37D51D78F57A1880A27B8168E45F05BB8 (void);
// 0x00000258 System.Void Obi.ObiStretchShearConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiStretchShearConstraintsBatch_AddToSolver_mA25B51FDA1EBF1E1BE6AD594D45C153216AA8C94 (void);
// 0x00000259 System.Void Obi.ObiStretchShearConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiStretchShearConstraintsBatch_RemoveFromSolver_m45797BE9F2A7CC2B0BD983E810A313C126621382 (void);
// 0x0000025A Oni/ConstraintType Obi.ObiTetherConstraintsBatch::get_constraintType()
extern void ObiTetherConstraintsBatch_get_constraintType_m8483A4DC0E6B622F67EC74C1BB113C00981EFA6C (void);
// 0x0000025B Obi.IConstraintsBatchImpl Obi.ObiTetherConstraintsBatch::get_implementation()
extern void ObiTetherConstraintsBatch_get_implementation_mC6C6A9923482CF3A380B6D250E1A63C0EC69E02F (void);
// 0x0000025C System.Void Obi.ObiTetherConstraintsBatch::.ctor(Obi.ObiTetherConstraintsData)
extern void ObiTetherConstraintsBatch__ctor_m8866FC2DA610911877A83CE2904D78CACB19FC51 (void);
// 0x0000025D System.Void Obi.ObiTetherConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Single,System.Single)
extern void ObiTetherConstraintsBatch_AddConstraint_m1BF702B37FD24AD678D201379C614BFA7C24824A (void);
// 0x0000025E System.Void Obi.ObiTetherConstraintsBatch::Clear()
extern void ObiTetherConstraintsBatch_Clear_m2F60F212808B2D2566D9486751BA43D287D7A616 (void);
// 0x0000025F System.Void Obi.ObiTetherConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiTetherConstraintsBatch_GetParticlesInvolved_m77FB1AD6E9F76D24F63C6CF2FCA60F8627D497BA (void);
// 0x00000260 System.Void Obi.ObiTetherConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiTetherConstraintsBatch_SwapConstraints_mE3AD347DF0A9C8FA3D0467F87FFEC0B13FD61E72 (void);
// 0x00000261 System.Void Obi.ObiTetherConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiTetherConstraintsBatch_Merge_m1AD265CC0EE520D80F27DEFBE161FF9E44B80A76 (void);
// 0x00000262 System.Void Obi.ObiTetherConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiTetherConstraintsBatch_AddToSolver_mD57866191E80F88A3D272C0E0AC76364326A80E3 (void);
// 0x00000263 System.Void Obi.ObiTetherConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiTetherConstraintsBatch_RemoveFromSolver_mEE3D5BD7C2CF365225947E773A815095121EF2C6 (void);
// 0x00000264 System.Void Obi.ObiTetherConstraintsBatch::SetParameters(System.Single,System.Single)
extern void ObiTetherConstraintsBatch_SetParameters_m1577626B4E1C0111CF067DABC451BC0503615466 (void);
// 0x00000265 Oni/ConstraintType Obi.ObiVolumeConstraintsBatch::get_constraintType()
extern void ObiVolumeConstraintsBatch_get_constraintType_m381ED3FD4921DB235D1AE2E0D93D354C840CE86A (void);
// 0x00000266 Obi.IConstraintsBatchImpl Obi.ObiVolumeConstraintsBatch::get_implementation()
extern void ObiVolumeConstraintsBatch_get_implementation_mA945BB5C6B7A7EE83AC6C01C6B5DD21207FB0325 (void);
// 0x00000267 System.Void Obi.ObiVolumeConstraintsBatch::.ctor(Obi.ObiVolumeConstraintsData)
extern void ObiVolumeConstraintsBatch__ctor_mA44A358BC97BD1830050A02DFFDF5108A6FD9715 (void);
// 0x00000268 System.Void Obi.ObiVolumeConstraintsBatch::AddConstraint(System.Int32[],System.Single)
extern void ObiVolumeConstraintsBatch_AddConstraint_m411C62C1E1117BF0B3B6954C3641F5B418583424 (void);
// 0x00000269 System.Void Obi.ObiVolumeConstraintsBatch::Clear()
extern void ObiVolumeConstraintsBatch_Clear_m53B029F54892E0D5B9D4D2BCB647A4176FE793A0 (void);
// 0x0000026A System.Void Obi.ObiVolumeConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiVolumeConstraintsBatch_GetParticlesInvolved_mC38E81ADD236D1D8841141D16BA6989DA3A489F2 (void);
// 0x0000026B System.Void Obi.ObiVolumeConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiVolumeConstraintsBatch_SwapConstraints_m42B669FC7CBDFA494210C6486682BD7A3CFFDDB4 (void);
// 0x0000026C System.Void Obi.ObiVolumeConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiVolumeConstraintsBatch_Merge_m1DDAB49D1408ADCD12D27C72737072480252DA7B (void);
// 0x0000026D System.Void Obi.ObiVolumeConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiVolumeConstraintsBatch_AddToSolver_m1070A8D58A5B37CC00B5290D1448490F3DD9CE29 (void);
// 0x0000026E System.Void Obi.ObiVolumeConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiVolumeConstraintsBatch_RemoveFromSolver_m01AB99DBC04273CEB49EFAAFE595F93D5F05B726 (void);
// 0x0000026F System.Void Obi.ObiVolumeConstraintsBatch::SetParameters(System.Single,System.Single)
extern void ObiVolumeConstraintsBatch_SetParameters_m8B574D29F6430355A0AF20B229AA493E0119AEF5 (void);
// 0x00000270 System.Boolean Obi.IAerodynamicConstraintsUser::get_aerodynamicsEnabled()
// 0x00000271 System.Void Obi.IAerodynamicConstraintsUser::set_aerodynamicsEnabled(System.Boolean)
// 0x00000272 System.Single Obi.IAerodynamicConstraintsUser::get_drag()
// 0x00000273 System.Void Obi.IAerodynamicConstraintsUser::set_drag(System.Single)
// 0x00000274 System.Single Obi.IAerodynamicConstraintsUser::get_lift()
// 0x00000275 System.Void Obi.IAerodynamicConstraintsUser::set_lift(System.Single)
// 0x00000276 Obi.ObiAerodynamicConstraintsBatch Obi.ObiAerodynamicConstraintsData::CreateBatch(Obi.ObiAerodynamicConstraintsBatch)
extern void ObiAerodynamicConstraintsData_CreateBatch_mD91F60B676E69FE28DD53AA57D5E64E621F7E489 (void);
// 0x00000277 System.Void Obi.ObiAerodynamicConstraintsData::.ctor()
extern void ObiAerodynamicConstraintsData__ctor_mD5BD0D829EC39F1CACD35CF16E040852B4B4784B (void);
// 0x00000278 System.Boolean Obi.IBendConstraintsUser::get_bendConstraintsEnabled()
// 0x00000279 System.Void Obi.IBendConstraintsUser::set_bendConstraintsEnabled(System.Boolean)
// 0x0000027A System.Single Obi.IBendConstraintsUser::get_bendCompliance()
// 0x0000027B System.Void Obi.IBendConstraintsUser::set_bendCompliance(System.Single)
// 0x0000027C System.Single Obi.IBendConstraintsUser::get_maxBending()
// 0x0000027D System.Void Obi.IBendConstraintsUser::set_maxBending(System.Single)
// 0x0000027E System.Single Obi.IBendConstraintsUser::get_plasticYield()
// 0x0000027F System.Void Obi.IBendConstraintsUser::set_plasticYield(System.Single)
// 0x00000280 System.Single Obi.IBendConstraintsUser::get_plasticCreep()
// 0x00000281 System.Void Obi.IBendConstraintsUser::set_plasticCreep(System.Single)
// 0x00000282 Obi.ObiBendConstraintsBatch Obi.ObiBendConstraintsData::CreateBatch(Obi.ObiBendConstraintsBatch)
extern void ObiBendConstraintsData_CreateBatch_mD0A372B63F6C4F88AC48D4E4E9304944D3FEA0F2 (void);
// 0x00000283 System.Void Obi.ObiBendConstraintsData::.ctor()
extern void ObiBendConstraintsData__ctor_mFE69ED43DBE4731FAAAF8C32658B04596342980A (void);
// 0x00000284 System.Boolean Obi.IBendTwistConstraintsUser::get_bendTwistConstraintsEnabled()
// 0x00000285 System.Void Obi.IBendTwistConstraintsUser::set_bendTwistConstraintsEnabled(System.Boolean)
// 0x00000286 UnityEngine.Vector3 Obi.IBendTwistConstraintsUser::GetBendTwistCompliance(Obi.ObiBendTwistConstraintsBatch,System.Int32)
// 0x00000287 UnityEngine.Vector2 Obi.IBendTwistConstraintsUser::GetBendTwistPlasticity(Obi.ObiBendTwistConstraintsBatch,System.Int32)
// 0x00000288 Obi.ObiBendTwistConstraintsBatch Obi.ObiBendTwistConstraintsData::CreateBatch(Obi.ObiBendTwistConstraintsBatch)
extern void ObiBendTwistConstraintsData_CreateBatch_m9A87A7382C1A63A339F0C8B17CF14D1CB03B0D46 (void);
// 0x00000289 System.Void Obi.ObiBendTwistConstraintsData::.ctor()
extern void ObiBendTwistConstraintsData__ctor_mC6E27E26B877C88510C9E27FB9ED17BF94E98B9A (void);
// 0x0000028A System.Boolean Obi.IChainConstraintsUser::get_chainConstraintsEnabled()
// 0x0000028B System.Void Obi.IChainConstraintsUser::set_chainConstraintsEnabled(System.Boolean)
// 0x0000028C System.Single Obi.IChainConstraintsUser::get_tightness()
// 0x0000028D System.Void Obi.IChainConstraintsUser::set_tightness(System.Single)
// 0x0000028E Obi.ObiChainConstraintsBatch Obi.ObiChainConstraintsData::CreateBatch(Obi.ObiChainConstraintsBatch)
extern void ObiChainConstraintsData_CreateBatch_mC2FB00A47639537C6959AE16661DAAF1AD4C3F7B (void);
// 0x0000028F System.Void Obi.ObiChainConstraintsData::.ctor()
extern void ObiChainConstraintsData__ctor_m9D1C51412A1878F85D8C173777A60EC621A9FFDD (void);
// 0x00000290 System.Boolean Obi.IDistanceConstraintsUser::get_distanceConstraintsEnabled()
// 0x00000291 System.Void Obi.IDistanceConstraintsUser::set_distanceConstraintsEnabled(System.Boolean)
// 0x00000292 System.Single Obi.IDistanceConstraintsUser::get_stretchingScale()
// 0x00000293 System.Void Obi.IDistanceConstraintsUser::set_stretchingScale(System.Single)
// 0x00000294 System.Single Obi.IDistanceConstraintsUser::get_stretchCompliance()
// 0x00000295 System.Void Obi.IDistanceConstraintsUser::set_stretchCompliance(System.Single)
// 0x00000296 System.Single Obi.IDistanceConstraintsUser::get_maxCompression()
// 0x00000297 System.Void Obi.IDistanceConstraintsUser::set_maxCompression(System.Single)
// 0x00000298 Obi.ObiDistanceConstraintsBatch Obi.ObiDistanceConstraintsData::CreateBatch(Obi.ObiDistanceConstraintsBatch)
extern void ObiDistanceConstraintsData_CreateBatch_mA63574AD8629D5C6907EB15B3FFA9F0D1616D026 (void);
// 0x00000299 System.Void Obi.ObiDistanceConstraintsData::.ctor()
extern void ObiDistanceConstraintsData__ctor_m10E8E535222BCC55670623BBB7DA36CAF7A2A95E (void);
// 0x0000029A Obi.ObiPinConstraintsBatch Obi.ObiPinConstraintsData::CreateBatch(Obi.ObiPinConstraintsBatch)
extern void ObiPinConstraintsData_CreateBatch_mC8E21D95D7C5E27097C6B75B20DC74CBDD2D345B (void);
// 0x0000029B System.Void Obi.ObiPinConstraintsData::.ctor()
extern void ObiPinConstraintsData__ctor_mA86A434A33F8CF37FA8E7E2410BF8223116F03A0 (void);
// 0x0000029C System.Boolean Obi.IShapeMatchingConstraintsUser::get_shapeMatchingConstraintsEnabled()
// 0x0000029D System.Void Obi.IShapeMatchingConstraintsUser::set_shapeMatchingConstraintsEnabled(System.Boolean)
// 0x0000029E System.Single Obi.IShapeMatchingConstraintsUser::get_deformationResistance()
// 0x0000029F System.Void Obi.IShapeMatchingConstraintsUser::set_deformationResistance(System.Single)
// 0x000002A0 System.Single Obi.IShapeMatchingConstraintsUser::get_maxDeformation()
// 0x000002A1 System.Void Obi.IShapeMatchingConstraintsUser::set_maxDeformation(System.Single)
// 0x000002A2 System.Single Obi.IShapeMatchingConstraintsUser::get_plasticYield()
// 0x000002A3 System.Void Obi.IShapeMatchingConstraintsUser::set_plasticYield(System.Single)
// 0x000002A4 System.Single Obi.IShapeMatchingConstraintsUser::get_plasticCreep()
// 0x000002A5 System.Void Obi.IShapeMatchingConstraintsUser::set_plasticCreep(System.Single)
// 0x000002A6 System.Single Obi.IShapeMatchingConstraintsUser::get_plasticRecovery()
// 0x000002A7 System.Void Obi.IShapeMatchingConstraintsUser::set_plasticRecovery(System.Single)
// 0x000002A8 Obi.ObiShapeMatchingConstraintsBatch Obi.ObiShapeMatchingConstraintsData::CreateBatch(Obi.ObiShapeMatchingConstraintsBatch)
extern void ObiShapeMatchingConstraintsData_CreateBatch_mDD0753126D9E04AB8FD0D7F4CB4D6C9ACA8BF359 (void);
// 0x000002A9 System.Void Obi.ObiShapeMatchingConstraintsData::.ctor()
extern void ObiShapeMatchingConstraintsData__ctor_m39A87174E47FB428D6589800F2F18FE1F79D340A (void);
// 0x000002AA System.Boolean Obi.ISkinConstraintsUser::get_skinConstraintsEnabled()
// 0x000002AB System.Void Obi.ISkinConstraintsUser::set_skinConstraintsEnabled(System.Boolean)
// 0x000002AC UnityEngine.Vector3 Obi.ISkinConstraintsUser::GetSkinRadiiBackstop(Obi.ObiSkinConstraintsBatch,System.Int32)
// 0x000002AD System.Single Obi.ISkinConstraintsUser::GetSkinCompliance(Obi.ObiSkinConstraintsBatch,System.Int32)
// 0x000002AE Obi.ObiSkinConstraintsBatch Obi.ObiSkinConstraintsData::CreateBatch(Obi.ObiSkinConstraintsBatch)
extern void ObiSkinConstraintsData_CreateBatch_m0420AD7727A873F46A9E43F8BF1F94895AB14369 (void);
// 0x000002AF System.Void Obi.ObiSkinConstraintsData::.ctor()
extern void ObiSkinConstraintsData__ctor_mCB9A63E7B637DEAEB1063E52913985321ABBCAA3 (void);
// 0x000002B0 System.Boolean Obi.IStretchShearConstraintsUser::get_stretchShearConstraintsEnabled()
// 0x000002B1 System.Void Obi.IStretchShearConstraintsUser::set_stretchShearConstraintsEnabled(System.Boolean)
// 0x000002B2 UnityEngine.Vector3 Obi.IStretchShearConstraintsUser::GetStretchShearCompliance(Obi.ObiStretchShearConstraintsBatch,System.Int32)
// 0x000002B3 Obi.ObiStretchShearConstraintsBatch Obi.ObiStretchShearConstraintsData::CreateBatch(Obi.ObiStretchShearConstraintsBatch)
extern void ObiStretchShearConstraintsData_CreateBatch_mFBE3B0B12D6BD8E226076AB93E16906F92B60C68 (void);
// 0x000002B4 System.Void Obi.ObiStretchShearConstraintsData::.ctor()
extern void ObiStretchShearConstraintsData__ctor_m81576073ED6B14D835150C4666A3588AEDBD29FC (void);
// 0x000002B5 System.Boolean Obi.ITetherConstraintsUser::get_tetherConstraintsEnabled()
// 0x000002B6 System.Void Obi.ITetherConstraintsUser::set_tetherConstraintsEnabled(System.Boolean)
// 0x000002B7 System.Single Obi.ITetherConstraintsUser::get_tetherCompliance()
// 0x000002B8 System.Void Obi.ITetherConstraintsUser::set_tetherCompliance(System.Single)
// 0x000002B9 System.Single Obi.ITetherConstraintsUser::get_tetherScale()
// 0x000002BA System.Void Obi.ITetherConstraintsUser::set_tetherScale(System.Single)
// 0x000002BB Obi.ObiTetherConstraintsBatch Obi.ObiTetherConstraintsData::CreateBatch(Obi.ObiTetherConstraintsBatch)
extern void ObiTetherConstraintsData_CreateBatch_m28C477BD204B78B77C867473CB959C5340B918D7 (void);
// 0x000002BC System.Void Obi.ObiTetherConstraintsData::.ctor()
extern void ObiTetherConstraintsData__ctor_m7488642C0CCCDA9C252605F19F887C961D3BB5C4 (void);
// 0x000002BD System.Boolean Obi.IVolumeConstraintsUser::get_volumeConstraintsEnabled()
// 0x000002BE System.Void Obi.IVolumeConstraintsUser::set_volumeConstraintsEnabled(System.Boolean)
// 0x000002BF System.Single Obi.IVolumeConstraintsUser::get_compressionCompliance()
// 0x000002C0 System.Void Obi.IVolumeConstraintsUser::set_compressionCompliance(System.Single)
// 0x000002C1 System.Single Obi.IVolumeConstraintsUser::get_pressure()
// 0x000002C2 System.Void Obi.IVolumeConstraintsUser::set_pressure(System.Single)
// 0x000002C3 Obi.ObiVolumeConstraintsBatch Obi.ObiVolumeConstraintsData::CreateBatch(Obi.ObiVolumeConstraintsBatch)
extern void ObiVolumeConstraintsData_CreateBatch_m1DDE0FAC895B8FC54D095EB773D7AEBDE107A061 (void);
// 0x000002C4 System.Void Obi.ObiVolumeConstraintsData::.ctor()
extern void ObiVolumeConstraintsData__ctor_m09F646B51D450E9B87B194F9E29B8B5FE9C2404B (void);
// 0x000002C5 System.Nullable`1<Oni/ConstraintType> Obi.IObiConstraints::GetConstraintType()
// 0x000002C6 Obi.IObiConstraintsBatch Obi.IObiConstraints::GetBatch(System.Int32)
// 0x000002C7 System.Int32 Obi.IObiConstraints::GetBatchCount()
// 0x000002C8 System.Void Obi.IObiConstraints::Clear()
// 0x000002C9 System.Boolean Obi.IObiConstraints::AddToSolver(Obi.ObiSolver)
// 0x000002CA System.Boolean Obi.IObiConstraints::RemoveFromSolver()
// 0x000002CB System.Int32 Obi.IObiConstraints::GetConstraintCount()
// 0x000002CC System.Int32 Obi.IObiConstraints::GetActiveConstraintCount()
// 0x000002CD System.Void Obi.IObiConstraints::DeactivateAllConstraints()
// 0x000002CE System.Void Obi.IObiConstraints::Merge(Obi.ObiActor,Obi.IObiConstraints)
// 0x000002CF System.Void Obi.ObiConstraints`1::Merge(Obi.ObiActor,Obi.IObiConstraints)
// 0x000002D0 Obi.IObiConstraintsBatch Obi.ObiConstraints`1::GetBatch(System.Int32)
// 0x000002D1 System.Int32 Obi.ObiConstraints`1::GetBatchCount()
// 0x000002D2 System.Int32 Obi.ObiConstraints`1::GetConstraintCount()
// 0x000002D3 System.Int32 Obi.ObiConstraints`1::GetActiveConstraintCount()
// 0x000002D4 System.Void Obi.ObiConstraints`1::DeactivateAllConstraints()
// 0x000002D5 T Obi.ObiConstraints`1::GetFirstBatch()
// 0x000002D6 System.Nullable`1<Oni/ConstraintType> Obi.ObiConstraints`1::GetConstraintType()
// 0x000002D7 System.Void Obi.ObiConstraints`1::Clear()
// 0x000002D8 T Obi.ObiConstraints`1::CreateBatch(T)
// 0x000002D9 System.Void Obi.ObiConstraints`1::AddBatch(T)
// 0x000002DA System.Boolean Obi.ObiConstraints`1::RemoveBatch(T)
// 0x000002DB System.Boolean Obi.ObiConstraints`1::AddToSolver(Obi.ObiSolver)
// 0x000002DC System.Boolean Obi.ObiConstraints`1::RemoveFromSolver()
// 0x000002DD System.Void Obi.ObiConstraints`1::.ctor()
// 0x000002DE System.Single Obi.StructuralConstraint::get_restLength()
extern void StructuralConstraint_get_restLength_mA86CC2512F19A0314F76C459C7027A50DEB66F57 (void);
// 0x000002DF System.Void Obi.StructuralConstraint::set_restLength(System.Single)
extern void StructuralConstraint_set_restLength_mFDDB0536829F1845FAB3F39B80ED0520431D2BDF (void);
// 0x000002E0 System.Void Obi.StructuralConstraint::.ctor(Obi.IStructuralConstraintBatch,System.Int32,System.Single)
extern void StructuralConstraint__ctor_m9F7FF404C8685160AC46543FCE7681E91FF6B322 (void);
// 0x000002E1 System.Int32[] Obi.GraphColoring::Colorize(System.Int32[],System.Int32[])
extern void GraphColoring_Colorize_mC1E72C7E56EE6F1F72682546D5CB94934287958C (void);
// 0x000002E2 System.Void Obi.ObiActorBlueprint::add_OnBlueprintGenerate(Obi.ObiActorBlueprint/BlueprintCallback)
extern void ObiActorBlueprint_add_OnBlueprintGenerate_mA116B88726BA07CDAC7EA40506772CC39F8FF86E (void);
// 0x000002E3 System.Void Obi.ObiActorBlueprint::remove_OnBlueprintGenerate(Obi.ObiActorBlueprint/BlueprintCallback)
extern void ObiActorBlueprint_remove_OnBlueprintGenerate_m780AFE9B203E214D86769A37EADEE63D5FC9967E (void);
// 0x000002E4 System.Int32 Obi.ObiActorBlueprint::get_particleCount()
extern void ObiActorBlueprint_get_particleCount_mCC6F47C83D7F2BE2BBF1667511B5C96BAA5A94B2 (void);
// 0x000002E5 System.Int32 Obi.ObiActorBlueprint::get_activeParticleCount()
extern void ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A (void);
// 0x000002E6 System.Boolean Obi.ObiActorBlueprint::get_usesOrientedParticles()
extern void ObiActorBlueprint_get_usesOrientedParticles_m8E951A7BDC8EEA54633C2AA8BE8E41F01A418233 (void);
// 0x000002E7 System.Boolean Obi.ObiActorBlueprint::get_usesTethers()
extern void ObiActorBlueprint_get_usesTethers_m7A16422C66E95F3984D2A3A8F950305E9D5C2026 (void);
// 0x000002E8 System.Boolean Obi.ObiActorBlueprint::IsParticleActive(System.Int32)
extern void ObiActorBlueprint_IsParticleActive_mD079A02C9E95DB8F8900FA0C1FE9A057C1F1D05A (void);
// 0x000002E9 System.Void Obi.ObiActorBlueprint::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiActorBlueprint_SwapWithFirstInactiveParticle_mCBD4A74BAC682B6DB6228F3B53FCC5D90B069126 (void);
// 0x000002EA System.Boolean Obi.ObiActorBlueprint::ActivateParticle(System.Int32)
extern void ObiActorBlueprint_ActivateParticle_mA772B1D5DA1364AA8A889F069D9F903E565A3F02 (void);
// 0x000002EB System.Boolean Obi.ObiActorBlueprint::DeactivateParticle(System.Int32)
extern void ObiActorBlueprint_DeactivateParticle_m2EFB2F10CE118C9041593C482C6FC80F24A9C8A7 (void);
// 0x000002EC System.Boolean Obi.ObiActorBlueprint::get_empty()
extern void ObiActorBlueprint_get_empty_m5407EABADC0A7CFE5EBBA320717C3B4609F2FDD1 (void);
// 0x000002ED System.Void Obi.ObiActorBlueprint::RecalculateBounds()
extern void ObiActorBlueprint_RecalculateBounds_mC32E8C37E376B8342D46E8894A36161EDF37E48A (void);
// 0x000002EE UnityEngine.Bounds Obi.ObiActorBlueprint::get_bounds()
extern void ObiActorBlueprint_get_bounds_m8F16C4CEAFB92614DEC83B9F41D565B4FE542C15 (void);
// 0x000002EF System.Collections.Generic.IEnumerable`1<Obi.IObiConstraints> Obi.ObiActorBlueprint::GetConstraints()
extern void ObiActorBlueprint_GetConstraints_m1C48E3155094EC159203D4F23B25015FCA6F0597 (void);
// 0x000002F0 Obi.IObiConstraints Obi.ObiActorBlueprint::GetConstraintsByType(Oni/ConstraintType)
extern void ObiActorBlueprint_GetConstraintsByType_mD32BF0C8BE5453BF0F534BCC92F2BD30477E7761 (void);
// 0x000002F1 System.Int32 Obi.ObiActorBlueprint::GetParticleRuntimeIndex(System.Int32)
extern void ObiActorBlueprint_GetParticleRuntimeIndex_m6495778E4FF39D59DA86401684ABED0361E6803B (void);
// 0x000002F2 UnityEngine.Vector3 Obi.ObiActorBlueprint::GetParticlePosition(System.Int32)
extern void ObiActorBlueprint_GetParticlePosition_m4B9F2D62EEF90A610C7DC8E1B07D4525ED90A081 (void);
// 0x000002F3 UnityEngine.Quaternion Obi.ObiActorBlueprint::GetParticleOrientation(System.Int32)
extern void ObiActorBlueprint_GetParticleOrientation_mE80B9B4333A1EE1670B7DAD73FD55F8C8F6F178D (void);
// 0x000002F4 System.Void Obi.ObiActorBlueprint::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
extern void ObiActorBlueprint_GetParticleAnisotropy_mBFB7DD70BF52D6DEC5E08DEFC5A8F07BBFCBE6B9 (void);
// 0x000002F5 System.Single Obi.ObiActorBlueprint::GetParticleMaxRadius(System.Int32)
extern void ObiActorBlueprint_GetParticleMaxRadius_m1106A97F11068907833E40301081A52749BC79F4 (void);
// 0x000002F6 UnityEngine.Color Obi.ObiActorBlueprint::GetParticleColor(System.Int32)
extern void ObiActorBlueprint_GetParticleColor_mA1CA0E89A089B4CEBAFFD4EB83CBFC5EB5A6EB0D (void);
// 0x000002F7 System.Void Obi.ObiActorBlueprint::GenerateImmediate()
extern void ObiActorBlueprint_GenerateImmediate_mD3FF977E24F4965AB776BF1E1DF0272F3D6CCA33 (void);
// 0x000002F8 System.Collections.IEnumerator Obi.ObiActorBlueprint::Generate()
extern void ObiActorBlueprint_Generate_m14C8F36BC89354F15F0F82AE9546B5024F47FF7A (void);
// 0x000002F9 System.Void Obi.ObiActorBlueprint::Clear()
extern void ObiActorBlueprint_Clear_mD6317D8274462CCEF04D76D0957286FF73D692EB (void);
// 0x000002FA Obi.ObiParticleGroup Obi.ObiActorBlueprint::InsertNewParticleGroup(System.String,System.Int32,System.Boolean)
extern void ObiActorBlueprint_InsertNewParticleGroup_m5C512B2A9ED1227695249B3DDC9C19A9D421AAAD (void);
// 0x000002FB Obi.ObiParticleGroup Obi.ObiActorBlueprint::AppendNewParticleGroup(System.String,System.Boolean)
extern void ObiActorBlueprint_AppendNewParticleGroup_m33A05025BCF9CD281364DDA0E3563AE5091E6D79 (void);
// 0x000002FC System.Boolean Obi.ObiActorBlueprint::RemoveParticleGroupAt(System.Int32,System.Boolean)
extern void ObiActorBlueprint_RemoveParticleGroupAt_m583F9722B398FB85BFFB3AA17AB4E53424FD4E18 (void);
// 0x000002FD System.Boolean Obi.ObiActorBlueprint::SetParticleGroupName(System.Int32,System.String,System.Boolean)
extern void ObiActorBlueprint_SetParticleGroupName_mEC41E6FFFC12BB73B6BA214F81CF70CE0A034A34 (void);
// 0x000002FE System.Void Obi.ObiActorBlueprint::ClearParticleGroups(System.Boolean)
extern void ObiActorBlueprint_ClearParticleGroups_mBC589A70CC5E642C10FD126B482590D27FEC8B18 (void);
// 0x000002FF System.Boolean Obi.ObiActorBlueprint::IsParticleSharedInConstraint(System.Int32,System.Collections.Generic.List`1<System.Int32>,System.Boolean[])
extern void ObiActorBlueprint_IsParticleSharedInConstraint_m9F71E2E0E9B16D9CCB47B7D270391F65232D64A1 (void);
// 0x00000300 System.Boolean Obi.ObiActorBlueprint::DoesParticleShareConstraints(Obi.IObiConstraints,System.Int32,System.Collections.Generic.List`1<System.Int32>,System.Boolean[])
extern void ObiActorBlueprint_DoesParticleShareConstraints_mCCD929D6E8D050B9B2A377A3965B6F8E64538883 (void);
// 0x00000301 System.Void Obi.ObiActorBlueprint::DeactivateConstraintsWithInactiveParticles(Obi.IObiConstraints,System.Collections.Generic.List`1<System.Int32>)
extern void ObiActorBlueprint_DeactivateConstraintsWithInactiveParticles_m2F8B411DA9FE9FCDC8116158247E0E83EEA513E1 (void);
// 0x00000302 System.Void Obi.ObiActorBlueprint::ParticlesSwappedInGroups(System.Int32,System.Int32)
extern void ObiActorBlueprint_ParticlesSwappedInGroups_m9104E8E53B4D76CFFD62EF8277E5313A73867EE5 (void);
// 0x00000303 System.Void Obi.ObiActorBlueprint::RemoveSelectedParticles(System.Boolean[]&,System.Boolean)
extern void ObiActorBlueprint_RemoveSelectedParticles_m1FD86679B5F6C5DEDED32F79658292F13566C63F (void);
// 0x00000304 System.Void Obi.ObiActorBlueprint::RestoreRemovedParticles()
extern void ObiActorBlueprint_RestoreRemovedParticles_m6968118C8E330062E76B6F79155E07B86C091400 (void);
// 0x00000305 System.Void Obi.ObiActorBlueprint::GenerateTethers(System.Boolean[])
extern void ObiActorBlueprint_GenerateTethers_mE0CECBEF8A33A7E44D7EFD24A87697C45209B668 (void);
// 0x00000306 System.Void Obi.ObiActorBlueprint::ClearTethers()
extern void ObiActorBlueprint_ClearTethers_mA11CB91D97A72D5311C67FCC335ACA3900020EEB (void);
// 0x00000307 System.Collections.IEnumerator Obi.ObiActorBlueprint::Initialize()
// 0x00000308 System.Void Obi.ObiActorBlueprint::.ctor()
extern void ObiActorBlueprint__ctor_m9D95F18391AC084CE03CF008E1D47864DF75A636 (void);
// 0x00000309 System.Void Obi.ObiActorBlueprint/BlueprintCallback::.ctor(System.Object,System.IntPtr)
extern void BlueprintCallback__ctor_m75F65AE60505CE5651A0F020168360371AD0D0EE (void);
// 0x0000030A System.Void Obi.ObiActorBlueprint/BlueprintCallback::Invoke(Obi.ObiActorBlueprint)
extern void BlueprintCallback_Invoke_m0F0A317733BA923A00DCF82C10E2AF745667377A (void);
// 0x0000030B System.IAsyncResult Obi.ObiActorBlueprint/BlueprintCallback::BeginInvoke(Obi.ObiActorBlueprint,System.AsyncCallback,System.Object)
extern void BlueprintCallback_BeginInvoke_mD8A830F18B9F7B76E81DB436AF1462092202B3F3 (void);
// 0x0000030C System.Void Obi.ObiActorBlueprint/BlueprintCallback::EndInvoke(System.IAsyncResult)
extern void BlueprintCallback_EndInvoke_m35D1EBBFF55BAC420142369AEB4B24D44D031B3F (void);
// 0x0000030D System.Void Obi.ObiActorBlueprint/<GetConstraints>d__50::.ctor(System.Int32)
extern void U3CGetConstraintsU3Ed__50__ctor_m463D73E183DDE1A5C71E6E3CC821D0E7F30E347C (void);
// 0x0000030E System.Void Obi.ObiActorBlueprint/<GetConstraints>d__50::System.IDisposable.Dispose()
extern void U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m1C1B68AF4C49065E226E78E8D46FD68A406FC4DB (void);
// 0x0000030F System.Boolean Obi.ObiActorBlueprint/<GetConstraints>d__50::MoveNext()
extern void U3CGetConstraintsU3Ed__50_MoveNext_m440BD75AC003FACCE06308279346A5432EDC9BFC (void);
// 0x00000310 Obi.IObiConstraints Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.Generic.IEnumerator<Obi.IObiConstraints>.get_Current()
extern void U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m603F544C865B5A382E4D45071F9EA103E35296E2 (void);
// 0x00000311 System.Void Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.IEnumerator.Reset()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m8E40706068668910D9F7D8A1EACB85F8E6FFF77A (void);
// 0x00000312 System.Object Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.IEnumerator.get_Current()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mD7F6DAA1BCB1AB519151F38ACED86E62BB422730 (void);
// 0x00000313 System.Collections.Generic.IEnumerator`1<Obi.IObiConstraints> Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.Generic.IEnumerable<Obi.IObiConstraints>.GetEnumerator()
extern void U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_mFFFD55BE544D6CB567D145A4DCB2564DD48B4CDF (void);
// 0x00000314 System.Collections.IEnumerator Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m3284B5FE565ECE74AF7F1FBE65BCD30C41CADA8D (void);
// 0x00000315 System.Void Obi.ObiActorBlueprint/<Generate>d__59::.ctor(System.Int32)
extern void U3CGenerateU3Ed__59__ctor_mD0933DCC8582C89719A5869AA40F1D96BC46A2A1 (void);
// 0x00000316 System.Void Obi.ObiActorBlueprint/<Generate>d__59::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__59_System_IDisposable_Dispose_m4C2610F3D57B4EB14EE56E2E116536B5A23C8B22 (void);
// 0x00000317 System.Boolean Obi.ObiActorBlueprint/<Generate>d__59::MoveNext()
extern void U3CGenerateU3Ed__59_MoveNext_m1B248A89761AD00C954EAEB25335090B6378CB9E (void);
// 0x00000318 System.Object Obi.ObiActorBlueprint/<Generate>d__59::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7A53073B04207358D27C5F279097C3AECBDC48E (void);
// 0x00000319 System.Void Obi.ObiActorBlueprint/<Generate>d__59::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_m326C85F4CC091D9CEBD26372E5491D58808FECB6 (void);
// 0x0000031A System.Object Obi.ObiActorBlueprint/<Generate>d__59::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_mB78D5553C5C65C889A8A4E2F6FCCADA45472D734 (void);
// 0x0000031B System.Void Obi.ObiMeshBasedActorBlueprint::.ctor()
extern void ObiMeshBasedActorBlueprint__ctor_m68ACFAD2ACE6D36D748D434E2BD130B742B17BE8 (void);
// 0x0000031C Obi.ObiActorBlueprint Obi.ObiParticleGroup::get_blueprint()
extern void ObiParticleGroup_get_blueprint_m7B969093BC47FD5B58C6D6839D8F9E3BDA3119FF (void);
// 0x0000031D System.Void Obi.ObiParticleGroup::SetSourceBlueprint(Obi.ObiActorBlueprint)
extern void ObiParticleGroup_SetSourceBlueprint_m83016ECF67DCB8BDF873C154BB4A0EE7A8091011 (void);
// 0x0000031E System.Int32 Obi.ObiParticleGroup::get_Count()
extern void ObiParticleGroup_get_Count_m98490363FF19DE1FFEBC361D7302AEF05B6F21B4 (void);
// 0x0000031F System.Boolean Obi.ObiParticleGroup::ContainsParticle(System.Int32)
extern void ObiParticleGroup_ContainsParticle_mFF1BABB2B3D4D3CD4AE1E5B44C3DC83765EB11AC (void);
// 0x00000320 System.Void Obi.ObiParticleGroup::.ctor()
extern void ObiParticleGroup__ctor_mD6FDCA405D8CBEB22044CFD3635E3202AF1A45A2 (void);
// 0x00000321 System.Void Obi.ObiBoxShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.BoxCollider2D)
extern void ObiBoxShapeTracker2D__ctor_m628217C8133AD0BA22DE8815770C03CD31236C8F (void);
// 0x00000322 System.Boolean Obi.ObiBoxShapeTracker2D::UpdateIfNeeded()
extern void ObiBoxShapeTracker2D_UpdateIfNeeded_m7CF0C0561350D4A21F8FC768C7158E4BAE328434 (void);
// 0x00000323 System.Void Obi.ObiCapsuleShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.CapsuleCollider2D)
extern void ObiCapsuleShapeTracker2D__ctor_m9F0647AFDA77ACDB5B00DDA8F614F2CD3F61F464 (void);
// 0x00000324 System.Boolean Obi.ObiCapsuleShapeTracker2D::UpdateIfNeeded()
extern void ObiCapsuleShapeTracker2D_UpdateIfNeeded_m65EF7C477B78DF113F5C43446E266DA9790B333A (void);
// 0x00000325 System.Void Obi.ObiCircleShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.CircleCollider2D)
extern void ObiCircleShapeTracker2D__ctor_m91428FB089519F84073171B1EF2AF27FA7029F13 (void);
// 0x00000326 System.Boolean Obi.ObiCircleShapeTracker2D::UpdateIfNeeded()
extern void ObiCircleShapeTracker2D_UpdateIfNeeded_m855CD60D865E47BD776A38CE2F44554BD5B69E49 (void);
// 0x00000327 System.Void Obi.ObiEdgeShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.EdgeCollider2D)
extern void ObiEdgeShapeTracker2D__ctor_m9CF5E0F349BC73573202F01E32482578DE1C1A10 (void);
// 0x00000328 System.Void Obi.ObiEdgeShapeTracker2D::UpdateEdgeData()
extern void ObiEdgeShapeTracker2D_UpdateEdgeData_m7208F5B1FF03775D5A425836047FB54E9BB747C1 (void);
// 0x00000329 System.Boolean Obi.ObiEdgeShapeTracker2D::UpdateIfNeeded()
extern void ObiEdgeShapeTracker2D_UpdateIfNeeded_m380635280878C5D86F90898E11CAD1664E3E4BDC (void);
// 0x0000032A System.Void Obi.ObiEdgeShapeTracker2D::Destroy()
extern void ObiEdgeShapeTracker2D_Destroy_m01DDDC54A11D898BAABA7391098DD4D2458CA946 (void);
// 0x0000032B System.Void Obi.ObiBoxShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.BoxCollider)
extern void ObiBoxShapeTracker__ctor_mE75CAF1F86BB53D6AEF98085C09D17E66DEEF582 (void);
// 0x0000032C System.Boolean Obi.ObiBoxShapeTracker::UpdateIfNeeded()
extern void ObiBoxShapeTracker_UpdateIfNeeded_mF819B8E4C30C6AFAF29A4117798FDF1316EA834A (void);
// 0x0000032D System.Void Obi.ObiCapsuleShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.CapsuleCollider)
extern void ObiCapsuleShapeTracker__ctor_mBE6839D0297E242539ABACA7E8D10CC7D1FF90BD (void);
// 0x0000032E System.Boolean Obi.ObiCapsuleShapeTracker::UpdateIfNeeded()
extern void ObiCapsuleShapeTracker_UpdateIfNeeded_mE44AC47537FB4437697D85609FB76AC306E29C1A (void);
// 0x0000032F System.Void Obi.ObiCharacterControllerShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.CharacterController)
extern void ObiCharacterControllerShapeTracker__ctor_m58178DB03B830C4137C4E297650BAB50EFD3625E (void);
// 0x00000330 System.Boolean Obi.ObiCharacterControllerShapeTracker::UpdateIfNeeded()
extern void ObiCharacterControllerShapeTracker_UpdateIfNeeded_m37B806A39DB2EEF9B54A5B5F191B7F9C7602E517 (void);
// 0x00000331 System.Void Obi.ObiDistanceFieldShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.Component,Obi.ObiDistanceField)
extern void ObiDistanceFieldShapeTracker__ctor_mA5B4583A9547EB0FE0C666EBB88E671DEC7F9035 (void);
// 0x00000332 System.Void Obi.ObiDistanceFieldShapeTracker::UpdateDistanceFieldData()
extern void ObiDistanceFieldShapeTracker_UpdateDistanceFieldData_mA701C6AE3562FD84B9C2C56A60A80A88D8B961E7 (void);
// 0x00000333 System.Boolean Obi.ObiDistanceFieldShapeTracker::UpdateIfNeeded()
extern void ObiDistanceFieldShapeTracker_UpdateIfNeeded_mA4E6477716FF64D462625E6D8F4898A7E47BD384 (void);
// 0x00000334 System.Void Obi.ObiDistanceFieldShapeTracker::Destroy()
extern void ObiDistanceFieldShapeTracker_Destroy_mF2D78CB9BA87BFF2D01E82CA3796B3E975B387C3 (void);
// 0x00000335 System.Void Obi.ObiMeshShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.MeshCollider)
extern void ObiMeshShapeTracker__ctor_m08CD689CF8203745944B75624F6F10B9B5991B41 (void);
// 0x00000336 System.Void Obi.ObiMeshShapeTracker::UpdateMeshData()
extern void ObiMeshShapeTracker_UpdateMeshData_m3FE402BFC2EA1B1AEF97C10345A781D1F1453374 (void);
// 0x00000337 System.Boolean Obi.ObiMeshShapeTracker::UpdateIfNeeded()
extern void ObiMeshShapeTracker_UpdateIfNeeded_m12F1895305D96BA383B3C6D04D0960CDE65BB9DD (void);
// 0x00000338 System.Void Obi.ObiMeshShapeTracker::Destroy()
extern void ObiMeshShapeTracker_Destroy_mF175565DFFD64344104928752D528653AEEF4C1D (void);
// 0x00000339 System.Void Obi.ObiShapeTracker::Destroy()
extern void ObiShapeTracker_Destroy_m0FF375956D9FB5363A7595568F62FC49288D102A (void);
// 0x0000033A System.Boolean Obi.ObiShapeTracker::UpdateIfNeeded()
// 0x0000033B System.Void Obi.ObiShapeTracker::.ctor()
extern void ObiShapeTracker__ctor_mD3C85F1D395CDC2FA8F0FADBF02F5035493C1A7C (void);
// 0x0000033C System.Void Obi.ObiSphereShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.SphereCollider)
extern void ObiSphereShapeTracker__ctor_m1F91597098DFDE9D3D331BB46F9885EF84816FCC (void);
// 0x0000033D System.Boolean Obi.ObiSphereShapeTracker::UpdateIfNeeded()
extern void ObiSphereShapeTracker_UpdateIfNeeded_m956A2557E0849CFD2C667CED772595BB3DB2517F (void);
// 0x0000033E System.Void Obi.ObiTerrainShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.TerrainCollider)
extern void ObiTerrainShapeTracker__ctor_m7BEE7A899503A6A8D1B542BB48B2902966AB22B6 (void);
// 0x0000033F System.Void Obi.ObiTerrainShapeTracker::UpdateHeightData()
extern void ObiTerrainShapeTracker_UpdateHeightData_mF979C90B8BF4BAA1D77BB55829060BE1D02DB1FB (void);
// 0x00000340 System.Boolean Obi.ObiTerrainShapeTracker::UpdateIfNeeded()
extern void ObiTerrainShapeTracker_UpdateIfNeeded_mF4287DBBCE367005E4F1EF1342AB50882CDBE460 (void);
// 0x00000341 System.Void Obi.ObiTerrainShapeTracker::Destroy()
extern void ObiTerrainShapeTracker_Destroy_m778990BC8F8E80F2EAD34786597F7803DA9E55EE (void);
// 0x00000342 System.Void Obi.ObiCollider::set_sourceCollider(UnityEngine.Collider)
extern void ObiCollider_set_sourceCollider_mE74FCFF39705097FEAD7580CD74ADA13071B3CB1 (void);
// 0x00000343 UnityEngine.Collider Obi.ObiCollider::get_sourceCollider()
extern void ObiCollider_get_sourceCollider_m9128E7F1E524ACFDD350E7347570A6253DFDB19D (void);
// 0x00000344 System.Void Obi.ObiCollider::set_distanceField(Obi.ObiDistanceField)
extern void ObiCollider_set_distanceField_m1E7758FFA96690595BB90B1BEB48B7AD958EB53F (void);
// 0x00000345 Obi.ObiDistanceField Obi.ObiCollider::get_distanceField()
extern void ObiCollider_get_distanceField_mC9798980E3932FBA4649A14AB780CA819D06B050 (void);
// 0x00000346 System.Void Obi.ObiCollider::CreateTracker()
extern void ObiCollider_CreateTracker_mD3862A5A96ACE781C43578C71C7F3B34597B72B5 (void);
// 0x00000347 UnityEngine.Component Obi.ObiCollider::GetUnityCollider(System.Boolean&)
extern void ObiCollider_GetUnityCollider_m8F9125F3CE4C77B54A293FCB58A506F9003F1645 (void);
// 0x00000348 System.Void Obi.ObiCollider::FindSourceCollider()
extern void ObiCollider_FindSourceCollider_m637BA0D9C7416963F07A6C91C46898E63A2AA7E6 (void);
// 0x00000349 System.Void Obi.ObiCollider::.ctor()
extern void ObiCollider__ctor_mC40B217031806CE6CC171DB8519F2BDC13237937 (void);
// 0x0000034A System.Void Obi.ObiCollider2D::set_SourceCollider(UnityEngine.Collider2D)
extern void ObiCollider2D_set_SourceCollider_m1018D5A5D84BEE9BFF223E7C96DFEF6391A61025 (void);
// 0x0000034B UnityEngine.Collider2D Obi.ObiCollider2D::get_SourceCollider()
extern void ObiCollider2D_get_SourceCollider_m0F0FA6FE50C824AA1D508B0624CFC78BC5CB1324 (void);
// 0x0000034C System.Void Obi.ObiCollider2D::CreateTracker()
extern void ObiCollider2D_CreateTracker_mF85B5C235CC7FD0AA628697696F24E75B0677D95 (void);
// 0x0000034D UnityEngine.Component Obi.ObiCollider2D::GetUnityCollider(System.Boolean&)
extern void ObiCollider2D_GetUnityCollider_mC7022D7CE3DD5DAFC483FE44489C1826C6F719A9 (void);
// 0x0000034E System.Void Obi.ObiCollider2D::FindSourceCollider()
extern void ObiCollider2D_FindSourceCollider_m1C3542EAB959FFB8DD376825B89F1FCF6EF5B71A (void);
// 0x0000034F System.Void Obi.ObiCollider2D::.ctor()
extern void ObiCollider2D__ctor_m1D8E050EE358DD5E91FBCBC06CEEA010D1096877 (void);
// 0x00000350 System.Void Obi.ObiColliderBase::set_CollisionMaterial(Obi.ObiCollisionMaterial)
extern void ObiColliderBase_set_CollisionMaterial_m79AC8EC4C2B24D56D2EEB8E7E3EC8758D71DBCEF (void);
// 0x00000351 Obi.ObiCollisionMaterial Obi.ObiColliderBase::get_CollisionMaterial()
extern void ObiColliderBase_get_CollisionMaterial_m0DF69ECB8E1D3B265A93C3411558938452320DB8 (void);
// 0x00000352 System.Void Obi.ObiColliderBase::set_Filter(System.Int32)
extern void ObiColliderBase_set_Filter_m7EFACC9D0DDC53860DD8EC7711CA2B537861B143 (void);
// 0x00000353 System.Int32 Obi.ObiColliderBase::get_Filter()
extern void ObiColliderBase_get_Filter_mC113E8E2C6A721BDC36A1380CDA7B1274877FA9F (void);
// 0x00000354 System.Void Obi.ObiColliderBase::set_Thickness(System.Single)
extern void ObiColliderBase_set_Thickness_m8B643679A1B8F3502E0983283BFAFC788F59FED7 (void);
// 0x00000355 System.Single Obi.ObiColliderBase::get_Thickness()
extern void ObiColliderBase_get_Thickness_m39EFFE2661BFC77996AFE413C11C9842813B0CCD (void);
// 0x00000356 Obi.ObiShapeTracker Obi.ObiColliderBase::get_Tracker()
extern void ObiColliderBase_get_Tracker_mC248A17400AE5158EBE13D85059023F2777EF96E (void);
// 0x00000357 Obi.ObiColliderHandle Obi.ObiColliderBase::get_Handle()
extern void ObiColliderBase_get_Handle_mC54EFB3D8F0D65205AECE01C1523F9684610FDF9 (void);
// 0x00000358 System.IntPtr Obi.ObiColliderBase::get_OniCollider()
extern void ObiColliderBase_get_OniCollider_m1A453A14EEE1A71AD174EA6FD490CBF11FB13516 (void);
// 0x00000359 Obi.ObiRigidbodyBase Obi.ObiColliderBase::get_Rigidbody()
extern void ObiColliderBase_get_Rigidbody_mFE29C0B5E72DE47B2F8CECC10D691E3CB1113260 (void);
// 0x0000035A System.Void Obi.ObiColliderBase::CreateTracker()
// 0x0000035B UnityEngine.Component Obi.ObiColliderBase::GetUnityCollider(System.Boolean&)
// 0x0000035C System.Void Obi.ObiColliderBase::FindSourceCollider()
// 0x0000035D System.Void Obi.ObiColliderBase::CreateRigidbody()
extern void ObiColliderBase_CreateRigidbody_m28447722765BD28A1F74FD197E44CD73585CFA55 (void);
// 0x0000035E System.Void Obi.ObiColliderBase::OnTransformParentChanged()
extern void ObiColliderBase_OnTransformParentChanged_mB24B89C5A1D09A5CDE05559DDDE3AE584CB45385 (void);
// 0x0000035F System.Void Obi.ObiColliderBase::AddCollider()
extern void ObiColliderBase_AddCollider_mAE172D1DDA35D52A9D36D68E68CB739157F34870 (void);
// 0x00000360 System.Void Obi.ObiColliderBase::RemoveCollider()
extern void ObiColliderBase_RemoveCollider_m95F3C0AD19C848FFF82DF5DB8584D5E949F808DC (void);
// 0x00000361 System.Void Obi.ObiColliderBase::UpdateIfNeeded()
extern void ObiColliderBase_UpdateIfNeeded_mF9F82931F7D1F7703B77125BB3612E1781EF4622 (void);
// 0x00000362 System.Void Obi.ObiColliderBase::OnEnable()
extern void ObiColliderBase_OnEnable_m9D06C6503D3AA87D2ED910B831C6927655449D5C (void);
// 0x00000363 System.Void Obi.ObiColliderBase::OnDisable()
extern void ObiColliderBase_OnDisable_mDCA28DAF071BB2F8AB35E5A290B1935BB47F320F (void);
// 0x00000364 System.Void Obi.ObiColliderBase::.ctor()
extern void ObiColliderBase__ctor_m0425A5B3CFADDE3908C4CDE7564045D2E49156C6 (void);
// 0x00000365 System.Boolean Obi.ObiResourceHandle`1::get_isValid()
// 0x00000366 System.Void Obi.ObiResourceHandle`1::Invalidate()
// 0x00000367 System.Void Obi.ObiResourceHandle`1::Reference()
// 0x00000368 System.Boolean Obi.ObiResourceHandle`1::Dereference()
// 0x00000369 System.Void Obi.ObiResourceHandle`1::.ctor(System.Int32)
// 0x0000036A System.Void Obi.ObiColliderHandle::.ctor(System.Int32)
extern void ObiColliderHandle__ctor_m928271CA0B5407C025BBCC1A4C1B41A772842B98 (void);
// 0x0000036B System.Void Obi.ObiCollisionMaterialHandle::.ctor(System.Int32)
extern void ObiCollisionMaterialHandle__ctor_mF9EF7E13E41C1CFA35A66984FEAE0EA7A6E01622 (void);
// 0x0000036C System.Void Obi.ObiRigidbodyHandle::.ctor(System.Int32)
extern void ObiRigidbodyHandle__ctor_m7A187DF1938668726DB0353C6DF5A404B9661FDB (void);
// 0x0000036D Obi.ObiColliderWorld Obi.ObiColliderWorld::GetInstance()
extern void ObiColliderWorld_GetInstance_mDCD3CE9448DD243AF9314FC3AE6EFF25A0A9D804 (void);
// 0x0000036E System.Void Obi.ObiColliderWorld::Initialize()
extern void ObiColliderWorld_Initialize_mA686CF0330B087A58794A38B02A5C474C0ACE80D (void);
// 0x0000036F System.Void Obi.ObiColliderWorld::Destroy()
extern void ObiColliderWorld_Destroy_mB2941E38388F04B017C98D209866BF6234D85C66 (void);
// 0x00000370 System.Void Obi.ObiColliderWorld::DestroyIfUnused()
extern void ObiColliderWorld_DestroyIfUnused_mF7FB902FE93CE094938D57DC0048249F810AFAB0 (void);
// 0x00000371 System.Void Obi.ObiColliderWorld::RegisterImplementation(Obi.IColliderWorldImpl)
extern void ObiColliderWorld_RegisterImplementation_m04E6D07B4FE9DB89CB602DDE1177100646AB5F80 (void);
// 0x00000372 System.Void Obi.ObiColliderWorld::UnregisterImplementation(Obi.IColliderWorldImpl)
extern void ObiColliderWorld_UnregisterImplementation_m1FB0CC2F87786339836A3FE23662CE7E87AFD313 (void);
// 0x00000373 Obi.ObiColliderHandle Obi.ObiColliderWorld::CreateCollider()
extern void ObiColliderWorld_CreateCollider_m209298226F546EF09F80E9DC3C9CEDB9EA6A3ECF (void);
// 0x00000374 Obi.ObiRigidbodyHandle Obi.ObiColliderWorld::CreateRigidbody()
extern void ObiColliderWorld_CreateRigidbody_m1A6A020A8B5E9F2333767813122FAD64F13E58C7 (void);
// 0x00000375 Obi.ObiCollisionMaterialHandle Obi.ObiColliderWorld::CreateCollisionMaterial()
extern void ObiColliderWorld_CreateCollisionMaterial_m88809726D6F5777CA5CA979375202E079FBA523F (void);
// 0x00000376 Obi.ObiTriangleMeshHandle Obi.ObiColliderWorld::GetOrCreateTriangleMesh(UnityEngine.Mesh)
extern void ObiColliderWorld_GetOrCreateTriangleMesh_m68BA906C938414D9FA39EF9434D16EDD5BC6B008 (void);
// 0x00000377 System.Void Obi.ObiColliderWorld::DestroyTriangleMesh(Obi.ObiTriangleMeshHandle)
extern void ObiColliderWorld_DestroyTriangleMesh_m0BAE4F6D11B77B8B13C2D4C1DE5A6140E76E582C (void);
// 0x00000378 Obi.ObiEdgeMeshHandle Obi.ObiColliderWorld::GetOrCreateEdgeMesh(UnityEngine.EdgeCollider2D)
extern void ObiColliderWorld_GetOrCreateEdgeMesh_mD3188273479E4DC678356643F456C0AA97BFBD1B (void);
// 0x00000379 System.Void Obi.ObiColliderWorld::DestroyEdgeMesh(Obi.ObiEdgeMeshHandle)
extern void ObiColliderWorld_DestroyEdgeMesh_mC35DD757DCFE047FCE86C3190EEFA0B19A891306 (void);
// 0x0000037A Obi.ObiDistanceFieldHandle Obi.ObiColliderWorld::GetOrCreateDistanceField(Obi.ObiDistanceField)
extern void ObiColliderWorld_GetOrCreateDistanceField_mD90630455B72C5BAD3E94DAE4F3FE4E979E2C431 (void);
// 0x0000037B System.Void Obi.ObiColliderWorld::DestroyDistanceField(Obi.ObiDistanceFieldHandle)
extern void ObiColliderWorld_DestroyDistanceField_mB645511AF520AEF6AB93B8EE03F0A2A7D62ECEE8 (void);
// 0x0000037C Obi.ObiHeightFieldHandle Obi.ObiColliderWorld::GetOrCreateHeightField(UnityEngine.TerrainData)
extern void ObiColliderWorld_GetOrCreateHeightField_m67768E9620255D9407B6D664A21B5F46A06DA9DD (void);
// 0x0000037D System.Void Obi.ObiColliderWorld::DestroyHeightField(Obi.ObiHeightFieldHandle)
extern void ObiColliderWorld_DestroyHeightField_mEDED727FD4CD60E71555E1F0B7223CF0C2F060D7 (void);
// 0x0000037E System.Void Obi.ObiColliderWorld::DestroyCollider(Obi.ObiColliderHandle)
extern void ObiColliderWorld_DestroyCollider_m834E6E9C211C47C4746C0658D072D11488444C7D (void);
// 0x0000037F System.Void Obi.ObiColliderWorld::DestroyRigidbody(Obi.ObiRigidbodyHandle)
extern void ObiColliderWorld_DestroyRigidbody_m06DB0A63D4471A043E38A1BFEC429E693E36E82B (void);
// 0x00000380 System.Void Obi.ObiColliderWorld::DestroyCollisionMaterial(Obi.ObiCollisionMaterialHandle)
extern void ObiColliderWorld_DestroyCollisionMaterial_m95080D33292635B836E68FDF7DBD877FF51A3302 (void);
// 0x00000381 System.Void Obi.ObiColliderWorld::UpdateColliders()
extern void ObiColliderWorld_UpdateColliders_mFE5C2781107A2BF876523BC7D1A371930FA231DA (void);
// 0x00000382 System.Void Obi.ObiColliderWorld::UpdateRigidbodies(System.Collections.Generic.List`1<Obi.ObiSolver>,System.Single)
extern void ObiColliderWorld_UpdateRigidbodies_m8561FC0652FF0142400D2886A02C9EFAEB685819 (void);
// 0x00000383 System.Void Obi.ObiColliderWorld::UpdateWorld(System.Single)
extern void ObiColliderWorld_UpdateWorld_m864B8D4C74E8309F9FA29064D39B1A37FCC9538D (void);
// 0x00000384 System.Void Obi.ObiColliderWorld::UpdateRigidbodyVelocities(System.Collections.Generic.List`1<Obi.ObiSolver>)
extern void ObiColliderWorld_UpdateRigidbodyVelocities_m98240C979197C4339F335E642B3DF480F7A281E3 (void);
// 0x00000385 System.Void Obi.ObiColliderWorld::.ctor()
extern void ObiColliderWorld__ctor_mB72724E3C4F8ABB4A7845EA540DF7FE796E1E971 (void);
// 0x00000386 Obi.ObiCollisionMaterialHandle Obi.ObiCollisionMaterial::get_handle()
extern void ObiCollisionMaterial_get_handle_m594CB055313A49F8543490E78B1F48E7C21C7462 (void);
// 0x00000387 System.Void Obi.ObiCollisionMaterial::OnEnable()
extern void ObiCollisionMaterial_OnEnable_m8FA0F5FEF8BDA6E093C8A903CD09B45F107B87F3 (void);
// 0x00000388 System.Void Obi.ObiCollisionMaterial::OnDisable()
extern void ObiCollisionMaterial_OnDisable_m835688D4697C04CFE7B9F09ECBA5E0DD0201EE4E (void);
// 0x00000389 System.Void Obi.ObiCollisionMaterial::OnValidate()
extern void ObiCollisionMaterial_OnValidate_m39E140FEFC698F6790902D5698BCCDAA4C037250 (void);
// 0x0000038A System.Void Obi.ObiCollisionMaterial::UpdateMaterial()
extern void ObiCollisionMaterial_UpdateMaterial_m01C9A16AD952A5AC4E018F035CD98AD7C3F7D6F2 (void);
// 0x0000038B System.Void Obi.ObiCollisionMaterial::CreateMaterialIfNeeded()
extern void ObiCollisionMaterial_CreateMaterialIfNeeded_m7B6E786F93918CFCBCAFE79D73C6D39DB7D2E467 (void);
// 0x0000038C System.Void Obi.ObiCollisionMaterial::.ctor()
extern void ObiCollisionMaterial__ctor_m72400798714AEFF19085712777080ABFBB13BAC6 (void);
// 0x0000038D System.Boolean Obi.ObiDistanceField::get_Initialized()
extern void ObiDistanceField_get_Initialized_mAABFFF3B58AB387E5CB5FB8C1AB746B0247F983C (void);
// 0x0000038E UnityEngine.Bounds Obi.ObiDistanceField::get_FieldBounds()
extern void ObiDistanceField_get_FieldBounds_m9863DC2138A3DBE77E04380C54236345A1133CBD (void);
// 0x0000038F System.Single Obi.ObiDistanceField::get_EffectiveSampleSize()
extern void ObiDistanceField_get_EffectiveSampleSize_mB9F3DB29086DB4AD2249B28C1C8C888484833AAD (void);
// 0x00000390 System.Void Obi.ObiDistanceField::set_InputMesh(UnityEngine.Mesh)
extern void ObiDistanceField_set_InputMesh_mD2BFC4B7BAE4B8EBEFF52AC8E89CF319F03D3780 (void);
// 0x00000391 UnityEngine.Mesh Obi.ObiDistanceField::get_InputMesh()
extern void ObiDistanceField_get_InputMesh_m9B9ABB7CAE379468D3FACDBD886581F843FA22B5 (void);
// 0x00000392 System.Void Obi.ObiDistanceField::Reset()
extern void ObiDistanceField_Reset_m00EC5397C61DF8D42A0E2201EBE65C30AB391DF8 (void);
// 0x00000393 System.Collections.IEnumerator Obi.ObiDistanceField::Generate()
extern void ObiDistanceField_Generate_m2FF083D72E253BE27D9DFD07C8816B449F778266 (void);
// 0x00000394 UnityEngine.Texture3D Obi.ObiDistanceField::GetVolumeTexture(System.Int32)
extern void ObiDistanceField_GetVolumeTexture_m3C63485A3F2BB3E0D84E496DFD190836DD7C9A64 (void);
// 0x00000395 System.Void Obi.ObiDistanceField::.ctor()
extern void ObiDistanceField__ctor_m79451FE12A75F2ED6B511F1DFC382E6493E09DB2 (void);
// 0x00000396 System.Void Obi.ObiDistanceField/<Generate>d__16::.ctor(System.Int32)
extern void U3CGenerateU3Ed__16__ctor_m926CEA2198C2B31432AD7B0C629E0026FD9135A8 (void);
// 0x00000397 System.Void Obi.ObiDistanceField/<Generate>d__16::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__16_System_IDisposable_Dispose_mE8B29A18143A848E1C4C46A4D41CE2EE66999F73 (void);
// 0x00000398 System.Boolean Obi.ObiDistanceField/<Generate>d__16::MoveNext()
extern void U3CGenerateU3Ed__16_MoveNext_mE09C7584F62ED7F884B6289D784C5474BAB6648D (void);
// 0x00000399 System.Object Obi.ObiDistanceField/<Generate>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39320DBD9BADA847C3DC1B06385734720B3ED5D1 (void);
// 0x0000039A System.Void Obi.ObiDistanceField/<Generate>d__16::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m94171808CA1CAE76B9F0511FE6570131CD857EBB (void);
// 0x0000039B System.Object Obi.ObiDistanceField/<Generate>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mE403FD60E16DACEE30BE06E73A882BBF823618FC (void);
// 0x0000039C System.Void Obi.ObiDistanceFieldHandle::.ctor(Obi.ObiDistanceField,System.Int32)
extern void ObiDistanceFieldHandle__ctor_m6E0E72314F9B3CD649F6E52743A19A05BDD53486 (void);
// 0x0000039D System.Void Obi.DistanceFieldHeader::.ctor(System.Int32,System.Int32)
extern void DistanceFieldHeader__ctor_mD9023657AE562DC6E93CAEBAEA87D8BBFBE3CAC0 (void);
// 0x0000039E System.Void Obi.ObiDistanceFieldContainer::.ctor()
extern void ObiDistanceFieldContainer__ctor_m3770E97A1E94FE7F79D6C4476A4B5538615321FC (void);
// 0x0000039F Obi.ObiDistanceFieldHandle Obi.ObiDistanceFieldContainer::GetOrCreateDistanceField(Obi.ObiDistanceField)
extern void ObiDistanceFieldContainer_GetOrCreateDistanceField_mACCA0F4E84E3D53991F18FEC18CCC8230451C913 (void);
// 0x000003A0 System.Void Obi.ObiDistanceFieldContainer::DestroyDistanceField(Obi.ObiDistanceFieldHandle)
extern void ObiDistanceFieldContainer_DestroyDistanceField_m85914D4DE23293E844CC8082A6B7B4F427C8505B (void);
// 0x000003A1 System.Void Obi.ObiDistanceFieldContainer::Dispose()
extern void ObiDistanceFieldContainer_Dispose_m7A92A1441B9CB196E1D25BF4587B01FDFF2CA2A8 (void);
// 0x000003A2 System.Void Obi.Edge::.ctor(System.Int32,System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern void Edge__ctor_m58FEEBE96D412AFED6635E824686137251119088 (void);
// 0x000003A3 Obi.Aabb Obi.Edge::GetBounds()
extern void Edge_GetBounds_m3627A8DC0128EA13268432FD42FDFFA5E9E8FC16 (void);
// 0x000003A4 System.Void Obi.ObiEdgeMeshHandle::.ctor(UnityEngine.EdgeCollider2D,System.Int32)
extern void ObiEdgeMeshHandle__ctor_m055ABDB8025F78423ED6926D1E316EB48FABC4AE (void);
// 0x000003A5 System.Void Obi.EdgeMeshHeader::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void EdgeMeshHeader__ctor_mFBC1ECFE2844D2E52FDDEF03C23174C84E12C124 (void);
// 0x000003A6 System.Void Obi.ObiEdgeMeshContainer::.ctor()
extern void ObiEdgeMeshContainer__ctor_m7E7182023FCAB6FD0840A93D3F33CD76868DEC2D (void);
// 0x000003A7 Obi.ObiEdgeMeshHandle Obi.ObiEdgeMeshContainer::GetOrCreateEdgeMesh(UnityEngine.EdgeCollider2D)
extern void ObiEdgeMeshContainer_GetOrCreateEdgeMesh_m08E80D7BB9E4C1AD3040ECCB3E3F80BB0EBC5127 (void);
// 0x000003A8 System.Void Obi.ObiEdgeMeshContainer::DestroyEdgeMesh(Obi.ObiEdgeMeshHandle)
extern void ObiEdgeMeshContainer_DestroyEdgeMesh_m6A49B7BD3DACF76A1B0E7F591A523795F55758EC (void);
// 0x000003A9 System.Void Obi.ObiEdgeMeshContainer::Dispose()
extern void ObiEdgeMeshContainer_Dispose_m1005586E5D7BE477D43008737857278C3088223A (void);
// 0x000003AA System.Void Obi.ObiEdgeMeshContainer/<>c::.cctor()
extern void U3CU3Ec__cctor_m319B04707AB4F191DD80BB15D8CE099EE0A1A7B4 (void);
// 0x000003AB System.Void Obi.ObiEdgeMeshContainer/<>c::.ctor()
extern void U3CU3Ec__ctor_m0A7F956BDB67E4A8C2BCD9D83C276D02DC561279 (void);
// 0x000003AC Obi.Edge Obi.ObiEdgeMeshContainer/<>c::<GetOrCreateEdgeMesh>b__6_0(Obi.IBounded)
extern void U3CU3Ec_U3CGetOrCreateEdgeMeshU3Eb__6_0_mDEAC05CDAF4D92AFBB301FED8F1C6099004401DD (void);
// 0x000003AD System.Void Obi.ObiHeightFieldHandle::.ctor(UnityEngine.TerrainData,System.Int32)
extern void ObiHeightFieldHandle__ctor_m7487BDAEEC65A9BBFE0A38658CA884E309BC6FEA (void);
// 0x000003AE System.Void Obi.HeightFieldHeader::.ctor(System.Int32,System.Int32)
extern void HeightFieldHeader__ctor_m68533ABFD4D9CB9C9AE92C04D96F69D03704B421 (void);
// 0x000003AF System.Void Obi.ObiHeightFieldContainer::.ctor()
extern void ObiHeightFieldContainer__ctor_m5666C4D77395B12D8510DE3C1C91F7DF7080917E (void);
// 0x000003B0 Obi.ObiHeightFieldHandle Obi.ObiHeightFieldContainer::GetOrCreateHeightField(UnityEngine.TerrainData)
extern void ObiHeightFieldContainer_GetOrCreateHeightField_m24A1A0266B0B0558347D317ECAF37AE00CF5F73D (void);
// 0x000003B1 System.Void Obi.ObiHeightFieldContainer::DestroyHeightField(Obi.ObiHeightFieldHandle)
extern void ObiHeightFieldContainer_DestroyHeightField_m0C864AEC3E3DF063366196C10432FC0E9582C124 (void);
// 0x000003B2 System.Void Obi.ObiHeightFieldContainer::Dispose()
extern void ObiHeightFieldContainer_Dispose_m381A36CB411FE4B112A455EB52BEF459C07B542E (void);
// 0x000003B3 System.Void Obi.ObiRigidbody::Awake()
extern void ObiRigidbody_Awake_mF76BC3EB63FCF9C9203C0490FB685630EA22B0E2 (void);
// 0x000003B4 System.Void Obi.ObiRigidbody::UpdateKinematicVelocities(System.Single)
extern void ObiRigidbody_UpdateKinematicVelocities_mCB621E407E54750602D501EF243278687D054900 (void);
// 0x000003B5 System.Void Obi.ObiRigidbody::UpdateIfNeeded(System.Single)
extern void ObiRigidbody_UpdateIfNeeded_mE8E08B343D9013FC5168AC526FDD2FD60713C63F (void);
// 0x000003B6 System.Void Obi.ObiRigidbody::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiRigidbody_UpdateVelocities_m8383481CAC453BA89BF272E3DA3AC19A575ED21F (void);
// 0x000003B7 System.Void Obi.ObiRigidbody::.ctor()
extern void ObiRigidbody__ctor_mD7D0C954158E4444C3068E83B3327DF0C19F22B4 (void);
// 0x000003B8 System.Void Obi.ObiRigidbody2D::Awake()
extern void ObiRigidbody2D_Awake_mFBE100A1729F05F2F8C0D6F81A5D55A200C18D8F (void);
// 0x000003B9 System.Void Obi.ObiRigidbody2D::UpdateKinematicVelocities(System.Single)
extern void ObiRigidbody2D_UpdateKinematicVelocities_m9FA8BC7541C8841BA05CEFB2E5153BEF8D0F0998 (void);
// 0x000003BA System.Void Obi.ObiRigidbody2D::UpdateIfNeeded(System.Single)
extern void ObiRigidbody2D_UpdateIfNeeded_m2B7DDB040171C3260191C90FCA6E67EA1658C21F (void);
// 0x000003BB System.Void Obi.ObiRigidbody2D::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiRigidbody2D_UpdateVelocities_m9E81C9B71FCBB4C7F67940267609771648B6FC02 (void);
// 0x000003BC System.Void Obi.ObiRigidbody2D::.ctor()
extern void ObiRigidbody2D__ctor_m4B1B8A6B4DC629D021CE278C6D8926801ABF2B5A (void);
// 0x000003BD System.Void Obi.ObiRigidbodyBase::Awake()
extern void ObiRigidbodyBase_Awake_m76001FC03150D7505A5D630EA20933DF245BA3C7 (void);
// 0x000003BE System.Void Obi.ObiRigidbodyBase::OnDestroy()
extern void ObiRigidbodyBase_OnDestroy_mE8BDF21E79746CC2EF9F1CD492C425FE5FD46159 (void);
// 0x000003BF System.Void Obi.ObiRigidbodyBase::UpdateIfNeeded(System.Single)
// 0x000003C0 System.Void Obi.ObiRigidbodyBase::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
// 0x000003C1 System.Void Obi.ObiRigidbodyBase::.ctor()
extern void ObiRigidbodyBase__ctor_m97C098719F7244A8923BA99EB88178D5BC81E375 (void);
// 0x000003C2 System.Void Obi.Triangle::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Triangle__ctor_mF3FB7D0163513605C1284E55D476938CFA8D8B3E (void);
// 0x000003C3 Obi.Aabb Obi.Triangle::GetBounds()
extern void Triangle_GetBounds_m96DD7752DB622FBB5D130CC0203D16EF9A16183D (void);
// 0x000003C4 System.Void Obi.ObiTriangleMeshHandle::.ctor(UnityEngine.Mesh,System.Int32)
extern void ObiTriangleMeshHandle__ctor_m4C8FBE34410F6DE0216D655610C66D865609FD82 (void);
// 0x000003C5 System.Void Obi.TriangleMeshHeader::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void TriangleMeshHeader__ctor_m05352EA77E9896540763C1FC4DA9A86E8FD110E4 (void);
// 0x000003C6 System.Void Obi.ObiTriangleMeshContainer::.ctor()
extern void ObiTriangleMeshContainer__ctor_m7744CBB6264FB551DA0E62E0AE2B14750075FAFC (void);
// 0x000003C7 Obi.ObiTriangleMeshHandle Obi.ObiTriangleMeshContainer::GetOrCreateTriangleMesh(UnityEngine.Mesh)
extern void ObiTriangleMeshContainer_GetOrCreateTriangleMesh_m7966A3DEF57A37FDCD4EAB1254350FCF3C443C54 (void);
// 0x000003C8 System.Void Obi.ObiTriangleMeshContainer::DestroyTriangleMesh(Obi.ObiTriangleMeshHandle)
extern void ObiTriangleMeshContainer_DestroyTriangleMesh_mC9C784E4C7DD7154C4F28D56E3D49B0CCFDFA4A8 (void);
// 0x000003C9 System.Void Obi.ObiTriangleMeshContainer::Dispose()
extern void ObiTriangleMeshContainer_Dispose_m5B8C0A01AFA58E44642169B3B0230566C3901B80 (void);
// 0x000003CA System.Void Obi.ObiTriangleMeshContainer/<>c::.cctor()
extern void U3CU3Ec__cctor_m78E18BF63E0FC93B04931361A400A550DF8E42FE (void);
// 0x000003CB System.Void Obi.ObiTriangleMeshContainer/<>c::.ctor()
extern void U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8 (void);
// 0x000003CC Obi.Triangle Obi.ObiTriangleMeshContainer/<>c::<GetOrCreateTriangleMesh>b__6_0(Obi.IBounded)
extern void U3CU3Ec_U3CGetOrCreateTriangleMeshU3Eb__6_0_m55F60D5CC64D104ED8E0B37F968D4479C3124E42 (void);
// 0x000003CD System.Collections.IEnumerator Obi.ASDF::Build(System.Single,System.Int32,UnityEngine.Vector3[],System.Int32[],System.Collections.Generic.List`1<Obi.DFNode>,System.Int32)
extern void ASDF_Build_m0B3E28D5C89253E2A2FAA9545A0B748B5C69E1D4 (void);
// 0x000003CE System.Single Obi.ASDF::Sample(System.Collections.Generic.List`1<Obi.DFNode>,UnityEngine.Vector3)
extern void ASDF_Sample_mA68406A166BFA7E95D0C62E769CB30FC8F2C78C0 (void);
// 0x000003CF System.Void Obi.ASDF::.ctor()
extern void ASDF__ctor_mC6D0B839501A5338ADE298E18D27946D91973849 (void);
// 0x000003D0 System.Void Obi.ASDF::.cctor()
extern void ASDF__cctor_m3BDC04F77E29E81E3A541CFA21851E2153E7EC1A (void);
// 0x000003D1 System.Void Obi.ASDF/<>c::.cctor()
extern void U3CU3Ec__cctor_m02BD313FFAEEDA3D68EE3E46312C75EB44777102 (void);
// 0x000003D2 System.Void Obi.ASDF/<>c::.ctor()
extern void U3CU3Ec__ctor_m4CEC5676823706F8C33924A47217BB2E3C584BB8 (void);
// 0x000003D3 Obi.Triangle Obi.ASDF/<>c::<Build>b__3_0(Obi.IBounded)
extern void U3CU3Ec_U3CBuildU3Eb__3_0_m2F53BFE53C74A23E70A52AA23A3625EA199232CA (void);
// 0x000003D4 System.Void Obi.ASDF/<Build>d__3::.ctor(System.Int32)
extern void U3CBuildU3Ed__3__ctor_m2AF6E46C3AF1DCE8EB0AC87345B0082720891BF3 (void);
// 0x000003D5 System.Void Obi.ASDF/<Build>d__3::System.IDisposable.Dispose()
extern void U3CBuildU3Ed__3_System_IDisposable_Dispose_m407D5BEFA6CC050E62B83A69FC1C81FA11062FE4 (void);
// 0x000003D6 System.Boolean Obi.ASDF/<Build>d__3::MoveNext()
extern void U3CBuildU3Ed__3_MoveNext_m2878CF634F624CD9D2E068EB30A27BFF838B1AE9 (void);
// 0x000003D7 System.Object Obi.ASDF/<Build>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FD71D70EB820213EDF7BC3853561DF86E43D30D (void);
// 0x000003D8 System.Void Obi.ASDF/<Build>d__3::System.Collections.IEnumerator.Reset()
extern void U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m026B7C1E35013639385A979664E9479DCFB7269A (void);
// 0x000003D9 System.Object Obi.ASDF/<Build>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_m8730288CA28692A77A479C27FB99AFB9421E825B (void);
// 0x000003DA System.Void Obi.DFNode::.ctor(UnityEngine.Vector4)
extern void DFNode__ctor_m5AA5A22FD71D7439C9136C2FBC6F151E25C8A609 (void);
// 0x000003DB System.Single Obi.DFNode::Sample(UnityEngine.Vector3)
extern void DFNode_Sample_mD228ACC5980D03D69BA1510383BA88AD4253D1D3 (void);
// 0x000003DC UnityEngine.Vector3 Obi.DFNode::GetNormalizedPos(UnityEngine.Vector3)
extern void DFNode_GetNormalizedPos_m48205296253B504FB1F9D3177EADD8E62A70F31B (void);
// 0x000003DD System.Int32 Obi.DFNode::GetOctant(UnityEngine.Vector3)
extern void DFNode_GetOctant_m8E57E952AB3E07E8596E051854C4B9EF62575192 (void);
// 0x000003DE UnityEngine.Vector4 Obi.Aabb::get_center()
extern void Aabb_get_center_m6F29EA934C6E7C6A2557550214746BE327737113 (void);
// 0x000003DF UnityEngine.Vector4 Obi.Aabb::get_size()
extern void Aabb_get_size_m316B3CACF5BB496D6CF8532AF06AF826C78486D7 (void);
// 0x000003E0 System.Void Obi.Aabb::.ctor(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Aabb__ctor_m8B1C8639B79DD7C951EF63FD820BB63B8A6B5DB4 (void);
// 0x000003E1 System.Void Obi.Aabb::.ctor(UnityEngine.Vector4)
extern void Aabb__ctor_m293B0520F59E98F3B14FEAFE282685CA663D2E0B (void);
// 0x000003E2 System.Void Obi.Aabb::Encapsulate(UnityEngine.Vector4)
extern void Aabb_Encapsulate_mDCE164B716BFA4A7F8147BC997DDDAFF5A0A9F08 (void);
// 0x000003E3 System.Void Obi.Aabb::Encapsulate(Obi.Aabb)
extern void Aabb_Encapsulate_m4F8536D0A22C6A596F1393D02F97BA7C45B9F9A8 (void);
// 0x000003E4 System.Void Obi.Aabb::FromBounds(UnityEngine.Bounds,System.Single,System.Boolean)
extern void Aabb_FromBounds_mB313907D1B69DA3917270DE5C2B39AD5ECCCF664 (void);
// 0x000003E5 System.Void Obi.AffineTransform::.ctor(UnityEngine.Vector4,UnityEngine.Quaternion,UnityEngine.Vector4)
extern void AffineTransform__ctor_m1113EA596451380FE97BF986F98D180B67B50914 (void);
// 0x000003E6 System.Void Obi.AffineTransform::FromTransform(UnityEngine.Transform,System.Boolean)
extern void AffineTransform_FromTransform_mF590DA4EFBCE17F0245A9C33FE65B7405154A7EB (void);
// 0x000003E7 Obi.BIHNode[] Obi.BIH::Build(Obi.IBounded[]&,System.Int32,System.Single)
extern void BIH_Build_m543AD7EA6BD7D11FCF4DD88B06747F4B797784D9 (void);
// 0x000003E8 System.Int32 Obi.BIH::HoarePartition(Obi.IBounded[],System.Int32,System.Int32,System.Single,Obi.BIHNode&,System.Int32)
extern void BIH_HoarePartition_m26E2AC749940517A897AF2963BE5322BB38A3CF9 (void);
// 0x000003E9 System.Single Obi.BIH::DistanceToSurface(Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],Obi.BIHNode&,UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905 (void);
// 0x000003EA System.Single Obi.BIH::DistanceToSurface(Obi.BIHNode[],Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_mAE13B1EFAA791602268DFF2A5ED429691EA76178 (void);
// 0x000003EB System.Single Obi.BIH::DistanceToSurface(Obi.BIHNode[],Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],Obi.BIHNode&,UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05 (void);
// 0x000003EC System.Void Obi.BIH::.ctor()
extern void BIH__ctor_m6D1B7878A01D15CA2622A366286C9308BFB07727 (void);
// 0x000003ED System.Single Obi.BIH::<DistanceToSurface>g__MinSignedDistance|4_0(System.Single,System.Single)
extern void BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_mED2F171342075378B5B140C14A73A72EF8ACC45F (void);
// 0x000003EE System.Void Obi.BIHNode::.ctor(System.Int32,System.Int32)
extern void BIHNode__ctor_mBE805A0C558FDCA2FEDE25366ED721EA3776834A (void);
// 0x000003EF Obi.Aabb Obi.IBounded::GetBounds()
// 0x000003F0 System.Void Obi.CellSpan::.ctor(Obi.VInt4,Obi.VInt4)
extern void CellSpan__ctor_m93CAC71E3E1DC790B927C8C76F72A3784072746B (void);
// 0x000003F1 System.Void Obi.ColliderRigidbody::FromRigidbody(UnityEngine.Rigidbody,System.Boolean)
extern void ColliderRigidbody_FromRigidbody_m4921C838DA8C3C147A91168C9232CB97780260BB (void);
// 0x000003F2 System.Void Obi.ColliderRigidbody::FromRigidbody(UnityEngine.Rigidbody2D,System.Boolean)
extern void ColliderRigidbody_FromRigidbody_m2311F1A25138EA07373D405B7EF2F1EB3859138E (void);
// 0x000003F3 System.Void Obi.CollisionMaterial::FromObiCollisionMaterial(Obi.ObiCollisionMaterial)
extern void CollisionMaterial_FromObiCollisionMaterial_mBF4E29EA56479F76E396B2B5EA9BDEFED6663530 (void);
// 0x000003F4 System.Void Obi.ObiNativeAabbList::.ctor()
extern void ObiNativeAabbList__ctor_mCDE9CAA207D408AC843A08BBE600AB4F0D6B75D6 (void);
// 0x000003F5 System.Void Obi.ObiNativeAabbList::.ctor(System.Int32,System.Int32)
extern void ObiNativeAabbList__ctor_m59E302FAD9AEFD8A3BE4E2C51B322FB0420E1E2F (void);
// 0x000003F6 System.Void Obi.ObiNativeAffineTransformList::.ctor()
extern void ObiNativeAffineTransformList__ctor_mA3B6D7D19DD4E487461C969B9C76DDCAF9564018 (void);
// 0x000003F7 System.Void Obi.ObiNativeAffineTransformList::.ctor(System.Int32,System.Int32)
extern void ObiNativeAffineTransformList__ctor_mCB63BE2A5D0F613E12C647669D05EB6BE52FC83E (void);
// 0x000003F8 System.Void Obi.ObiNativeBIHNodeList::.ctor()
extern void ObiNativeBIHNodeList__ctor_m9615EC2E166FBD0154489436203D9AC793BFB907 (void);
// 0x000003F9 System.Void Obi.ObiNativeBIHNodeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeBIHNodeList__ctor_m6CB7A19C221489E05D62A7996A07E287439F3D73 (void);
// 0x000003FA System.Void Obi.ObiNativeBoneWeightList::.ctor()
extern void ObiNativeBoneWeightList__ctor_m3412BBD683C143994278F1461106F9696B1D6835 (void);
// 0x000003FB System.Void Obi.ObiNativeBoneWeightList::.ctor(System.Int32,System.Int32)
extern void ObiNativeBoneWeightList__ctor_mCDE322908FD4B65971533ABDFB277CDB0DF40B49 (void);
// 0x000003FC System.Void Obi.ObiNativeByteList::.ctor()
extern void ObiNativeByteList__ctor_mE766091F60DF309D51D8C33F4C12AF80D4B9EB42 (void);
// 0x000003FD System.Void Obi.ObiNativeByteList::.ctor(System.Int32,System.Int32)
extern void ObiNativeByteList__ctor_mEA31E3AD637138D0161DB670D7DF7ACDC613B402 (void);
// 0x000003FE System.Void Obi.ObiNativeCellSpanList::.ctor()
extern void ObiNativeCellSpanList__ctor_m7A0292E9784EED2B0011E83FA5A7D22EC9DC9EEC (void);
// 0x000003FF System.Void Obi.ObiNativeCellSpanList::.ctor(System.Int32,System.Int32)
extern void ObiNativeCellSpanList__ctor_m9810884859E402D959A6369EEBD22320DC40D2D6 (void);
// 0x00000400 System.Void Obi.ObiNativeColliderShapeList::.ctor()
extern void ObiNativeColliderShapeList__ctor_m9E21BF5F9EF261AE3C9C92E53F15264713584DC7 (void);
// 0x00000401 System.Void Obi.ObiNativeColliderShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeColliderShapeList__ctor_m79BC575A7059E83119DCE5DFC6D7CE8E308245C3 (void);
// 0x00000402 System.Void Obi.ObiNativeCollisionMaterialList::.ctor()
extern void ObiNativeCollisionMaterialList__ctor_m1C70BE9BA86B92206DB617392F15D7B8BDEC9597 (void);
// 0x00000403 System.Void Obi.ObiNativeCollisionMaterialList::.ctor(System.Int32,System.Int32)
extern void ObiNativeCollisionMaterialList__ctor_m90089B05CB810F2C23D47F0077910F26FCC61888 (void);
// 0x00000404 System.Void Obi.ObiNativeContactShapeList::.ctor()
extern void ObiNativeContactShapeList__ctor_mD865C20C0CFC9AE7557CD251D78C03101CCF2695 (void);
// 0x00000405 System.Void Obi.ObiNativeContactShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeContactShapeList__ctor_mDB12EB79069ADCBD7B0F4377C8C5A1545F646DE8 (void);
// 0x00000406 System.Void Obi.ObiNativeDFNodeList::.ctor()
extern void ObiNativeDFNodeList__ctor_m9C5F3593C316319486AAF621BDD3A6C77A7AF4ED (void);
// 0x00000407 System.Void Obi.ObiNativeDFNodeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeDFNodeList__ctor_mCDC4549E10B99A08FD83058EFD46F45299C71FEC (void);
// 0x00000408 System.Void Obi.ObiNativeDistanceFieldHeaderList::.ctor()
extern void ObiNativeDistanceFieldHeaderList__ctor_m12DE4CCCF75D20BB404B8083EF45503F964599EB (void);
// 0x00000409 System.Void Obi.ObiNativeDistanceFieldHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeDistanceFieldHeaderList__ctor_mF1E073829DBEAD5BC9ECC60BB9AF6463019282EC (void);
// 0x0000040A System.Void Obi.ObiNativeEdgeList::.ctor()
extern void ObiNativeEdgeList__ctor_m376EED5071F0D70AB6595F5C98AC9FB16FCCCF46 (void);
// 0x0000040B System.Void Obi.ObiNativeEdgeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeEdgeList__ctor_m1DFD5ED07389DD12179C9D3494C4E2BF3D62D563 (void);
// 0x0000040C System.Void Obi.ObiNativeEdgeMeshHeaderList::.ctor()
extern void ObiNativeEdgeMeshHeaderList__ctor_m7A49700958AB248E4ABC1CCD1FA83731241FD1DA (void);
// 0x0000040D System.Void Obi.ObiNativeEdgeMeshHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeEdgeMeshHeaderList__ctor_m372AE7E0664D6BE77453AB0C52023FC77C720E8C (void);
// 0x0000040E System.Void Obi.ObiNativeFloatList::.ctor()
extern void ObiNativeFloatList__ctor_mB44D5F56545917B35AD1ECA717DF61D0C12047DC (void);
// 0x0000040F System.Void Obi.ObiNativeFloatList::.ctor(System.Int32,System.Int32)
extern void ObiNativeFloatList__ctor_m109DA72958D0EBB1AEF825F9A43C34F2BD1A42E5 (void);
// 0x00000410 System.Void Obi.ObiNativeHeightFieldHeaderList::.ctor()
extern void ObiNativeHeightFieldHeaderList__ctor_m0ED84840FACD5EF875EE422AD5274E42CE87D779 (void);
// 0x00000411 System.Void Obi.ObiNativeHeightFieldHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeHeightFieldHeaderList__ctor_m5EC50E82D2C0FE9DBB46618885DE4D9A4FB490A1 (void);
// 0x00000412 System.Void Obi.ObiNativeInt4List::.ctor()
extern void ObiNativeInt4List__ctor_mAD172784A90BDA971380CFA6F7BECB381A268EA6 (void);
// 0x00000413 System.Void Obi.ObiNativeInt4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeInt4List__ctor_mF3060DFCCBC09AF993C3C96608B51FA83537EDC8 (void);
// 0x00000414 System.Void Obi.ObiNativeInt4List::.ctor(System.Int32,System.Int32,Obi.VInt4)
extern void ObiNativeInt4List__ctor_m24F99A45DDF792A152AF6A140C910F32C460E40A (void);
// 0x00000415 System.Void Obi.ObiNativeIntList::.ctor()
extern void ObiNativeIntList__ctor_mDF10DEDB2E6CE5F2D27135F263B2DB96A9123F0E (void);
// 0x00000416 System.Void Obi.ObiNativeIntList::.ctor(System.Int32,System.Int32)
extern void ObiNativeIntList__ctor_m25ADBFB84E370EE592FFF05D343541D84462FED6 (void);
// 0x00000417 System.Void Obi.ObiNativeIntPtrList::.ctor()
extern void ObiNativeIntPtrList__ctor_m2188368B2E04FE7C32F8D069405C9F483C27D465 (void);
// 0x00000418 System.Void Obi.ObiNativeIntPtrList::.ctor(System.Int32,System.Int32)
extern void ObiNativeIntPtrList__ctor_mE54F7AA88D3E5B4AD478C37640EDFAD145057FB8 (void);
// 0x00000419 System.Void Obi.ObiNativeList`1::set_count(System.Int32)
// 0x0000041A System.Int32 Obi.ObiNativeList`1::get_count()
// 0x0000041B System.Void Obi.ObiNativeList`1::set_capacity(System.Int32)
// 0x0000041C System.Int32 Obi.ObiNativeList`1::get_capacity()
// 0x0000041D System.Boolean Obi.ObiNativeList`1::get_isCreated()
// 0x0000041E T Obi.ObiNativeList`1::get_Item(System.Int32)
// 0x0000041F System.Void Obi.ObiNativeList`1::set_Item(System.Int32,T)
// 0x00000420 System.Void Obi.ObiNativeList`1::.ctor()
// 0x00000421 System.Void Obi.ObiNativeList`1::.ctor(System.Int32,System.Int32)
// 0x00000422 System.Void Obi.ObiNativeList`1::Finalize()
// 0x00000423 System.Void Obi.ObiNativeList`1::Dispose(System.Boolean)
// 0x00000424 System.Void Obi.ObiNativeList`1::Dispose()
// 0x00000425 System.Void Obi.ObiNativeList`1::OnBeforeSerialize()
// 0x00000426 System.Void Obi.ObiNativeList`1::OnAfterDeserialize()
// 0x00000427 Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1::AsNativeArray()
// 0x00000428 Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1::AsNativeArray(System.Int32)
// 0x00000429 UnityEngine.ComputeBuffer Obi.ObiNativeList`1::AsComputeBuffer()
// 0x0000042A System.Void Obi.ObiNativeList`1::ChangeCapacity(System.Int32)
// 0x0000042B System.Boolean Obi.ObiNativeList`1::Compare(Obi.ObiNativeList`1<T>)
// 0x0000042C System.Void Obi.ObiNativeList`1::CopyFrom(Obi.ObiNativeList`1<T>)
// 0x0000042D System.Void Obi.ObiNativeList`1::CopyFrom(Obi.ObiNativeList`1<T>,System.Int32,System.Int32,System.Int32)
// 0x0000042E System.Void Obi.ObiNativeList`1::CopyReplicate(T,System.Int32,System.Int32)
// 0x0000042F System.Void Obi.ObiNativeList`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000430 System.Void Obi.ObiNativeList`1::Clear()
// 0x00000431 System.Void Obi.ObiNativeList`1::Add(T)
// 0x00000432 System.Void Obi.ObiNativeList`1::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000433 System.Void Obi.ObiNativeList`1::RemoveRange(System.Int32,System.Int32)
// 0x00000434 System.Void Obi.ObiNativeList`1::RemoveAt(System.Int32)
// 0x00000435 System.Boolean Obi.ObiNativeList`1::ResizeUninitialized(System.Int32)
// 0x00000436 System.Boolean Obi.ObiNativeList`1::ResizeInitialized(System.Int32,T)
// 0x00000437 System.Boolean Obi.ObiNativeList`1::EnsureCapacity(System.Int32)
// 0x00000438 System.Void Obi.ObiNativeList`1::WipeToZero()
// 0x00000439 System.String Obi.ObiNativeList`1::ToString()
// 0x0000043A System.Void* Obi.ObiNativeList`1::AddressOfElement(System.Int32)
// 0x0000043B System.IntPtr Obi.ObiNativeList`1::GetIntPtr()
// 0x0000043C System.Void Obi.ObiNativeList`1::Swap(System.Int32,System.Int32)
// 0x0000043D System.Collections.Generic.IEnumerator`1<T> Obi.ObiNativeList`1::GetEnumerator()
// 0x0000043E System.Collections.IEnumerator Obi.ObiNativeList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000043F System.Void Obi.ObiNativeList`1/<GetEnumerator>d__47::.ctor(System.Int32)
// 0x00000440 System.Void Obi.ObiNativeList`1/<GetEnumerator>d__47::System.IDisposable.Dispose()
// 0x00000441 System.Boolean Obi.ObiNativeList`1/<GetEnumerator>d__47::MoveNext()
// 0x00000442 T Obi.ObiNativeList`1/<GetEnumerator>d__47::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000443 System.Void Obi.ObiNativeList`1/<GetEnumerator>d__47::System.Collections.IEnumerator.Reset()
// 0x00000444 System.Object Obi.ObiNativeList`1/<GetEnumerator>d__47::System.Collections.IEnumerator.get_Current()
// 0x00000445 System.Void Obi.ObiNativeMatrix4x4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeMatrix4x4List__ctor_m8315DA2799111EE4624C6AD421E912B9E2C0D775 (void);
// 0x00000446 System.Void Obi.ObiNativeQuaternionList::.ctor()
extern void ObiNativeQuaternionList__ctor_m07E6B8255331075366483E69997B1E8FCC8812EE (void);
// 0x00000447 System.Void Obi.ObiNativeQuaternionList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQuaternionList__ctor_mF8798CFC21A168F77FDE7E4E433E4ED07BB42B0B (void);
// 0x00000448 System.Void Obi.ObiNativeQuaternionList::.ctor(System.Int32,System.Int32,UnityEngine.Quaternion)
extern void ObiNativeQuaternionList__ctor_m2733D11CD8396D239DBC16995D1E9404EA29AADA (void);
// 0x00000449 System.Void Obi.ObiNativeQueryResultList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQueryResultList__ctor_m5B404B4FBEB82A5A119AC92DFC671C161598266D (void);
// 0x0000044A System.Void Obi.ObiNativeQueryShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQueryShapeList__ctor_m1FC70B84E766982F194F6DC2FE87D15DA79493E6 (void);
// 0x0000044B System.Void Obi.ObiNativeRigidbodyList::.ctor()
extern void ObiNativeRigidbodyList__ctor_mE10B19341E895E8DD38465DE2FC0E2EF5FEC1FA9 (void);
// 0x0000044C System.Void Obi.ObiNativeRigidbodyList::.ctor(System.Int32,System.Int32)
extern void ObiNativeRigidbodyList__ctor_m21C628B6ADABDBDFA0D64D291A00D1BBC923B686 (void);
// 0x0000044D System.Void Obi.ObiNativeTriangleList::.ctor()
extern void ObiNativeTriangleList__ctor_m5FDA4E153D9F3E8B6479666E321DDA194CAA2A25 (void);
// 0x0000044E System.Void Obi.ObiNativeTriangleList::.ctor(System.Int32,System.Int32)
extern void ObiNativeTriangleList__ctor_m9FD16BE51A73FCC0ECD34FE1E3E60DE410644782 (void);
// 0x0000044F System.Void Obi.ObiNativeTriangleMeshHeaderList::.ctor()
extern void ObiNativeTriangleMeshHeaderList__ctor_mDAD0784830EED0C0268F2009CEC90E04EB39B72C (void);
// 0x00000450 System.Void Obi.ObiNativeTriangleMeshHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeTriangleMeshHeaderList__ctor_m4F5742A78ACC6F92F44E484750F2710E45D32A8F (void);
// 0x00000451 System.Void Obi.ObiNativeVector2List::.ctor()
extern void ObiNativeVector2List__ctor_mC9341AEC2A2B1610138A7763453BF7CDC2512299 (void);
// 0x00000452 System.Void Obi.ObiNativeVector2List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector2List__ctor_m3326D8FF9523937A0D58CCEB0AA53A7F715AB740 (void);
// 0x00000453 System.Void Obi.ObiNativeVector3List::.ctor()
extern void ObiNativeVector3List__ctor_mC37EA7399E0090469F5014DE5EF277117A8B40E4 (void);
// 0x00000454 System.Void Obi.ObiNativeVector3List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector3List__ctor_m454CD1792A828386A51E51AC80EF30B4EC59DB25 (void);
// 0x00000455 System.Void Obi.ObiNativeVector4List::.ctor()
extern void ObiNativeVector4List__ctor_mC18F1ECE7AE0C5F9E3D080D41011CCE123F29C55 (void);
// 0x00000456 System.Void Obi.ObiNativeVector4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector4List__ctor_m4E5196643444C3F0983F4C1D35EC054085549834 (void);
// 0x00000457 UnityEngine.Vector3 Obi.ObiNativeVector4List::GetVector3(System.Int32)
extern void ObiNativeVector4List_GetVector3_m8EAE7F64879DD9A415D25E05C258ADEBC921A55D (void);
// 0x00000458 System.Void Obi.ObiNativeVector4List::SetVector3(System.Int32,UnityEngine.Vector3)
extern void ObiNativeVector4List_SetVector3_mEA4A6008125CDE1854000FBF64CEA0887E8A7D94 (void);
// 0x00000459 System.Collections.Generic.IEnumerator`1<T> Obi.ObiList`1::GetEnumerator()
// 0x0000045A System.Collections.IEnumerator Obi.ObiList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000045B System.Void Obi.ObiList`1::Add(T)
// 0x0000045C System.Void Obi.ObiList`1::Clear()
// 0x0000045D System.Boolean Obi.ObiList`1::Contains(T)
// 0x0000045E System.Void Obi.ObiList`1::CopyTo(T[],System.Int32)
// 0x0000045F System.Boolean Obi.ObiList`1::Remove(T)
// 0x00000460 System.Int32 Obi.ObiList`1::get_Count()
// 0x00000461 System.Boolean Obi.ObiList`1::get_IsReadOnly()
// 0x00000462 System.Int32 Obi.ObiList`1::IndexOf(T)
// 0x00000463 System.Void Obi.ObiList`1::Insert(System.Int32,T)
// 0x00000464 System.Void Obi.ObiList`1::RemoveAt(System.Int32)
// 0x00000465 T Obi.ObiList`1::get_Item(System.Int32)
// 0x00000466 System.Void Obi.ObiList`1::set_Item(System.Int32,T)
// 0x00000467 T[] Obi.ObiList`1::get_Data()
// 0x00000468 System.Void Obi.ObiList`1::SetCount(System.Int32)
// 0x00000469 System.Void Obi.ObiList`1::EnsureCapacity(System.Int32)
// 0x0000046A System.Void Obi.ObiList`1::.ctor()
// 0x0000046B System.Void Obi.ObiList`1/<GetEnumerator>d__2::.ctor(System.Int32)
// 0x0000046C System.Void Obi.ObiList`1/<GetEnumerator>d__2::System.IDisposable.Dispose()
// 0x0000046D System.Boolean Obi.ObiList`1/<GetEnumerator>d__2::MoveNext()
// 0x0000046E T Obi.ObiList`1/<GetEnumerator>d__2::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000046F System.Void Obi.ObiList`1/<GetEnumerator>d__2::System.Collections.IEnumerator.Reset()
// 0x00000470 System.Object Obi.ObiList`1/<GetEnumerator>d__2::System.Collections.IEnumerator.get_Current()
// 0x00000471 System.Void Obi.ParticlePair::.ctor(System.Int32,System.Int32)
extern void ParticlePair__ctor_mDDAD78C599D557D2A63715207CBC71A5A93581CA (void);
// 0x00000472 System.Int32 Obi.ParticlePair::get_Item(System.Int32)
extern void ParticlePair_get_Item_m96F244D843B43EE4A0E48128BDAE3837B0574AF9 (void);
// 0x00000473 System.Void Obi.ParticlePair::set_Item(System.Int32,System.Int32)
extern void ParticlePair_set_Item_m39EFBD38D2AB94AA009AAF1C18B7EA40077EEDB1 (void);
// 0x00000474 System.Void Obi.QueryShape::.ctor(Obi.QueryShape/QueryType,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Int32)
extern void QueryShape__ctor_m10B944E68ED9FE30CB3D7EE090068A33F9EDB453 (void);
// 0x00000475 System.Int32 Obi.SimplexCounts::get_simplexCount()
extern void SimplexCounts_get_simplexCount_m99631648B641C2ECF96D0C8E5DB265D40F522A86 (void);
// 0x00000476 System.Void Obi.SimplexCounts::.ctor(System.Int32,System.Int32,System.Int32)
extern void SimplexCounts__ctor_m68F3A4CF7A694C3300E7BA4227020A89A724F066 (void);
// 0x00000477 System.Int32 Obi.SimplexCounts::GetSimplexStartAndSize(System.Int32,System.Int32&)
extern void SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95 (void);
// 0x00000478 System.Void Obi.VInt4::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void VInt4__ctor_m6FDFB28D4337C36A4A49681BA2E0B85D0B36AFCB (void);
// 0x00000479 System.Void Obi.VInt4::.ctor(System.Int32)
extern void VInt4__ctor_mDF85C8595BC6DCEAAF3A5A4A04704DEFDB238C6C (void);
// 0x0000047A UnityEngine.Vector3Int Obi.MeshVoxelizer::get_Origin()
extern void MeshVoxelizer_get_Origin_m98D23A3FAE59358DCED36A586C0AB6B4F3E77B4D (void);
// 0x0000047B System.Int32 Obi.MeshVoxelizer::get_voxelCount()
extern void MeshVoxelizer_get_voxelCount_m1590512CB3A933F6FD26076643E8FBBF72CD4565 (void);
// 0x0000047C System.Void Obi.MeshVoxelizer::.ctor(UnityEngine.Mesh,System.Single)
extern void MeshVoxelizer__ctor_m2E9885D259222CEE4CB38497584FE8F3EBB0E2B4 (void);
// 0x0000047D Obi.MeshVoxelizer/Voxel Obi.MeshVoxelizer::get_Item(System.Int32,System.Int32,System.Int32)
extern void MeshVoxelizer_get_Item_mC5F92567FD7C76447DF970069641190AE24C1356 (void);
// 0x0000047E System.Void Obi.MeshVoxelizer::set_Item(System.Int32,System.Int32,System.Int32,Obi.MeshVoxelizer/Voxel)
extern void MeshVoxelizer_set_Item_m6E59067E37D364C147DEC0335408F67C39286DBD (void);
// 0x0000047F System.Single Obi.MeshVoxelizer::GetDistanceToNeighbor(System.Int32)
extern void MeshVoxelizer_GetDistanceToNeighbor_mAC3238234874D704EEDE7B39EA4E2C557043FA8F (void);
// 0x00000480 System.Int32 Obi.MeshVoxelizer::GetVoxelIndex(System.Int32,System.Int32,System.Int32)
extern void MeshVoxelizer_GetVoxelIndex_m03964EDAA758946CBAFB630ECD6AC4DAF9B03921 (void);
// 0x00000481 UnityEngine.Vector3 Obi.MeshVoxelizer::GetVoxelCenter(UnityEngine.Vector3Int&)
extern void MeshVoxelizer_GetVoxelCenter_m3E452556E6139C67B397A378FDCBD9BA4F425089 (void);
// 0x00000482 UnityEngine.Bounds Obi.MeshVoxelizer::GetTriangleBounds(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06 (void);
// 0x00000483 System.Collections.Generic.List`1<System.Int32> Obi.MeshVoxelizer::GetTrianglesOverlappingVoxel(System.Int32)
extern void MeshVoxelizer_GetTrianglesOverlappingVoxel_mBEA8964FBC06F1AC96902CAB7A18AD815C4FBAEE (void);
// 0x00000484 UnityEngine.Vector3Int Obi.MeshVoxelizer::GetPointVoxel(UnityEngine.Vector3&)
extern void MeshVoxelizer_GetPointVoxel_mB53686BE0482AC0FE527505F8EC019E21CFAB6B1 (void);
// 0x00000485 System.Boolean Obi.MeshVoxelizer::VoxelExists(UnityEngine.Vector3Int&)
extern void MeshVoxelizer_VoxelExists_mBD68D5E79FB132F4304AAE5283DF58F65CDA0A4B (void);
// 0x00000486 System.Boolean Obi.MeshVoxelizer::VoxelExists(System.Int32,System.Int32,System.Int32)
extern void MeshVoxelizer_VoxelExists_m6075ED6BD1CD96FBC57C1935A45231C09ABCFA70 (void);
// 0x00000487 System.Void Obi.MeshVoxelizer::AppendOverlappingVoxels(UnityEngine.Bounds&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Int32)
extern void MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7 (void);
// 0x00000488 System.Collections.IEnumerator Obi.MeshVoxelizer::Voxelize(UnityEngine.Matrix4x4,System.Boolean)
extern void MeshVoxelizer_Voxelize_mFD415E5ED30A0BF0CBC964412DBBB37930CFA97A (void);
// 0x00000489 System.Void Obi.MeshVoxelizer::BoundaryThinning()
extern void MeshVoxelizer_BoundaryThinning_m4E5359902ABE3CD1C2541C9FCEADE519E69482D5 (void);
// 0x0000048A System.Collections.IEnumerator Obi.MeshVoxelizer::FloodFill()
extern void MeshVoxelizer_FloodFill_m584C8081EF5F8BC2660348F75BF0C006FBD0EC51 (void);
// 0x0000048B System.Boolean Obi.MeshVoxelizer::IsIntersecting(UnityEngine.Bounds&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MeshVoxelizer_IsIntersecting_m1F9C81AE3D544C1F8AA465823F7DDA7A32C7FBEC (void);
// 0x0000048C System.Boolean Obi.MeshVoxelizer::TriangleAabbSATTest(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10 (void);
// 0x0000048D System.Void Obi.MeshVoxelizer::.cctor()
extern void MeshVoxelizer__cctor_m221669B76FE23A351F3F850B1B8B6BA2FD840D15 (void);
// 0x0000048E System.Void Obi.MeshVoxelizer/<Voxelize>d__29::.ctor(System.Int32)
extern void U3CVoxelizeU3Ed__29__ctor_m8645BD0F9E12E3B4AE36A3A9D757A0AA317666E0 (void);
// 0x0000048F System.Void Obi.MeshVoxelizer/<Voxelize>d__29::System.IDisposable.Dispose()
extern void U3CVoxelizeU3Ed__29_System_IDisposable_Dispose_m68F6D58872E569B35178F3F948D25088FEE404DD (void);
// 0x00000490 System.Boolean Obi.MeshVoxelizer/<Voxelize>d__29::MoveNext()
extern void U3CVoxelizeU3Ed__29_MoveNext_m6AC2C7AA56B9E60005E531B6AFBC225E89351F62 (void);
// 0x00000491 System.Object Obi.MeshVoxelizer/<Voxelize>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVoxelizeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCADBFC708A9E44E40767E097632FC238158C8036 (void);
// 0x00000492 System.Void Obi.MeshVoxelizer/<Voxelize>d__29::System.Collections.IEnumerator.Reset()
extern void U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_Reset_mF260247FB427D7B3C0DAAFFD3085943EBA202B9C (void);
// 0x00000493 System.Object Obi.MeshVoxelizer/<Voxelize>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_get_Current_m4ED17979436917051AE4058242D7AF43D4A9F2C7 (void);
// 0x00000494 System.Void Obi.MeshVoxelizer/<FloodFill>d__31::.ctor(System.Int32)
extern void U3CFloodFillU3Ed__31__ctor_m70BCBE21D4D47E1482D966F7C9D35F7510603B97 (void);
// 0x00000495 System.Void Obi.MeshVoxelizer/<FloodFill>d__31::System.IDisposable.Dispose()
extern void U3CFloodFillU3Ed__31_System_IDisposable_Dispose_m874A33578734166DA64BFC7AE8771A031A25F2C2 (void);
// 0x00000496 System.Boolean Obi.MeshVoxelizer/<FloodFill>d__31::MoveNext()
extern void U3CFloodFillU3Ed__31_MoveNext_m35FC429F6648DE25CB9A4F53A3547E84ECAEEB8B (void);
// 0x00000497 System.Object Obi.MeshVoxelizer/<FloodFill>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFloodFillU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60C01825AF1A7118B99F44611D5A9FFA941E6189 (void);
// 0x00000498 System.Void Obi.MeshVoxelizer/<FloodFill>d__31::System.Collections.IEnumerator.Reset()
extern void U3CFloodFillU3Ed__31_System_Collections_IEnumerator_Reset_mC4010923219A1BAD954EEAC4B73CDD945265C9A3 (void);
// 0x00000499 System.Object Obi.MeshVoxelizer/<FloodFill>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CFloodFillU3Ed__31_System_Collections_IEnumerator_get_Current_m7964014EF80DB8139871A160F4672E40BED0788E (void);
// 0x0000049A System.Void Obi.PriorityQueue`1::.ctor()
// 0x0000049B System.Void Obi.PriorityQueue`1::Enqueue(T)
// 0x0000049C T Obi.PriorityQueue`1::Dequeue()
// 0x0000049D T Obi.PriorityQueue`1::Peek()
// 0x0000049E System.Collections.Generic.IEnumerable`1<T> Obi.PriorityQueue`1::GetEnumerator()
// 0x0000049F System.Void Obi.PriorityQueue`1::Clear()
// 0x000004A0 System.Int32 Obi.PriorityQueue`1::Count()
// 0x000004A1 System.String Obi.PriorityQueue`1::ToString()
// 0x000004A2 System.Boolean Obi.PriorityQueue`1::IsConsistent()
// 0x000004A3 System.Void Obi.PriorityQueue`1/<GetEnumerator>d__5::.ctor(System.Int32)
// 0x000004A4 System.Void Obi.PriorityQueue`1/<GetEnumerator>d__5::System.IDisposable.Dispose()
// 0x000004A5 System.Boolean Obi.PriorityQueue`1/<GetEnumerator>d__5::MoveNext()
// 0x000004A6 T Obi.PriorityQueue`1/<GetEnumerator>d__5::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000004A7 System.Void Obi.PriorityQueue`1/<GetEnumerator>d__5::System.Collections.IEnumerator.Reset()
// 0x000004A8 System.Object Obi.PriorityQueue`1/<GetEnumerator>d__5::System.Collections.IEnumerator.get_Current()
// 0x000004A9 System.Collections.Generic.IEnumerator`1<T> Obi.PriorityQueue`1/<GetEnumerator>d__5::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000004AA System.Collections.IEnumerator Obi.PriorityQueue`1/<GetEnumerator>d__5::System.Collections.IEnumerable.GetEnumerator()
// 0x000004AB System.Void Obi.VoxelDistanceField::.ctor(Obi.MeshVoxelizer)
extern void VoxelDistanceField__ctor_mF001EDBFAD1E407A198F85AE578DC94C131DC940 (void);
// 0x000004AC System.Single Obi.VoxelDistanceField::SampleUnfiltered(System.Int32,System.Int32,System.Int32)
extern void VoxelDistanceField_SampleUnfiltered_m1DCFD20D36F447D804A7DBE566E82FD4E6A134AA (void);
// 0x000004AD UnityEngine.Vector4 Obi.VoxelDistanceField::SampleFiltered(System.Single,System.Single,System.Single)
extern void VoxelDistanceField_SampleFiltered_mF03122890E95B4323F7318B9C27A8C65DA3F463E (void);
// 0x000004AE System.Collections.IEnumerator Obi.VoxelDistanceField::JumpFlood()
extern void VoxelDistanceField_JumpFlood_m8A165DB8B897C98F4DD564943140A9C71408E1F6 (void);
// 0x000004AF System.Void Obi.VoxelDistanceField::JumpFloodPass(System.Int32,UnityEngine.Vector3Int[0...,0...,0...],UnityEngine.Vector3Int[0...,0...,0...])
extern void VoxelDistanceField_JumpFloodPass_m0CAE942EA7B7FB237160C57ABAC04D2F77198AC3 (void);
// 0x000004B0 System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::.ctor(System.Int32)
extern void U3CJumpFloodU3Ed__5__ctor_m9A42F34DE8C7C452C8A53F17D6D572BC2453C654 (void);
// 0x000004B1 System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::System.IDisposable.Dispose()
extern void U3CJumpFloodU3Ed__5_System_IDisposable_Dispose_m0A919D1AE0822BB1475E5FE78DA4060A236911E6 (void);
// 0x000004B2 System.Boolean Obi.VoxelDistanceField/<JumpFlood>d__5::MoveNext()
extern void U3CJumpFloodU3Ed__5_MoveNext_mCF3E269446E8A96E7F313896B714A111CBBD3FA3 (void);
// 0x000004B3 System.Object Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CJumpFloodU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF414A3DC08D65DC419082B4635288E1F7E02FC0 (void);
// 0x000004B4 System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.IEnumerator.Reset()
extern void U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22 (void);
// 0x000004B5 System.Object Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_get_Current_m0850F7B63FF4B6EB05FB8A60525AB3BC48509D18 (void);
// 0x000004B6 System.Void Obi.VoxelPathFinder::.ctor(Obi.MeshVoxelizer)
extern void VoxelPathFinder__ctor_mEB3020C477C2C93B2F00C9CFC38CFE47068AE443 (void);
// 0x000004B7 Obi.VoxelPathFinder/TargetVoxel Obi.VoxelPathFinder::AStar(UnityEngine.Vector3Int&,System.Func`2<Obi.VoxelPathFinder/TargetVoxel,System.Boolean>,System.Func`2<UnityEngine.Vector3Int,System.Single>)
extern void VoxelPathFinder_AStar_mA838A6AEC2878DE3B68FB035FCB31C5ADE9958EC (void);
// 0x000004B8 Obi.VoxelPathFinder/TargetVoxel Obi.VoxelPathFinder::FindClosestNonEmptyVoxel(UnityEngine.Vector3Int&)
extern void VoxelPathFinder_FindClosestNonEmptyVoxel_m108974C29C2DE5DCB4E8092382271E90E1306714 (void);
// 0x000004B9 Obi.VoxelPathFinder/TargetVoxel Obi.VoxelPathFinder::FindPath(UnityEngine.Vector3Int&,UnityEngine.Vector3Int)
extern void VoxelPathFinder_FindPath_m12E0CEA61718E42317477C9B7A3825B9EAA590E5 (void);
// 0x000004BA System.Boolean Obi.VoxelPathFinder::<FindClosestNonEmptyVoxel>b__6_0(Obi.VoxelPathFinder/TargetVoxel)
extern void VoxelPathFinder_U3CFindClosestNonEmptyVoxelU3Eb__6_0_mBBEC0743F2CB9A04F014A64C652D3572607A4FDC (void);
// 0x000004BB System.Single Obi.VoxelPathFinder/TargetVoxel::get_cost()
extern void TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F (void);
// 0x000004BC System.Void Obi.VoxelPathFinder/TargetVoxel::.ctor(UnityEngine.Vector3Int,System.Single,System.Single)
extern void TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF (void);
// 0x000004BD System.Boolean Obi.VoxelPathFinder/TargetVoxel::Equals(Obi.VoxelPathFinder/TargetVoxel)
extern void TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681 (void);
// 0x000004BE System.Int32 Obi.VoxelPathFinder/TargetVoxel::CompareTo(Obi.VoxelPathFinder/TargetVoxel)
extern void TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8 (void);
// 0x000004BF System.Void Obi.VoxelPathFinder/<>c::.cctor()
extern void U3CU3Ec__cctor_m2E9966CB04100D59E489C39A64FAC67F8FD97311 (void);
// 0x000004C0 System.Void Obi.VoxelPathFinder/<>c::.ctor()
extern void U3CU3Ec__ctor_m7A494B57D1990F9D4FD4EF63281425ABEC97A888 (void);
// 0x000004C1 System.Single Obi.VoxelPathFinder/<>c::<FindClosestNonEmptyVoxel>b__6_1(UnityEngine.Vector3Int)
extern void U3CU3Ec_U3CFindClosestNonEmptyVoxelU3Eb__6_1_mF780311AA35327276D353A5C472EBABD63FB6D10 (void);
// 0x000004C2 System.Void Obi.VoxelPathFinder/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mCCDBC132E254A9EA49862CDDDB93E94F182AF58C (void);
// 0x000004C3 System.Boolean Obi.VoxelPathFinder/<>c__DisplayClass7_0::<FindPath>b__0(Obi.VoxelPathFinder/TargetVoxel)
extern void U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__0_m0CBCD40678980CB309FD296C8DCA140B2368C158 (void);
// 0x000004C4 System.Single Obi.VoxelPathFinder/<>c__DisplayClass7_0::<FindPath>b__1(UnityEngine.Vector3Int)
extern void U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__1_m93C19DF998EC9A85989D9980D3C89F8EB7724361 (void);
// 0x000004C5 System.Void Obi.ObiDistanceFieldRenderer::Awake()
extern void ObiDistanceFieldRenderer_Awake_m02202AAABFC3B738E4965E9B2B897A228E6AC210 (void);
// 0x000004C6 System.Void Obi.ObiDistanceFieldRenderer::OnEnable()
extern void ObiDistanceFieldRenderer_OnEnable_m5A75F9F5EE526E06FF0D410741740FCB6B6478E1 (void);
// 0x000004C7 System.Void Obi.ObiDistanceFieldRenderer::OnDisable()
extern void ObiDistanceFieldRenderer_OnDisable_m7C13494A2B29D5E6827B2757573C7370FC80A394 (void);
// 0x000004C8 System.Void Obi.ObiDistanceFieldRenderer::Cleanup()
extern void ObiDistanceFieldRenderer_Cleanup_mFE94B31A3E0EED155DC9650B112587501A88D532 (void);
// 0x000004C9 System.Void Obi.ObiDistanceFieldRenderer::ResizeTexture()
extern void ObiDistanceFieldRenderer_ResizeTexture_mE50107B1ECBAB2706311F61EB8EDFAA5A47FB02C (void);
// 0x000004CA System.Void Obi.ObiDistanceFieldRenderer::CreatePlaneMesh(Obi.ObiDistanceField)
extern void ObiDistanceFieldRenderer_CreatePlaneMesh_m7DABB1814E6572C53BCBCC12B382369FA534AAA4 (void);
// 0x000004CB System.Void Obi.ObiDistanceFieldRenderer::RefreshCutawayTexture(Obi.ObiDistanceField)
extern void ObiDistanceFieldRenderer_RefreshCutawayTexture_mFE87BC7C9D4D36CAE8BA3A2BB5CCFE9195422F93 (void);
// 0x000004CC System.Void Obi.ObiDistanceFieldRenderer::DrawCutawayPlane(Obi.ObiDistanceField,UnityEngine.Matrix4x4)
extern void ObiDistanceFieldRenderer_DrawCutawayPlane_m881EB8355EA4D1D21C65297319180645AD0BBB3A (void);
// 0x000004CD System.Void Obi.ObiDistanceFieldRenderer::OnDrawGizmos()
extern void ObiDistanceFieldRenderer_OnDrawGizmos_m216C9ED791D6583CFB5AC31CCF96FC999FEB065B (void);
// 0x000004CE System.Void Obi.ObiDistanceFieldRenderer::.ctor()
extern void ObiDistanceFieldRenderer__ctor_m5BDF67924CA8661E32DCAE76825AECE42C4E08AA (void);
// 0x000004CF System.Void Obi.ObiInstancedParticleRenderer::OnEnable()
extern void ObiInstancedParticleRenderer_OnEnable_mE355745DF976D2E5FB662C641FFF1207CFF849A8 (void);
// 0x000004D0 System.Void Obi.ObiInstancedParticleRenderer::OnDisable()
extern void ObiInstancedParticleRenderer_OnDisable_mB6727244E6FCBAA7A72470799C34118F47F54C26 (void);
// 0x000004D1 System.Void Obi.ObiInstancedParticleRenderer::DrawParticles(Obi.ObiActor)
extern void ObiInstancedParticleRenderer_DrawParticles_m006744E372DC552B4BBE90BDA10C2936E58A7CEB (void);
// 0x000004D2 System.Void Obi.ObiInstancedParticleRenderer::.ctor()
extern void ObiInstancedParticleRenderer__ctor_m36160262281CB03A2EF5F4F65B1599650E20D198 (void);
// 0x000004D3 System.Void Obi.ObiInstancedParticleRenderer::.cctor()
extern void ObiInstancedParticleRenderer__cctor_m8F094F8AD40C59C077031DB8223AF95B8123B2B7 (void);
// 0x000004D4 System.Collections.Generic.IEnumerable`1<UnityEngine.Mesh> Obi.ObiParticleRenderer::get_ParticleMeshes()
extern void ObiParticleRenderer_get_ParticleMeshes_m19B21A66F62AF9565B5610A7E406A9F4D9AB2624 (void);
// 0x000004D5 UnityEngine.Material Obi.ObiParticleRenderer::get_ParticleMaterial()
extern void ObiParticleRenderer_get_ParticleMaterial_mACE9EAEA565D0D3F7A7BF6C53B8C04111AAFB530 (void);
// 0x000004D6 System.Void Obi.ObiParticleRenderer::OnEnable()
extern void ObiParticleRenderer_OnEnable_m5936C65AFFDE90197548673B64DCDB559B634084 (void);
// 0x000004D7 System.Void Obi.ObiParticleRenderer::OnDisable()
extern void ObiParticleRenderer_OnDisable_mAA99859EE95AC59E3CCEBDAEE3C9F3D1EBE9F2A3 (void);
// 0x000004D8 System.Void Obi.ObiParticleRenderer::CreateMaterialIfNeeded()
extern void ObiParticleRenderer_CreateMaterialIfNeeded_m3F402725EA811E7191F495C02BC1D42C9D3508E2 (void);
// 0x000004D9 System.Void Obi.ObiParticleRenderer::DrawParticles(Obi.ObiActor)
extern void ObiParticleRenderer_DrawParticles_m968118939B2050BAB32AF92EFCB004552F31FCAE (void);
// 0x000004DA System.Void Obi.ObiParticleRenderer::DrawParticles()
extern void ObiParticleRenderer_DrawParticles_m10909ABED462F26F971463C7A569BC19C8115FD8 (void);
// 0x000004DB System.Void Obi.ObiParticleRenderer::.ctor()
extern void ObiParticleRenderer__ctor_mFC28AF85E8C3264440883850C411477777E11A29 (void);
// 0x000004DC System.Void Obi.ObiParticleRenderer::.cctor()
extern void ObiParticleRenderer__cctor_m25FE6E62F8C43957CDFA579FE81399B4270C8DDC (void);
// 0x000004DD System.Collections.Generic.IEnumerable`1<UnityEngine.Mesh> Obi.ParticleImpostorRendering::get_Meshes()
extern void ParticleImpostorRendering_get_Meshes_m755745FEE84971387953757DE9D15D9D32204C2F (void);
// 0x000004DE System.Void Obi.ParticleImpostorRendering::Apply(UnityEngine.Mesh)
extern void ParticleImpostorRendering_Apply_mC4B8B70A312A115D0D3264BA0F4D6CB8920AD9D5 (void);
// 0x000004DF System.Void Obi.ParticleImpostorRendering::ClearMeshes()
extern void ParticleImpostorRendering_ClearMeshes_m425FD6121111FFDD2834713DDE69988571B30BD8 (void);
// 0x000004E0 System.Void Obi.ParticleImpostorRendering::UpdateMeshes(Obi.IObiParticleCollection,System.Boolean[],UnityEngine.Color[])
extern void ParticleImpostorRendering_UpdateMeshes_m352C0F940E2857058D6A6B447A03AB70258E8A60 (void);
// 0x000004E1 System.Void Obi.ParticleImpostorRendering::.ctor()
extern void ParticleImpostorRendering__ctor_m83C3D5BED9E2463E4E916753E01D78C90E7C9BBE (void);
// 0x000004E2 System.Void Obi.ParticleImpostorRendering::.cctor()
extern void ParticleImpostorRendering__cctor_m096389C9C5C4FB998DFF6B43A943B8F29D300B72 (void);
// 0x000004E3 System.Void Obi.ShadowmapExposer::Awake()
extern void ShadowmapExposer_Awake_mA4E68783C4BCFD7B7FCCF2FB2047921F739DA519 (void);
// 0x000004E4 System.Void Obi.ShadowmapExposer::OnEnable()
extern void ShadowmapExposer_OnEnable_mA68155C92E4F83954076D9000038D58A265100F5 (void);
// 0x000004E5 System.Void Obi.ShadowmapExposer::OnDisable()
extern void ShadowmapExposer_OnDisable_m7CC85B1D1729AA03CDAC1816F9A84E3350254554 (void);
// 0x000004E6 System.Void Obi.ShadowmapExposer::Cleanup()
extern void ShadowmapExposer_Cleanup_m32D90F4E8CAC5C2EB13BFAD141A056F8D3B143E5 (void);
// 0x000004E7 System.Void Obi.ShadowmapExposer::SetupFluidShadowsCommandBuffer()
extern void ShadowmapExposer_SetupFluidShadowsCommandBuffer_mE799C5800875D6B7CA60B3775768C9F07B2288B4 (void);
// 0x000004E8 System.Void Obi.ShadowmapExposer::Update()
extern void ShadowmapExposer_Update_mBFA72090D0E7D36367BCA31B001E2965D1B264E2 (void);
// 0x000004E9 System.Void Obi.ShadowmapExposer::.ctor()
extern void ShadowmapExposer__ctor_m10E8C4FAA46FCB5CC04085336279E049BC51FAFA (void);
// 0x000004EA System.Void Obi.ObiSolver::add_OnCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_add_OnCollision_mEB324EABAE3F36C43F055950F52827839851DAF5 (void);
// 0x000004EB System.Void Obi.ObiSolver::remove_OnCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_remove_OnCollision_mA07AA8AA273F50BC4DDAFD0523A7CA23C628D674 (void);
// 0x000004EC System.Void Obi.ObiSolver::add_OnParticleCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_add_OnParticleCollision_m73A739A0544F098FE805CEF2C6ED3E9528060A52 (void);
// 0x000004ED System.Void Obi.ObiSolver::remove_OnParticleCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_remove_OnParticleCollision_m153C65501BB104D19DFB0BC50CE655A20BB725B7 (void);
// 0x000004EE System.Void Obi.ObiSolver::add_OnUpdateParameters(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnUpdateParameters_mDCE3C3436FCB6E239FC3C229799E1CA65048798B (void);
// 0x000004EF System.Void Obi.ObiSolver::remove_OnUpdateParameters(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnUpdateParameters_m1D7467B121B4968CCB10AE69A8FCE3F601A622E1 (void);
// 0x000004F0 System.Void Obi.ObiSolver::add_OnPrepareFrame(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnPrepareFrame_mF8245DFAA6E404FFD6135985C1B90BAD5A827B69 (void);
// 0x000004F1 System.Void Obi.ObiSolver::remove_OnPrepareFrame(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnPrepareFrame_mFF9D302946D3647FF7267652E21CCDA784AC68E6 (void);
// 0x000004F2 System.Void Obi.ObiSolver::add_OnPrepareStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_add_OnPrepareStep_m22DB6F7A692B42DCB0AF33EDDFAF38062A4AB42E (void);
// 0x000004F3 System.Void Obi.ObiSolver::remove_OnPrepareStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_remove_OnPrepareStep_m5D2D4E8CACADB7938868BE6F29546BA497A05250 (void);
// 0x000004F4 System.Void Obi.ObiSolver::add_OnBeginStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_add_OnBeginStep_mE11B9F3927E6908AC1C8CA09888FAC4145B60F72 (void);
// 0x000004F5 System.Void Obi.ObiSolver::remove_OnBeginStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_remove_OnBeginStep_m828F85A9556BCF73D5434B9A689918DEF5F5D39D (void);
// 0x000004F6 System.Void Obi.ObiSolver::add_OnSubstep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_add_OnSubstep_mF4D4FE1ED9E867E6F111B8B082E377D950607C6E (void);
// 0x000004F7 System.Void Obi.ObiSolver::remove_OnSubstep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_remove_OnSubstep_m60FD7FE897C29FEF9EE0EDC9303C62CFF70D2B33 (void);
// 0x000004F8 System.Void Obi.ObiSolver::add_OnEndStep(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnEndStep_m85BFAD019C29970E500BB5079DA70527A4EB324D (void);
// 0x000004F9 System.Void Obi.ObiSolver::remove_OnEndStep(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnEndStep_m53846A7D3E4D2113D49EAA4EE56FAE9752CB6CA3 (void);
// 0x000004FA System.Void Obi.ObiSolver::add_OnInterpolate(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnInterpolate_mBADEB5D974C090F97884D9FF885C017D0C36CF7D (void);
// 0x000004FB System.Void Obi.ObiSolver::remove_OnInterpolate(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnInterpolate_m5F9AAE96B45E17989172907EECA1966282A87626 (void);
// 0x000004FC Obi.ISolverImpl Obi.ObiSolver::get_implementation()
extern void ObiSolver_get_implementation_m9F3A6A2A2D6921B6A59A82F935039FC29F24E422 (void);
// 0x000004FD System.Boolean Obi.ObiSolver::get_initialized()
extern void ObiSolver_get_initialized_m245E2320CEE2189A248BB0DE795CD9FE80839C78 (void);
// 0x000004FE Obi.IObiBackend Obi.ObiSolver::get_simulationBackend()
extern void ObiSolver_get_simulationBackend_m67AEE52B9C07C78C1231FE5074F7125E47A8109A (void);
// 0x000004FF System.Void Obi.ObiSolver::set_backendType(Obi.ObiSolver/BackendType)
extern void ObiSolver_set_backendType_mE714E4CD3D72BE093E2ECBB2B418D7095A3B11B4 (void);
// 0x00000500 Obi.ObiSolver/BackendType Obi.ObiSolver::get_backendType()
extern void ObiSolver_get_backendType_m49EDD493C5D45A0D0728C26F95F1E955FEFB4104 (void);
// 0x00000501 Obi.SimplexCounts Obi.ObiSolver::get_simplexCounts()
extern void ObiSolver_get_simplexCounts_m78AF46D65457FB56118D4ECD884FEAD71D3E28C4 (void);
// 0x00000502 UnityEngine.Bounds Obi.ObiSolver::get_Bounds()
extern void ObiSolver_get_Bounds_m4C8914413BBBC0968BA6ADCF3E7149AA5F7FC7D3 (void);
// 0x00000503 System.Boolean Obi.ObiSolver::get_IsVisible()
extern void ObiSolver_get_IsVisible_mA12ECA2DFE503F721181D2A8A5F5F9065BABAF0B (void);
// 0x00000504 System.Single Obi.ObiSolver::get_maxScale()
extern void ObiSolver_get_maxScale_m2118E5D74E45C017D6D91E33790768FC10FE9B18 (void);
// 0x00000505 System.Int32 Obi.ObiSolver::get_allocParticleCount()
extern void ObiSolver_get_allocParticleCount_m98C18E7A785C31F32283F6A52AB1372313E3E16F (void);
// 0x00000506 System.Int32 Obi.ObiSolver::get_contactCount()
extern void ObiSolver_get_contactCount_mAE6F8DD8070340148DCC11C0F729F1B38C4AF654 (void);
// 0x00000507 System.Int32 Obi.ObiSolver::get_particleContactCount()
extern void ObiSolver_get_particleContactCount_mFDF3ADAB6B54D7FECF6F1943E4AE93C0AE10F18F (void);
// 0x00000508 Obi.ObiSolver/ParticleInActor[] Obi.ObiSolver::get_particleToActor()
extern void ObiSolver_get_particleToActor_mFCEA216986C39AD5264352D73B10017FE5E7F3D3 (void);
// 0x00000509 Obi.ObiNativeVector4List Obi.ObiSolver::get_rigidbodyLinearDeltas()
extern void ObiSolver_get_rigidbodyLinearDeltas_mEB6D6F68DEAD8616EA48FA36AEA9B37520CA9367 (void);
// 0x0000050A Obi.ObiNativeVector4List Obi.ObiSolver::get_rigidbodyAngularDeltas()
extern void ObiSolver_get_rigidbodyAngularDeltas_m05353966D848B59BC9586B0C19CAE476834502C9 (void);
// 0x0000050B UnityEngine.Color[] Obi.ObiSolver::get_colors()
extern void ObiSolver_get_colors_m3263CBAEB79EE1F583593969A2EAD32549CB875D (void);
// 0x0000050C Obi.ObiNativeInt4List Obi.ObiSolver::get_cellCoords()
extern void ObiSolver_get_cellCoords_m6BB3F63111C716E45AAC8FC0BE4AB0CD17331B28 (void);
// 0x0000050D Obi.ObiNativeVector4List Obi.ObiSolver::get_positions()
extern void ObiSolver_get_positions_mB7EA5209FDB8289380DBA07A2F28E640D485F557 (void);
// 0x0000050E Obi.ObiNativeVector4List Obi.ObiSolver::get_restPositions()
extern void ObiSolver_get_restPositions_m1BCA73DCC44BA1B9181838370F0E546B4D66CFC4 (void);
// 0x0000050F Obi.ObiNativeVector4List Obi.ObiSolver::get_prevPositions()
extern void ObiSolver_get_prevPositions_mA99888F076C4FB374B14DEE1178C625A3F8F9B00 (void);
// 0x00000510 Obi.ObiNativeVector4List Obi.ObiSolver::get_startPositions()
extern void ObiSolver_get_startPositions_m3B9144D015BDB7F0F676F065AAC9A22F0DA9CC27 (void);
// 0x00000511 Obi.ObiNativeVector4List Obi.ObiSolver::get_renderablePositions()
extern void ObiSolver_get_renderablePositions_m56EE654A3E7ECEF99EE3F1481E759B01DAB697BE (void);
// 0x00000512 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_orientations()
extern void ObiSolver_get_orientations_mAF1A79C344CC44789900361936635BBD508D2973 (void);
// 0x00000513 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_restOrientations()
extern void ObiSolver_get_restOrientations_m5A079EC8140D809FB02223ECCA6986917B333AE4 (void);
// 0x00000514 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_prevOrientations()
extern void ObiSolver_get_prevOrientations_mBB52AEB5D484B0A4BAE23213D65585BE498B5F68 (void);
// 0x00000515 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_startOrientations()
extern void ObiSolver_get_startOrientations_mED3EA83E62A458AE8F95A89CEC15ECA81FDF28D1 (void);
// 0x00000516 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_renderableOrientations()
extern void ObiSolver_get_renderableOrientations_mB71AEBAF05CD2CA3FED8936554686F5A0414C06A (void);
// 0x00000517 Obi.ObiNativeVector4List Obi.ObiSolver::get_velocities()
extern void ObiSolver_get_velocities_m73051C86B492795133F138E42FF08698222FCCD6 (void);
// 0x00000518 Obi.ObiNativeVector4List Obi.ObiSolver::get_angularVelocities()
extern void ObiSolver_get_angularVelocities_mE565A9561A09435E294755AA2F7E31993BD932DB (void);
// 0x00000519 Obi.ObiNativeFloatList Obi.ObiSolver::get_invMasses()
extern void ObiSolver_get_invMasses_m403082935BEB22CE4C8A086C31B748FC46B8C319 (void);
// 0x0000051A Obi.ObiNativeFloatList Obi.ObiSolver::get_invRotationalMasses()
extern void ObiSolver_get_invRotationalMasses_m35049317216303EC2D97F143D98DBB60C9EADF58 (void);
// 0x0000051B Obi.ObiNativeVector4List Obi.ObiSolver::get_invInertiaTensors()
extern void ObiSolver_get_invInertiaTensors_m66E7A977E525B6628FA0D14015A52DE6CC26AADA (void);
// 0x0000051C Obi.ObiNativeVector4List Obi.ObiSolver::get_externalForces()
extern void ObiSolver_get_externalForces_m6DE0779EAA775DB7E7A853B844E07464240BED59 (void);
// 0x0000051D Obi.ObiNativeVector4List Obi.ObiSolver::get_externalTorques()
extern void ObiSolver_get_externalTorques_m7A057FD836A21C5CA1DAEA2258E05F81D15F8991 (void);
// 0x0000051E Obi.ObiNativeVector4List Obi.ObiSolver::get_wind()
extern void ObiSolver_get_wind_m72179C4670DBCE8A1838066EA134F02A16DC371C (void);
// 0x0000051F Obi.ObiNativeVector4List Obi.ObiSolver::get_positionDeltas()
extern void ObiSolver_get_positionDeltas_mA020DD1DBE7DDC042F8C730EBE9A71D6AE213AF9 (void);
// 0x00000520 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_orientationDeltas()
extern void ObiSolver_get_orientationDeltas_m747EE331713678E42AE2A16F9201AF96E8CFAF21 (void);
// 0x00000521 Obi.ObiNativeIntList Obi.ObiSolver::get_positionConstraintCounts()
extern void ObiSolver_get_positionConstraintCounts_mFBC0F55E2F573CC7A38F71703D8617DF5C9BF895 (void);
// 0x00000522 Obi.ObiNativeIntList Obi.ObiSolver::get_orientationConstraintCounts()
extern void ObiSolver_get_orientationConstraintCounts_m6752E0D0CAA0272BD7FF4242E5A426F4310FAECA (void);
// 0x00000523 Obi.ObiNativeIntList Obi.ObiSolver::get_collisionMaterials()
extern void ObiSolver_get_collisionMaterials_mE98DA539A9F5EC9BE8B35D9AA8C6F4C4FDA211A1 (void);
// 0x00000524 Obi.ObiNativeIntList Obi.ObiSolver::get_phases()
extern void ObiSolver_get_phases_m1DFABF0F6107CA7A301BBC35A0896C69FA1D7422 (void);
// 0x00000525 Obi.ObiNativeIntList Obi.ObiSolver::get_filters()
extern void ObiSolver_get_filters_m35DFCECD0FEF501CB8903CF986F67CD3487DFEBB (void);
// 0x00000526 Obi.ObiNativeVector4List Obi.ObiSolver::get_anisotropies()
extern void ObiSolver_get_anisotropies_mDD48FB3AC63921409AAF55FB2FED3833FC458009 (void);
// 0x00000527 Obi.ObiNativeVector4List Obi.ObiSolver::get_principalRadii()
extern void ObiSolver_get_principalRadii_m6C3307DA982056B2AF918D77B539E22580E5F179 (void);
// 0x00000528 Obi.ObiNativeVector4List Obi.ObiSolver::get_normals()
extern void ObiSolver_get_normals_m241E9FFCE3A531BFF2679A3420EBCE7B313FF52C (void);
// 0x00000529 Obi.ObiNativeVector4List Obi.ObiSolver::get_vorticities()
extern void ObiSolver_get_vorticities_mDC8CC9D454FBD7AABF51AEDD83B63D62D74D2E62 (void);
// 0x0000052A Obi.ObiNativeVector4List Obi.ObiSolver::get_fluidData()
extern void ObiSolver_get_fluidData_m70D3FEC3C44F6EA5D68AB6B39BD72A08B41EF349 (void);
// 0x0000052B Obi.ObiNativeVector4List Obi.ObiSolver::get_userData()
extern void ObiSolver_get_userData_m63E5DF119EE501A9380C6A176079C9FD1361B561 (void);
// 0x0000052C Obi.ObiNativeFloatList Obi.ObiSolver::get_smoothingRadii()
extern void ObiSolver_get_smoothingRadii_mEE720BA5CB3140CCB508FC97466DDC06F18A9380 (void);
// 0x0000052D Obi.ObiNativeFloatList Obi.ObiSolver::get_buoyancies()
extern void ObiSolver_get_buoyancies_m14142F7AA18DAE0CD2BA25EA5F3F7E5796938365 (void);
// 0x0000052E Obi.ObiNativeFloatList Obi.ObiSolver::get_restDensities()
extern void ObiSolver_get_restDensities_mA85A3F3E9C265A9E3B01B957691808F072123C96 (void);
// 0x0000052F Obi.ObiNativeFloatList Obi.ObiSolver::get_viscosities()
extern void ObiSolver_get_viscosities_mBCDE34198266ABADF85472134F8A69F897BB37D6 (void);
// 0x00000530 Obi.ObiNativeFloatList Obi.ObiSolver::get_surfaceTension()
extern void ObiSolver_get_surfaceTension_m06BBC45CF2C65E927490672F37C89AD69ACE039A (void);
// 0x00000531 Obi.ObiNativeFloatList Obi.ObiSolver::get_vortConfinement()
extern void ObiSolver_get_vortConfinement_mBCA2B8E737E63A3DADDC34F1E7E7C4D26F7A71D0 (void);
// 0x00000532 Obi.ObiNativeFloatList Obi.ObiSolver::get_atmosphericDrag()
extern void ObiSolver_get_atmosphericDrag_m81584993AAF86345E489C4D833D1BAF3E704CB16 (void);
// 0x00000533 Obi.ObiNativeFloatList Obi.ObiSolver::get_atmosphericPressure()
extern void ObiSolver_get_atmosphericPressure_m363045FB67ED30D2413C84317FBF2033FF9BE1B6 (void);
// 0x00000534 Obi.ObiNativeFloatList Obi.ObiSolver::get_diffusion()
extern void ObiSolver_get_diffusion_m614EC192C40E1DB26E20E2B7AEEB1E3853E1CBAA (void);
// 0x00000535 System.Void Obi.ObiSolver::Update()
extern void ObiSolver_Update_m3432EE148E4943BFEA7F248CC1ADF410EB009532 (void);
// 0x00000536 System.Void Obi.ObiSolver::OnDestroy()
extern void ObiSolver_OnDestroy_m10E800119F5492F06C454965798031E2959B4D91 (void);
// 0x00000537 System.Void Obi.ObiSolver::CreateBackend()
extern void ObiSolver_CreateBackend_m91B701B5D03356B869ECCC5E5B4E0F9E82CA7377 (void);
// 0x00000538 System.Void Obi.ObiSolver::Initialize()
extern void ObiSolver_Initialize_m69393E4E890B2AF174E313FAB09AF20C8858713B (void);
// 0x00000539 System.Void Obi.ObiSolver::Teardown()
extern void ObiSolver_Teardown_m7AFA06EFC9E0BA723690DF91837C3E3B8D6C30C1 (void);
// 0x0000053A System.Void Obi.ObiSolver::UpdateBackend()
extern void ObiSolver_UpdateBackend_mED822610A9CB49AC6197C9EB12A70EBF7F5959A5 (void);
// 0x0000053B System.Void Obi.ObiSolver::FreeRigidbodyArrays()
extern void ObiSolver_FreeRigidbodyArrays_m75E7122190DD0CFFE6594F39BED2EE9685488FA2 (void);
// 0x0000053C System.Void Obi.ObiSolver::EnsureRigidbodyArraysCapacity(System.Int32)
extern void ObiSolver_EnsureRigidbodyArraysCapacity_mC0A5656D8985DE06BC6084750EC6AE266B7E059A (void);
// 0x0000053D System.Void Obi.ObiSolver::FreeParticleArrays()
extern void ObiSolver_FreeParticleArrays_mC803A0F5E3291D91B643E13F40F56A1DD906DB19 (void);
// 0x0000053E System.Void Obi.ObiSolver::EnsureParticleArraysCapacity(System.Int32)
extern void ObiSolver_EnsureParticleArraysCapacity_mA3FF2925ACDE72EC10DF9853E51FF3440F27EB67 (void);
// 0x0000053F System.Void Obi.ObiSolver::AllocateParticles(System.Int32[])
extern void ObiSolver_AllocateParticles_m2437F6049C3AB8F791378C25BB220F3D447E9209 (void);
// 0x00000540 System.Void Obi.ObiSolver::FreeParticles(System.Int32[])
extern void ObiSolver_FreeParticles_m4CD1BFC3A67A853618E2BC48B666735D57D69CBB (void);
// 0x00000541 System.Boolean Obi.ObiSolver::AddActor(Obi.ObiActor)
extern void ObiSolver_AddActor_m33CCB3774385993AD4756D44613032EDF7602076 (void);
// 0x00000542 System.Boolean Obi.ObiSolver::RemoveActor(Obi.ObiActor)
extern void ObiSolver_RemoveActor_m9816A116E0848361565F850D70B3B8A876ACC5A2 (void);
// 0x00000543 System.Void Obi.ObiSolver::PushSolverParameters()
extern void ObiSolver_PushSolverParameters_mC566D7A04AABE0FD982060FDDCDF94BF0330A898 (void);
// 0x00000544 Oni/ConstraintParameters Obi.ObiSolver::GetConstraintParameters(Oni/ConstraintType)
extern void ObiSolver_GetConstraintParameters_m612C27EA24521064F789D15FABB06E8320EC50DD (void);
// 0x00000545 Obi.IObiConstraints Obi.ObiSolver::GetConstraintsByType(Oni/ConstraintType)
extern void ObiSolver_GetConstraintsByType_mE11A373E6629FB18A577363D79AE2626806FB095 (void);
// 0x00000546 System.Void Obi.ObiSolver::PushActiveParticles()
extern void ObiSolver_PushActiveParticles_m699BFA15ECA1837EB8E6FB0C4C06564BC00A4356 (void);
// 0x00000547 System.Void Obi.ObiSolver::PushSimplices()
extern void ObiSolver_PushSimplices_mCF761C06FD3C0023D8AC8CAF847F22856CA29F2B (void);
// 0x00000548 System.Void Obi.ObiSolver::PushConstraints()
extern void ObiSolver_PushConstraints_m1FE372176ADB5BDDB3C11F042850CA126D62EA7D (void);
// 0x00000549 System.Void Obi.ObiSolver::UpdateVisibility()
extern void ObiSolver_UpdateVisibility_m78B2DB3B14730D673C823699F0AF2CEB06524552 (void);
// 0x0000054A System.Void Obi.ObiSolver::InitializeTransformFrame()
extern void ObiSolver_InitializeTransformFrame_m37F3F63EE2DD9DDE17329B3FDF750A4C9AFF317C (void);
// 0x0000054B System.Void Obi.ObiSolver::UpdateTransformFrame(System.Single)
extern void ObiSolver_UpdateTransformFrame_m95D29D02A287E86629B2BF566891B519A5D3B2F4 (void);
// 0x0000054C System.Void Obi.ObiSolver::PrepareFrame()
extern void ObiSolver_PrepareFrame_mC4616463C08C47D37E9B5A9610850BD0853BCF59 (void);
// 0x0000054D Obi.IObiJobHandle Obi.ObiSolver::BeginStep(System.Single)
extern void ObiSolver_BeginStep_m08735BDB104DA6C7B894DDCBCE685C8F08EAE8C5 (void);
// 0x0000054E Obi.IObiJobHandle Obi.ObiSolver::Substep(System.Single,System.Single,System.Int32)
extern void ObiSolver_Substep_m8EF42B8FCB4A92B66A18967A5E30C0FAF9D91226 (void);
// 0x0000054F System.Void Obi.ObiSolver::EndStep(System.Single)
extern void ObiSolver_EndStep_m12D95AB19D02AB5C1E2ED1D8F3B1E586831DBA64 (void);
// 0x00000550 System.Void Obi.ObiSolver::Interpolate(System.Single,System.Single)
extern void ObiSolver_Interpolate_mAE7A83C120C8BCD50676E781D540026B6BAEEDE2 (void);
// 0x00000551 System.Void Obi.ObiSolver::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void ObiSolver_SpatialQuery_m6D0D03F289F49B89217E8A449DAABC0CCB90D83B (void);
// 0x00000552 Obi.QueryResult[] Obi.ObiSolver::SpatialQuery(Obi.QueryShape,Obi.AffineTransform)
extern void ObiSolver_SpatialQuery_m5C0D958E05B3BE7A66EFA6E320C0CB6E3A196607 (void);
// 0x00000553 System.Boolean Obi.ObiSolver::Raycast(UnityEngine.Ray,Obi.QueryResult&,System.Int32,System.Single,System.Single)
extern void ObiSolver_Raycast_m8F28F2F638DC71BC4716816A590D3D86E0274409 (void);
// 0x00000554 Obi.QueryResult[] Obi.ObiSolver::Raycast(System.Collections.Generic.List`1<UnityEngine.Ray>,System.Int32,System.Single,System.Single)
extern void ObiSolver_Raycast_mBABB28101E4819C4AC8C9D3189DB8636FD255007 (void);
// 0x00000555 System.Void Obi.ObiSolver::.ctor()
extern void ObiSolver__ctor_mA963ED6A2F8A02E33B2E3BACB178FC2F0436F753 (void);
// 0x00000556 System.Void Obi.ObiSolver::.cctor()
extern void ObiSolver__cctor_m4023459B57CD95EEF9656B8BDA9B18BDD558C46B (void);
// 0x00000557 System.Void Obi.ObiSolver/ObiCollisionEventArgs::.ctor()
extern void ObiCollisionEventArgs__ctor_m346881EB85AB69845A91659208E75756E4EF6D17 (void);
// 0x00000558 System.Void Obi.ObiSolver/ParticleInActor::.ctor()
extern void ParticleInActor__ctor_mAA089A9B7EBB63E8671D8AAB97FD0836220D7EEA (void);
// 0x00000559 System.Void Obi.ObiSolver/ParticleInActor::.ctor(Obi.ObiActor,System.Int32)
extern void ParticleInActor__ctor_m3829A4666F38432208F2811BDE98CD7A8C6B25F3 (void);
// 0x0000055A System.Void Obi.ObiSolver/SolverCallback::.ctor(System.Object,System.IntPtr)
extern void SolverCallback__ctor_mE4083F1B124E6A1FFB21E69EE2AEE7318B28AAD7 (void);
// 0x0000055B System.Void Obi.ObiSolver/SolverCallback::Invoke(Obi.ObiSolver)
extern void SolverCallback_Invoke_m45F6B3AC4BD61E22AAA076A7DBA5544E5C72F057 (void);
// 0x0000055C System.IAsyncResult Obi.ObiSolver/SolverCallback::BeginInvoke(Obi.ObiSolver,System.AsyncCallback,System.Object)
extern void SolverCallback_BeginInvoke_mD89642A3E1F37C4DC118EA6CACD1B6D68CD83A08 (void);
// 0x0000055D System.Void Obi.ObiSolver/SolverCallback::EndInvoke(System.IAsyncResult)
extern void SolverCallback_EndInvoke_m395BC54EFE0998B85005E9E4F66858F146C931F2 (void);
// 0x0000055E System.Void Obi.ObiSolver/SolverStepCallback::.ctor(System.Object,System.IntPtr)
extern void SolverStepCallback__ctor_m2079BE6990F358A8A5837AE21480E58DD4D0EEA3 (void);
// 0x0000055F System.Void Obi.ObiSolver/SolverStepCallback::Invoke(Obi.ObiSolver,System.Single)
extern void SolverStepCallback_Invoke_m68DD32CB29FBD1EC423CC5A72702D2CB05715754 (void);
// 0x00000560 System.IAsyncResult Obi.ObiSolver/SolverStepCallback::BeginInvoke(Obi.ObiSolver,System.Single,System.AsyncCallback,System.Object)
extern void SolverStepCallback_BeginInvoke_m434058E191E84C9A47950451E2B46A1F8F957F40 (void);
// 0x00000561 System.Void Obi.ObiSolver/SolverStepCallback::EndInvoke(System.IAsyncResult)
extern void SolverStepCallback_EndInvoke_mF3E0E0DE67ABA86472824000ACA507AB3352A533 (void);
// 0x00000562 System.Void Obi.ObiSolver/CollisionCallback::.ctor(System.Object,System.IntPtr)
extern void CollisionCallback__ctor_mA5A4EE9E910214B425B42DD40A2671EB949D661E (void);
// 0x00000563 System.Void Obi.ObiSolver/CollisionCallback::Invoke(Obi.ObiSolver,Obi.ObiSolver/ObiCollisionEventArgs)
extern void CollisionCallback_Invoke_m941AD708CC00FD8EA47569A5B061BB862B96F98C (void);
// 0x00000564 System.IAsyncResult Obi.ObiSolver/CollisionCallback::BeginInvoke(Obi.ObiSolver,Obi.ObiSolver/ObiCollisionEventArgs,System.AsyncCallback,System.Object)
extern void CollisionCallback_BeginInvoke_m0A88695225D3C02615D754DC211FD3EE3468BFAB (void);
// 0x00000565 System.Void Obi.ObiSolver/CollisionCallback::EndInvoke(System.IAsyncResult)
extern void CollisionCallback_EndInvoke_m46F561EE559B214E9D38C29ABCADAB3336B18A8A (void);
// 0x00000566 System.Void Obi.ObiSolver/<>c::.cctor()
extern void U3CU3Ec__cctor_m89EAB0A60F1D2D1667AB8DC5FFD689526BA4D3DE (void);
// 0x00000567 System.Void Obi.ObiSolver/<>c::.ctor()
extern void U3CU3Ec__ctor_m2D8F2915236AB77A7C36A87C7761B5503DCA10F2 (void);
// 0x00000568 System.Boolean Obi.ObiSolver/<>c::<get_allocParticleCount>b__148_0(Obi.ObiSolver/ParticleInActor)
extern void U3CU3Ec_U3Cget_allocParticleCountU3Eb__148_0_mDB5D6EA59542D260F8AA00864066AC567AF3613B (void);
// 0x00000569 System.Void Obi.ObiFixedUpdater::OnValidate()
extern void ObiFixedUpdater_OnValidate_m5F6FE3B9B8E80E3B125B39F142D2DA57A4DD8BFB (void);
// 0x0000056A System.Void Obi.ObiFixedUpdater::Awake()
extern void ObiFixedUpdater_Awake_m90D52F196706902484B59979C0A7537552FAB320 (void);
// 0x0000056B System.Void Obi.ObiFixedUpdater::OnDisable()
extern void ObiFixedUpdater_OnDisable_mEAE5E8BA5B2A650527D99517C37FBF98173259E9 (void);
// 0x0000056C System.Void Obi.ObiFixedUpdater::FixedUpdate()
extern void ObiFixedUpdater_FixedUpdate_m9CB26DEA62FD0EA6E7FBF86A2CFB12C375B81CE0 (void);
// 0x0000056D System.Void Obi.ObiFixedUpdater::Update()
extern void ObiFixedUpdater_Update_m485E215A24D5B8A2C3B191F2F65615D4C2A27A90 (void);
// 0x0000056E System.Void Obi.ObiFixedUpdater::.ctor()
extern void ObiFixedUpdater__ctor_m129BDE7A45025352A7238A317F1A3E7B31A00C9F (void);
// 0x0000056F System.Void Obi.ObiLateFixedUpdater::OnValidate()
extern void ObiLateFixedUpdater_OnValidate_m4B24969542BF8BAA45ABF2DD0FC90CD910FA27AC (void);
// 0x00000570 System.Void Obi.ObiLateFixedUpdater::Awake()
extern void ObiLateFixedUpdater_Awake_mA03AD9976B8FA43BE9FC155469524FBA224C096B (void);
// 0x00000571 System.Void Obi.ObiLateFixedUpdater::OnEnable()
extern void ObiLateFixedUpdater_OnEnable_m7AA55EFB2018C0FE7A7450DE8B124E9947735430 (void);
// 0x00000572 System.Void Obi.ObiLateFixedUpdater::OnDisable()
extern void ObiLateFixedUpdater_OnDisable_m508D776D30650C73FB72E47AEB5F2F7EFB64DFB9 (void);
// 0x00000573 System.Void Obi.ObiLateFixedUpdater::FixedUpdate()
extern void ObiLateFixedUpdater_FixedUpdate_m347A98DB1FFCE6D2654B99E6E228F26BC0646F21 (void);
// 0x00000574 System.Collections.IEnumerator Obi.ObiLateFixedUpdater::RunLateFixedUpdate()
extern void ObiLateFixedUpdater_RunLateFixedUpdate_mA207589FB295229010BC3ED39CA0A33B465AE4AB (void);
// 0x00000575 System.Void Obi.ObiLateFixedUpdater::LateFixedUpdate()
extern void ObiLateFixedUpdater_LateFixedUpdate_mC459F999C033D7BC22CD0A0BDA9569E93C2C6F2A (void);
// 0x00000576 System.Void Obi.ObiLateFixedUpdater::Update()
extern void ObiLateFixedUpdater_Update_mE16274CC3FCABACAA0430D79903C2298050907B9 (void);
// 0x00000577 System.Void Obi.ObiLateFixedUpdater::.ctor()
extern void ObiLateFixedUpdater__ctor_mA0140EBDF2452F0A415601A43DAE9822CCA249C6 (void);
// 0x00000578 System.Void Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__7::.ctor(System.Int32)
extern void U3CRunLateFixedUpdateU3Ed__7__ctor_m26D083432D24A29A1601BBEED04C921E5E757A62 (void);
// 0x00000579 System.Void Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__7::System.IDisposable.Dispose()
extern void U3CRunLateFixedUpdateU3Ed__7_System_IDisposable_Dispose_m28B4BB85E97B45021B22C15FEF3D8E93B5BF81C8 (void);
// 0x0000057A System.Boolean Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__7::MoveNext()
extern void U3CRunLateFixedUpdateU3Ed__7_MoveNext_mB69C634DCC614B7AB5126ED4FBF48E61309D1AC4 (void);
// 0x0000057B System.Object Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunLateFixedUpdateU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD948A54767F3CE844E780E8D04C37F8B9F865A05 (void);
// 0x0000057C System.Void Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRunLateFixedUpdateU3Ed__7_System_Collections_IEnumerator_Reset_mBC0D3D496D3790C6970DE839BF6F1DEE46B04FB3 (void);
// 0x0000057D System.Object Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRunLateFixedUpdateU3Ed__7_System_Collections_IEnumerator_get_Current_m200572F643F2FEB367A9BF53A37578AAAE32802F (void);
// 0x0000057E System.Void Obi.ObiLateUpdater::OnValidate()
extern void ObiLateUpdater_OnValidate_m6360BD327E35B0C56A590D51CB066B811DAD1774 (void);
// 0x0000057F System.Void Obi.ObiLateUpdater::Update()
extern void ObiLateUpdater_Update_mC52FDF37EE579A4D667D80C5327FD48FF06A7400 (void);
// 0x00000580 System.Void Obi.ObiLateUpdater::LateUpdate()
extern void ObiLateUpdater_LateUpdate_m04DE9233394AE2CC13CF1DA4F136364E81FE4C06 (void);
// 0x00000581 System.Void Obi.ObiLateUpdater::.ctor()
extern void ObiLateUpdater__ctor_mD001E04F8FFF222B20A8ECDBE7A8C2507C2F0894 (void);
// 0x00000582 System.Void Obi.ObiUpdater::PrepareFrame()
extern void ObiUpdater_PrepareFrame_m38DA2E247B4C02F4E18CA1E5890493598A019D65 (void);
// 0x00000583 System.Void Obi.ObiUpdater::BeginStep(System.Single)
extern void ObiUpdater_BeginStep_m871A82CAEA561457F1FB19B27ADC346661839AA5 (void);
// 0x00000584 System.Void Obi.ObiUpdater::Substep(System.Single,System.Single,System.Int32)
extern void ObiUpdater_Substep_m44F3765E77796D6D71287FF2883FF2E496E0767F (void);
// 0x00000585 System.Void Obi.ObiUpdater::EndStep(System.Single)
extern void ObiUpdater_EndStep_mD6CEC404A973E002CD4D81FC87442C77F537E55E (void);
// 0x00000586 System.Void Obi.ObiUpdater::Interpolate(System.Single,System.Single)
extern void ObiUpdater_Interpolate_m38784F191B281CBF7A3DE79B71EA71ED2FE60681 (void);
// 0x00000587 System.Void Obi.ObiUpdater::.ctor()
extern void ObiUpdater__ctor_m3A352531659D7BCCB0CF2AFF0732D3B28AB26092 (void);
// 0x00000588 System.Void Obi.ObiUpdater::.cctor()
extern void ObiUpdater__cctor_m272383BDBA28D7F89B9965940A9188BDE81EA1A9 (void);
// 0x00000589 System.Void Obi.ChildrenOnly::.ctor()
extern void ChildrenOnly__ctor_m4D3FAAA742E61B7FA2323CE9F448A1877F87CAE0 (void);
// 0x0000058A System.Void Obi.Indent::.ctor()
extern void Indent__ctor_m5A092B8D80C878888D885C6ADC0B3A9D3A90E3C7 (void);
// 0x0000058B System.Single Obi.InspectorButtonAttribute::get_ButtonWidth()
extern void InspectorButtonAttribute_get_ButtonWidth_mE4C1BF731F1203ABCDC7FF911FBC9AA619ECC4FB (void);
// 0x0000058C System.Void Obi.InspectorButtonAttribute::set_ButtonWidth(System.Single)
extern void InspectorButtonAttribute_set_ButtonWidth_mD7A12A99D234B3911E6D035C0224BC6AB4CC773A (void);
// 0x0000058D System.Void Obi.InspectorButtonAttribute::.ctor(System.String)
extern void InspectorButtonAttribute__ctor_m975A49E0C15C48813192407A712BDB21DCC86987 (void);
// 0x0000058E System.Void Obi.InspectorButtonAttribute::.cctor()
extern void InspectorButtonAttribute__cctor_mD9D19B7D28E89349026C4FBCAF129D59060E0349 (void);
// 0x0000058F System.Void Obi.MinMaxAttribute::.ctor(System.Single,System.Single)
extern void MinMaxAttribute__ctor_mAEC8FD68CEFC58D324E8BBF941E6940654EF2F78 (void);
// 0x00000590 System.Void Obi.MultiDelayed::.ctor()
extern void MultiDelayed__ctor_mC38D0EAB98C18CE8CCEBCC3767678783367BC0F5 (void);
// 0x00000591 System.Void Obi.MultiPropertyAttribute::.ctor()
extern void MultiPropertyAttribute__ctor_m0CEA425DBF3D690C2FBF18251EA3D45B8A2EEEFD (void);
// 0x00000592 System.Void Obi.MultiRange::.ctor(System.Single,System.Single)
extern void MultiRange__ctor_mB97A29FE677F7549CF13B4C38DEAA868640876C1 (void);
// 0x00000593 System.String Obi.SerializeProperty::get_PropertyName()
extern void SerializeProperty_get_PropertyName_mDC8A03791EF5C0FC8DB9BEE2645D05EC456A9B21 (void);
// 0x00000594 System.Void Obi.SerializeProperty::set_PropertyName(System.String)
extern void SerializeProperty_set_PropertyName_m0D097B39C9F982C6DA9CFE395012ED31AE075A55 (void);
// 0x00000595 System.Void Obi.SerializeProperty::.ctor(System.String)
extern void SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3 (void);
// 0x00000596 System.String Obi.VisibleIf::get_MethodName()
extern void VisibleIf_get_MethodName_mA9D547283CA5003031A6052EC2F01BDD20626C79 (void);
// 0x00000597 System.Void Obi.VisibleIf::set_MethodName(System.String)
extern void VisibleIf_set_MethodName_mEEE7DE756E758BB291ECC2AF95FB9331577A89BD (void);
// 0x00000598 System.Boolean Obi.VisibleIf::get_Negate()
extern void VisibleIf_get_Negate_m70429C0B2F6E15B7329A21127ED7F095B5B9F79C (void);
// 0x00000599 System.Void Obi.VisibleIf::set_Negate(System.Boolean)
extern void VisibleIf_set_Negate_m7AE9FD0E091984A71A593C3FE0A0A4CB89CDDF7E (void);
// 0x0000059A System.Void Obi.VisibleIf::.ctor(System.String,System.Boolean)
extern void VisibleIf__ctor_m74CCE1E688720090402D0E285F11AA7EE004E6C6 (void);
// 0x0000059B System.Object Obi.CoroutineJob::get_Result()
extern void CoroutineJob_get_Result_mD748A43832803B6F5A24988519355E0B5E960BD3 (void);
// 0x0000059C System.Boolean Obi.CoroutineJob::get_IsDone()
extern void CoroutineJob_get_IsDone_m4278531E6DBB4F3ED38BEB44A7D3365F6008420B (void);
// 0x0000059D System.Boolean Obi.CoroutineJob::get_RaisedException()
extern void CoroutineJob_get_RaisedException_mB7118E9ABE0C6CC7C00D741FC8BC5E859CC81F9A (void);
// 0x0000059E System.Void Obi.CoroutineJob::Init()
extern void CoroutineJob_Init_m08092162CA93D4B06D6F23B48138F018CA204416 (void);
// 0x0000059F System.Object Obi.CoroutineJob::RunSynchronously(System.Collections.IEnumerator)
extern void CoroutineJob_RunSynchronously_mC0C37C33DF39624CE6811D4D2229BF1DBB004DAB (void);
// 0x000005A0 System.Collections.IEnumerator Obi.CoroutineJob::Start(System.Collections.IEnumerator)
extern void CoroutineJob_Start_m80C8E17CD749B13B237150583A6280C213F8C39F (void);
// 0x000005A1 System.Void Obi.CoroutineJob::Stop()
extern void CoroutineJob_Stop_m503EBD4DCD2DB059CA0681D562A66E37442C7E4B (void);
// 0x000005A2 System.Void Obi.CoroutineJob::.ctor()
extern void CoroutineJob__ctor_m3B1FC813C2F585D8B745A74C9F40059C41C72437 (void);
// 0x000005A3 System.Void Obi.CoroutineJob/ProgressInfo::.ctor(System.String,System.Single)
extern void ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1 (void);
// 0x000005A4 System.Void Obi.CoroutineJob/<Start>d__15::.ctor(System.Int32)
extern void U3CStartU3Ed__15__ctor_mD7A6880AAEB86FECE339EAA24C276AA5D91FE5AD (void);
// 0x000005A5 System.Void Obi.CoroutineJob/<Start>d__15::System.IDisposable.Dispose()
extern void U3CStartU3Ed__15_System_IDisposable_Dispose_m4542E430FBE633F30A5F1B001DA8C7D01BE636F1 (void);
// 0x000005A6 System.Boolean Obi.CoroutineJob/<Start>d__15::MoveNext()
extern void U3CStartU3Ed__15_MoveNext_mF349A26AF1025AF7487E1BB1D725340CD887C18F (void);
// 0x000005A7 System.Object Obi.CoroutineJob/<Start>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m857DB02265865988D8A060D28C87EAB01F6DEC55 (void);
// 0x000005A8 System.Void Obi.CoroutineJob/<Start>d__15::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_mC889240009E6BB1F36B3BC74F618A35A604608B3 (void);
// 0x000005A9 System.Object Obi.CoroutineJob/<Start>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mE937748101E5BB647A3762B97A9DE2FED38D8283 (void);
// 0x000005AA System.Void Obi.EditorCoroutine::ShowCoroutineProgressBar(System.String,System.Collections.IEnumerator&)
extern void EditorCoroutine_ShowCoroutineProgressBar_mBD8BA0BAB4A8F6DBA32D5EB38A2AC2598992E522 (void);
// 0x000005AB System.Void Obi.EditorCoroutine::.ctor()
extern void EditorCoroutine__ctor_mDA14C1513A94EABA131D822C5BC96C78411A872E (void);
// 0x000005AC System.Void Obi.ObiAmbientForceZone::ApplyForcesToActor(Obi.ObiActor)
extern void ObiAmbientForceZone_ApplyForcesToActor_mAE4A4FC41002FA3D906FEA74194C377D2C60A8D8 (void);
// 0x000005AD System.Void Obi.ObiAmbientForceZone::OnDrawGizmosSelected()
extern void ObiAmbientForceZone_OnDrawGizmosSelected_m9F4A990AC63D31EFB4E459DDDE79DA6D99ED98DE (void);
// 0x000005AE System.Void Obi.ObiAmbientForceZone::.ctor()
extern void ObiAmbientForceZone__ctor_mD887D3FBC82BCD8DF7720A9B25BF72572118351E (void);
// 0x000005AF System.Void Obi.ObiExternalForce::OnEnable()
extern void ObiExternalForce_OnEnable_m0FB8D535D2ED4F394E48424E48C073B8E3FBD10F (void);
// 0x000005B0 System.Void Obi.ObiExternalForce::OnDisable()
extern void ObiExternalForce_OnDisable_mD1867136686D828057C7DF70B6A77B5DE81E459C (void);
// 0x000005B1 System.Void Obi.ObiExternalForce::Solver_OnStepBegin(Obi.ObiSolver,System.Single)
extern void ObiExternalForce_Solver_OnStepBegin_mC46587312AE49CD7183DAD9D16BBB66C9BF64E0E (void);
// 0x000005B2 System.Single Obi.ObiExternalForce::GetTurbulence(System.Single)
extern void ObiExternalForce_GetTurbulence_m36B324BC86245A69B5FFB5D924E08E35BCEC135D (void);
// 0x000005B3 System.Void Obi.ObiExternalForce::ApplyForcesToActor(Obi.ObiActor)
// 0x000005B4 System.Void Obi.ObiExternalForce::.ctor()
extern void ObiExternalForce__ctor_mD434F0876366D23C0D68DAB6A8949E018A42ACBD (void);
// 0x000005B5 System.Void Obi.ObiSphericalForceZone::ApplyForcesToActor(Obi.ObiActor)
extern void ObiSphericalForceZone_ApplyForcesToActor_m506A142D9EF2B82CD7310E150EF302F8A6B2FA2B (void);
// 0x000005B6 System.Void Obi.ObiSphericalForceZone::OnDrawGizmosSelected()
extern void ObiSphericalForceZone_OnDrawGizmosSelected_m428130DB0C2E2D23A31E3D0FFE00EDA0FB6744B2 (void);
// 0x000005B7 System.Void Obi.ObiSphericalForceZone::.ctor()
extern void ObiSphericalForceZone__ctor_mCB4320E2FDC045FAC6D25069AA629243434775F0 (void);
// 0x000005B8 System.Int32 Obi.ObiContactEventDispatcher::CompareByRef(Oni/Contact&,Oni/Contact&,Obi.ObiSolver)
extern void ObiContactEventDispatcher_CompareByRef_mCC27E2711928972C0B16573985711C3432D1719E (void);
// 0x000005B9 System.Void Obi.ObiContactEventDispatcher::Awake()
extern void ObiContactEventDispatcher_Awake_m4C9DC422D39BD21B3BE61C3BE4EB262BD0FECAD9 (void);
// 0x000005BA System.Void Obi.ObiContactEventDispatcher::OnEnable()
extern void ObiContactEventDispatcher_OnEnable_m9E49028DB875725C353943480CB5C447179953DC (void);
// 0x000005BB System.Void Obi.ObiContactEventDispatcher::OnDisable()
extern void ObiContactEventDispatcher_OnDisable_m8F75848B0553FD8776A22ABD1253D891B592CFA0 (void);
// 0x000005BC System.Int32 Obi.ObiContactEventDispatcher::FilterOutDistantContacts(Oni/Contact[],System.Int32)
extern void ObiContactEventDispatcher_FilterOutDistantContacts_m2088093F9BFC093F73ECB24BDC9E6DA7A748962B (void);
// 0x000005BD System.Int32 Obi.ObiContactEventDispatcher::RemoveDuplicates(Oni/Contact[],System.Int32)
extern void ObiContactEventDispatcher_RemoveDuplicates_m394D908408DBDED555F844001682FD217E94C5F9 (void);
// 0x000005BE System.Void Obi.ObiContactEventDispatcher::InvokeCallbacks(Oni/Contact[],System.Int32)
extern void ObiContactEventDispatcher_InvokeCallbacks_mB588492B596248DBC043C8235D2AD8D3AC3AC423 (void);
// 0x000005BF System.Void Obi.ObiContactEventDispatcher::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ObiContactEventDispatcher_Solver_OnCollision_m12E8F9442701D7911DE4F0D18CB0F4EC82F263FC (void);
// 0x000005C0 System.Void Obi.ObiContactEventDispatcher::.ctor()
extern void ObiContactEventDispatcher__ctor_m357D739552531DA027D8F320CCFAC444814BD791 (void);
// 0x000005C1 System.Void Obi.ObiContactEventDispatcher/ContactComparer::.ctor(Obi.ObiSolver)
extern void ContactComparer__ctor_m5DC2082E03C9DA3062C2D13D3F1702B3763C4379 (void);
// 0x000005C2 System.Int32 Obi.ObiContactEventDispatcher/ContactComparer::Compare(Oni/Contact,Oni/Contact)
extern void ContactComparer_Compare_mB97171F79E54851FEF29ACC2ED3F06C5A1452362 (void);
// 0x000005C3 System.Void Obi.ObiContactEventDispatcher/ContactCallback::.ctor()
extern void ContactCallback__ctor_m77F3DD3B53374589F094279143B1638E57919AD3 (void);
// 0x000005C4 Obi.ObiActor Obi.ObiParticleAttachment::get_actor()
extern void ObiParticleAttachment_get_actor_m8A6C711886370813BD7BC301BFB6C7812B6B193A (void);
// 0x000005C5 UnityEngine.Transform Obi.ObiParticleAttachment::get_target()
extern void ObiParticleAttachment_get_target_m87F09D825C3B834F3F32869BA35539B4525FA08E (void);
// 0x000005C6 System.Void Obi.ObiParticleAttachment::set_target(UnityEngine.Transform)
extern void ObiParticleAttachment_set_target_m05F5DB1086FA979873CCECE322EDE77C26550DF7 (void);
// 0x000005C7 Obi.ObiParticleGroup Obi.ObiParticleAttachment::get_particleGroup()
extern void ObiParticleAttachment_get_particleGroup_mE66342EC326E748311096EBF6508598B2D0E9CC1 (void);
// 0x000005C8 System.Void Obi.ObiParticleAttachment::set_particleGroup(Obi.ObiParticleGroup)
extern void ObiParticleAttachment_set_particleGroup_mD2ACDCEDFFDECA0B1737C92F4E2941CD93BEFDE7 (void);
// 0x000005C9 System.Boolean Obi.ObiParticleAttachment::get_isBound()
extern void ObiParticleAttachment_get_isBound_mD1067521C804A334B7A28CA3A9BD65237C0FD29D (void);
// 0x000005CA Obi.ObiParticleAttachment/AttachmentType Obi.ObiParticleAttachment::get_attachmentType()
extern void ObiParticleAttachment_get_attachmentType_m3AE549A02BCF14D72CE6EDCC93541FD03EA3B271 (void);
// 0x000005CB System.Void Obi.ObiParticleAttachment::set_attachmentType(Obi.ObiParticleAttachment/AttachmentType)
extern void ObiParticleAttachment_set_attachmentType_m5631ABD4AF1A1AFB91F5851BF869AD3974C2586F (void);
// 0x000005CC System.Boolean Obi.ObiParticleAttachment::get_constrainOrientation()
extern void ObiParticleAttachment_get_constrainOrientation_mD07EF64C3AE8F1F9CE99F747CF0D12C2DE7D6E1D (void);
// 0x000005CD System.Void Obi.ObiParticleAttachment::set_constrainOrientation(System.Boolean)
extern void ObiParticleAttachment_set_constrainOrientation_m17A07949BE7D8BBF51FCE020DFB2B07DA73A722F (void);
// 0x000005CE System.Single Obi.ObiParticleAttachment::get_compliance()
extern void ObiParticleAttachment_get_compliance_mF04CA0E1C82403AC5039D8F3816CB750E16E22D9 (void);
// 0x000005CF System.Void Obi.ObiParticleAttachment::set_compliance(System.Single)
extern void ObiParticleAttachment_set_compliance_m559FB99156912E50F36AE6239AEEF8DCE995A132 (void);
// 0x000005D0 System.Single Obi.ObiParticleAttachment::get_breakThreshold()
extern void ObiParticleAttachment_get_breakThreshold_mA89F93A18D03F1DADE699A97634716D15617B02F (void);
// 0x000005D1 System.Void Obi.ObiParticleAttachment::set_breakThreshold(System.Single)
extern void ObiParticleAttachment_set_breakThreshold_mF3F55D89A7E9E6EEF600D372A34DB537A752EB06 (void);
// 0x000005D2 System.Void Obi.ObiParticleAttachment::Awake()
extern void ObiParticleAttachment_Awake_m3F7F1CA4DFB4B2C06A8F98ACAA99335C589D62A8 (void);
// 0x000005D3 System.Void Obi.ObiParticleAttachment::OnDestroy()
extern void ObiParticleAttachment_OnDestroy_m5F5AB114D3BB369BF21F771A3F56EE68D5CF47B7 (void);
// 0x000005D4 System.Void Obi.ObiParticleAttachment::OnEnable()
extern void ObiParticleAttachment_OnEnable_mE67B8CA340675DC78907D05D206A0B32B47C8F6E (void);
// 0x000005D5 System.Void Obi.ObiParticleAttachment::OnDisable()
extern void ObiParticleAttachment_OnDisable_m970C2F665946F9526288866F57E00845E57F6231 (void);
// 0x000005D6 System.Void Obi.ObiParticleAttachment::OnValidate()
extern void ObiParticleAttachment_OnValidate_m8F4069C273A48281199B02A40AECFF1D83EA0C32 (void);
// 0x000005D7 System.Void Obi.ObiParticleAttachment::Actor_OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiParticleAttachment_Actor_OnBlueprintLoaded_m5736115ED7F6C5E1935D2217E7E5DBFDB5B20487 (void);
// 0x000005D8 System.Void Obi.ObiParticleAttachment::Actor_OnPrepareStep(Obi.ObiActor,System.Single)
extern void ObiParticleAttachment_Actor_OnPrepareStep_m9EA64FC23D3365CA10F37123637E56D333D6A2A8 (void);
// 0x000005D9 System.Void Obi.ObiParticleAttachment::Actor_OnEndStep(Obi.ObiActor,System.Single)
extern void ObiParticleAttachment_Actor_OnEndStep_m09B95B61E9325F33E9DF9B85945979C11962E0C1 (void);
// 0x000005DA System.Void Obi.ObiParticleAttachment::Bind()
extern void ObiParticleAttachment_Bind_m41671D060988813057E5D6CB710621B387C3B4C7 (void);
// 0x000005DB System.Void Obi.ObiParticleAttachment::EnableAttachment(Obi.ObiParticleAttachment/AttachmentType)
extern void ObiParticleAttachment_EnableAttachment_mB6BAA968F14899C9E41D3827506F5A05E45247CF (void);
// 0x000005DC System.Void Obi.ObiParticleAttachment::DisableAttachment(Obi.ObiParticleAttachment/AttachmentType)
extern void ObiParticleAttachment_DisableAttachment_m91236D7F5CFA0439C6EB8D250816CB587AD6392A (void);
// 0x000005DD System.Void Obi.ObiParticleAttachment::UpdateAttachment()
extern void ObiParticleAttachment_UpdateAttachment_m5537ED01556A4DC62E3F46AB5D43A9E2C875856A (void);
// 0x000005DE System.Void Obi.ObiParticleAttachment::BreakDynamicAttachment(System.Single)
extern void ObiParticleAttachment_BreakDynamicAttachment_m3AA55F0956AA144B3AC657224DF303EBACBB9022 (void);
// 0x000005DF System.Void Obi.ObiParticleAttachment::.ctor()
extern void ObiParticleAttachment__ctor_mAC181D9F03BE52C0DB29C07F539A74C0DEA1744B (void);
// 0x000005E0 System.Void Obi.ObiParticleDragger::OnEnable()
extern void ObiParticleDragger_OnEnable_m72B8C23953592A731785106A8459284845816A55 (void);
// 0x000005E1 System.Void Obi.ObiParticleDragger::OnDisable()
extern void ObiParticleDragger_OnDisable_mB11CAEAA8554518A0B474976561C8FEF0D6E8E49 (void);
// 0x000005E2 System.Void Obi.ObiParticleDragger::FixedUpdate()
extern void ObiParticleDragger_FixedUpdate_m9851F3C063BF90687B18E10701B7B1F5193A8E6E (void);
// 0x000005E3 System.Void Obi.ObiParticleDragger::Picker_OnParticleDragged(Obi.ObiParticlePicker/ParticlePickEventArgs)
extern void ObiParticleDragger_Picker_OnParticleDragged_mECCECAF2424C1D2656A6601ED602BFEA343EBB7C (void);
// 0x000005E4 System.Void Obi.ObiParticleDragger::Picker_OnParticleReleased(Obi.ObiParticlePicker/ParticlePickEventArgs)
extern void ObiParticleDragger_Picker_OnParticleReleased_mD5AFBB9569AFDD6F33D4E148A5A63B542331C87C (void);
// 0x000005E5 System.Void Obi.ObiParticleDragger::.ctor()
extern void ObiParticleDragger__ctor_mC7F5FD7D2B50D344D2D85CDA6CB55EA76DFD7A73 (void);
// 0x000005E6 System.Void Obi.ObiParticleGridDebugger::OnEnable()
extern void ObiParticleGridDebugger_OnEnable_mDCE0CF67BDC98D81F9FB3B4AA3DA73485F24A8E3 (void);
// 0x000005E7 System.Void Obi.ObiParticleGridDebugger::OnDisable()
extern void ObiParticleGridDebugger_OnDisable_m6EEAA88C141739F08763B496873E4FB8583ABA2D (void);
// 0x000005E8 System.Void Obi.ObiParticleGridDebugger::LateUpdate()
extern void ObiParticleGridDebugger_LateUpdate_m7A114715F2660DE5C8004F414580ABAB4B6BD796 (void);
// 0x000005E9 System.Void Obi.ObiParticleGridDebugger::OnDrawGizmos()
extern void ObiParticleGridDebugger_OnDrawGizmos_mFF5DD489989676AD3C6BACA195FE47F4E18C22AC (void);
// 0x000005EA System.Void Obi.ObiParticleGridDebugger::.ctor()
extern void ObiParticleGridDebugger__ctor_mC3547171B3F6C6C08F8E759186D7007E186A29B8 (void);
// 0x000005EB System.Void Obi.ObiParticlePicker::Awake()
extern void ObiParticlePicker_Awake_m74E07553D81756450A0D1B8351E1CBF8142F8F11 (void);
// 0x000005EC System.Void Obi.ObiParticlePicker::LateUpdate()
extern void ObiParticlePicker_LateUpdate_m9E75050EBAC6B52C81851303DEE0B2EA70353CF1 (void);
// 0x000005ED System.Void Obi.ObiParticlePicker::.ctor()
extern void ObiParticlePicker__ctor_m5818ABF9055AD22D294ED1EB13021BC9FE8AB3A9 (void);
// 0x000005EE System.Void Obi.ObiParticlePicker/ParticlePickEventArgs::.ctor(System.Int32,UnityEngine.Vector3)
extern void ParticlePickEventArgs__ctor_m64293DD03DECF493CE8EBD7844C21D2746C09EF0 (void);
// 0x000005EF System.Void Obi.ObiParticlePicker/ParticlePickUnityEvent::.ctor()
extern void ParticlePickUnityEvent__ctor_m8E79C656B1CDE3FB655B899990D215C91879BFA0 (void);
// 0x000005F0 System.Void Obi.ObiProfiler::Awake()
extern void ObiProfiler_Awake_mAB7F0E70026EC600D00A9130AEA40B9F2DD4D0D7 (void);
// 0x000005F1 System.Void Obi.ObiProfiler::OnDestroy()
extern void ObiProfiler_OnDestroy_m952E74CFF016FB1DB495D76228352ABED2E56166 (void);
// 0x000005F2 System.Void Obi.ObiProfiler::OnEnable()
extern void ObiProfiler_OnEnable_m2FA73A238837F2034B8E2907F2E87C347A07EFDC (void);
// 0x000005F3 System.Void Obi.ObiProfiler::OnDisable()
extern void ObiProfiler_OnDisable_m677BD3207054B10CDE9BAFA6EDC6D7C52DAD7B09 (void);
// 0x000005F4 System.Void Obi.ObiProfiler::EnableProfiler()
extern void ObiProfiler_EnableProfiler_mE07D4D3052ADC88FA8D0D882DF933E094D9F25FD (void);
// 0x000005F5 System.Void Obi.ObiProfiler::DisableProfiler()
extern void ObiProfiler_DisableProfiler_m6E225ACE957C1D38336D2C4FFEEBAC52540241E9 (void);
// 0x000005F6 System.Void Obi.ObiProfiler::BeginSample(System.String,System.Byte)
extern void ObiProfiler_BeginSample_mCA17FA232FD590BF488C6304FC5C4B4B77105DEC (void);
// 0x000005F7 System.Void Obi.ObiProfiler::EndSample()
extern void ObiProfiler_EndSample_mA76E83B9E7BB9DCEA7626780607D999D34868425 (void);
// 0x000005F8 System.Void Obi.ObiProfiler::UpdateProfilerInfo()
extern void ObiProfiler_UpdateProfilerInfo_m89B2E0A641CDEA86AB5195C8CB8CDE7ED666B9DF (void);
// 0x000005F9 System.Void Obi.ObiProfiler::OnGUI()
extern void ObiProfiler_OnGUI_m975F6EE95578505D3E6F1792AD1AFBBAD1375139 (void);
// 0x000005FA System.Void Obi.ObiProfiler::.ctor()
extern void ObiProfiler__ctor_m3EEB8D1C03DFE08C0EC0E23E334C190F41E40424 (void);
// 0x000005FB System.Void Obi.ObiStitcher::set_Actor1(Obi.ObiActor)
extern void ObiStitcher_set_Actor1_mABB05BE8A26AE51871FE07C7EAA9D53359016679 (void);
// 0x000005FC Obi.ObiActor Obi.ObiStitcher::get_Actor1()
extern void ObiStitcher_get_Actor1_mF7B8097B74CDE0D5491AA8CA0BFB9EA6F6521DE7 (void);
// 0x000005FD System.Void Obi.ObiStitcher::set_Actor2(Obi.ObiActor)
extern void ObiStitcher_set_Actor2_m8D4021102EDF55C787F2AFB5CBD83AEA4443C95E (void);
// 0x000005FE Obi.ObiActor Obi.ObiStitcher::get_Actor2()
extern void ObiStitcher_get_Actor2_mF6FCD44A6E57935EF04782FE6E80FF30A500356E (void);
// 0x000005FF System.Int32 Obi.ObiStitcher::get_StitchCount()
extern void ObiStitcher_get_StitchCount_m0A3C3380AA1B0536814453807186D85002F57744 (void);
// 0x00000600 System.Collections.Generic.IEnumerable`1<Obi.ObiStitcher/Stitch> Obi.ObiStitcher::get_Stitches()
extern void ObiStitcher_get_Stitches_mD04C880005B8B5F6261CFF2E5ABB4C713C27CDFA (void);
// 0x00000601 System.Void Obi.ObiStitcher::RegisterActor(Obi.ObiActor)
extern void ObiStitcher_RegisterActor_m6CF833604BDC8434EA68BD461456A891CCE1A6B6 (void);
// 0x00000602 System.Void Obi.ObiStitcher::UnregisterActor(Obi.ObiActor)
extern void ObiStitcher_UnregisterActor_m1C06E9E8003BEAEF75F59ED25B8B56561E5D4A4C (void);
// 0x00000603 System.Void Obi.ObiStitcher::OnEnable()
extern void ObiStitcher_OnEnable_mDEE87492FEFF02F28EC6D8CB231D8DA3893A8BBC (void);
// 0x00000604 System.Void Obi.ObiStitcher::OnDisable()
extern void ObiStitcher_OnDisable_m44319848A4EA3CB9AF55AE503389703F57250C00 (void);
// 0x00000605 System.Int32 Obi.ObiStitcher::AddStitch(System.Int32,System.Int32)
extern void ObiStitcher_AddStitch_m9CDD6C005F4A71C8E942AFCF0EE588AC47CBA1D7 (void);
// 0x00000606 System.Void Obi.ObiStitcher::RemoveStitch(System.Int32)
extern void ObiStitcher_RemoveStitch_m050637591BDE1882809E3902CD2E7C0FF62EB0CC (void);
// 0x00000607 System.Void Obi.ObiStitcher::Clear()
extern void ObiStitcher_Clear_mC2BBA753DCB0899C7C486C20565D05F6ED1F1B2F (void);
// 0x00000608 System.Void Obi.ObiStitcher::Actor_OnBlueprintUnloaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiStitcher_Actor_OnBlueprintUnloaded_m6CE4420D85CBD27371B1BCD548FC50F983C7DDC6 (void);
// 0x00000609 System.Void Obi.ObiStitcher::Actor_OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiStitcher_Actor_OnBlueprintLoaded_mF598296F75D17D0F818E4D7F7A1E98F7F3B8200A (void);
// 0x0000060A System.Void Obi.ObiStitcher::AddToSolver(Obi.ObiSolver)
extern void ObiStitcher_AddToSolver_mACC6329F09C6AC34703CC2C3CDCD6531D7ADCA63 (void);
// 0x0000060B System.Void Obi.ObiStitcher::RemoveFromSolver(Obi.ObiSolver)
extern void ObiStitcher_RemoveFromSolver_m5B3C5E8739E3393D3654A34D0D84972D7582C457 (void);
// 0x0000060C System.Void Obi.ObiStitcher::PushDataToSolver()
extern void ObiStitcher_PushDataToSolver_mA3B052D96FECDB53E4616B27E72A44F77A88EB31 (void);
// 0x0000060D System.Void Obi.ObiStitcher::.ctor()
extern void ObiStitcher__ctor_m8703E582D33DF3E6FB4265F91A5061420D984F97 (void);
// 0x0000060E System.Void Obi.ObiStitcher/Stitch::.ctor(System.Int32,System.Int32)
extern void Stitch__ctor_mA9EBE5BCBF80CAFB90725EF8F4B9E8EA693A9800 (void);
// 0x0000060F System.Void Obi.ObiUtils::DrawArrowGizmo(System.Single,System.Single,System.Single,System.Single)
extern void ObiUtils_DrawArrowGizmo_mB0F16371F7CA701F1934AB1D68E2C543BBEDC6D7 (void);
// 0x00000610 System.Void Obi.ObiUtils::DebugDrawCross(UnityEngine.Vector3,System.Single,UnityEngine.Color)
extern void ObiUtils_DebugDrawCross_m56B5756A7B53EF07A54DA1521D785F4B447A0794 (void);
// 0x00000611 System.Void Obi.ObiUtils::Swap(T&,T&)
// 0x00000612 System.Void Obi.ObiUtils::Swap(T[],System.Int32,System.Int32)
// 0x00000613 System.Void Obi.ObiUtils::Swap(System.Collections.Generic.IList`1<T>,System.Int32,System.Int32)
// 0x00000614 System.Void Obi.ObiUtils::ShiftLeft(T[],System.Int32,System.Int32,System.Int32)
// 0x00000615 System.Void Obi.ObiUtils::ShiftRight(T[],System.Int32,System.Int32,System.Int32)
// 0x00000616 System.Boolean Obi.ObiUtils::AreValid(UnityEngine.Bounds)
extern void ObiUtils_AreValid_m36409B4F74E4DA83A742954433F6CA4EE168BAE6 (void);
// 0x00000617 UnityEngine.Bounds Obi.ObiUtils::Transform(UnityEngine.Bounds,UnityEngine.Matrix4x4)
extern void ObiUtils_Transform_m733A8AC89C2BFBC7A0E0089512FFC4709454DE88 (void);
// 0x00000618 System.Int32 Obi.ObiUtils::CountTrailingZeroes(System.Int32)
extern void ObiUtils_CountTrailingZeroes_mD04646F64AE816B0BBE5FE3044C2DED6E90A0333 (void);
// 0x00000619 System.Void Obi.ObiUtils::Add(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiUtils_Add_m33CB21C40D66A37C46501B8D0F17D03411F7A6B8 (void);
// 0x0000061A System.Single Obi.ObiUtils::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void ObiUtils_Remap_m8F506FF64E3B9168DE2B0235E274CC1033B7D86F (void);
// 0x0000061B System.Single Obi.ObiUtils::Mod(System.Single,System.Single)
extern void ObiUtils_Mod_mD2EF1D694D52B0B1882C801A2FD576D8FC49D90E (void);
// 0x0000061C UnityEngine.Matrix4x4 Obi.ObiUtils::Add(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern void ObiUtils_Add_mC0C822C7DBBF1BA01EE882208292D519B2C9C2D4 (void);
// 0x0000061D System.Single Obi.ObiUtils::FrobeniusNorm(UnityEngine.Matrix4x4)
extern void ObiUtils_FrobeniusNorm_m864D4DA65931429E717193CB634FFAAF31C5DCF4 (void);
// 0x0000061E UnityEngine.Matrix4x4 Obi.ObiUtils::ScalarMultiply(UnityEngine.Matrix4x4,System.Single)
extern void ObiUtils_ScalarMultiply_mE25A02716B8B63129015ACA756D1DB1AD469DE51 (void);
// 0x0000061F UnityEngine.Vector3 Obi.ObiUtils::ProjectPointLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Boolean)
extern void ObiUtils_ProjectPointLine_m39534F2B61F19A53C004F9F72C2D5561C3BFCA15 (void);
// 0x00000620 System.Boolean Obi.ObiUtils::LinePlaneIntersection(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiUtils_LinePlaneIntersection_mA331CE0E861BFF21738B92A85B6009EA4641AD91 (void);
// 0x00000621 System.Single Obi.ObiUtils::RaySphereIntersection(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ObiUtils_RaySphereIntersection_m878269850BD66F1202937DEFF22C3A31ED3B5996 (void);
// 0x00000622 System.Single Obi.ObiUtils::InvMassToMass(System.Single)
extern void ObiUtils_InvMassToMass_m1ECAAAEDD4E27ADA1FDE1594DACCD249742ED136 (void);
// 0x00000623 System.Single Obi.ObiUtils::MassToInvMass(System.Single)
extern void ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68 (void);
// 0x00000624 System.Int32 Obi.ObiUtils::PureSign(System.Single)
extern void ObiUtils_PureSign_m92E94D198EF88DC390CCBA619718D6A6C27BA5C5 (void);
// 0x00000625 System.Void Obi.ObiUtils::NearestPointOnTri(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED (void);
// 0x00000626 System.Single Obi.ObiUtils::TriangleArea(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiUtils_TriangleArea_m560C3409898B4D67699FCFD0338C3ECDB244BCEB (void);
// 0x00000627 System.Single Obi.ObiUtils::EllipsoidVolume(UnityEngine.Vector3)
extern void ObiUtils_EllipsoidVolume_m406A1FFC6FADF93B25B2B072064B345E5DA5D2C5 (void);
// 0x00000628 UnityEngine.Quaternion Obi.ObiUtils::RestDarboux(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void ObiUtils_RestDarboux_mA5FBE38C5DA10FBD3F920E2F0E96683D3EF91507 (void);
// 0x00000629 System.Single Obi.ObiUtils::RestBendingConstraint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiUtils_RestBendingConstraint_mA2A2E588C9B5B6907DC2670CF3176A0441D95C78 (void);
// 0x0000062A System.Collections.IEnumerable Obi.ObiUtils::BilateralInterleaved(System.Int32)
extern void ObiUtils_BilateralInterleaved_m17A79FDAFD53A3C8E7CB5403AC539CD1BC4D5453 (void);
// 0x0000062B System.Void Obi.ObiUtils::BarycentricCoordinates(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197 (void);
// 0x0000062C System.Void Obi.ObiUtils::BarycentricInterpolation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1 (void);
// 0x0000062D System.Single Obi.ObiUtils::BarycentricInterpolation(System.Single,System.Single,System.Single,UnityEngine.Vector3)
extern void ObiUtils_BarycentricInterpolation_mAC20CA1234A3FB1D1D9AB2AA29D7987C0DF12361 (void);
// 0x0000062E System.Single Obi.ObiUtils::BarycentricExtrapolationScale(UnityEngine.Vector3)
extern void ObiUtils_BarycentricExtrapolationScale_m84AD9B6B2173A5538FDB174D72AA5CE127045D33 (void);
// 0x0000062F UnityEngine.Vector3[] Obi.ObiUtils::CalculateAngleWeightedNormals(UnityEngine.Vector3[],System.Int32[])
extern void ObiUtils_CalculateAngleWeightedNormals_m97CD00C800AA1BD2705A48113DD44B57E450DD34 (void);
// 0x00000630 System.Int32 Obi.ObiUtils::MakePhase(System.Int32,Obi.ObiUtils/ParticleFlags)
extern void ObiUtils_MakePhase_mED5844CB11913FC70EC9041F512C75E0B2F4E167 (void);
// 0x00000631 System.Int32 Obi.ObiUtils::GetGroupFromPhase(System.Int32)
extern void ObiUtils_GetGroupFromPhase_m95E80301165EB08A322A5043278D698903867054 (void);
// 0x00000632 Obi.ObiUtils/ParticleFlags Obi.ObiUtils::GetFlagsFromPhase(System.Int32)
extern void ObiUtils_GetFlagsFromPhase_m6712C3AFEE4371CD66C9A5548C96FEF64A990A57 (void);
// 0x00000633 System.Int32 Obi.ObiUtils::MakeFilter(System.Int32,System.Int32)
extern void ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3 (void);
// 0x00000634 System.Int32 Obi.ObiUtils::GetCategoryFromFilter(System.Int32)
extern void ObiUtils_GetCategoryFromFilter_mE5495C835432A19EE7A32B4E72D104A5179AD9ED (void);
// 0x00000635 System.Int32 Obi.ObiUtils::GetMaskFromFilter(System.Int32)
extern void ObiUtils_GetMaskFromFilter_mD6E7EF064AF96C8238D05A7EB5504E72CA1ECFCD (void);
// 0x00000636 System.Void Obi.ObiUtils::EigenSolve(UnityEngine.Matrix4x4,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern void ObiUtils_EigenSolve_m52D85DC48A8905EEFA96A9E0618B6569757370F7 (void);
// 0x00000637 UnityEngine.Vector3 Obi.ObiUtils::unitOrthogonal(UnityEngine.Vector3)
extern void ObiUtils_unitOrthogonal_m08E2004A970D966D5227E325780C23E6FAB28529 (void);
// 0x00000638 UnityEngine.Vector3 Obi.ObiUtils::EigenVector(UnityEngine.Matrix4x4,System.Single)
extern void ObiUtils_EigenVector_m650F505BE39681115F5A78F3AB4C76D6B766E360 (void);
// 0x00000639 UnityEngine.Vector3 Obi.ObiUtils::EigenValues(UnityEngine.Matrix4x4)
extern void ObiUtils_EigenValues_m4B78523278F5C36F028CC1E7AB8FA13BE10FB476 (void);
// 0x0000063A UnityEngine.Vector3 Obi.ObiUtils::GetPointCloudCentroid(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ObiUtils_GetPointCloudCentroid_m8C1020D867B29A5A516F0DF741330765D4B47BB5 (void);
// 0x0000063B System.Void Obi.ObiUtils::GetPointCloudAnisotropy(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single,System.Single,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void ObiUtils_GetPointCloudAnisotropy_m3D48980B4381A7B54F37C063FE47EF7C737CA9A2 (void);
// 0x0000063C System.Void Obi.ObiUtils::.cctor()
extern void ObiUtils__cctor_mE9D4CD7AA4BC90F980D0AC4F4DA0B543DFD75D01 (void);
// 0x0000063D System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::.ctor(System.Int32)
extern void U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10 (void);
// 0x0000063E System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::System.IDisposable.Dispose()
extern void U3CBilateralInterleavedU3Ed__40_System_IDisposable_Dispose_m8831F3EF2CA071AF13BD9D630BBA7F87306BD89C (void);
// 0x0000063F System.Boolean Obi.ObiUtils/<BilateralInterleaved>d__40::MoveNext()
extern void U3CBilateralInterleavedU3Ed__40_MoveNext_m77C2C019B232853A6CC9102657015E05AC815D61 (void);
// 0x00000640 System.Object Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9E1918A629F5BD2E500DBD47B57BEA70DA7126 (void);
// 0x00000641 System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerator.Reset()
extern void U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE (void);
// 0x00000642 System.Object Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_get_Current_m82E938AAC7D70CD2629E214DDAC1F71A6BB51474 (void);
// 0x00000643 System.Collections.Generic.IEnumerator`1<System.Object> Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
extern void U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279 (void);
// 0x00000644 System.Collections.IEnumerator Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerable.GetEnumerator()
extern void U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mBF01BD41B11190F2A9C48BE6A1A9FB04EF151CF6 (void);
// 0x00000645 System.Void Obi.ObiVectorMath::Cross(UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Single&,System.Single&)
extern void ObiVectorMath_Cross_m23CD1AEE1CC6B74D7930C98AA1D280513238A553 (void);
// 0x00000646 System.Void Obi.ObiVectorMath::Cross(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiVectorMath_Cross_mA47A8583DC381295371BD66F8397DD209C2B7731 (void);
// 0x00000647 System.Void Obi.ObiVectorMath::Cross(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single&,System.Single&,System.Single&)
extern void ObiVectorMath_Cross_m4C1FC1059C8BF006437BAA9D3CBBCD7323016E19 (void);
// 0x00000648 System.Void Obi.ObiVectorMath::Subtract(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiVectorMath_Subtract_mD703C48EC9EAAAF9901BCB1F1D0BF46E23A8925A (void);
// 0x00000649 System.Void Obi.ObiVectorMath::BarycentricInterpolation(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3&)
extern void ObiVectorMath_BarycentricInterpolation_m7CD9E0E0E11533DEB84C7003A1FBC5E415B6CAD4 (void);
// 0x0000064A System.Void Obi.SetCategory::Awake()
extern void SetCategory_Awake_mC91A0F14152A3306C3D58FFDB3268FC42CA75A1B (void);
// 0x0000064B System.Void Obi.SetCategory::OnDestroy()
extern void SetCategory_OnDestroy_m7E095279FCAA7AC75B26DBC31335E8C1B6156A5F (void);
// 0x0000064C System.Void Obi.SetCategory::OnValidate()
extern void SetCategory_OnValidate_m96641FEA78031AF96A7904C7B89F5526989E129A (void);
// 0x0000064D System.Void Obi.SetCategory::OnLoad(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void SetCategory_OnLoad_m105F18C77751C59F67759FE5006D36A29B4480AF (void);
// 0x0000064E System.Void Obi.SetCategory::.ctor()
extern void SetCategory__ctor_m5F2C7ADC7EAEF4C3E7B89A74858C43C67F3F9AD5 (void);
// 0x0000064F System.Void Obi.ObiBone::set_Filter(System.Int32)
extern void ObiBone_set_Filter_mB82194BCDD3BBEBC1AED600C28998746A1581139 (void);
// 0x00000650 System.Int32 Obi.ObiBone::get_Filter()
extern void ObiBone_get_Filter_m4376145C12B24B2534BA13A781D6627EB8D63881 (void);
// 0x00000651 System.Boolean Obi.ObiBone::get_selfCollisions()
extern void ObiBone_get_selfCollisions_mF1676B154190C4BC540B52E90B80E5BAD68CFA29 (void);
// 0x00000652 System.Void Obi.ObiBone::set_selfCollisions(System.Boolean)
extern void ObiBone_set_selfCollisions_m689A7F0F793147E6D5F79361AFCC5329B9361D1A (void);
// 0x00000653 Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_radius()
extern void ObiBone_get_radius_m1EBD5FBFDE44C849B4175A35CAD16168E5B26FCB (void);
// 0x00000654 System.Void Obi.ObiBone::set_radius(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_radius_mA4BC2323CFC84744775B0ADD88591372D0FB8A7E (void);
// 0x00000655 Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_mass()
extern void ObiBone_get_mass_mDF67BD9E5FCAEF4DB88A80FF1657A6AA96269937 (void);
// 0x00000656 System.Void Obi.ObiBone::set_mass(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_mass_m6A69DF3184A387E7F3F85DB546FB332807F4785A (void);
// 0x00000657 Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_rotationalMass()
extern void ObiBone_get_rotationalMass_mF1847788FF19552892D6E0F6B1B97987CDD9ACE3 (void);
// 0x00000658 System.Void Obi.ObiBone::set_rotationalMass(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_rotationalMass_m85EBF804BF8396DF8F596E775A71C6051ACF42ED (void);
// 0x00000659 System.Boolean Obi.ObiBone::get_skinConstraintsEnabled()
extern void ObiBone_get_skinConstraintsEnabled_mE05F5A90FBEDB3EA94F4ED3C580612555DF7A612 (void);
// 0x0000065A System.Void Obi.ObiBone::set_skinConstraintsEnabled(System.Boolean)
extern void ObiBone_set_skinConstraintsEnabled_mE1C59969DA5E5BC34C78362D874AE1BAA25D3EC5 (void);
// 0x0000065B Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_skinCompliance()
extern void ObiBone_get_skinCompliance_m775B8073203CF01A555595196689BFB632A4D0BE (void);
// 0x0000065C System.Void Obi.ObiBone::set_skinCompliance(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_skinCompliance_m9BB057DC71E2BEC64E09003D0B50B1374FDFAE8B (void);
// 0x0000065D Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_skinRadius()
extern void ObiBone_get_skinRadius_mA4B80A966827D27BECE9FBCE80DF47DDFD9107C9 (void);
// 0x0000065E System.Void Obi.ObiBone::set_skinRadius(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_skinRadius_mDA796FFAD3F1991C328DE2C596198377E5BF57A1 (void);
// 0x0000065F System.Boolean Obi.ObiBone::get_stretchShearConstraintsEnabled()
extern void ObiBone_get_stretchShearConstraintsEnabled_mAEEC2A924CAF912C3FC5BABB5DED2461A7852C37 (void);
// 0x00000660 System.Void Obi.ObiBone::set_stretchShearConstraintsEnabled(System.Boolean)
extern void ObiBone_set_stretchShearConstraintsEnabled_m16F142EFE0B768840503821F89E6234ABDB75F82 (void);
// 0x00000661 Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_stretchCompliance()
extern void ObiBone_get_stretchCompliance_mCE39CCB072F246783041A6541D5B8CEC688BF378 (void);
// 0x00000662 System.Void Obi.ObiBone::set_stretchCompliance(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_stretchCompliance_mFD349E90C44EF8AF81871E2AC3D4F9B9502BB20E (void);
// 0x00000663 Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_shear1Compliance()
extern void ObiBone_get_shear1Compliance_mBC21AE2AAA082BA20452DF7E60791F554CDFD2CC (void);
// 0x00000664 System.Void Obi.ObiBone::set_shear1Compliance(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_shear1Compliance_m2A934368B5C05CEDE1E0A0CE2BAB406DA9F0B12F (void);
// 0x00000665 Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_shear2Compliance()
extern void ObiBone_get_shear2Compliance_mF39DF6DB3D3EBE9467D5A0BB330216717C5A0783 (void);
// 0x00000666 System.Void Obi.ObiBone::set_shear2Compliance(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_shear2Compliance_m772C23DA827D6A69EEA6DB720CF78A6E9F3D3228 (void);
// 0x00000667 System.Boolean Obi.ObiBone::get_bendTwistConstraintsEnabled()
extern void ObiBone_get_bendTwistConstraintsEnabled_m4FF85604C8C37A41C0794FB0B8369ACF75985A74 (void);
// 0x00000668 System.Void Obi.ObiBone::set_bendTwistConstraintsEnabled(System.Boolean)
extern void ObiBone_set_bendTwistConstraintsEnabled_mEE7256D7949FB40E5D3668B644C0BAFFBAA1B785 (void);
// 0x00000669 Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_torsionCompliance()
extern void ObiBone_get_torsionCompliance_mD91D07722D08A5560F784881CE6539354F6907A5 (void);
// 0x0000066A System.Void Obi.ObiBone::set_torsionCompliance(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_torsionCompliance_m23DF39FF5E9374C187262D9BC592FFA4ABBF5B62 (void);
// 0x0000066B Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_bend1Compliance()
extern void ObiBone_get_bend1Compliance_m7E1CD0459126E20CEF76F75EB626B6F88F268E75 (void);
// 0x0000066C System.Void Obi.ObiBone::set_bend1Compliance(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_bend1Compliance_m0F8C7EFB8B106F6919E3D468183C3F08CDECEF22 (void);
// 0x0000066D Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_bend2Compliance()
extern void ObiBone_get_bend2Compliance_mDEDA98119786B607ACBBB67FE8F6B631FD467ED0 (void);
// 0x0000066E System.Void Obi.ObiBone::set_bend2Compliance(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_bend2Compliance_mB22F9C11E42F480E19EBF055078F1C670369D323 (void);
// 0x0000066F Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_plasticYield()
extern void ObiBone_get_plasticYield_m69B1721B7F1FD741DD62892203E0B4F49F4F7F44 (void);
// 0x00000670 System.Void Obi.ObiBone::set_plasticYield(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_plasticYield_m28F33914E711EEA1E3ECD99B19D84E6AF9A63B74 (void);
// 0x00000671 Obi.ObiBone/BonePropertyCurve Obi.ObiBone::get_plasticCreep()
extern void ObiBone_get_plasticCreep_m934520EB537785A61E6EFBACBE31E6AC2D542996 (void);
// 0x00000672 System.Void Obi.ObiBone::set_plasticCreep(Obi.ObiBone/BonePropertyCurve)
extern void ObiBone_set_plasticCreep_m8755FCA8ACC6FF5F6288F173481AEFC0341A3A2F (void);
// 0x00000673 Obi.ObiActorBlueprint Obi.ObiBone::get_sourceBlueprint()
extern void ObiBone_get_sourceBlueprint_m6981EEDCD3CC6D846011597AC50767DAD3CB5BAD (void);
// 0x00000674 Obi.ObiBoneBlueprint Obi.ObiBone::get_boneBlueprint()
extern void ObiBone_get_boneBlueprint_m2B65BE43F80CAD94BFF5CB3D22ECBD4C5283B853 (void);
// 0x00000675 System.Void Obi.ObiBone::set_boneBlueprint(Obi.ObiBoneBlueprint)
extern void ObiBone_set_boneBlueprint_mCDA8F86B77B30BC2937D5E21F994968695AB59CD (void);
// 0x00000676 System.Void Obi.ObiBone::Awake()
extern void ObiBone_Awake_m8AE667C282E0040D574D8C7F83C7BF84AEF8C39F (void);
// 0x00000677 System.Void Obi.ObiBone::OnDestroy()
extern void ObiBone_OnDestroy_mBA59F980819221EB8A4BD7C3B99C15BE60EB39E3 (void);
// 0x00000678 System.Void Obi.ObiBone::OnValidate()
extern void ObiBone_OnValidate_m9548925307E889440DE38A33C11A9137DBD4456F (void);
// 0x00000679 System.Void Obi.ObiBone::UpdateBlueprint()
extern void ObiBone_UpdateBlueprint_m1DBB634F8E2A3304F4F0342E94549C207BAACB97 (void);
// 0x0000067A System.Void Obi.ObiBone::LoadBlueprint(Obi.ObiSolver)
extern void ObiBone_LoadBlueprint_m51C03031A13F084FB76F917918C6E2B3B05D15E0 (void);
// 0x0000067B System.Void Obi.ObiBone::UnloadBlueprint(Obi.ObiSolver)
extern void ObiBone_UnloadBlueprint_mC6A78D9DD9C2CE95DF31B536E8CA22C1A17913F4 (void);
// 0x0000067C System.Void Obi.ObiBone::SetupRuntimeConstraints()
extern void ObiBone_SetupRuntimeConstraints_m6F158B5FC17EFE474455A4C6D5D6F7938979CE0E (void);
// 0x0000067D System.Void Obi.ObiBone::FixRoot()
extern void ObiBone_FixRoot_m56FA73306594480AADFE560EB7B6E7C6D89C8B96 (void);
// 0x0000067E System.Void Obi.ObiBone::UpdateFilter()
extern void ObiBone_UpdateFilter_mB579CD813F17057D517ECEAC9FDBF2F44A38B19E (void);
// 0x0000067F System.Void Obi.ObiBone::UpdateRadius()
extern void ObiBone_UpdateRadius_m056B887BF15C8F43BF8FB1307010AE236B615860 (void);
// 0x00000680 System.Void Obi.ObiBone::UpdateMasses()
extern void ObiBone_UpdateMasses_mAD10D4E9A2F0B178F333474AB0B92B445FFB7FAB (void);
// 0x00000681 UnityEngine.Vector3 Obi.ObiBone::GetSkinRadiiBackstop(Obi.ObiSkinConstraintsBatch,System.Int32)
extern void ObiBone_GetSkinRadiiBackstop_m9B2BB8C02DC7941BFDF908E2636636B0DCFCF270 (void);
// 0x00000682 System.Single Obi.ObiBone::GetSkinCompliance(Obi.ObiSkinConstraintsBatch,System.Int32)
extern void ObiBone_GetSkinCompliance_m1736CBFF470A0E948C7366A7C3348B0BF00DD178 (void);
// 0x00000683 UnityEngine.Vector3 Obi.ObiBone::GetBendTwistCompliance(Obi.ObiBendTwistConstraintsBatch,System.Int32)
extern void ObiBone_GetBendTwistCompliance_m3DDD20F692EED64EC24A8B44B70F2823A8E567BD (void);
// 0x00000684 UnityEngine.Vector2 Obi.ObiBone::GetBendTwistPlasticity(Obi.ObiBendTwistConstraintsBatch,System.Int32)
extern void ObiBone_GetBendTwistPlasticity_m40EDBD03EEB67FF291666E20B0AB298318198403 (void);
// 0x00000685 UnityEngine.Vector3 Obi.ObiBone::GetStretchShearCompliance(Obi.ObiStretchShearConstraintsBatch,System.Int32)
extern void ObiBone_GetStretchShearCompliance_m3D971128CE596E9634D45F7553808B7FEC744FC8 (void);
// 0x00000686 System.Void Obi.ObiBone::BeginStep(System.Single)
extern void ObiBone_BeginStep_m779035D44B6883800B160D4EC6EA2E4D629A4E2D (void);
// 0x00000687 System.Void Obi.ObiBone::PrepareFrame()
extern void ObiBone_PrepareFrame_m21331B51116F0074E101E77AF749E7E7082DC1B5 (void);
// 0x00000688 System.Void Obi.ObiBone::Interpolate()
extern void ObiBone_Interpolate_m32367276BF665A19C6DE3F2D7AF8C98D0B18AA46 (void);
// 0x00000689 System.Void Obi.ObiBone::ResetToCurrentShape()
extern void ObiBone_ResetToCurrentShape_m9D3AD6CC32B50881A9B435979A8B1DB5FB3C2DA4 (void);
// 0x0000068A System.Void Obi.ObiBone::ResetReferenceOrientations()
extern void ObiBone_ResetReferenceOrientations_m9883C404257269137250CFE258481796D3A22449 (void);
// 0x0000068B System.Void Obi.ObiBone::UpdateRestShape()
extern void ObiBone_UpdateRestShape_m839D95FF4B72AEA0B6CB7E3F9FB42BD9D59F5163 (void);
// 0x0000068C System.Void Obi.ObiBone::CopyParticleDataToTransforms()
extern void ObiBone_CopyParticleDataToTransforms_mD95205602C8B3FEA4E25722D0E983891123EF0F9 (void);
// 0x0000068D System.Void Obi.ObiBone::.ctor()
extern void ObiBone__ctor_m5FD25B5CA6E834E39E552AAF0F5D698D4691BEDB (void);
// 0x0000068E System.Void Obi.ObiBone/BonePropertyCurve::.ctor(System.Single,System.Single)
extern void BonePropertyCurve__ctor_mB95157224C06C09854DF712065D2AE0F24FD87B3 (void);
// 0x0000068F System.Single Obi.ObiBone/BonePropertyCurve::Evaluate(System.Single)
extern void BonePropertyCurve_Evaluate_m3E05031EF7155563B70DB6BA04B28BBB3DEE39F7 (void);
// 0x00000690 System.Void Obi.ObiBone/IgnoredBone::.ctor()
extern void IgnoredBone__ctor_mDC468B916D95F3913283BE1B6CD09203717C9C7E (void);
// 0x00000691 System.Boolean Obi.ObiRod::get_selfCollisions()
extern void ObiRod_get_selfCollisions_mE9183861A26FB1AEB33690D9670E6B2FED085DAD (void);
// 0x00000692 System.Void Obi.ObiRod::set_selfCollisions(System.Boolean)
extern void ObiRod_set_selfCollisions_mD26D3B8B8267A0E44142AE7684E90A4E73C5D326 (void);
// 0x00000693 System.Boolean Obi.ObiRod::get_stretchShearConstraintsEnabled()
extern void ObiRod_get_stretchShearConstraintsEnabled_m134C94FD6508FB5C983019CEFF3E19F1ABBDEB47 (void);
// 0x00000694 System.Void Obi.ObiRod::set_stretchShearConstraintsEnabled(System.Boolean)
extern void ObiRod_set_stretchShearConstraintsEnabled_m5A5893CDE5B97D9CDF92554FC1F89A00D218B425 (void);
// 0x00000695 System.Single Obi.ObiRod::get_stretchCompliance()
extern void ObiRod_get_stretchCompliance_mA429E219BBAFC25B96D7E6F883209CB579A22296 (void);
// 0x00000696 System.Void Obi.ObiRod::set_stretchCompliance(System.Single)
extern void ObiRod_set_stretchCompliance_m8392980511E473452A2E51CD19C70F9BF6E5AF78 (void);
// 0x00000697 System.Single Obi.ObiRod::get_shear1Compliance()
extern void ObiRod_get_shear1Compliance_mBAED97836BD0BCC734D0D5205530BE91ABF443CE (void);
// 0x00000698 System.Void Obi.ObiRod::set_shear1Compliance(System.Single)
extern void ObiRod_set_shear1Compliance_mF41784E93B06FB466D99CFFE0F62779BD277056C (void);
// 0x00000699 System.Single Obi.ObiRod::get_shear2Compliance()
extern void ObiRod_get_shear2Compliance_m174B88A01B300DE16E82B316D8A7043C00985D5F (void);
// 0x0000069A System.Void Obi.ObiRod::set_shear2Compliance(System.Single)
extern void ObiRod_set_shear2Compliance_m3F24261279E57EE1953CACD81CD8A26CD52EB157 (void);
// 0x0000069B System.Boolean Obi.ObiRod::get_bendTwistConstraintsEnabled()
extern void ObiRod_get_bendTwistConstraintsEnabled_mB03FCAE861CF5B574E221DBE72AC5B0EE5885423 (void);
// 0x0000069C System.Void Obi.ObiRod::set_bendTwistConstraintsEnabled(System.Boolean)
extern void ObiRod_set_bendTwistConstraintsEnabled_m41025553FA9B05DA71539F741BCC80739E372286 (void);
// 0x0000069D System.Single Obi.ObiRod::get_torsionCompliance()
extern void ObiRod_get_torsionCompliance_m9B201D7D2B3A37E1FE3951415F133782B131B598 (void);
// 0x0000069E System.Void Obi.ObiRod::set_torsionCompliance(System.Single)
extern void ObiRod_set_torsionCompliance_mB45A259015000A7797682C11DF9DB538E4DD8DB6 (void);
// 0x0000069F System.Single Obi.ObiRod::get_bend1Compliance()
extern void ObiRod_get_bend1Compliance_m9F78AD3F731DE5B8DE3720FD174EFF77BAC97C82 (void);
// 0x000006A0 System.Void Obi.ObiRod::set_bend1Compliance(System.Single)
extern void ObiRod_set_bend1Compliance_mC87920DDBA91089A8899EDE3ED9ADEB2EF98FC14 (void);
// 0x000006A1 System.Single Obi.ObiRod::get_bend2Compliance()
extern void ObiRod_get_bend2Compliance_m23272235C9E69D5754340387D36F2687B50459B6 (void);
// 0x000006A2 System.Void Obi.ObiRod::set_bend2Compliance(System.Single)
extern void ObiRod_set_bend2Compliance_mE3496FD453AE558DD047128855149278EA88FA4E (void);
// 0x000006A3 System.Single Obi.ObiRod::get_plasticYield()
extern void ObiRod_get_plasticYield_m0065816F70FD4A02D7058F77887A59F64CD3D23C (void);
// 0x000006A4 System.Void Obi.ObiRod::set_plasticYield(System.Single)
extern void ObiRod_set_plasticYield_m3A3A9495145722A3A58E208627AC7068272721C6 (void);
// 0x000006A5 System.Single Obi.ObiRod::get_plasticCreep()
extern void ObiRod_get_plasticCreep_m2C98131B098B6E95E2FB774911053A62D7EE491E (void);
// 0x000006A6 System.Void Obi.ObiRod::set_plasticCreep(System.Single)
extern void ObiRod_set_plasticCreep_m9703D71D219EC3B694B2990F7F1CE2E36ACB2B50 (void);
// 0x000006A7 System.Boolean Obi.ObiRod::get_chainConstraintsEnabled()
extern void ObiRod_get_chainConstraintsEnabled_mF398577FB32AC2F7366B9731C74086CD31E6410C (void);
// 0x000006A8 System.Void Obi.ObiRod::set_chainConstraintsEnabled(System.Boolean)
extern void ObiRod_set_chainConstraintsEnabled_m46D6C7C03D1A1A42F464DB990A43CEB8212D2377 (void);
// 0x000006A9 System.Single Obi.ObiRod::get_tightness()
extern void ObiRod_get_tightness_m4113D75F222782F0F543F49DCFF9A0655F5FDA6E (void);
// 0x000006AA System.Void Obi.ObiRod::set_tightness(System.Single)
extern void ObiRod_set_tightness_m2632FBE37B4F81A0D3206339802CA6EFDCD1A70D (void);
// 0x000006AB System.Single Obi.ObiRod::get_interParticleDistance()
extern void ObiRod_get_interParticleDistance_m4B8CE4FBDD98994D21B2AABCF34EF8102749E884 (void);
// 0x000006AC Obi.ObiActorBlueprint Obi.ObiRod::get_sourceBlueprint()
extern void ObiRod_get_sourceBlueprint_mEA32815318034D19AC68E42451B170AD29221C8D (void);
// 0x000006AD Obi.ObiRodBlueprint Obi.ObiRod::get_rodBlueprint()
extern void ObiRod_get_rodBlueprint_m7D87017048D1F8A1B66BB6EE13B5A071E1787FE9 (void);
// 0x000006AE System.Void Obi.ObiRod::set_rodBlueprint(Obi.ObiRodBlueprint)
extern void ObiRod_set_rodBlueprint_m50978B3C11B1E0DACCED1055AF457377DEB58A93 (void);
// 0x000006AF System.Void Obi.ObiRod::OnValidate()
extern void ObiRod_OnValidate_m0E1BE0E449175DB9D36961851E32E455FB33C0FA (void);
// 0x000006B0 System.Void Obi.ObiRod::LoadBlueprint(Obi.ObiSolver)
extern void ObiRod_LoadBlueprint_mA932979582E924746DFC470C837AAF706DD98E45 (void);
// 0x000006B1 System.Void Obi.ObiRod::SetupRuntimeConstraints()
extern void ObiRod_SetupRuntimeConstraints_mDD133102C5C36FF988973EE643D0288FBB1F8F12 (void);
// 0x000006B2 UnityEngine.Vector3 Obi.ObiRod::GetBendTwistCompliance(Obi.ObiBendTwistConstraintsBatch,System.Int32)
extern void ObiRod_GetBendTwistCompliance_mCFC552CA6216A5C88F270C85A07AB6F0CAE62FEE (void);
// 0x000006B3 UnityEngine.Vector2 Obi.ObiRod::GetBendTwistPlasticity(Obi.ObiBendTwistConstraintsBatch,System.Int32)
extern void ObiRod_GetBendTwistPlasticity_mE6D70FBE4283FFB50A6802CE67381357562E770F (void);
// 0x000006B4 UnityEngine.Vector3 Obi.ObiRod::GetStretchShearCompliance(Obi.ObiStretchShearConstraintsBatch,System.Int32)
extern void ObiRod_GetStretchShearCompliance_m7F5CED48D01A9E1DE36E5E9ADEC9BD77CAED742D (void);
// 0x000006B5 System.Void Obi.ObiRod::RebuildElementsFromConstraintsInternal()
extern void ObiRod_RebuildElementsFromConstraintsInternal_m06CD554B61B7E3639D1F0E57D70C31EDE68DA165 (void);
// 0x000006B6 System.Void Obi.ObiRod::.ctor()
extern void ObiRod__ctor_mE0D8CB442D6F5CDA019C1B9352B9A5B96B3ECFFB (void);
// 0x000006B7 System.Boolean Obi.ObiRope::get_selfCollisions()
extern void ObiRope_get_selfCollisions_m466A37ECD8D40D71A1EF9BE9A59A8D182F3522B6 (void);
// 0x000006B8 System.Void Obi.ObiRope::set_selfCollisions(System.Boolean)
extern void ObiRope_set_selfCollisions_m14B2A380D7B14B3B11621718ED371C0DA1914F30 (void);
// 0x000006B9 System.Boolean Obi.ObiRope::get_distanceConstraintsEnabled()
extern void ObiRope_get_distanceConstraintsEnabled_m014F78BEA9B263AC58C1908AB2AA16469283A212 (void);
// 0x000006BA System.Void Obi.ObiRope::set_distanceConstraintsEnabled(System.Boolean)
extern void ObiRope_set_distanceConstraintsEnabled_mAC6131E60DD536D5ACA5DD5AC54944C0042C6D89 (void);
// 0x000006BB System.Single Obi.ObiRope::get_stretchingScale()
extern void ObiRope_get_stretchingScale_m828729DFA156687DF38B0EAFF40EA9EC5EAFCFE6 (void);
// 0x000006BC System.Void Obi.ObiRope::set_stretchingScale(System.Single)
extern void ObiRope_set_stretchingScale_m2AFCA9A3B98C34FD435194036D29CDD739F23AEF (void);
// 0x000006BD System.Single Obi.ObiRope::get_stretchCompliance()
extern void ObiRope_get_stretchCompliance_mA5A38FBB9984BF0C081D39FE1F9145BAABF5B3DD (void);
// 0x000006BE System.Void Obi.ObiRope::set_stretchCompliance(System.Single)
extern void ObiRope_set_stretchCompliance_mA703D9C1430BC2DA0C59D1D43DC5C6D87DB71DD3 (void);
// 0x000006BF System.Single Obi.ObiRope::get_maxCompression()
extern void ObiRope_get_maxCompression_mD8AA2252CEDCBF8B8FDB0E06CFD6A0CF7EC3F341 (void);
// 0x000006C0 System.Void Obi.ObiRope::set_maxCompression(System.Single)
extern void ObiRope_set_maxCompression_m9C1AA47C47EE6F4D743CD3D5AC90FDF1D0359DC2 (void);
// 0x000006C1 System.Boolean Obi.ObiRope::get_bendConstraintsEnabled()
extern void ObiRope_get_bendConstraintsEnabled_m81710C435F918413DF98DEB226BDB6269991094A (void);
// 0x000006C2 System.Void Obi.ObiRope::set_bendConstraintsEnabled(System.Boolean)
extern void ObiRope_set_bendConstraintsEnabled_mA265575A5F1993828D7A5788F8DA29B527C9805E (void);
// 0x000006C3 System.Single Obi.ObiRope::get_bendCompliance()
extern void ObiRope_get_bendCompliance_m3F8B6B00DCA2A3A4288785B840F61DD76B883C56 (void);
// 0x000006C4 System.Void Obi.ObiRope::set_bendCompliance(System.Single)
extern void ObiRope_set_bendCompliance_m5DADBD580299BB6B83FE543B7813F4AA0EFFD7BB (void);
// 0x000006C5 System.Single Obi.ObiRope::get_maxBending()
extern void ObiRope_get_maxBending_mBB0783E2F3C4F93611CD827193D031CFFC46509A (void);
// 0x000006C6 System.Void Obi.ObiRope::set_maxBending(System.Single)
extern void ObiRope_set_maxBending_m089CF2E6791FE7573E2F4C172341B33AF7855729 (void);
// 0x000006C7 System.Single Obi.ObiRope::get_plasticYield()
extern void ObiRope_get_plasticYield_m43A5F27180E61D99A230C0F8EA743215B4590090 (void);
// 0x000006C8 System.Void Obi.ObiRope::set_plasticYield(System.Single)
extern void ObiRope_set_plasticYield_m6282A37741B42F7F489BD9CA4F3FD22040A8CCCC (void);
// 0x000006C9 System.Single Obi.ObiRope::get_plasticCreep()
extern void ObiRope_get_plasticCreep_mB625840F0032B6000A4C0BCC4B84D39EE10D6C7E (void);
// 0x000006CA System.Void Obi.ObiRope::set_plasticCreep(System.Single)
extern void ObiRope_set_plasticCreep_m3E26642B38EF1439982FB67D5F4B4730E3B9EA97 (void);
// 0x000006CB System.Single Obi.ObiRope::get_interParticleDistance()
extern void ObiRope_get_interParticleDistance_m3D6B5CB17F7CCCEC7F3E3FF619B15ACF2AFB2740 (void);
// 0x000006CC Obi.ObiActorBlueprint Obi.ObiRope::get_sourceBlueprint()
extern void ObiRope_get_sourceBlueprint_m778885AC5ACB50CF2D5E2283F644CCB58228723B (void);
// 0x000006CD Obi.ObiRopeBlueprint Obi.ObiRope::get_ropeBlueprint()
extern void ObiRope_get_ropeBlueprint_mB585217A4141B42877FDF506957447CBEE268856 (void);
// 0x000006CE System.Void Obi.ObiRope::set_ropeBlueprint(Obi.ObiRopeBlueprint)
extern void ObiRope_set_ropeBlueprint_m9420BEF4A7365801B289A53420BC132BF1E7905F (void);
// 0x000006CF System.Void Obi.ObiRope::add_OnRopeTorn(Obi.ObiRope/RopeTornCallback)
extern void ObiRope_add_OnRopeTorn_m82818AFDC4E5B53B9BDB3BC5F448DC5819AEAF17 (void);
// 0x000006D0 System.Void Obi.ObiRope::remove_OnRopeTorn(Obi.ObiRope/RopeTornCallback)
extern void ObiRope_remove_OnRopeTorn_mDCCBD9E8FEAA87E7DB5622737423840AD4EE1A5F (void);
// 0x000006D1 System.Void Obi.ObiRope::OnValidate()
extern void ObiRope_OnValidate_m2DE95ADDCC76CF26B486F895D82513F3888C4CB5 (void);
// 0x000006D2 System.Void Obi.ObiRope::LoadBlueprint(Obi.ObiSolver)
extern void ObiRope_LoadBlueprint_m40B2EC79FD19E8746D04A5E6B5EE11BDF4186D4E (void);
// 0x000006D3 System.Void Obi.ObiRope::UnloadBlueprint(Obi.ObiSolver)
extern void ObiRope_UnloadBlueprint_m0C3352A53979E5701391C9A8DA9489DC60A01DC9 (void);
// 0x000006D4 System.Void Obi.ObiRope::SetupRuntimeConstraints()
extern void ObiRope_SetupRuntimeConstraints_m4E7E18E6F869E2C2497931390EF27C2A88FF2F83 (void);
// 0x000006D5 System.Void Obi.ObiRope::Substep(System.Single)
extern void ObiRope_Substep_mEF3BE7DF70726281DE8908A31C95AC724F55B387 (void);
// 0x000006D6 System.Void Obi.ObiRope::ApplyTearing(System.Single)
extern void ObiRope_ApplyTearing_mB720B5C8911A09FA2154066F9D8E2525FBEDC91E (void);
// 0x000006D7 System.Int32 Obi.ObiRope::SplitParticle(System.Int32)
extern void ObiRope_SplitParticle_m7FF40778B8998EB1090521401FF3373B1444EFAF (void);
// 0x000006D8 System.Boolean Obi.ObiRope::Tear(Obi.ObiStructuralElement)
extern void ObiRope_Tear_m612ED4759B7F149F4263E218FFCDF9A6374BD08C (void);
// 0x000006D9 System.Void Obi.ObiRope::RebuildElementsFromConstraintsInternal()
extern void ObiRope_RebuildElementsFromConstraintsInternal_m5185A20153E1C85F8609AC97E69045370C6992CC (void);
// 0x000006DA System.Void Obi.ObiRope::RebuildConstraintsFromElements()
extern void ObiRope_RebuildConstraintsFromElements_m16F44C93051F66275628E315018CCE104BE67F7E (void);
// 0x000006DB System.Void Obi.ObiRope::.ctor()
extern void ObiRope__ctor_m4A887B1B2064D68904BC6C7A1ADC89115A4ED4F7 (void);
// 0x000006DC System.Void Obi.ObiRope/RopeTornCallback::.ctor(System.Object,System.IntPtr)
extern void RopeTornCallback__ctor_m74940736BB0FE86E5D9978DE57B83F6514334BEE (void);
// 0x000006DD System.Void Obi.ObiRope/RopeTornCallback::Invoke(Obi.ObiRope,Obi.ObiRope/ObiRopeTornEventArgs)
extern void RopeTornCallback_Invoke_mA260D7064D0816B8431CD1EFF00C0D5056B6CC55 (void);
// 0x000006DE System.IAsyncResult Obi.ObiRope/RopeTornCallback::BeginInvoke(Obi.ObiRope,Obi.ObiRope/ObiRopeTornEventArgs,System.AsyncCallback,System.Object)
extern void RopeTornCallback_BeginInvoke_mFC33BEC1937BA04BC48653AFA9D75E530B95525C (void);
// 0x000006DF System.Void Obi.ObiRope/RopeTornCallback::EndInvoke(System.IAsyncResult)
extern void RopeTornCallback_EndInvoke_m9E58F646BB4EDF02833D0D8A97BF71746B3C5712 (void);
// 0x000006E0 System.Void Obi.ObiRope/ObiRopeTornEventArgs::.ctor(Obi.ObiStructuralElement,System.Int32)
extern void ObiRopeTornEventArgs__ctor_m9FDD4BA5BA9BEFAB7A96904A52289FCEA3624DF8 (void);
// 0x000006E1 System.Void Obi.ObiRope/<>c::.cctor()
extern void U3CU3Ec__cctor_m1EB58583F6224716B470370411E9FAA7866AD049 (void);
// 0x000006E2 System.Void Obi.ObiRope/<>c::.ctor()
extern void U3CU3Ec__ctor_m4B4378CC0AB345511F93C07572A6368778B3AE8E (void);
// 0x000006E3 System.Int32 Obi.ObiRope/<>c::<ApplyTearing>b__61_0(Obi.ObiStructuralElement,Obi.ObiStructuralElement)
extern void U3CU3Ec_U3CApplyTearingU3Eb__61_0_mB850523B03651A79FDF5AA49504936E26D51D959 (void);
// 0x000006E4 System.Void Obi.ObiRopeBase::add_OnElementsGenerated(Obi.ObiActor/ActorCallback)
extern void ObiRopeBase_add_OnElementsGenerated_mD9B02AE3DC85CC5762C7AD9F125360E17E1566C8 (void);
// 0x000006E5 System.Void Obi.ObiRopeBase::remove_OnElementsGenerated(Obi.ObiActor/ActorCallback)
extern void ObiRopeBase_remove_OnElementsGenerated_m628F81AC8C51256C7D4EA46CAC6F983413DB27A4 (void);
// 0x000006E6 System.Single Obi.ObiRopeBase::get_restLength()
extern void ObiRopeBase_get_restLength_m7D49C2A8AB446A1D437FE693B1288E0190B53C0C (void);
// 0x000006E7 Obi.ObiPath Obi.ObiRopeBase::get_path()
extern void ObiRopeBase_get_path_mAB91CC45421724C51566122E9F627241EFC2AD44 (void);
// 0x000006E8 System.Single Obi.ObiRopeBase::CalculateLength()
extern void ObiRopeBase_CalculateLength_mDDE1BCD878BB9B456E2FD2FCF4F04F7CC6839108 (void);
// 0x000006E9 System.Void Obi.ObiRopeBase::RecalculateRestLength()
extern void ObiRopeBase_RecalculateRestLength_m57DADABA77123543C1FA768D1971CBD262AFA6A7 (void);
// 0x000006EA System.Void Obi.ObiRopeBase::RecalculateRestPositions()
extern void ObiRopeBase_RecalculateRestPositions_m9FCDE898B1F9A428A93B793D32430CC20866E3BA (void);
// 0x000006EB System.Void Obi.ObiRopeBase::RebuildElementsFromConstraints()
extern void ObiRopeBase_RebuildElementsFromConstraints_m4820BCBA928B7734C90749DD35DC2159E7DA8488 (void);
// 0x000006EC System.Void Obi.ObiRopeBase::RebuildElementsFromConstraintsInternal()
// 0x000006ED System.Void Obi.ObiRopeBase::RebuildConstraintsFromElements()
extern void ObiRopeBase_RebuildConstraintsFromElements_mFB74A59B9584885713FA06A0B416101549970B9B (void);
// 0x000006EE Obi.ObiStructuralElement Obi.ObiRopeBase::GetElementAt(System.Single,System.Single&)
extern void ObiRopeBase_GetElementAt_m53793A9718C26B5F040CBC3B51080E4563AB6500 (void);
// 0x000006EF System.Void Obi.ObiRopeBase::.ctor()
extern void ObiRopeBase__ctor_mFF02FBF9B1434DE5C413D9A516DA5955BCF1E2BC (void);
// 0x000006F0 System.Void Obi.ObiRopeCursor::set_cursorMu(System.Single)
extern void ObiRopeCursor_set_cursorMu_m022763DC739C25672480EB812DE4BA33BA3DD2D4 (void);
// 0x000006F1 System.Single Obi.ObiRopeCursor::get_cursorMu()
extern void ObiRopeCursor_get_cursorMu_m09C322830636F58E9EF1D9B1B8659D36C80DB568 (void);
// 0x000006F2 System.Void Obi.ObiRopeCursor::set_sourceMu(System.Single)
extern void ObiRopeCursor_set_sourceMu_mB2211CE8E6FCE96B94E33196321BB776A1E40618 (void);
// 0x000006F3 System.Single Obi.ObiRopeCursor::get_sourceMu()
extern void ObiRopeCursor_get_sourceMu_mE49A7218FE50A40864B2A0B3BC9BB51407F6FC79 (void);
// 0x000006F4 Obi.ObiStructuralElement Obi.ObiRopeCursor::get_cursorElement()
extern void ObiRopeCursor_get_cursorElement_mA9BAF8D5547126DF99C3E55E2B39D806D8910A1C (void);
// 0x000006F5 System.Int32 Obi.ObiRopeCursor::get_sourceParticleIndex()
extern void ObiRopeCursor_get_sourceParticleIndex_mF2412A00A51B02B0F3C804A3F0394CF961A81F06 (void);
// 0x000006F6 System.Void Obi.ObiRopeCursor::Awake()
extern void ObiRopeCursor_Awake_m58F08987C4F3D569129ADDED494377D435B9B8F0 (void);
// 0x000006F7 System.Void Obi.ObiRopeCursor::Actor_OnElementsGenerated(Obi.ObiActor)
extern void ObiRopeCursor_Actor_OnElementsGenerated_mB1F6111BCA3CDA17D0127D564C035759B3682147 (void);
// 0x000006F8 System.Void Obi.ObiRopeCursor::OnDestroy()
extern void ObiRopeCursor_OnDestroy_m7CABECB180A9792C501341E4FFE539C5AF8AEBE2 (void);
// 0x000006F9 System.Void Obi.ObiRopeCursor::UpdateCursor()
extern void ObiRopeCursor_UpdateCursor_m63E06723DAE8EDCEA5FCF7A2E8AA1838ADDB926A (void);
// 0x000006FA System.Void Obi.ObiRopeCursor::UpdateSource()
extern void ObiRopeCursor_UpdateSource_mED8CE6E7CCA321A83BD2320542029B58B6FF13E8 (void);
// 0x000006FB System.Int32 Obi.ObiRopeCursor::AddParticleAt(System.Int32)
extern void ObiRopeCursor_AddParticleAt_m18D67195977538581D4587C98BD24DE0C632BDF7 (void);
// 0x000006FC System.Void Obi.ObiRopeCursor::RemoveParticleAt(System.Int32)
extern void ObiRopeCursor_RemoveParticleAt_m019EE0305C73EB9E85D2046E128167B70D879B65 (void);
// 0x000006FD System.Void Obi.ObiRopeCursor::ChangeLength(System.Single)
extern void ObiRopeCursor_ChangeLength_m10B57C115599C5E27867F6787E5824D68F86E61A (void);
// 0x000006FE System.Void Obi.ObiRopeCursor::.ctor()
extern void ObiRopeCursor__ctor_mB7E00FFC6CF7FE3731CF31FC87F2DCD6B22AF7EA (void);
// 0x000006FF Obi.ObiBone/IgnoredBone Obi.ObiBoneBlueprint::GetIgnoredBone(UnityEngine.Transform)
extern void ObiBoneBlueprint_GetIgnoredBone_m0C8F53B2CDAEA8363812DDA4135084D0954C3D04 (void);
// 0x00000700 System.Collections.IEnumerator Obi.ObiBoneBlueprint::Initialize()
extern void ObiBoneBlueprint_Initialize_m2A9787CD6FC33DBD5B198E7D667BE12DE690C382 (void);
// 0x00000701 System.Void Obi.ObiBoneBlueprint::CreateSimplices()
extern void ObiBoneBlueprint_CreateSimplices_m6481927058B5F1BB3AD6BCAFF6C39C247704E87F (void);
// 0x00000702 System.Collections.IEnumerator Obi.ObiBoneBlueprint::CreateStretchShearConstraints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ObiBoneBlueprint_CreateStretchShearConstraints_m20DA40DA29C69B617A52422B5084F44019F99604 (void);
// 0x00000703 System.Collections.IEnumerator Obi.ObiBoneBlueprint::CreateBendTwistConstraints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ObiBoneBlueprint_CreateBendTwistConstraints_m61BD7C27BF0655EA303B4E58C0B98D04B424D6B5 (void);
// 0x00000704 System.Collections.IEnumerator Obi.ObiBoneBlueprint::CreateSkinConstraints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ObiBoneBlueprint_CreateSkinConstraints_mE42F073A01A3AD9EAD21A01B6FB490F178E695CB (void);
// 0x00000705 System.Void Obi.ObiBoneBlueprint::.ctor()
extern void ObiBoneBlueprint__ctor_m7E4730BB7CB95139E82325CAC84CEEAF3A71C4DA (void);
// 0x00000706 System.Void Obi.ObiBoneBlueprint/<Initialize>d__14::.ctor(System.Int32)
extern void U3CInitializeU3Ed__14__ctor_m27140A7DF8EF3E999F1FC723535A04C964525F82 (void);
// 0x00000707 System.Void Obi.ObiBoneBlueprint/<Initialize>d__14::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__14_System_IDisposable_Dispose_m88FFDD6CFBB11319DDBBB0F8B34089B80C4F0F01 (void);
// 0x00000708 System.Boolean Obi.ObiBoneBlueprint/<Initialize>d__14::MoveNext()
extern void U3CInitializeU3Ed__14_MoveNext_m81A96994CEF377D7F2D99824DD3B417A0BC82849 (void);
// 0x00000709 System.Object Obi.ObiBoneBlueprint/<Initialize>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC52279203D514C26549053545D8AF9B41AEB60F (void);
// 0x0000070A System.Void Obi.ObiBoneBlueprint/<Initialize>d__14::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__14_System_Collections_IEnumerator_Reset_m9EC41DFD1D0FFE3D465A64CEE388F88EA13B2D0B (void);
// 0x0000070B System.Object Obi.ObiBoneBlueprint/<Initialize>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__14_System_Collections_IEnumerator_get_Current_m5A676E16757268FF757D0DC38F8AD1E8EDF2EEA4 (void);
// 0x0000070C System.Void Obi.ObiBoneBlueprint/<CreateStretchShearConstraints>d__16::.ctor(System.Int32)
extern void U3CCreateStretchShearConstraintsU3Ed__16__ctor_mC9883EA29286F581B5BE6546F53E1803A36FC572 (void);
// 0x0000070D System.Void Obi.ObiBoneBlueprint/<CreateStretchShearConstraints>d__16::System.IDisposable.Dispose()
extern void U3CCreateStretchShearConstraintsU3Ed__16_System_IDisposable_Dispose_m6F122F292C0A6014F0DFBAAD6E22DFEFEF7F74F8 (void);
// 0x0000070E System.Boolean Obi.ObiBoneBlueprint/<CreateStretchShearConstraints>d__16::MoveNext()
extern void U3CCreateStretchShearConstraintsU3Ed__16_MoveNext_m73750DBF37C0962CF3B97EA763465CB3D2590931 (void);
// 0x0000070F System.Object Obi.ObiBoneBlueprint/<CreateStretchShearConstraints>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C73E4EA2DE56F4924F1F4D440D8DEBA59271AD2 (void);
// 0x00000710 System.Void Obi.ObiBoneBlueprint/<CreateStretchShearConstraints>d__16::System.Collections.IEnumerator.Reset()
extern void U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_IEnumerator_Reset_m622BC462DD1E9B4018F1F51A846510E62CECDA84 (void);
// 0x00000711 System.Object Obi.ObiBoneBlueprint/<CreateStretchShearConstraints>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_IEnumerator_get_Current_m4A178A0000B1F11C4A79E52395DDC3CAB3B647C8 (void);
// 0x00000712 System.Void Obi.ObiBoneBlueprint/<CreateBendTwistConstraints>d__17::.ctor(System.Int32)
extern void U3CCreateBendTwistConstraintsU3Ed__17__ctor_m97129288678462023812E6355E3F1BEB10871686 (void);
// 0x00000713 System.Void Obi.ObiBoneBlueprint/<CreateBendTwistConstraints>d__17::System.IDisposable.Dispose()
extern void U3CCreateBendTwistConstraintsU3Ed__17_System_IDisposable_Dispose_m7FA46F1AE20471FF759E6AD807D8CFD6E46DFD2A (void);
// 0x00000714 System.Boolean Obi.ObiBoneBlueprint/<CreateBendTwistConstraints>d__17::MoveNext()
extern void U3CCreateBendTwistConstraintsU3Ed__17_MoveNext_mCA47FFAA72F9266F9F71AF9F3D9FA22EB1E60E54 (void);
// 0x00000715 System.Object Obi.ObiBoneBlueprint/<CreateBendTwistConstraints>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB14225394B51F4DE1158DFBBFD03146485847A8 (void);
// 0x00000716 System.Void Obi.ObiBoneBlueprint/<CreateBendTwistConstraints>d__17::System.Collections.IEnumerator.Reset()
extern void U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_IEnumerator_Reset_mE9692B1811A0104396D417403B04677B7C8903DD (void);
// 0x00000717 System.Object Obi.ObiBoneBlueprint/<CreateBendTwistConstraints>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_IEnumerator_get_Current_mACB8F571A51435DCA534D455C05DEC2BCBD56BBA (void);
// 0x00000718 System.Void Obi.ObiBoneBlueprint/<CreateSkinConstraints>d__18::.ctor(System.Int32)
extern void U3CCreateSkinConstraintsU3Ed__18__ctor_m01559B1A25470AEFB06D7A19008967898DB8A6FA (void);
// 0x00000719 System.Void Obi.ObiBoneBlueprint/<CreateSkinConstraints>d__18::System.IDisposable.Dispose()
extern void U3CCreateSkinConstraintsU3Ed__18_System_IDisposable_Dispose_m13FC3CA635500255354816726C2A0334D420AAA0 (void);
// 0x0000071A System.Boolean Obi.ObiBoneBlueprint/<CreateSkinConstraints>d__18::MoveNext()
extern void U3CCreateSkinConstraintsU3Ed__18_MoveNext_m1F87A21158EA0BF7D8F342934552AFD30CC86417 (void);
// 0x0000071B System.Object Obi.ObiBoneBlueprint/<CreateSkinConstraints>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateSkinConstraintsU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m025792FC9DCA9844F1948AAF26303D5DC3941422 (void);
// 0x0000071C System.Void Obi.ObiBoneBlueprint/<CreateSkinConstraints>d__18::System.Collections.IEnumerator.Reset()
extern void U3CCreateSkinConstraintsU3Ed__18_System_Collections_IEnumerator_Reset_m989EDC08F01688A58194BA6518DE7A3D9F10934A (void);
// 0x0000071D System.Object Obi.ObiBoneBlueprint/<CreateSkinConstraints>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CCreateSkinConstraintsU3Ed__18_System_Collections_IEnumerator_get_Current_mC414552ED2BBDCAA4A581F8CA9BC1CCAC78D9B7F (void);
// 0x0000071E System.Collections.IEnumerator Obi.ObiRodBlueprint::Initialize()
extern void ObiRodBlueprint_Initialize_mB0D5422A4133FF08FC0F52DA417299A628B98B26 (void);
// 0x0000071F System.Collections.IEnumerator Obi.ObiRodBlueprint::CreateStretchShearConstraints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ObiRodBlueprint_CreateStretchShearConstraints_m9413EBB5F95ABFD44A15EDF6DA9CC67830A98C6A (void);
// 0x00000720 System.Collections.IEnumerator Obi.ObiRodBlueprint::CreateBendTwistConstraints()
extern void ObiRodBlueprint_CreateBendTwistConstraints_m5F1A04D6C343F07E764A8641AA54EF2EEB2162E8 (void);
// 0x00000721 System.Collections.IEnumerator Obi.ObiRodBlueprint::CreateChainConstraints()
extern void ObiRodBlueprint_CreateChainConstraints_m665ECE23FA005CB35338951024B674EB2172B5EF (void);
// 0x00000722 System.Void Obi.ObiRodBlueprint::.ctor()
extern void ObiRodBlueprint__ctor_m23C27EE3D33A28E5D542EE7898FBBCCA0E06FF00 (void);
// 0x00000723 System.Void Obi.ObiRodBlueprint/<Initialize>d__3::.ctor(System.Int32)
extern void U3CInitializeU3Ed__3__ctor_m41BDF4E1CF1181A4B67AA1298FF91C848705149D (void);
// 0x00000724 System.Void Obi.ObiRodBlueprint/<Initialize>d__3::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__3_System_IDisposable_Dispose_mAAB9B58959E17D6316D0376867E1ADEA76D9F1EC (void);
// 0x00000725 System.Boolean Obi.ObiRodBlueprint/<Initialize>d__3::MoveNext()
extern void U3CInitializeU3Ed__3_MoveNext_mF4E8C487EB520B7BB8AC9C68DC554D6ABB40D2B4 (void);
// 0x00000726 System.Object Obi.ObiRodBlueprint/<Initialize>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD199EE52973224E0C633E71C904164111168B9B7 (void);
// 0x00000727 System.Void Obi.ObiRodBlueprint/<Initialize>d__3::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__3_System_Collections_IEnumerator_Reset_m70CE6B4088FBF17D28720288E2F251679B22BD11 (void);
// 0x00000728 System.Object Obi.ObiRodBlueprint/<Initialize>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__3_System_Collections_IEnumerator_get_Current_m6B81FD82D37E5ED58B0B639232283B9902ED448B (void);
// 0x00000729 System.Void Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::.ctor(System.Int32)
extern void U3CCreateStretchShearConstraintsU3Ed__4__ctor_mDFFBA8618ED30102037B3C56180A9BEA9487C330 (void);
// 0x0000072A System.Void Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::System.IDisposable.Dispose()
extern void U3CCreateStretchShearConstraintsU3Ed__4_System_IDisposable_Dispose_m89DF329DFEB0C57A2E9159B524D238E75D784621 (void);
// 0x0000072B System.Boolean Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::MoveNext()
extern void U3CCreateStretchShearConstraintsU3Ed__4_MoveNext_m35DE1D56E478B7D4BBA5ECF95C387263211AADF8 (void);
// 0x0000072C System.Object Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48490361DF35F3911FA88CC9E8488B9976632C81 (void);
// 0x0000072D System.Void Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m3EC8612B472DCDC0A5BA07CF265742F7ADB074CC (void);
// 0x0000072E System.Object Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_mDF13C68A7A9199168F5434C5FDAE03DECED69AD9 (void);
// 0x0000072F System.Void Obi.ObiRodBlueprint/<CreateBendTwistConstraints>d__5::.ctor(System.Int32)
extern void U3CCreateBendTwistConstraintsU3Ed__5__ctor_m74EC6A8D57860822339D5CCF9A93DA20335836FD (void);
// 0x00000730 System.Void Obi.ObiRodBlueprint/<CreateBendTwistConstraints>d__5::System.IDisposable.Dispose()
extern void U3CCreateBendTwistConstraintsU3Ed__5_System_IDisposable_Dispose_mCD67BAFE6581BBC3F6B167D165F996E217FA17A5 (void);
// 0x00000731 System.Boolean Obi.ObiRodBlueprint/<CreateBendTwistConstraints>d__5::MoveNext()
extern void U3CCreateBendTwistConstraintsU3Ed__5_MoveNext_m8A6621E1C88EE695336ED524D65C9C7A3FE7A932 (void);
// 0x00000732 System.Object Obi.ObiRodBlueprint/<CreateBendTwistConstraints>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE73B33CF3970D7E2EC7E22CC25D72B5F318EEBD0 (void);
// 0x00000733 System.Void Obi.ObiRodBlueprint/<CreateBendTwistConstraints>d__5::System.Collections.IEnumerator.Reset()
extern void U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_IEnumerator_Reset_m334DE000C0E71E3904B0922ACE92B45250B40DAF (void);
// 0x00000734 System.Object Obi.ObiRodBlueprint/<CreateBendTwistConstraints>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_IEnumerator_get_Current_mDAD52D5C97FC4B1965C20CF8C10650EF73423A09 (void);
// 0x00000735 System.Void Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::.ctor(System.Int32)
extern void U3CCreateChainConstraintsU3Ed__6__ctor_mCCD0039AE63FA7C9AF769CB6DA517174CFE80C80 (void);
// 0x00000736 System.Void Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::System.IDisposable.Dispose()
extern void U3CCreateChainConstraintsU3Ed__6_System_IDisposable_Dispose_m8D90A9C0F15B87BFEE0F460C6F3DA96A7595273C (void);
// 0x00000737 System.Boolean Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::MoveNext()
extern void U3CCreateChainConstraintsU3Ed__6_MoveNext_m1D5D41CD0B98482AACC5F3425B74A62581E06E4A (void);
// 0x00000738 System.Object Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateChainConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m15E671F86EEE570870E5A45BF66B72D282E1CBD5 (void);
// 0x00000739 System.Void Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_m394694E12A8EAEEB4EA80649BEA9BBE861C306FB (void);
// 0x0000073A System.Object Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_mC6B36F52F2460C2F7E972D940E2F69B588832FED (void);
// 0x0000073B System.Collections.IEnumerator Obi.ObiRopeBlueprint::Initialize()
extern void ObiRopeBlueprint_Initialize_m253C93BBFF0249E6F38C1B014E0FC4D6B0263F30 (void);
// 0x0000073C System.Collections.IEnumerator Obi.ObiRopeBlueprint::CreateDistanceConstraints()
extern void ObiRopeBlueprint_CreateDistanceConstraints_m0179DE68BDE1A682413705118D3034FE4070A492 (void);
// 0x0000073D System.Collections.IEnumerator Obi.ObiRopeBlueprint::CreateBendingConstraints()
extern void ObiRopeBlueprint_CreateBendingConstraints_m38CAD4B112A942FDE1242E78797329EBF7F0F468 (void);
// 0x0000073E System.Void Obi.ObiRopeBlueprint::.ctor()
extern void ObiRopeBlueprint__ctor_mF24D397BFF2093F7F4DE1D9F82C2223E408DC5D5 (void);
// 0x0000073F System.Void Obi.ObiRopeBlueprint/<Initialize>d__2::.ctor(System.Int32)
extern void U3CInitializeU3Ed__2__ctor_mE9943407AC4BCDD21A5F1BB4759F285BAE447D8D (void);
// 0x00000740 System.Void Obi.ObiRopeBlueprint/<Initialize>d__2::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__2_System_IDisposable_Dispose_m97A6F5247B0287130D582F2BB4E544062A689B13 (void);
// 0x00000741 System.Boolean Obi.ObiRopeBlueprint/<Initialize>d__2::MoveNext()
extern void U3CInitializeU3Ed__2_MoveNext_m0A32F555A52037AFECFE0D01A86660C18222209F (void);
// 0x00000742 System.Object Obi.ObiRopeBlueprint/<Initialize>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAF2CC89A62774D134AF088ADE0119C979EB1F72 (void);
// 0x00000743 System.Void Obi.ObiRopeBlueprint/<Initialize>d__2::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m7A4B24373876BDE2453048631F2B963E386E7D12 (void);
// 0x00000744 System.Object Obi.ObiRopeBlueprint/<Initialize>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m16BDC9B0E8C7DB5818965339EC2F1EFEFEF37DD9 (void);
// 0x00000745 System.Void Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::.ctor(System.Int32)
extern void U3CCreateDistanceConstraintsU3Ed__3__ctor_mC2881BA9A2C3B5FFD584032842FC4D8016768534 (void);
// 0x00000746 System.Void Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::System.IDisposable.Dispose()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_m1CE3F312F7B66A397DE190A3983DA6CB907431EC (void);
// 0x00000747 System.Boolean Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::MoveNext()
extern void U3CCreateDistanceConstraintsU3Ed__3_MoveNext_mD9AAE5B282EF03644F559C1BC11856CE97E7F88A (void);
// 0x00000748 System.Object Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74A1152B6135923B3BAACBAC1F2F5932512D8046 (void);
// 0x00000749 System.Void Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::System.Collections.IEnumerator.Reset()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mD6AC7761E802DE2F6F0CADB0A5682051E6E4669C (void);
// 0x0000074A System.Object Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8EF851B496CA7FD1CC7F31747EEF8484A7FCBB15 (void);
// 0x0000074B System.Void Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::.ctor(System.Int32)
extern void U3CCreateBendingConstraintsU3Ed__4__ctor_mF09A262B24850675B97784C745083E957AC70DDB (void);
// 0x0000074C System.Void Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::System.IDisposable.Dispose()
extern void U3CCreateBendingConstraintsU3Ed__4_System_IDisposable_Dispose_m1ACE661B7DC059891776A5AD8632DD995321EBA3 (void);
// 0x0000074D System.Boolean Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::MoveNext()
extern void U3CCreateBendingConstraintsU3Ed__4_MoveNext_mBF7AB4425E651E4E8AE9B8FF0406A4DE65085F8E (void);
// 0x0000074E System.Object Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateBendingConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD5D1A5BD31642785B698F9B9734EBE8775F45FA6 (void);
// 0x0000074F System.Void Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m8B106EC62D22296800CE67482EE3F81810C06907 (void);
// 0x00000750 System.Object Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m9475B499E2444C3687CB7A86ADA02EFA9627693A (void);
// 0x00000751 System.Single Obi.ObiRopeBlueprintBase::get_interParticleDistance()
extern void ObiRopeBlueprintBase_get_interParticleDistance_m81CE9A357F6D263A01ACEE7B9132C261D8CE4DB4 (void);
// 0x00000752 System.Single Obi.ObiRopeBlueprintBase::get_restLength()
extern void ObiRopeBlueprintBase_get_restLength_m72D335E89BFE161985036DCB2531C82C35C71FE0 (void);
// 0x00000753 System.Void Obi.ObiRopeBlueprintBase::OnEnable()
extern void ObiRopeBlueprintBase_OnEnable_m787B9D3FA1853A5AECF34A73E56E853B0007D298 (void);
// 0x00000754 System.Void Obi.ObiRopeBlueprintBase::OnDisable()
extern void ObiRopeBlueprintBase_OnDisable_m6A820811B66A7E2EE024185C0C50FC5A39F03D28 (void);
// 0x00000755 System.Void Obi.ObiRopeBlueprintBase::ControlPointAdded(System.Int32)
extern void ObiRopeBlueprintBase_ControlPointAdded_m8C4A8D8F9D775A1CDD82F6441C825A24C016EE48 (void);
// 0x00000756 System.Void Obi.ObiRopeBlueprintBase::ControlPointRenamed(System.Int32)
extern void ObiRopeBlueprintBase_ControlPointRenamed_m3BACB0CD0C836BF1D12A67F66E6DCECCA03EDE18 (void);
// 0x00000757 System.Void Obi.ObiRopeBlueprintBase::ControlPointRemoved(System.Int32)
extern void ObiRopeBlueprintBase_ControlPointRemoved_mE7FA0A89190C4005F244D75BE7A05676B2B7BAC9 (void);
// 0x00000758 System.Void Obi.ObiRopeBlueprintBase::CreateSimplices(System.Int32)
extern void ObiRopeBlueprintBase_CreateSimplices_mB064904BA09A240A79F509214EDD53AF206F9FDD (void);
// 0x00000759 System.Collections.IEnumerator Obi.ObiRopeBlueprintBase::Initialize()
extern void ObiRopeBlueprintBase_Initialize_m49A4560B16A115658987C79F148B7D9B9983A5A9 (void);
// 0x0000075A System.Void Obi.ObiRopeBlueprintBase::.ctor()
extern void ObiRopeBlueprintBase__ctor_m3A167140A2569F4196A84492072FF673189C1C4C (void);
// 0x0000075B System.Void Obi.ObiRopeBlueprintBase/<Initialize>d__17::.ctor(System.Int32)
extern void U3CInitializeU3Ed__17__ctor_m52F67707D008A9F9B010B0538ECEE7A5CC69F2EB (void);
// 0x0000075C System.Void Obi.ObiRopeBlueprintBase/<Initialize>d__17::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__17_System_IDisposable_Dispose_mF0B5E8B1440934FC4C19F4F949EACFE581448839 (void);
// 0x0000075D System.Boolean Obi.ObiRopeBlueprintBase/<Initialize>d__17::MoveNext()
extern void U3CInitializeU3Ed__17_MoveNext_m22EAFC8213CAC2C43D4012E007F6CE8431DD155D (void);
// 0x0000075E System.Object Obi.ObiRopeBlueprintBase/<Initialize>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DD4C0990CD4B35386F56E79FF03AA864B1D6C21 (void);
// 0x0000075F System.Void Obi.ObiRopeBlueprintBase/<Initialize>d__17::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__17_System_Collections_IEnumerator_Reset_m28AA9E89D0D2B4BD55BE44CC152105CAB3FE46A7 (void);
// 0x00000760 System.Object Obi.ObiRopeBlueprintBase/<Initialize>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__17_System_Collections_IEnumerator_get_Current_mC609C587F1D824ADC1D1CD66C9D51AB95F222BAA (void);
// 0x00000761 System.Int32 Obi.ObiRopeSection::get_Segments()
extern void ObiRopeSection_get_Segments_m0F3B7788562D36702926C94384D7A2512C5BA8AA (void);
// 0x00000762 System.Void Obi.ObiRopeSection::OnEnable()
extern void ObiRopeSection_OnEnable_m72E432ADC85EFCB53ADF526067B2DDFAA41EF4D8 (void);
// 0x00000763 System.Void Obi.ObiRopeSection::CirclePreset(System.Int32)
extern void ObiRopeSection_CirclePreset_mF0CFC0E074EF29DABDB6F8DA3C92F4B90E8040F0 (void);
// 0x00000764 System.Int32 Obi.ObiRopeSection::SnapTo(System.Single,System.Int32,System.Int32)
extern void ObiRopeSection_SnapTo_mB8FEAAFD26343973EF029E6F95E8949E7B99177D (void);
// 0x00000765 System.Void Obi.ObiRopeSection::.ctor()
extern void ObiRopeSection__ctor_mB4EFF529E32758C69BBE6BEB984CA992F52BFB1A (void);
// 0x00000766 System.Void Obi.ObiStructuralElement::.ctor()
extern void ObiStructuralElement__ctor_m0DBA768A6C7DCF64B0CA06942AFBBC9FAC656088 (void);
// 0x00000767 System.Void Obi.ObiColorDataChannel::.ctor()
extern void ObiColorDataChannel__ctor_mF7C3988C6EDFA5576F0271264869AF1673DAE555 (void);
// 0x00000768 System.Void Obi.ObiPhaseDataChannel::.ctor()
extern void ObiPhaseDataChannel__ctor_m59B976ADAC7A5050F6B898A4D7B3443C2B42663D (void);
// 0x00000769 System.Void Obi.ObiMassDataChannel::.ctor()
extern void ObiMassDataChannel__ctor_mB2FA019EFE8667AF089AB5CE4B174C2A7CAEC504 (void);
// 0x0000076A System.Void Obi.ObiNormalDataChannel::.ctor()
extern void ObiNormalDataChannel__ctor_mFE5B22AE6C83DC020B2BFD7B7C2A10C87DDCDE1B (void);
// 0x0000076B System.Int32 Obi.IObiPathDataChannel::get_Count()
// 0x0000076C System.Boolean Obi.IObiPathDataChannel::get_Dirty()
// 0x0000076D System.Void Obi.IObiPathDataChannel::Clean()
// 0x0000076E System.Void Obi.IObiPathDataChannel::RemoveAt(System.Int32)
// 0x0000076F System.Int32 Obi.ObiPathDataChannel`2::get_Count()
// 0x00000770 System.Boolean Obi.ObiPathDataChannel`2::get_Dirty()
// 0x00000771 System.Void Obi.ObiPathDataChannel`2::Clean()
// 0x00000772 System.Void Obi.ObiPathDataChannel`2::.ctor(Obi.ObiInterpolator`1<U>)
// 0x00000773 T Obi.ObiPathDataChannel`2::get_Item(System.Int32)
// 0x00000774 System.Void Obi.ObiPathDataChannel`2::set_Item(System.Int32,T)
// 0x00000775 System.Void Obi.ObiPathDataChannel`2::RemoveAt(System.Int32)
// 0x00000776 U Obi.ObiPathDataChannel`2::Evaluate(U,U,U,U,System.Single)
// 0x00000777 U Obi.ObiPathDataChannel`2::EvaluateFirstDerivative(U,U,U,U,System.Single)
// 0x00000778 U Obi.ObiPathDataChannel`2::EvaluateSecondDerivative(U,U,U,U,System.Single)
// 0x00000779 System.Int32 Obi.ObiPathDataChannel`2::GetSpanCount(System.Boolean)
// 0x0000077A System.Int32 Obi.ObiPathDataChannel`2::GetSpanControlPointAtMu(System.Boolean,System.Single,System.Single&)
// 0x0000077B System.Void Obi.ObiPathDataChannelIdentity`1::.ctor(Obi.ObiInterpolator`1<T>)
// 0x0000077C T Obi.ObiPathDataChannelIdentity`1::GetFirstDerivative(System.Int32)
// 0x0000077D T Obi.ObiPathDataChannelIdentity`1::GetSecondDerivative(System.Int32)
// 0x0000077E T Obi.ObiPathDataChannelIdentity`1::GetAtMu(System.Boolean,System.Single)
// 0x0000077F T Obi.ObiPathDataChannelIdentity`1::GetFirstDerivativeAtMu(System.Boolean,System.Single)
// 0x00000780 T Obi.ObiPathDataChannelIdentity`1::GetSecondDerivativeAtMu(System.Boolean,System.Single)
// 0x00000781 System.Void Obi.ObiPointsDataChannel::.ctor()
extern void ObiPointsDataChannel__ctor_m0E32942FBF0EFCFCA256E7FC1DF684B7C8EF7BB3 (void);
// 0x00000782 UnityEngine.Vector3 Obi.ObiPointsDataChannel::GetTangent(System.Int32)
extern void ObiPointsDataChannel_GetTangent_m672DE54B725D53690F1E830D2AFFD37EDC4400FD (void);
// 0x00000783 UnityEngine.Vector3 Obi.ObiPointsDataChannel::GetAcceleration(System.Int32)
extern void ObiPointsDataChannel_GetAcceleration_mF055C83DA7E61032FA54EDD296B23E930021C54C (void);
// 0x00000784 UnityEngine.Vector3 Obi.ObiPointsDataChannel::GetPositionAtMu(System.Boolean,System.Single)
extern void ObiPointsDataChannel_GetPositionAtMu_m5DA1AF5F4A0C5EB6EDAE4AA729DAC149E3E17DF5 (void);
// 0x00000785 UnityEngine.Vector3 Obi.ObiPointsDataChannel::GetTangentAtMu(System.Boolean,System.Single)
extern void ObiPointsDataChannel_GetTangentAtMu_mFBCBAB990A9552E85C983C74226D8C2FF4DD5315 (void);
// 0x00000786 UnityEngine.Vector3 Obi.ObiPointsDataChannel::GetAccelerationAtMu(System.Boolean,System.Single)
extern void ObiPointsDataChannel_GetAccelerationAtMu_m11001BB62AD2418364B3C5F6E3FBB73D56A02E8B (void);
// 0x00000787 System.Void Obi.ObiRotationalMassDataChannel::.ctor()
extern void ObiRotationalMassDataChannel__ctor_m0AEEA1343FEF24CF70B782C9A6648BEE0B61663C (void);
// 0x00000788 System.Void Obi.ObiThicknessDataChannel::.ctor()
extern void ObiThicknessDataChannel__ctor_mF56BA457503E1D6B5E1E543974E750CF19969946 (void);
// 0x00000789 System.Single Obi.ObiCatmullRomInterpolator::Evaluate(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void ObiCatmullRomInterpolator_Evaluate_mFE52C9F643FDE2424B49500063A4084B2A4C2690 (void);
// 0x0000078A System.Single Obi.ObiCatmullRomInterpolator::EvaluateFirstDerivative(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void ObiCatmullRomInterpolator_EvaluateFirstDerivative_mBD404A2D620B41F2FF54DADC0174A06686344B4D (void);
// 0x0000078B System.Single Obi.ObiCatmullRomInterpolator::EvaluateSecondDerivative(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void ObiCatmullRomInterpolator_EvaluateSecondDerivative_m7BDDA2C407FE631C7947DC09B5E1DDE3CCF783AF (void);
// 0x0000078C System.Void Obi.ObiCatmullRomInterpolator::.ctor()
extern void ObiCatmullRomInterpolator__ctor_m3E37199D2CFE50A52C5A9CA7FD180BE3B876FBD5 (void);
// 0x0000078D UnityEngine.Vector3 Obi.ObiCatmullRomInterpolator3D::Evaluate(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ObiCatmullRomInterpolator3D_Evaluate_m0E59C94236EB4852BF72D9345C8CBD50304C1EAF (void);
// 0x0000078E UnityEngine.Vector3 Obi.ObiCatmullRomInterpolator3D::EvaluateFirstDerivative(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ObiCatmullRomInterpolator3D_EvaluateFirstDerivative_mE0801CDF9845DDE32858B014A36B1FABDE0F6E31 (void);
// 0x0000078F UnityEngine.Vector3 Obi.ObiCatmullRomInterpolator3D::EvaluateSecondDerivative(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ObiCatmullRomInterpolator3D_EvaluateSecondDerivative_mF539CA500C3944D61F6387AF19F393C5F70DD5E8 (void);
// 0x00000790 System.Void Obi.ObiCatmullRomInterpolator3D::.ctor()
extern void ObiCatmullRomInterpolator3D__ctor_m26DF84ABFA78D87532ABD407FF68239528022177 (void);
// 0x00000791 UnityEngine.Color Obi.ObiColorInterpolator3D::Evaluate(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void ObiColorInterpolator3D_Evaluate_m35B7EA4CB54DA824576F42337D5233EDC7BAB4AB (void);
// 0x00000792 UnityEngine.Color Obi.ObiColorInterpolator3D::EvaluateFirstDerivative(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void ObiColorInterpolator3D_EvaluateFirstDerivative_m54335AD741451FB23370BCA6B56037620B1705FB (void);
// 0x00000793 UnityEngine.Color Obi.ObiColorInterpolator3D::EvaluateSecondDerivative(UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,UnityEngine.Color,System.Single)
extern void ObiColorInterpolator3D_EvaluateSecondDerivative_mC8E7322FFED86AB60DE8A989362696C39F321C73 (void);
// 0x00000794 System.Void Obi.ObiColorInterpolator3D::.ctor()
extern void ObiColorInterpolator3D__ctor_m3D3CCBEC0E7298A1339CA213F361713DEB3A53A0 (void);
// 0x00000795 System.Int32 Obi.ObiConstantInterpolator::Evaluate(System.Int32,System.Int32,System.Int32,System.Int32,System.Single)
extern void ObiConstantInterpolator_Evaluate_m785DA19E7352DFFE33F1469F63CF75ACB14C76E8 (void);
// 0x00000796 System.Int32 Obi.ObiConstantInterpolator::EvaluateFirstDerivative(System.Int32,System.Int32,System.Int32,System.Int32,System.Single)
extern void ObiConstantInterpolator_EvaluateFirstDerivative_m621BF807F926A21E4CDFD01631278440C25DFDCD (void);
// 0x00000797 System.Int32 Obi.ObiConstantInterpolator::EvaluateSecondDerivative(System.Int32,System.Int32,System.Int32,System.Int32,System.Single)
extern void ObiConstantInterpolator_EvaluateSecondDerivative_m821B9B0376A707CE06B853973110070B49FB4A62 (void);
// 0x00000798 System.Void Obi.ObiConstantInterpolator::.ctor()
extern void ObiConstantInterpolator__ctor_m3A4A87D67512E15BD41B98827A3BF47176AF79CE (void);
// 0x00000799 T Obi.ObiInterpolator`1::Evaluate(T,T,T,T,System.Single)
// 0x0000079A T Obi.ObiInterpolator`1::EvaluateFirstDerivative(T,T,T,T,System.Single)
// 0x0000079B T Obi.ObiInterpolator`1::EvaluateSecondDerivative(T,T,T,T,System.Single)
// 0x0000079C System.Void Obi.PathControlPointEvent::.ctor()
extern void PathControlPointEvent__ctor_m99C8384E8222613B455CE373869A48ED20B34B40 (void);
// 0x0000079D System.Collections.Generic.IEnumerable`1<Obi.IObiPathDataChannel> Obi.ObiPath::GetDataChannels()
extern void ObiPath_GetDataChannels_m971B8134825853C6010D8D02446AB4EEE35CE5C1 (void);
// 0x0000079E Obi.ObiPointsDataChannel Obi.ObiPath::get_points()
extern void ObiPath_get_points_m6F044F0A220D99C2C5184CB5FE1769BAED6FFD3C (void);
// 0x0000079F Obi.ObiNormalDataChannel Obi.ObiPath::get_normals()
extern void ObiPath_get_normals_mD27D9F0689AB44FF831629FC2F0F7F2BC5EA08F6 (void);
// 0x000007A0 Obi.ObiColorDataChannel Obi.ObiPath::get_colors()
extern void ObiPath_get_colors_mB1315A257BB672D26575C3CEFD3FC642D65C8C6F (void);
// 0x000007A1 Obi.ObiThicknessDataChannel Obi.ObiPath::get_thicknesses()
extern void ObiPath_get_thicknesses_m435418A103FD1CC0D151EF3B75689969F0E23C8D (void);
// 0x000007A2 Obi.ObiMassDataChannel Obi.ObiPath::get_masses()
extern void ObiPath_get_masses_m563E313A7734C29F2476B68228BAB71731EBC326 (void);
// 0x000007A3 Obi.ObiRotationalMassDataChannel Obi.ObiPath::get_rotationalMasses()
extern void ObiPath_get_rotationalMasses_m65A0959B14AEBD424FC35853DF6EBEE9A4D2648A (void);
// 0x000007A4 Obi.ObiPhaseDataChannel Obi.ObiPath::get_filters()
extern void ObiPath_get_filters_m6D02BA0F0D6E24496882A16B37C12B185D9CC028 (void);
// 0x000007A5 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> Obi.ObiPath::get_ArcLengthTable()
extern void ObiPath_get_ArcLengthTable_m473E96A0299C036CB1835FE04F20733B6D824C81 (void);
// 0x000007A6 System.Single Obi.ObiPath::get_Length()
extern void ObiPath_get_Length_m702A025AC3A2C08D2F9629728753916FA662A7E9 (void);
// 0x000007A7 System.Int32 Obi.ObiPath::get_ArcLengthSamples()
extern void ObiPath_get_ArcLengthSamples_m3C36FC6DDE2FBD76AD4F46EB7C5A17FDC4B1D9BA (void);
// 0x000007A8 System.Int32 Obi.ObiPath::get_ControlPointCount()
extern void ObiPath_get_ControlPointCount_m09869836E934AC2B253907636DDE6D72BA95D05F (void);
// 0x000007A9 System.Boolean Obi.ObiPath::get_Closed()
extern void ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856 (void);
// 0x000007AA System.Void Obi.ObiPath::set_Closed(System.Boolean)
extern void ObiPath_set_Closed_mAA84C7E40584DA3417206E5F3BAF0B9ACB858F19 (void);
// 0x000007AB System.Int32 Obi.ObiPath::GetSpanCount()
extern void ObiPath_GetSpanCount_m09199A6F5BB9A1619E0A8A7FB826FA1472B29623 (void);
// 0x000007AC System.Int32 Obi.ObiPath::GetSpanControlPointForMu(System.Single,System.Single&)
extern void ObiPath_GetSpanControlPointForMu_m7D575E28219797CFC37B35F0219962D3EE81FFB9 (void);
// 0x000007AD System.Int32 Obi.ObiPath::GetClosestControlPointIndex(System.Single)
extern void ObiPath_GetClosestControlPointIndex_mA8A32BE9790A6E266966BF8A2520126E80BACF51 (void);
// 0x000007AE System.Single Obi.ObiPath::GetMuAtLenght(System.Single)
extern void ObiPath_GetMuAtLenght_m9B36DC8C812923123B8D5F9D290BAD46CF555E6B (void);
// 0x000007AF System.Single Obi.ObiPath::RecalculateLenght(UnityEngine.Matrix4x4,System.Single,System.Int32)
extern void ObiPath_RecalculateLenght_mDF1CD450DBBA7B9823BA8090C340595338260C0E (void);
// 0x000007B0 System.Single Obi.ObiPath::GaussLobattoIntegrationStep(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Int32,System.Int32,System.Single)
extern void ObiPath_GaussLobattoIntegrationStep_m49FA0CE206E86C254C4A1A5A6F1E7B3F272560C3 (void);
// 0x000007B1 System.Void Obi.ObiPath::SetName(System.Int32,System.String)
extern void ObiPath_SetName_m6C56DDCAE3B62E48F3F24BF93336D06A33A7DC9E (void);
// 0x000007B2 System.String Obi.ObiPath::GetName(System.Int32)
extern void ObiPath_GetName_mF8D4C50F9267FF07E8507593D59314ABB3691ED1 (void);
// 0x000007B3 System.Void Obi.ObiPath::AddControlPoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Int32,UnityEngine.Color,System.String)
extern void ObiPath_AddControlPoint_m85A6300B52B202EEA3CDB7BEBF90205E2233B041 (void);
// 0x000007B4 System.Void Obi.ObiPath::InsertControlPoint(System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Int32,UnityEngine.Color,System.String)
extern void ObiPath_InsertControlPoint_m978D7BDBC828882C6D3B1C85F029C6B1BC63BDDA (void);
// 0x000007B5 System.Int32 Obi.ObiPath::InsertControlPoint(System.Single)
extern void ObiPath_InsertControlPoint_m4E84171B74A483ECF63FA71B829B6B78372FB02B (void);
// 0x000007B6 System.Void Obi.ObiPath::Clear()
extern void ObiPath_Clear_m672C64B04F5911FF4628FC18C3D3C957EEEB340B (void);
// 0x000007B7 System.Void Obi.ObiPath::RemoveControlPoint(System.Int32)
extern void ObiPath_RemoveControlPoint_m013CC0D417CEC11360D7684269DA273C04951431 (void);
// 0x000007B8 System.Void Obi.ObiPath::FlushEvents()
extern void ObiPath_FlushEvents_mBCD68B863C6A2FE354C2D9D1013F13611B71E003 (void);
// 0x000007B9 System.Void Obi.ObiPath::.ctor()
extern void ObiPath__ctor_m1137C422538A8B28723575D3C358855E737CA02D (void);
// 0x000007BA System.Void Obi.ObiPath/<GetDataChannels>d__17::.ctor(System.Int32)
extern void U3CGetDataChannelsU3Ed__17__ctor_mD0F362BFD59ACE15D4D3A884E8226BF10577B2B1 (void);
// 0x000007BB System.Void Obi.ObiPath/<GetDataChannels>d__17::System.IDisposable.Dispose()
extern void U3CGetDataChannelsU3Ed__17_System_IDisposable_Dispose_m1491BD58F339286AD9852B89147455FFADE4FB35 (void);
// 0x000007BC System.Boolean Obi.ObiPath/<GetDataChannels>d__17::MoveNext()
extern void U3CGetDataChannelsU3Ed__17_MoveNext_mA67F3AF60338CC6FD16F4C888F7770FA03B3ABCE (void);
// 0x000007BD Obi.IObiPathDataChannel Obi.ObiPath/<GetDataChannels>d__17::System.Collections.Generic.IEnumerator<Obi.IObiPathDataChannel>.get_Current()
extern void U3CGetDataChannelsU3Ed__17_System_Collections_Generic_IEnumeratorU3CObi_IObiPathDataChannelU3E_get_Current_mA4EFA4B8901E742332F05D5737EA190D71393707 (void);
// 0x000007BE System.Void Obi.ObiPath/<GetDataChannels>d__17::System.Collections.IEnumerator.Reset()
extern void U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerator_Reset_mEBF818027E97B5B63CBDDC30BE2E6B48F896B7DF (void);
// 0x000007BF System.Object Obi.ObiPath/<GetDataChannels>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerator_get_Current_m88CC0E7304DD9F211246F7A845B040ADB99D4662 (void);
// 0x000007C0 System.Collections.Generic.IEnumerator`1<Obi.IObiPathDataChannel> Obi.ObiPath/<GetDataChannels>d__17::System.Collections.Generic.IEnumerable<Obi.IObiPathDataChannel>.GetEnumerator()
extern void U3CGetDataChannelsU3Ed__17_System_Collections_Generic_IEnumerableU3CObi_IObiPathDataChannelU3E_GetEnumerator_m796DAF5CF8553C1133063B7487009CBB8F6E31B8 (void);
// 0x000007C1 System.Collections.IEnumerator Obi.ObiPath/<GetDataChannels>d__17::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerable_GetEnumerator_mD094D18AC457EA20B62A9CCF2837713884B4A7A5 (void);
// 0x000007C2 System.Void Obi.ObiPathFrame::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector4,System.Single)
extern void ObiPathFrame__ctor_m025EA159EAFA9E15FAD8CF083C002438D6A43641 (void);
// 0x000007C3 System.Void Obi.ObiPathFrame::Reset()
extern void ObiPathFrame_Reset_m6589829D261335ABE1D4733AD2BA58A2E8D5FF15 (void);
// 0x000007C4 Obi.ObiPathFrame Obi.ObiPathFrame::op_Addition(Obi.ObiPathFrame,Obi.ObiPathFrame)
extern void ObiPathFrame_op_Addition_mDDBC2765A80B58557DD24F16754B91B7517D572C (void);
// 0x000007C5 Obi.ObiPathFrame Obi.ObiPathFrame::op_Multiply(System.Single,Obi.ObiPathFrame)
extern void ObiPathFrame_op_Multiply_m84C683E4981AF52397CFF8FC7F845AC6A3F79BB6 (void);
// 0x000007C6 System.Void Obi.ObiPathFrame::WeightedSum(System.Single,System.Single,System.Single,Obi.ObiPathFrame&,Obi.ObiPathFrame&,Obi.ObiPathFrame&,Obi.ObiPathFrame&)
extern void ObiPathFrame_WeightedSum_m4EC5C0E01A31BCCDBBA1B0EB371819FBB4F3F14C (void);
// 0x000007C7 System.Void Obi.ObiPathFrame::SetTwist(System.Single)
extern void ObiPathFrame_SetTwist_m15557FA7BEAF865EC68821EA1CCC3D349855F667 (void);
// 0x000007C8 System.Void Obi.ObiPathFrame::SetTwistAndTangent(System.Single,UnityEngine.Vector3)
extern void ObiPathFrame_SetTwistAndTangent_m820B046082B1FA969D75F517E60F14B6C888B9A2 (void);
// 0x000007C9 System.Void Obi.ObiPathFrame::Transport(Obi.ObiPathFrame,System.Single)
extern void ObiPathFrame_Transport_m30DB5DB5B8D169DF5B54D56BA7B39A776604F769 (void);
// 0x000007CA System.Void Obi.ObiPathFrame::Transport(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ObiPathFrame_Transport_m0B1B3CA7069547BE12B49F7FDB4E67EEADC239D4 (void);
// 0x000007CB System.Void Obi.ObiPathFrame::Transport(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ObiPathFrame_Transport_mE3DC94CBBB61AE7CFC04178FADD5862011E485B0 (void);
// 0x000007CC UnityEngine.Matrix4x4 Obi.ObiPathFrame::ToMatrix(Obi.ObiPathFrame/Axis)
extern void ObiPathFrame_ToMatrix_m3941E65FC0FD3C60411092AAA8CDCF24EEE1E5C5 (void);
// 0x000007CD System.Void Obi.ObiPathFrame::DebugDraw(System.Single)
extern void ObiPathFrame_DebugDraw_m5397213F80C59B56BBA30A19B770A3E88BF6712C (void);
// 0x000007CE System.Void Obi.ObiPathSmoother::add_OnCurveGenerated(Obi.ObiActor/ActorCallback)
extern void ObiPathSmoother_add_OnCurveGenerated_m1612F15B48776929153BBCB1C62F5B1720320755 (void);
// 0x000007CF System.Void Obi.ObiPathSmoother::remove_OnCurveGenerated(Obi.ObiActor/ActorCallback)
extern void ObiPathSmoother_remove_OnCurveGenerated_m0136223598B7485378680ECE56861613BAE13476 (void);
// 0x000007D0 System.Single Obi.ObiPathSmoother::get_SmoothLength()
extern void ObiPathSmoother_get_SmoothLength_m7C8EA10C055F41D716ADAB7A563FB5BBF44808DE (void);
// 0x000007D1 System.Single Obi.ObiPathSmoother::get_SmoothSections()
extern void ObiPathSmoother_get_SmoothSections_mDEDF3D59D8F205E1399B0F48B25881DFAA05F21D (void);
// 0x000007D2 System.Void Obi.ObiPathSmoother::OnEnable()
extern void ObiPathSmoother_OnEnable_m581B0AC297BE743EBB2C7B1E4E428B18B2E69F90 (void);
// 0x000007D3 System.Void Obi.ObiPathSmoother::OnDisable()
extern void ObiPathSmoother_OnDisable_mF9E9D6D7B24926240864E7B42A75F31541765879 (void);
// 0x000007D4 System.Void Obi.ObiPathSmoother::Actor_OnInterpolate(Obi.ObiActor)
extern void ObiPathSmoother_Actor_OnInterpolate_mEC73625A45D9D51F38E00713D03FD6AA4F401339 (void);
// 0x000007D5 System.Void Obi.ObiPathSmoother::AllocateChunk(System.Int32)
extern void ObiPathSmoother_AllocateChunk_m52584A3348E9882935F367F8E976D75A84E39189 (void);
// 0x000007D6 System.Single Obi.ObiPathSmoother::CalculateChunkLength(Obi.ObiList`1<Obi.ObiPathFrame>)
extern void ObiPathSmoother_CalculateChunkLength_m11557F5F54F9B2B489FF80721D6D478FF73BFEEA (void);
// 0x000007D7 System.Void Obi.ObiPathSmoother::AllocateRawChunks(Obi.ObiRopeBase)
extern void ObiPathSmoother_AllocateRawChunks_mD0E6F4F45ADACD8737EF080CEAD40287208BD318 (void);
// 0x000007D8 System.Void Obi.ObiPathSmoother::PathFrameFromParticle(Obi.ObiRopeBase,Obi.ObiPathFrame&,System.Int32,System.Boolean)
extern void ObiPathSmoother_PathFrameFromParticle_m51B6C4C4E523E705AE61BFEE3DAD380C383D2344 (void);
// 0x000007D9 System.Void Obi.ObiPathSmoother::GenerateSmoothChunks(Obi.ObiRopeBase,System.UInt32)
extern void ObiPathSmoother_GenerateSmoothChunks_m924257E4279D037AEDB15E1D78C8A3287DC62AC9 (void);
// 0x000007DA Obi.ObiPathFrame Obi.ObiPathSmoother::GetSectionAt(System.Single)
extern void ObiPathSmoother_GetSectionAt_m2D886BB2898A8849CF2B29DFA59F079FE2B47FF2 (void);
// 0x000007DB System.Boolean Obi.ObiPathSmoother::Decimate(Obi.ObiList`1<Obi.ObiPathFrame>,Obi.ObiList`1<Obi.ObiPathFrame>,System.Single)
extern void ObiPathSmoother_Decimate_mBE4DC0A5377EAF01B6130BA89EB6EA8B00CB8983 (void);
// 0x000007DC System.Void Obi.ObiPathSmoother::Chaikin(Obi.ObiList`1<Obi.ObiPathFrame>,Obi.ObiList`1<Obi.ObiPathFrame>,System.UInt32)
extern void ObiPathSmoother_Chaikin_mAE5894E878D1DB84E10229EEE6C2AE63FC8A90DE (void);
// 0x000007DD System.Void Obi.ObiPathSmoother::.ctor()
extern void ObiPathSmoother__ctor_m661E02B23AEFFCCD7A0ABF2634E8FDD9E80F7DFD (void);
// 0x000007DE System.Void Obi.ObiPathSmoother::.cctor()
extern void ObiPathSmoother__cctor_mABF2352E628EEE6E56ABB615D2DC85067B424E24 (void);
// 0x000007DF UnityEngine.Vector3 Obi.ObiWingedPoint::get_inTangentEndpoint()
extern void ObiWingedPoint_get_inTangentEndpoint_mB04887363665D698ADFB0964437641EBEE625AAB (void);
// 0x000007E0 UnityEngine.Vector3 Obi.ObiWingedPoint::get_outTangentEndpoint()
extern void ObiWingedPoint_get_outTangentEndpoint_m13FD62B607A3335392B17CD3230F7FFDC8D14A0D (void);
// 0x000007E1 System.Void Obi.ObiWingedPoint::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiWingedPoint__ctor_mAD5A99BEA0E2B2C4D6563AB336276D0EFC057EC2 (void);
// 0x000007E2 System.Void Obi.ObiWingedPoint::SetInTangentEndpoint(UnityEngine.Vector3)
extern void ObiWingedPoint_SetInTangentEndpoint_mC89AD4F0A7639ED4F2213166497F9B9B07399039 (void);
// 0x000007E3 System.Void Obi.ObiWingedPoint::SetOutTangentEndpoint(UnityEngine.Vector3)
extern void ObiWingedPoint_SetOutTangentEndpoint_m2F82C9A47C3FF51341B24470191045CE79BB9B9B (void);
// 0x000007E4 System.Void Obi.ObiWingedPoint::Transform(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void ObiWingedPoint_Transform_m59146866606ECDBA07E40D3525E70F59AC5A6BC4 (void);
// 0x000007E5 System.Void Obi.ObiRopeChainRenderer::Awake()
extern void ObiRopeChainRenderer_Awake_m3BB0C150D1E9F76BB6060CEF9CA4D39AD685EDA7 (void);
// 0x000007E6 System.Boolean Obi.ObiRopeChainRenderer::get_RandomizeLinks()
extern void ObiRopeChainRenderer_get_RandomizeLinks_mF00C148739F0B41CB0BE6F52E5163C3704877BD7 (void);
// 0x000007E7 System.Void Obi.ObiRopeChainRenderer::set_RandomizeLinks(System.Boolean)
extern void ObiRopeChainRenderer_set_RandomizeLinks_m995C2895CCFD6B6E2670D4405066C8110D91DE52 (void);
// 0x000007E8 System.Void Obi.ObiRopeChainRenderer::OnEnable()
extern void ObiRopeChainRenderer_OnEnable_mC4FD146269A944E94A17500B5F72A434AFFB10D6 (void);
// 0x000007E9 System.Void Obi.ObiRopeChainRenderer::OnDisable()
extern void ObiRopeChainRenderer_OnDisable_m274BF4B80ECE9854B7FE714FCA648166D1E713DB (void);
// 0x000007EA System.Void Obi.ObiRopeChainRenderer::ClearChainLinkInstances()
extern void ObiRopeChainRenderer_ClearChainLinkInstances_m91C00166579736A08196135BE8467B5084A1A63D (void);
// 0x000007EB System.Void Obi.ObiRopeChainRenderer::CreateChainLinkInstances(Obi.ObiRopeBase)
extern void ObiRopeChainRenderer_CreateChainLinkInstances_m13D114CD5DB8B93E820AD9B71EE407C7099D0FDB (void);
// 0x000007EC System.Void Obi.ObiRopeChainRenderer::UpdateRenderer(Obi.ObiActor)
extern void ObiRopeChainRenderer_UpdateRenderer_m66D06E408B6A3A1826F7C88540D354FEBCA64225 (void);
// 0x000007ED System.Void Obi.ObiRopeChainRenderer::.ctor()
extern void ObiRopeChainRenderer__ctor_m856DF5AF44EE731B5FCD2C17531D730E2C7E1F97 (void);
// 0x000007EE System.Void Obi.ObiRopeChainRenderer::.cctor()
extern void ObiRopeChainRenderer__cctor_m864544CB0188E7CB1C1C319BC872EA95440993B0 (void);
// 0x000007EF System.Void Obi.ObiRopeExtrudedRenderer::OnEnable()
extern void ObiRopeExtrudedRenderer_OnEnable_m2CBE22BF32CEA5D15FB376095ECC9D76839335C1 (void);
// 0x000007F0 System.Void Obi.ObiRopeExtrudedRenderer::OnDisable()
extern void ObiRopeExtrudedRenderer_OnDisable_m1DE835B7E05BAD3282274C6E3CAED96ACD7CE108 (void);
// 0x000007F1 System.Void Obi.ObiRopeExtrudedRenderer::CreateMeshIfNeeded()
extern void ObiRopeExtrudedRenderer_CreateMeshIfNeeded_m53BCE7E556FF5C8027B75A15AC2EB70ABED08CF2 (void);
// 0x000007F2 System.Void Obi.ObiRopeExtrudedRenderer::UpdateRenderer(Obi.ObiActor)
extern void ObiRopeExtrudedRenderer_UpdateRenderer_m30D59CBD734BCC7DD135BB5A4BBAB0F14291BE79 (void);
// 0x000007F3 System.Void Obi.ObiRopeExtrudedRenderer::ClearMeshData()
extern void ObiRopeExtrudedRenderer_ClearMeshData_m1AE1E6DB7365ACF85A024E5071187D2DD4A60A9F (void);
// 0x000007F4 System.Void Obi.ObiRopeExtrudedRenderer::CommitMeshData()
extern void ObiRopeExtrudedRenderer_CommitMeshData_m48FE391A3AD444A343E6C21729825B5A28053E23 (void);
// 0x000007F5 System.Void Obi.ObiRopeExtrudedRenderer::.ctor()
extern void ObiRopeExtrudedRenderer__ctor_mDDCD16FB03BDF41D73831C7665A454C4A011BE6D (void);
// 0x000007F6 System.Void Obi.ObiRopeExtrudedRenderer::.cctor()
extern void ObiRopeExtrudedRenderer__cctor_mBADC91968E87645ADF6F00562C8323E13DD36338 (void);
// 0x000007F7 System.Void Obi.ObiRopeLineRenderer::OnEnable()
extern void ObiRopeLineRenderer_OnEnable_mE43229CC36A78E88C87242842AA884CEBEB19805 (void);
// 0x000007F8 System.Void Obi.ObiRopeLineRenderer::OnDisable()
extern void ObiRopeLineRenderer_OnDisable_m3FFE0911B8FFD320C957480ADEB5353F6159FE6B (void);
// 0x000007F9 System.Void Obi.ObiRopeLineRenderer::CreateMeshIfNeeded()
extern void ObiRopeLineRenderer_CreateMeshIfNeeded_m51BC08D0F383756CAE8790B9A54BF5AA624DEC45 (void);
// 0x000007FA System.Void Obi.ObiRopeLineRenderer::UpdateRenderer(UnityEngine.Camera)
extern void ObiRopeLineRenderer_UpdateRenderer_m6587B55D0CB96E9E7AC8405B962EDA575229D425 (void);
// 0x000007FB System.Void Obi.ObiRopeLineRenderer::ClearMeshData()
extern void ObiRopeLineRenderer_ClearMeshData_mB23CB53F6BB94B1AF8E2288441513BB6B353BAA3 (void);
// 0x000007FC System.Void Obi.ObiRopeLineRenderer::CommitMeshData()
extern void ObiRopeLineRenderer_CommitMeshData_mDD9B2C824B4D6FC36E9446190EEF98A28D770B02 (void);
// 0x000007FD System.Void Obi.ObiRopeLineRenderer::.ctor()
extern void ObiRopeLineRenderer__ctor_m77286C9C570003CE8C5BB5F6AF51CEC1394799FE (void);
// 0x000007FE System.Void Obi.ObiRopeLineRenderer::.cctor()
extern void ObiRopeLineRenderer__cctor_m28AC5331FB155ADD575216CD6DED626398A4FABC (void);
// 0x000007FF System.Void Obi.ObiRopeLineRenderer::<OnEnable>b__15_0(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera)
extern void ObiRopeLineRenderer_U3COnEnableU3Eb__15_0_m6793DE39FBF2171A3AF9F73DC1442DCC302C8694 (void);
// 0x00000800 System.Void Obi.ObiRopeMeshRenderer::set_SourceMesh(UnityEngine.Mesh)
extern void ObiRopeMeshRenderer_set_SourceMesh_mABB00F322C30D9903B768CD2ACE3007D0A924409 (void);
// 0x00000801 UnityEngine.Mesh Obi.ObiRopeMeshRenderer::get_SourceMesh()
extern void ObiRopeMeshRenderer_get_SourceMesh_m0068E1A186BF741C6EE74048DADA660C04015EBC (void);
// 0x00000802 System.Void Obi.ObiRopeMeshRenderer::set_SweepAxis(Obi.ObiPathFrame/Axis)
extern void ObiRopeMeshRenderer_set_SweepAxis_mED22BD018A3915542FD681B0B1F4D755F06155BB (void);
// 0x00000803 Obi.ObiPathFrame/Axis Obi.ObiRopeMeshRenderer::get_SweepAxis()
extern void ObiRopeMeshRenderer_get_SweepAxis_m2BC90F696A1433C0248339A45E41A795CE2C8B4D (void);
// 0x00000804 System.Void Obi.ObiRopeMeshRenderer::set_Instances(System.Int32)
extern void ObiRopeMeshRenderer_set_Instances_mC7C1482F709BF5F7B1FBC075144B068FA1EA12F4 (void);
// 0x00000805 System.Int32 Obi.ObiRopeMeshRenderer::get_Instances()
extern void ObiRopeMeshRenderer_get_Instances_m9CE541D38231EEBEA1D50EFBEED83E2F92C69D8B (void);
// 0x00000806 System.Void Obi.ObiRopeMeshRenderer::set_InstanceSpacing(System.Single)
extern void ObiRopeMeshRenderer_set_InstanceSpacing_mF9B069DAE0D1F71E50E99E33AF0F950402E4B4AA (void);
// 0x00000807 System.Single Obi.ObiRopeMeshRenderer::get_InstanceSpacing()
extern void ObiRopeMeshRenderer_get_InstanceSpacing_m9C6AEF6CCC9A3EA7D94552A48A781D984E124856 (void);
// 0x00000808 System.Void Obi.ObiRopeMeshRenderer::OnEnable()
extern void ObiRopeMeshRenderer_OnEnable_mCA4ACCE43A10C160B4FA5D046614C98542412387 (void);
// 0x00000809 System.Void Obi.ObiRopeMeshRenderer::OnDisable()
extern void ObiRopeMeshRenderer_OnDisable_mB94D19D22E4DB0A0B06602571195487714957803 (void);
// 0x0000080A System.Void Obi.ObiRopeMeshRenderer::PreprocessInputMesh()
extern void ObiRopeMeshRenderer_PreprocessInputMesh_m7EF1EE49A0487D53A0472FF4BCB6BCA84230E911 (void);
// 0x0000080B System.Void Obi.ObiRopeMeshRenderer::UpdateRenderer(Obi.ObiActor)
extern void ObiRopeMeshRenderer_UpdateRenderer_mE9FFC55F211FFD9BA63E319D8F77F8C80D58D8AD (void);
// 0x0000080C System.Void Obi.ObiRopeMeshRenderer::CommitMeshData()
extern void ObiRopeMeshRenderer_CommitMeshData_m5934A704FB06A7B37D8EC29D4EDDC25254AC9756 (void);
// 0x0000080D System.Void Obi.ObiRopeMeshRenderer::.ctor()
extern void ObiRopeMeshRenderer__ctor_mE5BC1801089B3FD4D812020C3BDCA8B249675C20 (void);
// 0x0000080E System.Void Obi.ObiRopeMeshRenderer::.cctor()
extern void ObiRopeMeshRenderer__cctor_m95B600F74A946C4B0C9EF50B3479D496AC31D0CB (void);
// 0x0000080F System.Void Obi.ObiRopeAttach::LateUpdate()
extern void ObiRopeAttach_LateUpdate_m8A0247A5D2C0AEFD4D608EC687722C14A81B8674 (void);
// 0x00000810 System.Void Obi.ObiRopeAttach::.ctor()
extern void ObiRopeAttach__ctor_mA5527E87A206EC0601B23C3FDD325F0EF19348F3 (void);
// 0x00000811 System.Void Obi.ObiRopePrefabPlugger::OnEnable()
extern void ObiRopePrefabPlugger_OnEnable_mFAC396C907027689582E1F8D33BE045758F0570B (void);
// 0x00000812 System.Void Obi.ObiRopePrefabPlugger::OnDisable()
extern void ObiRopePrefabPlugger_OnDisable_mC973B39792A52C083ECA9B35EA24415BEF56C644 (void);
// 0x00000813 UnityEngine.GameObject Obi.ObiRopePrefabPlugger::GetOrCreatePrefabInstance(System.Int32)
extern void ObiRopePrefabPlugger_GetOrCreatePrefabInstance_mAB7F8BAA8692E5C334C014EDFF95F63C3DE89E0B (void);
// 0x00000814 System.Void Obi.ObiRopePrefabPlugger::ClearPrefabInstances()
extern void ObiRopePrefabPlugger_ClearPrefabInstances_m019D264CAD4FD43AFC7801E6A7E284F26244A1B4 (void);
// 0x00000815 System.Void Obi.ObiRopePrefabPlugger::UpdatePlugs(Obi.ObiActor)
extern void ObiRopePrefabPlugger_UpdatePlugs_m785485C0BF137163959433C48BB5D8153D815E45 (void);
// 0x00000816 System.Void Obi.ObiRopePrefabPlugger::.ctor()
extern void ObiRopePrefabPlugger__ctor_mBBC26B2FFB9C07B5C26E5429AE79E05E7F65906F (void);
// 0x00000817 System.Void Obi.ObiRopeReel::Awake()
extern void ObiRopeReel_Awake_mA6FA3792ED36E3917682DE7F89E3C518A58BF95B (void);
// 0x00000818 System.Void Obi.ObiRopeReel::OnValidate()
extern void ObiRopeReel_OnValidate_mC46F4295A6C54AD97935E43EF7DC01164FDCD580 (void);
// 0x00000819 System.Void Obi.ObiRopeReel::Update()
extern void ObiRopeReel_Update_m2F0E0E5D6EC299100F832CFA1E0E2AD490385ECA (void);
// 0x0000081A System.Void Obi.ObiRopeReel::.ctor()
extern void ObiRopeReel__ctor_m880E0A2CD5DD907FC02806BFE19D0C899562B1FF (void);
static Il2CppMethodPointer s_methodPointers[2074] = 
{
	ObiContactGrabber_get_grabbed_mB390126778E0A789B441B372D5F525DB318BC301,
	ObiContactGrabber_Awake_m81FAB1A7D0894DFF410B881A4BD34C377CCDF33D,
	ObiContactGrabber_OnEnable_m0CDF84CEFA0997E0C363D99406A72B7F6617015D,
	ObiContactGrabber_OnDisable_m70E63109356461451716F2656B1F703ABDF30B67,
	ObiContactGrabber_Solver_OnCollision_m02CB781C94F8F518ADC1B61E01487BBDC62BA026,
	ObiContactGrabber_UpdateParticleProperties_m874B590742CF4C376FDD13678A387918D098B615,
	ObiContactGrabber_GrabParticle_mA112EAB2920DB3B64AA31FCF9455C6F4702858B5,
	ObiContactGrabber_Grab_mF88B7A50127D874AC0295C6D7D98FFEBE6D8F618,
	ObiContactGrabber_Release_m82B5D7B992BC9423026E8CE0BD6DE0884D72BA68,
	ObiContactGrabber_FixedUpdate_mCAD4BA24875E897F95967F4540B06AC26800BF01,
	ObiContactGrabber__ctor_m9C1D048C7D8633509DB48156260CB765A7E6E364,
	GrabbedParticle__ctor_m29866A846BC99D62A7E53A7F55FD6234B7DB7567,
	GrabbedParticle_Equals_mFF2582C3BFDDE99366B35AC5DF73C20ECE8E430D,
	GrabbedParticle_GetHashCode_m37D33E44CE7086BE8CD4AF64B4270BE0FADDACC9,
	Oni_PinMemory_m03E1EA6D2AD2B2501D052E52FDE072606EDEB6EF,
	Oni_UnpinMemory_m029F0BA710E2DBE187A2300ED9F5CFA9DE37B62F,
	Oni_UpdateColliderGrid_m75034617DEA373FD3AAC6D3F57A9530AA426895D,
	Oni_SetColliders_m901960CF23BF96A6DCE6751F09B4425431091A65,
	Oni_SetRigidbodies_mF84FEC7A0C4A850D6CD1F9CF39A29C2D6AFEAECF,
	Oni_SetCollisionMaterials_mAAE3428F89A91AF30BB753E728556AD1F592F3B1,
	Oni_SetTriangleMeshData_mFFF4366C8A8B13EDA5301EDE1CED9A2CD92AE43D,
	Oni_SetEdgeMeshData_m7D6EBD72BA2169E3C697F341B75BC716161524D0,
	Oni_SetDistanceFieldData_mED7CC8724B49A2AF36F817A57152CD95AA6E0200,
	Oni_SetHeightFieldData_m3890F2905A2990080378C1C2F28A9AD2DA077BEE,
	Oni_CreateSolver_m95484CA4C8E4048EB871BB224918540A317C885A,
	Oni_DestroySolver_m0ADEE02261B34DC1C348911FCDC53E4471B37D0B,
	Oni_SetCapacity_m4804EBFCAF115506C8DD5332214C6A2E0CB4810B,
	Oni_InitializeFrame_mB306333E4A867678C088DACC72B039EF58E80D31,
	Oni_UpdateFrame_m680BE80814872FABDBFDC7ABBE5FF1E740C5773D,
	Oni_ApplyFrame_mD136F376FE68D93651459FB1B946CE18BFAA8D10,
	Oni_RecalculateInertiaTensors_m5D778943EF3E20DCF00A85175E39DD691A8AFA16,
	Oni_ResetForces_m38A179189C49CCAC6142D9F38F234BA8D03C5F64,
	Oni_SetRigidbodyLinearDeltas_m3D5B23B5969BC13392B4E7E019D03730E61E3639,
	Oni_SetRigidbodyAngularDeltas_m4A436C37D1F6F9FE39F1184279883463F586CBF0,
	Oni_GetBounds_m363A96EE8BAB3FB760C636B8C5F361B8EE1E53B5,
	Oni_GetParticleGridSize_m2BD9A7939A0F7733323CA9C9EDE32C53FC7B6ABD,
	Oni_GetParticleGrid_m21D4AA3548CF77E25C5E456096E16162C1BAD4C0,
	Oni_SpatialQuery_m526F90093505F1A4FB3E31A2AC6EC6B8C1F850A5,
	Oni_GetQueryResults_mB69A3B8645A8218700E75C49956E15AC27B125B7,
	Oni_SetSolverParameters_mA78FF4C834E23D50A56D989521B278AA6B11BD48,
	Oni_GetSolverParameters_mD299B385DF9770D77C9E0D4F4781EFADE20C30E4,
	Oni_SetActiveParticles_mA8361CFE331459188A7701673465FCEC0770F84B,
	Oni_CollisionDetection_mD890F2AED814E8B7F3766DDD603916EDA4B1B861,
	Oni_Step_m73AE7B946E5053FC14C8061FCD6508D4824213B7,
	Oni_ApplyPositionInterpolation_mA7A631314B43D3A62FAF81D209F1CCB00A3A901D,
	Oni_UpdateSkeletalAnimation_m0F0F49630F0ABF1B6C742D13FEDB8A69B3C71A31,
	Oni_GetConstraintCount_m046D7B8D04B0EDDC24A336FA1AD6F93B181B143A,
	Oni_SetRenderableParticlePositions_mF10457D7F9724C687D3AB6E83DEEBB4C9937F138,
	Oni_SetParticlePhases_m330FC442852B31A851FB05486D9D5D4A9E85080C,
	Oni_SetParticleFilters_m4689F2DCACACC85971902111BF779817074E976C,
	Oni_SetParticleCollisionMaterials_m64562F4539FF5805E44B181ED119B61FB636DB91,
	Oni_SetParticlePositions_mBF4B08453E99AACC5D3C934F3B7F1EDB2AA3934A,
	Oni_SetParticlePreviousPositions_mF7643A2DD9F3D10409DF62601E0D8EE2D0DBD7E4,
	Oni_SetParticleOrientations_m92BF1084EB29939C992A0666D1E75C6AAB14801D,
	Oni_SetParticlePreviousOrientations_m08E1313630723606D179B6FB44025E49B838D2BB,
	Oni_SetRenderableParticleOrientations_mE90A2228D4CDC6641661C730F1164F65609788B8,
	Oni_SetParticleInverseMasses_mA5F3998095FBD44B558C75B634E18628520D2385,
	Oni_SetParticleInverseRotationalMasses_mBB6281669729C39BEF623DDDEDCC12EC93C31FBB,
	Oni_SetParticlePrincipalRadii_mCE141D972ABA98C2CCC7B66419BC0009E227F8CE,
	Oni_SetParticleVelocities_m806F66F28AB0D4E632921B7E0CF3464C777B00C7,
	Oni_SetParticleAngularVelocities_mD376C89032B153B1C2EB36FAF673C90600871052,
	Oni_SetParticleExternalForces_mABE204855B05F30D980984785EFC178E8178657B,
	Oni_SetParticleExternalTorques_m52F453DFC994BB2CB946C0ECFCA90755E8CE81BF,
	Oni_SetParticleWinds_m4D8B34B1F691B0D06B66B5AC4A9CE7A47C438D7A,
	Oni_SetParticlePositionDeltas_m44290488FA81D82A7B5E34C6C419183E4AC4C7F3,
	Oni_SetParticleOrientationDeltas_m80BA163C8E315CDE4D535070E044140582F4FB75,
	Oni_SetParticlePositionConstraintCounts_mC6C3DF0767ABFE55B7D56664BD2B85CACC2F2AF3,
	Oni_SetParticleOrientationConstraintCounts_mE8DE30A217492D6C2961351F94471600E84DDF33,
	Oni_SetParticleNormals_mC7DB3812E3D8DA4944D5783D612327C14DAA1407,
	Oni_SetParticleInverseInertiaTensors_m107AC92B3D36DA0EC84A2BC81EAAFDE1A87645F3,
	Oni_SetParticleSmoothingRadii_m23C24AAADB64BB87F5AB6C0F29146E045BD15F32,
	Oni_SetParticleBuoyancy_m1D5C0C42C6E1F7F9F0009907C3C47777493BFFFD,
	Oni_SetParticleRestDensities_mC2C5BF07F3F31A9A9BD30F1E0FD65FA90DAB8668,
	Oni_SetParticleViscosities_m40D16C05777FCCBA236F8812E90FCE61732F8F65,
	Oni_SetParticleSurfaceTension_mEB650D756ACFD8AFCE0B0CA1FA516B8A04166803,
	Oni_SetParticleVorticityConfinement_m490207BCEB92C2090FA6F812903E9DC720C542CD,
	Oni_SetParticleAtmosphericDragPressure_m0DE4F75155DE8255D542463CED62BBA10DE95991,
	Oni_SetParticleDiffusion_m2E7A56B1429ABF2C68BCBEAF9A8B903BCFBBEB97,
	Oni_SetParticleVorticities_mBFAFD13B80D055251ECE3BB09331FEE93EC096CE,
	Oni_SetParticleFluidData_m5475F465341FBD9D3646CF21F640D31DBBDA52BC,
	Oni_SetParticleUserData_m1C95E2860A342CC2408369BE71453D688A4940E2,
	Oni_SetParticleAnisotropies_m4F40683E247F34D06C6FAC46E7BB2D100699D943,
	Oni_SetSimplices_mFA1EA60EF9F96B8AD384993D7AC15B73A2B08D95,
	Oni_GetDeformableTriangleCount_m3D720CBB04BADD1E4AAE5D0F630E7E8A1AB8D398,
	Oni_SetDeformableTriangles_m93D02F1E05CEF46D51296532191387392C8885C6,
	Oni_RemoveDeformableTriangles_mA0A4A28EA77A35743C37CF80A333559E7C8CC8AF,
	Oni_SetConstraintGroupParameters_m7402C64CA0198FD7D07E4F2BBFC81F8C9DB926B3,
	Oni_GetConstraintGroupParameters_m841DD84B6D78A0B694506D815E4756313934B29F,
	Oni_SetRestPositions_m819342E902C330762AEB5BE9D4C68F31DA6CA8A1,
	Oni_SetRestOrientations_m87B3C65B2FA89627A6526A5C22B674280DC3C577,
	Oni_CreateBatch_m289C846274F339F1F3E4A026FE857DAAAC56ABE8,
	Oni_DestroyBatch_m0C065F052A4E0B16A426180BF8EC84E74845007E,
	Oni_AddBatch_m22AFEB35DB5CD69377763213EEED12183545A8DE,
	Oni_RemoveBatch_m62A5EE513FF64B56579A035F1463C0308DD01A48,
	Oni_EnableBatch_mE5AAEE57C0386681A3016600946C205D90CCBD0B,
	Oni_GetBatchConstraintForces_m2F4FB4C6732CFC54C859D7DB5C4A3AC75E4400FE,
	Oni_SetBatchConstraintCount_m31FD93985340AF90A5D98742034D010888144B41,
	Oni_GetBatchConstraintCount_mCDA7F7C7B72B04711AAD3435FFE3AB6FDF657966,
	Oni_SetDistanceConstraints_m6BAF3B1FE95B48E8E681121385077A369140BA88,
	Oni_SetBendingConstraints_m4DD1607554632126E50CA62B31DE223000DEE579,
	Oni_SetSkinConstraints_m980F260DC4107F86B16555CB40B9B63A196F558C,
	Oni_SetAerodynamicConstraints_m97BED4EFAF9B73A677757503EF1AF7EA59CB712C,
	Oni_SetVolumeConstraints_m7914A0DFEEF418BE4C6C81DE6A27EEC21701F511,
	Oni_SetShapeMatchingConstraints_mE2D86779FF689D2FBEB8F39FF1683CEAA9D7547E,
	Oni_CalculateRestShapeMatching_mD268677C88F2E8ABB2C0E8149F0683FFF9AF3BDD,
	Oni_SetStretchShearConstraints_m49F7345551857151AE2572A3F59A90FCA236CDA8,
	Oni_SetBendTwistConstraints_mE4976251409BFB7882CC39AD1F21D4A97B7F4D03,
	Oni_SetTetherConstraints_m10810C34086C7751FB8A5600EFBBC9C70887449E,
	Oni_SetPinConstraints_mC5929BC4A3B8861A32C47072223BD7C88CAC8843,
	Oni_SetStitchConstraints_m8576B5213542C38E501BD55E1C0936DF69506253,
	Oni_SetChainConstraints_m8F08E1B952B69C7A6D3F1957CA4511BE3D95D25D,
	Oni_GetCollisionContacts_mD80B55291B76FB42F97EE2C75EB2E5089540AE63,
	Oni_GetParticleCollisionContacts_mCDAC4DB2F8B1A798CEC15191044C4FE761AF69D1,
	Oni_InterpolateDiffuseParticles_m4A3F61BFA0B073204F17E45D12E58B292B3C5D44,
	Oni_MakePhase_m9FFA8D86C974D669797A0DF8FF1E6A6270554D71,
	Oni_GetGroupFromPhase_m1E9E33EEBF211AD24F224C4DDF044F55DEC4E6CF,
	Oni_GetFlagsFromPhase_m4EC4AEB3B15B30111080A14FB54D7939A4924F0B,
	Oni_BendingConstraintRest_mBF9E6E4CB4E998E291B956B4304FFB31A1ED6058,
	Oni_CompleteAll_m24DA6FE9992164ABDA19A2E0E5BB58280FD02582,
	Oni_Complete_m4909573C5EF85056474F9EEB3A8C91F6F62050B5,
	Oni_CreateEmpty_mEEB6A72C1C211426318B7A3164BD1697F1C0859C,
	Oni_Schedule_mF1BBBF5D226699D18E8EB55A8F31A08BE1DE528B,
	Oni_AddChild_m2AA3FE95D394A0AB211C7FAF46DA6DDA34645B49,
	Oni_GetMaxSystemConcurrency_mA467F4FF67759D65BBACC64B265BBD53DBEE3177,
	Oni_ClearProfiler_m3CE1E415966CFAA8CBF71A57B412B980CE3B5E11,
	Oni_EnableProfiler_m50B45E5B063E18108D99A6851C2C441A8FEDCFD4,
	Oni_BeginSample_m584963073106DEA213EA5679DA42F89AB29B0931,
	Oni_EndSample_mE3D8141E3F15F6DEEB30C16BDA9BC9E82368F8EE,
	Oni_GetProfilingInfoCount_m89525656BF980E088FBF35150F29DC2BCAF974EB,
	Oni_GetProfilingInfo_m5450B203396A0BD0042264E198B358D3FA41BA23,
	SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3,
	ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiActor_add_OnBlueprintLoaded_m04B2952178ABFA72F0A7E7219EB78A4ECEAE156E,
	ObiActor_remove_OnBlueprintLoaded_m998C17DB7FED249637AC6D40775D9D50ED1CA3F8,
	ObiActor_add_OnBlueprintUnloaded_mB1182B14609EA8CCD5A9BD7C2F3CEDDAFF78C601,
	ObiActor_remove_OnBlueprintUnloaded_mB1AB8C2B322EB4633B0CD45EF5AAF7448BAD68BB,
	ObiActor_add_OnPrepareFrame_m3014BCA91C860098AA7A396A7FFCE7AB7CA1049E,
	ObiActor_remove_OnPrepareFrame_m0D90685372167486999B646FC83D4E2C36C570AB,
	ObiActor_add_OnPrepareStep_mC586B960142DDDF81A5B1C87D393C7CA379E743E,
	ObiActor_remove_OnPrepareStep_mCB8B15D01E8DA343CF2DD8E655932C886DD863CC,
	ObiActor_add_OnBeginStep_m7A401277ED988AF4C6388B1F768B4E613BA02E10,
	ObiActor_remove_OnBeginStep_m80E4C1C9DE59FB6075BFB27E9B8D885ADC413C13,
	ObiActor_add_OnSubstep_m4E244BDD4F0DA8AFBEF75036632A65CF4EC1286F,
	ObiActor_remove_OnSubstep_mABFE386CEC0194553AEE608F465F311B21360592,
	ObiActor_add_OnEndStep_mE226631AC3F2AA3BE52CCB4AB9972A0620E76208,
	ObiActor_remove_OnEndStep_m18354446434FA0FE622CFA9F09B7D0D98B7365F9,
	ObiActor_add_OnInterpolate_mA39BB43FF56C2FF95981971698C30856DC25ED65,
	ObiActor_remove_OnInterpolate_mE2B57508C1595198B4E27E10DA174A0B3792AD8D,
	ObiActor_get_solver_mDE668FD9EA16553E2252B51FC4AA0EE5F2476940,
	ObiActor_get_isLoaded_mE37E960D0EE5EF278241FE730E4E6D56FBBE217E,
	ObiActor_get_collisionMaterial_mFEBA9610A92672D379939CCF56E5A07800EF7AB7,
	ObiActor_set_collisionMaterial_m04C98A6B84C31070613652C4D12C3EFC0D98A0C4,
	ObiActor_get_surfaceCollisions_m280DE5273E14E28B1037C4CEA893F3BFFBEB2CB8,
	ObiActor_set_surfaceCollisions_m40EE79424632CAE1EBA8FB1C3087D54FD9FC5046,
	ObiActor_get_particleCount_m8A511B9B8EE21FE09B45EA484ADE54CE1A469C24,
	ObiActor_get_activeParticleCount_m330540C2CFF296103F759A43A156960A14EF40E3,
	ObiActor_get_usesOrientedParticles_m1F5AC23A705419FFA18109ACE2CD45861FFCADDE,
	ObiActor_get_usesAnisotropicParticles_m54157FE27C8DBE561125BC87EA21BA034F246B69,
	ObiActor_get_usesCustomExternalForces_m63A0FBED9893C8176AA7DB6B5567978FB1D71DCA,
	ObiActor_get_actorLocalToSolverMatrix_mCF3080C74BC381BF19F21BB6204A19DBDDFB82DE,
	ObiActor_get_actorSolverToLocalMatrix_m467C1F4C003B591FBE2C93BA9BDB33C0371962CE,
	NULL,
	ObiActor_get_sharedBlueprint_mE4DB059D49A26A1EF8D5AFD8CECEDB63CDACD863,
	ObiActor_get_blueprint_m47A228582F43BE1A2AF98A4CFBF052C0DCF852F5,
	ObiActor_Awake_m241F3014CB9150A6234C8BBFE5F7F1506CC4A4A8,
	ObiActor_OnDestroy_m2F3D4A01290E0E5EB2F3753AB9913AC96249A655,
	ObiActor_OnEnable_m971216E2924B472ED6E1EF5CA46C20711919C967,
	ObiActor_OnDisable_mCD8A0D978C2AD361EA37D62139DD083606463FD7,
	ObiActor_OnValidate_m52B606D1428A6597252E13B0A7664A6A2A9AC196,
	ObiActor_OnTransformParentChanged_mE56EB6C5787A9B68CC2DCBF59A69FC5DBAC340B7,
	ObiActor_AddToSolver_m4612A91281D3B2EEA44B2654DF515E757FA4D3FF,
	ObiActor_RemoveFromSolver_m6E84D3DEBFE722DD4EC517EB2622FC80350D4FEA,
	ObiActor_SetSolver_m6163A3076A049EAC233DCD2BB28B05C282DA55B6,
	ObiActor_OnBlueprintRegenerate_mC7B6851CA42C438CCA449E6F6B675C88F4515C71,
	ObiActor_UpdateCollisionMaterials_m5E96C2AE2B9F7811085F85D0D25A5621C46D6817,
	ObiActor_CopyParticle_m8AA520BF621655A1D7FD60BE63769B097E1996D6,
	ObiActor_TeleportParticle_mD6150ED98C32F82A0ADB734DDE58A37BA0B78E39,
	ObiActor_Teleport_mE63A190E1F4170CF5E1BE19522E3B6061AE96A04,
	ObiActor_SwapWithFirstInactiveParticle_mC6593AA19F1C4AF00040F46D4EC723008B97B52B,
	ObiActor_ActivateParticle_m45FF9EA8B2788D3C9EAE0EDE77DA4FA2133BF9BA,
	ObiActor_DeactivateParticle_m04DDF95A73A8AE09950848D4A8DBB2A57EC42C59,
	ObiActor_IsParticleActive_mFD14C2BCE7E86131404B2EEDA413555B7288C0E3,
	ObiActor_SetSelfCollisions_m45F0B0F5D1A4306D161C7EEC3C1435D9D52E3686,
	ObiActor_SetOneSided_m99DA6DD46C17B2357C1815E76EA6FB70B6525D33,
	ObiActor_SetSimplicesDirty_m1FA63CB84FD2F2219BB54CB69CC5E8473FE2167A,
	ObiActor_SetConstraintsDirty_mF1D5DEE2405AE6F94AE264FC9CD0A63C31189D0F,
	ObiActor_GetConstraintsByType_mA485A63E65B2E5A863562BA515B15F6DE11138E7,
	ObiActor_UpdateParticleProperties_m9B2C616C0275DB62285F9E0CB0F1FF615D9FFAFF,
	ObiActor_GetParticleRuntimeIndex_m4AECF54626524359EE8CA930022DDFA864946B0D,
	ObiActor_GetParticlePosition_m02493A97C489BA4E448F1B3BE3AAB59DB1E9039A,
	ObiActor_GetParticleOrientation_mC35CD07B08A81AC2159E02DF583118B7ACF8E9C3,
	ObiActor_GetParticleAnisotropy_mC8BAD8C2ACCEB1DF3284AEF13CC5E12ABEFF727A,
	ObiActor_GetParticleMaxRadius_m834AF87A1D3204613AC0A6658EDBAF54871C13BD,
	ObiActor_GetParticleColor_m08A638A989F579EFADEA293B32C74635EECD25EA,
	ObiActor_SetFilterCategory_mAE60934D51EBD397225139EE05F2E80F08118D7D,
	ObiActor_SetFilterMask_m7FED73FD99F34632090C9174E5EB278DC5D76F08,
	ObiActor_SetMass_m8D79A26F4FBEF1A895C6F482808A23A02B8BFAC4,
	ObiActor_GetMass_m2E9973F1EE3CBB300948F36489B7A7637B768F98,
	ObiActor_AddForce_mE87CF6C0A21C03ED45209758E7BFE0E1AB2B1A00,
	ObiActor_AddTorque_m43A494B40EA87E0EC7A6274A0931F9CF27B5D77D,
	ObiActor_LoadBlueprintParticles_m11D7669681BB9F35863E2BE6B9B50F9C7F8CFE88,
	ObiActor_UnloadBlueprintParticles_mAFE44CA41C0A763A6142420F2145AAEDCFD296A3,
	ObiActor_ResetParticles_m081DAAF528B4EF4F9E79AA17ECBEFC2C0BCB03D2,
	ObiActor_SaveStateToBlueprint_m86BB211764276B407A42BF4C603D75AEBD260F84,
	ObiActor_StoreState_mDF522A37AED6A366CD98C42E6D12EB524E5A6B67,
	ObiActor_ClearState_m2708E40E0CC1259F0A4A23A134D7C08A34D64D9E,
	ObiActor_LoadBlueprint_mD8204B2EFE860EADB85ABAAEA380D0EB7DE52966,
	ObiActor_UnloadBlueprint_m0380FCD5E41E56876B933D34F94EE112FE49D14E,
	ObiActor_PrepareFrame_mB2B1C5E2FB1A0570C0B7527B5654C17ADE677A53,
	ObiActor_PrepareStep_mB06CED03B998A63A4AC43A5318D99D1AE8ABA400,
	ObiActor_BeginStep_m2CB8338B9754937B0F151D9F06AF97263B2D313F,
	ObiActor_Substep_m784F85C0BD4464D6E4B1BCE24E44047956C78486,
	ObiActor_EndStep_m5076BADB8970EC68040EA131738946988F140513,
	ObiActor_Interpolate_m589CB41AB063846C5C621944F1B14EACB90C3683,
	ObiActor_OnSolverVisibilityChanged_mA9B03F298B37CD3BB66DD5527F9E3CDB43A728EB,
	ObiActor__ctor_mE992775E0773F687C886A5C462E2EAB7FA5D740E,
	ObiActorSolverArgs_get_solver_m2C65EFA043F915BB25BF5EE5F467EA1119C55BDF,
	ObiActorSolverArgs__ctor_m645D2D61FF1A0780D4B70C2494C8C90607C7FA1B,
	ActorCallback__ctor_mA6CF95A70DAB4FFF21823741E58A598FD0A3CC61,
	ActorCallback_Invoke_m44385DCB93F7AC483E18B7477BDEB1C44993D508,
	ActorCallback_BeginInvoke_m511CEA8353354A1869B3548D98C3EA166C757E30,
	ActorCallback_EndInvoke_m44778EF05F7C946EE7ACD0315B4F14F101980024,
	ActorStepCallback__ctor_m48119AF13D841B3D5476543A1C79108F94D53E00,
	ActorStepCallback_Invoke_m25120AB25E0465B7AF5976D51946EED8935E2A38,
	ActorStepCallback_BeginInvoke_mE4197C4E139BFCC95434E2C3E20BE882EB5F2989,
	ActorStepCallback_EndInvoke_m8AA8AAD3C0E68BB1AF5F7E6A61EA2269F3A57AFB,
	ActorBlueprintCallback__ctor_mADC829707A4F631EADEE895A25D2AAEA59A4D64D,
	ActorBlueprintCallback_Invoke_m7BFF8526B4E3B31969016C044E63EA9AF3FFE7AD,
	ActorBlueprintCallback_BeginInvoke_m32D286D241BEC1F0C4FD9CF6C04C5E7BF1823485,
	ActorBlueprintCallback_EndInvoke_m7B651D23DA455A93DCC87B95CCE21A5789433013,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NullBackend_CreateSolver_m149A74012C4CF9DEC9EC68F1C16EE99F3D6E88AF,
	NullBackend_DestroySolver_m5E51947F4BEC3D57534FD796732535ED2B83D3F6,
	NullBackend__ctor_m7C4DE499807A5119E6914F1A7B6666E7BBFE44A9,
	NullSolverImpl_Destroy_mCEF74F887CB2BF0E13C8176A052B1539F51BD6AB,
	NullSolverImpl_InitializeFrame_m1A9943BBB1200D17394A300365060CC1C180C333,
	NullSolverImpl_UpdateFrame_mE9842C5421EB8BCEF09BEF66B7ECE4C6BBF68453,
	NullSolverImpl_ApplyFrame_m6E1688FD679EF4B17A6C885CD116D47885BA9A6D,
	NullSolverImpl_GetDeformableTriangleCount_m5B5658F9B5D43BC6CF7131229FE97D16E931E5AB,
	NullSolverImpl_SetDeformableTriangles_m3555C196B17530814FC47EB0D65B0B19428AE73E,
	NullSolverImpl_RemoveDeformableTriangles_m327A23C82D523DB2799DC060C88746468C9CB012,
	NullSolverImpl_SetSimplices_m31A164287A45DD5E59CD0C787A24C85DAB4C3D40,
	NullSolverImpl_ParticleCountChanged_m8557EBD352BB202123699487E00B843F3CA0DC0B,
	NullSolverImpl_SetRigidbodyArrays_mEC64908F48D6E3EE3F03975F21201DAD33E36848,
	NullSolverImpl_SetActiveParticles_m270CC166EEFCBB7494E52DFD1F7385B091AFB05B,
	NullSolverImpl_ResetForces_m02F4803EE6AB50D5563B60A00E5535B12F8E3B11,
	NullSolverImpl_GetBounds_m4D24F01134C67A078F93680525FB8DC5804A3DBB,
	NullSolverImpl_SetParameters_mDCC55383F8800DB49278298ABF8A176E6F5B3CAA,
	NullSolverImpl_GetConstraintCount_mA5540FE96215F61ADEFE829931F4D927E70019E0,
	NullSolverImpl_GetCollisionContacts_mD2C3B675F93A95F33D2472E11AD4F48C503B2B54,
	NullSolverImpl_GetParticleCollisionContacts_m55885C928BC24A887BD63F783CD979F085E07381,
	NullSolverImpl_SetConstraintGroupParameters_m3A8213C3F85B7A2187B6030CC6EDC4DE5CB505BE,
	NullSolverImpl_CreateConstraintsBatch_mB98D773275819E868529278DC289D6E084CC913D,
	NullSolverImpl_DestroyConstraintsBatch_m42D46BA79047F3A0DBD2B305549E97FA654EF2B7,
	NullSolverImpl_CollisionDetection_m3BE61763707C061EB9912C7E9DCD44C3F135C0FB,
	NullSolverImpl_Substep_m46BCCC5D64A69DD461D4EF42A3D227A2F53A6A08,
	NullSolverImpl_ApplyInterpolation_mB63837B2AE0542F306D50ABF7A05868F2A9A75BC,
	NullSolverImpl_InterpolateDiffuseProperties_m5D4052E47F4C0CE896A55E4932A7AB35F0001133,
	NullSolverImpl_GetParticleGridSize_mD74EA2D59BC1B4472754D76EFF3B4FBC969B2A66,
	NullSolverImpl_GetParticleGrid_mCBCC36302C00CB01775017E21F63754021E04BEC,
	NullSolverImpl_SpatialQuery_m252FFC078850F330BB8276D6996CD2303F90D6F1,
	NullSolverImpl__ctor_m835D73EC52194B7861F10BCEC2C8F3D30E98D6EF,
	OniAerodynamicConstraintsBatchImpl__ctor_m08CF6D0353900E2F5161D01B30D6D4FD181EAF67,
	OniAerodynamicConstraintsBatchImpl_SetAerodynamicConstraints_m78BC6505E02E608B6E7E64C31C785C0653DCCDD5,
	OniAerodynamicConstraintsImpl__ctor_m525B8236B34A2BEB8A129BC2DFCAD6433E10673D,
	OniAerodynamicConstraintsImpl_CreateConstraintsBatch_m3C15118FE42A98BE00C420EFF5CC108133E8C9D1,
	OniAerodynamicConstraintsImpl_RemoveBatch_m7312BFD8301F75F44EF449DAB1CA208CDF256820,
	OniBendConstraintsBatchImpl__ctor_mD888A9E984F69E1F75358F7C87F224E5BBFEF9C1,
	OniBendConstraintsBatchImpl_SetBendConstraints_mACD9168310968682EC7F9FAF4FAF01D439561251,
	OniBendConstraintsImpl__ctor_mDF7E828956758CC442DFCE530D6ACB3DA28C7520,
	OniBendConstraintsImpl_CreateConstraintsBatch_mE943BA310CF0508FA4B457AE9C4CEDE436C0256C,
	OniBendConstraintsImpl_RemoveBatch_mE65A138E34D866312EA8D6811377B0D2A5A8F759,
	OniBendTwistConstraintsBatchImpl__ctor_m8F131CD09F6F7F09BBDA55AF3E953E5EA2D1F52F,
	OniBendTwistConstraintsBatchImpl_SetBendTwistConstraints_m93208190B4C9F21B66804826F3F388365464C7E6,
	OniBendTwistConstraintsImpl__ctor_m2D810DA4120C0AB84298B4A542CA5B932E774DCB,
	OniBendTwistConstraintsImpl_CreateConstraintsBatch_m5F1FC4850DB914A408732085F9AAEB1FE7D9DEF9,
	OniBendTwistConstraintsImpl_RemoveBatch_m571856C1E93A0B2507FD9309A2984CAD3143BB55,
	OniChainConstraintsBatchImpl__ctor_m11A76775D8DA7D09B7DACBF1D1168D4FEF4F1694,
	OniChainConstraintsBatchImpl_SetChainConstraints_mA704113F26DE40220400DE5B85B1475C2B3D0A82,
	OniChainConstraintsImpl__ctor_mAE3D234A2597D468A4621529AECFC4CD2D1BB328,
	OniChainConstraintsImpl_CreateConstraintsBatch_m84FCCD1FC3FF8E6DE2FF68D3B259A0AF6E595B9D,
	OniChainConstraintsImpl_RemoveBatch_mD1FF35D0BD359EB12FC8A74DE8A560E487A9057B,
	OniDistanceConstraintsBatchImpl__ctor_m9005CD0907A9E6BACBBDA3777B6717B8611AFA18,
	OniDistanceConstraintsBatchImpl_SetDistanceConstraints_m2305BC0CB077A2F81D031356D983305E8344B34B,
	OniDistanceConstraintsImpl__ctor_m46EF17FABD8A1BFB1EDE24EDE0F6EB61473907F8,
	OniDistanceConstraintsImpl_CreateConstraintsBatch_m68ACFFA63F7B3ACF12200ACCDD9796BC1E00453D,
	OniDistanceConstraintsImpl_RemoveBatch_m4F114348DEFF33909A2AFCF15D10D98187F7912C,
	OniConstraintsBatchImpl_get_oniBatch_mD3068ADFBFA72D2C5AEC1E4493AF44A61BAAB3D6,
	OniConstraintsBatchImpl_get_constraintType_mA4D89C531D6E50262AAB808ED481F01FCBC4554F,
	OniConstraintsBatchImpl_get_constraints_mC86F66AEBA1CCFE2EE1484C2CA0844257A7A0CCD,
	OniConstraintsBatchImpl_set_enabled_mE3FE79E17C8786437000CF9DF7ED7CC24C58DD1F,
	OniConstraintsBatchImpl_get_enabled_m74C3931C9C466399F511D1258F6DF49FAF007D8F,
	OniConstraintsBatchImpl__ctor_m7E856D436221DA306E7EFF8409F799DC8F926C38,
	OniConstraintsBatchImpl_Destroy_mB934F5194DA3A35DAC45581D183BE8C1BB056A1A,
	OniConstraintsBatchImpl_SetConstraintCount_m9A9C7B081DBCAFE727F2682F742B32A0A5CD1FC7,
	OniConstraintsBatchImpl_GetConstraintCount_mBC3CD543643EBE713BEDBCEC5266DE52F0E6E53E,
	NULL,
	NULL,
	OniConstraintsImpl_get_solver_m44585FB563C805AF280120F7FDE3B5737F4C0411,
	OniConstraintsImpl_get_constraintType_mF0F59F28976DF29B32C94C4156BD7752105087CC,
	OniConstraintsImpl__ctor_m9A0E8C6A195C96B8CB7EF27D99B7A0988672BF61,
	NULL,
	NULL,
	OniConstraintsImpl_GetConstraintCount_mDE65EE94DCFAB8905A9AEF5543E7119928F2900A,
	OniPinConstraintsBatchImpl__ctor_mA95003D5E91633FA72AA1F912FFCC947CBFC25CF,
	OniPinConstraintsBatchImpl_SetPinConstraints_mB28FD52B804E30D4B06AADE5A8764C9F3F4DC223,
	OniPinConstraintsImpl__ctor_m2F0E0AF0C30CE5AA9BBA5B0F2C170582A8442ABE,
	OniPinConstraintsImpl_CreateConstraintsBatch_mF0305992D09B680091BD1EEF5519A801F19F69D0,
	OniPinConstraintsImpl_RemoveBatch_m1EA7BE417BEF16951FAD0CA1106DB5BC257F38E1,
	OniShapeMatchingConstraintsBatchImpl__ctor_mA35A5ADBE7591DFAFCD6775D89CD7C7BBEB7E25D,
	OniShapeMatchingConstraintsBatchImpl_SetShapeMatchingConstraints_mDDBCB5CFAB68DDF0E19581524119A7981D214588,
	OniShapeMatchingConstraintsBatchImpl_CalculateRestShapeMatching_m6E5073835BB3D1CED559A45D987CC3FCD9D4C21D,
	OniShapeMatchingConstraintsImpl__ctor_m945AA881838FE9644266E98CA531247FE004C1D7,
	OniShapeMatchingConstraintsImpl_CreateConstraintsBatch_mB055C2DDEB21A68DC4E3A56E2A989231D1109BBD,
	OniShapeMatchingConstraintsImpl_RemoveBatch_m0C9FC7FFB2680541A78037980C49C907F3598756,
	OniSkinConstraintsBatchImpl__ctor_m5DDA5D3F347C6EC9FF501C52A4374AD88A10AABD,
	OniSkinConstraintsBatchImpl_SetSkinConstraints_m8065C2605A9D036F95C2F1B234883144F9C84CD3,
	OniSkinConstraintsImpl__ctor_m9A29E8FD8F9F99978DFF2EB7149829624A7939A5,
	OniSkinConstraintsImpl_CreateConstraintsBatch_m9FB2EAD640C26270C35A54351F354F8AA1D7E043,
	OniSkinConstraintsImpl_RemoveBatch_mC0102FC9F40941F1DDEBCDA28632CBDD43660523,
	OniStitchConstraintsBatchImpl__ctor_mC565125A40106134B8A526F21E536F819AACDB34,
	OniStitchConstraintsBatchImpl_SetStitchConstraints_m850BB5F12FBB10D113073FFD360406906FE84F55,
	OniStitchConstraintsImpl__ctor_m5A0F9B6CD43ECC7D7AE65AD107B559178559308C,
	OniStitchConstraintsImpl_CreateConstraintsBatch_m94119E21DE281CD292B556C5077176B4D9C9284F,
	OniStitchConstraintsImpl_RemoveBatch_m7B1ED49DA5FB278734D0C9F8597E820B3E284FAC,
	OniStretchShearConstraintsBatchImpl__ctor_m06A2F02D467608368981ECD3800852E4B1229908,
	OniStretchShearConstraintsBatchImpl_SetStretchShearConstraints_mACC9CB76D4FDD53A4FDD96FA2031EBD5F893B6AA,
	OniStretchShearConstraintsImpl__ctor_m743767DB7C25C334E9584FACAF87392343A39ACC,
	OniStretchShearConstraintsImpl_CreateConstraintsBatch_m78706F1FEE7FEAA5EF471FAB390E0E0D71861127,
	OniStretchShearConstraintsImpl_RemoveBatch_mF8F67B57D02D49DEC0DC0197146B243A44090457,
	OniTetherConstraintsBatchImpl__ctor_mA18D6CEBB82288A3B578C8C4C52F4E0119DAB0B4,
	OniTetherConstraintsBatchImpl_SetTetherConstraints_mCA9A516027A94559796B9C29CD716F10CB4BEFD7,
	OniTetherConstraintsImpl__ctor_m79D07270D5F4E7E62395AA8BE2B0F5B2C5B9AD25,
	OniTetherConstraintsImpl_CreateConstraintsBatch_mD49AD0DA234A2CBAF481401273DC88CF16D87AD7,
	OniTetherConstraintsImpl_RemoveBatch_m4B4909C5425342565704DFD35EDD47C5A1E2FB20,
	OniVolumeConstraintsBatchImpl__ctor_m17E79E7DCFE92922A8861BE569C3FF460D175CAE,
	OniVolumeConstraintsBatchImpl_SetVolumeConstraints_m9C580C7D330A95E2032129920D22CC13D70E8AFC,
	OniVolumeConstraintsImpl__ctor_m5147F9E64F13F4D205515CE6F1906887F787BA74,
	OniVolumeConstraintsImpl_CreateConstraintsBatch_mF7765774D05626A213C729351AE71CF664A0A8B2,
	OniVolumeConstraintsImpl_RemoveBatch_m726F19C3F55138E1BB32C8D3C97804E44013547C,
	OniBackend_CreateSolver_m5E3C44A4EA9BCDD470E7E10E684F0AF897C8AF5F,
	OniBackend_DestroySolver_mC6AFB023EC9F9532FF2B840DDF955E89C71A0D01,
	OniBackend_GetOrCreateColliderWorld_m92C5F279126E62C80B84C7ACAC6FEB209751EF1D,
	OniBackend__ctor_m45B8DE94A53B0740C6704A51A4BC4FDEB7C7D4F7,
	OniColliderWorld_get_referenceCount_m2E1D878672ADF99B997AA9EF3050BD41B27DC9FD,
	OniColliderWorld_Awake_mBB4F8B9A43CB88C337CD9DB344E0B7169D47E923,
	OniColliderWorld_OnDestroy_mAF83AA5D931A38B0E55E2226B94527B592FD9D83,
	OniColliderWorld_IncreaseReferenceCount_m78FCC3BFF315F84942D3994F5092A54A8D7F678E,
	OniColliderWorld_DecreaseReferenceCount_m35AE9297895C720AC03E0D0F320DDAD1366BF64C,
	OniColliderWorld_UpdateWorld_m07BB5250B08FE6F9B66FBDC04C2B54A98C4B4251,
	OniColliderWorld_SetColliders_mBC49AA3ED5E83E70CE733DCEC0683FF672872263,
	OniColliderWorld_SetRigidbodies_m28F9E1B58302DB5A756C243EF52E788E708DA85F,
	OniColliderWorld_SetCollisionMaterials_mEB26C8AC30F1B5CE27D8F19A77ABB7B252115E0D,
	OniColliderWorld_SetTriangleMeshData_m4965F3D38925BFCACB96B6B3479DD4F232ED7104,
	OniColliderWorld_SetEdgeMeshData_mD9F54F0603E44D518BC9243FF1979EC24E25FD44,
	OniColliderWorld_SetDistanceFieldData_m2C1ADF6E94752BC41CE632A804CBAE1AC19A9EDF,
	OniColliderWorld_SetHeightFieldData_m4C593F78531A7AE191FA9F8D4A8BBFC23E84F68A,
	OniColliderWorld__ctor_m4DE663B94B5C376427CEB42288BA88428E549A52,
	OniJobHandle__ctor_m3C938A79AA35165ECEC272FDB9E229430994231B,
	OniJobHandle_Complete_mB9E3D94D50CF207A0F961C9F4ADEB30B2FA8A6DE,
	OniSolverImpl_get_oniSolver_mA021288BE5461FA7F14399158965ECDD9ECA1706,
	OniSolverImpl__ctor_m2FE0413D6AF7939DBE480B789D43360A39DDFAD4,
	OniSolverImpl_Destroy_m317E88AD3A919043461CE59F399DF3670A904961,
	OniSolverImpl_InitializeFrame_mC8E921CBD6A70A2F9F46310EB5556586B6749DC7,
	OniSolverImpl_UpdateFrame_m52DA894215FDD134640AD29208E780EEBD65180A,
	OniSolverImpl_ApplyFrame_m67E28FA4B4FBE5C072E359F3574F562C59820DB4,
	OniSolverImpl_GetDeformableTriangleCount_m230CCC63F4275668FF4EF1B34F56859D5C70E68F,
	OniSolverImpl_SetDeformableTriangles_mE67CB6225F084DE826F48AA4D27E43FA43BE9827,
	OniSolverImpl_RemoveDeformableTriangles_m3B80FB7C6FA7DB0890B5BC37670151643F54C650,
	OniSolverImpl_SetSimplices_mD571A7AFB950F771AE96A6A30743D2E7CE5BAE5B,
	OniSolverImpl_ParticleCountChanged_mC72028DD674A3E54C4E08E758A3CE3D26E3E7577,
	OniSolverImpl_SetRigidbodyArrays_mFA7655D4B61E7E1C65C4E42C8B77EA55DFC70AEE,
	OniSolverImpl_SetActiveParticles_m850250C4A4DE24AB05AB59AADCD43D6F232077E8,
	OniSolverImpl_ResetForces_m5E8F36B6FCA63BD1E720DBFD5B8764863E3CE626,
	OniSolverImpl_GetBounds_m478CF8A2FAB372FE347E9899D262869163ACCB57,
	OniSolverImpl_SetParameters_mF506A029DB22FF674610C76D892D387BAD7178A5,
	OniSolverImpl_GetConstraintCount_m7C95C3E195679C7076DEBAB096213874408B03EF,
	OniSolverImpl_GetCollisionContacts_m3179A5E075C13134C753E47D58F150C004D15906,
	OniSolverImpl_GetParticleCollisionContacts_m4B99957B57A3E6BAF05C9E3D64DF28B5E0748259,
	OniSolverImpl_SetConstraintGroupParameters_mB58C6320D75A44B682B4AEA82214F8ACA62E53DA,
	OniSolverImpl_CreateConstraintsBatch_m388EEDF658EF8F63CD78DC8DC33F0DC642688911,
	OniSolverImpl_DestroyConstraintsBatch_m7769244161551FB17B31600D453AC2F68F156860,
	OniSolverImpl_CollisionDetection_m56F7F7DC6C224E1DEE5CF8A1437ED6FE02BAB5E2,
	OniSolverImpl_Substep_mCA3F4A59045D0A506692A48C78ACE6BB4E884D93,
	OniSolverImpl_ApplyInterpolation_mAD4820F8A797FF35107C1C920D5B50D4415C67AD,
	OniSolverImpl_InterpolateDiffuseProperties_m0678F52DF465E6E9B34DECE91C6C3D48F96E8821,
	OniSolverImpl_GetParticleGridSize_mC858C1BFF95D86B1B0841E40378F882A014E8E78,
	OniSolverImpl_GetParticleGrid_m3A454F1EA5EDCF6A590F99A29EEAC84F0D65B790,
	OniSolverImpl_SpatialQuery_m6BD4BCC1AACE17D8D66365F9FE7F16012A1E8F7F,
	NULL,
	NULL,
	NULL,
	ObiAerodynamicConstraintsBatch_get_constraintType_mBC6B3C472BCF899A37C3C9F15B61F0B22DB160F4,
	ObiAerodynamicConstraintsBatch_get_implementation_mD6185053ABF60610B561CE15B51770F6E2556E08,
	ObiAerodynamicConstraintsBatch__ctor_m9DE858EA294B273AEED18201A289AEC50F28F02E,
	ObiAerodynamicConstraintsBatch_AddConstraint_mB9A927126FB700DC1E84DDA91D9132310EFEE124,
	ObiAerodynamicConstraintsBatch_GetParticlesInvolved_m430311A6B436224B845F2F196525AD41A67EB605,
	ObiAerodynamicConstraintsBatch_Clear_m5ACDB0CA34591C9F454153521B67AC2478467EA2,
	ObiAerodynamicConstraintsBatch_SwapConstraints_m3F24E8A02E9F7E06130C7E27A8438623CBC36B99,
	ObiAerodynamicConstraintsBatch_Merge_m4CEBEA6CE4426002B0326CFCAE2C25E3F4954CD0,
	ObiAerodynamicConstraintsBatch_AddToSolver_mF2BA5925F8D160264B0EC1995C4EF9856E4E48FB,
	ObiAerodynamicConstraintsBatch_RemoveFromSolver_m3288141E2907B24EF4337F9C513C6C69D68D8684,
	ObiBendConstraintsBatch_get_constraintType_mA302627388787213E045E481476B7C9EB7F43D92,
	ObiBendConstraintsBatch_get_implementation_mE6CC10221A772592842A23E0EE91871FAA8ED5FA,
	ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9,
	ObiBendConstraintsBatch_Merge_mC7EBF91A7220B4C03792E048ABB718337C6F03AA,
	ObiBendConstraintsBatch_AddConstraint_m59872A1ADDD39CF3FB2C303EA08FD4E6E9AFAA0C,
	ObiBendConstraintsBatch_Clear_mA0DC32AE2AE98E09AF547DE49FB25D8CDFF0DB97,
	ObiBendConstraintsBatch_GetParticlesInvolved_mA9C31748143F96BC5E341BE711B68E9F78204C4A,
	ObiBendConstraintsBatch_SwapConstraints_mD42CD848B36CEB0540B9F211A1C7592348949487,
	ObiBendConstraintsBatch_AddToSolver_m65C74A506765E2DFFD6575EE0E9D8AF1F723EBC1,
	ObiBendConstraintsBatch_RemoveFromSolver_m771E799A2D261FFC3AB2686EB70676ABEBCE21F7,
	ObiBendTwistConstraintsBatch_get_constraintType_mAC26F20B9CE45F7ACE966652B0ACAB8337A9703D,
	ObiBendTwistConstraintsBatch_get_implementation_m293D659D51624728D74EB2EC1BEF2EBA93A301D2,
	ObiBendTwistConstraintsBatch__ctor_mA84FE1A64BC385B93B82ACDB515147D1BFD9D8E9,
	ObiBendTwistConstraintsBatch_AddConstraint_m04DEFCAA48B91E8849ECC4FB1F151C17740B3E78,
	ObiBendTwistConstraintsBatch_Clear_mEA7D8BB6BCCA8C72D2CB42CF1D9C197F3C3FB905,
	ObiBendTwistConstraintsBatch_GetParticlesInvolved_m7F039D1FBFA40A6E0EFFB3F66FAA2A5D4D621393,
	ObiBendTwistConstraintsBatch_SwapConstraints_m19754F194196E8509DE60D4C6A4E01CDE61951CC,
	ObiBendTwistConstraintsBatch_Merge_m65D2EB86911176AE0DBCFC755950330B0FF4D9C4,
	ObiBendTwistConstraintsBatch_AddToSolver_m00517D42056B5BD7E11AF70F1C617FD33E313B35,
	ObiBendTwistConstraintsBatch_RemoveFromSolver_m89781A48762FBA02AFF8BE9423981220F26A147B,
	ObiChainConstraintsBatch_get_constraintType_mBAD7C6C6A0862F3A03015BCA2BA2B969706672C7,
	ObiChainConstraintsBatch_get_implementation_m2883B97D7E6175B2C1FE458CD3E667789C1FEC92,
	ObiChainConstraintsBatch__ctor_m8E8E6310A3DDA3527AFF7F0DF6A851F75B52AEDC,
	ObiChainConstraintsBatch_AddConstraint_m8615D25C5C8628CEE00BD28F9D2D465A635813CB,
	ObiChainConstraintsBatch_Clear_mC5BA1FD8A165468C43063493EF8BFB93A3D2CE21,
	ObiChainConstraintsBatch_GetParticlesInvolved_m1B0852EE90B8C0089C7793F4915B9D5C5E739187,
	ObiChainConstraintsBatch_SwapConstraints_m56826B5809EDF1D888AC022EC80813AB5E8A8029,
	ObiChainConstraintsBatch_Merge_m7C8E59CC49A14B6A3F20B923CB9983A18709369E,
	ObiChainConstraintsBatch_AddToSolver_m8E12FD5F4ACAA95E5FFA4E57A15C6D01796354AA,
	ObiChainConstraintsBatch_RemoveFromSolver_m97FD3AAF49F7296DC655C1BE2B0C866D40330AAB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiConstraintsBatch_get_constraintCount_mA8FC55D7B67C6DE94D5B7383C39D4330CEC3C4A8,
	ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02,
	ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A,
	ObiConstraintsBatch_get_initialActiveConstraintCount_m5F0841BDCCB934AB148E2A870F3EAA2F8552189B,
	ObiConstraintsBatch_set_initialActiveConstraintCount_mF8081C7DCFDC4CF1B2A306517632F6E9C026912C,
	NULL,
	NULL,
	ObiConstraintsBatch_Merge_m0157340350A52C6574BBB2D68D612AA6852ADEC3,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiConstraintsBatch_CopyConstraint_m204FC07814B69BA73E78A0E743D059F565B85874,
	ObiConstraintsBatch_InnerSwapConstraints_m72121F6E08F53AA93A94891FB98F3B6FF2D23A91,
	ObiConstraintsBatch_RegisterConstraint_m8EABAFCE01074053D942CE05D9CF5CEC9381F5FB,
	ObiConstraintsBatch_Clear_mC782D142E91B8F81F511A011339846EC1310C2D5,
	ObiConstraintsBatch_GetConstraintIndex_mA7193601AF174D3A9323DCBC41B86CF2F9EDF24B,
	ObiConstraintsBatch_IsConstraintActive_m86CFCB36A5A1AEBBE1FA877DFEE386BB244A7C65,
	ObiConstraintsBatch_ActivateConstraint_m715C1DA819EA321555CC3E50E102E4130B0DF12E,
	ObiConstraintsBatch_DeactivateConstraint_m75EE8E2E70006545791D2BA14E1E4513F9193025,
	ObiConstraintsBatch_DeactivateAllConstraints_mC7259C1E725CF3BFB8EC6ECFD548E6C70C2F63C5,
	ObiConstraintsBatch_RemoveConstraint_m5AAEA90963297F57C4BFF0C6858AA1E13AE6EC46,
	ObiConstraintsBatch_ParticlesSwapped_mFA21282517AF5D3D422315E8748273AC39BC7ED1,
	ObiConstraintsBatch__ctor_m2B0D34B60AB411AA8D929BA04F3BF98231678A81,
	ObiDistanceConstraintsBatch_get_constraintType_m738D4B915ECC57433D1FBE3736930F510797A6D5,
	ObiDistanceConstraintsBatch_get_implementation_mB2318A9886BF7C3D0513E65A18526B4F28740515,
	ObiDistanceConstraintsBatch__ctor_mE47DA7819812295CB5E274C6A916E54E705A0EC3,
	ObiDistanceConstraintsBatch_AddConstraint_m118C1575A23FFAC933204048E9DD894F44CDD021,
	ObiDistanceConstraintsBatch_Clear_mEE9F3DDD52ECC58C3A9B4391667F88D3FF4B48C4,
	ObiDistanceConstraintsBatch_GetRestLength_m5E1C408F549700872BADA871F7301C1A368E0F12,
	ObiDistanceConstraintsBatch_SetRestLength_m1CA836FD76DCC1AC15BECCF86DD92E487F71397E,
	ObiDistanceConstraintsBatch_GetParticleIndices_mB6664FFE2333714B74F7BFEE6D915DC49C3812C6,
	ObiDistanceConstraintsBatch_GetParticlesInvolved_mC73EBE0B6DB236EC4BE938CB43ACE50F2AE65EFE,
	ObiDistanceConstraintsBatch_CopyConstraint_m0EBF810160015C8A19A39367C329D343DBD1BF44,
	ObiDistanceConstraintsBatch_SwapConstraints_mFC45320687BA195BF90408368D14ED2B804DC370,
	ObiDistanceConstraintsBatch_Merge_m4D30F883821CC7CF022584BEC90B548D14C6DC15,
	ObiDistanceConstraintsBatch_AddToSolver_m3308FD903CF4BC955B8D2F2E0B387D2C8A79B7B8,
	ObiDistanceConstraintsBatch_RemoveFromSolver_m6BC9C6E426F431C44B10DCA8CBFFEF4F67E0936E,
	ObiPinConstraintsBatch_get_constraintType_m41C00D52B2D27084965D72E1F35D4E075444E482,
	ObiPinConstraintsBatch_get_implementation_m4578AAC572F0C5466E210AC1BC23930F8C09C0E6,
	ObiPinConstraintsBatch__ctor_mEE99790C9B386AE46D24F129CDDCF2C1C5B5F5FC,
	ObiPinConstraintsBatch_AddConstraint_mF22DD07678C73491928469A7741BD8B076071DCC,
	ObiPinConstraintsBatch_Clear_mB0A07E7DF21B6C3CF9C5E054F58510FFDAA522AE,
	ObiPinConstraintsBatch_GetParticlesInvolved_m91C044A47CCA58E91BD3E72C7DE0ECE74BC37DA8,
	ObiPinConstraintsBatch_SwapConstraints_mAE0389E323855EC02CB78CE15E73076B72CB1D85,
	ObiPinConstraintsBatch_Merge_m2E0E93A262BDA66314A1CF7125B02B0E3D3AA3D7,
	ObiPinConstraintsBatch_AddToSolver_mDAB05B2150F51AA80C2911CEF1BCE5E104D37E86,
	ObiPinConstraintsBatch_RemoveFromSolver_mC766AF925EE815328507994A26FC0DA6399A473C,
	ObiShapeMatchingConstraintsBatch_get_constraintType_m1D1B013631E72B441D2F440FABC9A47260BC1EFC,
	ObiShapeMatchingConstraintsBatch_get_implementation_mDE72A361D520AEF2417D5FA3D99265B24AE622BE,
	ObiShapeMatchingConstraintsBatch__ctor_m07C5E1D77AD00968700D64371442119BD0EFD860,
	ObiShapeMatchingConstraintsBatch_AddConstraint_m36DB2368774B955435601E74A559F9194797B73E,
	ObiShapeMatchingConstraintsBatch_Clear_m7613DFE3A4A449971A4C9C0E7F78D8ED7D99B7CF,
	ObiShapeMatchingConstraintsBatch_GetParticlesInvolved_m460EACD5CA047B22B48866E7980990188EF04B88,
	ObiShapeMatchingConstraintsBatch_RemoveParticleFromConstraint_mD284790EE70116086A1EF3AF9565322E629039FF,
	ObiShapeMatchingConstraintsBatch_SwapConstraints_m203CA70E418F8DF2BF506ED333695247E286BA2B,
	ObiShapeMatchingConstraintsBatch_Merge_m5A9DEA535310B3863D91C0FBE5A52C051E0357A7,
	ObiShapeMatchingConstraintsBatch_AddToSolver_mA64BACE29C81AC0D914642A2326C5A768BCAD690,
	ObiShapeMatchingConstraintsBatch_RemoveFromSolver_m64F2CB468C81ED844CEE845FC280A5497A18AD33,
	ObiShapeMatchingConstraintsBatch_RecalculateRestShapeMatching_m49C8363C8157E30089548C4BDF8BDA633D26DDB7,
	ObiSkinConstraintsBatch_get_constraintType_mB8336F19B6DBE7777042FA4814EA12115D899223,
	ObiSkinConstraintsBatch_get_implementation_m8EFE8C0407125BE90F4A544FD0BBD6801F606924,
	ObiSkinConstraintsBatch__ctor_m1C9F633F90BE267DDA66B580DED05A7F19BD61C5,
	ObiSkinConstraintsBatch_AddConstraint_m5D3D3C2490F09B0C7B72F763AA56E3AE9D485134,
	ObiSkinConstraintsBatch_Clear_m9760EF8B82970CC9CDE9A840BD9EAF78BD982393,
	ObiSkinConstraintsBatch_GetParticlesInvolved_m77E2085BC3B66F68FE4B9453F0D76A9566718597,
	ObiSkinConstraintsBatch_SwapConstraints_m90010A1D7C64381F5575F0F393B7030C2201DF8F,
	ObiSkinConstraintsBatch_Merge_m5AC677F66016C8CC6DCC768E4A60ECEE633075A6,
	ObiSkinConstraintsBatch_AddToSolver_mF9FF0C39D2BA6C21C642F2F0880004B3D719DC2C,
	ObiSkinConstraintsBatch_RemoveFromSolver_m146DF16BA35D6BBBAE5781504D744C59D61D80FE,
	ObiStretchShearConstraintsBatch_get_constraintType_mC6DEA77B39D02D597E2CA898182A9BD560B545C6,
	ObiStretchShearConstraintsBatch_get_implementation_m0DAC376106BC35C967BE578A40049E3C35E27024,
	ObiStretchShearConstraintsBatch__ctor_m8927550DA897869840F27FAE27254EF2961AEFB7,
	ObiStretchShearConstraintsBatch_AddConstraint_m3EDB080395E5167A460038CE0B00CEEC306C19D5,
	ObiStretchShearConstraintsBatch_Clear_m827C6A5985D286459089DC4635044609BC6B098C,
	ObiStretchShearConstraintsBatch_GetRestLength_mC0368700D80E14C1F758085E1051265DB7E6F140,
	ObiStretchShearConstraintsBatch_SetRestLength_m4401CCB79E8593DE77C2F66191AE967C6DC7E5DC,
	ObiStretchShearConstraintsBatch_GetParticleIndices_mC265643A55371F01BD13A0C3FC34A11D9FFE838C,
	ObiStretchShearConstraintsBatch_GetParticlesInvolved_m304EEFB3092A0112EAE84B4CECF3F6A948D9833E,
	ObiStretchShearConstraintsBatch_SwapConstraints_m2C8677D96612ADAE5BFCC4825A9BA4E42685609E,
	ObiStretchShearConstraintsBatch_Merge_mC487E3A37D51D78F57A1880A27B8168E45F05BB8,
	ObiStretchShearConstraintsBatch_AddToSolver_mA25B51FDA1EBF1E1BE6AD594D45C153216AA8C94,
	ObiStretchShearConstraintsBatch_RemoveFromSolver_m45797BE9F2A7CC2B0BD983E810A313C126621382,
	ObiTetherConstraintsBatch_get_constraintType_m8483A4DC0E6B622F67EC74C1BB113C00981EFA6C,
	ObiTetherConstraintsBatch_get_implementation_mC6C6A9923482CF3A380B6D250E1A63C0EC69E02F,
	ObiTetherConstraintsBatch__ctor_m8866FC2DA610911877A83CE2904D78CACB19FC51,
	ObiTetherConstraintsBatch_AddConstraint_m1BF702B37FD24AD678D201379C614BFA7C24824A,
	ObiTetherConstraintsBatch_Clear_m2F60F212808B2D2566D9486751BA43D287D7A616,
	ObiTetherConstraintsBatch_GetParticlesInvolved_m77FB1AD6E9F76D24F63C6CF2FCA60F8627D497BA,
	ObiTetherConstraintsBatch_SwapConstraints_mE3AD347DF0A9C8FA3D0467F87FFEC0B13FD61E72,
	ObiTetherConstraintsBatch_Merge_m1AD265CC0EE520D80F27DEFBE161FF9E44B80A76,
	ObiTetherConstraintsBatch_AddToSolver_mD57866191E80F88A3D272C0E0AC76364326A80E3,
	ObiTetherConstraintsBatch_RemoveFromSolver_mEE3D5BD7C2CF365225947E773A815095121EF2C6,
	ObiTetherConstraintsBatch_SetParameters_m1577626B4E1C0111CF067DABC451BC0503615466,
	ObiVolumeConstraintsBatch_get_constraintType_m381ED3FD4921DB235D1AE2E0D93D354C840CE86A,
	ObiVolumeConstraintsBatch_get_implementation_mA945BB5C6B7A7EE83AC6C01C6B5DD21207FB0325,
	ObiVolumeConstraintsBatch__ctor_mA44A358BC97BD1830050A02DFFDF5108A6FD9715,
	ObiVolumeConstraintsBatch_AddConstraint_m411C62C1E1117BF0B3B6954C3641F5B418583424,
	ObiVolumeConstraintsBatch_Clear_m53B029F54892E0D5B9D4D2BCB647A4176FE793A0,
	ObiVolumeConstraintsBatch_GetParticlesInvolved_mC38E81ADD236D1D8841141D16BA6989DA3A489F2,
	ObiVolumeConstraintsBatch_SwapConstraints_m42B669FC7CBDFA494210C6486682BD7A3CFFDDB4,
	ObiVolumeConstraintsBatch_Merge_m1DDAB49D1408ADCD12D27C72737072480252DA7B,
	ObiVolumeConstraintsBatch_AddToSolver_m1070A8D58A5B37CC00B5290D1448490F3DD9CE29,
	ObiVolumeConstraintsBatch_RemoveFromSolver_m01AB99DBC04273CEB49EFAAFE595F93D5F05B726,
	ObiVolumeConstraintsBatch_SetParameters_m8B574D29F6430355A0AF20B229AA493E0119AEF5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiAerodynamicConstraintsData_CreateBatch_mD91F60B676E69FE28DD53AA57D5E64E621F7E489,
	ObiAerodynamicConstraintsData__ctor_mD5BD0D829EC39F1CACD35CF16E040852B4B4784B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiBendConstraintsData_CreateBatch_mD0A372B63F6C4F88AC48D4E4E9304944D3FEA0F2,
	ObiBendConstraintsData__ctor_mFE69ED43DBE4731FAAAF8C32658B04596342980A,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiBendTwistConstraintsData_CreateBatch_m9A87A7382C1A63A339F0C8B17CF14D1CB03B0D46,
	ObiBendTwistConstraintsData__ctor_mC6E27E26B877C88510C9E27FB9ED17BF94E98B9A,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiChainConstraintsData_CreateBatch_mC2FB00A47639537C6959AE16661DAAF1AD4C3F7B,
	ObiChainConstraintsData__ctor_m9D1C51412A1878F85D8C173777A60EC621A9FFDD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiDistanceConstraintsData_CreateBatch_mA63574AD8629D5C6907EB15B3FFA9F0D1616D026,
	ObiDistanceConstraintsData__ctor_m10E8E535222BCC55670623BBB7DA36CAF7A2A95E,
	ObiPinConstraintsData_CreateBatch_mC8E21D95D7C5E27097C6B75B20DC74CBDD2D345B,
	ObiPinConstraintsData__ctor_mA86A434A33F8CF37FA8E7E2410BF8223116F03A0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiShapeMatchingConstraintsData_CreateBatch_mDD0753126D9E04AB8FD0D7F4CB4D6C9ACA8BF359,
	ObiShapeMatchingConstraintsData__ctor_m39A87174E47FB428D6589800F2F18FE1F79D340A,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiSkinConstraintsData_CreateBatch_m0420AD7727A873F46A9E43F8BF1F94895AB14369,
	ObiSkinConstraintsData__ctor_mCB9A63E7B637DEAEB1063E52913985321ABBCAA3,
	NULL,
	NULL,
	NULL,
	ObiStretchShearConstraintsData_CreateBatch_mFBE3B0B12D6BD8E226076AB93E16906F92B60C68,
	ObiStretchShearConstraintsData__ctor_m81576073ED6B14D835150C4666A3588AEDBD29FC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiTetherConstraintsData_CreateBatch_m28C477BD204B78B77C867473CB959C5340B918D7,
	ObiTetherConstraintsData__ctor_m7488642C0CCCDA9C252605F19F887C961D3BB5C4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiVolumeConstraintsData_CreateBatch_m1DDE0FAC895B8FC54D095EB773D7AEBDE107A061,
	ObiVolumeConstraintsData__ctor_m09F646B51D450E9B87B194F9E29B8B5FE9C2404B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	StructuralConstraint_get_restLength_mA86CC2512F19A0314F76C459C7027A50DEB66F57,
	StructuralConstraint_set_restLength_mFDDB0536829F1845FAB3F39B80ED0520431D2BDF,
	StructuralConstraint__ctor_m9F7FF404C8685160AC46543FCE7681E91FF6B322,
	GraphColoring_Colorize_mC1E72C7E56EE6F1F72682546D5CB94934287958C,
	ObiActorBlueprint_add_OnBlueprintGenerate_mA116B88726BA07CDAC7EA40506772CC39F8FF86E,
	ObiActorBlueprint_remove_OnBlueprintGenerate_m780AFE9B203E214D86769A37EADEE63D5FC9967E,
	ObiActorBlueprint_get_particleCount_mCC6F47C83D7F2BE2BBF1667511B5C96BAA5A94B2,
	ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A,
	ObiActorBlueprint_get_usesOrientedParticles_m8E951A7BDC8EEA54633C2AA8BE8E41F01A418233,
	ObiActorBlueprint_get_usesTethers_m7A16422C66E95F3984D2A3A8F950305E9D5C2026,
	ObiActorBlueprint_IsParticleActive_mD079A02C9E95DB8F8900FA0C1FE9A057C1F1D05A,
	ObiActorBlueprint_SwapWithFirstInactiveParticle_mCBD4A74BAC682B6DB6228F3B53FCC5D90B069126,
	ObiActorBlueprint_ActivateParticle_mA772B1D5DA1364AA8A889F069D9F903E565A3F02,
	ObiActorBlueprint_DeactivateParticle_m2EFB2F10CE118C9041593C482C6FC80F24A9C8A7,
	ObiActorBlueprint_get_empty_m5407EABADC0A7CFE5EBBA320717C3B4609F2FDD1,
	ObiActorBlueprint_RecalculateBounds_mC32E8C37E376B8342D46E8894A36161EDF37E48A,
	ObiActorBlueprint_get_bounds_m8F16C4CEAFB92614DEC83B9F41D565B4FE542C15,
	ObiActorBlueprint_GetConstraints_m1C48E3155094EC159203D4F23B25015FCA6F0597,
	ObiActorBlueprint_GetConstraintsByType_mD32BF0C8BE5453BF0F534BCC92F2BD30477E7761,
	ObiActorBlueprint_GetParticleRuntimeIndex_m6495778E4FF39D59DA86401684ABED0361E6803B,
	ObiActorBlueprint_GetParticlePosition_m4B9F2D62EEF90A610C7DC8E1B07D4525ED90A081,
	ObiActorBlueprint_GetParticleOrientation_mE80B9B4333A1EE1670B7DAD73FD55F8C8F6F178D,
	ObiActorBlueprint_GetParticleAnisotropy_mBFB7DD70BF52D6DEC5E08DEFC5A8F07BBFCBE6B9,
	ObiActorBlueprint_GetParticleMaxRadius_m1106A97F11068907833E40301081A52749BC79F4,
	ObiActorBlueprint_GetParticleColor_mA1CA0E89A089B4CEBAFFD4EB83CBFC5EB5A6EB0D,
	ObiActorBlueprint_GenerateImmediate_mD3FF977E24F4965AB776BF1E1DF0272F3D6CCA33,
	ObiActorBlueprint_Generate_m14C8F36BC89354F15F0F82AE9546B5024F47FF7A,
	ObiActorBlueprint_Clear_mD6317D8274462CCEF04D76D0957286FF73D692EB,
	ObiActorBlueprint_InsertNewParticleGroup_m5C512B2A9ED1227695249B3DDC9C19A9D421AAAD,
	ObiActorBlueprint_AppendNewParticleGroup_m33A05025BCF9CD281364DDA0E3563AE5091E6D79,
	ObiActorBlueprint_RemoveParticleGroupAt_m583F9722B398FB85BFFB3AA17AB4E53424FD4E18,
	ObiActorBlueprint_SetParticleGroupName_mEC41E6FFFC12BB73B6BA214F81CF70CE0A034A34,
	ObiActorBlueprint_ClearParticleGroups_mBC589A70CC5E642C10FD126B482590D27FEC8B18,
	ObiActorBlueprint_IsParticleSharedInConstraint_m9F71E2E0E9B16D9CCB47B7D270391F65232D64A1,
	ObiActorBlueprint_DoesParticleShareConstraints_mCCD929D6E8D050B9B2A377A3965B6F8E64538883,
	ObiActorBlueprint_DeactivateConstraintsWithInactiveParticles_m2F8B411DA9FE9FCDC8116158247E0E83EEA513E1,
	ObiActorBlueprint_ParticlesSwappedInGroups_m9104E8E53B4D76CFFD62EF8277E5313A73867EE5,
	ObiActorBlueprint_RemoveSelectedParticles_m1FD86679B5F6C5DEDED32F79658292F13566C63F,
	ObiActorBlueprint_RestoreRemovedParticles_m6968118C8E330062E76B6F79155E07B86C091400,
	ObiActorBlueprint_GenerateTethers_mE0CECBEF8A33A7E44D7EFD24A87697C45209B668,
	ObiActorBlueprint_ClearTethers_mA11CB91D97A72D5311C67FCC335ACA3900020EEB,
	NULL,
	ObiActorBlueprint__ctor_m9D95F18391AC084CE03CF008E1D47864DF75A636,
	BlueprintCallback__ctor_m75F65AE60505CE5651A0F020168360371AD0D0EE,
	BlueprintCallback_Invoke_m0F0A317733BA923A00DCF82C10E2AF745667377A,
	BlueprintCallback_BeginInvoke_mD8A830F18B9F7B76E81DB436AF1462092202B3F3,
	BlueprintCallback_EndInvoke_m35D1EBBFF55BAC420142369AEB4B24D44D031B3F,
	U3CGetConstraintsU3Ed__50__ctor_m463D73E183DDE1A5C71E6E3CC821D0E7F30E347C,
	U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m1C1B68AF4C49065E226E78E8D46FD68A406FC4DB,
	U3CGetConstraintsU3Ed__50_MoveNext_m440BD75AC003FACCE06308279346A5432EDC9BFC,
	U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m603F544C865B5A382E4D45071F9EA103E35296E2,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m8E40706068668910D9F7D8A1EACB85F8E6FFF77A,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mD7F6DAA1BCB1AB519151F38ACED86E62BB422730,
	U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_mFFFD55BE544D6CB567D145A4DCB2564DD48B4CDF,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m3284B5FE565ECE74AF7F1FBE65BCD30C41CADA8D,
	U3CGenerateU3Ed__59__ctor_mD0933DCC8582C89719A5869AA40F1D96BC46A2A1,
	U3CGenerateU3Ed__59_System_IDisposable_Dispose_m4C2610F3D57B4EB14EE56E2E116536B5A23C8B22,
	U3CGenerateU3Ed__59_MoveNext_m1B248A89761AD00C954EAEB25335090B6378CB9E,
	U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7A53073B04207358D27C5F279097C3AECBDC48E,
	U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_m326C85F4CC091D9CEBD26372E5491D58808FECB6,
	U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_mB78D5553C5C65C889A8A4E2F6FCCADA45472D734,
	ObiMeshBasedActorBlueprint__ctor_m68ACFAD2ACE6D36D748D434E2BD130B742B17BE8,
	ObiParticleGroup_get_blueprint_m7B969093BC47FD5B58C6D6839D8F9E3BDA3119FF,
	ObiParticleGroup_SetSourceBlueprint_m83016ECF67DCB8BDF873C154BB4A0EE7A8091011,
	ObiParticleGroup_get_Count_m98490363FF19DE1FFEBC361D7302AEF05B6F21B4,
	ObiParticleGroup_ContainsParticle_mFF1BABB2B3D4D3CD4AE1E5B44C3DC83765EB11AC,
	ObiParticleGroup__ctor_mD6FDCA405D8CBEB22044CFD3635E3202AF1A45A2,
	ObiBoxShapeTracker2D__ctor_m628217C8133AD0BA22DE8815770C03CD31236C8F,
	ObiBoxShapeTracker2D_UpdateIfNeeded_m7CF0C0561350D4A21F8FC768C7158E4BAE328434,
	ObiCapsuleShapeTracker2D__ctor_m9F0647AFDA77ACDB5B00DDA8F614F2CD3F61F464,
	ObiCapsuleShapeTracker2D_UpdateIfNeeded_m65EF7C477B78DF113F5C43446E266DA9790B333A,
	ObiCircleShapeTracker2D__ctor_m91428FB089519F84073171B1EF2AF27FA7029F13,
	ObiCircleShapeTracker2D_UpdateIfNeeded_m855CD60D865E47BD776A38CE2F44554BD5B69E49,
	ObiEdgeShapeTracker2D__ctor_m9CF5E0F349BC73573202F01E32482578DE1C1A10,
	ObiEdgeShapeTracker2D_UpdateEdgeData_m7208F5B1FF03775D5A425836047FB54E9BB747C1,
	ObiEdgeShapeTracker2D_UpdateIfNeeded_m380635280878C5D86F90898E11CAD1664E3E4BDC,
	ObiEdgeShapeTracker2D_Destroy_m01DDDC54A11D898BAABA7391098DD4D2458CA946,
	ObiBoxShapeTracker__ctor_mE75CAF1F86BB53D6AEF98085C09D17E66DEEF582,
	ObiBoxShapeTracker_UpdateIfNeeded_mF819B8E4C30C6AFAF29A4117798FDF1316EA834A,
	ObiCapsuleShapeTracker__ctor_mBE6839D0297E242539ABACA7E8D10CC7D1FF90BD,
	ObiCapsuleShapeTracker_UpdateIfNeeded_mE44AC47537FB4437697D85609FB76AC306E29C1A,
	ObiCharacterControllerShapeTracker__ctor_m58178DB03B830C4137C4E297650BAB50EFD3625E,
	ObiCharacterControllerShapeTracker_UpdateIfNeeded_m37B806A39DB2EEF9B54A5B5F191B7F9C7602E517,
	ObiDistanceFieldShapeTracker__ctor_mA5B4583A9547EB0FE0C666EBB88E671DEC7F9035,
	ObiDistanceFieldShapeTracker_UpdateDistanceFieldData_mA701C6AE3562FD84B9C2C56A60A80A88D8B961E7,
	ObiDistanceFieldShapeTracker_UpdateIfNeeded_mA4E6477716FF64D462625E6D8F4898A7E47BD384,
	ObiDistanceFieldShapeTracker_Destroy_mF2D78CB9BA87BFF2D01E82CA3796B3E975B387C3,
	ObiMeshShapeTracker__ctor_m08CD689CF8203745944B75624F6F10B9B5991B41,
	ObiMeshShapeTracker_UpdateMeshData_m3FE402BFC2EA1B1AEF97C10345A781D1F1453374,
	ObiMeshShapeTracker_UpdateIfNeeded_m12F1895305D96BA383B3C6D04D0960CDE65BB9DD,
	ObiMeshShapeTracker_Destroy_mF175565DFFD64344104928752D528653AEEF4C1D,
	ObiShapeTracker_Destroy_m0FF375956D9FB5363A7595568F62FC49288D102A,
	NULL,
	ObiShapeTracker__ctor_mD3C85F1D395CDC2FA8F0FADBF02F5035493C1A7C,
	ObiSphereShapeTracker__ctor_m1F91597098DFDE9D3D331BB46F9885EF84816FCC,
	ObiSphereShapeTracker_UpdateIfNeeded_m956A2557E0849CFD2C667CED772595BB3DB2517F,
	ObiTerrainShapeTracker__ctor_m7BEE7A899503A6A8D1B542BB48B2902966AB22B6,
	ObiTerrainShapeTracker_UpdateHeightData_mF979C90B8BF4BAA1D77BB55829060BE1D02DB1FB,
	ObiTerrainShapeTracker_UpdateIfNeeded_mF4287DBBCE367005E4F1EF1342AB50882CDBE460,
	ObiTerrainShapeTracker_Destroy_m778990BC8F8E80F2EAD34786597F7803DA9E55EE,
	ObiCollider_set_sourceCollider_mE74FCFF39705097FEAD7580CD74ADA13071B3CB1,
	ObiCollider_get_sourceCollider_m9128E7F1E524ACFDD350E7347570A6253DFDB19D,
	ObiCollider_set_distanceField_m1E7758FFA96690595BB90B1BEB48B7AD958EB53F,
	ObiCollider_get_distanceField_mC9798980E3932FBA4649A14AB780CA819D06B050,
	ObiCollider_CreateTracker_mD3862A5A96ACE781C43578C71C7F3B34597B72B5,
	ObiCollider_GetUnityCollider_m8F9125F3CE4C77B54A293FCB58A506F9003F1645,
	ObiCollider_FindSourceCollider_m637BA0D9C7416963F07A6C91C46898E63A2AA7E6,
	ObiCollider__ctor_mC40B217031806CE6CC171DB8519F2BDC13237937,
	ObiCollider2D_set_SourceCollider_m1018D5A5D84BEE9BFF223E7C96DFEF6391A61025,
	ObiCollider2D_get_SourceCollider_m0F0FA6FE50C824AA1D508B0624CFC78BC5CB1324,
	ObiCollider2D_CreateTracker_mF85B5C235CC7FD0AA628697696F24E75B0677D95,
	ObiCollider2D_GetUnityCollider_mC7022D7CE3DD5DAFC483FE44489C1826C6F719A9,
	ObiCollider2D_FindSourceCollider_m1C3542EAB959FFB8DD376825B89F1FCF6EF5B71A,
	ObiCollider2D__ctor_m1D8E050EE358DD5E91FBCBC06CEEA010D1096877,
	ObiColliderBase_set_CollisionMaterial_m79AC8EC4C2B24D56D2EEB8E7E3EC8758D71DBCEF,
	ObiColliderBase_get_CollisionMaterial_m0DF69ECB8E1D3B265A93C3411558938452320DB8,
	ObiColliderBase_set_Filter_m7EFACC9D0DDC53860DD8EC7711CA2B537861B143,
	ObiColliderBase_get_Filter_mC113E8E2C6A721BDC36A1380CDA7B1274877FA9F,
	ObiColliderBase_set_Thickness_m8B643679A1B8F3502E0983283BFAFC788F59FED7,
	ObiColliderBase_get_Thickness_m39EFFE2661BFC77996AFE413C11C9842813B0CCD,
	ObiColliderBase_get_Tracker_mC248A17400AE5158EBE13D85059023F2777EF96E,
	ObiColliderBase_get_Handle_mC54EFB3D8F0D65205AECE01C1523F9684610FDF9,
	ObiColliderBase_get_OniCollider_m1A453A14EEE1A71AD174EA6FD490CBF11FB13516,
	ObiColliderBase_get_Rigidbody_mFE29C0B5E72DE47B2F8CECC10D691E3CB1113260,
	NULL,
	NULL,
	NULL,
	ObiColliderBase_CreateRigidbody_m28447722765BD28A1F74FD197E44CD73585CFA55,
	ObiColliderBase_OnTransformParentChanged_mB24B89C5A1D09A5CDE05559DDDE3AE584CB45385,
	ObiColliderBase_AddCollider_mAE172D1DDA35D52A9D36D68E68CB739157F34870,
	ObiColliderBase_RemoveCollider_m95F3C0AD19C848FFF82DF5DB8584D5E949F808DC,
	ObiColliderBase_UpdateIfNeeded_mF9F82931F7D1F7703B77125BB3612E1781EF4622,
	ObiColliderBase_OnEnable_m9D06C6503D3AA87D2ED910B831C6927655449D5C,
	ObiColliderBase_OnDisable_mDCA28DAF071BB2F8AB35E5A290B1935BB47F320F,
	ObiColliderBase__ctor_m0425A5B3CFADDE3908C4CDE7564045D2E49156C6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiColliderHandle__ctor_m928271CA0B5407C025BBCC1A4C1B41A772842B98,
	ObiCollisionMaterialHandle__ctor_mF9EF7E13E41C1CFA35A66984FEAE0EA7A6E01622,
	ObiRigidbodyHandle__ctor_m7A187DF1938668726DB0353C6DF5A404B9661FDB,
	ObiColliderWorld_GetInstance_mDCD3CE9448DD243AF9314FC3AE6EFF25A0A9D804,
	ObiColliderWorld_Initialize_mA686CF0330B087A58794A38B02A5C474C0ACE80D,
	ObiColliderWorld_Destroy_mB2941E38388F04B017C98D209866BF6234D85C66,
	ObiColliderWorld_DestroyIfUnused_mF7FB902FE93CE094938D57DC0048249F810AFAB0,
	ObiColliderWorld_RegisterImplementation_m04E6D07B4FE9DB89CB602DDE1177100646AB5F80,
	ObiColliderWorld_UnregisterImplementation_m1FB0CC2F87786339836A3FE23662CE7E87AFD313,
	ObiColliderWorld_CreateCollider_m209298226F546EF09F80E9DC3C9CEDB9EA6A3ECF,
	ObiColliderWorld_CreateRigidbody_m1A6A020A8B5E9F2333767813122FAD64F13E58C7,
	ObiColliderWorld_CreateCollisionMaterial_m88809726D6F5777CA5CA979375202E079FBA523F,
	ObiColliderWorld_GetOrCreateTriangleMesh_m68BA906C938414D9FA39EF9434D16EDD5BC6B008,
	ObiColliderWorld_DestroyTriangleMesh_m0BAE4F6D11B77B8B13C2D4C1DE5A6140E76E582C,
	ObiColliderWorld_GetOrCreateEdgeMesh_mD3188273479E4DC678356643F456C0AA97BFBD1B,
	ObiColliderWorld_DestroyEdgeMesh_mC35DD757DCFE047FCE86C3190EEFA0B19A891306,
	ObiColliderWorld_GetOrCreateDistanceField_mD90630455B72C5BAD3E94DAE4F3FE4E979E2C431,
	ObiColliderWorld_DestroyDistanceField_mB645511AF520AEF6AB93B8EE03F0A2A7D62ECEE8,
	ObiColliderWorld_GetOrCreateHeightField_m67768E9620255D9407B6D664A21B5F46A06DA9DD,
	ObiColliderWorld_DestroyHeightField_mEDED727FD4CD60E71555E1F0B7223CF0C2F060D7,
	ObiColliderWorld_DestroyCollider_m834E6E9C211C47C4746C0658D072D11488444C7D,
	ObiColliderWorld_DestroyRigidbody_m06DB0A63D4471A043E38A1BFEC429E693E36E82B,
	ObiColliderWorld_DestroyCollisionMaterial_m95080D33292635B836E68FDF7DBD877FF51A3302,
	ObiColliderWorld_UpdateColliders_mFE5C2781107A2BF876523BC7D1A371930FA231DA,
	ObiColliderWorld_UpdateRigidbodies_m8561FC0652FF0142400D2886A02C9EFAEB685819,
	ObiColliderWorld_UpdateWorld_m864B8D4C74E8309F9FA29064D39B1A37FCC9538D,
	ObiColliderWorld_UpdateRigidbodyVelocities_m98240C979197C4339F335E642B3DF480F7A281E3,
	ObiColliderWorld__ctor_mB72724E3C4F8ABB4A7845EA540DF7FE796E1E971,
	ObiCollisionMaterial_get_handle_m594CB055313A49F8543490E78B1F48E7C21C7462,
	ObiCollisionMaterial_OnEnable_m8FA0F5FEF8BDA6E093C8A903CD09B45F107B87F3,
	ObiCollisionMaterial_OnDisable_m835688D4697C04CFE7B9F09ECBA5E0DD0201EE4E,
	ObiCollisionMaterial_OnValidate_m39E140FEFC698F6790902D5698BCCDAA4C037250,
	ObiCollisionMaterial_UpdateMaterial_m01C9A16AD952A5AC4E018F035CD98AD7C3F7D6F2,
	ObiCollisionMaterial_CreateMaterialIfNeeded_m7B6E786F93918CFCBCAFE79D73C6D39DB7D2E467,
	ObiCollisionMaterial__ctor_m72400798714AEFF19085712777080ABFBB13BAC6,
	ObiDistanceField_get_Initialized_mAABFFF3B58AB387E5CB5FB8C1AB746B0247F983C,
	ObiDistanceField_get_FieldBounds_m9863DC2138A3DBE77E04380C54236345A1133CBD,
	ObiDistanceField_get_EffectiveSampleSize_mB9F3DB29086DB4AD2249B28C1C8C888484833AAD,
	ObiDistanceField_set_InputMesh_mD2BFC4B7BAE4B8EBEFF52AC8E89CF319F03D3780,
	ObiDistanceField_get_InputMesh_m9B9ABB7CAE379468D3FACDBD886581F843FA22B5,
	ObiDistanceField_Reset_m00EC5397C61DF8D42A0E2201EBE65C30AB391DF8,
	ObiDistanceField_Generate_m2FF083D72E253BE27D9DFD07C8816B449F778266,
	ObiDistanceField_GetVolumeTexture_m3C63485A3F2BB3E0D84E496DFD190836DD7C9A64,
	ObiDistanceField__ctor_m79451FE12A75F2ED6B511F1DFC382E6493E09DB2,
	U3CGenerateU3Ed__16__ctor_m926CEA2198C2B31432AD7B0C629E0026FD9135A8,
	U3CGenerateU3Ed__16_System_IDisposable_Dispose_mE8B29A18143A848E1C4C46A4D41CE2EE66999F73,
	U3CGenerateU3Ed__16_MoveNext_mE09C7584F62ED7F884B6289D784C5474BAB6648D,
	U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39320DBD9BADA847C3DC1B06385734720B3ED5D1,
	U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m94171808CA1CAE76B9F0511FE6570131CD857EBB,
	U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mE403FD60E16DACEE30BE06E73A882BBF823618FC,
	ObiDistanceFieldHandle__ctor_m6E0E72314F9B3CD649F6E52743A19A05BDD53486,
	DistanceFieldHeader__ctor_mD9023657AE562DC6E93CAEBAEA87D8BBFBE3CAC0,
	ObiDistanceFieldContainer__ctor_m3770E97A1E94FE7F79D6C4476A4B5538615321FC,
	ObiDistanceFieldContainer_GetOrCreateDistanceField_mACCA0F4E84E3D53991F18FEC18CCC8230451C913,
	ObiDistanceFieldContainer_DestroyDistanceField_m85914D4DE23293E844CC8082A6B7B4F427C8505B,
	ObiDistanceFieldContainer_Dispose_m7A92A1441B9CB196E1D25BF4587B01FDFF2CA2A8,
	Edge__ctor_m58FEEBE96D412AFED6635E824686137251119088,
	Edge_GetBounds_m3627A8DC0128EA13268432FD42FDFFA5E9E8FC16,
	ObiEdgeMeshHandle__ctor_m055ABDB8025F78423ED6926D1E316EB48FABC4AE,
	EdgeMeshHeader__ctor_mFBC1ECFE2844D2E52FDDEF03C23174C84E12C124,
	ObiEdgeMeshContainer__ctor_m7E7182023FCAB6FD0840A93D3F33CD76868DEC2D,
	ObiEdgeMeshContainer_GetOrCreateEdgeMesh_m08E80D7BB9E4C1AD3040ECCB3E3F80BB0EBC5127,
	ObiEdgeMeshContainer_DestroyEdgeMesh_m6A49B7BD3DACF76A1B0E7F591A523795F55758EC,
	ObiEdgeMeshContainer_Dispose_m1005586E5D7BE477D43008737857278C3088223A,
	U3CU3Ec__cctor_m319B04707AB4F191DD80BB15D8CE099EE0A1A7B4,
	U3CU3Ec__ctor_m0A7F956BDB67E4A8C2BCD9D83C276D02DC561279,
	U3CU3Ec_U3CGetOrCreateEdgeMeshU3Eb__6_0_mDEAC05CDAF4D92AFBB301FED8F1C6099004401DD,
	ObiHeightFieldHandle__ctor_m7487BDAEEC65A9BBFE0A38658CA884E309BC6FEA,
	HeightFieldHeader__ctor_m68533ABFD4D9CB9C9AE92C04D96F69D03704B421,
	ObiHeightFieldContainer__ctor_m5666C4D77395B12D8510DE3C1C91F7DF7080917E,
	ObiHeightFieldContainer_GetOrCreateHeightField_m24A1A0266B0B0558347D317ECAF37AE00CF5F73D,
	ObiHeightFieldContainer_DestroyHeightField_m0C864AEC3E3DF063366196C10432FC0E9582C124,
	ObiHeightFieldContainer_Dispose_m381A36CB411FE4B112A455EB52BEF459C07B542E,
	ObiRigidbody_Awake_mF76BC3EB63FCF9C9203C0490FB685630EA22B0E2,
	ObiRigidbody_UpdateKinematicVelocities_mCB621E407E54750602D501EF243278687D054900,
	ObiRigidbody_UpdateIfNeeded_mE8E08B343D9013FC5168AC526FDD2FD60713C63F,
	ObiRigidbody_UpdateVelocities_m8383481CAC453BA89BF272E3DA3AC19A575ED21F,
	ObiRigidbody__ctor_mD7D0C954158E4444C3068E83B3327DF0C19F22B4,
	ObiRigidbody2D_Awake_mFBE100A1729F05F2F8C0D6F81A5D55A200C18D8F,
	ObiRigidbody2D_UpdateKinematicVelocities_m9FA8BC7541C8841BA05CEFB2E5153BEF8D0F0998,
	ObiRigidbody2D_UpdateIfNeeded_m2B7DDB040171C3260191C90FCA6E67EA1658C21F,
	ObiRigidbody2D_UpdateVelocities_m9E81C9B71FCBB4C7F67940267609771648B6FC02,
	ObiRigidbody2D__ctor_m4B1B8A6B4DC629D021CE278C6D8926801ABF2B5A,
	ObiRigidbodyBase_Awake_m76001FC03150D7505A5D630EA20933DF245BA3C7,
	ObiRigidbodyBase_OnDestroy_mE8BDF21E79746CC2EF9F1CD492C425FE5FD46159,
	NULL,
	NULL,
	ObiRigidbodyBase__ctor_m97C098719F7244A8923BA99EB88178D5BC81E375,
	Triangle__ctor_mF3FB7D0163513605C1284E55D476938CFA8D8B3E,
	Triangle_GetBounds_m96DD7752DB622FBB5D130CC0203D16EF9A16183D,
	ObiTriangleMeshHandle__ctor_m4C8FBE34410F6DE0216D655610C66D865609FD82,
	TriangleMeshHeader__ctor_m05352EA77E9896540763C1FC4DA9A86E8FD110E4,
	ObiTriangleMeshContainer__ctor_m7744CBB6264FB551DA0E62E0AE2B14750075FAFC,
	ObiTriangleMeshContainer_GetOrCreateTriangleMesh_m7966A3DEF57A37FDCD4EAB1254350FCF3C443C54,
	ObiTriangleMeshContainer_DestroyTriangleMesh_mC9C784E4C7DD7154C4F28D56E3D49B0CCFDFA4A8,
	ObiTriangleMeshContainer_Dispose_m5B8C0A01AFA58E44642169B3B0230566C3901B80,
	U3CU3Ec__cctor_m78E18BF63E0FC93B04931361A400A550DF8E42FE,
	U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8,
	U3CU3Ec_U3CGetOrCreateTriangleMeshU3Eb__6_0_m55F60D5CC64D104ED8E0B37F968D4479C3124E42,
	ASDF_Build_m0B3E28D5C89253E2A2FAA9545A0B748B5C69E1D4,
	ASDF_Sample_mA68406A166BFA7E95D0C62E769CB30FC8F2C78C0,
	ASDF__ctor_mC6D0B839501A5338ADE298E18D27946D91973849,
	ASDF__cctor_m3BDC04F77E29E81E3A541CFA21851E2153E7EC1A,
	U3CU3Ec__cctor_m02BD313FFAEEDA3D68EE3E46312C75EB44777102,
	U3CU3Ec__ctor_m4CEC5676823706F8C33924A47217BB2E3C584BB8,
	U3CU3Ec_U3CBuildU3Eb__3_0_m2F53BFE53C74A23E70A52AA23A3625EA199232CA,
	U3CBuildU3Ed__3__ctor_m2AF6E46C3AF1DCE8EB0AC87345B0082720891BF3,
	U3CBuildU3Ed__3_System_IDisposable_Dispose_m407D5BEFA6CC050E62B83A69FC1C81FA11062FE4,
	U3CBuildU3Ed__3_MoveNext_m2878CF634F624CD9D2E068EB30A27BFF838B1AE9,
	U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FD71D70EB820213EDF7BC3853561DF86E43D30D,
	U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m026B7C1E35013639385A979664E9479DCFB7269A,
	U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_m8730288CA28692A77A479C27FB99AFB9421E825B,
	DFNode__ctor_m5AA5A22FD71D7439C9136C2FBC6F151E25C8A609,
	DFNode_Sample_mD228ACC5980D03D69BA1510383BA88AD4253D1D3,
	DFNode_GetNormalizedPos_m48205296253B504FB1F9D3177EADD8E62A70F31B,
	DFNode_GetOctant_m8E57E952AB3E07E8596E051854C4B9EF62575192,
	Aabb_get_center_m6F29EA934C6E7C6A2557550214746BE327737113,
	Aabb_get_size_m316B3CACF5BB496D6CF8532AF06AF826C78486D7,
	Aabb__ctor_m8B1C8639B79DD7C951EF63FD820BB63B8A6B5DB4,
	Aabb__ctor_m293B0520F59E98F3B14FEAFE282685CA663D2E0B,
	Aabb_Encapsulate_mDCE164B716BFA4A7F8147BC997DDDAFF5A0A9F08,
	Aabb_Encapsulate_m4F8536D0A22C6A596F1393D02F97BA7C45B9F9A8,
	Aabb_FromBounds_mB313907D1B69DA3917270DE5C2B39AD5ECCCF664,
	AffineTransform__ctor_m1113EA596451380FE97BF986F98D180B67B50914,
	AffineTransform_FromTransform_mF590DA4EFBCE17F0245A9C33FE65B7405154A7EB,
	BIH_Build_m543AD7EA6BD7D11FCF4DD88B06747F4B797784D9,
	BIH_HoarePartition_m26E2AC749940517A897AF2963BE5322BB38A3CF9,
	BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905,
	BIH_DistanceToSurface_mAE13B1EFAA791602268DFF2A5ED429691EA76178,
	BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05,
	BIH__ctor_m6D1B7878A01D15CA2622A366286C9308BFB07727,
	BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_mED2F171342075378B5B140C14A73A72EF8ACC45F,
	BIHNode__ctor_mBE805A0C558FDCA2FEDE25366ED721EA3776834A,
	NULL,
	CellSpan__ctor_m93CAC71E3E1DC790B927C8C76F72A3784072746B,
	ColliderRigidbody_FromRigidbody_m4921C838DA8C3C147A91168C9232CB97780260BB,
	ColliderRigidbody_FromRigidbody_m2311F1A25138EA07373D405B7EF2F1EB3859138E,
	CollisionMaterial_FromObiCollisionMaterial_mBF4E29EA56479F76E396B2B5EA9BDEFED6663530,
	ObiNativeAabbList__ctor_mCDE9CAA207D408AC843A08BBE600AB4F0D6B75D6,
	ObiNativeAabbList__ctor_m59E302FAD9AEFD8A3BE4E2C51B322FB0420E1E2F,
	ObiNativeAffineTransformList__ctor_mA3B6D7D19DD4E487461C969B9C76DDCAF9564018,
	ObiNativeAffineTransformList__ctor_mCB63BE2A5D0F613E12C647669D05EB6BE52FC83E,
	ObiNativeBIHNodeList__ctor_m9615EC2E166FBD0154489436203D9AC793BFB907,
	ObiNativeBIHNodeList__ctor_m6CB7A19C221489E05D62A7996A07E287439F3D73,
	ObiNativeBoneWeightList__ctor_m3412BBD683C143994278F1461106F9696B1D6835,
	ObiNativeBoneWeightList__ctor_mCDE322908FD4B65971533ABDFB277CDB0DF40B49,
	ObiNativeByteList__ctor_mE766091F60DF309D51D8C33F4C12AF80D4B9EB42,
	ObiNativeByteList__ctor_mEA31E3AD637138D0161DB670D7DF7ACDC613B402,
	ObiNativeCellSpanList__ctor_m7A0292E9784EED2B0011E83FA5A7D22EC9DC9EEC,
	ObiNativeCellSpanList__ctor_m9810884859E402D959A6369EEBD22320DC40D2D6,
	ObiNativeColliderShapeList__ctor_m9E21BF5F9EF261AE3C9C92E53F15264713584DC7,
	ObiNativeColliderShapeList__ctor_m79BC575A7059E83119DCE5DFC6D7CE8E308245C3,
	ObiNativeCollisionMaterialList__ctor_m1C70BE9BA86B92206DB617392F15D7B8BDEC9597,
	ObiNativeCollisionMaterialList__ctor_m90089B05CB810F2C23D47F0077910F26FCC61888,
	ObiNativeContactShapeList__ctor_mD865C20C0CFC9AE7557CD251D78C03101CCF2695,
	ObiNativeContactShapeList__ctor_mDB12EB79069ADCBD7B0F4377C8C5A1545F646DE8,
	ObiNativeDFNodeList__ctor_m9C5F3593C316319486AAF621BDD3A6C77A7AF4ED,
	ObiNativeDFNodeList__ctor_mCDC4549E10B99A08FD83058EFD46F45299C71FEC,
	ObiNativeDistanceFieldHeaderList__ctor_m12DE4CCCF75D20BB404B8083EF45503F964599EB,
	ObiNativeDistanceFieldHeaderList__ctor_mF1E073829DBEAD5BC9ECC60BB9AF6463019282EC,
	ObiNativeEdgeList__ctor_m376EED5071F0D70AB6595F5C98AC9FB16FCCCF46,
	ObiNativeEdgeList__ctor_m1DFD5ED07389DD12179C9D3494C4E2BF3D62D563,
	ObiNativeEdgeMeshHeaderList__ctor_m7A49700958AB248E4ABC1CCD1FA83731241FD1DA,
	ObiNativeEdgeMeshHeaderList__ctor_m372AE7E0664D6BE77453AB0C52023FC77C720E8C,
	ObiNativeFloatList__ctor_mB44D5F56545917B35AD1ECA717DF61D0C12047DC,
	ObiNativeFloatList__ctor_m109DA72958D0EBB1AEF825F9A43C34F2BD1A42E5,
	ObiNativeHeightFieldHeaderList__ctor_m0ED84840FACD5EF875EE422AD5274E42CE87D779,
	ObiNativeHeightFieldHeaderList__ctor_m5EC50E82D2C0FE9DBB46618885DE4D9A4FB490A1,
	ObiNativeInt4List__ctor_mAD172784A90BDA971380CFA6F7BECB381A268EA6,
	ObiNativeInt4List__ctor_mF3060DFCCBC09AF993C3C96608B51FA83537EDC8,
	ObiNativeInt4List__ctor_m24F99A45DDF792A152AF6A140C910F32C460E40A,
	ObiNativeIntList__ctor_mDF10DEDB2E6CE5F2D27135F263B2DB96A9123F0E,
	ObiNativeIntList__ctor_m25ADBFB84E370EE592FFF05D343541D84462FED6,
	ObiNativeIntPtrList__ctor_m2188368B2E04FE7C32F8D069405C9F483C27D465,
	ObiNativeIntPtrList__ctor_mE54F7AA88D3E5B4AD478C37640EDFAD145057FB8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiNativeMatrix4x4List__ctor_m8315DA2799111EE4624C6AD421E912B9E2C0D775,
	ObiNativeQuaternionList__ctor_m07E6B8255331075366483E69997B1E8FCC8812EE,
	ObiNativeQuaternionList__ctor_mF8798CFC21A168F77FDE7E4E433E4ED07BB42B0B,
	ObiNativeQuaternionList__ctor_m2733D11CD8396D239DBC16995D1E9404EA29AADA,
	ObiNativeQueryResultList__ctor_m5B404B4FBEB82A5A119AC92DFC671C161598266D,
	ObiNativeQueryShapeList__ctor_m1FC70B84E766982F194F6DC2FE87D15DA79493E6,
	ObiNativeRigidbodyList__ctor_mE10B19341E895E8DD38465DE2FC0E2EF5FEC1FA9,
	ObiNativeRigidbodyList__ctor_m21C628B6ADABDBDFA0D64D291A00D1BBC923B686,
	ObiNativeTriangleList__ctor_m5FDA4E153D9F3E8B6479666E321DDA194CAA2A25,
	ObiNativeTriangleList__ctor_m9FD16BE51A73FCC0ECD34FE1E3E60DE410644782,
	ObiNativeTriangleMeshHeaderList__ctor_mDAD0784830EED0C0268F2009CEC90E04EB39B72C,
	ObiNativeTriangleMeshHeaderList__ctor_m4F5742A78ACC6F92F44E484750F2710E45D32A8F,
	ObiNativeVector2List__ctor_mC9341AEC2A2B1610138A7763453BF7CDC2512299,
	ObiNativeVector2List__ctor_m3326D8FF9523937A0D58CCEB0AA53A7F715AB740,
	ObiNativeVector3List__ctor_mC37EA7399E0090469F5014DE5EF277117A8B40E4,
	ObiNativeVector3List__ctor_m454CD1792A828386A51E51AC80EF30B4EC59DB25,
	ObiNativeVector4List__ctor_mC18F1ECE7AE0C5F9E3D080D41011CCE123F29C55,
	ObiNativeVector4List__ctor_m4E5196643444C3F0983F4C1D35EC054085549834,
	ObiNativeVector4List_GetVector3_m8EAE7F64879DD9A415D25E05C258ADEBC921A55D,
	ObiNativeVector4List_SetVector3_mEA4A6008125CDE1854000FBF64CEA0887E8A7D94,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ParticlePair__ctor_mDDAD78C599D557D2A63715207CBC71A5A93581CA,
	ParticlePair_get_Item_m96F244D843B43EE4A0E48128BDAE3837B0574AF9,
	ParticlePair_set_Item_m39EFBD38D2AB94AA009AAF1C18B7EA40077EEDB1,
	QueryShape__ctor_m10B944E68ED9FE30CB3D7EE090068A33F9EDB453,
	SimplexCounts_get_simplexCount_m99631648B641C2ECF96D0C8E5DB265D40F522A86,
	SimplexCounts__ctor_m68F3A4CF7A694C3300E7BA4227020A89A724F066,
	SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95,
	VInt4__ctor_m6FDFB28D4337C36A4A49681BA2E0B85D0B36AFCB,
	VInt4__ctor_mDF85C8595BC6DCEAAF3A5A4A04704DEFDB238C6C,
	MeshVoxelizer_get_Origin_m98D23A3FAE59358DCED36A586C0AB6B4F3E77B4D,
	MeshVoxelizer_get_voxelCount_m1590512CB3A933F6FD26076643E8FBBF72CD4565,
	MeshVoxelizer__ctor_m2E9885D259222CEE4CB38497584FE8F3EBB0E2B4,
	MeshVoxelizer_get_Item_mC5F92567FD7C76447DF970069641190AE24C1356,
	MeshVoxelizer_set_Item_m6E59067E37D364C147DEC0335408F67C39286DBD,
	MeshVoxelizer_GetDistanceToNeighbor_mAC3238234874D704EEDE7B39EA4E2C557043FA8F,
	MeshVoxelizer_GetVoxelIndex_m03964EDAA758946CBAFB630ECD6AC4DAF9B03921,
	MeshVoxelizer_GetVoxelCenter_m3E452556E6139C67B397A378FDCBD9BA4F425089,
	MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06,
	MeshVoxelizer_GetTrianglesOverlappingVoxel_mBEA8964FBC06F1AC96902CAB7A18AD815C4FBAEE,
	MeshVoxelizer_GetPointVoxel_mB53686BE0482AC0FE527505F8EC019E21CFAB6B1,
	MeshVoxelizer_VoxelExists_mBD68D5E79FB132F4304AAE5283DF58F65CDA0A4B,
	MeshVoxelizer_VoxelExists_m6075ED6BD1CD96FBC57C1935A45231C09ABCFA70,
	MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7,
	MeshVoxelizer_Voxelize_mFD415E5ED30A0BF0CBC964412DBBB37930CFA97A,
	MeshVoxelizer_BoundaryThinning_m4E5359902ABE3CD1C2541C9FCEADE519E69482D5,
	MeshVoxelizer_FloodFill_m584C8081EF5F8BC2660348F75BF0C006FBD0EC51,
	MeshVoxelizer_IsIntersecting_m1F9C81AE3D544C1F8AA465823F7DDA7A32C7FBEC,
	MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10,
	MeshVoxelizer__cctor_m221669B76FE23A351F3F850B1B8B6BA2FD840D15,
	U3CVoxelizeU3Ed__29__ctor_m8645BD0F9E12E3B4AE36A3A9D757A0AA317666E0,
	U3CVoxelizeU3Ed__29_System_IDisposable_Dispose_m68F6D58872E569B35178F3F948D25088FEE404DD,
	U3CVoxelizeU3Ed__29_MoveNext_m6AC2C7AA56B9E60005E531B6AFBC225E89351F62,
	U3CVoxelizeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCADBFC708A9E44E40767E097632FC238158C8036,
	U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_Reset_mF260247FB427D7B3C0DAAFFD3085943EBA202B9C,
	U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_get_Current_m4ED17979436917051AE4058242D7AF43D4A9F2C7,
	U3CFloodFillU3Ed__31__ctor_m70BCBE21D4D47E1482D966F7C9D35F7510603B97,
	U3CFloodFillU3Ed__31_System_IDisposable_Dispose_m874A33578734166DA64BFC7AE8771A031A25F2C2,
	U3CFloodFillU3Ed__31_MoveNext_m35FC429F6648DE25CB9A4F53A3547E84ECAEEB8B,
	U3CFloodFillU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60C01825AF1A7118B99F44611D5A9FFA941E6189,
	U3CFloodFillU3Ed__31_System_Collections_IEnumerator_Reset_mC4010923219A1BAD954EEAC4B73CDD945265C9A3,
	U3CFloodFillU3Ed__31_System_Collections_IEnumerator_get_Current_m7964014EF80DB8139871A160F4672E40BED0788E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	VoxelDistanceField__ctor_mF001EDBFAD1E407A198F85AE578DC94C131DC940,
	VoxelDistanceField_SampleUnfiltered_m1DCFD20D36F447D804A7DBE566E82FD4E6A134AA,
	VoxelDistanceField_SampleFiltered_mF03122890E95B4323F7318B9C27A8C65DA3F463E,
	VoxelDistanceField_JumpFlood_m8A165DB8B897C98F4DD564943140A9C71408E1F6,
	VoxelDistanceField_JumpFloodPass_m0CAE942EA7B7FB237160C57ABAC04D2F77198AC3,
	U3CJumpFloodU3Ed__5__ctor_m9A42F34DE8C7C452C8A53F17D6D572BC2453C654,
	U3CJumpFloodU3Ed__5_System_IDisposable_Dispose_m0A919D1AE0822BB1475E5FE78DA4060A236911E6,
	U3CJumpFloodU3Ed__5_MoveNext_mCF3E269446E8A96E7F313896B714A111CBBD3FA3,
	U3CJumpFloodU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF414A3DC08D65DC419082B4635288E1F7E02FC0,
	U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22,
	U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_get_Current_m0850F7B63FF4B6EB05FB8A60525AB3BC48509D18,
	VoxelPathFinder__ctor_mEB3020C477C2C93B2F00C9CFC38CFE47068AE443,
	VoxelPathFinder_AStar_mA838A6AEC2878DE3B68FB035FCB31C5ADE9958EC,
	VoxelPathFinder_FindClosestNonEmptyVoxel_m108974C29C2DE5DCB4E8092382271E90E1306714,
	VoxelPathFinder_FindPath_m12E0CEA61718E42317477C9B7A3825B9EAA590E5,
	VoxelPathFinder_U3CFindClosestNonEmptyVoxelU3Eb__6_0_mBBEC0743F2CB9A04F014A64C652D3572607A4FDC,
	TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F,
	TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF,
	TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681,
	TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8,
	U3CU3Ec__cctor_m2E9966CB04100D59E489C39A64FAC67F8FD97311,
	U3CU3Ec__ctor_m7A494B57D1990F9D4FD4EF63281425ABEC97A888,
	U3CU3Ec_U3CFindClosestNonEmptyVoxelU3Eb__6_1_mF780311AA35327276D353A5C472EBABD63FB6D10,
	U3CU3Ec__DisplayClass7_0__ctor_mCCDBC132E254A9EA49862CDDDB93E94F182AF58C,
	U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__0_m0CBCD40678980CB309FD296C8DCA140B2368C158,
	U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__1_m93C19DF998EC9A85989D9980D3C89F8EB7724361,
	ObiDistanceFieldRenderer_Awake_m02202AAABFC3B738E4965E9B2B897A228E6AC210,
	ObiDistanceFieldRenderer_OnEnable_m5A75F9F5EE526E06FF0D410741740FCB6B6478E1,
	ObiDistanceFieldRenderer_OnDisable_m7C13494A2B29D5E6827B2757573C7370FC80A394,
	ObiDistanceFieldRenderer_Cleanup_mFE94B31A3E0EED155DC9650B112587501A88D532,
	ObiDistanceFieldRenderer_ResizeTexture_mE50107B1ECBAB2706311F61EB8EDFAA5A47FB02C,
	ObiDistanceFieldRenderer_CreatePlaneMesh_m7DABB1814E6572C53BCBCC12B382369FA534AAA4,
	ObiDistanceFieldRenderer_RefreshCutawayTexture_mFE87BC7C9D4D36CAE8BA3A2BB5CCFE9195422F93,
	ObiDistanceFieldRenderer_DrawCutawayPlane_m881EB8355EA4D1D21C65297319180645AD0BBB3A,
	ObiDistanceFieldRenderer_OnDrawGizmos_m216C9ED791D6583CFB5AC31CCF96FC999FEB065B,
	ObiDistanceFieldRenderer__ctor_m5BDF67924CA8661E32DCAE76825AECE42C4E08AA,
	ObiInstancedParticleRenderer_OnEnable_mE355745DF976D2E5FB662C641FFF1207CFF849A8,
	ObiInstancedParticleRenderer_OnDisable_mB6727244E6FCBAA7A72470799C34118F47F54C26,
	ObiInstancedParticleRenderer_DrawParticles_m006744E372DC552B4BBE90BDA10C2936E58A7CEB,
	ObiInstancedParticleRenderer__ctor_m36160262281CB03A2EF5F4F65B1599650E20D198,
	ObiInstancedParticleRenderer__cctor_m8F094F8AD40C59C077031DB8223AF95B8123B2B7,
	ObiParticleRenderer_get_ParticleMeshes_m19B21A66F62AF9565B5610A7E406A9F4D9AB2624,
	ObiParticleRenderer_get_ParticleMaterial_mACE9EAEA565D0D3F7A7BF6C53B8C04111AAFB530,
	ObiParticleRenderer_OnEnable_m5936C65AFFDE90197548673B64DCDB559B634084,
	ObiParticleRenderer_OnDisable_mAA99859EE95AC59E3CCEBDAEE3C9F3D1EBE9F2A3,
	ObiParticleRenderer_CreateMaterialIfNeeded_m3F402725EA811E7191F495C02BC1D42C9D3508E2,
	ObiParticleRenderer_DrawParticles_m968118939B2050BAB32AF92EFCB004552F31FCAE,
	ObiParticleRenderer_DrawParticles_m10909ABED462F26F971463C7A569BC19C8115FD8,
	ObiParticleRenderer__ctor_mFC28AF85E8C3264440883850C411477777E11A29,
	ObiParticleRenderer__cctor_m25FE6E62F8C43957CDFA579FE81399B4270C8DDC,
	ParticleImpostorRendering_get_Meshes_m755745FEE84971387953757DE9D15D9D32204C2F,
	ParticleImpostorRendering_Apply_mC4B8B70A312A115D0D3264BA0F4D6CB8920AD9D5,
	ParticleImpostorRendering_ClearMeshes_m425FD6121111FFDD2834713DDE69988571B30BD8,
	ParticleImpostorRendering_UpdateMeshes_m352C0F940E2857058D6A6B447A03AB70258E8A60,
	ParticleImpostorRendering__ctor_m83C3D5BED9E2463E4E916753E01D78C90E7C9BBE,
	ParticleImpostorRendering__cctor_m096389C9C5C4FB998DFF6B43A943B8F29D300B72,
	ShadowmapExposer_Awake_mA4E68783C4BCFD7B7FCCF2FB2047921F739DA519,
	ShadowmapExposer_OnEnable_mA68155C92E4F83954076D9000038D58A265100F5,
	ShadowmapExposer_OnDisable_m7CC85B1D1729AA03CDAC1816F9A84E3350254554,
	ShadowmapExposer_Cleanup_m32D90F4E8CAC5C2EB13BFAD141A056F8D3B143E5,
	ShadowmapExposer_SetupFluidShadowsCommandBuffer_mE799C5800875D6B7CA60B3775768C9F07B2288B4,
	ShadowmapExposer_Update_mBFA72090D0E7D36367BCA31B001E2965D1B264E2,
	ShadowmapExposer__ctor_m10E8C4FAA46FCB5CC04085336279E049BC51FAFA,
	ObiSolver_add_OnCollision_mEB324EABAE3F36C43F055950F52827839851DAF5,
	ObiSolver_remove_OnCollision_mA07AA8AA273F50BC4DDAFD0523A7CA23C628D674,
	ObiSolver_add_OnParticleCollision_m73A739A0544F098FE805CEF2C6ED3E9528060A52,
	ObiSolver_remove_OnParticleCollision_m153C65501BB104D19DFB0BC50CE655A20BB725B7,
	ObiSolver_add_OnUpdateParameters_mDCE3C3436FCB6E239FC3C229799E1CA65048798B,
	ObiSolver_remove_OnUpdateParameters_m1D7467B121B4968CCB10AE69A8FCE3F601A622E1,
	ObiSolver_add_OnPrepareFrame_mF8245DFAA6E404FFD6135985C1B90BAD5A827B69,
	ObiSolver_remove_OnPrepareFrame_mFF9D302946D3647FF7267652E21CCDA784AC68E6,
	ObiSolver_add_OnPrepareStep_m22DB6F7A692B42DCB0AF33EDDFAF38062A4AB42E,
	ObiSolver_remove_OnPrepareStep_m5D2D4E8CACADB7938868BE6F29546BA497A05250,
	ObiSolver_add_OnBeginStep_mE11B9F3927E6908AC1C8CA09888FAC4145B60F72,
	ObiSolver_remove_OnBeginStep_m828F85A9556BCF73D5434B9A689918DEF5F5D39D,
	ObiSolver_add_OnSubstep_mF4D4FE1ED9E867E6F111B8B082E377D950607C6E,
	ObiSolver_remove_OnSubstep_m60FD7FE897C29FEF9EE0EDC9303C62CFF70D2B33,
	ObiSolver_add_OnEndStep_m85BFAD019C29970E500BB5079DA70527A4EB324D,
	ObiSolver_remove_OnEndStep_m53846A7D3E4D2113D49EAA4EE56FAE9752CB6CA3,
	ObiSolver_add_OnInterpolate_mBADEB5D974C090F97884D9FF885C017D0C36CF7D,
	ObiSolver_remove_OnInterpolate_m5F9AAE96B45E17989172907EECA1966282A87626,
	ObiSolver_get_implementation_m9F3A6A2A2D6921B6A59A82F935039FC29F24E422,
	ObiSolver_get_initialized_m245E2320CEE2189A248BB0DE795CD9FE80839C78,
	ObiSolver_get_simulationBackend_m67AEE52B9C07C78C1231FE5074F7125E47A8109A,
	ObiSolver_set_backendType_mE714E4CD3D72BE093E2ECBB2B418D7095A3B11B4,
	ObiSolver_get_backendType_m49EDD493C5D45A0D0728C26F95F1E955FEFB4104,
	ObiSolver_get_simplexCounts_m78AF46D65457FB56118D4ECD884FEAD71D3E28C4,
	ObiSolver_get_Bounds_m4C8914413BBBC0968BA6ADCF3E7149AA5F7FC7D3,
	ObiSolver_get_IsVisible_mA12ECA2DFE503F721181D2A8A5F5F9065BABAF0B,
	ObiSolver_get_maxScale_m2118E5D74E45C017D6D91E33790768FC10FE9B18,
	ObiSolver_get_allocParticleCount_m98C18E7A785C31F32283F6A52AB1372313E3E16F,
	ObiSolver_get_contactCount_mAE6F8DD8070340148DCC11C0F729F1B38C4AF654,
	ObiSolver_get_particleContactCount_mFDF3ADAB6B54D7FECF6F1943E4AE93C0AE10F18F,
	ObiSolver_get_particleToActor_mFCEA216986C39AD5264352D73B10017FE5E7F3D3,
	ObiSolver_get_rigidbodyLinearDeltas_mEB6D6F68DEAD8616EA48FA36AEA9B37520CA9367,
	ObiSolver_get_rigidbodyAngularDeltas_m05353966D848B59BC9586B0C19CAE476834502C9,
	ObiSolver_get_colors_m3263CBAEB79EE1F583593969A2EAD32549CB875D,
	ObiSolver_get_cellCoords_m6BB3F63111C716E45AAC8FC0BE4AB0CD17331B28,
	ObiSolver_get_positions_mB7EA5209FDB8289380DBA07A2F28E640D485F557,
	ObiSolver_get_restPositions_m1BCA73DCC44BA1B9181838370F0E546B4D66CFC4,
	ObiSolver_get_prevPositions_mA99888F076C4FB374B14DEE1178C625A3F8F9B00,
	ObiSolver_get_startPositions_m3B9144D015BDB7F0F676F065AAC9A22F0DA9CC27,
	ObiSolver_get_renderablePositions_m56EE654A3E7ECEF99EE3F1481E759B01DAB697BE,
	ObiSolver_get_orientations_mAF1A79C344CC44789900361936635BBD508D2973,
	ObiSolver_get_restOrientations_m5A079EC8140D809FB02223ECCA6986917B333AE4,
	ObiSolver_get_prevOrientations_mBB52AEB5D484B0A4BAE23213D65585BE498B5F68,
	ObiSolver_get_startOrientations_mED3EA83E62A458AE8F95A89CEC15ECA81FDF28D1,
	ObiSolver_get_renderableOrientations_mB71AEBAF05CD2CA3FED8936554686F5A0414C06A,
	ObiSolver_get_velocities_m73051C86B492795133F138E42FF08698222FCCD6,
	ObiSolver_get_angularVelocities_mE565A9561A09435E294755AA2F7E31993BD932DB,
	ObiSolver_get_invMasses_m403082935BEB22CE4C8A086C31B748FC46B8C319,
	ObiSolver_get_invRotationalMasses_m35049317216303EC2D97F143D98DBB60C9EADF58,
	ObiSolver_get_invInertiaTensors_m66E7A977E525B6628FA0D14015A52DE6CC26AADA,
	ObiSolver_get_externalForces_m6DE0779EAA775DB7E7A853B844E07464240BED59,
	ObiSolver_get_externalTorques_m7A057FD836A21C5CA1DAEA2258E05F81D15F8991,
	ObiSolver_get_wind_m72179C4670DBCE8A1838066EA134F02A16DC371C,
	ObiSolver_get_positionDeltas_mA020DD1DBE7DDC042F8C730EBE9A71D6AE213AF9,
	ObiSolver_get_orientationDeltas_m747EE331713678E42AE2A16F9201AF96E8CFAF21,
	ObiSolver_get_positionConstraintCounts_mFBC0F55E2F573CC7A38F71703D8617DF5C9BF895,
	ObiSolver_get_orientationConstraintCounts_m6752E0D0CAA0272BD7FF4242E5A426F4310FAECA,
	ObiSolver_get_collisionMaterials_mE98DA539A9F5EC9BE8B35D9AA8C6F4C4FDA211A1,
	ObiSolver_get_phases_m1DFABF0F6107CA7A301BBC35A0896C69FA1D7422,
	ObiSolver_get_filters_m35DFCECD0FEF501CB8903CF986F67CD3487DFEBB,
	ObiSolver_get_anisotropies_mDD48FB3AC63921409AAF55FB2FED3833FC458009,
	ObiSolver_get_principalRadii_m6C3307DA982056B2AF918D77B539E22580E5F179,
	ObiSolver_get_normals_m241E9FFCE3A531BFF2679A3420EBCE7B313FF52C,
	ObiSolver_get_vorticities_mDC8CC9D454FBD7AABF51AEDD83B63D62D74D2E62,
	ObiSolver_get_fluidData_m70D3FEC3C44F6EA5D68AB6B39BD72A08B41EF349,
	ObiSolver_get_userData_m63E5DF119EE501A9380C6A176079C9FD1361B561,
	ObiSolver_get_smoothingRadii_mEE720BA5CB3140CCB508FC97466DDC06F18A9380,
	ObiSolver_get_buoyancies_m14142F7AA18DAE0CD2BA25EA5F3F7E5796938365,
	ObiSolver_get_restDensities_mA85A3F3E9C265A9E3B01B957691808F072123C96,
	ObiSolver_get_viscosities_mBCDE34198266ABADF85472134F8A69F897BB37D6,
	ObiSolver_get_surfaceTension_m06BBC45CF2C65E927490672F37C89AD69ACE039A,
	ObiSolver_get_vortConfinement_mBCA2B8E737E63A3DADDC34F1E7E7C4D26F7A71D0,
	ObiSolver_get_atmosphericDrag_m81584993AAF86345E489C4D833D1BAF3E704CB16,
	ObiSolver_get_atmosphericPressure_m363045FB67ED30D2413C84317FBF2033FF9BE1B6,
	ObiSolver_get_diffusion_m614EC192C40E1DB26E20E2B7AEEB1E3853E1CBAA,
	ObiSolver_Update_m3432EE148E4943BFEA7F248CC1ADF410EB009532,
	ObiSolver_OnDestroy_m10E800119F5492F06C454965798031E2959B4D91,
	ObiSolver_CreateBackend_m91B701B5D03356B869ECCC5E5B4E0F9E82CA7377,
	ObiSolver_Initialize_m69393E4E890B2AF174E313FAB09AF20C8858713B,
	ObiSolver_Teardown_m7AFA06EFC9E0BA723690DF91837C3E3B8D6C30C1,
	ObiSolver_UpdateBackend_mED822610A9CB49AC6197C9EB12A70EBF7F5959A5,
	ObiSolver_FreeRigidbodyArrays_m75E7122190DD0CFFE6594F39BED2EE9685488FA2,
	ObiSolver_EnsureRigidbodyArraysCapacity_mC0A5656D8985DE06BC6084750EC6AE266B7E059A,
	ObiSolver_FreeParticleArrays_mC803A0F5E3291D91B643E13F40F56A1DD906DB19,
	ObiSolver_EnsureParticleArraysCapacity_mA3FF2925ACDE72EC10DF9853E51FF3440F27EB67,
	ObiSolver_AllocateParticles_m2437F6049C3AB8F791378C25BB220F3D447E9209,
	ObiSolver_FreeParticles_m4CD1BFC3A67A853618E2BC48B666735D57D69CBB,
	ObiSolver_AddActor_m33CCB3774385993AD4756D44613032EDF7602076,
	ObiSolver_RemoveActor_m9816A116E0848361565F850D70B3B8A876ACC5A2,
	ObiSolver_PushSolverParameters_mC566D7A04AABE0FD982060FDDCDF94BF0330A898,
	ObiSolver_GetConstraintParameters_m612C27EA24521064F789D15FABB06E8320EC50DD,
	ObiSolver_GetConstraintsByType_mE11A373E6629FB18A577363D79AE2626806FB095,
	ObiSolver_PushActiveParticles_m699BFA15ECA1837EB8E6FB0C4C06564BC00A4356,
	ObiSolver_PushSimplices_mCF761C06FD3C0023D8AC8CAF847F22856CA29F2B,
	ObiSolver_PushConstraints_m1FE372176ADB5BDDB3C11F042850CA126D62EA7D,
	ObiSolver_UpdateVisibility_m78B2DB3B14730D673C823699F0AF2CEB06524552,
	ObiSolver_InitializeTransformFrame_m37F3F63EE2DD9DDE17329B3FDF750A4C9AFF317C,
	ObiSolver_UpdateTransformFrame_m95D29D02A287E86629B2BF566891B519A5D3B2F4,
	ObiSolver_PrepareFrame_mC4616463C08C47D37E9B5A9610850BD0853BCF59,
	ObiSolver_BeginStep_m08735BDB104DA6C7B894DDCBCE685C8F08EAE8C5,
	ObiSolver_Substep_m8EF42B8FCB4A92B66A18967A5E30C0FAF9D91226,
	ObiSolver_EndStep_m12D95AB19D02AB5C1E2ED1D8F3B1E586831DBA64,
	ObiSolver_Interpolate_mAE7A83C120C8BCD50676E781D540026B6BAEEDE2,
	ObiSolver_SpatialQuery_m6D0D03F289F49B89217E8A449DAABC0CCB90D83B,
	ObiSolver_SpatialQuery_m5C0D958E05B3BE7A66EFA6E320C0CB6E3A196607,
	ObiSolver_Raycast_m8F28F2F638DC71BC4716816A590D3D86E0274409,
	ObiSolver_Raycast_mBABB28101E4819C4AC8C9D3189DB8636FD255007,
	ObiSolver__ctor_mA963ED6A2F8A02E33B2E3BACB178FC2F0436F753,
	ObiSolver__cctor_m4023459B57CD95EEF9656B8BDA9B18BDD558C46B,
	ObiCollisionEventArgs__ctor_m346881EB85AB69845A91659208E75756E4EF6D17,
	ParticleInActor__ctor_mAA089A9B7EBB63E8671D8AAB97FD0836220D7EEA,
	ParticleInActor__ctor_m3829A4666F38432208F2811BDE98CD7A8C6B25F3,
	SolverCallback__ctor_mE4083F1B124E6A1FFB21E69EE2AEE7318B28AAD7,
	SolverCallback_Invoke_m45F6B3AC4BD61E22AAA076A7DBA5544E5C72F057,
	SolverCallback_BeginInvoke_mD89642A3E1F37C4DC118EA6CACD1B6D68CD83A08,
	SolverCallback_EndInvoke_m395BC54EFE0998B85005E9E4F66858F146C931F2,
	SolverStepCallback__ctor_m2079BE6990F358A8A5837AE21480E58DD4D0EEA3,
	SolverStepCallback_Invoke_m68DD32CB29FBD1EC423CC5A72702D2CB05715754,
	SolverStepCallback_BeginInvoke_m434058E191E84C9A47950451E2B46A1F8F957F40,
	SolverStepCallback_EndInvoke_mF3E0E0DE67ABA86472824000ACA507AB3352A533,
	CollisionCallback__ctor_mA5A4EE9E910214B425B42DD40A2671EB949D661E,
	CollisionCallback_Invoke_m941AD708CC00FD8EA47569A5B061BB862B96F98C,
	CollisionCallback_BeginInvoke_m0A88695225D3C02615D754DC211FD3EE3468BFAB,
	CollisionCallback_EndInvoke_m46F561EE559B214E9D38C29ABCADAB3336B18A8A,
	U3CU3Ec__cctor_m89EAB0A60F1D2D1667AB8DC5FFD689526BA4D3DE,
	U3CU3Ec__ctor_m2D8F2915236AB77A7C36A87C7761B5503DCA10F2,
	U3CU3Ec_U3Cget_allocParticleCountU3Eb__148_0_mDB5D6EA59542D260F8AA00864066AC567AF3613B,
	ObiFixedUpdater_OnValidate_m5F6FE3B9B8E80E3B125B39F142D2DA57A4DD8BFB,
	ObiFixedUpdater_Awake_m90D52F196706902484B59979C0A7537552FAB320,
	ObiFixedUpdater_OnDisable_mEAE5E8BA5B2A650527D99517C37FBF98173259E9,
	ObiFixedUpdater_FixedUpdate_m9CB26DEA62FD0EA6E7FBF86A2CFB12C375B81CE0,
	ObiFixedUpdater_Update_m485E215A24D5B8A2C3B191F2F65615D4C2A27A90,
	ObiFixedUpdater__ctor_m129BDE7A45025352A7238A317F1A3E7B31A00C9F,
	ObiLateFixedUpdater_OnValidate_m4B24969542BF8BAA45ABF2DD0FC90CD910FA27AC,
	ObiLateFixedUpdater_Awake_mA03AD9976B8FA43BE9FC155469524FBA224C096B,
	ObiLateFixedUpdater_OnEnable_m7AA55EFB2018C0FE7A7450DE8B124E9947735430,
	ObiLateFixedUpdater_OnDisable_m508D776D30650C73FB72E47AEB5F2F7EFB64DFB9,
	ObiLateFixedUpdater_FixedUpdate_m347A98DB1FFCE6D2654B99E6E228F26BC0646F21,
	ObiLateFixedUpdater_RunLateFixedUpdate_mA207589FB295229010BC3ED39CA0A33B465AE4AB,
	ObiLateFixedUpdater_LateFixedUpdate_mC459F999C033D7BC22CD0A0BDA9569E93C2C6F2A,
	ObiLateFixedUpdater_Update_mE16274CC3FCABACAA0430D79903C2298050907B9,
	ObiLateFixedUpdater__ctor_mA0140EBDF2452F0A415601A43DAE9822CCA249C6,
	U3CRunLateFixedUpdateU3Ed__7__ctor_m26D083432D24A29A1601BBEED04C921E5E757A62,
	U3CRunLateFixedUpdateU3Ed__7_System_IDisposable_Dispose_m28B4BB85E97B45021B22C15FEF3D8E93B5BF81C8,
	U3CRunLateFixedUpdateU3Ed__7_MoveNext_mB69C634DCC614B7AB5126ED4FBF48E61309D1AC4,
	U3CRunLateFixedUpdateU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD948A54767F3CE844E780E8D04C37F8B9F865A05,
	U3CRunLateFixedUpdateU3Ed__7_System_Collections_IEnumerator_Reset_mBC0D3D496D3790C6970DE839BF6F1DEE46B04FB3,
	U3CRunLateFixedUpdateU3Ed__7_System_Collections_IEnumerator_get_Current_m200572F643F2FEB367A9BF53A37578AAAE32802F,
	ObiLateUpdater_OnValidate_m6360BD327E35B0C56A590D51CB066B811DAD1774,
	ObiLateUpdater_Update_mC52FDF37EE579A4D667D80C5327FD48FF06A7400,
	ObiLateUpdater_LateUpdate_m04DE9233394AE2CC13CF1DA4F136364E81FE4C06,
	ObiLateUpdater__ctor_mD001E04F8FFF222B20A8ECDBE7A8C2507C2F0894,
	ObiUpdater_PrepareFrame_m38DA2E247B4C02F4E18CA1E5890493598A019D65,
	ObiUpdater_BeginStep_m871A82CAEA561457F1FB19B27ADC346661839AA5,
	ObiUpdater_Substep_m44F3765E77796D6D71287FF2883FF2E496E0767F,
	ObiUpdater_EndStep_mD6CEC404A973E002CD4D81FC87442C77F537E55E,
	ObiUpdater_Interpolate_m38784F191B281CBF7A3DE79B71EA71ED2FE60681,
	ObiUpdater__ctor_m3A352531659D7BCCB0CF2AFF0732D3B28AB26092,
	ObiUpdater__cctor_m272383BDBA28D7F89B9965940A9188BDE81EA1A9,
	ChildrenOnly__ctor_m4D3FAAA742E61B7FA2323CE9F448A1877F87CAE0,
	Indent__ctor_m5A092B8D80C878888D885C6ADC0B3A9D3A90E3C7,
	InspectorButtonAttribute_get_ButtonWidth_mE4C1BF731F1203ABCDC7FF911FBC9AA619ECC4FB,
	InspectorButtonAttribute_set_ButtonWidth_mD7A12A99D234B3911E6D035C0224BC6AB4CC773A,
	InspectorButtonAttribute__ctor_m975A49E0C15C48813192407A712BDB21DCC86987,
	InspectorButtonAttribute__cctor_mD9D19B7D28E89349026C4FBCAF129D59060E0349,
	MinMaxAttribute__ctor_mAEC8FD68CEFC58D324E8BBF941E6940654EF2F78,
	MultiDelayed__ctor_mC38D0EAB98C18CE8CCEBCC3767678783367BC0F5,
	MultiPropertyAttribute__ctor_m0CEA425DBF3D690C2FBF18251EA3D45B8A2EEEFD,
	MultiRange__ctor_mB97A29FE677F7549CF13B4C38DEAA868640876C1,
	SerializeProperty_get_PropertyName_mDC8A03791EF5C0FC8DB9BEE2645D05EC456A9B21,
	SerializeProperty_set_PropertyName_m0D097B39C9F982C6DA9CFE395012ED31AE075A55,
	SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3,
	VisibleIf_get_MethodName_mA9D547283CA5003031A6052EC2F01BDD20626C79,
	VisibleIf_set_MethodName_mEEE7DE756E758BB291ECC2AF95FB9331577A89BD,
	VisibleIf_get_Negate_m70429C0B2F6E15B7329A21127ED7F095B5B9F79C,
	VisibleIf_set_Negate_m7AE9FD0E091984A71A593C3FE0A0A4CB89CDDF7E,
	VisibleIf__ctor_m74CCE1E688720090402D0E285F11AA7EE004E6C6,
	CoroutineJob_get_Result_mD748A43832803B6F5A24988519355E0B5E960BD3,
	CoroutineJob_get_IsDone_m4278531E6DBB4F3ED38BEB44A7D3365F6008420B,
	CoroutineJob_get_RaisedException_mB7118E9ABE0C6CC7C00D741FC8BC5E859CC81F9A,
	CoroutineJob_Init_m08092162CA93D4B06D6F23B48138F018CA204416,
	CoroutineJob_RunSynchronously_mC0C37C33DF39624CE6811D4D2229BF1DBB004DAB,
	CoroutineJob_Start_m80C8E17CD749B13B237150583A6280C213F8C39F,
	CoroutineJob_Stop_m503EBD4DCD2DB059CA0681D562A66E37442C7E4B,
	CoroutineJob__ctor_m3B1FC813C2F585D8B745A74C9F40059C41C72437,
	ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1,
	U3CStartU3Ed__15__ctor_mD7A6880AAEB86FECE339EAA24C276AA5D91FE5AD,
	U3CStartU3Ed__15_System_IDisposable_Dispose_m4542E430FBE633F30A5F1B001DA8C7D01BE636F1,
	U3CStartU3Ed__15_MoveNext_mF349A26AF1025AF7487E1BB1D725340CD887C18F,
	U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m857DB02265865988D8A060D28C87EAB01F6DEC55,
	U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_mC889240009E6BB1F36B3BC74F618A35A604608B3,
	U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mE937748101E5BB647A3762B97A9DE2FED38D8283,
	EditorCoroutine_ShowCoroutineProgressBar_mBD8BA0BAB4A8F6DBA32D5EB38A2AC2598992E522,
	EditorCoroutine__ctor_mDA14C1513A94EABA131D822C5BC96C78411A872E,
	ObiAmbientForceZone_ApplyForcesToActor_mAE4A4FC41002FA3D906FEA74194C377D2C60A8D8,
	ObiAmbientForceZone_OnDrawGizmosSelected_m9F4A990AC63D31EFB4E459DDDE79DA6D99ED98DE,
	ObiAmbientForceZone__ctor_mD887D3FBC82BCD8DF7720A9B25BF72572118351E,
	ObiExternalForce_OnEnable_m0FB8D535D2ED4F394E48424E48C073B8E3FBD10F,
	ObiExternalForce_OnDisable_mD1867136686D828057C7DF70B6A77B5DE81E459C,
	ObiExternalForce_Solver_OnStepBegin_mC46587312AE49CD7183DAD9D16BBB66C9BF64E0E,
	ObiExternalForce_GetTurbulence_m36B324BC86245A69B5FFB5D924E08E35BCEC135D,
	NULL,
	ObiExternalForce__ctor_mD434F0876366D23C0D68DAB6A8949E018A42ACBD,
	ObiSphericalForceZone_ApplyForcesToActor_m506A142D9EF2B82CD7310E150EF302F8A6B2FA2B,
	ObiSphericalForceZone_OnDrawGizmosSelected_m428130DB0C2E2D23A31E3D0FFE00EDA0FB6744B2,
	ObiSphericalForceZone__ctor_mCB4320E2FDC045FAC6D25069AA629243434775F0,
	ObiContactEventDispatcher_CompareByRef_mCC27E2711928972C0B16573985711C3432D1719E,
	ObiContactEventDispatcher_Awake_m4C9DC422D39BD21B3BE61C3BE4EB262BD0FECAD9,
	ObiContactEventDispatcher_OnEnable_m9E49028DB875725C353943480CB5C447179953DC,
	ObiContactEventDispatcher_OnDisable_m8F75848B0553FD8776A22ABD1253D891B592CFA0,
	ObiContactEventDispatcher_FilterOutDistantContacts_m2088093F9BFC093F73ECB24BDC9E6DA7A748962B,
	ObiContactEventDispatcher_RemoveDuplicates_m394D908408DBDED555F844001682FD217E94C5F9,
	ObiContactEventDispatcher_InvokeCallbacks_mB588492B596248DBC043C8235D2AD8D3AC3AC423,
	ObiContactEventDispatcher_Solver_OnCollision_m12E8F9442701D7911DE4F0D18CB0F4EC82F263FC,
	ObiContactEventDispatcher__ctor_m357D739552531DA027D8F320CCFAC444814BD791,
	ContactComparer__ctor_m5DC2082E03C9DA3062C2D13D3F1702B3763C4379,
	ContactComparer_Compare_mB97171F79E54851FEF29ACC2ED3F06C5A1452362,
	ContactCallback__ctor_m77F3DD3B53374589F094279143B1638E57919AD3,
	ObiParticleAttachment_get_actor_m8A6C711886370813BD7BC301BFB6C7812B6B193A,
	ObiParticleAttachment_get_target_m87F09D825C3B834F3F32869BA35539B4525FA08E,
	ObiParticleAttachment_set_target_m05F5DB1086FA979873CCECE322EDE77C26550DF7,
	ObiParticleAttachment_get_particleGroup_mE66342EC326E748311096EBF6508598B2D0E9CC1,
	ObiParticleAttachment_set_particleGroup_mD2ACDCEDFFDECA0B1737C92F4E2941CD93BEFDE7,
	ObiParticleAttachment_get_isBound_mD1067521C804A334B7A28CA3A9BD65237C0FD29D,
	ObiParticleAttachment_get_attachmentType_m3AE549A02BCF14D72CE6EDCC93541FD03EA3B271,
	ObiParticleAttachment_set_attachmentType_m5631ABD4AF1A1AFB91F5851BF869AD3974C2586F,
	ObiParticleAttachment_get_constrainOrientation_mD07EF64C3AE8F1F9CE99F747CF0D12C2DE7D6E1D,
	ObiParticleAttachment_set_constrainOrientation_m17A07949BE7D8BBF51FCE020DFB2B07DA73A722F,
	ObiParticleAttachment_get_compliance_mF04CA0E1C82403AC5039D8F3816CB750E16E22D9,
	ObiParticleAttachment_set_compliance_m559FB99156912E50F36AE6239AEEF8DCE995A132,
	ObiParticleAttachment_get_breakThreshold_mA89F93A18D03F1DADE699A97634716D15617B02F,
	ObiParticleAttachment_set_breakThreshold_mF3F55D89A7E9E6EEF600D372A34DB537A752EB06,
	ObiParticleAttachment_Awake_m3F7F1CA4DFB4B2C06A8F98ACAA99335C589D62A8,
	ObiParticleAttachment_OnDestroy_m5F5AB114D3BB369BF21F771A3F56EE68D5CF47B7,
	ObiParticleAttachment_OnEnable_mE67B8CA340675DC78907D05D206A0B32B47C8F6E,
	ObiParticleAttachment_OnDisable_m970C2F665946F9526288866F57E00845E57F6231,
	ObiParticleAttachment_OnValidate_m8F4069C273A48281199B02A40AECFF1D83EA0C32,
	ObiParticleAttachment_Actor_OnBlueprintLoaded_m5736115ED7F6C5E1935D2217E7E5DBFDB5B20487,
	ObiParticleAttachment_Actor_OnPrepareStep_m9EA64FC23D3365CA10F37123637E56D333D6A2A8,
	ObiParticleAttachment_Actor_OnEndStep_m09B95B61E9325F33E9DF9B85945979C11962E0C1,
	ObiParticleAttachment_Bind_m41671D060988813057E5D6CB710621B387C3B4C7,
	ObiParticleAttachment_EnableAttachment_mB6BAA968F14899C9E41D3827506F5A05E45247CF,
	ObiParticleAttachment_DisableAttachment_m91236D7F5CFA0439C6EB8D250816CB587AD6392A,
	ObiParticleAttachment_UpdateAttachment_m5537ED01556A4DC62E3F46AB5D43A9E2C875856A,
	ObiParticleAttachment_BreakDynamicAttachment_m3AA55F0956AA144B3AC657224DF303EBACBB9022,
	ObiParticleAttachment__ctor_mAC181D9F03BE52C0DB29C07F539A74C0DEA1744B,
	ObiParticleDragger_OnEnable_m72B8C23953592A731785106A8459284845816A55,
	ObiParticleDragger_OnDisable_mB11CAEAA8554518A0B474976561C8FEF0D6E8E49,
	ObiParticleDragger_FixedUpdate_m9851F3C063BF90687B18E10701B7B1F5193A8E6E,
	ObiParticleDragger_Picker_OnParticleDragged_mECCECAF2424C1D2656A6601ED602BFEA343EBB7C,
	ObiParticleDragger_Picker_OnParticleReleased_mD5AFBB9569AFDD6F33D4E148A5A63B542331C87C,
	ObiParticleDragger__ctor_mC7F5FD7D2B50D344D2D85CDA6CB55EA76DFD7A73,
	ObiParticleGridDebugger_OnEnable_mDCE0CF67BDC98D81F9FB3B4AA3DA73485F24A8E3,
	ObiParticleGridDebugger_OnDisable_m6EEAA88C141739F08763B496873E4FB8583ABA2D,
	ObiParticleGridDebugger_LateUpdate_m7A114715F2660DE5C8004F414580ABAB4B6BD796,
	ObiParticleGridDebugger_OnDrawGizmos_mFF5DD489989676AD3C6BACA195FE47F4E18C22AC,
	ObiParticleGridDebugger__ctor_mC3547171B3F6C6C08F8E759186D7007E186A29B8,
	ObiParticlePicker_Awake_m74E07553D81756450A0D1B8351E1CBF8142F8F11,
	ObiParticlePicker_LateUpdate_m9E75050EBAC6B52C81851303DEE0B2EA70353CF1,
	ObiParticlePicker__ctor_m5818ABF9055AD22D294ED1EB13021BC9FE8AB3A9,
	ParticlePickEventArgs__ctor_m64293DD03DECF493CE8EBD7844C21D2746C09EF0,
	ParticlePickUnityEvent__ctor_m8E79C656B1CDE3FB655B899990D215C91879BFA0,
	ObiProfiler_Awake_mAB7F0E70026EC600D00A9130AEA40B9F2DD4D0D7,
	ObiProfiler_OnDestroy_m952E74CFF016FB1DB495D76228352ABED2E56166,
	ObiProfiler_OnEnable_m2FA73A238837F2034B8E2907F2E87C347A07EFDC,
	ObiProfiler_OnDisable_m677BD3207054B10CDE9BAFA6EDC6D7C52DAD7B09,
	ObiProfiler_EnableProfiler_mE07D4D3052ADC88FA8D0D882DF933E094D9F25FD,
	ObiProfiler_DisableProfiler_m6E225ACE957C1D38336D2C4FFEEBAC52540241E9,
	ObiProfiler_BeginSample_mCA17FA232FD590BF488C6304FC5C4B4B77105DEC,
	ObiProfiler_EndSample_mA76E83B9E7BB9DCEA7626780607D999D34868425,
	ObiProfiler_UpdateProfilerInfo_m89B2E0A641CDEA86AB5195C8CB8CDE7ED666B9DF,
	ObiProfiler_OnGUI_m975F6EE95578505D3E6F1792AD1AFBBAD1375139,
	ObiProfiler__ctor_m3EEB8D1C03DFE08C0EC0E23E334C190F41E40424,
	ObiStitcher_set_Actor1_mABB05BE8A26AE51871FE07C7EAA9D53359016679,
	ObiStitcher_get_Actor1_mF7B8097B74CDE0D5491AA8CA0BFB9EA6F6521DE7,
	ObiStitcher_set_Actor2_m8D4021102EDF55C787F2AFB5CBD83AEA4443C95E,
	ObiStitcher_get_Actor2_mF6FCD44A6E57935EF04782FE6E80FF30A500356E,
	ObiStitcher_get_StitchCount_m0A3C3380AA1B0536814453807186D85002F57744,
	ObiStitcher_get_Stitches_mD04C880005B8B5F6261CFF2E5ABB4C713C27CDFA,
	ObiStitcher_RegisterActor_m6CF833604BDC8434EA68BD461456A891CCE1A6B6,
	ObiStitcher_UnregisterActor_m1C06E9E8003BEAEF75F59ED25B8B56561E5D4A4C,
	ObiStitcher_OnEnable_mDEE87492FEFF02F28EC6D8CB231D8DA3893A8BBC,
	ObiStitcher_OnDisable_m44319848A4EA3CB9AF55AE503389703F57250C00,
	ObiStitcher_AddStitch_m9CDD6C005F4A71C8E942AFCF0EE588AC47CBA1D7,
	ObiStitcher_RemoveStitch_m050637591BDE1882809E3902CD2E7C0FF62EB0CC,
	ObiStitcher_Clear_mC2BBA753DCB0899C7C486C20565D05F6ED1F1B2F,
	ObiStitcher_Actor_OnBlueprintUnloaded_m6CE4420D85CBD27371B1BCD548FC50F983C7DDC6,
	ObiStitcher_Actor_OnBlueprintLoaded_mF598296F75D17D0F818E4D7F7A1E98F7F3B8200A,
	ObiStitcher_AddToSolver_mACC6329F09C6AC34703CC2C3CDCD6531D7ADCA63,
	ObiStitcher_RemoveFromSolver_m5B3C5E8739E3393D3654A34D0D84972D7582C457,
	ObiStitcher_PushDataToSolver_mA3B052D96FECDB53E4616B27E72A44F77A88EB31,
	ObiStitcher__ctor_m8703E582D33DF3E6FB4265F91A5061420D984F97,
	Stitch__ctor_mA9EBE5BCBF80CAFB90725EF8F4B9E8EA693A9800,
	ObiUtils_DrawArrowGizmo_mB0F16371F7CA701F1934AB1D68E2C543BBEDC6D7,
	ObiUtils_DebugDrawCross_m56B5756A7B53EF07A54DA1521D785F4B447A0794,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiUtils_AreValid_m36409B4F74E4DA83A742954433F6CA4EE168BAE6,
	ObiUtils_Transform_m733A8AC89C2BFBC7A0E0089512FFC4709454DE88,
	ObiUtils_CountTrailingZeroes_mD04646F64AE816B0BBE5FE3044C2DED6E90A0333,
	ObiUtils_Add_m33CB21C40D66A37C46501B8D0F17D03411F7A6B8,
	ObiUtils_Remap_m8F506FF64E3B9168DE2B0235E274CC1033B7D86F,
	ObiUtils_Mod_mD2EF1D694D52B0B1882C801A2FD576D8FC49D90E,
	ObiUtils_Add_mC0C822C7DBBF1BA01EE882208292D519B2C9C2D4,
	ObiUtils_FrobeniusNorm_m864D4DA65931429E717193CB634FFAAF31C5DCF4,
	ObiUtils_ScalarMultiply_mE25A02716B8B63129015ACA756D1DB1AD469DE51,
	ObiUtils_ProjectPointLine_m39534F2B61F19A53C004F9F72C2D5561C3BFCA15,
	ObiUtils_LinePlaneIntersection_mA331CE0E861BFF21738B92A85B6009EA4641AD91,
	ObiUtils_RaySphereIntersection_m878269850BD66F1202937DEFF22C3A31ED3B5996,
	ObiUtils_InvMassToMass_m1ECAAAEDD4E27ADA1FDE1594DACCD249742ED136,
	ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68,
	ObiUtils_PureSign_m92E94D198EF88DC390CCBA619718D6A6C27BA5C5,
	ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED,
	ObiUtils_TriangleArea_m560C3409898B4D67699FCFD0338C3ECDB244BCEB,
	ObiUtils_EllipsoidVolume_m406A1FFC6FADF93B25B2B072064B345E5DA5D2C5,
	ObiUtils_RestDarboux_mA5FBE38C5DA10FBD3F920E2F0E96683D3EF91507,
	ObiUtils_RestBendingConstraint_mA2A2E588C9B5B6907DC2670CF3176A0441D95C78,
	ObiUtils_BilateralInterleaved_m17A79FDAFD53A3C8E7CB5403AC539CD1BC4D5453,
	ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197,
	ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1,
	ObiUtils_BarycentricInterpolation_mAC20CA1234A3FB1D1D9AB2AA29D7987C0DF12361,
	ObiUtils_BarycentricExtrapolationScale_m84AD9B6B2173A5538FDB174D72AA5CE127045D33,
	ObiUtils_CalculateAngleWeightedNormals_m97CD00C800AA1BD2705A48113DD44B57E450DD34,
	ObiUtils_MakePhase_mED5844CB11913FC70EC9041F512C75E0B2F4E167,
	ObiUtils_GetGroupFromPhase_m95E80301165EB08A322A5043278D698903867054,
	ObiUtils_GetFlagsFromPhase_m6712C3AFEE4371CD66C9A5548C96FEF64A990A57,
	ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3,
	ObiUtils_GetCategoryFromFilter_mE5495C835432A19EE7A32B4E72D104A5179AD9ED,
	ObiUtils_GetMaskFromFilter_mD6E7EF064AF96C8238D05A7EB5504E72CA1ECFCD,
	ObiUtils_EigenSolve_m52D85DC48A8905EEFA96A9E0618B6569757370F7,
	ObiUtils_unitOrthogonal_m08E2004A970D966D5227E325780C23E6FAB28529,
	ObiUtils_EigenVector_m650F505BE39681115F5A78F3AB4C76D6B766E360,
	ObiUtils_EigenValues_m4B78523278F5C36F028CC1E7AB8FA13BE10FB476,
	ObiUtils_GetPointCloudCentroid_m8C1020D867B29A5A516F0DF741330765D4B47BB5,
	ObiUtils_GetPointCloudAnisotropy_m3D48980B4381A7B54F37C063FE47EF7C737CA9A2,
	ObiUtils__cctor_mE9D4CD7AA4BC90F980D0AC4F4DA0B543DFD75D01,
	U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10,
	U3CBilateralInterleavedU3Ed__40_System_IDisposable_Dispose_m8831F3EF2CA071AF13BD9D630BBA7F87306BD89C,
	U3CBilateralInterleavedU3Ed__40_MoveNext_m77C2C019B232853A6CC9102657015E05AC815D61,
	U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9E1918A629F5BD2E500DBD47B57BEA70DA7126,
	U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE,
	U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_get_Current_m82E938AAC7D70CD2629E214DDAC1F71A6BB51474,
	U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279,
	U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mBF01BD41B11190F2A9C48BE6A1A9FB04EF151CF6,
	ObiVectorMath_Cross_m23CD1AEE1CC6B74D7930C98AA1D280513238A553,
	ObiVectorMath_Cross_mA47A8583DC381295371BD66F8397DD209C2B7731,
	ObiVectorMath_Cross_m4C1FC1059C8BF006437BAA9D3CBBCD7323016E19,
	ObiVectorMath_Subtract_mD703C48EC9EAAAF9901BCB1F1D0BF46E23A8925A,
	ObiVectorMath_BarycentricInterpolation_m7CD9E0E0E11533DEB84C7003A1FBC5E415B6CAD4,
	SetCategory_Awake_mC91A0F14152A3306C3D58FFDB3268FC42CA75A1B,
	SetCategory_OnDestroy_m7E095279FCAA7AC75B26DBC31335E8C1B6156A5F,
	SetCategory_OnValidate_m96641FEA78031AF96A7904C7B89F5526989E129A,
	SetCategory_OnLoad_m105F18C77751C59F67759FE5006D36A29B4480AF,
	SetCategory__ctor_m5F2C7ADC7EAEF4C3E7B89A74858C43C67F3F9AD5,
	ObiBone_set_Filter_mB82194BCDD3BBEBC1AED600C28998746A1581139,
	ObiBone_get_Filter_m4376145C12B24B2534BA13A781D6627EB8D63881,
	ObiBone_get_selfCollisions_mF1676B154190C4BC540B52E90B80E5BAD68CFA29,
	ObiBone_set_selfCollisions_m689A7F0F793147E6D5F79361AFCC5329B9361D1A,
	ObiBone_get_radius_m1EBD5FBFDE44C849B4175A35CAD16168E5B26FCB,
	ObiBone_set_radius_mA4BC2323CFC84744775B0ADD88591372D0FB8A7E,
	ObiBone_get_mass_mDF67BD9E5FCAEF4DB88A80FF1657A6AA96269937,
	ObiBone_set_mass_m6A69DF3184A387E7F3F85DB546FB332807F4785A,
	ObiBone_get_rotationalMass_mF1847788FF19552892D6E0F6B1B97987CDD9ACE3,
	ObiBone_set_rotationalMass_m85EBF804BF8396DF8F596E775A71C6051ACF42ED,
	ObiBone_get_skinConstraintsEnabled_mE05F5A90FBEDB3EA94F4ED3C580612555DF7A612,
	ObiBone_set_skinConstraintsEnabled_mE1C59969DA5E5BC34C78362D874AE1BAA25D3EC5,
	ObiBone_get_skinCompliance_m775B8073203CF01A555595196689BFB632A4D0BE,
	ObiBone_set_skinCompliance_m9BB057DC71E2BEC64E09003D0B50B1374FDFAE8B,
	ObiBone_get_skinRadius_mA4B80A966827D27BECE9FBCE80DF47DDFD9107C9,
	ObiBone_set_skinRadius_mDA796FFAD3F1991C328DE2C596198377E5BF57A1,
	ObiBone_get_stretchShearConstraintsEnabled_mAEEC2A924CAF912C3FC5BABB5DED2461A7852C37,
	ObiBone_set_stretchShearConstraintsEnabled_m16F142EFE0B768840503821F89E6234ABDB75F82,
	ObiBone_get_stretchCompliance_mCE39CCB072F246783041A6541D5B8CEC688BF378,
	ObiBone_set_stretchCompliance_mFD349E90C44EF8AF81871E2AC3D4F9B9502BB20E,
	ObiBone_get_shear1Compliance_mBC21AE2AAA082BA20452DF7E60791F554CDFD2CC,
	ObiBone_set_shear1Compliance_m2A934368B5C05CEDE1E0A0CE2BAB406DA9F0B12F,
	ObiBone_get_shear2Compliance_mF39DF6DB3D3EBE9467D5A0BB330216717C5A0783,
	ObiBone_set_shear2Compliance_m772C23DA827D6A69EEA6DB720CF78A6E9F3D3228,
	ObiBone_get_bendTwistConstraintsEnabled_m4FF85604C8C37A41C0794FB0B8369ACF75985A74,
	ObiBone_set_bendTwistConstraintsEnabled_mEE7256D7949FB40E5D3668B644C0BAFFBAA1B785,
	ObiBone_get_torsionCompliance_mD91D07722D08A5560F784881CE6539354F6907A5,
	ObiBone_set_torsionCompliance_m23DF39FF5E9374C187262D9BC592FFA4ABBF5B62,
	ObiBone_get_bend1Compliance_m7E1CD0459126E20CEF76F75EB626B6F88F268E75,
	ObiBone_set_bend1Compliance_m0F8C7EFB8B106F6919E3D468183C3F08CDECEF22,
	ObiBone_get_bend2Compliance_mDEDA98119786B607ACBBB67FE8F6B631FD467ED0,
	ObiBone_set_bend2Compliance_mB22F9C11E42F480E19EBF055078F1C670369D323,
	ObiBone_get_plasticYield_m69B1721B7F1FD741DD62892203E0B4F49F4F7F44,
	ObiBone_set_plasticYield_m28F33914E711EEA1E3ECD99B19D84E6AF9A63B74,
	ObiBone_get_plasticCreep_m934520EB537785A61E6EFBACBE31E6AC2D542996,
	ObiBone_set_plasticCreep_m8755FCA8ACC6FF5F6288F173481AEFC0341A3A2F,
	ObiBone_get_sourceBlueprint_m6981EEDCD3CC6D846011597AC50767DAD3CB5BAD,
	ObiBone_get_boneBlueprint_m2B65BE43F80CAD94BFF5CB3D22ECBD4C5283B853,
	ObiBone_set_boneBlueprint_mCDA8F86B77B30BC2937D5E21F994968695AB59CD,
	ObiBone_Awake_m8AE667C282E0040D574D8C7F83C7BF84AEF8C39F,
	ObiBone_OnDestroy_mBA59F980819221EB8A4BD7C3B99C15BE60EB39E3,
	ObiBone_OnValidate_m9548925307E889440DE38A33C11A9137DBD4456F,
	ObiBone_UpdateBlueprint_m1DBB634F8E2A3304F4F0342E94549C207BAACB97,
	ObiBone_LoadBlueprint_m51C03031A13F084FB76F917918C6E2B3B05D15E0,
	ObiBone_UnloadBlueprint_mC6A78D9DD9C2CE95DF31B536E8CA22C1A17913F4,
	ObiBone_SetupRuntimeConstraints_m6F158B5FC17EFE474455A4C6D5D6F7938979CE0E,
	ObiBone_FixRoot_m56FA73306594480AADFE560EB7B6E7C6D89C8B96,
	ObiBone_UpdateFilter_mB579CD813F17057D517ECEAC9FDBF2F44A38B19E,
	ObiBone_UpdateRadius_m056B887BF15C8F43BF8FB1307010AE236B615860,
	ObiBone_UpdateMasses_mAD10D4E9A2F0B178F333474AB0B92B445FFB7FAB,
	ObiBone_GetSkinRadiiBackstop_m9B2BB8C02DC7941BFDF908E2636636B0DCFCF270,
	ObiBone_GetSkinCompliance_m1736CBFF470A0E948C7366A7C3348B0BF00DD178,
	ObiBone_GetBendTwistCompliance_m3DDD20F692EED64EC24A8B44B70F2823A8E567BD,
	ObiBone_GetBendTwistPlasticity_m40EDBD03EEB67FF291666E20B0AB298318198403,
	ObiBone_GetStretchShearCompliance_m3D971128CE596E9634D45F7553808B7FEC744FC8,
	ObiBone_BeginStep_m779035D44B6883800B160D4EC6EA2E4D629A4E2D,
	ObiBone_PrepareFrame_m21331B51116F0074E101E77AF749E7E7082DC1B5,
	ObiBone_Interpolate_m32367276BF665A19C6DE3F2D7AF8C98D0B18AA46,
	ObiBone_ResetToCurrentShape_m9D3AD6CC32B50881A9B435979A8B1DB5FB3C2DA4,
	ObiBone_ResetReferenceOrientations_m9883C404257269137250CFE258481796D3A22449,
	ObiBone_UpdateRestShape_m839D95FF4B72AEA0B6CB7E3F9FB42BD9D59F5163,
	ObiBone_CopyParticleDataToTransforms_mD95205602C8B3FEA4E25722D0E983891123EF0F9,
	ObiBone__ctor_m5FD25B5CA6E834E39E552AAF0F5D698D4691BEDB,
	BonePropertyCurve__ctor_mB95157224C06C09854DF712065D2AE0F24FD87B3,
	BonePropertyCurve_Evaluate_m3E05031EF7155563B70DB6BA04B28BBB3DEE39F7,
	IgnoredBone__ctor_mDC468B916D95F3913283BE1B6CD09203717C9C7E,
	ObiRod_get_selfCollisions_mE9183861A26FB1AEB33690D9670E6B2FED085DAD,
	ObiRod_set_selfCollisions_mD26D3B8B8267A0E44142AE7684E90A4E73C5D326,
	ObiRod_get_stretchShearConstraintsEnabled_m134C94FD6508FB5C983019CEFF3E19F1ABBDEB47,
	ObiRod_set_stretchShearConstraintsEnabled_m5A5893CDE5B97D9CDF92554FC1F89A00D218B425,
	ObiRod_get_stretchCompliance_mA429E219BBAFC25B96D7E6F883209CB579A22296,
	ObiRod_set_stretchCompliance_m8392980511E473452A2E51CD19C70F9BF6E5AF78,
	ObiRod_get_shear1Compliance_mBAED97836BD0BCC734D0D5205530BE91ABF443CE,
	ObiRod_set_shear1Compliance_mF41784E93B06FB466D99CFFE0F62779BD277056C,
	ObiRod_get_shear2Compliance_m174B88A01B300DE16E82B316D8A7043C00985D5F,
	ObiRod_set_shear2Compliance_m3F24261279E57EE1953CACD81CD8A26CD52EB157,
	ObiRod_get_bendTwistConstraintsEnabled_mB03FCAE861CF5B574E221DBE72AC5B0EE5885423,
	ObiRod_set_bendTwistConstraintsEnabled_m41025553FA9B05DA71539F741BCC80739E372286,
	ObiRod_get_torsionCompliance_m9B201D7D2B3A37E1FE3951415F133782B131B598,
	ObiRod_set_torsionCompliance_mB45A259015000A7797682C11DF9DB538E4DD8DB6,
	ObiRod_get_bend1Compliance_m9F78AD3F731DE5B8DE3720FD174EFF77BAC97C82,
	ObiRod_set_bend1Compliance_mC87920DDBA91089A8899EDE3ED9ADEB2EF98FC14,
	ObiRod_get_bend2Compliance_m23272235C9E69D5754340387D36F2687B50459B6,
	ObiRod_set_bend2Compliance_mE3496FD453AE558DD047128855149278EA88FA4E,
	ObiRod_get_plasticYield_m0065816F70FD4A02D7058F77887A59F64CD3D23C,
	ObiRod_set_plasticYield_m3A3A9495145722A3A58E208627AC7068272721C6,
	ObiRod_get_plasticCreep_m2C98131B098B6E95E2FB774911053A62D7EE491E,
	ObiRod_set_plasticCreep_m9703D71D219EC3B694B2990F7F1CE2E36ACB2B50,
	ObiRod_get_chainConstraintsEnabled_mF398577FB32AC2F7366B9731C74086CD31E6410C,
	ObiRod_set_chainConstraintsEnabled_m46D6C7C03D1A1A42F464DB990A43CEB8212D2377,
	ObiRod_get_tightness_m4113D75F222782F0F543F49DCFF9A0655F5FDA6E,
	ObiRod_set_tightness_m2632FBE37B4F81A0D3206339802CA6EFDCD1A70D,
	ObiRod_get_interParticleDistance_m4B8CE4FBDD98994D21B2AABCF34EF8102749E884,
	ObiRod_get_sourceBlueprint_mEA32815318034D19AC68E42451B170AD29221C8D,
	ObiRod_get_rodBlueprint_m7D87017048D1F8A1B66BB6EE13B5A071E1787FE9,
	ObiRod_set_rodBlueprint_m50978B3C11B1E0DACCED1055AF457377DEB58A93,
	ObiRod_OnValidate_m0E1BE0E449175DB9D36961851E32E455FB33C0FA,
	ObiRod_LoadBlueprint_mA932979582E924746DFC470C837AAF706DD98E45,
	ObiRod_SetupRuntimeConstraints_mDD133102C5C36FF988973EE643D0288FBB1F8F12,
	ObiRod_GetBendTwistCompliance_mCFC552CA6216A5C88F270C85A07AB6F0CAE62FEE,
	ObiRod_GetBendTwistPlasticity_mE6D70FBE4283FFB50A6802CE67381357562E770F,
	ObiRod_GetStretchShearCompliance_m7F5CED48D01A9E1DE36E5E9ADEC9BD77CAED742D,
	ObiRod_RebuildElementsFromConstraintsInternal_m06CD554B61B7E3639D1F0E57D70C31EDE68DA165,
	ObiRod__ctor_mE0D8CB442D6F5CDA019C1B9352B9A5B96B3ECFFB,
	ObiRope_get_selfCollisions_m466A37ECD8D40D71A1EF9BE9A59A8D182F3522B6,
	ObiRope_set_selfCollisions_m14B2A380D7B14B3B11621718ED371C0DA1914F30,
	ObiRope_get_distanceConstraintsEnabled_m014F78BEA9B263AC58C1908AB2AA16469283A212,
	ObiRope_set_distanceConstraintsEnabled_mAC6131E60DD536D5ACA5DD5AC54944C0042C6D89,
	ObiRope_get_stretchingScale_m828729DFA156687DF38B0EAFF40EA9EC5EAFCFE6,
	ObiRope_set_stretchingScale_m2AFCA9A3B98C34FD435194036D29CDD739F23AEF,
	ObiRope_get_stretchCompliance_mA5A38FBB9984BF0C081D39FE1F9145BAABF5B3DD,
	ObiRope_set_stretchCompliance_mA703D9C1430BC2DA0C59D1D43DC5C6D87DB71DD3,
	ObiRope_get_maxCompression_mD8AA2252CEDCBF8B8FDB0E06CFD6A0CF7EC3F341,
	ObiRope_set_maxCompression_m9C1AA47C47EE6F4D743CD3D5AC90FDF1D0359DC2,
	ObiRope_get_bendConstraintsEnabled_m81710C435F918413DF98DEB226BDB6269991094A,
	ObiRope_set_bendConstraintsEnabled_mA265575A5F1993828D7A5788F8DA29B527C9805E,
	ObiRope_get_bendCompliance_m3F8B6B00DCA2A3A4288785B840F61DD76B883C56,
	ObiRope_set_bendCompliance_m5DADBD580299BB6B83FE543B7813F4AA0EFFD7BB,
	ObiRope_get_maxBending_mBB0783E2F3C4F93611CD827193D031CFFC46509A,
	ObiRope_set_maxBending_m089CF2E6791FE7573E2F4C172341B33AF7855729,
	ObiRope_get_plasticYield_m43A5F27180E61D99A230C0F8EA743215B4590090,
	ObiRope_set_plasticYield_m6282A37741B42F7F489BD9CA4F3FD22040A8CCCC,
	ObiRope_get_plasticCreep_mB625840F0032B6000A4C0BCC4B84D39EE10D6C7E,
	ObiRope_set_plasticCreep_m3E26642B38EF1439982FB67D5F4B4730E3B9EA97,
	ObiRope_get_interParticleDistance_m3D6B5CB17F7CCCEC7F3E3FF619B15ACF2AFB2740,
	ObiRope_get_sourceBlueprint_m778885AC5ACB50CF2D5E2283F644CCB58228723B,
	ObiRope_get_ropeBlueprint_mB585217A4141B42877FDF506957447CBEE268856,
	ObiRope_set_ropeBlueprint_m9420BEF4A7365801B289A53420BC132BF1E7905F,
	ObiRope_add_OnRopeTorn_m82818AFDC4E5B53B9BDB3BC5F448DC5819AEAF17,
	ObiRope_remove_OnRopeTorn_mDCCBD9E8FEAA87E7DB5622737423840AD4EE1A5F,
	ObiRope_OnValidate_m2DE95ADDCC76CF26B486F895D82513F3888C4CB5,
	ObiRope_LoadBlueprint_m40B2EC79FD19E8746D04A5E6B5EE11BDF4186D4E,
	ObiRope_UnloadBlueprint_m0C3352A53979E5701391C9A8DA9489DC60A01DC9,
	ObiRope_SetupRuntimeConstraints_m4E7E18E6F869E2C2497931390EF27C2A88FF2F83,
	ObiRope_Substep_mEF3BE7DF70726281DE8908A31C95AC724F55B387,
	ObiRope_ApplyTearing_mB720B5C8911A09FA2154066F9D8E2525FBEDC91E,
	ObiRope_SplitParticle_m7FF40778B8998EB1090521401FF3373B1444EFAF,
	ObiRope_Tear_m612ED4759B7F149F4263E218FFCDF9A6374BD08C,
	ObiRope_RebuildElementsFromConstraintsInternal_m5185A20153E1C85F8609AC97E69045370C6992CC,
	ObiRope_RebuildConstraintsFromElements_m16F44C93051F66275628E315018CCE104BE67F7E,
	ObiRope__ctor_m4A887B1B2064D68904BC6C7A1ADC89115A4ED4F7,
	RopeTornCallback__ctor_m74940736BB0FE86E5D9978DE57B83F6514334BEE,
	RopeTornCallback_Invoke_mA260D7064D0816B8431CD1EFF00C0D5056B6CC55,
	RopeTornCallback_BeginInvoke_mFC33BEC1937BA04BC48653AFA9D75E530B95525C,
	RopeTornCallback_EndInvoke_m9E58F646BB4EDF02833D0D8A97BF71746B3C5712,
	ObiRopeTornEventArgs__ctor_m9FDD4BA5BA9BEFAB7A96904A52289FCEA3624DF8,
	U3CU3Ec__cctor_m1EB58583F6224716B470370411E9FAA7866AD049,
	U3CU3Ec__ctor_m4B4378CC0AB345511F93C07572A6368778B3AE8E,
	U3CU3Ec_U3CApplyTearingU3Eb__61_0_mB850523B03651A79FDF5AA49504936E26D51D959,
	ObiRopeBase_add_OnElementsGenerated_mD9B02AE3DC85CC5762C7AD9F125360E17E1566C8,
	ObiRopeBase_remove_OnElementsGenerated_m628F81AC8C51256C7D4EA46CAC6F983413DB27A4,
	ObiRopeBase_get_restLength_m7D49C2A8AB446A1D437FE693B1288E0190B53C0C,
	ObiRopeBase_get_path_mAB91CC45421724C51566122E9F627241EFC2AD44,
	ObiRopeBase_CalculateLength_mDDE1BCD878BB9B456E2FD2FCF4F04F7CC6839108,
	ObiRopeBase_RecalculateRestLength_m57DADABA77123543C1FA768D1971CBD262AFA6A7,
	ObiRopeBase_RecalculateRestPositions_m9FCDE898B1F9A428A93B793D32430CC20866E3BA,
	ObiRopeBase_RebuildElementsFromConstraints_m4820BCBA928B7734C90749DD35DC2159E7DA8488,
	NULL,
	ObiRopeBase_RebuildConstraintsFromElements_mFB74A59B9584885713FA06A0B416101549970B9B,
	ObiRopeBase_GetElementAt_m53793A9718C26B5F040CBC3B51080E4563AB6500,
	ObiRopeBase__ctor_mFF02FBF9B1434DE5C413D9A516DA5955BCF1E2BC,
	ObiRopeCursor_set_cursorMu_m022763DC739C25672480EB812DE4BA33BA3DD2D4,
	ObiRopeCursor_get_cursorMu_m09C322830636F58E9EF1D9B1B8659D36C80DB568,
	ObiRopeCursor_set_sourceMu_mB2211CE8E6FCE96B94E33196321BB776A1E40618,
	ObiRopeCursor_get_sourceMu_mE49A7218FE50A40864B2A0B3BC9BB51407F6FC79,
	ObiRopeCursor_get_cursorElement_mA9BAF8D5547126DF99C3E55E2B39D806D8910A1C,
	ObiRopeCursor_get_sourceParticleIndex_mF2412A00A51B02B0F3C804A3F0394CF961A81F06,
	ObiRopeCursor_Awake_m58F08987C4F3D569129ADDED494377D435B9B8F0,
	ObiRopeCursor_Actor_OnElementsGenerated_mB1F6111BCA3CDA17D0127D564C035759B3682147,
	ObiRopeCursor_OnDestroy_m7CABECB180A9792C501341E4FFE539C5AF8AEBE2,
	ObiRopeCursor_UpdateCursor_m63E06723DAE8EDCEA5FCF7A2E8AA1838ADDB926A,
	ObiRopeCursor_UpdateSource_mED8CE6E7CCA321A83BD2320542029B58B6FF13E8,
	ObiRopeCursor_AddParticleAt_m18D67195977538581D4587C98BD24DE0C632BDF7,
	ObiRopeCursor_RemoveParticleAt_m019EE0305C73EB9E85D2046E128167B70D879B65,
	ObiRopeCursor_ChangeLength_m10B57C115599C5E27867F6787E5824D68F86E61A,
	ObiRopeCursor__ctor_mB7E00FFC6CF7FE3731CF31FC87F2DCD6B22AF7EA,
	ObiBoneBlueprint_GetIgnoredBone_m0C8F53B2CDAEA8363812DDA4135084D0954C3D04,
	ObiBoneBlueprint_Initialize_m2A9787CD6FC33DBD5B198E7D667BE12DE690C382,
	ObiBoneBlueprint_CreateSimplices_m6481927058B5F1BB3AD6BCAFF6C39C247704E87F,
	ObiBoneBlueprint_CreateStretchShearConstraints_m20DA40DA29C69B617A52422B5084F44019F99604,
	ObiBoneBlueprint_CreateBendTwistConstraints_m61BD7C27BF0655EA303B4E58C0B98D04B424D6B5,
	ObiBoneBlueprint_CreateSkinConstraints_mE42F073A01A3AD9EAD21A01B6FB490F178E695CB,
	ObiBoneBlueprint__ctor_m7E4730BB7CB95139E82325CAC84CEEAF3A71C4DA,
	U3CInitializeU3Ed__14__ctor_m27140A7DF8EF3E999F1FC723535A04C964525F82,
	U3CInitializeU3Ed__14_System_IDisposable_Dispose_m88FFDD6CFBB11319DDBBB0F8B34089B80C4F0F01,
	U3CInitializeU3Ed__14_MoveNext_m81A96994CEF377D7F2D99824DD3B417A0BC82849,
	U3CInitializeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC52279203D514C26549053545D8AF9B41AEB60F,
	U3CInitializeU3Ed__14_System_Collections_IEnumerator_Reset_m9EC41DFD1D0FFE3D465A64CEE388F88EA13B2D0B,
	U3CInitializeU3Ed__14_System_Collections_IEnumerator_get_Current_m5A676E16757268FF757D0DC38F8AD1E8EDF2EEA4,
	U3CCreateStretchShearConstraintsU3Ed__16__ctor_mC9883EA29286F581B5BE6546F53E1803A36FC572,
	U3CCreateStretchShearConstraintsU3Ed__16_System_IDisposable_Dispose_m6F122F292C0A6014F0DFBAAD6E22DFEFEF7F74F8,
	U3CCreateStretchShearConstraintsU3Ed__16_MoveNext_m73750DBF37C0962CF3B97EA763465CB3D2590931,
	U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C73E4EA2DE56F4924F1F4D440D8DEBA59271AD2,
	U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_IEnumerator_Reset_m622BC462DD1E9B4018F1F51A846510E62CECDA84,
	U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_IEnumerator_get_Current_m4A178A0000B1F11C4A79E52395DDC3CAB3B647C8,
	U3CCreateBendTwistConstraintsU3Ed__17__ctor_m97129288678462023812E6355E3F1BEB10871686,
	U3CCreateBendTwistConstraintsU3Ed__17_System_IDisposable_Dispose_m7FA46F1AE20471FF759E6AD807D8CFD6E46DFD2A,
	U3CCreateBendTwistConstraintsU3Ed__17_MoveNext_mCA47FFAA72F9266F9F71AF9F3D9FA22EB1E60E54,
	U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB14225394B51F4DE1158DFBBFD03146485847A8,
	U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_IEnumerator_Reset_mE9692B1811A0104396D417403B04677B7C8903DD,
	U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_IEnumerator_get_Current_mACB8F571A51435DCA534D455C05DEC2BCBD56BBA,
	U3CCreateSkinConstraintsU3Ed__18__ctor_m01559B1A25470AEFB06D7A19008967898DB8A6FA,
	U3CCreateSkinConstraintsU3Ed__18_System_IDisposable_Dispose_m13FC3CA635500255354816726C2A0334D420AAA0,
	U3CCreateSkinConstraintsU3Ed__18_MoveNext_m1F87A21158EA0BF7D8F342934552AFD30CC86417,
	U3CCreateSkinConstraintsU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m025792FC9DCA9844F1948AAF26303D5DC3941422,
	U3CCreateSkinConstraintsU3Ed__18_System_Collections_IEnumerator_Reset_m989EDC08F01688A58194BA6518DE7A3D9F10934A,
	U3CCreateSkinConstraintsU3Ed__18_System_Collections_IEnumerator_get_Current_mC414552ED2BBDCAA4A581F8CA9BC1CCAC78D9B7F,
	ObiRodBlueprint_Initialize_mB0D5422A4133FF08FC0F52DA417299A628B98B26,
	ObiRodBlueprint_CreateStretchShearConstraints_m9413EBB5F95ABFD44A15EDF6DA9CC67830A98C6A,
	ObiRodBlueprint_CreateBendTwistConstraints_m5F1A04D6C343F07E764A8641AA54EF2EEB2162E8,
	ObiRodBlueprint_CreateChainConstraints_m665ECE23FA005CB35338951024B674EB2172B5EF,
	ObiRodBlueprint__ctor_m23C27EE3D33A28E5D542EE7898FBBCCA0E06FF00,
	U3CInitializeU3Ed__3__ctor_m41BDF4E1CF1181A4B67AA1298FF91C848705149D,
	U3CInitializeU3Ed__3_System_IDisposable_Dispose_mAAB9B58959E17D6316D0376867E1ADEA76D9F1EC,
	U3CInitializeU3Ed__3_MoveNext_mF4E8C487EB520B7BB8AC9C68DC554D6ABB40D2B4,
	U3CInitializeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD199EE52973224E0C633E71C904164111168B9B7,
	U3CInitializeU3Ed__3_System_Collections_IEnumerator_Reset_m70CE6B4088FBF17D28720288E2F251679B22BD11,
	U3CInitializeU3Ed__3_System_Collections_IEnumerator_get_Current_m6B81FD82D37E5ED58B0B639232283B9902ED448B,
	U3CCreateStretchShearConstraintsU3Ed__4__ctor_mDFFBA8618ED30102037B3C56180A9BEA9487C330,
	U3CCreateStretchShearConstraintsU3Ed__4_System_IDisposable_Dispose_m89DF329DFEB0C57A2E9159B524D238E75D784621,
	U3CCreateStretchShearConstraintsU3Ed__4_MoveNext_m35DE1D56E478B7D4BBA5ECF95C387263211AADF8,
	U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48490361DF35F3911FA88CC9E8488B9976632C81,
	U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m3EC8612B472DCDC0A5BA07CF265742F7ADB074CC,
	U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_mDF13C68A7A9199168F5434C5FDAE03DECED69AD9,
	U3CCreateBendTwistConstraintsU3Ed__5__ctor_m74EC6A8D57860822339D5CCF9A93DA20335836FD,
	U3CCreateBendTwistConstraintsU3Ed__5_System_IDisposable_Dispose_mCD67BAFE6581BBC3F6B167D165F996E217FA17A5,
	U3CCreateBendTwistConstraintsU3Ed__5_MoveNext_m8A6621E1C88EE695336ED524D65C9C7A3FE7A932,
	U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE73B33CF3970D7E2EC7E22CC25D72B5F318EEBD0,
	U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_IEnumerator_Reset_m334DE000C0E71E3904B0922ACE92B45250B40DAF,
	U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_IEnumerator_get_Current_mDAD52D5C97FC4B1965C20CF8C10650EF73423A09,
	U3CCreateChainConstraintsU3Ed__6__ctor_mCCD0039AE63FA7C9AF769CB6DA517174CFE80C80,
	U3CCreateChainConstraintsU3Ed__6_System_IDisposable_Dispose_m8D90A9C0F15B87BFEE0F460C6F3DA96A7595273C,
	U3CCreateChainConstraintsU3Ed__6_MoveNext_m1D5D41CD0B98482AACC5F3425B74A62581E06E4A,
	U3CCreateChainConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m15E671F86EEE570870E5A45BF66B72D282E1CBD5,
	U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_m394694E12A8EAEEB4EA80649BEA9BBE861C306FB,
	U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_mC6B36F52F2460C2F7E972D940E2F69B588832FED,
	ObiRopeBlueprint_Initialize_m253C93BBFF0249E6F38C1B014E0FC4D6B0263F30,
	ObiRopeBlueprint_CreateDistanceConstraints_m0179DE68BDE1A682413705118D3034FE4070A492,
	ObiRopeBlueprint_CreateBendingConstraints_m38CAD4B112A942FDE1242E78797329EBF7F0F468,
	ObiRopeBlueprint__ctor_mF24D397BFF2093F7F4DE1D9F82C2223E408DC5D5,
	U3CInitializeU3Ed__2__ctor_mE9943407AC4BCDD21A5F1BB4759F285BAE447D8D,
	U3CInitializeU3Ed__2_System_IDisposable_Dispose_m97A6F5247B0287130D582F2BB4E544062A689B13,
	U3CInitializeU3Ed__2_MoveNext_m0A32F555A52037AFECFE0D01A86660C18222209F,
	U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAF2CC89A62774D134AF088ADE0119C979EB1F72,
	U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m7A4B24373876BDE2453048631F2B963E386E7D12,
	U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m16BDC9B0E8C7DB5818965339EC2F1EFEFEF37DD9,
	U3CCreateDistanceConstraintsU3Ed__3__ctor_mC2881BA9A2C3B5FFD584032842FC4D8016768534,
	U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_m1CE3F312F7B66A397DE190A3983DA6CB907431EC,
	U3CCreateDistanceConstraintsU3Ed__3_MoveNext_mD9AAE5B282EF03644F559C1BC11856CE97E7F88A,
	U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74A1152B6135923B3BAACBAC1F2F5932512D8046,
	U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mD6AC7761E802DE2F6F0CADB0A5682051E6E4669C,
	U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8EF851B496CA7FD1CC7F31747EEF8484A7FCBB15,
	U3CCreateBendingConstraintsU3Ed__4__ctor_mF09A262B24850675B97784C745083E957AC70DDB,
	U3CCreateBendingConstraintsU3Ed__4_System_IDisposable_Dispose_m1ACE661B7DC059891776A5AD8632DD995321EBA3,
	U3CCreateBendingConstraintsU3Ed__4_MoveNext_mBF7AB4425E651E4E8AE9B8FF0406A4DE65085F8E,
	U3CCreateBendingConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD5D1A5BD31642785B698F9B9734EBE8775F45FA6,
	U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m8B106EC62D22296800CE67482EE3F81810C06907,
	U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m9475B499E2444C3687CB7A86ADA02EFA9627693A,
	ObiRopeBlueprintBase_get_interParticleDistance_m81CE9A357F6D263A01ACEE7B9132C261D8CE4DB4,
	ObiRopeBlueprintBase_get_restLength_m72D335E89BFE161985036DCB2531C82C35C71FE0,
	ObiRopeBlueprintBase_OnEnable_m787B9D3FA1853A5AECF34A73E56E853B0007D298,
	ObiRopeBlueprintBase_OnDisable_m6A820811B66A7E2EE024185C0C50FC5A39F03D28,
	ObiRopeBlueprintBase_ControlPointAdded_m8C4A8D8F9D775A1CDD82F6441C825A24C016EE48,
	ObiRopeBlueprintBase_ControlPointRenamed_m3BACB0CD0C836BF1D12A67F66E6DCECCA03EDE18,
	ObiRopeBlueprintBase_ControlPointRemoved_mE7FA0A89190C4005F244D75BE7A05676B2B7BAC9,
	ObiRopeBlueprintBase_CreateSimplices_mB064904BA09A240A79F509214EDD53AF206F9FDD,
	ObiRopeBlueprintBase_Initialize_m49A4560B16A115658987C79F148B7D9B9983A5A9,
	ObiRopeBlueprintBase__ctor_m3A167140A2569F4196A84492072FF673189C1C4C,
	U3CInitializeU3Ed__17__ctor_m52F67707D008A9F9B010B0538ECEE7A5CC69F2EB,
	U3CInitializeU3Ed__17_System_IDisposable_Dispose_mF0B5E8B1440934FC4C19F4F949EACFE581448839,
	U3CInitializeU3Ed__17_MoveNext_m22EAFC8213CAC2C43D4012E007F6CE8431DD155D,
	U3CInitializeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DD4C0990CD4B35386F56E79FF03AA864B1D6C21,
	U3CInitializeU3Ed__17_System_Collections_IEnumerator_Reset_m28AA9E89D0D2B4BD55BE44CC152105CAB3FE46A7,
	U3CInitializeU3Ed__17_System_Collections_IEnumerator_get_Current_mC609C587F1D824ADC1D1CD66C9D51AB95F222BAA,
	ObiRopeSection_get_Segments_m0F3B7788562D36702926C94384D7A2512C5BA8AA,
	ObiRopeSection_OnEnable_m72E432ADC85EFCB53ADF526067B2DDFAA41EF4D8,
	ObiRopeSection_CirclePreset_mF0CFC0E074EF29DABDB6F8DA3C92F4B90E8040F0,
	ObiRopeSection_SnapTo_mB8FEAAFD26343973EF029E6F95E8949E7B99177D,
	ObiRopeSection__ctor_mB4EFF529E32758C69BBE6BEB984CA992F52BFB1A,
	ObiStructuralElement__ctor_m0DBA768A6C7DCF64B0CA06942AFBBC9FAC656088,
	ObiColorDataChannel__ctor_mF7C3988C6EDFA5576F0271264869AF1673DAE555,
	ObiPhaseDataChannel__ctor_m59B976ADAC7A5050F6B898A4D7B3443C2B42663D,
	ObiMassDataChannel__ctor_mB2FA019EFE8667AF089AB5CE4B174C2A7CAEC504,
	ObiNormalDataChannel__ctor_mFE5B22AE6C83DC020B2BFD7B7C2A10C87DDCDE1B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiPointsDataChannel__ctor_m0E32942FBF0EFCFCA256E7FC1DF684B7C8EF7BB3,
	ObiPointsDataChannel_GetTangent_m672DE54B725D53690F1E830D2AFFD37EDC4400FD,
	ObiPointsDataChannel_GetAcceleration_mF055C83DA7E61032FA54EDD296B23E930021C54C,
	ObiPointsDataChannel_GetPositionAtMu_m5DA1AF5F4A0C5EB6EDAE4AA729DAC149E3E17DF5,
	ObiPointsDataChannel_GetTangentAtMu_mFBCBAB990A9552E85C983C74226D8C2FF4DD5315,
	ObiPointsDataChannel_GetAccelerationAtMu_m11001BB62AD2418364B3C5F6E3FBB73D56A02E8B,
	ObiRotationalMassDataChannel__ctor_m0AEEA1343FEF24CF70B782C9A6648BEE0B61663C,
	ObiThicknessDataChannel__ctor_mF56BA457503E1D6B5E1E543974E750CF19969946,
	ObiCatmullRomInterpolator_Evaluate_mFE52C9F643FDE2424B49500063A4084B2A4C2690,
	ObiCatmullRomInterpolator_EvaluateFirstDerivative_mBD404A2D620B41F2FF54DADC0174A06686344B4D,
	ObiCatmullRomInterpolator_EvaluateSecondDerivative_m7BDDA2C407FE631C7947DC09B5E1DDE3CCF783AF,
	ObiCatmullRomInterpolator__ctor_m3E37199D2CFE50A52C5A9CA7FD180BE3B876FBD5,
	ObiCatmullRomInterpolator3D_Evaluate_m0E59C94236EB4852BF72D9345C8CBD50304C1EAF,
	ObiCatmullRomInterpolator3D_EvaluateFirstDerivative_mE0801CDF9845DDE32858B014A36B1FABDE0F6E31,
	ObiCatmullRomInterpolator3D_EvaluateSecondDerivative_mF539CA500C3944D61F6387AF19F393C5F70DD5E8,
	ObiCatmullRomInterpolator3D__ctor_m26DF84ABFA78D87532ABD407FF68239528022177,
	ObiColorInterpolator3D_Evaluate_m35B7EA4CB54DA824576F42337D5233EDC7BAB4AB,
	ObiColorInterpolator3D_EvaluateFirstDerivative_m54335AD741451FB23370BCA6B56037620B1705FB,
	ObiColorInterpolator3D_EvaluateSecondDerivative_mC8E7322FFED86AB60DE8A989362696C39F321C73,
	ObiColorInterpolator3D__ctor_m3D3CCBEC0E7298A1339CA213F361713DEB3A53A0,
	ObiConstantInterpolator_Evaluate_m785DA19E7352DFFE33F1469F63CF75ACB14C76E8,
	ObiConstantInterpolator_EvaluateFirstDerivative_m621BF807F926A21E4CDFD01631278440C25DFDCD,
	ObiConstantInterpolator_EvaluateSecondDerivative_m821B9B0376A707CE06B853973110070B49FB4A62,
	ObiConstantInterpolator__ctor_m3A4A87D67512E15BD41B98827A3BF47176AF79CE,
	NULL,
	NULL,
	NULL,
	PathControlPointEvent__ctor_m99C8384E8222613B455CE373869A48ED20B34B40,
	ObiPath_GetDataChannels_m971B8134825853C6010D8D02446AB4EEE35CE5C1,
	ObiPath_get_points_m6F044F0A220D99C2C5184CB5FE1769BAED6FFD3C,
	ObiPath_get_normals_mD27D9F0689AB44FF831629FC2F0F7F2BC5EA08F6,
	ObiPath_get_colors_mB1315A257BB672D26575C3CEFD3FC642D65C8C6F,
	ObiPath_get_thicknesses_m435418A103FD1CC0D151EF3B75689969F0E23C8D,
	ObiPath_get_masses_m563E313A7734C29F2476B68228BAB71731EBC326,
	ObiPath_get_rotationalMasses_m65A0959B14AEBD424FC35853DF6EBEE9A4D2648A,
	ObiPath_get_filters_m6D02BA0F0D6E24496882A16B37C12B185D9CC028,
	ObiPath_get_ArcLengthTable_m473E96A0299C036CB1835FE04F20733B6D824C81,
	ObiPath_get_Length_m702A025AC3A2C08D2F9629728753916FA662A7E9,
	ObiPath_get_ArcLengthSamples_m3C36FC6DDE2FBD76AD4F46EB7C5A17FDC4B1D9BA,
	ObiPath_get_ControlPointCount_m09869836E934AC2B253907636DDE6D72BA95D05F,
	ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856,
	ObiPath_set_Closed_mAA84C7E40584DA3417206E5F3BAF0B9ACB858F19,
	ObiPath_GetSpanCount_m09199A6F5BB9A1619E0A8A7FB826FA1472B29623,
	ObiPath_GetSpanControlPointForMu_m7D575E28219797CFC37B35F0219962D3EE81FFB9,
	ObiPath_GetClosestControlPointIndex_mA8A32BE9790A6E266966BF8A2520126E80BACF51,
	ObiPath_GetMuAtLenght_m9B36DC8C812923123B8D5F9D290BAD46CF555E6B,
	ObiPath_RecalculateLenght_mDF1CD450DBBA7B9823BA8090C340595338260C0E,
	ObiPath_GaussLobattoIntegrationStep_m49FA0CE206E86C254C4A1A5A6F1E7B3F272560C3,
	ObiPath_SetName_m6C56DDCAE3B62E48F3F24BF93336D06A33A7DC9E,
	ObiPath_GetName_mF8D4C50F9267FF07E8507593D59314ABB3691ED1,
	ObiPath_AddControlPoint_m85A6300B52B202EEA3CDB7BEBF90205E2233B041,
	ObiPath_InsertControlPoint_m978D7BDBC828882C6D3B1C85F029C6B1BC63BDDA,
	ObiPath_InsertControlPoint_m4E84171B74A483ECF63FA71B829B6B78372FB02B,
	ObiPath_Clear_m672C64B04F5911FF4628FC18C3D3C957EEEB340B,
	ObiPath_RemoveControlPoint_m013CC0D417CEC11360D7684269DA273C04951431,
	ObiPath_FlushEvents_mBCD68B863C6A2FE354C2D9D1013F13611B71E003,
	ObiPath__ctor_m1137C422538A8B28723575D3C358855E737CA02D,
	U3CGetDataChannelsU3Ed__17__ctor_mD0F362BFD59ACE15D4D3A884E8226BF10577B2B1,
	U3CGetDataChannelsU3Ed__17_System_IDisposable_Dispose_m1491BD58F339286AD9852B89147455FFADE4FB35,
	U3CGetDataChannelsU3Ed__17_MoveNext_mA67F3AF60338CC6FD16F4C888F7770FA03B3ABCE,
	U3CGetDataChannelsU3Ed__17_System_Collections_Generic_IEnumeratorU3CObi_IObiPathDataChannelU3E_get_Current_mA4EFA4B8901E742332F05D5737EA190D71393707,
	U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerator_Reset_mEBF818027E97B5B63CBDDC30BE2E6B48F896B7DF,
	U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerator_get_Current_m88CC0E7304DD9F211246F7A845B040ADB99D4662,
	U3CGetDataChannelsU3Ed__17_System_Collections_Generic_IEnumerableU3CObi_IObiPathDataChannelU3E_GetEnumerator_m796DAF5CF8553C1133063B7487009CBB8F6E31B8,
	U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerable_GetEnumerator_mD094D18AC457EA20B62A9CCF2837713884B4A7A5,
	ObiPathFrame__ctor_m025EA159EAFA9E15FAD8CF083C002438D6A43641,
	ObiPathFrame_Reset_m6589829D261335ABE1D4733AD2BA58A2E8D5FF15,
	ObiPathFrame_op_Addition_mDDBC2765A80B58557DD24F16754B91B7517D572C,
	ObiPathFrame_op_Multiply_m84C683E4981AF52397CFF8FC7F845AC6A3F79BB6,
	ObiPathFrame_WeightedSum_m4EC5C0E01A31BCCDBBA1B0EB371819FBB4F3F14C,
	ObiPathFrame_SetTwist_m15557FA7BEAF865EC68821EA1CCC3D349855F667,
	ObiPathFrame_SetTwistAndTangent_m820B046082B1FA969D75F517E60F14B6C888B9A2,
	ObiPathFrame_Transport_m30DB5DB5B8D169DF5B54D56BA7B39A776604F769,
	ObiPathFrame_Transport_m0B1B3CA7069547BE12B49F7FDB4E67EEADC239D4,
	ObiPathFrame_Transport_mE3DC94CBBB61AE7CFC04178FADD5862011E485B0,
	ObiPathFrame_ToMatrix_m3941E65FC0FD3C60411092AAA8CDCF24EEE1E5C5,
	ObiPathFrame_DebugDraw_m5397213F80C59B56BBA30A19B770A3E88BF6712C,
	ObiPathSmoother_add_OnCurveGenerated_m1612F15B48776929153BBCB1C62F5B1720320755,
	ObiPathSmoother_remove_OnCurveGenerated_m0136223598B7485378680ECE56861613BAE13476,
	ObiPathSmoother_get_SmoothLength_m7C8EA10C055F41D716ADAB7A563FB5BBF44808DE,
	ObiPathSmoother_get_SmoothSections_mDEDF3D59D8F205E1399B0F48B25881DFAA05F21D,
	ObiPathSmoother_OnEnable_m581B0AC297BE743EBB2C7B1E4E428B18B2E69F90,
	ObiPathSmoother_OnDisable_mF9E9D6D7B24926240864E7B42A75F31541765879,
	ObiPathSmoother_Actor_OnInterpolate_mEC73625A45D9D51F38E00713D03FD6AA4F401339,
	ObiPathSmoother_AllocateChunk_m52584A3348E9882935F367F8E976D75A84E39189,
	ObiPathSmoother_CalculateChunkLength_m11557F5F54F9B2B489FF80721D6D478FF73BFEEA,
	ObiPathSmoother_AllocateRawChunks_mD0E6F4F45ADACD8737EF080CEAD40287208BD318,
	ObiPathSmoother_PathFrameFromParticle_m51B6C4C4E523E705AE61BFEE3DAD380C383D2344,
	ObiPathSmoother_GenerateSmoothChunks_m924257E4279D037AEDB15E1D78C8A3287DC62AC9,
	ObiPathSmoother_GetSectionAt_m2D886BB2898A8849CF2B29DFA59F079FE2B47FF2,
	ObiPathSmoother_Decimate_mBE4DC0A5377EAF01B6130BA89EB6EA8B00CB8983,
	ObiPathSmoother_Chaikin_mAE5894E878D1DB84E10229EEE6C2AE63FC8A90DE,
	ObiPathSmoother__ctor_m661E02B23AEFFCCD7A0ABF2634E8FDD9E80F7DFD,
	ObiPathSmoother__cctor_mABF2352E628EEE6E56ABB615D2DC85067B424E24,
	ObiWingedPoint_get_inTangentEndpoint_mB04887363665D698ADFB0964437641EBEE625AAB,
	ObiWingedPoint_get_outTangentEndpoint_m13FD62B607A3335392B17CD3230F7FFDC8D14A0D,
	ObiWingedPoint__ctor_mAD5A99BEA0E2B2C4D6563AB336276D0EFC057EC2,
	ObiWingedPoint_SetInTangentEndpoint_mC89AD4F0A7639ED4F2213166497F9B9B07399039,
	ObiWingedPoint_SetOutTangentEndpoint_m2F82C9A47C3FF51341B24470191045CE79BB9B9B,
	ObiWingedPoint_Transform_m59146866606ECDBA07E40D3525E70F59AC5A6BC4,
	ObiRopeChainRenderer_Awake_m3BB0C150D1E9F76BB6060CEF9CA4D39AD685EDA7,
	ObiRopeChainRenderer_get_RandomizeLinks_mF00C148739F0B41CB0BE6F52E5163C3704877BD7,
	ObiRopeChainRenderer_set_RandomizeLinks_m995C2895CCFD6B6E2670D4405066C8110D91DE52,
	ObiRopeChainRenderer_OnEnable_mC4FD146269A944E94A17500B5F72A434AFFB10D6,
	ObiRopeChainRenderer_OnDisable_m274BF4B80ECE9854B7FE714FCA648166D1E713DB,
	ObiRopeChainRenderer_ClearChainLinkInstances_m91C00166579736A08196135BE8467B5084A1A63D,
	ObiRopeChainRenderer_CreateChainLinkInstances_m13D114CD5DB8B93E820AD9B71EE407C7099D0FDB,
	ObiRopeChainRenderer_UpdateRenderer_m66D06E408B6A3A1826F7C88540D354FEBCA64225,
	ObiRopeChainRenderer__ctor_m856DF5AF44EE731B5FCD2C17531D730E2C7E1F97,
	ObiRopeChainRenderer__cctor_m864544CB0188E7CB1C1C319BC872EA95440993B0,
	ObiRopeExtrudedRenderer_OnEnable_m2CBE22BF32CEA5D15FB376095ECC9D76839335C1,
	ObiRopeExtrudedRenderer_OnDisable_m1DE835B7E05BAD3282274C6E3CAED96ACD7CE108,
	ObiRopeExtrudedRenderer_CreateMeshIfNeeded_m53BCE7E556FF5C8027B75A15AC2EB70ABED08CF2,
	ObiRopeExtrudedRenderer_UpdateRenderer_m30D59CBD734BCC7DD135BB5A4BBAB0F14291BE79,
	ObiRopeExtrudedRenderer_ClearMeshData_m1AE1E6DB7365ACF85A024E5071187D2DD4A60A9F,
	ObiRopeExtrudedRenderer_CommitMeshData_m48FE391A3AD444A343E6C21729825B5A28053E23,
	ObiRopeExtrudedRenderer__ctor_mDDCD16FB03BDF41D73831C7665A454C4A011BE6D,
	ObiRopeExtrudedRenderer__cctor_mBADC91968E87645ADF6F00562C8323E13DD36338,
	ObiRopeLineRenderer_OnEnable_mE43229CC36A78E88C87242842AA884CEBEB19805,
	ObiRopeLineRenderer_OnDisable_m3FFE0911B8FFD320C957480ADEB5353F6159FE6B,
	ObiRopeLineRenderer_CreateMeshIfNeeded_m51BC08D0F383756CAE8790B9A54BF5AA624DEC45,
	ObiRopeLineRenderer_UpdateRenderer_m6587B55D0CB96E9E7AC8405B962EDA575229D425,
	ObiRopeLineRenderer_ClearMeshData_mB23CB53F6BB94B1AF8E2288441513BB6B353BAA3,
	ObiRopeLineRenderer_CommitMeshData_mDD9B2C824B4D6FC36E9446190EEF98A28D770B02,
	ObiRopeLineRenderer__ctor_m77286C9C570003CE8C5BB5F6AF51CEC1394799FE,
	ObiRopeLineRenderer__cctor_m28AC5331FB155ADD575216CD6DED626398A4FABC,
	ObiRopeLineRenderer_U3COnEnableU3Eb__15_0_m6793DE39FBF2171A3AF9F73DC1442DCC302C8694,
	ObiRopeMeshRenderer_set_SourceMesh_mABB00F322C30D9903B768CD2ACE3007D0A924409,
	ObiRopeMeshRenderer_get_SourceMesh_m0068E1A186BF741C6EE74048DADA660C04015EBC,
	ObiRopeMeshRenderer_set_SweepAxis_mED22BD018A3915542FD681B0B1F4D755F06155BB,
	ObiRopeMeshRenderer_get_SweepAxis_m2BC90F696A1433C0248339A45E41A795CE2C8B4D,
	ObiRopeMeshRenderer_set_Instances_mC7C1482F709BF5F7B1FBC075144B068FA1EA12F4,
	ObiRopeMeshRenderer_get_Instances_m9CE541D38231EEBEA1D50EFBEED83E2F92C69D8B,
	ObiRopeMeshRenderer_set_InstanceSpacing_mF9B069DAE0D1F71E50E99E33AF0F950402E4B4AA,
	ObiRopeMeshRenderer_get_InstanceSpacing_m9C6AEF6CCC9A3EA7D94552A48A781D984E124856,
	ObiRopeMeshRenderer_OnEnable_mCA4ACCE43A10C160B4FA5D046614C98542412387,
	ObiRopeMeshRenderer_OnDisable_mB94D19D22E4DB0A0B06602571195487714957803,
	ObiRopeMeshRenderer_PreprocessInputMesh_m7EF1EE49A0487D53A0472FF4BCB6BCA84230E911,
	ObiRopeMeshRenderer_UpdateRenderer_mE9FFC55F211FFD9BA63E319D8F77F8C80D58D8AD,
	ObiRopeMeshRenderer_CommitMeshData_m5934A704FB06A7B37D8EC29D4EDDC25254AC9756,
	ObiRopeMeshRenderer__ctor_mE5BC1801089B3FD4D812020C3BDCA8B249675C20,
	ObiRopeMeshRenderer__cctor_m95B600F74A946C4B0C9EF50B3479D496AC31D0CB,
	ObiRopeAttach_LateUpdate_m8A0247A5D2C0AEFD4D608EC687722C14A81B8674,
	ObiRopeAttach__ctor_mA5527E87A206EC0601B23C3FDD325F0EF19348F3,
	ObiRopePrefabPlugger_OnEnable_mFAC396C907027689582E1F8D33BE045758F0570B,
	ObiRopePrefabPlugger_OnDisable_mC973B39792A52C083ECA9B35EA24415BEF56C644,
	ObiRopePrefabPlugger_GetOrCreatePrefabInstance_mAB7F8BAA8692E5C334C014EDFF95F63C3DE89E0B,
	ObiRopePrefabPlugger_ClearPrefabInstances_m019D264CAD4FD43AFC7801E6A7E284F26244A1B4,
	ObiRopePrefabPlugger_UpdatePlugs_m785485C0BF137163959433C48BB5D8153D815E45,
	ObiRopePrefabPlugger__ctor_mBBC26B2FFB9C07B5C26E5429AE79E05E7F65906F,
	ObiRopeReel_Awake_mA6FA3792ED36E3917682DE7F89E3C518A58BF95B,
	ObiRopeReel_OnValidate_mC46F4295A6C54AD97935E43EF7DC01164FDCD580,
	ObiRopeReel_Update_m2F0E0E5D6EC299100F832CFA1E0E2AD490385ECA,
	ObiRopeReel__ctor_m880E0A2CD5DD907FC02806BFE19D0C899562B1FF,
};
extern void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3_AdjustorThunk (void);
extern void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566_AdjustorThunk (void);
extern void StructuralConstraint_get_restLength_mA86CC2512F19A0314F76C459C7027A50DEB66F57_AdjustorThunk (void);
extern void StructuralConstraint_set_restLength_mFDDB0536829F1845FAB3F39B80ED0520431D2BDF_AdjustorThunk (void);
extern void StructuralConstraint__ctor_m9F7FF404C8685160AC46543FCE7681E91FF6B322_AdjustorThunk (void);
extern void DistanceFieldHeader__ctor_mD9023657AE562DC6E93CAEBAEA87D8BBFBE3CAC0_AdjustorThunk (void);
extern void Edge__ctor_m58FEEBE96D412AFED6635E824686137251119088_AdjustorThunk (void);
extern void Edge_GetBounds_m3627A8DC0128EA13268432FD42FDFFA5E9E8FC16_AdjustorThunk (void);
extern void EdgeMeshHeader__ctor_mFBC1ECFE2844D2E52FDDEF03C23174C84E12C124_AdjustorThunk (void);
extern void HeightFieldHeader__ctor_m68533ABFD4D9CB9C9AE92C04D96F69D03704B421_AdjustorThunk (void);
extern void Triangle__ctor_mF3FB7D0163513605C1284E55D476938CFA8D8B3E_AdjustorThunk (void);
extern void Triangle_GetBounds_m96DD7752DB622FBB5D130CC0203D16EF9A16183D_AdjustorThunk (void);
extern void TriangleMeshHeader__ctor_m05352EA77E9896540763C1FC4DA9A86E8FD110E4_AdjustorThunk (void);
extern void DFNode__ctor_m5AA5A22FD71D7439C9136C2FBC6F151E25C8A609_AdjustorThunk (void);
extern void DFNode_Sample_mD228ACC5980D03D69BA1510383BA88AD4253D1D3_AdjustorThunk (void);
extern void DFNode_GetNormalizedPos_m48205296253B504FB1F9D3177EADD8E62A70F31B_AdjustorThunk (void);
extern void DFNode_GetOctant_m8E57E952AB3E07E8596E051854C4B9EF62575192_AdjustorThunk (void);
extern void Aabb_get_center_m6F29EA934C6E7C6A2557550214746BE327737113_AdjustorThunk (void);
extern void Aabb_get_size_m316B3CACF5BB496D6CF8532AF06AF826C78486D7_AdjustorThunk (void);
extern void Aabb__ctor_m8B1C8639B79DD7C951EF63FD820BB63B8A6B5DB4_AdjustorThunk (void);
extern void Aabb__ctor_m293B0520F59E98F3B14FEAFE282685CA663D2E0B_AdjustorThunk (void);
extern void Aabb_Encapsulate_mDCE164B716BFA4A7F8147BC997DDDAFF5A0A9F08_AdjustorThunk (void);
extern void Aabb_Encapsulate_m4F8536D0A22C6A596F1393D02F97BA7C45B9F9A8_AdjustorThunk (void);
extern void Aabb_FromBounds_mB313907D1B69DA3917270DE5C2B39AD5ECCCF664_AdjustorThunk (void);
extern void AffineTransform__ctor_m1113EA596451380FE97BF986F98D180B67B50914_AdjustorThunk (void);
extern void AffineTransform_FromTransform_mF590DA4EFBCE17F0245A9C33FE65B7405154A7EB_AdjustorThunk (void);
extern void BIHNode__ctor_mBE805A0C558FDCA2FEDE25366ED721EA3776834A_AdjustorThunk (void);
extern void CellSpan__ctor_m93CAC71E3E1DC790B927C8C76F72A3784072746B_AdjustorThunk (void);
extern void ColliderRigidbody_FromRigidbody_m4921C838DA8C3C147A91168C9232CB97780260BB_AdjustorThunk (void);
extern void ColliderRigidbody_FromRigidbody_m2311F1A25138EA07373D405B7EF2F1EB3859138E_AdjustorThunk (void);
extern void CollisionMaterial_FromObiCollisionMaterial_mBF4E29EA56479F76E396B2B5EA9BDEFED6663530_AdjustorThunk (void);
extern void ParticlePair__ctor_mDDAD78C599D557D2A63715207CBC71A5A93581CA_AdjustorThunk (void);
extern void ParticlePair_get_Item_m96F244D843B43EE4A0E48128BDAE3837B0574AF9_AdjustorThunk (void);
extern void ParticlePair_set_Item_m39EFBD38D2AB94AA009AAF1C18B7EA40077EEDB1_AdjustorThunk (void);
extern void QueryShape__ctor_m10B944E68ED9FE30CB3D7EE090068A33F9EDB453_AdjustorThunk (void);
extern void SimplexCounts_get_simplexCount_m99631648B641C2ECF96D0C8E5DB265D40F522A86_AdjustorThunk (void);
extern void SimplexCounts__ctor_m68F3A4CF7A694C3300E7BA4227020A89A724F066_AdjustorThunk (void);
extern void SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95_AdjustorThunk (void);
extern void VInt4__ctor_m6FDFB28D4337C36A4A49681BA2E0B85D0B36AFCB_AdjustorThunk (void);
extern void VInt4__ctor_mDF85C8595BC6DCEAAF3A5A4A04704DEFDB238C6C_AdjustorThunk (void);
extern void TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F_AdjustorThunk (void);
extern void TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF_AdjustorThunk (void);
extern void TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681_AdjustorThunk (void);
extern void TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8_AdjustorThunk (void);
extern void ObiPathFrame__ctor_m025EA159EAFA9E15FAD8CF083C002438D6A43641_AdjustorThunk (void);
extern void ObiPathFrame_Reset_m6589829D261335ABE1D4733AD2BA58A2E8D5FF15_AdjustorThunk (void);
extern void ObiPathFrame_SetTwist_m15557FA7BEAF865EC68821EA1CCC3D349855F667_AdjustorThunk (void);
extern void ObiPathFrame_SetTwistAndTangent_m820B046082B1FA969D75F517E60F14B6C888B9A2_AdjustorThunk (void);
extern void ObiPathFrame_Transport_m30DB5DB5B8D169DF5B54D56BA7B39A776604F769_AdjustorThunk (void);
extern void ObiPathFrame_Transport_m0B1B3CA7069547BE12B49F7FDB4E67EEADC239D4_AdjustorThunk (void);
extern void ObiPathFrame_Transport_mE3DC94CBBB61AE7CFC04178FADD5862011E485B0_AdjustorThunk (void);
extern void ObiPathFrame_ToMatrix_m3941E65FC0FD3C60411092AAA8CDCF24EEE1E5C5_AdjustorThunk (void);
extern void ObiPathFrame_DebugDraw_m5397213F80C59B56BBA30A19B770A3E88BF6712C_AdjustorThunk (void);
extern void ObiWingedPoint_get_inTangentEndpoint_mB04887363665D698ADFB0964437641EBEE625AAB_AdjustorThunk (void);
extern void ObiWingedPoint_get_outTangentEndpoint_m13FD62B607A3335392B17CD3230F7FFDC8D14A0D_AdjustorThunk (void);
extern void ObiWingedPoint__ctor_mAD5A99BEA0E2B2C4D6563AB336276D0EFC057EC2_AdjustorThunk (void);
extern void ObiWingedPoint_SetInTangentEndpoint_mC89AD4F0A7639ED4F2213166497F9B9B07399039_AdjustorThunk (void);
extern void ObiWingedPoint_SetOutTangentEndpoint_m2F82C9A47C3FF51341B24470191045CE79BB9B9B_AdjustorThunk (void);
extern void ObiWingedPoint_Transform_m59146866606ECDBA07E40D3525E70F59AC5A6BC4_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[59] = 
{
	{ 0x06000083, SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3_AdjustorThunk },
	{ 0x06000084, ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566_AdjustorThunk },
	{ 0x060002DE, StructuralConstraint_get_restLength_mA86CC2512F19A0314F76C459C7027A50DEB66F57_AdjustorThunk },
	{ 0x060002DF, StructuralConstraint_set_restLength_mFDDB0536829F1845FAB3F39B80ED0520431D2BDF_AdjustorThunk },
	{ 0x060002E0, StructuralConstraint__ctor_m9F7FF404C8685160AC46543FCE7681E91FF6B322_AdjustorThunk },
	{ 0x0600039D, DistanceFieldHeader__ctor_mD9023657AE562DC6E93CAEBAEA87D8BBFBE3CAC0_AdjustorThunk },
	{ 0x060003A2, Edge__ctor_m58FEEBE96D412AFED6635E824686137251119088_AdjustorThunk },
	{ 0x060003A3, Edge_GetBounds_m3627A8DC0128EA13268432FD42FDFFA5E9E8FC16_AdjustorThunk },
	{ 0x060003A5, EdgeMeshHeader__ctor_mFBC1ECFE2844D2E52FDDEF03C23174C84E12C124_AdjustorThunk },
	{ 0x060003AE, HeightFieldHeader__ctor_m68533ABFD4D9CB9C9AE92C04D96F69D03704B421_AdjustorThunk },
	{ 0x060003C2, Triangle__ctor_mF3FB7D0163513605C1284E55D476938CFA8D8B3E_AdjustorThunk },
	{ 0x060003C3, Triangle_GetBounds_m96DD7752DB622FBB5D130CC0203D16EF9A16183D_AdjustorThunk },
	{ 0x060003C5, TriangleMeshHeader__ctor_m05352EA77E9896540763C1FC4DA9A86E8FD110E4_AdjustorThunk },
	{ 0x060003DA, DFNode__ctor_m5AA5A22FD71D7439C9136C2FBC6F151E25C8A609_AdjustorThunk },
	{ 0x060003DB, DFNode_Sample_mD228ACC5980D03D69BA1510383BA88AD4253D1D3_AdjustorThunk },
	{ 0x060003DC, DFNode_GetNormalizedPos_m48205296253B504FB1F9D3177EADD8E62A70F31B_AdjustorThunk },
	{ 0x060003DD, DFNode_GetOctant_m8E57E952AB3E07E8596E051854C4B9EF62575192_AdjustorThunk },
	{ 0x060003DE, Aabb_get_center_m6F29EA934C6E7C6A2557550214746BE327737113_AdjustorThunk },
	{ 0x060003DF, Aabb_get_size_m316B3CACF5BB496D6CF8532AF06AF826C78486D7_AdjustorThunk },
	{ 0x060003E0, Aabb__ctor_m8B1C8639B79DD7C951EF63FD820BB63B8A6B5DB4_AdjustorThunk },
	{ 0x060003E1, Aabb__ctor_m293B0520F59E98F3B14FEAFE282685CA663D2E0B_AdjustorThunk },
	{ 0x060003E2, Aabb_Encapsulate_mDCE164B716BFA4A7F8147BC997DDDAFF5A0A9F08_AdjustorThunk },
	{ 0x060003E3, Aabb_Encapsulate_m4F8536D0A22C6A596F1393D02F97BA7C45B9F9A8_AdjustorThunk },
	{ 0x060003E4, Aabb_FromBounds_mB313907D1B69DA3917270DE5C2B39AD5ECCCF664_AdjustorThunk },
	{ 0x060003E5, AffineTransform__ctor_m1113EA596451380FE97BF986F98D180B67B50914_AdjustorThunk },
	{ 0x060003E6, AffineTransform_FromTransform_mF590DA4EFBCE17F0245A9C33FE65B7405154A7EB_AdjustorThunk },
	{ 0x060003EE, BIHNode__ctor_mBE805A0C558FDCA2FEDE25366ED721EA3776834A_AdjustorThunk },
	{ 0x060003F0, CellSpan__ctor_m93CAC71E3E1DC790B927C8C76F72A3784072746B_AdjustorThunk },
	{ 0x060003F1, ColliderRigidbody_FromRigidbody_m4921C838DA8C3C147A91168C9232CB97780260BB_AdjustorThunk },
	{ 0x060003F2, ColliderRigidbody_FromRigidbody_m2311F1A25138EA07373D405B7EF2F1EB3859138E_AdjustorThunk },
	{ 0x060003F3, CollisionMaterial_FromObiCollisionMaterial_mBF4E29EA56479F76E396B2B5EA9BDEFED6663530_AdjustorThunk },
	{ 0x06000471, ParticlePair__ctor_mDDAD78C599D557D2A63715207CBC71A5A93581CA_AdjustorThunk },
	{ 0x06000472, ParticlePair_get_Item_m96F244D843B43EE4A0E48128BDAE3837B0574AF9_AdjustorThunk },
	{ 0x06000473, ParticlePair_set_Item_m39EFBD38D2AB94AA009AAF1C18B7EA40077EEDB1_AdjustorThunk },
	{ 0x06000474, QueryShape__ctor_m10B944E68ED9FE30CB3D7EE090068A33F9EDB453_AdjustorThunk },
	{ 0x06000475, SimplexCounts_get_simplexCount_m99631648B641C2ECF96D0C8E5DB265D40F522A86_AdjustorThunk },
	{ 0x06000476, SimplexCounts__ctor_m68F3A4CF7A694C3300E7BA4227020A89A724F066_AdjustorThunk },
	{ 0x06000477, SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95_AdjustorThunk },
	{ 0x06000478, VInt4__ctor_m6FDFB28D4337C36A4A49681BA2E0B85D0B36AFCB_AdjustorThunk },
	{ 0x06000479, VInt4__ctor_mDF85C8595BC6DCEAAF3A5A4A04704DEFDB238C6C_AdjustorThunk },
	{ 0x060004BB, TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F_AdjustorThunk },
	{ 0x060004BC, TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF_AdjustorThunk },
	{ 0x060004BD, TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681_AdjustorThunk },
	{ 0x060004BE, TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8_AdjustorThunk },
	{ 0x060007C2, ObiPathFrame__ctor_m025EA159EAFA9E15FAD8CF083C002438D6A43641_AdjustorThunk },
	{ 0x060007C3, ObiPathFrame_Reset_m6589829D261335ABE1D4733AD2BA58A2E8D5FF15_AdjustorThunk },
	{ 0x060007C7, ObiPathFrame_SetTwist_m15557FA7BEAF865EC68821EA1CCC3D349855F667_AdjustorThunk },
	{ 0x060007C8, ObiPathFrame_SetTwistAndTangent_m820B046082B1FA969D75F517E60F14B6C888B9A2_AdjustorThunk },
	{ 0x060007C9, ObiPathFrame_Transport_m30DB5DB5B8D169DF5B54D56BA7B39A776604F769_AdjustorThunk },
	{ 0x060007CA, ObiPathFrame_Transport_m0B1B3CA7069547BE12B49F7FDB4E67EEADC239D4_AdjustorThunk },
	{ 0x060007CB, ObiPathFrame_Transport_mE3DC94CBBB61AE7CFC04178FADD5862011E485B0_AdjustorThunk },
	{ 0x060007CC, ObiPathFrame_ToMatrix_m3941E65FC0FD3C60411092AAA8CDCF24EEE1E5C5_AdjustorThunk },
	{ 0x060007CD, ObiPathFrame_DebugDraw_m5397213F80C59B56BBA30A19B770A3E88BF6712C_AdjustorThunk },
	{ 0x060007DF, ObiWingedPoint_get_inTangentEndpoint_mB04887363665D698ADFB0964437641EBEE625AAB_AdjustorThunk },
	{ 0x060007E0, ObiWingedPoint_get_outTangentEndpoint_m13FD62B607A3335392B17CD3230F7FFDC8D14A0D_AdjustorThunk },
	{ 0x060007E1, ObiWingedPoint__ctor_mAD5A99BEA0E2B2C4D6563AB336276D0EFC057EC2_AdjustorThunk },
	{ 0x060007E2, ObiWingedPoint_SetInTangentEndpoint_mC89AD4F0A7639ED4F2213166497F9B9B07399039_AdjustorThunk },
	{ 0x060007E3, ObiWingedPoint_SetOutTangentEndpoint_m2F82C9A47C3FF51341B24470191045CE79BB9B9B_AdjustorThunk },
	{ 0x060007E4, ObiWingedPoint_Transform_m59146866606ECDBA07E40D3525E70F59AC5A6BC4_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2074] = 
{
	4017,
	4055,
	4055,
	4055,
	1976,
	4055,
	1551,
	4055,
	4055,
	4055,
	4055,
	1234,
	1553,
	2417,
	5913,
	6099,
	6112,
	4893,
	6102,
	6102,
	4894,
	4894,
	5753,
	5753,
	5958,
	6102,
	5751,
	4889,
	4581,
	4378,
	6102,
	6102,
	5753,
	5753,
	5314,
	5934,
	5754,
	4639,
	5326,
	5749,
	5749,
	5021,
	5513,
	4732,
	4584,
	6102,
	5473,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5753,
	5328,
	5753,
	5753,
	5753,
	5753,
	5753,
	4585,
	5934,
	4896,
	5017,
	5315,
	5315,
	5753,
	5753,
	5958,
	6102,
	5511,
	5753,
	5644,
	4640,
	5751,
	5934,
	4377,
	4281,
	4229,
	4893,
	4229,
	4164,
	5753,
	4229,
	4281,
	4377,
	4229,
	4583,
	4377,
	5334,
	5334,
	4308,
	5464,
	5932,
	5932,
	6066,
	6207,
	6102,
	6178,
	6102,
	5753,
	6176,
	6207,
	6109,
	5776,
	6207,
	6176,
	5767,
	1869,
	1274,
	3965,
	3965,
	4017,
	2403,
	3096,
	2608,
	795,
	3039,
	2199,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3983,
	4017,
	3983,
	3287,
	4017,
	3320,
	3965,
	3965,
	4017,
	4017,
	4017,
	3975,
	3975,
	3983,
	3983,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	1521,
	1867,
	2026,
	3271,
	2869,
	2869,
	2869,
	3320,
	3320,
	4055,
	3271,
	2581,
	4055,
	2403,
	3096,
	2608,
	795,
	3039,
	2199,
	3271,
	3271,
	3324,
	3038,
	2023,
	2023,
	1971,
	4055,
	4055,
	3287,
	4055,
	4055,
	3287,
	3287,
	4055,
	3324,
	3324,
	3324,
	3324,
	4055,
	3320,
	4055,
	3983,
	3287,
	1973,
	3287,
	1022,
	3287,
	1973,
	1981,
	653,
	3287,
	1973,
	1976,
	645,
	3287,
	3965,
	3324,
	855,
	3287,
	3287,
	856,
	856,
	1976,
	1976,
	1242,
	282,
	282,
	437,
	3965,
	3983,
	3320,
	4017,
	4055,
	3271,
	3965,
	3965,
	3983,
	3965,
	437,
	154,
	10,
	4055,
	154,
	855,
	154,
	437,
	154,
	1449,
	3287,
	4055,
	4055,
	1315,
	914,
	1290,
	3287,
	1971,
	437,
	3287,
	2581,
	3287,
	2403,
	1971,
	1971,
	1758,
	2594,
	1047,
	866,
	3965,
	1230,
	1356,
	1980,
	3421,
	1647,
	4055,
	3965,
	3287,
	1245,
	1449,
	3287,
	4055,
	4055,
	1315,
	914,
	1290,
	3965,
	1230,
	1356,
	1980,
	3287,
	3287,
	1971,
	4055,
	1647,
	3421,
	2403,
	1971,
	1971,
	1758,
	2581,
	3287,
	2594,
	1047,
	866,
	437,
	3965,
	3287,
	1245,
	4055,
	3287,
	1242,
	3287,
	3983,
	3287,
	3287,
	282,
	3287,
	3983,
	3287,
	3287,
	282,
	3287,
	3983,
	3287,
	3287,
	437,
	3287,
	3983,
	3287,
	3287,
	437,
	3287,
	3983,
	3287,
	3967,
	3965,
	3983,
	3320,
	4017,
	1971,
	4055,
	3271,
	3965,
	3983,
	3287,
	3983,
	3965,
	1971,
	3983,
	3287,
	3965,
	3287,
	154,
	3287,
	3983,
	3287,
	3287,
	10,
	4055,
	3287,
	3983,
	3287,
	3287,
	154,
	3287,
	3983,
	3287,
	3287,
	855,
	3287,
	3983,
	3287,
	3287,
	154,
	3287,
	3983,
	3287,
	3287,
	437,
	3287,
	3983,
	3287,
	3287,
	154,
	3287,
	3983,
	3287,
	1449,
	3287,
	4055,
	4055,
	3965,
	4055,
	4055,
	4055,
	4055,
	3324,
	855,
	3287,
	3287,
	856,
	856,
	1976,
	1976,
	4055,
	3273,
	4055,
	3967,
	3273,
	4055,
	1315,
	914,
	1290,
	3965,
	1230,
	1356,
	1980,
	3287,
	3287,
	1971,
	4055,
	1647,
	3421,
	2403,
	1971,
	1971,
	1758,
	2581,
	3287,
	2594,
	1047,
	866,
	437,
	3965,
	3287,
	1245,
	3039,
	1840,
	2603,
	3965,
	3983,
	3287,
	819,
	1816,
	4055,
	1802,
	1976,
	3287,
	3287,
	3965,
	3983,
	3287,
	1976,
	2030,
	4055,
	1816,
	1802,
	3287,
	3287,
	3965,
	3983,
	3287,
	2021,
	4055,
	1816,
	1802,
	1976,
	3287,
	3287,
	3965,
	3983,
	3287,
	878,
	4055,
	1816,
	1802,
	1976,
	3287,
	3287,
	3965,
	3965,
	3271,
	3965,
	3271,
	3965,
	3983,
	3287,
	3287,
	1976,
	2869,
	2869,
	4055,
	4055,
	1816,
	1802,
	3965,
	3965,
	3271,
	3965,
	3271,
	3965,
	3983,
	1976,
	1802,
	1816,
	3287,
	3287,
	1971,
	1802,
	4055,
	4055,
	2403,
	2869,
	2869,
	2869,
	4055,
	3271,
	1802,
	4055,
	3965,
	3983,
	3271,
	2022,
	4055,
	3039,
	1840,
	2603,
	1816,
	1971,
	1802,
	1976,
	3287,
	3287,
	3965,
	3983,
	3287,
	136,
	4055,
	1816,
	1802,
	1976,
	3287,
	3287,
	3965,
	3983,
	3287,
	1979,
	4055,
	1816,
	1802,
	1802,
	1976,
	3287,
	3287,
	4055,
	3965,
	3983,
	3287,
	137,
	4055,
	1816,
	1802,
	1976,
	3287,
	3287,
	3965,
	3983,
	3287,
	910,
	4055,
	3039,
	1840,
	2603,
	1816,
	1802,
	1976,
	3287,
	3287,
	3965,
	3983,
	3287,
	1298,
	4055,
	1816,
	1802,
	1976,
	3287,
	3287,
	2011,
	3965,
	3983,
	3287,
	1981,
	4055,
	1816,
	1802,
	1976,
	3287,
	3287,
	2011,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	2588,
	4055,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	2588,
	4055,
	4017,
	3320,
	1637,
	1629,
	2588,
	4055,
	4017,
	3320,
	4023,
	3324,
	2588,
	4055,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	2588,
	4055,
	2588,
	4055,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	2588,
	4055,
	4017,
	3320,
	1637,
	1619,
	2588,
	4055,
	4017,
	3320,
	1637,
	2588,
	4055,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	2588,
	4055,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	2588,
	4055,
	3881,
	2581,
	3965,
	4055,
	2887,
	4017,
	3965,
	3965,
	4055,
	1976,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4023,
	3324,
	1234,
	5562,
	3287,
	3287,
	3965,
	3965,
	4017,
	4017,
	2869,
	3271,
	2869,
	2869,
	4017,
	4055,
	3915,
	3983,
	2581,
	2403,
	3096,
	2608,
	795,
	3039,
	2199,
	4055,
	3983,
	4055,
	1019,
	1451,
	1529,
	1100,
	3320,
	1099,
	727,
	1976,
	1802,
	1650,
	4055,
	3287,
	4055,
	3983,
	4055,
	1973,
	3287,
	1022,
	3287,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	3983,
	3287,
	3965,
	2869,
	4055,
	1976,
	4017,
	1976,
	4017,
	1976,
	4017,
	1976,
	4055,
	4017,
	4055,
	1976,
	4017,
	1976,
	4017,
	1976,
	4017,
	1245,
	4055,
	4017,
	4055,
	1976,
	4055,
	4017,
	4055,
	4055,
	4017,
	4055,
	1976,
	4017,
	1976,
	4055,
	4017,
	4055,
	3287,
	3983,
	3287,
	3983,
	4055,
	2576,
	4055,
	4055,
	3287,
	3983,
	4055,
	2576,
	4055,
	4055,
	3287,
	3983,
	3271,
	3965,
	3324,
	4023,
	3983,
	3983,
	3967,
	3983,
	4055,
	2576,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	-1,
	-1,
	-1,
	-1,
	-1,
	3271,
	3271,
	3271,
	6183,
	4055,
	4055,
	4055,
	3287,
	3287,
	3983,
	3983,
	3983,
	2588,
	3287,
	2588,
	3287,
	2588,
	3287,
	2588,
	3287,
	3287,
	3287,
	3287,
	4055,
	1981,
	3324,
	3287,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4017,
	3915,
	4023,
	3287,
	3983,
	4055,
	3983,
	2581,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	1971,
	1802,
	4055,
	2588,
	3287,
	4055,
	812,
	3908,
	1971,
	245,
	4055,
	2588,
	3287,
	4055,
	6207,
	4055,
	2234,
	1971,
	1802,
	4055,
	2588,
	3287,
	4055,
	4055,
	3324,
	3324,
	2029,
	4055,
	4055,
	3324,
	3324,
	2029,
	4055,
	4055,
	4055,
	3324,
	2029,
	4055,
	252,
	3908,
	1971,
	245,
	4055,
	2588,
	3287,
	4055,
	6207,
	4055,
	3074,
	4347,
	5686,
	4055,
	6207,
	6207,
	4055,
	3074,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3358,
	3045,
	3099,
	2468,
	4053,
	4053,
	2032,
	3358,
	3358,
	3208,
	1167,
	1313,
	1979,
	5093,
	4314,
	4538,
	4539,
	4366,
	4055,
	5688,
	1802,
	3908,
	2017,
	1979,
	1979,
	3287,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	1195,
	4055,
	1802,
	4055,
	1802,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1802,
	4055,
	1802,
	1192,
	1802,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	4055,
	1802,
	3096,
	1867,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1802,
	2403,
	1802,
	254,
	3965,
	1190,
	1355,
	800,
	3271,
	4052,
	3965,
	1981,
	939,
	800,
	3039,
	939,
	3095,
	921,
	2581,
	3100,
	2797,
	1095,
	391,
	1447,
	4055,
	3983,
	4797,
	4507,
	6207,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3287,
	1141,
	1153,
	3983,
	1200,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	1322,
	3548,
	2042,
	3033,
	4023,
	1311,
	3033,
	2539,
	6207,
	4055,
	3046,
	4055,
	3033,
	3046,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	1975,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	6207,
	3983,
	3983,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	6207,
	3983,
	3287,
	4055,
	1245,
	4055,
	6207,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3983,
	4017,
	3983,
	3271,
	3965,
	4022,
	3915,
	4017,
	4023,
	3965,
	3965,
	3965,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	3271,
	3287,
	3287,
	2887,
	2887,
	4055,
	3513,
	2581,
	4055,
	4055,
	4055,
	4055,
	4055,
	3324,
	4055,
	2594,
	1047,
	3324,
	2011,
	1245,
	1454,
	387,
	640,
	4055,
	6207,
	4055,
	4055,
	1971,
	1973,
	3287,
	1022,
	3287,
	1973,
	1981,
	653,
	3287,
	1973,
	1976,
	645,
	3287,
	6207,
	4055,
	2887,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3983,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	3324,
	1288,
	3324,
	2011,
	4055,
	6207,
	4055,
	4055,
	4023,
	3324,
	3287,
	6207,
	2011,
	4055,
	4055,
	2011,
	3983,
	3287,
	3287,
	3983,
	3287,
	4017,
	3320,
	1979,
	3983,
	4017,
	4017,
	4055,
	6004,
	2588,
	4055,
	4055,
	1981,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	5761,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	1981,
	3043,
	3287,
	4055,
	3287,
	4055,
	4055,
	4999,
	4055,
	4055,
	4055,
	1375,
	1375,
	1971,
	1976,
	4055,
	3287,
	1411,
	4055,
	3983,
	3983,
	3287,
	3983,
	3287,
	4017,
	3965,
	3271,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	4055,
	4055,
	4055,
	4055,
	4055,
	1976,
	1981,
	1981,
	4055,
	3271,
	3271,
	4055,
	3324,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1867,
	4055,
	4055,
	4055,
	4055,
	4055,
	6207,
	6207,
	5776,
	6207,
	4055,
	4055,
	4055,
	3287,
	3983,
	3287,
	3983,
	3965,
	3983,
	3287,
	3287,
	4055,
	4055,
	1356,
	3271,
	4055,
	1976,
	1976,
	3287,
	3287,
	4055,
	4055,
	1802,
	4932,
	5380,
	-1,
	-1,
	-1,
	-1,
	-1,
	6042,
	5414,
	5932,
	5381,
	4540,
	5688,
	5524,
	6065,
	5526,
	4541,
	4535,
	4840,
	6068,
	6068,
	5940,
	4574,
	5214,
	6069,
	5581,
	5214,
	6001,
	4574,
	4574,
	4839,
	6069,
	5562,
	5464,
	5932,
	5932,
	5464,
	5932,
	5932,
	5341,
	6085,
	5716,
	6081,
	6082,
	4295,
	6207,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	3983,
	4615,
	5381,
	4201,
	5381,
	4202,
	4055,
	4055,
	4055,
	1976,
	4055,
	3271,
	3965,
	4017,
	3320,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	4017,
	3320,
	3983,
	3287,
	3983,
	3287,
	4017,
	3320,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	4017,
	3320,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	3983,
	3983,
	3287,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	1637,
	1619,
	1637,
	1629,
	1637,
	3324,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	2011,
	3043,
	4055,
	4017,
	3320,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	4017,
	3320,
	4023,
	3324,
	4023,
	3983,
	3983,
	3287,
	4055,
	3287,
	4055,
	1637,
	1629,
	1637,
	4055,
	4055,
	4017,
	3320,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	4023,
	3324,
	4023,
	3983,
	3983,
	3287,
	3287,
	3287,
	4055,
	3287,
	3287,
	4055,
	3324,
	3324,
	2403,
	2887,
	4055,
	4055,
	4055,
	1973,
	1976,
	645,
	3287,
	1971,
	6207,
	4055,
	1376,
	3287,
	3287,
	4023,
	3983,
	4023,
	4055,
	4055,
	4055,
	4055,
	4055,
	1460,
	4055,
	3324,
	4023,
	3324,
	4023,
	3983,
	3965,
	4055,
	3287,
	4055,
	4055,
	4055,
	2403,
	3271,
	3324,
	4055,
	2588,
	3983,
	4055,
	2588,
	2588,
	2588,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	2588,
	3983,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	3983,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4023,
	4023,
	4055,
	4055,
	3271,
	3271,
	3271,
	3271,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3965,
	4055,
	3271,
	5037,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3965,
	4017,
	4055,
	3271,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4055,
	3096,
	3096,
	1638,
	1638,
	1638,
	4055,
	4055,
	389,
	389,
	389,
	4055,
	390,
	390,
	390,
	4055,
	302,
	302,
	302,
	4055,
	306,
	306,
	306,
	4055,
	-1,
	-1,
	-1,
	4055,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	3983,
	4023,
	3965,
	3965,
	4017,
	3320,
	3965,
	1391,
	2441,
	3043,
	1143,
	14,
	1816,
	2581,
	28,
	18,
	2441,
	4055,
	3271,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	3983,
	295,
	4055,
	5533,
	5534,
	4296,
	3324,
	2012,
	1958,
	1309,
	913,
	2555,
	3324,
	3287,
	3287,
	4023,
	4023,
	4055,
	4055,
	3287,
	3271,
	3041,
	3287,
	827,
	1971,
	2565,
	1121,
	1242,
	4055,
	6207,
	4051,
	4051,
	1310,
	3356,
	3356,
	1304,
	4055,
	4017,
	3320,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	6207,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	6207,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	6207,
	2005,
	3287,
	3983,
	3271,
	3965,
	3271,
	3965,
	3324,
	4023,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	6207,
	4055,
	4055,
	4055,
	4055,
	2581,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
};
static const Il2CppTokenRangePair s_rgctxIndices[15] = 
{
	{ 0x02000073, { 0, 17 } },
	{ 0x020000C6, { 17, 24 } },
	{ 0x020000C7, { 45, 3 } },
	{ 0x020000D2, { 48, 8 } },
	{ 0x020000D3, { 56, 1 } },
	{ 0x020000DE, { 57, 13 } },
	{ 0x020000DF, { 70, 6 } },
	{ 0x0200013D, { 80, 9 } },
	{ 0x0200013E, { 89, 8 } },
	{ 0x06000427, { 41, 1 } },
	{ 0x06000428, { 42, 1 } },
	{ 0x06000429, { 43, 2 } },
	{ 0x06000613, { 76, 2 } },
	{ 0x06000614, { 78, 1 } },
	{ 0x06000615, { 79, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[97] = 
{
	{ (Il2CppRGCTXDataType)2, 3492 },
	{ (Il2CppRGCTXDataType)3, 18479 },
	{ (Il2CppRGCTXDataType)3, 18478 },
	{ (Il2CppRGCTXDataType)3, 18477 },
	{ (Il2CppRGCTXDataType)3, 13670 },
	{ (Il2CppRGCTXDataType)2, 468 },
	{ (Il2CppRGCTXDataType)3, 13669 },
	{ (Il2CppRGCTXDataType)3, 13667 },
	{ (Il2CppRGCTXDataType)3, 7038 },
	{ (Il2CppRGCTXDataType)3, 7037 },
	{ (Il2CppRGCTXDataType)2, 1523 },
	{ (Il2CppRGCTXDataType)3, 18480 },
	{ (Il2CppRGCTXDataType)3, 13666 },
	{ (Il2CppRGCTXDataType)3, 13665 },
	{ (Il2CppRGCTXDataType)3, 13668 },
	{ (Il2CppRGCTXDataType)2, 2850 },
	{ (Il2CppRGCTXDataType)3, 13664 },
	{ (Il2CppRGCTXDataType)3, 18698 },
	{ (Il2CppRGCTXDataType)3, 18696 },
	{ (Il2CppRGCTXDataType)3, 30098 },
	{ (Il2CppRGCTXDataType)3, 30203 },
	{ (Il2CppRGCTXDataType)3, 18692 },
	{ (Il2CppRGCTXDataType)3, 28954 },
	{ (Il2CppRGCTXDataType)3, 18697 },
	{ (Il2CppRGCTXDataType)3, 18703 },
	{ (Il2CppRGCTXDataType)2, 4723 },
	{ (Il2CppRGCTXDataType)3, 18700 },
	{ (Il2CppRGCTXDataType)3, 30134 },
	{ (Il2CppRGCTXDataType)3, 18702 },
	{ (Il2CppRGCTXDataType)3, 18695 },
	{ (Il2CppRGCTXDataType)3, 30029 },
	{ (Il2CppRGCTXDataType)3, 18704 },
	{ (Il2CppRGCTXDataType)2, 1932 },
	{ (Il2CppRGCTXDataType)2, 2126 },
	{ (Il2CppRGCTXDataType)2, 2277 },
	{ (Il2CppRGCTXDataType)3, 18694 },
	{ (Il2CppRGCTXDataType)3, 18701 },
	{ (Il2CppRGCTXDataType)2, 472 },
	{ (Il2CppRGCTXDataType)2, 870 },
	{ (Il2CppRGCTXDataType)3, 217 },
	{ (Il2CppRGCTXDataType)3, 18699 },
	{ (Il2CppRGCTXDataType)3, 18693 },
	{ (Il2CppRGCTXDataType)3, 29572 },
	{ (Il2CppRGCTXDataType)3, 18691 },
	{ (Il2CppRGCTXDataType)3, 28953 },
	{ (Il2CppRGCTXDataType)3, 18705 },
	{ (Il2CppRGCTXDataType)3, 18706 },
	{ (Il2CppRGCTXDataType)2, 608 },
	{ (Il2CppRGCTXDataType)2, 867 },
	{ (Il2CppRGCTXDataType)3, 184 },
	{ (Il2CppRGCTXDataType)3, 18629 },
	{ (Il2CppRGCTXDataType)3, 18628 },
	{ (Il2CppRGCTXDataType)2, 471 },
	{ (Il2CppRGCTXDataType)3, 25410 },
	{ (Il2CppRGCTXDataType)3, 28384 },
	{ (Il2CppRGCTXDataType)2, 4721 },
	{ (Il2CppRGCTXDataType)2, 606 },
	{ (Il2CppRGCTXDataType)2, 2855 },
	{ (Il2CppRGCTXDataType)3, 13678 },
	{ (Il2CppRGCTXDataType)3, 13679 },
	{ (Il2CppRGCTXDataType)3, 13682 },
	{ (Il2CppRGCTXDataType)3, 13683 },
	{ (Il2CppRGCTXDataType)2, 490 },
	{ (Il2CppRGCTXDataType)2, 2001 },
	{ (Il2CppRGCTXDataType)3, 10579 },
	{ (Il2CppRGCTXDataType)3, 13684 },
	{ (Il2CppRGCTXDataType)3, 13681 },
	{ (Il2CppRGCTXDataType)2, 871 },
	{ (Il2CppRGCTXDataType)3, 386 },
	{ (Il2CppRGCTXDataType)3, 13680 },
	{ (Il2CppRGCTXDataType)3, 13694 },
	{ (Il2CppRGCTXDataType)3, 13693 },
	{ (Il2CppRGCTXDataType)2, 615 },
	{ (Il2CppRGCTXDataType)2, 872 },
	{ (Il2CppRGCTXDataType)3, 387 },
	{ (Il2CppRGCTXDataType)3, 388 },
	{ (Il2CppRGCTXDataType)2, 1911 },
	{ (Il2CppRGCTXDataType)2, 2525 },
	{ (Il2CppRGCTXDataType)3, 29663 },
	{ (Il2CppRGCTXDataType)3, 29664 },
	{ (Il2CppRGCTXDataType)3, 13673 },
	{ (Il2CppRGCTXDataType)2, 2852 },
	{ (Il2CppRGCTXDataType)3, 13671 },
	{ (Il2CppRGCTXDataType)3, 13674 },
	{ (Il2CppRGCTXDataType)3, 13675 },
	{ (Il2CppRGCTXDataType)3, 13672 },
	{ (Il2CppRGCTXDataType)2, 3506 },
	{ (Il2CppRGCTXDataType)3, 19784 },
	{ (Il2CppRGCTXDataType)3, 19783 },
	{ (Il2CppRGCTXDataType)3, 19773 },
	{ (Il2CppRGCTXDataType)2, 3552 },
	{ (Il2CppRGCTXDataType)3, 19780 },
	{ (Il2CppRGCTXDataType)3, 19782 },
	{ (Il2CppRGCTXDataType)3, 19776 },
	{ (Il2CppRGCTXDataType)3, 19777 },
	{ (Il2CppRGCTXDataType)3, 19778 },
	{ (Il2CppRGCTXDataType)3, 19775 },
};
extern const CustomAttributesCacheGenerator g_Obi_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Obi_CodeGenModule;
const Il2CppCodeGenModule g_Obi_CodeGenModule = 
{
	"Obi.dll",
	2074,
	s_methodPointers,
	59,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	15,
	s_rgctxIndices,
	97,
	s_rgctxValues,
	NULL,
	g_Obi_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
