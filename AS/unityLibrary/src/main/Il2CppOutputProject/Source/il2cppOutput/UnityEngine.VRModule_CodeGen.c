﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean UnityEngine.XR.XRSettings::get_enabled()
extern void XRSettings_get_enabled_m970BB98BF899D943776BE6EB66FE40AA9C12A902 (void);
// 0x00000002 System.Boolean UnityEngine.XR.XRSettings::get_isDeviceActive()
extern void XRSettings_get_isDeviceActive_m8A54A2D4D91CA2BC1B1554576B2FACFE5F65B087 (void);
// 0x00000003 System.Boolean UnityEngine.XR.XRSettings::get_showDeviceView()
extern void XRSettings_get_showDeviceView_mCC1B3C94F32DE036471D10B2E75840D8A3AE0CDB (void);
// 0x00000004 System.Void UnityEngine.XR.XRSettings::set_showDeviceView(System.Boolean)
extern void XRSettings_set_showDeviceView_mA6BAEC79FBDD01F977A931DE1B2BDA4AFADD6E55 (void);
// 0x00000005 System.Single UnityEngine.XR.XRSettings::get_eyeTextureResolutionScale()
extern void XRSettings_get_eyeTextureResolutionScale_mA45B94CA056D8DBFFF534FB9DAF6185796033614 (void);
// 0x00000006 System.Void UnityEngine.XR.XRSettings::set_eyeTextureResolutionScale(System.Single)
extern void XRSettings_set_eyeTextureResolutionScale_mBABE036EB59B20AF02A26771F34BB35453C806D2 (void);
// 0x00000007 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureWidth()
extern void XRSettings_get_eyeTextureWidth_m6202CB8B350531730FAFBBC6CF64EECCA3CBD860 (void);
// 0x00000008 System.Int32 UnityEngine.XR.XRSettings::get_eyeTextureHeight()
extern void XRSettings_get_eyeTextureHeight_m045874DF2D8935D59582C65D8EA9A0A3D96D091A (void);
// 0x00000009 UnityEngine.RenderTextureDescriptor UnityEngine.XR.XRSettings::get_eyeTextureDesc()
extern void XRSettings_get_eyeTextureDesc_m58F62EE4C7F46984BDC58081AF2E40DE625AC582 (void);
// 0x0000000A System.Single UnityEngine.XR.XRSettings::get_renderViewportScale()
extern void XRSettings_get_renderViewportScale_m7611DFAB2B3914ABAB79D1BF1A61909DD91A9538 (void);
// 0x0000000B System.Single UnityEngine.XR.XRSettings::get_renderViewportScaleInternal()
extern void XRSettings_get_renderViewportScaleInternal_m99A2E45DE86E39CAD2795A32424B3FF99A10261C (void);
// 0x0000000C System.String UnityEngine.XR.XRSettings::get_loadedDeviceName()
extern void XRSettings_get_loadedDeviceName_m1E091DB259635ACAE9C3B77980CDB00AC06B6D4C (void);
// 0x0000000D System.String[] UnityEngine.XR.XRSettings::get_supportedDevices()
extern void XRSettings_get_supportedDevices_m48F7F0CC911EB45AD210523FA957A4E8412AE93F (void);
// 0x0000000E UnityEngine.XR.XRSettings/StereoRenderingMode UnityEngine.XR.XRSettings::get_stereoRenderingMode()
extern void XRSettings_get_stereoRenderingMode_m2E44AF3D772559836F2357AEC1E175C68E6D94B9 (void);
// 0x0000000F System.Void UnityEngine.XR.XRSettings::get_eyeTextureDesc_Injected(UnityEngine.RenderTextureDescriptor&)
extern void XRSettings_get_eyeTextureDesc_Injected_m639509084F5EC222779474C77EF7586989C4F856 (void);
// 0x00000010 System.Void UnityEngine.XR.XRDevice::InvokeDeviceLoaded(System.String)
extern void XRDevice_InvokeDeviceLoaded_m3BDF6825A2A56E4923D4E6593C7BA2949B6A3581 (void);
// 0x00000011 System.Void UnityEngine.XR.XRDevice::.cctor()
extern void XRDevice__cctor_mC83C1293819B81E68EC72D01A5CC107DFE29B98C (void);
static Il2CppMethodPointer s_methodPointers[17] = 
{
	XRSettings_get_enabled_m970BB98BF899D943776BE6EB66FE40AA9C12A902,
	XRSettings_get_isDeviceActive_m8A54A2D4D91CA2BC1B1554576B2FACFE5F65B087,
	XRSettings_get_showDeviceView_mCC1B3C94F32DE036471D10B2E75840D8A3AE0CDB,
	XRSettings_set_showDeviceView_mA6BAEC79FBDD01F977A931DE1B2BDA4AFADD6E55,
	XRSettings_get_eyeTextureResolutionScale_mA45B94CA056D8DBFFF534FB9DAF6185796033614,
	XRSettings_set_eyeTextureResolutionScale_mBABE036EB59B20AF02A26771F34BB35453C806D2,
	XRSettings_get_eyeTextureWidth_m6202CB8B350531730FAFBBC6CF64EECCA3CBD860,
	XRSettings_get_eyeTextureHeight_m045874DF2D8935D59582C65D8EA9A0A3D96D091A,
	XRSettings_get_eyeTextureDesc_m58F62EE4C7F46984BDC58081AF2E40DE625AC582,
	XRSettings_get_renderViewportScale_m7611DFAB2B3914ABAB79D1BF1A61909DD91A9538,
	XRSettings_get_renderViewportScaleInternal_m99A2E45DE86E39CAD2795A32424B3FF99A10261C,
	XRSettings_get_loadedDeviceName_m1E091DB259635ACAE9C3B77980CDB00AC06B6D4C,
	XRSettings_get_supportedDevices_m48F7F0CC911EB45AD210523FA957A4E8412AE93F,
	XRSettings_get_stereoRenderingMode_m2E44AF3D772559836F2357AEC1E175C68E6D94B9,
	XRSettings_get_eyeTextureDesc_Injected_m639509084F5EC222779474C77EF7586989C4F856,
	XRDevice_InvokeDeviceLoaded_m3BDF6825A2A56E4923D4E6593C7BA2949B6A3581,
	XRDevice__cctor_mC83C1293819B81E68EC72D01A5CC107DFE29B98C,
};
static const int32_t s_InvokerIndices[17] = 
{
	6195,
	6195,
	6195,
	6109,
	6197,
	6112,
	6176,
	6176,
	6194,
	6197,
	6197,
	6183,
	6183,
	6176,
	6096,
	6104,
	6207,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_VRModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_VRModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VRModule_CodeGenModule = 
{
	"UnityEngine.VRModule.dll",
	17,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_VRModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
