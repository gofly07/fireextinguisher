﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using static GPTT.Data.API;

public class Panel_Extinguishing : MonoBehaviour
{
    [SerializeField] private AudioClip win;
    [SerializeField] private AudioClip lose;

    [SerializeField] private Text txtTitle;
    [SerializeField] private Button btHome;
    [SerializeField] private Slider sldProgressBar;

    [Header("Main Flow")]
    [SerializeField] private List<GameObject> extinguishTargets;
    [SerializeField] private List<float> waitTimes = new List<float>();
    [SerializeField] private List<float> targetTimes = new List<float>();
    [SerializeField] private Ignis.FlameEngine flameEngine;
    private Vector3 previousPos = Vector3.zero;
    private Quaternion previousQuat = Quaternion.identity;
    private bool fire = false;
    private float timer = 0;
    private bool end = false;
    private int counr = 0; 
    public List<string> typeTarget = new List<string>();

    //**
    private FireExtinguisher fireExtinguisher = new FireExtinguisher();
    private DateTime _startTime;
    private DateTime _endTime;
    //**

    private void OnEnable()
    {  
        //**
        fireExtinguisher = new FireExtinguisher();
        _startTime = DateTime.Now;
        fireExtinguisher.startTime = _startTime.ToString("yyyy-MM-dd HH:mm:ss");
        //**

        btHome.onClick.AddListener(delegate { StartCoroutine(BackToMainScene()); });
        click = false;
    }

    bool click = false;
    private IEnumerator BackToMainScene()
    {
        if (!click)
        {
            click = true;
#if !UNITY_EDITOR
            //**
            _endTime = DateTime.Now;
            fireExtinguisher.endTime = _endTime.ToString("yyyy-MM-dd HH:mm:ss");

            var diff = _endTime.Subtract(_startTime);
            var res = String.Format("{0}h {1}m {2}s", diff.Hours, diff.Minutes, diff.Seconds);
            fireExtinguisher.totalTime = res;

            fireExtinguisher.project = "滅火測驗-" + UserIDCheck.roomName;

            if (txtTitle.text== "滅火完成")
                fireExtinguisher.status = "已完成";
            else
                fireExtinguisher.status = "未完成";

            fireExtinguisher.userId = UserIDCheck.userID;

            yield return (GPTT.Data.APIHelper.FireExtinguisher_Insert(fireExtinguisher));
            //**
#endif
            yield return null;
            SceneManager.LoadScene("Client_Quest2");
        }
    }

    private void Update()
    {
        if (end)
            return;

        timer += Time.deltaTime;
        if (timer < 60)
        {
            sldProgressBar.value = timer;
        }

        //failed
        if (timer >= 60)
        {
            end = true;
            PlayVoice(lose);
            txtTitle.text = "滅火失敗";
            sldProgressBar.gameObject.SetActive(false);
            CancelInvoke("Win");
        }

        if (Input.GetKeyDown(KeyCode.H))
            fire = true;
        else if (Input.GetKeyDown(KeyCode.L))
            fire = false;

        foreach (var i in typeTarget)
        {
            if (i == Scene_Manager.instance.type)
            {

                //--------------------------------------------------------------------------------

                Vector3 newPos = Scene_Manager.instance.handsVisualizationR.transform.position;
                Quaternion newQuat = Scene_Manager.instance.handsVisualizationR.transform.rotation;

                bool sw = false;
                if (Vector3.Distance(previousPos, newPos) > 0.005f ||
                    Quaternion.Angle(previousQuat, newQuat) > 2f)
                {
                    sw = true;
                }
                else
                    sw = false;

                for (int ii = 0; ii < extinguishTargets.Count; ii++)
                {
                    if (GameObject.Find("RightHandAnchor").GetComponent<RayCasting>().hitCollider != null &&
                        GameObject.Find("RightHandAnchor").GetComponent<RayCasting>().hitCollider.gameObject == extinguishTargets[ii] &&
                        (OVRInput.Get(OVRInput.RawButton.X) || fire) &&
                        sw)
                    {
                        waitTimes[ii] += Time.deltaTime;

                        if (waitTimes[ii] >= targetTimes[ii] && extinguishTargets[ii].transform.GetChild(0).GetComponent<Ignis.SphereExtinguish>().enabled == false)
                        {
                            extinguishTargets[ii].transform.GetChild(0).GetComponent<Ignis.SphereExtinguish>().enabled = true;
                            counr++;
                            //win
                            if (counr >= extinguishTargets.Count)
                            {
                                Invoke("Win", 2);
                            }
                        }
                    }
                }

                previousPos = newPos;
                previousQuat = newQuat;
            }
        }
    }

    void Win()
    {
        end = true;
        txtTitle.text = "滅火完成";
        PlayVoice(win);
        sldProgressBar.gameObject.SetActive(false);

        if (flameEngine != null)
        {
            foreach (var ad in flameEngine.GetComponentsInChildren<AudioSource>())
                ad.loop = false;
        }
    }

    private void PlayVoice(AudioClip ac)
    {
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().clip = ac;
        GetComponent<AudioSource>().Play();
        GetComponent<AudioSource>().loop = false;
    }
}
