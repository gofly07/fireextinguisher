using System;
using System.Collections;
using UnityEngine;
using static GPTT.Data.API;

namespace GPTT.Data
{
    public static class APIHelper
    {
        public static bool isLogin = false;

        public static void Init()
        {
            Debug.Log("APIHelper");
            Util.Run();
        }

        public static IEnumerator Login(Login login, Action<User> onSuccess = null, Action<string> onError = null)
        {
            yield return API.Login_API(login);

            if (API.ResponseMessage["responseCode"] == "200")
            {
                //Debug.Log(UserData.id + " " + UserData.name + " " + UserData.email + " " + UserData.unit);
                isLogin = true;
                onSuccess?.Invoke(UserData);
            }
            else
            {
                //Debug.Log(API.ResponseMessage["message"]);
                onError?.Invoke(API.ResponseMessage["message"]);
            }
        }

        public static void Logout()
        {
            isLogin = false;
            UserData = null;
        }

        public static User GetUserData()
        {
            return UserData;
        }

        public static IEnumerator FireExtinguisher_Insert(FireExtinguisher fireExtinguisher, Action<User> onSuccess = null, Action<string> onError = null)
        {
            yield return API.FireExtinguisher_API(fireExtinguisher);
            //Debug.Log(API.ResponseMessage["responseCode"]);
            //Debug.Log(API.ResponseMessage["message"]);
        }

        public static IEnumerator FireExtinguisherList_Search(Search search, Action<User> onSuccess = null, Action<string> onError = null)
        {
            yield return API.FireExtinguisherList_API(search);

            if (API.ResponseMessage["responseCode"] == "200")
            {
                //Debug.Log(API.fireExtinguisherListData.total);
                //Debug.Log(API.fireExtinguisherListData.last_page);

                foreach (FireExtinguisher data in API.fireExtinguisherListData.Data)
                {
                    //Debug.Log(data.id + " " + data.startTime + " " + data.endTime + " " + data.totalTime + " " + data.project + " " + data.status + " " + data.userName);
                }
            }
            else
            {
                //Debug.Log(API.ResponseMessage["message"]);
                onError?.Invoke(API.ResponseMessage["message"]);
            }
        }

        public static IEnumerator Firemen_Insert(Firemen firemen, Action onSuccess = null, Action<string> onError = null)
        {
            yield return API.Firemen_API(firemen);

            if (API.ResponseMessage["responseCode"] == "200")
            {
                onSuccess?.Invoke();
            }
            else
            {
               // Debug.Log(API.ResponseMessage["message"]);
                onError?.Invoke(API.ResponseMessage["message"]);
            }
        }

        public static IEnumerator FiremenList_Search(Search search, Action<FiremenList> onSuccess = null, Action<string> onError = null)
        {
            yield return API.FiremenList_API(search);

            if (API.ResponseMessage["responseCode"] == "200")
            {
                //Debug.Log(API.firemenListData.total);
                //Debug.Log(API.firemenListData.last_page);
                onSuccess?.Invoke(API.firemenListData);
                foreach (Firemen data in API.firemenListData.Data)
                {
                    //Debug.Log(data.id + " " + data.trainingDate + " " + data.startTime + " " + data.endTime + " " + data.totalTime + " " + data.modCategory + " " + data.place + " " + data.userName);
                    foreach (FiremenDetailed firemenDetailed in data.detailedList)
                    {
                        //Debug.Log(firemenDetailed.id + " " + firemenDetailed.trainingTime + " " + firemenDetailed + " " + firemenDetailed.posture + " " + firemenDetailed.behavior + " " + firemenDetailed.location + " " + firemenDetailed.userName);
                    }
                }
            }
            else
            {
                //Debug.Log(API.ResponseMessage["message"]);
                onError?.Invoke(API.ResponseMessage["message"]);
            }
        }

        public static IEnumerator FiremenDetailList_Search(Search search, Action<FiremenDetailedList> onSuccess = null, Action<string> onError = null)
        {
            yield return API.FiremenDetailList_API(search);

            if (API.ResponseMessage["responseCode"] == "200")
            {
                //Debug.Log(API.firemenDetailedData.total);
                //Debug.Log(API.firemenDetailedData.last_page);
                onSuccess?.Invoke(API.firemenDetailedData);
                foreach (FiremenDetailed data in API.firemenDetailedData.Data)
                {
                    //Debug.Log(data.id + " " + data.trainingTime + " " + data.personnel + " " + data.posture + " " + data.behavior + " " + data.location + " " + data.userName);
                }
            }
            else
            {
                //Debug.Log(API.ResponseMessage["message"]);
                onError?.Invoke(API.ResponseMessage["message"]);
            }
        }
    }
}