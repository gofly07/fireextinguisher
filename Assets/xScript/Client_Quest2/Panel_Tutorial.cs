﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using static GPTT.Data.API;

public class Panel_Tutorial : MonoBehaviour
{
    [Header("Main Paramenter")]
    [SerializeField] private Button btBack;

    [Header("Progress Bar")]
    [SerializeField] private Text txtCurrentState;
    [SerializeField] private Slider sldProgressBar;

    [Header("State Panel")]
    [SerializeField] private Text txtStepCount;
    [SerializeField] private List<ProgressState> progressStates = new List<ProgressState>();
    [TextArea][SerializeField] private List<string> strSentences = new List<string>();

    [SerializeField] private List<AudioClip> audios = new List<AudioClip>();


    [System.Serializable]
    private class ProgressState
    {
        public Text txtSentence;
        public Image imgClick;
    }

    [Header("Progress Paramenters")]
    [SerializeField] private int currentState = 0;
    [SerializeField] private int currentNum = 0;
    [SerializeField] private GameObject fireTutorialTarget;
    private GameObject target;
    private Vector3 previousPos = Vector3.zero;
    private Quaternion previousQuat = Quaternion.identity;
    private bool fire = false;

    //**
    private FireExtinguisher fireExtinguisher = new FireExtinguisher();
    private DateTime _startTime;
    private DateTime _endTime;
    //**

    public event EventHandler EHCloseTutorial;

    private void OnEnable()
    {
        //**
        fireExtinguisher = new FireExtinguisher();
        _startTime = DateTime.Now;
        fireExtinguisher.startTime = _startTime.ToString("yyyy-MM-dd HH:mm:ss");
        //**

        Init();

        btBack.onClick.AddListener(delegate {
            if (target != null)
                Destroy(target);
            StartCoroutine(BackToMenu());
        }); 
        click = false;
    }

    private void Init()
    {
        sldProgressBar.gameObject.SetActive(false);
        sldProgressBar.value = 0;

        StopAllCoroutines();
        StartCoroutine(StateProgress());
    }

    // 0 1 2 3 4 5 6 7
    private void ChangeState(int num)
    {
        txtStepCount.text = string.Format("{0}/7", num);
        txtCurrentState.text = strSentences[num];
        currentNum = num;
        foreach (var i in progressStates)
        {
            i.txtSentence.color = new Color32(55, 95, 118, 255);
            i.imgClick.enabled = false;
        }

        if (num != 7)
            progressStates[num].txtSentence.color = Color.black;

        for (int i = 0; i < num; i++)
            progressStates[i].imgClick.enabled = true;
    }

    private void PlayVoice(int num)
    {
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().clip = audios[num];
        GetComponent<AudioSource>().Play();
        GetComponent<AudioSource>().loop = false;
    }

    private IEnumerator StateProgress()
    {
        yield return null;

        float waitTime = 0;
        currentState = 0;
        PlayVoice(currentState);
        previousPos = Vector3.zero;
        previousQuat = Quaternion.identity;

        while (true)
        {
            yield return new WaitForEndOfFrame();

            if (currentState == 0)
            {
                ChangeState(currentState);

                if (GameObject.Find("CenterEyeAnchor").GetComponent<RayCasting>().hitCollider != null &&
                    GameObject.Find("CenterEyeAnchor").GetComponent<RayCasting>().hitCollider.gameObject.name == "HeadCollider")
                {
                    waitTime += Time.deltaTime;
                    GameObject.Find("HeadCollider").transform.GetChild(0).gameObject.SetActive(true);
                    GameObject.Find("HeadCollider").transform.GetChild(0).GetChild(1).GetComponent<Image>().fillAmount = waitTime * 0.5f;

                    if (waitTime >= 2)
                    {
                        GameObject.Find("HeadCollider").transform.GetChild(0).gameObject.SetActive(false);
                        currentState = 1;
                        PlayVoice(currentState);
                        waitTime = 0;
                    }
                }
                else if (GameObject.Find("CenterEyeAnchor").GetComponent<RayCasting>().hitCollider == null)
                {
                    //GameObject.Find("HeadCollider").transform.GetChild(0).gameObject.SetActive(false);
                    //waitTime = 0;
                }
            }
            else if (currentState == 1)
            {
                ChangeState(currentState);

                if (GameObject.Find("CenterEyeAnchor").GetComponent<RayCasting>().hitCollider != null &&
                    GameObject.Find("CenterEyeAnchor").GetComponent<RayCasting>().hitCollider.gameObject.name == "BodyCollider")
                {
                    waitTime += Time.deltaTime;
                    GameObject.Find("BodyCollider").transform.GetChild(0).gameObject.SetActive(true);
                    GameObject.Find("BodyCollider").transform.GetChild(0).GetChild(1).GetComponent<Image>().fillAmount = waitTime * 0.5f;

                    if (waitTime >= 2)
                    {
                        GameObject.Find("BodyCollider").transform.GetChild(0).gameObject.SetActive(false);
                        currentState = 2; 
                        PlayVoice(currentState);
                        waitTime = 0;
                    }
                }
                else if (GameObject.Find("CenterEyeAnchor").GetComponent<RayCasting>().hitCollider == null)
                {
                    //GameObject.Find("BodyCollider").transform.GetChild(0).gameObject.SetActive(false);
                    //waitTime = 0;
                }
            }
            else if (currentState == 2)
            {
                ChangeState(currentState);

                if (OVRInput.GetDown(OVRInput.RawButton.Y))
                {
                    target =  Instantiate(fireTutorialTarget) as GameObject;
                    currentState = 3;
                    PlayVoice(currentState);
                }
            }
            else if (currentState == 3)
            {
                ChangeState(currentState);

                
                if (GameObject.Find("RightHandAnchor").GetComponent<RayCasting>().hitCollider != null &&
                    GameObject.Find("RightHandAnchor").GetComponent<RayCasting>().hitCollider.gameObject == target)
                {
                    waitTime += Time.deltaTime;

                    if (waitTime >= 1)
                    {
                        currentState = 4;
                        PlayVoice(currentState);
                        waitTime = 0;
                    }
                }
            }
            else if (currentState == 4)
            {
                ChangeState(currentState);

                if (OVRInput.Get(OVRInput.RawButton.X) || fire)
                {
                    waitTime += Time.deltaTime;
                    if (waitTime >= 1)
                    {
                        currentState = 5;
                        PlayVoice(currentState);
                        waitTime = 0;
                    }
                }
            }
            else if (currentState == 5)
            {
                ChangeState(currentState);

                sldProgressBar.gameObject.SetActive(true);

                if (GameObject.Find("RightHandAnchor").GetComponent<RayCasting>().hitCollider != null &&
                    GameObject.Find("RightHandAnchor").GetComponent<RayCasting>().hitCollider.gameObject == target &&
                    (OVRInput.Get(OVRInput.RawButton.X) || fire))
                {
                    waitTime += Time.deltaTime;
                    if (waitTime < 3)
                        sldProgressBar.value = waitTime;

                    if (waitTime >= 3)
                    {
                        currentState = 6;
                        PlayVoice(currentState);
                        waitTime = 0;
                        sldProgressBar.gameObject.SetActive(false);
                    }
                }
            }
            else if (currentState == 6)
            {
                ChangeState(currentState);

                Vector3 newPos = ClientManager.instance.handsVisualizationR.transform.position;
                Quaternion newQuat = ClientManager.instance.handsVisualizationR.transform.rotation;

                bool sw = false;
                if (Vector3.Distance(previousPos, newPos) > 0.005f ||
                    Quaternion.Angle(previousQuat, newQuat) > 2f)
                    sw = true;
                else
                    sw = false;

                if (GameObject.Find("RightHandAnchor").GetComponent<RayCasting>().hitCollider != null &&
                    GameObject.Find("RightHandAnchor").GetComponent<RayCasting>().hitCollider.gameObject == target &&
                    (OVRInput.Get(OVRInput.RawButton.X) || fire) &&
                    sw)
                {
                    waitTime += Time.deltaTime;

                    if (waitTime >= 1.5f)
                    {
                        target.transform.GetChild(3).GetComponent<Ignis.SphereExtinguish>().enabled = true;
                        ChangeState(7);
                        PlayVoice(7);
                        waitTime = 0;
                        break;
                    }
                }

                previousPos = newPos;
                previousQuat = newQuat;
            }
        }
    }

    private void Update()
    {
        if (currentState == 2)
        {
            if (Input.GetKeyDown(KeyCode.O))
            {
                target = Instantiate(fireTutorialTarget) as GameObject;
                currentState = 3;
            }
        }
        if (Input.GetKeyDown(KeyCode.H))
            fire = true;
        else if (Input.GetKeyDown(KeyCode.L))
            fire = false;

    }

    bool click = false;
    private IEnumerator BackToMenu()
    {
        if (!click)
        {
            click = true;
#if !UNITY_EDITOR
            //**
            _endTime = DateTime.Now;
            fireExtinguisher.endTime = _endTime.ToString("yyyy-MM-dd HH:mm:ss");

            var diff = _endTime.Subtract(_startTime);
            var res = String.Format("{0}h {1}m {2}s", diff.Hours, diff.Minutes, diff.Seconds);
            fireExtinguisher.totalTime = res;

            fireExtinguisher.project =String.Format("滅火器使用教學-步驟{0}/7", currentNum);

            if (currentNum!=7)
                fireExtinguisher.status = "未完成";
            else
                fireExtinguisher.status = "已完成";

            fireExtinguisher.userId = UserIDCheck.userID;

            yield return (GPTT.Data.APIHelper.FireExtinguisher_Insert(fireExtinguisher));
            //**
#endif 
            yield return null;
            EHCloseTutorial(this, EventArgs.Empty);
        }
    }
}
