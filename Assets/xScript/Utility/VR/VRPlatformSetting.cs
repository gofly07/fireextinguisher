﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;
using UnityEngine.XR.Management;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace DKP.VR
{
    public enum Hand
    {
        left, right
    }

    public static class ControlState
    {
        public static bool rayCasting = false;
        public static bool drawing = false;
        public static bool leftHandTouching = false;
        public static bool rightHandTouching = false;
    }

    public static class VRIO
    {
        async public static void Haptics(Hand hand)
        {
            if (hand == Hand.left)
            {
                OVRInput.SetControllerVibration(0.125f, 0.125f, OVRInput.Controller.LTouch);
                await Task.Delay(100);
                OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.LTouch);
            }
            else if (hand == Hand.right)
            {
                OVRInput.SetControllerVibration(0.125f, 0.125f, OVRInput.Controller.RTouch);
                await Task.Delay(100);
                OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.RTouch);
            }
            await Task.Yield();
        }
        public static void Throw(Hand hand, GameObject obj)
        {
            if (hand == Hand.left && obj.GetComponent<Rigidbody>() != null)
            {
                obj.GetComponent<Rigidbody>().velocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.LTouch);
                obj.GetComponent<Rigidbody>().angularVelocity = OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.LTouch);
            }
            else if (hand == Hand.right && obj.GetComponent<Rigidbody>() != null)
            {
                obj.GetComponent<Rigidbody>().velocity = OVRInput.GetLocalControllerVelocity(OVRInput.Controller.RTouch);
                obj.GetComponent<Rigidbody>().angularVelocity = OVRInput.GetLocalControllerAngularVelocity(OVRInput.Controller.RTouch);
            }
        }
    }

    public class VRPlatformSetting : MonoBehaviour
    {
        public static VRPlatformSetting instance;

        [Header("Oculus Setting (camera)")]
        public GameObject oVRCameraRig;
        public OVRControllerHelper oVRLeft;
        public OVRControllerHelper oVRRight;

        private void Awake()
        {
            instance = this;
        }
    }
}

#if UNITY_EDITOR
namespace DKP.VR
{
    public class VRPlatformSwitcher : EditorWindow
    {
        [MenuItem("DKP/VR Platform Switcher", false, 0)]
        static void Window()
        {
            VRPlatformSwitcher window = GetWindowWithRect<VRPlatformSwitcher>(new Rect(0, 0, 300, 75), true, "VR Platform Switcher");
            window.Show();
        }

        int index = 0;
        Dictionary<int, string> allPlatform = new Dictionary<int, string>()
        {
            {0, "Oculus Integration"},
            {1, "Steam VR 2"}
        };

        private void OnGUI()
        {
            GUILayout.Space(5);
            GUILayout.Label("Please select one of these VR SDK.", EditorStyles.boldLabel);
            index = EditorGUILayout.IntPopup("SDK: ", index, allPlatform.Values.ToArray(), allPlatform.Keys.ToArray());

            if (GUILayout.Button("Switch"))
            {
                switch (allPlatform[index])
                {
                    case "Oculus Integration":
                        AddDefineSymbols("DKP_STEAMVR_2", "DKP_OCULUS_INTEGRATION");
                        //UnityEditor.XR.Management.XRGeneralSettingsPerBuildTarget.XRGeneralSettingsForBuildTarget(BuildTargetGroup.Standalone);
                        XRGeneralSettings.Instance.Manager.StartSubsystems();
                        //PlayerSettings.SetVirtualRealitySupported(BuildTargetGroup.Standalone, true);
                        //PlayerSettings.SetVirtualRealitySDKs(BuildTargetGroup.Standalone, new List<string> { "Oculus" }.ToArray());
                        ActivateVRCameraByEachScene(true, false);
                        break;

                    case "Steam VR 2":
                        AddDefineSymbols("DKP_OCULUS_INTEGRATION", "DKP_STEAMVR_2");
                        //PlayerSettings.SetVirtualRealitySupported(BuildTargetGroup.Standalone, true);
                        //PlayerSettings.SetVirtualRealitySDKs(BuildTargetGroup.Standalone, new List<string> { "OpenVR" }.ToArray());
                        ActivateVRCameraByEachScene(false, true);
                        break;
                }
            }
        }

        private void AddDefineSymbols(string oldString, string newString)
        {
            string definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
            List<string> allDefines = definesString.Split(';').ToList();

            if (!allDefines.Contains(newString))
                allDefines.Add(newString);

            if (allDefines.Contains(oldString))
                allDefines.Remove(oldString);

            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup,
                                                             string.Join(";", allDefines.ToArray()));
        }

        private void ActivateVRCameraByEachScene(bool oculusCameraRig, bool viveCameraRig)
        {
            EditorBuildSettingsScene[] allScenes = EditorBuildSettings.scenes;

            foreach (EditorBuildSettingsScene scene in allScenes)
            {
                Scene sceneRef = EditorSceneManager.OpenScene(scene.path, OpenSceneMode.Single);
                var platformSetting = GameObject.FindObjectOfType<VRPlatformSetting>();
                if (null == platformSetting)
                    continue;

                platformSetting.oVRCameraRig.SetActive(oculusCameraRig);

                EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
                AssetDatabase.SaveAssets();
            }
        }
    }
}
#endif