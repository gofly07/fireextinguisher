﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Panel_Menu : MonoBehaviour
{
    [SerializeField] private Button btAllInOne;
    [SerializeField] private Button btDataRecord;
    [SerializeField] private Button btLogout;

    public event EventHandler EHOpenAllInOne;
    public event EventHandler EHOpenDataRecord;
    public event EventHandler EHLogout;

    private void OnEnable()
    {
        btAllInOne.onClick.AddListener(delegate { AllInOneMode(); });
        btDataRecord.onClick.AddListener(delegate { DataRecordMode(); });
        btLogout.onClick.AddListener(delegate { Logout(); });
    }

    private void AllInOneMode()
    {
        EHOpenAllInOne(this, EventArgs.Empty);
    }

    private void DataRecordMode()
    {
        EHOpenDataRecord(this, EventArgs.Empty);
    }

    private void Logout()
    {
        //登出
        GPTT.Data.APIHelper.Logout();
        EHLogout(this, EventArgs.Empty);
    }
}