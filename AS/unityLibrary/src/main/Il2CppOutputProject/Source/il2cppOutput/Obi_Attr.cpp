﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// UnityEngine.DelayedAttribute
struct DelayedAttribute_t84E6423E615599EDDEAA39BC245C5B871A2D9948;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// Obi.Indent
struct Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584;
// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MinAttribute
struct MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// Obi.SerializeProperty
struct SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// Obi.VisibleIf
struct VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiParticlePicker_tB0405EAD1BFCF9EBDD82F494C3DE49EBECD0F732_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiRopeCursor_t3A3DC1CEC7DA311FC3F7D0FDAE771A06BCFCAFB4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableState
struct DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091 
{
public:
	// System.Int32 System.Diagnostics.DebuggerBrowsableState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.DelayedAttribute
struct DelayedAttribute_t84E6423E615599EDDEAA39BC245C5B871A2D9948  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.MinAttribute
struct MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};


// Obi.MultiPropertyAttribute
struct MultiPropertyAttribute_tD831A4A240690E32C80AEF58B3E877919F8BB38E  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// Obi.SerializeProperty
struct SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String Obi.SerializeProperty::<PropertyName>k__BackingField
	String_t* ___U3CPropertyNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CPropertyNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0, ___U3CPropertyNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CPropertyNameU3Ek__BackingField_0() const { return ___U3CPropertyNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPropertyNameU3Ek__BackingField_0() { return &___U3CPropertyNameU3Ek__BackingField_0; }
	inline void set_U3CPropertyNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CPropertyNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPropertyNameU3Ek__BackingField_0), (void*)value);
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggerBrowsableState System.Diagnostics.DebuggerBrowsableAttribute::state
	int32_t ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}
};


// Obi.Indent
struct Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584  : public MultiPropertyAttribute_tD831A4A240690E32C80AEF58B3E877919F8BB38E
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// Obi.VisibleIf
struct VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3  : public MultiPropertyAttribute_tD831A4A240690E32C80AEF58B3E877919F8BB38E
{
public:
	// System.String Obi.VisibleIf::<MethodName>k__BackingField
	String_t* ___U3CMethodNameU3Ek__BackingField_0;
	// System.Boolean Obi.VisibleIf::<Negate>k__BackingField
	bool ___U3CNegateU3Ek__BackingField_1;
	// System.Reflection.MethodInfo Obi.VisibleIf::eventMethodInfo
	MethodInfo_t * ___eventMethodInfo_2;
	// System.Reflection.FieldInfo Obi.VisibleIf::fieldInfo
	FieldInfo_t * ___fieldInfo_3;

public:
	inline static int32_t get_offset_of_U3CMethodNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3, ___U3CMethodNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CMethodNameU3Ek__BackingField_0() const { return ___U3CMethodNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMethodNameU3Ek__BackingField_0() { return &___U3CMethodNameU3Ek__BackingField_0; }
	inline void set_U3CMethodNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CMethodNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMethodNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNegateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3, ___U3CNegateU3Ek__BackingField_1)); }
	inline bool get_U3CNegateU3Ek__BackingField_1() const { return ___U3CNegateU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CNegateU3Ek__BackingField_1() { return &___U3CNegateU3Ek__BackingField_1; }
	inline void set_U3CNegateU3Ek__BackingField_1(bool value)
	{
		___U3CNegateU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_eventMethodInfo_2() { return static_cast<int32_t>(offsetof(VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3, ___eventMethodInfo_2)); }
	inline MethodInfo_t * get_eventMethodInfo_2() const { return ___eventMethodInfo_2; }
	inline MethodInfo_t ** get_address_of_eventMethodInfo_2() { return &___eventMethodInfo_2; }
	inline void set_eventMethodInfo_2(MethodInfo_t * value)
	{
		___eventMethodInfo_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventMethodInfo_2), (void*)value);
	}

	inline static int32_t get_offset_of_fieldInfo_3() { return static_cast<int32_t>(offsetof(VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3, ___fieldInfo_3)); }
	inline FieldInfo_t * get_fieldInfo_3() const { return ___fieldInfo_3; }
	inline FieldInfo_t ** get_address_of_fieldInfo_3() { return &___fieldInfo_3; }
	inline void set_fieldInfo_3(FieldInfo_t * value)
	{
		___fieldInfo_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fieldInfo_3), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerBrowsableAttribute::.ctor(System.Diagnostics.DebuggerBrowsableState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5 (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * __this, int32_t ___state0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void Obi.SerializeProperty::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3 (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * __this, String_t* ___propertyName0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, const RuntimeMethod* method);
// System.Void Obi.VisibleIf::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibleIf__ctor_m74CCE1E688720090402D0E285F11AA7EE004E6C6 (VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3 * __this, String_t* ___methodName0, bool ___negate1, const RuntimeMethod* method);
// System.Void Obi.Indent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Indent__ctor_m5A092B8D80C878888D885C6ADC0B3A9D3A90E3C7 (Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void UnityEngine.DelayedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DelayedAttribute__ctor_mA73AFD50BA612774A729641927483BC80142A11A (DelayedAttribute_t84E6423E615599EDDEAA39BC245C5B871A2D9948 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.MinAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * __this, float ___min0, const RuntimeMethod* method);
static void Obi_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void ObiContactGrabber_t14D0338C4C370DF79A94FA1DE76A34470B4DF04A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_0_0_0_var), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_mode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x20\x32\x44\x20\x6D\x6F\x64\x65\x2C\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x73\x20\x61\x72\x65\x20\x73\x69\x6D\x75\x6C\x61\x74\x65\x64\x20\x6F\x6E\x20\x74\x68\x65\x20\x58\x59\x20\x70\x6C\x61\x6E\x65\x20\x6F\x6E\x6C\x79\x2E\x20\x46\x6F\x72\x20\x75\x73\x65\x20\x69\x6E\x20\x63\x6F\x6E\x6A\x75\x6E\x63\x74\x69\x6F\x6E\x20\x77\x69\x74\x68\x20\x55\x6E\x69\x74\x79\x27\x73\x20\x32\x44\x20\x6D\x6F\x64\x65\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_interpolation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x6D\x65\x20\x61\x73\x20\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x2E\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x69\x6F\x6E\x2E\x20\x53\x65\x74\x20\x74\x6F\x20\x49\x4E\x54\x45\x52\x50\x4F\x4C\x41\x54\x45\x20\x66\x6F\x72\x20\x63\x6C\x6F\x74\x68\x20\x74\x68\x61\x74\x20\x69\x73\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x6F\x6E\x20\x61\x20\x6D\x61\x69\x6E\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x6F\x72\x20\x63\x6C\x6F\x73\x65\x6C\x79\x20\x66\x6F\x6C\x6C\x6F\x77\x65\x64\x20\x62\x79\x20\x61\x20\x63\x61\x6D\x65\x72\x61\x2E\x20\x4E\x4F\x4E\x45\x20\x66\x6F\x72\x20\x65\x76\x65\x72\x79\x74\x68\x69\x6E\x67\x20\x65\x6C\x73\x65\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_gravity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x6D\x75\x6C\x61\x74\x69\x6F\x6E\x20\x67\x72\x61\x76\x69\x74\x79\x20\x65\x78\x70\x72\x65\x73\x73\x65\x64\x20\x69\x6E\x20\x6C\x6F\x63\x61\x6C\x20\x73\x70\x61\x63\x65\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x63\x65\x6E\x74\x61\x67\x65\x20\x6F\x66\x20\x76\x65\x6C\x6F\x63\x69\x74\x79\x20\x6C\x6F\x73\x74\x20\x70\x65\x72\x20\x73\x65\x63\x6F\x6E\x64\x2C\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x25\x20\x28\x30\x29\x20\x61\x6E\x64\x20\x31\x30\x30\x25\x20\x28\x31\x29\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_maxAnisotropy(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 5.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x20\x72\x61\x74\x69\x6F\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x61\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x27\x73\x20\x6C\x6F\x6E\x67\x65\x73\x74\x20\x61\x6E\x64\x20\x73\x68\x6F\x72\x74\x65\x73\x74\x20\x61\x78\x69\x73\x2E\x20\x55\x73\x65\x20\x31\x20\x66\x6F\x72\x20\x69\x73\x6F\x74\x72\x6F\x70\x69\x63\x20\x28\x63\x6F\x6D\x70\x6C\x65\x74\x65\x6C\x79\x20\x72\x6F\x75\x6E\x64\x29\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x73\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_sleepThreshold(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x73\x73\x2D\x6E\x6F\x72\x6D\x61\x6C\x69\x7A\x65\x64\x20\x6B\x69\x6E\x65\x74\x69\x63\x20\x65\x6E\x65\x72\x67\x79\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x62\x65\x6C\x6F\x77\x20\x77\x68\x69\x63\x68\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x6E\x27\x74\x20\x75\x70\x64\x61\x74\x65\x64\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_collisionMargin(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x28\x73\x69\x6D\x70\x6C\x69\x63\x65\x73\x2F\x63\x6F\x6C\x6C\x69\x64\x65\x72\x73\x29\x20\x66\x6F\x72\x20\x61\x20\x63\x6F\x6E\x74\x61\x63\x74\x20\x74\x6F\x20\x62\x65\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x64\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_maxDepenetration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x64\x65\x70\x65\x6E\x65\x74\x72\x61\x74\x69\x6F\x6E\x20\x76\x65\x6C\x6F\x63\x69\x74\x79\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x73\x20\x74\x68\x61\x74\x20\x73\x74\x61\x72\x74\x20\x61\x20\x66\x72\x61\x6D\x65\x20\x69\x6E\x73\x69\x64\x65\x20\x61\x6E\x20\x6F\x62\x6A\x65\x63\x74\x2E\x20\x4C\x6F\x77\x20\x76\x61\x6C\x75\x65\x73\x20\x65\x6E\x73\x75\x72\x65\x20\x6E\x6F\x20\x27\x65\x78\x70\x6C\x6F\x73\x69\x76\x65\x27\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x72\x65\x73\x6F\x6C\x75\x74\x69\x6F\x6E\x2E\x20\x53\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x3E\x20\x30\x20\x75\x6E\x6C\x65\x73\x73\x20\x6C\x6F\x6F\x6B\x69\x6E\x67\x20\x66\x6F\x72\x20\x6E\x6F\x6E\x2D\x70\x68\x79\x73\x69\x63\x61\x6C\x20\x65\x66\x66\x65\x63\x74\x73\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_continuousCollisionDetection(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x63\x65\x6E\x74\x61\x67\x65\x20\x6F\x66\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x20\x76\x65\x6C\x6F\x63\x69\x74\x69\x65\x73\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x63\x6F\x6E\x74\x69\x6E\x75\x6F\x75\x73\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x64\x65\x74\x65\x63\x74\x69\x6F\x6E\x2E\x20\x53\x65\x74\x20\x74\x6F\x20\x30\x20\x66\x6F\x72\x20\x70\x75\x72\x65\x6C\x79\x20\x73\x74\x61\x74\x69\x63\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x73\x2C\x20\x73\x65\x74\x20\x74\x6F\x20\x31\x20\x66\x6F\x72\x20\x70\x75\x72\x65\x20\x63\x6F\x6E\x74\x69\x6E\x75\x6F\x75\x73\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x73\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_shockPropagation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x63\x65\x6E\x74\x61\x67\x65\x20\x6F\x66\x20\x73\x68\x6F\x63\x6B\x20\x70\x72\x6F\x70\x61\x67\x61\x74\x69\x6F\x6E\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x2D\x70\x61\x72\x74\x69\x63\x6C\x65\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x73\x2E\x20\x55\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x20\x73\x74\x61\x63\x6B\x69\x6E\x67\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_surfaceCollisionIterations(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x69\x74\x65\x72\x61\x74\x69\x6F\x6E\x73\x20\x73\x70\x65\x6E\x74\x20\x6F\x6E\x20\x63\x6F\x6E\x76\x65\x78\x20\x6F\x70\x74\x69\x6D\x69\x7A\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x73\x75\x72\x66\x61\x63\x65\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x73\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 32.0f, NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_surfaceCollisionTolerance(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x72\x72\x6F\x72\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x6F\x20\x73\x74\x6F\x70\x20\x63\x6F\x6E\x76\x65\x78\x20\x6F\x70\x74\x69\x6D\x69\x7A\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x73\x75\x72\x66\x61\x63\x65\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_evaluationOrder(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x72\x64\x65\x72\x20\x69\x6E\x20\x77\x68\x69\x63\x68\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x74\x73\x20\x61\x72\x65\x20\x65\x76\x61\x6C\x75\x61\x74\x65\x64\x2E\x20\x53\x45\x51\x55\x45\x4E\x54\x49\x41\x4C\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x73\x20\x66\x61\x73\x74\x65\x72\x20\x62\x75\x74\x20\x69\x73\x20\x6E\x6F\x74\x20\x76\x65\x72\x79\x20\x73\x74\x61\x62\x6C\x65\x2E\x20\x50\x41\x52\x41\x4C\x4C\x45\x4C\x20\x69\x73\x20\x76\x65\x72\x79\x20\x73\x74\x61\x62\x6C\x65\x20\x62\x75\x74\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x73\x20\x73\x6C\x6F\x77\x6C\x79\x2C\x20\x72\x65\x71\x75\x69\x72\x69\x6E\x67\x20\x6D\x6F\x72\x65\x20\x69\x74\x65\x72\x61\x74\x69\x6F\x6E\x73\x20\x74\x6F\x20\x61\x63\x68\x69\x65\x76\x65\x20\x74\x68\x65\x20\x73\x61\x6D\x65\x20\x72\x65\x73\x75\x6C\x74\x2E"), NULL);
	}
}
static void ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_iterations(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x72\x65\x6C\x61\x78\x61\x74\x69\x6F\x6E\x20\x69\x74\x65\x72\x61\x74\x69\x6F\x6E\x73\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x62\x79\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x74\x20\x73\x6F\x6C\x76\x65\x72\x2E\x20\x41\x20\x6C\x6F\x77\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x69\x74\x65\x72\x61\x74\x69\x6F\x6E\x73\x20\x77\x69\x6C\x6C\x20\x70\x65\x72\x66\x6F\x72\x6D\x20\x62\x65\x74\x74\x65\x72\x2C\x20\x62\x75\x74\x20\x62\x65\x20\x6C\x65\x73\x73\x20\x61\x63\x63\x75\x72\x61\x74\x65\x2E"), NULL);
	}
}
static void ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_SORFactor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x76\x65\x72\x20\x28\x6F\x72\x20\x75\x6E\x64\x65\x72\x20\x69\x66\x20\x3C\x20\x31\x29\x20\x72\x65\x6C\x61\x78\x61\x74\x69\x6F\x6E\x20\x66\x61\x63\x74\x6F\x72\x20\x75\x73\x65\x64\x2E\x20\x41\x74\x20\x31\x2C\x20\x6E\x6F\x20\x6F\x76\x65\x72\x72\x65\x6C\x61\x78\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x2E\x20\x41\x74\x20\x32\x2C\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x74\x73\x20\x64\x6F\x75\x62\x6C\x65\x20\x74\x68\x65\x69\x72\x20\x72\x65\x6C\x61\x78\x61\x74\x69\x6F\x6E\x20\x72\x61\x74\x65\x2E\x20\x48\x69\x67\x68\x20\x76\x61\x6C\x75\x65\x73\x20\x72\x65\x64\x75\x63\x65\x20\x73\x74\x61\x62\x69\x6C\x69\x74\x79\x20\x62\x75\x74\x20\x69\x6D\x70\x72\x6F\x76\x65\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x6E\x63\x65\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 2.0f, NULL);
	}
}
static void ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_enabled(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x74\x68\x65\x72\x20\x74\x68\x69\x73\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x74\x20\x67\x72\x6F\x75\x70\x20\x69\x73\x20\x73\x6F\x6C\x76\x65\x64\x20\x6F\x72\x20\x6E\x6F\x74\x2E"), NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBlueprintLoaded(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBlueprintUnloaded(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnPrepareFrame(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnPrepareStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBeginStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnSubstep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnEndStep(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnInterpolate(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_ActiveParticleCount(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_solverIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_solverBatchOffsets(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_CollisionMaterial(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_SurfaceCollisions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBlueprintLoaded_m04B2952178ABFA72F0A7E7219EB78A4ECEAE156E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBlueprintLoaded_m998C17DB7FED249637AC6D40775D9D50ED1CA3F8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBlueprintUnloaded_mB1182B14609EA8CCD5A9BD7C2F3CEDDAFF78C601(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBlueprintUnloaded_mB1AB8C2B322EB4633B0CD45EF5AAF7448BAD68BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnPrepareFrame_m3014BCA91C860098AA7A396A7FFCE7AB7CA1049E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnPrepareFrame_m0D90685372167486999B646FC83D4E2C36C570AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnPrepareStep_mC586B960142DDDF81A5B1C87D393C7CA379E743E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnPrepareStep_mCB8B15D01E8DA343CF2DD8E655932C886DD863CC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBeginStep_m7A401277ED988AF4C6388B1F768B4E613BA02E10(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBeginStep_m80E4C1C9DE59FB6075BFB27E9B8D885ADC413C13(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnSubstep_m4E244BDD4F0DA8AFBEF75036632A65CF4EC1286F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnSubstep_mABFE386CEC0194553AEE608F465F311B21360592(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnEndStep_mE226631AC3F2AA3BE52CCB4AB9972A0620E76208(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnEndStep_m18354446434FA0FE622CFA9F09B7D0D98B7365F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnInterpolate_mA39BB43FF56C2FF95981971698C30856DC25ED65(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnInterpolate_mE2B57508C1595198B4E27E10DA174A0B3792AD8D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActorSolverArgs_t652D2183A88C8F5DDF27C6EEE9B4023CC9CE3725_CustomAttributesCacheGenerator_U3CsolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiActorSolverArgs_t652D2183A88C8F5DDF27C6EEE9B4023CC9CE3725_CustomAttributesCacheGenerator_ObiActorSolverArgs_get_solver_m2C65EFA043F915BB25BF5EE5F467EA1119C55BDF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiAerodynamicConstraintsBatch_tDA5413517E58B2320FF2DC31BEDA23C71B42E746_CustomAttributesCacheGenerator_aerodynamicCoeffs(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_restBends(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_bendingStiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_plasticity(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_restDarbouxVectors(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_plasticity(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_firstParticle(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_numParticles(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_lengths(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_IDs(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_IDToIndex(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_ConstraintCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_ActiveConstraintCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_InitialActiveConstraintCount(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_lambdas(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_CustomAttributesCacheGenerator_restLengths(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_pinBodies(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_colliderIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_offsets(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_restDarbouxVectors(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_breakThresholds(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinPoints(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinNormals(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinRadiiBackstop(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinCompliance(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_orientationIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_restLengths(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_restOrientations(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTetherConstraintsBatch_tB72B2385E36323C87F8EA5CC41CEF9D5A3BD826B_CustomAttributesCacheGenerator_maxLengthsScales(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTetherConstraintsBatch_tB72B2385E36323C87F8EA5CC41CEF9D5A3BD826B_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_firstTriangle(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_numTriangles(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_restVolumes(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_pressureStiffness(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiConstraints_1_t814AA25938B7022E214F5C54272335BCECBACF49_CustomAttributesCacheGenerator_batches(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_OnBlueprintGenerate(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_Empty(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_ActiveParticleCount(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_InitialActiveParticleCount(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator__bounds(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_restPositions(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_restOrientations(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_angularVelocities(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_invRotationalMasses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_filters(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x70\x68\x61\x73\x65\x73"), NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_principalRadii(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_colors(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_points(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_edges(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_triangles(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_distanceConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_bendConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_skinConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_tetherConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_stretchShearConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_bendTwistConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_shapeMatchingConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_aerodynamicConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_chainConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_volumeConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_groups(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_add_OnBlueprintGenerate_mA116B88726BA07CDAC7EA40506772CC39F8FF86E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_remove_OnBlueprintGenerate_m780AFE9B203E214D86769A37EADEE63D5FC9967E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_GetConstraints_m1C48E3155094EC159203D4F23B25015FCA6F0597(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_0_0_0_var), NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_Generate_m14C8F36BC89354F15F0F82AE9546B5024F47FF7A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_0_0_0_var), NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50__ctor_m463D73E183DDE1A5C71E6E3CC821D0E7F30E347C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m1C1B68AF4C49065E226E78E8D46FD68A406FC4DB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m603F544C865B5A382E4D45071F9EA103E35296E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m8E40706068668910D9F7D8A1EACB85F8E6FFF77A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mD7F6DAA1BCB1AB519151F38ACED86E62BB422730(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_mFFFD55BE544D6CB567D145A4DCB2564DD48B4CDF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m3284B5FE565ECE74AF7F1FBE65BCD30C41CADA8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59__ctor_mD0933DCC8582C89719A5869AA40F1D96BC46A2A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_IDisposable_Dispose_m4C2610F3D57B4EB14EE56E2E116536B5A23C8B22(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7A53073B04207358D27C5F279097C3AECBDC48E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_m326C85F4CC091D9CEBD26372E5491D58808FECB6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_mB78D5553C5C65C889A8A4E2F6FCCADA45472D734(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator_m_SourceCollider(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x72\x63\x65\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[2];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x73\x6F\x75\x72\x63\x65\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
}
static void ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator_m_DistanceField(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x64\x69\x73\x74\x61\x6E\x63\x65\x46\x69\x65\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[2];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x64\x69\x73\x74\x61\x6E\x63\x65\x46\x69\x65\x6C\x64"), NULL);
	}
}
static void ObiCollider2D_t5C38F1F5146454644E07250E0307C330A1CB4E31_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_0_0_0_var), NULL);
	}
}
static void ObiCollider2D_t5C38F1F5146454644E07250E0307C330A1CB4E31_CustomAttributesCacheGenerator_sourceCollider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[1];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x72\x63\x65\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
}
static void ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_thickness(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[1];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x63\x6B\x6E\x65\x73\x73"), NULL);
	}
}
static void ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_material(CustomAttributesCache* cache)
{
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[0];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x4D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_filter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiColliderWorld_t097AB009E3BABD5E1B19CBDA94BDBF223049CEA2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x6D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x43\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x4D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 180LL, NULL);
	}
}
static void ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator_rollingContacts(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator_rollingFriction(CustomAttributesCache* cache)
{
	{
		VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3 * tmp = (VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3 *)cache->attributes[0];
		VisibleIf__ctor_m74CCE1E688720090402D0E285F11AA7EE004E6C6(tmp, il2cpp_codegen_string_new_wrapper("\x72\x6F\x6C\x6C\x69\x6E\x67\x43\x6F\x6E\x74\x61\x63\x74\x73"), false, NULL);
	}
	{
		Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584 * tmp = (Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584 *)cache->attributes[1];
		Indent__ctor_m5A092B8D80C878888D885C6ADC0B3A9D3A90E3C7(tmp, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x66\x69\x65\x6C\x64"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x44\x69\x73\x74\x61\x6E\x63\x65\x20\x46\x69\x65\x6C\x64"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 181LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_input(CustomAttributesCache* cache)
{
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[0];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x70\x75\x74\x4D\x65\x73\x68"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_minNodeSize(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_bounds(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_nodes(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_maxError(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.00000001E-07f, 0.100000001f, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_maxDepth(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 8.0f, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_ObiDistanceField_Generate_m2FF083D72E253BE27D9DFD07C8816B449F778266(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_0_0_0_var), NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16__ctor_m926CEA2198C2B31432AD7B0C629E0026FD9135A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_IDisposable_Dispose_mE8B29A18143A848E1C4C46A4D41CE2EE66999F73(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39320DBD9BADA847C3DC1B06385734720B3ED5D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m94171808CA1CAE76B9F0511FE6570131CD857EBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mE403FD60E16DACEE30BE06E73A882BBF823618FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_tA63F9C4000CEB9B4FE78BFDCB600DD468CD3A6BD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiRigidbody_tC8F0A6971374DEE39EE5F6CCD90EBF5DD0EA2660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiRigidbody2D_t43E9E6CD4E39486D4BAA830D7F225D323241A318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiRigidbodyBase_tA95EE97A5497D66D571ABA3D52491F363471272A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ASDF_tE13A467BED85E73DEF9E1A637B2471D28DFB9665_CustomAttributesCacheGenerator_ASDF_Build_m0B3E28D5C89253E2A2FAA9545A0B748B5C69E1D4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_t9515851623053EA80E03D72E699F5A819F304CD9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3__ctor_m2AF6E46C3AF1DCE8EB0AC87345B0082720891BF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_IDisposable_Dispose_m407D5BEFA6CC050E62B83A69FC1C81FA11062FE4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FD71D70EB820213EDF7BC3853561DF86E43D30D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m026B7C1E35013639385A979664E9479DCFB7269A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_m8730288CA28692A77A479C27FB99AFB9421E825B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905____node3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905____point4(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mAE13B1EFAA791602268DFF2A5ED429691EA76178____point4(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05____node4(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05____point5(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_mED2F171342075378B5B140C14A73A72EF8ACC45F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator_m_AlignBytes(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator_ObiNativeList_1_GetEnumerator_mAA681A9160D0F5ABB78834DDCE50A090F9027FFD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_0_0_0_var), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47__ctor_m44C213C4523E721D89454E9E0DB188309DAA0C2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_IDisposable_Dispose_m3677A4910B2FBA94AB21EC467EA0F8D017654F32(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m577FD8C7518180375EAF528F1953D6A86B76129F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_IEnumerator_Reset_m59513FC87FBEC9A3F46BF8F1A27BE3F8C45CA05D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_IEnumerator_get_Current_m45C9E58B0197FD608861CCFE12CE9E08134F9D32(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiList_1_tAFBF97086278598236664F1BB4032FED42C06E1E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ObiList_1_tAFBF97086278598236664F1BB4032FED42C06E1E_CustomAttributesCacheGenerator_ObiList_1_GetEnumerator_m669DC89479304E618D4B41858753616F692A64B8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_0_0_0_var), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2__ctor_m220A0BB8B4845E3E03C861BC45721DD12E9E7C7B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_m6C1E284C049482492472BDE417FEC2D9BC5F53E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m8D4794A9AC960B97A584FB17906AFA27C95E4C0B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_mE6DC01E508903376308CBEEFDB0FD2C1990A7DEF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_m34A4E8F5E7BB811C2830337581A6877BD404D22B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ParticlePair_t3ED4B7BFA5D10A75C057CC637E2D4719B6E15A8C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_voxels(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetVoxelCenter_m3E452556E6139C67B397A378FDCBD9BA4F425089____coords0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v10(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v21(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v32(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetPointVoxel_mB53686BE0482AC0FE527505F8EC019E21CFAB6B1____point0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_VoxelExists_mBD68D5E79FB132F4304AAE5283DF58F65CDA0A4B____coords0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____bounds0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v11(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v22(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v33(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_Voxelize_mFD415E5ED30A0BF0CBC964412DBBB37930CFA97A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_0_0_0_var), NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_FloodFill_m584C8081EF5F8BC2660348F75BF0C006FBD0EC51(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_0_0_0_var), NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_IsIntersecting_m1F9C81AE3D544C1F8AA465823F7DDA7A32C7FBEC____box0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v00(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v11(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v22(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____aabbExtents3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____axis4(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void Voxel_tDC7ED04FB9683A1CAF022400103FC1FD89F6CFE3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29__ctor_m8645BD0F9E12E3B4AE36A3A9D757A0AA317666E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_IDisposable_Dispose_m68F6D58872E569B35178F3F948D25088FEE404DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCADBFC708A9E44E40767E097632FC238158C8036(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_Reset_mF260247FB427D7B3C0DAAFFD3085943EBA202B9C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_get_Current_m4ED17979436917051AE4058242D7AF43D4A9F2C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31__ctor_m70BCBE21D4D47E1482D966F7C9D35F7510603B97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_IDisposable_Dispose_m874A33578734166DA64BFC7AE8771A031A25F2C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60C01825AF1A7118B99F44611D5A9FFA941E6189(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_IEnumerator_Reset_mC4010923219A1BAD954EEAC4B73CDD945265C9A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_IEnumerator_get_Current_m7964014EF80DB8139871A160F4672E40BED0788E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PriorityQueue_1_tDBFAA8299A447FDE7A02B777CB2142412A0808C9_CustomAttributesCacheGenerator_PriorityQueue_1_GetEnumerator_mE59F3123BC2AC2349A2019082A77861F7C5E19B9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_0_0_0_var), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5__ctor_mE283398E42D28AE61C9072FB8E231BDCA9867986(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_IDisposable_Dispose_m84D4F91D281E15BADD9CF82AE81CE1F0B9175998(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m9BA3FA8D6F2F77E738345A1A1EE37171EF68A96F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerator_Reset_m01671BA954E40111F5CCF141F7B255B8F41519F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerator_get_Current_mA8032079CD99DE6D6B0A0C8AF31E894B4C7AADE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mDD221B9EC48424B9BBA073FC887B08439C91DA49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerable_GetEnumerator_m050DABA4F326191228EDB111C0B34B0E6DE60B0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA_CustomAttributesCacheGenerator_VoxelDistanceField_JumpFlood_m8A165DB8B897C98F4DD564943140A9C71408E1F6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_0_0_0_var), NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5__ctor_m9A42F34DE8C7C452C8A53F17D6D572BC2453C654(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_IDisposable_Dispose_m0A919D1AE0822BB1475E5FE78DA4060A236911E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF414A3DC08D65DC419082B4635288E1F7E02FC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_get_Current_m0850F7B63FF4B6EB05FB8A60525AB3BC48509D18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_AStar_mA838A6AEC2878DE3B68FB035FCB31C5ADE9958EC____start0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_FindClosestNonEmptyVoxel_m108974C29C2DE5DCB4E8092382271E90E1306714____start0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_FindPath_m12E0CEA61718E42317477C9B7A3825B9EAA590E5____start0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_U3CFindClosestNonEmptyVoxelU3Eb__6_0_mBBEC0743F2CB9A04F014A64C652D3572607A4FDC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiDistanceFieldRenderer_tF6263B78F384FE44C40CF3D3AAB28EF4C6802E49_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x44\x69\x73\x74\x61\x6E\x63\x65\x20\x46\x69\x65\x6C\x64\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 1003LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_0_0_0_var), NULL);
	}
}
static void ObiDistanceFieldRenderer_tF6263B78F384FE44C40CF3D3AAB28EF4C6802E49_CustomAttributesCacheGenerator_slice(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiInstancedParticleRenderer_t5F00A7A1B77C575661533F2B2DAA2E1DD63BD4DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x49\x6E\x73\x74\x61\x6E\x63\x65\x64\x20\x50\x61\x72\x74\x69\x63\x6C\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 1001LL, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiParticleRenderer_tF781C9AA9C544382CE049D22AD3B45EBA7A30AFB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x50\x61\x72\x74\x69\x63\x6C\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 1000LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x53\x6F\x6C\x76\x65\x72"), 800LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[2];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnCollision(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnParticleCollision(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnUpdateParameters(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnPrepareFrame(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnPrepareStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnBeginStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnSubstep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnEndStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnInterpolate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_simulateWhenInvisible(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x77\x69\x6C\x6C\x20\x66\x6F\x72\x63\x65\x20\x74\x68\x65\x20\x73\x6F\x6C\x76\x65\x72\x20\x74\x6F\x20\x6B\x65\x65\x70\x20\x73\x69\x6D\x75\x6C\x61\x74\x69\x6E\x67\x20\x65\x76\x65\x6E\x20\x77\x68\x65\x6E\x20\x6E\x6F\x74\x20\x76\x69\x73\x69\x62\x6C\x65\x20\x66\x72\x6F\x6D\x20\x61\x6E\x79\x20\x63\x61\x6D\x65\x72\x61\x2E"), NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_m_Backend(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_worldLinearInertiaScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_worldAngularInertiaScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_actors(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_m_ParticleToActor(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtyActiveParticles(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtySimplices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtyConstraints(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnCollision_mEB324EABAE3F36C43F055950F52827839851DAF5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnCollision_mA07AA8AA273F50BC4DDAFD0523A7CA23C628D674(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnParticleCollision_m73A739A0544F098FE805CEF2C6ED3E9528060A52(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnParticleCollision_m153C65501BB104D19DFB0BC50CE655A20BB725B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnUpdateParameters_mDCE3C3436FCB6E239FC3C229799E1CA65048798B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnUpdateParameters_m1D7467B121B4968CCB10AE69A8FCE3F601A622E1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnPrepareFrame_mF8245DFAA6E404FFD6135985C1B90BAD5A827B69(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnPrepareFrame_mFF9D302946D3647FF7267652E21CCDA784AC68E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnPrepareStep_m22DB6F7A692B42DCB0AF33EDDFAF38062A4AB42E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnPrepareStep_m5D2D4E8CACADB7938868BE6F29546BA497A05250(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnBeginStep_mE11B9F3927E6908AC1C8CA09888FAC4145B60F72(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnBeginStep_m828F85A9556BCF73D5434B9A689918DEF5F5D39D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnSubstep_mF4D4FE1ED9E867E6F111B8B082E377D950607C6E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnSubstep_m60FD7FE897C29FEF9EE0EDC9303C62CFF70D2B33(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnEndStep_m85BFAD019C29970E500BB5079DA70527A4EB324D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnEndStep_m53846A7D3E4D2113D49EAA4EE56FAE9752CB6CA3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnInterpolate_mBADEB5D974C090F97884D9FF885C017D0C36CF7D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnInterpolate_m5F9AAE96B45E17989172907EECA1966282A87626(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiFixedUpdater_t4D2492FAB85AE92AD8329DC2DA70CD416EC235D1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x46\x69\x78\x65\x64\x20\x55\x70\x64\x61\x74\x65\x72"), 801LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiFixedUpdater_t4D2492FAB85AE92AD8329DC2DA70CD416EC235D1_CustomAttributesCacheGenerator_substeps(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x70\x65\x72\x20\x46\x69\x78\x65\x64\x55\x70\x64\x61\x74\x65\x2E\x20\x49\x6E\x63\x72\x65\x61\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x67\x72\x65\x61\x74\x6C\x79\x20\x69\x6D\x70\x72\x6F\x76\x65\x73\x20\x61\x63\x63\x75\x72\x61\x63\x79\x20\x61\x6E\x64\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x6E\x63\x65\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
}
static void ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x4C\x61\x74\x65\x20\x46\x69\x78\x65\x64\x20\x55\x70\x64\x61\x74\x65\x72"), 802LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator_substeps(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x70\x65\x72\x20\x46\x69\x78\x65\x64\x55\x70\x64\x61\x74\x65\x2E\x20\x49\x6E\x63\x72\x65\x61\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x67\x72\x65\x61\x74\x6C\x79\x20\x69\x6D\x70\x72\x6F\x76\x65\x73\x20\x61\x63\x63\x75\x72\x61\x63\x79\x20\x61\x6E\x64\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x6E\x63\x65\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
}
static void ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator_ObiLateFixedUpdater_RunLateFixedUpdate_mA207589FB295229010BC3ED39CA0A33B465AE4AB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_0_0_0_var), NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__7__ctor_m26D083432D24A29A1601BBEED04C921E5E757A62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__7_System_IDisposable_Dispose_m28B4BB85E97B45021B22C15FEF3D8E93B5BF81C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD948A54767F3CE844E780E8D04C37F8B9F865A05(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__7_System_Collections_IEnumerator_Reset_mBC0D3D496D3790C6970DE839BF6F1DEE46B04FB3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__7_System_Collections_IEnumerator_get_Current_m200572F643F2FEB367A9BF53A37578AAAE32802F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x4C\x61\x74\x65\x20\x55\x70\x64\x61\x74\x65\x72"), 802LL, NULL);
	}
}
static void ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_deltaSmoothing(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x66\x61\x63\x74\x6F\x72\x20\x66\x6F\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x73\x74\x65\x70\x20\x28\x73\x6D\x6F\x6F\x74\x68\x44\x65\x6C\x74\x61\x29\x2E\x20\x56\x61\x6C\x75\x65\x73\x20\x63\x6C\x6F\x73\x65\x72\x20\x74\x6F\x20\x31\x20\x77\x69\x6C\x6C\x20\x79\x69\x65\x6C\x64\x20\x73\x74\x61\x62\x6C\x65\x72\x20\x73\x69\x6D\x75\x6C\x61\x74\x69\x6F\x6E\x2C\x20\x62\x75\x74\x20\x69\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x6F\x66\x66\x2D\x73\x79\x6E\x63\x20\x77\x69\x74\x68\x20\x72\x65\x6E\x64\x65\x72\x69\x6E\x67\x2E"), NULL);
	}
}
static void ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_smoothDelta(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74\x20\x74\x69\x6D\x65\x73\x74\x65\x70\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x64\x76\x61\x6E\x63\x65\x20\x74\x68\x65\x20\x73\x69\x6D\x75\x6C\x61\x74\x69\x6F\x6E\x2E\x20\x54\x68\x65\x20\x75\x70\x64\x61\x74\x65\x72\x20\x77\x69\x6C\x6C\x20\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x65\x20\x74\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x77\x69\x74\x68\x20\x54\x69\x6D\x65\x2E\x64\x65\x6C\x74\x61\x54\x69\x6D\x65\x20\x74\x6F\x20\x66\x69\x6E\x64\x20\x74\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x74\x69\x6D\x65\x73\x74\x65\x70\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x65\x61\x63\x68\x20\x66\x72\x61\x6D\x65\x2E"), NULL);
	}
}
static void ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_substeps(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x70\x65\x72\x20\x46\x69\x78\x65\x64\x55\x70\x64\x61\x74\x65\x2E\x20\x49\x6E\x63\x72\x65\x61\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x67\x72\x65\x61\x74\x6C\x79\x20\x69\x6D\x70\x72\x6F\x76\x65\x73\x20\x61\x63\x63\x75\x72\x61\x63\x79\x20\x61\x6E\x64\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x6E\x63\x65\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
}
static void ObiUpdater_tEDDEEBBDCA484C0905FCEEF73A513392DA1245B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ChildrenOnly_t27661D32C6D07127A678EA62748D58AEA19F47F7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void InspectorButtonAttribute_t361DBAA21C1410ED19C67A38FDA4182A3BF3F36C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void MinMaxAttribute_t95F00C66854692E74D4564051A2C5B744E864D74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void MultiDelayed_tD671996D67E3E5A430A576B2B670D47D1D4EAD39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void MultiPropertyAttribute_tD831A4A240690E32C80AEF58B3E877919F8BB38E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void MultiRange_t05AA64C7BC22CB3BFD931E9DE77DFC5C08DC8C2D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_U3CPropertyNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_SerializeProperty_get_PropertyName_mDC8A03791EF5C0FC8DB9BEE2645D05EC456A9B21(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_SerializeProperty_set_PropertyName_m0D097B39C9F982C6DA9CFE395012ED31AE075A55(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_U3CMethodNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_U3CNegateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_get_MethodName_mA9D547283CA5003031A6052EC2F01BDD20626C79(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_set_MethodName_mEEE7DE756E758BB291ECC2AF95FB9331577A89BD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_get_Negate_m70429C0B2F6E15B7329A21127ED7F095B5B9F79C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_set_Negate_m7AE9FD0E091984A71A593C3FE0A0A4CB89CDDF7E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CoroutineJob_t80DB8BDF56DB40B9329335C29878C9D46D8C879F_CustomAttributesCacheGenerator_CoroutineJob_Start_m80C8E17CD749B13B237150583A6280C213F8C39F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15__ctor_mD7A6880AAEB86FECE339EAA24C276AA5D91FE5AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_IDisposable_Dispose_m4542E430FBE633F30A5F1B001DA8C7D01BE636F1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m857DB02265865988D8A060D28C87EAB01F6DEC55(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_mC889240009E6BB1F36B3BC74F618A35A604608B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mE937748101E5BB647A3762B97A9DE2FED38D8283(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiContactEventDispatcher_tE03476E199D615BC2771BEBCF78A8D89F74DB738_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var), NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x50\x61\x72\x74\x69\x63\x6C\x65\x20\x41\x74\x74\x61\x63\x68\x6D\x65\x6E\x74"), 820LL, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Actor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_ParticleGroup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_AttachmentType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_ConstrainOrientation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Compliance(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_BreakThreshold(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		DelayedAttribute_t84E6423E615599EDDEAA39BC245C5B871A2D9948 * tmp = (DelayedAttribute_t84E6423E615599EDDEAA39BC245C5B871A2D9948 *)cache->attributes[2];
		DelayedAttribute__ctor_mA73AFD50BA612774A729641927483BC80142A11A(tmp, NULL);
	}
}
static void ObiParticleDragger_tB8EA35A74E226E0A73483C34703F508483E93095_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiParticlePicker_tB0405EAD1BFCF9EBDD82F494C3DE49EBECD0F732_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiParticlePicker_tB0405EAD1BFCF9EBDD82F494C3DE49EBECD0F732_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var), NULL);
	}
}
static void ObiParticleGridDebugger_tE80EC21184C40EAE8D508C93B9EEFB00D69B0BB6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var), NULL);
	}
}
static void ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator_skin(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x70\x70\x65\x61\x72\x61\x6E\x63\x65"), NULL);
	}
}
static void ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator_showPercentages(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x56\x69\x73\x75\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_stitches(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_actor1(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_actor2(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_lambdas(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Swap_mF56D360C3B427C99DB48F907D7B8F1F2D96B2474(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Swap_m4545B5452717048909109CF73C8CAC6880F7F227(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ShiftLeft_m983E32714E08933EE70C58FD314AB2F11C10DB5D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ShiftRight_m0CCEF49D7BEEB3D21A740CEEF10750BFA3E10ED5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_AreValid_m36409B4F74E4DA83A742954433F6CA4EE168BAE6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Transform_m733A8AC89C2BFBC7A0E0089512FFC4709454DE88(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Remap_m8F506FF64E3B9168DE2B0235E274CC1033B7D86F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Add_mC0C822C7DBBF1BA01EE882208292D519B2C9C2D4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_FrobeniusNorm_m864D4DA65931429E717193CB634FFAAF31C5DCF4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ScalarMultiply_mE25A02716B8B63129015ACA756D1DB1AD469DE51(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_PureSign_m92E94D198EF88DC390CCBA619718D6A6C27BA5C5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p10(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p21(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p32(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BilateralInterleaved_m17A79FDAFD53A3C8E7CB5403AC539CD1BC4D5453(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_0_0_0_var), NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____A0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____B1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____C2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____P3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p10(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p21(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p32(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____coords3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_unitOrthogonal_m08E2004A970D966D5227E325780C23E6FAB28529(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_GetPointCloudAnisotropy_m3D48980B4381A7B54F37C063FE47EF7C737CA9A2____hint_normal3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_IDisposable_Dispose_m8831F3EF2CA071AF13BD9D630BBA7F87306BD89C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9E1918A629F5BD2E500DBD47B57BEA70DA7126(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_get_Current_m82E938AAC7D70CD2629E214DDAC1F71A6BB51474(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mBF01BD41B11190F2A9C48BE6A1A9FB04EF151CF6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SetCategory_t5394A96DA7AC622E8E85A161EA2AA22C1D08CDC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x42\x6F\x6E\x65"), 882LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[2];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator_m_SelfCollisions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__mass(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__rotationalMass(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__skinConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__skinCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__skinRadius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__stretchShearConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__stretchCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__shear1Compliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__shear2Compliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__bendTwistConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__torsionCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__bend1Compliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__bend2Compliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__plasticYield(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__plasticCreep(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator_filter(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6C\x74\x65\x72\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x64\x65\x74\x65\x63\x74\x69\x6F\x6E\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BonePropertyCurve_t9D1823487E8E274DE6EE0BF366B805ADE1CD6FC1_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x52\x6F\x64"), 881LL, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator_m_RodBlueprint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__stretchShearConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__stretchCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__shear1Compliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__shear2Compliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__bendTwistConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__torsionCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__bend1Compliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__bend2Compliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__plasticYield(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 0.100000001f, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__plasticCreep(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__chainConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__tightness(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x52\x6F\x70\x65"), 880LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[2];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator_m_RopeBlueprint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__distanceConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__stretchingScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__stretchCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__maxCompression(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__bendConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__bendCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__maxBending(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 0.5f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__plasticYield(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 0.100000001f, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__plasticCreep(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator_OnRopeTorn(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator_ObiRope_add_OnRopeTorn_m82818AFDC4E5B53B9BDB3BC5F448DC5819AEAF17(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator_ObiRope_remove_OnRopeTorn_mDCCBD9E8FEAA87E7DB5622737423840AD4EE1A5F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_m_SelfCollisions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_restLength_(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_elements(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_OnElementsGenerated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_ObiRopeBase_add_OnElementsGenerated_mD9B02AE3DC85CC5762C7AD9F125360E17E1566C8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_ObiRopeBase_remove_OnElementsGenerated_m628F81AC8C51256C7D4EA46CAC6F983413DB27A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiRopeCursor_t3A3DC1CEC7DA311FC3F7D0FDAE771A06BCFCAFB4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x52\x6F\x70\x65\x20\x43\x75\x72\x73\x6F\x72"), 883LL, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_0_0_0_var), NULL);
	}
}
static void ObiRopeCursor_t3A3DC1CEC7DA311FC3F7D0FDAE771A06BCFCAFB4_CustomAttributesCacheGenerator_m_CursorMu(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiRopeCursor_t3A3DC1CEC7DA311FC3F7D0FDAE771A06BCFCAFB4_CustomAttributesCacheGenerator_m_SourceMu(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x62\x6F\x6E\x65\x20\x62\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x42\x6F\x6E\x65\x20\x42\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 142LL, NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_transforms(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_restTransformOrientations(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_parentIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_normalizedLengths(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_ignored(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_mass(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_rotationalMass(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_ObiBoneBlueprint_Initialize_m2A9787CD6FC33DBD5B198E7D667BE12DE690C382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_0_0_0_var), NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_ObiBoneBlueprint_CreateStretchShearConstraints_m20DA40DA29C69B617A52422B5084F44019F99604(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_0_0_0_var), NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_ObiBoneBlueprint_CreateBendTwistConstraints_m61BD7C27BF0655EA303B4E58C0B98D04B424D6B5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_0_0_0_var), NULL);
	}
}
static void ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_ObiBoneBlueprint_CreateSkinConstraints_mE42F073A01A3AD9EAD21A01B6FB490F178E695CB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_0_0_0_var), NULL);
	}
}
static void U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__14__ctor_m27140A7DF8EF3E999F1FC723535A04C964525F82(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__14_System_IDisposable_Dispose_m88FFDD6CFBB11319DDBBB0F8B34089B80C4F0F01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC52279203D514C26549053545D8AF9B41AEB60F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__14_System_Collections_IEnumerator_Reset_m9EC41DFD1D0FFE3D465A64CEE388F88EA13B2D0B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__14_System_Collections_IEnumerator_get_Current_m5A676E16757268FF757D0DC38F8AD1E8EDF2EEA4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__16__ctor_mC9883EA29286F581B5BE6546F53E1803A36FC572(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__16_System_IDisposable_Dispose_m6F122F292C0A6014F0DFBAAD6E22DFEFEF7F74F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C73E4EA2DE56F4924F1F4D440D8DEBA59271AD2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_IEnumerator_Reset_m622BC462DD1E9B4018F1F51A846510E62CECDA84(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_IEnumerator_get_Current_m4A178A0000B1F11C4A79E52395DDC3CAB3B647C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__17__ctor_m97129288678462023812E6355E3F1BEB10871686(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__17_System_IDisposable_Dispose_m7FA46F1AE20471FF759E6AD807D8CFD6E46DFD2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB14225394B51F4DE1158DFBBFD03146485847A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_IEnumerator_Reset_mE9692B1811A0104396D417403B04677B7C8903DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_IEnumerator_get_Current_mACB8F571A51435DCA534D455C05DEC2BCBD56BBA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__18__ctor_m01559B1A25470AEFB06D7A19008967898DB8A6FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__18_System_IDisposable_Dispose_m13FC3CA635500255354816726C2A0334D420AAA0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m025792FC9DCA9844F1948AAF26303D5DC3941422(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__18_System_Collections_IEnumerator_Reset_m989EDC08F01688A58194BA6518DE7A3D9F10934A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__18_System_Collections_IEnumerator_get_Current_mC414552ED2BBDCAA4A581F8CA9BC1CCAC78D9B7F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x72\x6F\x64\x20\x62\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x52\x6F\x64\x20\x42\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 141LL, NULL);
	}
}
static void ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597_CustomAttributesCacheGenerator_ObiRodBlueprint_Initialize_mB0D5422A4133FF08FC0F52DA417299A628B98B26(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_0_0_0_var), NULL);
	}
}
static void ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597_CustomAttributesCacheGenerator_ObiRodBlueprint_CreateStretchShearConstraints_m9413EBB5F95ABFD44A15EDF6DA9CC67830A98C6A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_0_0_0_var), NULL);
	}
}
static void ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597_CustomAttributesCacheGenerator_ObiRodBlueprint_CreateBendTwistConstraints_m5F1A04D6C343F07E764A8641AA54EF2EEB2162E8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_0_0_0_var), NULL);
	}
}
static void ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597_CustomAttributesCacheGenerator_ObiRodBlueprint_CreateChainConstraints_m665ECE23FA005CB35338951024B674EB2172B5EF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_0_0_0_var), NULL);
	}
}
static void U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator_U3CInitializeU3Ed__3__ctor_m41BDF4E1CF1181A4B67AA1298FF91C848705149D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator_U3CInitializeU3Ed__3_System_IDisposable_Dispose_mAAB9B58959E17D6316D0376867E1ADEA76D9F1EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator_U3CInitializeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD199EE52973224E0C633E71C904164111168B9B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator_U3CInitializeU3Ed__3_System_Collections_IEnumerator_Reset_m70CE6B4088FBF17D28720288E2F251679B22BD11(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator_U3CInitializeU3Ed__3_System_Collections_IEnumerator_get_Current_m6B81FD82D37E5ED58B0B639232283B9902ED448B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__4__ctor_mDFFBA8618ED30102037B3C56180A9BEA9487C330(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__4_System_IDisposable_Dispose_m89DF329DFEB0C57A2E9159B524D238E75D784621(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48490361DF35F3911FA88CC9E8488B9976632C81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m3EC8612B472DCDC0A5BA07CF265742F7ADB074CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_mDF13C68A7A9199168F5434C5FDAE03DECED69AD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__5__ctor_m74EC6A8D57860822339D5CCF9A93DA20335836FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__5_System_IDisposable_Dispose_mCD67BAFE6581BBC3F6B167D165F996E217FA17A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE73B33CF3970D7E2EC7E22CC25D72B5F318EEBD0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_IEnumerator_Reset_m334DE000C0E71E3904B0922ACE92B45250B40DAF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_IEnumerator_get_Current_mDAD52D5C97FC4B1965C20CF8C10650EF73423A09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator_U3CCreateChainConstraintsU3Ed__6__ctor_mCCD0039AE63FA7C9AF769CB6DA517174CFE80C80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator_U3CCreateChainConstraintsU3Ed__6_System_IDisposable_Dispose_m8D90A9C0F15B87BFEE0F460C6F3DA96A7595273C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator_U3CCreateChainConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m15E671F86EEE570870E5A45BF66B72D282E1CBD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator_U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_m394694E12A8EAEEB4EA80649BEA9BBE861C306FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator_U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_mC6B36F52F2460C2F7E972D940E2F69B588832FED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x72\x6F\x70\x65\x20\x62\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x52\x6F\x70\x65\x20\x42\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 140LL, NULL);
	}
}
static void ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B_CustomAttributesCacheGenerator_ObiRopeBlueprint_Initialize_m253C93BBFF0249E6F38C1B014E0FC4D6B0263F30(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_0_0_0_var), NULL);
	}
}
static void ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B_CustomAttributesCacheGenerator_ObiRopeBlueprint_CreateDistanceConstraints_m0179DE68BDE1A682413705118D3034FE4070A492(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_0_0_0_var), NULL);
	}
}
static void ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B_CustomAttributesCacheGenerator_ObiRopeBlueprint_CreateBendingConstraints_m38CAD4B112A942FDE1242E78797329EBF7F0F468(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_0_0_0_var), NULL);
	}
}
static void U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2__ctor_mE9943407AC4BCDD21A5F1BB4759F285BAE447D8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_IDisposable_Dispose_m97A6F5247B0287130D582F2BB4E544062A689B13(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAF2CC89A62774D134AF088ADE0119C979EB1F72(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m7A4B24373876BDE2453048631F2B963E386E7D12(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m16BDC9B0E8C7DB5818965339EC2F1EFEFEF37DD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3__ctor_mC2881BA9A2C3B5FFD584032842FC4D8016768534(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_m1CE3F312F7B66A397DE190A3983DA6CB907431EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74A1152B6135923B3BAACBAC1F2F5932512D8046(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mD6AC7761E802DE2F6F0CADB0A5682051E6E4669C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8EF851B496CA7FD1CC7F31747EEF8484A7FCBB15(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__4__ctor_mF09A262B24850675B97784C745083E957AC70DDB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__4_System_IDisposable_Dispose_m1ACE661B7DC059891776A5AD8632DD995321EBA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD5D1A5BD31642785B698F9B9734EBE8775F45FA6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m8B106EC62D22296800CE67482EE3F81810C06907(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m9475B499E2444C3687CB7A86ADA02EFA9627693A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_path(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_resolution(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_m_InterParticleDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_totalParticles(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_m_RestLength(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_restLengths(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_ObiRopeBlueprintBase_Initialize_m49A4560B16A115658987C79F148B7D9B9983A5A9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_0_0_0_var), NULL);
	}
}
static void U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator_U3CInitializeU3Ed__17__ctor_m52F67707D008A9F9B010B0538ECEE7A5CC69F2EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator_U3CInitializeU3Ed__17_System_IDisposable_Dispose_mF0B5E8B1440934FC4C19F4F949EACFE581448839(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator_U3CInitializeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DD4C0990CD4B35386F56E79FF03AA864B1D6C21(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator_U3CInitializeU3Ed__17_System_Collections_IEnumerator_Reset_m28AA9E89D0D2B4BD55BE44CC152105CAB3FE46A7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator_U3CInitializeU3Ed__17_System_Collections_IEnumerator_get_Current_mC609C587F1D824ADC1D1CD66C9D51AB95F222BAA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiRopeSection_tB462F344FF03C60586C93F498EDFB4E8F8E32D34_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x72\x6F\x70\x65\x20\x73\x65\x63\x74\x69\x6F\x6E"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x52\x6F\x70\x65\x20\x53\x65\x63\x74\x69\x6F\x6E"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 142LL, NULL);
	}
}
static void ObiRopeSection_tB462F344FF03C60586C93F498EDFB4E8F8E32D34_CustomAttributesCacheGenerator_vertices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPathDataChannel_2_t318D0482FA88FB984537DA754769D160EBB6B558_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Names(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Points(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Normals(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Colors(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Thickness(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Masses(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_RotationalMasses(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Filters(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x5F\x50\x68\x61\x73\x65\x73"), NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Closed(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_ArcLengthTable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_TotalSplineLenght(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_ObiPath_GetDataChannels_m971B8134825853C6010D8D02446AB4EEE35CE5C1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_0_0_0_var), NULL);
	}
}
static void U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17__ctor_mD0F362BFD59ACE15D4D3A884E8226BF10577B2B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_IDisposable_Dispose_m1491BD58F339286AD9852B89147455FFADE4FB35(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_Collections_Generic_IEnumeratorU3CObi_IObiPathDataChannelU3E_get_Current_mA4EFA4B8901E742332F05D5737EA190D71393707(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerator_Reset_mEBF818027E97B5B63CBDDC30BE2E6B48F896B7DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerator_get_Current_m88CC0E7304DD9F211246F7A845B040ADB99D4662(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_Collections_Generic_IEnumerableU3CObi_IObiPathDataChannelU3E_GetEnumerator_m796DAF5CF8553C1133063B7487009CBB8F6E31B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerable_GetEnumerator_mD094D18AC457EA20B62A9CCF2837713884B4A7A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_decimation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x75\x72\x76\x61\x74\x75\x72\x65\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x62\x65\x6C\x6F\x77\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x70\x61\x74\x68\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x65\x63\x69\x6D\x61\x74\x65\x64\x2E\x20\x41\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x30\x20\x77\x6F\x6E\x27\x74\x20\x61\x70\x70\x6C\x79\x20\x61\x6E\x79\x20\x64\x65\x63\x69\x6D\x61\x74\x69\x6F\x6E\x2E\x20\x41\x73\x20\x79\x6F\x75\x20\x69\x6E\x63\x72\x65\x61\x73\x65\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x2C\x20\x64\x65\x63\x69\x6D\x61\x74\x69\x6F\x6E\x20\x77\x69\x6C\x6C\x20\x62\x65\x63\x6F\x6D\x65\x20\x6D\x6F\x72\x65\x20\x61\x67\x67\x72\x65\x73\x69\x76\x65\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_smoothing(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x69\x74\x65\x72\x61\x74\x69\x6F\x6E\x73\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x70\x61\x74\x68\x2E\x20\x41\x20\x73\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x30\x20\x77\x6F\x6E\x27\x74\x20\x70\x65\x72\x66\x6F\x72\x6D\x20\x61\x6E\x79\x20\x73\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x61\x74\x20\x61\x6C\x6C\x2E\x20\x4E\x6F\x74\x65\x20\x74\x68\x61\x74\x20\x73\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x69\x73\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x61\x66\x74\x65\x72\x20\x64\x65\x63\x69\x6D\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
}
static void ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_twist(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x77\x69\x73\x74\x20\x69\x6E\x20\x64\x65\x67\x72\x65\x65\x73\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x65\x61\x63\x68\x20\x73\x75\x63\x65\x73\x73\x69\x76\x65\x20\x70\x61\x74\x68\x20\x73\x65\x63\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_OnCurveGenerated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_rawChunks(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_smoothChunks(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_ObiPathSmoother_add_OnCurveGenerated_m1612F15B48776929153BBCB1C62F5B1720320755(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_ObiPathSmoother_remove_OnCurveGenerated_m0136223598B7485378680ECE56861613BAE13476(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiRopeChainRenderer_t329C0DFFCB68B733AB1F215E95048DF74CBAA696_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x52\x6F\x70\x65\x20\x43\x68\x61\x69\x6E\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 885LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiRopeChainRenderer_t329C0DFFCB68B733AB1F215E95048DF74CBAA696_CustomAttributesCacheGenerator_linkInstances(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeChainRenderer_t329C0DFFCB68B733AB1F215E95048DF74CBAA696_CustomAttributesCacheGenerator_randomizeLinks(CustomAttributesCache* cache)
{
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[0];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x69\x7A\x65\x4C\x69\x6E\x6B\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRopeChainRenderer_t329C0DFFCB68B733AB1F215E95048DF74CBAA696_CustomAttributesCacheGenerator_twistAnchor(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiRopeExtrudedRenderer_t703051713A9D76924137234732D3F1DE1A9C8E0C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[3];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x52\x6F\x70\x65\x20\x45\x78\x74\x72\x75\x64\x65\x64\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 883LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[4];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiRopeExtrudedRenderer_t703051713A9D76924137234732D3F1DE1A9C8E0C_CustomAttributesCacheGenerator_extrudedMesh(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeExtrudedRenderer_t703051713A9D76924137234732D3F1DE1A9C8E0C_CustomAttributesCacheGenerator_uvAnchor(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiRopeLineRenderer_t39E7F2D3A153DAA2F4D7260C39BCB85E20D81A60_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x52\x6F\x70\x65\x20\x4C\x69\x6E\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 884LL, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[3];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[4];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
}
static void ObiRopeLineRenderer_t39E7F2D3A153DAA2F4D7260C39BCB85E20D81A60_CustomAttributesCacheGenerator_lineMesh(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeLineRenderer_t39E7F2D3A153DAA2F4D7260C39BCB85E20D81A60_CustomAttributesCacheGenerator_uvAnchor(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiRopeLineRenderer_t39E7F2D3A153DAA2F4D7260C39BCB85E20D81A60_CustomAttributesCacheGenerator_ObiRopeLineRenderer_U3COnEnableU3Eb__15_0_m6793DE39FBF2171A3AF9F73DC1442DCC302C8694(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[3];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[4];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x52\x6F\x70\x65\x20\x4D\x65\x73\x68\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 886LL, NULL);
	}
}
static void ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_mesh(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[1];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x72\x63\x65\x4D\x65\x73\x68"), NULL);
	}
}
static void ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_axis(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[1];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x77\x65\x65\x70\x41\x78\x69\x73"), NULL);
	}
}
static void ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_instances(CustomAttributesCache* cache)
{
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[0];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x73\x74\x61\x6E\x63\x65\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_instanceSpacing(CustomAttributesCache* cache)
{
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[0];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x73\x74\x61\x6E\x63\x65\x53\x70\x61\x63\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_meshSizeAlongAxis(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_deformedMesh(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiRopeAttach_t2EED0099FD22C1D89C5290D4D07160CC783C5B58_CustomAttributesCacheGenerator_m(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiRopePrefabPlugger_t377F2388AED024B2636EBA5333D4110D459BDB55_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_0_0_0_var), NULL);
	}
}
static void ObiRopeReel_t8479AF7D982E5FCD67E57C3032EA222552DF7BD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiRopeCursor_t3A3DC1CEC7DA311FC3F7D0FDAE771A06BCFCAFB4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiRopeCursor_t3A3DC1CEC7DA311FC3F7D0FDAE771A06BCFCAFB4_0_0_0_var), NULL);
	}
}
static void ObiRopeReel_t8479AF7D982E5FCD67E57C3032EA222552DF7BD8_CustomAttributesCacheGenerator_outThreshold(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x6C\x6C\x20\x6F\x75\x74\x2F\x69\x6E\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x73"), NULL);
	}
}
static void ObiRopeReel_t8479AF7D982E5FCD67E57C3032EA222552DF7BD8_CustomAttributesCacheGenerator_outSpeed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x6C\x6C\x20\x6F\x75\x74\x2F\x69\x6E\x20\x73\x70\x65\x65\x64\x73"), NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_tA5C757109DE911EDFAAE6282BDA413CB8C7D1E83_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Obi_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Obi_AttributeGenerators[612] = 
{
	ObiContactGrabber_t14D0338C4C370DF79A94FA1DE76A34470B4DF04A_CustomAttributesCacheGenerator,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator,
	ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator,
	ObiCollider2D_t5C38F1F5146454644E07250E0307C330A1CB4E31_CustomAttributesCacheGenerator,
	ObiColliderWorld_t097AB009E3BABD5E1B19CBDA94BDBF223049CEA2_CustomAttributesCacheGenerator,
	ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator,
	U3CU3Ec_tA63F9C4000CEB9B4FE78BFDCB600DD468CD3A6BD_CustomAttributesCacheGenerator,
	ObiRigidbody_tC8F0A6971374DEE39EE5F6CCD90EBF5DD0EA2660_CustomAttributesCacheGenerator,
	ObiRigidbody2D_t43E9E6CD4E39486D4BAA830D7F225D323241A318_CustomAttributesCacheGenerator,
	ObiRigidbodyBase_tA95EE97A5497D66D571ABA3D52491F363471272A_CustomAttributesCacheGenerator,
	U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_CustomAttributesCacheGenerator,
	U3CU3Ec_t9515851623053EA80E03D72E699F5A819F304CD9_CustomAttributesCacheGenerator,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator,
	ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator,
	ObiList_1_tAFBF97086278598236664F1BB4032FED42C06E1E_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator,
	ParticlePair_t3ED4B7BFA5D10A75C057CC637E2D4719B6E15A8C_CustomAttributesCacheGenerator,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator,
	Voxel_tDC7ED04FB9683A1CAF022400103FC1FD89F6CFE3_CustomAttributesCacheGenerator,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator,
	U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E_CustomAttributesCacheGenerator,
	ObiDistanceFieldRenderer_tF6263B78F384FE44C40CF3D3AAB28EF4C6802E49_CustomAttributesCacheGenerator,
	ObiInstancedParticleRenderer_t5F00A7A1B77C575661533F2B2DAA2E1DD63BD4DD_CustomAttributesCacheGenerator,
	ObiParticleRenderer_tF781C9AA9C544382CE049D22AD3B45EBA7A30AFB_CustomAttributesCacheGenerator,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator,
	U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_CustomAttributesCacheGenerator,
	ObiFixedUpdater_t4D2492FAB85AE92AD8329DC2DA70CD416EC235D1_CustomAttributesCacheGenerator,
	ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator,
	U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator,
	ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator,
	ObiUpdater_tEDDEEBBDCA484C0905FCEEF73A513392DA1245B1_CustomAttributesCacheGenerator,
	ChildrenOnly_t27661D32C6D07127A678EA62748D58AEA19F47F7_CustomAttributesCacheGenerator,
	Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584_CustomAttributesCacheGenerator,
	InspectorButtonAttribute_t361DBAA21C1410ED19C67A38FDA4182A3BF3F36C_CustomAttributesCacheGenerator,
	MinMaxAttribute_t95F00C66854692E74D4564051A2C5B744E864D74_CustomAttributesCacheGenerator,
	MultiDelayed_tD671996D67E3E5A430A576B2B670D47D1D4EAD39_CustomAttributesCacheGenerator,
	MultiPropertyAttribute_tD831A4A240690E32C80AEF58B3E877919F8BB38E_CustomAttributesCacheGenerator,
	MultiRange_t05AA64C7BC22CB3BFD931E9DE77DFC5C08DC8C2D_CustomAttributesCacheGenerator,
	SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator,
	ObiContactEventDispatcher_tE03476E199D615BC2771BEBCF78A8D89F74DB738_CustomAttributesCacheGenerator,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator,
	ObiParticleDragger_tB8EA35A74E226E0A73483C34703F508483E93095_CustomAttributesCacheGenerator,
	ObiParticleGridDebugger_tE80EC21184C40EAE8D508C93B9EEFB00D69B0BB6_CustomAttributesCacheGenerator,
	ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator,
	ParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F_CustomAttributesCacheGenerator,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator,
	SetCategory_t5394A96DA7AC622E8E85A161EA2AA22C1D08CDC2_CustomAttributesCacheGenerator,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator,
	U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102_CustomAttributesCacheGenerator,
	ObiRopeCursor_t3A3DC1CEC7DA311FC3F7D0FDAE771A06BCFCAFB4_CustomAttributesCacheGenerator,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator,
	U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator,
	U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator,
	U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator,
	U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator,
	ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597_CustomAttributesCacheGenerator,
	U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator,
	U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator,
	U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator,
	U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator,
	ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B_CustomAttributesCacheGenerator,
	U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator,
	U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator,
	U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator,
	U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator,
	ObiRopeSection_tB462F344FF03C60586C93F498EDFB4E8F8E32D34_CustomAttributesCacheGenerator,
	ObiPathDataChannel_2_t318D0482FA88FB984537DA754769D160EBB6B558_CustomAttributesCacheGenerator,
	U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator,
	ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator,
	ObiRopeChainRenderer_t329C0DFFCB68B733AB1F215E95048DF74CBAA696_CustomAttributesCacheGenerator,
	ObiRopeExtrudedRenderer_t703051713A9D76924137234732D3F1DE1A9C8E0C_CustomAttributesCacheGenerator,
	ObiRopeLineRenderer_t39E7F2D3A153DAA2F4D7260C39BCB85E20D81A60_CustomAttributesCacheGenerator,
	ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator,
	ObiRopePrefabPlugger_t377F2388AED024B2636EBA5333D4110D459BDB55_CustomAttributesCacheGenerator,
	ObiRopeReel_t8479AF7D982E5FCD67E57C3032EA222552DF7BD8_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_tA5C757109DE911EDFAAE6282BDA413CB8C7D1E83_CustomAttributesCacheGenerator,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_mode,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_interpolation,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_gravity,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_damping,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_maxAnisotropy,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_sleepThreshold,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_collisionMargin,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_maxDepenetration,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_continuousCollisionDetection,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_shockPropagation,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_surfaceCollisionIterations,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_surfaceCollisionTolerance,
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_evaluationOrder,
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_iterations,
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_SORFactor,
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_enabled,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBlueprintLoaded,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBlueprintUnloaded,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnPrepareFrame,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnPrepareStep,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBeginStep,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnSubstep,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnEndStep,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnInterpolate,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_ActiveParticleCount,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_solverIndices,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_solverBatchOffsets,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_CollisionMaterial,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_SurfaceCollisions,
	ObiActorSolverArgs_t652D2183A88C8F5DDF27C6EEE9B4023CC9CE3725_CustomAttributesCacheGenerator_U3CsolverU3Ek__BackingField,
	ObiAerodynamicConstraintsBatch_tDA5413517E58B2320FF2DC31BEDA23C71B42E746_CustomAttributesCacheGenerator_aerodynamicCoeffs,
	ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_restBends,
	ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_bendingStiffnesses,
	ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_plasticity,
	ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_restDarbouxVectors,
	ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_stiffnesses,
	ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_plasticity,
	ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_firstParticle,
	ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_numParticles,
	ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_lengths,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_IDs,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_IDToIndex,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_ConstraintCount,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_ActiveConstraintCount,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_InitialActiveConstraintCount,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_particleIndices,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_lambdas,
	ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_CustomAttributesCacheGenerator_restLengths,
	ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_CustomAttributesCacheGenerator_stiffnesses,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_pinBodies,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_colliderIndices,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_offsets,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_restDarbouxVectors,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_stiffnesses,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_breakThresholds,
	ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinPoints,
	ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinNormals,
	ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinRadiiBackstop,
	ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinCompliance,
	ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_orientationIndices,
	ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_restLengths,
	ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_restOrientations,
	ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_stiffnesses,
	ObiTetherConstraintsBatch_tB72B2385E36323C87F8EA5CC41CEF9D5A3BD826B_CustomAttributesCacheGenerator_maxLengthsScales,
	ObiTetherConstraintsBatch_tB72B2385E36323C87F8EA5CC41CEF9D5A3BD826B_CustomAttributesCacheGenerator_stiffnesses,
	ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_firstTriangle,
	ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_numTriangles,
	ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_restVolumes,
	ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_pressureStiffness,
	ObiConstraints_1_t814AA25938B7022E214F5C54272335BCECBACF49_CustomAttributesCacheGenerator_batches,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_OnBlueprintGenerate,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_Empty,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_ActiveParticleCount,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_InitialActiveParticleCount,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator__bounds,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_positions,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_restPositions,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_orientations,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_restOrientations,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_velocities,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_angularVelocities,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_invMasses,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_invRotationalMasses,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_filters,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_principalRadii,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_colors,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_points,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_edges,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_triangles,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_distanceConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_bendConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_skinConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_tetherConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_stretchShearConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_bendTwistConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_shapeMatchingConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_aerodynamicConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_chainConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_volumeConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_groups,
	ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator_m_SourceCollider,
	ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator_m_DistanceField,
	ObiCollider2D_t5C38F1F5146454644E07250E0307C330A1CB4E31_CustomAttributesCacheGenerator_sourceCollider,
	ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_thickness,
	ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_material,
	ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_filter,
	ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator_rollingContacts,
	ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator_rollingFriction,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_input,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_minNodeSize,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_bounds,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_nodes,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_maxError,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_maxDepth,
	ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator_m_AlignBytes,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_voxels,
	ObiDistanceFieldRenderer_tF6263B78F384FE44C40CF3D3AAB28EF4C6802E49_CustomAttributesCacheGenerator_slice,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnCollision,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnParticleCollision,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnUpdateParameters,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnPrepareFrame,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnPrepareStep,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnBeginStep,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnSubstep,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnEndStep,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnInterpolate,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_simulateWhenInvisible,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_m_Backend,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_worldLinearInertiaScale,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_worldAngularInertiaScale,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_actors,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_m_ParticleToActor,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtyActiveParticles,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtySimplices,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtyConstraints,
	ObiFixedUpdater_t4D2492FAB85AE92AD8329DC2DA70CD416EC235D1_CustomAttributesCacheGenerator_substeps,
	ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator_substeps,
	ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_deltaSmoothing,
	ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_smoothDelta,
	ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_substeps,
	SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_U3CPropertyNameU3Ek__BackingField,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_U3CMethodNameU3Ek__BackingField,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_U3CNegateU3Ek__BackingField,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Actor,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Target,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_ParticleGroup,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_AttachmentType,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_ConstrainOrientation,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Compliance,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_BreakThreshold,
	ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator_skin,
	ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator_showPercentages,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_stitches,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_actor1,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_actor2,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_particleIndices,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_stiffnesses,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_lambdas,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator_m_SelfCollisions,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__radius,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__mass,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__rotationalMass,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__skinConstraintsEnabled,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__skinCompliance,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__skinRadius,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__stretchShearConstraintsEnabled,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__stretchCompliance,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__shear1Compliance,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__shear2Compliance,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__bendTwistConstraintsEnabled,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__torsionCompliance,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__bend1Compliance,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__bend2Compliance,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__plasticYield,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator__plasticCreep,
	ObiBone_tFDCA66A4BF467790A833B31D312BD2ADBCF2C4C5_CustomAttributesCacheGenerator_filter,
	BonePropertyCurve_t9D1823487E8E274DE6EE0BF366B805ADE1CD6FC1_CustomAttributesCacheGenerator_multiplier,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator_m_RodBlueprint,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__stretchShearConstraintsEnabled,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__stretchCompliance,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__shear1Compliance,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__shear2Compliance,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__bendTwistConstraintsEnabled,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__torsionCompliance,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__bend1Compliance,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__bend2Compliance,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__plasticYield,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__plasticCreep,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__chainConstraintsEnabled,
	ObiRod_tDC4A471C43308BB532AEDC6E3C376FF2108B880C_CustomAttributesCacheGenerator__tightness,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator_m_RopeBlueprint,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__distanceConstraintsEnabled,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__stretchingScale,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__stretchCompliance,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__maxCompression,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__bendConstraintsEnabled,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__bendCompliance,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__maxBending,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__plasticYield,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator__plasticCreep,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator_OnRopeTorn,
	ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_m_SelfCollisions,
	ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_restLength_,
	ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_elements,
	ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_OnElementsGenerated,
	ObiRopeCursor_t3A3DC1CEC7DA311FC3F7D0FDAE771A06BCFCAFB4_CustomAttributesCacheGenerator_m_CursorMu,
	ObiRopeCursor_t3A3DC1CEC7DA311FC3F7D0FDAE771A06BCFCAFB4_CustomAttributesCacheGenerator_m_SourceMu,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_transforms,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_restTransformOrientations,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_parentIndices,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_normalizedLengths,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_ignored,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_mass,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_rotationalMass,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_radius,
	ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_path,
	ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_resolution,
	ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_m_InterParticleDistance,
	ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_totalParticles,
	ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_m_RestLength,
	ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_restLengths,
	ObiRopeSection_tB462F344FF03C60586C93F498EDFB4E8F8E32D34_CustomAttributesCacheGenerator_vertices,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Names,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Points,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Normals,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Colors,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Thickness,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Masses,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_RotationalMasses,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Filters,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_Closed,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_ArcLengthTable,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_m_TotalSplineLenght,
	ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_decimation,
	ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_smoothing,
	ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_twist,
	ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_OnCurveGenerated,
	ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_rawChunks,
	ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_smoothChunks,
	ObiRopeChainRenderer_t329C0DFFCB68B733AB1F215E95048DF74CBAA696_CustomAttributesCacheGenerator_linkInstances,
	ObiRopeChainRenderer_t329C0DFFCB68B733AB1F215E95048DF74CBAA696_CustomAttributesCacheGenerator_randomizeLinks,
	ObiRopeChainRenderer_t329C0DFFCB68B733AB1F215E95048DF74CBAA696_CustomAttributesCacheGenerator_twistAnchor,
	ObiRopeExtrudedRenderer_t703051713A9D76924137234732D3F1DE1A9C8E0C_CustomAttributesCacheGenerator_extrudedMesh,
	ObiRopeExtrudedRenderer_t703051713A9D76924137234732D3F1DE1A9C8E0C_CustomAttributesCacheGenerator_uvAnchor,
	ObiRopeLineRenderer_t39E7F2D3A153DAA2F4D7260C39BCB85E20D81A60_CustomAttributesCacheGenerator_lineMesh,
	ObiRopeLineRenderer_t39E7F2D3A153DAA2F4D7260C39BCB85E20D81A60_CustomAttributesCacheGenerator_uvAnchor,
	ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_mesh,
	ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_axis,
	ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_instances,
	ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_instanceSpacing,
	ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_meshSizeAlongAxis,
	ObiRopeMeshRenderer_t7ED5B4B54B66B04E58240E99C6FBD6A8D5A740AC_CustomAttributesCacheGenerator_deformedMesh,
	ObiRopeAttach_t2EED0099FD22C1D89C5290D4D07160CC783C5B58_CustomAttributesCacheGenerator_m,
	ObiRopeReel_t8479AF7D982E5FCD67E57C3032EA222552DF7BD8_CustomAttributesCacheGenerator_outThreshold,
	ObiRopeReel_t8479AF7D982E5FCD67E57C3032EA222552DF7BD8_CustomAttributesCacheGenerator_outSpeed,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBlueprintLoaded_m04B2952178ABFA72F0A7E7219EB78A4ECEAE156E,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBlueprintLoaded_m998C17DB7FED249637AC6D40775D9D50ED1CA3F8,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBlueprintUnloaded_mB1182B14609EA8CCD5A9BD7C2F3CEDDAFF78C601,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBlueprintUnloaded_mB1AB8C2B322EB4633B0CD45EF5AAF7448BAD68BB,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnPrepareFrame_m3014BCA91C860098AA7A396A7FFCE7AB7CA1049E,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnPrepareFrame_m0D90685372167486999B646FC83D4E2C36C570AB,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnPrepareStep_mC586B960142DDDF81A5B1C87D393C7CA379E743E,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnPrepareStep_mCB8B15D01E8DA343CF2DD8E655932C886DD863CC,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBeginStep_m7A401277ED988AF4C6388B1F768B4E613BA02E10,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBeginStep_m80E4C1C9DE59FB6075BFB27E9B8D885ADC413C13,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnSubstep_m4E244BDD4F0DA8AFBEF75036632A65CF4EC1286F,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnSubstep_mABFE386CEC0194553AEE608F465F311B21360592,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnEndStep_mE226631AC3F2AA3BE52CCB4AB9972A0620E76208,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnEndStep_m18354446434FA0FE622CFA9F09B7D0D98B7365F9,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnInterpolate_mA39BB43FF56C2FF95981971698C30856DC25ED65,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnInterpolate_mE2B57508C1595198B4E27E10DA174A0B3792AD8D,
	ObiActorSolverArgs_t652D2183A88C8F5DDF27C6EEE9B4023CC9CE3725_CustomAttributesCacheGenerator_ObiActorSolverArgs_get_solver_m2C65EFA043F915BB25BF5EE5F467EA1119C55BDF,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_add_OnBlueprintGenerate_mA116B88726BA07CDAC7EA40506772CC39F8FF86E,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_remove_OnBlueprintGenerate_m780AFE9B203E214D86769A37EADEE63D5FC9967E,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_GetConstraints_m1C48E3155094EC159203D4F23B25015FCA6F0597,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_Generate_m14C8F36BC89354F15F0F82AE9546B5024F47FF7A,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50__ctor_m463D73E183DDE1A5C71E6E3CC821D0E7F30E347C,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m1C1B68AF4C49065E226E78E8D46FD68A406FC4DB,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m603F544C865B5A382E4D45071F9EA103E35296E2,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m8E40706068668910D9F7D8A1EACB85F8E6FFF77A,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mD7F6DAA1BCB1AB519151F38ACED86E62BB422730,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_mFFFD55BE544D6CB567D145A4DCB2564DD48B4CDF,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m3284B5FE565ECE74AF7F1FBE65BCD30C41CADA8D,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59__ctor_mD0933DCC8582C89719A5869AA40F1D96BC46A2A1,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_IDisposable_Dispose_m4C2610F3D57B4EB14EE56E2E116536B5A23C8B22,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7A53073B04207358D27C5F279097C3AECBDC48E,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_m326C85F4CC091D9CEBD26372E5491D58808FECB6,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_mB78D5553C5C65C889A8A4E2F6FCCADA45472D734,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_ObiDistanceField_Generate_m2FF083D72E253BE27D9DFD07C8816B449F778266,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16__ctor_m926CEA2198C2B31432AD7B0C629E0026FD9135A8,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_IDisposable_Dispose_mE8B29A18143A848E1C4C46A4D41CE2EE66999F73,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39320DBD9BADA847C3DC1B06385734720B3ED5D1,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m94171808CA1CAE76B9F0511FE6570131CD857EBB,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mE403FD60E16DACEE30BE06E73A882BBF823618FC,
	ASDF_tE13A467BED85E73DEF9E1A637B2471D28DFB9665_CustomAttributesCacheGenerator_ASDF_Build_m0B3E28D5C89253E2A2FAA9545A0B748B5C69E1D4,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3__ctor_m2AF6E46C3AF1DCE8EB0AC87345B0082720891BF3,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_IDisposable_Dispose_m407D5BEFA6CC050E62B83A69FC1C81FA11062FE4,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FD71D70EB820213EDF7BC3853561DF86E43D30D,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m026B7C1E35013639385A979664E9479DCFB7269A,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_m8730288CA28692A77A479C27FB99AFB9421E825B,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_mED2F171342075378B5B140C14A73A72EF8ACC45F,
	ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator_ObiNativeList_1_GetEnumerator_mAA681A9160D0F5ABB78834DDCE50A090F9027FFD,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47__ctor_m44C213C4523E721D89454E9E0DB188309DAA0C2C,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_IDisposable_Dispose_m3677A4910B2FBA94AB21EC467EA0F8D017654F32,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m577FD8C7518180375EAF528F1953D6A86B76129F,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_IEnumerator_Reset_m59513FC87FBEC9A3F46BF8F1A27BE3F8C45CA05D,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_IEnumerator_get_Current_m45C9E58B0197FD608861CCFE12CE9E08134F9D32,
	ObiList_1_tAFBF97086278598236664F1BB4032FED42C06E1E_CustomAttributesCacheGenerator_ObiList_1_GetEnumerator_m669DC89479304E618D4B41858753616F692A64B8,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2__ctor_m220A0BB8B4845E3E03C861BC45721DD12E9E7C7B,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_m6C1E284C049482492472BDE417FEC2D9BC5F53E6,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m8D4794A9AC960B97A584FB17906AFA27C95E4C0B,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_mE6DC01E508903376308CBEEFDB0FD2C1990A7DEF,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_m34A4E8F5E7BB811C2830337581A6877BD404D22B,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_Voxelize_mFD415E5ED30A0BF0CBC964412DBBB37930CFA97A,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_FloodFill_m584C8081EF5F8BC2660348F75BF0C006FBD0EC51,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29__ctor_m8645BD0F9E12E3B4AE36A3A9D757A0AA317666E0,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_IDisposable_Dispose_m68F6D58872E569B35178F3F948D25088FEE404DD,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCADBFC708A9E44E40767E097632FC238158C8036,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_Reset_mF260247FB427D7B3C0DAAFFD3085943EBA202B9C,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_get_Current_m4ED17979436917051AE4058242D7AF43D4A9F2C7,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31__ctor_m70BCBE21D4D47E1482D966F7C9D35F7510603B97,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_IDisposable_Dispose_m874A33578734166DA64BFC7AE8771A031A25F2C2,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60C01825AF1A7118B99F44611D5A9FFA941E6189,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_IEnumerator_Reset_mC4010923219A1BAD954EEAC4B73CDD945265C9A3,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_IEnumerator_get_Current_m7964014EF80DB8139871A160F4672E40BED0788E,
	PriorityQueue_1_tDBFAA8299A447FDE7A02B777CB2142412A0808C9_CustomAttributesCacheGenerator_PriorityQueue_1_GetEnumerator_mE59F3123BC2AC2349A2019082A77861F7C5E19B9,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5__ctor_mE283398E42D28AE61C9072FB8E231BDCA9867986,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_IDisposable_Dispose_m84D4F91D281E15BADD9CF82AE81CE1F0B9175998,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m9BA3FA8D6F2F77E738345A1A1EE37171EF68A96F,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerator_Reset_m01671BA954E40111F5CCF141F7B255B8F41519F8,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerator_get_Current_mA8032079CD99DE6D6B0A0C8AF31E894B4C7AADE3,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mDD221B9EC48424B9BBA073FC887B08439C91DA49,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerable_GetEnumerator_m050DABA4F326191228EDB111C0B34B0E6DE60B0C,
	VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA_CustomAttributesCacheGenerator_VoxelDistanceField_JumpFlood_m8A165DB8B897C98F4DD564943140A9C71408E1F6,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5__ctor_m9A42F34DE8C7C452C8A53F17D6D572BC2453C654,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_IDisposable_Dispose_m0A919D1AE0822BB1475E5FE78DA4060A236911E6,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF414A3DC08D65DC419082B4635288E1F7E02FC0,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_get_Current_m0850F7B63FF4B6EB05FB8A60525AB3BC48509D18,
	VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_U3CFindClosestNonEmptyVoxelU3Eb__6_0_mBBEC0743F2CB9A04F014A64C652D3572607A4FDC,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnCollision_mEB324EABAE3F36C43F055950F52827839851DAF5,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnCollision_mA07AA8AA273F50BC4DDAFD0523A7CA23C628D674,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnParticleCollision_m73A739A0544F098FE805CEF2C6ED3E9528060A52,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnParticleCollision_m153C65501BB104D19DFB0BC50CE655A20BB725B7,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnUpdateParameters_mDCE3C3436FCB6E239FC3C229799E1CA65048798B,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnUpdateParameters_m1D7467B121B4968CCB10AE69A8FCE3F601A622E1,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnPrepareFrame_mF8245DFAA6E404FFD6135985C1B90BAD5A827B69,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnPrepareFrame_mFF9D302946D3647FF7267652E21CCDA784AC68E6,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnPrepareStep_m22DB6F7A692B42DCB0AF33EDDFAF38062A4AB42E,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnPrepareStep_m5D2D4E8CACADB7938868BE6F29546BA497A05250,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnBeginStep_mE11B9F3927E6908AC1C8CA09888FAC4145B60F72,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnBeginStep_m828F85A9556BCF73D5434B9A689918DEF5F5D39D,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnSubstep_mF4D4FE1ED9E867E6F111B8B082E377D950607C6E,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnSubstep_m60FD7FE897C29FEF9EE0EDC9303C62CFF70D2B33,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnEndStep_m85BFAD019C29970E500BB5079DA70527A4EB324D,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnEndStep_m53846A7D3E4D2113D49EAA4EE56FAE9752CB6CA3,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnInterpolate_mBADEB5D974C090F97884D9FF885C017D0C36CF7D,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnInterpolate_m5F9AAE96B45E17989172907EECA1966282A87626,
	ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator_ObiLateFixedUpdater_RunLateFixedUpdate_mA207589FB295229010BC3ED39CA0A33B465AE4AB,
	U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__7__ctor_m26D083432D24A29A1601BBEED04C921E5E757A62,
	U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__7_System_IDisposable_Dispose_m28B4BB85E97B45021B22C15FEF3D8E93B5BF81C8,
	U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD948A54767F3CE844E780E8D04C37F8B9F865A05,
	U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__7_System_Collections_IEnumerator_Reset_mBC0D3D496D3790C6970DE839BF6F1DEE46B04FB3,
	U3CRunLateFixedUpdateU3Ed__7_tAA7814212636C20D135CA37D8443E2FC243A517A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__7_System_Collections_IEnumerator_get_Current_m200572F643F2FEB367A9BF53A37578AAAE32802F,
	SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_SerializeProperty_get_PropertyName_mDC8A03791EF5C0FC8DB9BEE2645D05EC456A9B21,
	SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_SerializeProperty_set_PropertyName_m0D097B39C9F982C6DA9CFE395012ED31AE075A55,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_get_MethodName_mA9D547283CA5003031A6052EC2F01BDD20626C79,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_set_MethodName_mEEE7DE756E758BB291ECC2AF95FB9331577A89BD,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_get_Negate_m70429C0B2F6E15B7329A21127ED7F095B5B9F79C,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_set_Negate_m7AE9FD0E091984A71A593C3FE0A0A4CB89CDDF7E,
	CoroutineJob_t80DB8BDF56DB40B9329335C29878C9D46D8C879F_CustomAttributesCacheGenerator_CoroutineJob_Start_m80C8E17CD749B13B237150583A6280C213F8C39F,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15__ctor_mD7A6880AAEB86FECE339EAA24C276AA5D91FE5AD,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_IDisposable_Dispose_m4542E430FBE633F30A5F1B001DA8C7D01BE636F1,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m857DB02265865988D8A060D28C87EAB01F6DEC55,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_mC889240009E6BB1F36B3BC74F618A35A604608B3,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mE937748101E5BB647A3762B97A9DE2FED38D8283,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Swap_mF56D360C3B427C99DB48F907D7B8F1F2D96B2474,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Swap_m4545B5452717048909109CF73C8CAC6880F7F227,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ShiftLeft_m983E32714E08933EE70C58FD314AB2F11C10DB5D,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ShiftRight_m0CCEF49D7BEEB3D21A740CEEF10750BFA3E10ED5,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_AreValid_m36409B4F74E4DA83A742954433F6CA4EE168BAE6,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Transform_m733A8AC89C2BFBC7A0E0089512FFC4709454DE88,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Remap_m8F506FF64E3B9168DE2B0235E274CC1033B7D86F,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Add_mC0C822C7DBBF1BA01EE882208292D519B2C9C2D4,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_FrobeniusNorm_m864D4DA65931429E717193CB634FFAAF31C5DCF4,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ScalarMultiply_mE25A02716B8B63129015ACA756D1DB1AD469DE51,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_PureSign_m92E94D198EF88DC390CCBA619718D6A6C27BA5C5,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BilateralInterleaved_m17A79FDAFD53A3C8E7CB5403AC539CD1BC4D5453,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_unitOrthogonal_m08E2004A970D966D5227E325780C23E6FAB28529,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_IDisposable_Dispose_m8831F3EF2CA071AF13BD9D630BBA7F87306BD89C,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9E1918A629F5BD2E500DBD47B57BEA70DA7126,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_get_Current_m82E938AAC7D70CD2629E214DDAC1F71A6BB51474,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mBF01BD41B11190F2A9C48BE6A1A9FB04EF151CF6,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator_ObiRope_add_OnRopeTorn_m82818AFDC4E5B53B9BDB3BC5F448DC5819AEAF17,
	ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464_CustomAttributesCacheGenerator_ObiRope_remove_OnRopeTorn_mDCCBD9E8FEAA87E7DB5622737423840AD4EE1A5F,
	ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_ObiRopeBase_add_OnElementsGenerated_mD9B02AE3DC85CC5762C7AD9F125360E17E1566C8,
	ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7_CustomAttributesCacheGenerator_ObiRopeBase_remove_OnElementsGenerated_m628F81AC8C51256C7D4EA46CAC6F983413DB27A4,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_ObiBoneBlueprint_Initialize_m2A9787CD6FC33DBD5B198E7D667BE12DE690C382,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_ObiBoneBlueprint_CreateStretchShearConstraints_m20DA40DA29C69B617A52422B5084F44019F99604,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_ObiBoneBlueprint_CreateBendTwistConstraints_m61BD7C27BF0655EA303B4E58C0B98D04B424D6B5,
	ObiBoneBlueprint_tE692DE347D037692D2DD9C764FD2229DEAA5014B_CustomAttributesCacheGenerator_ObiBoneBlueprint_CreateSkinConstraints_mE42F073A01A3AD9EAD21A01B6FB490F178E695CB,
	U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__14__ctor_m27140A7DF8EF3E999F1FC723535A04C964525F82,
	U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__14_System_IDisposable_Dispose_m88FFDD6CFBB11319DDBBB0F8B34089B80C4F0F01,
	U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC52279203D514C26549053545D8AF9B41AEB60F,
	U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__14_System_Collections_IEnumerator_Reset_m9EC41DFD1D0FFE3D465A64CEE388F88EA13B2D0B,
	U3CInitializeU3Ed__14_t5B2F3C253E8BD9912C9C9CE86AE9A9A49E1E1F4F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__14_System_Collections_IEnumerator_get_Current_m5A676E16757268FF757D0DC38F8AD1E8EDF2EEA4,
	U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__16__ctor_mC9883EA29286F581B5BE6546F53E1803A36FC572,
	U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__16_System_IDisposable_Dispose_m6F122F292C0A6014F0DFBAAD6E22DFEFEF7F74F8,
	U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C73E4EA2DE56F4924F1F4D440D8DEBA59271AD2,
	U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_IEnumerator_Reset_m622BC462DD1E9B4018F1F51A846510E62CECDA84,
	U3CCreateStretchShearConstraintsU3Ed__16_tED9950C0AD62903356CEDB807251D6B0488CA379_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__16_System_Collections_IEnumerator_get_Current_m4A178A0000B1F11C4A79E52395DDC3CAB3B647C8,
	U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__17__ctor_m97129288678462023812E6355E3F1BEB10871686,
	U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__17_System_IDisposable_Dispose_m7FA46F1AE20471FF759E6AD807D8CFD6E46DFD2A,
	U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB14225394B51F4DE1158DFBBFD03146485847A8,
	U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_IEnumerator_Reset_mE9692B1811A0104396D417403B04677B7C8903DD,
	U3CCreateBendTwistConstraintsU3Ed__17_t56B0B3932A1901D6BD831842A5A72D23CD68B762_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__17_System_Collections_IEnumerator_get_Current_mACB8F571A51435DCA534D455C05DEC2BCBD56BBA,
	U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__18__ctor_m01559B1A25470AEFB06D7A19008967898DB8A6FA,
	U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__18_System_IDisposable_Dispose_m13FC3CA635500255354816726C2A0334D420AAA0,
	U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m025792FC9DCA9844F1948AAF26303D5DC3941422,
	U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__18_System_Collections_IEnumerator_Reset_m989EDC08F01688A58194BA6518DE7A3D9F10934A,
	U3CCreateSkinConstraintsU3Ed__18_t0E3C49BC6BB259D6FA730E93CE81C8220C6E4E78_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__18_System_Collections_IEnumerator_get_Current_mC414552ED2BBDCAA4A581F8CA9BC1CCAC78D9B7F,
	ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597_CustomAttributesCacheGenerator_ObiRodBlueprint_Initialize_mB0D5422A4133FF08FC0F52DA417299A628B98B26,
	ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597_CustomAttributesCacheGenerator_ObiRodBlueprint_CreateStretchShearConstraints_m9413EBB5F95ABFD44A15EDF6DA9CC67830A98C6A,
	ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597_CustomAttributesCacheGenerator_ObiRodBlueprint_CreateBendTwistConstraints_m5F1A04D6C343F07E764A8641AA54EF2EEB2162E8,
	ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597_CustomAttributesCacheGenerator_ObiRodBlueprint_CreateChainConstraints_m665ECE23FA005CB35338951024B674EB2172B5EF,
	U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator_U3CInitializeU3Ed__3__ctor_m41BDF4E1CF1181A4B67AA1298FF91C848705149D,
	U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator_U3CInitializeU3Ed__3_System_IDisposable_Dispose_mAAB9B58959E17D6316D0376867E1ADEA76D9F1EC,
	U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator_U3CInitializeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD199EE52973224E0C633E71C904164111168B9B7,
	U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator_U3CInitializeU3Ed__3_System_Collections_IEnumerator_Reset_m70CE6B4088FBF17D28720288E2F251679B22BD11,
	U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C_CustomAttributesCacheGenerator_U3CInitializeU3Ed__3_System_Collections_IEnumerator_get_Current_m6B81FD82D37E5ED58B0B639232283B9902ED448B,
	U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__4__ctor_mDFFBA8618ED30102037B3C56180A9BEA9487C330,
	U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__4_System_IDisposable_Dispose_m89DF329DFEB0C57A2E9159B524D238E75D784621,
	U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48490361DF35F3911FA88CC9E8488B9976632C81,
	U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m3EC8612B472DCDC0A5BA07CF265742F7ADB074CC,
	U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46_CustomAttributesCacheGenerator_U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_mDF13C68A7A9199168F5434C5FDAE03DECED69AD9,
	U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__5__ctor_m74EC6A8D57860822339D5CCF9A93DA20335836FD,
	U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__5_System_IDisposable_Dispose_mCD67BAFE6581BBC3F6B167D165F996E217FA17A5,
	U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE73B33CF3970D7E2EC7E22CC25D72B5F318EEBD0,
	U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_IEnumerator_Reset_m334DE000C0E71E3904B0922ACE92B45250B40DAF,
	U3CCreateBendTwistConstraintsU3Ed__5_tFDFACC083FFACD7C9F0098CDEB5CEBF5B04767A3_CustomAttributesCacheGenerator_U3CCreateBendTwistConstraintsU3Ed__5_System_Collections_IEnumerator_get_Current_mDAD52D5C97FC4B1965C20CF8C10650EF73423A09,
	U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator_U3CCreateChainConstraintsU3Ed__6__ctor_mCCD0039AE63FA7C9AF769CB6DA517174CFE80C80,
	U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator_U3CCreateChainConstraintsU3Ed__6_System_IDisposable_Dispose_m8D90A9C0F15B87BFEE0F460C6F3DA96A7595273C,
	U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator_U3CCreateChainConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m15E671F86EEE570870E5A45BF66B72D282E1CBD5,
	U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator_U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_m394694E12A8EAEEB4EA80649BEA9BBE861C306FB,
	U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872_CustomAttributesCacheGenerator_U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_mC6B36F52F2460C2F7E972D940E2F69B588832FED,
	ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B_CustomAttributesCacheGenerator_ObiRopeBlueprint_Initialize_m253C93BBFF0249E6F38C1B014E0FC4D6B0263F30,
	ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B_CustomAttributesCacheGenerator_ObiRopeBlueprint_CreateDistanceConstraints_m0179DE68BDE1A682413705118D3034FE4070A492,
	ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B_CustomAttributesCacheGenerator_ObiRopeBlueprint_CreateBendingConstraints_m38CAD4B112A942FDE1242E78797329EBF7F0F468,
	U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2__ctor_mE9943407AC4BCDD21A5F1BB4759F285BAE447D8D,
	U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_IDisposable_Dispose_m97A6F5247B0287130D582F2BB4E544062A689B13,
	U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAF2CC89A62774D134AF088ADE0119C979EB1F72,
	U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m7A4B24373876BDE2453048631F2B963E386E7D12,
	U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m16BDC9B0E8C7DB5818965339EC2F1EFEFEF37DD9,
	U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3__ctor_mC2881BA9A2C3B5FFD584032842FC4D8016768534,
	U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_m1CE3F312F7B66A397DE190A3983DA6CB907431EC,
	U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74A1152B6135923B3BAACBAC1F2F5932512D8046,
	U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mD6AC7761E802DE2F6F0CADB0A5682051E6E4669C,
	U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8EF851B496CA7FD1CC7F31747EEF8484A7FCBB15,
	U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__4__ctor_mF09A262B24850675B97784C745083E957AC70DDB,
	U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__4_System_IDisposable_Dispose_m1ACE661B7DC059891776A5AD8632DD995321EBA3,
	U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD5D1A5BD31642785B698F9B9734EBE8775F45FA6,
	U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m8B106EC62D22296800CE67482EE3F81810C06907,
	U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m9475B499E2444C3687CB7A86ADA02EFA9627693A,
	ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687_CustomAttributesCacheGenerator_ObiRopeBlueprintBase_Initialize_m49A4560B16A115658987C79F148B7D9B9983A5A9,
	U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator_U3CInitializeU3Ed__17__ctor_m52F67707D008A9F9B010B0538ECEE7A5CC69F2EB,
	U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator_U3CInitializeU3Ed__17_System_IDisposable_Dispose_mF0B5E8B1440934FC4C19F4F949EACFE581448839,
	U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator_U3CInitializeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DD4C0990CD4B35386F56E79FF03AA864B1D6C21,
	U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator_U3CInitializeU3Ed__17_System_Collections_IEnumerator_Reset_m28AA9E89D0D2B4BD55BE44CC152105CAB3FE46A7,
	U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8_CustomAttributesCacheGenerator_U3CInitializeU3Ed__17_System_Collections_IEnumerator_get_Current_mC609C587F1D824ADC1D1CD66C9D51AB95F222BAA,
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459_CustomAttributesCacheGenerator_ObiPath_GetDataChannels_m971B8134825853C6010D8D02446AB4EEE35CE5C1,
	U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17__ctor_mD0F362BFD59ACE15D4D3A884E8226BF10577B2B1,
	U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_IDisposable_Dispose_m1491BD58F339286AD9852B89147455FFADE4FB35,
	U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_Collections_Generic_IEnumeratorU3CObi_IObiPathDataChannelU3E_get_Current_mA4EFA4B8901E742332F05D5737EA190D71393707,
	U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerator_Reset_mEBF818027E97B5B63CBDDC30BE2E6B48F896B7DF,
	U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerator_get_Current_m88CC0E7304DD9F211246F7A845B040ADB99D4662,
	U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_Collections_Generic_IEnumerableU3CObi_IObiPathDataChannelU3E_GetEnumerator_m796DAF5CF8553C1133063B7487009CBB8F6E31B8,
	U3CGetDataChannelsU3Ed__17_t241BBEF274EF0DFC43781014B300E31B0410F6C4_CustomAttributesCacheGenerator_U3CGetDataChannelsU3Ed__17_System_Collections_IEnumerable_GetEnumerator_mD094D18AC457EA20B62A9CCF2837713884B4A7A5,
	ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_ObiPathSmoother_add_OnCurveGenerated_m1612F15B48776929153BBCB1C62F5B1720320755,
	ObiPathSmoother_tF3C589F5BC1AFA2E1167E0A0385C6ACAF170C970_CustomAttributesCacheGenerator_ObiPathSmoother_remove_OnCurveGenerated_m0136223598B7485378680ECE56861613BAE13476,
	ObiRopeLineRenderer_t39E7F2D3A153DAA2F4D7260C39BCB85E20D81A60_CustomAttributesCacheGenerator_ObiRopeLineRenderer_U3COnEnableU3Eb__15_0_m6793DE39FBF2171A3AF9F73DC1442DCC302C8694,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905____node3,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905____point4,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mAE13B1EFAA791602268DFF2A5ED429691EA76178____point4,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05____node4,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05____point5,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetVoxelCenter_m3E452556E6139C67B397A378FDCBD9BA4F425089____coords0,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v10,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v21,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v32,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetPointVoxel_mB53686BE0482AC0FE527505F8EC019E21CFAB6B1____point0,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_VoxelExists_mBD68D5E79FB132F4304AAE5283DF58F65CDA0A4B____coords0,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____bounds0,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v11,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v22,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v33,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_IsIntersecting_m1F9C81AE3D544C1F8AA465823F7DDA7A32C7FBEC____box0,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v00,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v11,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v22,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____aabbExtents3,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____axis4,
	VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_AStar_mA838A6AEC2878DE3B68FB035FCB31C5ADE9958EC____start0,
	VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_FindClosestNonEmptyVoxel_m108974C29C2DE5DCB4E8092382271E90E1306714____start0,
	VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_FindPath_m12E0CEA61718E42317477C9B7A3825B9EAA590E5____start0,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p10,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p21,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p32,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p3,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____A0,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____B1,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____C2,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____P3,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p10,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p21,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p32,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____coords3,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_GetPointCloudAnisotropy_m3D48980B4381A7B54F37C063FE47EF7C737CA9A2____hint_normal3,
	Obi_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}
