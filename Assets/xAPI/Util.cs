﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace GPTT.Data
{
    public static class Util : object
    {
        public static void Run()
        {
            Debug.Log("Init");
            //UnityWebRequest webRequest = UnityWebRequest.Get(Path.Combine(Application.streamingAssetsPath, "Machine.txt"));
            //webRequest.SendWebRequest();
            //MachineData = JsonUtility.FromJson<Machine>(webRequest.downloadHandler.text);
            //webRequest.Abort();
            var unityWebRequest = UnityWebRequest.Get(Path.Combine(Application.streamingAssetsPath, "Machine.txt"));
            unityWebRequest.SendWebRequest();
            while (!unityWebRequest.isDone) { }
            string text = unityWebRequest.downloadHandler.text;
            //string text = System.IO.File.ReadAllText(Path.Combine(Application.streamingAssetsPath, "Machine.txt"));
            //Debug.Log(text);
            MachineData = JsonUtility.FromJson<Machine>(text);
        }

        /// <summary>
        /// 加密後回傳base64String，相同明碼文字編碼後的base64String結果會相同(類似雜湊)，除非變更key或iv
        /// 如果key和iv忘記遺失的話，資料就解密不回來
        /// </summary>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Encrypt(string text)
        {
            Aes aes = Aes.Create();
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            ICryptoTransform transform = aes.CreateEncryptor(Encoding.UTF8.GetBytes(MachineData.MachineKey), Encoding.UTF8.GetBytes(MachineData.MachineIV));

            byte[] bPlainText = Encoding.UTF8.GetBytes(text);
            byte[] outputData = transform.TransformFinalBlock(bPlainText, 0, bPlainText.Length);
            return Convert.ToBase64String(outputData);
        }

        /// <summary>
        /// 解密後，回傳明碼文字
        /// </summary>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <param name="base64String"></param>
        /// <returns></returns>
        public static string Decrypt(string base64String)
        {
            Aes aes = Aes.Create();
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            ICryptoTransform transform = aes.CreateDecryptor(Encoding.UTF8.GetBytes(MachineData.MachineKey), Encoding.UTF8.GetBytes(MachineData.MachineIV));
            byte[] bEnBase64String = null;
            byte[] outputData = null;
            try
            {
                bEnBase64String = Convert.FromBase64String(base64String);
                outputData = transform.TransformFinalBlock(bEnBase64String, 0, bEnBase64String.Length);
            }
            catch (Exception ex)
            {
                throw new Exception($@"解密出錯:{ex.Message}");
            }

            return Encoding.UTF8.GetString(outputData);

        }

        /// <summary>
        /// 取Timestamp
        /// </summary>
        public static long GetTimeStamp()
        {
            return GetTimeStamp(DateTimeOffset.UtcNow);
        }

        public static long GetTimeStamp(DateTimeOffset dateTimeOffset)
        {
            return dateTimeOffset.ToUnixTimeSeconds();
        }

        /// <summary>
        /// MD5加密
        /// </summary>
        public static string ToMD5(string str)
        {
            string pwd = "";
            MD5 md5 = MD5.Create();
            byte[] s = md5.ComputeHash(Encoding.UTF8.GetBytes(str));
            for (int i = 0; i < s.Length; i++)
            {
                pwd = pwd + s[i].ToString("X2");
            }
            return pwd.ToLower();
        }

        /// <summary>
        /// 取設備唯一碼並做MD5加密
        /// </summary>
        public static string GetSerialNumber()
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            return "465a00031cf99a80647c512bea5b6a6a";
#else
            string deviceType = SystemInfo.deviceType.ToString();
            string deviceModel = SystemInfo.deviceModel.ToString();
            string serialNumber = SystemInfo.deviceUniqueIdentifier;
            string serialNumberToMD5 = ToMD5(deviceType + deviceModel + serialNumber);
            return serialNumberToMD5;
#endif
        }

        public class Machine
        {
            public string MachineToken;
            public string MachineKey;
            public string MachineIV;
        }

        public static Machine MachineData;
    }
}