﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* CAPI_LoggingCallback_mE9CB96D4CEB343BBBBAA0ACD39A6281C11D4110B_RuntimeMethod_var;
extern const RuntimeMethod* SocialPlatformManager_MicFilter_m0FB5A641DE3104796155D242D62D10DFFB182F54_RuntimeMethod_var;



// 0x00000001 System.Void CUI_CameraRotationOnButtonHeld::Start()
extern void CUI_CameraRotationOnButtonHeld_Start_m30296B8C7411400158611D7529DA36871CA1C08A (void);
// 0x00000002 System.Void CUI_CameraRotationOnButtonHeld::.ctor()
extern void CUI_CameraRotationOnButtonHeld__ctor_m8171E5BE364721612E199DEC96FC29B55B2C85F7 (void);
// 0x00000003 System.Void CUI_MoveAlong::Start()
extern void CUI_MoveAlong_Start_mED9D864632FB694902CF7A79840C0DBD62276B6C (void);
// 0x00000004 System.Void CUI_MoveAlong::Update()
extern void CUI_MoveAlong_Update_m9A2DC00C717B9D5AD62CC9C39B6520ED21ED0697 (void);
// 0x00000005 System.Void CUI_MoveAlong::.ctor()
extern void CUI_MoveAlong__ctor_m7DB256B85D07BB4EBA7BF57D94C4FFC2C0773DAC (void);
// 0x00000006 System.Void CUI_MoveHeartbeat::Start()
extern void CUI_MoveHeartbeat_Start_m1B77A93B8C10B65EF9DD39E375344B2C0706A070 (void);
// 0x00000007 System.Void CUI_MoveHeartbeat::Update()
extern void CUI_MoveHeartbeat_Update_mCE8CA7B51BA29FACAC94747FCAB835939D708F2C (void);
// 0x00000008 System.Void CUI_MoveHeartbeat::.ctor()
extern void CUI_MoveHeartbeat__ctor_mCF476DDD167DA09AA9DE860EC05B3DC656D3B7BE (void);
// 0x00000009 System.Void CUI_ShowParentCoordinates::Start()
extern void CUI_ShowParentCoordinates_Start_mE18C4A26580030AEAB52BA3C9383FB7FF1400229 (void);
// 0x0000000A System.Void CUI_ShowParentCoordinates::Update()
extern void CUI_ShowParentCoordinates_Update_m57ABED7A44EF3044621912AD32E63D6E0F326848 (void);
// 0x0000000B System.Void CUI_ShowParentCoordinates::.ctor()
extern void CUI_ShowParentCoordinates__ctor_mA672F201FA44A897396BB6CE9E0AFED694B02D94 (void);
// 0x0000000C System.Void CUI_rotation_anim::Start()
extern void CUI_rotation_anim_Start_m0CAEB6FEC4FC7B02DD1860698E01E337D85342B5 (void);
// 0x0000000D System.Void CUI_rotation_anim::Update()
extern void CUI_rotation_anim_Update_m4CC2C3F01E8AD5D0FBD92E4114E2729942E32FD7 (void);
// 0x0000000E System.Void CUI_rotation_anim::.ctor()
extern void CUI_rotation_anim__ctor_m3DF66001A411C57DDB9B29CC904CC9B1B73D207E (void);
// 0x0000000F System.Void CUI_touchpad::Awake()
extern void CUI_touchpad_Awake_m20432FA4CB5FAEF5ED50CC322658D664223CC2D4 (void);
// 0x00000010 System.Void CUI_touchpad::MoveDotOnTouchpadAxisChanged(System.Object,CurvedUI.ViveInputArgs)
extern void CUI_touchpad_MoveDotOnTouchpadAxisChanged_m331119E1F7FBC1ADC238F9D25D87F526B42E3B6B (void);
// 0x00000011 System.Void CUI_touchpad::.ctor()
extern void CUI_touchpad__ctor_m214DEE14E28C03DF6BEBB98F281030E961E57622 (void);
// 0x00000012 System.Void CurvedUIInputModule::Awake()
extern void CurvedUIInputModule_Awake_m459F6CE5FCA6237742503D156E33FB307BB75A89 (void);
// 0x00000013 System.Void CurvedUIInputModule::Start()
extern void CurvedUIInputModule_Start_m40A7CE503CF7F076EC4C870C3DA74CF133DDEEE1 (void);
// 0x00000014 System.Void CurvedUIInputModule::Update()
extern void CurvedUIInputModule_Update_m361E8DA033404BEA21194F20B291B1CAE84FCC4C (void);
// 0x00000015 System.Void CurvedUIInputModule::Process()
extern void CurvedUIInputModule_Process_m111FD300F4935C6B216D4F34890A0D1BEE0409C0 (void);
// 0x00000016 System.Void CurvedUIInputModule::ProcessGaze()
extern void CurvedUIInputModule_ProcessGaze_m62BA5A85D37E3C4BAB8B8692E64A0B8B16712765 (void);
// 0x00000017 System.Void CurvedUIInputModule::ProcessCustomRayController()
extern void CurvedUIInputModule_ProcessCustomRayController_m1CD280750DAF41039906B20351137839779D8C34 (void);
// 0x00000018 UnityEngine.EventSystems.PointerInputModule/MouseState CurvedUIInputModule::GetMousePointerEventData(System.Int32)
extern void CurvedUIInputModule_GetMousePointerEventData_mE9B50FB0C07545577E80E8F6D2ED84086075422C (void);
// 0x00000019 UnityEngine.EventSystems.PointerEventData/FramePressState CurvedUIInputModule::CustomRayFramePressedState()
extern void CurvedUIInputModule_CustomRayFramePressedState_m1431D59909516A341EA9D3E2FB1B147B661D5782 (void);
// 0x0000001A System.Void CurvedUIInputModule::ProcessViveControllers()
extern void CurvedUIInputModule_ProcessViveControllers_mD9D820D27E1ADCB7AF2237D0ABB52027BD30CDD1 (void);
// 0x0000001B System.Void CurvedUIInputModule::ProcessOculusVRController()
extern void CurvedUIInputModule_ProcessOculusVRController_m21223AA14E4C5AD193596AB7DFBFFADB994C826E (void);
// 0x0000001C System.Void CurvedUIInputModule::ProcessSteamVR2Controllers()
extern void CurvedUIInputModule_ProcessSteamVR2Controllers_m5EEDE3D7AAB8A885E3C703B783368D7E4CE3E009 (void);
// 0x0000001D System.Void CurvedUIInputModule::ProcessUnityXRController()
extern void CurvedUIInputModule_ProcessUnityXRController_m8CEACBEC138A601094325A6F40B6A4CC6746160F (void);
// 0x0000001E T CurvedUIInputModule::EnableInputModule()
// 0x0000001F CurvedUIInputModule CurvedUIInputModule::get_Instance()
extern void CurvedUIInputModule_get_Instance_mC9DDA54886BF85942DC1550EA2E0402870DB2A6B (void);
// 0x00000020 System.Void CurvedUIInputModule::set_Instance(CurvedUIInputModule)
extern void CurvedUIInputModule_set_Instance_m51EF0229D6836DF3FC59FA0B0691C5EEE4D6D12A (void);
// 0x00000021 CurvedUIInputModule/CUIControlMethod CurvedUIInputModule::get_ControlMethod()
extern void CurvedUIInputModule_get_ControlMethod_mC512EF503B901ADFB66F30F116688DE6A7DFCC4C (void);
// 0x00000022 System.Void CurvedUIInputModule::set_ControlMethod(CurvedUIInputModule/CUIControlMethod)
extern void CurvedUIInputModule_set_ControlMethod_mABF3F935318DCA34E13DD3D3C3191660B6023F89 (void);
// 0x00000023 UnityEngine.LayerMask CurvedUIInputModule::get_RaycastLayerMask()
extern void CurvedUIInputModule_get_RaycastLayerMask_mAEEA2CC7326CA09868F82C89312548481D0B2298 (void);
// 0x00000024 System.Void CurvedUIInputModule::set_RaycastLayerMask(UnityEngine.LayerMask)
extern void CurvedUIInputModule_set_RaycastLayerMask_mB5424866EC6399E035CCF596C292A232D2EDA333 (void);
// 0x00000025 CurvedUIInputModule/Hand CurvedUIInputModule::get_UsedHand()
extern void CurvedUIInputModule_get_UsedHand_m79EBF036F5EDD2BAAAE61122B1157551C529C8D0 (void);
// 0x00000026 System.Void CurvedUIInputModule::set_UsedHand(CurvedUIInputModule/Hand)
extern void CurvedUIInputModule_set_UsedHand_m009D73A0B802C0E268900E01C0B0C279C9CF80CB (void);
// 0x00000027 UnityEngine.Transform CurvedUIInputModule::get_ControllerTransform()
extern void CurvedUIInputModule_get_ControllerTransform_m3784A551AD38C88CFA1BB4CA46D6D367FDAAC94A (void);
// 0x00000028 UnityEngine.Vector3 CurvedUIInputModule::get_ControllerPointingDirection()
extern void CurvedUIInputModule_get_ControllerPointingDirection_mC6CC7A3EFF829A3967F557EB52EE5B5AAB00CD3F (void);
// 0x00000029 UnityEngine.Vector3 CurvedUIInputModule::get_ControllerPointingOrigin()
extern void CurvedUIInputModule_get_ControllerPointingOrigin_m0AD6C220EDE79AC6D03E3D66A8322080B34D5720 (void);
// 0x0000002A UnityEngine.Transform CurvedUIInputModule::get_PointerTransformOverride()
extern void CurvedUIInputModule_get_PointerTransformOverride_m1E3D241ACFC1D6E757E8AA210BBBD230175C0073 (void);
// 0x0000002B System.Void CurvedUIInputModule::set_PointerTransformOverride(UnityEngine.Transform)
extern void CurvedUIInputModule_set_PointerTransformOverride_mD46F1D5865E0CCC7D4FF3A7F676EEC8AF0EE80E9 (void);
// 0x0000002C UnityEngine.GameObject CurvedUIInputModule::get_CurrentPointedAt()
extern void CurvedUIInputModule_get_CurrentPointedAt_m9E544751C0B26B9A6455D018597B7B7E600F9154 (void);
// 0x0000002D UnityEngine.Camera CurvedUIInputModule::get_EventCamera()
extern void CurvedUIInputModule_get_EventCamera_m55A6F530658A878D1A470143193927B2ABF8FFD8 (void);
// 0x0000002E System.Void CurvedUIInputModule::set_EventCamera(UnityEngine.Camera)
extern void CurvedUIInputModule_set_EventCamera_mFA4958069001525D55C1A7D9C508C146E4CF2CE8 (void);
// 0x0000002F UnityEngine.Ray CurvedUIInputModule::GetEventRay(UnityEngine.Camera)
extern void CurvedUIInputModule_GetEventRay_m5597EB634A051D1B0FAD09B52BCE14A83E01AA58 (void);
// 0x00000030 UnityEngine.Ray CurvedUIInputModule::get_CustomControllerRay()
extern void CurvedUIInputModule_get_CustomControllerRay_mD775162A32166B7A27EED89C4AD3D0733351A363 (void);
// 0x00000031 System.Void CurvedUIInputModule::set_CustomControllerRay(UnityEngine.Ray)
extern void CurvedUIInputModule_set_CustomControllerRay_m286E2D79D8A2E646CA98C35A9CF05BE5AEE19313 (void);
// 0x00000032 System.Boolean CurvedUIInputModule::get_CustomControllerButtonState()
extern void CurvedUIInputModule_get_CustomControllerButtonState_mE0C18DB1EB150E5964A9EAF418E24C05C79ECC2A (void);
// 0x00000033 System.Void CurvedUIInputModule::set_CustomControllerButtonState(System.Boolean)
extern void CurvedUIInputModule_set_CustomControllerButtonState_mBA5989AB6711974989B046E2870161B59AECDE3E (void);
// 0x00000034 System.Boolean CurvedUIInputModule::get_CustomControllerButtonDown()
extern void CurvedUIInputModule_get_CustomControllerButtonDown_mA5EA5E83C71D5DE523EDBBDFF63DD7483C2537F7 (void);
// 0x00000035 System.Void CurvedUIInputModule::set_CustomControllerButtonDown(System.Boolean)
extern void CurvedUIInputModule_set_CustomControllerButtonDown_m57603955FB8A6508B80FB3EDA44C08C51FDD8464 (void);
// 0x00000036 UnityEngine.Vector2 CurvedUIInputModule::get_WorldSpaceMouseInCanvasSpace()
extern void CurvedUIInputModule_get_WorldSpaceMouseInCanvasSpace_mE3B7299C01A132F0B367A229F83849142947B90A (void);
// 0x00000037 System.Void CurvedUIInputModule::set_WorldSpaceMouseInCanvasSpace(UnityEngine.Vector2)
extern void CurvedUIInputModule_set_WorldSpaceMouseInCanvasSpace_m592AF804859C8A76EE6186EEA766CDB11574C2EF (void);
// 0x00000038 UnityEngine.Vector2 CurvedUIInputModule::get_WorldSpaceMouseInCanvasSpaceDelta()
extern void CurvedUIInputModule_get_WorldSpaceMouseInCanvasSpaceDelta_m8353EE3480DB7777F92F4FF16FD3A41BE65C5E14 (void);
// 0x00000039 System.Single CurvedUIInputModule::get_WorldSpaceMouseSensitivity()
extern void CurvedUIInputModule_get_WorldSpaceMouseSensitivity_m47FD779EED3A1C4C4DFC020254DF87520F776D44 (void);
// 0x0000003A System.Void CurvedUIInputModule::set_WorldSpaceMouseSensitivity(System.Single)
extern void CurvedUIInputModule_set_WorldSpaceMouseSensitivity_mB4EEBB97515A2F87A5110A6AFC29739845999582 (void);
// 0x0000003B System.Boolean CurvedUIInputModule::get_GazeUseTimedClick()
extern void CurvedUIInputModule_get_GazeUseTimedClick_m970ABA37DE32C3DF93F5759AB277484E8CA303A9 (void);
// 0x0000003C System.Void CurvedUIInputModule::set_GazeUseTimedClick(System.Boolean)
extern void CurvedUIInputModule_set_GazeUseTimedClick_m208635934C547289449741B5090E98950F528944 (void);
// 0x0000003D System.Single CurvedUIInputModule::get_GazeClickTimer()
extern void CurvedUIInputModule_get_GazeClickTimer_mE2C0614954C9956711F286589B74FE10BAB9C21C (void);
// 0x0000003E System.Void CurvedUIInputModule::set_GazeClickTimer(System.Single)
extern void CurvedUIInputModule_set_GazeClickTimer_m787E445F77BAE33B13B0AC75982C68C98D1F37A8 (void);
// 0x0000003F System.Single CurvedUIInputModule::get_GazeClickTimerDelay()
extern void CurvedUIInputModule_get_GazeClickTimerDelay_mF176D90CBFEB45FEB704F83EBD421459CDE3AC1D (void);
// 0x00000040 System.Void CurvedUIInputModule::set_GazeClickTimerDelay(System.Single)
extern void CurvedUIInputModule_set_GazeClickTimerDelay_m08937B805F85A87F2755D13A99614EE148F3D533 (void);
// 0x00000041 System.Single CurvedUIInputModule::get_GazeTimerProgress()
extern void CurvedUIInputModule_get_GazeTimerProgress_m0B25A06E1E5F9C52D7B71D380C86C70DBE110AB4 (void);
// 0x00000042 UnityEngine.UI.Image CurvedUIInputModule::get_GazeTimedClickProgressImage()
extern void CurvedUIInputModule_get_GazeTimedClickProgressImage_m1B8B52C8DFECDEABF937662AF5568BB25788C012 (void);
// 0x00000043 System.Void CurvedUIInputModule::set_GazeTimedClickProgressImage(UnityEngine.UI.Image)
extern void CurvedUIInputModule_set_GazeTimedClickProgressImage_m39FB7BD7B8C2CB36120FE9910AFF01EFBDC26172 (void);
// 0x00000044 System.Void CurvedUIInputModule::.ctor()
extern void CurvedUIInputModule__ctor_m97851764FD907046199A7B0FB2181224D0153259 (void);
// 0x00000045 System.Void CurvedUIInputModule::.cctor()
extern void CurvedUIInputModule__cctor_mABCC99D3E956CD146CF7562F29222DB7E9D10E03 (void);
// 0x00000046 System.Void UnityEventFloat::.ctor()
extern void UnityEventFloat__ctor_mFF740EC4E18228A93E8F8B0FFAF5517C3C436522 (void);
// 0x00000047 System.Void UnityEventInt::.ctor()
extern void UnityEventInt__ctor_mA0D74C4DA52EDEE0B64277B447BA1497B85589C1 (void);
// 0x00000048 System.Void UnityEventBool::.ctor()
extern void UnityEventBool__ctor_m750449D2D0FC1AE986D7695E90625E5951435B5F (void);
// 0x00000049 System.Void UnityEventString::.ctor()
extern void UnityEventString__ctor_m05DAE652CAD13F1AD4ECBC288E11380EC61B3EDF (void);
// 0x0000004A System.Void UnityEventByteArray::.ctor()
extern void UnityEventByteArray__ctor_m3E4AAE33E671513548067B0C29F65212D3E5405F (void);
// 0x0000004B System.Void UnityEventFloatArray::.ctor()
extern void UnityEventFloatArray__ctor_m360DF59A91ED20BDEFAC68A7F985A1DB53163987 (void);
// 0x0000004C System.Void UnityEventTexture::.ctor()
extern void UnityEventTexture__ctor_mF6B125236323AD42B8AF7BC65546B89FF77629AE (void);
// 0x0000004D System.Void UnityEventTexture2D::.ctor()
extern void UnityEventTexture2D__ctor_m8604648B368E08DD20E834BE7277BD05C5232422 (void);
// 0x0000004E System.Void UnityEventRenderTexture::.ctor()
extern void UnityEventRenderTexture__ctor_m46862E0E8722739A2D6C008F6B0D191E30EFCC66 (void);
// 0x0000004F System.Void UnityEventWebcamTexture::.ctor()
extern void UnityEventWebcamTexture__ctor_mB672C8E341F209D2BD6DF002DAAD75CE6E894FBA (void);
// 0x00000050 System.Void UnityEventClass::.ctor()
extern void UnityEventClass__ctor_m55E05F3FC82A4B4BD632C6CA0B4A686390301DC6 (void);
// 0x00000051 System.Int32 Loom::get_maxThreads()
extern void Loom_get_maxThreads_m45F4C5CDCCA9EA30915641C7D48A8A1B312CDD2A (void);
// 0x00000052 Loom Loom::get_Current()
extern void Loom_get_Current_mB4398B65D91C82F7B1700229D366D233DE6D5292 (void);
// 0x00000053 System.Void Loom::Awake()
extern void Loom_Awake_mDC841F2F83D833F3CE27C3A8CD7CBEAF0830D006 (void);
// 0x00000054 System.Void Loom::Initialize()
extern void Loom_Initialize_mC9A387246A6232763DD1DDAE10CA8F3BC6DF44F4 (void);
// 0x00000055 System.Void Loom::QueueOnMainThread(System.Action)
extern void Loom_QueueOnMainThread_mC6481FC8BB376138713AFE2B0F847B989FD04799 (void);
// 0x00000056 System.Void Loom::QueueOnMainThread(System.Action,System.Single)
extern void Loom_QueueOnMainThread_m52456F68B754CFBAF8ED1EC2831B60DAD50B8C79 (void);
// 0x00000057 System.Threading.Thread Loom::RunAsync(System.Action)
extern void Loom_RunAsync_mF19ED64A5D75CBD0C9150286CEB6EB97CD3A8CF5 (void);
// 0x00000058 System.Void Loom::RunAction(System.Object)
extern void Loom_RunAction_mCD028185DE1B5145E0FC75CAE05C40AB02DEE520 (void);
// 0x00000059 System.Void Loom::OnDisable()
extern void Loom_OnDisable_mF74455FCA2989CBF239B670FAFBA486C56C17CAB (void);
// 0x0000005A System.Void Loom::Start()
extern void Loom_Start_m3D61391AD1D7486F7955ABFDADD02DDEC12582F8 (void);
// 0x0000005B System.Void Loom::Update()
extern void Loom_Update_m250490A6D28C0384C57E1D62E4E2E11E9DD5B8F5 (void);
// 0x0000005C System.Void Loom::.ctor()
extern void Loom__ctor_m6E368A4B8D9DF24AAD415BD8C82218416C6E8CAF (void);
// 0x0000005D System.Void Loom::.cctor()
extern void Loom__cctor_m27488AF9914F4DAB513FC3566F38776051FF5C85 (void);
// 0x0000005E System.Void Loom/<>c::.cctor()
extern void U3CU3Ec__cctor_mB83C3D68BCDA1E58351A40057BE43CCCB0E30702 (void);
// 0x0000005F System.Void Loom/<>c::.ctor()
extern void U3CU3Ec__ctor_m0EE64F19B66C14511BE1D90EE7D8C21B22C5DF2E (void);
// 0x00000060 System.Boolean Loom/<>c::<Update>b__22_0(Loom/DelayedQueueItem)
extern void U3CU3Ec_U3CUpdateU3Eb__22_0_mA9D277BC34D1B562E09AD8D99662EC0172BFF73A (void);
// 0x00000061 System.Boolean EnergySavingManager::getGyroChanged()
extern void EnergySavingManager_getGyroChanged_m07F73DF70A51F47EE86F9A69F4607DD802388864 (void);
// 0x00000062 System.Boolean EnergySavingManager::getAnyKeyChanged()
extern void EnergySavingManager_getAnyKeyChanged_m2AB138D86D9DCA1A9BEDE38F43D2D0701F77830B (void);
// 0x00000063 System.Boolean EnergySavingManager::getTouchChanged()
extern void EnergySavingManager_getTouchChanged_mC720722117AACE00171FC8994C13764F728039F2 (void);
// 0x00000064 System.Void EnergySavingManager::Awake()
extern void EnergySavingManager_Awake_m90F344C979D39343AB6B36A56204280485089C77 (void);
// 0x00000065 System.Void EnergySavingManager::Start()
extern void EnergySavingManager_Start_m8455DC19313A7FF0EE1817EA99EDF6F04E31D4DB (void);
// 0x00000066 System.Void EnergySavingManager::Update()
extern void EnergySavingManager_Update_mFBC12CCE7FA59B98DEB51178E648F142A88E430F (void);
// 0x00000067 System.Void EnergySavingManager::Action_ReloadScene(System.Int32,System.Int32)
extern void EnergySavingManager_Action_ReloadScene_m4EF9676A6BD46BDADDAAB7454A4B49A943C6D6B4 (void);
// 0x00000068 System.Collections.IEnumerator EnergySavingManager::DelayReloadScene(System.Int32,System.Int32)
extern void EnergySavingManager_DelayReloadScene_mAEB56F9BDDABE85C234AB20E51B7AC97ECF89DF8 (void);
// 0x00000069 System.Void EnergySavingManager::Action_QuitApp(System.Int32,System.Int32)
extern void EnergySavingManager_Action_QuitApp_mA06AE0E144A29BF58C5F142106AA5ADB425BB775 (void);
// 0x0000006A System.Collections.IEnumerator EnergySavingManager::DelayQuitApp(System.Int32,System.Int32)
extern void EnergySavingManager_DelayQuitApp_mEBF1F5293C9099BF208F9F39C9FC3F74AEAC8CD8 (void);
// 0x0000006B System.Void EnergySavingManager::.ctor()
extern void EnergySavingManager__ctor_m0A9D34308EB71860EF4748DB70E3032036A526EB (void);
// 0x0000006C System.Void EnergySavingManager/<DelayReloadScene>d__40::.ctor(System.Int32)
extern void U3CDelayReloadSceneU3Ed__40__ctor_m378410F6A5C31E902391874A3B9E5575B3B47B20 (void);
// 0x0000006D System.Void EnergySavingManager/<DelayReloadScene>d__40::System.IDisposable.Dispose()
extern void U3CDelayReloadSceneU3Ed__40_System_IDisposable_Dispose_m3B3AA69F100D170E2074348E2D09995D587E2255 (void);
// 0x0000006E System.Boolean EnergySavingManager/<DelayReloadScene>d__40::MoveNext()
extern void U3CDelayReloadSceneU3Ed__40_MoveNext_mEDAC1CD91D991D6237B37DCF66AD265EF183AA2D (void);
// 0x0000006F System.Object EnergySavingManager/<DelayReloadScene>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayReloadSceneU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0596AA92419A6E79BDE7EF5C75FD5B91836ECD10 (void);
// 0x00000070 System.Void EnergySavingManager/<DelayReloadScene>d__40::System.Collections.IEnumerator.Reset()
extern void U3CDelayReloadSceneU3Ed__40_System_Collections_IEnumerator_Reset_m4C23331135E8E94BE05C1C7FF40BA5BAB83F6D4A (void);
// 0x00000071 System.Object EnergySavingManager/<DelayReloadScene>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CDelayReloadSceneU3Ed__40_System_Collections_IEnumerator_get_Current_m2F19C025F98F1EF3C47A3E00BD88A14E1160EAB4 (void);
// 0x00000072 System.Void EnergySavingManager/<DelayQuitApp>d__42::.ctor(System.Int32)
extern void U3CDelayQuitAppU3Ed__42__ctor_m2982F7BD45D512C22B8071F2A16CD7BC0AEC5D3E (void);
// 0x00000073 System.Void EnergySavingManager/<DelayQuitApp>d__42::System.IDisposable.Dispose()
extern void U3CDelayQuitAppU3Ed__42_System_IDisposable_Dispose_m4ED41B74ADAA3AF5E127CD18D01851957230A57D (void);
// 0x00000074 System.Boolean EnergySavingManager/<DelayQuitApp>d__42::MoveNext()
extern void U3CDelayQuitAppU3Ed__42_MoveNext_mB96130378F55019EF308216C5027E466F724D3DE (void);
// 0x00000075 System.Object EnergySavingManager/<DelayQuitApp>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayQuitAppU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB3215365FC516F5429FC10B1470F7AF8DF2FCC8 (void);
// 0x00000076 System.Void EnergySavingManager/<DelayQuitApp>d__42::System.Collections.IEnumerator.Reset()
extern void U3CDelayQuitAppU3Ed__42_System_Collections_IEnumerator_Reset_m10AEA39667252F8C0366EE02D14D0FFEB53C87E6 (void);
// 0x00000077 System.Object EnergySavingManager/<DelayQuitApp>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CDelayQuitAppU3Ed__42_System_Collections_IEnumerator_get_Current_m3AEFD1FB81A442FD1677D5653F954536DCA624D8 (void);
// 0x00000078 System.Void FPSManager::Update()
extern void FPSManager_Update_m0522EFA7F4F133BC59618DFA4DFAC12C2087969C (void);
// 0x00000079 System.Void FPSManager::Action_SetFPS(System.Int32)
extern void FPSManager_Action_SetFPS_mBC76DAE4286ABF5D9BD9EF6CA162D8686071F564 (void);
// 0x0000007A System.Void FPSManager::.ctor()
extern void FPSManager__ctor_m2E6A21D5CEDD1BA8884A37F175EA3D71BA883093 (void);
// 0x0000007B System.Void DemoObjectAnimation::Start()
extern void DemoObjectAnimation_Start_m61B26A3EEFEE316B23A692EB9819B0936A33B5AD (void);
// 0x0000007C System.Void DemoObjectAnimation::Update()
extern void DemoObjectAnimation_Update_m0BCB030740300DA20DF73C47E1C680BD50CAD216 (void);
// 0x0000007D System.Void DemoObjectAnimation::.ctor()
extern void DemoObjectAnimation__ctor_m4E17FDCB4EBA99D7C84551E9D42244E8AEF105C6 (void);
// 0x0000007E System.Void DemoScreenshot::Reset()
extern void DemoScreenshot_Reset_mBE57397437DFA75D8960EFB2962650F67307C8EA (void);
// 0x0000007F System.Void DemoScreenshot::Update()
extern void DemoScreenshot_Update_mC462597259A047ED1AE56828BDDE993DC74E42AD (void);
// 0x00000080 System.Void DemoScreenshot::SaveScreenshot()
extern void DemoScreenshot_SaveScreenshot_mBA35D8AD7F8B3F7BEF357653309DDF826C3BB955 (void);
// 0x00000081 System.Collections.IEnumerator DemoScreenshot::SaveJPG(System.String)
extern void DemoScreenshot_SaveJPG_mDD3390BF87787F36FDDFB828F593EDE5B99E320B (void);
// 0x00000082 System.Collections.IEnumerator DemoScreenshot::SaveScreenshotPano()
extern void DemoScreenshot_SaveScreenshotPano_mB0F592F9B03FE248F5A8AE1B95B06535936F1964 (void);
// 0x00000083 System.Void DemoScreenshot::.ctor()
extern void DemoScreenshot__ctor_mBA024C4EDF18B31CCA84DC824CADE928BA036570 (void);
// 0x00000084 System.Void DemoScreenshot/<SaveJPG>d__10::.ctor(System.Int32)
extern void U3CSaveJPGU3Ed__10__ctor_m80B017FB7621082559EE1234D41D7B212334D6AD (void);
// 0x00000085 System.Void DemoScreenshot/<SaveJPG>d__10::System.IDisposable.Dispose()
extern void U3CSaveJPGU3Ed__10_System_IDisposable_Dispose_m6242FD59BB6674148EF1EE49B9BAD2989B3A891E (void);
// 0x00000086 System.Boolean DemoScreenshot/<SaveJPG>d__10::MoveNext()
extern void U3CSaveJPGU3Ed__10_MoveNext_mA8014B6A1489057D93EAA6D564968F0B67A47905 (void);
// 0x00000087 System.Object DemoScreenshot/<SaveJPG>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSaveJPGU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9A0AAB82531970F1329EA3AA22821C7522D0527F (void);
// 0x00000088 System.Void DemoScreenshot/<SaveJPG>d__10::System.Collections.IEnumerator.Reset()
extern void U3CSaveJPGU3Ed__10_System_Collections_IEnumerator_Reset_m48FAB3D5B99C2D5767FBC8C88D78078B971E8D46 (void);
// 0x00000089 System.Object DemoScreenshot/<SaveJPG>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CSaveJPGU3Ed__10_System_Collections_IEnumerator_get_Current_m366D57B80A453196657D362CA20A1D7773F1F179 (void);
// 0x0000008A System.Void DemoScreenshot/<SaveScreenshotPano>d__11::.ctor(System.Int32)
extern void U3CSaveScreenshotPanoU3Ed__11__ctor_m8FAF53D59B7743762915EFC238292666D39F461D (void);
// 0x0000008B System.Void DemoScreenshot/<SaveScreenshotPano>d__11::System.IDisposable.Dispose()
extern void U3CSaveScreenshotPanoU3Ed__11_System_IDisposable_Dispose_mCA343FD70613D0F0B5FBFFD4CC01D208CB2A7ECE (void);
// 0x0000008C System.Boolean DemoScreenshot/<SaveScreenshotPano>d__11::MoveNext()
extern void U3CSaveScreenshotPanoU3Ed__11_MoveNext_m6D5CEB4D0CB57C64E1DF0323266D39FEBFD0D899 (void);
// 0x0000008D System.Object DemoScreenshot/<SaveScreenshotPano>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSaveScreenshotPanoU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3291B88A4B56AC7F8B4A38146C4265838FCD4058 (void);
// 0x0000008E System.Void DemoScreenshot/<SaveScreenshotPano>d__11::System.Collections.IEnumerator.Reset()
extern void U3CSaveScreenshotPanoU3Ed__11_System_Collections_IEnumerator_Reset_m1E37A2C1B0FFB2103292655CE06EAB86055855E5 (void);
// 0x0000008F System.Object DemoScreenshot/<SaveScreenshotPano>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CSaveScreenshotPanoU3Ed__11_System_Collections_IEnumerator_get_Current_m4831A82DF401B704EF1149B943E6457A760BDBB7 (void);
// 0x00000090 System.Void FillCameraBackground::Start()
extern void FillCameraBackground_Start_m83A28EE7F248150FB6DF327464F96375F753CE70 (void);
// 0x00000091 System.Void FillCameraBackground::LateUpdate()
extern void FillCameraBackground_LateUpdate_mE11227CEA4E886E71D218E049DE251E022233995 (void);
// 0x00000092 System.Void FillCameraBackground::.ctor()
extern void FillCameraBackground__ctor_m1DD54F12BB631FD917CF7069BE7A4799137A6168 (void);
// 0x00000093 System.Void GyroController::Awake()
extern void GyroController_Awake_m9DE28977F11F876B392510E0A106EFF171DF6670 (void);
// 0x00000094 System.Void GyroController::Start()
extern void GyroController_Start_m74034C95FD00F13D038ABC9B1E7D20DD54E315FF (void);
// 0x00000095 System.Boolean GyroController::EnableGyro()
extern void GyroController_EnableGyro_m4E6CA669420E086E9D1798093E40D1D1565827F9 (void);
// 0x00000096 System.Void GyroController::Update()
extern void GyroController_Update_m7DC451D70AC3512B5EC4A9A77EDE97D546545841 (void);
// 0x00000097 System.Void GyroController::AimingOffset(System.Single)
extern void GyroController_AimingOffset_mCFC1D9E3581EFEC7A69D889E90F45553A92CAB4F (void);
// 0x00000098 System.Void GyroController::MouseDragRot()
extern void GyroController_MouseDragRot_mC021B1138BDCFAE31801668F2C6F359AB41CEC94 (void);
// 0x00000099 System.Void GyroController::TouchDragRot()
extern void GyroController_TouchDragRot_mAEB8AA78851F7B15648A929FCC5267CCFE09A41C (void);
// 0x0000009A System.Void GyroController::.ctor()
extern void GyroController__ctor_m8F76C3F9F945BDE364299A3C3E23A92E44B57442 (void);
// 0x0000009B System.Void _UnityEventFloat::.ctor()
extern void _UnityEventFloat__ctor_m3C7D96090897BCAB60F0D082A4AE1D263BC41C83 (void);
// 0x0000009C System.Void PointerEvents::Start()
extern void PointerEvents_Start_mEAE7F0645485260A2956330EFF57358846B59763 (void);
// 0x0000009D System.Void PointerEvents::Update()
extern void PointerEvents_Update_mB5E4941825808D9B099210CE3A95C551CF3E30CA (void);
// 0x0000009E System.Void PointerEvents::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerDown_m72CD1D5401C5938F6E2A221C94C772D99E8F5AEF (void);
// 0x0000009F System.Void PointerEvents::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerUp_mC2DA25B8BC3E0C4C3EBC979491114093EC70E986 (void);
// 0x000000A0 System.Void PointerEvents::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerEnter_m16550B1C48E09257AB2C0555F508344E48BDF6D5 (void);
// 0x000000A1 System.Void PointerEvents::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerExit_m6A148D6CDAE4DF94D188B53AEED69DDF6C4F30CD (void);
// 0x000000A2 System.Void PointerEvents::.ctor()
extern void PointerEvents__ctor_mC7FF3430D7AAAEBCDA95C0723A59B2C3FE292441 (void);
// 0x000000A3 System.Void TouchVisualization::Start()
extern void TouchVisualization_Start_mE98CFC665943969CA267FA257A8DD00D675AF833 (void);
// 0x000000A4 System.Void TouchVisualization::Update()
extern void TouchVisualization_Update_m073D885C35E8E17C453FFDE621516C80CF31F585 (void);
// 0x000000A5 System.Void TouchVisualization::.ctor()
extern void TouchVisualization__ctor_m73E55042EACC2476F91AD811F756D3D529C7EEE7 (void);
// 0x000000A6 System.Void OffScreenTrigger::Start()
extern void OffScreenTrigger_Start_mFA3F5D962CC09261A9ED9F97304303A5D16DEDD8 (void);
// 0x000000A7 System.Void OffScreenTrigger::Update()
extern void OffScreenTrigger_Update_m8B50798E9369E64B38A1B5D8418576BBA8F2EB9E (void);
// 0x000000A8 System.Void OffScreenTrigger::Action_offscreen()
extern void OffScreenTrigger_Action_offscreen_m309DEA55FA4E1AC8763EBC43F489816678358F87 (void);
// 0x000000A9 System.Void OffScreenTrigger::.ctor()
extern void OffScreenTrigger__ctor_mEF41D1BAEF64AE177D8B860AEE3E39FF5CD6189E (void);
// 0x000000AA System.Void WorldToScreenSpace::Start()
extern void WorldToScreenSpace_Start_m02CD9588E96DE188C6C7AF4BA467CF7E4AE44D4E (void);
// 0x000000AB System.Void WorldToScreenSpace::Update()
extern void WorldToScreenSpace_Update_mA51732AEF18A583B410F4C5AFB11B51C7C0EFD0A (void);
// 0x000000AC System.Void WorldToScreenSpace::InvokeOnScreen()
extern void WorldToScreenSpace_InvokeOnScreen_m2518C7AA233A5C5526E909F9E1568C6B0EF8DA20 (void);
// 0x000000AD System.Void WorldToScreenSpace::InvokeOffScreen()
extern void WorldToScreenSpace_InvokeOffScreen_m032EEC8A29DD2B163EB6811DE9444C8D40E2DDF7 (void);
// 0x000000AE System.Void WorldToScreenSpace::OnDisable()
extern void WorldToScreenSpace_OnDisable_mBD6D5A879F6180931A4898C1253825496882F1E1 (void);
// 0x000000AF System.Void WorldToScreenSpace::OnEnable()
extern void WorldToScreenSpace_OnEnable_m6DD74E6045A5E0AB9CBC4961187462E0814BEE52 (void);
// 0x000000B0 System.Void WorldToScreenSpace::DebugTest(System.String)
extern void WorldToScreenSpace_DebugTest_m333E5F31F445884C07702D81B95F89ED658517F9 (void);
// 0x000000B1 System.Void WorldToScreenSpace::.ctor()
extern void WorldToScreenSpace__ctor_m697DF27F1AB8AEA6D847524FD22698F489C395E5 (void);
// 0x000000B2 System.Void ZoomManager::Start()
extern void ZoomManager_Start_m5564B07802ECD1890FD37C118660F9341DB70A78 (void);
// 0x000000B3 System.Void ZoomManager::Update()
extern void ZoomManager_Update_mFFB0DB68EDECC1CEE73F6E9E61F6FD357D479AC3 (void);
// 0x000000B4 System.Void ZoomManager::GestureZoom()
extern void ZoomManager_GestureZoom_m27472A4680B46B9EE14AFBDEC415E026A0A4DB5A (void);
// 0x000000B5 System.Void ZoomManager::ZoomIn()
extern void ZoomManager_ZoomIn_mF9DED265600855232D21C366D41872E02BA0A10A (void);
// 0x000000B6 System.Void ZoomManager::ZoomOut()
extern void ZoomManager_ZoomOut_m30DE34CC9DECAF76CE557B460C65890B50F00BA5 (void);
// 0x000000B7 System.Void ZoomManager::.ctor()
extern void ZoomManager__ctor_m2DC1B15361F9233ED42BE447B6FED5D57C6DC1FE (void);
// 0x000000B8 System.Void SceneManager_helper::Action_LoadSceneName(System.String)
extern void SceneManager_helper_Action_LoadSceneName_mE45650E0ED0D6B19B9622967E9F9EA814756ADE7 (void);
// 0x000000B9 System.Void SceneManager_helper::Action_Quit()
extern void SceneManager_helper_Action_Quit_mAA22B68E3E7D071D9B1F083DE0BB4F806741DFD3 (void);
// 0x000000BA System.Void SceneManager_helper::.ctor()
extern void SceneManager_helper__ctor_mD59AEDC0E2B8F355D19F9385C530AE69F190FF8A (void);
// 0x000000BB System.Void TargetProjectionMatrix::Action_UpdateFOV(System.Single)
extern void TargetProjectionMatrix_Action_UpdateFOV_m15AB3F37BA03F650029246E7BB3E932318982552 (void);
// 0x000000BC System.Void TargetProjectionMatrix::Action_SetAllowUpdate(System.Boolean)
extern void TargetProjectionMatrix_Action_SetAllowUpdate_mB1330AEA23BB4D0A116F9806F0EC2667C1A62E1F (void);
// 0x000000BD System.Void TargetProjectionMatrix::Action_SetForceDisableUpdate(System.Boolean)
extern void TargetProjectionMatrix_Action_SetForceDisableUpdate_mDF1165DE2FCCD1B816C77EC184733E61B3D5AED6 (void);
// 0x000000BE System.Collections.IEnumerator TargetProjectionMatrix::UpdateProjectionMatrixLoop()
extern void TargetProjectionMatrix_UpdateProjectionMatrixLoop_m9BBD15A81609B32704AD6F714CBB2FFF25F0F02B (void);
// 0x000000BF System.Void TargetProjectionMatrix::Start()
extern void TargetProjectionMatrix_Start_m4859CF1A76FB4A432DCD561251C907A6D22BE2BC (void);
// 0x000000C0 System.Void TargetProjectionMatrix::.ctor()
extern void TargetProjectionMatrix__ctor_mFCA533A6831AC695D313F3B6600804F52D226894 (void);
// 0x000000C1 System.Void TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::.ctor(System.Int32)
extern void U3CUpdateProjectionMatrixLoopU3Ed__10__ctor_m0C230674C5D40700A51231AB15CD4362B42129F5 (void);
// 0x000000C2 System.Void TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.IDisposable.Dispose()
extern void U3CUpdateProjectionMatrixLoopU3Ed__10_System_IDisposable_Dispose_mCC990D854F67E1FEAB8C5942350714360CAA6C4B (void);
// 0x000000C3 System.Boolean TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::MoveNext()
extern void U3CUpdateProjectionMatrixLoopU3Ed__10_MoveNext_m60419720F9995FAD7FF38AEB23CF2CD5660607D3 (void);
// 0x000000C4 System.Object TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3DC7F959E369D83651E676D2FFCF66AE85A2C36 (void);
// 0x000000C5 System.Void TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.Collections.IEnumerator.Reset()
extern void U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_Reset_m70A89E669D02A74E6DAABC101346075B8F0F3F25 (void);
// 0x000000C6 System.Object TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_get_Current_m7071709350C18754652B239E532208352E7E4601 (void);
// 0x000000C7 System.Void WebcamManager::Action_useFrontCam(System.Boolean)
extern void WebcamManager_Action_useFrontCam_m96E70AB022B4E8FAC541A9D225DC85B01A3D0F62 (void);
// 0x000000C8 System.Collections.IEnumerator WebcamManager::RestartWebcam()
extern void WebcamManager_RestartWebcam_m606BBE4978CDDE71EFC322393B370501657C3288 (void);
// 0x000000C9 UnityEngine.WebCamTexture WebcamManager::get_WebCamTexture()
extern void WebcamManager_get_WebCamTexture_mDAA40C51424298F9C63A01CA51B61BECC6C9407F (void);
// 0x000000CA System.Boolean WebcamManager::get_IsFlipped()
extern void WebcamManager_get_IsFlipped_mC198CF5C254EAD69D7F688DDCF103C9C2839C2C6 (void);
// 0x000000CB System.Void WebcamManager::Start()
extern void WebcamManager_Start_m19A031E1F1F000DD3083AB2A9D97D0CA740AFBC0 (void);
// 0x000000CC System.Void WebcamManager::Update()
extern void WebcamManager_Update_m70BFF30AF71CB1ECE20E1905878A8D0C5A63DE81 (void);
// 0x000000CD System.Void WebcamManager::OnApplicationQuit()
extern void WebcamManager_OnApplicationQuit_mBE25BE76FD98A3709C1010E6D506E06BE9FA5A63 (void);
// 0x000000CE System.Void WebcamManager::CalculateBackgroundQuad()
extern void WebcamManager_CalculateBackgroundQuad_mA707C37E7AF9177F385C64094F4CD2DA99F6B6F7 (void);
// 0x000000CF System.Void WebcamManager::Action_StopWebcam()
extern void WebcamManager_Action_StopWebcam_m3B387E8BCA377260DF46C7189AD053032E173C1A (void);
// 0x000000D0 System.Void WebcamManager::Action_StartWebcam()
extern void WebcamManager_Action_StartWebcam_mE2758C0B9DC4BF39E7BBA1405C726E8C54C24198 (void);
// 0x000000D1 System.Void WebcamManager::OnEnable()
extern void WebcamManager_OnEnable_mF52E952C81D735CB23AC17B768CBDEC35E0B4A47 (void);
// 0x000000D2 System.Void WebcamManager::OnDisable()
extern void WebcamManager_OnDisable_mF7D0A167F8E6D6765DFFB1DB4658F0D43E1BD88A (void);
// 0x000000D3 System.Collections.IEnumerator WebcamManager::initAndWaitForWebCamTexture()
extern void WebcamManager_initAndWaitForWebCamTexture_m6475CE2D9148E36CF76C11720BB8EBC75C3E8494 (void);
// 0x000000D4 System.Void WebcamManager::.ctor()
extern void WebcamManager__ctor_m1C5A3E28015B0ACB88EAA02428040165C9B0BD76 (void);
// 0x000000D5 System.Void WebcamManager/<RestartWebcam>d__8::.ctor(System.Int32)
extern void U3CRestartWebcamU3Ed__8__ctor_m28872094CCAA094B43F22696A627F91DB681702E (void);
// 0x000000D6 System.Void WebcamManager/<RestartWebcam>d__8::System.IDisposable.Dispose()
extern void U3CRestartWebcamU3Ed__8_System_IDisposable_Dispose_m64D07C3BFF909EE7CCCBBF13483490CDFD984122 (void);
// 0x000000D7 System.Boolean WebcamManager/<RestartWebcam>d__8::MoveNext()
extern void U3CRestartWebcamU3Ed__8_MoveNext_m0073A78F7D1343C48FDB671B463ED829CDA3C822 (void);
// 0x000000D8 System.Object WebcamManager/<RestartWebcam>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestartWebcamU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA6D2C7491AB5B6855FC00F3F90253FC3DB64DCC3 (void);
// 0x000000D9 System.Void WebcamManager/<RestartWebcam>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_Reset_mA3A746F18872D6F97B3CC5A4977A1F692BA7E8EA (void);
// 0x000000DA System.Object WebcamManager/<RestartWebcam>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_get_Current_m0D001B2B663BA976A2D539566D366D6BCB620327 (void);
// 0x000000DB System.Void WebcamManager/<initAndWaitForWebCamTexture>d__34::.ctor(System.Int32)
extern void U3CinitAndWaitForWebCamTextureU3Ed__34__ctor_m52FA2EEEB153A88A5AA5BAA71AB02EA1EF8C4D7E (void);
// 0x000000DC System.Void WebcamManager/<initAndWaitForWebCamTexture>d__34::System.IDisposable.Dispose()
extern void U3CinitAndWaitForWebCamTextureU3Ed__34_System_IDisposable_Dispose_m2727360BB45FFE1C4D81B8AF9B1B04400CD74F47 (void);
// 0x000000DD System.Boolean WebcamManager/<initAndWaitForWebCamTexture>d__34::MoveNext()
extern void U3CinitAndWaitForWebCamTextureU3Ed__34_MoveNext_m42785171471996265574A92AFC79AFFB0ADEFE23 (void);
// 0x000000DE System.Object WebcamManager/<initAndWaitForWebCamTexture>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03936395905AC4CD42FCADDBCFDC81AA99AB1180 (void);
// 0x000000DF System.Void WebcamManager/<initAndWaitForWebCamTexture>d__34::System.Collections.IEnumerator.Reset()
extern void U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_Reset_m40B8DC7783ACD7CDF9D80B46263092CF7CCD2C02 (void);
// 0x000000E0 System.Object WebcamManager/<initAndWaitForWebCamTexture>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_get_Current_m1B82795B8517450D9B2E0420375A1E3D234BE130 (void);
// 0x000000E1 System.Void AudioDecoder::Start()
extern void AudioDecoder_Start_m4A47FE23BB46592FEFB8827CBC0ECE4ABE8ECDC0 (void);
// 0x000000E2 System.Void AudioDecoder::Action_ProcessData(System.Byte[])
extern void AudioDecoder_Action_ProcessData_m4E6B0BDDDE0AAB0C6CE8A9AD25E72347FDDEF241 (void);
// 0x000000E3 System.Single AudioDecoder::get_Volume()
extern void AudioDecoder_get_Volume_m474CC6458E9DBF6C3E05B1A20D89D37A3FFA0EDB (void);
// 0x000000E4 System.Void AudioDecoder::set_Volume(System.Single)
extern void AudioDecoder_set_Volume_mC3D8A106DE0D8B8804AC0BF45D0F1AA3187337FF (void);
// 0x000000E5 System.Collections.IEnumerator AudioDecoder::ProcessAudioData(System.Byte[])
extern void AudioDecoder_ProcessAudioData_mCD60D45A102AC845C95F710E97ED9AC84764608D (void);
// 0x000000E6 System.Void AudioDecoder::CreateClip()
extern void AudioDecoder_CreateClip_mD97DF673B8DDAA7643B53CE9FEB39BAB86BE5689 (void);
// 0x000000E7 System.Void AudioDecoder::OnAudioRead(System.Single[])
extern void AudioDecoder_OnAudioRead_m1794D32AD1D7DB0ECA7AF488FD848B26C2F30915 (void);
// 0x000000E8 System.Void AudioDecoder::OnAudioSetPosition(System.Int32)
extern void AudioDecoder_OnAudioSetPosition_m6188583CB8F1CB8E8C3B1BA6283B6B259709B919 (void);
// 0x000000E9 System.Single[] AudioDecoder::ToFloatArray(System.Byte[])
extern void AudioDecoder_ToFloatArray_m8E8BA27CA0333C1DAA52FD0067109FC5AFA24439 (void);
// 0x000000EA System.Void AudioDecoder::.ctor()
extern void AudioDecoder__ctor_m0BBC264F0BE17A77E373B38972CC77292FC76DB4 (void);
// 0x000000EB System.Void AudioDecoder/<ProcessAudioData>d__19::.ctor(System.Int32)
extern void U3CProcessAudioDataU3Ed__19__ctor_mE0848B84AE84FDE79B6FE343E3D683EAA49E7970 (void);
// 0x000000EC System.Void AudioDecoder/<ProcessAudioData>d__19::System.IDisposable.Dispose()
extern void U3CProcessAudioDataU3Ed__19_System_IDisposable_Dispose_mD25A519AED3454A362024DFC8D2ED00C90718B84 (void);
// 0x000000ED System.Boolean AudioDecoder/<ProcessAudioData>d__19::MoveNext()
extern void U3CProcessAudioDataU3Ed__19_MoveNext_mC6B474AD5047D98A69C3D5BA6F10AC0F22AABBD5 (void);
// 0x000000EE System.Object AudioDecoder/<ProcessAudioData>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessAudioDataU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD07EFA8A3BBF9FB0487C83D2C8F5CFCB6BC8D8F4 (void);
// 0x000000EF System.Void AudioDecoder/<ProcessAudioData>d__19::System.Collections.IEnumerator.Reset()
extern void U3CProcessAudioDataU3Ed__19_System_Collections_IEnumerator_Reset_m1AC29B6F730B329545A2840B093F62B10215D6E4 (void);
// 0x000000F0 System.Object AudioDecoder/<ProcessAudioData>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CProcessAudioDataU3Ed__19_System_Collections_IEnumerator_get_Current_m7D17C6D80A40F7A84F86140A78B47A3A680A64D6 (void);
// 0x000000F1 System.Void AudioEncoder::Start()
extern void AudioEncoder_Start_mC6E27FB33C1A93F4E0AAEA967F0330D62D494ACA (void);
// 0x000000F2 System.Void AudioEncoder::OnAudioFilterRead(System.Single[],System.Int32)
extern void AudioEncoder_OnAudioFilterRead_m4B02FEBA050FF299DFEF08DB2EB85BCF67A62F0F (void);
// 0x000000F3 System.Collections.IEnumerator AudioEncoder::SenderCOR()
extern void AudioEncoder_SenderCOR_m927CA7B10E5E48E0E83B7E0E88BBA863ADD50878 (void);
// 0x000000F4 System.Int16 AudioEncoder::FloatToInt16(System.Single)
extern void AudioEncoder_FloatToInt16_mA700BD6EF5E0C453B7039DCC969472464FBBA851 (void);
// 0x000000F5 System.Void AudioEncoder::EncodeBytes()
extern void AudioEncoder_EncodeBytes_m5F714A2D8AFBBA5277E66BBFC6D8BA03B1882A26 (void);
// 0x000000F6 System.Void AudioEncoder::OnEnable()
extern void AudioEncoder_OnEnable_m11999C31B52870FC6809BF7AD22834E2A09479A7 (void);
// 0x000000F7 System.Void AudioEncoder::OnDisable()
extern void AudioEncoder_OnDisable_mD3A71D2BD0DF0FD4345D1E6CBAB7058A370408B1 (void);
// 0x000000F8 System.Void AudioEncoder::.ctor()
extern void AudioEncoder__ctor_mE955CC6B5EC97CA461D6523FF7A89131C07B8A9B (void);
// 0x000000F9 System.Void AudioEncoder/<SenderCOR>d__22::.ctor(System.Int32)
extern void U3CSenderCORU3Ed__22__ctor_mA784695FBBA952AEDA2F1836217E9B9532533B30 (void);
// 0x000000FA System.Void AudioEncoder/<SenderCOR>d__22::System.IDisposable.Dispose()
extern void U3CSenderCORU3Ed__22_System_IDisposable_Dispose_mFC00E9099AB61671A87DC369640809AB166C9B93 (void);
// 0x000000FB System.Boolean AudioEncoder/<SenderCOR>d__22::MoveNext()
extern void U3CSenderCORU3Ed__22_MoveNext_m6DCC893FF65CBDC6CA8E93EF0366A9E428171743 (void);
// 0x000000FC System.Object AudioEncoder/<SenderCOR>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSenderCORU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79A57D218C729C4CBB8F88B7771BF63A351DD582 (void);
// 0x000000FD System.Void AudioEncoder/<SenderCOR>d__22::System.Collections.IEnumerator.Reset()
extern void U3CSenderCORU3Ed__22_System_Collections_IEnumerator_Reset_m47EF41E4C31BD26B8B93922AD1CFBEEC2D43F057 (void);
// 0x000000FE System.Object AudioEncoder/<SenderCOR>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CSenderCORU3Ed__22_System_Collections_IEnumerator_get_Current_m3CD170DF647398653FD6BC8C76779BF24305CFB5 (void);
// 0x000000FF System.Void FMPCStreamDecoder::Reset()
extern void FMPCStreamDecoder_Reset_mD516E2837782C0998AA13DBD160B69CA36765313 (void);
// 0x00000100 System.Void FMPCStreamDecoder::Start()
extern void FMPCStreamDecoder_Start_mD154DAB53A3D993DCAA6643C3065480826BFD594 (void);
// 0x00000101 System.Void FMPCStreamDecoder::Update()
extern void FMPCStreamDecoder_Update_mB513E3EBCEAE9931F4EC96A44DA39C3A7B28E245 (void);
// 0x00000102 System.Void FMPCStreamDecoder::Action_ProcessPointCloudData(System.Byte[])
extern void FMPCStreamDecoder_Action_ProcessPointCloudData_m9AE4E232B791A9A13EB1EB6FD7B2432340B78F50 (void);
// 0x00000103 System.Collections.IEnumerator FMPCStreamDecoder::ProcessImageData(System.Byte[])
extern void FMPCStreamDecoder_ProcessImageData_m462C8F6C2E0CFFE88CC4C7348CFDFC53B95382E0 (void);
// 0x00000104 System.Void FMPCStreamDecoder::OnDisable()
extern void FMPCStreamDecoder_OnDisable_mF185D10601FCC5F39D5939E2D27665B48F7CEC88 (void);
// 0x00000105 System.Void FMPCStreamDecoder::Action_ProcessImage(UnityEngine.Texture2D)
extern void FMPCStreamDecoder_Action_ProcessImage_m9DF4F10AC003E9D116CA53446641969BE0D2D757 (void);
// 0x00000106 System.Void FMPCStreamDecoder::.ctor()
extern void FMPCStreamDecoder__ctor_mE90CEB60B0B3E3E41F5699E046603FCCD8FE257E (void);
// 0x00000107 System.Void FMPCStreamDecoder/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m65D7C3C6C9BADEA6DCADB46D1CC86A9AA1B6B45B (void);
// 0x00000108 System.Void FMPCStreamDecoder/<>c__DisplayClass28_1::.ctor()
extern void U3CU3Ec__DisplayClass28_1__ctor_m1B611E718B7F8879DAE5C1D87C4217673B75BF80 (void);
// 0x00000109 System.Void FMPCStreamDecoder/<>c__DisplayClass28_1::<ProcessImageData>b__0()
extern void U3CU3Ec__DisplayClass28_1_U3CProcessImageDataU3Eb__0_m6E156A0A7C817E9FA82241C2F69668A787A58D2C (void);
// 0x0000010A System.Void FMPCStreamDecoder/<ProcessImageData>d__28::.ctor(System.Int32)
extern void U3CProcessImageDataU3Ed__28__ctor_m65E9267CA3923A1ED5273964813B34E414E6C1A8 (void);
// 0x0000010B System.Void FMPCStreamDecoder/<ProcessImageData>d__28::System.IDisposable.Dispose()
extern void U3CProcessImageDataU3Ed__28_System_IDisposable_Dispose_mB132AB81D7B361538A451E56DE6288BB00B415BC (void);
// 0x0000010C System.Boolean FMPCStreamDecoder/<ProcessImageData>d__28::MoveNext()
extern void U3CProcessImageDataU3Ed__28_MoveNext_m8636F87D9C47E6BD28A5381F5A4DDD8D45869B63 (void);
// 0x0000010D System.Object FMPCStreamDecoder/<ProcessImageData>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessImageDataU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1CB91832319CBA52A62C70735673C95257DC1CA (void);
// 0x0000010E System.Void FMPCStreamDecoder/<ProcessImageData>d__28::System.Collections.IEnumerator.Reset()
extern void U3CProcessImageDataU3Ed__28_System_Collections_IEnumerator_Reset_mFEDBB74A6DDD44A49D4215E3BA8D403DE3B85009 (void);
// 0x0000010F System.Object FMPCStreamDecoder/<ProcessImageData>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CProcessImageDataU3Ed__28_System_Collections_IEnumerator_get_Current_m24D648EBC336DA7161885D872B2308FDA4EE1EE5 (void);
// 0x00000110 System.Void FMPCStreamEncoder::Reset()
extern void FMPCStreamEncoder_Reset_m060C641D90DB243600DE6F6C690517E0EAB94279 (void);
// 0x00000111 System.Void FMPCStreamEncoder::CheckResolution()
extern void FMPCStreamEncoder_CheckResolution_m9383BE69C4C0D139BD3231004E2DD69E8F14735A (void);
// 0x00000112 System.Void FMPCStreamEncoder::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void FMPCStreamEncoder_OnRenderImage_mFDBC0F48C15582A61E93094FF1D09F0C7356AC03 (void);
// 0x00000113 System.Void FMPCStreamEncoder::LateUpdate()
extern void FMPCStreamEncoder_LateUpdate_mEE1717ABC183EECB1B06F8014F8CB2FA77F33906 (void);
// 0x00000114 System.Void FMPCStreamEncoder::RequestTextureUpdate()
extern void FMPCStreamEncoder_RequestTextureUpdate_m3BE507F6BB313CD894712B96CA114C9E68695689 (void);
// 0x00000115 System.Boolean FMPCStreamEncoder::get_SupportsAsyncGPUReadback()
extern void FMPCStreamEncoder_get_SupportsAsyncGPUReadback_m54B799AF9C5A6523897FC4034DF29F3947B2F612 (void);
// 0x00000116 UnityEngine.Texture FMPCStreamEncoder::get_GetStreamTexture()
extern void FMPCStreamEncoder_get_GetStreamTexture_m6170C92092A4E1BF2A8EDC55ECB8A9E3D776B9B1 (void);
// 0x00000117 System.Void FMPCStreamEncoder::ProcessCapturedTexture()
extern void FMPCStreamEncoder_ProcessCapturedTexture_m93ED4CDD6B12A4634112D011E1DF57FADB5623F9 (void);
// 0x00000118 System.Collections.IEnumerator FMPCStreamEncoder::ProcessCapturedTextureCOR()
extern void FMPCStreamEncoder_ProcessCapturedTextureCOR_m06E07C7C35D38473F939CE051118329E708FE270 (void);
// 0x00000119 System.Collections.IEnumerator FMPCStreamEncoder::ProcessCapturedTextureGPUReadbackCOR()
extern void FMPCStreamEncoder_ProcessCapturedTextureGPUReadbackCOR_m17EC58D224FFFA01491BB4B14C7139CCD8CBF25B (void);
// 0x0000011A System.Collections.IEnumerator FMPCStreamEncoder::EncodeBytes(System.Byte[])
extern void FMPCStreamEncoder_EncodeBytes_mE685726FB5710F700D1DE50896F43383AE8B15F1 (void);
// 0x0000011B System.Void FMPCStreamEncoder::.ctor()
extern void FMPCStreamEncoder__ctor_m30EDCF533329D9546E6A091E4B1CB062A078A8E1 (void);
// 0x0000011C System.Void FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::.ctor(System.Int32)
extern void U3CProcessCapturedTextureCORU3Ed__42__ctor_m15FFF09B7B159B87D28F5D33B144A2746818A1AD (void);
// 0x0000011D System.Void FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::System.IDisposable.Dispose()
extern void U3CProcessCapturedTextureCORU3Ed__42_System_IDisposable_Dispose_m7729875E21C169DB7607CD7AA78FDCAA64EABD79 (void);
// 0x0000011E System.Boolean FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::MoveNext()
extern void U3CProcessCapturedTextureCORU3Ed__42_MoveNext_mCA2A0F715D2889BDE77782D83D3DCB81EC59249E (void);
// 0x0000011F System.Object FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessCapturedTextureCORU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EDA088CC58BB139147565B228C736438F3D1535 (void);
// 0x00000120 System.Void FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::System.Collections.IEnumerator.Reset()
extern void U3CProcessCapturedTextureCORU3Ed__42_System_Collections_IEnumerator_Reset_m2CF3235A57BBD0185F62E97C5BB4B8D3FCE928E7 (void);
// 0x00000121 System.Object FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CProcessCapturedTextureCORU3Ed__42_System_Collections_IEnumerator_get_Current_m86506782F7D1CAA62F474DD37C2B8E4C39092B76 (void);
// 0x00000122 System.Void FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::.ctor(System.Int32)
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43__ctor_mFD7B06F8C9C3225C53B4A570A00B4B411A0E194F (void);
// 0x00000123 System.Void FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::System.IDisposable.Dispose()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_IDisposable_Dispose_m5AD15B51BE7D919588C1A8F64512C4D51D1B8614 (void);
// 0x00000124 System.Boolean FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::MoveNext()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_MoveNext_mE382A215BD6B180696A1D6AC5F4131B82E716112 (void);
// 0x00000125 System.Object FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD91424E3299A4B2722284CC4EEC2EF72A011B5F (void);
// 0x00000126 System.Void FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::System.Collections.IEnumerator.Reset()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_IEnumerator_Reset_mDD289D3618486F23BC4AFFC98D55B0E0BAB41D63 (void);
// 0x00000127 System.Object FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_IEnumerator_get_Current_m16B83FFBF70421CFEB212367B9AA144BAAE65173 (void);
// 0x00000128 System.Void FMPCStreamEncoder/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m2935EC52C8AAD44F8F55CA8EDB1ACDC824F63DEB (void);
// 0x00000129 System.Void FMPCStreamEncoder/<>c__DisplayClass44_1::.ctor()
extern void U3CU3Ec__DisplayClass44_1__ctor_mFA7BDA937A6B95CF50F55E89B53ED590D4F62E2B (void);
// 0x0000012A System.Void FMPCStreamEncoder/<>c__DisplayClass44_1::<EncodeBytes>b__0()
extern void U3CU3Ec__DisplayClass44_1_U3CEncodeBytesU3Eb__0_mEB50BEDB1F85AD02C80A1DE03D3D53BCB185F5F2 (void);
// 0x0000012B System.Void FMPCStreamEncoder/<EncodeBytes>d__44::.ctor(System.Int32)
extern void U3CEncodeBytesU3Ed__44__ctor_mB362F7B2E399D20CF9B1258BCDD198F8182C8678 (void);
// 0x0000012C System.Void FMPCStreamEncoder/<EncodeBytes>d__44::System.IDisposable.Dispose()
extern void U3CEncodeBytesU3Ed__44_System_IDisposable_Dispose_mC9163EBABB38DCA93DFFCA7EA69202082F854504 (void);
// 0x0000012D System.Boolean FMPCStreamEncoder/<EncodeBytes>d__44::MoveNext()
extern void U3CEncodeBytesU3Ed__44_MoveNext_m412619D11160EBE0E6CE0CDFBDC855F347648ED0 (void);
// 0x0000012E System.Object FMPCStreamEncoder/<EncodeBytes>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEncodeBytesU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD0D1F5ADBBBF396B19D6D50AF3D49D56BC87EA0 (void);
// 0x0000012F System.Void FMPCStreamEncoder/<EncodeBytes>d__44::System.Collections.IEnumerator.Reset()
extern void U3CEncodeBytesU3Ed__44_System_Collections_IEnumerator_Reset_m0A6EC7E76BB5A9824356A83320ABC41C4BABC3AB (void);
// 0x00000130 System.Object FMPCStreamEncoder/<EncodeBytes>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CEncodeBytesU3Ed__44_System_Collections_IEnumerator_get_Current_mD8957775782E2CDED6842E4BAC06BBFC114D72F2 (void);
// 0x00000131 UnityEngine.Texture GameViewDecoder::get_ReceivedTexture()
extern void GameViewDecoder_get_ReceivedTexture_mF67BA0855D0FCC63FDBA97286C6488DDCEFBA090 (void);
// 0x00000132 System.Void GameViewDecoder::Reset()
extern void GameViewDecoder_Reset_mAB4D75283F20BB464F83A051C94DAE18FA94ABEB (void);
// 0x00000133 System.Void GameViewDecoder::Start()
extern void GameViewDecoder_Start_m95CF1EA78DC41B6399E3B92DC7CEA639126CF794 (void);
// 0x00000134 System.Void GameViewDecoder::Action_ProcessImageData(System.Byte[])
extern void GameViewDecoder_Action_ProcessImageData_m50EFF638F1E082E0A9F0FB2A4367F55E03DF26C7 (void);
// 0x00000135 System.Collections.IEnumerator GameViewDecoder::ProcessImageData(System.Byte[])
extern void GameViewDecoder_ProcessImageData_mF85E9BC44B1BF094D87F8FBFEA767AC4AD540658 (void);
// 0x00000136 System.Void GameViewDecoder::OnDisable()
extern void GameViewDecoder_OnDisable_m768F881636B7601865898A1AC4BDEE99E29D639A (void);
// 0x00000137 System.Void GameViewDecoder::Action_ProcessMJPEGData(System.Byte[])
extern void GameViewDecoder_Action_ProcessMJPEGData_mB9D368B96F984D54AB204F98F1F66E50218091D2 (void);
// 0x00000138 System.Void GameViewDecoder::parseStreamBuffer(System.Byte[])
extern void GameViewDecoder_parseStreamBuffer_m34A1C9414FE4C26E4117E64E0F78034E7E3D06A1 (void);
// 0x00000139 System.Void GameViewDecoder::searchPicture(System.Byte[])
extern void GameViewDecoder_searchPicture_mE9318AA1AF44DBA04ED25636A3EAA2CEC47EEB24 (void);
// 0x0000013A System.Void GameViewDecoder::parsePicture(System.Byte[])
extern void GameViewDecoder_parsePicture_m00E4058E8261EE7C7971F4DA44AC0B42D1149EA6 (void);
// 0x0000013B System.Void GameViewDecoder::.ctor()
extern void GameViewDecoder__ctor_m546F57E09CC15846414235547ED2BA8593D6F793 (void);
// 0x0000013C System.Void GameViewDecoder/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m7131F0235948BECBB597F6170FDD17508A1D665D (void);
// 0x0000013D System.Void GameViewDecoder/<>c__DisplayClass26_1::.ctor()
extern void U3CU3Ec__DisplayClass26_1__ctor_m51A8AACEE71945705181A2E3DF4D527AF3D44B71 (void);
// 0x0000013E System.Void GameViewDecoder/<>c__DisplayClass26_1::<ProcessImageData>b__0()
extern void U3CU3Ec__DisplayClass26_1_U3CProcessImageDataU3Eb__0_mD3668D812B3812EDA9AF374399C48844028C4EE9 (void);
// 0x0000013F System.Void GameViewDecoder/<ProcessImageData>d__26::.ctor(System.Int32)
extern void U3CProcessImageDataU3Ed__26__ctor_mFF005381DEAFA85697691DB01D2E96A04A2F3F9A (void);
// 0x00000140 System.Void GameViewDecoder/<ProcessImageData>d__26::System.IDisposable.Dispose()
extern void U3CProcessImageDataU3Ed__26_System_IDisposable_Dispose_m104AD1D5B329FBA939C13D10D821C5A3DF5A5E40 (void);
// 0x00000141 System.Boolean GameViewDecoder/<ProcessImageData>d__26::MoveNext()
extern void U3CProcessImageDataU3Ed__26_MoveNext_mA877162B987ECC684487FD6AC82A8B31518C916A (void);
// 0x00000142 System.Object GameViewDecoder/<ProcessImageData>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessImageDataU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB862E09B3F79BDDD1138B058AD495CA9A1655B0 (void);
// 0x00000143 System.Void GameViewDecoder/<ProcessImageData>d__26::System.Collections.IEnumerator.Reset()
extern void U3CProcessImageDataU3Ed__26_System_Collections_IEnumerator_Reset_m4C616E8E673C04E08EE0A9CC111811BF6CFF216B (void);
// 0x00000144 System.Object GameViewDecoder/<ProcessImageData>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CProcessImageDataU3Ed__26_System_Collections_IEnumerator_get_Current_m17341C31260029C5D3893A16A2469E24CA2ACE59 (void);
// 0x00000145 System.Boolean GameViewEncoder::get_SupportsAsyncGPUReadback()
extern void GameViewEncoder_get_SupportsAsyncGPUReadback_mB7EABCD177286341C3DBBE73AF2C8E5B69E0FF51 (void);
// 0x00000146 UnityEngine.Texture GameViewEncoder::get_GetStreamTexture()
extern void GameViewEncoder_get_GetStreamTexture_m66B84400A91D49DC346FA5E38C21A9B074D196D5 (void);
// 0x00000147 System.Single GameViewEncoder::get_brightness()
extern void GameViewEncoder_get_brightness_m8540D0E2F6AB680C4A7E5AFD81A8AD6B9DA5F9EE (void);
// 0x00000148 System.Void GameViewEncoder::CaptureModeUpdate()
extern void GameViewEncoder_CaptureModeUpdate_m01E3DA6667F83361E464F3A6E601FECDABCCB1B9 (void);
// 0x00000149 System.Void GameViewEncoder::Reset()
extern void GameViewEncoder_Reset_m8148C23BE2C200581407E7E576C1854E7B891E2E (void);
// 0x0000014A System.Void GameViewEncoder::Start()
extern void GameViewEncoder_Start_m746BDB5B15E9F0EC7362F72FB57E1B0BF1DDADB4 (void);
// 0x0000014B System.Void GameViewEncoder::Update()
extern void GameViewEncoder_Update_m07CC0A29DAE467DE84589D865850F820D6D31AFF (void);
// 0x0000014C System.Void GameViewEncoder::CheckResolution()
extern void GameViewEncoder_CheckResolution_m72CD6F887C77B9BCCD7267F8ECCB40D97AED9E4E (void);
// 0x0000014D System.Void GameViewEncoder::ProcessCapturedTexture()
extern void GameViewEncoder_ProcessCapturedTexture_m065D42F559FFF10D37572C314956B8A19033462E (void);
// 0x0000014E System.Collections.IEnumerator GameViewEncoder::ProcessCapturedTextureCOR()
extern void GameViewEncoder_ProcessCapturedTextureCOR_m2F3C75858DCFF5FCEA1F75266CAD7C78D49FE216 (void);
// 0x0000014F System.Collections.IEnumerator GameViewEncoder::ProcessCapturedTextureGPUReadbackCOR()
extern void GameViewEncoder_ProcessCapturedTextureGPUReadbackCOR_mE20F256550ECCD02E1E544B2C81517D1C717BAB1 (void);
// 0x00000150 System.Void GameViewEncoder::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void GameViewEncoder_OnRenderImage_mFED3A2E5E855AF3CCCBE6EA0E37D636E652EDE02 (void);
// 0x00000151 System.Collections.IEnumerator GameViewEncoder::RenderTextureRefresh()
extern void GameViewEncoder_RenderTextureRefresh_m10F6FD6FBEF772E8038B944E220F1DD2DE566779 (void);
// 0x00000152 System.Void GameViewEncoder::Action_UpdateTexture()
extern void GameViewEncoder_Action_UpdateTexture_m287EA9A33302BC256F33E8D42BC3661C42DE0D4B (void);
// 0x00000153 System.Void GameViewEncoder::RequestTextureUpdate()
extern void GameViewEncoder_RequestTextureUpdate_mBFF2C8B375914C55D3556EEEDF976C4C2F5DE232 (void);
// 0x00000154 System.Collections.IEnumerator GameViewEncoder::SenderCOR()
extern void GameViewEncoder_SenderCOR_m01EDCB53B6A4252CE4F4E8159142FE1F53E43FC9 (void);
// 0x00000155 System.Collections.IEnumerator GameViewEncoder::EncodeBytes(System.Byte[])
extern void GameViewEncoder_EncodeBytes_m158CD186834EE71D24D6B29734DF224EE223578A (void);
// 0x00000156 System.Void GameViewEncoder::OnEnable()
extern void GameViewEncoder_OnEnable_m5B2591C89FD745F4BC47047415072184EA7E02AC (void);
// 0x00000157 System.Void GameViewEncoder::OnDisable()
extern void GameViewEncoder_OnDisable_m7646E72D204E45C9E244CEB1DC00F8A21ED6DDC4 (void);
// 0x00000158 System.Void GameViewEncoder::OnApplicationQuit()
extern void GameViewEncoder_OnApplicationQuit_m988AC9DD29A59AD833AE133E37EF398AA2517474 (void);
// 0x00000159 System.Void GameViewEncoder::OnDestroy()
extern void GameViewEncoder_OnDestroy_mF53329415142B1049240B81229046284AF820D9A (void);
// 0x0000015A System.Void GameViewEncoder::StopAll()
extern void GameViewEncoder_StopAll_mF8AB30BFE36976B1CE20BF32B84DD1C2BBB14307 (void);
// 0x0000015B System.Void GameViewEncoder::StartAll()
extern void GameViewEncoder_StartAll_m592905167330D9598AAE2264583EF0FCA4099F6C (void);
// 0x0000015C System.Void GameViewEncoder::.ctor()
extern void GameViewEncoder__ctor_mA630790521E624EE1DD73BD0288BE5A2E9DA547E (void);
// 0x0000015D System.Void GameViewEncoder/<ProcessCapturedTextureCOR>d__69::.ctor(System.Int32)
extern void U3CProcessCapturedTextureCORU3Ed__69__ctor_mF66269C3B7A0ED10EBF8E094D3BFC0F060D86A5D (void);
// 0x0000015E System.Void GameViewEncoder/<ProcessCapturedTextureCOR>d__69::System.IDisposable.Dispose()
extern void U3CProcessCapturedTextureCORU3Ed__69_System_IDisposable_Dispose_m086E685B3E9C1E36DB2E6EA87C0C3873C87A84E9 (void);
// 0x0000015F System.Boolean GameViewEncoder/<ProcessCapturedTextureCOR>d__69::MoveNext()
extern void U3CProcessCapturedTextureCORU3Ed__69_MoveNext_mE44F2FE3DBF6358C95020E8377AA8C7E297F09A9 (void);
// 0x00000160 System.Object GameViewEncoder/<ProcessCapturedTextureCOR>d__69::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessCapturedTextureCORU3Ed__69_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE181181BB0EC7ECF873706E1D34BF0BB10D5068B (void);
// 0x00000161 System.Void GameViewEncoder/<ProcessCapturedTextureCOR>d__69::System.Collections.IEnumerator.Reset()
extern void U3CProcessCapturedTextureCORU3Ed__69_System_Collections_IEnumerator_Reset_m70A1896DD9795F94E98749F40D58746320E05B45 (void);
// 0x00000162 System.Object GameViewEncoder/<ProcessCapturedTextureCOR>d__69::System.Collections.IEnumerator.get_Current()
extern void U3CProcessCapturedTextureCORU3Ed__69_System_Collections_IEnumerator_get_Current_mC4C6E62B6906AC9420398225007300BBB7C310A2 (void);
// 0x00000163 System.Void GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::.ctor(System.Int32)
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70__ctor_m22AC16A56D048F7390728187024E563C144A1D70 (void);
// 0x00000164 System.Void GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::System.IDisposable.Dispose()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_IDisposable_Dispose_m2D881E772C31EF5A3E5B524982ADF7327A31F266 (void);
// 0x00000165 System.Boolean GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::MoveNext()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_MoveNext_m215773F333C547E2B3B2F08BBAC4A24A06677BAD (void);
// 0x00000166 System.Object GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE3DAB2EEEC093449434F76C34C21918E5522966 (void);
// 0x00000167 System.Void GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::System.Collections.IEnumerator.Reset()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_IEnumerator_Reset_mD7D3F0A050DBDB59AA97F0CA55B7060AC65E20BD (void);
// 0x00000168 System.Object GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::System.Collections.IEnumerator.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_IEnumerator_get_Current_m66E2867825D436E43BBA5A1EFCF6F874341651E7 (void);
// 0x00000169 System.Void GameViewEncoder/<RenderTextureRefresh>d__72::.ctor(System.Int32)
extern void U3CRenderTextureRefreshU3Ed__72__ctor_m441A7A26F202683B4AF7A01A23EE9F91E26C1427 (void);
// 0x0000016A System.Void GameViewEncoder/<RenderTextureRefresh>d__72::System.IDisposable.Dispose()
extern void U3CRenderTextureRefreshU3Ed__72_System_IDisposable_Dispose_mCDCD7312CCE933380F22BD1B245DB76E3328A4BF (void);
// 0x0000016B System.Boolean GameViewEncoder/<RenderTextureRefresh>d__72::MoveNext()
extern void U3CRenderTextureRefreshU3Ed__72_MoveNext_m9005B964D5851EE2716DCC2498292CAE27DC690A (void);
// 0x0000016C System.Object GameViewEncoder/<RenderTextureRefresh>d__72::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRenderTextureRefreshU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC69DB3D7AA8EC1B9A61D2276C6A2AE960D8C8409 (void);
// 0x0000016D System.Void GameViewEncoder/<RenderTextureRefresh>d__72::System.Collections.IEnumerator.Reset()
extern void U3CRenderTextureRefreshU3Ed__72_System_Collections_IEnumerator_Reset_m521F9BE351ACF085DB25746D7471F7D97E2008AF (void);
// 0x0000016E System.Object GameViewEncoder/<RenderTextureRefresh>d__72::System.Collections.IEnumerator.get_Current()
extern void U3CRenderTextureRefreshU3Ed__72_System_Collections_IEnumerator_get_Current_mFC45BFE4ED404F1977AA77690A0502BCEC32A087 (void);
// 0x0000016F System.Void GameViewEncoder/<SenderCOR>d__75::.ctor(System.Int32)
extern void U3CSenderCORU3Ed__75__ctor_mB1D44F3E580E178C4525AE4F1E965AB6537AAF63 (void);
// 0x00000170 System.Void GameViewEncoder/<SenderCOR>d__75::System.IDisposable.Dispose()
extern void U3CSenderCORU3Ed__75_System_IDisposable_Dispose_mF5251E7986A74799D5234C699F5F5DE2018DCAB8 (void);
// 0x00000171 System.Boolean GameViewEncoder/<SenderCOR>d__75::MoveNext()
extern void U3CSenderCORU3Ed__75_MoveNext_m19B5394EDA9F584DE255EC49DD84741814E7E291 (void);
// 0x00000172 System.Object GameViewEncoder/<SenderCOR>d__75::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSenderCORU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64FDB59383175778B5F08CD111AC0152C8762D94 (void);
// 0x00000173 System.Void GameViewEncoder/<SenderCOR>d__75::System.Collections.IEnumerator.Reset()
extern void U3CSenderCORU3Ed__75_System_Collections_IEnumerator_Reset_m26A8498C71A09594D18B15654843CA89C017AD38 (void);
// 0x00000174 System.Object GameViewEncoder/<SenderCOR>d__75::System.Collections.IEnumerator.get_Current()
extern void U3CSenderCORU3Ed__75_System_Collections_IEnumerator_get_Current_mDA5B591513A1F48F3C6657B0F7D2D13BF3216660 (void);
// 0x00000175 System.Void GameViewEncoder/<>c__DisplayClass76_0::.ctor()
extern void U3CU3Ec__DisplayClass76_0__ctor_m425A9ED39A7D6CE5AC79A49DEDEA5897C81AEC44 (void);
// 0x00000176 System.Void GameViewEncoder/<>c__DisplayClass76_1::.ctor()
extern void U3CU3Ec__DisplayClass76_1__ctor_m757856454F0951330CF023FC4DA89C76A1BC6770 (void);
// 0x00000177 System.Void GameViewEncoder/<>c__DisplayClass76_1::<EncodeBytes>b__0()
extern void U3CU3Ec__DisplayClass76_1_U3CEncodeBytesU3Eb__0_mF2BCCD0900D22AE1E459932C9B15412536F4153B (void);
// 0x00000178 System.Void GameViewEncoder/<EncodeBytes>d__76::.ctor(System.Int32)
extern void U3CEncodeBytesU3Ed__76__ctor_m05FFBA45D64E7B0C2A417B23A3BB88952C899118 (void);
// 0x00000179 System.Void GameViewEncoder/<EncodeBytes>d__76::System.IDisposable.Dispose()
extern void U3CEncodeBytesU3Ed__76_System_IDisposable_Dispose_m3A445380FE83AC6C121EAB423FCC0DC76F24AF14 (void);
// 0x0000017A System.Boolean GameViewEncoder/<EncodeBytes>d__76::MoveNext()
extern void U3CEncodeBytesU3Ed__76_MoveNext_m727982AC08D4850C26E458ED9F15EA89B2F7F6F0 (void);
// 0x0000017B System.Object GameViewEncoder/<EncodeBytes>d__76::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEncodeBytesU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE8F2F18944E9749DCE685168852188B1D0A3DBF (void);
// 0x0000017C System.Void GameViewEncoder/<EncodeBytes>d__76::System.Collections.IEnumerator.Reset()
extern void U3CEncodeBytesU3Ed__76_System_Collections_IEnumerator_Reset_m0D2CAE677BE939A31EE1CCC3057CEBDBD72E4927 (void);
// 0x0000017D System.Object GameViewEncoder/<EncodeBytes>d__76::System.Collections.IEnumerator.get_Current()
extern void U3CEncodeBytesU3Ed__76_System_Collections_IEnumerator_get_Current_m44B4F166663727A00D3269D1E5D5FA6FA101BBFA (void);
// 0x0000017E System.Void MicEncoder::Start()
extern void MicEncoder_Start_m53DA7A39A7AF825CBA08B2C2842AA3E7DEA56EBB (void);
// 0x0000017F System.Collections.IEnumerator MicEncoder::CaptureMic()
extern void MicEncoder_CaptureMic_mCF075DE7D509C2F50F85DCA752A6DFC890211ABE (void);
// 0x00000180 System.Int16 MicEncoder::FloatToInt16(System.Single)
extern void MicEncoder_FloatToInt16_m555E6262FB2F33514A6098636DE6020521914608 (void);
// 0x00000181 System.Void MicEncoder::AddMicData()
extern void MicEncoder_AddMicData_m5324698332010F054884AA42792D1B375262CF3C (void);
// 0x00000182 System.Collections.IEnumerator MicEncoder::SenderCOR()
extern void MicEncoder_SenderCOR_mF9DDA52D68EA3C7AC1762793C3D03072960F212F (void);
// 0x00000183 System.Void MicEncoder::EncodeBytes()
extern void MicEncoder_EncodeBytes_m0DB78BA240940B5AE3F421E4DB87A33C84DFD796 (void);
// 0x00000184 System.Void MicEncoder::OnEnable()
extern void MicEncoder_OnEnable_m3A80338C5ED4F15066E95D41A1C9003E6B3A2FF7 (void);
// 0x00000185 System.Void MicEncoder::OnDisable()
extern void MicEncoder_OnDisable_m8FB151163C744039B233BA89C2281D19EC521B1B (void);
// 0x00000186 System.Void MicEncoder::StartAll()
extern void MicEncoder_StartAll_m25BB3583F7F42AD43A06E256B40638616A36A786 (void);
// 0x00000187 System.Void MicEncoder::StopAll()
extern void MicEncoder_StopAll_m3D277EADD613EFCC2EF6A886515FB27523B85BC2 (void);
// 0x00000188 System.Void MicEncoder::.ctor()
extern void MicEncoder__ctor_m6E490823DE9B0DA48D2D815A3FB147EFF78145DD (void);
// 0x00000189 System.Void MicEncoder/<CaptureMic>d__25::.ctor(System.Int32)
extern void U3CCaptureMicU3Ed__25__ctor_m0BCAE3452B6D482727E672861BAF162325F90E00 (void);
// 0x0000018A System.Void MicEncoder/<CaptureMic>d__25::System.IDisposable.Dispose()
extern void U3CCaptureMicU3Ed__25_System_IDisposable_Dispose_mA5BA2DDBCA658E05BC4BA83D41DF209FFDAC0DFE (void);
// 0x0000018B System.Boolean MicEncoder/<CaptureMic>d__25::MoveNext()
extern void U3CCaptureMicU3Ed__25_MoveNext_mC5874582F40EDE1F6408B7ACADFD24270CB24A87 (void);
// 0x0000018C System.Object MicEncoder/<CaptureMic>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCaptureMicU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10322B18AC361696176C1C18CF4F29B2B1C28DE0 (void);
// 0x0000018D System.Void MicEncoder/<CaptureMic>d__25::System.Collections.IEnumerator.Reset()
extern void U3CCaptureMicU3Ed__25_System_Collections_IEnumerator_Reset_mE23C4A8F1B189131CCD122043EDB2C0E2657B29B (void);
// 0x0000018E System.Object MicEncoder/<CaptureMic>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CCaptureMicU3Ed__25_System_Collections_IEnumerator_get_Current_m9D74DBD73471CE428391634DF4C6AF4F92D3DD62 (void);
// 0x0000018F System.Void MicEncoder/<SenderCOR>d__28::.ctor(System.Int32)
extern void U3CSenderCORU3Ed__28__ctor_m98F7DD97280427E4F47C7C505A04704BAECD7F70 (void);
// 0x00000190 System.Void MicEncoder/<SenderCOR>d__28::System.IDisposable.Dispose()
extern void U3CSenderCORU3Ed__28_System_IDisposable_Dispose_mCAC6350DD4E60EF80637FCF114A1D4A5D5829BDD (void);
// 0x00000191 System.Boolean MicEncoder/<SenderCOR>d__28::MoveNext()
extern void U3CSenderCORU3Ed__28_MoveNext_mACBBC85A20BC627B8DF11957F481D723567A59F7 (void);
// 0x00000192 System.Object MicEncoder/<SenderCOR>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSenderCORU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EEE62547FE206388298E76F5ECE2EE1609537A9 (void);
// 0x00000193 System.Void MicEncoder/<SenderCOR>d__28::System.Collections.IEnumerator.Reset()
extern void U3CSenderCORU3Ed__28_System_Collections_IEnumerator_Reset_m5314ED9761438DE960AD963E34FE3254896D624B (void);
// 0x00000194 System.Object MicEncoder/<SenderCOR>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CSenderCORU3Ed__28_System_Collections_IEnumerator_get_Current_m96B53EFB88DE6AC60DD986DCAC39165906A78261 (void);
// 0x00000195 System.Void TextureEncoder::set_SetStreamWebCamTexture(UnityEngine.WebCamTexture)
extern void TextureEncoder_set_SetStreamWebCamTexture_m4BC52C1DABB16CBE607EA898EFC75060868344EF (void);
// 0x00000196 UnityEngine.Texture TextureEncoder::get_GetStreamTexture()
extern void TextureEncoder_get_GetStreamTexture_mC67E1EC94C8AE2A119EA2CE176F72B91B8225A4E (void);
// 0x00000197 UnityEngine.Texture TextureEncoder::get_GetPreviewTexture()
extern void TextureEncoder_get_GetPreviewTexture_mC1E7EA60F737994ACC02B93A80893C81723BEB39 (void);
// 0x00000198 System.Boolean TextureEncoder::get_SupportsAsyncGPUReadback()
extern void TextureEncoder_get_SupportsAsyncGPUReadback_m0C8E70073EA5FD168956CE663BCE5AD953BFD7F4 (void);
// 0x00000199 System.Int32 TextureEncoder::get_StreamWidth()
extern void TextureEncoder_get_StreamWidth_m39F28ED4CB09C702003D8A0FEB6FBD4EB374AB23 (void);
// 0x0000019A System.Int32 TextureEncoder::get_StreamHeight()
extern void TextureEncoder_get_StreamHeight_m3C7F58AAF640D6D5A8A4DA6A7E794CD8DDC8B4F8 (void);
// 0x0000019B System.Void TextureEncoder::Start()
extern void TextureEncoder_Start_m9C6BED8F944D06A581B6BC95842A8245B9C9B551 (void);
// 0x0000019C System.Void TextureEncoder::Action_StreamTexture(System.Byte[],System.Int32,System.Int32)
extern void TextureEncoder_Action_StreamTexture_m9CDAF27783BE37BCBAA8A816BFB70733C91E07DD (void);
// 0x0000019D System.Void TextureEncoder::Action_StreamTexture(UnityEngine.Texture2D)
extern void TextureEncoder_Action_StreamTexture_m49279A48FAA4555DC1E9D2FDC4EB3ED386454BBF (void);
// 0x0000019E System.Void TextureEncoder::Action_StreamWebcamTexture(UnityEngine.WebCamTexture)
extern void TextureEncoder_Action_StreamWebcamTexture_m657DC6148B1452DEF3C168053F0C909A2FAF01E9 (void);
// 0x0000019F System.Void TextureEncoder::Action_StreamRenderTexture(UnityEngine.RenderTexture)
extern void TextureEncoder_Action_StreamRenderTexture_m822E546F729527FC3B50363B385CA7EBCE06B8E0 (void);
// 0x000001A0 System.Collections.IEnumerator TextureEncoder::ProcessCapturedTextureGPUReadbackCOR(UnityEngine.RenderTexture)
extern void TextureEncoder_ProcessCapturedTextureGPUReadbackCOR_m9F3891B57BC8A6F67F89A7B55896CB9240E1003E (void);
// 0x000001A1 System.Void TextureEncoder::RequestTextureUpdate()
extern void TextureEncoder_RequestTextureUpdate_m18AEFAA45404F0ADA2195C0C0C5C788760D9A72C (void);
// 0x000001A2 System.Collections.IEnumerator TextureEncoder::SenderCOR()
extern void TextureEncoder_SenderCOR_mE21722767888B08DDC3D884C94134624EB3509B5 (void);
// 0x000001A3 System.Collections.IEnumerator TextureEncoder::EncodeBytes()
extern void TextureEncoder_EncodeBytes_mAC3DABEB4A2B62CE1F7C6B652D2CD0126F926E42 (void);
// 0x000001A4 System.Void TextureEncoder::OnEnable()
extern void TextureEncoder_OnEnable_m16D12003083336A653CB29C05414E2F6FB866344 (void);
// 0x000001A5 System.Void TextureEncoder::OnDisable()
extern void TextureEncoder_OnDisable_m843744E50FFB53AC2A4151CF23CFBF6B9E642FF7 (void);
// 0x000001A6 System.Void TextureEncoder::OnApplicationQuit()
extern void TextureEncoder_OnApplicationQuit_m407C24B90AAB5E4F382AA10C42EECC61A6A7F0E9 (void);
// 0x000001A7 System.Void TextureEncoder::OnDestroy()
extern void TextureEncoder_OnDestroy_m419DA1C2B096F89A50EB84C06898F5C6D2BBD0C9 (void);
// 0x000001A8 System.Void TextureEncoder::StopAll()
extern void TextureEncoder_StopAll_mC655C735A170A941D3B511B8CAFA7A2D1A892164 (void);
// 0x000001A9 System.Void TextureEncoder::StartAll()
extern void TextureEncoder_StartAll_m568278F6524B4862DEBC9733143C515883AFE16E (void);
// 0x000001AA System.Void TextureEncoder::.ctor()
extern void TextureEncoder__ctor_m3E23694B97D5B1226833C9CB674B97E3AAD7841B (void);
// 0x000001AB System.Void TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::.ctor(System.Int32)
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49__ctor_m7A7777671665613076FF3AA9D0634827F0CE1D1D (void);
// 0x000001AC System.Void TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.IDisposable.Dispose()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_IDisposable_Dispose_mC244647E21C1622C33C49437AA73760B07EA733B (void);
// 0x000001AD System.Boolean TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::MoveNext()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_MoveNext_mCC7C1D079ECD84A73980C682B79EA37EE42CFFB8 (void);
// 0x000001AE System.Object TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96C42559F165E59586219E1387AB2CB4D6BCA73D (void);
// 0x000001AF System.Void TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.Collections.IEnumerator.Reset()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_Reset_m765AE58387A0C89F17588BFF91919AEC413081FB (void);
// 0x000001B0 System.Object TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.Collections.IEnumerator.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_get_Current_m792B625630F599B92747F7863B39D0C1F7A87FA7 (void);
// 0x000001B1 System.Void TextureEncoder/<SenderCOR>d__51::.ctor(System.Int32)
extern void U3CSenderCORU3Ed__51__ctor_mA77EC35DD25ABA9B996DA8F9C5B0EDD5903A18FA (void);
// 0x000001B2 System.Void TextureEncoder/<SenderCOR>d__51::System.IDisposable.Dispose()
extern void U3CSenderCORU3Ed__51_System_IDisposable_Dispose_m83C50F3984BDE74629E8ACBF069BB0A0AA333753 (void);
// 0x000001B3 System.Boolean TextureEncoder/<SenderCOR>d__51::MoveNext()
extern void U3CSenderCORU3Ed__51_MoveNext_m80A1E315C02D774471163852BC228E65D95C5D69 (void);
// 0x000001B4 System.Object TextureEncoder/<SenderCOR>d__51::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSenderCORU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8322C0C2E7D89D2C7143F8EF17A2A6E09A2F2A6 (void);
// 0x000001B5 System.Void TextureEncoder/<SenderCOR>d__51::System.Collections.IEnumerator.Reset()
extern void U3CSenderCORU3Ed__51_System_Collections_IEnumerator_Reset_m8DF573C5E0A33310CB292F0C8B8E3A2AB46F659A (void);
// 0x000001B6 System.Object TextureEncoder/<SenderCOR>d__51::System.Collections.IEnumerator.get_Current()
extern void U3CSenderCORU3Ed__51_System_Collections_IEnumerator_get_Current_mDBCD80BACCBECF12F5C336C8003EB0C3BB48A9F5 (void);
// 0x000001B7 System.Void TextureEncoder/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_mC913D0C8AE158CA693E67A5CCABC2512C3245FF4 (void);
// 0x000001B8 System.Void TextureEncoder/<>c__DisplayClass52_0::<EncodeBytes>b__0()
extern void U3CU3Ec__DisplayClass52_0_U3CEncodeBytesU3Eb__0_m4C4569E76CED51732F353063F2BB0EDFCCC3A091 (void);
// 0x000001B9 System.Void TextureEncoder/<EncodeBytes>d__52::.ctor(System.Int32)
extern void U3CEncodeBytesU3Ed__52__ctor_m466C96E960C64C1A08D40CAFA7B15519B3BF7222 (void);
// 0x000001BA System.Void TextureEncoder/<EncodeBytes>d__52::System.IDisposable.Dispose()
extern void U3CEncodeBytesU3Ed__52_System_IDisposable_Dispose_mD120CA1771B0C5533F78D015D0754FE83065A0D1 (void);
// 0x000001BB System.Boolean TextureEncoder/<EncodeBytes>d__52::MoveNext()
extern void U3CEncodeBytesU3Ed__52_MoveNext_mD170B8A76DDDCB34603A5E040260C6A9F876381F (void);
// 0x000001BC System.Object TextureEncoder/<EncodeBytes>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEncodeBytesU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A775392B9DC6011391FC9DB18C0606286964374 (void);
// 0x000001BD System.Void TextureEncoder/<EncodeBytes>d__52::System.Collections.IEnumerator.Reset()
extern void U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_Reset_m5CC4264F59DB6C3D9A916AE8A5FEADDAF55A15A2 (void);
// 0x000001BE System.Object TextureEncoder/<EncodeBytes>d__52::System.Collections.IEnumerator.get_Current()
extern void U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_get_Current_m4890B1605C611D741CCA98D73B9980C33FD36A6E (void);
// 0x000001BF System.Void ConnectionDebugText::Start()
extern void ConnectionDebugText_Start_m7CAE9BA91D2E70145138045CA505FCCFA372F2DD (void);
// 0x000001C0 System.Void ConnectionDebugText::Update()
extern void ConnectionDebugText_Update_m3D64F82A50F59B71539DB801D24F1EB62D1A1D32 (void);
// 0x000001C1 System.Void ConnectionDebugText::.ctor()
extern void ConnectionDebugText__ctor_mEEB98F27DD2F5EDC426081AEADAA9D60B69D1A09 (void);
// 0x000001C2 System.Void NetworkActionClient::Awake()
extern void NetworkActionClient_Awake_mF86F12BB0EB2C0EF4E670722513C042B52BEDE02 (void);
// 0x000001C3 System.Void NetworkActionClient::Action_SetIP(System.String)
extern void NetworkActionClient_Action_SetIP_mDEE71162A2E4D57187A5240809994E9CCE969574 (void);
// 0x000001C4 System.Void NetworkActionClient::Action_SetServerListenPort(System.Int32)
extern void NetworkActionClient_Action_SetServerListenPort_m1E9A6A0E8AD34EFEC01D38E363D625DBB012AFB7 (void);
// 0x000001C5 System.Int32 NetworkActionClient::GetCurrentMS()
extern void NetworkActionClient_GetCurrentMS_m12D70F8F88D64DD0BEBEF85CB607A181768677C4 (void);
// 0x000001C6 System.Void NetworkActionClient::Action_ReceivedDataDebug(System.String)
extern void NetworkActionClient_Action_ReceivedDataDebug_mE04C41A0848D0B38F375F541A159F48DCC1EF574 (void);
// 0x000001C7 System.Void NetworkActionClient::Action_RpcSend(System.String)
extern void NetworkActionClient_Action_RpcSend_m8A035AFD85297132135EBD46E86EDADDF5084F33 (void);
// 0x000001C8 System.Void NetworkActionClient::Action_RpcSend(System.Byte[])
extern void NetworkActionClient_Action_RpcSend_mCB092BFD39392CD0A1856F3A73524D767673ADDB (void);
// 0x000001C9 System.Void NetworkActionClient::Start()
extern void NetworkActionClient_Start_m4A1FF36E770264CBE95B9F7E604714DEFF1AA682 (void);
// 0x000001CA System.Void NetworkActionClient::Update()
extern void NetworkActionClient_Update_m99FF45575FE161D9C7BEEF7EF7B1253054CBE3F4 (void);
// 0x000001CB System.Int32 NetworkActionClient::DataByteArrayToByteLength(System.Byte[])
extern void NetworkActionClient_DataByteArrayToByteLength_mDB3A6E8595BC1EDA3DBBF56D7704AA3D5644CCC2 (void);
// 0x000001CC System.Int32 NetworkActionClient::ReadDataByteSize(System.Int32)
extern void NetworkActionClient_ReadDataByteSize_mE20E1A2DE4A38D4D67041DC0D4A29B4922AB8500 (void);
// 0x000001CD System.Void NetworkActionClient::ReadDataByteArray(System.Int32)
extern void NetworkActionClient_ReadDataByteArray_mB33409027DAEE5CF4E83BD1157C356F60AA8020D (void);
// 0x000001CE System.Void NetworkActionClient::ProcessReceivedData(System.Byte[])
extern void NetworkActionClient_ProcessReceivedData_m39981BD8F7B743980CDCD3E5E99678C6225BB39D (void);
// 0x000001CF System.Collections.IEnumerator NetworkActionClient::RelaunchApp()
extern void NetworkActionClient_RelaunchApp_m169BC583FDCCD8B38DE27077A5715E78C0FE72BC (void);
// 0x000001D0 System.Void NetworkActionClient::Action_Disconnect()
extern void NetworkActionClient_Action_Disconnect_mE4A373855D69A45C99DCDAB6EE408611242F772D (void);
// 0x000001D1 System.Void NetworkActionClient::Action_ConnectServer()
extern void NetworkActionClient_Action_ConnectServer_m0707006053220BF330715FF8184510BBED094A6E (void);
// 0x000001D2 System.Collections.IEnumerator NetworkActionClient::ConnectServer()
extern void NetworkActionClient_ConnectServer_mAD0257352347719F65E80A8DE0040CDD4D06D940 (void);
// 0x000001D3 System.Collections.IEnumerator NetworkActionClient::MainThreadSender()
extern void NetworkActionClient_MainThreadSender_m622A0D7B47BE01980014D36BBBB09F334EA899C2 (void);
// 0x000001D4 System.Void NetworkActionClient::Sender()
extern void NetworkActionClient_Sender_m7D5CD6518E541625486056C65D3F5BBF233C6F4D (void);
// 0x000001D5 System.Void NetworkActionClient::ClientEndConnect(System.IAsyncResult)
extern void NetworkActionClient_ClientEndConnect_m709415FE0F577342490EEF98D339B45BD4F6697F (void);
// 0x000001D6 System.Void NetworkActionClient::LOGWARNING(System.String)
extern void NetworkActionClient_LOGWARNING_m4D60F0A371D528121BF77655970CF2CF344D3A40 (void);
// 0x000001D7 System.Void NetworkActionClient::OnApplicationQuit()
extern void NetworkActionClient_OnApplicationQuit_m44F85D653C3F25F58872540A5D26098DECF10F3C (void);
// 0x000001D8 System.Void NetworkActionClient::OnDestroy()
extern void NetworkActionClient_OnDestroy_m0B27CA4A88F6392F93D3D8F4C9C40E7E64E59EA5 (void);
// 0x000001D9 System.Void NetworkActionClient::CheckPause()
extern void NetworkActionClient_CheckPause_m43B9B63A68CF59691FFD5DF0ED7E1B57A5364219 (void);
// 0x000001DA System.Void NetworkActionClient::OnApplicationFocus(System.Boolean)
extern void NetworkActionClient_OnApplicationFocus_m0BBEBBEF98676E7090487970BBEE99A3E2845D53 (void);
// 0x000001DB System.Void NetworkActionClient::OnApplicationPause(System.Boolean)
extern void NetworkActionClient_OnApplicationPause_mFE5C243713B06223E5412BA6581203466FC39818 (void);
// 0x000001DC System.Void NetworkActionClient::OnDisable()
extern void NetworkActionClient_OnDisable_m79FD8899EA70F695A5D8F8D089EA093BA6F3B07B (void);
// 0x000001DD System.Void NetworkActionClient::OnEnable()
extern void NetworkActionClient_OnEnable_m9BB68941F0A63901AB9B39532EFB316646A22124 (void);
// 0x000001DE System.Void NetworkActionClient::.ctor()
extern void NetworkActionClient__ctor_mBDE3C62F1A79B999AEF997D2650892A68D41D3AC (void);
// 0x000001DF System.Void NetworkActionClient::<ConnectServer>b__45_0()
extern void NetworkActionClient_U3CConnectServerU3Eb__45_0_m442C6B6C470CC656027DF70E73DAF478E089B6CB (void);
// 0x000001E0 System.Void NetworkActionClient::<ConnectServer>b__45_1()
extern void NetworkActionClient_U3CConnectServerU3Eb__45_1_m179A541B8267512D610B9B300D41484E26284764 (void);
// 0x000001E1 System.Void NetworkActionClient/<RelaunchApp>d__42::.ctor(System.Int32)
extern void U3CRelaunchAppU3Ed__42__ctor_m8B105F3327D8A632F1D7C90BF50014DEEFC06FCD (void);
// 0x000001E2 System.Void NetworkActionClient/<RelaunchApp>d__42::System.IDisposable.Dispose()
extern void U3CRelaunchAppU3Ed__42_System_IDisposable_Dispose_mEEBC05919BF40AB942F11E926772F7EFEB21CD57 (void);
// 0x000001E3 System.Boolean NetworkActionClient/<RelaunchApp>d__42::MoveNext()
extern void U3CRelaunchAppU3Ed__42_MoveNext_mC32F576854618400F8E044D411CB5D20947ACDB2 (void);
// 0x000001E4 System.Object NetworkActionClient/<RelaunchApp>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRelaunchAppU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC26F400B84818EC3460756DE11E735D785BFB94 (void);
// 0x000001E5 System.Void NetworkActionClient/<RelaunchApp>d__42::System.Collections.IEnumerator.Reset()
extern void U3CRelaunchAppU3Ed__42_System_Collections_IEnumerator_Reset_m53A63EDFC749C27FB3BB9933690050F0D04B574F (void);
// 0x000001E6 System.Object NetworkActionClient/<RelaunchApp>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CRelaunchAppU3Ed__42_System_Collections_IEnumerator_get_Current_mF304273D01DC3101989FBD106DCF13E98709989E (void);
// 0x000001E7 System.Void NetworkActionClient/<ConnectServer>d__45::.ctor(System.Int32)
extern void U3CConnectServerU3Ed__45__ctor_m5B85B336E097535503ECCF6231A0F090E60D8CA8 (void);
// 0x000001E8 System.Void NetworkActionClient/<ConnectServer>d__45::System.IDisposable.Dispose()
extern void U3CConnectServerU3Ed__45_System_IDisposable_Dispose_mBE9FA24FBCB924B9DDFAF289C121D5B147256051 (void);
// 0x000001E9 System.Boolean NetworkActionClient/<ConnectServer>d__45::MoveNext()
extern void U3CConnectServerU3Ed__45_MoveNext_m9778BCC1EE6904540ACB90BBA216ED0B289A15BF (void);
// 0x000001EA System.Object NetworkActionClient/<ConnectServer>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConnectServerU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m272B27177D53C32FE40E38BE30C757C7C01BBDFE (void);
// 0x000001EB System.Void NetworkActionClient/<ConnectServer>d__45::System.Collections.IEnumerator.Reset()
extern void U3CConnectServerU3Ed__45_System_Collections_IEnumerator_Reset_m6EC4EBEF2DB4BBDA8AE731F223D52D673ADD8979 (void);
// 0x000001EC System.Object NetworkActionClient/<ConnectServer>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CConnectServerU3Ed__45_System_Collections_IEnumerator_get_Current_mBD22DE68B0C630F4024EF16188DE45274A2504AB (void);
// 0x000001ED System.Void NetworkActionClient/<MainThreadSender>d__46::.ctor(System.Int32)
extern void U3CMainThreadSenderU3Ed__46__ctor_mC82359B67D71E32828C89B6142552FA23EE3A05D (void);
// 0x000001EE System.Void NetworkActionClient/<MainThreadSender>d__46::System.IDisposable.Dispose()
extern void U3CMainThreadSenderU3Ed__46_System_IDisposable_Dispose_m93673DC46CDF23BBA139D59DFAA42E801556DB10 (void);
// 0x000001EF System.Boolean NetworkActionClient/<MainThreadSender>d__46::MoveNext()
extern void U3CMainThreadSenderU3Ed__46_MoveNext_mCC037827C5AF8E0A43CBA41DF17A7D2B21447C14 (void);
// 0x000001F0 System.Object NetworkActionClient/<MainThreadSender>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMainThreadSenderU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74012AD9BC7B95B1B3325C8B6DCA643B95A64804 (void);
// 0x000001F1 System.Void NetworkActionClient/<MainThreadSender>d__46::System.Collections.IEnumerator.Reset()
extern void U3CMainThreadSenderU3Ed__46_System_Collections_IEnumerator_Reset_m8EBA6BD61B072F3C906E725CA12D79F20807C3F2 (void);
// 0x000001F2 System.Object NetworkActionClient/<MainThreadSender>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CMainThreadSenderU3Ed__46_System_Collections_IEnumerator_get_Current_m447C88C8948FDDD82F61202F9CCCDF3FD4EC4C54 (void);
// 0x000001F3 System.Void NetworkActionDemo::Action_ProcessRpcMessage(System.String)
extern void NetworkActionDemo_Action_ProcessRpcMessage_m8501BA238EA0A9E619F99E5EB08237CD57B0836A (void);
// 0x000001F4 System.Void NetworkActionDemo::Rpc_SetInt(System.Int32)
extern void NetworkActionDemo_Rpc_SetInt_mD7B23D1FD6E57602B08B99D085DA6E965527FFA2 (void);
// 0x000001F5 System.Void NetworkActionDemo::Rpc_SetFloat(System.Single)
extern void NetworkActionDemo_Rpc_SetFloat_m76560C28EE5B5CC05112D8CBF3E1C44D184F9CEA (void);
// 0x000001F6 System.Void NetworkActionDemo::Rpc_SetBool(System.Boolean)
extern void NetworkActionDemo_Rpc_SetBool_mBCF20801C526B002022BDEA2D5F878EED04CBBA7 (void);
// 0x000001F7 System.Void NetworkActionDemo::Rpc_SetString(System.String)
extern void NetworkActionDemo_Rpc_SetString_mAB43CBDC892B99BDF9FD4C5BE4F653F8BB79267C (void);
// 0x000001F8 System.Void NetworkActionDemo::Rpc_SetVector3(UnityEngine.Vector3)
extern void NetworkActionDemo_Rpc_SetVector3_m545A3340C1C473F4509513FBFABF9852C814A8FA (void);
// 0x000001F9 System.Void NetworkActionDemo::Rpc_SetVector3Random()
extern void NetworkActionDemo_Rpc_SetVector3Random_mA35F3592067C14205A552EA2D07D80FBD0EBA5C5 (void);
// 0x000001FA System.Void NetworkActionDemo::Cmd_ServerSendByte()
extern void NetworkActionDemo_Cmd_ServerSendByte_mFCFA36F6546980E972712BD53B57BD0B78A63BA7 (void);
// 0x000001FB System.Void NetworkActionDemo::Action_ProcessByteData(System.Byte[])
extern void NetworkActionDemo_Action_ProcessByteData_m5E9BF37DFC0FEE3FE1AA26F2B1F81E248AC34B05 (void);
// 0x000001FC System.Void NetworkActionDemo::Action_SetColorRed()
extern void NetworkActionDemo_Action_SetColorRed_m9E972F4F1099CF47AAC07B6F73681CC75E1FBC77 (void);
// 0x000001FD System.Void NetworkActionDemo::Action_SetColorGreen()
extern void NetworkActionDemo_Action_SetColorGreen_mD2000946F4D41999E2D479A8DACB774006D75A0B (void);
// 0x000001FE System.Void NetworkActionDemo::Action_SetInt(System.Int32)
extern void NetworkActionDemo_Action_SetInt_mABB30D07F97761700F2868AF6EA5A2A8B7EF1F67 (void);
// 0x000001FF System.Void NetworkActionDemo::Action_SetFloat(System.Single)
extern void NetworkActionDemo_Action_SetFloat_mF77DA627B5D1C7D4887D46AA1E2304D8C9CC8458 (void);
// 0x00000200 System.Void NetworkActionDemo::Action_SetBool(System.Boolean)
extern void NetworkActionDemo_Action_SetBool_m6EC9B1110DE48B131681806E2E63ED41AD333A43 (void);
// 0x00000201 System.Void NetworkActionDemo::Action_SetString(System.String)
extern void NetworkActionDemo_Action_SetString_m622E1BF7AF69DEE4FFBC226F1F1FA663EE26A8FE (void);
// 0x00000202 System.Void NetworkActionDemo::Action_SetVector3(UnityEngine.Vector3)
extern void NetworkActionDemo_Action_SetVector3_m23C12C7C41C048EA60D6665F129098701E69BC46 (void);
// 0x00000203 System.Void NetworkActionDemo::.ctor()
extern void NetworkActionDemo__ctor_m40995801EE857AEB2237BFB9013AAB19F1D08C75 (void);
// 0x00000204 System.Void NetworkActionServer::Awake()
extern void NetworkActionServer_Awake_m356A33F0BA9A90FAF51BC7C88C09A65452C96E53 (void);
// 0x00000205 System.Void NetworkActionServer::Action_AddCmd(System.String)
extern void NetworkActionServer_Action_AddCmd_m83242BB96084BEAEF19EE0D2CB720FB61A0B1631 (void);
// 0x00000206 System.Void NetworkActionServer::Action_AddCmd(System.Byte[])
extern void NetworkActionServer_Action_AddCmd_m9FD8414C42AD2F633FEE631322FB67625BA7A6B5 (void);
// 0x00000207 System.Collections.IEnumerator NetworkActionServer::NetworkServerStart()
extern void NetworkActionServer_NetworkServerStart_mD5F977E58A6487DB3F3ED9C09334F9B67AB5A6EE (void);
// 0x00000208 System.Void NetworkActionServer::UdpReceiveCallback(System.IAsyncResult)
extern void NetworkActionServer_UdpReceiveCallback_m52BACCFA05E25B803F8B54211B3E4E1CF4139711 (void);
// 0x00000209 System.Void NetworkActionServer::StopServerListener()
extern void NetworkActionServer_StopServerListener_m85C3A8EBEFEB3FC487A52E5452309961BC38CF64 (void);
// 0x0000020A System.Void NetworkActionServer::Start()
extern void NetworkActionServer_Start_m682905EB145AD1A34E7FA5BEF3D5F5F69FA05FCF (void);
// 0x0000020B System.Void NetworkActionServer::Update()
extern void NetworkActionServer_Update_m3F95CBDF6493E6C0A1822D3ABE7D99C685746BAA (void);
// 0x0000020C System.Collections.IEnumerator NetworkActionServer::initServer()
extern void NetworkActionServer_initServer_mE1FC9057F31F33924D260DF9C25E20F2871DBA93 (void);
// 0x0000020D System.Void NetworkActionServer::AcceptCallback(System.IAsyncResult)
extern void NetworkActionServer_AcceptCallback_m7567880E91AD3285FD87DE77EB4EDE4E23048AD1 (void);
// 0x0000020E System.Collections.IEnumerator NetworkActionServer::senderCOR()
extern void NetworkActionServer_senderCOR_mD36F0F619872E82608780159A44632485206BADF (void);
// 0x0000020F System.Void NetworkActionServer::Sender()
extern void NetworkActionServer_Sender_m6FE9A162D8AE0FC33674212A17F42C08592EA96F (void);
// 0x00000210 System.Void NetworkActionServer::byteLengthToDataByteArray(System.Int32,System.Byte[])
extern void NetworkActionServer_byteLengthToDataByteArray_m6FF0525B5A68E990D64FC55C5E01D634E20A9F87 (void);
// 0x00000211 System.Void NetworkActionServer::StreamWrite(System.Net.Sockets.NetworkStream,System.Byte[],System.Byte[])
extern void NetworkActionServer_StreamWrite_m4A55CBE3D53E2A9728CF42F2DA22DA2F7C9F2F63 (void);
// 0x00000212 System.Void NetworkActionServer::RemoveClientConnection(System.Int32)
extern void NetworkActionServer_RemoveClientConnection_mC84BE472290CF9E442B2A299F9113D039BD93D02 (void);
// 0x00000213 System.Void NetworkActionServer::LOG(System.String)
extern void NetworkActionServer_LOG_m3D73FA1C73E2E79B5B095368E1803D968B5935C2 (void);
// 0x00000214 System.Void NetworkActionServer::StopAll()
extern void NetworkActionServer_StopAll_m8976E24CB6C40438CE55C8CA805CD1DD9EEA2A30 (void);
// 0x00000215 System.Void NetworkActionServer::OnApplicationQuit()
extern void NetworkActionServer_OnApplicationQuit_m64996CDE16C7BFAE0A511D6191F7610A97C70DCA (void);
// 0x00000216 System.Void NetworkActionServer::OnDisable()
extern void NetworkActionServer_OnDisable_mBA6DD57A03D7BF7B6615619B02C2754F487009C3 (void);
// 0x00000217 System.Void NetworkActionServer::OnDestroy()
extern void NetworkActionServer_OnDestroy_m2C56360714457E29A209A290B6137729686BBB5B (void);
// 0x00000218 System.Void NetworkActionServer::OnEnable()
extern void NetworkActionServer_OnEnable_m1FC9C76AEC36EB27E17F8153BF0798737E78406E (void);
// 0x00000219 System.Void NetworkActionServer::.ctor()
extern void NetworkActionServer__ctor_m23D3033A23672D90E3B4AD3CDC1253B1E94A8037 (void);
// 0x0000021A System.Void NetworkActionServer::<NetworkServerStart>b__27_0()
extern void NetworkActionServer_U3CNetworkServerStartU3Eb__27_0_m587120E8A38D4C97DDF732BC8D2AB35182FBC073 (void);
// 0x0000021B System.Void NetworkActionServer::<initServer>b__35_0()
extern void NetworkActionServer_U3CinitServerU3Eb__35_0_mAE1A30AAC7632B132A79EBB213D02321E61FDF36 (void);
// 0x0000021C System.Void NetworkActionServer::<initServer>b__35_1()
extern void NetworkActionServer_U3CinitServerU3Eb__35_1_mCF9490B0D57AF521FE01D6B0F0B6A37580D12CC3 (void);
// 0x0000021D System.Void NetworkActionServer/<NetworkServerStart>d__27::.ctor(System.Int32)
extern void U3CNetworkServerStartU3Ed__27__ctor_mCB2C852278E9F451A4E26CE1B413FB5110475993 (void);
// 0x0000021E System.Void NetworkActionServer/<NetworkServerStart>d__27::System.IDisposable.Dispose()
extern void U3CNetworkServerStartU3Ed__27_System_IDisposable_Dispose_m1CA80B998FA446E973ADFFCA18F495F5E298666E (void);
// 0x0000021F System.Boolean NetworkActionServer/<NetworkServerStart>d__27::MoveNext()
extern void U3CNetworkServerStartU3Ed__27_MoveNext_m4B7415CB64FAC61529468363166627B9BB9DC05A (void);
// 0x00000220 System.Object NetworkActionServer/<NetworkServerStart>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkServerStartU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7728EA245BC2307839F1B4A4149C65B8A951316 (void);
// 0x00000221 System.Void NetworkActionServer/<NetworkServerStart>d__27::System.Collections.IEnumerator.Reset()
extern void U3CNetworkServerStartU3Ed__27_System_Collections_IEnumerator_Reset_mAAE9C1A8BBD88F66F9DE95EEB2E0E3B9E81F8581 (void);
// 0x00000222 System.Object NetworkActionServer/<NetworkServerStart>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkServerStartU3Ed__27_System_Collections_IEnumerator_get_Current_m60B9EC98A4E541B38ED4166F2F1831DA0A61ACC7 (void);
// 0x00000223 System.Void NetworkActionServer/<initServer>d__35::.ctor(System.Int32)
extern void U3CinitServerU3Ed__35__ctor_m769D62C303CE7EC079538C0F8DBFB7BBEBDC95DB (void);
// 0x00000224 System.Void NetworkActionServer/<initServer>d__35::System.IDisposable.Dispose()
extern void U3CinitServerU3Ed__35_System_IDisposable_Dispose_m4A3E42FCC0B216893BF2FA0E85C42C10CF0A8550 (void);
// 0x00000225 System.Boolean NetworkActionServer/<initServer>d__35::MoveNext()
extern void U3CinitServerU3Ed__35_MoveNext_mE2DABB48FB243774BAA4A09FC74E9689EC8D03FD (void);
// 0x00000226 System.Object NetworkActionServer/<initServer>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CinitServerU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74370DDB23F94786FF901839AD1A30ABA66A50E3 (void);
// 0x00000227 System.Void NetworkActionServer/<initServer>d__35::System.Collections.IEnumerator.Reset()
extern void U3CinitServerU3Ed__35_System_Collections_IEnumerator_Reset_m3F1034C5ED02A11B8DEFA1FF9D6D715A02B7A43A (void);
// 0x00000228 System.Object NetworkActionServer/<initServer>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CinitServerU3Ed__35_System_Collections_IEnumerator_get_Current_m5837691CFA955995184267142FE100572A14D95D (void);
// 0x00000229 System.Void NetworkActionServer/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m93F546C886B1313964FB4A16327E7224C2CE1355 (void);
// 0x0000022A System.Void NetworkActionServer/<>c__DisplayClass37_0::<senderCOR>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CsenderCORU3Eb__0_m50C79F2F5E3EE434E4AFB98B1859E0568C04F3A5 (void);
// 0x0000022B System.Void NetworkActionServer/<senderCOR>d__37::.ctor(System.Int32)
extern void U3CsenderCORU3Ed__37__ctor_mBD331865F5DA308269214092CA652A6BECA978BE (void);
// 0x0000022C System.Void NetworkActionServer/<senderCOR>d__37::System.IDisposable.Dispose()
extern void U3CsenderCORU3Ed__37_System_IDisposable_Dispose_m46CCC2693D726470C1805ABBAD43E96EF5D38451 (void);
// 0x0000022D System.Boolean NetworkActionServer/<senderCOR>d__37::MoveNext()
extern void U3CsenderCORU3Ed__37_MoveNext_m8D7AD01909F0EC4C36DA16A7514F66E2970713A6 (void);
// 0x0000022E System.Object NetworkActionServer/<senderCOR>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsenderCORU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6E3F7FAFACD4AEEACAED1D538BB2E5824FF0C8B1 (void);
// 0x0000022F System.Void NetworkActionServer/<senderCOR>d__37::System.Collections.IEnumerator.Reset()
extern void U3CsenderCORU3Ed__37_System_Collections_IEnumerator_Reset_m42E68D766184FABAC0C3CF196255CB692C47B129 (void);
// 0x00000230 System.Object NetworkActionServer/<senderCOR>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CsenderCORU3Ed__37_System_Collections_IEnumerator_get_Current_m3312A4E6B021CC2099E75F2DA5E9E9742C578C0E (void);
// 0x00000231 System.Void NetworkActions_Debug::Action_TextUpdate(System.String)
extern void NetworkActions_Debug_Action_TextUpdate_m3376DFDE12FFDF9C3DFBA04B6E0E4DAC4AE573F5 (void);
// 0x00000232 System.Void NetworkActions_Debug::Start()
extern void NetworkActions_Debug_Start_mDFF34ACB60CF14594C8B901DE50AF7D52E75D5E3 (void);
// 0x00000233 System.Void NetworkActions_Debug::Update()
extern void NetworkActions_Debug_Update_mDE40BF3BBD4BD0A5081217DB24056EF97A581AED (void);
// 0x00000234 System.Void NetworkActions_Debug::.ctor()
extern void NetworkActions_Debug__ctor_m0617F64BCF20752A7F572A5B13DAE17F0DB4D602 (void);
// 0x00000235 System.String NetworkDiscovery::LocalIPAddress()
extern void NetworkDiscovery_LocalIPAddress_mBF16033A02B182501A303D10A25F26BB630861EF (void);
// 0x00000236 System.Void NetworkDiscovery::SetStreamingPort(System.Int32)
extern void NetworkDiscovery_SetStreamingPort_m91C7B677B575BA6D21E680CAA731314DA46F6788 (void);
// 0x00000237 System.Void NetworkDiscovery::Action_SetIsStreaming(System.Boolean)
extern void NetworkDiscovery_Action_SetIsStreaming_mB0A8D7AF81C9A43CBF3CD5FC257BCEA58E1D9D97 (void);
// 0x00000238 System.Void NetworkDiscovery::Start()
extern void NetworkDiscovery_Start_mC650727A1681B08D76685999C290594895EAA96A (void);
// 0x00000239 System.Void NetworkDiscovery::Update()
extern void NetworkDiscovery_Update_m89EBD95C2B788F298A2FF2993BC1A57875085663 (void);
// 0x0000023A System.Void NetworkDiscovery::Action_StartServer()
extern void NetworkDiscovery_Action_StartServer_mC9463914F5654F8D0E1A76F8AB06CD51660F270F (void);
// 0x0000023B System.Void NetworkDiscovery::Action_StartClient()
extern void NetworkDiscovery_Action_StartClient_mE1F22FE54ECBFDCF3E23277424F972D92531779A (void);
// 0x0000023C System.Collections.IEnumerator NetworkDiscovery::NetworkServerStart()
extern void NetworkDiscovery_NetworkServerStart_m18B5868E5008D8585C7E334F79B6F81C456B5E7A (void);
// 0x0000023D System.Void NetworkDiscovery::UdpReceiveCallback(System.IAsyncResult)
extern void NetworkDiscovery_UdpReceiveCallback_m6FD06145036B677B99A72ADC0863139846DD18B0 (void);
// 0x0000023E System.Collections.IEnumerator NetworkDiscovery::NetworkClientStart()
extern void NetworkDiscovery_NetworkClientStart_m0C87658DDD08D23DC867C26D9765B4696E42672C (void);
// 0x0000023F System.Void NetworkDiscovery::DebugLog(System.String)
extern void NetworkDiscovery_DebugLog_mD963FEC19FBF9F632F01969256410242A53CD7EE (void);
// 0x00000240 System.Void NetworkDiscovery::OnApplicationQuit()
extern void NetworkDiscovery_OnApplicationQuit_m8BE1B8FA5CED4BC79DEB83417FE01B60CDE1F029 (void);
// 0x00000241 System.Void NetworkDiscovery::OnDisable()
extern void NetworkDiscovery_OnDisable_mDFA183A01F541C092A1373C7B1780624A1EFDF4E (void);
// 0x00000242 System.Void NetworkDiscovery::OnDestroy()
extern void NetworkDiscovery_OnDestroy_mD71C97A283A1F7F72A9DC0673869D18B70F3C870 (void);
// 0x00000243 System.Void NetworkDiscovery::OnEnable()
extern void NetworkDiscovery_OnEnable_mFE481254F2E5D27A919B62CF9AA494E5AD15502C (void);
// 0x00000244 System.Void NetworkDiscovery::StartAll()
extern void NetworkDiscovery_StartAll_mF49F9F2CC070E3287A4077982435C22B81A8F8E6 (void);
// 0x00000245 System.Void NetworkDiscovery::StopAll()
extern void NetworkDiscovery_StopAll_mBCF2798C9C1DDA1F94F673D6BCB6AC7D202E7D6E (void);
// 0x00000246 System.Void NetworkDiscovery::.ctor()
extern void NetworkDiscovery__ctor_m959DBEF0B48CBFBB84AFAD83DACD48F4E8A7A605 (void);
// 0x00000247 System.Void NetworkDiscovery::<NetworkServerStart>b__25_0()
extern void NetworkDiscovery_U3CNetworkServerStartU3Eb__25_0_m69B3F6347256B019D2E3152F9D3A491EB8531419 (void);
// 0x00000248 System.Void NetworkDiscovery/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mD3CC686CEF342354C80F0FD8A679CEECF8D18E1B (void);
// 0x00000249 System.Void NetworkDiscovery/<>c__DisplayClass25_0::<NetworkServerStart>b__1()
extern void U3CU3Ec__DisplayClass25_0_U3CNetworkServerStartU3Eb__1_mFA14316C9EC26A8BF498812C744EC0DA36D5861E (void);
// 0x0000024A System.Void NetworkDiscovery/<NetworkServerStart>d__25::.ctor(System.Int32)
extern void U3CNetworkServerStartU3Ed__25__ctor_m9E6CA950392E1C360F05B286DE6ADC815EBBF241 (void);
// 0x0000024B System.Void NetworkDiscovery/<NetworkServerStart>d__25::System.IDisposable.Dispose()
extern void U3CNetworkServerStartU3Ed__25_System_IDisposable_Dispose_mDEFD1D08EF3DBB6ECEEE820BB5E8F4AE844534FA (void);
// 0x0000024C System.Boolean NetworkDiscovery/<NetworkServerStart>d__25::MoveNext()
extern void U3CNetworkServerStartU3Ed__25_MoveNext_m9707563A7EE49CDDA2142550E33F9E155F33FF94 (void);
// 0x0000024D System.Object NetworkDiscovery/<NetworkServerStart>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkServerStartU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75754473C27926F5FB4A444A1F25D332861E2769 (void);
// 0x0000024E System.Void NetworkDiscovery/<NetworkServerStart>d__25::System.Collections.IEnumerator.Reset()
extern void U3CNetworkServerStartU3Ed__25_System_Collections_IEnumerator_Reset_m3379482282EF79E2E7746C979E713167F99EB205 (void);
// 0x0000024F System.Object NetworkDiscovery/<NetworkServerStart>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkServerStartU3Ed__25_System_Collections_IEnumerator_get_Current_mB8AE6FB23F56DC8F37F185A99FF1BAA7A76727A8 (void);
// 0x00000250 System.Void NetworkDiscovery/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m8E57EF5811074A63AC563CD86D9093C44DE49EE1 (void);
// 0x00000251 System.Void NetworkDiscovery/<>c__DisplayClass28_0::<NetworkClientStart>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CNetworkClientStartU3Eb__0_m93B70165AC94F452E7CE31B781A627650FBACEBC (void);
// 0x00000252 System.Void NetworkDiscovery/<>c__DisplayClass28_1::.ctor()
extern void U3CU3Ec__DisplayClass28_1__ctor_m50146F8B01451F08AA535F8542D55D24D58B4A86 (void);
// 0x00000253 System.Void NetworkDiscovery/<>c__DisplayClass28_1::<NetworkClientStart>b__1()
extern void U3CU3Ec__DisplayClass28_1_U3CNetworkClientStartU3Eb__1_mF41931373697E36560FD28CA411D762CC9BAB810 (void);
// 0x00000254 System.Void NetworkDiscovery/<NetworkClientStart>d__28::.ctor(System.Int32)
extern void U3CNetworkClientStartU3Ed__28__ctor_mDCB1B215FA337B35B6B23CE5564442B6ED62A5BD (void);
// 0x00000255 System.Void NetworkDiscovery/<NetworkClientStart>d__28::System.IDisposable.Dispose()
extern void U3CNetworkClientStartU3Ed__28_System_IDisposable_Dispose_mB3863749BF1ABC4FB33E5A6E36F80506B34C24C3 (void);
// 0x00000256 System.Boolean NetworkDiscovery/<NetworkClientStart>d__28::MoveNext()
extern void U3CNetworkClientStartU3Ed__28_MoveNext_mD230EEE85BF15308CE925225DAB974BE0654FDB7 (void);
// 0x00000257 System.Object NetworkDiscovery/<NetworkClientStart>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21EA8CB49CD8ABA66A584C5B28405068771A97D7 (void);
// 0x00000258 System.Void NetworkDiscovery/<NetworkClientStart>d__28::System.Collections.IEnumerator.Reset()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_Reset_mE6C03EC48AD7CD76E1B99BE573B7656B65B49C1B (void);
// 0x00000259 System.Object NetworkDiscovery/<NetworkClientStart>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_get_Current_m063DFE03B905450BF7EC4D8C2B51D2C6ECB182B2 (void);
// 0x0000025A System.Void FMNetwork_Demo::Action_ProcessStringData(System.String)
extern void FMNetwork_Demo_Action_ProcessStringData_m3D969B44A7E78D832F3889B52C56B9E52CC3E5FB (void);
// 0x0000025B System.Void FMNetwork_Demo::Action_ProcessByteData(System.Byte[])
extern void FMNetwork_Demo_Action_ProcessByteData_m673512FFA11D6BE5FFB46F2140829CC4C345CE54 (void);
// 0x0000025C System.Void FMNetwork_Demo::Action_ShowRawByteLength(System.Byte[])
extern void FMNetwork_Demo_Action_ShowRawByteLength_m521940D61EE7A037670C3D7ACE4AE9624982B9F4 (void);
// 0x0000025D System.Void FMNetwork_Demo::Start()
extern void FMNetwork_Demo_Start_m90EE9BCFCC4988B92570EE13F156FEFB6E2EAAFD (void);
// 0x0000025E System.Void FMNetwork_Demo::Action_SetTargetIP(System.String)
extern void FMNetwork_Demo_Action_SetTargetIP_m2E9DF0B615B1F903BE8E15A9DB21C39D346D79F0 (void);
// 0x0000025F System.Void FMNetwork_Demo::Action_SendRandomLargeFile()
extern void FMNetwork_Demo_Action_SendRandomLargeFile_mE35B5499B725B7E1E9EBD4ACCBB71FDE07AD1D06 (void);
// 0x00000260 System.Void FMNetwork_Demo::Action_DemoReceivedLargeFile(System.Byte[])
extern void FMNetwork_Demo_Action_DemoReceivedLargeFile_m4E56CF31EC68A86365D60F17285D3D1B62E92DA1 (void);
// 0x00000261 System.Void FMNetwork_Demo::Update()
extern void FMNetwork_Demo_Update_m60ECFAE166BC35DCA788ECD757EE8683377E3984 (void);
// 0x00000262 System.Void FMNetwork_Demo::Action_SendByteToAll(System.Int32)
extern void FMNetwork_Demo_Action_SendByteToAll_m5056938F5DE5E34B349DA4530A33AA03FCD10184 (void);
// 0x00000263 System.Void FMNetwork_Demo::Action_SendByteToServer(System.Int32)
extern void FMNetwork_Demo_Action_SendByteToServer_m66C186E56C28D6759BC4755013E92EC0515EE269 (void);
// 0x00000264 System.Void FMNetwork_Demo::Action_SendByteToOthers(System.Int32)
extern void FMNetwork_Demo_Action_SendByteToOthers_m8B68D9B4C3781CBC246841613C6DBA6F7321EADF (void);
// 0x00000265 System.Void FMNetwork_Demo::Action_SendByteToTarget(System.Int32)
extern void FMNetwork_Demo_Action_SendByteToTarget_m1F8D84944B2A7A17A6561FEBA1833E94A9B028E6 (void);
// 0x00000266 System.Void FMNetwork_Demo::Action_SendTextToAll(System.String)
extern void FMNetwork_Demo_Action_SendTextToAll_m29260CFECDCA40EA5D3EE7B0E8024A2675E586CB (void);
// 0x00000267 System.Void FMNetwork_Demo::Action_SendTextToServer(System.String)
extern void FMNetwork_Demo_Action_SendTextToServer_m999D775A61F527A431B671738D02650E37D38C45 (void);
// 0x00000268 System.Void FMNetwork_Demo::Action_SendTextToOthers(System.String)
extern void FMNetwork_Demo_Action_SendTextToOthers_mA5A23FA7923F26BD85BCF39689C19DE4A35ACCE8 (void);
// 0x00000269 System.Void FMNetwork_Demo::Action_SendTextToTarget(System.String)
extern void FMNetwork_Demo_Action_SendTextToTarget_m3FF203C67A7E888BBC74224EBD60EF7A485BE017 (void);
// 0x0000026A System.Void FMNetwork_Demo::Action_SendRandomTextToAll()
extern void FMNetwork_Demo_Action_SendRandomTextToAll_m18C744E4BD7038A24F70C2D0088878105FA5ECD1 (void);
// 0x0000026B System.Void FMNetwork_Demo::Action_SendRandomTextToServer()
extern void FMNetwork_Demo_Action_SendRandomTextToServer_m383B330C05F1E0D6972BAC9C339A90AED7D1953F (void);
// 0x0000026C System.Void FMNetwork_Demo::Action_SendRandomTextToOthers()
extern void FMNetwork_Demo_Action_SendRandomTextToOthers_m6041D78D1F03A3766E87803DAF7AA19419F0B174 (void);
// 0x0000026D System.Void FMNetwork_Demo::Action_SendRandomTextToTarget()
extern void FMNetwork_Demo_Action_SendRandomTextToTarget_m4EF8E2C5C6024BD23E61A33A21C65F94A2D82435 (void);
// 0x0000026E System.Void FMNetwork_Demo::.ctor()
extern void FMNetwork_Demo__ctor_m32525BDC5CB7720599BD9B312AC50C9D4E99BDEB (void);
// 0x0000026F System.Void FMNetwork_DemoAnimation::Start()
extern void FMNetwork_DemoAnimation_Start_m55994DC6FECDEA65967B1F36CC7F18430FBA7DAF (void);
// 0x00000270 System.Void FMNetwork_DemoAnimation::Update()
extern void FMNetwork_DemoAnimation_Update_m0F7ABF21F646A8E4903A4315E9DA4867B8A6A1BB (void);
// 0x00000271 System.Void FMNetwork_DemoAnimation::.ctor()
extern void FMNetwork_DemoAnimation__ctor_m5BD90AB069BD702037121B8C550B74858192209E (void);
// 0x00000272 System.Void FMClient::Action_AddPacket(System.Byte[],FMSendType)
extern void FMClient_Action_AddPacket_m4384A8D2663748C0DAC53985D41EF579215B5231 (void);
// 0x00000273 System.Void FMClient::Action_AddPacket(System.String,FMSendType)
extern void FMClient_Action_AddPacket_m111859BF278F6E4B9982A2414D59E5D1F2F87049 (void);
// 0x00000274 System.Void FMClient::Action_AddPacket(System.Byte[],System.String)
extern void FMClient_Action_AddPacket_m2E3494FA4D747670B5F15D8D518725E3693132C0 (void);
// 0x00000275 System.Void FMClient::Action_AddPacket(System.String,System.String)
extern void FMClient_Action_AddPacket_m5EB2125BA3CCE60516506CA78E36D1D68C2224E1 (void);
// 0x00000276 System.Void FMClient::Start()
extern void FMClient_Start_m1A8E277C432EB72A03A979A960A23FCA372FC8C5 (void);
// 0x00000277 System.Void FMClient::Update()
extern void FMClient_Update_m4BE2E1C9C244EDBE8FA9CAD154942958F8DBC3B6 (void);
// 0x00000278 System.Void FMClient::Action_StartClient()
extern void FMClient_Action_StartClient_m0A5D15C0CCF7EE2FC756E0A96C8BCCAE6B6FC54B (void);
// 0x00000279 System.Collections.IEnumerator FMClient::NetworkClientStart()
extern void FMClient_NetworkClientStart_m5C1D3F095A33D1FC774B8C6EC098F7721D7E31B4 (void);
// 0x0000027A System.Collections.IEnumerator FMClient::MainThreadSender()
extern void FMClient_MainThreadSender_m6D18ADAB3294A9DEA6DF2D29C40F1F50BF6E99C8 (void);
// 0x0000027B System.Void FMClient::Sender()
extern void FMClient_Sender_m25F98C9C7A0C3D7D0FF2D9858B4E9B9BF4660356 (void);
// 0x0000027C System.Void FMClient::DebugLog(System.String)
extern void FMClient_DebugLog_m5D568908BAFB34296E19BAA3D9162453B3E5B0AF (void);
// 0x0000027D System.Void FMClient::OnApplicationQuit()
extern void FMClient_OnApplicationQuit_m00DD985C52D0FF12D5B91A51223E9EE363DFB020 (void);
// 0x0000027E System.Void FMClient::OnDisable()
extern void FMClient_OnDisable_m371622F814BF5311D1A36FC8A639E6F35478E1D7 (void);
// 0x0000027F System.Void FMClient::OnDestroy()
extern void FMClient_OnDestroy_m5BAEE745A17764085C604385EAF4E9EA2D2BA696 (void);
// 0x00000280 System.Void FMClient::OnEnable()
extern void FMClient_OnEnable_m8C2BDACC0D09C1C55C69C9DAD02189D62099264A (void);
// 0x00000281 System.Void FMClient::StartAll()
extern void FMClient_StartAll_mE2B2ED1EA90BA6D9CF9503B0FD6B7BFF47E67400 (void);
// 0x00000282 System.Void FMClient::StopAll()
extern void FMClient_StopAll_mAD5CC012381EF05F81DE74FC20A2CC1BC32AB5D9 (void);
// 0x00000283 System.Void FMClient::.ctor()
extern void FMClient__ctor_m3368DB666B571C4CA0EF2CC98E7DD4767D97C087 (void);
// 0x00000284 System.Void FMClient::<NetworkClientStart>b__28_0()
extern void FMClient_U3CNetworkClientStartU3Eb__28_0_mF1E5191AE17244C6DC39C9E713F0283479E83BC3 (void);
// 0x00000285 System.Void FMClient::<NetworkClientStart>b__28_1()
extern void FMClient_U3CNetworkClientStartU3Eb__28_1_m59932624A282FFE56FAED1432B64DA442A8A659F (void);
// 0x00000286 System.Void FMClient/<NetworkClientStart>d__28::.ctor(System.Int32)
extern void U3CNetworkClientStartU3Ed__28__ctor_m74FB441E0A122B39FA3E8BACE588F244CC3442C6 (void);
// 0x00000287 System.Void FMClient/<NetworkClientStart>d__28::System.IDisposable.Dispose()
extern void U3CNetworkClientStartU3Ed__28_System_IDisposable_Dispose_mDAF5B6EB73682E6DAD0467B3CC135597900FDA15 (void);
// 0x00000288 System.Boolean FMClient/<NetworkClientStart>d__28::MoveNext()
extern void U3CNetworkClientStartU3Ed__28_MoveNext_mDFF3581CDC0511F260C7C998E8A09130E78EC5CC (void);
// 0x00000289 System.Object FMClient/<NetworkClientStart>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD38932EFA7E11FA248F7DCE962FD7BE819F7F151 (void);
// 0x0000028A System.Void FMClient/<NetworkClientStart>d__28::System.Collections.IEnumerator.Reset()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_Reset_mAF78F0B9CF45FC45FDFF8CC2937CEE2789C18471 (void);
// 0x0000028B System.Object FMClient/<NetworkClientStart>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_get_Current_mE01B0407D66C75A6FB8EDA7C05DD36C2BD5ABC48 (void);
// 0x0000028C System.Void FMClient/<MainThreadSender>d__30::.ctor(System.Int32)
extern void U3CMainThreadSenderU3Ed__30__ctor_mA1D1255D636432AF89AF3753663A87FF0D6CE9B5 (void);
// 0x0000028D System.Void FMClient/<MainThreadSender>d__30::System.IDisposable.Dispose()
extern void U3CMainThreadSenderU3Ed__30_System_IDisposable_Dispose_m40BAFB1B370680F27AA552B07ABDBF86CE9AD75E (void);
// 0x0000028E System.Boolean FMClient/<MainThreadSender>d__30::MoveNext()
extern void U3CMainThreadSenderU3Ed__30_MoveNext_m9F7A69D36F655852240D7388286395A0C8156143 (void);
// 0x0000028F System.Object FMClient/<MainThreadSender>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMainThreadSenderU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16B157F33D163BDDBEC08E74BA62E7FCDF7FD29A (void);
// 0x00000290 System.Void FMClient/<MainThreadSender>d__30::System.Collections.IEnumerator.Reset()
extern void U3CMainThreadSenderU3Ed__30_System_Collections_IEnumerator_Reset_m9DD8471E6B7633F4DBA91DD16BFE95E879C46259 (void);
// 0x00000291 System.Object FMClient/<MainThreadSender>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CMainThreadSenderU3Ed__30_System_Collections_IEnumerator_get_Current_m3A2D221196A83D49C0259B9A97B0E912C667B7D0 (void);
// 0x00000292 System.String FMNetworkManager::LocalIPAddress()
extern void FMNetworkManager_LocalIPAddress_m2A21AA0317895E069D17566788F2873954084F54 (void);
// 0x00000293 System.String FMNetworkManager::get_ReadLocalIPAddress()
extern void FMNetworkManager_get_ReadLocalIPAddress_m7DCC079152B8447F43A53F32EF05E1152A879265 (void);
// 0x00000294 System.Void FMNetworkManager::Action_SendNetworkObjectTransform()
extern void FMNetworkManager_Action_SendNetworkObjectTransform_m0C03C34EAE2F0021EBB5B2363A343AC88EC6325B (void);
// 0x00000295 System.Byte[] FMNetworkManager::EncodeTransformByte(UnityEngine.GameObject)
extern void FMNetworkManager_EncodeTransformByte_m3A9DF15CAE923FDD8F22770C60CC0CDE077C367C (void);
// 0x00000296 System.Single[] FMNetworkManager::DecodeByteToFloatArray(System.Byte[],System.Int32)
extern void FMNetworkManager_DecodeByteToFloatArray_mC127030F2A1228A72386C10319A3708AFAEF764B (void);
// 0x00000297 System.Void FMNetworkManager::Action_SyncNetworkObjectTransform(System.Byte[])
extern void FMNetworkManager_Action_SyncNetworkObjectTransform_m03149D79DB7D9D17F45662668009A96ACFCDBDCE (void);
// 0x00000298 System.Void FMNetworkManager::Action_InitAsServer()
extern void FMNetworkManager_Action_InitAsServer_m0B1E52A07D5B6D78481F1A82DA5FE0B2F6430158 (void);
// 0x00000299 System.Void FMNetworkManager::Action_InitAsClient()
extern void FMNetworkManager_Action_InitAsClient_mF3D72A59BAF7CAC0173117BFDC26744B20D55AD0 (void);
// 0x0000029A System.Void FMNetworkManager::Action_InitStereoPi()
extern void FMNetworkManager_Action_InitStereoPi_mAA947D2C0A9F98EE6B088D1E7D7D035741BFF248 (void);
// 0x0000029B System.Void FMNetworkManager::Init()
extern void FMNetworkManager_Init_m11D9E28E3100F1496F90BB647DC796D9DD71C6DB (void);
// 0x0000029C System.Void FMNetworkManager::Awake()
extern void FMNetworkManager_Awake_m1774513DDB5D216E4AACFCF43D6E65E33AC84A27 (void);
// 0x0000029D System.Void FMNetworkManager::Start()
extern void FMNetworkManager_Start_m320FE8165AD4EE5B71C9DC437E246D9FEFEF3989 (void);
// 0x0000029E System.Void FMNetworkManager::Update()
extern void FMNetworkManager_Update_mA5198BADACFAF5FBDBC10C4779BDD30A1938AE24 (void);
// 0x0000029F System.Void FMNetworkManager::Send(System.Byte[],FMSendType)
extern void FMNetworkManager_Send_m8C2F12C1A7F5F268D037CEA720A4B3BCAE674E73 (void);
// 0x000002A0 System.Void FMNetworkManager::Send(System.String,FMSendType)
extern void FMNetworkManager_Send_m4108634695E8995D0C3E1141BE2555A1039F34DB (void);
// 0x000002A1 System.Void FMNetworkManager::SendToAll(System.Byte[])
extern void FMNetworkManager_SendToAll_mDAE7E215456CAEC0E0AC88EA0B1AEACB3CECC826 (void);
// 0x000002A2 System.Void FMNetworkManager::SendToServer(System.Byte[])
extern void FMNetworkManager_SendToServer_m8D48C24B1C491EDF21C76C04C86A24FCD623B1A3 (void);
// 0x000002A3 System.Void FMNetworkManager::SendToOthers(System.Byte[])
extern void FMNetworkManager_SendToOthers_m87549E3034721974B328A4E42A97E37405ED3AE0 (void);
// 0x000002A4 System.Void FMNetworkManager::SendToAll(System.String)
extern void FMNetworkManager_SendToAll_mBC96E0C6A1749ED039D5526E51511EE4F1476D57 (void);
// 0x000002A5 System.Void FMNetworkManager::SendToServer(System.String)
extern void FMNetworkManager_SendToServer_mB52C54BA4BABF484F45687BD0710F633EC79CF3F (void);
// 0x000002A6 System.Void FMNetworkManager::SendToOthers(System.String)
extern void FMNetworkManager_SendToOthers_m18812E19A37300606D07E6AD20D417F8DF21136E (void);
// 0x000002A7 System.Void FMNetworkManager::SendToTarget(System.Byte[],System.String)
extern void FMNetworkManager_SendToTarget_m0008F7DE2509EA8A0EB49A176D1F1E30F274A205 (void);
// 0x000002A8 System.Void FMNetworkManager::SendToTarget(System.String,System.String)
extern void FMNetworkManager_SendToTarget_m1B60D0F1FDA5826C5F7DACDF1B134106A8CBAC6A (void);
// 0x000002A9 System.Void FMNetworkManager::Send(System.Byte[],FMSendType,System.String)
extern void FMNetworkManager_Send_mC88400D86C67E664044CA4DCD13B1823818A3E48 (void);
// 0x000002AA System.Void FMNetworkManager::Send(System.String,FMSendType,System.String)
extern void FMNetworkManager_Send_mFBEF41B525A0DE6B5677224BEF5112937DE982DC (void);
// 0x000002AB System.Void FMNetworkManager::Action_ReloadScene()
extern void FMNetworkManager_Action_ReloadScene_mD9AAC5E14852F3972B8A70E5F11378AC1363621B (void);
// 0x000002AC System.Void FMNetworkManager::.ctor()
extern void FMNetworkManager__ctor_mCF6D22B4209B321D7860ECA779E5C924987D8CD1 (void);
// 0x000002AD System.Void FMNetworkManager/FMServerSettings::.ctor()
extern void FMServerSettings__ctor_mB88B438B37E13C1A9F610E2179A3437C2375D0A5 (void);
// 0x000002AE System.Void FMNetworkManager/FMClientSettings::.ctor()
extern void FMClientSettings__ctor_m10B41CF7BD14A31A54439D1DB8E3BE39CF2A879C (void);
// 0x000002AF System.Void FMNetworkManager/FMStereoPiSettings::.ctor()
extern void FMStereoPiSettings__ctor_mF731D9003D30D2816C10196E300E94551AA15CCD (void);
// 0x000002B0 System.Void FMServer::Action_CheckClientStatus(System.String)
extern void FMServer_Action_CheckClientStatus_m08CA18AA90ACF9F85369E57FDA8746BAADF87C01 (void);
// 0x000002B1 System.Void FMServer::Action_AddPacket(System.Byte[],FMSendType)
extern void FMServer_Action_AddPacket_m0CF954C60BCB5AF4D9187886DF3579BD6991309B (void);
// 0x000002B2 System.Void FMServer::Action_AddPacket(System.String,FMSendType)
extern void FMServer_Action_AddPacket_mC41AAF3BB2D3D62A3251FEC256C862FE7285926D (void);
// 0x000002B3 System.Void FMServer::Action_AddPacket(System.Byte[],System.String)
extern void FMServer_Action_AddPacket_m7ACAD0E45DC1B13F525C629F1E462EA69388C94E (void);
// 0x000002B4 System.Void FMServer::Action_AddPacket(System.String,System.String)
extern void FMServer_Action_AddPacket_mF24F6E7C93B2F1A4FE5E080D168C931F30C86AD1 (void);
// 0x000002B5 System.Void FMServer::Action_AddPacket(FMPacket)
extern void FMServer_Action_AddPacket_mD6C916EA5636A65AF6C7FDE4329CDD9AA1DAA9B6 (void);
// 0x000002B6 System.Void FMServer::Action_AddNetworkObjectPacket(System.Byte[],FMSendType)
extern void FMServer_Action_AddNetworkObjectPacket_mAE1BD8B7D3BEAEA1509D798BDE8D6F32C5AAB08A (void);
// 0x000002B7 System.Void FMServer::Start()
extern void FMServer_Start_mB8A6F846AD04910BB18CE17165A6FE5D39F54C8F (void);
// 0x000002B8 System.Void FMServer::Update()
extern void FMServer_Update_mDFE639F168331ED62B0D2DA16D1F07AF9C2BDD30 (void);
// 0x000002B9 System.Void FMServer::Action_StartServer()
extern void FMServer_Action_StartServer_mD183AA00CAA2FD6053AC436C2F2F337F0E03C691 (void);
// 0x000002BA System.Void FMServer::InitializeServerListener()
extern void FMServer_InitializeServerListener_m9E9268F9C4B46C9DDE416D3B36A8F9129FDEEDC6 (void);
// 0x000002BB System.Void FMServer::MulticastChecker()
extern void FMServer_MulticastChecker_m96C7979AAE41349983F70C087E9A9C0910EB5A97 (void);
// 0x000002BC System.Collections.IEnumerator FMServer::MulticastCheckerCOR()
extern void FMServer_MulticastCheckerCOR_mE2725B33D0CB15DCAAF488FCC012F15A320B9EE9 (void);
// 0x000002BD System.Collections.IEnumerator FMServer::NetworkServerStart()
extern void FMServer_NetworkServerStart_mADE0EFA3E23400B33FF8D8686241BB372317FB91 (void);
// 0x000002BE System.Void FMServer::UdpReceiveCallback(System.IAsyncResult)
extern void FMServer_UdpReceiveCallback_mC81B62C1A7D2BB0DF083D0C9CDC4782EB9326019 (void);
// 0x000002BF System.Collections.IEnumerator FMServer::MainThreadSender()
extern void FMServer_MainThreadSender_m583131AE11A85995F22EA303AEA1E78F1CF5F6C7 (void);
// 0x000002C0 System.Void FMServer::Sender()
extern void FMServer_Sender_m75BBFF91484C9D7E2CB64034A9C40B29F2FCE51E (void);
// 0x000002C1 System.Void FMServer::DebugLog(System.String)
extern void FMServer_DebugLog_m480EA7DD3B45EAB405F1370083664ECD87B0C703 (void);
// 0x000002C2 System.Void FMServer::OnApplicationQuit()
extern void FMServer_OnApplicationQuit_m2B06A75C93CF68FC6CB2CE67D8E82C2C2830A8F8 (void);
// 0x000002C3 System.Void FMServer::OnDisable()
extern void FMServer_OnDisable_mE066DD74919E26D3AE2B23280B495727955AA5D5 (void);
// 0x000002C4 System.Void FMServer::OnDestroy()
extern void FMServer_OnDestroy_mA6687ACC0736265F3CA1A56B696CF7174DBFA2E2 (void);
// 0x000002C5 System.Void FMServer::OnEnable()
extern void FMServer_OnEnable_m527C733A41C0BE99DD39BEB3F7295E2752F61B2A (void);
// 0x000002C6 System.Void FMServer::StartAll()
extern void FMServer_StartAll_m387372109DC7ADCDC641ED6BCA6D799530A11BFA (void);
// 0x000002C7 System.Void FMServer::StopAll()
extern void FMServer_StopAll_m98E16C304EB7599AE0A96AEFDF889C9D34B4F88A (void);
// 0x000002C8 System.Void FMServer::.ctor()
extern void FMServer__ctor_m63CB3AF1617032C73B0D3AC8DA1EF801DB26343E (void);
// 0x000002C9 System.Void FMServer::<NetworkServerStart>b__33_0()
extern void FMServer_U3CNetworkServerStartU3Eb__33_0_m94D4E67DD25C57BA594883C8BB8A3938F38A17EC (void);
// 0x000002CA System.Void FMServer::<NetworkServerStart>b__33_1()
extern void FMServer_U3CNetworkServerStartU3Eb__33_1_mAB86250B7255398C9E6DE59B2A66E6E5339D40C5 (void);
// 0x000002CB System.Void FMServer/ConnectedClient::Send(System.Byte[])
extern void ConnectedClient_Send_m61BF16FE82C71CC32EF19977F327585599C03342 (void);
// 0x000002CC System.Void FMServer/ConnectedClient::Close()
extern void ConnectedClient_Close_m691799BBCE9CA16F7FDCE62885395B09F46D3DDF (void);
// 0x000002CD System.Void FMServer/ConnectedClient::.ctor()
extern void ConnectedClient__ctor_mD3478E8BF36F6097F908D0052829192CB80F748B (void);
// 0x000002CE System.Void FMServer/<MulticastCheckerCOR>d__32::.ctor(System.Int32)
extern void U3CMulticastCheckerCORU3Ed__32__ctor_m22803B6B8723DE8AAD5DBDAA07727CC3ED4F697D (void);
// 0x000002CF System.Void FMServer/<MulticastCheckerCOR>d__32::System.IDisposable.Dispose()
extern void U3CMulticastCheckerCORU3Ed__32_System_IDisposable_Dispose_m3E6A6A5B9B8F959D5E3A97FE0C16DF116F3E1EA3 (void);
// 0x000002D0 System.Boolean FMServer/<MulticastCheckerCOR>d__32::MoveNext()
extern void U3CMulticastCheckerCORU3Ed__32_MoveNext_m382F62F98DD9118E9DC32F45E7FC95C2ABDC830D (void);
// 0x000002D1 System.Object FMServer/<MulticastCheckerCOR>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMulticastCheckerCORU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50E18A0ED10006EC08B1212D96DD4108123F33B1 (void);
// 0x000002D2 System.Void FMServer/<MulticastCheckerCOR>d__32::System.Collections.IEnumerator.Reset()
extern void U3CMulticastCheckerCORU3Ed__32_System_Collections_IEnumerator_Reset_m16BCF26480237987CB13158B859DDA879F20DC2F (void);
// 0x000002D3 System.Object FMServer/<MulticastCheckerCOR>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CMulticastCheckerCORU3Ed__32_System_Collections_IEnumerator_get_Current_m57CE0AA50FDC05977BD0BF3BB5D1F0030A95C980 (void);
// 0x000002D4 System.Void FMServer/<NetworkServerStart>d__33::.ctor(System.Int32)
extern void U3CNetworkServerStartU3Ed__33__ctor_m003D95822BA086D2EBF20C01A8AF02ADB09A4792 (void);
// 0x000002D5 System.Void FMServer/<NetworkServerStart>d__33::System.IDisposable.Dispose()
extern void U3CNetworkServerStartU3Ed__33_System_IDisposable_Dispose_mDD16B6E0A35425113C4F1F49E684BFD4F3954B4C (void);
// 0x000002D6 System.Boolean FMServer/<NetworkServerStart>d__33::MoveNext()
extern void U3CNetworkServerStartU3Ed__33_MoveNext_m143763191D31EB48F7E3865A2A8C3B569C00668B (void);
// 0x000002D7 System.Object FMServer/<NetworkServerStart>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkServerStartU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57D8D888ABDDD6F8373014058D5946F23AE912B1 (void);
// 0x000002D8 System.Void FMServer/<NetworkServerStart>d__33::System.Collections.IEnumerator.Reset()
extern void U3CNetworkServerStartU3Ed__33_System_Collections_IEnumerator_Reset_m987EF30AA6FAFB83981381D953E31ACFD3F8E791 (void);
// 0x000002D9 System.Object FMServer/<NetworkServerStart>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkServerStartU3Ed__33_System_Collections_IEnumerator_get_Current_mE9B204506AB7045EE686A73EBFA38CAB50BC7C15 (void);
// 0x000002DA System.Void FMServer/<MainThreadSender>d__35::.ctor(System.Int32)
extern void U3CMainThreadSenderU3Ed__35__ctor_mEEFF366163521219123028747B65BAD54CAE8413 (void);
// 0x000002DB System.Void FMServer/<MainThreadSender>d__35::System.IDisposable.Dispose()
extern void U3CMainThreadSenderU3Ed__35_System_IDisposable_Dispose_mB11B958AC613C529041005C1E719367C7568DD70 (void);
// 0x000002DC System.Boolean FMServer/<MainThreadSender>d__35::MoveNext()
extern void U3CMainThreadSenderU3Ed__35_MoveNext_m9434EFEF081094ABE2E73A237FE7D00E4F2FF062 (void);
// 0x000002DD System.Object FMServer/<MainThreadSender>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMainThreadSenderU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5992ED54360CC40B22F777EB6D69E2038A69CD5 (void);
// 0x000002DE System.Void FMServer/<MainThreadSender>d__35::System.Collections.IEnumerator.Reset()
extern void U3CMainThreadSenderU3Ed__35_System_Collections_IEnumerator_Reset_m5840A514453D43117ABBA39DC19A46B7ED52ACB5 (void);
// 0x000002DF System.Object FMServer/<MainThreadSender>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CMainThreadSenderU3Ed__35_System_Collections_IEnumerator_get_Current_m12BA27A2837055EE2552D4AE931685EE06F03EA8 (void);
// 0x000002E0 System.Void FMStereoPi::MulticastChecker()
extern void FMStereoPi_MulticastChecker_mA1B76F219B70818A47C318531F80DADDB8E4163F (void);
// 0x000002E1 System.Collections.IEnumerator FMStereoPi::NetworkServerStartTCP()
extern void FMStereoPi_NetworkServerStartTCP_mD09D99ADF2FE7161A475D5C62F73678B70F9CDF2 (void);
// 0x000002E2 System.Collections.IEnumerator FMStereoPi::TCPReceiverCOR(System.Net.Sockets.TcpClient,System.Net.Sockets.NetworkStream)
extern void FMStereoPi_TCPReceiverCOR_m053F2533E08C08E7B2BAD0DDE160095E29FC58F5 (void);
// 0x000002E3 System.Collections.IEnumerator FMStereoPi::NetworkClientStartUDP()
extern void FMStereoPi_NetworkClientStartUDP_m6F2FD2FE29DA7DFAB81AF847F517ADF42A4114D1 (void);
// 0x000002E4 System.Void FMStereoPi::Action_StartClient()
extern void FMStereoPi_Action_StartClient_mF7F98EAF478F5C1AAFFDDA8F4A2152915B04F0A8 (void);
// 0x000002E5 System.Void FMStereoPi::Action_StopClient()
extern void FMStereoPi_Action_StopClient_m29CE522A4A8474C36B42467E739F64CD3C623D1B (void);
// 0x000002E6 System.Void FMStereoPi::StartAll()
extern void FMStereoPi_StartAll_m4A520CB05FF5DF8D90B04C8CD41727A4D5379DA9 (void);
// 0x000002E7 System.Void FMStereoPi::StopAll()
extern void FMStereoPi_StopAll_mC9F120C0C8BEF4243E0A51332288DDF50DB03E20 (void);
// 0x000002E8 System.Void FMStereoPi::Start()
extern void FMStereoPi_Start_m403748CA9441AD024F44C5CC053A59566C9540F4 (void);
// 0x000002E9 System.Void FMStereoPi::Update()
extern void FMStereoPi_Update_mB0CC2343CD4F7425C4AB7B2C99265417CBFCADE3 (void);
// 0x000002EA System.Void FMStereoPi::DebugLog(System.String)
extern void FMStereoPi_DebugLog_m4FEBEB55ECCA8F927D81F38563EE4892E6C7C331 (void);
// 0x000002EB System.Void FMStereoPi::OnApplicationQuit()
extern void FMStereoPi_OnApplicationQuit_m8E9703593C9E3FA30734380F8B17CC348358D388 (void);
// 0x000002EC System.Void FMStereoPi::OnDisable()
extern void FMStereoPi_OnDisable_mFE1831B6291DC460253CCEDA89A84D935965252F (void);
// 0x000002ED System.Void FMStereoPi::OnDestroy()
extern void FMStereoPi_OnDestroy_m002D26363D2CCDEC8CB03355EF4CBEED67BCBA09 (void);
// 0x000002EE System.Void FMStereoPi::OnEnable()
extern void FMStereoPi_OnEnable_m5B8CEA197848EAD2A2B0F5B78F4A1F2B22278E7B (void);
// 0x000002EF System.Void FMStereoPi::.ctor()
extern void FMStereoPi__ctor_mEA034CD53544599F4F56FC0F195FF963EACF43E3 (void);
// 0x000002F0 System.Void FMStereoPi::<NetworkServerStartTCP>b__17_0()
extern void FMStereoPi_U3CNetworkServerStartTCPU3Eb__17_0_mAD211109B9D9ACF6355D40D7D09D2E30C91A0926 (void);
// 0x000002F1 System.Void FMStereoPi::<NetworkServerStartTCP>b__17_1()
extern void FMStereoPi_U3CNetworkServerStartTCPU3Eb__17_1_mFF8E324C3E21AC1048101868450F80A7B6DE1F08 (void);
// 0x000002F2 System.Void FMStereoPi::<NetworkClientStartUDP>b__19_0()
extern void FMStereoPi_U3CNetworkClientStartUDPU3Eb__19_0_mE7E8B0756AAED4615D1A62351D1CF295303F8471 (void);
// 0x000002F3 System.Void FMStereoPi/<NetworkServerStartTCP>d__17::.ctor(System.Int32)
extern void U3CNetworkServerStartTCPU3Ed__17__ctor_mE0314820D968F3A49C0E6FF5B01E26717284BB7B (void);
// 0x000002F4 System.Void FMStereoPi/<NetworkServerStartTCP>d__17::System.IDisposable.Dispose()
extern void U3CNetworkServerStartTCPU3Ed__17_System_IDisposable_Dispose_mBE078FF37D259E010A03A6484F4A340A40D74D71 (void);
// 0x000002F5 System.Boolean FMStereoPi/<NetworkServerStartTCP>d__17::MoveNext()
extern void U3CNetworkServerStartTCPU3Ed__17_MoveNext_mF489580D4B92A92371F48E2429809DD9396ADF51 (void);
// 0x000002F6 System.Object FMStereoPi/<NetworkServerStartTCP>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkServerStartTCPU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C9EA44B0303A454404E8381F01F118C8D0F8459 (void);
// 0x000002F7 System.Void FMStereoPi/<NetworkServerStartTCP>d__17::System.Collections.IEnumerator.Reset()
extern void U3CNetworkServerStartTCPU3Ed__17_System_Collections_IEnumerator_Reset_m5007E2C8482C4CEFE81CF74D936FF0A7C07DC93A (void);
// 0x000002F8 System.Object FMStereoPi/<NetworkServerStartTCP>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkServerStartTCPU3Ed__17_System_Collections_IEnumerator_get_Current_mBA9F0CCFC24599A08DA4B1E951E5DA06A9352D61 (void);
// 0x000002F9 System.Void FMStereoPi/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m134CEAE04BE2FEDE9EFA11D7295E24769F381845 (void);
// 0x000002FA System.Void FMStereoPi/<>c__DisplayClass18_0::<TCPReceiverCOR>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CTCPReceiverCORU3Eb__0_m7283781305E022DC9A5972AA90D14BD4B8836F26 (void);
// 0x000002FB System.Void FMStereoPi/<TCPReceiverCOR>d__18::.ctor(System.Int32)
extern void U3CTCPReceiverCORU3Ed__18__ctor_mF4813BCD2E45B641891847411481E574D7318350 (void);
// 0x000002FC System.Void FMStereoPi/<TCPReceiverCOR>d__18::System.IDisposable.Dispose()
extern void U3CTCPReceiverCORU3Ed__18_System_IDisposable_Dispose_m5F6BB7A97097970C9825699DCB95B6661949D6C4 (void);
// 0x000002FD System.Boolean FMStereoPi/<TCPReceiverCOR>d__18::MoveNext()
extern void U3CTCPReceiverCORU3Ed__18_MoveNext_mB7A962FC45293826B6A1E343B9CCEB3A22092CD6 (void);
// 0x000002FE System.Object FMStereoPi/<TCPReceiverCOR>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTCPReceiverCORU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m17AA4BB212021D7D2CAF7D6A959D39168C8CFB6D (void);
// 0x000002FF System.Void FMStereoPi/<TCPReceiverCOR>d__18::System.Collections.IEnumerator.Reset()
extern void U3CTCPReceiverCORU3Ed__18_System_Collections_IEnumerator_Reset_mC2BB26FFA15EEABCC8A8BBC632FEB34BED63744D (void);
// 0x00000300 System.Object FMStereoPi/<TCPReceiverCOR>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CTCPReceiverCORU3Ed__18_System_Collections_IEnumerator_get_Current_mF22F2BC3DCB0963C48EC14EC3B817D90FF0A5222 (void);
// 0x00000301 System.Void FMStereoPi/<NetworkClientStartUDP>d__19::.ctor(System.Int32)
extern void U3CNetworkClientStartUDPU3Ed__19__ctor_mD2D6DE21BB1A7D4A054760914643543A4C996788 (void);
// 0x00000302 System.Void FMStereoPi/<NetworkClientStartUDP>d__19::System.IDisposable.Dispose()
extern void U3CNetworkClientStartUDPU3Ed__19_System_IDisposable_Dispose_mA53E5147A12C4F90CB390C510B94C5C8CF2AD80F (void);
// 0x00000303 System.Boolean FMStereoPi/<NetworkClientStartUDP>d__19::MoveNext()
extern void U3CNetworkClientStartUDPU3Ed__19_MoveNext_m6879179DAAB8AB37CDA04D0AE4B416D7C9C38F89 (void);
// 0x00000304 System.Object FMStereoPi/<NetworkClientStartUDP>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkClientStartUDPU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C5CF71E1386F8AAEC4A69379236610C2DFA13D1 (void);
// 0x00000305 System.Void FMStereoPi/<NetworkClientStartUDP>d__19::System.Collections.IEnumerator.Reset()
extern void U3CNetworkClientStartUDPU3Ed__19_System_Collections_IEnumerator_Reset_mD4BB877B6B3A6BD3870668A3EB513F92BFE49C20 (void);
// 0x00000306 System.Object FMStereoPi/<NetworkClientStartUDP>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkClientStartUDPU3Ed__19_System_Collections_IEnumerator_get_Current_m03523BCBE848983E26C0F44806CEE08CE2EBCAB4 (void);
// 0x00000307 System.Void LargeFileDecoder::Start()
extern void LargeFileDecoder_Start_m8A75DDD0E127C5A2C1D41CF78D7E0581C23A6B8A (void);
// 0x00000308 System.Void LargeFileDecoder::Action_ProcessData(System.Byte[])
extern void LargeFileDecoder_Action_ProcessData_m2B9A1325FBD809329402EC76F777888B38C7301F (void);
// 0x00000309 System.Collections.IEnumerator LargeFileDecoder::ProcessData(System.Byte[])
extern void LargeFileDecoder_ProcessData_m2311EAA9443AA6B4E6F167E9A6FE1557E2B90F13 (void);
// 0x0000030A System.Void LargeFileDecoder::OnDisable()
extern void LargeFileDecoder_OnDisable_mB84E52E9D02E42F62C5A4965ADDDEE00470F74C2 (void);
// 0x0000030B System.Void LargeFileDecoder::.ctor()
extern void LargeFileDecoder__ctor_m459E40D94CDE806031571D17C42113AF188B95AD (void);
// 0x0000030C System.Void LargeFileDecoder/<ProcessData>d__9::.ctor(System.Int32)
extern void U3CProcessDataU3Ed__9__ctor_m074890C9D66E75DC716549F4CD67963C57A144DD (void);
// 0x0000030D System.Void LargeFileDecoder/<ProcessData>d__9::System.IDisposable.Dispose()
extern void U3CProcessDataU3Ed__9_System_IDisposable_Dispose_m41BB9C9CB32546C55B694EC712193613A175AFCA (void);
// 0x0000030E System.Boolean LargeFileDecoder/<ProcessData>d__9::MoveNext()
extern void U3CProcessDataU3Ed__9_MoveNext_m461DFDB2ACC7EA99F8724939CE646299E30FDEB3 (void);
// 0x0000030F System.Object LargeFileDecoder/<ProcessData>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessDataU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m80D685CFA55F83AE0FA445CC53F0DF227830CD62 (void);
// 0x00000310 System.Void LargeFileDecoder/<ProcessData>d__9::System.Collections.IEnumerator.Reset()
extern void U3CProcessDataU3Ed__9_System_Collections_IEnumerator_Reset_m1F1159FD210E745B8C334BAC10F3DDF768937431 (void);
// 0x00000311 System.Object LargeFileDecoder/<ProcessData>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CProcessDataU3Ed__9_System_Collections_IEnumerator_get_Current_m1CAA9605310113BD0F7D8D4CD0FA4B3512883CFD (void);
// 0x00000312 System.Void LargeFileEncoder::Start()
extern void LargeFileEncoder_Start_m7E65B00CBB0B821A62B40FD630976A66B40E37F0 (void);
// 0x00000313 System.Void LargeFileEncoder::Action_SendLargeByte(System.Byte[])
extern void LargeFileEncoder_Action_SendLargeByte_mEE15729811CA3E004C3499B71D4DF53BA8B3BC9C (void);
// 0x00000314 System.Collections.IEnumerator LargeFileEncoder::SenderCOR(System.Byte[])
extern void LargeFileEncoder_SenderCOR_m5891195A95AF73F12FC5F91F084C70A4E2FB12AC (void);
// 0x00000315 System.Void LargeFileEncoder::OnDisable()
extern void LargeFileEncoder_OnDisable_m0E1D113CCC30D1E325B61C39FEDE23FC91214960 (void);
// 0x00000316 System.Void LargeFileEncoder::OnApplicationQuit()
extern void LargeFileEncoder_OnApplicationQuit_mAB23876EEFC606B77271E0F1DC72A114AFD8F0A8 (void);
// 0x00000317 System.Void LargeFileEncoder::OnDestroy()
extern void LargeFileEncoder_OnDestroy_m21FE6842736295D83F8994DDBCCF7D599195036C (void);
// 0x00000318 System.Void LargeFileEncoder::StopAll()
extern void LargeFileEncoder_StopAll_mE4864F26B82CEDE3E91DD0000DD77F1469D2820B (void);
// 0x00000319 System.Void LargeFileEncoder::.ctor()
extern void LargeFileEncoder__ctor_m847C903529B79899D7B8E43C9F3F419671522678 (void);
// 0x0000031A System.Void LargeFileEncoder/<SenderCOR>d__10::.ctor(System.Int32)
extern void U3CSenderCORU3Ed__10__ctor_mF0E6C0CAB4B6DE80E30E476987AB86150658F8AF (void);
// 0x0000031B System.Void LargeFileEncoder/<SenderCOR>d__10::System.IDisposable.Dispose()
extern void U3CSenderCORU3Ed__10_System_IDisposable_Dispose_m7AB699C9394D6D03DAA48AFD83D6A96CE218A760 (void);
// 0x0000031C System.Boolean LargeFileEncoder/<SenderCOR>d__10::MoveNext()
extern void U3CSenderCORU3Ed__10_MoveNext_m6B896C113925C0DF2CA3885486AD351188835E7F (void);
// 0x0000031D System.Object LargeFileEncoder/<SenderCOR>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSenderCORU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE0B49284FA72DB77FE6D7F1172585E4AAA780929 (void);
// 0x0000031E System.Void LargeFileEncoder/<SenderCOR>d__10::System.Collections.IEnumerator.Reset()
extern void U3CSenderCORU3Ed__10_System_Collections_IEnumerator_Reset_mA3BA306D19B562FCD486EA9B27E2A058387C3E47 (void);
// 0x0000031F System.Object LargeFileEncoder/<SenderCOR>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CSenderCORU3Ed__10_System_Collections_IEnumerator_get_Current_m941E2DF11D6FB1E53992F88B8C61150B9CDA4AD7 (void);
// 0x00000320 System.Void FMWebSocketNetwork_debug::Action_SendStringAll(System.String)
extern void FMWebSocketNetwork_debug_Action_SendStringAll_m1DEAB244EB097156BA0ED95AFC813CC74B00A74F (void);
// 0x00000321 System.Void FMWebSocketNetwork_debug::Action_SendStringServer(System.String)
extern void FMWebSocketNetwork_debug_Action_SendStringServer_mEEB8867AE92E5ADD9A2556A41F77D617569444B0 (void);
// 0x00000322 System.Void FMWebSocketNetwork_debug::Action_SendStringOthers(System.String)
extern void FMWebSocketNetwork_debug_Action_SendStringOthers_m2D6A70B7E39CE45F289FC7DD91DE4864E7F732F9 (void);
// 0x00000323 System.Void FMWebSocketNetwork_debug::Action_SendByteAll()
extern void FMWebSocketNetwork_debug_Action_SendByteAll_m2598E2B9E9441787C94BC83D76420960EF216618 (void);
// 0x00000324 System.Void FMWebSocketNetwork_debug::Action_SendByteServer()
extern void FMWebSocketNetwork_debug_Action_SendByteServer_m961EE3773CEDDCBE60BD1F40D0F083C611DB57A3 (void);
// 0x00000325 System.Void FMWebSocketNetwork_debug::Action_SendByteOthers()
extern void FMWebSocketNetwork_debug_Action_SendByteOthers_m4A0D3032899D5304524EC3D295AD5A43BC8C83ED (void);
// 0x00000326 System.Void FMWebSocketNetwork_debug::Start()
extern void FMWebSocketNetwork_debug_Start_mC045C96F0436FD89E570A98F2314C364EF7BE75B (void);
// 0x00000327 System.Void FMWebSocketNetwork_debug::Update()
extern void FMWebSocketNetwork_debug_Update_m2C5B56FB1E949E99651D72EBE8DCEFC75FA9E8DB (void);
// 0x00000328 System.Void FMWebSocketNetwork_debug::Action_OnReceivedData(System.String)
extern void FMWebSocketNetwork_debug_Action_OnReceivedData_mF6FD58582305745F792EC448480F0196C09ED58C (void);
// 0x00000329 System.Void FMWebSocketNetwork_debug::Action_OnReceivedData(System.Byte[])
extern void FMWebSocketNetwork_debug_Action_OnReceivedData_m6DFB9721A0A7096CC1C43E2F2D80D2196F2A0B4D (void);
// 0x0000032A System.Void FMWebSocketNetwork_debug::.ctor()
extern void FMWebSocketNetwork_debug__ctor_m5D47B374D10296F8DC4572962F98ED3805DE146E (void);
// 0x0000032B System.Void FMSocketIOManager::Action_SetIP(System.String)
extern void FMSocketIOManager_Action_SetIP_m2DACCC3335E8356660E2237B840B9F7AA9BA91FD (void);
// 0x0000032C System.Void FMSocketIOManager::Action_SetPort(System.String)
extern void FMSocketIOManager_Action_SetPort_mF207E3AB17AB8787D725758C63436AE96646363E (void);
// 0x0000032D System.Void FMSocketIOManager::Action_SetSslEnabled(System.Boolean)
extern void FMSocketIOManager_Action_SetSslEnabled_m7359E9F5AEE4FDB5046332F9BA4A61A4494BE750 (void);
// 0x0000032E System.Void FMSocketIOManager::Action_SetPortRequired(System.Boolean)
extern void FMSocketIOManager_Action_SetPortRequired_mDD3EBF257B3D8514085A4006FA3BBB2A809C71A7 (void);
// 0x0000032F System.Void FMSocketIOManager::Action_SetSocketIORequired(System.Boolean)
extern void FMSocketIOManager_Action_SetSocketIORequired_m95F544F9629A528CC4E6D0D81E48ADF2165DE884 (void);
// 0x00000330 System.Void FMSocketIOManager::DebugLog(System.String)
extern void FMSocketIOManager_DebugLog_m74E32AD81199FF02ADE1EB49542C0EF9F482C1B7 (void);
// 0x00000331 System.Collections.IEnumerator FMSocketIOManager::WaitForSocketIOConnected()
extern void FMSocketIOManager_WaitForSocketIOConnected_m360334C5828064FD0709AFAB709D4BDAC631A368 (void);
// 0x00000332 System.Void FMSocketIOManager::OnReceivedData(FMSocketIO.SocketIOEvent)
extern void FMSocketIOManager_OnReceivedData_m564C1DC559441046CE292BBD4BA0153BBA051A16 (void);
// 0x00000333 System.Void FMSocketIOManager::OnReceivedData(FMSocketIOData)
extern void FMSocketIOManager_OnReceivedData_mEBCC6B0CAE3726D891E40355587465E4EB85C221 (void);
// 0x00000334 System.Void FMSocketIOManager::Action_OnReceivedData(System.String)
extern void FMSocketIOManager_Action_OnReceivedData_m3E9526566AEB6029BDB8355E3EF0B8964D5D7AD2 (void);
// 0x00000335 System.Void FMSocketIOManager::Action_OnReceivedData(System.Byte[])
extern void FMSocketIOManager_Action_OnReceivedData_m7E8F8E69D260AFCBE4A52AF29DA87FDCDFF65F0D (void);
// 0x00000336 System.Void FMSocketIOManager::Send(System.String,FMSocketIOEmitType)
extern void FMSocketIOManager_Send_m6205B10070EE4CD2BFD94D6632B3A40F7A8BA8AF (void);
// 0x00000337 System.Void FMSocketIOManager::Send(System.Byte[],FMSocketIOEmitType)
extern void FMSocketIOManager_Send_mED23DAD687C6CDF827EC72509E80AD1FB133A1D3 (void);
// 0x00000338 System.Void FMSocketIOManager::SendToAll(System.Byte[])
extern void FMSocketIOManager_SendToAll_mD2F07E2B650AC398967074E0A003AB1C85A40EA5 (void);
// 0x00000339 System.Void FMSocketIOManager::SendToServer(System.Byte[])
extern void FMSocketIOManager_SendToServer_m0AF01A6E5BE220B8A7C181D7FADD0E2A37CF5BA4 (void);
// 0x0000033A System.Void FMSocketIOManager::SendToOthers(System.Byte[])
extern void FMSocketIOManager_SendToOthers_mFE9DDC4124E403A03F56C252885FF53C68B625B0 (void);
// 0x0000033B System.Void FMSocketIOManager::SendToAll(System.String)
extern void FMSocketIOManager_SendToAll_mAAE7AEABC061961123564512FCC6194D24330862 (void);
// 0x0000033C System.Void FMSocketIOManager::SendToServer(System.String)
extern void FMSocketIOManager_SendToServer_m4E32E2CD464CC7A38A555B33DE629335F4CCB49C (void);
// 0x0000033D System.Void FMSocketIOManager::SendToOthers(System.String)
extern void FMSocketIOManager_SendToOthers_m60EFE030EC94F81844227F5048BA8858BD28DBED (void);
// 0x0000033E System.Void FMSocketIOManager::Awake()
extern void FMSocketIOManager_Awake_m846693922495F75D5DB5B543338B310787BE9150 (void);
// 0x0000033F System.Void FMSocketIOManager::Start()
extern void FMSocketIOManager_Start_m3D40AFB1E6D0412F215B85006D57A330E4E6C06C (void);
// 0x00000340 System.Void FMSocketIOManager::Update()
extern void FMSocketIOManager_Update_m207B5AD1BD2BF2AE0DAE93AA1019BB796F4EF12E (void);
// 0x00000341 System.Void FMSocketIOManager::InitAsServer()
extern void FMSocketIOManager_InitAsServer_mFDA2A1F200ED996ABEABA298D6054FB27F26A2FA (void);
// 0x00000342 System.Void FMSocketIOManager::InitAsClient()
extern void FMSocketIOManager_InitAsClient_m967E18EC3F19FFF2D144CC8B2C5BAEE3A88F1B6F (void);
// 0x00000343 System.Void FMSocketIOManager::Init()
extern void FMSocketIOManager_Init_mD898C19204626E0B606F98B597A09FFC55172E21 (void);
// 0x00000344 System.Boolean FMSocketIOManager::IsWebSocketConnected()
extern void FMSocketIOManager_IsWebSocketConnected_m731F48B7C8B7CB6B3CE0CF12285010CFD4387B74 (void);
// 0x00000345 System.Void FMSocketIOManager::Connect()
extern void FMSocketIOManager_Connect_m4FE665F0538F4B298D73F0C5B9866E8D65FE3A54 (void);
// 0x00000346 System.Void FMSocketIOManager::Close()
extern void FMSocketIOManager_Close_m05B4EACB3634D9DDC6CDCA06AE6BB58F32500B05 (void);
// 0x00000347 System.Void FMSocketIOManager::Emit(System.String)
extern void FMSocketIOManager_Emit_m3B409BCF830812E434D0399463E86CF45E8DA1B8 (void);
// 0x00000348 System.Void FMSocketIOManager::Emit(System.String,System.Action`1<System.String>)
extern void FMSocketIOManager_Emit_m5508330F8D3C823C18B864581A66B0C405AA0769 (void);
// 0x00000349 System.Void FMSocketIOManager::Emit(System.String,System.String)
extern void FMSocketIOManager_Emit_mA238F16091B6DF4D311BB008ECB2AF924744947B (void);
// 0x0000034A System.Void FMSocketIOManager::Emit(System.String,System.String,System.Action`1<System.String>)
extern void FMSocketIOManager_Emit_mB8A406CDEB1FA2470249AB8B5ABCC1543E2C460F (void);
// 0x0000034B System.Void FMSocketIOManager::On(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void FMSocketIOManager_On_m1CA9EFBEADF999D55BB444D54D283484675D3191 (void);
// 0x0000034C System.Void FMSocketIOManager::Off(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void FMSocketIOManager_Off_m9D273652A01A3C28EC5945CD4A8A61E295BAE973 (void);
// 0x0000034D System.Void FMSocketIOManager::OnEnable()
extern void FMSocketIOManager_OnEnable_m7A0FFD7BB5BDC714DEE967C03FEE79F1F646B12B (void);
// 0x0000034E System.Void FMSocketIOManager::OnDisable()
extern void FMSocketIOManager_OnDisable_m0B1FD36A0387CE0124317A519A49041A57B1D68A (void);
// 0x0000034F System.Void FMSocketIOManager::OnApplicationQuit()
extern void FMSocketIOManager_OnApplicationQuit_mE556B12B563B66CA2DEBAFD23B1928CF96735426 (void);
// 0x00000350 System.Void FMSocketIOManager::.ctor()
extern void FMSocketIOManager__ctor_m6AAE84CF5B41EA55E238E8F98AD4DA2B8C5B163F (void);
// 0x00000351 System.Void FMSocketIOManager::<Update>b__38_0(FMSocketIO.SocketIOEvent)
extern void FMSocketIOManager_U3CUpdateU3Eb__38_0_m6BB955FCFBDEBF552F3B375B5DCE96BFAAE4C163 (void);
// 0x00000352 System.Void FMSocketIOManager/SocketIOSettings::.ctor()
extern void SocketIOSettings__ctor_m130029042EAC430661F51E7B3EA51A0333FBFE7F (void);
// 0x00000353 System.Void FMSocketIOManager/<WaitForSocketIOConnected>d__23::.ctor(System.Int32)
extern void U3CWaitForSocketIOConnectedU3Ed__23__ctor_m502AB0FB1E28D8B7B6065D5DFC5D79E4A09BAF79 (void);
// 0x00000354 System.Void FMSocketIOManager/<WaitForSocketIOConnected>d__23::System.IDisposable.Dispose()
extern void U3CWaitForSocketIOConnectedU3Ed__23_System_IDisposable_Dispose_m90274DE1F545F83A61400FA206297200FFA0D3B5 (void);
// 0x00000355 System.Boolean FMSocketIOManager/<WaitForSocketIOConnected>d__23::MoveNext()
extern void U3CWaitForSocketIOConnectedU3Ed__23_MoveNext_m37A863F5EBC5D3B0D6C86A04A7CE3BB24D74D046 (void);
// 0x00000356 System.Object FMSocketIOManager/<WaitForSocketIOConnected>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9833FFFC5A7501B9152F7896E842E844F4A3C9BF (void);
// 0x00000357 System.Void FMSocketIOManager/<WaitForSocketIOConnected>d__23::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_IEnumerator_Reset_mB9ADDDD54A6DC5CC287AE4FA279F5E59703B52F1 (void);
// 0x00000358 System.Object FMSocketIOManager/<WaitForSocketIOConnected>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_IEnumerator_get_Current_mCBE89DB927B76215B3D44313952F2631B63666C0 (void);
// 0x00000359 System.Void EventJson::.ctor()
extern void EventJson__ctor_m60EB3DC222BE234D639F6FB67192E04A04193959 (void);
// 0x0000035A System.Void SocketIOComponentWebGL::Awake()
extern void SocketIOComponentWebGL_Awake_m50BE39A293703A9258D6B3AF626B6A8517C088AB (void);
// 0x0000035B System.Void SocketIOComponentWebGL::DebugLog(System.String)
extern void SocketIOComponentWebGL_DebugLog_mB13687BDF523632A2D11453F07430C9E7577FF4C (void);
// 0x0000035C System.Boolean SocketIOComponentWebGL::IsWebSocketConnected()
extern void SocketIOComponentWebGL_IsWebSocketConnected_mA226D79FB19CEFF8AE9C3844FF3CFB39ADEC6D80 (void);
// 0x0000035D System.Boolean SocketIOComponentWebGL::IsReady()
extern void SocketIOComponentWebGL_IsReady_mADCBA75B51F59C384FE75BE47120C21F11F1984F (void);
// 0x0000035E System.Void SocketIOComponentWebGL::Update()
extern void SocketIOComponentWebGL_Update_mA3D44CD4036F274062D738B726759AEE97E2B460 (void);
// 0x0000035F System.Void SocketIOComponentWebGL::Init()
extern void SocketIOComponentWebGL_Init_m067DD067D5744ED41CB780B3D8A8C24BE840BDD7 (void);
// 0x00000360 System.Void SocketIOComponentWebGL::OnConnected(FMSocketIO.SocketIOEvent)
extern void SocketIOComponentWebGL_OnConnected_m193A05D49B9C7E319E229ED907F6F99211CFE915 (void);
// 0x00000361 System.Void SocketIOComponentWebGL::AddSocketIO()
extern void SocketIOComponentWebGL_AddSocketIO_mF235A09507C32C094D7EF86F15F50135A1E66510 (void);
// 0x00000362 System.Void SocketIOComponentWebGL::AddEventListeners()
extern void SocketIOComponentWebGL_AddEventListeners_m5A923E46FBBD18541C674EE1ED49A4FFBCC061C0 (void);
// 0x00000363 System.Void SocketIOComponentWebGL::Connect()
extern void SocketIOComponentWebGL_Connect_m12823E19822749676E6D964BCE08D2AAA3B58D08 (void);
// 0x00000364 System.Void SocketIOComponentWebGL::Close()
extern void SocketIOComponentWebGL_Close_m96CC53D8EC7C842CF8E1F1ED30A9C92185B9C217 (void);
// 0x00000365 System.Void SocketIOComponentWebGL::Emit(System.String)
extern void SocketIOComponentWebGL_Emit_m49B2504BA984A6C8B9AF54E841624F73BC3676B5 (void);
// 0x00000366 System.Void SocketIOComponentWebGL::Emit(System.String,System.String)
extern void SocketIOComponentWebGL_Emit_m74CEE3BF253C5AC0856E2386F83B3EF30713FBA0 (void);
// 0x00000367 System.Void SocketIOComponentWebGL::Emit(System.String,System.Action`1<System.String>)
extern void SocketIOComponentWebGL_Emit_mE9AE1494A9C9F7F5F6C9B3A374B19868E985E38A (void);
// 0x00000368 System.Void SocketIOComponentWebGL::Emit(System.String,System.String,System.Action`1<System.String>)
extern void SocketIOComponentWebGL_Emit_m1E3275BC575D4D35CD3F52E9C41C2A472812D388 (void);
// 0x00000369 System.Void SocketIOComponentWebGL::On(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void SocketIOComponentWebGL_On_mA0FCE11E6AEA321DE5C94A805ADAF2CC7D0F28AE (void);
// 0x0000036A System.Void SocketIOComponentWebGL::Off(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void SocketIOComponentWebGL_Off_mF0D903B57858CA6D39C4751B19640075A5670F7C (void);
// 0x0000036B System.Void SocketIOComponentWebGL::InvokeAck(System.String)
extern void SocketIOComponentWebGL_InvokeAck_m0CAC2FBC4FBA22C903D2F27D09F944DC6150167D (void);
// 0x0000036C System.Void SocketIOComponentWebGL::OnOpen()
extern void SocketIOComponentWebGL_OnOpen_mD42EEEBDC92EE6713841EC1CA2FA0A969B2E5AE9 (void);
// 0x0000036D System.Void SocketIOComponentWebGL::SetSocketID(System.String)
extern void SocketIOComponentWebGL_SetSocketID_m484428E423FF885CFB5FB2D0239B332A494798A6 (void);
// 0x0000036E System.Void SocketIOComponentWebGL::InvokeEventCallback(System.String)
extern void SocketIOComponentWebGL_InvokeEventCallback_m3AC80C2526FCEFC82FC9766658DAE0E0CAC2F0EB (void);
// 0x0000036F System.Void SocketIOComponentWebGL::RegOnOpen()
extern void SocketIOComponentWebGL_RegOnOpen_m97190DA0DF7A6B942D5FE97219F53C0309F47835 (void);
// 0x00000370 System.Void SocketIOComponentWebGL::RegOnClose()
extern void SocketIOComponentWebGL_RegOnClose_m5AE9071D6FE06A7C40D551008DAECF7E7ECEFE8A (void);
// 0x00000371 System.Void SocketIOComponentWebGL::RegOnMessage(System.String)
extern void SocketIOComponentWebGL_RegOnMessage_m95B57268F4F7F573AB78F610C2E0A45B5CE60A4A (void);
// 0x00000372 System.Void SocketIOComponentWebGL::RegOnError(System.String)
extern void SocketIOComponentWebGL_RegOnError_mBCF0197900B581E456CB126C57D64045696B12FD (void);
// 0x00000373 System.Void SocketIOComponentWebGL::RegWebSocketConnected()
extern void SocketIOComponentWebGL_RegWebSocketConnected_mA47A1A8F82DD2292E5EA4030714EE19F8600DD7D (void);
// 0x00000374 System.Void SocketIOComponentWebGL::RegWebSocketDisconnected()
extern void SocketIOComponentWebGL_RegWebSocketDisconnected_m66BC9634595CBE9A2D5C3A38127F5781D30787CB (void);
// 0x00000375 System.Void SocketIOComponentWebGL::.ctor()
extern void SocketIOComponentWebGL__ctor_m20A0C18230A26AC642E91DA545D56740319CA19F (void);
// 0x00000376 System.Void MoveNegativeX::Update()
extern void MoveNegativeX_Update_m91CFD2F04AA9BE0AFD12189FE8E51ECAB596A78B (void);
// 0x00000377 System.Void MoveNegativeX::.ctor()
extern void MoveNegativeX__ctor_m622D7002829201D9A92CE7B03BB9394B52DEA36D (void);
// 0x00000378 System.Void FlameLightFlicker::Awake()
extern void FlameLightFlicker_Awake_m3353D02A27F32B952714E04BD2F773ED77DF4364 (void);
// 0x00000379 System.Void FlameLightFlicker::OnEnable()
extern void FlameLightFlicker_OnEnable_m6D3199C75399D0130BA06CB7FE50B4E204FDA931 (void);
// 0x0000037A System.Void FlameLightFlicker::SlowDisable()
extern void FlameLightFlicker_SlowDisable_mF0AE0A601FD5B924F9E2D7771A7BE2AD6493EB8A (void);
// 0x0000037B System.Void FlameLightFlicker::Update()
extern void FlameLightFlicker_Update_mECDE467860E5D1072EA55D1F73E640BC8653B237 (void);
// 0x0000037C System.Void FlameLightFlicker::.ctor()
extern void FlameLightFlicker__ctor_m35B7D3C879E6CD983DFA267005CABB7835CD9118 (void);
// 0x0000037D System.Void FlameEventInvoker::InvokeMaxBrightnessReachedIfNotAlreadyInvoked()
extern void FlameEventInvoker_InvokeMaxBrightnessReachedIfNotAlreadyInvoked_m18098365975E0D8166D366E6F4EB75F601646418 (void);
// 0x0000037E System.Void FlameEventInvoker::InvokeBurnOutStartedIfNotAlreadyInvoked()
extern void FlameEventInvoker_InvokeBurnOutStartedIfNotAlreadyInvoked_m3E98D587E0346B441712C101F601A88335E7F2DF (void);
// 0x0000037F System.Void FlameEventInvoker::.ctor()
extern void FlameEventInvoker__ctor_m41966F388B668DE030D235CFE9D773D72BACD575 (void);
// 0x00000380 System.Void ActorCOMTransform::Update()
extern void ActorCOMTransform_Update_m42CC68376DF28C96387BACFC64C48515AE9CE469 (void);
// 0x00000381 System.Void ActorCOMTransform::.ctor()
extern void ActorCOMTransform__ctor_mB7B969737BB7416B0E66C1CD4B8FDF1E8A7186CD (void);
// 0x00000382 System.Void ActorSpawner::Update()
extern void ActorSpawner_Update_mBB452D66B51B50FE636D74E7FF957A10A1FE2A07 (void);
// 0x00000383 System.Void ActorSpawner::.ctor()
extern void ActorSpawner__ctor_m48C1E536BA52A4F4EDE74F5B90749B4619F13C86 (void);
// 0x00000384 System.Void AddRandomVelocity::Update()
extern void AddRandomVelocity_Update_m6584A3F85D2E61F6D420B0B911B3C61637008449 (void);
// 0x00000385 System.Void AddRandomVelocity::.ctor()
extern void AddRandomVelocity__ctor_mC1CFE0D59EF94BA30BA6AAB81CA6FF0899F84DDF (void);
// 0x00000386 System.Void Blinker::Awake()
extern void Blinker_Awake_mC92D3CBC36B46F324C8197377BC40A434109C876 (void);
// 0x00000387 System.Void Blinker::Blink()
extern void Blinker_Blink_m4082F3A19006F464F4C14105BED460D5A1A0E273 (void);
// 0x00000388 System.Void Blinker::LateUpdate()
extern void Blinker_LateUpdate_mEE40A5EDA3EAFBB9CAD39BC413CB921E15CF6CCB (void);
// 0x00000389 System.Void Blinker::.ctor()
extern void Blinker__ctor_mAE1E307D31F5C3BB83001FBC5B2D61F827A69F3C (void);
// 0x0000038A System.Void ColliderHighlighter::Awake()
extern void ColliderHighlighter_Awake_m5920B8F626704DB40821E9BB9A85FDA1E0BFE4B3 (void);
// 0x0000038B System.Void ColliderHighlighter::OnEnable()
extern void ColliderHighlighter_OnEnable_m6F1425D169D88CC4D90E0A8F144A05C55F7E03B6 (void);
// 0x0000038C System.Void ColliderHighlighter::OnDisable()
extern void ColliderHighlighter_OnDisable_m2780DDE2CFE790FF4B04AD45654BDB74DD752893 (void);
// 0x0000038D System.Void ColliderHighlighter::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ColliderHighlighter_Solver_OnCollision_m8A90B739BAB8461A0179C2B4CD4EE87AB3DE8D30 (void);
// 0x0000038E System.Void ColliderHighlighter::.ctor()
extern void ColliderHighlighter__ctor_mBE8B58BC252B389A907738D87DAD06F094A62587 (void);
// 0x0000038F System.Void CollisionEventHandler::Awake()
extern void CollisionEventHandler_Awake_m744FDE1AB7D3F6777399B5108896E5E2CC61FE93 (void);
// 0x00000390 System.Void CollisionEventHandler::OnEnable()
extern void CollisionEventHandler_OnEnable_m2D3F33422D785CEDFCDCF1012A0B37B5F3ACB9F6 (void);
// 0x00000391 System.Void CollisionEventHandler::OnDisable()
extern void CollisionEventHandler_OnDisable_m75B03CB65B3D0226C04FF77A99D488596144BC32 (void);
// 0x00000392 System.Void CollisionEventHandler::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void CollisionEventHandler_Solver_OnCollision_m484E685CBD1E502504C567FD2D5CCB6F0860D616 (void);
// 0x00000393 System.Void CollisionEventHandler::OnDrawGizmos()
extern void CollisionEventHandler_OnDrawGizmos_m7939D47B098838344A41DD08F547354E268E07A8 (void);
// 0x00000394 System.Void CollisionEventHandler::.ctor()
extern void CollisionEventHandler__ctor_m92805BEBFF6E7114772EF8DFF4D5C244467A479B (void);
// 0x00000395 System.Void DebugParticleFrames::Awake()
extern void DebugParticleFrames_Awake_mA5C0197560CB8F73D7472974A693020EA29B28B3 (void);
// 0x00000396 System.Void DebugParticleFrames::OnDrawGizmos()
extern void DebugParticleFrames_OnDrawGizmos_m51378254E7930D9050D3355F810A3BC15A4CBBED (void);
// 0x00000397 System.Void DebugParticleFrames::.ctor()
extern void DebugParticleFrames__ctor_mBE2F687FB03E680B1957BB4D2C9F19E43F3CF260 (void);
// 0x00000398 System.Void ExtrapolationCamera::Start()
extern void ExtrapolationCamera_Start_mCB1FD551CBFB047EEA0FA534CE64CA969640D51E (void);
// 0x00000399 System.Void ExtrapolationCamera::FixedUpdate()
extern void ExtrapolationCamera_FixedUpdate_m622D9ECE2C7F91E1C5A70068B94B7C3E03AF4523 (void);
// 0x0000039A System.Void ExtrapolationCamera::LateUpdate()
extern void ExtrapolationCamera_LateUpdate_mB90259A761A12F0D8301E75BC381A6C0B32B443D (void);
// 0x0000039B System.Void ExtrapolationCamera::Teleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ExtrapolationCamera_Teleport_m8A6D37587FD1DF2518851886F5BE70B897DE0B67 (void);
// 0x0000039C System.Void ExtrapolationCamera::.ctor()
extern void ExtrapolationCamera__ctor_mA4E8D57C9E25BC7F9DA5676D79A7FC5505073032 (void);
// 0x0000039D System.Single FPSDisplay::get_CurrentFPS()
extern void FPSDisplay_get_CurrentFPS_mA9019EF8EAA0384D37F2DE714D84E5413E783C72 (void);
// 0x0000039E System.Single FPSDisplay::get_FPSMedian()
extern void FPSDisplay_get_FPSMedian_m6ACF1BBC790E915C8CE3D94C797CE4A4E8F116DD (void);
// 0x0000039F System.Single FPSDisplay::get_FPSAverage()
extern void FPSDisplay_get_FPSAverage_m52F2116EA4DD6DA75E483FE24D21321CF9F2D4C1 (void);
// 0x000003A0 System.Void FPSDisplay::Start()
extern void FPSDisplay_Start_mF1CE43CCE2AAB57ECD8D8B623B7E7358AB163C55 (void);
// 0x000003A1 System.Void FPSDisplay::Update()
extern void FPSDisplay_Update_m8740416265A05D0C56CA05EC0DE3A12E5132B028 (void);
// 0x000003A2 System.Void FPSDisplay::ResetMedianAndAverage()
extern void FPSDisplay_ResetMedianAndAverage_m82840B2C8D0189EA211AE4B14A8C94B22D932F70 (void);
// 0x000003A3 System.Void FPSDisplay::.ctor()
extern void FPSDisplay__ctor_mC4AD184EF09FAF5C43D89D0F7C211B2754D95B18 (void);
// 0x000003A4 System.Void ObiActorTeleport::Teleport()
extern void ObiActorTeleport_Teleport_m83F48614A9D7AE20261E92546E28B7C65C26DA1D (void);
// 0x000003A5 System.Void ObiActorTeleport::.ctor()
extern void ObiActorTeleport__ctor_mCC83730596EA6276332F31F582F5C8D1F08EE019 (void);
// 0x000003A6 System.Void ObiParticleCounter::Awake()
extern void ObiParticleCounter_Awake_m42D6C0254A2B27439769A96FE50705D33EC62B98 (void);
// 0x000003A7 System.Void ObiParticleCounter::OnEnable()
extern void ObiParticleCounter_OnEnable_mA9798F6382D0620125A95B047C974EBA660926CB (void);
// 0x000003A8 System.Void ObiParticleCounter::OnDisable()
extern void ObiParticleCounter_OnDisable_m3224433810D75B192BD03D399ADE656864BBD594 (void);
// 0x000003A9 System.Void ObiParticleCounter::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ObiParticleCounter_Solver_OnCollision_mC585B439D90C23BA5FE74E65D3195651DCAEF392 (void);
// 0x000003AA System.Void ObiParticleCounter::.ctor()
extern void ObiParticleCounter__ctor_m3107B0DABE8BEA1C8EB84C5A8EEEDB3C3E4B1B52 (void);
// 0x000003AB System.Void ObjectDragger::OnMouseDown()
extern void ObjectDragger_OnMouseDown_mC96AA07AABD47631E097AEA08E9A81AE1506EEEE (void);
// 0x000003AC System.Void ObjectDragger::OnMouseDrag()
extern void ObjectDragger_OnMouseDrag_m7E0B861E5546C709DED1ACF6F33ED31D6004FA6E (void);
// 0x000003AD System.Void ObjectDragger::.ctor()
extern void ObjectDragger__ctor_m97083AF9B77C7E2554A3D0701C6FAEE5D2282A6C (void);
// 0x000003AE System.Void ObjectLimit::Update()
extern void ObjectLimit_Update_m6689D4679CA59887B038EE4BD3F2294753C57E38 (void);
// 0x000003AF System.Void ObjectLimit::.ctor()
extern void ObjectLimit__ctor_m8F9F789DA4B949FECADA677D21A26E893A0A5F41 (void);
// 0x000003B0 System.Void SlowmoToggler::Slowmo(System.Boolean)
extern void SlowmoToggler_Slowmo_mF683360609D33F0B08F26A1019AAAD2BA21C8404 (void);
// 0x000003B1 System.Void SlowmoToggler::.ctor()
extern void SlowmoToggler__ctor_m17E4BB8E004DE90384ADF6074E74BC7C383E7001 (void);
// 0x000003B2 System.Void WorldSpaceGravity::Awake()
extern void WorldSpaceGravity_Awake_m20A0105B37EEF41CE69A528E59C13099276E4F2B (void);
// 0x000003B3 System.Void WorldSpaceGravity::Update()
extern void WorldSpaceGravity_Update_m9BFD6836B3B4FD9F671F9CC77622279143C1F72C (void);
// 0x000003B4 System.Void WorldSpaceGravity::.ctor()
extern void WorldSpaceGravity__ctor_m8A54BF21A4069DBE4B27EA67FDABDC1C26766539 (void);
// 0x000003B5 System.Void CharacterControl2D::Awake()
extern void CharacterControl2D_Awake_mBEDC7D4202A9C5DB77612B42672FEEA151A07DC5 (void);
// 0x000003B6 System.Void CharacterControl2D::FixedUpdate()
extern void CharacterControl2D_FixedUpdate_m45A35A8B2CD76109467D0CF2DC222087997E9977 (void);
// 0x000003B7 System.Void CharacterControl2D::.ctor()
extern void CharacterControl2D__ctor_m49BE177A57EA0352D49F2DD04C6C31CEEB8A7999 (void);
// 0x000003B8 System.Void CraneController::Start()
extern void CraneController_Start_mF01684DCC2B564B112A4368E334199C7C19AD74B (void);
// 0x000003B9 System.Void CraneController::Update()
extern void CraneController_Update_mD5DCB3A3C60F3F5CB5CCA7FC0D073F2882682A7D (void);
// 0x000003BA System.Void CraneController::.ctor()
extern void CraneController__ctor_mF9E29CD9F6BCDFE7AA44E7028D0B14CE7FDBC8FF (void);
// 0x000003BB System.Void CursorController::Start()
extern void CursorController_Start_m0B22294E05CD4E228C22A26E2FED8E074EA1A83F (void);
// 0x000003BC System.Void CursorController::Update()
extern void CursorController_Update_m415ECCFB2C1F667CCD6477CAF4D3A04E472A6C1C (void);
// 0x000003BD System.Void CursorController::.ctor()
extern void CursorController__ctor_m180053D2620A25532FBDEEAA4B88D3033623014C (void);
// 0x000003BE System.Void GrapplingHook::Awake()
extern void GrapplingHook_Awake_m4989432FB5EE2AEFF0DDD46692168EE9474D6E2F (void);
// 0x000003BF System.Void GrapplingHook::OnDestroy()
extern void GrapplingHook_OnDestroy_m966B313F51BFD2A96FABACA9536796964167391D (void);
// 0x000003C0 System.Void GrapplingHook::LaunchHook()
extern void GrapplingHook_LaunchHook_mBA12CEA6F36A0CC3C72257BADBBC5364804AA12C (void);
// 0x000003C1 System.Collections.IEnumerator GrapplingHook::AttachHook()
extern void GrapplingHook_AttachHook_m36E627486AE7D4866048929EA3937F0E72963C17 (void);
// 0x000003C2 System.Void GrapplingHook::DetachHook()
extern void GrapplingHook_DetachHook_m3ABB9323C8F0CE50EF0E3992970BA87794F1E7E8 (void);
// 0x000003C3 System.Void GrapplingHook::Update()
extern void GrapplingHook_Update_m5BF248318CAC3A90D6860DF6E25F996E3B67DF83 (void);
// 0x000003C4 System.Void GrapplingHook::.ctor()
extern void GrapplingHook__ctor_m9F75928EE3CDBC81C3D7E273DD9A02384D44DA43 (void);
// 0x000003C5 System.Void GrapplingHook/<AttachHook>d__13::.ctor(System.Int32)
extern void U3CAttachHookU3Ed__13__ctor_m9164753579B8C9146C64A515E50F24A7BE11B1B6 (void);
// 0x000003C6 System.Void GrapplingHook/<AttachHook>d__13::System.IDisposable.Dispose()
extern void U3CAttachHookU3Ed__13_System_IDisposable_Dispose_m4F8F47E389A0787F2AFC015113D1573AEA67A929 (void);
// 0x000003C7 System.Boolean GrapplingHook/<AttachHook>d__13::MoveNext()
extern void U3CAttachHookU3Ed__13_MoveNext_mBA824C87D8787B680AB4AB73B1315EABE2E5D866 (void);
// 0x000003C8 System.Object GrapplingHook/<AttachHook>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttachHookU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86BE01E073EC4AD283C3A0D22EEB1018E4C524A6 (void);
// 0x000003C9 System.Void GrapplingHook/<AttachHook>d__13::System.Collections.IEnumerator.Reset()
extern void U3CAttachHookU3Ed__13_System_Collections_IEnumerator_Reset_mCDCEBF557360577CE90A5311F7F8E4B554AF72A4 (void);
// 0x000003CA System.Object GrapplingHook/<AttachHook>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CAttachHookU3Ed__13_System_Collections_IEnumerator_get_Current_mF88E8E06C4CF813A1865B3E1D8B8E8BBAE36D5EB (void);
// 0x000003CB System.Void RobotArmController::Update()
extern void RobotArmController_Update_m840A4F091544CA7A4C5B70686142BAD5D8935285 (void);
// 0x000003CC System.Void RobotArmController::.ctor()
extern void RobotArmController__ctor_m6E744B8D725B51231442CDAC4E05AA6ED00EB32A (void);
// 0x000003CD System.Void RopeBetweenTwoPoints::Start()
extern void RopeBetweenTwoPoints_Start_m2A83ABEFB3C1A4DA1A4701FB8E413FFEA91239CD (void);
// 0x000003CE System.Void RopeBetweenTwoPoints::Generate()
extern void RopeBetweenTwoPoints_Generate_mD8EFE81845FB64A55D2F4A533EF45E3CB6F6D567 (void);
// 0x000003CF System.Void RopeBetweenTwoPoints::.ctor()
extern void RopeBetweenTwoPoints__ctor_m2C5314D4CBAF87AC5EC44276ABB46BA0693092DE (void);
// 0x000003D0 System.Void RopeNet::Awake()
extern void RopeNet_Awake_m91A7C469E5162B122FABB5098D57C835406A3FB1 (void);
// 0x000003D1 System.Void RopeNet::CreateNet(Obi.ObiSolver)
extern void RopeNet_CreateNet_m22482DF393195B29521F64D4A0A50ABDF1D31A8B (void);
// 0x000003D2 System.Void RopeNet::PinRope(Obi.ObiRope,Obi.ObiCollider,Obi.ObiCollider,UnityEngine.Vector3,UnityEngine.Vector3)
extern void RopeNet_PinRope_m486DBEAA8B687109D5F807D831CE538CAFC92579 (void);
// 0x000003D3 Obi.ObiRope RopeNet::CreateRope(UnityEngine.Vector3,UnityEngine.Vector3)
extern void RopeNet_CreateRope_mB8050E63103CB60CAEF1B476F8F6A9975383CA16 (void);
// 0x000003D4 System.Void RopeNet::.ctor()
extern void RopeNet__ctor_mA68E7CE8CF66BF32ACE6A9A50D495593FCA07EA9 (void);
// 0x000003D5 System.Void RopeSweepCut::Awake()
extern void RopeSweepCut_Awake_mB8FA7A121C0BAED955C801DA084FA4A48B226993 (void);
// 0x000003D6 System.Void RopeSweepCut::OnDestroy()
extern void RopeSweepCut_OnDestroy_mEC2478E3A69C3A562AF0191F9E5D55D3393CD2E9 (void);
// 0x000003D7 System.Void RopeSweepCut::AddMouseLine()
extern void RopeSweepCut_AddMouseLine_m59987D970ACC960BDD9A0819F2571C9A12FC9DAA (void);
// 0x000003D8 System.Void RopeSweepCut::DeleteMouseLine()
extern void RopeSweepCut_DeleteMouseLine_m434A0B4FC5BD368673C8A7842601E0D71E06BB24 (void);
// 0x000003D9 System.Void RopeSweepCut::LateUpdate()
extern void RopeSweepCut_LateUpdate_m17E67E5258C2589EB4853A75468372AD8DDB330C (void);
// 0x000003DA System.Void RopeSweepCut::ProcessInput()
extern void RopeSweepCut_ProcessInput_mFC4D9F1EEB6821E3714EC1831578044C914A783E (void);
// 0x000003DB System.Void RopeSweepCut::ScreenSpaceCut(UnityEngine.Vector2,UnityEngine.Vector2)
extern void RopeSweepCut_ScreenSpaceCut_mE46DA04B03ED57211F47D85664211A4F526D57EF (void);
// 0x000003DC System.Boolean RopeSweepCut::SegmentSegmentIntersection(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single&)
extern void RopeSweepCut_SegmentSegmentIntersection_m781A2777BCDBB2C0D734BF253EC279F7DF9CEA60 (void);
// 0x000003DD System.Void RopeSweepCut::.ctor()
extern void RopeSweepCut__ctor_m97CC3593ACF0C65A59D7111387B4F292F95D360B (void);
// 0x000003DE System.Void RopeTenser::Update()
extern void RopeTenser_Update_m5184BA8D81DA278AFCC867BD6DEB6C08EC998668 (void);
// 0x000003DF System.Void RopeTenser::.ctor()
extern void RopeTenser__ctor_mAA2EEFCA72E252AD76523321BE66F3FF29EC4CE7 (void);
// 0x000003E0 System.Void RopeTensionColorizer::Awake()
extern void RopeTensionColorizer_Awake_m6C6C0DFE55FCF5D59D8DBA7C630208CB8A8620E5 (void);
// 0x000003E1 System.Void RopeTensionColorizer::OnDestroy()
extern void RopeTensionColorizer_OnDestroy_mC5888FA0C292EFAA830AC640E3470086BA254709 (void);
// 0x000003E2 System.Void RopeTensionColorizer::Update()
extern void RopeTensionColorizer_Update_m690AD3894DF36C91918409F904A52B9AC743B9BA (void);
// 0x000003E3 System.Void RopeTensionColorizer::.ctor()
extern void RopeTensionColorizer__ctor_mF617811E5D9A9ACE5B957AF7FA3529C03FA54244 (void);
// 0x000003E4 System.Collections.IEnumerator RuntimeRopeGenerator::MakeRope(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern void RuntimeRopeGenerator_MakeRope_m75FA53894436086130B97A840F5FDD2FC27B16AE (void);
// 0x000003E5 System.Void RuntimeRopeGenerator::AddPendulum(Obi.ObiCollider,UnityEngine.Vector3)
extern void RuntimeRopeGenerator_AddPendulum_mC44C741BD87348055DED2613E4B3F2C1F036FF27 (void);
// 0x000003E6 System.Void RuntimeRopeGenerator::RemovePendulum()
extern void RuntimeRopeGenerator_RemovePendulum_m45255D311564674390956D3FE09428A818699C2A (void);
// 0x000003E7 System.Void RuntimeRopeGenerator::ChangeRopeLength(System.Single)
extern void RuntimeRopeGenerator_ChangeRopeLength_m5DD09878372434ADBF820AF657C4B20245638828 (void);
// 0x000003E8 System.Void RuntimeRopeGenerator::UpdateTethers()
extern void RuntimeRopeGenerator_UpdateTethers_mCD9D9C785979709408E1D779541D301E6BB8F37A (void);
// 0x000003E9 System.Void RuntimeRopeGenerator::.ctor()
extern void RuntimeRopeGenerator__ctor_mC6334710591DE477B4E1977384C18E199AC229BB (void);
// 0x000003EA System.Void RuntimeRopeGenerator/<MakeRope>d__2::.ctor(System.Int32)
extern void U3CMakeRopeU3Ed__2__ctor_mC9B96B6A48DE9C6A24651C61A20885D4B7BFAC2D (void);
// 0x000003EB System.Void RuntimeRopeGenerator/<MakeRope>d__2::System.IDisposable.Dispose()
extern void U3CMakeRopeU3Ed__2_System_IDisposable_Dispose_mAF88235C710602C1022A42A1C91CEAC0CFCB8204 (void);
// 0x000003EC System.Boolean RuntimeRopeGenerator/<MakeRope>d__2::MoveNext()
extern void U3CMakeRopeU3Ed__2_MoveNext_mD3BFD03EF7B85140E88BF2A029A1FBEEB5B83504 (void);
// 0x000003ED System.Object RuntimeRopeGenerator/<MakeRope>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMakeRopeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1E799A4BCA7AA45E2E66D688FC6DBE189FDFE596 (void);
// 0x000003EE System.Void RuntimeRopeGenerator/<MakeRope>d__2::System.Collections.IEnumerator.Reset()
extern void U3CMakeRopeU3Ed__2_System_Collections_IEnumerator_Reset_m21ABBBA4A71228DA2105ABF24C336ED84D28D1C2 (void);
// 0x000003EF System.Object RuntimeRopeGenerator/<MakeRope>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CMakeRopeU3Ed__2_System_Collections_IEnumerator_get_Current_m46764A418CA35B282FE4A27DE6EE40D497E8AE97 (void);
// 0x000003F0 System.Collections.IEnumerator RuntimeRopeGeneratorUse::Start()
extern void RuntimeRopeGeneratorUse_Start_m016745BFD195A82E4B89668A5271E64703A3000C (void);
// 0x000003F1 System.Void RuntimeRopeGeneratorUse::Update()
extern void RuntimeRopeGeneratorUse_Update_mF5A466B58BA8D471831BA6EA5627D5B1E31422FA (void);
// 0x000003F2 System.Void RuntimeRopeGeneratorUse::.ctor()
extern void RuntimeRopeGeneratorUse__ctor_mC97354E58383A34851903C4A577F2F792C853C21 (void);
// 0x000003F3 System.Void RuntimeRopeGeneratorUse/<Start>d__2::.ctor(System.Int32)
extern void U3CStartU3Ed__2__ctor_mB2576F12E41960219B8D4B925C0E50DF2A66FC9E (void);
// 0x000003F4 System.Void RuntimeRopeGeneratorUse/<Start>d__2::System.IDisposable.Dispose()
extern void U3CStartU3Ed__2_System_IDisposable_Dispose_m443D3F88C18C90B75A79EA1C9056696B94DCC154 (void);
// 0x000003F5 System.Boolean RuntimeRopeGeneratorUse/<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_m7175EACB83490273BDD9BD4FD91E1A4CBE7C38F7 (void);
// 0x000003F6 System.Object RuntimeRopeGeneratorUse/<Start>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E92812F9987D045E11CB2653D3BE2668B002811 (void);
// 0x000003F7 System.Void RuntimeRopeGeneratorUse/<Start>d__2::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_mF62F9BBE198D60E90417C026611B81DD3461E8DC (void);
// 0x000003F8 System.Object RuntimeRopeGeneratorUse/<Start>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m555815E4C6D334F953A3D60A4858AF925B400F3D (void);
// 0x000003F9 System.Void SnakeController::Start()
extern void SnakeController_Start_m46F63DB6E2B01EC690B7867EBB2818BF26DC279B (void);
// 0x000003FA System.Void SnakeController::OnDestroy()
extern void SnakeController_OnDestroy_m31645CE0FDC49E9F37A7551F944809D10D7F73C4 (void);
// 0x000003FB System.Void SnakeController::ResetSurfaceInfo(Obi.ObiSolver,System.Single)
extern void SnakeController_ResetSurfaceInfo_mC9A57457F8CC93AD82F820B172FDB936C891FE4C (void);
// 0x000003FC System.Void SnakeController::AnalyzeContacts(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void SnakeController_AnalyzeContacts_m36CCC13638269AA4530D576BA471E01C10DCD2C1 (void);
// 0x000003FD System.Void SnakeController::Update()
extern void SnakeController_Update_m1D28B620F00A46D61598472DA3B03BEFEC2E5681 (void);
// 0x000003FE System.Void SnakeController::.ctor()
extern void SnakeController__ctor_m5DD187E2ABC5A7F126EBE19C2A8D461DB837B9AE (void);
// 0x000003FF System.Void SpiralCurve::Awake()
extern void SpiralCurve_Awake_mA3D312955BD674577F25C169DDF76F8F8A9E01E3 (void);
// 0x00000400 System.Void SpiralCurve::Generate()
extern void SpiralCurve_Generate_mEE3D2D5D691C41EF442DECC262CDC897EBF3ABE4 (void);
// 0x00000401 System.Void SpiralCurve::.ctor()
extern void SpiralCurve__ctor_m6E99F9930728ADE344E5EFB74E9162C21B018EB3 (void);
// 0x00000402 System.Void WrapRopeGameController::Awake()
extern void WrapRopeGameController_Awake_m045ED1CAC6F8ACD3FB6BFCB027C9E009C4E7B06A (void);
// 0x00000403 System.Void WrapRopeGameController::OnEnable()
extern void WrapRopeGameController_OnEnable_m2E487E79BAC01C141875950CB5DA281840D041FD (void);
// 0x00000404 System.Void WrapRopeGameController::OnDisable()
extern void WrapRopeGameController_OnDisable_m29CEF7B251D8261332ED77E112AF3D95E2C830DD (void);
// 0x00000405 System.Void WrapRopeGameController::Update()
extern void WrapRopeGameController_Update_m19FCF5F3E86097865448E1E2E5B9203BFAEF8E44 (void);
// 0x00000406 System.Void WrapRopeGameController::Solver_OnCollision(Obi.ObiSolver,Obi.ObiSolver/ObiCollisionEventArgs)
extern void WrapRopeGameController_Solver_OnCollision_m1423CF5FAD56ACDCEB4B6FA20E715B493E78BCFC (void);
// 0x00000407 System.Void WrapRopeGameController::.ctor()
extern void WrapRopeGameController__ctor_mF05C3F3006F45A280C0CDA8258C9219CFC2B7B18 (void);
// 0x00000408 System.Void WrapRopePlayerController::Awake()
extern void WrapRopePlayerController_Awake_m8BA41B1F43833149BA401D08A91F497A3C7F44B8 (void);
// 0x00000409 System.Void WrapRopePlayerController::Update()
extern void WrapRopePlayerController_Update_mBFD4983CF399B3D30F4BDA939FD77F68C7BCC73C (void);
// 0x0000040A System.Void WrapRopePlayerController::.ctor()
extern void WrapRopePlayerController__ctor_m18B2CE68E53693F8BAF7B092199B6F21BE38CC3E (void);
// 0x0000040B System.Void Wrappable::Awake()
extern void Wrappable_Awake_mBAD2A0564D165C4A1247B404B7B26309AE392C07 (void);
// 0x0000040C System.Void Wrappable::OnDestroy()
extern void Wrappable_OnDestroy_m099CFA91817E95C96D6B758AE181F5EFA469FA51 (void);
// 0x0000040D System.Void Wrappable::Reset()
extern void Wrappable_Reset_mB1C37B1D99E5E783AA6D118957C4BC92668C8B9E (void);
// 0x0000040E System.Void Wrappable::SetWrapped()
extern void Wrappable_SetWrapped_m396EA26793C4C341226F87069F7634B2CEBCC6A3 (void);
// 0x0000040F System.Boolean Wrappable::IsWrapped()
extern void Wrappable_IsWrapped_mD1C6830A339AB6839C919D5CE52894B61C55FC89 (void);
// 0x00000410 System.Void Wrappable::.ctor()
extern void Wrappable__ctor_mFD07DD359F22A7AF5B3D6C174775313840A37D49 (void);
// 0x00000411 OvrAvatarDriver/ControllerPose OvrAvatarTestDriver::GetMalibuControllerPose(OVRInput/Controller)
extern void OvrAvatarTestDriver_GetMalibuControllerPose_mBBCCC1B71F759AD7ED527CD6CB1CD33B7CD3CA33 (void);
// 0x00000412 OvrAvatarDriver/ControllerPose OvrAvatarTestDriver::GetControllerPose(OVRInput/Controller)
extern void OvrAvatarTestDriver_GetControllerPose_mEFC942D9BC35972518EACB2D6842F0D75FBADD0D (void);
// 0x00000413 System.Void OvrAvatarTestDriver::CalculateCurrentPose()
extern void OvrAvatarTestDriver_CalculateCurrentPose_mE9C968915332F48537EDDA7485C04B74D6FA44C7 (void);
// 0x00000414 System.Void OvrAvatarTestDriver::UpdateTransforms(System.IntPtr)
extern void OvrAvatarTestDriver_UpdateTransforms_mDEB955A238CE55F370AB6B4A638F75B56A41AD4C (void);
// 0x00000415 System.Void OvrAvatarTestDriver::.ctor()
extern void OvrAvatarTestDriver__ctor_mEAEC601ACCF2713D049DE7D53EEA8BE13028E021 (void);
// 0x00000416 System.Void PoseEditHelper::OnDrawGizmos()
extern void PoseEditHelper_OnDrawGizmos_mD91616EF51322BFE811A39179D58F27E403FEB2F (void);
// 0x00000417 System.Void PoseEditHelper::DrawJoints(UnityEngine.Transform)
extern void PoseEditHelper_DrawJoints_mE9240514D1F4D82C2CBEFC30675E6F9AB1F90A5E (void);
// 0x00000418 System.Void PoseEditHelper::.ctor()
extern void PoseEditHelper__ctor_m14B364DCCE64D239A94E4EFFB94FA4021B649AD4 (void);
// 0x00000419 System.Boolean GazeTargetSpawner::get_IsVisible()
extern void GazeTargetSpawner_get_IsVisible_m027FD3301AF746556139C75719A516C56B604F3A (void);
// 0x0000041A System.Void GazeTargetSpawner::set_IsVisible(System.Boolean)
extern void GazeTargetSpawner_set_IsVisible_m656741C7D34BCC648A2FFFDD279DF9E749BC65EC (void);
// 0x0000041B System.Void GazeTargetSpawner::Start()
extern void GazeTargetSpawner_Start_m54A65954DEDFB05BE7B5ED8797EB9E9BBB69E0FE (void);
// 0x0000041C System.Void GazeTargetSpawner::OnValidate()
extern void GazeTargetSpawner_OnValidate_m0AD5B928BAC51C8F8C897A28A6F5B5C0C813F759 (void);
// 0x0000041D System.Void GazeTargetSpawner::.ctor()
extern void GazeTargetSpawner__ctor_m0CCC682B97C7E54454D450E31E3AD10FFFB01609 (void);
// 0x0000041E System.Void RemoteLoopbackManager::Start()
extern void RemoteLoopbackManager_Start_m3D88A609BEE0B233D3DB9FF31CECB9A02EA0622C (void);
// 0x0000041F System.Void RemoteLoopbackManager::OnLocalAvatarPacketRecorded(System.Object,OvrAvatar/PacketEventArgs)
extern void RemoteLoopbackManager_OnLocalAvatarPacketRecorded_mA6B085BFEB8A43C62BEA927F6897DA7D2703C509 (void);
// 0x00000420 System.Void RemoteLoopbackManager::Update()
extern void RemoteLoopbackManager_Update_m8580875FA8F63D34323D1DFE1FAABFE202ACCD83 (void);
// 0x00000421 System.Void RemoteLoopbackManager::SendPacketData(System.Byte[])
extern void RemoteLoopbackManager_SendPacketData_mE1366A23BF9706263E81A72270EBFD0552CC6F2C (void);
// 0x00000422 System.Void RemoteLoopbackManager::ReceivePacketData(System.Byte[])
extern void RemoteLoopbackManager_ReceivePacketData_m6C30CA3730DC2B8C3A369EAEB893CBC6A23E51E1 (void);
// 0x00000423 System.Void RemoteLoopbackManager::.ctor()
extern void RemoteLoopbackManager__ctor_m3411382CDA89A7DFD4D0BF9F90B554A6908A52DB (void);
// 0x00000424 System.Void RemoteLoopbackManager/PacketLatencyPair::.ctor()
extern void PacketLatencyPair__ctor_m8689B42992068A72EC9825E4D0D5BEA59C6CA17B (void);
// 0x00000425 System.Single RemoteLoopbackManager/SimulatedLatencySettings::NextValue()
extern void SimulatedLatencySettings_NextValue_mE0F727E70EE772D2380E3B700425942729C05EC4 (void);
// 0x00000426 System.Void RemoteLoopbackManager/SimulatedLatencySettings::.ctor()
extern void SimulatedLatencySettings__ctor_mDC4701382DCAB4B34E2AE9BD7ED27DCEC0306B6C (void);
// 0x00000427 System.Void P2PManager::.ctor()
extern void P2PManager__ctor_mB6608327AD13643BD03C5E68280FCD0B6125325A (void);
// 0x00000428 System.Void P2PManager::ConnectTo(System.UInt64)
extern void P2PManager_ConnectTo_m17196C73BF4289B60E1A87A89E346C9829531277 (void);
// 0x00000429 System.Void P2PManager::Disconnect(System.UInt64)
extern void P2PManager_Disconnect_m1D1F9D79468C213DE441570ED4063FBB3382B395 (void);
// 0x0000042A System.Void P2PManager::PeerConnectRequestCallback(Oculus.Platform.Message`1<Oculus.Platform.Models.NetworkingPeer>)
extern void P2PManager_PeerConnectRequestCallback_mA2B238158FFFD34238FF04504BE0DDAA79F46CF3 (void);
// 0x0000042B System.Void P2PManager::ConnectionStateChangedCallback(Oculus.Platform.Message`1<Oculus.Platform.Models.NetworkingPeer>)
extern void P2PManager_ConnectionStateChangedCallback_m4830C291FF361D6925FA5CE9990C75E28EBC1C1D (void);
// 0x0000042C System.Void P2PManager::SendAvatarUpdate(System.UInt64,UnityEngine.Transform,System.UInt32,System.Byte[])
extern void P2PManager_SendAvatarUpdate_m30D3B0C339473423E8E32210B7DA3A0EB837BF17 (void);
// 0x0000042D System.Void P2PManager::GetRemotePackets()
extern void P2PManager_GetRemotePackets_m927B54C77C08A0B750468DC6DF462AACE6080862 (void);
// 0x0000042E System.Void P2PManager::processAvatarPacket(RemotePlayer,System.Byte[]&,System.Int32&)
extern void P2PManager_processAvatarPacket_mB70AAB480DC71E2334F9024B392D086135503DCB (void);
// 0x0000042F System.Void P2PManager::PackByte(System.Byte,System.Byte[],System.Int32&)
extern void P2PManager_PackByte_mE3BAC7A5FF8DFB90FD34D7D094FEB52186D752CB (void);
// 0x00000430 System.Byte P2PManager::ReadByte(System.Byte[],System.Int32&)
extern void P2PManager_ReadByte_m62246D13ACE98725DDF1392AC7ABD465CF7ADC39 (void);
// 0x00000431 System.Void P2PManager::PackFloat(System.Single,System.Byte[],System.Int32&)
extern void P2PManager_PackFloat_m813A4ADC04B138338DC3D7C8B32F3DA23074C103 (void);
// 0x00000432 System.Single P2PManager::ReadFloat(System.Byte[],System.Int32&)
extern void P2PManager_ReadFloat_m533B3071FB90DB8C6AD2C4425CB586A0C0FD4507 (void);
// 0x00000433 System.Void P2PManager::PackULong(System.UInt64,System.Byte[],System.Int32&)
extern void P2PManager_PackULong_mE1BEA769C74E4C31B85D620D213B42890E56B982 (void);
// 0x00000434 System.UInt64 P2PManager::ReadULong(System.Byte[],System.Int32&)
extern void P2PManager_ReadULong_m2B0A88069265C6B0D7AD44AE3AD6E97AD4E0976A (void);
// 0x00000435 System.Void P2PManager::PackUInt32(System.UInt32,System.Byte[],System.Int32&)
extern void P2PManager_PackUInt32_mBD348EA800F6189A135EA39BB1B46115B1F5668A (void);
// 0x00000436 System.UInt32 P2PManager::ReadUInt32(System.Byte[],System.Int32&)
extern void P2PManager_ReadUInt32_m9B4BA0EFDA8E5242C3E342C87E9E39227957B579 (void);
// 0x00000437 System.Void PlayerController::Awake()
extern void PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23 (void);
// 0x00000438 System.Void PlayerController::Start()
extern void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (void);
// 0x00000439 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x0000043A System.Void PlayerController::checkInput()
extern void PlayerController_checkInput_m3D97AFD4C743C977628FB1AD033A11D510245E5D (void);
// 0x0000043B System.Void PlayerController::ToggleCamera()
extern void PlayerController_ToggleCamera_m5B3789403E84830A876162599A26AC0D60FE63BF (void);
// 0x0000043C System.Void PlayerController::ToggleUI()
extern void PlayerController_ToggleUI_mBF6001D97AEB40ED13E5D3B7D949A32A1A8EE767 (void);
// 0x0000043D System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x0000043E System.Void RemotePlayer::.ctor()
extern void RemotePlayer__ctor_m1D2EA078FE1DC4E73403B21D7276F151B485779B (void);
// 0x0000043F System.Void RoomManager::.ctor()
extern void RoomManager__ctor_mB80958CB1C8945EFECA6721A48CCD7FD4FD642C0 (void);
// 0x00000440 System.Void RoomManager::AcceptingInviteCallback(Oculus.Platform.Message`1<System.String>)
extern void RoomManager_AcceptingInviteCallback_mF113E845BBE2E6274413197E3F690A4CB6D31E54 (void);
// 0x00000441 System.Boolean RoomManager::CheckForInvite()
extern void RoomManager_CheckForInvite_mA303ECFF0732D3E8A431B195A44489B63A69FE61 (void);
// 0x00000442 System.Void RoomManager::CreateRoom()
extern void RoomManager_CreateRoom_m269D2F174277B1C17D40042717CECB404F62A00A (void);
// 0x00000443 System.Void RoomManager::CreateAndJoinPrivateRoomCallback(Oculus.Platform.Message`1<Oculus.Platform.Models.Room>)
extern void RoomManager_CreateAndJoinPrivateRoomCallback_m9D46166FC8AF5D761D1AA469ED7ABCC68206F75A (void);
// 0x00000444 System.Void RoomManager::OnLaunchInviteWorkflowComplete(Oculus.Platform.Message)
extern void RoomManager_OnLaunchInviteWorkflowComplete_mD38D5E0FE0AE71861760C9E1D1CF0145CB9062AD (void);
// 0x00000445 System.Void RoomManager::JoinExistingRoom(System.UInt64)
extern void RoomManager_JoinExistingRoom_m9EBD9604CE6455B6883010469352032A5194D33A (void);
// 0x00000446 System.Void RoomManager::JoinRoomCallback(Oculus.Platform.Message`1<Oculus.Platform.Models.Room>)
extern void RoomManager_JoinRoomCallback_m1595E1E4A1682F648B5EBB3F38F36CEFB92DFDD4 (void);
// 0x00000447 System.Void RoomManager::RoomUpdateCallback(Oculus.Platform.Message`1<Oculus.Platform.Models.Room>)
extern void RoomManager_RoomUpdateCallback_m5E41C1379A957AFEDB593D5E3CE5332795FA6E13 (void);
// 0x00000448 System.Void RoomManager::LeaveCurrentRoom()
extern void RoomManager_LeaveCurrentRoom_mECB32B537948F588D0033D33487CC18D82901396 (void);
// 0x00000449 System.Void RoomManager::ProcessRoomData(Oculus.Platform.Message`1<Oculus.Platform.Models.Room>)
extern void RoomManager_ProcessRoomData_m8B72D5D186602982ECCA901CD501C5090FEC554A (void);
// 0x0000044A System.Void SocialPlatformManager::Update()
extern void SocialPlatformManager_Update_mBA54B1BBAB8E2AFCD46CB881DF8F3CDC237502F4 (void);
// 0x0000044B System.Void SocialPlatformManager::Awake()
extern void SocialPlatformManager_Awake_mFA4B8BE84D03C963593DB6464796DC2575FEEF63 (void);
// 0x0000044C System.Void SocialPlatformManager::InitCallback(Oculus.Platform.Message`1<Oculus.Platform.Models.PlatformInitialize>)
extern void SocialPlatformManager_InitCallback_mEC7EF74DB7487D0ACA881942CD05575580983CCF (void);
// 0x0000044D System.Void SocialPlatformManager::Start()
extern void SocialPlatformManager_Start_m2590A29622024579EF9E7981569F3668B05C4B42 (void);
// 0x0000044E System.Void SocialPlatformManager::IsEntitledCallback(Oculus.Platform.Message)
extern void SocialPlatformManager_IsEntitledCallback_m4A11C466B9DCFEA08AB0F6D1C6719410242D4CA2 (void);
// 0x0000044F System.Void SocialPlatformManager::GetLoggedInUserCallback(Oculus.Platform.Message`1<Oculus.Platform.Models.User>)
extern void SocialPlatformManager_GetLoggedInUserCallback_m0073552C6096F9CFBB9AD5F02428D9EE468AF816 (void);
// 0x00000450 System.Void SocialPlatformManager::GetLoggedInUserFriendsAndRoomsCallback(Oculus.Platform.Message`1<Oculus.Platform.Models.UserAndRoomList>)
extern void SocialPlatformManager_GetLoggedInUserFriendsAndRoomsCallback_m0715D8600656E2FFCB5E9AA93CC948171EEFE364 (void);
// 0x00000451 System.Void SocialPlatformManager::OnLocalAvatarPacketRecorded(System.Object,OvrAvatar/PacketEventArgs)
extern void SocialPlatformManager_OnLocalAvatarPacketRecorded_m7C64D22280077B4CAC09D8506E18D56665C197C2 (void);
// 0x00000452 System.Void SocialPlatformManager::OnApplicationQuit()
extern void SocialPlatformManager_OnApplicationQuit_mEE92A60A70DEAB4FE2D6E4FF93F36BE01DA46E5F (void);
// 0x00000453 System.Void SocialPlatformManager::AddUser(System.UInt64,RemotePlayer&)
extern void SocialPlatformManager_AddUser_m66C4DF835C4E3CAB01E2C7F5E3DF15BE470DE684 (void);
// 0x00000454 System.Void SocialPlatformManager::LogOutputLine(System.String)
extern void SocialPlatformManager_LogOutputLine_m352A91FC8CCDA409B3667731204AA3FD819AFFAA (void);
// 0x00000455 System.Void SocialPlatformManager::TerminateWithError(Oculus.Platform.Message)
extern void SocialPlatformManager_TerminateWithError_m681E54AD4A07EA68D7C6C64E85C9E71B38CA81EB (void);
// 0x00000456 SocialPlatformManager/State SocialPlatformManager::get_CurrentState()
extern void SocialPlatformManager_get_CurrentState_m3179BA8104D33F2C148B8335C9DDAB76F0B88E0E (void);
// 0x00000457 System.UInt64 SocialPlatformManager::get_MyID()
extern void SocialPlatformManager_get_MyID_m7E25BD62EF6B364A1BD93382A06A0B8BA12B9C74 (void);
// 0x00000458 System.String SocialPlatformManager::get_MyOculusID()
extern void SocialPlatformManager_get_MyOculusID_mCC5312E6B8395BE762012533A6AEC13966E6A4EA (void);
// 0x00000459 System.Void SocialPlatformManager::TransitionToState(SocialPlatformManager/State)
extern void SocialPlatformManager_TransitionToState_m175EA407E7A7EA8960B43092D59BFF3F490B144D (void);
// 0x0000045A System.Void SocialPlatformManager::SetSphereColorForState()
extern void SocialPlatformManager_SetSphereColorForState_mD20681844CD61712C4AD977BEC7C05187CE31EE7 (void);
// 0x0000045B System.Void SocialPlatformManager::SetFloorColorForState(System.Boolean)
extern void SocialPlatformManager_SetFloorColorForState_mFA400D5420C4E9EFEC67F866046BFF60086D41BE (void);
// 0x0000045C System.Void SocialPlatformManager::MarkAllRemoteUsersAsNotInRoom()
extern void SocialPlatformManager_MarkAllRemoteUsersAsNotInRoom_m0DC4041159E579F85969424B72400DF5D1CB3822 (void);
// 0x0000045D System.Void SocialPlatformManager::MarkRemoteUserInRoom(System.UInt64)
extern void SocialPlatformManager_MarkRemoteUserInRoom_mE819D28052F78D02AE063DEB97595511D17FD0C2 (void);
// 0x0000045E System.Void SocialPlatformManager::ForgetRemoteUsersNotInRoom()
extern void SocialPlatformManager_ForgetRemoteUsersNotInRoom_mEA043618BBD474DBF2A7477A6CA1B8BFFB682EAE (void);
// 0x0000045F System.Void SocialPlatformManager::LogOutput(System.String)
extern void SocialPlatformManager_LogOutput_m13794D4E7BF84D05CD292E7C7F25ED03A7C0AB37 (void);
// 0x00000460 System.Boolean SocialPlatformManager::IsUserInRoom(System.UInt64)
extern void SocialPlatformManager_IsUserInRoom_m01AFEE74247D50385D62456E3DA432689550BA60 (void);
// 0x00000461 System.Void SocialPlatformManager::AddRemoteUser(System.UInt64)
extern void SocialPlatformManager_AddRemoteUser_m0E67E5165F5A716D8D19B6AE53EB50C22F4915A4 (void);
// 0x00000462 System.Void SocialPlatformManager::RemoveRemoteUser(System.UInt64)
extern void SocialPlatformManager_RemoveRemoteUser_m63251A56F7060B8814B76FBA9351A2B099FBCD5B (void);
// 0x00000463 System.Void SocialPlatformManager::UpdateVoiceData(System.Int16[],System.Int32)
extern void SocialPlatformManager_UpdateVoiceData_mCF8F832AF0077482F63563E55AA367BA9AC5E7EC (void);
// 0x00000464 System.Void SocialPlatformManager::MicFilter(System.Int16[],System.UIntPtr,System.Int32,System.Int32)
extern void SocialPlatformManager_MicFilter_m0FB5A641DE3104796155D242D62D10DFFB182F54 (void);
// 0x00000465 RemotePlayer SocialPlatformManager::GetRemoteUser(System.UInt64)
extern void SocialPlatformManager_GetRemoteUser_m8770DF5599B8C4BC117AF4C35BB2B43E8C806288 (void);
// 0x00000466 System.Void SocialPlatformManager::.ctor()
extern void SocialPlatformManager__ctor_mEB334D6BB761E5915EFC79D1CFA2DE61E1915700 (void);
// 0x00000467 System.Void SocialPlatformManager::.cctor()
extern void SocialPlatformManager__cctor_mE631E0315F36815C9F3F6BF35F3E232824B92D92 (void);
// 0x00000468 System.Void VoipManager::.ctor()
extern void VoipManager__ctor_mECBEE9E5787626F81F3487717805DA107A1AEB2D (void);
// 0x00000469 System.Void VoipManager::ConnectTo(System.UInt64)
extern void VoipManager_ConnectTo_m8544850E93404C45736EA96AF0C1707F077283C8 (void);
// 0x0000046A System.Void VoipManager::Disconnect(System.UInt64)
extern void VoipManager_Disconnect_m0652B9A829FAC11D7617211FC41B03217BC830C1 (void);
// 0x0000046B System.Void VoipManager::VoipConnectRequestCallback(Oculus.Platform.Message`1<Oculus.Platform.Models.NetworkingPeer>)
extern void VoipManager_VoipConnectRequestCallback_m0B922EF366F6CD9902ED9298C96CD640B551B655 (void);
// 0x0000046C System.Void VoipManager::VoipStateChangedCallback(Oculus.Platform.Message`1<Oculus.Platform.Models.NetworkingPeer>)
extern void VoipManager_VoipStateChangedCallback_m82F3EC6D4F09BD39F4E5CEFF77A930FD83F503E5 (void);
// 0x0000046D System.Void GazeTarget::.cctor()
extern void GazeTarget__cctor_m63D70FB4A884154F1A873541498B2F5FD847CEAC (void);
// 0x0000046E System.Void GazeTarget::Start()
extern void GazeTarget_Start_m573A7A8C6E6AE8114DD2942011CF6CFB672B3A7A (void);
// 0x0000046F System.Void GazeTarget::Update()
extern void GazeTarget_Update_mEBBFC7C145132060351AC1F16A7AB0828ECF5F4D (void);
// 0x00000470 System.Void GazeTarget::OnDestroy()
extern void GazeTarget_OnDestroy_mDE8B5B28EEB3452A56270E395875A49398F1BA84 (void);
// 0x00000471 System.Void GazeTarget::UpdateGazeTarget()
extern void GazeTarget_UpdateGazeTarget_mE12B5551370A1534E16B667052CE53B64ECAABD2 (void);
// 0x00000472 ovrAvatarGazeTarget GazeTarget::CreateOvrGazeTarget(System.UInt32,UnityEngine.Vector3,ovrAvatarGazeTargetType)
extern void GazeTarget_CreateOvrGazeTarget_mBF5ADB60F51A0D7B854D124D6842655935E83EC1 (void);
// 0x00000473 System.Void GazeTarget::.ctor()
extern void GazeTarget__ctor_mC673116584E41E083DDA84E891F70F0F53613519 (void);
// 0x00000474 System.Void AvatarLayer::.ctor()
extern void AvatarLayer__ctor_m76756963656B658651AD3BAD4D99F5F14A297529 (void);
// 0x00000475 System.Void PacketRecordSettings::.ctor()
extern void PacketRecordSettings__ctor_m052D8B988CA60332A17788508A2F73B347514D4C (void);
// 0x00000476 System.Void OvrAvatar::.cctor()
extern void OvrAvatar__cctor_m02C03ED710DFE5A5F6B2895C21E3C1A95E070E15 (void);
// 0x00000477 System.Void OvrAvatar::OnDestroy()
extern void OvrAvatar_OnDestroy_mC412A1B3D84723E215D7062BF9A176AACD226A72 (void);
// 0x00000478 System.Void OvrAvatar::AssetLoadedCallback(OvrAvatarAsset)
extern void OvrAvatar_AssetLoadedCallback_m70921F135999AAC208A86DC9AC5AA850A63515F7 (void);
// 0x00000479 System.Void OvrAvatar::CombinedMeshLoadedCallback(System.IntPtr)
extern void OvrAvatar_CombinedMeshLoadedCallback_m421FEAE81DA1784E5EAA835E9BF2C03A017CEB49 (void);
// 0x0000047A OvrAvatarSkinnedMeshRenderComponent OvrAvatar::AddSkinnedMeshRenderComponent(UnityEngine.GameObject,ovrAvatarRenderPart_SkinnedMeshRender)
extern void OvrAvatar_AddSkinnedMeshRenderComponent_mB46FB768614CD12912521699A1FBFB3459F12119 (void);
// 0x0000047B OvrAvatarSkinnedMeshRenderPBSComponent OvrAvatar::AddSkinnedMeshRenderPBSComponent(UnityEngine.GameObject,ovrAvatarRenderPart_SkinnedMeshRenderPBS)
extern void OvrAvatar_AddSkinnedMeshRenderPBSComponent_mAD30ABDE44F4344B5AF02513AE2F5698381861FF (void);
// 0x0000047C OvrAvatarSkinnedMeshPBSV2RenderComponent OvrAvatar::AddSkinnedMeshRenderPBSV2Component(System.IntPtr,UnityEngine.GameObject,ovrAvatarRenderPart_SkinnedMeshRenderPBS_V2,System.Boolean,System.Boolean)
extern void OvrAvatar_AddSkinnedMeshRenderPBSV2Component_m8531195959A66CD1C74B47EE78FC734D048497E3 (void);
// 0x0000047D System.IntPtr OvrAvatar::GetRenderPart(ovrAvatarComponent,System.UInt32)
extern void OvrAvatar_GetRenderPart_m0D4B0495AE9C8D2334044275E7D0E9EC2D425A0C (void);
// 0x0000047E System.String OvrAvatar::GetRenderPartName(ovrAvatarComponent,System.UInt32)
extern void OvrAvatar_GetRenderPartName_m04E97ECBD6A71336A0CB32FCB9078CC7DDD9CFF8 (void);
// 0x0000047F System.Void OvrAvatar::ConvertTransform(System.Single[],ovrAvatarTransform&)
extern void OvrAvatar_ConvertTransform_mC81F3B94DCE65CB4EE009963840332F32E7983A1 (void);
// 0x00000480 System.Void OvrAvatar::ConvertTransform(ovrAvatarTransform,UnityEngine.Transform)
extern void OvrAvatar_ConvertTransform_m21D8832CB2832797E104B022016A1EA739E1E147 (void);
// 0x00000481 ovrAvatarTransform OvrAvatar::CreateOvrAvatarTransform(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void OvrAvatar_CreateOvrAvatarTransform_m0741758157287F1C90255D82D7A9936EEB3A5FE7 (void);
// 0x00000482 ovrAvatarGazeTarget OvrAvatar::CreateOvrGazeTarget(System.UInt32,UnityEngine.Vector3,ovrAvatarGazeTargetType)
extern void OvrAvatar_CreateOvrGazeTarget_m967676E78CD412D13B2335E987285D81190198BB (void);
// 0x00000483 System.Void OvrAvatar::BuildRenderComponents()
extern void OvrAvatar_BuildRenderComponents_m2D4D78E1719099DE258FCED2862D75ACE00989A2 (void);
// 0x00000484 System.Void OvrAvatar::AddAvatarComponent(T&,ovrAvatarComponent)
// 0x00000485 System.Void OvrAvatar::UpdateCustomPoses()
extern void OvrAvatar_UpdateCustomPoses_m96AC60583AA8BEB4F68C0FF1D520EC8F29A2DA32 (void);
// 0x00000486 System.Boolean OvrAvatar::UpdatePoseRoot(UnityEngine.Transform,UnityEngine.Transform&,UnityEngine.Transform[]&,ovrAvatarTransform[]&)
extern void OvrAvatar_UpdatePoseRoot_m2864E2FD383663A9A77551499E69F8F2DA374BEE (void);
// 0x00000487 System.Boolean OvrAvatar::UpdateTransforms(UnityEngine.Transform[],ovrAvatarTransform[])
extern void OvrAvatar_UpdateTransforms_mBE373C2F2D62BAE010C42580CC4A9E47BE63F1EE (void);
// 0x00000488 System.Void OvrAvatar::OrderJoints(UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void OvrAvatar_OrderJoints_m207BB5F508A31EA26C0FFDBA9D25CA35983B45F0 (void);
// 0x00000489 System.Void OvrAvatar::AvatarSpecificationCallback(System.IntPtr)
extern void OvrAvatar_AvatarSpecificationCallback_mEECAAAA1502EAF866D2D5ABB985B4A6025566FC4 (void);
// 0x0000048A System.Void OvrAvatar::Start()
extern void OvrAvatar_Start_m4AB09F66F023610098F17600CC522620E3A1CE37 (void);
// 0x0000048B System.Void OvrAvatar::Update()
extern void OvrAvatar_Update_mF15EAF54811291C91AD417940E5B0133B54B48BD (void);
// 0x0000048C ovrAvatarHandInputState OvrAvatar::CreateInputState(ovrAvatarTransform,OvrAvatarDriver/ControllerPose)
extern void OvrAvatar_CreateInputState_m4A275E979A58BD232419463DB6F63EAC719F6F52 (void);
// 0x0000048D System.Void OvrAvatar::ShowControllers(System.Boolean)
extern void OvrAvatar_ShowControllers_m4D759FC08A9EA4CBC77C60CA09AE2E7F199311C1 (void);
// 0x0000048E System.Void OvrAvatar::ShowLeftController(System.Boolean)
extern void OvrAvatar_ShowLeftController_mBA2711BBD63E19516B156586B4FDAD3ECEDFC5D0 (void);
// 0x0000048F System.Void OvrAvatar::ShowRightController(System.Boolean)
extern void OvrAvatar_ShowRightController_mA362260B0837A8408F13AADDD0547F6E1C90EF25 (void);
// 0x00000490 System.Void OvrAvatar::UpdateVoiceVisualization(System.Single[])
extern void OvrAvatar_UpdateVoiceVisualization_m9FF25EA1FC1D9F596055875EDB6E5D92680F1712 (void);
// 0x00000491 System.Void OvrAvatar::RecordFrame()
extern void OvrAvatar_RecordFrame_m0E1F196470490BD6D1B9A136CE22919DC0B3ED6C (void);
// 0x00000492 System.Void OvrAvatar::RecordUnityFrame()
extern void OvrAvatar_RecordUnityFrame_mE29CEBA1AA8F7B081B1AEE21E2C4AA605895956C (void);
// 0x00000493 System.Void OvrAvatar::RecordSDKFrame()
extern void OvrAvatar_RecordSDKFrame_m741C1C1B7CCF95205A635E733E5F721BFCC72F45 (void);
// 0x00000494 System.Void OvrAvatar::AddRenderParts(OvrAvatarComponent,ovrAvatarComponent,UnityEngine.Transform)
extern void OvrAvatar_AddRenderParts_mD338D0A74FF393DF9C583D1C29D1ECC63877029C (void);
// 0x00000495 System.Void OvrAvatar::RefreshBodyParts()
extern void OvrAvatar_RefreshBodyParts_m5FF1B92685C1690638FD726C4F7A434F4DFF57ED (void);
// 0x00000496 System.Nullable`1<ovrAvatarBodyComponent> OvrAvatar::GetBodyComponent()
extern void OvrAvatar_GetBodyComponent_mB0E0D5918861985AD7CC4468D68505E193312915 (void);
// 0x00000497 UnityEngine.Transform OvrAvatar::GetHandTransform(OvrAvatar/HandType,OvrAvatar/HandJoint)
extern void OvrAvatar_GetHandTransform_m1165DA8669BF988BA908E700918D08F721B9A44B (void);
// 0x00000498 System.Void OvrAvatar::GetPointingDirection(OvrAvatar/HandType,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void OvrAvatar_GetPointingDirection_m729CF3681B6FBF203B99ECF4082F0B8BC75B2D2B (void);
// 0x00000499 System.Void OvrAvatar::UpdateVoiceBehavior()
extern void OvrAvatar_UpdateVoiceBehavior_m955A8CBBBD42069D9342B9935916B0455E11E154 (void);
// 0x0000049A System.Boolean OvrAvatar::IsValidMic()
extern void OvrAvatar_IsValidMic_m28534EEB808B1F34EA16C1F9AC74846400CA2B63 (void);
// 0x0000049B System.Void OvrAvatar::InitPostLoad()
extern void OvrAvatar_InitPostLoad_m052DD75178E0312A881F5FCB0160347146D60BF7 (void);
// 0x0000049C System.Void OvrAvatar::ExpressiveGlobalInit()
extern void OvrAvatar_ExpressiveGlobalInit_mE82993B820E52426286E4B3989536243BC7B1332 (void);
// 0x0000049D System.Void OvrAvatar::InitializeLights()
extern void OvrAvatar_InitializeLights_m6C51C212514BD1F867E57EE9D6A7640F304FC9EE (void);
// 0x0000049E ovrAvatarLight OvrAvatar::CreateLightDirectional(System.UInt32,UnityEngine.Vector3,System.Single,ovrAvatarLight&)
extern void OvrAvatar_CreateLightDirectional_m048B90BE869A4CF71DE15CA21881FD2657CB22F5 (void);
// 0x0000049F ovrAvatarLight OvrAvatar::CreateLightPoint(System.UInt32,UnityEngine.Vector3,System.Single,System.Single,ovrAvatarLight&)
extern void OvrAvatar_CreateLightPoint_m3DFFB4AC272AFE99ADFDE0589545A7ED9C649943 (void);
// 0x000004A0 ovrAvatarLight OvrAvatar::CreateLightSpot(System.UInt32,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,ovrAvatarLight&)
extern void OvrAvatar_CreateLightSpot_m1ECA6DE74031B1843269B7C5CB5D788668F10338 (void);
// 0x000004A1 System.Void OvrAvatar::UpdateExpressive()
extern void OvrAvatar_UpdateExpressive_mFAC1796755CE934DAF447CC55DDBEA027DFFF28C (void);
// 0x000004A2 System.Void OvrAvatar::ConfigureHelpers()
extern void OvrAvatar_ConfigureHelpers_mB9B34DCC14A1997470F18AC0061F2BC5EDB30B86 (void);
// 0x000004A3 System.Collections.IEnumerator OvrAvatar::WaitForMouthAudioSource()
extern void OvrAvatar_WaitForMouthAudioSource_m4B9D1847BB4B32B21C5D28794A837FC3326219C2 (void);
// 0x000004A4 System.Void OvrAvatar::DestroyHelperObjects()
extern void OvrAvatar_DestroyHelperObjects_m5685285FD7FBD4DA075C66DE1B64C418D07FC129 (void);
// 0x000004A5 UnityEngine.GameObject OvrAvatar::CreateHelperObject(UnityEngine.Transform,UnityEngine.Vector3,System.String,System.String)
extern void OvrAvatar_CreateHelperObject_m414E59562DDAFB8654E4E2C356F690CE057EDE1C (void);
// 0x000004A6 System.Void OvrAvatar::UpdateVoiceData(System.Int16[],System.Int32)
extern void OvrAvatar_UpdateVoiceData_m2C3C9A8DECB05BFA3253789D3BB1563C080218C6 (void);
// 0x000004A7 System.Void OvrAvatar::UpdateVoiceData(System.Single[],System.Int32)
extern void OvrAvatar_UpdateVoiceData_m21182F84E276DD53D4D4D4B560F759A02BC9987B (void);
// 0x000004A8 System.Void OvrAvatar::UpdateFacewave()
extern void OvrAvatar_UpdateFacewave_mD60684F907CC55D1D0ADFB1285E551B7708312B6 (void);
// 0x000004A9 System.Void OvrAvatar::.ctor()
extern void OvrAvatar__ctor_m3BD0FB8A94A434644B64D4F0AEC77300BE61E1C1 (void);
// 0x000004AA System.Void OvrAvatar/PacketEventArgs::.ctor(OvrAvatarPacket)
extern void PacketEventArgs__ctor_m0D15EF1306EC9B22B3DE0C03199ACC05DB3FBFC2 (void);
// 0x000004AB System.Void OvrAvatar/<WaitForMouthAudioSource>d__137::.ctor(System.Int32)
extern void U3CWaitForMouthAudioSourceU3Ed__137__ctor_m348BF982E093DEB28505198EE74C66C5C4EC91CD (void);
// 0x000004AC System.Void OvrAvatar/<WaitForMouthAudioSource>d__137::System.IDisposable.Dispose()
extern void U3CWaitForMouthAudioSourceU3Ed__137_System_IDisposable_Dispose_mEF1934995C5F6E9A73296732199E48298B0DD301 (void);
// 0x000004AD System.Boolean OvrAvatar/<WaitForMouthAudioSource>d__137::MoveNext()
extern void U3CWaitForMouthAudioSourceU3Ed__137_MoveNext_m304A31AF4083356D80E80C3B29886919DD7F8A6A (void);
// 0x000004AE System.Object OvrAvatar/<WaitForMouthAudioSource>d__137::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForMouthAudioSourceU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7158266C8CA3498E870069B16B95C9207B686BA7 (void);
// 0x000004AF System.Void OvrAvatar/<WaitForMouthAudioSource>d__137::System.Collections.IEnumerator.Reset()
extern void U3CWaitForMouthAudioSourceU3Ed__137_System_Collections_IEnumerator_Reset_mEDBCFD5369F19AC4FB15794210B618DB424EBDA0 (void);
// 0x000004B0 System.Object OvrAvatar/<WaitForMouthAudioSource>d__137::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForMouthAudioSourceU3Ed__137_System_Collections_IEnumerator_get_Current_m2A982A178BFE62260BED95D4341B8A4BFF23E337 (void);
// 0x000004B1 System.Void OvrAvatarAsset::.ctor()
extern void OvrAvatarAsset__ctor_m39F0A8F0CF014E13531C5F97B8A70731CC1138C3 (void);
// 0x000004B2 System.Void OvrAvatarAssetMesh::.ctor(System.UInt64,System.IntPtr,ovrAvatarAssetType)
extern void OvrAvatarAssetMesh__ctor_m9CCEE6602802003978B059098F85B792B2FA462F (void);
// 0x000004B3 System.Void OvrAvatarAssetMesh::LoadSubmeshes(System.IntPtr,System.IntPtr,System.UInt64)
extern void OvrAvatarAssetMesh_LoadSubmeshes_mC605617D9D31B1AB34245A5822BFF2DC49855FA7 (void);
// 0x000004B4 System.Void OvrAvatarAssetMesh::LoadBlendShapes(System.IntPtr,System.Int64)
extern void OvrAvatarAssetMesh_LoadBlendShapes_m0499ED13EBE059D00048A4D0AAF2BBDC4D4CDB77 (void);
// 0x000004B5 System.Void OvrAvatarAssetMesh::SetSkinnedBindPose(System.IntPtr,ovrAvatarAssetType)
extern void OvrAvatarAssetMesh_SetSkinnedBindPose_m833655AE4A7D9E909D6C0ED3DA78F9876185A40B (void);
// 0x000004B6 System.Void OvrAvatarAssetMesh::GetVertexAndIndexData(System.IntPtr,ovrAvatarAssetType,System.Int64&,System.IntPtr&,System.UInt32&,System.IntPtr&)
extern void OvrAvatarAssetMesh_GetVertexAndIndexData_mEE29DB37A7DDA8086D2922A32B744E7514101661 (void);
// 0x000004B7 UnityEngine.SkinnedMeshRenderer OvrAvatarAssetMesh::CreateSkinnedMeshRendererOnObject(UnityEngine.GameObject)
extern void OvrAvatarAssetMesh_CreateSkinnedMeshRendererOnObject_m71695AB5E91B1E2A78FD416FED69C6F26CE1EC51 (void);
// 0x000004B8 System.Void OvrAvatarAssetTexture::.ctor(System.UInt64,System.IntPtr)
extern void OvrAvatarAssetTexture__ctor_m376B684F3BBCE8A5510C5573AFAF79396F5F4DEF (void);
// 0x000004B9 System.Void OvrAvatarBase::Update()
extern void OvrAvatarBase_Update_m76690377EC334BEE5C0A8B22BB6E39F98226B2D7 (void);
// 0x000004BA System.Void OvrAvatarBase::.ctor()
extern void OvrAvatarBase__ctor_mE5886D4BC00231C332068218A68AAEE3AF258250 (void);
// 0x000004BB System.Nullable`1<ovrAvatarComponent> OvrAvatarBody::GetNativeAvatarComponent()
extern void OvrAvatarBody_GetNativeAvatarComponent_m350C8348D3AA56732F047EFA408B563B3256DBDA (void);
// 0x000004BC System.Void OvrAvatarBody::Update()
extern void OvrAvatarBody_Update_mFA3B50D702E9DC5BBDE34D4C4011465E98D6333E (void);
// 0x000004BD System.Void OvrAvatarBody::.ctor()
extern void OvrAvatarBody__ctor_m8F3B64CFEBDC1A537FA24AAAA154B96E81636D24 (void);
// 0x000004BE System.Void OvrAvatarComponent::SetOvrAvatarOwner(OvrAvatar)
extern void OvrAvatarComponent_SetOvrAvatarOwner_m49570F220F056450ECD604B205893AEC914F1243 (void);
// 0x000004BF System.Void OvrAvatarComponent::UpdateAvatar(System.IntPtr)
extern void OvrAvatarComponent_UpdateAvatar_m379B3EB66C76F958C6BEA47072B3F522609837CF (void);
// 0x000004C0 System.Void OvrAvatarComponent::UpdateActive(OvrAvatar,ovrAvatarVisibilityFlags)
extern void OvrAvatarComponent_UpdateActive_m5FB9CEE5C5A7C00B03E4694A93CBAA4D4CB7A7F1 (void);
// 0x000004C1 System.Void OvrAvatarComponent::UpdateAvatarMaterial(UnityEngine.Material,ovrAvatarMaterialState)
extern void OvrAvatarComponent_UpdateAvatarMaterial_m7AE0E007ADF57058775F3451C4DBDC9D7C914F72 (void);
// 0x000004C2 UnityEngine.Texture2D OvrAvatarComponent::GetLoadedTexture(System.UInt64)
extern void OvrAvatarComponent_GetLoadedTexture_mBF2D9B2CFF2F9C22B661DC446C30EE924080DA7B (void);
// 0x000004C3 System.Void OvrAvatarComponent::.ctor()
extern void OvrAvatarComponent__ctor_m9F063BF4835CF289D94707841DD135026645900C (void);
// 0x000004C4 System.Void OvrAvatarComponent::.cctor()
extern void OvrAvatarComponent__cctor_mEF653C66360FB1ED2D912492152A07FD533363BC (void);
// 0x000004C5 OvrAvatarDriver/PoseFrame OvrAvatarDriver::GetCurrentPose()
extern void OvrAvatarDriver_GetCurrentPose_mAAED7AE7B7BC0895E565AF3DEC2BB53842A0ECCA (void);
// 0x000004C6 System.Void OvrAvatarDriver::UpdateTransforms(System.IntPtr)
// 0x000004C7 System.Void OvrAvatarDriver::Start()
extern void OvrAvatarDriver_Start_mEE3CDA3C83A3C9EF9B9E9AC8E0527D43747A59F2 (void);
// 0x000004C8 System.Void OvrAvatarDriver::UpdateTransformsFromPose(System.IntPtr)
extern void OvrAvatarDriver_UpdateTransformsFromPose_mFA51834F9EEC8A48DBC35051F7324011EBC6CD3E (void);
// 0x000004C9 System.Boolean OvrAvatarDriver::GetIsTrackedRemote()
extern void OvrAvatarDriver_GetIsTrackedRemote_m656EE8484BA274FC3ADB3C592EF6D40D0B621454 (void);
// 0x000004CA System.Void OvrAvatarDriver::.ctor()
extern void OvrAvatarDriver__ctor_m8A450A986441B188BBCAEFEDF8333ACCAD64C7EA (void);
// 0x000004CB OvrAvatarDriver/ControllerPose OvrAvatarDriver/ControllerPose::Interpolate(OvrAvatarDriver/ControllerPose,OvrAvatarDriver/ControllerPose,System.Single)
extern void ControllerPose_Interpolate_mFFCD926E9C586FA2A7BAE504FCEB42A231D31300 (void);
// 0x000004CC OvrAvatarDriver/PoseFrame OvrAvatarDriver/PoseFrame::Interpolate(OvrAvatarDriver/PoseFrame,OvrAvatarDriver/PoseFrame,System.Single)
extern void PoseFrame_Interpolate_mF56FB29ACBA999727C9C7C7CC80534F60FB927ED (void);
// 0x000004CD System.Void OvrAvatarHand::Update()
extern void OvrAvatarHand_Update_m1F53059E67DE74F863992E1F4CA5B4A40D298082 (void);
// 0x000004CE System.Void OvrAvatarHand::.ctor()
extern void OvrAvatarHand__ctor_mDABA9708428E61124B640215227B83C857879F7B (void);
// 0x000004CF OvrAvatarDriver/ControllerPose OvrAvatarLocalDriver::GetMalibuControllerPose(OVRInput/Controller)
extern void OvrAvatarLocalDriver_GetMalibuControllerPose_mC41F7699C30BA32C9D609266E79CA0E0188E027E (void);
// 0x000004D0 OvrAvatarDriver/ControllerPose OvrAvatarLocalDriver::GetControllerPose(OVRInput/Controller)
extern void OvrAvatarLocalDriver_GetControllerPose_m120C1D0A44C4EF7C865EB093876BD718687EC523 (void);
// 0x000004D1 System.Void OvrAvatarLocalDriver::CalculateCurrentPose()
extern void OvrAvatarLocalDriver_CalculateCurrentPose_m52CB4B974BAB9EB4B728620B7501B1823E121D51 (void);
// 0x000004D2 System.Void OvrAvatarLocalDriver::UpdateTransforms(System.IntPtr)
extern void OvrAvatarLocalDriver_UpdateTransforms_mCCF860D225CA81F365C4FFCD5C8F2BCA9975462D (void);
// 0x000004D3 System.Void OvrAvatarLocalDriver::.ctor()
extern void OvrAvatarLocalDriver__ctor_m89C5BF97359084F41D9B973DA9D01815BFD57EBD (void);
// 0x000004D4 System.Void OvrAvatarMaterialManager::CreateTextureArrays()
extern void OvrAvatarMaterialManager_CreateTextureArrays_m4977F1D7A1834D0B557B8646C582324A566B4C2B (void);
// 0x000004D5 System.Void OvrAvatarMaterialManager::SetRenderer(UnityEngine.Renderer)
extern void OvrAvatarMaterialManager_SetRenderer_mA11FDEE6BC73C5B68170F30B6CE713397B9E812A (void);
// 0x000004D6 System.Void OvrAvatarMaterialManager::OnCombinedMeshReady()
extern void OvrAvatarMaterialManager_OnCombinedMeshReady_m496ED8C56791DF79B950B75BF346FFA777D50F57 (void);
// 0x000004D7 System.Void OvrAvatarMaterialManager::AddTextureIDToTextureManager(System.UInt64,System.Boolean)
extern void OvrAvatarMaterialManager_AddTextureIDToTextureManager_m585FA8EB055EDD9B0FF7E96CE977AC50F707DDB5 (void);
// 0x000004D8 System.Void OvrAvatarMaterialManager::DeleteTextureSet()
extern void OvrAvatarMaterialManager_DeleteTextureSet_m0E185753E4AA75E85181E7CDD872B9581A08CCCB (void);
// 0x000004D9 System.Void OvrAvatarMaterialManager::InitTextureArrays()
extern void OvrAvatarMaterialManager_InitTextureArrays_mD3FA9B2FBA114FB8CBED80A8E6F0C7C3D4B9F45D (void);
// 0x000004DA System.Void OvrAvatarMaterialManager::ProcessTexturesWithMips(UnityEngine.Texture2D[],System.Int32,UnityEngine.Texture2DArray)
extern void OvrAvatarMaterialManager_ProcessTexturesWithMips_m029BCD34869335D62816718377CA75F15C8AAC07 (void);
// 0x000004DB System.Void OvrAvatarMaterialManager::SetMaterialPropertyBlock()
extern void OvrAvatarMaterialManager_SetMaterialPropertyBlock_m5E8587155BF7C951269D280170EE6A1D20199C33 (void);
// 0x000004DC System.Void OvrAvatarMaterialManager::ApplyMaterialPropertyBlock()
extern void OvrAvatarMaterialManager_ApplyMaterialPropertyBlock_mBFBBAB70543AD985D044D1B1261EB07B8226325B (void);
// 0x000004DD ovrAvatarBodyPartType OvrAvatarMaterialManager::GetComponentType(System.String)
extern void OvrAvatarMaterialManager_GetComponentType_mC62C71B9DBA0F6A5B3E09BED485E18118DFB82A4 (void);
// 0x000004DE System.UInt64 OvrAvatarMaterialManager::GetTextureIDForType(ovrAvatarPBSMaterialState,OvrAvatarMaterialManager/TextureType)
extern void OvrAvatarMaterialManager_GetTextureIDForType_m9CD95EA1D320AEDBE52F290FEF8FDEBAFB1626AF (void);
// 0x000004DF System.Void OvrAvatarMaterialManager::ValidateTextures(ovrAvatarPBSMaterialState[])
extern void OvrAvatarMaterialManager_ValidateTextures_m34411C5A1DD8ED20357A53E77C500A9687D20924 (void);
// 0x000004E0 System.Collections.IEnumerator OvrAvatarMaterialManager::RunLoadingAnimation(System.Action)
extern void OvrAvatarMaterialManager_RunLoadingAnimation_mDC16C0FD084B74B67F28E53111AF0C8EFA967BBE (void);
// 0x000004E1 System.Void OvrAvatarMaterialManager::.ctor()
extern void OvrAvatarMaterialManager__ctor_m768DE47BEF2C881EA5071AFA38F8086C5D476E2E (void);
// 0x000004E2 System.Void OvrAvatarMaterialManager::.cctor()
extern void OvrAvatarMaterialManager__cctor_m01A9BF6AD6CC70B929652B9D94DE52DAD037E3F9 (void);
// 0x000004E3 System.Void OvrAvatarMaterialManager/AvatarMaterialConfig::.ctor()
extern void AvatarMaterialConfig__ctor_m94DAF6BC95EAE1BEEFB2C2E7D2CAB0942F14AEDC (void);
// 0x000004E4 System.Void OvrAvatarMaterialManager/<RunLoadingAnimation>d__49::.ctor(System.Int32)
extern void U3CRunLoadingAnimationU3Ed__49__ctor_m43C27C76F38D8F3F3F60BD229ACA2F5DE8A34A81 (void);
// 0x000004E5 System.Void OvrAvatarMaterialManager/<RunLoadingAnimation>d__49::System.IDisposable.Dispose()
extern void U3CRunLoadingAnimationU3Ed__49_System_IDisposable_Dispose_mE08FA819A41936B454B0DC7414239CE5E49BE750 (void);
// 0x000004E6 System.Boolean OvrAvatarMaterialManager/<RunLoadingAnimation>d__49::MoveNext()
extern void U3CRunLoadingAnimationU3Ed__49_MoveNext_m37A6368254A8C97B72D2550D01F14CAAFC442121 (void);
// 0x000004E7 System.Object OvrAvatarMaterialManager/<RunLoadingAnimation>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunLoadingAnimationU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC75F672BB6BAD350DD052C15CA28E17A0E2C380 (void);
// 0x000004E8 System.Void OvrAvatarMaterialManager/<RunLoadingAnimation>d__49::System.Collections.IEnumerator.Reset()
extern void U3CRunLoadingAnimationU3Ed__49_System_Collections_IEnumerator_Reset_mE0F680342E7E1E8ED7EF4068256FD39CEC1C3CB9 (void);
// 0x000004E9 System.Object OvrAvatarMaterialManager/<RunLoadingAnimation>d__49::System.Collections.IEnumerator.get_Current()
extern void U3CRunLoadingAnimationU3Ed__49_System_Collections_IEnumerator_get_Current_mA67FEEB470D478F15DB1A503103A714AB1794D33 (void);
// 0x000004EA System.Single OvrAvatarPacket::get_Duration()
extern void OvrAvatarPacket_get_Duration_m296F69BE606B54FA4E43D7B7F20C96CE370C88EC (void);
// 0x000004EB OvrAvatarDriver/PoseFrame OvrAvatarPacket::get_FinalFrame()
extern void OvrAvatarPacket_get_FinalFrame_mCEA24FF57B6279986B3FE4FE568276E571D1E4FE (void);
// 0x000004EC System.Void OvrAvatarPacket::.ctor()
extern void OvrAvatarPacket__ctor_mD52B967DF3F4460B58402F2F9B810ED42BEDB3BB (void);
// 0x000004ED System.Void OvrAvatarPacket::.ctor(OvrAvatarDriver/PoseFrame)
extern void OvrAvatarPacket__ctor_mE1CAA121E13C21C0DB97398373A56538AB8778D6 (void);
// 0x000004EE System.Void OvrAvatarPacket::.ctor(System.Collections.Generic.List`1<System.Single>,System.Collections.Generic.List`1<OvrAvatarDriver/PoseFrame>,System.Collections.Generic.List`1<System.Byte[]>)
extern void OvrAvatarPacket__ctor_mE8DECB846A7BAC69B4D50720C471E08F19A00371 (void);
// 0x000004EF System.Void OvrAvatarPacket::AddFrame(OvrAvatarDriver/PoseFrame,System.Single)
extern void OvrAvatarPacket_AddFrame_m38AF69FAF2A01CC1F49F12ABDFB679C3210EF040 (void);
// 0x000004F0 OvrAvatarDriver/PoseFrame OvrAvatarPacket::GetPoseFrame(System.Single)
extern void OvrAvatarPacket_GetPoseFrame_m6F949DB2D47B799213B7C68470222DC4ACDFF4DC (void);
// 0x000004F1 OvrAvatarPacket OvrAvatarPacket::Read(System.IO.Stream)
extern void OvrAvatarPacket_Read_m066559D73CE8DCA5AB1C5D53EB6F39FFD4545681 (void);
// 0x000004F2 System.Void OvrAvatarPacket::Write(System.IO.Stream)
extern void OvrAvatarPacket_Write_m7642998DF5BEDE118D40278CCE95E7CEA7BA1FEE (void);
// 0x000004F3 System.Void BinaryWriterExtensions::Write(System.IO.BinaryWriter,OvrAvatarDriver/PoseFrame)
extern void BinaryWriterExtensions_Write_m446C798DF132C6F24343370DB33C59326A1E95AC (void);
// 0x000004F4 System.Void BinaryWriterExtensions::Write(System.IO.BinaryWriter,UnityEngine.Vector3)
extern void BinaryWriterExtensions_Write_m21975F2DA2BAFC07AF3CF90B6203DF169A4E157C (void);
// 0x000004F5 System.Void BinaryWriterExtensions::Write(System.IO.BinaryWriter,UnityEngine.Vector2)
extern void BinaryWriterExtensions_Write_mC7841D88FC4F08AB79DFE3C521E197302C6BE074 (void);
// 0x000004F6 System.Void BinaryWriterExtensions::Write(System.IO.BinaryWriter,UnityEngine.Quaternion)
extern void BinaryWriterExtensions_Write_m4C10A3AAA917ADAB2B414844E19A4DF29EB3F136 (void);
// 0x000004F7 System.Void BinaryWriterExtensions::Write(System.IO.BinaryWriter,OvrAvatarDriver/ControllerPose)
extern void BinaryWriterExtensions_Write_m185C8626C0C25BE258AD731FFFA746C4D7EFC754 (void);
// 0x000004F8 OvrAvatarDriver/PoseFrame BinaryReaderExtensions::ReadPoseFrame(System.IO.BinaryReader)
extern void BinaryReaderExtensions_ReadPoseFrame_mFD945F0699DD814338CE03BDCA2211495254431E (void);
// 0x000004F9 UnityEngine.Vector2 BinaryReaderExtensions::ReadVector2(System.IO.BinaryReader)
extern void BinaryReaderExtensions_ReadVector2_mE3C25910DDFCFAF8981CB5001DD5EDF3098671A5 (void);
// 0x000004FA UnityEngine.Vector3 BinaryReaderExtensions::ReadVector3(System.IO.BinaryReader)
extern void BinaryReaderExtensions_ReadVector3_mB04805367268FC0013FF5824C402B6F65D3DBB80 (void);
// 0x000004FB UnityEngine.Quaternion BinaryReaderExtensions::ReadQuaternion(System.IO.BinaryReader)
extern void BinaryReaderExtensions_ReadQuaternion_m9B69DBADD15BB59D47169D955EDA4AFBB0DF8BE3 (void);
// 0x000004FC OvrAvatarDriver/ControllerPose BinaryReaderExtensions::ReadControllerPose(System.IO.BinaryReader)
extern void BinaryReaderExtensions_ReadControllerPose_m26574F17311CB8A944640B3757455239CEA73C0E (void);
// 0x000004FD System.Void OvrAvatarRemoteDriver::QueuePacket(System.Int32,OvrAvatarPacket)
extern void OvrAvatarRemoteDriver_QueuePacket_m2E81DB0E70F4BFD20B3758C568BDD88562D4622F (void);
// 0x000004FE System.Void OvrAvatarRemoteDriver::UpdateTransforms(System.IntPtr)
extern void OvrAvatarRemoteDriver_UpdateTransforms_m6B1FC6DFD64DE9AEB1FB6BCACB6CB88A38103D12 (void);
// 0x000004FF System.Void OvrAvatarRemoteDriver::UpdateFromSDKPacket(System.IntPtr)
extern void OvrAvatarRemoteDriver_UpdateFromSDKPacket_m528D84F57A02F1F2CBB2E69FC1C16DADB66C52B5 (void);
// 0x00000500 System.Void OvrAvatarRemoteDriver::UpdateFromUnityPacket(System.IntPtr)
extern void OvrAvatarRemoteDriver_UpdateFromUnityPacket_m26C3888D91024E4A5FE2F564B2D90DFCCAD9F10C (void);
// 0x00000501 System.Void OvrAvatarRemoteDriver::.ctor()
extern void OvrAvatarRemoteDriver__ctor_mADF67945EB1D689ACBA4F4741BD94957D3386132 (void);
// 0x00000502 System.Void OvrAvatarRenderComponent::UpdateActive(OvrAvatar,ovrAvatarVisibilityFlags)
extern void OvrAvatarRenderComponent_UpdateActive_m5DAFE0C6899E5AD32104CD4575CD253D42EC151C (void);
// 0x00000503 UnityEngine.SkinnedMeshRenderer OvrAvatarRenderComponent::CreateSkinnedMesh(System.UInt64,ovrAvatarVisibilityFlags,System.Int32,System.Int32)
extern void OvrAvatarRenderComponent_CreateSkinnedMesh_mFE6D9DA8E5F1B95F0782E4490CD94A7576DDDF35 (void);
// 0x00000504 System.Void OvrAvatarRenderComponent::UpdateSkinnedMesh(OvrAvatar,UnityEngine.Transform[],ovrAvatarTransform,ovrAvatarVisibilityFlags,System.IntPtr)
extern void OvrAvatarRenderComponent_UpdateSkinnedMesh_mBD6812162FC47D2A39BEEE9233C049B6DAD4734B (void);
// 0x00000505 UnityEngine.Material OvrAvatarRenderComponent::CreateAvatarMaterial(System.String,UnityEngine.Shader)
extern void OvrAvatarRenderComponent_CreateAvatarMaterial_m7CFCCA6394C8C11644BBB7F9240A77BB459C6126 (void);
// 0x00000506 System.Void OvrAvatarRenderComponent::.ctor()
extern void OvrAvatarRenderComponent__ctor_m24877CDBB946D3B93B6D3F9E653C380C09160376 (void);
// 0x00000507 System.Void ovrAvatarComponent_Offsets::.cctor()
extern void ovrAvatarComponent_Offsets__cctor_m79EA446A7D9C6A608AF45C62DF26A4AA27A0C779 (void);
// 0x00000508 System.Void ovrAvatarBodyComponent_Offsets::.cctor()
extern void ovrAvatarBodyComponent_Offsets__cctor_m37DCA998128851151E27F431221B6E7728F103B2 (void);
// 0x00000509 System.Boolean ovrAvatarMaterialLayerState::VectorEquals(UnityEngine.Vector4,UnityEngine.Vector4)
extern void ovrAvatarMaterialLayerState_VectorEquals_m0F49E3B7839726FD77C50D6AB1749DC8C6BC85F3 (void);
// 0x0000050A System.Boolean ovrAvatarMaterialLayerState::Equals(System.Object)
extern void ovrAvatarMaterialLayerState_Equals_m1C14E1DBA874C1083F95A564F30780DEF30E435E (void);
// 0x0000050B System.Int32 ovrAvatarMaterialLayerState::GetHashCode()
extern void ovrAvatarMaterialLayerState_GetHashCode_m3302E925482D7B9E2072339A1F3C3EF15F46B519 (void);
// 0x0000050C System.Boolean ovrAvatarMaterialState::VectorEquals(UnityEngine.Vector4,UnityEngine.Vector4)
extern void ovrAvatarMaterialState_VectorEquals_m8B0E482C9D79CB23F353C80DCAC9A9ECA0D0679B (void);
// 0x0000050D System.Boolean ovrAvatarMaterialState::Equals(System.Object)
extern void ovrAvatarMaterialState_Equals_m293D1C2CBB4C8403F0D14E1A68DB14379FEF491D (void);
// 0x0000050E System.Int32 ovrAvatarMaterialState::GetHashCode()
extern void ovrAvatarMaterialState_GetHashCode_m573CEF4EF15464DAF18046F8F0B7E56705B55740 (void);
// 0x0000050F System.Boolean ovrAvatarExpressiveParameters::VectorEquals(UnityEngine.Vector4,UnityEngine.Vector4)
extern void ovrAvatarExpressiveParameters_VectorEquals_m12124A3A84669FEF3298F1AE3AF7707E6FE4292F (void);
// 0x00000510 System.Boolean ovrAvatarExpressiveParameters::Equals(System.Object)
extern void ovrAvatarExpressiveParameters_Equals_m501E5CF62F65200176CC04A883C7CD3CB29C0DB1 (void);
// 0x00000511 System.Int32 ovrAvatarExpressiveParameters::GetHashCode()
extern void ovrAvatarExpressiveParameters_GetHashCode_mEA3158598F1C76B2480355833396802E41C4E75B (void);
// 0x00000512 System.Boolean ovrAvatarPBSMaterialState::VectorEquals(UnityEngine.Vector4,UnityEngine.Vector4)
extern void ovrAvatarPBSMaterialState_VectorEquals_mBE6A57EDA248DF344BCC3C204832D79266C1ABE9 (void);
// 0x00000513 System.Boolean ovrAvatarPBSMaterialState::Equals(System.Object)
extern void ovrAvatarPBSMaterialState_Equals_m1C1A3BFBDC22AE8A5A6A3E28725926A9B34C3209 (void);
// 0x00000514 System.Int32 ovrAvatarPBSMaterialState::GetHashCode()
extern void ovrAvatarPBSMaterialState_GetHashCode_m8D5F74AC0D6E2F3546275C51C5142D5572BFFA78 (void);
// 0x00000515 System.Void OvrAvatarAssetMaterial::.ctor(System.UInt64,System.IntPtr)
extern void OvrAvatarAssetMaterial__ctor_m7544427A852734E8A5D47AB90DF220301B710B56 (void);
// 0x00000516 System.Void ovrAvatarBlendShapeParams_Offsets::.cctor()
extern void ovrAvatarBlendShapeParams_Offsets__cctor_m6D9041980A2912344E42DAF204B833E30235EC29 (void);
// 0x00000517 System.Void ovrAvatarVisemes_Offsets::.cctor()
extern void ovrAvatarVisemes_Offsets__cctor_m4E405CF2A255B832C38697BCE85951DB07A73168 (void);
// 0x00000518 System.Void ovrAvatarGazeTarget_Offsets::.cctor()
extern void ovrAvatarGazeTarget_Offsets__cctor_mFDD3A73DC15E156590CD887592D50048E4249316 (void);
// 0x00000519 System.Void ovrAvatarGazeTargets_Offsets::.cctor()
extern void ovrAvatarGazeTargets_Offsets__cctor_mD23B3BAF735FD7EC7FD1B4B7109AB4D04324FC46 (void);
// 0x0000051A System.Void ovrAvatarLight_Offsets::.cctor()
extern void ovrAvatarLight_Offsets__cctor_m521DE5242539E17683EE76311BC289EDE65E04AE (void);
// 0x0000051B System.Void ovrAvatarLights_Offsets::.cctor()
extern void ovrAvatarLights_Offsets__cctor_mAAD61322FFEB720780FEA560BCC4E6888D7C6990 (void);
// 0x0000051C System.Void specificationCallback::.ctor(System.Object,System.IntPtr)
extern void specificationCallback__ctor_m49430870F265EE01A85245F2A8CDF76EF07E1620 (void);
// 0x0000051D System.Void specificationCallback::Invoke(System.IntPtr)
extern void specificationCallback_Invoke_mE6CEDC7F76E008998E965BE52ED26ECDE7428914 (void);
// 0x0000051E System.IAsyncResult specificationCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void specificationCallback_BeginInvoke_m39BC7E000A26EC2E691ADDFE8DB980B349237F80 (void);
// 0x0000051F System.Void specificationCallback::EndInvoke(System.IAsyncResult)
extern void specificationCallback_EndInvoke_m67BADEF6AAE9318D70EE75B34F1365205D9E8F1E (void);
// 0x00000520 System.Void assetLoadedCallback::.ctor(System.Object,System.IntPtr)
extern void assetLoadedCallback__ctor_m98D354A6C2CC719D0463B26F84A0157EFFD0702D (void);
// 0x00000521 System.Void assetLoadedCallback::Invoke(OvrAvatarAsset)
extern void assetLoadedCallback_Invoke_mACE414A8D1CE4671949BD46DA45D10CCD3785523 (void);
// 0x00000522 System.IAsyncResult assetLoadedCallback::BeginInvoke(OvrAvatarAsset,System.AsyncCallback,System.Object)
extern void assetLoadedCallback_BeginInvoke_mFEBEB34E8B4A4DCDBAB0562B43C59D013B1B624E (void);
// 0x00000523 System.Void assetLoadedCallback::EndInvoke(System.IAsyncResult)
extern void assetLoadedCallback_EndInvoke_m7D155B49E64129ADEC1DAB24680462380465F5C0 (void);
// 0x00000524 System.Void combinedMeshLoadedCallback::.ctor(System.Object,System.IntPtr)
extern void combinedMeshLoadedCallback__ctor_m95CE9B5144E8DE923CE6ECF548704E880D70391D (void);
// 0x00000525 System.Void combinedMeshLoadedCallback::Invoke(System.IntPtr)
extern void combinedMeshLoadedCallback_Invoke_mF25554358775A40E62C19201FD51AFD6C7750656 (void);
// 0x00000526 System.IAsyncResult combinedMeshLoadedCallback::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void combinedMeshLoadedCallback_BeginInvoke_m7B0251721E56C2D3CCD2F2CC13D29DDBB7787CB5 (void);
// 0x00000527 System.Void combinedMeshLoadedCallback::EndInvoke(System.IAsyncResult)
extern void combinedMeshLoadedCallback_EndInvoke_mA989F9F3703FB365A6BC5D656EA016B78C3F7B47 (void);
// 0x00000528 OvrAvatarSDKManager OvrAvatarSDKManager::get_Instance()
extern void OvrAvatarSDKManager_get_Instance_mB9B5FF8B8F1E78391247927F232F3F7383E70563 (void);
// 0x00000529 System.Boolean OvrAvatarSDKManager::Initialize()
extern void OvrAvatarSDKManager_Initialize_m5BD26AE5A66236ADADDFFBBEC2D22E93885AE0FF (void);
// 0x0000052A System.Void OvrAvatarSDKManager::OnDestroy()
extern void OvrAvatarSDKManager_OnDestroy_m5380FD15A8AAA3F246DBD4373EF79C5C9DF1CEEC (void);
// 0x0000052B System.Void OvrAvatarSDKManager::Update()
extern void OvrAvatarSDKManager_Update_m44DF588B43A11234E2FF4A64E6B87F651777367B (void);
// 0x0000052C System.Boolean OvrAvatarSDKManager::IsAvatarSpecWaiting()
extern void OvrAvatarSDKManager_IsAvatarSpecWaiting_mF531B80DACCF54019AFE42894710C6C0AA9EABF6 (void);
// 0x0000052D System.Boolean OvrAvatarSDKManager::IsAvatarLoading()
extern void OvrAvatarSDKManager_IsAvatarLoading_mFEDFCD39684FAC17722B08023D9DA140A1C4322D (void);
// 0x0000052E System.Void OvrAvatarSDKManager::AddLoadingAvatar(System.Int32)
extern void OvrAvatarSDKManager_AddLoadingAvatar_mF0590DEF0E1CF074E92085951129624B08C06FA0 (void);
// 0x0000052F System.Void OvrAvatarSDKManager::RemoveLoadingAvatar(System.Int32)
extern void OvrAvatarSDKManager_RemoveLoadingAvatar_m58DCEA6BC09480677D9134202DB39FBEB8945A0D (void);
// 0x00000530 System.Void OvrAvatarSDKManager::RequestAvatarSpecification(OvrAvatarSDKManager/AvatarSpecRequestParams)
extern void OvrAvatarSDKManager_RequestAvatarSpecification_mD355097C0CFA323AB0E6D2207D370300446EA9FF (void);
// 0x00000531 System.Void OvrAvatarSDKManager::DispatchAvatarSpecificationRequest(OvrAvatarSDKManager/AvatarSpecRequestParams)
extern void OvrAvatarSDKManager_DispatchAvatarSpecificationRequest_m69F1994F001D5F38A64479A033D9C7CE56804969 (void);
// 0x00000532 System.Void OvrAvatarSDKManager::BeginLoadingAsset(System.UInt64,ovrAvatarAssetLevelOfDetail,assetLoadedCallback)
extern void OvrAvatarSDKManager_BeginLoadingAsset_mCC22D7727D70A5BD2A4B2CC5CD788C5480BA3663 (void);
// 0x00000533 System.Void OvrAvatarSDKManager::RegisterCombinedMeshCallback(System.IntPtr,combinedMeshLoadedCallback)
extern void OvrAvatarSDKManager_RegisterCombinedMeshCallback_mF1A9841550BFA90A55371808D502EA719BE41783 (void);
// 0x00000534 OvrAvatarAsset OvrAvatarSDKManager::GetAsset(System.UInt64)
extern void OvrAvatarSDKManager_GetAsset_m5327170071598FD893DFDE5A4272A30F5750C55C (void);
// 0x00000535 System.Void OvrAvatarSDKManager::DeleteAssetFromCache(System.UInt64)
extern void OvrAvatarSDKManager_DeleteAssetFromCache_mB2BEEC55CFB786AF0B6EE172F2C75B7AAB2A97B3 (void);
// 0x00000536 System.String OvrAvatarSDKManager::GetAppId()
extern void OvrAvatarSDKManager_GetAppId_mDD2D0505570BF16344263D082F9EF0D419CD5EDA (void);
// 0x00000537 OvrAvatarTextureCopyManager OvrAvatarSDKManager::GetTextureCopyManager()
extern void OvrAvatarSDKManager_GetTextureCopyManager_mAD832C2A625A244AA199A83CF6729915A056625A (void);
// 0x00000538 System.Void OvrAvatarSDKManager::.ctor()
extern void OvrAvatarSDKManager__ctor_m2D3B0B78DD42EDE0C78AF88A4C57AADFD7744338 (void);
// 0x00000539 System.Void OvrAvatarSDKManager/AvatarSpecRequestParams::.ctor(System.UInt64,specificationCallback,System.Boolean,ovrAvatarAssetLevelOfDetail,System.Boolean,ovrAvatarLookAndFeelVersion,ovrAvatarLookAndFeelVersion,System.Boolean)
extern void AvatarSpecRequestParams__ctor_mC4E88966CA0552017DF34A410391946CFEF4021D (void);
// 0x0000053A System.String OvrAvatarSettings::get_AppID()
extern void OvrAvatarSettings_get_AppID_m5077983A11AC7223C3C80C81C2C3B348B9231F86 (void);
// 0x0000053B System.Void OvrAvatarSettings::set_AppID(System.String)
extern void OvrAvatarSettings_set_AppID_mFBF334F95276E3D8C9379870787EE2316E46CE9F (void);
// 0x0000053C System.String OvrAvatarSettings::get_MobileAppID()
extern void OvrAvatarSettings_get_MobileAppID_m422288C504386C1484BFED15CE1BEA3EDEB9E9EF (void);
// 0x0000053D System.Void OvrAvatarSettings::set_MobileAppID(System.String)
extern void OvrAvatarSettings_set_MobileAppID_mB599A44E4BD07D5A6E5EA14180275EAC58AF1487 (void);
// 0x0000053E OvrAvatarSettings OvrAvatarSettings::get_Instance()
extern void OvrAvatarSettings_get_Instance_m1F2D4D698A20F907854549C31911AEABCC5424EF (void);
// 0x0000053F System.Void OvrAvatarSettings::set_Instance(OvrAvatarSettings)
extern void OvrAvatarSettings_set_Instance_mB47E146E75A592D2E0BB1961BD6BD50547CBD247 (void);
// 0x00000540 System.Void OvrAvatarSettings::.ctor()
extern void OvrAvatarSettings__ctor_mC20E3765ACEE15B37A1A00AF1941524AA325D8FD (void);
// 0x00000541 System.Void OvrAvatarSkinnedMeshRenderComponent::Initialize(ovrAvatarRenderPart_SkinnedMeshRender,UnityEngine.Shader,UnityEngine.Shader,System.Int32,System.Int32)
extern void OvrAvatarSkinnedMeshRenderComponent_Initialize_mC1AFEA820CD38750C5E43AE3AE1B507D1BE99406 (void);
// 0x00000542 System.Void OvrAvatarSkinnedMeshRenderComponent::UpdateSkinnedMeshRender(OvrAvatarComponent,OvrAvatar,System.IntPtr)
extern void OvrAvatarSkinnedMeshRenderComponent_UpdateSkinnedMeshRender_m1CE4439BF8D73BF8503272232D166AAFA545F81E (void);
// 0x00000543 System.Void OvrAvatarSkinnedMeshRenderComponent::UpdateMeshMaterial(ovrAvatarVisibilityFlags,UnityEngine.SkinnedMeshRenderer)
extern void OvrAvatarSkinnedMeshRenderComponent_UpdateMeshMaterial_mA2EAD33553131CFF85ABE2A788DBB3EF9A286FB9 (void);
// 0x00000544 System.Void OvrAvatarSkinnedMeshRenderComponent::.ctor()
extern void OvrAvatarSkinnedMeshRenderComponent__ctor_mAAFF60F8F7011F8CABF1893F3C200C1F2E29C637 (void);
// 0x00000545 System.Void OvrAvatarSkinnedMeshRenderPBSComponent::Initialize(ovrAvatarRenderPart_SkinnedMeshRenderPBS,UnityEngine.Shader,System.Int32,System.Int32)
extern void OvrAvatarSkinnedMeshRenderPBSComponent_Initialize_m80494C16D424C44B683B2390A16FC600A5ADAB1F (void);
// 0x00000546 System.Void OvrAvatarSkinnedMeshRenderPBSComponent::UpdateSkinnedMeshRenderPBS(OvrAvatar,System.IntPtr,UnityEngine.Material)
extern void OvrAvatarSkinnedMeshRenderPBSComponent_UpdateSkinnedMeshRenderPBS_mD3B0539E844751EA3FC12A8168FA15A544A43F12 (void);
// 0x00000547 System.Void OvrAvatarSkinnedMeshRenderPBSComponent::.ctor()
extern void OvrAvatarSkinnedMeshRenderPBSComponent__ctor_m09F2C6B04806341383164F13D46E3A3C59BF533B (void);
// 0x00000548 System.Void OvrAvatarSkinnedMeshPBSV2RenderComponent::Initialize(System.IntPtr,ovrAvatarRenderPart_SkinnedMeshRenderPBS_V2,OvrAvatarMaterialManager,System.Int32,System.Int32,System.Boolean,ovrAvatarAssetLevelOfDetail,System.Boolean,OvrAvatar,System.Boolean)
extern void OvrAvatarSkinnedMeshPBSV2RenderComponent_Initialize_m48F349C1F7D008718FEF0CCB0BBB6E14F962B3A4 (void);
// 0x00000549 System.Void OvrAvatarSkinnedMeshPBSV2RenderComponent::UpdateSkinnedMeshRender(OvrAvatarComponent,OvrAvatar,System.IntPtr)
extern void OvrAvatarSkinnedMeshPBSV2RenderComponent_UpdateSkinnedMeshRender_m7D1981139187AA4E292360970620B34730AB7F5B (void);
// 0x0000054A System.Void OvrAvatarSkinnedMeshPBSV2RenderComponent::InitializeSingleComponentMaterial(System.IntPtr,System.Int32)
extern void OvrAvatarSkinnedMeshPBSV2RenderComponent_InitializeSingleComponentMaterial_mB54505D8670BE8979C5C9081E4F127D064980DC8 (void);
// 0x0000054B System.Void OvrAvatarSkinnedMeshPBSV2RenderComponent::InitializeCombinedMaterial(System.IntPtr,System.Int32)
extern void OvrAvatarSkinnedMeshPBSV2RenderComponent_InitializeCombinedMaterial_m26D97507031F1B647035660C464E816C31073670 (void);
// 0x0000054C System.Void OvrAvatarSkinnedMeshPBSV2RenderComponent::SetMaterialTransparent(UnityEngine.Material)
extern void OvrAvatarSkinnedMeshPBSV2RenderComponent_SetMaterialTransparent_mAC9A398B2903E420EE6D4D4F55625AFD4550CED6 (void);
// 0x0000054D System.Void OvrAvatarSkinnedMeshPBSV2RenderComponent::SetMaterialOpaque(UnityEngine.Material)
extern void OvrAvatarSkinnedMeshPBSV2RenderComponent_SetMaterialOpaque_mE542ABED3552C07BA6B7289653AD43855073BA5D (void);
// 0x0000054E System.Void OvrAvatarSkinnedMeshPBSV2RenderComponent::.ctor()
extern void OvrAvatarSkinnedMeshPBSV2RenderComponent__ctor_m6BF007FC10A10C8C9A1E52C06CA90C3203B62BEF (void);
// 0x0000054F System.Void OvrAvatarTextureCopyManager::.ctor()
extern void OvrAvatarTextureCopyManager__ctor_m0CCDBDF969C1BAF740B7BC3A84D809007A135113 (void);
// 0x00000550 System.Void OvrAvatarTextureCopyManager::Update()
extern void OvrAvatarTextureCopyManager_Update_m737B583D618DC4073C3D7EF2CA9134680BC47CAD (void);
// 0x00000551 System.Int32 OvrAvatarTextureCopyManager::GetTextureCount()
extern void OvrAvatarTextureCopyManager_GetTextureCount_m7E8EFA97005056B17B3ADDBF04E715C20AE287DA (void);
// 0x00000552 System.Void OvrAvatarTextureCopyManager::CopyTexture(UnityEngine.Texture,UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void OvrAvatarTextureCopyManager_CopyTexture_m78E65D5F8F1742D6E3859F2FD1305E8D75327AEC (void);
// 0x00000553 System.Void OvrAvatarTextureCopyManager::CopyTexture(OvrAvatarTextureCopyManager/CopyTextureParams)
extern void OvrAvatarTextureCopyManager_CopyTexture_m41E7EBA84335D0E9146D9938FA32DD831EBC1E2D (void);
// 0x00000554 System.Void OvrAvatarTextureCopyManager::AddTextureIDToTextureSet(System.Int32,System.UInt64,System.Boolean)
extern void OvrAvatarTextureCopyManager_AddTextureIDToTextureSet_mD9D20776722FD413D977173033B4772489DAB951 (void);
// 0x00000555 System.Void OvrAvatarTextureCopyManager::DeleteTextureSet(System.Int32)
extern void OvrAvatarTextureCopyManager_DeleteTextureSet_mDAD44978C2CFDC23C82CB1E176532483D15D6C91 (void);
// 0x00000556 System.Collections.IEnumerator OvrAvatarTextureCopyManager::DeleteTextureSetCoroutine(OvrAvatarTextureCopyManager/TextureSet,System.Int32)
extern void OvrAvatarTextureCopyManager_DeleteTextureSetCoroutine_m7207FDEA225FFD902D1896643088801AA3C7E128 (void);
// 0x00000557 System.Void OvrAvatarTextureCopyManager::CheckFallbackTextureSet(ovrAvatarAssetLevelOfDetail)
extern void OvrAvatarTextureCopyManager_CheckFallbackTextureSet_m012EBD8463DABD4557C70C495A3EC89478BA5C62 (void);
// 0x00000558 System.Void OvrAvatarTextureCopyManager::InitFallbackTextureSet(ovrAvatarAssetLevelOfDetail)
extern void OvrAvatarTextureCopyManager_InitFallbackTextureSet_m0713E1FD188454AC61EDDE1EC12EE2BEDE5C3A0E (void);
// 0x00000559 System.Void OvrAvatarTextureCopyManager/CopyTextureParams::.ctor(UnityEngine.Texture,UnityEngine.Texture,System.Int32,System.Int32,System.Int32)
extern void CopyTextureParams__ctor_m1B3ADEEC9C61F14F8CA8EBCFF0FED7020C338DC2 (void);
// 0x0000055A System.Void OvrAvatarTextureCopyManager/TextureSet::.ctor(System.Collections.Generic.Dictionary`2<System.UInt64,System.Boolean>,System.Boolean)
extern void TextureSet__ctor_mD82F34E402F4351FF6634FAF2614B45506EC8B8C (void);
// 0x0000055B System.Void OvrAvatarTextureCopyManager/<DeleteTextureSetCoroutine>d__24::.ctor(System.Int32)
extern void U3CDeleteTextureSetCoroutineU3Ed__24__ctor_m79238538B0AB6415A5C3E0847361F2043C061051 (void);
// 0x0000055C System.Void OvrAvatarTextureCopyManager/<DeleteTextureSetCoroutine>d__24::System.IDisposable.Dispose()
extern void U3CDeleteTextureSetCoroutineU3Ed__24_System_IDisposable_Dispose_mC19B8CDE02AA7463AF3A312C1BDA6DCF40660D7E (void);
// 0x0000055D System.Boolean OvrAvatarTextureCopyManager/<DeleteTextureSetCoroutine>d__24::MoveNext()
extern void U3CDeleteTextureSetCoroutineU3Ed__24_MoveNext_m5D84322450A11F76FAE8AC3D25744EFF9B3198A6 (void);
// 0x0000055E System.Object OvrAvatarTextureCopyManager/<DeleteTextureSetCoroutine>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeleteTextureSetCoroutineU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3BB8FBE6FBCB81BC013B5E77224876F974568FF (void);
// 0x0000055F System.Void OvrAvatarTextureCopyManager/<DeleteTextureSetCoroutine>d__24::System.Collections.IEnumerator.Reset()
extern void U3CDeleteTextureSetCoroutineU3Ed__24_System_Collections_IEnumerator_Reset_m6A4C0016B310142BB80ECBD048150ADCA25E6EDD (void);
// 0x00000560 System.Object OvrAvatarTextureCopyManager/<DeleteTextureSetCoroutine>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CDeleteTextureSetCoroutineU3Ed__24_System_Collections_IEnumerator_get_Current_m1941004ECF1ADA168283893C759EA71E996784C1 (void);
// 0x00000561 System.Void OvrAvatarTouchController::Update()
extern void OvrAvatarTouchController_Update_mD0090C21D424B18F7CE3E579DE7B7B5C3AAD85D3 (void);
// 0x00000562 System.Void OvrAvatarTouchController::.ctor()
extern void OvrAvatarTouchController__ctor_m9FF70E0776D27496432CC9F96D9A28300A30E3B9 (void);
// 0x00000563 System.Void DebugUIBuilder::Awake()
extern void DebugUIBuilder_Awake_m403F18586F94598A6A54C5B1899CF76032848602 (void);
// 0x00000564 System.Void DebugUIBuilder::Show()
extern void DebugUIBuilder_Show_mF284EB0ED0943893BD5799EBFCCB1DB6B192DC2E (void);
// 0x00000565 System.Void DebugUIBuilder::Hide()
extern void DebugUIBuilder_Hide_m23BD4DC6DB4E714215B97C9A37417D70356FD0C2 (void);
// 0x00000566 System.Void DebugUIBuilder::Relayout()
extern void DebugUIBuilder_Relayout_m3937EE57F0E4C6F007BE6CB4CF0940FBEC300C28 (void);
// 0x00000567 System.Void DebugUIBuilder::AddRect(UnityEngine.RectTransform,System.Int32)
extern void DebugUIBuilder_AddRect_m877BEB5A300FE474E3A148ABB8BA0C0FBB224EA9 (void);
// 0x00000568 UnityEngine.RectTransform DebugUIBuilder::AddButton(System.String,DebugUIBuilder/OnClick,System.Int32)
extern void DebugUIBuilder_AddButton_m2FA1F9C35EEDC9AD3BDFEA645B88E8ABAC9F9274 (void);
// 0x00000569 UnityEngine.RectTransform DebugUIBuilder::AddLabel(System.String,System.Int32)
extern void DebugUIBuilder_AddLabel_m6A98E248939BD65B054A4B4C4420D950F4811CAD (void);
// 0x0000056A UnityEngine.RectTransform DebugUIBuilder::AddSlider(System.String,System.Single,System.Single,DebugUIBuilder/OnSlider,System.Boolean,System.Int32)
extern void DebugUIBuilder_AddSlider_mC4E1DB56081E390B06F2B7D02C67BA683D84686F (void);
// 0x0000056B UnityEngine.RectTransform DebugUIBuilder::AddDivider(System.Int32)
extern void DebugUIBuilder_AddDivider_m858451240D2EAE0D367F8145E2AC917929B15DB2 (void);
// 0x0000056C UnityEngine.RectTransform DebugUIBuilder::AddToggle(System.String,DebugUIBuilder/OnToggleValueChange,System.Int32)
extern void DebugUIBuilder_AddToggle_m3A79A54CC94114F24B771CD465F40B001CBFCAFD (void);
// 0x0000056D UnityEngine.RectTransform DebugUIBuilder::AddToggle(System.String,DebugUIBuilder/OnToggleValueChange,System.Boolean,System.Int32)
extern void DebugUIBuilder_AddToggle_mAA47BCD38AB222645D00709AFB422F4D5CF02DB8 (void);
// 0x0000056E UnityEngine.RectTransform DebugUIBuilder::AddRadio(System.String,System.String,DebugUIBuilder/OnToggleValueChange,System.Int32)
extern void DebugUIBuilder_AddRadio_m783EACB19FD91F15BD9BE7EDABCDD6B05D639639 (void);
// 0x0000056F UnityEngine.RectTransform DebugUIBuilder::AddTextField(System.String,System.Int32)
extern void DebugUIBuilder_AddTextField_m726C38770CC36156FC5B71739F63536EA55540A1 (void);
// 0x00000570 System.Void DebugUIBuilder::ToggleLaserPointer(System.Boolean)
extern void DebugUIBuilder_ToggleLaserPointer_m19D2F9E314DF85DEE576A53C5E719685B14A2B55 (void);
// 0x00000571 System.Void DebugUIBuilder::.ctor()
extern void DebugUIBuilder__ctor_m95BD850859B88DCA361D481FC4A0C998DB46496E (void);
// 0x00000572 System.Void DebugUIBuilder/OnClick::.ctor(System.Object,System.IntPtr)
extern void OnClick__ctor_mC5739B4F2FE456031D0F032D2B1335A8C7E07F76 (void);
// 0x00000573 System.Void DebugUIBuilder/OnClick::Invoke()
extern void OnClick_Invoke_mDC64AF5D49F6C27A1C15E0695D0B9B8A51958AC9 (void);
// 0x00000574 System.IAsyncResult DebugUIBuilder/OnClick::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnClick_BeginInvoke_m26A6065C75CFBA6B575C90229E2689F93697C002 (void);
// 0x00000575 System.Void DebugUIBuilder/OnClick::EndInvoke(System.IAsyncResult)
extern void OnClick_EndInvoke_m9DC1A8BFEE9B5B1036401A570221ECB4C32AD697 (void);
// 0x00000576 System.Void DebugUIBuilder/OnToggleValueChange::.ctor(System.Object,System.IntPtr)
extern void OnToggleValueChange__ctor_mDED41EA7D290F566BBDE14F48F7DAB95A65513A3 (void);
// 0x00000577 System.Void DebugUIBuilder/OnToggleValueChange::Invoke(UnityEngine.UI.Toggle)
extern void OnToggleValueChange_Invoke_m7C6B23451E232EEEB6C2F95BAD41C5E5716303CE (void);
// 0x00000578 System.IAsyncResult DebugUIBuilder/OnToggleValueChange::BeginInvoke(UnityEngine.UI.Toggle,System.AsyncCallback,System.Object)
extern void OnToggleValueChange_BeginInvoke_m33F94F819A1EB1D23BC216963765EDB1740702C8 (void);
// 0x00000579 System.Void DebugUIBuilder/OnToggleValueChange::EndInvoke(System.IAsyncResult)
extern void OnToggleValueChange_EndInvoke_mA11DE32406ADE7239AD2833535BF1C49DB4E292B (void);
// 0x0000057A System.Void DebugUIBuilder/OnSlider::.ctor(System.Object,System.IntPtr)
extern void OnSlider__ctor_m748CDFE6C061BCC4EAB8C70EDEF6413DBC5748C8 (void);
// 0x0000057B System.Void DebugUIBuilder/OnSlider::Invoke(System.Single)
extern void OnSlider_Invoke_mC63EB553A881C3BD597451B10831BAD4EC03F5C9 (void);
// 0x0000057C System.IAsyncResult DebugUIBuilder/OnSlider::BeginInvoke(System.Single,System.AsyncCallback,System.Object)
extern void OnSlider_BeginInvoke_mFA997752B26047A4D133433B32C14383D0E1F7DB (void);
// 0x0000057D System.Void DebugUIBuilder/OnSlider::EndInvoke(System.IAsyncResult)
extern void OnSlider_EndInvoke_mCC34E0F20749EC0287A3FDC4FDC354A88202FEB2 (void);
// 0x0000057E System.Void DebugUIBuilder/ActiveUpdate::.ctor(System.Object,System.IntPtr)
extern void ActiveUpdate__ctor_mC45D2B4C2665FD2DC12090EAB9C0B5044514DF19 (void);
// 0x0000057F System.Boolean DebugUIBuilder/ActiveUpdate::Invoke()
extern void ActiveUpdate_Invoke_m769E06EC3AB44D98C5C452D5B2336809F87C1E18 (void);
// 0x00000580 System.IAsyncResult DebugUIBuilder/ActiveUpdate::BeginInvoke(System.AsyncCallback,System.Object)
extern void ActiveUpdate_BeginInvoke_m720D31DDBFECDC31563B554DE0450793E969F877 (void);
// 0x00000581 System.Boolean DebugUIBuilder/ActiveUpdate::EndInvoke(System.IAsyncResult)
extern void ActiveUpdate_EndInvoke_mFA8C1E60854662BE8B396F7BC93367E235ACB707 (void);
// 0x00000582 System.Void DebugUIBuilder/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m236E036B67727BA8579DE1C608CD3ED374C1211D (void);
// 0x00000583 System.Void DebugUIBuilder/<>c__DisplayClass36_0::<AddButton>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CAddButtonU3Eb__0_m5FF7DBCC3B8181548A527A9F7EF193DB9D0D327E (void);
// 0x00000584 System.Void DebugUIBuilder/<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_mF607CC87B637181BABC35D98A59112CD9D091E56 (void);
// 0x00000585 System.Void DebugUIBuilder/<>c__DisplayClass38_0::<AddSlider>b__0(System.Single)
extern void U3CU3Ec__DisplayClass38_0_U3CAddSliderU3Eb__0_m0CE3BBB5E665B1DA0C1A3BDA8E627A6C77CA20F3 (void);
// 0x00000586 System.Void DebugUIBuilder/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_mB01C29C346234077CBCF5D492C168E8F58A0BB7A (void);
// 0x00000587 System.Void DebugUIBuilder/<>c__DisplayClass40_0::<AddToggle>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass40_0_U3CAddToggleU3Eb__0_mE1F178F830E6B71B8F71612362E3121C1B9A45A2 (void);
// 0x00000588 System.Void DebugUIBuilder/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_m6527E632BD200CEC2C12AA6E5775DD1A9B9385A2 (void);
// 0x00000589 System.Void DebugUIBuilder/<>c__DisplayClass41_0::<AddToggle>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass41_0_U3CAddToggleU3Eb__0_m5CADE8ED54BB65D80A8CB46587DA461303E926D0 (void);
// 0x0000058A System.Void DebugUIBuilder/<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_m17DDADD259C7C0F83EAC16A3DC2527C61619CAF0 (void);
// 0x0000058B System.Void DebugUIBuilder/<>c__DisplayClass42_0::<AddRadio>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass42_0_U3CAddRadioU3Eb__0_mC9894607A1275C248AA27CC402078C91704EBC0A (void);
// 0x0000058C System.Void HandedInputSelector::Start()
extern void HandedInputSelector_Start_m9F0E0845F78B86C943B82C6C00D00C2755379664 (void);
// 0x0000058D System.Void HandedInputSelector::Update()
extern void HandedInputSelector_Update_m4AD93E3F8125E142011798D2CABF248A3DFB73C6 (void);
// 0x0000058E System.Void HandedInputSelector::SetActiveController(OVRInput/Controller)
extern void HandedInputSelector_SetActiveController_m675CE04EEBD60BA97F10205CFCC8E0360A57640D (void);
// 0x0000058F System.Void HandedInputSelector::.ctor()
extern void HandedInputSelector__ctor_mFB9B45C64E23FAA0F3AC5FC734673E1A4EC7B6FD (void);
// 0x00000590 System.Void LaserPointer::set_laserBeamBehavior(LaserPointer/LaserBeamBehavior)
extern void LaserPointer_set_laserBeamBehavior_mAEDF3DABF1D4C13A02570DFB7539EA32D96C3EB7 (void);
// 0x00000591 LaserPointer/LaserBeamBehavior LaserPointer::get_laserBeamBehavior()
extern void LaserPointer_get_laserBeamBehavior_mC452C41CF65C2D1E3D13990D519BCE2B5459F4CE (void);
// 0x00000592 System.Void LaserPointer::Awake()
extern void LaserPointer_Awake_mB3F93300A58061D247AAD53BA961CA67148670CA (void);
// 0x00000593 System.Void LaserPointer::Start()
extern void LaserPointer_Start_mC859C4A4E40BCB5E1A39D8B6135D256178A1B0C1 (void);
// 0x00000594 System.Void LaserPointer::SetCursorStartDest(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LaserPointer_SetCursorStartDest_m58BE76CF5DC6A138D6F39812B510E58A8195CA18 (void);
// 0x00000595 System.Void LaserPointer::SetCursorRay(UnityEngine.Transform)
extern void LaserPointer_SetCursorRay_m73C5954CB43A8D590BF1FF9674846886C869E3CA (void);
// 0x00000596 System.Void LaserPointer::LateUpdate()
extern void LaserPointer_LateUpdate_m922E6CC571CF3BF292D1E4A89ABA3DE721940E40 (void);
// 0x00000597 System.Void LaserPointer::UpdateLaserBeam(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LaserPointer_UpdateLaserBeam_m2EC759248E0200A9EDC06D995886AED2C3BBE84D (void);
// 0x00000598 System.Void LaserPointer::OnDisable()
extern void LaserPointer_OnDisable_m386D5CB4ED7CC015BDD625F9BB173D48A44963BC (void);
// 0x00000599 System.Void LaserPointer::OnInputFocusLost()
extern void LaserPointer_OnInputFocusLost_m03F3165CDC0823985D85DBDECE16679C4A574EC1 (void);
// 0x0000059A System.Void LaserPointer::OnInputFocusAcquired()
extern void LaserPointer_OnInputFocusAcquired_m253C322D76D9D1A00FD1041F30DE52EC189857D8 (void);
// 0x0000059B System.Void LaserPointer::OnDestroy()
extern void LaserPointer_OnDestroy_mA7F7B1BDDB6897671BC633CF81F15F3C2AC8A27E (void);
// 0x0000059C System.Void LaserPointer::.ctor()
extern void LaserPointer__ctor_m9834E61B31A7B83B51FC2BEB7B9F59FCE781B66A (void);
// 0x0000059D System.Void CharacterCameraConstraint::.ctor()
extern void CharacterCameraConstraint__ctor_mB4775E6A3FD15170E7EDA1FF7083E59DC9D9356B (void);
// 0x0000059E System.Void CharacterCameraConstraint::Awake()
extern void CharacterCameraConstraint_Awake_m11B961F3351B1DCF59D2F9F25F6129261B996D8C (void);
// 0x0000059F System.Void CharacterCameraConstraint::OnEnable()
extern void CharacterCameraConstraint_OnEnable_mA51E71B397BB043BDCDBD57881301ACEDBB7A937 (void);
// 0x000005A0 System.Void CharacterCameraConstraint::OnDisable()
extern void CharacterCameraConstraint_OnDisable_m0C85B3913ABB8DD8EF6AB5A38003925365C97700 (void);
// 0x000005A1 System.Void CharacterCameraConstraint::CameraUpdate()
extern void CharacterCameraConstraint_CameraUpdate_m7D846796F0337138A375E39F69E31C68B0EBA41B (void);
// 0x000005A2 System.Boolean CharacterCameraConstraint::CheckCameraOverlapped()
extern void CharacterCameraConstraint_CheckCameraOverlapped_m7EF5BBE41ADD0F7610CE1CC6EE39CA631513C783 (void);
// 0x000005A3 System.Boolean CharacterCameraConstraint::CheckCameraNearClipping(System.Single&)
extern void CharacterCameraConstraint_CheckCameraNearClipping_m3BD4B953D24FBF24CAB9E86824E600EAFE549483 (void);
// 0x000005A4 System.Void LocomotionController::Start()
extern void LocomotionController_Start_mFF771607088B6AC1B9647FFD5262BD156C32990B (void);
// 0x000005A5 System.Void LocomotionController::.ctor()
extern void LocomotionController__ctor_m5AA73F4FF00769A834D5732FC4BD08BDD4634BDF (void);
// 0x000005A6 System.Void LocomotionTeleport::EnableMovement(System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void LocomotionTeleport_EnableMovement_m8ADC24447284A9EE7AEECCED581CC2B51326FDC9 (void);
// 0x000005A7 System.Void LocomotionTeleport::EnableRotation(System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void LocomotionTeleport_EnableRotation_m1B04E1D2444E02AC4407BEE5362EF08441C2AFA5 (void);
// 0x000005A8 LocomotionTeleport/States LocomotionTeleport::get_CurrentState()
extern void LocomotionTeleport_get_CurrentState_mCEDC22238BFC0EB60D9DE2616698C71D0CEF0BA8 (void);
// 0x000005A9 System.Void LocomotionTeleport::set_CurrentState(LocomotionTeleport/States)
extern void LocomotionTeleport_set_CurrentState_m6D752383FDB712A2347A0CAC2F7734E26274DA77 (void);
// 0x000005AA System.Void LocomotionTeleport::add_UpdateTeleportDestination(System.Action`4<System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Quaternion>>)
extern void LocomotionTeleport_add_UpdateTeleportDestination_m5CF34E190EF7950C68B07AA59440C8904B000EDA (void);
// 0x000005AB System.Void LocomotionTeleport::remove_UpdateTeleportDestination(System.Action`4<System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Quaternion>>)
extern void LocomotionTeleport_remove_UpdateTeleportDestination_m6D4B9E576EAFC2F0F8812B24237AE65840368C8D (void);
// 0x000005AC System.Void LocomotionTeleport::OnUpdateTeleportDestination(System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Quaternion>)
extern void LocomotionTeleport_OnUpdateTeleportDestination_m14192CB82A0C6B54908D63B87428912B984459D1 (void);
// 0x000005AD UnityEngine.Quaternion LocomotionTeleport::get_DestinationRotation()
extern void LocomotionTeleport_get_DestinationRotation_m713A8A847E529FFF3636C695730856FF0F591CC6 (void);
// 0x000005AE LocomotionController LocomotionTeleport::get_LocomotionController()
extern void LocomotionTeleport_get_LocomotionController_mED79293C6EA765335D25527FC7111EF971257498 (void);
// 0x000005AF System.Void LocomotionTeleport::set_LocomotionController(LocomotionController)
extern void LocomotionTeleport_set_LocomotionController_m460991F216978FB103CB533BF9DEF4BC93DA814B (void);
// 0x000005B0 System.Boolean LocomotionTeleport::AimCollisionTest(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.LayerMask,UnityEngine.RaycastHit&)
extern void LocomotionTeleport_AimCollisionTest_m230A548C1CE75228EFA5CA239FA7171FB0FB4D3E (void);
// 0x000005B1 System.Void LocomotionTeleport::LogState(System.String)
extern void LocomotionTeleport_LogState_m2272C15F12A05D8C031A50FB08AE122C1FCA2EA7 (void);
// 0x000005B2 System.Void LocomotionTeleport::CreateNewTeleportDestination()
extern void LocomotionTeleport_CreateNewTeleportDestination_m985E3B33C931F465655EF6156CC75DC3B7A1D99D (void);
// 0x000005B3 System.Void LocomotionTeleport::DeactivateDestination()
extern void LocomotionTeleport_DeactivateDestination_m4F8704E73CA946449DF98D49763F10EF733987A0 (void);
// 0x000005B4 System.Void LocomotionTeleport::RecycleTeleportDestination(TeleportDestination)
extern void LocomotionTeleport_RecycleTeleportDestination_m6DA8327336475283A325074C859CD4D281BA9879 (void);
// 0x000005B5 System.Void LocomotionTeleport::EnableMotion(System.Boolean,System.Boolean)
extern void LocomotionTeleport_EnableMotion_m9ACD9EF8F110F90CD996939A058242FF89BCEA22 (void);
// 0x000005B6 System.Void LocomotionTeleport::Awake()
extern void LocomotionTeleport_Awake_m9C80807870EFB91801F476BAE526C31D9E29E66E (void);
// 0x000005B7 System.Void LocomotionTeleport::OnEnable()
extern void LocomotionTeleport_OnEnable_mFCFB80918BF2D1AF964CB043299BBE69CC83FDB5 (void);
// 0x000005B8 System.Void LocomotionTeleport::OnDisable()
extern void LocomotionTeleport_OnDisable_mA887B447817410EA3AC80DF9C6B2390D0B26788D (void);
// 0x000005B9 System.Void LocomotionTeleport::add_EnterStateReady(System.Action)
extern void LocomotionTeleport_add_EnterStateReady_m694F17237FD2AEEDCADA5B5BF9D32F91E249BD39 (void);
// 0x000005BA System.Void LocomotionTeleport::remove_EnterStateReady(System.Action)
extern void LocomotionTeleport_remove_EnterStateReady_m02A5BF8752C71DC2A54CFA7325F737BC3B6099C1 (void);
// 0x000005BB System.Collections.IEnumerator LocomotionTeleport::ReadyStateCoroutine()
extern void LocomotionTeleport_ReadyStateCoroutine_mEF056F1254CED1ABD142E517BDEB2815AAE24A0C (void);
// 0x000005BC System.Void LocomotionTeleport::add_EnterStateAim(System.Action)
extern void LocomotionTeleport_add_EnterStateAim_m9EEEAB1100A4CC7635EF023A0078BCE95434C5A2 (void);
// 0x000005BD System.Void LocomotionTeleport::remove_EnterStateAim(System.Action)
extern void LocomotionTeleport_remove_EnterStateAim_mBBF9AA5970D87E22D59FC766F8278643C61B6250 (void);
// 0x000005BE System.Void LocomotionTeleport::add_UpdateAimData(System.Action`1<LocomotionTeleport/AimData>)
extern void LocomotionTeleport_add_UpdateAimData_mF17892665B70341AB40666AA39187787B9E060E5 (void);
// 0x000005BF System.Void LocomotionTeleport::remove_UpdateAimData(System.Action`1<LocomotionTeleport/AimData>)
extern void LocomotionTeleport_remove_UpdateAimData_mBFAF399B9B0200FDC43ADEF36B4123F60941D24C (void);
// 0x000005C0 System.Void LocomotionTeleport::OnUpdateAimData(LocomotionTeleport/AimData)
extern void LocomotionTeleport_OnUpdateAimData_m76ED9D517FF45B8A7CE7757BD0E8D147892F90A8 (void);
// 0x000005C1 System.Void LocomotionTeleport::add_ExitStateAim(System.Action)
extern void LocomotionTeleport_add_ExitStateAim_m93E127BBB502440C22348EDF011EF6ED12E1E607 (void);
// 0x000005C2 System.Void LocomotionTeleport::remove_ExitStateAim(System.Action)
extern void LocomotionTeleport_remove_ExitStateAim_m86CEE4BAF96D0CDAB9CEF69231DDB1A3FCE7964F (void);
// 0x000005C3 System.Collections.IEnumerator LocomotionTeleport::AimStateCoroutine()
extern void LocomotionTeleport_AimStateCoroutine_mC98C40FA612A3DC181B2A622B2AFC2B78C72D182 (void);
// 0x000005C4 System.Void LocomotionTeleport::add_EnterStateCancelAim(System.Action)
extern void LocomotionTeleport_add_EnterStateCancelAim_m98AE021C2A1274CE289ADBDC891DCC027AB06668 (void);
// 0x000005C5 System.Void LocomotionTeleport::remove_EnterStateCancelAim(System.Action)
extern void LocomotionTeleport_remove_EnterStateCancelAim_mF547CBA5F10D4A7374318065008202266E14E729 (void);
// 0x000005C6 System.Collections.IEnumerator LocomotionTeleport::CancelAimStateCoroutine()
extern void LocomotionTeleport_CancelAimStateCoroutine_m89E30D85AE2200D46C6B6CEC487CA86309336DFE (void);
// 0x000005C7 System.Void LocomotionTeleport::add_EnterStatePreTeleport(System.Action)
extern void LocomotionTeleport_add_EnterStatePreTeleport_m5436C07729E835E670F3D438A7EAC8743646071F (void);
// 0x000005C8 System.Void LocomotionTeleport::remove_EnterStatePreTeleport(System.Action)
extern void LocomotionTeleport_remove_EnterStatePreTeleport_m1BB7C07D45DA26BA3FC294749B845A6F04E9035D (void);
// 0x000005C9 System.Collections.IEnumerator LocomotionTeleport::PreTeleportStateCoroutine()
extern void LocomotionTeleport_PreTeleportStateCoroutine_m4D4467A0B6570EEE09C5366CF0B443665718D27C (void);
// 0x000005CA System.Void LocomotionTeleport::add_EnterStateCancelTeleport(System.Action)
extern void LocomotionTeleport_add_EnterStateCancelTeleport_mE447D28CF848635B7BF086DA4CC7592E174225F5 (void);
// 0x000005CB System.Void LocomotionTeleport::remove_EnterStateCancelTeleport(System.Action)
extern void LocomotionTeleport_remove_EnterStateCancelTeleport_mD9C6A70C33006402CC14DF75C06AA68EE0CB38B7 (void);
// 0x000005CC System.Collections.IEnumerator LocomotionTeleport::CancelTeleportStateCoroutine()
extern void LocomotionTeleport_CancelTeleportStateCoroutine_m376EE91D4337D6E0DFE214FAF8277E4AD3A80F09 (void);
// 0x000005CD System.Void LocomotionTeleport::add_EnterStateTeleporting(System.Action)
extern void LocomotionTeleport_add_EnterStateTeleporting_m2939235BCF9A6D936A57686136AAF38FA38F7AE9 (void);
// 0x000005CE System.Void LocomotionTeleport::remove_EnterStateTeleporting(System.Action)
extern void LocomotionTeleport_remove_EnterStateTeleporting_mE99129EE268F102CFBF05222E234285FD41EA602 (void);
// 0x000005CF System.Collections.IEnumerator LocomotionTeleport::TeleportingStateCoroutine()
extern void LocomotionTeleport_TeleportingStateCoroutine_m29325603C91C678B7035FDA9AA324D657149A1C7 (void);
// 0x000005D0 System.Void LocomotionTeleport::add_EnterStatePostTeleport(System.Action)
extern void LocomotionTeleport_add_EnterStatePostTeleport_m6352237DD5B10EB9C90693107DD8C33A6CE538FA (void);
// 0x000005D1 System.Void LocomotionTeleport::remove_EnterStatePostTeleport(System.Action)
extern void LocomotionTeleport_remove_EnterStatePostTeleport_m9E53B5CBF3804AC61F9E38AEA246E2599341373D (void);
// 0x000005D2 System.Collections.IEnumerator LocomotionTeleport::PostTeleportStateCoroutine()
extern void LocomotionTeleport_PostTeleportStateCoroutine_m55EFBB7452FA0FEF1F0C025CE559516ED977AA96 (void);
// 0x000005D3 System.Void LocomotionTeleport::add_Teleported(System.Action`3<UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion>)
extern void LocomotionTeleport_add_Teleported_m8CAC3A947E40F5C79B38D5F059E94B75A6CE623A (void);
// 0x000005D4 System.Void LocomotionTeleport::remove_Teleported(System.Action`3<UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion>)
extern void LocomotionTeleport_remove_Teleported_m27E84C43ACE915F7D5ADA07A974F7A76530BBA3E (void);
// 0x000005D5 System.Void LocomotionTeleport::DoTeleport()
extern void LocomotionTeleport_DoTeleport_m1477EB38D2FD6E2AD4A7553EB3237B18D40C0B0C (void);
// 0x000005D6 UnityEngine.Vector3 LocomotionTeleport::GetCharacterPosition()
extern void LocomotionTeleport_GetCharacterPosition_m30F7EB551F5787D2489CE2716284B848D2C11D26 (void);
// 0x000005D7 UnityEngine.Quaternion LocomotionTeleport::GetHeadRotationY()
extern void LocomotionTeleport_GetHeadRotationY_m0F2078ED6649DCED3A8022BA2CD27BF02378639B (void);
// 0x000005D8 System.Void LocomotionTeleport::DoWarp(UnityEngine.Vector3,System.Single)
extern void LocomotionTeleport_DoWarp_m2195B0DF360E9B04AD486855F66560DFC22EEB76 (void);
// 0x000005D9 System.Void LocomotionTeleport::.ctor()
extern void LocomotionTeleport__ctor_m9AFDEE21E452CC311C904A90D753CB815495A59C (void);
// 0x000005DA System.Void LocomotionTeleport/AimData::.ctor()
extern void AimData__ctor_mDEB139E72E07987C9EC05AFFACE3A5C393EE56B8 (void);
// 0x000005DB System.Collections.Generic.List`1<UnityEngine.Vector3> LocomotionTeleport/AimData::get_Points()
extern void AimData_get_Points_m3169977C66C0D412F32CECB2B7FD471F65A191B2 (void);
// 0x000005DC System.Void LocomotionTeleport/AimData::set_Points(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void AimData_set_Points_mDECAE723C539EE8F8513307662F0CCF743ED7BF4 (void);
// 0x000005DD System.Void LocomotionTeleport/AimData::Reset()
extern void AimData_Reset_mA22ED9AA08B2642374D760A48D198B98DA3D49E7 (void);
// 0x000005DE System.Void LocomotionTeleport/<ReadyStateCoroutine>d__52::.ctor(System.Int32)
extern void U3CReadyStateCoroutineU3Ed__52__ctor_m8DE1982B50214C0C02CF0920B2BD0C8503DD7BC6 (void);
// 0x000005DF System.Void LocomotionTeleport/<ReadyStateCoroutine>d__52::System.IDisposable.Dispose()
extern void U3CReadyStateCoroutineU3Ed__52_System_IDisposable_Dispose_m281149F5ECF711FBFD9262A09DA4339B2DB2F7FC (void);
// 0x000005E0 System.Boolean LocomotionTeleport/<ReadyStateCoroutine>d__52::MoveNext()
extern void U3CReadyStateCoroutineU3Ed__52_MoveNext_mA3BE219EA412B9FF5022A2D51A8C98962618203D (void);
// 0x000005E1 System.Object LocomotionTeleport/<ReadyStateCoroutine>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReadyStateCoroutineU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF049A6B9DF16EBDE92993801D9D70B3DF6724BB3 (void);
// 0x000005E2 System.Void LocomotionTeleport/<ReadyStateCoroutine>d__52::System.Collections.IEnumerator.Reset()
extern void U3CReadyStateCoroutineU3Ed__52_System_Collections_IEnumerator_Reset_m57879FEF2CA0007DFA14EE7C948F128236C49F20 (void);
// 0x000005E3 System.Object LocomotionTeleport/<ReadyStateCoroutine>d__52::System.Collections.IEnumerator.get_Current()
extern void U3CReadyStateCoroutineU3Ed__52_System_Collections_IEnumerator_get_Current_m8CFB46F37C1A1C464F6C813BD5B61E5E4D60D65B (void);
// 0x000005E4 System.Void LocomotionTeleport/<AimStateCoroutine>d__64::.ctor(System.Int32)
extern void U3CAimStateCoroutineU3Ed__64__ctor_m58306B48D136424B1A697461F28CCD0112C0C386 (void);
// 0x000005E5 System.Void LocomotionTeleport/<AimStateCoroutine>d__64::System.IDisposable.Dispose()
extern void U3CAimStateCoroutineU3Ed__64_System_IDisposable_Dispose_m1F7871364B779E25C381A3A485C5E8360E58BA33 (void);
// 0x000005E6 System.Boolean LocomotionTeleport/<AimStateCoroutine>d__64::MoveNext()
extern void U3CAimStateCoroutineU3Ed__64_MoveNext_mF0CCCD83D0E4DDD4864A079C21C7423C75D51811 (void);
// 0x000005E7 System.Object LocomotionTeleport/<AimStateCoroutine>d__64::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAimStateCoroutineU3Ed__64_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCEA3E5325E391A08ECF74573A3F1DA3F202A609F (void);
// 0x000005E8 System.Void LocomotionTeleport/<AimStateCoroutine>d__64::System.Collections.IEnumerator.Reset()
extern void U3CAimStateCoroutineU3Ed__64_System_Collections_IEnumerator_Reset_m430B643E53905F6ECA3AFC5D72DF1F1C7431E345 (void);
// 0x000005E9 System.Object LocomotionTeleport/<AimStateCoroutine>d__64::System.Collections.IEnumerator.get_Current()
extern void U3CAimStateCoroutineU3Ed__64_System_Collections_IEnumerator_get_Current_m800D35A57B1A8C7F13AB42BA27EC5C8C763D7EA8 (void);
// 0x000005EA System.Void LocomotionTeleport/<CancelAimStateCoroutine>d__68::.ctor(System.Int32)
extern void U3CCancelAimStateCoroutineU3Ed__68__ctor_m1DDF58501076B8077E3441AF06D8A6A7F13B6747 (void);
// 0x000005EB System.Void LocomotionTeleport/<CancelAimStateCoroutine>d__68::System.IDisposable.Dispose()
extern void U3CCancelAimStateCoroutineU3Ed__68_System_IDisposable_Dispose_mD7599C67A3F973C9B291989150D3639890B5BD84 (void);
// 0x000005EC System.Boolean LocomotionTeleport/<CancelAimStateCoroutine>d__68::MoveNext()
extern void U3CCancelAimStateCoroutineU3Ed__68_MoveNext_m0387258B4B35F7D9F037F52A915D247B2EBDF011 (void);
// 0x000005ED System.Object LocomotionTeleport/<CancelAimStateCoroutine>d__68::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCancelAimStateCoroutineU3Ed__68_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m422572D5779C1216188EDB8521144B4BD48335B0 (void);
// 0x000005EE System.Void LocomotionTeleport/<CancelAimStateCoroutine>d__68::System.Collections.IEnumerator.Reset()
extern void U3CCancelAimStateCoroutineU3Ed__68_System_Collections_IEnumerator_Reset_m537442ED372D14EA04A5967D6B7078DC83C32107 (void);
// 0x000005EF System.Object LocomotionTeleport/<CancelAimStateCoroutine>d__68::System.Collections.IEnumerator.get_Current()
extern void U3CCancelAimStateCoroutineU3Ed__68_System_Collections_IEnumerator_get_Current_mCA030531FABF8A358DCC082E6F56B591B13DC194 (void);
// 0x000005F0 System.Void LocomotionTeleport/<PreTeleportStateCoroutine>d__72::.ctor(System.Int32)
extern void U3CPreTeleportStateCoroutineU3Ed__72__ctor_m9FCD57D81CBFA88D2D1B4A777B29A236034CAF8E (void);
// 0x000005F1 System.Void LocomotionTeleport/<PreTeleportStateCoroutine>d__72::System.IDisposable.Dispose()
extern void U3CPreTeleportStateCoroutineU3Ed__72_System_IDisposable_Dispose_mBD12FF7C2306AAEF739BCCFF39B2C1F2C42D6ECD (void);
// 0x000005F2 System.Boolean LocomotionTeleport/<PreTeleportStateCoroutine>d__72::MoveNext()
extern void U3CPreTeleportStateCoroutineU3Ed__72_MoveNext_mAB1EF7B08ADA03C55A05F1ADC5EF0628EAB6858B (void);
// 0x000005F3 System.Object LocomotionTeleport/<PreTeleportStateCoroutine>d__72::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m842BD3A40FAB08CE9ED7384F00A4035A6A8A6006 (void);
// 0x000005F4 System.Void LocomotionTeleport/<PreTeleportStateCoroutine>d__72::System.Collections.IEnumerator.Reset()
extern void U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_IEnumerator_Reset_m8AA3FEA6ABDAC9A68A96DD7A90B1190B504ED09A (void);
// 0x000005F5 System.Object LocomotionTeleport/<PreTeleportStateCoroutine>d__72::System.Collections.IEnumerator.get_Current()
extern void U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_IEnumerator_get_Current_m1FC972F30DE3D7BAB7166BB72A107D0FD9E1E49E (void);
// 0x000005F6 System.Void LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::.ctor(System.Int32)
extern void U3CCancelTeleportStateCoroutineU3Ed__76__ctor_m6BBBA231C275F8B93FEFD97A2279B18FED30AFFF (void);
// 0x000005F7 System.Void LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::System.IDisposable.Dispose()
extern void U3CCancelTeleportStateCoroutineU3Ed__76_System_IDisposable_Dispose_mBB05318F123DC133C386E503BD026985038DF644 (void);
// 0x000005F8 System.Boolean LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::MoveNext()
extern void U3CCancelTeleportStateCoroutineU3Ed__76_MoveNext_m02B6031D1BB5B1B0662A91F6E9B78231CDC0CBAE (void);
// 0x000005F9 System.Object LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B45046196D18E8F11B7FFCA3836CDD0E069BFB6 (void);
// 0x000005FA System.Void LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::System.Collections.IEnumerator.Reset()
extern void U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_IEnumerator_Reset_m2FE09112ABFFCA00C122EFD44AE58DA551BD9817 (void);
// 0x000005FB System.Object LocomotionTeleport/<CancelTeleportStateCoroutine>d__76::System.Collections.IEnumerator.get_Current()
extern void U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_IEnumerator_get_Current_m888B89F4C478800F9FACD7F1EC137820F5094C57 (void);
// 0x000005FC System.Void LocomotionTeleport/<TeleportingStateCoroutine>d__80::.ctor(System.Int32)
extern void U3CTeleportingStateCoroutineU3Ed__80__ctor_m1A3D09A30548B419B3E2083990FF4DA65B66A1C2 (void);
// 0x000005FD System.Void LocomotionTeleport/<TeleportingStateCoroutine>d__80::System.IDisposable.Dispose()
extern void U3CTeleportingStateCoroutineU3Ed__80_System_IDisposable_Dispose_m52588F27903FD706F72A84E217008CDCEA1CE19C (void);
// 0x000005FE System.Boolean LocomotionTeleport/<TeleportingStateCoroutine>d__80::MoveNext()
extern void U3CTeleportingStateCoroutineU3Ed__80_MoveNext_m01613F55E14EE215BD297AA0E61DFD1B400B786E (void);
// 0x000005FF System.Object LocomotionTeleport/<TeleportingStateCoroutine>d__80::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTeleportingStateCoroutineU3Ed__80_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m59D9DB94A89EE8EC99B646C00995380363E17BA6 (void);
// 0x00000600 System.Void LocomotionTeleport/<TeleportingStateCoroutine>d__80::System.Collections.IEnumerator.Reset()
extern void U3CTeleportingStateCoroutineU3Ed__80_System_Collections_IEnumerator_Reset_m87DE12198C0424F46FA9337E312A56AC85B48F34 (void);
// 0x00000601 System.Object LocomotionTeleport/<TeleportingStateCoroutine>d__80::System.Collections.IEnumerator.get_Current()
extern void U3CTeleportingStateCoroutineU3Ed__80_System_Collections_IEnumerator_get_Current_mD05407DB2BA285B804EC4E4DF432340D1424DF0D (void);
// 0x00000602 System.Void LocomotionTeleport/<PostTeleportStateCoroutine>d__84::.ctor(System.Int32)
extern void U3CPostTeleportStateCoroutineU3Ed__84__ctor_m73CB3DEE73F665F765BA80FDF21B564AE8F22CAB (void);
// 0x00000603 System.Void LocomotionTeleport/<PostTeleportStateCoroutine>d__84::System.IDisposable.Dispose()
extern void U3CPostTeleportStateCoroutineU3Ed__84_System_IDisposable_Dispose_mDDC6BB7ECF31923D9B8AA237DFDC162642BE3F5E (void);
// 0x00000604 System.Boolean LocomotionTeleport/<PostTeleportStateCoroutine>d__84::MoveNext()
extern void U3CPostTeleportStateCoroutineU3Ed__84_MoveNext_m5428FACDB4F6A7C6B9C2966106C9CA97A70CAFEC (void);
// 0x00000605 System.Object LocomotionTeleport/<PostTeleportStateCoroutine>d__84::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50185F8995916556B5531091EAC1D796207EE4D6 (void);
// 0x00000606 System.Void LocomotionTeleport/<PostTeleportStateCoroutine>d__84::System.Collections.IEnumerator.Reset()
extern void U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_IEnumerator_Reset_m8E16BCCA8A3B018A2CAC418CDAA14DB7C9B3D897 (void);
// 0x00000607 System.Object LocomotionTeleport/<PostTeleportStateCoroutine>d__84::System.Collections.IEnumerator.get_Current()
extern void U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_IEnumerator_get_Current_mA97CEA8F012879C52B5756B734B3AA464439F83F (void);
// 0x00000608 System.Void SimpleCapsuleWithStickMovement::add_CameraUpdated(System.Action)
extern void SimpleCapsuleWithStickMovement_add_CameraUpdated_m62D68F4E8BD481D3679FDB0979AE6CAC8DAFAC46 (void);
// 0x00000609 System.Void SimpleCapsuleWithStickMovement::remove_CameraUpdated(System.Action)
extern void SimpleCapsuleWithStickMovement_remove_CameraUpdated_mA39A1F5D5F0CA44D0B6EB9E9208FD39B7D0D3E06 (void);
// 0x0000060A System.Void SimpleCapsuleWithStickMovement::add_PreCharacterMove(System.Action)
extern void SimpleCapsuleWithStickMovement_add_PreCharacterMove_m682E5AAFBCDF0BF832F5447D69CC73DB39C096D2 (void);
// 0x0000060B System.Void SimpleCapsuleWithStickMovement::remove_PreCharacterMove(System.Action)
extern void SimpleCapsuleWithStickMovement_remove_PreCharacterMove_m57CBA41BD4CD9C1EA3864D4AD464C1255EA80B22 (void);
// 0x0000060C System.Void SimpleCapsuleWithStickMovement::Awake()
extern void SimpleCapsuleWithStickMovement_Awake_m0C068EE45D2BEDB94F8D1D289C04BF7480443D2E (void);
// 0x0000060D System.Void SimpleCapsuleWithStickMovement::Start()
extern void SimpleCapsuleWithStickMovement_Start_m517C18566428FBB777185B97264539BE48D5A0F5 (void);
// 0x0000060E System.Void SimpleCapsuleWithStickMovement::FixedUpdate()
extern void SimpleCapsuleWithStickMovement_FixedUpdate_mE88BF998394A1EB4FCEBC8A04875FA26D3D6C1BD (void);
// 0x0000060F System.Void SimpleCapsuleWithStickMovement::RotatePlayerToHMD()
extern void SimpleCapsuleWithStickMovement_RotatePlayerToHMD_m762E683133240C6E945140A357920BC6ACB7D116 (void);
// 0x00000610 System.Void SimpleCapsuleWithStickMovement::StickMovement()
extern void SimpleCapsuleWithStickMovement_StickMovement_m4550B6DF9A0272F282866822CC8F0E8AE32E1893 (void);
// 0x00000611 System.Void SimpleCapsuleWithStickMovement::SnapTurn()
extern void SimpleCapsuleWithStickMovement_SnapTurn_m6BA0E49AA935C048AC9E8D59C90CBF50E087B396 (void);
// 0x00000612 System.Void SimpleCapsuleWithStickMovement::.ctor()
extern void SimpleCapsuleWithStickMovement__ctor_m96AC7462B0905E8D35D74FCF0C54E3F96DDB68B9 (void);
// 0x00000613 System.Void TeleportAimHandler::OnEnable()
extern void TeleportAimHandler_OnEnable_mACD1F9393ADE87B704B4A4EAA298FBA36EDAC843 (void);
// 0x00000614 System.Void TeleportAimHandler::OnDisable()
extern void TeleportAimHandler_OnDisable_m61175BD9492F8C39A0BC8573FCAB06D008DF3961 (void);
// 0x00000615 System.Void TeleportAimHandler::GetPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
// 0x00000616 System.Void TeleportAimHandler::.ctor()
extern void TeleportAimHandler__ctor_m1763BE837F8F8FA3B3C34EBA78BE0D593F50A5E7 (void);
// 0x00000617 System.Void TeleportAimHandlerLaser::GetPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void TeleportAimHandlerLaser_GetPoints_m5DE7FA7926409D22B8F5E329EC2A4935B95EC3A0 (void);
// 0x00000618 System.Void TeleportAimHandlerLaser::.ctor()
extern void TeleportAimHandlerLaser__ctor_m0D6E559AC138E39BBBF5E12AD223F13571A32D9C (void);
// 0x00000619 System.Void TeleportAimHandlerParabolic::GetPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void TeleportAimHandlerParabolic_GetPoints_m2AF261807D5A7F6FE7E14F20B473D985A93DD1C3 (void);
// 0x0000061A System.Void TeleportAimHandlerParabolic::.ctor()
extern void TeleportAimHandlerParabolic__ctor_mAB05081CBCE394C762E8B8F4F6CA12D3E9846A32 (void);
// 0x0000061B System.Void TeleportAimVisualLaser::.ctor()
extern void TeleportAimVisualLaser__ctor_mC232B12213106D0E44DD02D7EE16080182C5FCAC (void);
// 0x0000061C System.Void TeleportAimVisualLaser::EnterAimState()
extern void TeleportAimVisualLaser_EnterAimState_mFBE7E0BB8190B822638EB470B2559E9A52B29832 (void);
// 0x0000061D System.Void TeleportAimVisualLaser::ExitAimState()
extern void TeleportAimVisualLaser_ExitAimState_m7076D541D8CFF0F24A2A19CBFE8C0EED448773D9 (void);
// 0x0000061E System.Void TeleportAimVisualLaser::Awake()
extern void TeleportAimVisualLaser_Awake_mFC14E2C04A8ACFACA52131512BAE0FCDB425754A (void);
// 0x0000061F System.Void TeleportAimVisualLaser::AddEventHandlers()
extern void TeleportAimVisualLaser_AddEventHandlers_m5E7A188CF65F15DEEE335F3B40C261EF1D1E0D10 (void);
// 0x00000620 System.Void TeleportAimVisualLaser::RemoveEventHandlers()
extern void TeleportAimVisualLaser_RemoveEventHandlers_mAEC2273E026F953D7B71945590F0E7ED65539415 (void);
// 0x00000621 System.Void TeleportAimVisualLaser::UpdateAimData(LocomotionTeleport/AimData)
extern void TeleportAimVisualLaser_UpdateAimData_m8060F21ACBE191832A7247F4D750DF9A5A7C9984 (void);
// 0x00000622 System.Boolean TeleportDestination::get_IsValidDestination()
extern void TeleportDestination_get_IsValidDestination_m4A83CF1ABD625233373782FABAC9AC210A5FD151 (void);
// 0x00000623 System.Void TeleportDestination::set_IsValidDestination(System.Boolean)
extern void TeleportDestination_set_IsValidDestination_m2A37F62F1E80ECB2FDE63D42471514A26B0ED0BA (void);
// 0x00000624 System.Void TeleportDestination::.ctor()
extern void TeleportDestination__ctor_m01B9174443C128BB3639D39F0D2EBD7283DEB972 (void);
// 0x00000625 System.Void TeleportDestination::OnEnable()
extern void TeleportDestination_OnEnable_mFB65AD58BD57263B4D14E91B6FA583DA9607EB91 (void);
// 0x00000626 System.Void TeleportDestination::TryDisableEventHandlers()
extern void TeleportDestination_TryDisableEventHandlers_m450B9F64A3EAB6059B85B7573377A574606A9062 (void);
// 0x00000627 System.Void TeleportDestination::OnDisable()
extern void TeleportDestination_OnDisable_m0D75681D1CD83894438624CD3A7878C95904F63F (void);
// 0x00000628 System.Void TeleportDestination::add_Deactivated(System.Action`1<TeleportDestination>)
extern void TeleportDestination_add_Deactivated_m63745B4674198FDFA8D3CB45587CA14132840F9A (void);
// 0x00000629 System.Void TeleportDestination::remove_Deactivated(System.Action`1<TeleportDestination>)
extern void TeleportDestination_remove_Deactivated_mE2D6A7DCD9734262F34A8935272156A4B029A83A (void);
// 0x0000062A System.Void TeleportDestination::OnDeactivated()
extern void TeleportDestination_OnDeactivated_mA84FB07520E0C6EDA28F13A6349B994236BF8B46 (void);
// 0x0000062B System.Void TeleportDestination::Recycle()
extern void TeleportDestination_Recycle_m49B5F4B6776D55D76519AFA221B2881A51FED91E (void);
// 0x0000062C System.Void TeleportDestination::UpdateTeleportDestination(System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Quaternion>)
extern void TeleportDestination_UpdateTeleportDestination_m3FF1158B3A2BB73D914DC806E094D20F93A674B3 (void);
// 0x0000062D System.Void TeleportInputHandler::.ctor()
extern void TeleportInputHandler__ctor_m54EE60B8D0F7935760E81AB820D4570094939F71 (void);
// 0x0000062E System.Void TeleportInputHandler::AddEventHandlers()
extern void TeleportInputHandler_AddEventHandlers_m4609349778D031303E2767296E2B7E48E5E20FD2 (void);
// 0x0000062F System.Void TeleportInputHandler::RemoveEventHandlers()
extern void TeleportInputHandler_RemoveEventHandlers_m739F90CC264BE61AC9316C2B1645BFE8BCBA0555 (void);
// 0x00000630 System.Collections.IEnumerator TeleportInputHandler::TeleportReadyCoroutine()
extern void TeleportInputHandler_TeleportReadyCoroutine_m579E5F381DEA656B2A90444ADFA6DB5CB05BCD50 (void);
// 0x00000631 System.Collections.IEnumerator TeleportInputHandler::TeleportAimCoroutine()
extern void TeleportInputHandler_TeleportAimCoroutine_m82502080BCE2BD26E5BEBD89D54C3E57A4780B07 (void);
// 0x00000632 LocomotionTeleport/TeleportIntentions TeleportInputHandler::GetIntention()
// 0x00000633 System.Void TeleportInputHandler::GetAimData(UnityEngine.Ray&)
// 0x00000634 System.Void TeleportInputHandler::<.ctor>b__2_0()
extern void TeleportInputHandler_U3C_ctorU3Eb__2_0_m919CD5070F20E3A9E28CB8A95C9C9374663C52A1 (void);
// 0x00000635 System.Void TeleportInputHandler::<.ctor>b__2_1()
extern void TeleportInputHandler_U3C_ctorU3Eb__2_1_m2CE609639EC53A6A19E043A2E053F30FAE77D486 (void);
// 0x00000636 System.Void TeleportInputHandler/<TeleportReadyCoroutine>d__5::.ctor(System.Int32)
extern void U3CTeleportReadyCoroutineU3Ed__5__ctor_m7C396BD2404ABCBF1CF83320ACF13FA1B621B4FB (void);
// 0x00000637 System.Void TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.IDisposable.Dispose()
extern void U3CTeleportReadyCoroutineU3Ed__5_System_IDisposable_Dispose_mA2DFF1E0C765EA7B2F4BA2E1EF7721049E8EA046 (void);
// 0x00000638 System.Boolean TeleportInputHandler/<TeleportReadyCoroutine>d__5::MoveNext()
extern void U3CTeleportReadyCoroutineU3Ed__5_MoveNext_m5A64679B873617863C4B38AA8008C8CC1E564029 (void);
// 0x00000639 System.Object TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTeleportReadyCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41A381D2B7B87AD5810E05A3C55A66FD7D8CADA2 (void);
// 0x0000063A System.Void TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.Collections.IEnumerator.Reset()
extern void U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m12F830ADD072802D84BE4B3231F43A12FA4F8E73 (void);
// 0x0000063B System.Object TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mF8243C3A677F944C06A59D21D17CC3AF8DD1B594 (void);
// 0x0000063C System.Void TeleportInputHandler/<TeleportAimCoroutine>d__6::.ctor(System.Int32)
extern void U3CTeleportAimCoroutineU3Ed__6__ctor_mE2F9BDC7C3225737B5ADD2AD7AAF0E449CFADE47 (void);
// 0x0000063D System.Void TeleportInputHandler/<TeleportAimCoroutine>d__6::System.IDisposable.Dispose()
extern void U3CTeleportAimCoroutineU3Ed__6_System_IDisposable_Dispose_m443089F5C3CB24CF2E58C43B6D940BB6A97E7059 (void);
// 0x0000063E System.Boolean TeleportInputHandler/<TeleportAimCoroutine>d__6::MoveNext()
extern void U3CTeleportAimCoroutineU3Ed__6_MoveNext_m3D532A7ED8718469618989D091672C1E1E833FA0 (void);
// 0x0000063F System.Object TeleportInputHandler/<TeleportAimCoroutine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTeleportAimCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02583125A115ADE4CC8896C0108B7A2ADD9FCAFE (void);
// 0x00000640 System.Void TeleportInputHandler/<TeleportAimCoroutine>d__6::System.Collections.IEnumerator.Reset()
extern void U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mE894BDC51B100595A10489704983F8922185CCF8 (void);
// 0x00000641 System.Object TeleportInputHandler/<TeleportAimCoroutine>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m28CB29942297ECF9C327DDDAB3D53AF8A77ABF05 (void);
// 0x00000642 UnityEngine.Transform TeleportInputHandlerHMD::get_Pointer()
extern void TeleportInputHandlerHMD_get_Pointer_mCBC08A1A1B5D076AF31755E7771F3000034939FF (void);
// 0x00000643 System.Void TeleportInputHandlerHMD::set_Pointer(UnityEngine.Transform)
extern void TeleportInputHandlerHMD_set_Pointer_m5F8552C6C93A90A8BA5E3473532D687559EB2E9D (void);
// 0x00000644 LocomotionTeleport/TeleportIntentions TeleportInputHandlerHMD::GetIntention()
extern void TeleportInputHandlerHMD_GetIntention_m474B95CF0690765C9B5C7DCC8381385E9BF5FC6A (void);
// 0x00000645 System.Void TeleportInputHandlerHMD::GetAimData(UnityEngine.Ray&)
extern void TeleportInputHandlerHMD_GetAimData_mBE4EAEA4E4AD483994731D9FE82D0491E131AA37 (void);
// 0x00000646 System.Void TeleportInputHandlerHMD::.ctor()
extern void TeleportInputHandlerHMD__ctor_m3E2641D8040D2F44A53E2A184ECDDCA1ED6153C9 (void);
// 0x00000647 System.Void TeleportInputHandlerTouch::Start()
extern void TeleportInputHandlerTouch_Start_m14F2576DDA7F7AA52A05AD9456703D1FF68F2387 (void);
// 0x00000648 LocomotionTeleport/TeleportIntentions TeleportInputHandlerTouch::GetIntention()
extern void TeleportInputHandlerTouch_GetIntention_m23F1835AE8064C76A1C2F9631B343C721DB6A993 (void);
// 0x00000649 System.Void TeleportInputHandlerTouch::GetAimData(UnityEngine.Ray&)
extern void TeleportInputHandlerTouch_GetAimData_m4155B513561DCB66F1180407A5AB8B2B5936E91B (void);
// 0x0000064A System.Void TeleportInputHandlerTouch::.ctor()
extern void TeleportInputHandlerTouch__ctor_mE4E2BF438B9790F07ECA23A6D77748104C707E31 (void);
// 0x0000064B System.Void TeleportOrientationHandler::.ctor()
extern void TeleportOrientationHandler__ctor_m21A7BA2A330541DFEB4ADF80A53BE1046E65C10A (void);
// 0x0000064C System.Void TeleportOrientationHandler::UpdateAimData(LocomotionTeleport/AimData)
extern void TeleportOrientationHandler_UpdateAimData_m9B52AC667A8BF78D509B49403962756A1714DD9B (void);
// 0x0000064D System.Void TeleportOrientationHandler::AddEventHandlers()
extern void TeleportOrientationHandler_AddEventHandlers_m2507C63FDF190C3BC81D2479EA8D488F32C6D4C6 (void);
// 0x0000064E System.Void TeleportOrientationHandler::RemoveEventHandlers()
extern void TeleportOrientationHandler_RemoveEventHandlers_mB46ADA4F854BD6919ED74D88F027AB423FF522BF (void);
// 0x0000064F System.Collections.IEnumerator TeleportOrientationHandler::UpdateOrientationCoroutine()
extern void TeleportOrientationHandler_UpdateOrientationCoroutine_m0CFA18DF96F986F14A52E6CC7EEFC90FA0C73B7B (void);
// 0x00000650 System.Void TeleportOrientationHandler::InitializeTeleportDestination()
// 0x00000651 System.Void TeleportOrientationHandler::UpdateTeleportDestination()
// 0x00000652 UnityEngine.Quaternion TeleportOrientationHandler::GetLandingOrientation(TeleportOrientationHandler/OrientationModes,UnityEngine.Quaternion)
extern void TeleportOrientationHandler_GetLandingOrientation_m4CC4D8950AFCDAA13EB2244AD987B3225FEFAABA (void);
// 0x00000653 System.Void TeleportOrientationHandler::<.ctor>b__3_0()
extern void TeleportOrientationHandler_U3C_ctorU3Eb__3_0_m3CA25FA37637FD4D799437E6037E2C278EC4B9A0 (void);
// 0x00000654 System.Void TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::.ctor(System.Int32)
extern void U3CUpdateOrientationCoroutineU3Ed__7__ctor_m5DFE7ADCE7BF42B372F2097C1C019F301BBE3DFE (void);
// 0x00000655 System.Void TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.IDisposable.Dispose()
extern void U3CUpdateOrientationCoroutineU3Ed__7_System_IDisposable_Dispose_m159520706F62855F2F55F4686A47A89D093AD2B0 (void);
// 0x00000656 System.Boolean TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::MoveNext()
extern void U3CUpdateOrientationCoroutineU3Ed__7_MoveNext_m2D766D50632FD169A27EF1E4EE39B50AAEF4512C (void);
// 0x00000657 System.Object TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87C4EB920BA4BF331493243117342D5E99905F1C (void);
// 0x00000658 System.Void TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m8957A7F5F8D1D9C630D84F24A7C2B66642CA9B95 (void);
// 0x00000659 System.Object TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mDB4E059717A313A10584DB05F88E813A45D9A606 (void);
// 0x0000065A System.Void TeleportOrientationHandler360::InitializeTeleportDestination()
extern void TeleportOrientationHandler360_InitializeTeleportDestination_m6372AC3D398F0ABB35CDE87532ED8ACEE579351A (void);
// 0x0000065B System.Void TeleportOrientationHandler360::UpdateTeleportDestination()
extern void TeleportOrientationHandler360_UpdateTeleportDestination_mF98B570F0875BCE3DEF88E3EA2BDCF415C759E71 (void);
// 0x0000065C System.Void TeleportOrientationHandler360::.ctor()
extern void TeleportOrientationHandler360__ctor_m6FDD3AEEEB9FB23CEC3C6D7B6A453FF38428B49A (void);
// 0x0000065D System.Void TeleportOrientationHandlerHMD::InitializeTeleportDestination()
extern void TeleportOrientationHandlerHMD_InitializeTeleportDestination_m507B4108A8B145144CC59BFB969FB37F3EDB88E9 (void);
// 0x0000065E System.Void TeleportOrientationHandlerHMD::UpdateTeleportDestination()
extern void TeleportOrientationHandlerHMD_UpdateTeleportDestination_mA3F3F034F1C248EBE74640A15F13D287D1C2AE2E (void);
// 0x0000065F System.Void TeleportOrientationHandlerHMD::.ctor()
extern void TeleportOrientationHandlerHMD__ctor_mED7328A59A1C729AC845F7A8410DD4BD5C633DE2 (void);
// 0x00000660 System.Void TeleportOrientationHandlerThumbstick::InitializeTeleportDestination()
extern void TeleportOrientationHandlerThumbstick_InitializeTeleportDestination_m4ABFA62C2B8CB74DE7829D0005E6C98FE32EB8F2 (void);
// 0x00000661 System.Void TeleportOrientationHandlerThumbstick::UpdateTeleportDestination()
extern void TeleportOrientationHandlerThumbstick_UpdateTeleportDestination_mBE2E2B2A0BCED3855974AAC1AAFBEE6F6081587B (void);
// 0x00000662 System.Void TeleportOrientationHandlerThumbstick::.ctor()
extern void TeleportOrientationHandlerThumbstick__ctor_mEAA7AC3ABA1CE6E0253CF38FB2CA3E4C318F59BF (void);
// 0x00000663 System.Void TeleportPoint::Start()
extern void TeleportPoint_Start_m30ECA69CD4353B458F2295BAEBB450CC01F59805 (void);
// 0x00000664 UnityEngine.Transform TeleportPoint::GetDestTransform()
extern void TeleportPoint_GetDestTransform_m721C2E343BE9B54FABF84CA15E207FD2390F7DBD (void);
// 0x00000665 System.Void TeleportPoint::Update()
extern void TeleportPoint_Update_mAF9BC964BBC534053D53659EF53DA1A09B22F484 (void);
// 0x00000666 System.Void TeleportPoint::OnLookAt()
extern void TeleportPoint_OnLookAt_m2564753261F0D09670931BBF308955CB21B8302A (void);
// 0x00000667 System.Void TeleportPoint::.ctor()
extern void TeleportPoint__ctor_mA783018C82A82D1A80BA40DF577BC9D3BC245A6A (void);
// 0x00000668 LocomotionTeleport TeleportSupport::get_LocomotionTeleport()
extern void TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714 (void);
// 0x00000669 System.Void TeleportSupport::set_LocomotionTeleport(LocomotionTeleport)
extern void TeleportSupport_set_LocomotionTeleport_m718C84A5C4E1D039C61850BA424EFA71B5FF9327 (void);
// 0x0000066A System.Void TeleportSupport::OnEnable()
extern void TeleportSupport_OnEnable_mF890133242B29B518FDCBD5FEAFAF29298CBA609 (void);
// 0x0000066B System.Void TeleportSupport::OnDisable()
extern void TeleportSupport_OnDisable_m9C71979791E9FCD13837605745FA3C6CFF0B1409 (void);
// 0x0000066C System.Void TeleportSupport::LogEventHandler(System.String)
extern void TeleportSupport_LogEventHandler_m65943E6F024E9B73CE5D5ED5099B2A0164B3E2C5 (void);
// 0x0000066D System.Void TeleportSupport::AddEventHandlers()
extern void TeleportSupport_AddEventHandlers_mE4502DF40E56A3B5061FB931FC4F21678BAE3E84 (void);
// 0x0000066E System.Void TeleportSupport::RemoveEventHandlers()
extern void TeleportSupport_RemoveEventHandlers_mE7C77E2C141F41B7799C511FC1B8DEB699B6E8E1 (void);
// 0x0000066F System.Void TeleportSupport::.ctor()
extern void TeleportSupport__ctor_m05529A4609A1DFC91979CE4AE1790639B372E754 (void);
// 0x00000670 System.Void TeleportTargetHandler::.ctor()
extern void TeleportTargetHandler__ctor_m3AE469213D738BE0ABD908BB5AA27AB6FF60240E (void);
// 0x00000671 System.Void TeleportTargetHandler::AddEventHandlers()
extern void TeleportTargetHandler_AddEventHandlers_mFFEC0649545A9B1614D7114872C7AECEED6AE715 (void);
// 0x00000672 System.Void TeleportTargetHandler::RemoveEventHandlers()
extern void TeleportTargetHandler_RemoveEventHandlers_m3DC103164A1F0089BBC66E83C9E754B7F9C79849 (void);
// 0x00000673 System.Collections.IEnumerator TeleportTargetHandler::TargetAimCoroutine()
extern void TeleportTargetHandler_TargetAimCoroutine_m1CEA9B7FA57455F77D9639E1B62C205C515E959D (void);
// 0x00000674 System.Void TeleportTargetHandler::ResetAimData()
extern void TeleportTargetHandler_ResetAimData_mA32665779CE5C63B71357211C714615BF9CD4DA2 (void);
// 0x00000675 System.Boolean TeleportTargetHandler::ConsiderTeleport(UnityEngine.Vector3,UnityEngine.Vector3&)
// 0x00000676 System.Nullable`1<UnityEngine.Vector3> TeleportTargetHandler::ConsiderDestination(UnityEngine.Vector3)
extern void TeleportTargetHandler_ConsiderDestination_m1A96940FCF8FD62A59948C3080FF813ABA4F8331 (void);
// 0x00000677 System.Void TeleportTargetHandler::<.ctor>b__3_0()
extern void TeleportTargetHandler_U3C_ctorU3Eb__3_0_m6384FA377850643BB1088262AAFC6FFA8748F8F9 (void);
// 0x00000678 System.Void TeleportTargetHandler/<TargetAimCoroutine>d__7::.ctor(System.Int32)
extern void U3CTargetAimCoroutineU3Ed__7__ctor_m23D9D0D8A352A65E0D99985A676E454304BF8FE4 (void);
// 0x00000679 System.Void TeleportTargetHandler/<TargetAimCoroutine>d__7::System.IDisposable.Dispose()
extern void U3CTargetAimCoroutineU3Ed__7_System_IDisposable_Dispose_m21B111D2C0E7C1EB3CE98929690C0DD190716414 (void);
// 0x0000067A System.Boolean TeleportTargetHandler/<TargetAimCoroutine>d__7::MoveNext()
extern void U3CTargetAimCoroutineU3Ed__7_MoveNext_m67B9E5EA47ACD29BD93DB469A85D596C9D94F69A (void);
// 0x0000067B System.Object TeleportTargetHandler/<TargetAimCoroutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTargetAimCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73510D945678774D27D2FEE03C45CF482A2EBBE6 (void);
// 0x0000067C System.Void TeleportTargetHandler/<TargetAimCoroutine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m2BC8EDEB99F6EEEA6DB6EE8F703C99E5CC6A7301 (void);
// 0x0000067D System.Object TeleportTargetHandler/<TargetAimCoroutine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mF0915E668ADBDEC27BF20ED85385A44114046F62 (void);
// 0x0000067E System.Void TeleportTargetHandlerNavMesh::Awake()
extern void TeleportTargetHandlerNavMesh_Awake_m12528F076C3DFA64B7F8C2738B086CCBA390746A (void);
// 0x0000067F System.Boolean TeleportTargetHandlerNavMesh::ConsiderTeleport(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void TeleportTargetHandlerNavMesh_ConsiderTeleport_m437418CEDAA72639344228B31EA8BDC8A52AD169 (void);
// 0x00000680 System.Nullable`1<UnityEngine.Vector3> TeleportTargetHandlerNavMesh::ConsiderDestination(UnityEngine.Vector3)
extern void TeleportTargetHandlerNavMesh_ConsiderDestination_m0095B7CAB6515175C4652DAFB166FB7851298DD8 (void);
// 0x00000681 System.Void TeleportTargetHandlerNavMesh::OnDrawGizmos()
extern void TeleportTargetHandlerNavMesh_OnDrawGizmos_mBBC3AFFADE1EA512DEF62DE6FB277B82EE99E1DC (void);
// 0x00000682 System.Void TeleportTargetHandlerNavMesh::.ctor()
extern void TeleportTargetHandlerNavMesh__ctor_m5BB4F183D1C263E465000903520478FBB0D2E990 (void);
// 0x00000683 System.Boolean TeleportTargetHandlerNode::ConsiderTeleport(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void TeleportTargetHandlerNode_ConsiderTeleport_m144CD7243791916F4D785F4E01AAAD729C1F1150 (void);
// 0x00000684 System.Void TeleportTargetHandlerNode::.ctor()
extern void TeleportTargetHandlerNode__ctor_m66245B4B90DFAE3D51056B3DC1B825CF128E4145 (void);
// 0x00000685 System.Boolean TeleportTargetHandlerPhysical::ConsiderTeleport(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void TeleportTargetHandlerPhysical_ConsiderTeleport_m3F6B5CBCEFE9B53FB42E5A853B728740B30A879F (void);
// 0x00000686 System.Void TeleportTargetHandlerPhysical::.ctor()
extern void TeleportTargetHandlerPhysical__ctor_mED10B7430B7A3A1104C1EDFF1A2F7B4B4B007B4D (void);
// 0x00000687 System.Void TeleportTransition::AddEventHandlers()
extern void TeleportTransition_AddEventHandlers_m7945828C3E62F3C4EF094AC2B12950618720397F (void);
// 0x00000688 System.Void TeleportTransition::RemoveEventHandlers()
extern void TeleportTransition_RemoveEventHandlers_mBD20E23B4E42E8DE6D30126DA98CF0B744864546 (void);
// 0x00000689 System.Void TeleportTransition::LocomotionTeleportOnEnterStateTeleporting()
// 0x0000068A System.Void TeleportTransition::.ctor()
extern void TeleportTransition__ctor_m1A4530AB5DEEA55E946DF9A491B5145EFE239ACF (void);
// 0x0000068B System.Void TeleportTransitionBlink::LocomotionTeleportOnEnterStateTeleporting()
extern void TeleportTransitionBlink_LocomotionTeleportOnEnterStateTeleporting_mCB87A486BE2733C4BCD2B57AEA4FAA69FED1D74D (void);
// 0x0000068C System.Collections.IEnumerator TeleportTransitionBlink::BlinkCoroutine()
extern void TeleportTransitionBlink_BlinkCoroutine_m502BAE8615CE4CC76BFD91E15746263D10CF2E77 (void);
// 0x0000068D System.Void TeleportTransitionBlink::.ctor()
extern void TeleportTransitionBlink__ctor_m709CADAB68439933BA41B5C318E4E60616F09932 (void);
// 0x0000068E System.Void TeleportTransitionBlink/<BlinkCoroutine>d__4::.ctor(System.Int32)
extern void U3CBlinkCoroutineU3Ed__4__ctor_m73115DAF8E2D89A49A9F6BD9AB03485AA7551BCD (void);
// 0x0000068F System.Void TeleportTransitionBlink/<BlinkCoroutine>d__4::System.IDisposable.Dispose()
extern void U3CBlinkCoroutineU3Ed__4_System_IDisposable_Dispose_m6A66CCE325E70F5794414025EA6A7B8EABFAE259 (void);
// 0x00000690 System.Boolean TeleportTransitionBlink/<BlinkCoroutine>d__4::MoveNext()
extern void U3CBlinkCoroutineU3Ed__4_MoveNext_mBEC5B507954BA0961920B4141D150885CE4D309B (void);
// 0x00000691 System.Object TeleportTransitionBlink/<BlinkCoroutine>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBlinkCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB31D456C6490D00742481C0F18817914FC76018 (void);
// 0x00000692 System.Void TeleportTransitionBlink/<BlinkCoroutine>d__4::System.Collections.IEnumerator.Reset()
extern void U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m6E21661229B022076FA60AF6C49A1980E56609E1 (void);
// 0x00000693 System.Object TeleportTransitionBlink/<BlinkCoroutine>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_mAA053761AFFEF4D53F8AC25C2BDA6DC4729996DF (void);
// 0x00000694 System.Void TeleportTransitionInstant::LocomotionTeleportOnEnterStateTeleporting()
extern void TeleportTransitionInstant_LocomotionTeleportOnEnterStateTeleporting_m09B573BE4E14DD8DD571233C7F3CCA384A775A4B (void);
// 0x00000695 System.Void TeleportTransitionInstant::.ctor()
extern void TeleportTransitionInstant__ctor_m2596160D526DF43F58D24D83F33B43AEB3774DA4 (void);
// 0x00000696 System.Void TeleportTransitionWarp::LocomotionTeleportOnEnterStateTeleporting()
extern void TeleportTransitionWarp_LocomotionTeleportOnEnterStateTeleporting_m2D397B890161CCF004AF8C0AC064F28EAD6EFB8C (void);
// 0x00000697 System.Collections.IEnumerator TeleportTransitionWarp::DoWarp()
extern void TeleportTransitionWarp_DoWarp_m12AF9CC51FC2784F1FCBCD37A9D0CBBDE2EEF117 (void);
// 0x00000698 System.Void TeleportTransitionWarp::.ctor()
extern void TeleportTransitionWarp__ctor_mA2A7FA91C271FEC14A97EF24341CC25EF5C5639B (void);
// 0x00000699 System.Void TeleportTransitionWarp/<DoWarp>d__3::.ctor(System.Int32)
extern void U3CDoWarpU3Ed__3__ctor_mA023019BAD546DB8BFCFC8326BF22566DC855B45 (void);
// 0x0000069A System.Void TeleportTransitionWarp/<DoWarp>d__3::System.IDisposable.Dispose()
extern void U3CDoWarpU3Ed__3_System_IDisposable_Dispose_mE361978D56E79BC8335160F0DA180CB96DCE29F9 (void);
// 0x0000069B System.Boolean TeleportTransitionWarp/<DoWarp>d__3::MoveNext()
extern void U3CDoWarpU3Ed__3_MoveNext_m84CA338E50BBE257229411F56CD6B4D7F46015B1 (void);
// 0x0000069C System.Object TeleportTransitionWarp/<DoWarp>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoWarpU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94EA3502B1F9442D23A5EBF53483F6C85D484208 (void);
// 0x0000069D System.Void TeleportTransitionWarp/<DoWarp>d__3::System.Collections.IEnumerator.Reset()
extern void U3CDoWarpU3Ed__3_System_Collections_IEnumerator_Reset_mE692557C58D90117915B599FA91A88FC7D238200 (void);
// 0x0000069E System.Object TeleportTransitionWarp/<DoWarp>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CDoWarpU3Ed__3_System_Collections_IEnumerator_get_Current_mD1CE2712B5B0919B97EE26421E8DDC32F11E5314 (void);
// 0x0000069F System.IntPtr NativeVideoPlayer::get_VideoPlayerClass()
extern void NativeVideoPlayer_get_VideoPlayerClass_mDF6B6206956E93EEBC7AF18915278A2D6421B53E (void);
// 0x000006A0 System.IntPtr NativeVideoPlayer::get_Activity()
extern void NativeVideoPlayer_get_Activity_m57A6F34EE8FDF96D02CC8911A3A412FCD1E84AA1 (void);
// 0x000006A1 System.Boolean NativeVideoPlayer::get_IsAvailable()
extern void NativeVideoPlayer_get_IsAvailable_m9EE0A75867D070F6DB6F146827B53778B1B15ECE (void);
// 0x000006A2 System.Boolean NativeVideoPlayer::get_IsPlaying()
extern void NativeVideoPlayer_get_IsPlaying_mEC08CBAC641A59742B7463B69DA7D259794238F9 (void);
// 0x000006A3 NativeVideoPlayer/PlabackState NativeVideoPlayer::get_CurrentPlaybackState()
extern void NativeVideoPlayer_get_CurrentPlaybackState_mC8D05EA20BFC1762858006863DA54611CD8F768F (void);
// 0x000006A4 System.Int64 NativeVideoPlayer::get_Duration()
extern void NativeVideoPlayer_get_Duration_m1DD26EE88BA047E7D07A5122433E8D53E70C295D (void);
// 0x000006A5 NativeVideoPlayer/StereoMode NativeVideoPlayer::get_VideoStereoMode()
extern void NativeVideoPlayer_get_VideoStereoMode_mA9EE9AC1F8865143D950B8DA844262FA4CEEB2AB (void);
// 0x000006A6 System.Int32 NativeVideoPlayer::get_VideoWidth()
extern void NativeVideoPlayer_get_VideoWidth_m0E4C6D08D48905A44F419C979943A14ABEADBC8A (void);
// 0x000006A7 System.Int32 NativeVideoPlayer::get_VideoHeight()
extern void NativeVideoPlayer_get_VideoHeight_m8E9DC31F1B90C05AE00C1BAC77AF7B96B3A38E66 (void);
// 0x000006A8 System.Int64 NativeVideoPlayer::get_PlaybackPosition()
extern void NativeVideoPlayer_get_PlaybackPosition_m8A7BC6FB1BE4B1E5CABE2EFFF156FEB84F029A25 (void);
// 0x000006A9 System.Void NativeVideoPlayer::set_PlaybackPosition(System.Int64)
extern void NativeVideoPlayer_set_PlaybackPosition_m5E361F1B165A814D829FA2E5A772855E804D0381 (void);
// 0x000006AA System.Void NativeVideoPlayer::PlayVideo(System.String,System.String,System.IntPtr)
extern void NativeVideoPlayer_PlayVideo_m369B2893EDF0A9C4D6CC3ABD8C39B87A5ADF878E (void);
// 0x000006AB System.Void NativeVideoPlayer::Stop()
extern void NativeVideoPlayer_Stop_m88F76F02AB21CDE313F7333C365AD3CA2D835D48 (void);
// 0x000006AC System.Void NativeVideoPlayer::Play()
extern void NativeVideoPlayer_Play_m1C55384A56E016C7F76701D9339BCDFDB2384278 (void);
// 0x000006AD System.Void NativeVideoPlayer::Pause()
extern void NativeVideoPlayer_Pause_m85CEFB984199ED08E3F13D1E324BD5CB60FF5EC0 (void);
// 0x000006AE System.Void NativeVideoPlayer::SetPlaybackSpeed(System.Single)
extern void NativeVideoPlayer_SetPlaybackSpeed_m917A50C5444FD8BED5324AE2DDF2C3ED0C09D8CA (void);
// 0x000006AF System.Void NativeVideoPlayer::SetLooping(System.Boolean)
extern void NativeVideoPlayer_SetLooping_m6A045BEABE99B0776A2A35F3A814B40FC7DF4B0C (void);
// 0x000006B0 System.Void NativeVideoPlayer::SetListenerRotation(UnityEngine.Quaternion)
extern void NativeVideoPlayer_SetListenerRotation_mF2264AF57895244DAFFA22A75780D48B2A8CE177 (void);
// 0x000006B1 System.Void NativeVideoPlayer::.cctor()
extern void NativeVideoPlayer__cctor_m2C57854907E5DD300D863EA75D72296752FD6C94 (void);
// 0x000006B2 System.Void ButtonDownListener::add_onButtonDown(System.Action)
extern void ButtonDownListener_add_onButtonDown_m6A369389838232FEEA1344146C1FD176F2ECE3E0 (void);
// 0x000006B3 System.Void ButtonDownListener::remove_onButtonDown(System.Action)
extern void ButtonDownListener_remove_onButtonDown_m88F58309C99390BCEB654878D6C45519ADFB5A02 (void);
// 0x000006B4 System.Void ButtonDownListener::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ButtonDownListener_OnPointerDown_mDC9FE990B96FBF2C6D980F8DBCB327E86BD04C9B (void);
// 0x000006B5 System.Void ButtonDownListener::.ctor()
extern void ButtonDownListener__ctor_mD2522D29DCA57136E34554C01F2C1DB18B6B4DD7 (void);
// 0x000006B6 MediaPlayerImage/ButtonType MediaPlayerImage::get_buttonType()
extern void MediaPlayerImage_get_buttonType_m738EF74C0EDD4616EFC1E68DC56AC701B53DD2A8 (void);
// 0x000006B7 System.Void MediaPlayerImage::set_buttonType(MediaPlayerImage/ButtonType)
extern void MediaPlayerImage_set_buttonType_m86058D07781C3EE3FCEE2546DE6506CA31BB1874 (void);
// 0x000006B8 System.Void MediaPlayerImage::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void MediaPlayerImage_OnPopulateMesh_mBFADD4CECF3314066C0377254FCB875E8C8F078F (void);
// 0x000006B9 System.Void MediaPlayerImage::.ctor()
extern void MediaPlayerImage__ctor_mB86C91BF2885DC329EDD99F24916F9AD717B42D3 (void);
// 0x000006BA System.Boolean MoviePlayerSample::get_IsPlaying()
extern void MoviePlayerSample_get_IsPlaying_mB7FE8B620130A38FB30BC09F7079E12084113CDF (void);
// 0x000006BB System.Void MoviePlayerSample::set_IsPlaying(System.Boolean)
extern void MoviePlayerSample_set_IsPlaying_mB98C234AC3AC146C0F74729906F71A7E9F849CE6 (void);
// 0x000006BC System.Int64 MoviePlayerSample::get_Duration()
extern void MoviePlayerSample_get_Duration_m4A055DBAEAC1819C7091ACEF4E2B8DBE5EF4BAFF (void);
// 0x000006BD System.Void MoviePlayerSample::set_Duration(System.Int64)
extern void MoviePlayerSample_set_Duration_m1B29A541BFA2385B82B3A793817338A24081ECBB (void);
// 0x000006BE System.Int64 MoviePlayerSample::get_PlaybackPosition()
extern void MoviePlayerSample_get_PlaybackPosition_m7B1A095968AFDF410EE031761D5BC5239328E00E (void);
// 0x000006BF System.Void MoviePlayerSample::set_PlaybackPosition(System.Int64)
extern void MoviePlayerSample_set_PlaybackPosition_mD8FC604C1C51A0577071A069BD3780DA303D4260 (void);
// 0x000006C0 System.Void MoviePlayerSample::Awake()
extern void MoviePlayerSample_Awake_m096BC4D6D5E6B709F4BB2441B58E489540CAB06A (void);
// 0x000006C1 System.Boolean MoviePlayerSample::IsLocalVideo(System.String)
extern void MoviePlayerSample_IsLocalVideo_m3B89DA94A65891DF9D5F3D6B8F7045A4B10897E9 (void);
// 0x000006C2 System.Void MoviePlayerSample::UpdateShapeAndStereo()
extern void MoviePlayerSample_UpdateShapeAndStereo_mA373D088B7381DB5A34923E79695184EC0AA1F0C (void);
// 0x000006C3 System.Collections.IEnumerator MoviePlayerSample::Start()
extern void MoviePlayerSample_Start_m41862FCCB8907A26C31EC7C5B374DEDD661F61A9 (void);
// 0x000006C4 System.Void MoviePlayerSample::Play(System.String,System.String)
extern void MoviePlayerSample_Play_m64FB2763D4DCAC19B89BEE384C53488B702F84AB (void);
// 0x000006C5 System.Void MoviePlayerSample::Play()
extern void MoviePlayerSample_Play_m43C298C6CA45290022B7F30B8CB648003469E44B (void);
// 0x000006C6 System.Void MoviePlayerSample::Pause()
extern void MoviePlayerSample_Pause_m1A8CFD8E325D63FFB48510A74E79F4CD7A07892B (void);
// 0x000006C7 System.Void MoviePlayerSample::SeekTo(System.Int64)
extern void MoviePlayerSample_SeekTo_mA2F91C759B810802D66038A151701C6AAC13B363 (void);
// 0x000006C8 System.Void MoviePlayerSample::Update()
extern void MoviePlayerSample_Update_m844225048CC9A23F2DD1AA0544BB61DD7D3867E5 (void);
// 0x000006C9 System.Void MoviePlayerSample::SetPlaybackSpeed(System.Single)
extern void MoviePlayerSample_SetPlaybackSpeed_mC9B71A0C6AD6851BE290D41ABB21689BE86BFE82 (void);
// 0x000006CA System.Void MoviePlayerSample::Stop()
extern void MoviePlayerSample_Stop_m869069EB49DB2E8DDC06D6833BA00905D0EC45CD (void);
// 0x000006CB System.Void MoviePlayerSample::OnApplicationPause(System.Boolean)
extern void MoviePlayerSample_OnApplicationPause_mD8379876774437E10F757DE1A6CEF26245746263 (void);
// 0x000006CC System.Void MoviePlayerSample::.ctor()
extern void MoviePlayerSample__ctor_mB1DFDA2F1E1E621CEDDE6898330836B240F1B7B7 (void);
// 0x000006CD System.Void MoviePlayerSample/<Start>d__33::.ctor(System.Int32)
extern void U3CStartU3Ed__33__ctor_mFBD4AF6C95F797D0753DD1E6789AB7EFB4F3A4B3 (void);
// 0x000006CE System.Void MoviePlayerSample/<Start>d__33::System.IDisposable.Dispose()
extern void U3CStartU3Ed__33_System_IDisposable_Dispose_mFD89D5A20E48873E01335A257B97824BE1B9EB56 (void);
// 0x000006CF System.Boolean MoviePlayerSample/<Start>d__33::MoveNext()
extern void U3CStartU3Ed__33_MoveNext_mD5A4E90D0615EF2D769C53930662CE60239B99F9 (void);
// 0x000006D0 System.Object MoviePlayerSample/<Start>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BCA0FBB841D0017B1050286ADA36684C73A329D (void);
// 0x000006D1 System.Void MoviePlayerSample/<Start>d__33::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__33_System_Collections_IEnumerator_Reset_m8ECDE46C41B27547EFB6B7E24E378DD06DD9107A (void);
// 0x000006D2 System.Object MoviePlayerSample/<Start>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__33_System_Collections_IEnumerator_get_Current_m8D673748B930D29175C9D928B493B015EB8158A9 (void);
// 0x000006D3 System.Void MoviePlayerSample/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m744287E1C5682D76E3E690286A071F39718E25A9 (void);
// 0x000006D4 System.Void MoviePlayerSample/<>c__DisplayClass34_0::<Play>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CPlayU3Eb__0_mA90EE1D1E1F6E42A47F0488801D4A7734840FD4C (void);
// 0x000006D5 System.Void MoviePlayerSampleControls::Start()
extern void MoviePlayerSampleControls_Start_m625008A6C19107D6BE93FF40434BAF580E2C1D80 (void);
// 0x000006D6 System.Void MoviePlayerSampleControls::OnPlayPauseClicked()
extern void MoviePlayerSampleControls_OnPlayPauseClicked_mDFCC988FB43880C3A320FFC326517C5EEA554099 (void);
// 0x000006D7 System.Void MoviePlayerSampleControls::OnFastForwardClicked()
extern void MoviePlayerSampleControls_OnFastForwardClicked_mDBE37296203D54341C0DFAA545CD2C37A3C6D36C (void);
// 0x000006D8 System.Void MoviePlayerSampleControls::OnRewindClicked()
extern void MoviePlayerSampleControls_OnRewindClicked_m890FFF45A080BEC11F3B0B3A11D492A1F6692C6E (void);
// 0x000006D9 System.Void MoviePlayerSampleControls::OnSeekBarMoved(System.Single)
extern void MoviePlayerSampleControls_OnSeekBarMoved_m53704CFB9FDB6324FEA81FC2E7EA57DA12AE04B6 (void);
// 0x000006DA System.Void MoviePlayerSampleControls::Seek(System.Int64)
extern void MoviePlayerSampleControls_Seek_mEC78032CC65FE67AA6A153DB42A4588955341EE5 (void);
// 0x000006DB System.Void MoviePlayerSampleControls::Update()
extern void MoviePlayerSampleControls_Update_m747FB37CA5F8CFBD485486E4517F4748489E38B3 (void);
// 0x000006DC System.Void MoviePlayerSampleControls::SetVisible(System.Boolean)
extern void MoviePlayerSampleControls_SetVisible_mB5C9350C361FC20DE05207E6CAB3D4805D28248F (void);
// 0x000006DD System.Void MoviePlayerSampleControls::.ctor()
extern void MoviePlayerSampleControls__ctor_mB9AEEE8EBB60CE81FD9C5BA1C8154171C669BB7A (void);
// 0x000006DE UnityEngine.Vector4 VectorUtil::ToVector(UnityEngine.Rect)
extern void VectorUtil_ToVector_m5B5C2B6F166F7D975A94BAEA079D2328252A8E96 (void);
// 0x000006DF System.Void AppDeeplinkUI::Start()
extern void AppDeeplinkUI_Start_m775C152A115FF43C1AB34D97D9DC20A34FA705EA (void);
// 0x000006E0 System.Void AppDeeplinkUI::Update()
extern void AppDeeplinkUI_Update_m8D48F1E3305A2469B0935D4E829931DCB0D76713 (void);
// 0x000006E1 System.Void AppDeeplinkUI::LaunchUnrealDeeplinkSample()
extern void AppDeeplinkUI_LaunchUnrealDeeplinkSample_mA5F8A1774DB5724E43C917EBE88431C6FE0D9236 (void);
// 0x000006E2 System.Void AppDeeplinkUI::LaunchSelf()
extern void AppDeeplinkUI_LaunchSelf_m2CA3C1FCE4414860CC1101B7D29B483B9AE45090 (void);
// 0x000006E3 System.Void AppDeeplinkUI::LaunchOtherApp()
extern void AppDeeplinkUI_LaunchOtherApp_mD63AA72CBA23EA78C7B91B1E4B59D9B7860B0823 (void);
// 0x000006E4 System.Void AppDeeplinkUI::.ctor()
extern void AppDeeplinkUI__ctor_mA31F77CBBA609E0128E496E7D23C0FE41A5F94A6 (void);
// 0x000006E5 System.Void CustomDebugUI::Awake()
extern void CustomDebugUI_Awake_m312D4A67E8EA4824658432AE459FA21877975F71 (void);
// 0x000006E6 System.Void CustomDebugUI::Start()
extern void CustomDebugUI_Start_m28DD234ADCAAC557207C669CE37554CD7C7CC833 (void);
// 0x000006E7 System.Void CustomDebugUI::Update()
extern void CustomDebugUI_Update_mBF9B69FFE07A90FEA92DF170D99F185C952B043E (void);
// 0x000006E8 UnityEngine.RectTransform CustomDebugUI::AddTextField(System.String,System.Int32)
extern void CustomDebugUI_AddTextField_m6CF8AA8D2CE855EF4B86EEF85E16E0F5AFC9F6FD (void);
// 0x000006E9 System.Void CustomDebugUI::RemoveFromCanvas(UnityEngine.RectTransform,System.Int32)
extern void CustomDebugUI_RemoveFromCanvas_mC32F1E9CA35DFF39075ADC86BF7112A207C9F961 (void);
// 0x000006EA System.Void CustomDebugUI::.ctor()
extern void CustomDebugUI__ctor_m27BB96E142EA068E3029D709C3065D65DC9D2CFC (void);
// 0x000006EB System.Void DebugUISample::Start()
extern void DebugUISample_Start_mF7AB8D2997FF58D02F5EB0FFA83D5B59DB982C7B (void);
// 0x000006EC System.Void DebugUISample::TogglePressed(UnityEngine.UI.Toggle)
extern void DebugUISample_TogglePressed_m2A267F4169FCDC7C8E0DD77FD85253225B6F381C (void);
// 0x000006ED System.Void DebugUISample::RadioPressed(System.String,System.String,UnityEngine.UI.Toggle)
extern void DebugUISample_RadioPressed_m7E36B100115C60405CC14822838E3ECD66418624 (void);
// 0x000006EE System.Void DebugUISample::SliderPressed(System.Single)
extern void DebugUISample_SliderPressed_m6A03CFB38E2BAF3DC6E4C63A7DD2B895F2BD5F37 (void);
// 0x000006EF System.Void DebugUISample::Update()
extern void DebugUISample_Update_m7FA91668B8F852E7FDE02E7F8F82F3643D729C72 (void);
// 0x000006F0 System.Void DebugUISample::LogButtonPressed()
extern void DebugUISample_LogButtonPressed_m8198BFFA8627BFEE6BAE268C801B5EE1C4B658F4 (void);
// 0x000006F1 System.Void DebugUISample::.ctor()
extern void DebugUISample__ctor_mBD27AFA42BD0FA8EF120FCC4CFE624D2CD6B205B (void);
// 0x000006F2 System.Void DebugUISample::<Start>b__2_0(UnityEngine.UI.Toggle)
extern void DebugUISample_U3CStartU3Eb__2_0_mC3BA080F2C315CCC00B545B43B4EC960D6CEA6D3 (void);
// 0x000006F3 System.Void DebugUISample::<Start>b__2_1(UnityEngine.UI.Toggle)
extern void DebugUISample_U3CStartU3Eb__2_1_m499D6E39FF2F002CD620BD3A790C87CF58BFBAE3 (void);
// 0x000006F4 System.Void DebugUISample::<Start>b__2_2(UnityEngine.UI.Toggle)
extern void DebugUISample_U3CStartU3Eb__2_2_m1D0C6B17BCC797AB931BDD9128F04612252A4517 (void);
// 0x000006F5 System.Void DebugUISample::<Start>b__2_3(UnityEngine.UI.Toggle)
extern void DebugUISample_U3CStartU3Eb__2_3_m148206FDC1F12C8E839B21019CFDFBCB7C320CD4 (void);
// 0x000006F6 System.Void AnalyticsUI::.ctor()
extern void AnalyticsUI__ctor_m21788905A6C8F76C59714536A99939E5B74426A3 (void);
// 0x000006F7 System.Void SampleUI::Start()
extern void SampleUI_Start_m982A3C805909F26B7AA71851484369D105F0A2A5 (void);
// 0x000006F8 System.Void SampleUI::Update()
extern void SampleUI_Update_m3509E9A6966DBFBC160CFF3D96B508D1F854113F (void);
// 0x000006F9 System.String SampleUI::GetText()
extern void SampleUI_GetText_m26599FA6C9771765EC9AD94F73BAAD00AEE049BD (void);
// 0x000006FA System.Void SampleUI::.ctor()
extern void SampleUI__ctor_mD3D7D360CE2BFF47B7799D39FBC3A99E1D5F211D (void);
// 0x000006FB System.Void StartCrashlytics::.ctor()
extern void StartCrashlytics__ctor_m9C967A85082223F55E9138CB80F4107DE4A2C9BC (void);
// 0x000006FC System.Void HandsActiveChecker::Awake()
extern void HandsActiveChecker_Awake_m789AB07810059246D886069A33494EDE150458DD (void);
// 0x000006FD System.Void HandsActiveChecker::Update()
extern void HandsActiveChecker_Update_m6C574E921E2E058430A4D6EFD50FB45F27F5526D (void);
// 0x000006FE System.Collections.IEnumerator HandsActiveChecker::GetCenterEye()
extern void HandsActiveChecker_GetCenterEye_mEAD4DB3FC55FE5F6AA80E74EFFA4BFF053534D0E (void);
// 0x000006FF System.Void HandsActiveChecker::.ctor()
extern void HandsActiveChecker__ctor_m8875DA4F87EC8FE57A7587203AA1F695AEA6CBA4 (void);
// 0x00000700 System.Void HandsActiveChecker/<GetCenterEye>d__6::.ctor(System.Int32)
extern void U3CGetCenterEyeU3Ed__6__ctor_m99DAFAB56EEBC093A2EFF1857B7E41EDA623C071 (void);
// 0x00000701 System.Void HandsActiveChecker/<GetCenterEye>d__6::System.IDisposable.Dispose()
extern void U3CGetCenterEyeU3Ed__6_System_IDisposable_Dispose_m87282416027E1D142DA109AC2CAE4B8A202B7F25 (void);
// 0x00000702 System.Boolean HandsActiveChecker/<GetCenterEye>d__6::MoveNext()
extern void U3CGetCenterEyeU3Ed__6_MoveNext_m8F91FCC289F307F81C3B0A1A6D16809AEAFFFA8A (void);
// 0x00000703 System.Object HandsActiveChecker/<GetCenterEye>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetCenterEyeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52AA44D25E603CBD76F6492C277DFA1502C50301 (void);
// 0x00000704 System.Void HandsActiveChecker/<GetCenterEye>d__6::System.Collections.IEnumerator.Reset()
extern void U3CGetCenterEyeU3Ed__6_System_Collections_IEnumerator_Reset_m60966632955FF20F0859533FE10315801CF3EFCC (void);
// 0x00000705 System.Object HandsActiveChecker/<GetCenterEye>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CGetCenterEyeU3Ed__6_System_Collections_IEnumerator_get_Current_m45CF38A1CA12804B458CFC1C748D8A3490DF1854 (void);
// 0x00000706 System.Void CharacterCapsule::Update()
extern void CharacterCapsule_Update_mD56302C0703C800E264601DFCECAD70323B6D8E4 (void);
// 0x00000707 System.Void CharacterCapsule::.ctor()
extern void CharacterCapsule__ctor_m3517E2CB4B11230AC5AF2F46D6FCBBBD3F2FBC0E (void);
// 0x00000708 LocomotionTeleport LocomotionSampleSupport::get_TeleportController()
extern void LocomotionSampleSupport_get_TeleportController_m8E3B912620DEA5687BAC288FE3CE5F8116706216 (void);
// 0x00000709 System.Void LocomotionSampleSupport::Start()
extern void LocomotionSampleSupport_Start_mCF86670570E36A40E41DC7613F8AFD7FAA740A89 (void);
// 0x0000070A System.Void LocomotionSampleSupport::Update()
extern void LocomotionSampleSupport_Update_m031DA7C38919902529A0267E72B70BB5B294C1BB (void);
// 0x0000070B System.Void LocomotionSampleSupport::Log(System.String)
extern void LocomotionSampleSupport_Log_m0389971AE83EE5B9F1F560DD9692E55CB2AA2791 (void);
// 0x0000070C TActivate LocomotionSampleSupport::ActivateCategory(UnityEngine.GameObject)
// 0x0000070D System.Void LocomotionSampleSupport::ActivateHandlers()
// 0x0000070E System.Void LocomotionSampleSupport::ActivateInput()
// 0x0000070F System.Void LocomotionSampleSupport::ActivateAim()
// 0x00000710 System.Void LocomotionSampleSupport::ActivateTarget()
// 0x00000711 System.Void LocomotionSampleSupport::ActivateOrientation()
// 0x00000712 System.Void LocomotionSampleSupport::ActivateTransition()
// 0x00000713 TActivate LocomotionSampleSupport::ActivateCategory()
// 0x00000714 System.Void LocomotionSampleSupport::UpdateToggle(UnityEngine.UI.Toggle,System.Boolean)
extern void LocomotionSampleSupport_UpdateToggle_mD1A6755435B3A901645BDFA66781E84BB141B17A (void);
// 0x00000715 System.Void LocomotionSampleSupport::SetupNonCap()
extern void LocomotionSampleSupport_SetupNonCap_mADDA8F8E1129D705CF311C52F138ACC60C3826B5 (void);
// 0x00000716 System.Void LocomotionSampleSupport::SetupTeleportDefaults()
extern void LocomotionSampleSupport_SetupTeleportDefaults_m5ACC86875EBD2D512ED55DB8211FB894F462B737 (void);
// 0x00000717 UnityEngine.GameObject LocomotionSampleSupport::AddInstance(UnityEngine.GameObject,System.String)
extern void LocomotionSampleSupport_AddInstance_m585F0527D6C6C2351CC04DF1AD6AFDFE9252B09F (void);
// 0x00000718 System.Void LocomotionSampleSupport::SetupNodeTeleport()
extern void LocomotionSampleSupport_SetupNodeTeleport_m553B0B6C9BA5C94F976B0EA0828523B6C7FA8F79 (void);
// 0x00000719 System.Void LocomotionSampleSupport::SetupTwoStickTeleport()
extern void LocomotionSampleSupport_SetupTwoStickTeleport_m0BBFB6EA935A3715A1D1677BC957562431346217 (void);
// 0x0000071A System.Void LocomotionSampleSupport::SetupWalkOnly()
extern void LocomotionSampleSupport_SetupWalkOnly_m1EE59A1C4972A91669F90AD573D410C26DBB370E (void);
// 0x0000071B System.Void LocomotionSampleSupport::SetupLeftStrafeRightTeleport()
extern void LocomotionSampleSupport_SetupLeftStrafeRightTeleport_mAF0F734749B4AEEEF9E7BA5B6131BA447174A7C5 (void);
// 0x0000071C System.Void LocomotionSampleSupport::.ctor()
extern void LocomotionSampleSupport__ctor_m613DB25830E2B519F9C75665ABC4F75F3FBB3A44 (void);
// 0x0000071D System.Void OVROverlayCanvas::Start()
extern void OVROverlayCanvas_Start_m02FACA531257AF78493D111425986BDE67AB2991 (void);
// 0x0000071E System.Void OVROverlayCanvas::OnDestroy()
extern void OVROverlayCanvas_OnDestroy_mB9B73D9E5A1C61426EF8989A4BA55E64B920CB29 (void);
// 0x0000071F System.Void OVROverlayCanvas::OnEnable()
extern void OVROverlayCanvas_OnEnable_m9FF98CCC4F3C096B72D990F0CF8FE6453D6FE7A1 (void);
// 0x00000720 System.Void OVROverlayCanvas::OnDisable()
extern void OVROverlayCanvas_OnDisable_m9E0AE938543EDE56DB3FC961CB82FD14D6715961 (void);
// 0x00000721 System.Boolean OVROverlayCanvas::ShouldRender()
extern void OVROverlayCanvas_ShouldRender_mAB1021F516AA15A84D0F9FA33C9DAE329A8C7157 (void);
// 0x00000722 System.Void OVROverlayCanvas::Update()
extern void OVROverlayCanvas_Update_m6E50C6A065641B95F393BAB2C20ED325FCBBC750 (void);
// 0x00000723 System.Boolean OVROverlayCanvas::get_overlayEnabled()
extern void OVROverlayCanvas_get_overlayEnabled_m9B539BAF0CDCCE9E1E5EBD918BE874E76F004BDF (void);
// 0x00000724 System.Void OVROverlayCanvas::set_overlayEnabled(System.Boolean)
extern void OVROverlayCanvas_set_overlayEnabled_mDA991B436B2AB9BA5CBA3F2566352635795AD5F3 (void);
// 0x00000725 System.Void OVROverlayCanvas::.ctor()
extern void OVROverlayCanvas__ctor_m8B8FA48BC2B16C384FEBB9DA6B887942548C6539 (void);
// 0x00000726 System.Void OVROverlayCanvas::.cctor()
extern void OVROverlayCanvas__cctor_mC6F4BF85FBE55F37ED781CB002A4FFDFC0704915 (void);
// 0x00000727 System.Void AugmentedObject::Start()
extern void AugmentedObject_Start_m8E0AD00236ACFDEAD69389D7467109BEE6B11B58 (void);
// 0x00000728 System.Void AugmentedObject::Update()
extern void AugmentedObject_Update_m3A9B0C9619B59930AC5508F8FE2758CB0BD02E55 (void);
// 0x00000729 System.Void AugmentedObject::Grab(OVRInput/Controller)
extern void AugmentedObject_Grab_m459B031A6D80E520AE75A6A43DBEE1A087DC1CB2 (void);
// 0x0000072A System.Void AugmentedObject::Release()
extern void AugmentedObject_Release_mAB1BE9AD51540219158AF0EFC23E4472C9B7BD3D (void);
// 0x0000072B System.Void AugmentedObject::ToggleShadowType()
extern void AugmentedObject_ToggleShadowType_mE9B7FDE7086D783F599CFE4164FFE16548F0E620 (void);
// 0x0000072C System.Void AugmentedObject::.ctor()
extern void AugmentedObject__ctor_m1BEA4EB1EEF2E15B9D43AF499898A2DC25F6056D (void);
// 0x0000072D System.Void BrushController::Start()
extern void BrushController_Start_mC8607F5C5E0D3C39318951CE99D90FBE562B9BCB (void);
// 0x0000072E System.Void BrushController::Update()
extern void BrushController_Update_mD50E4CA311FB894AAA68222ACA91EBE6C30593E1 (void);
// 0x0000072F System.Void BrushController::Grab(OVRInput/Controller)
extern void BrushController_Grab_mD6E7E5B9DC7580A384370CAC4A26AFF0379EE6CB (void);
// 0x00000730 System.Void BrushController::Release()
extern void BrushController_Release_m3D7E2EFE180929EF8235DFE5CCB7DEDBF7FD9507 (void);
// 0x00000731 System.Collections.IEnumerator BrushController::FadeCameraClearColor(UnityEngine.Color,System.Single)
extern void BrushController_FadeCameraClearColor_mF279AC0C77EA6F6F1758D99361413188B619AB05 (void);
// 0x00000732 System.Collections.IEnumerator BrushController::FadeSphere(UnityEngine.Color,System.Single,System.Boolean)
extern void BrushController_FadeSphere_mFE0E2D3B7CAB66CCBD2A263BF9E3C5234682E455 (void);
// 0x00000733 System.Void BrushController::.ctor()
extern void BrushController__ctor_m9257CBA4722EF69E6444F82933C2A2123AAE0CC6 (void);
// 0x00000734 System.Void BrushController/<FadeCameraClearColor>d__8::.ctor(System.Int32)
extern void U3CFadeCameraClearColorU3Ed__8__ctor_m71E38D8AED062F90AA945D9CC0540F9CDC001EE7 (void);
// 0x00000735 System.Void BrushController/<FadeCameraClearColor>d__8::System.IDisposable.Dispose()
extern void U3CFadeCameraClearColorU3Ed__8_System_IDisposable_Dispose_mB45544A56417DF7024AF0BF2FAC2892BEAA4CB57 (void);
// 0x00000736 System.Boolean BrushController/<FadeCameraClearColor>d__8::MoveNext()
extern void U3CFadeCameraClearColorU3Ed__8_MoveNext_mADA92C9B3E3117DC738F397AA647A824FCC4BCB1 (void);
// 0x00000737 System.Object BrushController/<FadeCameraClearColor>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeCameraClearColorU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CCA66E9BC227F6064FBD2B1EBDB29027B712EC6 (void);
// 0x00000738 System.Void BrushController/<FadeCameraClearColor>d__8::System.Collections.IEnumerator.Reset()
extern void U3CFadeCameraClearColorU3Ed__8_System_Collections_IEnumerator_Reset_m669F3DAFD50F24E439427435286DCBE5D5AAC116 (void);
// 0x00000739 System.Object BrushController/<FadeCameraClearColor>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CFadeCameraClearColorU3Ed__8_System_Collections_IEnumerator_get_Current_mD305219E1C23CE52D59AED4BFEF4C16461DAAAE1 (void);
// 0x0000073A System.Void BrushController/<FadeSphere>d__9::.ctor(System.Int32)
extern void U3CFadeSphereU3Ed__9__ctor_mFF2CC1B385DBDFF11A4C2EA5F29E290C231DFBE7 (void);
// 0x0000073B System.Void BrushController/<FadeSphere>d__9::System.IDisposable.Dispose()
extern void U3CFadeSphereU3Ed__9_System_IDisposable_Dispose_mBA8495222AE0CBF7E4A4569A41D5292D973A052B (void);
// 0x0000073C System.Boolean BrushController/<FadeSphere>d__9::MoveNext()
extern void U3CFadeSphereU3Ed__9_MoveNext_m3941BF83B9BCB6851DA0EAC9DBF79BC9052E9825 (void);
// 0x0000073D System.Object BrushController/<FadeSphere>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeSphereU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09FCC5A8134E70A5773ABDBD3771EC06F0EFB03F (void);
// 0x0000073E System.Void BrushController/<FadeSphere>d__9::System.Collections.IEnumerator.Reset()
extern void U3CFadeSphereU3Ed__9_System_Collections_IEnumerator_Reset_m38A51A10EA31A82FEC63C26F45E5AD2A52F86AA6 (void);
// 0x0000073F System.Object BrushController/<FadeSphere>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CFadeSphereU3Ed__9_System_Collections_IEnumerator_get_Current_m86BCBEDDEC870379C62644F1C8BF0CDB327A86C9 (void);
// 0x00000740 System.Void EnableUnpremultipliedAlpha::Start()
extern void EnableUnpremultipliedAlpha_Start_m4926EC7506FF573EADA7ECC9CBB7D7E4DECAA67F (void);
// 0x00000741 System.Void EnableUnpremultipliedAlpha::.ctor()
extern void EnableUnpremultipliedAlpha__ctor_m72150D5AFC1B04755F8960E4A7E2211BC3E6BF0A (void);
// 0x00000742 System.Void Flashlight::LateUpdate()
extern void Flashlight_LateUpdate_mC96F82CE200FBCB574AE8EC09BC4A6794C3D8192 (void);
// 0x00000743 System.Void Flashlight::ToggleFlashlight()
extern void Flashlight_ToggleFlashlight_m2C0E6D9CB2AFB7BC18058A37692B42554F53E0AD (void);
// 0x00000744 System.Void Flashlight::EnableFlashlight(System.Boolean)
extern void Flashlight_EnableFlashlight_mB6ED8C5FDB101A26CD2329430939DC65D087907E (void);
// 0x00000745 System.Void Flashlight::.ctor()
extern void Flashlight__ctor_m3126420E3768B6D3CFD0472251F924BE38E81671 (void);
// 0x00000746 System.Void FlashlightController::Start()
extern void FlashlightController_Start_mA7C1D7727D1DE21DCFD4FD4A1FD3A432DFAD5A56 (void);
// 0x00000747 System.Void FlashlightController::LateUpdate()
extern void FlashlightController_LateUpdate_m6B632F81EDC2907F75EB22CBCE2BC4ECCAA121CD (void);
// 0x00000748 System.Void FlashlightController::FindHands()
extern void FlashlightController_FindHands_mBC00BF0B7AF22181B8586036875B7C9DB3C531C4 (void);
// 0x00000749 System.Void FlashlightController::AlignWithHand(OVRHand,OVRSkeleton)
extern void FlashlightController_AlignWithHand_mCE1A47A2AFD6232C0EA1D8D220E33454CD980E55 (void);
// 0x0000074A System.Void FlashlightController::AlignWithController(OVRInput/Controller)
extern void FlashlightController_AlignWithController_m124649B6FC7BDB21BCE91087F5AF313E83EAAB37 (void);
// 0x0000074B System.Void FlashlightController::Grab(OVRInput/Controller)
extern void FlashlightController_Grab_m1C763114DEBABE1752067BB044130A9BB5A75175 (void);
// 0x0000074C System.Void FlashlightController::Release()
extern void FlashlightController_Release_m0B6874BDB6CBABE601DB5C94FAD11F11C6F4FE67 (void);
// 0x0000074D System.Collections.IEnumerator FlashlightController::FadeLighting(UnityEngine.Color,System.Single,System.Single)
extern void FlashlightController_FadeLighting_mE6DADFDB6FE28475B62BB3D04E99568CCC8052D8 (void);
// 0x0000074E System.Void FlashlightController::.ctor()
extern void FlashlightController__ctor_mB8B30045D45FF7336C3D1ED3C2343C40EA1EEA10 (void);
// 0x0000074F System.Void FlashlightController/<FadeLighting>d__17::.ctor(System.Int32)
extern void U3CFadeLightingU3Ed__17__ctor_m3178D02F76D3965BBFFA4F56F2F4243AF9B3B33A (void);
// 0x00000750 System.Void FlashlightController/<FadeLighting>d__17::System.IDisposable.Dispose()
extern void U3CFadeLightingU3Ed__17_System_IDisposable_Dispose_m636516D26E0FC21793138E4235EAAFD9B87723BF (void);
// 0x00000751 System.Boolean FlashlightController/<FadeLighting>d__17::MoveNext()
extern void U3CFadeLightingU3Ed__17_MoveNext_m114DFA09D1CD0CF21952702777E86C16871EADAA (void);
// 0x00000752 System.Object FlashlightController/<FadeLighting>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeLightingU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9E7E8152BD8F758715E45BC1885F51A63E26315 (void);
// 0x00000753 System.Void FlashlightController/<FadeLighting>d__17::System.Collections.IEnumerator.Reset()
extern void U3CFadeLightingU3Ed__17_System_Collections_IEnumerator_Reset_m532D658BF737F23E5DE75444F9DED652DF6FFC27 (void);
// 0x00000754 System.Object FlashlightController/<FadeLighting>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CFadeLightingU3Ed__17_System_Collections_IEnumerator_get_Current_mCCF09AD2D019507FE4A94EE11AFAE44708D1A70E (void);
// 0x00000755 System.Void GrabObject::Grab(OVRInput/Controller)
extern void GrabObject_Grab_mB218C144950CC665DEF2C0D2621EC3BAB58F4DDB (void);
// 0x00000756 System.Void GrabObject::Release()
extern void GrabObject_Release_m87C19D984E37BBC51F11AE79362BCC463D9DF541 (void);
// 0x00000757 System.Void GrabObject::CursorPos(UnityEngine.Vector3)
extern void GrabObject_CursorPos_m712B6B88A9A282219C22F5AC6D91563EF6CF129A (void);
// 0x00000758 System.Void GrabObject::.ctor()
extern void GrabObject__ctor_mE34AA1EC033A0A74D6E8F881A20312EAD2E37316 (void);
// 0x00000759 System.Void GrabObject/GrabbedObject::.ctor(System.Object,System.IntPtr)
extern void GrabbedObject__ctor_mD71655859A58277437D2D41BF2D2E32ED45A3142 (void);
// 0x0000075A System.Void GrabObject/GrabbedObject::Invoke(OVRInput/Controller)
extern void GrabbedObject_Invoke_mC5D11C66F1CA103D44AC0120309B8329C8C5AC12 (void);
// 0x0000075B System.IAsyncResult GrabObject/GrabbedObject::BeginInvoke(OVRInput/Controller,System.AsyncCallback,System.Object)
extern void GrabbedObject_BeginInvoke_m673B6B520980FE487E8E3698AC7C5E34E3999F7D (void);
// 0x0000075C System.Void GrabObject/GrabbedObject::EndInvoke(System.IAsyncResult)
extern void GrabbedObject_EndInvoke_mE288DA466CEF66AE67D22C3A85FC4FF0340AC220 (void);
// 0x0000075D System.Void GrabObject/ReleasedObject::.ctor(System.Object,System.IntPtr)
extern void ReleasedObject__ctor_mA1EB137D310B511053A0A1EB8F02C1A879C50E37 (void);
// 0x0000075E System.Void GrabObject/ReleasedObject::Invoke()
extern void ReleasedObject_Invoke_mAD25C4E4B23562D2DC5EA00C154F670B80B2EC1E (void);
// 0x0000075F System.IAsyncResult GrabObject/ReleasedObject::BeginInvoke(System.AsyncCallback,System.Object)
extern void ReleasedObject_BeginInvoke_mCF974E830722A67E3C0E157F95722386F5135C72 (void);
// 0x00000760 System.Void GrabObject/ReleasedObject::EndInvoke(System.IAsyncResult)
extern void ReleasedObject_EndInvoke_m022CDE0C465D8F488CB44DF0F4537DC8D8F97EBF (void);
// 0x00000761 System.Void GrabObject/SetCursorPosition::.ctor(System.Object,System.IntPtr)
extern void SetCursorPosition__ctor_mFF389CC65BAD558CBA69A47B907658D65C53A63E (void);
// 0x00000762 System.Void GrabObject/SetCursorPosition::Invoke(UnityEngine.Vector3)
extern void SetCursorPosition_Invoke_m15B4D29A4EFC2AA3C15ECF436299830A14AEA2F8 (void);
// 0x00000763 System.IAsyncResult GrabObject/SetCursorPosition::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern void SetCursorPosition_BeginInvoke_mEC0A62FFCF845C61EB8FF6587D574FFEE524AFE5 (void);
// 0x00000764 System.Void GrabObject/SetCursorPosition::EndInvoke(System.IAsyncResult)
extern void SetCursorPosition_EndInvoke_m621EE977B55491598884A9DFBFAC6D7B247250AE (void);
// 0x00000765 System.Void HandMeshMask::Awake()
extern void HandMeshMask_Awake_m507910DB71C1F770805606FBE79EC77CAE9E0896 (void);
// 0x00000766 System.Void HandMeshMask::Update()
extern void HandMeshMask_Update_m38AC51F8B4FAD5EB72AF1C53379AD9230B4D6185 (void);
// 0x00000767 System.Void HandMeshMask::CreateHandMesh()
extern void HandMeshMask_CreateHandMesh_m520E9CAE4BB1236F888D480F46994CDBC82BF3A7 (void);
// 0x00000768 System.Void HandMeshMask::AddKnuckleMesh(System.Int32,System.Single,System.Single,UnityEngine.Vector3,UnityEngine.Vector3)
extern void HandMeshMask_AddKnuckleMesh_m7FE07234E6CE21E5C1930864ADFE9F4E5A878EC9 (void);
// 0x00000769 System.Void HandMeshMask::AddPalmMesh(System.Int32)
extern void HandMeshMask_AddPalmMesh_m86D1CFFDA37877F6DDA84DEB1EB000ADE491CB5F (void);
// 0x0000076A System.Void HandMeshMask::AddVertex(UnityEngine.Vector3,UnityEngine.Vector2,UnityEngine.Color)
extern void HandMeshMask_AddVertex_mFC6D3205D1524A29258D1E7C25257D6A3EB0DE2A (void);
// 0x0000076B System.Void HandMeshMask::.ctor()
extern void HandMeshMask__ctor_m37E35E530849032B1EF4024717644B8FC3723361 (void);
// 0x0000076C System.Void HandMeshUI::Start()
extern void HandMeshUI_Start_m80C99FE9B881C46B86AE58C885846979489F8E2F (void);
// 0x0000076D System.Void HandMeshUI::Update()
extern void HandMeshUI_Update_mAC47D9C487ADFA21CB64CB5DA2735E72EB8A3345 (void);
// 0x0000076E System.Void HandMeshUI::SetSliderValue(System.Int32,System.Single,System.Boolean)
extern void HandMeshUI_SetSliderValue_m91E3D3B63E9A6677816037BB8DE109815A111FD5 (void);
// 0x0000076F System.Void HandMeshUI::CheckForHands()
extern void HandMeshUI_CheckForHands_m5C8F16DA9E3D2B1906E4681B453190E184D9048D (void);
// 0x00000770 System.Void HandMeshUI::.ctor()
extern void HandMeshUI__ctor_m85C3A4E86D772F5799B0271350C9564E31FDFC89 (void);
// 0x00000771 System.Void ObjectManipulator::Start()
extern void ObjectManipulator_Start_mEBCDF18C339DB050547E7FAC14A140A2DEFEB4C9 (void);
// 0x00000772 System.Void ObjectManipulator::Update()
extern void ObjectManipulator_Update_m72C78567C8C72FC86BB069AD411212072EA8F03D (void);
// 0x00000773 System.Void ObjectManipulator::GrabHoverObject(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ObjectManipulator_GrabHoverObject_mBC43E1E7E210F0D296012D424536B74C00805A5E (void);
// 0x00000774 System.Void ObjectManipulator::ReleaseObject()
extern void ObjectManipulator_ReleaseObject_m02231C55BA814EDC06B3F938B7C2E8DEAB914E28 (void);
// 0x00000775 System.Collections.IEnumerator ObjectManipulator::StartDemo()
extern void ObjectManipulator_StartDemo_m0754E0B57F00F054718FB66F9C646CB3882B26F9 (void);
// 0x00000776 System.Void ObjectManipulator::FindHoverObject(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ObjectManipulator_FindHoverObject_m3E6E90C275DF5252101B82C80D2CDBB5C22C4A37 (void);
// 0x00000777 System.Void ObjectManipulator::ManipulateObject(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ObjectManipulator_ManipulateObject_m229CCFCC09F5B4C39EFDE9E1D83993FB4675511C (void);
// 0x00000778 System.Void ObjectManipulator::ClampGrabOffset(UnityEngine.Vector3&,System.Single)
extern void ObjectManipulator_ClampGrabOffset_m53081A466E925B6FBBE4AFF4850DF53E4531B7A7 (void);
// 0x00000779 UnityEngine.Vector3 ObjectManipulator::ClampScale(UnityEngine.Vector3,UnityEngine.Vector2)
extern void ObjectManipulator_ClampScale_mAD641AE21DC8D808B1E66B18906A701471035267 (void);
// 0x0000077A System.Void ObjectManipulator::CheckForDominantHand()
extern void ObjectManipulator_CheckForDominantHand_m5492FB8645B7165F98D80E4F219962EA4DA4CBCD (void);
// 0x0000077B System.Void ObjectManipulator::AssignInstructions(GrabObject)
extern void ObjectManipulator_AssignInstructions_mA360EDC3C9EC6D0BAAF1C6D16618803917B3CC02 (void);
// 0x0000077C System.Void ObjectManipulator::.ctor()
extern void ObjectManipulator__ctor_mEEE59FC77CA6A41814536C9DFCB24192D5F21DE8 (void);
// 0x0000077D System.Void ObjectManipulator/<StartDemo>d__23::.ctor(System.Int32)
extern void U3CStartDemoU3Ed__23__ctor_m93B55F7FFD26DDF5A738871EBEB2107BF95DE4C7 (void);
// 0x0000077E System.Void ObjectManipulator/<StartDemo>d__23::System.IDisposable.Dispose()
extern void U3CStartDemoU3Ed__23_System_IDisposable_Dispose_m474A042BE09520BAFF40F9DAF52345B48C09EBD4 (void);
// 0x0000077F System.Boolean ObjectManipulator/<StartDemo>d__23::MoveNext()
extern void U3CStartDemoU3Ed__23_MoveNext_mD24727A203FF95967F58B93DAC73148D97E43ECF (void);
// 0x00000780 System.Object ObjectManipulator/<StartDemo>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartDemoU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19A26D41F7FB9A11FB058C438E02F555FB51BA20 (void);
// 0x00000781 System.Void ObjectManipulator/<StartDemo>d__23::System.Collections.IEnumerator.Reset()
extern void U3CStartDemoU3Ed__23_System_Collections_IEnumerator_Reset_m4291011401E9FEB2BEB0922EAEC7AEF435F7D49B (void);
// 0x00000782 System.Object ObjectManipulator/<StartDemo>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CStartDemoU3Ed__23_System_Collections_IEnumerator_get_Current_m804A4428AFF3C3D87F16FAD3D707FE4585EB405A (void);
// 0x00000783 System.Void OverlayPassthrough::Start()
extern void OverlayPassthrough_Start_m97C9F6B31E6315147C9E6EB5E796CA724229751A (void);
// 0x00000784 System.Void OverlayPassthrough::Update()
extern void OverlayPassthrough_Update_m5553EE278872B84F1DEB50AE402E7B512D997CD7 (void);
// 0x00000785 System.Void OverlayPassthrough::.ctor()
extern void OverlayPassthrough__ctor_mF913DC5B9EA0FCDC65A0E34CAF78F74F5057B499 (void);
// 0x00000786 System.Void PassthroughBrush::OnDisable()
extern void PassthroughBrush_OnDisable_m838B38AE602884CCB0BE04DE661EA60F012E47FB (void);
// 0x00000787 System.Void PassthroughBrush::LateUpdate()
extern void PassthroughBrush_LateUpdate_m03306C8C4FA821AA69875F16A21EF9F9908DE954 (void);
// 0x00000788 System.Void PassthroughBrush::StartLine(UnityEngine.Vector3)
extern void PassthroughBrush_StartLine_mEDDC47E33545C96489E037E8B136D4CA579624BE (void);
// 0x00000789 System.Void PassthroughBrush::UpdateLine(UnityEngine.Vector3)
extern void PassthroughBrush_UpdateLine_m508789A468A107D18B3C06A34E1A0A48FCA59FF0 (void);
// 0x0000078A System.Void PassthroughBrush::ClearLines()
extern void PassthroughBrush_ClearLines_m684A85937EAF8304462A02BB40F5167779554BF2 (void);
// 0x0000078B System.Void PassthroughBrush::UndoInkLine()
extern void PassthroughBrush_UndoInkLine_m23CD97DDBA483D18F39256132BDD62F5F0B2A63A (void);
// 0x0000078C System.Void PassthroughBrush::.ctor()
extern void PassthroughBrush__ctor_mDEF0A84B7E60A4F9FE0D7584CA5F2F0D92A89311 (void);
// 0x0000078D System.Void PassthroughController::Start()
extern void PassthroughController_Start_m086EE35AA2F6701D88686F0A874F2D3AB9684631 (void);
// 0x0000078E System.Void PassthroughController::Update()
extern void PassthroughController_Update_mCE0679C2D8F409F507F99818B9FFB4467554FFD1 (void);
// 0x0000078F System.Void PassthroughController::.ctor()
extern void PassthroughController__ctor_mB542E27BD18002805C29FA8B8AE4E70101CC2B12 (void);
// 0x00000790 System.Void PassthroughProjectionSurface::Start()
extern void PassthroughProjectionSurface_Start_m1C289BDEF11167F3B6B3CC13CA3D2B0FB243A94B (void);
// 0x00000791 System.Void PassthroughProjectionSurface::Update()
extern void PassthroughProjectionSurface_Update_m32347B174938842813CB23FF93C7DB52E17320AC (void);
// 0x00000792 System.Void PassthroughProjectionSurface::.ctor()
extern void PassthroughProjectionSurface__ctor_m62D8E3BAA312AF5CA874323872CD7E14EA0251C4 (void);
// 0x00000793 System.Void PassthroughStyler::Start()
extern void PassthroughStyler_Start_m4092C1316E5C3612B264AF1E185992BBBE17C3C6 (void);
// 0x00000794 System.Void PassthroughStyler::Update()
extern void PassthroughStyler_Update_mDDBAB8B2F74B759734BAB13249701F84AE886D8F (void);
// 0x00000795 System.Void PassthroughStyler::Grab(OVRInput/Controller)
extern void PassthroughStyler_Grab_mC1D22409F8C813A9516E1BC8C38762359F008BE4 (void);
// 0x00000796 System.Void PassthroughStyler::Release()
extern void PassthroughStyler_Release_m64EC76A02BD96B9818C1838EE93B509F0D59A28A (void);
// 0x00000797 System.Collections.IEnumerator PassthroughStyler::FadeToCurrentStyle(System.Single)
extern void PassthroughStyler_FadeToCurrentStyle_m89B40CBE7FC06A936547F98A3A11CE491E62F75C (void);
// 0x00000798 System.Collections.IEnumerator PassthroughStyler::FadeToDefaultPassthrough(System.Single)
extern void PassthroughStyler_FadeToDefaultPassthrough_m9CDB8FB4E0D030CB89347B19B531763D9E35EC6D (void);
// 0x00000799 System.Void PassthroughStyler::OnBrightnessChanged(System.Single)
extern void PassthroughStyler_OnBrightnessChanged_mAD761CCF67D62E8B6C31BA3E430E388310FD3C8A (void);
// 0x0000079A System.Void PassthroughStyler::OnContrastChanged(System.Single)
extern void PassthroughStyler_OnContrastChanged_mD62F12B93AD84016661953D7AC62A910E794D97C (void);
// 0x0000079B System.Void PassthroughStyler::OnPosterizeChanged(System.Single)
extern void PassthroughStyler_OnPosterizeChanged_m56B23239FA40FA946248F57D7142C1EBCC10E112 (void);
// 0x0000079C System.Void PassthroughStyler::OnAlphaChanged(System.Single)
extern void PassthroughStyler_OnAlphaChanged_m2E29232FB7882E0CDD74227B63A6B3409EA71568 (void);
// 0x0000079D System.Void PassthroughStyler::ShowFullMenu(System.Boolean)
extern void PassthroughStyler_ShowFullMenu_m63597CE4A8078251E6BA27225B375A4C493D310B (void);
// 0x0000079E System.Void PassthroughStyler::Cursor(UnityEngine.Vector3)
extern void PassthroughStyler_Cursor_m7BA7DDF0FAD50949DC8C251499BB8000BF2FD16D (void);
// 0x0000079F System.Void PassthroughStyler::DoColorDrag(System.Boolean)
extern void PassthroughStyler_DoColorDrag_m11B7DA9E8F21113859605E888C3BEB871684E2A5 (void);
// 0x000007A0 System.Void PassthroughStyler::GetColorFromWheel()
extern void PassthroughStyler_GetColorFromWheel_mA52723A4C04478A9E36FD6BEADA82662744EBEBE (void);
// 0x000007A1 System.Void PassthroughStyler::.ctor()
extern void PassthroughStyler__ctor_m5F97489280B446110F4C2BAAD9327E817821B130 (void);
// 0x000007A2 System.Void PassthroughStyler/<FadeToCurrentStyle>d__19::.ctor(System.Int32)
extern void U3CFadeToCurrentStyleU3Ed__19__ctor_mAE485AE3924BF8387544A21DBBDC07DCC91AA9BD (void);
// 0x000007A3 System.Void PassthroughStyler/<FadeToCurrentStyle>d__19::System.IDisposable.Dispose()
extern void U3CFadeToCurrentStyleU3Ed__19_System_IDisposable_Dispose_mB7C7E02379F912796844ECB81A7ABAB5101237E4 (void);
// 0x000007A4 System.Boolean PassthroughStyler/<FadeToCurrentStyle>d__19::MoveNext()
extern void U3CFadeToCurrentStyleU3Ed__19_MoveNext_m695EBBCC6271D725364D7EDA234F63636A989360 (void);
// 0x000007A5 System.Object PassthroughStyler/<FadeToCurrentStyle>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeToCurrentStyleU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m62E9360956FC142D7DBC1E9381A9FBDD782771BA (void);
// 0x000007A6 System.Void PassthroughStyler/<FadeToCurrentStyle>d__19::System.Collections.IEnumerator.Reset()
extern void U3CFadeToCurrentStyleU3Ed__19_System_Collections_IEnumerator_Reset_mB5C2710DD59CC9A98A6208DB18E20E1779BE046F (void);
// 0x000007A7 System.Object PassthroughStyler/<FadeToCurrentStyle>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CFadeToCurrentStyleU3Ed__19_System_Collections_IEnumerator_get_Current_mAA54D3F70B51CFA5841047A0C02E509EBCA0E1B2 (void);
// 0x000007A8 System.Void PassthroughStyler/<FadeToDefaultPassthrough>d__20::.ctor(System.Int32)
extern void U3CFadeToDefaultPassthroughU3Ed__20__ctor_mD5734AE35E978B6016AC4BAAF5BD2885854E7289 (void);
// 0x000007A9 System.Void PassthroughStyler/<FadeToDefaultPassthrough>d__20::System.IDisposable.Dispose()
extern void U3CFadeToDefaultPassthroughU3Ed__20_System_IDisposable_Dispose_m098A05EF49FFD658B51912059ADA457D2E5BCF4E (void);
// 0x000007AA System.Boolean PassthroughStyler/<FadeToDefaultPassthrough>d__20::MoveNext()
extern void U3CFadeToDefaultPassthroughU3Ed__20_MoveNext_m2E27578A23C55A27E901446E92898D8B8C50843E (void);
// 0x000007AB System.Object PassthroughStyler/<FadeToDefaultPassthrough>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53DC68C1CCCDF30EC9CB9DB49C519C931805FABC (void);
// 0x000007AC System.Void PassthroughStyler/<FadeToDefaultPassthrough>d__20::System.Collections.IEnumerator.Reset()
extern void U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_IEnumerator_Reset_m67B84B18ED37EF94A7E28DC9D942BF28EE813ED8 (void);
// 0x000007AD System.Object PassthroughStyler/<FadeToDefaultPassthrough>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_IEnumerator_get_Current_m271ADFE5A502CB0CE024D87718FCE1B1BD5220BC (void);
// 0x000007AE System.Void PassthroughSurface::Start()
extern void PassthroughSurface_Start_m7E506149753FD8679C9CB718543978CA53CE1F74 (void);
// 0x000007AF System.Void PassthroughSurface::.ctor()
extern void PassthroughSurface__ctor_m6B56B0E135BDB59C84145F3D6DC79877B2C65090 (void);
// 0x000007B0 System.Void SPPquad::Start()
extern void SPPquad_Start_m092C0AF73D6B9CD60ED91D3DAF859FDAA4EF5896 (void);
// 0x000007B1 System.Void SPPquad::Grab(OVRInput/Controller)
extern void SPPquad_Grab_m8B4D385FEA16E6FFBD1A49860DB3B6FB13295A17 (void);
// 0x000007B2 System.Void SPPquad::Release()
extern void SPPquad_Release_mE8CA2CE6FBABBFA6CFBA60A468981092C0914A63 (void);
// 0x000007B3 System.Void SPPquad::.ctor()
extern void SPPquad__ctor_mE270AE3BB58A6EBD5FB9C47F99428A242A79CC41 (void);
// 0x000007B4 System.Void SceneSampler::Awake()
extern void SceneSampler_Awake_m632DFE6F8657193318C2BD87E035227C40EB8507 (void);
// 0x000007B5 System.Void SceneSampler::Update()
extern void SceneSampler_Update_m3D06DA05BAACAA52006AF75D138BCD5D4923207E (void);
// 0x000007B6 System.Void SceneSampler::.ctor()
extern void SceneSampler__ctor_m613B46CE973907D623D017AD04DAF9B991B61C75 (void);
// 0x000007B7 System.Void SelectivePassthroughExperience::Update()
extern void SelectivePassthroughExperience_Update_m0DA48E563BEB1B36C67E476AF5745FCB13B88903 (void);
// 0x000007B8 System.Void SelectivePassthroughExperience::.ctor()
extern void SelectivePassthroughExperience__ctor_m312145A2A5A8560CE04653F4F4799F843525FD62 (void);
// 0x000007B9 System.Void StartMenu::Start()
extern void StartMenu_Start_m38A6B8AD8F7E61759540CF792C58672CFD13ED46 (void);
// 0x000007BA System.Void StartMenu::LoadScene(System.Int32)
extern void StartMenu_LoadScene_mF7663ABFF602CD53F3A26C076E6A8FA200347F97 (void);
// 0x000007BB System.Void StartMenu::.ctor()
extern void StartMenu__ctor_mF07C92F5B046EB38E3933ABD69D96918F29EB952 (void);
// 0x000007BC System.Void StartMenu/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB4BEA21B02CB620F151FB8C1D57C0384883E9130 (void);
// 0x000007BD System.Void StartMenu/<>c__DisplayClass3_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CStartU3Eb__0_mFE41F96EFC3E7314714C2E5F6392280A788A23D2 (void);
// 0x000007BE System.Void ClientManager::Start()
extern void ClientManager_Start_mC42606D276CB33FA9190F652B50D1AB2E605117A (void);
// 0x000007BF System.Void ClientManager::SwitchFireExtinguisher(DKP.VR.HandsVisualization/DisplayMode)
extern void ClientManager_SwitchFireExtinguisher_m9382E50BCFD1CE3D905DF3C14F5F22720F20A8A9 (void);
// 0x000007C0 System.Void ClientManager::Update()
extern void ClientManager_Update_m457E07414871AE4B7E376EF218D5116E9D24AE4E (void);
// 0x000007C1 System.Void ClientManager::.ctor()
extern void ClientManager__ctor_mAC4A063EAA17352DF73913CB522DBF39A1D102BE (void);
// 0x000007C2 System.Void ClientManager::.cctor()
extern void ClientManager__cctor_m783D8D02E2732C18649B3403B4D88D5D53294988 (void);
// 0x000007C3 System.Collections.IEnumerator ClientManagerUI::Init()
extern void ClientManagerUI_Init_m6C53E13B2F02099F38BC949E8DA1956D2D801883 (void);
// 0x000007C4 System.Void ClientManagerUI::BackToClientSceneFromTestingScene()
extern void ClientManagerUI_BackToClientSceneFromTestingScene_m9A0AD57DDE8E135099847DC26A18DCAA316E8309 (void);
// 0x000007C5 System.Void ClientManagerUI::OpenBlueTooth(System.Object,System.EventArgs)
extern void ClientManagerUI_OpenBlueTooth_m4A97600EC257F2F9884BB6D08E4A979FF76817C7 (void);
// 0x000007C6 System.Void ClientManagerUI::OpenClientMenu(System.Object,System.EventArgs)
extern void ClientManagerUI_OpenClientMenu_m14B2EDE4C2D24F20603BDC77E3D13FE5CF847484 (void);
// 0x000007C7 System.Void ClientManagerUI::OpenFireType(System.Object,System.EventArgs)
extern void ClientManagerUI_OpenFireType_mD13E0EC82B70E981ACCC96DCA5352F7C99D14CA7 (void);
// 0x000007C8 System.Void ClientManagerUI::CloseFireType(System.Object,System.EventArgs)
extern void ClientManagerUI_CloseFireType_mD553092AE12FA204756BC165A64F9165A7DFF758 (void);
// 0x000007C9 System.Void ClientManagerUI::OpenFireExtinguisherType(System.Object,System.EventArgs)
extern void ClientManagerUI_OpenFireExtinguisherType_m8066C08243415F8AC293F7A0540DA287DD3EF325 (void);
// 0x000007CA System.Void ClientManagerUI::CloseFireExtinguisherType(System.Object,System.EventArgs)
extern void ClientManagerUI_CloseFireExtinguisherType_mAC1580F518620259C194AB1604EDFCC95548BD91 (void);
// 0x000007CB System.Void ClientManagerUI::OpenTutorial(System.Object,System.EventArgs)
extern void ClientManagerUI_OpenTutorial_m01CB6179A76D1D8D9694E7B4E887CE822DBE50D1 (void);
// 0x000007CC System.Void ClientManagerUI::CloseTutorial(System.Object,System.EventArgs)
extern void ClientManagerUI_CloseTutorial_m13599DC43B9C293807DCA313AEB5C640767F84D1 (void);
// 0x000007CD System.Void ClientManagerUI::OpenTesting(System.Object,System.EventArgs)
extern void ClientManagerUI_OpenTesting_m3A2D646838908E5C8F0CA21D157B7B965727BDA1 (void);
// 0x000007CE System.Void ClientManagerUI::CloseTesting(System.Object,System.EventArgs)
extern void ClientManagerUI_CloseTesting_m44966651F90226A1CC9ED04F1AFBD3EF43F45F9E (void);
// 0x000007CF System.Void ClientManagerUI::.ctor()
extern void ClientManagerUI__ctor_m0F2083392B0A1FA6BE1F9D922C4C1D8850BB535A (void);
// 0x000007D0 System.Void ClientManagerUI/<Init>d__8::.ctor(System.Int32)
extern void U3CInitU3Ed__8__ctor_m41B4C9710FAE4F482DF33EC5FA05FABF988D2B0F (void);
// 0x000007D1 System.Void ClientManagerUI/<Init>d__8::System.IDisposable.Dispose()
extern void U3CInitU3Ed__8_System_IDisposable_Dispose_mB01922B66FFA86C9A8562B6F33DA653BC6CED2B6 (void);
// 0x000007D2 System.Boolean ClientManagerUI/<Init>d__8::MoveNext()
extern void U3CInitU3Ed__8_MoveNext_m03A6410A1F945E81B99CBA03AC6C0521FE766897 (void);
// 0x000007D3 System.Object ClientManagerUI/<Init>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB2CF953FC40BC2D53DCEE0EF2E1DF56F084C2CC (void);
// 0x000007D4 System.Void ClientManagerUI/<Init>d__8::System.Collections.IEnumerator.Reset()
extern void U3CInitU3Ed__8_System_Collections_IEnumerator_Reset_m254BEF0ECFA3EBCD31C16644667216173E97DECE (void);
// 0x000007D5 System.Object ClientManagerUI/<Init>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CInitU3Ed__8_System_Collections_IEnumerator_get_Current_mA2B1581C289A8F80B83E0FE21203EDF1B3A635B1 (void);
// 0x000007D6 System.Void Panel_BlueToothConnection::add_EHOpenClientMenu(System.EventHandler)
extern void Panel_BlueToothConnection_add_EHOpenClientMenu_mC24D329E99943235A495CC07AEEF901E76F98B88 (void);
// 0x000007D7 System.Void Panel_BlueToothConnection::remove_EHOpenClientMenu(System.EventHandler)
extern void Panel_BlueToothConnection_remove_EHOpenClientMenu_mF55F6586F1CFC94DB1B4F5081273DEA0D037C8AA (void);
// 0x000007D8 System.Void Panel_BlueToothConnection::OnEnable()
extern void Panel_BlueToothConnection_OnEnable_m590C1B77FD677A79026E89CA0BB3AA0A748DEB63 (void);
// 0x000007D9 System.Void Panel_BlueToothConnection::Init()
extern void Panel_BlueToothConnection_Init_m159BEFC5DC9E7514C77B8A3D6566BC7D24F68648 (void);
// 0x000007DA System.Void Panel_BlueToothConnection::Update()
extern void Panel_BlueToothConnection_Update_m85E43DDF1BE68C1A26F0A52C1148A73A8E7A31CA (void);
// 0x000007DB System.Collections.IEnumerator Panel_BlueToothConnection::BlueToothConectionProgress()
extern void Panel_BlueToothConnection_BlueToothConectionProgress_m1CEC15509A0D8493312DA3DCCDDF699BE0A8C0DB (void);
// 0x000007DC System.Void Panel_BlueToothConnection::.ctor()
extern void Panel_BlueToothConnection__ctor_m71C905D020E5B93A8889BD09C5514AF9D8916BFB (void);
// 0x000007DD System.Void Panel_BlueToothConnection/<BlueToothConectionProgress>d__11::.ctor(System.Int32)
extern void U3CBlueToothConectionProgressU3Ed__11__ctor_m01BF3E098DA933CA379066D0423721A73DC0E3E8 (void);
// 0x000007DE System.Void Panel_BlueToothConnection/<BlueToothConectionProgress>d__11::System.IDisposable.Dispose()
extern void U3CBlueToothConectionProgressU3Ed__11_System_IDisposable_Dispose_mD936704747FF3F0FB42DDFF456BFD3A01F1157FF (void);
// 0x000007DF System.Boolean Panel_BlueToothConnection/<BlueToothConectionProgress>d__11::MoveNext()
extern void U3CBlueToothConectionProgressU3Ed__11_MoveNext_mBA888AB1F584DCEF3626224FE0601EBD69981BAC (void);
// 0x000007E0 System.Object Panel_BlueToothConnection/<BlueToothConectionProgress>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBlueToothConectionProgressU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m298ADFBA0EFEE51DCBC7EC12DCFB53ED45FC4584 (void);
// 0x000007E1 System.Void Panel_BlueToothConnection/<BlueToothConectionProgress>d__11::System.Collections.IEnumerator.Reset()
extern void U3CBlueToothConectionProgressU3Ed__11_System_Collections_IEnumerator_Reset_mFAEEE761A5A341D3BDC2B2B591CBC98E3CB57BB3 (void);
// 0x000007E2 System.Object Panel_BlueToothConnection/<BlueToothConectionProgress>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CBlueToothConectionProgressU3Ed__11_System_Collections_IEnumerator_get_Current_m182D5CCB96349DC360874DFDB3EF56A7EB2F58DE (void);
// 0x000007E3 System.Void Panel_ClientMenu::add_EHOpenFireType(System.EventHandler)
extern void Panel_ClientMenu_add_EHOpenFireType_m1F12490A13502E3B33CE46710DF47F045FEF137B (void);
// 0x000007E4 System.Void Panel_ClientMenu::remove_EHOpenFireType(System.EventHandler)
extern void Panel_ClientMenu_remove_EHOpenFireType_m93D0788D4BAD47F36DE5509745F4FF436CBA08F0 (void);
// 0x000007E5 System.Void Panel_ClientMenu::add_EHOpenFireExtinguisherType(System.EventHandler)
extern void Panel_ClientMenu_add_EHOpenFireExtinguisherType_mFD8A7BC7FC3BC9C85467388872878CA91FB15D6D (void);
// 0x000007E6 System.Void Panel_ClientMenu::remove_EHOpenFireExtinguisherType(System.EventHandler)
extern void Panel_ClientMenu_remove_EHOpenFireExtinguisherType_mDE2FA761FAF765F0FF960E5CDD4FBE30330018B5 (void);
// 0x000007E7 System.Void Panel_ClientMenu::add_EHOpenTutorial(System.EventHandler)
extern void Panel_ClientMenu_add_EHOpenTutorial_m971A7ABECC662140214B88DFF3611BF5FDBD64D8 (void);
// 0x000007E8 System.Void Panel_ClientMenu::remove_EHOpenTutorial(System.EventHandler)
extern void Panel_ClientMenu_remove_EHOpenTutorial_m62DAAD720BF64A88A3DF207BFE03CAEFA375C696 (void);
// 0x000007E9 System.Void Panel_ClientMenu::add_EHOpenTesting(System.EventHandler)
extern void Panel_ClientMenu_add_EHOpenTesting_m002F7F6D3B9CC2E15E43F8206C4277632AA1B01C (void);
// 0x000007EA System.Void Panel_ClientMenu::remove_EHOpenTesting(System.EventHandler)
extern void Panel_ClientMenu_remove_EHOpenTesting_m4E6987D627D99CD1768D77770F70955546157081 (void);
// 0x000007EB System.Void Panel_ClientMenu::OnEnable()
extern void Panel_ClientMenu_OnEnable_m778760271D7701D67863F5CFB616DA43EDE88601 (void);
// 0x000007EC System.Void Panel_ClientMenu::Init()
extern void Panel_ClientMenu_Init_m603E66406B70FC964330598B17937B6E656B29A0 (void);
// 0x000007ED System.Void Panel_ClientMenu::IntroductionMenu()
extern void Panel_ClientMenu_IntroductionMenu_m07093709B05A05ED082D86D7400209B8587DCD49 (void);
// 0x000007EE System.Void Panel_ClientMenu::MainMenu()
extern void Panel_ClientMenu_MainMenu_m96BF5BBBF95D5AFF4BF1CA4B2D47A4DE789842D4 (void);
// 0x000007EF System.Void Panel_ClientMenu::FireType()
extern void Panel_ClientMenu_FireType_m0679EC5126DF795BCE9209E7240D75B9D8204138 (void);
// 0x000007F0 System.Void Panel_ClientMenu::FireExtinguisherType()
extern void Panel_ClientMenu_FireExtinguisherType_m2C4CB776ABD2DA08D00B2BAE8299086FAC994351 (void);
// 0x000007F1 System.Void Panel_ClientMenu::Tutorial()
extern void Panel_ClientMenu_Tutorial_mB8B8E134DD25350747B57DE2405437CD8DBB940F (void);
// 0x000007F2 System.Void Panel_ClientMenu::EHTesting()
extern void Panel_ClientMenu_EHTesting_m32D55FB90B8386DF0A11C99E6A44254EB023A8E6 (void);
// 0x000007F3 System.Void Panel_ClientMenu::.ctor()
extern void Panel_ClientMenu__ctor_m73279859DDBD31F9D04716E5162ABF31C6B955DB (void);
// 0x000007F4 System.Void Panel_ClientMenu::<OnEnable>b__18_0()
extern void Panel_ClientMenu_U3COnEnableU3Eb__18_0_mB4E022B442BB744420D71FCBE4A13242395FAB3C (void);
// 0x000007F5 System.Void Panel_ClientMenu::<OnEnable>b__18_1()
extern void Panel_ClientMenu_U3COnEnableU3Eb__18_1_m2C9FDA45A3E576181439CF52059A1936A791547F (void);
// 0x000007F6 System.Void Panel_ClientMenu::<OnEnable>b__18_2()
extern void Panel_ClientMenu_U3COnEnableU3Eb__18_2_m2FBDCE613F6F3689D7D839549CAAD405B57AE7E1 (void);
// 0x000007F7 System.Void Panel_ClientMenu::<OnEnable>b__18_3()
extern void Panel_ClientMenu_U3COnEnableU3Eb__18_3_mE7E8545C5AD1B5184135BD5023B6ED120930F85D (void);
// 0x000007F8 System.Void Panel_ClientMenu::<OnEnable>b__18_4()
extern void Panel_ClientMenu_U3COnEnableU3Eb__18_4_m04A18B1F32CDB7DCEBAB12C729A009221EAF5935 (void);
// 0x000007F9 System.Void Panel_ClientMenu::<OnEnable>b__18_5()
extern void Panel_ClientMenu_U3COnEnableU3Eb__18_5_m6BD88BB52DA83623A0707E72FAC03E47A4E25401 (void);
// 0x000007FA System.Void Panel_FireExtinguisherTypeIntroduction::add_EHCloseFireExtinguisherType(System.EventHandler)
extern void Panel_FireExtinguisherTypeIntroduction_add_EHCloseFireExtinguisherType_m70250C7EA75648C9AD19CED9865F6BD05DBCD9E2 (void);
// 0x000007FB System.Void Panel_FireExtinguisherTypeIntroduction::remove_EHCloseFireExtinguisherType(System.EventHandler)
extern void Panel_FireExtinguisherTypeIntroduction_remove_EHCloseFireExtinguisherType_mACBA17461D0B9A616F5954AB2337288C58703885 (void);
// 0x000007FC System.Void Panel_FireExtinguisherTypeIntroduction::OnEnable()
extern void Panel_FireExtinguisherTypeIntroduction_OnEnable_m19FDB7FBC77A6FAFE16A78D25337F205E37912D2 (void);
// 0x000007FD System.Void Panel_FireExtinguisherTypeIntroduction::Init()
extern void Panel_FireExtinguisherTypeIntroduction_Init_mF660D630B576FA5E567CB533C8EC18D2659D51D2 (void);
// 0x000007FE System.Void Panel_FireExtinguisherTypeIntroduction::FireTypeChange(UnityEngine.UI.Toggle)
extern void Panel_FireExtinguisherTypeIntroduction_FireTypeChange_m198387A1168F8EFAEA975455D531FFF6DFE52FAE (void);
// 0x000007FF System.Collections.IEnumerator Panel_FireExtinguisherTypeIntroduction::BackToMenu()
extern void Panel_FireExtinguisherTypeIntroduction_BackToMenu_m3216871D862A632EBDD1BF4BE9F53D8B6888C727 (void);
// 0x00000800 System.Void Panel_FireExtinguisherTypeIntroduction::.ctor()
extern void Panel_FireExtinguisherTypeIntroduction__ctor_m6E2532AAD8E47F84798C582DE0A4AA5F740BBFC1 (void);
// 0x00000801 System.Void Panel_FireExtinguisherTypeIntroduction::<OnEnable>b__13_0()
extern void Panel_FireExtinguisherTypeIntroduction_U3COnEnableU3Eb__13_0_m4E9A97BA43505B731F79688B2CE2A5FE50C537F9 (void);
// 0x00000802 System.Void Panel_FireExtinguisherTypeIntroduction/FireExtinguisherType::.ctor()
extern void FireExtinguisherType__ctor_mD1FE9920CE475DB069329C5A41B44149478AA0E7 (void);
// 0x00000803 System.Void Panel_FireExtinguisherTypeIntroduction/Board::.ctor()
extern void Board__ctor_mCAF81E135B44EEBE92F9AA73F20075CFE14F887C (void);
// 0x00000804 System.Void Panel_FireExtinguisherTypeIntroduction/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m2243666E2E1AC6C1E64C85AF82F518A309784B80 (void);
// 0x00000805 System.Void Panel_FireExtinguisherTypeIntroduction/<>c__DisplayClass13_0::<OnEnable>b__1(System.Boolean)
extern void U3CU3Ec__DisplayClass13_0_U3COnEnableU3Eb__1_mBB6AEF8FCEB5E9F87E1B5F9D92DB026C7C802F41 (void);
// 0x00000806 System.Void Panel_FireExtinguisherTypeIntroduction/<BackToMenu>d__17::.ctor(System.Int32)
extern void U3CBackToMenuU3Ed__17__ctor_m5EB2B50782BFBDDB3F59135355A8BFA1D63AB42B (void);
// 0x00000807 System.Void Panel_FireExtinguisherTypeIntroduction/<BackToMenu>d__17::System.IDisposable.Dispose()
extern void U3CBackToMenuU3Ed__17_System_IDisposable_Dispose_mA3C89B679718D465E60E5854B721EB3C750329FD (void);
// 0x00000808 System.Boolean Panel_FireExtinguisherTypeIntroduction/<BackToMenu>d__17::MoveNext()
extern void U3CBackToMenuU3Ed__17_MoveNext_m424F51A3C15F4C1C888940EC36960A6EB3CDD352 (void);
// 0x00000809 System.Object Panel_FireExtinguisherTypeIntroduction/<BackToMenu>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBackToMenuU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B1D697678AC7EB50CF9A5CDB9D343BDF75B0002 (void);
// 0x0000080A System.Void Panel_FireExtinguisherTypeIntroduction/<BackToMenu>d__17::System.Collections.IEnumerator.Reset()
extern void U3CBackToMenuU3Ed__17_System_Collections_IEnumerator_Reset_m2CF6EDD8F56899E97B535669FC523E3C450987B3 (void);
// 0x0000080B System.Object Panel_FireExtinguisherTypeIntroduction/<BackToMenu>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CBackToMenuU3Ed__17_System_Collections_IEnumerator_get_Current_mF51C96CD8F5704CFA827F29A8866F5249F35EAC8 (void);
// 0x0000080C System.Void Panel_FireTypeIntroduction::add_EHCloseFireType(System.EventHandler)
extern void Panel_FireTypeIntroduction_add_EHCloseFireType_mB3F6C66ED424FBF5BC76A541036AE96BA6708DC4 (void);
// 0x0000080D System.Void Panel_FireTypeIntroduction::remove_EHCloseFireType(System.EventHandler)
extern void Panel_FireTypeIntroduction_remove_EHCloseFireType_m6086A58798CCBA3DD40C487BA58D89E7E6AFA325 (void);
// 0x0000080E System.Void Panel_FireTypeIntroduction::OnEnable()
extern void Panel_FireTypeIntroduction_OnEnable_m712837B975684B938FDC19BE225D0678ACBB9BF7 (void);
// 0x0000080F System.Void Panel_FireTypeIntroduction::Init()
extern void Panel_FireTypeIntroduction_Init_m14419696CAE5E61A5B222C53838ADE9410D19ED2 (void);
// 0x00000810 System.Void Panel_FireTypeIntroduction::FireTypeChange(UnityEngine.UI.Toggle)
extern void Panel_FireTypeIntroduction_FireTypeChange_mD83F23C885F8BF5937B1D59DC41D1570F23E01C4 (void);
// 0x00000811 System.Collections.IEnumerator Panel_FireTypeIntroduction::BackToMenu()
extern void Panel_FireTypeIntroduction_BackToMenu_m0252E7C01B7F4C807E5D454B2043A929FD3E1EC2 (void);
// 0x00000812 System.Void Panel_FireTypeIntroduction::.ctor()
extern void Panel_FireTypeIntroduction__ctor_m9F02DA2EAD0012CD612826BD885B20D6053756A8 (void);
// 0x00000813 System.Void Panel_FireTypeIntroduction::<OnEnable>b__11_0()
extern void Panel_FireTypeIntroduction_U3COnEnableU3Eb__11_0_m889CCEEBE3DA47462F470F54215FBA5B894F853E (void);
// 0x00000814 System.Void Panel_FireTypeIntroduction/FireType::.ctor()
extern void FireType__ctor_m5BC653B019BF5411AD9F45315AD28FAAD5AC0431 (void);
// 0x00000815 System.Void Panel_FireTypeIntroduction/Board::.ctor()
extern void Board__ctor_m56730F9EC20145A28995C56D68B85A48F97F18CF (void);
// 0x00000816 System.Void Panel_FireTypeIntroduction/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m647E1ADC4584DAEC22E9AEA545B4B12B48355200 (void);
// 0x00000817 System.Void Panel_FireTypeIntroduction/<>c__DisplayClass11_0::<OnEnable>b__1(System.Boolean)
extern void U3CU3Ec__DisplayClass11_0_U3COnEnableU3Eb__1_m477ED89C12CA61B5DD88CD455725B3DC725A9C40 (void);
// 0x00000818 System.Void Panel_FireTypeIntroduction/<BackToMenu>d__15::.ctor(System.Int32)
extern void U3CBackToMenuU3Ed__15__ctor_m139127FED6B85A1E2CB003B51642B4098183BE3D (void);
// 0x00000819 System.Void Panel_FireTypeIntroduction/<BackToMenu>d__15::System.IDisposable.Dispose()
extern void U3CBackToMenuU3Ed__15_System_IDisposable_Dispose_m71E53129DA703A82C8871C459B6AE736FADDD831 (void);
// 0x0000081A System.Boolean Panel_FireTypeIntroduction/<BackToMenu>d__15::MoveNext()
extern void U3CBackToMenuU3Ed__15_MoveNext_m28D7989BBCFD6524F111C75D4A1F00B3F208CAA3 (void);
// 0x0000081B System.Object Panel_FireTypeIntroduction/<BackToMenu>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBackToMenuU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C1C0E7F1E742F622CB4E35C20D338626B932722 (void);
// 0x0000081C System.Void Panel_FireTypeIntroduction/<BackToMenu>d__15::System.Collections.IEnumerator.Reset()
extern void U3CBackToMenuU3Ed__15_System_Collections_IEnumerator_Reset_m60C93D4119AFFC64CEB5FEC3E20CC9379CBA5E4C (void);
// 0x0000081D System.Object Panel_FireTypeIntroduction/<BackToMenu>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CBackToMenuU3Ed__15_System_Collections_IEnumerator_get_Current_m107D38B598E9D32DDF6F23BBCB8E200AFFFE1E19 (void);
// 0x0000081E System.Void Panel_IPConnection::add_EHOpenBlueToothPanel(System.EventHandler)
extern void Panel_IPConnection_add_EHOpenBlueToothPanel_m3ED6F64311871D89F10B9EF42429BF85D6760499 (void);
// 0x0000081F System.Void Panel_IPConnection::remove_EHOpenBlueToothPanel(System.EventHandler)
extern void Panel_IPConnection_remove_EHOpenBlueToothPanel_m6BB351BA431529054767EB1281816F0E404B94E0 (void);
// 0x00000820 System.Void Panel_IPConnection::OnEnable()
extern void Panel_IPConnection_OnEnable_mE1F1EE0BD5B7FD857AEAFE8DB3DA8BA380FB5AAC (void);
// 0x00000821 System.Void Panel_IPConnection::Init()
extern void Panel_IPConnection_Init_mB21EA518D0B459C8E1167910724C3348EB49492F (void);
// 0x00000822 System.Void Panel_IPConnection::ClickOK()
extern void Panel_IPConnection_ClickOK_m7A517CCEA0EB87B7C4ABF501BEB6F0487BD8508B (void);
// 0x00000823 System.Collections.IEnumerator Panel_IPConnection::Connect()
extern void Panel_IPConnection_Connect_m6FFBCFB8F9FB0E28BB8969BEE095651044A6FE87 (void);
// 0x00000824 System.Void Panel_IPConnection::ClickNum(UnityEngine.UI.Button)
extern void Panel_IPConnection_ClickNum_m0F0B841A7F27332F4B3EC2F8E3CD769CA58F8AD3 (void);
// 0x00000825 System.Void Panel_IPConnection::ClickDot()
extern void Panel_IPConnection_ClickDot_mC937D7AFD68AD24C8AC03BEE496888682A143A67 (void);
// 0x00000826 System.Void Panel_IPConnection::ClickBack()
extern void Panel_IPConnection_ClickBack_mB95E51D2297CA3271B6418350D2AB61D4D76658A (void);
// 0x00000827 System.Void Panel_IPConnection::.ctor()
extern void Panel_IPConnection__ctor_m99B7D2ED0535A4F80AC172F1F297FC7AF04CC4AB (void);
// 0x00000828 System.Void Panel_IPConnection::<OnEnable>b__11_0()
extern void Panel_IPConnection_U3COnEnableU3Eb__11_0_m3C5686BB46BA275284584E76DCBEDF8EF1451475 (void);
// 0x00000829 System.Void Panel_IPConnection::<OnEnable>b__11_1()
extern void Panel_IPConnection_U3COnEnableU3Eb__11_1_m81F252BD267551CF8D7EE5241DAB2C12F8E7223F (void);
// 0x0000082A System.Void Panel_IPConnection::<OnEnable>b__11_2()
extern void Panel_IPConnection_U3COnEnableU3Eb__11_2_mDBE36F5D073C32BEFDED1028961E4D453DAB9AC0 (void);
// 0x0000082B System.Void Panel_IPConnection/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m862EBF53643A74C164649D0568AB88499EB41D22 (void);
// 0x0000082C System.Void Panel_IPConnection/<>c__DisplayClass11_0::<OnEnable>b__3()
extern void U3CU3Ec__DisplayClass11_0_U3COnEnableU3Eb__3_m2EF425A819D06D3BE34DE0AD9E2C4821C9A95641 (void);
// 0x0000082D System.Void Panel_IPConnection/<Connect>d__14::.ctor(System.Int32)
extern void U3CConnectU3Ed__14__ctor_mE7DEB2DA2327BC1920528E104F0D0BA5C48573F5 (void);
// 0x0000082E System.Void Panel_IPConnection/<Connect>d__14::System.IDisposable.Dispose()
extern void U3CConnectU3Ed__14_System_IDisposable_Dispose_m0A30C01F2EEF3B71D9A39FF87D1CD7896FDDC864 (void);
// 0x0000082F System.Boolean Panel_IPConnection/<Connect>d__14::MoveNext()
extern void U3CConnectU3Ed__14_MoveNext_m707D3F0466D6A62179BB27BC3C4DD2AAD5A53B63 (void);
// 0x00000830 System.Object Panel_IPConnection/<Connect>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConnectU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC83A6B4013CD244022DD6AA926A5D413F22FF2CD (void);
// 0x00000831 System.Void Panel_IPConnection/<Connect>d__14::System.Collections.IEnumerator.Reset()
extern void U3CConnectU3Ed__14_System_Collections_IEnumerator_Reset_m0C8BFAE71C8ADA2430D49E7B97F3D131FE99F6DB (void);
// 0x00000832 System.Object Panel_IPConnection/<Connect>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CConnectU3Ed__14_System_Collections_IEnumerator_get_Current_m263BCD911AB490ABB52DF1F4A06EF7C5BFBEB5E7 (void);
// 0x00000833 System.Void Panel_Testing::add_EHCloseTesting(System.EventHandler)
extern void Panel_Testing_add_EHCloseTesting_m2D5A085B194DB435F2CD3AE5DF271BE6BD21E796 (void);
// 0x00000834 System.Void Panel_Testing::remove_EHCloseTesting(System.EventHandler)
extern void Panel_Testing_remove_EHCloseTesting_mAB689057627A6B4532E65E42BAC749739137927B (void);
// 0x00000835 System.Void Panel_Testing::OnEnable()
extern void Panel_Testing_OnEnable_m5AE6508B54EFE6E20A32570AC06A447818713209 (void);
// 0x00000836 System.Void Panel_Testing::Init()
extern void Panel_Testing_Init_m4AB1F57E93BF112755A26D3E665D87CF6A70D642 (void);
// 0x00000837 System.Void Panel_Testing::OnClick(Panel_Testing/ChooseScene)
extern void Panel_Testing_OnClick_mF0DBA53075F52F805EB82AE13308104E2589F353 (void);
// 0x00000838 System.Void Panel_Testing::BackToMenu()
extern void Panel_Testing_BackToMenu_m4E93FB0049B08CBBFA8F3A0D75319A8D62FA3C6A (void);
// 0x00000839 System.Void Panel_Testing::.ctor()
extern void Panel_Testing__ctor_mA2DB7C1012EFB7FBE8FE9B61339B66D5A5C01735 (void);
// 0x0000083A System.Void Panel_Testing::<OnEnable>b__6_0()
extern void Panel_Testing_U3COnEnableU3Eb__6_0_mFF330C7C4BA29DE4FB0C10C06F658BC4D16A8729 (void);
// 0x0000083B System.Void Panel_Testing/ChooseScene::.ctor()
extern void ChooseScene__ctor_m8261B0B1064E4D1412AA754C64669AEC0855E8F5 (void);
// 0x0000083C System.Void Panel_Testing/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m9048AD7F7DE3C4F70B4809AE8F2268401CD70404 (void);
// 0x0000083D System.Void Panel_Testing/<>c__DisplayClass6_0::<OnEnable>b__1()
extern void U3CU3Ec__DisplayClass6_0_U3COnEnableU3Eb__1_mC724D04C5473CA9EE224D1C2B0925EA84E625581 (void);
// 0x0000083E System.Void Panel_Tutorial::add_EHCloseTutorial(System.EventHandler)
extern void Panel_Tutorial_add_EHCloseTutorial_m885D08A61B99EB73A3FF57B152596CB8BE26E1E5 (void);
// 0x0000083F System.Void Panel_Tutorial::remove_EHCloseTutorial(System.EventHandler)
extern void Panel_Tutorial_remove_EHCloseTutorial_m00F5190BA6D5819D570BBD2ABD18FC453D7B8565 (void);
// 0x00000840 System.Void Panel_Tutorial::OnEnable()
extern void Panel_Tutorial_OnEnable_m199629279E7E08D74AF33E10DA4EB0FD0333A79F (void);
// 0x00000841 System.Void Panel_Tutorial::Init()
extern void Panel_Tutorial_Init_m0BBF57DCE10DEB22A12E9A8978E786D70EE72FD0 (void);
// 0x00000842 System.Void Panel_Tutorial::ChangeState(System.Int32)
extern void Panel_Tutorial_ChangeState_m1E717D0BD6B6F5248B444ED05B5A8F1146400BEE (void);
// 0x00000843 System.Void Panel_Tutorial::PlayVoice(System.Int32)
extern void Panel_Tutorial_PlayVoice_mA2D92771119D88CCF5F749138B4F202495F67353 (void);
// 0x00000844 System.Collections.IEnumerator Panel_Tutorial::StateProgress()
extern void Panel_Tutorial_StateProgress_m10B81AF1913FD7BDB15D65334FD964958E74096E (void);
// 0x00000845 System.Void Panel_Tutorial::Update()
extern void Panel_Tutorial_Update_m32A4FAF3ED38EF69BCE809D60D1C6B8338AB76FF (void);
// 0x00000846 System.Collections.IEnumerator Panel_Tutorial::BackToMenu()
extern void Panel_Tutorial_BackToMenu_mA17165D63698E0F8FC4C44176BEA2082406740B5 (void);
// 0x00000847 System.Void Panel_Tutorial::.ctor()
extern void Panel_Tutorial__ctor_m6636F4237C157E2A38FCB778DB778AABCDC7EE3C (void);
// 0x00000848 System.Void Panel_Tutorial::<OnEnable>b__21_0()
extern void Panel_Tutorial_U3COnEnableU3Eb__21_0_m92BC49055BA1DCB362B73000DA924C0DCB7DFC7A (void);
// 0x00000849 System.Void Panel_Tutorial/ProgressState::.ctor()
extern void ProgressState__ctor_mBAE105CDFC15FA7A0BDF2ECE7B81D6769C29AB64 (void);
// 0x0000084A System.Void Panel_Tutorial/<StateProgress>d__25::.ctor(System.Int32)
extern void U3CStateProgressU3Ed__25__ctor_mA55D19F99728EF1C1DC364BDAB2F2C31A735B198 (void);
// 0x0000084B System.Void Panel_Tutorial/<StateProgress>d__25::System.IDisposable.Dispose()
extern void U3CStateProgressU3Ed__25_System_IDisposable_Dispose_mB5E9634F972993FBC4733B03F2050AF58AD4177F (void);
// 0x0000084C System.Boolean Panel_Tutorial/<StateProgress>d__25::MoveNext()
extern void U3CStateProgressU3Ed__25_MoveNext_m4B096EAB8612BB5B50A50B197789F8F401E15555 (void);
// 0x0000084D System.Object Panel_Tutorial/<StateProgress>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStateProgressU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43A691FF45B15C1411322E182C2ADAA28D4B186A (void);
// 0x0000084E System.Void Panel_Tutorial/<StateProgress>d__25::System.Collections.IEnumerator.Reset()
extern void U3CStateProgressU3Ed__25_System_Collections_IEnumerator_Reset_mFE7E1FCB2BD5C8673D69A691B1E204FFA3E28778 (void);
// 0x0000084F System.Object Panel_Tutorial/<StateProgress>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CStateProgressU3Ed__25_System_Collections_IEnumerator_get_Current_mEDD9C00E13CBB77795F3063C422A4FB40EAC5011 (void);
// 0x00000850 System.Void Panel_Tutorial/<BackToMenu>d__28::.ctor(System.Int32)
extern void U3CBackToMenuU3Ed__28__ctor_mCFD9249726D23513A5E1F3A8B11AEE464D91A587 (void);
// 0x00000851 System.Void Panel_Tutorial/<BackToMenu>d__28::System.IDisposable.Dispose()
extern void U3CBackToMenuU3Ed__28_System_IDisposable_Dispose_m903836E31DFC665AFB87CB8428790301EE84C46B (void);
// 0x00000852 System.Boolean Panel_Tutorial/<BackToMenu>d__28::MoveNext()
extern void U3CBackToMenuU3Ed__28_MoveNext_mE08361449256D33A91C795408E9C1D8973CE6B21 (void);
// 0x00000853 System.Object Panel_Tutorial/<BackToMenu>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBackToMenuU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920BD50906164146F37AC70F04EA7E69A0AE63E9 (void);
// 0x00000854 System.Void Panel_Tutorial/<BackToMenu>d__28::System.Collections.IEnumerator.Reset()
extern void U3CBackToMenuU3Ed__28_System_Collections_IEnumerator_Reset_mC2F4113D63E94175EE29C9594D7DA81D7F2C3D24 (void);
// 0x00000855 System.Object Panel_Tutorial/<BackToMenu>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CBackToMenuU3Ed__28_System_Collections_IEnumerator_get_Current_mA88ADCDC8A8824B6EBC8A91C80FE0233CA3E9F8D (void);
// 0x00000856 System.Void Panel_Extinguishing::OnEnable()
extern void Panel_Extinguishing_OnEnable_m8E47320B61134B757C0A19CCEBF0220924C7209D (void);
// 0x00000857 System.Collections.IEnumerator Panel_Extinguishing::BackToMainScene()
extern void Panel_Extinguishing_BackToMainScene_m91E877EDFDA971F2008E02F8DC09FAEB412D5756 (void);
// 0x00000858 System.Void Panel_Extinguishing::Update()
extern void Panel_Extinguishing_Update_m9A5D322E37D657F9D8A715DF137E8BE748301F8B (void);
// 0x00000859 System.Void Panel_Extinguishing::.ctor()
extern void Panel_Extinguishing__ctor_m47D881C0C3A6A4256BFD6C69C822A4105DB7D9BD (void);
// 0x0000085A System.Void Panel_Extinguishing::<OnEnable>b__17_0()
extern void Panel_Extinguishing_U3COnEnableU3Eb__17_0_mA022839CEC499F7ADD7B69BDC79101E2801D7A8E (void);
// 0x0000085B System.Void Panel_Extinguishing/<BackToMainScene>d__19::.ctor(System.Int32)
extern void U3CBackToMainSceneU3Ed__19__ctor_m91F54967A8EB4D4D06942E47E8704DC149F9E2AB (void);
// 0x0000085C System.Void Panel_Extinguishing/<BackToMainScene>d__19::System.IDisposable.Dispose()
extern void U3CBackToMainSceneU3Ed__19_System_IDisposable_Dispose_m087200B439C68408557830A216F518216C8F3919 (void);
// 0x0000085D System.Boolean Panel_Extinguishing/<BackToMainScene>d__19::MoveNext()
extern void U3CBackToMainSceneU3Ed__19_MoveNext_m7D00D08C05599A9066DD3E044129949081A09ACE (void);
// 0x0000085E System.Object Panel_Extinguishing/<BackToMainScene>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBackToMainSceneU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2851117CF1AF75D37B4B679D90AD29294B674C5F (void);
// 0x0000085F System.Void Panel_Extinguishing/<BackToMainScene>d__19::System.Collections.IEnumerator.Reset()
extern void U3CBackToMainSceneU3Ed__19_System_Collections_IEnumerator_Reset_m4E818025912DA085DF1BEFF5B655F8FCC2371901 (void);
// 0x00000860 System.Object Panel_Extinguishing/<BackToMainScene>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CBackToMainSceneU3Ed__19_System_Collections_IEnumerator_get_Current_mB52FA104A877B14A3D25F2B772B3F9FE1FEFB683 (void);
// 0x00000861 System.Void Scene_Manager::Start()
extern void Scene_Manager_Start_mFC00DAC848A94FB84F305C1A6C259141E173E0E1 (void);
// 0x00000862 System.Void Scene_Manager::SwitchFireExtinguisher(DKP.VR.HandsVisualization/DisplayMode)
extern void Scene_Manager_SwitchFireExtinguisher_m687233F02853ED82EA456057A53E171AF759CF16 (void);
// 0x00000863 System.Void Scene_Manager::Update()
extern void Scene_Manager_Update_mD499C044AEF444A2B1DC5FB953859CCEB7521674 (void);
// 0x00000864 System.Void Scene_Manager::.ctor()
extern void Scene_Manager__ctor_m1AF5F8B64587F0C59862025A9D14510A934C2015 (void);
// 0x00000865 System.Void Scene_Manager::.cctor()
extern void Scene_Manager__cctor_m799737852231F58FB038939198E585B3BD13F33D (void);
// 0x00000866 System.Void Panel_AllInOne::add_EHCloseAllInOne(System.EventHandler)
extern void Panel_AllInOne_add_EHCloseAllInOne_m65626A3BB4CF4110B13AD9BBB334F32B219DC1CE (void);
// 0x00000867 System.Void Panel_AllInOne::remove_EHCloseAllInOne(System.EventHandler)
extern void Panel_AllInOne_remove_EHCloseAllInOne_m5B22F1C0B00A5570A7E0EAF938E93B2A36721978 (void);
// 0x00000868 System.Void Panel_AllInOne::OnEnable()
extern void Panel_AllInOne_OnEnable_m98520E69B161718260E45FF3F2170F1D3229BDFB (void);
// 0x00000869 System.Void Panel_AllInOne::OnDisable()
extern void Panel_AllInOne_OnDisable_m6BA22BE851DD56B5A11D10EC1B14206E3FD3C15A (void);
// 0x0000086A System.Void Panel_AllInOne::Init()
extern void Panel_AllInOne_Init_mA7E93524FC07F607E1AA76537FECFF40A744BAA4 (void);
// 0x0000086B System.Void Panel_AllInOne::Action_ProcessStringData(System.String)
extern void Panel_AllInOne_Action_ProcessStringData_m7F8D84C8E7F161C4DC16372C71E7CF2FDD46BF88 (void);
// 0x0000086C System.Void Panel_AllInOne::Update()
extern void Panel_AllInOne_Update_m25A1A47C37B24A2F057EDE53AB404743A2D8E289 (void);
// 0x0000086D System.Void Panel_AllInOne::BackToMenu()
extern void Panel_AllInOne_BackToMenu_m7CA1B61CC39A504F5E417999690C0E78264E118D (void);
// 0x0000086E System.Void Panel_AllInOne::Sync()
extern void Panel_AllInOne_Sync_m03C1D2BADAAF7AB5B1DC95E0EEEAFD51A2FBA312 (void);
// 0x0000086F System.Void Panel_AllInOne::QuitSync()
extern void Panel_AllInOne_QuitSync_m0182216EFDF5799F2A7CDD2B580319864F88C2C6 (void);
// 0x00000870 System.String Panel_AllInOne::GetLocalIPAddress()
extern void Panel_AllInOne_GetLocalIPAddress_m1F69E8243C29FB15AE5BA484F85AF87A1771C26D (void);
// 0x00000871 System.Void Panel_AllInOne::.ctor()
extern void Panel_AllInOne__ctor_mAFA60210C320A40F899129CE6E7EAA428DA88876 (void);
// 0x00000872 System.Void Panel_AllInOne::.cctor()
extern void Panel_AllInOne__cctor_mD4751A913D4CDE552D8D4C53DE6723052D47EE4B (void);
// 0x00000873 System.Void Panel_AllInOne::<OnEnable>b__13_0()
extern void Panel_AllInOne_U3COnEnableU3Eb__13_0_mB0DE17C829A8F30591B8921BA811DC6747BB5ADE (void);
// 0x00000874 System.Void Panel_AllInOne::<OnEnable>b__13_1()
extern void Panel_AllInOne_U3COnEnableU3Eb__13_1_m5273AC4E0B79BDE9B5DDE8A9861FCFC10F04DF31 (void);
// 0x00000875 System.Void Panel_AllInOne::<OnEnable>b__13_2()
extern void Panel_AllInOne_U3COnEnableU3Eb__13_2_m62CD6D18DBA99ED8C2825A06792AB7B6E85ACC88 (void);
// 0x00000876 System.Void Panel_DataRecord::add_EHCloseDataRecord(System.EventHandler)
extern void Panel_DataRecord_add_EHCloseDataRecord_mCC095FF201C79024C6A9A708A1C44EACF59E7300 (void);
// 0x00000877 System.Void Panel_DataRecord::remove_EHCloseDataRecord(System.EventHandler)
extern void Panel_DataRecord_remove_EHCloseDataRecord_m2D4FA24071B451858F508706C42B362BAED456E0 (void);
// 0x00000878 System.Void Panel_DataRecord::OnEnable()
extern void Panel_DataRecord_OnEnable_m0EB29226BCCD094DC5A61A71EDAC201EB14D8B9B (void);
// 0x00000879 System.Void Panel_DataRecord::BackToMenu()
extern void Panel_DataRecord_BackToMenu_mB34DF035DFAD9DA4A82BE2EA8B3FF73D7A37757B (void);
// 0x0000087A System.Void Panel_DataRecord::FireExtinguisherSearchOnclick(System.Int32)
extern void Panel_DataRecord_FireExtinguisherSearchOnclick_m6A7C2B7F4D1F766F1B130E10DC39073F17F73618 (void);
// 0x0000087B System.Collections.IEnumerator Panel_DataRecord::FFireExtinguisherList_Search(GPTT.Data.API/Search,System.Action`1<GPTT.Data.API/User>,System.Action`1<System.String>)
extern void Panel_DataRecord_FFireExtinguisherList_Search_mF9C8651EF88A1E56FCC4496C805658BEB2E4CF31 (void);
// 0x0000087C System.Void Panel_DataRecord::MatchData(System.Collections.Generic.List`1<GPTT.Data.API/FireExtinguisher>)
extern void Panel_DataRecord_MatchData_m1C114B94972458986BE3493565751067E2CD7BB7 (void);
// 0x0000087D System.Void Panel_DataRecord::FirstPage()
extern void Panel_DataRecord_FirstPage_m29EBB0B6675E973EB6B7EDA3FF64BD936F2B5D72 (void);
// 0x0000087E System.Void Panel_DataRecord::PreviousPage()
extern void Panel_DataRecord_PreviousPage_mF3B2E215F6FDEFB9FD349BCBC692C570A9DCFB86 (void);
// 0x0000087F System.Void Panel_DataRecord::LastPage()
extern void Panel_DataRecord_LastPage_mF1E01CE4B09086C99B4B0053662D24AA55D67B31 (void);
// 0x00000880 System.Void Panel_DataRecord::NextPage()
extern void Panel_DataRecord_NextPage_mB1C464EA751F3E732AD155C35DAD3F0C95C015A6 (void);
// 0x00000881 System.Void Panel_DataRecord::ClickTargetPages(UnityEngine.UI.Button)
extern void Panel_DataRecord_ClickTargetPages_m62FCA4EECFDDB7E17F5658788ED7F7488AFD51C2 (void);
// 0x00000882 System.Void Panel_DataRecord::UpdatePages()
extern void Panel_DataRecord_UpdatePages_m18599772233E25EB66A582A5F6A9018FBBCDD822 (void);
// 0x00000883 System.Void Panel_DataRecord::.ctor()
extern void Panel_DataRecord__ctor_m3F15910DDA46CD577C38697D9BD04946833D0EC5 (void);
// 0x00000884 System.Void Panel_DataRecord::<OnEnable>b__16_0()
extern void Panel_DataRecord_U3COnEnableU3Eb__16_0_m1883757F8FF433363F935BCAE5590F33623D4D25 (void);
// 0x00000885 System.Void Panel_DataRecord::<OnEnable>b__16_1()
extern void Panel_DataRecord_U3COnEnableU3Eb__16_1_m8B301FE4DFB5A9CE47BE3F35A6A0F07D8DA180C1 (void);
// 0x00000886 System.Void Panel_DataRecord::<OnEnable>b__16_2()
extern void Panel_DataRecord_U3COnEnableU3Eb__16_2_mDBA317E7E07BBDFB7D10F53EF1889A428DFBF732 (void);
// 0x00000887 System.Void Panel_DataRecord/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m139BC4BF7153CE6AFA2AC7333A74FAE293CE5085 (void);
// 0x00000888 System.Void Panel_DataRecord/<>c__DisplayClass16_0::<OnEnable>b__3()
extern void U3CU3Ec__DisplayClass16_0_U3COnEnableU3Eb__3_mD49AAA39C69F4306EB14BC49B6C5FC161D026EB1 (void);
// 0x00000889 System.Void Panel_DataRecord/<FFireExtinguisherList_Search>d__19::.ctor(System.Int32)
extern void U3CFFireExtinguisherList_SearchU3Ed__19__ctor_mBCA4A19B22A5B11DCCF7A7BBFFD12B0A7562D534 (void);
// 0x0000088A System.Void Panel_DataRecord/<FFireExtinguisherList_Search>d__19::System.IDisposable.Dispose()
extern void U3CFFireExtinguisherList_SearchU3Ed__19_System_IDisposable_Dispose_mB051B9257BE2AF4D4FC150AF518E010CBC5237FA (void);
// 0x0000088B System.Boolean Panel_DataRecord/<FFireExtinguisherList_Search>d__19::MoveNext()
extern void U3CFFireExtinguisherList_SearchU3Ed__19_MoveNext_m34B0882E56905E9A4E5B61184CA11F63536569C6 (void);
// 0x0000088C System.Object Panel_DataRecord/<FFireExtinguisherList_Search>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFFireExtinguisherList_SearchU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67ABDD9443F6AC3AED551BE4DBEA19FCA75CF4C4 (void);
// 0x0000088D System.Void Panel_DataRecord/<FFireExtinguisherList_Search>d__19::System.Collections.IEnumerator.Reset()
extern void U3CFFireExtinguisherList_SearchU3Ed__19_System_Collections_IEnumerator_Reset_mC634A246C61D7726D687D683459D5901493C2745 (void);
// 0x0000088E System.Object Panel_DataRecord/<FFireExtinguisherList_Search>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CFFireExtinguisherList_SearchU3Ed__19_System_Collections_IEnumerator_get_Current_m162E99DD6AC81019B428119B3EFA7EC7D31BA58B (void);
// 0x0000088F System.Void Panel_Login::add_EHOpenMenuPanel(System.EventHandler)
extern void Panel_Login_add_EHOpenMenuPanel_mA55C094873CC99345C4F40C8F7EDB9541AF77731 (void);
// 0x00000890 System.Void Panel_Login::remove_EHOpenMenuPanel(System.EventHandler)
extern void Panel_Login_remove_EHOpenMenuPanel_mF22F74E438CA98B34685FF2A445BA625657E8ED4 (void);
// 0x00000891 System.Void Panel_Login::OnEnable()
extern void Panel_Login_OnEnable_mB6942C8447CD4A7D17E325B7A11F975BC9930438 (void);
// 0x00000892 System.Void Panel_Login::LoginByAccount()
extern void Panel_Login_LoginByAccount_m7DE03DBC4C99459C3677E409EDA3C2D4FAE0CA98 (void);
// 0x00000893 System.Collections.IEnumerator Panel_Login::FLogin(GPTT.Data.API/Login,System.Action`1<GPTT.Data.API/User>,System.Action`1<System.String>)
extern void Panel_Login_FLogin_m7E10800D23B853E3FF8941EF7D2D45A506497B13 (void);
// 0x00000894 System.Void Panel_Login::LoginByVisitor()
extern void Panel_Login_LoginByVisitor_m7DD64051F4AA7B6E76FC80BAFB096E86E654C956 (void);
// 0x00000895 System.Void Panel_Login::TgVisiableValueChanged(UnityEngine.UI.Toggle)
extern void Panel_Login_TgVisiableValueChanged_mCE428C5C500E9E0C0A466C3AA52E8E125A43440C (void);
// 0x00000896 System.Void Panel_Login::.ctor()
extern void Panel_Login__ctor_m410877F3F0DCCBB46E49E88A282EDCC839A942BF (void);
// 0x00000897 System.Void Panel_Login::<OnEnable>b__11_0()
extern void Panel_Login_U3COnEnableU3Eb__11_0_m449882B2262B8F72F74FC1A327AADA39FD712C78 (void);
// 0x00000898 System.Void Panel_Login::<OnEnable>b__11_1()
extern void Panel_Login_U3COnEnableU3Eb__11_1_m0A5E76704B164BFCD49DACAE3EF61D5EAFD03CA4 (void);
// 0x00000899 System.Void Panel_Login::<OnEnable>b__11_2(System.Boolean)
extern void Panel_Login_U3COnEnableU3Eb__11_2_m5846FDF9FE97714F6154CAE91C4A2CE6D7DEF7CC (void);
// 0x0000089A System.Void Panel_Login/<>c::.cctor()
extern void U3CU3Ec__cctor_m298E7007DFF3A763829014FD6FD236B01B28F4F2 (void);
// 0x0000089B System.Void Panel_Login/<>c::.ctor()
extern void U3CU3Ec__ctor_mD003A3E6913A88E6C9322515B1B59BD79BD16BC5 (void);
// 0x0000089C System.Void Panel_Login/<>c::<LoginByAccount>b__12_0(GPTT.Data.API/User)
extern void U3CU3Ec_U3CLoginByAccountU3Eb__12_0_mE26C298150EAA90B81500564C597AB866EB72BCB (void);
// 0x0000089D System.Void Panel_Login/<FLogin>d__13::.ctor(System.Int32)
extern void U3CFLoginU3Ed__13__ctor_mC079873B18D6FEEFD59DBD8EC972CC4CC64C4E11 (void);
// 0x0000089E System.Void Panel_Login/<FLogin>d__13::System.IDisposable.Dispose()
extern void U3CFLoginU3Ed__13_System_IDisposable_Dispose_m1E3549457085DC8E8C2372032C0D8DDB3275C43D (void);
// 0x0000089F System.Boolean Panel_Login/<FLogin>d__13::MoveNext()
extern void U3CFLoginU3Ed__13_MoveNext_m15003411CFB4951967A274DC35EBFEFA7C327D58 (void);
// 0x000008A0 System.Object Panel_Login/<FLogin>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFLoginU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DCA039700C52215A58CB57A4686196D533F1DDE (void);
// 0x000008A1 System.Void Panel_Login/<FLogin>d__13::System.Collections.IEnumerator.Reset()
extern void U3CFLoginU3Ed__13_System_Collections_IEnumerator_Reset_mC697FD5C29652BB1F47A13AC9520CBF51A5D5DBA (void);
// 0x000008A2 System.Object Panel_Login/<FLogin>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CFLoginU3Ed__13_System_Collections_IEnumerator_get_Current_m2E7950E9FC70F044040FA77916E340F848F7B10B (void);
// 0x000008A3 System.Void Panel_Menu::add_EHOpenAllInOne(System.EventHandler)
extern void Panel_Menu_add_EHOpenAllInOne_m37C17885A25E4A6D83FFFF330F3E37F6BECCB8E2 (void);
// 0x000008A4 System.Void Panel_Menu::remove_EHOpenAllInOne(System.EventHandler)
extern void Panel_Menu_remove_EHOpenAllInOne_m5B1F6549F8BB499F8240B1898AE6B4A985075731 (void);
// 0x000008A5 System.Void Panel_Menu::add_EHOpenDataRecord(System.EventHandler)
extern void Panel_Menu_add_EHOpenDataRecord_m193BE0C8E56955ABE74A747F3579592A2416965A (void);
// 0x000008A6 System.Void Panel_Menu::remove_EHOpenDataRecord(System.EventHandler)
extern void Panel_Menu_remove_EHOpenDataRecord_m48067132DBD959AFB0944EB6BC19489EC9E050EE (void);
// 0x000008A7 System.Void Panel_Menu::add_EHLogout(System.EventHandler)
extern void Panel_Menu_add_EHLogout_mAA883EE7947DB7A0540919530C59FF5846FEDC93 (void);
// 0x000008A8 System.Void Panel_Menu::remove_EHLogout(System.EventHandler)
extern void Panel_Menu_remove_EHLogout_mE8801C4FE6750C6F747CF72EC0164D2FF66D7119 (void);
// 0x000008A9 System.Void Panel_Menu::OnEnable()
extern void Panel_Menu_OnEnable_m4614286D00F7F2EAF49D7A3CB5B1ED2558BD5DC6 (void);
// 0x000008AA System.Void Panel_Menu::AllInOneMode()
extern void Panel_Menu_AllInOneMode_m13398C5A706C218E5BE2870624799E09C3C92FF0 (void);
// 0x000008AB System.Void Panel_Menu::DataRecordMode()
extern void Panel_Menu_DataRecordMode_m6F6D1206D1D7B645C547990AAEB4C1E506165CD9 (void);
// 0x000008AC System.Void Panel_Menu::Logout()
extern void Panel_Menu_Logout_m42D572E495ABBA21FD32DFDCF7D464EE67ADE698 (void);
// 0x000008AD System.Void Panel_Menu::.ctor()
extern void Panel_Menu__ctor_m6931C94624415A9739EF8576FEF93EB5AB8F2CC9 (void);
// 0x000008AE System.Void Panel_Menu::<OnEnable>b__12_0()
extern void Panel_Menu_U3COnEnableU3Eb__12_0_m8CBDF70120EEB65BD89746A115275E3AF3A86AEF (void);
// 0x000008AF System.Void Panel_Menu::<OnEnable>b__12_1()
extern void Panel_Menu_U3COnEnableU3Eb__12_1_m275C6C788C72B9CE1CF75D720D66581F68D7214B (void);
// 0x000008B0 System.Void Panel_Menu::<OnEnable>b__12_2()
extern void Panel_Menu_U3COnEnableU3Eb__12_2_mAF2A27194E2557D0D8FAF139D2DD0FD62F13B37D (void);
// 0x000008B1 System.Void ServerManager::Start()
extern void ServerManager_Start_mF7EF750999CD42D9B6EF0BCC056CAC5C4D49FB88 (void);
// 0x000008B2 System.String ServerManager::GetLocalIPAddress()
extern void ServerManager_GetLocalIPAddress_m8D97795E01D54DCB6F5234D09648A77CE155F6F4 (void);
// 0x000008B3 System.Void ServerManager::.ctor()
extern void ServerManager__ctor_m023716C9C9B41DDA0833CEB210D5F70CD919DFD0 (void);
// 0x000008B4 System.Collections.IEnumerator ServerManagerUI::Init()
extern void ServerManagerUI_Init_mDCFE72456B0F6DB27B7FFEBC7F200422F25EE23F (void);
// 0x000008B5 System.Void ServerManagerUI::EventOpenMenu(System.Object,System.EventArgs)
extern void ServerManagerUI_EventOpenMenu_m862BD21B1C41A02A3EC56B6C849F2CD6FD5A192C (void);
// 0x000008B6 System.Void ServerManagerUI::EventOpenAllInOne(System.Object,System.EventArgs)
extern void ServerManagerUI_EventOpenAllInOne_m4B5B93774BAB0D0F99E375B317B5297C6423B02F (void);
// 0x000008B7 System.Void ServerManagerUI::EventOpenDataRecord(System.Object,System.EventArgs)
extern void ServerManagerUI_EventOpenDataRecord_m3C42AE3C3F077C2D88079C129DEB95601D606A1A (void);
// 0x000008B8 System.Void ServerManagerUI::EventLogout(System.Object,System.EventArgs)
extern void ServerManagerUI_EventLogout_mE50C0004EB72022F33618CA19F5192B54105BEA3 (void);
// 0x000008B9 System.Void ServerManagerUI::EventBackToMenu(System.Object,System.EventArgs)
extern void ServerManagerUI_EventBackToMenu_m00165244790CC8711D3DCDD50583E651FA4C2B1F (void);
// 0x000008BA System.Void ServerManagerUI::.ctor()
extern void ServerManagerUI__ctor_m76B67313410C36300DA69229040F19E9C5FF1DFD (void);
// 0x000008BB System.Void ServerManagerUI/<Init>d__4::.ctor(System.Int32)
extern void U3CInitU3Ed__4__ctor_m96FAED00B123A4C00B5EF446FCDC08A68379DF49 (void);
// 0x000008BC System.Void ServerManagerUI/<Init>d__4::System.IDisposable.Dispose()
extern void U3CInitU3Ed__4_System_IDisposable_Dispose_mC8CFAEBABC78B67AD5FB8DA056A5D20F6D807142 (void);
// 0x000008BD System.Boolean ServerManagerUI/<Init>d__4::MoveNext()
extern void U3CInitU3Ed__4_MoveNext_mE9566C1EB60855ABD5C0B78367C5D1A52E72A38B (void);
// 0x000008BE System.Object ServerManagerUI/<Init>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED67422E32CABAC4657637A811C9DD6C6689B4D0 (void);
// 0x000008BF System.Void ServerManagerUI/<Init>d__4::System.Collections.IEnumerator.Reset()
extern void U3CInitU3Ed__4_System_Collections_IEnumerator_Reset_mAD7BB77764B97952953510C1E0453EE78593E9D4 (void);
// 0x000008C0 System.Object ServerManagerUI/<Init>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CInitU3Ed__4_System_Collections_IEnumerator_get_Current_mEA9B9FE6F9C30ECB95B87241FEA66918AD8BACE8 (void);
// 0x000008C1 System.Void AutoRotate::Update()
extern void AutoRotate_Update_mB64C3E4C500CA7ACF369E2C1519DA8D65D8A12AB (void);
// 0x000008C2 System.Void AutoRotate::.ctor()
extern void AutoRotate__ctor_m734E0E82503C1250A1C45AE292672C28607D856B (void);
// 0x000008C3 System.Void FollowObjectPosition::Update()
extern void FollowObjectPosition_Update_m7A2163E64DB2AD26BC3E23625D9F24EB85D387A2 (void);
// 0x000008C4 System.Void FollowObjectPosition::.ctor()
extern void FollowObjectPosition__ctor_mDB60D885D341F0A006F8C900C9C5AAB9D982F271 (void);
// 0x000008C5 System.Void ParticleController::Start()
extern void ParticleController_Start_m7DB7ECDC47B6BE9A938A0D900D2F9A8CA95EC13D (void);
// 0x000008C6 System.Void ParticleController::OnEnable()
extern void ParticleController_OnEnable_m850F8E3BF25F6D582589561FAB0BD33A389E04E4 (void);
// 0x000008C7 System.Void ParticleController::Update()
extern void ParticleController_Update_m2C837B5D6C99AA727449C4458B373AB5D18E216F (void);
// 0x000008C8 System.Void ParticleController::Play()
extern void ParticleController_Play_m9704E5ADC97C236761DD608674EDA1CFB8509B43 (void);
// 0x000008C9 System.Void ParticleController::Stop()
extern void ParticleController_Stop_m7C4FD910BCEB4620A4271F33C19B8EDAD77FD9B0 (void);
// 0x000008CA System.Void ParticleController::.ctor()
extern void ParticleController__ctor_mFE7357596D5D39DD236F3BFCBFF71AFAC99C64AB (void);
// 0x000008CB System.Void RayCasting::Start()
extern void RayCasting_Start_m6A5ED478C3E7F039CDA628E841C6FA686F503167 (void);
// 0x000008CC System.Void RayCasting::Update()
extern void RayCasting_Update_m76D59315B4FE0288779D7A2B8B26C978A99D900D (void);
// 0x000008CD System.Void RayCasting::.ctor()
extern void RayCasting__ctor_mE28827612C030EEE4023A42F70549D3DBF065B7C (void);
// 0x000008CE System.Void UGUI_Hover::OnEnable()
extern void UGUI_Hover_OnEnable_mEB168FB1D752666CCEAEE84CDB8C238698D6B1A7 (void);
// 0x000008CF System.Void UGUI_Hover::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void UGUI_Hover_OnPointerEnter_m79805FD893214753331E0B1BFB147FD3DEF11663 (void);
// 0x000008D0 System.Void UGUI_Hover::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void UGUI_Hover_OnPointerExit_mC42813F3611FB896CC6F81E298331C91992D394B (void);
// 0x000008D1 System.Void UGUI_Hover::.ctor()
extern void UGUI_Hover__ctor_mADB45CE18CE14848C928960151B948AD5B37EE47 (void);
// 0x000008D2 System.Void UserIDCheck::Action_ProcessStringData(System.String)
extern void UserIDCheck_Action_ProcessStringData_m58224527A6C5E2B39B5B543DCD5CD82F8349AFD5 (void);
// 0x000008D3 System.Void UserIDCheck::.ctor()
extern void UserIDCheck__ctor_m26FE5C64E8FC139E3CF6DC162DB567F498C56668 (void);
// 0x000008D4 System.Void UserIDCheck::.cctor()
extern void UserIDCheck__cctor_mF2B76C2FD991CB46030C16B5FCE0AE4E47FB60A2 (void);
// 0x000008D5 System.Void DKP.VR.HandsVisualization::Update()
extern void HandsVisualization_Update_mD8418A158C986348A57598128279C52D796BEFA1 (void);
// 0x000008D6 System.Void DKP.VR.HandsVisualization::ChangeState(DKP.VR.HandsVisualization/DisplayMode)
extern void HandsVisualization_ChangeState_m11849B5F36C8FCF74B1EBA9E7322097FA75BCD83 (void);
// 0x000008D7 System.Void DKP.VR.HandsVisualization::.ctor()
extern void HandsVisualization__ctor_m7DA8877C57583BE4C14A0B3637519BE71018A1CE (void);
// 0x000008D8 System.Void DKP.VR.LaserRay::Start()
extern void LaserRay_Start_mDB9F96A87B00625A79A0B339009550A51C44FB08 (void);
// 0x000008D9 System.Void DKP.VR.LaserRay::Update()
extern void LaserRay_Update_m738D5DC746623A29ACA1D457A6F1A92013502B6F (void);
// 0x000008DA System.Void DKP.VR.LaserRay::RayHit()
extern void LaserRay_RayHit_m43A03C4D7F3CDDC088ECEF07EDC0DC1C2184FD50 (void);
// 0x000008DB System.Void DKP.VR.LaserRay::.ctor()
extern void LaserRay__ctor_m4197B860737EBC446089CF358FF58CC8597B641E (void);
// 0x000008DC System.Void DKP.VR.ControlState::.cctor()
extern void ControlState__cctor_mBDB237564AD51DED7F4D37EA9D696E057AB9CABE (void);
// 0x000008DD System.Void DKP.VR.VRIO::Haptics(DKP.VR.Hand)
extern void VRIO_Haptics_mDEDCBBE17224C75F57744B84333F4FFA6933907D (void);
// 0x000008DE System.Void DKP.VR.VRIO::Throw(DKP.VR.Hand,UnityEngine.GameObject)
extern void VRIO_Throw_m85BEEC3343521C3CE9072B2B686B3D38EF9099A3 (void);
// 0x000008DF System.Void DKP.VR.VRIO/<Haptics>d__0::.ctor()
extern void U3CHapticsU3Ed__0__ctor_mCD6188E048F28ED1D22E38A31DB31CD4B81E8C16 (void);
// 0x000008E0 System.Void DKP.VR.VRIO/<Haptics>d__0::MoveNext()
extern void U3CHapticsU3Ed__0_MoveNext_m9373353F1A7C1785E118156DCE0D27A8C52E0E52 (void);
// 0x000008E1 System.Void DKP.VR.VRIO/<Haptics>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CHapticsU3Ed__0_SetStateMachine_m80A4CFDD96530258F3A3865E6A5E3B39614DCB25 (void);
// 0x000008E2 System.Void DKP.VR.VRPlatformSetting::Awake()
extern void VRPlatformSetting_Awake_m6FF9D5546D60BE40314F1E0AD5EE966759961B72 (void);
// 0x000008E3 System.Void DKP.VR.VRPlatformSetting::.ctor()
extern void VRPlatformSetting__ctor_mC91EA019BBD199BF9AE9BF19B120E34FDACFFAD4 (void);
// 0x000008E4 UnityEngine.Rect Shinn.Console::get_SizeOffset()
extern void Console_get_SizeOffset_mAE766FF6C2A1E64A441FCF18ECE95A1CFBB7AAF4 (void);
// 0x000008E5 System.Void Shinn.Console::set_SizeOffset(UnityEngine.Rect)
extern void Console_set_SizeOffset_mF0EE6F76811BA9C00E05C04974999BAFF9AB4DB5 (void);
// 0x000008E6 System.Boolean Shinn.Console::get_ShowConsole()
extern void Console_get_ShowConsole_mD4411F1872A10C1BF9AFFB752FD60913EF3E47B8 (void);
// 0x000008E7 System.Void Shinn.Console::set_ShowConsole(System.Boolean)
extern void Console_set_ShowConsole_m254D008826892EE2D8424DEFC2FFDB73B1356D76 (void);
// 0x000008E8 System.Void Shinn.Console::OnEnable()
extern void Console_OnEnable_m1D5D0ECDE0C01A0794DED111B7A590EB21503982 (void);
// 0x000008E9 System.Void Shinn.Console::OnDisable()
extern void Console_OnDisable_mAABB8E1D554880D111759181281854138BD7FB1D (void);
// 0x000008EA System.Void Shinn.Console::Update()
extern void Console_Update_m06834FCB3E2B451247AF9ECB932AFAC75CA188B3 (void);
// 0x000008EB System.Void Shinn.Console::OnGUI()
extern void Console_OnGUI_mE245A80CEC1003471364B9B0A1BC9174E2466183 (void);
// 0x000008EC System.Void Shinn.Console::ConsoleWindow(System.Int32)
extern void Console_ConsoleWindow_m77D6AACFEE7F91006B06D7C251C6253617E1D79B (void);
// 0x000008ED System.Void Shinn.Console::HandleLog(System.String,System.String,UnityEngine.LogType)
extern void Console_HandleLog_mFA2325313E8A2A5641A334CD98FFBB17DE3A1DE2 (void);
// 0x000008EE System.Void Shinn.Console::ExportMessages()
extern void Console_ExportMessages_m2EA7177DEC60DCBEED44D3530EE5E46F9BEA5709 (void);
// 0x000008EF System.Void Shinn.Console::.ctor()
extern void Console__ctor_m9F86FDBEB005BCE8604A3F5135BD3DCECF1074AF (void);
// 0x000008F0 System.Void Shinn.Console::.cctor()
extern void Console__cctor_mB3B14CE8294B4B55C36E3C511660704222DDA3FE (void);
// 0x000008F1 System.Collections.IEnumerator GPTT.Data.API::Login_API(GPTT.Data.API/Login)
extern void API_Login_API_mB88E9E47FD413402B94CE8BE21389466FB2A4B5F (void);
// 0x000008F2 System.Collections.IEnumerator GPTT.Data.API::FireExtinguisher_API(GPTT.Data.API/FireExtinguisher)
extern void API_FireExtinguisher_API_mD22898A0EAC074FC1546B479D06B2278B2C401D8 (void);
// 0x000008F3 System.Collections.IEnumerator GPTT.Data.API::FireExtinguisherList_API(GPTT.Data.API/Search)
extern void API_FireExtinguisherList_API_mFA38695002692D9E8747788F682970C4C56BAB7D (void);
// 0x000008F4 System.Collections.IEnumerator GPTT.Data.API::Firemen_API(GPTT.Data.API/Firemen)
extern void API_Firemen_API_m762BD6FE4ABE7B7C8C4A5B8373238C707AF72303 (void);
// 0x000008F5 System.Collections.IEnumerator GPTT.Data.API::FiremenList_API(GPTT.Data.API/Search)
extern void API_FiremenList_API_mC39E64B49DBDD88EE45FBA624B4EE1FCE5086C4E (void);
// 0x000008F6 System.Collections.IEnumerator GPTT.Data.API::FiremenDetailList_API(GPTT.Data.API/Search)
extern void API_FiremenDetailList_API_m5CE8000DBC8E6D079E6E67B5E438201C3FF9E460 (void);
// 0x000008F7 System.Collections.IEnumerator GPTT.Data.API::SelectVideo(System.String,System.String,System.DateTime,System.Int32)
extern void API_SelectVideo_m184ECF99E47F8594D6626BAAB24BCF495F4C7090 (void);
// 0x000008F8 System.Collections.IEnumerator GPTT.Data.API::CheckVideo(System.String,System.String,System.String,System.String,System.DateTime,System.Int32)
extern void API_CheckVideo_m233867B761183D99AA04442B1751FCC48E65EF94 (void);
// 0x000008F9 System.Collections.IEnumerator GPTT.Data.API::UploadVideo(UnityEngine.WWWForm,System.String)
extern void API_UploadVideo_m2207B4F85113E40A7256BA9063FEEA4FFD29EE1D (void);
// 0x000008FA System.Void GPTT.Data.API::.cctor()
extern void API__cctor_m5C950070AE71A037928544CE4FA2D65FFF7DF380 (void);
// 0x000008FB System.Void GPTT.Data.API/Login::.ctor()
extern void Login__ctor_m7F1F98F28179603D8698EEF6572D0B723F663657 (void);
// 0x000008FC System.Void GPTT.Data.API/User::.ctor()
extern void User__ctor_mE64BFA90B5E315FC33D9170235F8464961688482 (void);
// 0x000008FD System.Void GPTT.Data.API/FireExtinguisher::.ctor()
extern void FireExtinguisher__ctor_mAE78FF7ADA50E81072520B1196A961BCA92677D6 (void);
// 0x000008FE System.Void GPTT.Data.API/FireExtinguisherList::.ctor()
extern void FireExtinguisherList__ctor_m6A7C6B895C88D4DA3A491FAE85080851E5ECEA07 (void);
// 0x000008FF System.Void GPTT.Data.API/FiremenInsertResponse::.ctor()
extern void FiremenInsertResponse__ctor_mB2FAF27217C8DF3D791EB19A9AF078DBABD97F09 (void);
// 0x00000900 System.Void GPTT.Data.API/FiremenDetailed::.ctor()
extern void FiremenDetailed__ctor_m925E2B7169510C77D0930187C5073295AC387FED (void);
// 0x00000901 System.Void GPTT.Data.API/FiremenDetailedList::.ctor()
extern void FiremenDetailedList__ctor_m68474C4E0190F3935A780202B9C05F768DCB6382 (void);
// 0x00000902 System.Void GPTT.Data.API/Firemen::.ctor()
extern void Firemen__ctor_m2A881E00E190694E376F29AFAB795912F863F86E (void);
// 0x00000903 System.Void GPTT.Data.API/FiremenList::.ctor()
extern void FiremenList__ctor_m419026132BDB8BAC0E8A0C3670406E3E8D1E1D00 (void);
// 0x00000904 System.Void GPTT.Data.API/RequestData::.ctor()
extern void RequestData__ctor_m5B29AF3CB727501125FBD1BCA00EB3195E578DFB (void);
// 0x00000905 System.Void GPTT.Data.API/Search::.ctor()
extern void Search__ctor_m9F1D82CBD4FBA64A53CF46E06C22BB18B90979A5 (void);
// 0x00000906 System.Void GPTT.Data.API/<Login_API>d__10::.ctor(System.Int32)
extern void U3CLogin_APIU3Ed__10__ctor_m5A371B2FC2C44AAF0A86334AEA7CC1C17F8E587F (void);
// 0x00000907 System.Void GPTT.Data.API/<Login_API>d__10::System.IDisposable.Dispose()
extern void U3CLogin_APIU3Ed__10_System_IDisposable_Dispose_m1E36A717738B5EFC60E33438811C8F3DD1B68943 (void);
// 0x00000908 System.Boolean GPTT.Data.API/<Login_API>d__10::MoveNext()
extern void U3CLogin_APIU3Ed__10_MoveNext_m80887A731F456AD7C423425A86052CF733017AD0 (void);
// 0x00000909 System.Object GPTT.Data.API/<Login_API>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLogin_APIU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B1309DF06DFD499B36DEE1B77CE53F99252B372 (void);
// 0x0000090A System.Void GPTT.Data.API/<Login_API>d__10::System.Collections.IEnumerator.Reset()
extern void U3CLogin_APIU3Ed__10_System_Collections_IEnumerator_Reset_m5CF1BAC5246C0E187E1D4CF1E2BA812CABB0651F (void);
// 0x0000090B System.Object GPTT.Data.API/<Login_API>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CLogin_APIU3Ed__10_System_Collections_IEnumerator_get_Current_m5E377DF1E518E6FB8A68CC55770A9A837D710941 (void);
// 0x0000090C System.Void GPTT.Data.API/<FireExtinguisher_API>d__14::.ctor(System.Int32)
extern void U3CFireExtinguisher_APIU3Ed__14__ctor_m6E37AD2479D669253DB457C8174636523EE9E51E (void);
// 0x0000090D System.Void GPTT.Data.API/<FireExtinguisher_API>d__14::System.IDisposable.Dispose()
extern void U3CFireExtinguisher_APIU3Ed__14_System_IDisposable_Dispose_mE48CDB272A08C4F57B9C6C8354E3B49EB5D3AE29 (void);
// 0x0000090E System.Boolean GPTT.Data.API/<FireExtinguisher_API>d__14::MoveNext()
extern void U3CFireExtinguisher_APIU3Ed__14_MoveNext_m8C5DA611CCA33D4491D9A8E87745266F179EDBF2 (void);
// 0x0000090F System.Object GPTT.Data.API/<FireExtinguisher_API>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFireExtinguisher_APIU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC5259F54744D75AAFAA3F2A160AFFB5258F7A840 (void);
// 0x00000910 System.Void GPTT.Data.API/<FireExtinguisher_API>d__14::System.Collections.IEnumerator.Reset()
extern void U3CFireExtinguisher_APIU3Ed__14_System_Collections_IEnumerator_Reset_m559439118F928FC49F8B4FA087E2D5AECDD93EDB (void);
// 0x00000911 System.Object GPTT.Data.API/<FireExtinguisher_API>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CFireExtinguisher_APIU3Ed__14_System_Collections_IEnumerator_get_Current_mA8757CA3F093FF2DF205838526B4432E234F56A0 (void);
// 0x00000912 System.Void GPTT.Data.API/<FireExtinguisherList_API>d__15::.ctor(System.Int32)
extern void U3CFireExtinguisherList_APIU3Ed__15__ctor_mAD02ADC58EBFFE16FFF6825C95393285F6F4E051 (void);
// 0x00000913 System.Void GPTT.Data.API/<FireExtinguisherList_API>d__15::System.IDisposable.Dispose()
extern void U3CFireExtinguisherList_APIU3Ed__15_System_IDisposable_Dispose_m5839073769B867DFF93CC20F6B6894472F533C09 (void);
// 0x00000914 System.Boolean GPTT.Data.API/<FireExtinguisherList_API>d__15::MoveNext()
extern void U3CFireExtinguisherList_APIU3Ed__15_MoveNext_m6B02FD0CAC2F0D364DF0FEECA66A0F68BC513C82 (void);
// 0x00000915 System.Object GPTT.Data.API/<FireExtinguisherList_API>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFireExtinguisherList_APIU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0C5E92A3D4397B2B001594290C0F010CD62390A (void);
// 0x00000916 System.Void GPTT.Data.API/<FireExtinguisherList_API>d__15::System.Collections.IEnumerator.Reset()
extern void U3CFireExtinguisherList_APIU3Ed__15_System_Collections_IEnumerator_Reset_m10D2D1A4FE103557CE313347C9359BC5B69B098D (void);
// 0x00000917 System.Object GPTT.Data.API/<FireExtinguisherList_API>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CFireExtinguisherList_APIU3Ed__15_System_Collections_IEnumerator_get_Current_mEE38B5D80B82DCC2CD39863EC5488A0E7D41A82E (void);
// 0x00000918 System.Void GPTT.Data.API/<Firemen_API>d__19::.ctor(System.Int32)
extern void U3CFiremen_APIU3Ed__19__ctor_m8C5EC8A86E21946157DF2EFD0AD3E535A8FDDF90 (void);
// 0x00000919 System.Void GPTT.Data.API/<Firemen_API>d__19::System.IDisposable.Dispose()
extern void U3CFiremen_APIU3Ed__19_System_IDisposable_Dispose_mE41130179346E193D46C65D75958EBC602B05865 (void);
// 0x0000091A System.Boolean GPTT.Data.API/<Firemen_API>d__19::MoveNext()
extern void U3CFiremen_APIU3Ed__19_MoveNext_m101508CE3EA4CEA57B12AADA3278BB3D7581EB9E (void);
// 0x0000091B System.Object GPTT.Data.API/<Firemen_API>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFiremen_APIU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7CA8E7652383684AA3EE9278831691B45481B57 (void);
// 0x0000091C System.Void GPTT.Data.API/<Firemen_API>d__19::System.Collections.IEnumerator.Reset()
extern void U3CFiremen_APIU3Ed__19_System_Collections_IEnumerator_Reset_m7CA0FE9DE77579C146500F74667E5E1948E687B0 (void);
// 0x0000091D System.Object GPTT.Data.API/<Firemen_API>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CFiremen_APIU3Ed__19_System_Collections_IEnumerator_get_Current_m4A00CF3770D67C7616305FCCBA0DD461C4002B13 (void);
// 0x0000091E System.Void GPTT.Data.API/<FiremenList_API>d__20::.ctor(System.Int32)
extern void U3CFiremenList_APIU3Ed__20__ctor_m39C6253B3A0205A47293C746609DDE86F82F39CE (void);
// 0x0000091F System.Void GPTT.Data.API/<FiremenList_API>d__20::System.IDisposable.Dispose()
extern void U3CFiremenList_APIU3Ed__20_System_IDisposable_Dispose_mD13C41F413CEA831D55090AEBA3A998ADB5273F0 (void);
// 0x00000920 System.Boolean GPTT.Data.API/<FiremenList_API>d__20::MoveNext()
extern void U3CFiremenList_APIU3Ed__20_MoveNext_m5AE9CE7E4F074FFDD918DFADD96F52F9800045AF (void);
// 0x00000921 System.Object GPTT.Data.API/<FiremenList_API>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFiremenList_APIU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m80F7E984F20A3C7030CF100F7DB4656ACFEFEF9A (void);
// 0x00000922 System.Void GPTT.Data.API/<FiremenList_API>d__20::System.Collections.IEnumerator.Reset()
extern void U3CFiremenList_APIU3Ed__20_System_Collections_IEnumerator_Reset_m7E9DF3ACADE55D44B84E3CFDEBF7C251580FACCB (void);
// 0x00000923 System.Object GPTT.Data.API/<FiremenList_API>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CFiremenList_APIU3Ed__20_System_Collections_IEnumerator_get_Current_m45E78CBB8D455374A390226B32BAD3A8A690CEBD (void);
// 0x00000924 System.Void GPTT.Data.API/<FiremenDetailList_API>d__21::.ctor(System.Int32)
extern void U3CFiremenDetailList_APIU3Ed__21__ctor_mAC930F79937AFEE1FB31FFE94BF8561B5EEE594E (void);
// 0x00000925 System.Void GPTT.Data.API/<FiremenDetailList_API>d__21::System.IDisposable.Dispose()
extern void U3CFiremenDetailList_APIU3Ed__21_System_IDisposable_Dispose_mD08FCFAA218F78E704EE1575143FEC4987C45231 (void);
// 0x00000926 System.Boolean GPTT.Data.API/<FiremenDetailList_API>d__21::MoveNext()
extern void U3CFiremenDetailList_APIU3Ed__21_MoveNext_mEBD2F2F524EC645D5D20FAC2DCCCBC03344008A6 (void);
// 0x00000927 System.Object GPTT.Data.API/<FiremenDetailList_API>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFiremenDetailList_APIU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3B244E3C064F290DFB1B86CB92A536BC63F61F1 (void);
// 0x00000928 System.Void GPTT.Data.API/<FiremenDetailList_API>d__21::System.Collections.IEnumerator.Reset()
extern void U3CFiremenDetailList_APIU3Ed__21_System_Collections_IEnumerator_Reset_mEFF9BE445E82C3385FC3E37AE8ADD1B82E1DF6E0 (void);
// 0x00000929 System.Object GPTT.Data.API/<FiremenDetailList_API>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CFiremenDetailList_APIU3Ed__21_System_Collections_IEnumerator_get_Current_m2C9725029111E95E8B88F47C0A6730E3C47E1510 (void);
// 0x0000092A System.Void GPTT.Data.API/<SelectVideo>d__30::.ctor(System.Int32)
extern void U3CSelectVideoU3Ed__30__ctor_mBBA963469CCD6FF9B6EF87EE8F313AE0FD19D26A (void);
// 0x0000092B System.Void GPTT.Data.API/<SelectVideo>d__30::System.IDisposable.Dispose()
extern void U3CSelectVideoU3Ed__30_System_IDisposable_Dispose_mD1053B5B6FA17801CBE26915CFC79B3AA48766E6 (void);
// 0x0000092C System.Boolean GPTT.Data.API/<SelectVideo>d__30::MoveNext()
extern void U3CSelectVideoU3Ed__30_MoveNext_mE0B8B307E8CF9B0508E6533FF9B660333FF7B240 (void);
// 0x0000092D System.Object GPTT.Data.API/<SelectVideo>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSelectVideoU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF3D7A6217D0104B0E45456632F6D9E9B2BB37B1 (void);
// 0x0000092E System.Void GPTT.Data.API/<SelectVideo>d__30::System.Collections.IEnumerator.Reset()
extern void U3CSelectVideoU3Ed__30_System_Collections_IEnumerator_Reset_mB93214CC946EBDE373EA3EEE748CCCA94F0E2CE4 (void);
// 0x0000092F System.Object GPTT.Data.API/<SelectVideo>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CSelectVideoU3Ed__30_System_Collections_IEnumerator_get_Current_mAC954C4889366ABDB27CF8BD34E23F03D9140153 (void);
// 0x00000930 System.Void GPTT.Data.API/<CheckVideo>d__31::.ctor(System.Int32)
extern void U3CCheckVideoU3Ed__31__ctor_m1BE34ADBC6D6B756257DE8E006216A712C85FF95 (void);
// 0x00000931 System.Void GPTT.Data.API/<CheckVideo>d__31::System.IDisposable.Dispose()
extern void U3CCheckVideoU3Ed__31_System_IDisposable_Dispose_m22951DB866AA82D4826A581A8CA3A2A450818B24 (void);
// 0x00000932 System.Boolean GPTT.Data.API/<CheckVideo>d__31::MoveNext()
extern void U3CCheckVideoU3Ed__31_MoveNext_mC9F001B04F13F86AA21F6AD9E96ADFFF56704E70 (void);
// 0x00000933 System.Object GPTT.Data.API/<CheckVideo>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckVideoU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18BF553D353ACE9AA61693EDA687DF94E73EE4CB (void);
// 0x00000934 System.Void GPTT.Data.API/<CheckVideo>d__31::System.Collections.IEnumerator.Reset()
extern void U3CCheckVideoU3Ed__31_System_Collections_IEnumerator_Reset_mB7DD11826ECFFA05FFC4558E5CD8BD8CBF65BF40 (void);
// 0x00000935 System.Object GPTT.Data.API/<CheckVideo>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CCheckVideoU3Ed__31_System_Collections_IEnumerator_get_Current_mEE0F174E52399BE7BE5E8FC7896254D2CCEBCF35 (void);
// 0x00000936 System.Void GPTT.Data.API/<UploadVideo>d__32::.ctor(System.Int32)
extern void U3CUploadVideoU3Ed__32__ctor_m3F82494FAD51A1744988E92C31B6DAA5865E4E39 (void);
// 0x00000937 System.Void GPTT.Data.API/<UploadVideo>d__32::System.IDisposable.Dispose()
extern void U3CUploadVideoU3Ed__32_System_IDisposable_Dispose_m198334FE7081A64C780AB3F40289802E20D7ACD7 (void);
// 0x00000938 System.Boolean GPTT.Data.API/<UploadVideo>d__32::MoveNext()
extern void U3CUploadVideoU3Ed__32_MoveNext_m92535121B50DCC8B841EAC1BFDD0389672EBF3FF (void);
// 0x00000939 System.Object GPTT.Data.API/<UploadVideo>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUploadVideoU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3CE180FC3EB271F3727E401E57BE13C92BF0B88 (void);
// 0x0000093A System.Void GPTT.Data.API/<UploadVideo>d__32::System.Collections.IEnumerator.Reset()
extern void U3CUploadVideoU3Ed__32_System_Collections_IEnumerator_Reset_m5197E35D8E4DA480777B92F26FED6A6120CC9796 (void);
// 0x0000093B System.Object GPTT.Data.API/<UploadVideo>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CUploadVideoU3Ed__32_System_Collections_IEnumerator_get_Current_m764EA69BB57540EE73138B73F3F3F3F977B18DFF (void);
// 0x0000093C System.Void GPTT.Data.APIHelper::Init()
extern void APIHelper_Init_m9BE28A0AA4EF3293CB7FA78561872573A92112B2 (void);
// 0x0000093D System.Collections.IEnumerator GPTT.Data.APIHelper::Login(GPTT.Data.API/Login,System.Action`1<GPTT.Data.API/User>,System.Action`1<System.String>)
extern void APIHelper_Login_mCA1B3060A6BA7184B8FF9949CAF575F9E32CA7F0 (void);
// 0x0000093E System.Void GPTT.Data.APIHelper::Logout()
extern void APIHelper_Logout_m6541E32B40679060BAFB83857657CC12457F9B7A (void);
// 0x0000093F GPTT.Data.API/User GPTT.Data.APIHelper::GetUserData()
extern void APIHelper_GetUserData_m2BD2B8CC63E978945051A99D2239C3E5F830ED51 (void);
// 0x00000940 System.Collections.IEnumerator GPTT.Data.APIHelper::FireExtinguisher_Insert(GPTT.Data.API/FireExtinguisher,System.Action`1<GPTT.Data.API/User>,System.Action`1<System.String>)
extern void APIHelper_FireExtinguisher_Insert_m8FDF32147D2E7C3C81A280DEF0646623D0945CCF (void);
// 0x00000941 System.Collections.IEnumerator GPTT.Data.APIHelper::FireExtinguisherList_Search(GPTT.Data.API/Search,System.Action`1<GPTT.Data.API/User>,System.Action`1<System.String>)
extern void APIHelper_FireExtinguisherList_Search_m1B24D39D5DE004F6A9211C73AAD8D1A8AA7B202C (void);
// 0x00000942 System.Collections.IEnumerator GPTT.Data.APIHelper::Firemen_Insert(GPTT.Data.API/Firemen,System.Action,System.Action`1<System.String>)
extern void APIHelper_Firemen_Insert_mBE4236003F4BBD2379402F98B800C3EEA1231B51 (void);
// 0x00000943 System.Collections.IEnumerator GPTT.Data.APIHelper::FiremenList_Search(GPTT.Data.API/Search,System.Action`1<GPTT.Data.API/FiremenList>,System.Action`1<System.String>)
extern void APIHelper_FiremenList_Search_m122E771393E463AF5EEE431A0F3C294FDFEC23A1 (void);
// 0x00000944 System.Collections.IEnumerator GPTT.Data.APIHelper::FiremenDetailList_Search(GPTT.Data.API/Search,System.Action`1<GPTT.Data.API/FiremenDetailedList>,System.Action`1<System.String>)
extern void APIHelper_FiremenDetailList_Search_mD22CBF6F95A9A938865A85401880944D452B58E0 (void);
// 0x00000945 System.Void GPTT.Data.APIHelper::.cctor()
extern void APIHelper__cctor_mA921E7652C0F8B07F71AB0D06690877B5998C2F3 (void);
// 0x00000946 System.Void GPTT.Data.APIHelper/<Login>d__2::.ctor(System.Int32)
extern void U3CLoginU3Ed__2__ctor_m0D04E9BF7397D44770472F91B12F8637CDB631EA (void);
// 0x00000947 System.Void GPTT.Data.APIHelper/<Login>d__2::System.IDisposable.Dispose()
extern void U3CLoginU3Ed__2_System_IDisposable_Dispose_m3A65362A650183875DF9B61A839F5400226CDC16 (void);
// 0x00000948 System.Boolean GPTT.Data.APIHelper/<Login>d__2::MoveNext()
extern void U3CLoginU3Ed__2_MoveNext_m257954EA96E2AE5DAE8F7FECC1D3AACE5C0C458C (void);
// 0x00000949 System.Object GPTT.Data.APIHelper/<Login>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoginU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m843480DC9090DC568AA3A02092C6DEFE0A0D4762 (void);
// 0x0000094A System.Void GPTT.Data.APIHelper/<Login>d__2::System.Collections.IEnumerator.Reset()
extern void U3CLoginU3Ed__2_System_Collections_IEnumerator_Reset_m2A23CF954AD85C566998F85FD206085FBC9A1C9A (void);
// 0x0000094B System.Object GPTT.Data.APIHelper/<Login>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CLoginU3Ed__2_System_Collections_IEnumerator_get_Current_m0DD5DD3C2B599755AE2034BD3252FA233AA8A1A3 (void);
// 0x0000094C System.Void GPTT.Data.APIHelper/<FireExtinguisher_Insert>d__5::.ctor(System.Int32)
extern void U3CFireExtinguisher_InsertU3Ed__5__ctor_m8F43656725CCCEAD050D72F2217CDD178702E270 (void);
// 0x0000094D System.Void GPTT.Data.APIHelper/<FireExtinguisher_Insert>d__5::System.IDisposable.Dispose()
extern void U3CFireExtinguisher_InsertU3Ed__5_System_IDisposable_Dispose_mB1883EF22AB69E048F8158069CFACA08D6D80033 (void);
// 0x0000094E System.Boolean GPTT.Data.APIHelper/<FireExtinguisher_Insert>d__5::MoveNext()
extern void U3CFireExtinguisher_InsertU3Ed__5_MoveNext_mC4F5B4C21E860EB52782178C13D47FAE1A527F38 (void);
// 0x0000094F System.Object GPTT.Data.APIHelper/<FireExtinguisher_Insert>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFireExtinguisher_InsertU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAFBA099A1B758D7B78DE1F69136F6A531802FAC1 (void);
// 0x00000950 System.Void GPTT.Data.APIHelper/<FireExtinguisher_Insert>d__5::System.Collections.IEnumerator.Reset()
extern void U3CFireExtinguisher_InsertU3Ed__5_System_Collections_IEnumerator_Reset_mA32F5A9A452A20176846DB08EE957BD3EF4D7847 (void);
// 0x00000951 System.Object GPTT.Data.APIHelper/<FireExtinguisher_Insert>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CFireExtinguisher_InsertU3Ed__5_System_Collections_IEnumerator_get_Current_m11D7AEF50426AF5B415D6A2F2184402ABDED735A (void);
// 0x00000952 System.Void GPTT.Data.APIHelper/<FireExtinguisherList_Search>d__6::.ctor(System.Int32)
extern void U3CFireExtinguisherList_SearchU3Ed__6__ctor_m8B83EB57DB1B6F24A05DF05A397738FC29E94848 (void);
// 0x00000953 System.Void GPTT.Data.APIHelper/<FireExtinguisherList_Search>d__6::System.IDisposable.Dispose()
extern void U3CFireExtinguisherList_SearchU3Ed__6_System_IDisposable_Dispose_m2C717BC1DD54BA2CB06C0E19D9BAA5EF79B303BD (void);
// 0x00000954 System.Boolean GPTT.Data.APIHelper/<FireExtinguisherList_Search>d__6::MoveNext()
extern void U3CFireExtinguisherList_SearchU3Ed__6_MoveNext_m85B4E3FDE96FD14A79FFC77C08F185E900257987 (void);
// 0x00000955 System.Object GPTT.Data.APIHelper/<FireExtinguisherList_Search>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFireExtinguisherList_SearchU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2622088E65A2D1D8669017AE3A57454D9C284FC9 (void);
// 0x00000956 System.Void GPTT.Data.APIHelper/<FireExtinguisherList_Search>d__6::System.Collections.IEnumerator.Reset()
extern void U3CFireExtinguisherList_SearchU3Ed__6_System_Collections_IEnumerator_Reset_m14F5AA453C1EF93BD9259DF45DEE0150B2B5F98D (void);
// 0x00000957 System.Object GPTT.Data.APIHelper/<FireExtinguisherList_Search>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CFireExtinguisherList_SearchU3Ed__6_System_Collections_IEnumerator_get_Current_mCA1CAB7CD498E26A7D8A8B225FC545E364E2EA67 (void);
// 0x00000958 System.Void GPTT.Data.APIHelper/<Firemen_Insert>d__7::.ctor(System.Int32)
extern void U3CFiremen_InsertU3Ed__7__ctor_mD08B037037AB223208CBB69E39302F0AF002A00D (void);
// 0x00000959 System.Void GPTT.Data.APIHelper/<Firemen_Insert>d__7::System.IDisposable.Dispose()
extern void U3CFiremen_InsertU3Ed__7_System_IDisposable_Dispose_mAFFC436912F6B03F1CD9ED0113002948035FBA9B (void);
// 0x0000095A System.Boolean GPTT.Data.APIHelper/<Firemen_Insert>d__7::MoveNext()
extern void U3CFiremen_InsertU3Ed__7_MoveNext_m5D74BF7CE972A863902D5EBBAADA2D2A0553292A (void);
// 0x0000095B System.Object GPTT.Data.APIHelper/<Firemen_Insert>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFiremen_InsertU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m304114036223ED98A053CBB1FE9820272B2254D7 (void);
// 0x0000095C System.Void GPTT.Data.APIHelper/<Firemen_Insert>d__7::System.Collections.IEnumerator.Reset()
extern void U3CFiremen_InsertU3Ed__7_System_Collections_IEnumerator_Reset_m83176A07D2F9054B7674EBD2F3FEA3DB51BF455F (void);
// 0x0000095D System.Object GPTT.Data.APIHelper/<Firemen_Insert>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CFiremen_InsertU3Ed__7_System_Collections_IEnumerator_get_Current_mCCBA54FA3C9CCD69BC671C704AB43CA7EEDEA7A9 (void);
// 0x0000095E System.Void GPTT.Data.APIHelper/<FiremenList_Search>d__8::.ctor(System.Int32)
extern void U3CFiremenList_SearchU3Ed__8__ctor_mED2B29AE3E7DB3158EB2CE62BE11D2D340004816 (void);
// 0x0000095F System.Void GPTT.Data.APIHelper/<FiremenList_Search>d__8::System.IDisposable.Dispose()
extern void U3CFiremenList_SearchU3Ed__8_System_IDisposable_Dispose_m63529BAA60B667EC7AC9EAF4A0B69181BF65F326 (void);
// 0x00000960 System.Boolean GPTT.Data.APIHelper/<FiremenList_Search>d__8::MoveNext()
extern void U3CFiremenList_SearchU3Ed__8_MoveNext_mDFE28F3A188E71CF39F7E21A16C834B1F394273E (void);
// 0x00000961 System.Object GPTT.Data.APIHelper/<FiremenList_Search>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFiremenList_SearchU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A01DD4641B64B68990C01041C746A4DD4965D29 (void);
// 0x00000962 System.Void GPTT.Data.APIHelper/<FiremenList_Search>d__8::System.Collections.IEnumerator.Reset()
extern void U3CFiremenList_SearchU3Ed__8_System_Collections_IEnumerator_Reset_m1C9157B81C1E20582C0FDFA5AE6F65A316DAAB29 (void);
// 0x00000963 System.Object GPTT.Data.APIHelper/<FiremenList_Search>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CFiremenList_SearchU3Ed__8_System_Collections_IEnumerator_get_Current_mCFFAAC9A4DF161AA75E664F45BC504BF367564A4 (void);
// 0x00000964 System.Void GPTT.Data.APIHelper/<FiremenDetailList_Search>d__9::.ctor(System.Int32)
extern void U3CFiremenDetailList_SearchU3Ed__9__ctor_m48E3561794F6F154FAD67C35C37CD84756565984 (void);
// 0x00000965 System.Void GPTT.Data.APIHelper/<FiremenDetailList_Search>d__9::System.IDisposable.Dispose()
extern void U3CFiremenDetailList_SearchU3Ed__9_System_IDisposable_Dispose_mBD69026A306F5B6B18720BBB79D83F51074BAECA (void);
// 0x00000966 System.Boolean GPTT.Data.APIHelper/<FiremenDetailList_Search>d__9::MoveNext()
extern void U3CFiremenDetailList_SearchU3Ed__9_MoveNext_mABB9FA607BEB19F011F52CC9FF73AA4E5A93E42F (void);
// 0x00000967 System.Object GPTT.Data.APIHelper/<FiremenDetailList_Search>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFiremenDetailList_SearchU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8AF08EC7D09DF8B616A430DCF3D021CD0720D5B (void);
// 0x00000968 System.Void GPTT.Data.APIHelper/<FiremenDetailList_Search>d__9::System.Collections.IEnumerator.Reset()
extern void U3CFiremenDetailList_SearchU3Ed__9_System_Collections_IEnumerator_Reset_m60C2AED1F904C9F214DF4D2BC0E1891A1E9EB910 (void);
// 0x00000969 System.Object GPTT.Data.APIHelper/<FiremenDetailList_Search>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CFiremenDetailList_SearchU3Ed__9_System_Collections_IEnumerator_get_Current_mB396A6841F15B386A9E248BF2157F7A65891E834 (void);
// 0x0000096A System.Void GPTT.Data.Sample::Start()
extern void Sample_Start_m17A1B560310479D6145D865A647BD612404C85AD (void);
// 0x0000096B System.Void GPTT.Data.Sample::LoginOnclick()
extern void Sample_LoginOnclick_m4BC19770D8779D9DE535E74F30C8536740BDDD36 (void);
// 0x0000096C System.Void GPTT.Data.Sample::LogoutOnclick()
extern void Sample_LogoutOnclick_m5B31C85FFF312831931213811C93AF0303A061AA (void);
// 0x0000096D System.Void GPTT.Data.Sample::FireExtinguisherOnclick()
extern void Sample_FireExtinguisherOnclick_m6027A01965E34F9963CEDFCFC5BB82797BC4FA1F (void);
// 0x0000096E System.Void GPTT.Data.Sample::FireExtinguisherSearchOnclick()
extern void Sample_FireExtinguisherSearchOnclick_m6F12DB56A89A7C84A758E16DCDE4B401B5CB1F0D (void);
// 0x0000096F System.Void GPTT.Data.Sample::FiremenOnclick()
extern void Sample_FiremenOnclick_mAC88FF47ACF2E3D9B548C27CAB988A4204CA5694 (void);
// 0x00000970 System.Void GPTT.Data.Sample::FiremenSearchOnclick()
extern void Sample_FiremenSearchOnclick_mF4E379D0ACD259EAC0985DF3AC0950F4921EBECF (void);
// 0x00000971 System.Void GPTT.Data.Sample::FiremenDetailSearchOnclick()
extern void Sample_FiremenDetailSearchOnclick_m7AB32715B1EA558A4FA59CED5ABAB7D568F8392D (void);
// 0x00000972 System.Void GPTT.Data.Sample::.ctor()
extern void Sample__ctor_mA8231634864E6A7574C766EB7929D3B175E9DFDD (void);
// 0x00000973 System.Void GPTT.Data.Sample::<LoginOnclick>b__2_0(GPTT.Data.API/User)
extern void Sample_U3CLoginOnclickU3Eb__2_0_mF31B1B56CB2B8C586A2EB6CCD88C6AEE84090523 (void);
// 0x00000974 System.Void GPTT.Data.Util::Run()
extern void Util_Run_m7D124C4D8E9934C0E8888D47FFE217DD39370831 (void);
// 0x00000975 System.String GPTT.Data.Util::Encrypt(System.String)
extern void Util_Encrypt_mAD930D9A854DE4776504B0CB05CB1E23AD0E3F03 (void);
// 0x00000976 System.String GPTT.Data.Util::Decrypt(System.String)
extern void Util_Decrypt_mF902BCA1E0CD412BDA35080CC1826D7EBBD0E67D (void);
// 0x00000977 System.Int64 GPTT.Data.Util::GetTimeStamp()
extern void Util_GetTimeStamp_mA1486B0B832C4FE31A60D97CE7922A415F3D9B4D (void);
// 0x00000978 System.Int64 GPTT.Data.Util::GetTimeStamp(System.DateTimeOffset)
extern void Util_GetTimeStamp_m3CA5281EC0FBFEBB622AEA11091945A6C3D8AAED (void);
// 0x00000979 System.String GPTT.Data.Util::ToMD5(System.String)
extern void Util_ToMD5_m42E2FAD12D61F9A131F6E58B1D2D068638618D1B (void);
// 0x0000097A System.String GPTT.Data.Util::GetSerialNumber()
extern void Util_GetSerialNumber_m102F3075F6B492063DE286FEA38692CB571D34E9 (void);
// 0x0000097B System.Void GPTT.Data.Util/Machine::.ctor()
extern void Machine__ctor_m296C002DE86C45F85F38604A3C4B2A6028146A22 (void);
// 0x0000097C System.Boolean OculusSampleFramework.ColorGrabbable::get_Highlight()
extern void ColorGrabbable_get_Highlight_mEE9A5BC6077D6415C0F5AAEBF9268BCEDA9192CB (void);
// 0x0000097D System.Void OculusSampleFramework.ColorGrabbable::set_Highlight(System.Boolean)
extern void ColorGrabbable_set_Highlight_mF9C67E77A7CAB0FF638BDF7EE4C359530267191F (void);
// 0x0000097E System.Void OculusSampleFramework.ColorGrabbable::UpdateColor()
extern void ColorGrabbable_UpdateColor_m4D60081FC49D8BD81C4B63B35405B144232F8C60 (void);
// 0x0000097F System.Void OculusSampleFramework.ColorGrabbable::GrabBegin(OVRGrabber,UnityEngine.Collider)
extern void ColorGrabbable_GrabBegin_m756D3F58E950E5B54A1B7C4E79F2BD99AE630108 (void);
// 0x00000980 System.Void OculusSampleFramework.ColorGrabbable::GrabEnd(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ColorGrabbable_GrabEnd_mCBB05FAD02AA291052245779AD795CEDA97F960F (void);
// 0x00000981 System.Void OculusSampleFramework.ColorGrabbable::Awake()
extern void ColorGrabbable_Awake_mC26EA500134341FD38388600111FF6E5668A8832 (void);
// 0x00000982 System.Void OculusSampleFramework.ColorGrabbable::SetColor(UnityEngine.Color)
extern void ColorGrabbable_SetColor_m1EA4F276B7EFDBD7EE35D598963E888307850540 (void);
// 0x00000983 System.Void OculusSampleFramework.ColorGrabbable::.ctor()
extern void ColorGrabbable__ctor_mCC1D8EDB3DB7A4DBEE8D15AC5A361A1B102EEA34 (void);
// 0x00000984 System.Void OculusSampleFramework.ColorGrabbable::.cctor()
extern void ColorGrabbable__cctor_mF2135B0A5F041846D69DE4C6085F38F57A9BFE97 (void);
// 0x00000985 System.Boolean OculusSampleFramework.DistanceGrabbable::get_InRange()
extern void DistanceGrabbable_get_InRange_m056971DE194AD0A8B85A8E8E3E82F76711FAF8EA (void);
// 0x00000986 System.Void OculusSampleFramework.DistanceGrabbable::set_InRange(System.Boolean)
extern void DistanceGrabbable_set_InRange_m0EFDE0426D5819B04621E607860A63531577586D (void);
// 0x00000987 System.Boolean OculusSampleFramework.DistanceGrabbable::get_Targeted()
extern void DistanceGrabbable_get_Targeted_m5D48533FA91BE16785B21D7881202E27DA1A8D0A (void);
// 0x00000988 System.Void OculusSampleFramework.DistanceGrabbable::set_Targeted(System.Boolean)
extern void DistanceGrabbable_set_Targeted_m9D01FAEC07F2BBD49B6DA12F1A014D2AFDBDD0E0 (void);
// 0x00000989 System.Void OculusSampleFramework.DistanceGrabbable::Start()
extern void DistanceGrabbable_Start_m0F13F152E7FCB05A41B8E0E8B6699462531AF358 (void);
// 0x0000098A System.Void OculusSampleFramework.DistanceGrabbable::RefreshCrosshair()
extern void DistanceGrabbable_RefreshCrosshair_m5461E3A1849F56BC79A9BEC50F63EE0F5C8C08A9 (void);
// 0x0000098B System.Void OculusSampleFramework.DistanceGrabbable::.ctor()
extern void DistanceGrabbable__ctor_mB69087D8AA20E7C90FAA564AC6D963C5606CDE22 (void);
// 0x0000098C System.Boolean OculusSampleFramework.DistanceGrabber::get_UseSpherecast()
extern void DistanceGrabber_get_UseSpherecast_mF25C99BCAF0C6E6CA18A4C42E4AB34BF8BFC6D5F (void);
// 0x0000098D System.Void OculusSampleFramework.DistanceGrabber::set_UseSpherecast(System.Boolean)
extern void DistanceGrabber_set_UseSpherecast_mE0BAA3127799C6328433698FA578D4E71D8AC86F (void);
// 0x0000098E System.Void OculusSampleFramework.DistanceGrabber::Start()
extern void DistanceGrabber_Start_m775855E7B9023538B7E56155A6597519A953ED70 (void);
// 0x0000098F System.Void OculusSampleFramework.DistanceGrabber::Update()
extern void DistanceGrabber_Update_m960BB0DB05A12AE7386926698E0153ED5BEAEAEE (void);
// 0x00000990 System.Void OculusSampleFramework.DistanceGrabber::GrabBegin()
extern void DistanceGrabber_GrabBegin_m77865480609755B1282A5A546DC6A53FA63B49CF (void);
// 0x00000991 System.Void OculusSampleFramework.DistanceGrabber::MoveGrabbedObject(UnityEngine.Vector3,UnityEngine.Quaternion,System.Boolean)
extern void DistanceGrabber_MoveGrabbedObject_m6B815535F8AAC860485491CCA49CD8046802F0A9 (void);
// 0x00000992 OculusSampleFramework.DistanceGrabbable OculusSampleFramework.DistanceGrabber::HitInfoToGrabbable(UnityEngine.RaycastHit)
extern void DistanceGrabber_HitInfoToGrabbable_m6A5994E443368A6D81B8FDABD2202A42D581AF19 (void);
// 0x00000993 System.Boolean OculusSampleFramework.DistanceGrabber::FindTarget(OculusSampleFramework.DistanceGrabbable&,UnityEngine.Collider&)
extern void DistanceGrabber_FindTarget_m1F30C7639B39EEB998BC94B7AD6483A235F162EC (void);
// 0x00000994 System.Boolean OculusSampleFramework.DistanceGrabber::FindTargetWithSpherecast(OculusSampleFramework.DistanceGrabbable&,UnityEngine.Collider&)
extern void DistanceGrabber_FindTargetWithSpherecast_m561C09222DCCD4C52DF037E1AFD95D6EEFF3645D (void);
// 0x00000995 System.Void OculusSampleFramework.DistanceGrabber::GrabVolumeEnable(System.Boolean)
extern void DistanceGrabber_GrabVolumeEnable_m5AE9B41CBEB5A5EBC1D2F7945B875794FD93A405 (void);
// 0x00000996 System.Void OculusSampleFramework.DistanceGrabber::OffhandGrabbed(OVRGrabbable)
extern void DistanceGrabber_OffhandGrabbed_m31A56380ABE7DB5847799670467539468AF085DB (void);
// 0x00000997 System.Void OculusSampleFramework.DistanceGrabber::.ctor()
extern void DistanceGrabber__ctor_m80196B68D9FB9CDE30E2CEC5690F4BB97740A366 (void);
// 0x00000998 System.Void OculusSampleFramework.GrabManager::OnTriggerEnter(UnityEngine.Collider)
extern void GrabManager_OnTriggerEnter_m6EF4F094BEAE1A7A31854F98826B3FBB1B74E91F (void);
// 0x00000999 System.Void OculusSampleFramework.GrabManager::OnTriggerExit(UnityEngine.Collider)
extern void GrabManager_OnTriggerExit_mC52984A8FECA9E7098E2615B0DAFEFAEA92997F7 (void);
// 0x0000099A System.Void OculusSampleFramework.GrabManager::.ctor()
extern void GrabManager__ctor_m5A60AC8D6E026A5A4FDAB55DAC7CEDCDD2913376 (void);
// 0x0000099B System.Void OculusSampleFramework.GrabbableCrosshair::Start()
extern void GrabbableCrosshair_Start_mFE636535DBC51AC58F63DDB687E6D1875D63FD97 (void);
// 0x0000099C System.Void OculusSampleFramework.GrabbableCrosshair::SetState(OculusSampleFramework.GrabbableCrosshair/CrosshairState)
extern void GrabbableCrosshair_SetState_m5D2604BA1418ED7C18E0CFB823CAA172EA1B23C6 (void);
// 0x0000099D System.Void OculusSampleFramework.GrabbableCrosshair::Update()
extern void GrabbableCrosshair_Update_m9A0A89691DDE8B292D735158EB3C6EDC852DC979 (void);
// 0x0000099E System.Void OculusSampleFramework.GrabbableCrosshair::.ctor()
extern void GrabbableCrosshair__ctor_m59EFA7290C1E0CFC102C201C1303868B7AE1ED0D (void);
// 0x0000099F System.Void OculusSampleFramework.PauseOnInputLoss::Start()
extern void PauseOnInputLoss_Start_m309D5D446C65347D01220D6965E848AE62DB809D (void);
// 0x000009A0 System.Void OculusSampleFramework.PauseOnInputLoss::OnInputFocusLost()
extern void PauseOnInputLoss_OnInputFocusLost_mAC19B6B6F2BD7435BD93C83448F67F22A86ECEB2 (void);
// 0x000009A1 System.Void OculusSampleFramework.PauseOnInputLoss::OnInputFocusAcquired()
extern void PauseOnInputLoss_OnInputFocusAcquired_m21D3CFB7EF526E324CBF8F43F6B2102EACDEC057 (void);
// 0x000009A2 System.Void OculusSampleFramework.PauseOnInputLoss::.ctor()
extern void PauseOnInputLoss__ctor_m0648631C18A65BFA8306CBD7C10288D6CB81EC25 (void);
// 0x000009A3 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::OnDisable()
extern void BoneCapsuleTriggerLogic_OnDisable_m78D90F38B9238A03909E14C79AC447972BE23702 (void);
// 0x000009A4 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::Update()
extern void BoneCapsuleTriggerLogic_Update_mA3BC03489B0871D073AD4CF06850AD26768D4A24 (void);
// 0x000009A5 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::OnTriggerEnter(UnityEngine.Collider)
extern void BoneCapsuleTriggerLogic_OnTriggerEnter_m711C97FA1E316AB56D8084519BE087A638497C39 (void);
// 0x000009A6 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::OnTriggerExit(UnityEngine.Collider)
extern void BoneCapsuleTriggerLogic_OnTriggerExit_m5E0F1A427B4A13D901592A0CA13FDC839C6C7C65 (void);
// 0x000009A7 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::CleanUpDeadColliders()
extern void BoneCapsuleTriggerLogic_CleanUpDeadColliders_m93077C24AD19895958A85B39B597DDAE5EFC90B1 (void);
// 0x000009A8 System.Void OculusSampleFramework.BoneCapsuleTriggerLogic::.ctor()
extern void BoneCapsuleTriggerLogic__ctor_m11A006B3E2E5D97ACC8F7908F57598EB8A7FADBD (void);
// 0x000009A9 System.Int32 OculusSampleFramework.ButtonController::get_ValidToolTagsMask()
extern void ButtonController_get_ValidToolTagsMask_mC1A23D42383354957AC3ED2EE4E0E443E4BD7EE8 (void);
// 0x000009AA UnityEngine.Vector3 OculusSampleFramework.ButtonController::get_LocalButtonDirection()
extern void ButtonController_get_LocalButtonDirection_mCD9B2BA21A5208ACF45B9DE2348951CED9A2F597 (void);
// 0x000009AB OculusSampleFramework.InteractableState OculusSampleFramework.ButtonController::get_CurrentButtonState()
extern void ButtonController_get_CurrentButtonState_m97D3281E1AA62F0B28B9FC13D5C6D97E8898374A (void);
// 0x000009AC System.Void OculusSampleFramework.ButtonController::set_CurrentButtonState(OculusSampleFramework.InteractableState)
extern void ButtonController_set_CurrentButtonState_mDDD60059979F7FCE78D320A3D034ED7550F076C1 (void);
// 0x000009AD System.Void OculusSampleFramework.ButtonController::Awake()
extern void ButtonController_Awake_m9A642360ACD027C8D3C3B39598D5D7483AB9D199 (void);
// 0x000009AE System.Void OculusSampleFramework.ButtonController::FireInteractionEventsOnDepth(OculusSampleFramework.InteractableCollisionDepth,OculusSampleFramework.InteractableTool,OculusSampleFramework.InteractionType)
extern void ButtonController_FireInteractionEventsOnDepth_m8532030AB88C8754A41BA83EFE29ED352DE838C9 (void);
// 0x000009AF System.Void OculusSampleFramework.ButtonController::UpdateCollisionDepth(OculusSampleFramework.InteractableTool,OculusSampleFramework.InteractableCollisionDepth,OculusSampleFramework.InteractableCollisionDepth)
extern void ButtonController_UpdateCollisionDepth_mD5E62BC8315DE7342BBD20473AEE3FDD65788A3D (void);
// 0x000009B0 OculusSampleFramework.InteractableState OculusSampleFramework.ButtonController::GetUpcomingStateNearField(OculusSampleFramework.InteractableState,OculusSampleFramework.InteractableCollisionDepth,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void ButtonController_GetUpcomingStateNearField_m4377557E72D77C2AA2D3FE1DEAE6F5067F54D12E (void);
// 0x000009B1 System.Void OculusSampleFramework.ButtonController::ForceResetButton()
extern void ButtonController_ForceResetButton_m82BCDD2D684499C11C329CB5AD25C9DAD23AEE9E (void);
// 0x000009B2 System.Boolean OculusSampleFramework.ButtonController::IsValidContact(OculusSampleFramework.InteractableTool,UnityEngine.Vector3)
extern void ButtonController_IsValidContact_mF9BEA01F40D5CF760D0D26E15603D51B0BE2C59E (void);
// 0x000009B3 System.Boolean OculusSampleFramework.ButtonController::PassEntryTest(OculusSampleFramework.InteractableTool,UnityEngine.Vector3)
extern void ButtonController_PassEntryTest_m1CC5A6460FD676ADC06C3D8C003D3E90784028B0 (void);
// 0x000009B4 System.Boolean OculusSampleFramework.ButtonController::PassPerpTest(OculusSampleFramework.InteractableTool,UnityEngine.Vector3)
extern void ButtonController_PassPerpTest_mCCEFC8E20AAF9FBE58E5BEDCE554CEF048EC6196 (void);
// 0x000009B5 System.Void OculusSampleFramework.ButtonController::.ctor()
extern void ButtonController__ctor_m64221E61350B4251820CAA250163801C4AAC1781 (void);
// 0x000009B6 UnityEngine.Collider OculusSampleFramework.ButtonTriggerZone::get_Collider()
extern void ButtonTriggerZone_get_Collider_mA97511B633C8FA23B1E1B864FE94DF7771424CCA (void);
// 0x000009B7 System.Void OculusSampleFramework.ButtonTriggerZone::set_Collider(UnityEngine.Collider)
extern void ButtonTriggerZone_set_Collider_mB7AE7E1CA02A2EB268DD49032D0F33472F443AB5 (void);
// 0x000009B8 OculusSampleFramework.Interactable OculusSampleFramework.ButtonTriggerZone::get_ParentInteractable()
extern void ButtonTriggerZone_get_ParentInteractable_m14EDBF5B2E361C7B71513E0FD4D10EA87D388693 (void);
// 0x000009B9 System.Void OculusSampleFramework.ButtonTriggerZone::set_ParentInteractable(OculusSampleFramework.Interactable)
extern void ButtonTriggerZone_set_ParentInteractable_m6BF1259D2ED0DF4B565C08D00C2A867074C03A53 (void);
// 0x000009BA OculusSampleFramework.InteractableCollisionDepth OculusSampleFramework.ButtonTriggerZone::get_CollisionDepth()
extern void ButtonTriggerZone_get_CollisionDepth_m066AACBCB480A4D05E1F9910AAB71F3EB4F90558 (void);
// 0x000009BB System.Void OculusSampleFramework.ButtonTriggerZone::Awake()
extern void ButtonTriggerZone_Awake_mAEDDFC443C2337ECC22046203921081412A8253C (void);
// 0x000009BC System.Void OculusSampleFramework.ButtonTriggerZone::.ctor()
extern void ButtonTriggerZone__ctor_mE84EA0EF8CC850618DF29DCF722071F662F46D35 (void);
// 0x000009BD UnityEngine.Collider OculusSampleFramework.ColliderZone::get_Collider()
// 0x000009BE OculusSampleFramework.Interactable OculusSampleFramework.ColliderZone::get_ParentInteractable()
// 0x000009BF OculusSampleFramework.InteractableCollisionDepth OculusSampleFramework.ColliderZone::get_CollisionDepth()
// 0x000009C0 System.Void OculusSampleFramework.ColliderZoneArgs::.ctor(OculusSampleFramework.ColliderZone,System.Single,OculusSampleFramework.InteractableTool,OculusSampleFramework.InteractionType)
extern void ColliderZoneArgs__ctor_m2F17926EE45B385F3291ABB057D0DB1C46E7C721 (void);
// 0x000009C1 OVRHand OculusSampleFramework.HandsManager::get_RightHand()
extern void HandsManager_get_RightHand_mE9F238C463B7E656776BAD73EFE3EF3EDF4479B3 (void);
// 0x000009C2 System.Void OculusSampleFramework.HandsManager::set_RightHand(OVRHand)
extern void HandsManager_set_RightHand_m007F2D3739DF6176DA4F7E496C36EEB66A2437B6 (void);
// 0x000009C3 OVRSkeleton OculusSampleFramework.HandsManager::get_RightHandSkeleton()
extern void HandsManager_get_RightHandSkeleton_mBCF403959F6ABB1D38A64EDFB2EF5410A71BA9D8 (void);
// 0x000009C4 System.Void OculusSampleFramework.HandsManager::set_RightHandSkeleton(OVRSkeleton)
extern void HandsManager_set_RightHandSkeleton_m9F1BE77E728E2B0ADE142380CA4EA62589754159 (void);
// 0x000009C5 OVRSkeletonRenderer OculusSampleFramework.HandsManager::get_RightHandSkeletonRenderer()
extern void HandsManager_get_RightHandSkeletonRenderer_mEBEE32D6AB8B63D8DA808FDBB8FC20F6E066D28C (void);
// 0x000009C6 System.Void OculusSampleFramework.HandsManager::set_RightHandSkeletonRenderer(OVRSkeletonRenderer)
extern void HandsManager_set_RightHandSkeletonRenderer_mE330C23C803564C20D05F29CCA79AA575E32D3F1 (void);
// 0x000009C7 OVRMesh OculusSampleFramework.HandsManager::get_RightHandMesh()
extern void HandsManager_get_RightHandMesh_mA1B64CFD74E89E1F8A3A59C040AC9AA732837C6D (void);
// 0x000009C8 System.Void OculusSampleFramework.HandsManager::set_RightHandMesh(OVRMesh)
extern void HandsManager_set_RightHandMesh_m8B308743A2F158A112E5C0EBB6A54A1D19451DBD (void);
// 0x000009C9 OVRMeshRenderer OculusSampleFramework.HandsManager::get_RightHandMeshRenderer()
extern void HandsManager_get_RightHandMeshRenderer_mE2D447583D12CB6E51360CD78BD25B85C33D613E (void);
// 0x000009CA System.Void OculusSampleFramework.HandsManager::set_RightHandMeshRenderer(OVRMeshRenderer)
extern void HandsManager_set_RightHandMeshRenderer_mD2468B3A5B1C6EA97F07D6314DA50969C600380E (void);
// 0x000009CB OVRHand OculusSampleFramework.HandsManager::get_LeftHand()
extern void HandsManager_get_LeftHand_m53807ABE4939BD2D9260E268C6674B7011713BA8 (void);
// 0x000009CC System.Void OculusSampleFramework.HandsManager::set_LeftHand(OVRHand)
extern void HandsManager_set_LeftHand_m32768139F21ADDBCD87E2833D2BB30DAE5B59F4F (void);
// 0x000009CD OVRSkeleton OculusSampleFramework.HandsManager::get_LeftHandSkeleton()
extern void HandsManager_get_LeftHandSkeleton_m44A310FFC70C3CB410D2DC2F235F57B1B58FF2D2 (void);
// 0x000009CE System.Void OculusSampleFramework.HandsManager::set_LeftHandSkeleton(OVRSkeleton)
extern void HandsManager_set_LeftHandSkeleton_mC76A55A9DCF670FC60B34CC92DD81027E7735B08 (void);
// 0x000009CF OVRSkeletonRenderer OculusSampleFramework.HandsManager::get_LeftHandSkeletonRenderer()
extern void HandsManager_get_LeftHandSkeletonRenderer_m8FD29F034B1EC81684F4C74A8301596395011B48 (void);
// 0x000009D0 System.Void OculusSampleFramework.HandsManager::set_LeftHandSkeletonRenderer(OVRSkeletonRenderer)
extern void HandsManager_set_LeftHandSkeletonRenderer_m1C5A2A642C6614C040085355D8214E16D934D28D (void);
// 0x000009D1 OVRMesh OculusSampleFramework.HandsManager::get_LeftHandMesh()
extern void HandsManager_get_LeftHandMesh_m16A9E535B1B520F309D0886704A3201648B16CDD (void);
// 0x000009D2 System.Void OculusSampleFramework.HandsManager::set_LeftHandMesh(OVRMesh)
extern void HandsManager_set_LeftHandMesh_m56E8B4E036DA7AACB922228B4FE27FCC272278E3 (void);
// 0x000009D3 OVRMeshRenderer OculusSampleFramework.HandsManager::get_LeftHandMeshRenderer()
extern void HandsManager_get_LeftHandMeshRenderer_mDE3580A395755759374279284C9650D59D09CFF2 (void);
// 0x000009D4 System.Void OculusSampleFramework.HandsManager::set_LeftHandMeshRenderer(OVRMeshRenderer)
extern void HandsManager_set_LeftHandMeshRenderer_m102CF222EBA2577F468099C726325DF3B779FF64 (void);
// 0x000009D5 OculusSampleFramework.HandsManager OculusSampleFramework.HandsManager::get_Instance()
extern void HandsManager_get_Instance_m749E74CBE6267511C58A4033FB6FDF3C8D03AF1F (void);
// 0x000009D6 System.Void OculusSampleFramework.HandsManager::set_Instance(OculusSampleFramework.HandsManager)
extern void HandsManager_set_Instance_m0F4B8CD87D1B95FB7F64D3BD246FAEC5ACBD7F53 (void);
// 0x000009D7 System.Void OculusSampleFramework.HandsManager::Awake()
extern void HandsManager_Awake_m7D236135CCB8A2F47F442B2EE89281B985ABA02A (void);
// 0x000009D8 System.Void OculusSampleFramework.HandsManager::Update()
extern void HandsManager_Update_m3AAFA9F8B8B994326B7695807BA0D38FF592C2A3 (void);
// 0x000009D9 System.Collections.IEnumerator OculusSampleFramework.HandsManager::FindSkeletonVisualGameObjects()
extern void HandsManager_FindSkeletonVisualGameObjects_m31E51747D167E74132150423190EC4AAC13B1E53 (void);
// 0x000009DA System.Void OculusSampleFramework.HandsManager::SwitchVisualization()
extern void HandsManager_SwitchVisualization_mFF5FEEE41ACCB620EC9E98BA8364A343CE7F5584 (void);
// 0x000009DB System.Void OculusSampleFramework.HandsManager::SetToCurrentVisualMode()
extern void HandsManager_SetToCurrentVisualMode_m183D5AEF5523A39C0054A34E304FE88E25E9E277 (void);
// 0x000009DC System.Collections.Generic.List`1<OVRBoneCapsule> OculusSampleFramework.HandsManager::GetCapsulesPerBone(OVRSkeleton,OVRSkeleton/BoneId)
extern void HandsManager_GetCapsulesPerBone_mC98582D7BF3248388FA2A5153586EDCAF029D8EB (void);
// 0x000009DD System.Boolean OculusSampleFramework.HandsManager::IsInitialized()
extern void HandsManager_IsInitialized_m607714995C91CC61C6672A91A631EAC7FA934917 (void);
// 0x000009DE System.Void OculusSampleFramework.HandsManager::.ctor()
extern void HandsManager__ctor_mF764E9D2BE5BB64D933E7B6925E0F549EA1341C6 (void);
// 0x000009DF System.Void OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::.ctor(System.Int32)
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52__ctor_mB55CA2BF6CE22AC93FB1D1E0324FA76F2895B6BB (void);
// 0x000009E0 System.Void OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::System.IDisposable.Dispose()
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52_System_IDisposable_Dispose_mBB9AC6B3987FBE05D17229FC65772D6AEF1AE5C5 (void);
// 0x000009E1 System.Boolean OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::MoveNext()
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52_MoveNext_mDB2E7404E01F1E9764DFD7C7E69BE594B4130CFA (void);
// 0x000009E2 System.Object OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53507F454464337DB141FA4B5B17D2ABBD2C0FE4 (void);
// 0x000009E3 System.Void OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::System.Collections.IEnumerator.Reset()
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_IEnumerator_Reset_m38AAC961B881CFB50A5DD5D622E430A3F56E6416 (void);
// 0x000009E4 System.Object OculusSampleFramework.HandsManager/<FindSkeletonVisualGameObjects>d__52::System.Collections.IEnumerator.get_Current()
extern void U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_IEnumerator_get_Current_m1534C6E698DEA0AE22B6DB0F7352E4815D614CD3 (void);
// 0x000009E5 OculusSampleFramework.ColliderZone OculusSampleFramework.Interactable::get_ProximityCollider()
extern void Interactable_get_ProximityCollider_mF6F604B5AC84CDF8FDA76502CF97731CC056FF08 (void);
// 0x000009E6 OculusSampleFramework.ColliderZone OculusSampleFramework.Interactable::get_ContactCollider()
extern void Interactable_get_ContactCollider_m4735130C7B2B2FE783C186EBC1A482C7E29D3873 (void);
// 0x000009E7 OculusSampleFramework.ColliderZone OculusSampleFramework.Interactable::get_ActionCollider()
extern void Interactable_get_ActionCollider_mBAF9B046536B533C7BA2226926BBC50E17B6726D (void);
// 0x000009E8 System.Int32 OculusSampleFramework.Interactable::get_ValidToolTagsMask()
extern void Interactable_get_ValidToolTagsMask_m85DABBD0A6116F184E84F198CAC92C26121F6DF6 (void);
// 0x000009E9 System.Void OculusSampleFramework.Interactable::add_ProximityZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_add_ProximityZoneEvent_m2EFA9B8880CE03E14E54AAF2282D089F2E4B3FA0 (void);
// 0x000009EA System.Void OculusSampleFramework.Interactable::remove_ProximityZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_remove_ProximityZoneEvent_m314D6AC5B0CAC5A5FF915B383F9C4E24A44C0183 (void);
// 0x000009EB System.Void OculusSampleFramework.Interactable::OnProximityZoneEvent(OculusSampleFramework.ColliderZoneArgs)
extern void Interactable_OnProximityZoneEvent_mDED9C800E85CF54A06FB3CCA1DB2EF51ECF15A1F (void);
// 0x000009EC System.Void OculusSampleFramework.Interactable::add_ContactZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_add_ContactZoneEvent_m284AEAF4458BB62779A75C591F261091518B3481 (void);
// 0x000009ED System.Void OculusSampleFramework.Interactable::remove_ContactZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_remove_ContactZoneEvent_m96A59B1DA482F032CF5D490918466CF68A1A5D02 (void);
// 0x000009EE System.Void OculusSampleFramework.Interactable::OnContactZoneEvent(OculusSampleFramework.ColliderZoneArgs)
extern void Interactable_OnContactZoneEvent_mF346FBAB8E4F53A26B148E8126759D64A70B5E73 (void);
// 0x000009EF System.Void OculusSampleFramework.Interactable::add_ActionZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_add_ActionZoneEvent_m303D57C7749AD0BE641D5031D90F4A1053B9B438 (void);
// 0x000009F0 System.Void OculusSampleFramework.Interactable::remove_ActionZoneEvent(System.Action`1<OculusSampleFramework.ColliderZoneArgs>)
extern void Interactable_remove_ActionZoneEvent_m4E8476203B9F028436EBD7F9E29DD629BE44FF2D (void);
// 0x000009F1 System.Void OculusSampleFramework.Interactable::OnActionZoneEvent(OculusSampleFramework.ColliderZoneArgs)
extern void Interactable_OnActionZoneEvent_mA4413FFF1DD8593420D456C9A8EEA3487D1CB9DE (void);
// 0x000009F2 System.Void OculusSampleFramework.Interactable::UpdateCollisionDepth(OculusSampleFramework.InteractableTool,OculusSampleFramework.InteractableCollisionDepth,OculusSampleFramework.InteractableCollisionDepth)
// 0x000009F3 System.Void OculusSampleFramework.Interactable::Awake()
extern void Interactable_Awake_m673622B78CF87568C609EDD039F0D947CF4E9F32 (void);
// 0x000009F4 System.Void OculusSampleFramework.Interactable::OnDestroy()
extern void Interactable_OnDestroy_m24FA62B42CADA4737F199EFB99C057CCD0ADFB94 (void);
// 0x000009F5 System.Void OculusSampleFramework.Interactable::.ctor()
extern void Interactable__ctor_m38A75FA2BE69E84CE6E111E03319FA303714B4D6 (void);
// 0x000009F6 System.Void OculusSampleFramework.Interactable/InteractableStateArgsEvent::.ctor()
extern void InteractableStateArgsEvent__ctor_mD35D9AB2D5844DC8C2246F8AD4EA36A0B09308DE (void);
// 0x000009F7 System.Void OculusSampleFramework.InteractableStateArgs::.ctor(OculusSampleFramework.Interactable,OculusSampleFramework.InteractableTool,OculusSampleFramework.InteractableState,OculusSampleFramework.InteractableState,OculusSampleFramework.ColliderZoneArgs)
extern void InteractableStateArgs__ctor_m3C317F0382FE5F267D662DDCFABE336F06862624 (void);
// 0x000009F8 System.Collections.Generic.HashSet`1<OculusSampleFramework.Interactable> OculusSampleFramework.InteractableRegistry::get_Interactables()
extern void InteractableRegistry_get_Interactables_mA04622D45767EF063B111AC2407C5B0A46DE07B9 (void);
// 0x000009F9 System.Void OculusSampleFramework.InteractableRegistry::RegisterInteractable(OculusSampleFramework.Interactable)
extern void InteractableRegistry_RegisterInteractable_m3D376455E898032EF5A1C68D44E0A14AE631CB84 (void);
// 0x000009FA System.Void OculusSampleFramework.InteractableRegistry::UnregisterInteractable(OculusSampleFramework.Interactable)
extern void InteractableRegistry_UnregisterInteractable_m7DF572861A04C4D9D3C14627C2A367F2AEFE5E0A (void);
// 0x000009FB System.Void OculusSampleFramework.InteractableRegistry::.ctor()
extern void InteractableRegistry__ctor_mF51128E4BD939E58FF4E97F8F43DF49290548B80 (void);
// 0x000009FC System.Void OculusSampleFramework.InteractableRegistry::.cctor()
extern void InteractableRegistry__cctor_m2C0C6016085C12A509690BDD8585897D6576D023 (void);
// 0x000009FD System.Void OculusSampleFramework.InteractableToolsCreator::Awake()
extern void InteractableToolsCreator_Awake_m52A416198A9987B601BFB7B4DA2D4F81763787D5 (void);
// 0x000009FE System.Collections.IEnumerator OculusSampleFramework.InteractableToolsCreator::AttachToolsToHands(UnityEngine.Transform[],System.Boolean)
extern void InteractableToolsCreator_AttachToolsToHands_m494AA7A9F37A70A890E0E386D44474483AAB9169 (void);
// 0x000009FF System.Void OculusSampleFramework.InteractableToolsCreator::AttachToolToHandTransform(UnityEngine.Transform,System.Boolean)
extern void InteractableToolsCreator_AttachToolToHandTransform_m76F801FCEFADB36E154C683F39DAC5A38CD6F1D5 (void);
// 0x00000A00 System.Void OculusSampleFramework.InteractableToolsCreator::.ctor()
extern void InteractableToolsCreator__ctor_m3B28F7ECE7DC21C19A7F090418D93B4A90D279A4 (void);
// 0x00000A01 System.Void OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::.ctor(System.Int32)
extern void U3CAttachToolsToHandsU3Ed__3__ctor_m734EDB11729722D3E2FF780175D03056A3FB6B59 (void);
// 0x00000A02 System.Void OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::System.IDisposable.Dispose()
extern void U3CAttachToolsToHandsU3Ed__3_System_IDisposable_Dispose_m465E039D390579A8A71EDEA3E92B1B21759BBFF9 (void);
// 0x00000A03 System.Boolean OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::MoveNext()
extern void U3CAttachToolsToHandsU3Ed__3_MoveNext_mED0C488063607CCD70041DB0782687049723B1E0 (void);
// 0x00000A04 System.Void OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::<>m__Finally1()
extern void U3CAttachToolsToHandsU3Ed__3_U3CU3Em__Finally1_m8A2BD809ED53D8BAB20DB018A676FEC81B1C6206 (void);
// 0x00000A05 System.Object OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttachToolsToHandsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9D3D39C28404672F886B0E4DF9571ADEFD737D (void);
// 0x00000A06 System.Void OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAttachToolsToHandsU3Ed__3_System_Collections_IEnumerator_Reset_mF4B96290820469D2C97EEF77C56725A051EE2A5B (void);
// 0x00000A07 System.Object OculusSampleFramework.InteractableToolsCreator/<AttachToolsToHands>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAttachToolsToHandsU3Ed__3_System_Collections_IEnumerator_get_Current_m1BC783E9E2AEAC011D84DDF68154B2A50B83686F (void);
// 0x00000A08 OculusSampleFramework.InteractableToolsInputRouter OculusSampleFramework.InteractableToolsInputRouter::get_Instance()
extern void InteractableToolsInputRouter_get_Instance_m688D5ADD47AD768092EA65094D4763D31CA37DC7 (void);
// 0x00000A09 System.Void OculusSampleFramework.InteractableToolsInputRouter::RegisterInteractableTool(OculusSampleFramework.InteractableTool)
extern void InteractableToolsInputRouter_RegisterInteractableTool_m0427DDDC21A231B5E405F2D2B2C91470C99F0656 (void);
// 0x00000A0A System.Void OculusSampleFramework.InteractableToolsInputRouter::UnregisterInteractableTool(OculusSampleFramework.InteractableTool)
extern void InteractableToolsInputRouter_UnregisterInteractableTool_m10F7F03FBF73F6D75A7D9FB96564C26190BCF96A (void);
// 0x00000A0B System.Void OculusSampleFramework.InteractableToolsInputRouter::Update()
extern void InteractableToolsInputRouter_Update_mFFABA2B1A23E61F20573C7589FF38352FBF5B09B (void);
// 0x00000A0C System.Boolean OculusSampleFramework.InteractableToolsInputRouter::UpdateToolsAndEnableState(System.Collections.Generic.HashSet`1<OculusSampleFramework.InteractableTool>,System.Boolean)
extern void InteractableToolsInputRouter_UpdateToolsAndEnableState_mDC4F03CF1C73759E460B7C55D69DF2953B928516 (void);
// 0x00000A0D System.Boolean OculusSampleFramework.InteractableToolsInputRouter::UpdateTools(System.Collections.Generic.HashSet`1<OculusSampleFramework.InteractableTool>,System.Boolean)
extern void InteractableToolsInputRouter_UpdateTools_m497363491848B42184F5CD40662A510E5236FE5F (void);
// 0x00000A0E System.Void OculusSampleFramework.InteractableToolsInputRouter::ToggleToolsEnableState(System.Collections.Generic.HashSet`1<OculusSampleFramework.InteractableTool>,System.Boolean)
extern void InteractableToolsInputRouter_ToggleToolsEnableState_mCB03C3A19E2CF7287E61B7BD684523AF54DECAAF (void);
// 0x00000A0F System.Void OculusSampleFramework.InteractableToolsInputRouter::.ctor()
extern void InteractableToolsInputRouter__ctor_m1F12838905D93E459F495D3417F16A3347BC7359 (void);
// 0x00000A10 OculusSampleFramework.InteractableToolTags OculusSampleFramework.FingerTipPokeTool::get_ToolTags()
extern void FingerTipPokeTool_get_ToolTags_mBD6F55F53B1190B9D1A07EE70C58B117C35DC144 (void);
// 0x00000A11 OculusSampleFramework.ToolInputState OculusSampleFramework.FingerTipPokeTool::get_ToolInputState()
extern void FingerTipPokeTool_get_ToolInputState_m5135949E46396699EFB438DCEF083077CA58A42D (void);
// 0x00000A12 System.Boolean OculusSampleFramework.FingerTipPokeTool::get_IsFarFieldTool()
extern void FingerTipPokeTool_get_IsFarFieldTool_m19F2F230707836E77DACA32EA3295BB3836E38FE (void);
// 0x00000A13 System.Boolean OculusSampleFramework.FingerTipPokeTool::get_EnableState()
extern void FingerTipPokeTool_get_EnableState_mAFAC89BBE30FA63FD0361CB2C73F546A6421A81A (void);
// 0x00000A14 System.Void OculusSampleFramework.FingerTipPokeTool::set_EnableState(System.Boolean)
extern void FingerTipPokeTool_set_EnableState_m53263FC9F17DBD554E665FE970FFFB8660840772 (void);
// 0x00000A15 System.Void OculusSampleFramework.FingerTipPokeTool::Initialize()
extern void FingerTipPokeTool_Initialize_mF6EAF229A254AAA628C55FD25DFC45F6DB52603F (void);
// 0x00000A16 System.Collections.IEnumerator OculusSampleFramework.FingerTipPokeTool::AttachTriggerLogic()
extern void FingerTipPokeTool_AttachTriggerLogic_m806CC48725DBC7E41BE57F20DA730BEB3C96EE1F (void);
// 0x00000A17 System.Void OculusSampleFramework.FingerTipPokeTool::Update()
extern void FingerTipPokeTool_Update_m2C3F38F5A27DC330842FE5B82FCA1C850EE3D85B (void);
// 0x00000A18 System.Void OculusSampleFramework.FingerTipPokeTool::UpdateAverageVelocity()
extern void FingerTipPokeTool_UpdateAverageVelocity_mC1BFD7068F92A60898578833DB93CF37BCA75E38 (void);
// 0x00000A19 System.Void OculusSampleFramework.FingerTipPokeTool::CheckAndUpdateScale()
extern void FingerTipPokeTool_CheckAndUpdateScale_mDE1DDF48D29BA291BCE6EDB0EDB6CDD04E803207 (void);
// 0x00000A1A System.Collections.Generic.List`1<OculusSampleFramework.InteractableCollisionInfo> OculusSampleFramework.FingerTipPokeTool::GetNextIntersectingObjects()
extern void FingerTipPokeTool_GetNextIntersectingObjects_mA962AC37DB3CBF382E81F163B450CC69A42E156B (void);
// 0x00000A1B System.Void OculusSampleFramework.FingerTipPokeTool::FocusOnInteractable(OculusSampleFramework.Interactable,OculusSampleFramework.ColliderZone)
extern void FingerTipPokeTool_FocusOnInteractable_mBA11E0AA0F742ADA5FC0859146C644DEA5D51AE7 (void);
// 0x00000A1C System.Void OculusSampleFramework.FingerTipPokeTool::DeFocus()
extern void FingerTipPokeTool_DeFocus_mEA2E9F73BCF74755786061B17A11B663BB01474C (void);
// 0x00000A1D System.Void OculusSampleFramework.FingerTipPokeTool::.ctor()
extern void FingerTipPokeTool__ctor_m0793D77F8FC85C10E5943D692BEC9AC367F68173 (void);
// 0x00000A1E System.Void OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::.ctor(System.Int32)
extern void U3CAttachTriggerLogicU3Ed__21__ctor_mCC871AFFBBB476D21423473C54CBDEBD2F14CE9D (void);
// 0x00000A1F System.Void OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::System.IDisposable.Dispose()
extern void U3CAttachTriggerLogicU3Ed__21_System_IDisposable_Dispose_mBBABBA85272B6862FE1453CBD314E245B6EA24E7 (void);
// 0x00000A20 System.Boolean OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::MoveNext()
extern void U3CAttachTriggerLogicU3Ed__21_MoveNext_m4D36E1F74B87AF1B2AE443A494AC3BD5616CD4FF (void);
// 0x00000A21 System.Object OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttachTriggerLogicU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91F1B4E86C3AF75E8070FF63A39EBFF0539499B5 (void);
// 0x00000A22 System.Void OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::System.Collections.IEnumerator.Reset()
extern void U3CAttachTriggerLogicU3Ed__21_System_Collections_IEnumerator_Reset_m550408ED48447F61859BFBD490115411547A6856 (void);
// 0x00000A23 System.Object OculusSampleFramework.FingerTipPokeTool/<AttachTriggerLogic>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CAttachTriggerLogicU3Ed__21_System_Collections_IEnumerator_get_Current_mB807F3B3CCBDAF22F0233E0A7A6CDB2DE46DC36E (void);
// 0x00000A24 OculusSampleFramework.InteractableTool OculusSampleFramework.FingerTipPokeToolView::get_InteractableTool()
extern void FingerTipPokeToolView_get_InteractableTool_mBB4D33BE156B9EE4FBFEB6D3558E3E04B8ADB51F (void);
// 0x00000A25 System.Void OculusSampleFramework.FingerTipPokeToolView::set_InteractableTool(OculusSampleFramework.InteractableTool)
extern void FingerTipPokeToolView_set_InteractableTool_mDF529210D8F673D622B4427BBDC28106C80FF9AB (void);
// 0x00000A26 System.Boolean OculusSampleFramework.FingerTipPokeToolView::get_EnableState()
extern void FingerTipPokeToolView_get_EnableState_m4C69AA39F36971BCB98909949405B1B0766A7391 (void);
// 0x00000A27 System.Void OculusSampleFramework.FingerTipPokeToolView::set_EnableState(System.Boolean)
extern void FingerTipPokeToolView_set_EnableState_m5D51EB5315EB2FBF3FE5F9B471C9B1313905820F (void);
// 0x00000A28 System.Boolean OculusSampleFramework.FingerTipPokeToolView::get_ToolActivateState()
extern void FingerTipPokeToolView_get_ToolActivateState_mE74D50A247203ABF3902B02AC10F46172A64155B (void);
// 0x00000A29 System.Void OculusSampleFramework.FingerTipPokeToolView::set_ToolActivateState(System.Boolean)
extern void FingerTipPokeToolView_set_ToolActivateState_m5D27DF2EB537D9BA0E4DBA30FA1A339E7EC45EFF (void);
// 0x00000A2A System.Single OculusSampleFramework.FingerTipPokeToolView::get_SphereRadius()
extern void FingerTipPokeToolView_get_SphereRadius_mA3C7336BF367853C422F0AFEB012A77CFB627BD8 (void);
// 0x00000A2B System.Void OculusSampleFramework.FingerTipPokeToolView::set_SphereRadius(System.Single)
extern void FingerTipPokeToolView_set_SphereRadius_mAD1E864E25672BE5A87327B53BEF716C3AF34275 (void);
// 0x00000A2C System.Void OculusSampleFramework.FingerTipPokeToolView::Awake()
extern void FingerTipPokeToolView_Awake_m5F066BEF3A88699EBF8BD848574BD8CDCFDFDC24 (void);
// 0x00000A2D System.Void OculusSampleFramework.FingerTipPokeToolView::SetFocusedInteractable(OculusSampleFramework.Interactable)
extern void FingerTipPokeToolView_SetFocusedInteractable_m5B02B5A8DECB19168DF581C171368E46F36D9409 (void);
// 0x00000A2E System.Void OculusSampleFramework.FingerTipPokeToolView::.ctor()
extern void FingerTipPokeToolView__ctor_mAF5D655B9724611A68D1A582B2473D035D308784 (void);
// 0x00000A2F System.Void OculusSampleFramework.InteractableCollisionInfo::.ctor(OculusSampleFramework.ColliderZone,OculusSampleFramework.InteractableCollisionDepth,OculusSampleFramework.InteractableTool)
extern void InteractableCollisionInfo__ctor_m7348491A5EAFD2DB5F4B9702C512F9244B6EAC91 (void);
// 0x00000A30 UnityEngine.Transform OculusSampleFramework.InteractableTool::get_ToolTransform()
extern void InteractableTool_get_ToolTransform_m205F590619ED079C0A72EA22A89EE3927842DCA7 (void);
// 0x00000A31 System.Boolean OculusSampleFramework.InteractableTool::get_IsRightHandedTool()
extern void InteractableTool_get_IsRightHandedTool_m737F795BBD30E1EC82B68A7EF69CD222FB31541A (void);
// 0x00000A32 System.Void OculusSampleFramework.InteractableTool::set_IsRightHandedTool(System.Boolean)
extern void InteractableTool_set_IsRightHandedTool_m465A3CE11D7F4A97C4D2B0FC69F90922C886D53D (void);
// 0x00000A33 OculusSampleFramework.InteractableToolTags OculusSampleFramework.InteractableTool::get_ToolTags()
// 0x00000A34 OculusSampleFramework.ToolInputState OculusSampleFramework.InteractableTool::get_ToolInputState()
// 0x00000A35 System.Boolean OculusSampleFramework.InteractableTool::get_IsFarFieldTool()
// 0x00000A36 UnityEngine.Vector3 OculusSampleFramework.InteractableTool::get_Velocity()
extern void InteractableTool_get_Velocity_mAC1D35D388D176A5F972A883516180AC66B8090F (void);
// 0x00000A37 System.Void OculusSampleFramework.InteractableTool::set_Velocity(UnityEngine.Vector3)
extern void InteractableTool_set_Velocity_m42BE2E5966AF9A78A1C157A35B123926EFB1814B (void);
// 0x00000A38 UnityEngine.Vector3 OculusSampleFramework.InteractableTool::get_InteractionPosition()
extern void InteractableTool_get_InteractionPosition_mE0FB722D4A4A52B1ACFB6D35CAFB876AA01C2111 (void);
// 0x00000A39 System.Void OculusSampleFramework.InteractableTool::set_InteractionPosition(UnityEngine.Vector3)
extern void InteractableTool_set_InteractionPosition_m7D1E65ACF3D7C8D81E733557B4B97C858B11FAE2 (void);
// 0x00000A3A System.Collections.Generic.List`1<OculusSampleFramework.InteractableCollisionInfo> OculusSampleFramework.InteractableTool::GetCurrentIntersectingObjects()
extern void InteractableTool_GetCurrentIntersectingObjects_m6F4274996E629AF558DAE685B712356444DE3147 (void);
// 0x00000A3B System.Collections.Generic.List`1<OculusSampleFramework.InteractableCollisionInfo> OculusSampleFramework.InteractableTool::GetNextIntersectingObjects()
// 0x00000A3C System.Void OculusSampleFramework.InteractableTool::FocusOnInteractable(OculusSampleFramework.Interactable,OculusSampleFramework.ColliderZone)
// 0x00000A3D System.Void OculusSampleFramework.InteractableTool::DeFocus()
// 0x00000A3E System.Boolean OculusSampleFramework.InteractableTool::get_EnableState()
// 0x00000A3F System.Void OculusSampleFramework.InteractableTool::set_EnableState(System.Boolean)
// 0x00000A40 System.Void OculusSampleFramework.InteractableTool::Initialize()
// 0x00000A41 System.Collections.Generic.KeyValuePair`2<OculusSampleFramework.Interactable,OculusSampleFramework.InteractableCollisionInfo> OculusSampleFramework.InteractableTool::GetFirstCurrentCollisionInfo()
extern void InteractableTool_GetFirstCurrentCollisionInfo_m128C0E18F5FF4B752ECAA380CEAD01A0C35D4C40 (void);
// 0x00000A42 System.Void OculusSampleFramework.InteractableTool::ClearAllCurrentCollisionInfos()
extern void InteractableTool_ClearAllCurrentCollisionInfos_mB2B670C225F4D7E9ECA1FE8F32A4591C794D3F95 (void);
// 0x00000A43 System.Void OculusSampleFramework.InteractableTool::UpdateCurrentCollisionsBasedOnDepth()
extern void InteractableTool_UpdateCurrentCollisionsBasedOnDepth_m0C7062FF22E200B4D7E8E315BC862B9D47698F9A (void);
// 0x00000A44 System.Void OculusSampleFramework.InteractableTool::UpdateLatestCollisionData()
extern void InteractableTool_UpdateLatestCollisionData_m464888128C71919DDBB2C6C7688EAF0740634250 (void);
// 0x00000A45 System.Void OculusSampleFramework.InteractableTool::.ctor()
extern void InteractableTool__ctor_m53710FF617E85E4274C1AAA0653FF167C28A3722 (void);
// 0x00000A46 OculusSampleFramework.InteractableTool OculusSampleFramework.InteractableToolView::get_InteractableTool()
// 0x00000A47 System.Void OculusSampleFramework.InteractableToolView::SetFocusedInteractable(OculusSampleFramework.Interactable)
// 0x00000A48 System.Boolean OculusSampleFramework.InteractableToolView::get_EnableState()
// 0x00000A49 System.Void OculusSampleFramework.InteractableToolView::set_EnableState(System.Boolean)
// 0x00000A4A System.Boolean OculusSampleFramework.InteractableToolView::get_ToolActivateState()
// 0x00000A4B System.Void OculusSampleFramework.InteractableToolView::set_ToolActivateState(System.Boolean)
// 0x00000A4C System.Boolean OculusSampleFramework.PinchStateModule::get_PinchUpAndDownOnFocusedObject()
extern void PinchStateModule_get_PinchUpAndDownOnFocusedObject_m6A5427560A3F4A2372BC64C89FD513CB60D6E345 (void);
// 0x00000A4D System.Boolean OculusSampleFramework.PinchStateModule::get_PinchSteadyOnFocusedObject()
extern void PinchStateModule_get_PinchSteadyOnFocusedObject_mE211E69F60B0867D44633FA5B25FFDFEDE2E5D13 (void);
// 0x00000A4E System.Boolean OculusSampleFramework.PinchStateModule::get_PinchDownOnFocusedObject()
extern void PinchStateModule_get_PinchDownOnFocusedObject_m1D2C7ED77EFB4833B1921792A3735636BA207109 (void);
// 0x00000A4F System.Void OculusSampleFramework.PinchStateModule::.ctor()
extern void PinchStateModule__ctor_m06DE10F3EADE5A00F264E444C105C23A0F4D6891 (void);
// 0x00000A50 System.Void OculusSampleFramework.PinchStateModule::UpdateState(OVRHand,OculusSampleFramework.Interactable)
extern void PinchStateModule_UpdateState_mE6EC067C8424F21E0C1553703E69E5B25E56DCBE (void);
// 0x00000A51 OculusSampleFramework.InteractableToolTags OculusSampleFramework.RayTool::get_ToolTags()
extern void RayTool_get_ToolTags_mF79256339F1A375AF576B2E2029537F43EE2C0B7 (void);
// 0x00000A52 OculusSampleFramework.ToolInputState OculusSampleFramework.RayTool::get_ToolInputState()
extern void RayTool_get_ToolInputState_m54478A71F7ED3FE7E08BE631D08037E4A6F76D89 (void);
// 0x00000A53 System.Boolean OculusSampleFramework.RayTool::get_IsFarFieldTool()
extern void RayTool_get_IsFarFieldTool_mF690CF4ED00E55B933E58EE8B46737D215F4C6A7 (void);
// 0x00000A54 System.Boolean OculusSampleFramework.RayTool::get_EnableState()
extern void RayTool_get_EnableState_mF4A05EDD10F5FE793D780CF3745D6881ED3B3D3D (void);
// 0x00000A55 System.Void OculusSampleFramework.RayTool::set_EnableState(System.Boolean)
extern void RayTool_set_EnableState_mE27327551A200506EC3FA1185A10F9392AAAC4C9 (void);
// 0x00000A56 System.Void OculusSampleFramework.RayTool::Initialize()
extern void RayTool_Initialize_m6C629CDBF7127645DCD9CABC894F1D1327FC036C (void);
// 0x00000A57 System.Void OculusSampleFramework.RayTool::OnDestroy()
extern void RayTool_OnDestroy_m8332B404479AA0DA187DF0C424576FB9262442C1 (void);
// 0x00000A58 System.Void OculusSampleFramework.RayTool::Update()
extern void RayTool_Update_mFF995DE32795EB15C757CFD5CB8201786B0F62DC (void);
// 0x00000A59 UnityEngine.Vector3 OculusSampleFramework.RayTool::GetRayCastOrigin()
extern void RayTool_GetRayCastOrigin_m4AE3575A3D65B27E3809814C3415E39FC5489B47 (void);
// 0x00000A5A System.Collections.Generic.List`1<OculusSampleFramework.InteractableCollisionInfo> OculusSampleFramework.RayTool::GetNextIntersectingObjects()
extern void RayTool_GetNextIntersectingObjects_m8BA14292395263E433DD82B9F64080C42A5A4821 (void);
// 0x00000A5B System.Boolean OculusSampleFramework.RayTool::HasRayReleasedInteractable(OculusSampleFramework.Interactable)
extern void RayTool_HasRayReleasedInteractable_m9B7A76FC8F15B453D91980F1D118EFEF432BA5CA (void);
// 0x00000A5C OculusSampleFramework.Interactable OculusSampleFramework.RayTool::FindTargetInteractable()
extern void RayTool_FindTargetInteractable_m4D0BD21D1425CD76DD0FA675ECBF7E3AA4AAB685 (void);
// 0x00000A5D OculusSampleFramework.Interactable OculusSampleFramework.RayTool::FindPrimaryRaycastHit(UnityEngine.Vector3,UnityEngine.Vector3)
extern void RayTool_FindPrimaryRaycastHit_mB6C7E211ADEC012A379E02BBC2D60B5B567CB8F8 (void);
// 0x00000A5E OculusSampleFramework.Interactable OculusSampleFramework.RayTool::FindInteractableViaConeTest(UnityEngine.Vector3,UnityEngine.Vector3)
extern void RayTool_FindInteractableViaConeTest_mEDB0AE5A0EF2B9FF895B7D08EE6B50A5DD1AD5CF (void);
// 0x00000A5F System.Void OculusSampleFramework.RayTool::FocusOnInteractable(OculusSampleFramework.Interactable,OculusSampleFramework.ColliderZone)
extern void RayTool_FocusOnInteractable_m535AB705865AB75D40842C9B5587E17732DDD2F9 (void);
// 0x00000A60 System.Void OculusSampleFramework.RayTool::DeFocus()
extern void RayTool_DeFocus_m085E103A8A047F7067906C31DACE15ACE126C6CE (void);
// 0x00000A61 System.Void OculusSampleFramework.RayTool::.ctor()
extern void RayTool__ctor_mC1D1E10E1C6B15267986E22E04C6A23966CA681B (void);
// 0x00000A62 System.Boolean OculusSampleFramework.RayToolView::get_EnableState()
extern void RayToolView_get_EnableState_m464E298E5F0DDCF469868DA3F7A07C1BC74FCDE7 (void);
// 0x00000A63 System.Void OculusSampleFramework.RayToolView::set_EnableState(System.Boolean)
extern void RayToolView_set_EnableState_m0C757E1F42C28C763F57C93F4670ED3DA3B511A5 (void);
// 0x00000A64 System.Boolean OculusSampleFramework.RayToolView::get_ToolActivateState()
extern void RayToolView_get_ToolActivateState_m5DE8AD80488ACB610C8A00467984095998DB4723 (void);
// 0x00000A65 System.Void OculusSampleFramework.RayToolView::set_ToolActivateState(System.Boolean)
extern void RayToolView_set_ToolActivateState_m382F731A347E5EF495984B48AB4EE7941FD0E99B (void);
// 0x00000A66 System.Void OculusSampleFramework.RayToolView::Awake()
extern void RayToolView_Awake_m8D4B4C38C29579601A9E21E8BE3A2E4E95246985 (void);
// 0x00000A67 OculusSampleFramework.InteractableTool OculusSampleFramework.RayToolView::get_InteractableTool()
extern void RayToolView_get_InteractableTool_m6678005A9883BB94200F353DF981AE211CFFE44A (void);
// 0x00000A68 System.Void OculusSampleFramework.RayToolView::set_InteractableTool(OculusSampleFramework.InteractableTool)
extern void RayToolView_set_InteractableTool_m0E3083D4DD470BB252858CF5D401E248705FD46B (void);
// 0x00000A69 System.Void OculusSampleFramework.RayToolView::SetFocusedInteractable(OculusSampleFramework.Interactable)
extern void RayToolView_SetFocusedInteractable_mE7F566BFFD744607512ACBBDC070D22D737A1E64 (void);
// 0x00000A6A System.Void OculusSampleFramework.RayToolView::Update()
extern void RayToolView_Update_mC0702F11E7C9A01B8E14CDD5C607E15B2B1882D7 (void);
// 0x00000A6B UnityEngine.Vector3 OculusSampleFramework.RayToolView::GetPointOnBezierCurve(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void RayToolView_GetPointOnBezierCurve_m714E7DE6A245136E03757F982C68847DBF0F55E4 (void);
// 0x00000A6C System.Void OculusSampleFramework.RayToolView::.ctor()
extern void RayToolView__ctor_m729843F49E716782D52EEB4D3A725EAE9E7F59D9 (void);
// 0x00000A6D System.Boolean OculusSampleFramework.DistanceGrabberSample::get_UseSpherecast()
extern void DistanceGrabberSample_get_UseSpherecast_m1940F73780371F34046759ED81DDBEDBFCA99FAC (void);
// 0x00000A6E System.Void OculusSampleFramework.DistanceGrabberSample::set_UseSpherecast(System.Boolean)
extern void DistanceGrabberSample_set_UseSpherecast_mE18E95F373903F263003BD244ED69D60E4F17C74 (void);
// 0x00000A6F System.Boolean OculusSampleFramework.DistanceGrabberSample::get_AllowGrabThroughWalls()
extern void DistanceGrabberSample_get_AllowGrabThroughWalls_m0E2C0AE5522B8066A49EE17D4ED8901796CEB3F2 (void);
// 0x00000A70 System.Void OculusSampleFramework.DistanceGrabberSample::set_AllowGrabThroughWalls(System.Boolean)
extern void DistanceGrabberSample_set_AllowGrabThroughWalls_m47024AC8A8622EB0582E9FBBD92AFAE9FDC70186 (void);
// 0x00000A71 System.Void OculusSampleFramework.DistanceGrabberSample::Start()
extern void DistanceGrabberSample_Start_m17A8BC2B0CD5CA1D6A19E5AFAA95B07239A6EFDC (void);
// 0x00000A72 System.Void OculusSampleFramework.DistanceGrabberSample::ToggleSphereCasting(UnityEngine.UI.Toggle)
extern void DistanceGrabberSample_ToggleSphereCasting_mA150837732BE9CA29DB37AAA3D21D0956F198488 (void);
// 0x00000A73 System.Void OculusSampleFramework.DistanceGrabberSample::ToggleGrabThroughWalls(UnityEngine.UI.Toggle)
extern void DistanceGrabberSample_ToggleGrabThroughWalls_mCEBEA385FE68A84650D3849E5B20D729FE663A74 (void);
// 0x00000A74 System.Void OculusSampleFramework.DistanceGrabberSample::.ctor()
extern void DistanceGrabberSample__ctor_m91EB15189BB6C76FE3D70960D66F80C3F474CE39 (void);
// 0x00000A75 System.Void OculusSampleFramework.ControllerBoxController::Awake()
extern void ControllerBoxController_Awake_m10CE6D278450B3090C9F6FEBE087CA23CC9FFD8F (void);
// 0x00000A76 System.Void OculusSampleFramework.ControllerBoxController::StartStopStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_StartStopStateChanged_mD500AA96C8E03F97F611D5EA54F954C933753451 (void);
// 0x00000A77 System.Void OculusSampleFramework.ControllerBoxController::DecreaseSpeedStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_DecreaseSpeedStateChanged_m3229EACD0C33045DC2CB25A0BF6120F0838F0EE0 (void);
// 0x00000A78 System.Void OculusSampleFramework.ControllerBoxController::IncreaseSpeedStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_IncreaseSpeedStateChanged_mEFE12568E8F140F770F50AF38395E0DB22C9788B (void);
// 0x00000A79 System.Void OculusSampleFramework.ControllerBoxController::SmokeButtonStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_SmokeButtonStateChanged_m886E38B2844CF61820F76E9724FDCCA2C61CA372 (void);
// 0x00000A7A System.Void OculusSampleFramework.ControllerBoxController::WhistleButtonStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_WhistleButtonStateChanged_m28F2550607F6321A43FFB7891295EBBCCC90179A (void);
// 0x00000A7B System.Void OculusSampleFramework.ControllerBoxController::ReverseButtonStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_ReverseButtonStateChanged_m8183F155BD2B5E5F76680E51DA08B88B315AE0FA (void);
// 0x00000A7C System.Void OculusSampleFramework.ControllerBoxController::SwitchVisualization(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_SwitchVisualization_m5D7DB78DF80500F6C712B626B834CEC6BD2D59D0 (void);
// 0x00000A7D System.Void OculusSampleFramework.ControllerBoxController::GoMoo(OculusSampleFramework.InteractableStateArgs)
extern void ControllerBoxController_GoMoo_mFBEDA2C31525584CD95F0C59E73B5A10E9FCD509 (void);
// 0x00000A7E System.Void OculusSampleFramework.ControllerBoxController::.ctor()
extern void ControllerBoxController__ctor_mE2B5C31F1DA7DC4A076BB4715E72787A1FB3705C (void);
// 0x00000A7F System.Void OculusSampleFramework.CowController::Start()
extern void CowController_Start_mC4B6534879CC6708AFC5D442E5282DFE915173D7 (void);
// 0x00000A80 System.Void OculusSampleFramework.CowController::PlayMooSound()
extern void CowController_PlayMooSound_m6EC41C83F14928C33AAB8C96D1DB6F10E70B5104 (void);
// 0x00000A81 System.Void OculusSampleFramework.CowController::GoMooCowGo()
extern void CowController_GoMooCowGo_m0F2CD8AAFA84A5E77222026981CE7BE734090D96 (void);
// 0x00000A82 System.Void OculusSampleFramework.CowController::.ctor()
extern void CowController__ctor_m4EC07EAB37300BBD79EDBAA4073CB736455CBF28 (void);
// 0x00000A83 System.Void OculusSampleFramework.PanelHMDFollower::Awake()
extern void PanelHMDFollower_Awake_m2CF549A1B7AD1994E135D0C3A64B3AAE4BE1C91C (void);
// 0x00000A84 System.Void OculusSampleFramework.PanelHMDFollower::Update()
extern void PanelHMDFollower_Update_m78849650838D5B71CC1A8AF283E9A26DF5E78E17 (void);
// 0x00000A85 UnityEngine.Vector3 OculusSampleFramework.PanelHMDFollower::CalculateIdealAnchorPosition()
extern void PanelHMDFollower_CalculateIdealAnchorPosition_m74F280D8BC9291C194EB4FC9817A4150ABC944E2 (void);
// 0x00000A86 System.Collections.IEnumerator OculusSampleFramework.PanelHMDFollower::LerpToHMD()
extern void PanelHMDFollower_LerpToHMD_mD8B9F448F88D9F598368D4B930DFA7BC19A843BB (void);
// 0x00000A87 System.Void OculusSampleFramework.PanelHMDFollower::.ctor()
extern void PanelHMDFollower__ctor_mDFD1C555C729E0009E9FC989048FA5806267EC72 (void);
// 0x00000A88 System.Void OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::.ctor(System.Int32)
extern void U3CLerpToHMDU3Ed__13__ctor_m2718D92561980DA3B2F04C2D102AB928155D0AAA (void);
// 0x00000A89 System.Void OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::System.IDisposable.Dispose()
extern void U3CLerpToHMDU3Ed__13_System_IDisposable_Dispose_m6F79FD9B76A0633FBC96538B385DB07AF57C90DC (void);
// 0x00000A8A System.Boolean OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::MoveNext()
extern void U3CLerpToHMDU3Ed__13_MoveNext_m9C0F7AA018E489420080E85A9C327248E2301DC4 (void);
// 0x00000A8B System.Object OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLerpToHMDU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD58D03CC073A72DDD7B0EC4188CB4614E92A68AF (void);
// 0x00000A8C System.Void OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::System.Collections.IEnumerator.Reset()
extern void U3CLerpToHMDU3Ed__13_System_Collections_IEnumerator_Reset_mA09722794732F819AA5D31CB2AF5BCB0C2FD817A (void);
// 0x00000A8D System.Object OculusSampleFramework.PanelHMDFollower/<LerpToHMD>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CLerpToHMDU3Ed__13_System_Collections_IEnumerator_get_Current_mF39A3910A6D633D8600FF5B154BC7B6764772E9C (void);
// 0x00000A8E OculusSampleFramework.SelectionCylinder/SelectionState OculusSampleFramework.SelectionCylinder::get_CurrSelectionState()
extern void SelectionCylinder_get_CurrSelectionState_mEEA0EACE12CE6A5F1BDAA7E59A7EC5D77D56FAEA (void);
// 0x00000A8F System.Void OculusSampleFramework.SelectionCylinder::set_CurrSelectionState(OculusSampleFramework.SelectionCylinder/SelectionState)
extern void SelectionCylinder_set_CurrSelectionState_m960B75182FF50885FD248F74A05F7D33BA3D67D3 (void);
// 0x00000A90 System.Void OculusSampleFramework.SelectionCylinder::Awake()
extern void SelectionCylinder_Awake_m3C9863696F1825751F817545A99016F0840CC0EF (void);
// 0x00000A91 System.Void OculusSampleFramework.SelectionCylinder::OnDestroy()
extern void SelectionCylinder_OnDestroy_mB8F6CF93EB637C30627D4851B5F0290D0D730A49 (void);
// 0x00000A92 System.Void OculusSampleFramework.SelectionCylinder::AffectSelectionColor(UnityEngine.Color[])
extern void SelectionCylinder_AffectSelectionColor_m98DE925B474211ACCCCB6085DDB3A90399E4DCA3 (void);
// 0x00000A93 System.Void OculusSampleFramework.SelectionCylinder::.ctor()
extern void SelectionCylinder__ctor_m19D204E84FD5989ECFE8FBF1480C7FCF758CFE92 (void);
// 0x00000A94 System.Void OculusSampleFramework.SelectionCylinder::.cctor()
extern void SelectionCylinder__cctor_m3809D35D3304F30A86CCECB1D957BE393A8685F5 (void);
// 0x00000A95 System.Single OculusSampleFramework.TrackSegment::get_StartDistance()
extern void TrackSegment_get_StartDistance_m7754F5C2A4BE8A74DB09CE3CED8D2598630B0D7B (void);
// 0x00000A96 System.Void OculusSampleFramework.TrackSegment::set_StartDistance(System.Single)
extern void TrackSegment_set_StartDistance_mCAF42BD5254AFCFFE3001A6E20070939A6F20195 (void);
// 0x00000A97 System.Single OculusSampleFramework.TrackSegment::get_GridSize()
extern void TrackSegment_get_GridSize_m3D99EF429B98F214239CA9CA54A076BBB7FE92E3 (void);
// 0x00000A98 System.Void OculusSampleFramework.TrackSegment::set_GridSize(System.Single)
extern void TrackSegment_set_GridSize_m9823CF02AE0029655CBEAFF47200B88A76BD2E85 (void);
// 0x00000A99 System.Int32 OculusSampleFramework.TrackSegment::get_SubDivCount()
extern void TrackSegment_get_SubDivCount_mE962363663526F01DC7D77F360A5E97A5E8D333E (void);
// 0x00000A9A System.Void OculusSampleFramework.TrackSegment::set_SubDivCount(System.Int32)
extern void TrackSegment_set_SubDivCount_m208543971918F3C42B26627EB3BFC845A034B19A (void);
// 0x00000A9B OculusSampleFramework.TrackSegment/SegmentType OculusSampleFramework.TrackSegment::get_Type()
extern void TrackSegment_get_Type_mB16F5DB9C4FD3AF8FA91CF5880D5DD48AA8681FF (void);
// 0x00000A9C OculusSampleFramework.Pose OculusSampleFramework.TrackSegment::get_EndPose()
extern void TrackSegment_get_EndPose_m0F2F98AAF1A5C6C067720CA8067C3FD47D43876A (void);
// 0x00000A9D System.Single OculusSampleFramework.TrackSegment::get_Radius()
extern void TrackSegment_get_Radius_mB458D02292A84615CA9BFEA5F03C1059268D6BE6 (void);
// 0x00000A9E System.Single OculusSampleFramework.TrackSegment::setGridSize(System.Single)
extern void TrackSegment_setGridSize_mC74C3F3285B5ABD033CF68BDB0770DE56F16E990 (void);
// 0x00000A9F System.Single OculusSampleFramework.TrackSegment::get_SegmentLength()
extern void TrackSegment_get_SegmentLength_m63FFFA4AD5055C552C805872637BC6839AC55F39 (void);
// 0x00000AA0 System.Void OculusSampleFramework.TrackSegment::Awake()
extern void TrackSegment_Awake_m9650E6E117E2253990A03F82515ED14E990DDA97 (void);
// 0x00000AA1 System.Void OculusSampleFramework.TrackSegment::UpdatePose(System.Single,OculusSampleFramework.Pose)
extern void TrackSegment_UpdatePose_m97FBB83914AB8D3CB49D9D85467DEB7431248B53 (void);
// 0x00000AA2 System.Void OculusSampleFramework.TrackSegment::Update()
extern void TrackSegment_Update_m108D239B881A8A7A1729E74C2EF814E2376D5DB5 (void);
// 0x00000AA3 System.Void OculusSampleFramework.TrackSegment::OnDisable()
extern void TrackSegment_OnDisable_mB9B6EB483265C1B1BCF980EA6F58ED15D06ABB0A (void);
// 0x00000AA4 System.Void OculusSampleFramework.TrackSegment::DrawDebugLines()
extern void TrackSegment_DrawDebugLines_m37FEF6E5D1842508645C8F8FEB28BABDA03D7253 (void);
// 0x00000AA5 System.Void OculusSampleFramework.TrackSegment::RegenerateTrackAndMesh()
extern void TrackSegment_RegenerateTrackAndMesh_m4841B12BDC519C7862B4901565B44A9B6787AD5C (void);
// 0x00000AA6 System.Void OculusSampleFramework.TrackSegment::.ctor()
extern void TrackSegment__ctor_mC6FA53B0963D2B395BCE338C1EC39985B28C4F96 (void);
// 0x00000AA7 System.Void OculusSampleFramework.TrainButtonVisualController::Awake()
extern void TrainButtonVisualController_Awake_m84F6745B2D689E89F2AAF1ECA67B2FC59FEC4F14 (void);
// 0x00000AA8 System.Void OculusSampleFramework.TrainButtonVisualController::OnDestroy()
extern void TrainButtonVisualController_OnDestroy_m297A762E98F73EF8414F095856CE387C438D1798 (void);
// 0x00000AA9 System.Void OculusSampleFramework.TrainButtonVisualController::OnEnable()
extern void TrainButtonVisualController_OnEnable_m0564F0AD8EDBCF9FAFE21A3D15EB7CCC7C65DE97 (void);
// 0x00000AAA System.Void OculusSampleFramework.TrainButtonVisualController::OnDisable()
extern void TrainButtonVisualController_OnDisable_m24A294D4F2E5C301D05DE52F29EDA4400EC831C5 (void);
// 0x00000AAB System.Void OculusSampleFramework.TrainButtonVisualController::ActionOrInContactZoneStayEvent(OculusSampleFramework.ColliderZoneArgs)
extern void TrainButtonVisualController_ActionOrInContactZoneStayEvent_m7AF8B0FD00D7F603EC903094DC343CC42BF7ABD0 (void);
// 0x00000AAC System.Void OculusSampleFramework.TrainButtonVisualController::InteractableStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void TrainButtonVisualController_InteractableStateChanged_mCF09476B0142274EAFE2C5ADE762C28633BD23EF (void);
// 0x00000AAD System.Void OculusSampleFramework.TrainButtonVisualController::PlaySound(UnityEngine.AudioClip)
extern void TrainButtonVisualController_PlaySound_m65FC0A9207B130533020BEA24C3F291415B02249 (void);
// 0x00000AAE System.Void OculusSampleFramework.TrainButtonVisualController::StopResetLerping()
extern void TrainButtonVisualController_StopResetLerping_m9ADCE7C308758261CC14032402BEED4F21EE4B4B (void);
// 0x00000AAF System.Void OculusSampleFramework.TrainButtonVisualController::LerpToOldPosition()
extern void TrainButtonVisualController_LerpToOldPosition_m95E0582BA249685039EEA46A3AD1BDD57CCE3B68 (void);
// 0x00000AB0 System.Collections.IEnumerator OculusSampleFramework.TrainButtonVisualController::ResetPosition()
extern void TrainButtonVisualController_ResetPosition_m9232F6767751D6404FFFCB0944D9FE7926F02B0B (void);
// 0x00000AB1 System.Void OculusSampleFramework.TrainButtonVisualController::.ctor()
extern void TrainButtonVisualController__ctor_m67AA3D848961906B2E5F20150B0C82462B7E190F (void);
// 0x00000AB2 System.Void OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::.ctor(System.Int32)
extern void U3CResetPositionU3Ed__26__ctor_m5635000743474AB03A282A54DAA034BE871944D8 (void);
// 0x00000AB3 System.Void OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.IDisposable.Dispose()
extern void U3CResetPositionU3Ed__26_System_IDisposable_Dispose_mFD4D0FB62BBFDC93E0DEAD7EA68430B7A4ACAF69 (void);
// 0x00000AB4 System.Boolean OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::MoveNext()
extern void U3CResetPositionU3Ed__26_MoveNext_m5E72132EBEF7DB0236E6E8EBD6632A16E0F748E0 (void);
// 0x00000AB5 System.Object OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetPositionU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BE3FF62F4373A00DD3E8BBCBB2036F6761E547B (void);
// 0x00000AB6 System.Void OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.Collections.IEnumerator.Reset()
extern void U3CResetPositionU3Ed__26_System_Collections_IEnumerator_Reset_mFAB28539B7B0AF0B783CD738C41D9DC51A1E8853 (void);
// 0x00000AB7 System.Object OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CResetPositionU3Ed__26_System_Collections_IEnumerator_get_Current_m84A8CF3B9E09A2B224C5C4969FBB701BB9E8FF7B (void);
// 0x00000AB8 System.Single OculusSampleFramework.TrainCar::get_DistanceBehindParentScaled()
extern void TrainCar_get_DistanceBehindParentScaled_m373B41D22495598A3431F0C5FC5017D442208A2F (void);
// 0x00000AB9 System.Void OculusSampleFramework.TrainCar::Awake()
extern void TrainCar_Awake_m05D4446FBB4ADAF94AECC0466BBF94055909C8F0 (void);
// 0x00000ABA System.Void OculusSampleFramework.TrainCar::UpdatePosition()
extern void TrainCar_UpdatePosition_m338A73EAC14EC679C9F5E00DEED605CEA2BDE432 (void);
// 0x00000ABB System.Void OculusSampleFramework.TrainCar::.ctor()
extern void TrainCar__ctor_m086A9601B0ADD3B4FD4118437F6DED5539A25486 (void);
// 0x00000ABC System.Single OculusSampleFramework.TrainCarBase::get_Distance()
extern void TrainCarBase_get_Distance_mA33D6D4848DFD0820D2C75F9E1F2A76DC755E8A6 (void);
// 0x00000ABD System.Void OculusSampleFramework.TrainCarBase::set_Distance(System.Single)
extern void TrainCarBase_set_Distance_m4C3DF32DF27C6B14AB536FA6A983A8AD056B55CB (void);
// 0x00000ABE System.Single OculusSampleFramework.TrainCarBase::get_Scale()
extern void TrainCarBase_get_Scale_m5DBF114FDC5DBE97AD3C5BC2712624BBADF3F5D5 (void);
// 0x00000ABF System.Void OculusSampleFramework.TrainCarBase::set_Scale(System.Single)
extern void TrainCarBase_set_Scale_mAF09262F53F500D9ED7DA7829847172C5410ED61 (void);
// 0x00000AC0 System.Void OculusSampleFramework.TrainCarBase::Awake()
extern void TrainCarBase_Awake_m37A702A4A3B24463E19C126F64F2D89F11A9A502 (void);
// 0x00000AC1 System.Void OculusSampleFramework.TrainCarBase::UpdatePose(System.Single,OculusSampleFramework.TrainCarBase,OculusSampleFramework.Pose)
extern void TrainCarBase_UpdatePose_m0A67C10B69FB1D9A986E883B1CDC382B571085BE (void);
// 0x00000AC2 System.Void OculusSampleFramework.TrainCarBase::UpdateCarPosition()
extern void TrainCarBase_UpdateCarPosition_m2B2E8799969C239937C47D4615D2101E6F9133A1 (void);
// 0x00000AC3 System.Void OculusSampleFramework.TrainCarBase::RotateCarWheels()
extern void TrainCarBase_RotateCarWheels_m965650C8E46E761835CC060092CB6F4B69252858 (void);
// 0x00000AC4 System.Void OculusSampleFramework.TrainCarBase::UpdatePosition()
// 0x00000AC5 System.Void OculusSampleFramework.TrainCarBase::.ctor()
extern void TrainCarBase__ctor_mF505ADDC2083B90F7A3F45C89C32025DD0002ED8 (void);
// 0x00000AC6 System.Void OculusSampleFramework.TrainCarBase::.cctor()
extern void TrainCarBase__cctor_m9F34E6EE6F8D87A0B97D210D6550DE0CC0755706 (void);
// 0x00000AC7 System.Void OculusSampleFramework.TrainCrossingController::Awake()
extern void TrainCrossingController_Awake_mC8448F974906D7A72F4FB52C18CC6B00887E120A (void);
// 0x00000AC8 System.Void OculusSampleFramework.TrainCrossingController::OnDestroy()
extern void TrainCrossingController_OnDestroy_m5913750A3AF404FB4E8FC54EA209A020A231A5D4 (void);
// 0x00000AC9 System.Void OculusSampleFramework.TrainCrossingController::CrossingButtonStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void TrainCrossingController_CrossingButtonStateChanged_mE31580E5F7428418ECBE6FF15AB6BE20D73D998D (void);
// 0x00000ACA System.Void OculusSampleFramework.TrainCrossingController::Update()
extern void TrainCrossingController_Update_m580817DC6CF5C8E2EB48A214F1F84EE51EFBB736 (void);
// 0x00000ACB System.Void OculusSampleFramework.TrainCrossingController::ActivateTrainCrossing()
extern void TrainCrossingController_ActivateTrainCrossing_m85B47BC1A24078D2E973B527337EEB61794E7849 (void);
// 0x00000ACC System.Collections.IEnumerator OculusSampleFramework.TrainCrossingController::AnimateCrossing(System.Single)
extern void TrainCrossingController_AnimateCrossing_m8AE7CFD19C0FF9120FA3040DFD02538E8A297C65 (void);
// 0x00000ACD System.Void OculusSampleFramework.TrainCrossingController::AffectMaterials(UnityEngine.Material[],UnityEngine.Color)
extern void TrainCrossingController_AffectMaterials_m6401A1A3E5B6E85E11681CF60FA492BB4BA510FA (void);
// 0x00000ACE System.Void OculusSampleFramework.TrainCrossingController::ToggleLightObjects(System.Boolean)
extern void TrainCrossingController_ToggleLightObjects_m4B93FC82AC5CA62BC1F0755839F5AE2D3983C38C (void);
// 0x00000ACF System.Void OculusSampleFramework.TrainCrossingController::.ctor()
extern void TrainCrossingController__ctor_m0EA7B850A7E56F174B1E49FF0513392222383826 (void);
// 0x00000AD0 System.Void OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::.ctor(System.Int32)
extern void U3CAnimateCrossingU3Ed__15__ctor_mFAA4EB62DD8CF2E800A17FB70610D210D4572B2C (void);
// 0x00000AD1 System.Void OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.IDisposable.Dispose()
extern void U3CAnimateCrossingU3Ed__15_System_IDisposable_Dispose_m15B4C628F2391E4765AAD7E5FBEB1ED37163CF6C (void);
// 0x00000AD2 System.Boolean OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::MoveNext()
extern void U3CAnimateCrossingU3Ed__15_MoveNext_m9E22324ADCB019B88C3415E7B1087C2E5E277572 (void);
// 0x00000AD3 System.Object OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateCrossingU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E5821A766805B7DA85AF5CDF5B3E79C9007A97D (void);
// 0x00000AD4 System.Void OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.Collections.IEnumerator.Reset()
extern void U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_Reset_m1221B7705FF6866AB85D415E8D00A92F1B40B0C3 (void);
// 0x00000AD5 System.Object OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_get_Current_mDF8E56DBC547B33C024A5EB10750F3D8B075A412 (void);
// 0x00000AD6 System.Void OculusSampleFramework.TrainLocomotive::Start()
extern void TrainLocomotive_Start_m596B010A45C501F05A1BE9CE412138AB82498900 (void);
// 0x00000AD7 System.Void OculusSampleFramework.TrainLocomotive::Update()
extern void TrainLocomotive_Update_mC64296C895306B462D399E315CCFE6106979396A (void);
// 0x00000AD8 System.Void OculusSampleFramework.TrainLocomotive::UpdatePosition()
extern void TrainLocomotive_UpdatePosition_mF409B3053EC449F8F57AB7B971A9053C642C0D1A (void);
// 0x00000AD9 System.Void OculusSampleFramework.TrainLocomotive::StartStopStateChanged()
extern void TrainLocomotive_StartStopStateChanged_m37D2604A94869E9ABAE686C05C06DA8039D5EAFC (void);
// 0x00000ADA System.Collections.IEnumerator OculusSampleFramework.TrainLocomotive::StartStopTrain(System.Boolean)
extern void TrainLocomotive_StartStopTrain_m2983A1995E2F95A8642F03A8105C0B489F9C8037 (void);
// 0x00000ADB System.Single OculusSampleFramework.TrainLocomotive::PlayEngineSound(OculusSampleFramework.TrainLocomotive/EngineSoundState)
extern void TrainLocomotive_PlayEngineSound_mBBE7F6F9988F707FFC60EE195E18C7C7C37C05CE (void);
// 0x00000ADC System.Void OculusSampleFramework.TrainLocomotive::UpdateDistance()
extern void TrainLocomotive_UpdateDistance_m18B8F2E7593CBA725B2A7B5034AF46279D35A321 (void);
// 0x00000ADD System.Void OculusSampleFramework.TrainLocomotive::DecreaseSpeedStateChanged()
extern void TrainLocomotive_DecreaseSpeedStateChanged_m58EE2750F1CC037AB7C44A209C5D7A3D6FDCB9A1 (void);
// 0x00000ADE System.Void OculusSampleFramework.TrainLocomotive::IncreaseSpeedStateChanged()
extern void TrainLocomotive_IncreaseSpeedStateChanged_m4DEB392A7FA443870D2A849A663EE6E3E740A22B (void);
// 0x00000ADF System.Void OculusSampleFramework.TrainLocomotive::UpdateSmokeEmissionBasedOnSpeed()
extern void TrainLocomotive_UpdateSmokeEmissionBasedOnSpeed_mABC70306F1FBF3D00E0BA0901760D44793F9D3EF (void);
// 0x00000AE0 System.Single OculusSampleFramework.TrainLocomotive::GetCurrentSmokeEmission()
extern void TrainLocomotive_GetCurrentSmokeEmission_mCBF588B02E4B7CB77E4A4EEE98FA557C6C09A267 (void);
// 0x00000AE1 System.Void OculusSampleFramework.TrainLocomotive::SmokeButtonStateChanged()
extern void TrainLocomotive_SmokeButtonStateChanged_m2E6E404156BF780272ED69C4A2D7D07FF94E01EC (void);
// 0x00000AE2 System.Void OculusSampleFramework.TrainLocomotive::WhistleButtonStateChanged()
extern void TrainLocomotive_WhistleButtonStateChanged_m2B8FF22070F53567B9DB0EF8F021411531CCF240 (void);
// 0x00000AE3 System.Void OculusSampleFramework.TrainLocomotive::ReverseButtonStateChanged()
extern void TrainLocomotive_ReverseButtonStateChanged_mDC7C10B6A595AABCF8351143917C1CDCFEA74A98 (void);
// 0x00000AE4 System.Void OculusSampleFramework.TrainLocomotive::.ctor()
extern void TrainLocomotive__ctor_mF249EAA95B7C51F5D9493892D960C0E8460AF909 (void);
// 0x00000AE5 System.Void OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::.ctor(System.Int32)
extern void U3CStartStopTrainU3Ed__34__ctor_m39BEC907A4658AB1DC65D214A86D35F6E246795A (void);
// 0x00000AE6 System.Void OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.IDisposable.Dispose()
extern void U3CStartStopTrainU3Ed__34_System_IDisposable_Dispose_m6A0A898733F46F849ACEF0984B4C8938E5A88B20 (void);
// 0x00000AE7 System.Boolean OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::MoveNext()
extern void U3CStartStopTrainU3Ed__34_MoveNext_m02D96704D3CA82F8BECFC12A487D9C4AC7903FA1 (void);
// 0x00000AE8 System.Object OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartStopTrainU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26D7E65A695D870236E43D9C2E2D46D88EA33065 (void);
// 0x00000AE9 System.Void OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.Collections.IEnumerator.Reset()
extern void U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_Reset_mE5B9500E4605106C99B8718D3105A15B0CB73A87 (void);
// 0x00000AEA System.Object OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_get_Current_mEF6808D31C6E8D74E845C551B8618F3902FC469E (void);
// 0x00000AEB System.Single OculusSampleFramework.TrainTrack::get_TrackLength()
extern void TrainTrack_get_TrackLength_m2527E507114E21D65C843667ECF4F42CBA3F7FCA (void);
// 0x00000AEC System.Void OculusSampleFramework.TrainTrack::set_TrackLength(System.Single)
extern void TrainTrack_set_TrackLength_m32ED334511B32330D895514C07B3BED3381F02E5 (void);
// 0x00000AED System.Void OculusSampleFramework.TrainTrack::Awake()
extern void TrainTrack_Awake_mF48A851C3560D8D9ADA00466789E9571E49047B4 (void);
// 0x00000AEE OculusSampleFramework.TrackSegment OculusSampleFramework.TrainTrack::GetSegment(System.Single)
extern void TrainTrack_GetSegment_m357BC8C004D29CAD4A3CA54E7896EB1A9C7F6B4D (void);
// 0x00000AEF System.Void OculusSampleFramework.TrainTrack::Regenerate()
extern void TrainTrack_Regenerate_m69F9C3D51A88D5FC844056750E4B233C06B427D6 (void);
// 0x00000AF0 System.Void OculusSampleFramework.TrainTrack::SetScale(System.Single)
extern void TrainTrack_SetScale_mA57F06F4C3421BB4EB73D3311A661210F8FE7416 (void);
// 0x00000AF1 System.Void OculusSampleFramework.TrainTrack::.ctor()
extern void TrainTrack__ctor_mB9A2233319F4A20CD3719E44B1A65AF41FA2CB38 (void);
// 0x00000AF2 System.Void OculusSampleFramework.Pose::.ctor()
extern void Pose__ctor_mB4BBD3D8EA8C633539F804F43FB086FB99C168E2 (void);
// 0x00000AF3 System.Void OculusSampleFramework.Pose::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Pose__ctor_m8C52CA7D6BF59F3F82C09B76750DE7AF776F06C5 (void);
// 0x00000AF4 System.Boolean OculusSampleFramework.WindmillBladesController::get_IsMoving()
extern void WindmillBladesController_get_IsMoving_m5B2902A40908BBD84B5478EDA121ED00DBDECF56 (void);
// 0x00000AF5 System.Void OculusSampleFramework.WindmillBladesController::set_IsMoving(System.Boolean)
extern void WindmillBladesController_set_IsMoving_mAFFB89C8CAA4C34CA91560EE2188E80B3B66897A (void);
// 0x00000AF6 System.Void OculusSampleFramework.WindmillBladesController::Start()
extern void WindmillBladesController_Start_m68D68EA86272066DDC385AF8B8FDC2307AE6D4CA (void);
// 0x00000AF7 System.Void OculusSampleFramework.WindmillBladesController::Update()
extern void WindmillBladesController_Update_m6D72C558EA8D91B66F86A4B977F32B78C6A19C9A (void);
// 0x00000AF8 System.Void OculusSampleFramework.WindmillBladesController::SetMoveState(System.Boolean,System.Single)
extern void WindmillBladesController_SetMoveState_m7E9FF42447DFDD4DC710B07309C4ECB6A9027B3F (void);
// 0x00000AF9 System.Collections.IEnumerator OculusSampleFramework.WindmillBladesController::LerpToSpeed(System.Single)
extern void WindmillBladesController_LerpToSpeed_mEC59BAB1ADEBDA2D4051ADE4DF879F255B80C50A (void);
// 0x00000AFA System.Collections.IEnumerator OculusSampleFramework.WindmillBladesController::PlaySoundDelayed(UnityEngine.AudioClip,UnityEngine.AudioClip,System.Single)
extern void WindmillBladesController_PlaySoundDelayed_m5C9465C96F6157B1A20D4F7F5A7D4F307D10D0FA (void);
// 0x00000AFB System.Void OculusSampleFramework.WindmillBladesController::PlaySound(UnityEngine.AudioClip,System.Boolean)
extern void WindmillBladesController_PlaySound_mBFB8F38297A3551225D2E57F4285B58E9EF1EA63 (void);
// 0x00000AFC System.Void OculusSampleFramework.WindmillBladesController::.ctor()
extern void WindmillBladesController__ctor_m9ACCD7787AFD75BF05A2D1282EAC87C3E8EFAF50 (void);
// 0x00000AFD System.Void OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::.ctor(System.Int32)
extern void U3CLerpToSpeedU3Ed__17__ctor_mC566935DE291006EA7DBEFE0365E202472FD9E7B (void);
// 0x00000AFE System.Void OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.IDisposable.Dispose()
extern void U3CLerpToSpeedU3Ed__17_System_IDisposable_Dispose_m47D668CCF11A5E20E41EE17981808A194A891934 (void);
// 0x00000AFF System.Boolean OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::MoveNext()
extern void U3CLerpToSpeedU3Ed__17_MoveNext_mEA106649270155724FF7B4F3557200EB0C46C649 (void);
// 0x00000B00 System.Object OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLerpToSpeedU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0BCE854267BF1FBEBD932AD293ECE426245E39C3 (void);
// 0x00000B01 System.Void OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.Collections.IEnumerator.Reset()
extern void U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_Reset_mDB45CD8EC190F294BD562D4090ADB533E5EBF9F7 (void);
// 0x00000B02 System.Object OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_get_Current_m81542027D8F2618D5B750EF7344E51128D3A6EA3 (void);
// 0x00000B03 System.Void OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::.ctor(System.Int32)
extern void U3CPlaySoundDelayedU3Ed__18__ctor_m43BFCD0BBEAFA46AF3EC55B65A2F55BF43541166 (void);
// 0x00000B04 System.Void OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.IDisposable.Dispose()
extern void U3CPlaySoundDelayedU3Ed__18_System_IDisposable_Dispose_m3978D8C006709205406E26F305AAD051EDA0CB61 (void);
// 0x00000B05 System.Boolean OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::MoveNext()
extern void U3CPlaySoundDelayedU3Ed__18_MoveNext_m5CE5E650B1870D226841E3D4EA3A269EC73C7493 (void);
// 0x00000B06 System.Object OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlaySoundDelayedU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DE8A8951E14A7AB0C941F512740278C4DF7841A (void);
// 0x00000B07 System.Void OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.Collections.IEnumerator.Reset()
extern void U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_Reset_mD0C04D5F54AAD444CE77C15B853BE79F5504160F (void);
// 0x00000B08 System.Object OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_get_Current_mC1F5FCA59AD560DFD85357BAC5AD0581AE393182 (void);
// 0x00000B09 System.Void OculusSampleFramework.WindmillController::Awake()
extern void WindmillController_Awake_m819B04397F7D5A549DB64C972D9B35A0B54DCA77 (void);
// 0x00000B0A System.Void OculusSampleFramework.WindmillController::OnEnable()
extern void WindmillController_OnEnable_mD0EBB3C707F79AB7CFB88EE7CE33355A24311BD0 (void);
// 0x00000B0B System.Void OculusSampleFramework.WindmillController::OnDisable()
extern void WindmillController_OnDisable_mDAF50F89AB116A19F75AD91C088CF0B58F441365 (void);
// 0x00000B0C System.Void OculusSampleFramework.WindmillController::StartStopStateChanged(OculusSampleFramework.InteractableStateArgs)
extern void WindmillController_StartStopStateChanged_m662296A93D8F31FC2608D8C3E89D1E3714B9561F (void);
// 0x00000B0D System.Void OculusSampleFramework.WindmillController::Update()
extern void WindmillController_Update_mA8A7025A970E66000E92C858A97B07D1A73650C2 (void);
// 0x00000B0E System.Void OculusSampleFramework.WindmillController::.ctor()
extern void WindmillController__ctor_m2AE15DFF314EC3FE467821FB5F2EB1EE0F19EABC (void);
// 0x00000B0F System.Void OculusSampleFramework.OVROverlaySample::Start()
extern void OVROverlaySample_Start_m4266C95062A3A7F9DB99739F13F1767E8E135CDC (void);
// 0x00000B10 System.Void OculusSampleFramework.OVROverlaySample::Update()
extern void OVROverlaySample_Update_mE6B42EE1FD90945F73BB8A1E71C98D6FE45F8BAB (void);
// 0x00000B11 System.Void OculusSampleFramework.OVROverlaySample::ActivateWorldGeo()
extern void OVROverlaySample_ActivateWorldGeo_mE88CC5B6F95FF940DF64CC3E30A163299A0028AB (void);
// 0x00000B12 System.Void OculusSampleFramework.OVROverlaySample::ActivateOVROverlay()
extern void OVROverlaySample_ActivateOVROverlay_m0D8B1F2CE350FC1D6530827588176FED1E8B0E52 (void);
// 0x00000B13 System.Void OculusSampleFramework.OVROverlaySample::ActivateNone()
extern void OVROverlaySample_ActivateNone_mA8A0AC683DA78AACC6DBB04E1BFCCAA41CC83D66 (void);
// 0x00000B14 System.Void OculusSampleFramework.OVROverlaySample::TriggerLoad()
extern void OVROverlaySample_TriggerLoad_mA4868A61FDAFE908F1178A852FAAC4DA4A614B42 (void);
// 0x00000B15 System.Collections.IEnumerator OculusSampleFramework.OVROverlaySample::WaitforOVROverlay()
extern void OVROverlaySample_WaitforOVROverlay_mE2A11AA7618CAF92461A97CFFF9BF0163B14D463 (void);
// 0x00000B16 System.Void OculusSampleFramework.OVROverlaySample::TriggerUnload()
extern void OVROverlaySample_TriggerUnload_m6B1496A0AF1064FF3E131ED27D4BE41916FEAACA (void);
// 0x00000B17 System.Void OculusSampleFramework.OVROverlaySample::CameraAndRenderTargetSetup()
extern void OVROverlaySample_CameraAndRenderTargetSetup_mF5D8804C1E12AE7E92207ACE4AE38C9C4B12005B (void);
// 0x00000B18 System.Void OculusSampleFramework.OVROverlaySample::SimulateLevelLoad()
extern void OVROverlaySample_SimulateLevelLoad_m5792B7A4F133CB3538E9A9CF021C1EED354E47C4 (void);
// 0x00000B19 System.Void OculusSampleFramework.OVROverlaySample::ClearObjects()
extern void OVROverlaySample_ClearObjects_m0B71842496007E962955E65310B2A4B15653E446 (void);
// 0x00000B1A System.Void OculusSampleFramework.OVROverlaySample::RadioPressed(System.String,System.String,UnityEngine.UI.Toggle)
extern void OVROverlaySample_RadioPressed_m67B72653B224FFE4E86C34D625F9B442D32BD19F (void);
// 0x00000B1B System.Void OculusSampleFramework.OVROverlaySample::.ctor()
extern void OVROverlaySample__ctor_mB44CEBA69BEAF1A6A58A099C565D834246303FEC (void);
// 0x00000B1C System.Void OculusSampleFramework.OVROverlaySample::<Start>b__24_0(UnityEngine.UI.Toggle)
extern void OVROverlaySample_U3CStartU3Eb__24_0_mBD67EE6633B4F08B23C48B5757BF037F011FA444 (void);
// 0x00000B1D System.Void OculusSampleFramework.OVROverlaySample::<Start>b__24_1(UnityEngine.UI.Toggle)
extern void OVROverlaySample_U3CStartU3Eb__24_1_m9C707473B8AD3A3E9A2A5DE4CBB97FE7067691B9 (void);
// 0x00000B1E System.Void OculusSampleFramework.OVROverlaySample::<Start>b__24_2(UnityEngine.UI.Toggle)
extern void OVROverlaySample_U3CStartU3Eb__24_2_m4676B4B0549094AF080DCCCC880399B388366734 (void);
// 0x00000B1F System.Void OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::.ctor(System.Int32)
extern void U3CWaitforOVROverlayU3Ed__30__ctor_m2AB6E29E1D1F5472C16F001263DCA536B3140AB4 (void);
// 0x00000B20 System.Void OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::System.IDisposable.Dispose()
extern void U3CWaitforOVROverlayU3Ed__30_System_IDisposable_Dispose_m1650460A7C7917454B04CB2BE659585291CBEA40 (void);
// 0x00000B21 System.Boolean OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::MoveNext()
extern void U3CWaitforOVROverlayU3Ed__30_MoveNext_mEC25CDDFCBF7B22EEB0CD01AE64AF805A2A11534 (void);
// 0x00000B22 System.Object OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitforOVROverlayU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m615D0B2569205ABB59D00AC9BA50160C1554A6F2 (void);
// 0x00000B23 System.Void OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::System.Collections.IEnumerator.Reset()
extern void U3CWaitforOVROverlayU3Ed__30_System_Collections_IEnumerator_Reset_mEEB99599B3CF4FC0F90F891156B2FDE7157003F3 (void);
// 0x00000B24 System.Object OculusSampleFramework.OVROverlaySample/<WaitforOVROverlay>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CWaitforOVROverlayU3Ed__30_System_Collections_IEnumerator_get_Current_mD516520BDEAB4043256A81E49B7366A842120F09 (void);
// 0x00000B25 System.Void OVRTouchSample.Hand::Awake()
extern void Hand_Awake_m8481AE5F78D80F6D46DEB65B2B9D0182A56DF09F (void);
// 0x00000B26 System.Void OVRTouchSample.Hand::Start()
extern void Hand_Start_m9705EEDEA171DF9AF0D8E107F3E1D42EF11BC79B (void);
// 0x00000B27 System.Void OVRTouchSample.Hand::OnDestroy()
extern void Hand_OnDestroy_mD3C5A20383810964D0D12F41E40C185DE7AAA046 (void);
// 0x00000B28 System.Void OVRTouchSample.Hand::Update()
extern void Hand_Update_mB561E1D8A8122B9C20C5C6A00B9A43F952A09F2A (void);
// 0x00000B29 System.Void OVRTouchSample.Hand::UpdateCapTouchStates()
extern void Hand_UpdateCapTouchStates_mD04F599C0EB3A207AE420014E51B65DA66F5A49E (void);
// 0x00000B2A System.Void OVRTouchSample.Hand::LateUpdate()
extern void Hand_LateUpdate_mA7932BE84EA2D5A09A4637502E4E52CA4321B8E3 (void);
// 0x00000B2B System.Void OVRTouchSample.Hand::OnInputFocusLost()
extern void Hand_OnInputFocusLost_m8C6FE8A97BA23153CEFD5D8AB561781E43AB6E02 (void);
// 0x00000B2C System.Void OVRTouchSample.Hand::OnInputFocusAcquired()
extern void Hand_OnInputFocusAcquired_m57F1BE51C79CB9B267C1866F851C75E6B848FA1C (void);
// 0x00000B2D System.Single OVRTouchSample.Hand::InputValueRateChange(System.Boolean,System.Single)
extern void Hand_InputValueRateChange_m76AA4719D87E5D5C3BC14DBE98B145551F685784 (void);
// 0x00000B2E System.Void OVRTouchSample.Hand::UpdateAnimStates()
extern void Hand_UpdateAnimStates_m513AAE4141F73A2CF86B65C1E5C88804AC288D6E (void);
// 0x00000B2F System.Void OVRTouchSample.Hand::CollisionEnable(System.Boolean)
extern void Hand_CollisionEnable_m23E8B46BA537F57146805D86CE15089DB121554E (void);
// 0x00000B30 System.Void OVRTouchSample.Hand::.ctor()
extern void Hand__ctor_m7990A68D0B224E873A13E2ABFF28659D667951AE (void);
// 0x00000B31 System.Void OVRTouchSample.Hand/<>c::.cctor()
extern void U3CU3Ec__cctor_m722784DF370910842E77B13ABF29607D0DA4DA74 (void);
// 0x00000B32 System.Void OVRTouchSample.Hand/<>c::.ctor()
extern void U3CU3Ec__ctor_m29502B502F048DD5DE6F011936D3ACF3E92D116E (void);
// 0x00000B33 System.Boolean OVRTouchSample.Hand/<>c::<Start>b__28_0(UnityEngine.Collider)
extern void U3CU3Ec_U3CStartU3Eb__28_0_m982333D324DA5B0E763CCC459B17E62E62201591 (void);
// 0x00000B34 System.Boolean OVRTouchSample.HandPose::get_AllowPointing()
extern void HandPose_get_AllowPointing_mC5B4C9CDBFCF4883AC67C1637C2D1FA5C5553D3D (void);
// 0x00000B35 System.Boolean OVRTouchSample.HandPose::get_AllowThumbsUp()
extern void HandPose_get_AllowThumbsUp_mAE1D1913C2FEE204F4A74A8E40135873C9DB6615 (void);
// 0x00000B36 OVRTouchSample.HandPoseId OVRTouchSample.HandPose::get_PoseId()
extern void HandPose_get_PoseId_m676CB369677442E624811201F99B12E4F5BB8F8E (void);
// 0x00000B37 System.Void OVRTouchSample.HandPose::.ctor()
extern void HandPose__ctor_mB1A903B2059D5FB6676ACBD06E475480BBC767C9 (void);
// 0x00000B38 System.Void OVRTouchSample.TouchController::Update()
extern void TouchController_Update_m19376C0269F927CC0CFD041646CEA78E2748D6C1 (void);
// 0x00000B39 System.Void OVRTouchSample.TouchController::OnInputFocusLost()
extern void TouchController_OnInputFocusLost_m09A7F448B045C56328B5E92B8DE149BA7ADA065A (void);
// 0x00000B3A System.Void OVRTouchSample.TouchController::OnInputFocusAcquired()
extern void TouchController_OnInputFocusAcquired_m7B23CB3B2CAD8C70AE1D8711D16F06F3E9ADA4A9 (void);
// 0x00000B3B System.Void OVRTouchSample.TouchController::.ctor()
extern void TouchController__ctor_mBE4D0026D782C0768D1B286B617197245B28D9C6 (void);
// 0x00000B3C System.Void Oculus.Avatar.AvatarLogger::Log(System.String)
extern void AvatarLogger_Log_mD82AB03540DFED158D2A47B7234DA2E82F83641A (void);
// 0x00000B3D System.Void Oculus.Avatar.AvatarLogger::Log(System.String,UnityEngine.Object)
extern void AvatarLogger_Log_mB15C4AE1EBEE9DCDFBE0E0582B56CC2A57B18568 (void);
// 0x00000B3E System.Void Oculus.Avatar.AvatarLogger::LogWarning(System.String)
extern void AvatarLogger_LogWarning_m4D65766C75422BD60E328AE048685147FBC8235A (void);
// 0x00000B3F System.Void Oculus.Avatar.AvatarLogger::LogError(System.String)
extern void AvatarLogger_LogError_m228ECB3375BEABA9539FB9A9C3946537D36E9991 (void);
// 0x00000B40 System.Void Oculus.Avatar.AvatarLogger::LogError(System.String,UnityEngine.Object)
extern void AvatarLogger_LogError_m5C5E5B23A65ECCA310F45407126FE8CDE7F300B8 (void);
// 0x00000B41 System.Void Oculus.Avatar.CAPI::ovrAvatar_InitializeAndroidUnity(System.String)
extern void CAPI_ovrAvatar_InitializeAndroidUnity_mC3AFCAAC97FB003250BE0CB0DEE4E38598698655 (void);
// 0x00000B42 System.Void Oculus.Avatar.CAPI::Initialize()
extern void CAPI_Initialize_mCFE421CB2BBBD32A30DBAC812779F11DB9C789A9 (void);
// 0x00000B43 System.Void Oculus.Avatar.CAPI::Shutdown()
extern void CAPI_Shutdown_m5213FFC230918B7C139643CEBEE90F4B54D5EC85 (void);
// 0x00000B44 System.Void Oculus.Avatar.CAPI::ovrAvatar_Shutdown()
extern void CAPI_ovrAvatar_Shutdown_m641D8D674098A24109719418021C44EB43BDF9B6 (void);
// 0x00000B45 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarMessage_Pop()
extern void CAPI_ovrAvatarMessage_Pop_mB9AFAFA67F16C9A90444772AC3D999C137B36FC5 (void);
// 0x00000B46 ovrAvatarMessageType Oculus.Avatar.CAPI::ovrAvatarMessage_GetType(System.IntPtr)
extern void CAPI_ovrAvatarMessage_GetType_m300CECDD197F9D8CF189F992685F2985ACD0230C (void);
// 0x00000B47 ovrAvatarMessage_AvatarSpecification Oculus.Avatar.CAPI::ovrAvatarMessage_GetAvatarSpecification(System.IntPtr)
extern void CAPI_ovrAvatarMessage_GetAvatarSpecification_mF47B0AB5074AAA64F9B1EEF29BAFF0BEA81FEB29 (void);
// 0x00000B48 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarMessage_GetAvatarSpecification_Native(System.IntPtr)
extern void CAPI_ovrAvatarMessage_GetAvatarSpecification_Native_m3BBB41279950D874A51A87D4432E0995A25F1A63 (void);
// 0x00000B49 ovrAvatarMessage_AssetLoaded Oculus.Avatar.CAPI::ovrAvatarMessage_GetAssetLoaded(System.IntPtr)
extern void CAPI_ovrAvatarMessage_GetAssetLoaded_m16C066A389042915A5FD37CB1EF9BBE00EA54F30 (void);
// 0x00000B4A System.IntPtr Oculus.Avatar.CAPI::ovrAvatarMessage_GetAssetLoaded_Native(System.IntPtr)
extern void CAPI_ovrAvatarMessage_GetAssetLoaded_Native_mA44E113C8F42B99305017AFDC5FEA1B2877B4746 (void);
// 0x00000B4B System.Void Oculus.Avatar.CAPI::ovrAvatarMessage_Free(System.IntPtr)
extern void CAPI_ovrAvatarMessage_Free_m6C11CF04C0C33DE6956DF293F664401140918F83 (void);
// 0x00000B4C System.IntPtr Oculus.Avatar.CAPI::ovrAvatarSpecificationRequest_Create(System.UInt64)
extern void CAPI_ovrAvatarSpecificationRequest_Create_m4922625DE77E33F591119E7840C5C8F553C5F02F (void);
// 0x00000B4D System.Void Oculus.Avatar.CAPI::ovrAvatarSpecificationRequest_Destroy(System.IntPtr)
extern void CAPI_ovrAvatarSpecificationRequest_Destroy_m22BB42A3C3D22344241058550A686F6D2D756708 (void);
// 0x00000B4E System.Void Oculus.Avatar.CAPI::ovrAvatarSpecificationRequest_SetCombineMeshes(System.IntPtr,System.Boolean)
extern void CAPI_ovrAvatarSpecificationRequest_SetCombineMeshes_m4506CA79C68CAFA97627F706EEE2E88E778A1207 (void);
// 0x00000B4F System.Void Oculus.Avatar.CAPI::ovrAvatarSpecificationRequest_SetLookAndFeelVersion(System.IntPtr,ovrAvatarLookAndFeelVersion)
extern void CAPI_ovrAvatarSpecificationRequest_SetLookAndFeelVersion_m3E892769BC7318EB5F4735A1538EF9FCCBDCB0A1 (void);
// 0x00000B50 System.Void Oculus.Avatar.CAPI::ovrAvatarSpecificationRequest_SetLevelOfDetail(System.IntPtr,ovrAvatarAssetLevelOfDetail)
extern void CAPI_ovrAvatarSpecificationRequest_SetLevelOfDetail_mEE3431DC85D80B5560C6C5F323D3F4B33B3F2F0D (void);
// 0x00000B51 System.Void Oculus.Avatar.CAPI::ovrAvatar_RequestAvatarSpecification(System.UInt64)
extern void CAPI_ovrAvatar_RequestAvatarSpecification_m8A6F491E57EB844E56AC8227EC00C3623127B906 (void);
// 0x00000B52 System.Void Oculus.Avatar.CAPI::ovrAvatar_RequestAvatarSpecificationFromSpecRequest(System.IntPtr)
extern void CAPI_ovrAvatar_RequestAvatarSpecificationFromSpecRequest_m854EFCB6926682E600936B5092A6B271479A45FC (void);
// 0x00000B53 System.Void Oculus.Avatar.CAPI::ovrAvatarSpecificationRequest_SetFallbackLookAndFeelVersion(System.IntPtr,ovrAvatarLookAndFeelVersion)
extern void CAPI_ovrAvatarSpecificationRequest_SetFallbackLookAndFeelVersion_m965D436E837C7FA927AC5C95BD61D45670D40C34 (void);
// 0x00000B54 System.Void Oculus.Avatar.CAPI::ovrAvatarSpecificationRequest_SetExpressiveFlag(System.IntPtr,System.Boolean)
extern void CAPI_ovrAvatarSpecificationRequest_SetExpressiveFlag_m8AD558362CA25158047625D276CF0BE1518DB1CB (void);
// 0x00000B55 System.IntPtr Oculus.Avatar.CAPI::ovrAvatar_Create(System.IntPtr,ovrAvatarCapabilities)
extern void CAPI_ovrAvatar_Create_mFF4CDAC619F6B0F5DF351B7BF7A91A23DEC5DC2F (void);
// 0x00000B56 System.Void Oculus.Avatar.CAPI::ovrAvatar_Destroy(System.IntPtr)
extern void CAPI_ovrAvatar_Destroy_m6279128A1650962A75521A3D767A5114481262B9 (void);
// 0x00000B57 System.Void Oculus.Avatar.CAPI::ovrAvatarPose_UpdateBody(System.IntPtr,ovrAvatarTransform)
extern void CAPI_ovrAvatarPose_UpdateBody_m5451FF570370967D2CA5CF5A2FD4AD7B387002A2 (void);
// 0x00000B58 System.Void Oculus.Avatar.CAPI::ovrAvatarPose_UpdateVoiceVisualization(System.IntPtr,System.Single[])
extern void CAPI_ovrAvatarPose_UpdateVoiceVisualization_m83B3B8C7786ECDC63592A5AC61B009BBD07BF8A0 (void);
// 0x00000B59 System.Void Oculus.Avatar.CAPI::ovrAvatarPose_UpdateVoiceVisualization_Native(System.IntPtr,System.UInt32,System.Single[])
extern void CAPI_ovrAvatarPose_UpdateVoiceVisualization_Native_m86657348A18C3D4DE40B30EE8AAA19414CCB758A (void);
// 0x00000B5A System.Void Oculus.Avatar.CAPI::ovrAvatarPose_UpdateHands(System.IntPtr,ovrAvatarHandInputState,ovrAvatarHandInputState)
extern void CAPI_ovrAvatarPose_UpdateHands_m11125DF72FFEA7B81DC9423AB489E897BA98AEC7 (void);
// 0x00000B5B System.Void Oculus.Avatar.CAPI::ovrAvatarPose_UpdateHandsWithType(System.IntPtr,ovrAvatarHandInputState,ovrAvatarHandInputState,ovrAvatarControllerType)
extern void CAPI_ovrAvatarPose_UpdateHandsWithType_m47D2DD93341719D6315370F0F66FAD3CE3FAC079 (void);
// 0x00000B5C System.Void Oculus.Avatar.CAPI::ovrAvatarPose_Finalize(System.IntPtr,System.Single)
extern void CAPI_ovrAvatarPose_Finalize_m894288410291C8D1B0FF176AC7D7A03803614444 (void);
// 0x00000B5D System.Void Oculus.Avatar.CAPI::ovrAvatar_SetLeftControllerVisibility(System.IntPtr,System.Boolean)
extern void CAPI_ovrAvatar_SetLeftControllerVisibility_m8E28B5C24659C6746F4107B0107242E8B746BB77 (void);
// 0x00000B5E System.Void Oculus.Avatar.CAPI::ovrAvatar_SetRightControllerVisibility(System.IntPtr,System.Boolean)
extern void CAPI_ovrAvatar_SetRightControllerVisibility_m305EE4BE5E299ED15646AE6DF3DE5DCE857F8032 (void);
// 0x00000B5F System.Void Oculus.Avatar.CAPI::ovrAvatar_SetLeftHandVisibility(System.IntPtr,System.Boolean)
extern void CAPI_ovrAvatar_SetLeftHandVisibility_m74D741849CE137EF10E74309DF4604C5EF537B45 (void);
// 0x00000B60 System.Void Oculus.Avatar.CAPI::ovrAvatar_SetRightHandVisibility(System.IntPtr,System.Boolean)
extern void CAPI_ovrAvatar_SetRightHandVisibility_m44DA3611F78B67D6939891D3A5D9E43E866D8611 (void);
// 0x00000B61 System.UInt32 Oculus.Avatar.CAPI::ovrAvatarComponent_Count(System.IntPtr)
extern void CAPI_ovrAvatarComponent_Count_m83DAD58C3C43AB1A1F8C22A35BC338444567787F (void);
// 0x00000B62 System.Void Oculus.Avatar.CAPI::ovrAvatarComponent_Get(System.IntPtr,System.UInt32,System.Boolean,ovrAvatarComponent&)
extern void CAPI_ovrAvatarComponent_Get_m856C62C27353C0E8048F8F016A5CB8CB881BA22C (void);
// 0x00000B63 System.Void Oculus.Avatar.CAPI::ovrAvatarComponent_Get(System.IntPtr,System.Boolean,ovrAvatarComponent&)
extern void CAPI_ovrAvatarComponent_Get_mA52630A153E9A4EE8D01FEE7046EE90F5948066C (void);
// 0x00000B64 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarComponent_Get_Native(System.IntPtr,System.UInt32)
extern void CAPI_ovrAvatarComponent_Get_Native_mCA8B12981816470E2C357D5F51C61AB713F4164D (void);
// 0x00000B65 System.Boolean Oculus.Avatar.CAPI::ovrAvatarPose_GetBaseComponent(System.IntPtr,ovrAvatarBaseComponent&)
extern void CAPI_ovrAvatarPose_GetBaseComponent_mFB79C5989BA57CA1165701730AFBC929D27833D9 (void);
// 0x00000B66 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarPose_GetBaseComponent_Native(System.IntPtr)
extern void CAPI_ovrAvatarPose_GetBaseComponent_Native_m9DBDAF1292E05A652CD67C0277FA71B0EB4E26C9 (void);
// 0x00000B67 System.IntPtr Oculus.Avatar.CAPI::MarshalRenderComponent(System.IntPtr)
// 0x00000B68 System.Boolean Oculus.Avatar.CAPI::ovrAvatarPose_GetBodyComponent(System.IntPtr,ovrAvatarBodyComponent&)
extern void CAPI_ovrAvatarPose_GetBodyComponent_mEDD41CF57AF83FCBF9E79B025743106444A7DF96 (void);
// 0x00000B69 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarPose_GetBodyComponent_Native(System.IntPtr)
extern void CAPI_ovrAvatarPose_GetBodyComponent_Native_m3CD40DE7FC868648986D9DF7F041F8920E57515D (void);
// 0x00000B6A System.Boolean Oculus.Avatar.CAPI::ovrAvatarPose_GetLeftControllerComponent(System.IntPtr,ovrAvatarControllerComponent&)
extern void CAPI_ovrAvatarPose_GetLeftControllerComponent_m5E9AC67844EF05B994D699B554CCA1DCBD489BEB (void);
// 0x00000B6B System.IntPtr Oculus.Avatar.CAPI::ovrAvatarPose_GetLeftControllerComponent_Native(System.IntPtr)
extern void CAPI_ovrAvatarPose_GetLeftControllerComponent_Native_m4789DF168842A0C009153634F763A720302C7F76 (void);
// 0x00000B6C System.Boolean Oculus.Avatar.CAPI::ovrAvatarPose_GetRightControllerComponent(System.IntPtr,ovrAvatarControllerComponent&)
extern void CAPI_ovrAvatarPose_GetRightControllerComponent_mCB3AB6B9A1DE307070973A8C91B764B67DAEDB27 (void);
// 0x00000B6D System.IntPtr Oculus.Avatar.CAPI::ovrAvatarPose_GetRightControllerComponent_Native(System.IntPtr)
extern void CAPI_ovrAvatarPose_GetRightControllerComponent_Native_mE2471D94A0BCB0D26ABFEC8E72074AF277558ED9 (void);
// 0x00000B6E System.Boolean Oculus.Avatar.CAPI::ovrAvatarPose_GetLeftHandComponent(System.IntPtr,ovrAvatarHandComponent&)
extern void CAPI_ovrAvatarPose_GetLeftHandComponent_m56C5861D621E221D3EA6E0E4E5105851433B556C (void);
// 0x00000B6F System.IntPtr Oculus.Avatar.CAPI::ovrAvatarPose_GetLeftHandComponent_Native(System.IntPtr)
extern void CAPI_ovrAvatarPose_GetLeftHandComponent_Native_mB5EC966B7623A7E46EB6D3AD85D4823A43300CDE (void);
// 0x00000B70 System.Boolean Oculus.Avatar.CAPI::ovrAvatarPose_GetRightHandComponent(System.IntPtr,ovrAvatarHandComponent&)
extern void CAPI_ovrAvatarPose_GetRightHandComponent_m65EC0025B2F955F0F1288F5DB2CF4A74127EB792 (void);
// 0x00000B71 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarPose_GetRightHandComponent_Native(System.IntPtr)
extern void CAPI_ovrAvatarPose_GetRightHandComponent_Native_mC3139C0C1DDF6EC0D160E18205790D4F6B70AFA4 (void);
// 0x00000B72 System.Void Oculus.Avatar.CAPI::ovrAvatarAsset_BeginLoading(System.UInt64)
extern void CAPI_ovrAvatarAsset_BeginLoading_m412A64C9C59E2F0EBCFE02927961BA9D4CB901AF (void);
// 0x00000B73 System.Boolean Oculus.Avatar.CAPI::ovrAvatarAsset_BeginLoadingLOD(System.UInt64,ovrAvatarAssetLevelOfDetail)
extern void CAPI_ovrAvatarAsset_BeginLoadingLOD_m5F2AC7457EA9F2F1842ADA810250ABF92E51DED2 (void);
// 0x00000B74 ovrAvatarAssetType Oculus.Avatar.CAPI::ovrAvatarAsset_GetType(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetType_m0D9E2212F900FB45AA11875EE3C48E7184BE0947 (void);
// 0x00000B75 ovrAvatarMeshAssetData Oculus.Avatar.CAPI::ovrAvatarAsset_GetMeshData(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetMeshData_m7787644F34AC7F6596CD317AB4147235824C0E3D (void);
// 0x00000B76 ovrAvatarMeshAssetDataV2 Oculus.Avatar.CAPI::ovrAvatarAsset_GetCombinedMeshData(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetCombinedMeshData_m1F7138A785791EE1610F61CA3D62239EAA76C707 (void);
// 0x00000B77 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarAsset_GetCombinedMeshData_Native(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetCombinedMeshData_Native_m1DED67A93FC3B775CC3935B87709A01302F47DC3 (void);
// 0x00000B78 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarAsset_GetMeshData_Native(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetMeshData_Native_m68F79C0D03B495C23B201641F4FD26ABB873E78C (void);
// 0x00000B79 System.UInt32 Oculus.Avatar.CAPI::ovrAvatarAsset_GetMeshBlendShapeCount(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetMeshBlendShapeCount_m5AE3A4FA1113F89436DFA080FB319A0EF529B032 (void);
// 0x00000B7A System.IntPtr Oculus.Avatar.CAPI::ovrAvatarAsset_GetMeshBlendShapeName(System.IntPtr,System.UInt32)
extern void CAPI_ovrAvatarAsset_GetMeshBlendShapeName_mEA5EBB824098910900EC38EDAE6EE2E68DE5B2E5 (void);
// 0x00000B7B System.UInt32 Oculus.Avatar.CAPI::ovrAvatarAsset_GetSubmeshCount(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetSubmeshCount_mA73ED35AC9BE04E6B9988AB8F305C4ACD6684A11 (void);
// 0x00000B7C System.UInt32 Oculus.Avatar.CAPI::ovrAvatarAsset_GetSubmeshLastIndex(System.IntPtr,System.UInt32)
extern void CAPI_ovrAvatarAsset_GetSubmeshLastIndex_m715D60734E002A939C148A4354E4E46178A48AD6 (void);
// 0x00000B7D System.IntPtr Oculus.Avatar.CAPI::ovrAvatarAsset_GetMeshBlendShapeVertices(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetMeshBlendShapeVertices_mA17001F2BD4CA9DECE4CF348E7EB31266C4F1AC0 (void);
// 0x00000B7E System.IntPtr Oculus.Avatar.CAPI::ovrAvatarAsset_GetAvatar(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetAvatar_mE78CF7692CF3E6EAF03B065480FA89A23600C58E (void);
// 0x00000B7F System.UInt64[] Oculus.Avatar.CAPI::ovrAvatarAsset_GetCombinedMeshIDs(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetCombinedMeshIDs_mA167C983CFB41E0A85A84BD6711C35FEE9D9DC2B (void);
// 0x00000B80 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarAsset_GetCombinedMeshIDs_Native(System.IntPtr,System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetCombinedMeshIDs_Native_m3CC1814E3DE7FA14C1A711B36CF20525FAD5DF9E (void);
// 0x00000B81 System.Void Oculus.Avatar.CAPI::ovrAvatar_GetCombinedMeshAlphaData(System.IntPtr,System.UInt64&,UnityEngine.Vector4&)
extern void CAPI_ovrAvatar_GetCombinedMeshAlphaData_m21EBC47B8A242FAFF5595F97DA1D7B68923FF837 (void);
// 0x00000B82 System.IntPtr Oculus.Avatar.CAPI::ovrAvatar_GetCombinedMeshAlphaData_Native(System.IntPtr,System.IntPtr,System.IntPtr)
extern void CAPI_ovrAvatar_GetCombinedMeshAlphaData_Native_mCF90F067F0A3C586D8A3ED5F60F89B957E303CB1 (void);
// 0x00000B83 ovrAvatarTextureAssetData Oculus.Avatar.CAPI::ovrAvatarAsset_GetTextureData(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetTextureData_mAAB3144F5EE4FF98B35828798D84B1B5EB65B526 (void);
// 0x00000B84 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarAsset_GetTextureData_Native(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetTextureData_Native_m923758A280773BE3AB3C1FEE8DA33194EA2011EC (void);
// 0x00000B85 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarAsset_GetMaterialData_Native(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetMaterialData_Native_m151A2936E08570427FFF09A12FCD3E31274CDE78 (void);
// 0x00000B86 ovrAvatarMaterialState Oculus.Avatar.CAPI::ovrAvatarAsset_GetMaterialState(System.IntPtr)
extern void CAPI_ovrAvatarAsset_GetMaterialState_m180F9D6F3C0CE06C402DCDCD10CA5C7557D0E963 (void);
// 0x00000B87 ovrAvatarRenderPartType Oculus.Avatar.CAPI::ovrAvatarRenderPart_GetType(System.IntPtr)
extern void CAPI_ovrAvatarRenderPart_GetType_m73C2BBEA895BA843BF7FA5CA68150EADDF70229A (void);
// 0x00000B88 ovrAvatarRenderPart_SkinnedMeshRender Oculus.Avatar.CAPI::ovrAvatarRenderPart_GetSkinnedMeshRender(System.IntPtr)
extern void CAPI_ovrAvatarRenderPart_GetSkinnedMeshRender_mE35AE01E81CB778BBAE15AC9D104086A4A2D9E53 (void);
// 0x00000B89 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarRenderPart_GetSkinnedMeshRender_Native(System.IntPtr)
extern void CAPI_ovrAvatarRenderPart_GetSkinnedMeshRender_Native_m9774E96A201192F53EF698299568523E5A1B8862 (void);
// 0x00000B8A ovrAvatarTransform Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRender_GetTransform(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRender_GetTransform_m35FCA048E97B96A46ACE9A123391B59AEA77F508 (void);
// 0x00000B8B ovrAvatarTransform Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBS_GetTransform(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBS_GetTransform_mAB0C72AA6EC962A5EF82DD03546DF0AE870698B4 (void);
// 0x00000B8C ovrAvatarTransform Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBSV2_GetTransform(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBSV2_GetTransform_m410DD5215D3D73B227B855451463068DEE3FF098 (void);
// 0x00000B8D ovrAvatarVisibilityFlags Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRender_GetVisibilityMask(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRender_GetVisibilityMask_m93046DB38EC0F5544C0DC06812A59A3E502BCE5F (void);
// 0x00000B8E System.Boolean Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRender_MaterialStateChanged(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRender_MaterialStateChanged_mCFFA92521C1E4CA16D6EAED8459C77B7FEC352CF (void);
// 0x00000B8F System.Boolean Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBSV2_MaterialStateChanged(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBSV2_MaterialStateChanged_m4E6D330334A76173F99B67A94A0ED70CA1739BE3 (void);
// 0x00000B90 ovrAvatarVisibilityFlags Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBS_GetVisibilityMask(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBS_GetVisibilityMask_m51CF15349045242C26C0518EA4F8AEA73D480028 (void);
// 0x00000B91 ovrAvatarVisibilityFlags Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBSV2_GetVisibilityMask(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBSV2_GetVisibilityMask_mB20C93D31AB725CDA7F9020977A1613B21F84CF2 (void);
// 0x00000B92 ovrAvatarMaterialState Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRender_GetMaterialState(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRender_GetMaterialState_mE60F44C8BCE3F7E6AD540B2B8847BE65BC76A8EA (void);
// 0x00000B93 ovrAvatarPBSMaterialState Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBSV2_GetPBSMaterialState(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBSV2_GetPBSMaterialState_m1DF341E631EE792107E84C25645596523D4BAF12 (void);
// 0x00000B94 ovrAvatarExpressiveParameters Oculus.Avatar.CAPI::ovrAvatar_GetExpressiveParameters(System.IntPtr)
extern void CAPI_ovrAvatar_GetExpressiveParameters_mE5A33D5D5625B3F07CC1884886A1C1198C8301F6 (void);
// 0x00000B95 System.UInt64 Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRender_GetDirtyJoints(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRender_GetDirtyJoints_m015B56A89B28DF47A1FDC94A30EAF1235536F235 (void);
// 0x00000B96 System.UInt64 Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBS_GetDirtyJoints(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBS_GetDirtyJoints_m2C663BD3285C21DA9C672DFB61949630DB050237 (void);
// 0x00000B97 System.UInt64 Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBSV2_GetDirtyJoints(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBSV2_GetDirtyJoints_m01644BC1D48FDAC32F9754A56BE34BCF348E7026 (void);
// 0x00000B98 ovrAvatarTransform Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRender_GetJointTransform(System.IntPtr,System.UInt32)
extern void CAPI_ovrAvatarSkinnedMeshRender_GetJointTransform_m649E628F1A8FFFFD0842F53B7E834F4371DE334C (void);
// 0x00000B99 System.Void Oculus.Avatar.CAPI::ovrAvatar_SetActionUnitOnsetSpeed(System.IntPtr,System.Single)
extern void CAPI_ovrAvatar_SetActionUnitOnsetSpeed_m1C970DE451D337915A13251B250D636261DE7106 (void);
// 0x00000B9A System.Void Oculus.Avatar.CAPI::ovrAvatar_SetActionUnitFalloffSpeed(System.IntPtr,System.Single)
extern void CAPI_ovrAvatar_SetActionUnitFalloffSpeed_m1C6645B4C0628E9E468A3E319BEC2CC0C0D79039 (void);
// 0x00000B9B System.Void Oculus.Avatar.CAPI::ovrAvatar_SetVisemeMultiplier(System.IntPtr,System.Single)
extern void CAPI_ovrAvatar_SetVisemeMultiplier_m925D7D67D82CB100C4081B767AF603A3A99D2EE5 (void);
// 0x00000B9C ovrAvatarTransform Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBS_GetJointTransform(System.IntPtr,System.UInt32)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBS_GetJointTransform_mB3D9A366D251BEEF09448E6055918657679924C3 (void);
// 0x00000B9D ovrAvatarTransform Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBSV2_GetJointTransform(System.IntPtr,System.UInt32)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBSV2_GetJointTransform_m2D30C3004C92C1A25A9E825F11A7C7465AB40CD6 (void);
// 0x00000B9E System.UInt64 Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBS_GetAlbedoTextureAssetID(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBS_GetAlbedoTextureAssetID_m3A4E055B2D93CE5EA7333C2A4C64AD40032BB83B (void);
// 0x00000B9F System.UInt64 Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRenderPBS_GetSurfaceTextureAssetID(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRenderPBS_GetSurfaceTextureAssetID_m5934F61E6459781AB3BCAE08CA103E17D45ED240 (void);
// 0x00000BA0 ovrAvatarRenderPart_SkinnedMeshRenderPBS Oculus.Avatar.CAPI::ovrAvatarRenderPart_GetSkinnedMeshRenderPBS(System.IntPtr)
extern void CAPI_ovrAvatarRenderPart_GetSkinnedMeshRenderPBS_mCF50C6A154A9366C0A82DFE3BFC8E3E7FB88A68F (void);
// 0x00000BA1 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarRenderPart_GetSkinnedMeshRenderPBS_Native(System.IntPtr)
extern void CAPI_ovrAvatarRenderPart_GetSkinnedMeshRenderPBS_Native_m54E4803DEF35DE27C47D6E24604429F65B415AF2 (void);
// 0x00000BA2 ovrAvatarRenderPart_SkinnedMeshRenderPBS_V2 Oculus.Avatar.CAPI::ovrAvatarRenderPart_GetSkinnedMeshRenderPBSV2(System.IntPtr)
extern void CAPI_ovrAvatarRenderPart_GetSkinnedMeshRenderPBSV2_m45D0F9A059342C06596F9993D7422501E3089BF9 (void);
// 0x00000BA3 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarRenderPart_GetSkinnedMeshRenderPBSV2_Native(System.IntPtr)
extern void CAPI_ovrAvatarRenderPart_GetSkinnedMeshRenderPBSV2_Native_m109C808BBB07AEAAFF04FDB6B204D886943721F2 (void);
// 0x00000BA4 System.Void Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRender_GetBlendShapeParams(System.IntPtr,ovrAvatarBlendShapeParams&)
extern void CAPI_ovrAvatarSkinnedMeshRender_GetBlendShapeParams_m6BEF2EDE46D67787CBADF5204AB66F160A96BF95 (void);
// 0x00000BA5 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarSkinnedMeshRender_GetBlendShapeParams_Native(System.IntPtr)
extern void CAPI_ovrAvatarSkinnedMeshRender_GetBlendShapeParams_Native_m0849E1CB05483EFC139298EE8950F40E99EB5C87 (void);
// 0x00000BA6 ovrAvatarRenderPart_ProjectorRender Oculus.Avatar.CAPI::ovrAvatarRenderPart_GetProjectorRender(System.IntPtr)
extern void CAPI_ovrAvatarRenderPart_GetProjectorRender_mE6A8741B119852271D39374B25D29418756D01A4 (void);
// 0x00000BA7 ovrAvatarPBSMaterialState[] Oculus.Avatar.CAPI::ovrAvatar_GetBodyPBSMaterialStates(System.IntPtr)
extern void CAPI_ovrAvatar_GetBodyPBSMaterialStates_mD93F87EC0B218CE87531BCAE3EAE330CA7A54DD2 (void);
// 0x00000BA8 System.IntPtr Oculus.Avatar.CAPI::ovrAvatar_GetBodyPBSMaterialStates_Native(System.IntPtr,System.IntPtr)
extern void CAPI_ovrAvatar_GetBodyPBSMaterialStates_Native_m3F059EC2600CA680759A0530230BA61C6282F456 (void);
// 0x00000BA9 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarRenderPart_GetProjectorRender_Native(System.IntPtr)
extern void CAPI_ovrAvatarRenderPart_GetProjectorRender_Native_mCA06C0BC0D537583DFF90E3AD26FE9F8E2169FB8 (void);
// 0x00000BAA System.UInt32 Oculus.Avatar.CAPI::ovrAvatar_GetReferencedAssetCount(System.IntPtr)
extern void CAPI_ovrAvatar_GetReferencedAssetCount_m4FADDE26FABA9C1AD3D832AB71D3947A7C6B5D6C (void);
// 0x00000BAB System.UInt64 Oculus.Avatar.CAPI::ovrAvatar_GetReferencedAsset(System.IntPtr,System.UInt32)
extern void CAPI_ovrAvatar_GetReferencedAsset_m946755160D5D17674A8B1EABD526537C17D58261 (void);
// 0x00000BAC System.Void Oculus.Avatar.CAPI::ovrAvatar_SetLeftHandGesture(System.IntPtr,ovrAvatarHandGesture)
extern void CAPI_ovrAvatar_SetLeftHandGesture_mFE13D368CE500F7C6112B30F1E8D119C42045569 (void);
// 0x00000BAD System.Void Oculus.Avatar.CAPI::ovrAvatar_SetRightHandGesture(System.IntPtr,ovrAvatarHandGesture)
extern void CAPI_ovrAvatar_SetRightHandGesture_m77AFC95EE0D2BB6329AB8D15D48545A0617EC57D (void);
// 0x00000BAE System.Void Oculus.Avatar.CAPI::ovrAvatar_SetLeftHandCustomGesture(System.IntPtr,System.UInt32,ovrAvatarTransform[])
extern void CAPI_ovrAvatar_SetLeftHandCustomGesture_m2304B758B028F976631B41F42A960CD93608618F (void);
// 0x00000BAF System.Void Oculus.Avatar.CAPI::ovrAvatar_SetRightHandCustomGesture(System.IntPtr,System.UInt32,ovrAvatarTransform[])
extern void CAPI_ovrAvatar_SetRightHandCustomGesture_m2D456833A8A31FB8257581B33EAD3125508807C1 (void);
// 0x00000BB0 System.Void Oculus.Avatar.CAPI::ovrAvatar_UpdatePoseFromPacket(System.IntPtr,System.IntPtr,System.Single)
extern void CAPI_ovrAvatar_UpdatePoseFromPacket_m843F6ACC15BAE43CAD6C8369129BFDB234130817 (void);
// 0x00000BB1 System.Void Oculus.Avatar.CAPI::ovrAvatarPacket_BeginRecording(System.IntPtr)
extern void CAPI_ovrAvatarPacket_BeginRecording_m563474205D13D2C81DF0EBA0E4FA859A8B6545CB (void);
// 0x00000BB2 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarPacket_EndRecording(System.IntPtr)
extern void CAPI_ovrAvatarPacket_EndRecording_mC19C294C14694BC8FE5DD778538C32B35F6EFFF6 (void);
// 0x00000BB3 System.UInt32 Oculus.Avatar.CAPI::ovrAvatarPacket_GetSize(System.IntPtr)
extern void CAPI_ovrAvatarPacket_GetSize_m8911AECB2FF7CAB7B521CE22BB8A3AF6F189691A (void);
// 0x00000BB4 System.Single Oculus.Avatar.CAPI::ovrAvatarPacket_GetDurationSeconds(System.IntPtr)
extern void CAPI_ovrAvatarPacket_GetDurationSeconds_mAF67CD5A9731348400DE5D5B0D142C9B4B80101B (void);
// 0x00000BB5 System.Void Oculus.Avatar.CAPI::ovrAvatarPacket_Free(System.IntPtr)
extern void CAPI_ovrAvatarPacket_Free_mDD18D124EC797E80A397A6309196A34B69AF38F7 (void);
// 0x00000BB6 System.Boolean Oculus.Avatar.CAPI::ovrAvatarPacket_Write(System.IntPtr,System.UInt32,System.Byte[])
extern void CAPI_ovrAvatarPacket_Write_m3FA7BCDE2E9C17A173F4546F292B6F500E373F90 (void);
// 0x00000BB7 System.IntPtr Oculus.Avatar.CAPI::ovrAvatarPacket_Read(System.UInt32,System.Byte[])
extern void CAPI_ovrAvatarPacket_Read_mC585E78E3DDE9F704F62A80A173D371BCBD01F6A (void);
// 0x00000BB8 System.Void Oculus.Avatar.CAPI::ovrAvatar_SetInternalForceASTCTextures(System.Boolean)
extern void CAPI_ovrAvatar_SetInternalForceASTCTextures_m179DB4365DE27697CEB2C1CE7DCF681470B8AE48 (void);
// 0x00000BB9 System.Void Oculus.Avatar.CAPI::ovrAvatar_SetForceASTCTextures(System.Boolean)
extern void CAPI_ovrAvatar_SetForceASTCTextures_m740DAB9EE4F7A772EE075650E8C6E81754EA5B56 (void);
// 0x00000BBA System.Void Oculus.Avatar.CAPI::ovrAvatar_OverrideExpressiveLogic(System.IntPtr,ovrAvatarBlendShapeParams)
extern void CAPI_ovrAvatar_OverrideExpressiveLogic_m5DC0F50ED696C8408D5550CAA58D852E1F06D0F6 (void);
// 0x00000BBB System.Void Oculus.Avatar.CAPI::ovrAvatar_OverrideExpressiveLogic_Native(System.IntPtr,System.IntPtr)
extern void CAPI_ovrAvatar_OverrideExpressiveLogic_Native_mE7C62285999472B0BB3E19487049B9D6447FD718 (void);
// 0x00000BBC System.Void Oculus.Avatar.CAPI::ovrAvatar_SetVisemes(System.IntPtr,ovrAvatarVisemes)
extern void CAPI_ovrAvatar_SetVisemes_m9AEE1056FB54DAA7458CEB6F1E62DADE3BF309C7 (void);
// 0x00000BBD System.Void Oculus.Avatar.CAPI::ovrAvatar_SetVisemes_Native(System.IntPtr,System.IntPtr)
extern void CAPI_ovrAvatar_SetVisemes_Native_mAA20508E980D4A1FA17AC9CA3B9CDA46E92F4D16 (void);
// 0x00000BBE System.Void Oculus.Avatar.CAPI::ovrAvatar_UpdateWorldTransform(System.IntPtr,ovrAvatarTransform)
extern void CAPI_ovrAvatar_UpdateWorldTransform_m6295F3809955F0BE82959AA6E1B02947ABBE6F26 (void);
// 0x00000BBF System.Void Oculus.Avatar.CAPI::ovrAvatar_UpdateGazeTargets(ovrAvatarGazeTargets)
extern void CAPI_ovrAvatar_UpdateGazeTargets_m2427C132C152AC9F96755A4CB55933B4FCA3B173 (void);
// 0x00000BC0 System.Void Oculus.Avatar.CAPI::ovrAvatar_UpdateGazeTargets_Native(System.IntPtr)
extern void CAPI_ovrAvatar_UpdateGazeTargets_Native_mDE1B61A465C0B35D56ADD7E17A76534A2D4BE74E (void);
// 0x00000BC1 System.Void Oculus.Avatar.CAPI::ovrAvatar_RemoveGazeTargets(System.UInt32,System.UInt32[])
extern void CAPI_ovrAvatar_RemoveGazeTargets_m33FE86B4F58B30F059B05DE3F2B8CDD6FEFA73D8 (void);
// 0x00000BC2 System.Void Oculus.Avatar.CAPI::ovrAvatar_UpdateLights(ovrAvatarLights)
extern void CAPI_ovrAvatar_UpdateLights_mCDA09C2977A0E65A2A47850E146552E742B5FAFD (void);
// 0x00000BC3 System.Void Oculus.Avatar.CAPI::ovrAvatar_UpdateLights_Native(System.IntPtr)
extern void CAPI_ovrAvatar_UpdateLights_Native_m551EA55815F41323BAAB0A4033E8DAD2677547D9 (void);
// 0x00000BC4 System.Void Oculus.Avatar.CAPI::ovrAvatar_RemoveLights(System.UInt32,System.UInt32[])
extern void CAPI_ovrAvatar_RemoveLights_m0F5E1BB5D7EC370C7464E9B28068310BFF7964F0 (void);
// 0x00000BC5 System.Void Oculus.Avatar.CAPI::LoggingCallback(System.IntPtr)
extern void CAPI_LoggingCallback_mE9CB96D4CEB343BBBBAA0ACD39A6281C11D4110B (void);
// 0x00000BC6 System.Void Oculus.Avatar.CAPI::ovrAvatar_RegisterLoggingCallback(Oculus.Avatar.CAPI/LoggingDelegate)
extern void CAPI_ovrAvatar_RegisterLoggingCallback_mC3CB369A24FBB4E09D8609FC0FA9822788DFB371 (void);
// 0x00000BC7 System.Void Oculus.Avatar.CAPI::ovrAvatar_SetLoggingLevel(ovrAvatarLogLevel)
extern void CAPI_ovrAvatar_SetLoggingLevel_m9E569FD2FF24290A14C61169D4D25352C2E0C2EA (void);
// 0x00000BC8 System.IntPtr Oculus.Avatar.CAPI::ovrAvatar_GetDebugTransforms_Native(System.IntPtr)
extern void CAPI_ovrAvatar_GetDebugTransforms_Native_mEC12C23FCDB91C82B2C82CA6AF845F02B8C3D5F0 (void);
// 0x00000BC9 System.IntPtr Oculus.Avatar.CAPI::ovrAvatar_GetDebugLines_Native(System.IntPtr)
extern void CAPI_ovrAvatar_GetDebugLines_Native_mCE0C0453BEC741E27AFD02BD843DF51F0143E5EE (void);
// 0x00000BCA System.Void Oculus.Avatar.CAPI::ovrAvatar_DrawDebugLines()
extern void CAPI_ovrAvatar_DrawDebugLines_mE5E484E9686916F4B31C49FF4E5633C26C845D92 (void);
// 0x00000BCB System.Void Oculus.Avatar.CAPI::ovrAvatar_SetDebugDrawContext(System.UInt32)
extern void CAPI_ovrAvatar_SetDebugDrawContext_mC4B961A7059F3574401E7D332B79A4DFBB73FE95 (void);
// 0x00000BCC System.Boolean Oculus.Avatar.CAPI::SendEvent(System.String,System.String,System.String)
extern void CAPI_SendEvent_m02C69575F737F9751E1143A8041F884E3AA7B123 (void);
// 0x00000BCD System.IntPtr Oculus.Avatar.CAPI::_ovrp_GetVersion()
extern void CAPI__ovrp_GetVersion_m4EBDC24710D40A75EC52A5C970A36F0810F0EE40 (void);
// 0x00000BCE System.String Oculus.Avatar.CAPI::ovrp_GetVersion()
extern void CAPI_ovrp_GetVersion_mA0A03FA3CA5C3C3C8F42CE0B889D099B5224FB70 (void);
// 0x00000BCF System.Void Oculus.Avatar.CAPI::.ctor()
extern void CAPI__ctor_mBE1FA2A63058548F6EDD1569CFCC07A75C778512 (void);
// 0x00000BD0 System.Void Oculus.Avatar.CAPI::.cctor()
extern void CAPI__cctor_mE274F449E306322E410B64038426400CCB646766 (void);
// 0x00000BD1 System.Void Oculus.Avatar.CAPI/LoggingDelegate::.ctor(System.Object,System.IntPtr)
extern void LoggingDelegate__ctor_mC675B66814B9B9686DB64204977D33F783791245 (void);
// 0x00000BD2 System.Void Oculus.Avatar.CAPI/LoggingDelegate::Invoke(System.IntPtr)
extern void LoggingDelegate_Invoke_m33D2B930F9EBF07BC2FCA8E9C05C053E011394CD (void);
// 0x00000BD3 System.IAsyncResult Oculus.Avatar.CAPI/LoggingDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void LoggingDelegate_BeginInvoke_mC00109CB6D9B7D2B923275339A8001FABB3FDC06 (void);
// 0x00000BD4 System.Void Oculus.Avatar.CAPI/LoggingDelegate::EndInvoke(System.IAsyncResult)
extern void LoggingDelegate_EndInvoke_mA9ACBF14A04F2B84FD884DB9F00DDE480A0119E3 (void);
// 0x00000BD5 Oculus.Avatar.CAPI/Result Oculus.Avatar.CAPI/OVRP_1_30_0::ovrp_SendEvent2(System.String,System.String,System.String)
extern void OVRP_1_30_0_ovrp_SendEvent2_mC8B91A032C7E71967F8841B996384EA3150F943E (void);
// 0x00000BD6 System.Void Oculus.Avatar.CAPI/OVRP_1_30_0::.cctor()
extern void OVRP_1_30_0__cctor_m3418A2000FF05F393EE1604666B66467E83787E9 (void);
// 0x00000BD7 System.Void Obi.ObiCharacter::Start()
extern void ObiCharacter_Start_m9AF9959BA6BB669EC44CC6A19F2C4921A8D3EC12 (void);
// 0x00000BD8 System.Void Obi.ObiCharacter::Move(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern void ObiCharacter_Move_mE36529E342431BC2E3F82ED9221503AF7F957934 (void);
// 0x00000BD9 System.Void Obi.ObiCharacter::ScaleCapsuleForCrouching(System.Boolean)
extern void ObiCharacter_ScaleCapsuleForCrouching_mBEE0D314BF3D424DEC1B2D4249AC6F1F1B577A51 (void);
// 0x00000BDA System.Void Obi.ObiCharacter::PreventStandingInLowHeadroom()
extern void ObiCharacter_PreventStandingInLowHeadroom_mE8D5660997F99D5B260E7CCC9BF758FFF4FDFEDD (void);
// 0x00000BDB System.Void Obi.ObiCharacter::UpdateAnimator(UnityEngine.Vector3)
extern void ObiCharacter_UpdateAnimator_m0133BC89517D199811A84D3308D99A39508A6163 (void);
// 0x00000BDC System.Void Obi.ObiCharacter::HandleAirborneMovement()
extern void ObiCharacter_HandleAirborneMovement_m2237FCB54D63F3C66E8DC444AE9485868A07E604 (void);
// 0x00000BDD System.Void Obi.ObiCharacter::HandleGroundedMovement(System.Boolean,System.Boolean)
extern void ObiCharacter_HandleGroundedMovement_m665A36CA6A0EAD5D0C62AA11737F2A6308EEEDC6 (void);
// 0x00000BDE System.Void Obi.ObiCharacter::ApplyExtraTurnRotation()
extern void ObiCharacter_ApplyExtraTurnRotation_m0D9CC7D6882FDB7888330B062F69EE0FA0667BF5 (void);
// 0x00000BDF System.Void Obi.ObiCharacter::OnAnimatorMove()
extern void ObiCharacter_OnAnimatorMove_mE0C7A78FB55C063DF34E2BC3FF6F84125B63AB6A (void);
// 0x00000BE0 System.Void Obi.ObiCharacter::CheckGroundStatus()
extern void ObiCharacter_CheckGroundStatus_m21B58AEB9AB71B4D7885687B1C5979A9CB9CF45E (void);
// 0x00000BE1 System.Void Obi.ObiCharacter::.ctor()
extern void ObiCharacter__ctor_m16255C98358F19058CDB9991C96F04FE19EA7A93 (void);
// 0x00000BE2 System.Void Obi.SampleCharacterController::Start()
extern void SampleCharacterController_Start_mE5EAF1421DBAD9FFAC2D550E04332581CDFFE4BC (void);
// 0x00000BE3 System.Void Obi.SampleCharacterController::FixedUpdate()
extern void SampleCharacterController_FixedUpdate_m1AC87DDCCC039856B18C977E02E0CD2D80F1A524 (void);
// 0x00000BE4 System.Void Obi.SampleCharacterController::.ctor()
extern void SampleCharacterController__ctor_m8605568614106AD773DE856FE8AC1968C23A4262 (void);
// 0x00000BE5 System.Void Obi.ColorFromPhase::Awake()
extern void ColorFromPhase_Awake_m8EE051CBD4CAF6406768C2D6941DCABCD6CFD2B9 (void);
// 0x00000BE6 System.Void Obi.ColorFromPhase::LateUpdate()
extern void ColorFromPhase_LateUpdate_m56F07E6A796032EB6DD06CE10C57090983DAA1AB (void);
// 0x00000BE7 System.Void Obi.ColorFromPhase::.ctor()
extern void ColorFromPhase__ctor_mECC2D22AC69AC7C21BC64BF585AB14C65E7A2415 (void);
// 0x00000BE8 System.Void Obi.ColorFromVelocity::Awake()
extern void ColorFromVelocity_Awake_mBA50845A66FAE55EB4F4637C1BF190EE329FAEB5 (void);
// 0x00000BE9 System.Void Obi.ColorFromVelocity::OnEnable()
extern void ColorFromVelocity_OnEnable_m60CE9B091B8725925F60E867BE08F1F137E2AB9D (void);
// 0x00000BEA System.Void Obi.ColorFromVelocity::LateUpdate()
extern void ColorFromVelocity_LateUpdate_m4A026CE0BCACBCA70F219D29A3EEFA7EDE33C2A3 (void);
// 0x00000BEB System.Void Obi.ColorFromVelocity::.ctor()
extern void ColorFromVelocity__ctor_mD26A1B2F148FC8BF6F57EDEA8179E0B2D8CD8FFC (void);
// 0x00000BEC System.Void Obi.ColorRandomizer::Start()
extern void ColorRandomizer_Start_mF174C12B77903483554BBCE5D4A4D82118DD4651 (void);
// 0x00000BED System.Void Obi.ColorRandomizer::.ctor()
extern void ColorRandomizer__ctor_mF1141F2D29657D595C863D186B46C6B3CD672A85 (void);
// 0x00000BEE System.Void Obi.LookAroundCamera::Awake()
extern void LookAroundCamera_Awake_mE90B4191F1288756CA34EEC0C21C1998251B0E8E (void);
// 0x00000BEF System.Void Obi.LookAroundCamera::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LookAroundCamera_LookAt_mEF29B91315C1AD0156E505C73EF9931CDD6E7718 (void);
// 0x00000BF0 System.Void Obi.LookAroundCamera::UpdateShot()
extern void LookAroundCamera_UpdateShot_m8C764716D01D701607A13430043C9E5B87A50597 (void);
// 0x00000BF1 System.Void Obi.LookAroundCamera::LateUpdate()
extern void LookAroundCamera_LateUpdate_m18FD061FDD268DE063DEC79BAD72F08432F1A7A8 (void);
// 0x00000BF2 System.Void Obi.LookAroundCamera::.ctor()
extern void LookAroundCamera__ctor_m52D5B9733D32E65A768D29868D4B07EB63988D12 (void);
// 0x00000BF3 System.Void Obi.LookAroundCamera/CameraShot::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Single)
extern void CameraShot__ctor_mD9736DEA6A3F14DF47920666D00E1ABA5899E179 (void);
// 0x00000BF4 System.Void Obi.MoveAndRotate::Start()
extern void MoveAndRotate_Start_m81492AAA391819DF463B1781F3F647B8F50E8E1D (void);
// 0x00000BF5 System.Void Obi.MoveAndRotate::FixedUpdate()
extern void MoveAndRotate_FixedUpdate_mC9020498CC29B200215037EB0EA7508367CCD8BD (void);
// 0x00000BF6 System.Void Obi.MoveAndRotate::.ctor()
extern void MoveAndRotate__ctor_mC46EA5D888532267ADC051A68F468CD7FD5D2130 (void);
// 0x00000BF7 System.Void Obi.MoveAndRotate/Vector3andSpace::.ctor()
extern void Vector3andSpace__ctor_m5653715EE0EAB2271B055D2729CCF8046D2CE78B (void);
// 0x00000BF8 System.Void Ignis.OAVAShaderCompatibilitySO::.ctor()
extern void OAVAShaderCompatibilitySO__ctor_mAA228997C3D85FC960519BA027203240D4666FED (void);
// 0x00000BF9 System.Void Ignis.OAVAShaderCompatibilitySO/ShaderProperty::.ctor()
extern void ShaderProperty__ctor_m997A0C6F6C2D131D775B6A226E0ACDD2EE0418D4 (void);
// 0x00000BFA System.Void Ignis.TouchedTheFlame::TouchedTheFlameOuch()
extern void TouchedTheFlame_TouchedTheFlameOuch_mFE81C59E0E5287A3CAA04B019DBD8CC6F1470101 (void);
// 0x00000BFB System.Void Ignis.TouchedTheFlame::.ctor()
extern void TouchedTheFlame__ctor_m0CBCFA2771B2A381DC1C67233DC22559E28656BE (void);
// 0x00000BFC System.Void Ignis.FireTrigger::FixedUpdate()
extern void FireTrigger_FixedUpdate_mBCEE37C16BF24A28A54CF51E24B04600AF4A19FF (void);
// 0x00000BFD System.Void Ignis.FireTrigger::Update()
extern void FireTrigger_Update_mD64612F52DEB2CF78148B944D09FEE88D6454857 (void);
// 0x00000BFE System.Void Ignis.FireTrigger::StartTrigger()
extern void FireTrigger_StartTrigger_mBB9BFB3C0051D4EE8FFEE09FC1DA9799C585090F (void);
// 0x00000BFF System.Void Ignis.FireTrigger::TriggerUpdate()
extern void FireTrigger_TriggerUpdate_m1A8E5FEFFDABDDFE168EEDE4696065B581E8C2DA (void);
// 0x00000C00 System.Void Ignis.FireTrigger::ProcessCollidedObject(UnityEngine.Collider)
extern void FireTrigger_ProcessCollidedObject_mA9075C2917D7F435CF97BBEBB7FF054CB3608CCA (void);
// 0x00000C01 System.Void Ignis.FireTrigger::OnDrawGizmos()
extern void FireTrigger_OnDrawGizmos_mFFE3214519A5BEB4E056352AB36C1CBC16E84395 (void);
// 0x00000C02 System.Void Ignis.FireTrigger::.ctor()
extern void FireTrigger__ctor_mB2770FCA6AC51839D36D2503D0052C97840DFA45 (void);
// 0x00000C03 System.Void Ignis.FlameCollisionCallbacks::TryToTriggerCollisionEvents(UnityEngine.GameObject)
extern void FlameCollisionCallbacks_TryToTriggerCollisionEvents_mF9FC6337910EF35C16B540B56CF3422CB2492E3A (void);
// 0x00000C04 System.Void Ignis.FlameCollisionCallbacks::TriggerCollisionEvents()
extern void FlameCollisionCallbacks_TriggerCollisionEvents_mEF1AC1BBB87A64687E135BABAEABF9421ABA5AB1 (void);
// 0x00000C05 System.Boolean Ignis.FlameCollisionCallbacks::HasCallback(UnityEngine.GameObject)
extern void FlameCollisionCallbacks_HasCallback_mED2E81D6527F9D0E60797F82781618678B4695A2 (void);
// 0x00000C06 System.Void Ignis.FlameCollisionCallbacks::.ctor()
extern void FlameCollisionCallbacks__ctor_mAADF8621641E35A5EE3FD114DC50EFD5D2917BB8 (void);
// 0x00000C07 Ignis.FlameEngine Ignis.FlameEngine::get_instance()
extern void FlameEngine_get_instance_m232F83C18F4BF627FF52595F12EEBEEDC7BED039 (void);
// 0x00000C08 System.Void Ignis.FlameEngine::Awake()
extern void FlameEngine_Awake_mB83AD523C7DF9DC72017C3D61FE21B9DC351FB3D (void);
// 0x00000C09 System.Void Ignis.FlameEngine::Start()
extern void FlameEngine_Start_m2FC007786DF6696258A1B99A57F58048F88D3C60 (void);
// 0x00000C0A System.Void Ignis.FlameEngine::PauseFlames()
extern void FlameEngine_PauseFlames_mB0A734A1FB4E2441EC6FA51914F456B354E0D24A (void);
// 0x00000C0B System.Void Ignis.FlameEngine::ResumeFlames()
extern void FlameEngine_ResumeFlames_mBF39CD2758FA544479946BB41F626A8268EB6D13 (void);
// 0x00000C0C UnityEngine.GameObject Ignis.FlameEngine::GetFireVFX(Ignis.FlameEngine/FireVFXVariant,System.Boolean)
extern void FlameEngine_GetFireVFX_mE24A9AEDF3C29D56409654670F31584033AE287B (void);
// 0x00000C0D UnityEngine.GameObject Ignis.FlameEngine::GetFireVFX()
extern void FlameEngine_GetFireVFX_m53705E8AF3FDEF1104DD5E6C5403109B97D367C0 (void);
// 0x00000C0E UnityEngine.GameObject Ignis.FlameEngine::GetFireVFX(Ignis.FlameEngine/FireVFXVariant)
extern void FlameEngine_GetFireVFX_m69BA741535A64C23EB6383F80A8D4FB9B59B1B70 (void);
// 0x00000C0F System.Collections.Generic.List`1<Ignis.OAVAShaderCompatibilitySO> Ignis.FlameEngine::GetCompatibleShaders()
extern void FlameEngine_GetCompatibleShaders_mF63E2716C709437A56D3904CA2D0DEC441340032 (void);
// 0x00000C10 System.Collections.Generic.List`1<Ignis.FlameCollisionCallbacks> Ignis.FlameEngine::GetCollisionCallbacks()
extern void FlameEngine_GetCollisionCallbacks_mE751FB06B4E80AC17C32C3814ED3B8C43E3121E6 (void);
// 0x00000C11 UnityEngine.GameObject Ignis.FlameEngine::MaskInstanceAndSpawnAPrefabIfNecessary(UnityEngine.GameObject)
extern void FlameEngine_MaskInstanceAndSpawnAPrefabIfNecessary_mD68F04E4A694A289E7D98E23E1283B116DEDDEFE (void);
// 0x00000C12 System.Void Ignis.FlameEngine::.ctor()
extern void FlameEngine__ctor_mC09FE9AE4EDA5AB337C4742C54C18242C8F0D852 (void);
// 0x00000C13 System.Void Ignis.IgnisUnityTerrain::Start()
extern void IgnisUnityTerrain_Start_m88AFB9A25BAE4C0339FE86579D00EA87280EF87D (void);
// 0x00000C14 System.Void Ignis.IgnisUnityTerrain::UpdateTrees()
extern void IgnisUnityTerrain_UpdateTrees_mBE389708FD27AD2EF36286E47ECAC19ACA6151D2 (void);
// 0x00000C15 System.Void Ignis.IgnisUnityTerrain::OnApplicationQuit()
extern void IgnisUnityTerrain_OnApplicationQuit_m96822240263D3AFCA2E5A19F6A402454EFF99384 (void);
// 0x00000C16 System.Void Ignis.IgnisUnityTerrain::.ctor()
extern void IgnisUnityTerrain__ctor_m78BAB8EFA3A0784197F2A8C6D9F02B22C63B8F57 (void);
// 0x00000C17 System.Void Ignis.VegetationStudioProTreeUnMasker::Update()
extern void VegetationStudioProTreeUnMasker_Update_m7C5E38F4C2C81583D8A5A236B279B829A689906D (void);
// 0x00000C18 System.Void Ignis.VegetationStudioProTreeUnMasker::Unmask()
extern void VegetationStudioProTreeUnMasker_Unmask_m409D219E096A8B31DDB90A775F9CC6DFECC7D011 (void);
// 0x00000C19 System.Void Ignis.VegetationStudioProTreeUnMasker::.ctor()
extern void VegetationStudioProTreeUnMasker__ctor_m14BA2E2B1787670C66AD9BB8E16FDF02623C282C (void);
// 0x00000C1A System.Void Ignis.WindRetrieve::Start()
extern void WindRetrieve_Start_mD03B31BCFB340E75D2C07FECEBF1473F22DC13C6 (void);
// 0x00000C1B System.Void Ignis.WindRetrieve::Update()
extern void WindRetrieve_Update_m0E8C775E0EAC5C4C5715EB8A09617FDF0A161ECC (void);
// 0x00000C1C System.Boolean Ignis.WindRetrieve::OnUse()
extern void WindRetrieve_OnUse_m5CB76729B104977E482C03905B587D34E58A2621 (void);
// 0x00000C1D UnityEngine.Vector3 Ignis.WindRetrieve::GetCurrentWindVelocity()
extern void WindRetrieve_GetCurrentWindVelocity_mCA7856B1773745D48B1EBB3A3A4DB184F75C4BB7 (void);
// 0x00000C1E System.Void Ignis.WindRetrieve::SetupParticleSystem()
extern void WindRetrieve_SetupParticleSystem_mCFC2952D9F5CF86F4856C91423DC9BC9E9A501B6 (void);
// 0x00000C1F System.Void Ignis.WindRetrieve::.ctor()
extern void WindRetrieve__ctor_m51453E6D716020DB7E56B64805648210B14051FF (void);
// 0x00000C20 System.Void Ignis.DebugFlammableVFXBox::OnEnable()
extern void DebugFlammableVFXBox_OnEnable_m2A97FA2CB574ADF2F7E72561084FD558CFEA2549 (void);
// 0x00000C21 System.Void Ignis.DebugFlammableVFXBox::Awake()
extern void DebugFlammableVFXBox_Awake_m8F38894B51CD3F2B461EB6B587224F45276483E9 (void);
// 0x00000C22 System.Void Ignis.DebugFlammableVFXBox::Update()
extern void DebugFlammableVFXBox_Update_m4C4B6EF690528C5669F5357D3F6B5AFDD8F4D545 (void);
// 0x00000C23 System.Void Ignis.DebugFlammableVFXBox::SetMeshOnFire()
extern void DebugFlammableVFXBox_SetMeshOnFire_mACC70574A1C2A3C06B286990EBD9508F4A7743D2 (void);
// 0x00000C24 System.Void Ignis.DebugFlammableVFXBox::SetBoxesOnFire()
extern void DebugFlammableVFXBox_SetBoxesOnFire_mD58E7A45CE8559D31C29B843F238E5D146B2BE27 (void);
// 0x00000C25 System.Void Ignis.DebugFlammableVFXBox::SetupFireConstants(UnityEngine.VFX.VisualEffect)
extern void DebugFlammableVFXBox_SetupFireConstants_mB205435CEFAEF77F77BF7D8B056B24E845BA9D20 (void);
// 0x00000C26 System.Void Ignis.DebugFlammableVFXBox::GenerateColliderFromMesh(UnityEngine.GameObject)
extern void DebugFlammableVFXBox_GenerateColliderFromMesh_m7D882D4D3F5208593475FF94CF2E66691076A89B (void);
// 0x00000C27 System.Void Ignis.DebugFlammableVFXBox::OnDisable()
extern void DebugFlammableVFXBox_OnDisable_mDC14E163FA38D1457074E6610BEB415F7D12805B (void);
// 0x00000C28 System.Void Ignis.DebugFlammableVFXBox::Clean()
extern void DebugFlammableVFXBox_Clean_mEA5DC66AA0B341289F8740567CA10E3D0F5EFB06 (void);
// 0x00000C29 System.Void Ignis.DebugFlammableVFXBox::OnDestroy()
extern void DebugFlammableVFXBox_OnDestroy_m5ABF1B4209B5562DEA57E7359FABBFC327F26991 (void);
// 0x00000C2A System.Void Ignis.DebugFlammableVFXBox::.ctor()
extern void DebugFlammableVFXBox__ctor_m56A59DF73ED4E23F407FA86A55A2BBE45EF1A372 (void);
// 0x00000C2B System.Void Ignis.FlameTriggerCallbacks::TriggerEvents()
extern void FlameTriggerCallbacks_TriggerEvents_m1D14BDFEC8B93791C11629AB221986F72A8F5A95 (void);
// 0x00000C2C System.Void Ignis.FlameTriggerCallbacks::.ctor()
extern void FlameTriggerCallbacks__ctor_mBD0C4EE1F92B4C697C26A109E48D1F50BC5FC076 (void);
// 0x00000C2D System.Void Ignis.FlammableObject::Awake()
extern void FlammableObject_Awake_m45C7971EA8DDC0680EDD3A46090FBE479CC6EBCA (void);
// 0x00000C2E System.Void Ignis.FlammableObject::Start()
extern void FlammableObject_Start_m42C1FBD14CA817C984D85784DB65052304AEC2EB (void);
// 0x00000C2F System.Void Ignis.FlammableObject::OnEnable()
extern void FlammableObject_OnEnable_mE9BB40E9E8696B84455B8AF2B3822A91BE76F765 (void);
// 0x00000C30 System.Void Ignis.FlammableObject::FlammableOnStart()
extern void FlammableObject_FlammableOnStart_m7F775E152727CAD1220E50978D2183DF1D561CB4 (void);
// 0x00000C31 System.Void Ignis.FlammableObject::OnDisable()
extern void FlammableObject_OnDisable_m27395D3C9343BE0C48E9D5CF13736B13C0C361C7 (void);
// 0x00000C32 System.Void Ignis.FlammableObject::Update()
extern void FlammableObject_Update_m69D6CC4DF6BE79E76F585E1CE7C84272A0674868 (void);
// 0x00000C33 System.Void Ignis.FlammableObject::SaveOriginalMaterialShaderProperties()
extern void FlammableObject_SaveOriginalMaterialShaderProperties_mD8AE9957F20310D6A7A51C00E67F82BD827CB0AA (void);
// 0x00000C34 System.Void Ignis.FlammableObject::ResetMaterialFromIgnis()
extern void FlammableObject_ResetMaterialFromIgnis_mA78F56E637D438EBCA9DDDCA54CFAFEB9C56658E (void);
// 0x00000C35 System.Void Ignis.FlammableObject::ResetObj()
extern void FlammableObject_ResetObj_mC85841A0F6D78680D9ED5641EA9A8B22197E95EE (void);
// 0x00000C36 System.Void Ignis.FlammableObject::SetOnFireFromCenter()
extern void FlammableObject_SetOnFireFromCenter_m4894E67CA373F34B98D290EDFFAEC3CA88C64AAE (void);
// 0x00000C37 System.Void Ignis.FlammableObject::TryToSetOnFire(UnityEngine.Vector3,System.Single)
extern void FlammableObject_TryToSetOnFire_mFBB99527ED1C876673D3210D86B53495C95F4B1D (void);
// 0x00000C38 System.Void Ignis.FlammableObject::TryToSetOnFireIgniteProgressIncrease(UnityEngine.Vector3,System.Single)
extern void FlammableObject_TryToSetOnFireIgniteProgressIncrease_m63C2B4944FA48F824743020C4093A0FCF4F0928E (void);
// 0x00000C39 System.Collections.IEnumerator Ignis.FlammableObject::CountExtinguishTime()
extern void FlammableObject_CountExtinguishTime_m56EF944C3DB03B7A9C185A79E5B0043F89D1C161 (void);
// 0x00000C3A System.Void Ignis.FlammableObject::IncrementalExtinguish(UnityEngine.Vector3,System.Single,System.Single)
extern void FlammableObject_IncrementalExtinguish_mAC7A854C82E30EB85F09CF3CD44B39A148C4DADD (void);
// 0x00000C3B System.Void Ignis.FlammableObject::UpdateLights()
extern void FlammableObject_UpdateLights_m769B0B7FF81D40563012014EE070B5185F62CA3E (void);
// 0x00000C3C System.Void Ignis.FlammableObject::UpdateShaders()
extern void FlammableObject_UpdateShaders_mBAB5DC9D231FED6DC79861179EA3DF7156693800 (void);
// 0x00000C3D System.Void Ignis.FlammableObject::SetupShaders()
extern void FlammableObject_SetupShaders_m4DEF8C57CF77FDE5F81A22E810E1ACC23160D011 (void);
// 0x00000C3E System.Void Ignis.FlammableObject::SetupIgnisShader(UnityEngine.Material)
extern void FlammableObject_SetupIgnisShader_mB664F34BC4F6EB4D879291C09752CCDB3C8FECAA (void);
// 0x00000C3F System.Void Ignis.FlammableObject::KeywordsEnableCompatibleShaders(UnityEngine.Material)
extern void FlammableObject_KeywordsEnableCompatibleShaders_mB3495D8ADD368A113956CE189752052674B2A5EE (void);
// 0x00000C40 System.Void Ignis.FlammableObject::GenerateColliderFromRenderer(UnityEngine.GameObject)
extern void FlammableObject_GenerateColliderFromRenderer_m09FFD402BB2688EFBE619376A7F8152DB4FAF915 (void);
// 0x00000C41 System.Void Ignis.FlammableObject::CalculateApproxSize()
extern void FlammableObject_CalculateApproxSize_mE2059C5D4707FE78A107F6C49A825AC93D80D5DE (void);
// 0x00000C42 System.Void Ignis.FlammableObject::UpdateSupportedThirdPartyShaders(UnityEngine.Material,UnityEngine.Renderer)
extern void FlammableObject_UpdateSupportedThirdPartyShaders_m8993F249B00A11DAB289DFF73E998DB6213AF6BC (void);
// 0x00000C43 System.Void Ignis.FlammableObject::UpdateCompatibleShaders(UnityEngine.Material,UnityEngine.Renderer)
extern void FlammableObject_UpdateCompatibleShaders_m7B653977AACA388BA72F160BF8AFDDA30B63218D (void);
// 0x00000C44 System.Void Ignis.FlammableObject::UpdateIgnisShader(UnityEngine.Material,UnityEngine.Renderer)
extern void FlammableObject_UpdateIgnisShader_mC1DB0891850339334A487D47E25E77395A4B43A6 (void);
// 0x00000C45 System.Void Ignis.FlammableObject::UpdateVFX()
extern void FlammableObject_UpdateVFX_m99F99F9436AE0DFECAF8C92648E9F49285A6205B (void);
// 0x00000C46 System.Void Ignis.FlammableObject::UpdateSFX(System.Int32,System.Single)
extern void FlammableObject_UpdateSFX_mE66C7252C15BC1E27903B826F67407A91AB32451 (void);
// 0x00000C47 System.Void Ignis.FlammableObject::CheckForRemove()
extern void FlammableObject_CheckForRemove_m31CACD93DF6C9FE5C827F3F20AA1F162309CBC89 (void);
// 0x00000C48 System.Void Ignis.FlammableObject::InvokeCallbacks()
extern void FlammableObject_InvokeCallbacks_m7ACD7BEE58C98D5E3BB645E2C1961076263B4A88 (void);
// 0x00000C49 System.Void Ignis.FlammableObject::SetupFireConstants(UnityEngine.VFX.VisualEffect)
extern void FlammableObject_SetupFireConstants_m4FD7FDF391DF216206D4FBB6C454BE98C851E9F9 (void);
// 0x00000C4A System.Void Ignis.FlammableObject::SetupSFX(UnityEngine.Vector3,UnityEngine.Transform)
extern void FlammableObject_SetupSFX_m6B4C3D4E7E4789F812FD8928312992BD9CD8E822 (void);
// 0x00000C4B System.Void Ignis.FlammableObject::SetBoxesOnFire()
extern void FlammableObject_SetBoxesOnFire_m0FAC7AFE84B99C27286CA85D9A132E90B0281F42 (void);
// 0x00000C4C System.Void Ignis.FlammableObject::SetMeshOnFire()
extern void FlammableObject_SetMeshOnFire_mF03C2122F3255F6ADABA698462B2DF0630045D3A (void);
// 0x00000C4D UnityEngine.Vector3 Ignis.FlammableObject::Clamp0360Vector(UnityEngine.Vector3)
extern void FlammableObject_Clamp0360Vector_m0A07E8258FE6DAB7B09C27ECB83F790786F87501 (void);
// 0x00000C4E System.Single Ignis.FlammableObject::Clamp0360(System.Single)
extern void FlammableObject_Clamp0360_mFC0609E277E664BBA3579FA0C8E8AA068EAC7955 (void);
// 0x00000C4F System.Boolean Ignis.FlammableObject::hasBurnedOut()
extern void FlammableObject_hasBurnedOut_m234E66CC1CAE01CAF4E59454A2A631DAFB095CF6 (void);
// 0x00000C50 System.Boolean Ignis.FlammableObject::IsExtinguished()
extern void FlammableObject_IsExtinguished_m5DC8F4CEAE06D50B2319565E694676EDF7D2AF62 (void);
// 0x00000C51 UnityEngine.Vector3 Ignis.FlammableObject::GetFireOrigin()
extern void FlammableObject_GetFireOrigin_m09A97E79B5F722AA946652E138414322DE6D6EF6 (void);
// 0x00000C52 System.Single Ignis.FlammableObject::GetPutOutRadius()
extern void FlammableObject_GetPutOutRadius_mE7B990B7BEF3CA14E21CFB87C4F47AB2DBF2DB46 (void);
// 0x00000C53 UnityEngine.Vector3 Ignis.FlammableObject::GetPutOutCenter()
extern void FlammableObject_GetPutOutCenter_m21606B2FBD804D2ED1E159EB2B0BCD44DC9C00EF (void);
// 0x00000C54 System.Single Ignis.FlammableObject::GetCurrentIgnitionProgress()
extern void FlammableObject_GetCurrentIgnitionProgress_m892956237F5EF0291B0CCE09398C569271D3A376 (void);
// 0x00000C55 System.Single Ignis.FlammableObject::GetObjectApproxSize()
extern void FlammableObject_GetObjectApproxSize_m000F4CF12F5D5D96EAD83CE40A3E462B9F84A465 (void);
// 0x00000C56 System.Void Ignis.FlammableObject::OnDrawGizmosSelected()
extern void FlammableObject_OnDrawGizmosSelected_mE5283B21685FD5AB0929FE3603D4FF268036BAC0 (void);
// 0x00000C57 System.Void Ignis.FlammableObject::OnDestroy()
extern void FlammableObject_OnDestroy_m71B01388C0CD8E3031220AAB70568FBB38A4D096 (void);
// 0x00000C58 System.Void Ignis.FlammableObject::.ctor()
extern void FlammableObject__ctor_m19AA98D4F8219F10A428EC7D212EA47674BC8E79 (void);
// 0x00000C59 System.Void Ignis.FlammableObject/<>c__DisplayClass93_0::.ctor()
extern void U3CU3Ec__DisplayClass93_0__ctor_mEA21044435E4031CFC46DACBEE1D64B22DDC532F (void);
// 0x00000C5A System.Boolean Ignis.FlammableObject/<>c__DisplayClass93_0::<SaveOriginalMaterialShaderProperties>b__0(Ignis.OAVAShaderCompatibilitySO)
extern void U3CU3Ec__DisplayClass93_0_U3CSaveOriginalMaterialShaderPropertiesU3Eb__0_mF956C54131A3050A5E7401F85F5E938BBA5CFA37 (void);
// 0x00000C5B System.Void Ignis.FlammableObject/<CountExtinguishTime>d__99::.ctor(System.Int32)
extern void U3CCountExtinguishTimeU3Ed__99__ctor_m4F83971CA9EB1399569059D5EB6702D6E34987D6 (void);
// 0x00000C5C System.Void Ignis.FlammableObject/<CountExtinguishTime>d__99::System.IDisposable.Dispose()
extern void U3CCountExtinguishTimeU3Ed__99_System_IDisposable_Dispose_mCD81AF4B8263D699430454DFD2B7B0A9BC8DECB5 (void);
// 0x00000C5D System.Boolean Ignis.FlammableObject/<CountExtinguishTime>d__99::MoveNext()
extern void U3CCountExtinguishTimeU3Ed__99_MoveNext_m702D4BDA4D99ACE25937D87B58BA7152A58EBF73 (void);
// 0x00000C5E System.Object Ignis.FlammableObject/<CountExtinguishTime>d__99::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCountExtinguishTimeU3Ed__99_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3FB046241BBBF34C5AFE8FC6A2122C3D44F4DC2 (void);
// 0x00000C5F System.Void Ignis.FlammableObject/<CountExtinguishTime>d__99::System.Collections.IEnumerator.Reset()
extern void U3CCountExtinguishTimeU3Ed__99_System_Collections_IEnumerator_Reset_m20C35A263EE2772A01687E0BB13AF22F49B63DD3 (void);
// 0x00000C60 System.Object Ignis.FlammableObject/<CountExtinguishTime>d__99::System.Collections.IEnumerator.get_Current()
extern void U3CCountExtinguishTimeU3Ed__99_System_Collections_IEnumerator_get_Current_m64793AB448EC8B3A4347CE70FC4C445A9BA4601B (void);
// 0x00000C61 System.Void Ignis.IInteractWithFire::OnCollisionWithFire(UnityEngine.GameObject)
// 0x00000C62 System.Void Ignis.ParticleExtinguish::Start()
extern void ParticleExtinguish_Start_m59DE92F510A2E7547DDDE47632A027A6D0B92E5C (void);
// 0x00000C63 System.Void Ignis.ParticleExtinguish::OnParticleCollision(UnityEngine.GameObject)
extern void ParticleExtinguish_OnParticleCollision_mF63191C5C36BE863771817DBB3852EB4EC75BEE2 (void);
// 0x00000C64 System.Void Ignis.ParticleExtinguish::.ctor()
extern void ParticleExtinguish__ctor_mED2BCD1315492224F7E7E3A810DEFA60F4522760 (void);
// 0x00000C65 System.Void Ignis.ParticleIgnite::Start()
extern void ParticleIgnite_Start_m0084E50FD65BB31D87BBA6F3B84F131D4E1229B3 (void);
// 0x00000C66 System.Void Ignis.ParticleIgnite::OnParticleCollision(UnityEngine.GameObject)
extern void ParticleIgnite_OnParticleCollision_mF8BC7A48F17F6A2C9483316126F21121DC493A1C (void);
// 0x00000C67 System.Collections.IEnumerator Ignis.ParticleIgnite::RemoveFromCollidedAlready(UnityEngine.GameObject)
extern void ParticleIgnite_RemoveFromCollidedAlready_m968F866681F10C528DF33C6D9C8260C294372F14 (void);
// 0x00000C68 System.Void Ignis.ParticleIgnite::.ctor()
extern void ParticleIgnite__ctor_m52214D921BD903D5BB9BDEF22538B0BE2ED1F617 (void);
// 0x00000C69 System.Void Ignis.ParticleIgnite/<RemoveFromCollidedAlready>d__6::.ctor(System.Int32)
extern void U3CRemoveFromCollidedAlreadyU3Ed__6__ctor_mFDD28DF1ECC09EA61E51D594DC28D8DF42E9D2F3 (void);
// 0x00000C6A System.Void Ignis.ParticleIgnite/<RemoveFromCollidedAlready>d__6::System.IDisposable.Dispose()
extern void U3CRemoveFromCollidedAlreadyU3Ed__6_System_IDisposable_Dispose_mE31A8FF4F0ACC0AFA00D055D269E0399BE2F0DAF (void);
// 0x00000C6B System.Boolean Ignis.ParticleIgnite/<RemoveFromCollidedAlready>d__6::MoveNext()
extern void U3CRemoveFromCollidedAlreadyU3Ed__6_MoveNext_mE7BD4427C78BB8664CBA314342BE5549655BA55B (void);
// 0x00000C6C System.Object Ignis.ParticleIgnite/<RemoveFromCollidedAlready>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRemoveFromCollidedAlreadyU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA64809CA13AFF7EB6703583ED7479A588D55896C (void);
// 0x00000C6D System.Void Ignis.ParticleIgnite/<RemoveFromCollidedAlready>d__6::System.Collections.IEnumerator.Reset()
extern void U3CRemoveFromCollidedAlreadyU3Ed__6_System_Collections_IEnumerator_Reset_mA87BDB84E77AEE03978D11B43AC960D5418A5A43 (void);
// 0x00000C6E System.Object Ignis.ParticleIgnite/<RemoveFromCollidedAlready>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CRemoveFromCollidedAlreadyU3Ed__6_System_Collections_IEnumerator_get_Current_m061736B34E6F21EDAC65B47E34F6B7E1306F357C (void);
// 0x00000C6F System.Void Ignis.RaycastExtinguish::Start()
extern void RaycastExtinguish_Start_m7B4FC13CC0CA04F43ADDF2AA3FBF6C8822E1BB6C (void);
// 0x00000C70 System.Void Ignis.RaycastExtinguish::CastRayCastExtinguish()
extern void RaycastExtinguish_CastRayCastExtinguish_m0204E6BA7DC82FF30A1E80981B942F2307BE95F1 (void);
// 0x00000C71 System.Void Ignis.RaycastExtinguish::OnDrawGizmos()
extern void RaycastExtinguish_OnDrawGizmos_m8523DCD9EC1007041D41C3D242D65C23BD9862DC (void);
// 0x00000C72 System.Void Ignis.RaycastExtinguish::.ctor()
extern void RaycastExtinguish__ctor_m9F60D3FDFD023FC0CF5D2A6FFB754CE083D759FF (void);
// 0x00000C73 System.Void Ignis.RaycastIgnite::Start()
extern void RaycastIgnite_Start_mA01C95A9938A2B7E171B784A38E691997BB69C3A (void);
// 0x00000C74 System.Void Ignis.RaycastIgnite::CastRayCastIgnite()
extern void RaycastIgnite_CastRayCastIgnite_mBA13CA9FF2F2FFCFB5E14B23F4B8C3FF3B2203D8 (void);
// 0x00000C75 System.Void Ignis.RaycastIgnite::OnDrawGizmos()
extern void RaycastIgnite_OnDrawGizmos_m3B20CFC26A8F117F13D32FC7AB8CDEE45F2AE2E7 (void);
// 0x00000C76 System.Void Ignis.RaycastIgnite::.ctor()
extern void RaycastIgnite__ctor_m9DEFA75DA71FE93D54053097AE41FC9F30432D56 (void);
// 0x00000C77 System.Void Ignis.SimpleInteractWithFire::OnCollisionWithFire(UnityEngine.GameObject)
extern void SimpleInteractWithFire_OnCollisionWithFire_mA0474A5EF2C55BB0C93365FD093781B0F8DAF8C1 (void);
// 0x00000C78 System.Void Ignis.SimpleInteractWithFire::.ctor()
extern void SimpleInteractWithFire__ctor_m1DEA5D394218C906CCEBBA2F5AB4B643D7AAA1F5 (void);
// 0x00000C79 System.Void Ignis.SphereExtinguish::Start()
extern void SphereExtinguish_Start_mA72ADA696F1334A4552CF11F0D89CAA8DEAF047F (void);
// 0x00000C7A System.Void Ignis.SphereExtinguish::SphereExtinguishCast()
extern void SphereExtinguish_SphereExtinguishCast_m3ECD64F5568A6165E4B991A206E3D49D6EB932D8 (void);
// 0x00000C7B System.Void Ignis.SphereExtinguish::OnDrawGizmos()
extern void SphereExtinguish_OnDrawGizmos_mCC133325FD07E391D1F428008B09D51255F4A425 (void);
// 0x00000C7C System.Void Ignis.SphereExtinguish::.ctor()
extern void SphereExtinguish__ctor_m61D7D03D4ABDFB2978FCFBFD21640753AF0D955D (void);
// 0x00000C7D System.Void Ignis.SphereIgnite::Start()
extern void SphereIgnite_Start_m14F1D8166457FEF9441E333B5E6D480E9549FE39 (void);
// 0x00000C7E System.Void Ignis.SphereIgnite::SphereIgniteCast()
extern void SphereIgnite_SphereIgniteCast_m1F89159B934161E66090917A5C95E048B9BD704E (void);
// 0x00000C7F System.Void Ignis.SphereIgnite::OnDrawGizmos()
extern void SphereIgnite_OnDrawGizmos_m12D675F95799C439B55698B6D28022171056EC71 (void);
// 0x00000C80 System.Void Ignis.SphereIgnite::.ctor()
extern void SphereIgnite__ctor_m5D7AF51286E6D2859050FD4BEA278F669100F164 (void);
// 0x00000C81 System.Void FMSocketIO.Ack::.ctor(System.Int32,System.Action`1<System.String>)
extern void Ack__ctor_mBF0834E0C661F166666626C9EC86CE21A2804B1D (void);
// 0x00000C82 System.Void FMSocketIO.Ack::Invoke(System.String)
extern void Ack_Invoke_m89254CC3600C1650B96AE76EFD3768063D77DFD7 (void);
// 0x00000C83 System.String FMSocketIO.Ack::ToString()
extern void Ack_ToString_m16FCC4670E1B9E865E24EECDA3E2AF86FD28D93E (void);
// 0x00000C84 System.String FMSocketIO.SocketIOEvent::get_name()
extern void SocketIOEvent_get_name_m272C6F0E0DEF3D858CEDB3CDF477998F04549424 (void);
// 0x00000C85 System.Void FMSocketIO.SocketIOEvent::set_name(System.String)
extern void SocketIOEvent_set_name_m8BDBF4BC266B74318C1A96E812A0BE9B459F0D9C (void);
// 0x00000C86 System.String FMSocketIO.SocketIOEvent::get_data()
extern void SocketIOEvent_get_data_m9CB85C9ACC0C2E158E9A5CCFFFEB8BC29FC8CA3F (void);
// 0x00000C87 System.Void FMSocketIO.SocketIOEvent::set_data(System.String)
extern void SocketIOEvent_set_data_m839254B4D3783F185A6E3C8C14EAE51E8D0DC37E (void);
// 0x00000C88 System.Void FMSocketIO.SocketIOEvent::.ctor(System.String)
extern void SocketIOEvent__ctor_mE6C1131BC31A5D69E2119E7531D6DF78D2CAA885 (void);
// 0x00000C89 System.Void FMSocketIO.SocketIOEvent::.ctor(System.String,System.String)
extern void SocketIOEvent__ctor_m7E06764CA0D91050034BB07C9BFD70FD174121F0 (void);
// 0x00000C8A FMSocketIO.Packet FMSocketIO.Decoder::Decode(WebSocketSharp.MessageEventArgs)
extern void Decoder_Decode_mDFEB1B2336FAC59EA8DB38524052CF14C05A7B6D (void);
// 0x00000C8B System.Void FMSocketIO.Decoder::.ctor()
extern void Decoder__ctor_m19A6D5F428AED1E51DBF942839A255389890EE7D (void);
// 0x00000C8C System.String FMSocketIO.Encoder::Encode(FMSocketIO.Packet)
extern void Encoder_Encode_m4DA14E756547AD1F2E0E2C36875A77F8B9193E03 (void);
// 0x00000C8D System.Void FMSocketIO.Encoder::.ctor()
extern void Encoder__ctor_m729ABCFE68F9DBADE61E68466382CF4BCD97B38B (void);
// 0x00000C8E FMSocketIO.SocketIOEvent FMSocketIO.Parser::Parse(System.String)
extern void Parser_Parse_m667792F77B19DBDC740C8B137DD0B067FA81A0A0 (void);
// 0x00000C8F System.String FMSocketIO.Parser::ParseData(System.String)
extern void Parser_ParseData_mA7F9344DD12AF4AFCF42147B34FF6B8B36806A90 (void);
// 0x00000C90 System.Void FMSocketIO.Parser::.ctor()
extern void Parser__ctor_mB2E752671FC0E2DF3DB9A45E5E1D6A3F086D722F (void);
// 0x00000C91 System.Void FMSocketIO.SocketOpenData::.ctor()
extern void SocketOpenData__ctor_mA8D183CCF76745B81A81ABA0E588F3BF5E0F005F (void);
// 0x00000C92 WebSocketSharp.WebSocket FMSocketIO.SocketIOComponent::get_socket()
extern void SocketIOComponent_get_socket_m367C734B9ACBA229C724C1D4240E555D4664F169 (void);
// 0x00000C93 System.String FMSocketIO.SocketIOComponent::get_sid()
extern void SocketIOComponent_get_sid_mA875D0C9A2D45CC4B14CE73997AF1867D43F09B7 (void);
// 0x00000C94 System.Void FMSocketIO.SocketIOComponent::set_sid(System.String)
extern void SocketIOComponent_set_sid_m42AAE7199C4F652EB567C24FFB5C65B13281D911 (void);
// 0x00000C95 System.Boolean FMSocketIO.SocketIOComponent::get_IsConnected()
extern void SocketIOComponent_get_IsConnected_m1D1C2E6B5A577639AE5EBA14B39E67BB2D644264 (void);
// 0x00000C96 System.Boolean FMSocketIO.SocketIOComponent::IsWebSocketConnected()
extern void SocketIOComponent_IsWebSocketConnected_mF53DB7E4707B517C639F4C2C74D01F2C7C9F1A98 (void);
// 0x00000C97 System.Boolean FMSocketIO.SocketIOComponent::IsWebSocketConnected(WebSocketSharp.WebSocket)
extern void SocketIOComponent_IsWebSocketConnected_mD65E312F201968123CA302BFB556828518C174F3 (void);
// 0x00000C98 System.Void FMSocketIO.SocketIOComponent::DebugLog(System.String)
extern void SocketIOComponent_DebugLog_m3063F4764FD156A7590B4C89BF5E3D8814492185 (void);
// 0x00000C99 System.Void FMSocketIO.SocketIOComponent::Awake()
extern void SocketIOComponent_Awake_mBFDCEE46A46027FD2FC07956389711158B55240C (void);
// 0x00000C9A System.Void FMSocketIO.SocketIOComponent::Init()
extern void SocketIOComponent_Init_m40A774ED935DEA19B992F158A5335EA78016497C (void);
// 0x00000C9B System.Void FMSocketIO.SocketIOComponent::Update()
extern void SocketIOComponent_Update_mDE380B53C01AB137E140ED9A7430663E43B08758 (void);
// 0x00000C9C System.Void FMSocketIO.SocketIOComponent::OnDestroy()
extern void SocketIOComponent_OnDestroy_m627F9AEAB5EB8A9B585601EB511BA942A66E91A6 (void);
// 0x00000C9D System.Void FMSocketIO.SocketIOComponent::OnApplicationQuit()
extern void SocketIOComponent_OnApplicationQuit_m58B848E579E407B8A3389D8A48EE6FE9C25B9BFA (void);
// 0x00000C9E System.Void FMSocketIO.SocketIOComponent::Connect()
extern void SocketIOComponent_Connect_mE2788920BFE2928F200A21D5DE1D8CCB5A1DB4D6 (void);
// 0x00000C9F System.Void FMSocketIO.SocketIOComponent::Close()
extern void SocketIOComponent_Close_m8A6924B82A28E90D42E019B2F8290C47EA673363 (void);
// 0x00000CA0 System.Void FMSocketIO.SocketIOComponent::On(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void SocketIOComponent_On_m0AB7A1F85759D816361F14F410E7D4AA756138B3 (void);
// 0x00000CA1 System.Void FMSocketIO.SocketIOComponent::Off(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void SocketIOComponent_Off_m9776C7EFA2162ABCE0C193D653D1E98653BBFECC (void);
// 0x00000CA2 System.Void FMSocketIO.SocketIOComponent::EmitRaw(System.String)
extern void SocketIOComponent_EmitRaw_mD9D7EED787262C0B154EEE20C8DD6DDF894CE74F (void);
// 0x00000CA3 System.Void FMSocketIO.SocketIOComponent::Emit(System.String)
extern void SocketIOComponent_Emit_m64685E9C78955DBE3A5B27EC3CDDC56A5CF2BC81 (void);
// 0x00000CA4 System.Void FMSocketIO.SocketIOComponent::Emit(System.String,System.Action`1<System.String>)
extern void SocketIOComponent_Emit_mE4FD86E57B3D1830CEDA8A73D7C2842A8BF2D5CC (void);
// 0x00000CA5 System.Void FMSocketIO.SocketIOComponent::Emit(System.String,System.String)
extern void SocketIOComponent_Emit_m8852CB355765564C0D81BE95B1AAA735B4AD24BD (void);
// 0x00000CA6 System.Void FMSocketIO.SocketIOComponent::Emit(System.String,System.String,System.Action`1<System.String>)
extern void SocketIOComponent_Emit_m63D8F481FE027021B0D6E8D2360F8EF134409023 (void);
// 0x00000CA7 System.Void FMSocketIO.SocketIOComponent::RunSocketThread(System.Object)
extern void SocketIOComponent_RunSocketThread_mC53A7E22576A749CD789D185123CC4E499C91133 (void);
// 0x00000CA8 System.Void FMSocketIO.SocketIOComponent::RunPingThread(System.Object)
extern void SocketIOComponent_RunPingThread_m621A5F071F04044F5FA0381C80179B4AA4384F42 (void);
// 0x00000CA9 System.Void FMSocketIO.SocketIOComponent::EmitMessage(System.Int32,System.String)
extern void SocketIOComponent_EmitMessage_m7D0DE2FA418CD31701E031B36B366CD5D60E0242 (void);
// 0x00000CAA System.Void FMSocketIO.SocketIOComponent::EmitClose()
extern void SocketIOComponent_EmitClose_m4DCAAABD40D2D184AD072D06B39D1BB37D0300E6 (void);
// 0x00000CAB System.Void FMSocketIO.SocketIOComponent::EmitPacket(FMSocketIO.Packet)
extern void SocketIOComponent_EmitPacket_m175B3D4FAF69CE1D1ABC33A4B5836ECF99507070 (void);
// 0x00000CAC System.Void FMSocketIO.SocketIOComponent::OnOpen(System.Object,System.EventArgs)
extern void SocketIOComponent_OnOpen_mC64D6102C5EE4A8CDDD197E35670E7D2F95A473A (void);
// 0x00000CAD System.Void FMSocketIO.SocketIOComponent::OnMessage(System.Object,WebSocketSharp.MessageEventArgs)
extern void SocketIOComponent_OnMessage_mD0677197828A697F1EBB8405B050C907389B7AF8 (void);
// 0x00000CAE System.Void FMSocketIO.SocketIOComponent::HandleOpen(FMSocketIO.Packet)
extern void SocketIOComponent_HandleOpen_m1512658D684F88914E03B6F050CBB7BF6F12A049 (void);
// 0x00000CAF System.Void FMSocketIO.SocketIOComponent::HandlePing()
extern void SocketIOComponent_HandlePing_mF9FA61F30A71E763E6D39089D1AB5E1B665BA9C7 (void);
// 0x00000CB0 System.Void FMSocketIO.SocketIOComponent::HandlePong()
extern void SocketIOComponent_HandlePong_m4DD49D832A4593DC45048CA37AB5969F0A451516 (void);
// 0x00000CB1 System.Void FMSocketIO.SocketIOComponent::HandleMessage(FMSocketIO.Packet)
extern void SocketIOComponent_HandleMessage_mF486AF4664986DF46695243106CC89C6ACB07649 (void);
// 0x00000CB2 System.Void FMSocketIO.SocketIOComponent::OnError(System.Object,WebSocketSharp.ErrorEventArgs)
extern void SocketIOComponent_OnError_mAAF0EFDB490349F96531F8C8AA48AE59481B9B62 (void);
// 0x00000CB3 System.Void FMSocketIO.SocketIOComponent::OnClose(System.Object,WebSocketSharp.CloseEventArgs)
extern void SocketIOComponent_OnClose_m3039A4D69424B5B5233B363AC66B85E94030D415 (void);
// 0x00000CB4 System.Void FMSocketIO.SocketIOComponent::EmitEvent(System.String)
extern void SocketIOComponent_EmitEvent_m0DF7622C4CCAD145F8111E6143BEECBF75B3F9A1 (void);
// 0x00000CB5 System.Void FMSocketIO.SocketIOComponent::EmitEvent(FMSocketIO.SocketIOEvent)
extern void SocketIOComponent_EmitEvent_mA5885DC30396687139226ACC9DE555A8FB801486 (void);
// 0x00000CB6 System.Void FMSocketIO.SocketIOComponent::InvokeAck(FMSocketIO.Packet)
extern void SocketIOComponent_InvokeAck_m2020DB72159487F841D333B689FDE3EB69E58CC1 (void);
// 0x00000CB7 System.Void FMSocketIO.SocketIOComponent::.ctor()
extern void SocketIOComponent__ctor_m80FAF8C0660DFBA10028C06CB4E975A10840EE2A (void);
// 0x00000CB8 System.Void FMSocketIO.SocketIOException::.ctor()
extern void SocketIOException__ctor_m81A53CF1903AABC1E6DC951EF776851F32D2B489 (void);
// 0x00000CB9 System.Void FMSocketIO.SocketIOException::.ctor(System.String)
extern void SocketIOException__ctor_mF497A66832B0596F89128D0026A5A13F800ED8D3 (void);
// 0x00000CBA System.Void FMSocketIO.SocketIOException::.ctor(System.String,System.Exception)
extern void SocketIOException__ctor_m10C3C06C82077E5134F4122D3A2C3362D67B9EC0 (void);
// 0x00000CBB System.Void FMSocketIO.Packet::.ctor()
extern void Packet__ctor_m9A00FE88F1B12E9F00DD47DE6A0900366FF28D05 (void);
// 0x00000CBC System.Void FMSocketIO.Packet::.ctor(FMSocketIO.EnginePacketType)
extern void Packet__ctor_mF24B9D858C39BA5D2CB4608FC9F57A7623244742 (void);
// 0x00000CBD System.Void FMSocketIO.Packet::.ctor(FMSocketIO.EnginePacketType,FMSocketIO.SocketPacketType,System.Int32,System.String,System.Int32,System.String)
extern void Packet__ctor_m2850E29D47F0A751A7EAD52D73AEA6DE605D3644 (void);
// 0x00000CBE System.Void CurvedUI.CUI_AnimateCurvedFillOnStart::Update()
extern void CUI_AnimateCurvedFillOnStart_Update_m08AA0297F31CC864744503C421800174399595DD (void);
// 0x00000CBF System.Void CurvedUI.CUI_AnimateCurvedFillOnStart::.ctor()
extern void CUI_AnimateCurvedFillOnStart__ctor_mAC391C896C24CEEBC43FFF03C068D80925C7FAFA (void);
// 0x00000CC0 System.Void CurvedUI.CUI_CameraController::Awake()
extern void CUI_CameraController_Awake_m5DD0FB6DF39DD842FE5F97D53DFAE3DAEA66BA15 (void);
// 0x00000CC1 System.Void CurvedUI.CUI_CameraController::.ctor()
extern void CUI_CameraController__ctor_mFC4F6F33C6C40D621C936C7FDDE8B2365667130B (void);
// 0x00000CC2 System.Void CurvedUI.CUI_ChangeColor::ChangeColorToBlue()
extern void CUI_ChangeColor_ChangeColorToBlue_m990E7CDD451A8EA703A45D9D27E0413309FD6FD5 (void);
// 0x00000CC3 System.Void CurvedUI.CUI_ChangeColor::ChangeColorToCyan()
extern void CUI_ChangeColor_ChangeColorToCyan_m61C429462F1F1B95CD5D8CAFB8B4CA6E5ED2CDED (void);
// 0x00000CC4 System.Void CurvedUI.CUI_ChangeColor::ChangeColorToWhite()
extern void CUI_ChangeColor_ChangeColorToWhite_m4BE3FCFA006656B69E2B9193762311DF214C439A (void);
// 0x00000CC5 System.Void CurvedUI.CUI_ChangeColor::.ctor()
extern void CUI_ChangeColor__ctor_m43ED965886F679B7209A689AFE501087262F8DA0 (void);
// 0x00000CC6 System.Void CurvedUI.CUI_ChangeValueOnHold::Update()
extern void CUI_ChangeValueOnHold_Update_mDC750B878125B8DC7892BC2D7CBF1A81E6C3CD9B (void);
// 0x00000CC7 System.Void CurvedUI.CUI_ChangeValueOnHold::ChangeVal()
extern void CUI_ChangeValueOnHold_ChangeVal_mE87124B16F2997632DED6C5DDC46A4B2158EEB2D (void);
// 0x00000CC8 System.Void CurvedUI.CUI_ChangeValueOnHold::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void CUI_ChangeValueOnHold_OnPointerEnter_mEFA0A76F2F750F0692A7834605604022AD55A888 (void);
// 0x00000CC9 System.Void CurvedUI.CUI_ChangeValueOnHold::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void CUI_ChangeValueOnHold_OnPointerExit_m1EE64AB5303BEEB47F68E2086CA9FCB6BE09A6F7 (void);
// 0x00000CCA System.Void CurvedUI.CUI_ChangeValueOnHold::.ctor()
extern void CUI_ChangeValueOnHold__ctor_mE077C29B46DAE7C33DA52BE53388088DCEDCFB58 (void);
// 0x00000CCB System.Void CurvedUI.CUI_DragBetweenCanvases::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void CUI_DragBetweenCanvases_OnBeginDrag_m1289AE15330551287F1C34261453B5D7FB42B1C7 (void);
// 0x00000CCC System.Void CurvedUI.CUI_DragBetweenCanvases::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void CUI_DragBetweenCanvases_OnDrag_m0BF8FF5F781972B5F024EF6013E1758F89AC5E35 (void);
// 0x00000CCD System.Void CurvedUI.CUI_DragBetweenCanvases::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void CUI_DragBetweenCanvases_OnEndDrag_m2F5193266203F6706CA6C914AB5A4245E347EA0A (void);
// 0x00000CCE System.Void CurvedUI.CUI_DragBetweenCanvases::RaycastPosition(UnityEngine.Vector2&)
extern void CUI_DragBetweenCanvases_RaycastPosition_mDA21C9D23250E1E9409E3B297E9B0C019A77749D (void);
// 0x00000CCF System.Void CurvedUI.CUI_DragBetweenCanvases::.ctor()
extern void CUI_DragBetweenCanvases__ctor_m14E2E548261AD01450A25955D268ABD2490A10A0 (void);
// 0x00000CD0 System.Void CurvedUI.CUI_GunController::Update()
extern void CUI_GunController_Update_m36E60BDB2F0D7EE14D26341EFD3D836FA626BEF0 (void);
// 0x00000CD1 System.Void CurvedUI.CUI_GunController::.ctor()
extern void CUI_GunController__ctor_m138EF56C55DB13887EBAF19D7F413D2EF2C6BC91 (void);
// 0x00000CD2 System.Void CurvedUI.CUI_GunController/<>c::.cctor()
extern void U3CU3Ec__cctor_mBEA2F417A0C3CD57FD8DE25221561DECE3BA4A82 (void);
// 0x00000CD3 System.Void CurvedUI.CUI_GunController/<>c::.ctor()
extern void U3CU3Ec__ctor_mCC2AE7B34B6FE1D5236FA2A09B19D187DA9A7E78 (void);
// 0x00000CD4 System.Boolean CurvedUI.CUI_GunController/<>c::<Update>b__2_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3CUpdateU3Eb__2_0_m6D7CEA45743E007494D891DD7FA27FDE3C7A0594 (void);
// 0x00000CD5 System.Void CurvedUI.CUI_GunMovement::Start()
extern void CUI_GunMovement_Start_m1F5A96BA9E13BA21599CE013B956B3F55120B0D1 (void);
// 0x00000CD6 System.Void CurvedUI.CUI_GunMovement::Update()
extern void CUI_GunMovement_Update_m64C72287EE449B6F1C5BB323755043C99CBB115B (void);
// 0x00000CD7 System.Void CurvedUI.CUI_GunMovement::.ctor()
extern void CUI_GunMovement__ctor_mCFE763C95F72212AA989ECCC00789C5D583F5446 (void);
// 0x00000CD8 System.Void CurvedUI.CUI_OrientOnCurvedSpace::Awake()
extern void CUI_OrientOnCurvedSpace_Awake_m4F777FAE1C0B7F3BEE5450542F034E26774B07FC (void);
// 0x00000CD9 System.Void CurvedUI.CUI_OrientOnCurvedSpace::Update()
extern void CUI_OrientOnCurvedSpace_Update_m331D55EB981ED973C38BDA30DF1BEE8A925E3F05 (void);
// 0x00000CDA System.Void CurvedUI.CUI_OrientOnCurvedSpace::.ctor()
extern void CUI_OrientOnCurvedSpace__ctor_mF7488BACB1FED15FC6A8185061FFE0FCCFAADFF3 (void);
// 0x00000CDB System.Void CurvedUI.CUI_PerlinNoisePosition::Start()
extern void CUI_PerlinNoisePosition_Start_mED61FFCB2F81200D3D5282D08D331C4F6B19C28A (void);
// 0x00000CDC System.Void CurvedUI.CUI_PerlinNoisePosition::Update()
extern void CUI_PerlinNoisePosition_Update_m28D8742BEEAE9B82062FB171EC4E54A4F7CCABE1 (void);
// 0x00000CDD System.Void CurvedUI.CUI_PerlinNoisePosition::.ctor()
extern void CUI_PerlinNoisePosition__ctor_m30164467BDB01269D7F0B7CD3607ACFB3CBB24FD (void);
// 0x00000CDE System.Void CurvedUI.CUI_PerlinNoiseRotation::Start()
extern void CUI_PerlinNoiseRotation_Start_m6B033CBA1B875C9B1D83B6E75FD0EE6FA4C9AE68 (void);
// 0x00000CDF System.Void CurvedUI.CUI_PerlinNoiseRotation::Update()
extern void CUI_PerlinNoiseRotation_Update_m2D8C8B962425E685B5DBAD2FFDCADC0CC79BA362 (void);
// 0x00000CE0 System.Void CurvedUI.CUI_PerlinNoiseRotation::.ctor()
extern void CUI_PerlinNoiseRotation__ctor_mE75B964A1634FB4179FA17FC536FAE9E5FEE7F7E (void);
// 0x00000CE1 System.Void CurvedUI.CUI_PickImageFromSet::PickThis()
extern void CUI_PickImageFromSet_PickThis_mF039517CAC67B443A8A82B4038CD7FEDAC907973 (void);
// 0x00000CE2 System.Void CurvedUI.CUI_PickImageFromSet::.ctor()
extern void CUI_PickImageFromSet__ctor_m133C98C8E9926644CC433240609F460FBDD9FA95 (void);
// 0x00000CE3 System.Void CurvedUI.CUI_PickImageFromSet::.cctor()
extern void CUI_PickImageFromSet__cctor_m3543BFCF3C1A2EEA62783DD276822FB614B66624 (void);
// 0x00000CE4 System.Void CurvedUI.CUI_RaycastToCanvas::Start()
extern void CUI_RaycastToCanvas_Start_mA1FD0F71147F660A151EB14CD885DE707AAE3D60 (void);
// 0x00000CE5 System.Void CurvedUI.CUI_RaycastToCanvas::Update()
extern void CUI_RaycastToCanvas_Update_mC4E15CAA713DDB0C609E1678F64AA5A2645B730F (void);
// 0x00000CE6 System.Void CurvedUI.CUI_RaycastToCanvas::.ctor()
extern void CUI_RaycastToCanvas__ctor_mD9D224C47F32D9096B2FCE21B1F5EBB55F2B568B (void);
// 0x00000CE7 System.Void CurvedUI.CUI_RiseChildrenOverTime::Start()
extern void CUI_RiseChildrenOverTime_Start_m86A7C7A590E268885769EBD1E9F7F7B20E502E9D (void);
// 0x00000CE8 System.Void CurvedUI.CUI_RiseChildrenOverTime::Update()
extern void CUI_RiseChildrenOverTime_Update_m0E0C3F589ADFEC43E00DAC36C340DE1EB1281773 (void);
// 0x00000CE9 System.Void CurvedUI.CUI_RiseChildrenOverTime::.ctor()
extern void CUI_RiseChildrenOverTime__ctor_m73D8A39EC92DBAB8546FB45B52A90AD5ABB9E0D7 (void);
// 0x00000CEA System.Void CurvedUI.CUI_TMPChecker::Start()
extern void CUI_TMPChecker_Start_mF5D746C1E08CFE2A2462266B61703B7B2B98992F (void);
// 0x00000CEB System.Void CurvedUI.CUI_TMPChecker::.ctor()
extern void CUI_TMPChecker__ctor_m1ECD982CA173EC821ED5FD8EE9EC013DE0F99698 (void);
// 0x00000CEC System.Void CurvedUI.CUI_ViveButtonState::.ctor()
extern void CUI_ViveButtonState__ctor_mDF5D552AE93D653B7ED7338FC5CB54F845FBEDB0 (void);
// 0x00000CED System.Void CurvedUI.CUI_ViveHapticPulse::Start()
extern void CUI_ViveHapticPulse_Start_m38DFDBFDF1DFBB7552ACF5867F884049052F147E (void);
// 0x00000CEE System.Void CurvedUI.CUI_ViveHapticPulse::SetPulseStrength(System.Single)
extern void CUI_ViveHapticPulse_SetPulseStrength_mBE06B9AAC0C9FEB505DF3164AEABB96D323DCDE0 (void);
// 0x00000CEF System.Void CurvedUI.CUI_ViveHapticPulse::TriggerPulse()
extern void CUI_ViveHapticPulse_TriggerPulse_m9F802EDFED42CC8A58FB9E73030FA5A5E5286C2F (void);
// 0x00000CF0 System.Void CurvedUI.CUI_ViveHapticPulse::.ctor()
extern void CUI_ViveHapticPulse__ctor_mEAA39CCB1A0D73FDF6707BB9C2CA5A9C6FE9B57E (void);
// 0x00000CF1 System.Void CurvedUI.CUI_WorldSpaceCursorFollow::Start()
extern void CUI_WorldSpaceCursorFollow_Start_mA7D5D85E4DDC9611B2CD88D11421B3D96151B347 (void);
// 0x00000CF2 System.Void CurvedUI.CUI_WorldSpaceCursorFollow::Update()
extern void CUI_WorldSpaceCursorFollow_Update_m79D6751B0C7D605B1536E943B950F0CA9CCC8F6D (void);
// 0x00000CF3 System.Void CurvedUI.CUI_WorldSpaceCursorFollow::.ctor()
extern void CUI_WorldSpaceCursorFollow__ctor_m2D2DCC066E501FE413B0A9F98DC149B063793433 (void);
// 0x00000CF4 System.Void CurvedUI.CUI_WorldSpaceMouseMultipleCanvases::Update()
extern void CUI_WorldSpaceMouseMultipleCanvases_Update_mCA4C8A5D3D8D9AE46FD6C4DC92CD2DBBB06A5350 (void);
// 0x00000CF5 System.Void CurvedUI.CUI_WorldSpaceMouseMultipleCanvases::.ctor()
extern void CUI_WorldSpaceMouseMultipleCanvases__ctor_m90AFE943803DBB76DACF6A8944C85622B50A8133 (void);
// 0x00000CF6 System.Void CurvedUI.CUI_draggable::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void CUI_draggable_OnBeginDrag_m99EF0FF2C391569B16201090A58274797FBFDA4E (void);
// 0x00000CF7 System.Void CurvedUI.CUI_draggable::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void CUI_draggable_OnDrag_m9BB0E276CE35CED133007835C3DCFC803AC4B824 (void);
// 0x00000CF8 System.Void CurvedUI.CUI_draggable::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void CUI_draggable_OnEndDrag_m2A370C66EB011D24669A944F5DD306CBDE30E126 (void);
// 0x00000CF9 System.Void CurvedUI.CUI_draggable::LateUpdate()
extern void CUI_draggable_LateUpdate_mA3956CF0D0018C63E9C48E6B56365FBFB7F17DDB (void);
// 0x00000CFA System.Void CurvedUI.CUI_draggable::RaycastPosition(UnityEngine.Vector2&)
extern void CUI_draggable_RaycastPosition_m08D4CB319883C3E692116DCB0F8901E78DDBC2DE (void);
// 0x00000CFB System.Void CurvedUI.CUI_draggable::.ctor()
extern void CUI_draggable__ctor_mC2EBF640917826503F7B9B9F173F51240A63121B (void);
// 0x00000CFC System.Void CurvedUI.CUI_inventory_paralax::Start()
extern void CUI_inventory_paralax_Start_m3CC700D87DEBCD19CC0BE97E4A6BE11877D50BEC (void);
// 0x00000CFD System.Void CurvedUI.CUI_inventory_paralax::Update()
extern void CUI_inventory_paralax_Update_m5AAA9CBB78D1F6B34A5C916BEEEA0236A2F9D8CE (void);
// 0x00000CFE System.Void CurvedUI.CUI_inventory_paralax::.ctor()
extern void CUI_inventory_paralax__ctor_m869020A7B07AE6E7E6993FC22049332BD07E9583 (void);
// 0x00000CFF System.Void CurvedUI.CurvedUIEventSystem::Awake()
extern void CurvedUIEventSystem_Awake_mFC9A77154DAB17EC7E0A84FCB511067E3DBFA09C (void);
// 0x00000D00 System.Void CurvedUI.CurvedUIEventSystem::OnApplicationFocus(System.Boolean)
extern void CurvedUIEventSystem_OnApplicationFocus_mECAD0CE88C8C50B820D8DBE5609108F466DAF8D5 (void);
// 0x00000D01 System.Void CurvedUI.CurvedUIEventSystem::.ctor()
extern void CurvedUIEventSystem__ctor_m516C0FF4FECFDA5B335CA49F92F05A599D46893F (void);
// 0x00000D02 System.Boolean CurvedUI.CurvedUIExtensionMethods::AlmostEqual(UnityEngine.Vector3,UnityEngine.Vector3,System.Double)
extern void CurvedUIExtensionMethods_AlmostEqual_m06D0CC3D545F8E2F991AF6C505B87F9E3BD84D12 (void);
// 0x00000D03 System.Single CurvedUI.CurvedUIExtensionMethods::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void CurvedUIExtensionMethods_Remap_mAEAC70B7C6CEFBC52700BF24A5E2B2D6CD28B658 (void);
// 0x00000D04 System.Single CurvedUI.CurvedUIExtensionMethods::RemapAndClamp(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void CurvedUIExtensionMethods_RemapAndClamp_m1187F4DD5070953163F8A7BBFAC08686430E4703 (void);
// 0x00000D05 System.Single CurvedUI.CurvedUIExtensionMethods::Remap(System.Int32,System.Single,System.Single,System.Single,System.Single)
extern void CurvedUIExtensionMethods_Remap_mB5DFE445630877414F1B509AC16816C261624397 (void);
// 0x00000D06 System.Double CurvedUI.CurvedUIExtensionMethods::Remap(System.Double,System.Double,System.Double,System.Double,System.Double)
extern void CurvedUIExtensionMethods_Remap_m4560F0F56FD7C1BBF4C19CCDBAD340F86F6BE2F2 (void);
// 0x00000D07 System.Single CurvedUI.CurvedUIExtensionMethods::Clamp(System.Single,System.Single,System.Single)
extern void CurvedUIExtensionMethods_Clamp_m473B57BE855F895390DA6586C4160A552BDACF2F (void);
// 0x00000D08 System.Single CurvedUI.CurvedUIExtensionMethods::Clamp(System.Int32,System.Int32,System.Int32)
extern void CurvedUIExtensionMethods_Clamp_m4A60B368AFAD3F893EFAE3EA80A0A434E8639278 (void);
// 0x00000D09 System.Int32 CurvedUI.CurvedUIExtensionMethods::Abs(System.Int32)
extern void CurvedUIExtensionMethods_Abs_m90B46839FD8F89DC2A4352A372F427411FBA4546 (void);
// 0x00000D0A System.Single CurvedUI.CurvedUIExtensionMethods::Abs(System.Single)
extern void CurvedUIExtensionMethods_Abs_m4BF5805D5915CEAA1BA364FCA10CC8973B1EFD6C (void);
// 0x00000D0B System.Int32 CurvedUI.CurvedUIExtensionMethods::ToInt(System.Single)
extern void CurvedUIExtensionMethods_ToInt_m6547A81D340623ABBB687DCD8E264D390E288B8F (void);
// 0x00000D0C System.Int32 CurvedUI.CurvedUIExtensionMethods::FloorToInt(System.Single)
extern void CurvedUIExtensionMethods_FloorToInt_mA2B4F18C3554AE79045B0881C2914AF8C587370F (void);
// 0x00000D0D System.Int32 CurvedUI.CurvedUIExtensionMethods::CeilToInt(System.Single)
extern void CurvedUIExtensionMethods_CeilToInt_m2685A18F5EAB3E9B86A1DD2740D5D9E74ADDEE88 (void);
// 0x00000D0E UnityEngine.Vector3 CurvedUI.CurvedUIExtensionMethods::ModifyX(UnityEngine.Vector3,System.Single)
extern void CurvedUIExtensionMethods_ModifyX_mC831B7C37D9752B8E96BE55600586CCAE8C17C06 (void);
// 0x00000D0F UnityEngine.Vector3 CurvedUI.CurvedUIExtensionMethods::ModifyY(UnityEngine.Vector3,System.Single)
extern void CurvedUIExtensionMethods_ModifyY_m0E892B4897F1156F337C8C736A3F374651AD3506 (void);
// 0x00000D10 UnityEngine.Vector3 CurvedUI.CurvedUIExtensionMethods::ModifyZ(UnityEngine.Vector3,System.Single)
extern void CurvedUIExtensionMethods_ModifyZ_m20137F0418097EBB0B00ECAB7828AA64B4551F7B (void);
// 0x00000D11 UnityEngine.Vector2 CurvedUI.CurvedUIExtensionMethods::ModifyVectorX(UnityEngine.Vector2,System.Single)
extern void CurvedUIExtensionMethods_ModifyVectorX_m352F2CFE5094BAC31B1D6D627CED77F1A94AFEDA (void);
// 0x00000D12 UnityEngine.Vector2 CurvedUI.CurvedUIExtensionMethods::ModifyVectorY(UnityEngine.Vector2,System.Single)
extern void CurvedUIExtensionMethods_ModifyVectorY_mA4E4BE861A802E16B5E6990B31F92DC646EFA6F8 (void);
// 0x00000D13 System.Void CurvedUI.CurvedUIExtensionMethods::ResetTransform(UnityEngine.Transform)
extern void CurvedUIExtensionMethods_ResetTransform_m0608AF599B60590872F013DCBC9D797B9A48933F (void);
// 0x00000D14 T CurvedUI.CurvedUIExtensionMethods::AddComponentIfMissing(UnityEngine.GameObject)
// 0x00000D15 T CurvedUI.CurvedUIExtensionMethods::AddComponentIfMissing(UnityEngine.Component)
// 0x00000D16 System.Void CurvedUI.CurvedUIHandSwitcher::SwitchHandTo(CurvedUIInputModule/Hand)
extern void CurvedUIHandSwitcher_SwitchHandTo_m06020E832646BA1137A4A9B6CA70F195C8300AA1 (void);
// 0x00000D17 System.Void CurvedUI.CurvedUIHandSwitcher::.ctor()
extern void CurvedUIHandSwitcher__ctor_m38A284E5E6CD80B19AF76CDAD4BA09DB119AA4BB (void);
// 0x00000D18 System.Void CurvedUI.CurvedUIInputFieldCaret::Awake()
extern void CurvedUIInputFieldCaret_Awake_m60173B3FA374FB03FAEB7B13AF89CD6218BD0747 (void);
// 0x00000D19 System.Void CurvedUI.CurvedUIInputFieldCaret::Update()
extern void CurvedUIInputFieldCaret_Update_m8077F0C95EE6DD0CFF895BB12CA4D45E79CEDA3F (void);
// 0x00000D1A System.Void CurvedUI.CurvedUIInputFieldCaret::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void CurvedUIInputFieldCaret_OnSelect_m58BDC6053D24E037AB3344EC99A59B4C22462272 (void);
// 0x00000D1B System.Void CurvedUI.CurvedUIInputFieldCaret::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern void CurvedUIInputFieldCaret_OnDeselect_mA110C3E453C57E54F1C43F74AEE39F66DF0E327F (void);
// 0x00000D1C System.Collections.IEnumerator CurvedUI.CurvedUIInputFieldCaret::CaretBlinker()
extern void CurvedUIInputFieldCaret_CaretBlinker_mDFE5E6834175B6A6427649AA2B4A9C5C2D6A21FD (void);
// 0x00000D1D System.Void CurvedUI.CurvedUIInputFieldCaret::CreateCaret()
extern void CurvedUIInputFieldCaret_CreateCaret_m10BBEEC4F7A370B81FD4F891A044CFA3910F85C9 (void);
// 0x00000D1E System.Void CurvedUI.CurvedUIInputFieldCaret::UpdateCaret()
extern void CurvedUIInputFieldCaret_UpdateCaret_mA9F8F426EC3520D114B47566B1E9514E9AB8FDF2 (void);
// 0x00000D1F UnityEngine.Vector2 CurvedUI.CurvedUIInputFieldCaret::GetLocalPositionInText(System.Int32)
extern void CurvedUIInputFieldCaret_GetLocalPositionInText_mAB42F91F0D6A0DAAEC400A8CC517B1D0136B0D2A (void);
// 0x00000D20 UnityEngine.Color CurvedUI.CurvedUIInputFieldCaret::get_CaretColor()
extern void CurvedUIInputFieldCaret_get_CaretColor_mC87E54D95B1E5839203366268B0A0DD72A9103E6 (void);
// 0x00000D21 System.Void CurvedUI.CurvedUIInputFieldCaret::set_CaretColor(UnityEngine.Color)
extern void CurvedUIInputFieldCaret_set_CaretColor_mAB74E1DF8F92B69CCFB4D2CC83C24B7551F25EAE (void);
// 0x00000D22 UnityEngine.Color CurvedUI.CurvedUIInputFieldCaret::get_SelectionColor()
extern void CurvedUIInputFieldCaret_get_SelectionColor_m4BC57166AC1E16AB0215DCB05E4B15AE90A9BBD4 (void);
// 0x00000D23 System.Void CurvedUI.CurvedUIInputFieldCaret::set_SelectionColor(UnityEngine.Color)
extern void CurvedUIInputFieldCaret_set_SelectionColor_mCDE6CDB94219BB3F228DFB370655085C46BCA27C (void);
// 0x00000D24 System.Single CurvedUI.CurvedUIInputFieldCaret::get_CaretBlinkRate()
extern void CurvedUIInputFieldCaret_get_CaretBlinkRate_m9A52EF65060B50207D6AB2B2F057416065957A36 (void);
// 0x00000D25 System.Void CurvedUI.CurvedUIInputFieldCaret::set_CaretBlinkRate(System.Single)
extern void CurvedUIInputFieldCaret_set_CaretBlinkRate_mC1AB3F74A449A8E40EEEFF5303F5AD381270FBB2 (void);
// 0x00000D26 System.Void CurvedUI.CurvedUIInputFieldCaret::.ctor()
extern void CurvedUIInputFieldCaret__ctor_mA1AA9A03A3349ECB68E432EA474AB87499436CD0 (void);
// 0x00000D27 System.Void CurvedUI.CurvedUIInputFieldCaret/<CaretBlinker>d__11::.ctor(System.Int32)
extern void U3CCaretBlinkerU3Ed__11__ctor_m67381478BF9345D3E969C25962E451EF91315AF5 (void);
// 0x00000D28 System.Void CurvedUI.CurvedUIInputFieldCaret/<CaretBlinker>d__11::System.IDisposable.Dispose()
extern void U3CCaretBlinkerU3Ed__11_System_IDisposable_Dispose_m018D6335DD56C7358CBAB7268D29E25C06496FD7 (void);
// 0x00000D29 System.Boolean CurvedUI.CurvedUIInputFieldCaret/<CaretBlinker>d__11::MoveNext()
extern void U3CCaretBlinkerU3Ed__11_MoveNext_mCBC54332F7307B856664B6B6CDCCD59A233835BE (void);
// 0x00000D2A System.Object CurvedUI.CurvedUIInputFieldCaret/<CaretBlinker>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCaretBlinkerU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD634726565727B1AECD173A723AC9192716DE83 (void);
// 0x00000D2B System.Void CurvedUI.CurvedUIInputFieldCaret/<CaretBlinker>d__11::System.Collections.IEnumerator.Reset()
extern void U3CCaretBlinkerU3Ed__11_System_Collections_IEnumerator_Reset_m98980809C0CEA288CA6DD8AE67E8C6CD25C55D7D (void);
// 0x00000D2C System.Object CurvedUI.CurvedUIInputFieldCaret/<CaretBlinker>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CCaretBlinkerU3Ed__11_System_Collections_IEnumerator_get_Current_m74515E26D1D8C92C52973FE874CCE1F7D22F14BA (void);
// 0x00000D2D System.Void CurvedUI.CurvedUILaserBeam::Update()
extern void CurvedUILaserBeam_Update_m239D2A72903FC76A3D9DEBCD936C537B9C38D090 (void);
// 0x00000D2E System.Void CurvedUI.CurvedUILaserBeam::.ctor()
extern void CurvedUILaserBeam__ctor_m9CAB1D1EDC73F95346EAA6766F293645F9EEEE56 (void);
// 0x00000D2F System.Void CurvedUI.CurvedUILaserBeam/<>c::.cctor()
extern void U3CU3Ec__cctor_m668D5A445F20CC34FCD5F1C04FD94163FD179076 (void);
// 0x00000D30 System.Void CurvedUI.CurvedUILaserBeam/<>c::.ctor()
extern void U3CU3Ec__ctor_m8E2F66F69E455F9CFF7731C0A756FCDDF1303A67 (void);
// 0x00000D31 System.Boolean CurvedUI.CurvedUILaserBeam/<>c::<Update>b__3_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3CUpdateU3Eb__3_0_m654CBA718A9C57D9969BB08F75670F5D5D29507E (void);
// 0x00000D32 System.Void CurvedUI.CurvedUIPhysicsRaycaster::.ctor()
extern void CurvedUIPhysicsRaycaster__ctor_m394C48FBB1CEBD81E60B9C1E5D4061985E9E1ED2 (void);
// 0x00000D33 System.Void CurvedUI.CurvedUIPhysicsRaycaster::Raycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern void CurvedUIPhysicsRaycaster_Raycast_mEAB5959326AC74D61C700F93A59FAECE5BCB6343 (void);
// 0x00000D34 System.Int32 CurvedUI.CurvedUIPhysicsRaycaster::get_CompoundEventMask()
extern void CurvedUIPhysicsRaycaster_get_CompoundEventMask_m88799FEB4593AC9BED95923EFFDBA97642FF9C86 (void);
// 0x00000D35 UnityEngine.Camera CurvedUI.CurvedUIPhysicsRaycaster::get_eventCamera()
extern void CurvedUIPhysicsRaycaster_get_eventCamera_m09B513DE7529B45E877935D50F1409040BE7058F (void);
// 0x00000D36 System.Int32 CurvedUI.CurvedUIPhysicsRaycaster::get_Depth()
extern void CurvedUIPhysicsRaycaster_get_Depth_m57F505637677A1A00FAAA33558B0FA6ECDAFCF4C (void);
// 0x00000D37 System.Int32 CurvedUI.CurvedUIPhysicsRaycaster::get_sortOrderPriority()
extern void CurvedUIPhysicsRaycaster_get_sortOrderPriority_mA831B0E1AC052CCA88F4148BBFEB3085933E6801 (void);
// 0x00000D38 System.Void CurvedUI.CurvedUIPointerEventData::.ctor(UnityEngine.EventSystems.EventSystem)
extern void CurvedUIPointerEventData__ctor_mC97436B80566CB27E88CD2D0CF5B3C20753E6A9C (void);
// 0x00000D39 System.Void CurvedUI.CurvedUIRaycaster::Awake()
extern void CurvedUIRaycaster_Awake_m41A71B7D05E239792E111C3AC3EECB501E6CF0F4 (void);
// 0x00000D3A System.Void CurvedUI.CurvedUIRaycaster::Start()
extern void CurvedUIRaycaster_Start_m7830A9CFAD30ECB0C9B2AD9364D1FFD142FDAA7B (void);
// 0x00000D3B System.Void CurvedUI.CurvedUIRaycaster::Update()
extern void CurvedUIRaycaster_Update_mF15776991BA44552DF39FBE063DA0AD88297F5BD (void);
// 0x00000D3C System.Void CurvedUI.CurvedUIRaycaster::ProcessGazeTimedClick()
extern void CurvedUIRaycaster_ProcessGazeTimedClick_mB9A9B264031551841D452A70E26328AB0C817FF2 (void);
// 0x00000D3D System.Void CurvedUI.CurvedUIRaycaster::ResetGazeTimedClick()
extern void CurvedUIRaycaster_ResetGazeTimedClick_m843C7C84068CB9C07C3A23D7D523F06D53C7C100 (void);
// 0x00000D3E System.Void CurvedUI.CurvedUIRaycaster::Raycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern void CurvedUIRaycaster_Raycast_m64586E1A925EE17353C4E4223DCEDCD15320D54A (void);
// 0x00000D3F System.Boolean CurvedUI.CurvedUIRaycaster::RaycastToCyllinderCanvas(UnityEngine.Ray,UnityEngine.Vector2&,System.Boolean)
extern void CurvedUIRaycaster_RaycastToCyllinderCanvas_m4EB80FA872167544AB7A1C280068B90BE7E24D45 (void);
// 0x00000D40 System.Boolean CurvedUI.CurvedUIRaycaster::RaycastToCyllinderVerticalCanvas(UnityEngine.Ray,UnityEngine.Vector2&,System.Boolean)
extern void CurvedUIRaycaster_RaycastToCyllinderVerticalCanvas_m57FF4C55C28EF9BF69F8780FB8094BA84E8D2233 (void);
// 0x00000D41 System.Boolean CurvedUI.CurvedUIRaycaster::RaycastToRingCanvas(UnityEngine.Ray,UnityEngine.Vector2&,System.Boolean)
extern void CurvedUIRaycaster_RaycastToRingCanvas_mB516E9C001D62B88ABA55A7DAA1CAB45FC50A427 (void);
// 0x00000D42 System.Boolean CurvedUI.CurvedUIRaycaster::RaycastToSphereCanvas(UnityEngine.Ray,UnityEngine.Vector2&,System.Boolean)
extern void CurvedUIRaycaster_RaycastToSphereCanvas_mEFE28824AFEC714493F5AFD1817F87A2481C9369 (void);
// 0x00000D43 System.Void CurvedUI.CurvedUIRaycaster::FlatRaycast(UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>)
extern void CurvedUIRaycaster_FlatRaycast_mE77907450A3E3BCAA6281F73937BD8F7A14F9845 (void);
// 0x00000D44 System.Void CurvedUI.CurvedUIRaycaster::FlatRaycastAndSort(UnityEngine.Canvas,UnityEngine.Camera,UnityEngine.Vector2,System.Collections.Generic.IList`1<UnityEngine.UI.Graphic>,System.Collections.Generic.List`1<UnityEngine.UI.Graphic>)
extern void CurvedUIRaycaster_FlatRaycastAndSort_mA8B7682956D651F95471FCA6471902B70E6BAE9E (void);
// 0x00000D45 System.Void CurvedUI.CurvedUIRaycaster::CreateCollider()
extern void CurvedUIRaycaster_CreateCollider_mA8BF560052053B5FC6077E85CCD79CFD7F629ACF (void);
// 0x00000D46 System.Void CurvedUI.CurvedUIRaycaster::SetupMeshColliderUsingMesh(UnityEngine.Mesh)
extern void CurvedUIRaycaster_SetupMeshColliderUsingMesh_m0286CC32AE1A28B97D1D01692E09118FAEC705C9 (void);
// 0x00000D47 UnityEngine.GameObject CurvedUI.CurvedUIRaycaster::CreateConvexCyllinderCollider(System.Boolean)
extern void CurvedUIRaycaster_CreateConvexCyllinderCollider_mA543C686BE160F470551D7CCE6C76C7B980CA8D8 (void);
// 0x00000D48 UnityEngine.Mesh CurvedUI.CurvedUIRaycaster::CreateCyllinderColliderMesh(System.Boolean)
extern void CurvedUIRaycaster_CreateCyllinderColliderMesh_m17DE884386AF2F807B4ABCA686A00AEF128B281D (void);
// 0x00000D49 UnityEngine.Mesh CurvedUI.CurvedUIRaycaster::CreateSphereColliderMesh()
extern void CurvedUIRaycaster_CreateSphereColliderMesh_m5DAB4225D7D0416B57F0358800294F290DDDED2F (void);
// 0x00000D4A System.Boolean CurvedUI.CurvedUIRaycaster::IsInLayerMask(System.Int32,UnityEngine.LayerMask)
extern void CurvedUIRaycaster_IsInLayerMask_m75DB04891FF16B4F91DF95CF2EC229394805B1CC (void);
// 0x00000D4B UnityEngine.LayerMask CurvedUI.CurvedUIRaycaster::GetRaycastLayerMask()
extern void CurvedUIRaycaster_GetRaycastLayerMask_m29B6E80D367057356377A91DD0D8B87F6D7E0111 (void);
// 0x00000D4C UnityEngine.UI.Image CurvedUI.CurvedUIRaycaster::get_GazeProgressImage()
extern void CurvedUIRaycaster_get_GazeProgressImage_mDF2E8876BCD902EDCC1D2E0F7A7BA5FFCF55BAEE (void);
// 0x00000D4D System.Single CurvedUI.CurvedUIRaycaster::AngleSigned(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void CurvedUIRaycaster_AngleSigned_mC5B773C402C43DEFC4951587BE44F3233A8D2B2E (void);
// 0x00000D4E System.Boolean CurvedUI.CurvedUIRaycaster::ShouldStartDrag(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Boolean)
extern void CurvedUIRaycaster_ShouldStartDrag_m9F9C98980D20F76E4750C64F8CF3B0500F4A152E (void);
// 0x00000D4F System.Void CurvedUI.CurvedUIRaycaster::ProcessMove(UnityEngine.EventSystems.PointerEventData)
extern void CurvedUIRaycaster_ProcessMove_mB380654DFD5C7D00493C843630F2EDEDCB861554 (void);
// 0x00000D50 System.Void CurvedUI.CurvedUIRaycaster::UpdateSelectedObjects(UnityEngine.EventSystems.PointerEventData)
extern void CurvedUIRaycaster_UpdateSelectedObjects_m2AA12EFC33A3889BC1B7ECCDD029E5B9C045FBF8 (void);
// 0x00000D51 System.Void CurvedUI.CurvedUIRaycaster::HandlePointerExitAndEnter(UnityEngine.EventSystems.PointerEventData,UnityEngine.GameObject)
extern void CurvedUIRaycaster_HandlePointerExitAndEnter_mB6AE6B4BB0C035B746E5EB993C1FF7AB5F639541 (void);
// 0x00000D52 UnityEngine.GameObject CurvedUI.CurvedUIRaycaster::FindCommonRoot(UnityEngine.GameObject,UnityEngine.GameObject)
extern void CurvedUIRaycaster_FindCommonRoot_mEE9AFC04C85BFEDC6124D9007009B144AFE6371F (void);
// 0x00000D53 System.Boolean CurvedUI.CurvedUIRaycaster::GetScreenSpacePointByRay(UnityEngine.Ray,UnityEngine.Vector2&)
extern void CurvedUIRaycaster_GetScreenSpacePointByRay_m29D009CB48063DFFD8A7E47596463B1B551812A4 (void);
// 0x00000D54 System.Boolean CurvedUI.CurvedUIRaycaster::CheckEventCamera()
extern void CurvedUIRaycaster_CheckEventCamera_m6613B2556B486767F0208408EC2ABBE0EA8DBA20 (void);
// 0x00000D55 System.Boolean CurvedUI.CurvedUIRaycaster::get_PointingAtCanvas()
extern void CurvedUIRaycaster_get_PointingAtCanvas_mB7403942A71A3ABD3342F50BB898520AACAE386A (void);
// 0x00000D56 System.Void CurvedUI.CurvedUIRaycaster::RebuildCollider()
extern void CurvedUIRaycaster_RebuildCollider_mDA76326696DBCB66EC1E9BAA302F3638A400695E (void);
// 0x00000D57 System.Collections.Generic.List`1<UnityEngine.GameObject> CurvedUI.CurvedUIRaycaster::GetObjectsUnderPointer()
extern void CurvedUIRaycaster_GetObjectsUnderPointer_mF213CEEBAA90A773CA0DDD71D00C4541830A30E0 (void);
// 0x00000D58 System.Collections.Generic.List`1<UnityEngine.GameObject> CurvedUI.CurvedUIRaycaster::GetObjectsUnderScreenPos(UnityEngine.Vector2,UnityEngine.Camera)
extern void CurvedUIRaycaster_GetObjectsUnderScreenPos_m87646F59031FE530555FC38F1D20466A981CB3BA (void);
// 0x00000D59 System.Collections.Generic.List`1<UnityEngine.GameObject> CurvedUI.CurvedUIRaycaster::GetObjectsHitByRay(UnityEngine.Ray)
extern void CurvedUIRaycaster_GetObjectsHitByRay_m148D933758579396D11CD5CC933FB2BE81B64A47 (void);
// 0x00000D5A System.Void CurvedUI.CurvedUIRaycaster::Click()
extern void CurvedUIRaycaster_Click_m4E0FB818486A780429EDC714D10F3599CB87ECAB (void);
// 0x00000D5B System.Void CurvedUI.CurvedUIRaycaster::ModifyQuad(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,UnityEngine.Vector2)
extern void CurvedUIRaycaster_ModifyQuad_mFB23B1772220A4612677D0630C593686613A9A9C (void);
// 0x00000D5C UnityEngine.Vector3 CurvedUI.CurvedUIRaycaster::TesselateQuad(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single,System.Single)
extern void CurvedUIRaycaster_TesselateQuad_mDAF3602A35B00586A317F20DD8A9DC977E9684B3 (void);
// 0x00000D5D System.Void CurvedUI.CurvedUIRaycaster::.ctor()
extern void CurvedUIRaycaster__ctor_m9BE516992572C4816332C16F25B4832CB8E8E6EB (void);
// 0x00000D5E System.Void CurvedUI.CurvedUIRaycaster::.cctor()
extern void CurvedUIRaycaster__cctor_m3F85146FA9FC4ACEDD66D97EF10E1DA8B74E24D4 (void);
// 0x00000D5F System.Void CurvedUI.CurvedUIRaycaster/<>c::.cctor()
extern void U3CU3Ec__cctor_mF56774A508D9974A40386403C590C0017029B878 (void);
// 0x00000D60 System.Void CurvedUI.CurvedUIRaycaster/<>c::.ctor()
extern void U3CU3Ec__ctor_m60DF2B930C5ACFFF5BFB160ED5B355B7E6E88C27 (void);
// 0x00000D61 System.Boolean CurvedUI.CurvedUIRaycaster/<>c::<Update>b__21_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3CUpdateU3Eb__21_0_mFC514F6FCED2666E408C30CEBCC7F3C165E9CBEC (void);
// 0x00000D62 System.Int32 CurvedUI.CurvedUIRaycaster/<>c::<FlatRaycastAndSort>b__32_0(UnityEngine.UI.Graphic,UnityEngine.UI.Graphic)
extern void U3CU3Ec_U3CFlatRaycastAndSortU3Eb__32_0_mEBDCF8ED8EE7BB8827893E5A1CEFE1A59B16CB40 (void);
// 0x00000D63 System.Int32 CurvedUI.CurvedUIRaycaster/<>c::<GetObjectsHitByRay>b__55_0(UnityEngine.UI.Graphic,UnityEngine.UI.Graphic)
extern void U3CU3Ec_U3CGetObjectsHitByRayU3Eb__55_0_m72535D85748212A6B27ABDB9950DBE256B0F4D90 (void);
// 0x00000D64 System.Void CurvedUI.CurvedUISettings::Awake()
extern void CurvedUISettings_Awake_m33E15DA403B2389FAD7FCA4969F395DC454B9A25 (void);
// 0x00000D65 System.Void CurvedUI.CurvedUISettings::Start()
extern void CurvedUISettings_Start_m88CF93D4DAB4941172D8F02B882F7A734BD9AB5A (void);
// 0x00000D66 System.Void CurvedUI.CurvedUISettings::OnEnable()
extern void CurvedUISettings_OnEnable_mC0C81E95DB6033E6A9AF8AA7FC60F32C8E57E03F (void);
// 0x00000D67 System.Void CurvedUI.CurvedUISettings::OnDisable()
extern void CurvedUISettings_OnDisable_m2B6EAA803350D9B9A7889B5FFF936C368586FDE6 (void);
// 0x00000D68 System.Void CurvedUI.CurvedUISettings::Update()
extern void CurvedUISettings_Update_m3AFCC97F3B4A410A9CFF600E7E4C95DF837DBFFF (void);
// 0x00000D69 System.Void CurvedUI.CurvedUISettings::SetUIAngle(System.Int32)
extern void CurvedUISettings_SetUIAngle_mEE7C7B911E083ECB9F1E86755C2E24E90232A4F6 (void);
// 0x00000D6A UnityEngine.Vector3 CurvedUI.CurvedUISettings::CanvasToCyllinder(UnityEngine.Vector3)
extern void CurvedUISettings_CanvasToCyllinder_m3E67A2527A7C582B69C61B88CD31199F42629823 (void);
// 0x00000D6B UnityEngine.Vector3 CurvedUI.CurvedUISettings::CanvasToCyllinderVertical(UnityEngine.Vector3)
extern void CurvedUISettings_CanvasToCyllinderVertical_mFB603527BF61C0D300DFA49B696A72F05D9BA1EE (void);
// 0x00000D6C UnityEngine.Vector3 CurvedUI.CurvedUISettings::CanvasToRing(UnityEngine.Vector3)
extern void CurvedUISettings_CanvasToRing_m0C25C1E32C66C19C9467E858A00A4A61E8C43DB7 (void);
// 0x00000D6D UnityEngine.Vector3 CurvedUI.CurvedUISettings::CanvasToSphere(UnityEngine.Vector3)
extern void CurvedUISettings_CanvasToSphere_m929920AF2E6469BB5ED5D21D1852259E140E6978 (void);
// 0x00000D6E UnityEngine.RectTransform CurvedUI.CurvedUISettings::get_RectTransform()
extern void CurvedUISettings_get_RectTransform_m9D237DC354456882D1C0E94C3DB66B1CC6BEC35B (void);
// 0x00000D6F System.Void CurvedUI.CurvedUISettings::AddEffectToChildren()
extern void CurvedUISettings_AddEffectToChildren_m36C3BAF8A9433A04E534A058EA886504280D9C32 (void);
// 0x00000D70 UnityEngine.Vector3 CurvedUI.CurvedUISettings::VertexPositionToCurvedCanvas(UnityEngine.Vector3)
extern void CurvedUISettings_VertexPositionToCurvedCanvas_mA5680842C563023029933E089D58BCDEF5F6510B (void);
// 0x00000D71 UnityEngine.Vector3 CurvedUI.CurvedUISettings::CanvasToCurvedCanvas(UnityEngine.Vector3)
extern void CurvedUISettings_CanvasToCurvedCanvas_m1658CCBFF20B993B94670E4DE4A938DB4A39F4EC (void);
// 0x00000D72 UnityEngine.Vector3 CurvedUI.CurvedUISettings::CanvasToCurvedCanvasNormal(UnityEngine.Vector3)
extern void CurvedUISettings_CanvasToCurvedCanvasNormal_m9CFF358CC25FA358156071F046E1C364C36E72E6 (void);
// 0x00000D73 System.Boolean CurvedUI.CurvedUISettings::RaycastToCanvasSpace(UnityEngine.Ray,UnityEngine.Vector2&)
extern void CurvedUISettings_RaycastToCanvasSpace_mE7642ABF453446669890DC049B36AE70FC295614 (void);
// 0x00000D74 System.Single CurvedUI.CurvedUISettings::GetCyllinderRadiusInCanvasSpace()
extern void CurvedUISettings_GetCyllinderRadiusInCanvasSpace_mFBCDDD132AFE0B8E308F0757B5EAB21294038855 (void);
// 0x00000D75 UnityEngine.Vector2 CurvedUI.CurvedUISettings::GetTesslationSize(System.Boolean)
extern void CurvedUISettings_GetTesslationSize_mA5A866B283EF76F227141A4208D18AD0874E12E6 (void);
// 0x00000D76 System.Single CurvedUI.CurvedUISettings::GetSegmentsByAngle(System.Single)
extern void CurvedUISettings_GetSegmentsByAngle_m9F396F0EE0559F7576566E04468270FBD4D90569 (void);
// 0x00000D77 System.Int32 CurvedUI.CurvedUISettings::get_BaseCircleSegments()
extern void CurvedUISettings_get_BaseCircleSegments_m76D735CBE7DF620256E7E34B1EFAD4924B838FCB (void);
// 0x00000D78 System.Int32 CurvedUI.CurvedUISettings::get_Angle()
extern void CurvedUISettings_get_Angle_m1EE2996AFD63A8450C903341A72939708998E5A0 (void);
// 0x00000D79 System.Void CurvedUI.CurvedUISettings::set_Angle(System.Int32)
extern void CurvedUISettings_set_Angle_mFBED67A77D1C8C618C440A1F7F9E21A6705D2733 (void);
// 0x00000D7A System.Single CurvedUI.CurvedUISettings::get_Quality()
extern void CurvedUISettings_get_Quality_mE6527143BD0F8AEC406A617F17DAA435B371496E (void);
// 0x00000D7B System.Void CurvedUI.CurvedUISettings::set_Quality(System.Single)
extern void CurvedUISettings_set_Quality_m166319A9FAB48C490BF767FA6FBBC926BABDD957 (void);
// 0x00000D7C CurvedUI.CurvedUISettings/CurvedUIShape CurvedUI.CurvedUISettings::get_Shape()
extern void CurvedUISettings_get_Shape_m6BE04B6D15EC541E67CBBAA98F79D12A3DA99978 (void);
// 0x00000D7D System.Void CurvedUI.CurvedUISettings::set_Shape(CurvedUI.CurvedUISettings/CurvedUIShape)
extern void CurvedUISettings_set_Shape_mB9C7A429B780CF018761B95A145F3D0BFB408972 (void);
// 0x00000D7E System.Int32 CurvedUI.CurvedUISettings::get_VerticalAngle()
extern void CurvedUISettings_get_VerticalAngle_mBEFF9D6FE2D5F11421A2FD99D42A8E775B7478C9 (void);
// 0x00000D7F System.Void CurvedUI.CurvedUISettings::set_VerticalAngle(System.Int32)
extern void CurvedUISettings_set_VerticalAngle_mE4B06780F9F9BC51A3EABE86207CFAE552249513 (void);
// 0x00000D80 System.Single CurvedUI.CurvedUISettings::get_RingFill()
extern void CurvedUISettings_get_RingFill_m9349A724E071D560F52F6079A71535B93992B65D (void);
// 0x00000D81 System.Void CurvedUI.CurvedUISettings::set_RingFill(System.Single)
extern void CurvedUISettings_set_RingFill_m6DE600750159292FA24758C5ED896AC88403285B (void);
// 0x00000D82 System.Single CurvedUI.CurvedUISettings::get_SavedRadius()
extern void CurvedUISettings_get_SavedRadius_m17969A1CFEFAED639C2017AD3BD35E189FE39C50 (void);
// 0x00000D83 System.Int32 CurvedUI.CurvedUISettings::get_RingExternalDiameter()
extern void CurvedUISettings_get_RingExternalDiameter_m9FB3A641AB503C081C1B28A5DB7EE2EB7073FB24 (void);
// 0x00000D84 System.Void CurvedUI.CurvedUISettings::set_RingExternalDiameter(System.Int32)
extern void CurvedUISettings_set_RingExternalDiameter_m0C8C0B2B0BF0A4899A298A12F2B4FA925A301D68 (void);
// 0x00000D85 System.Boolean CurvedUI.CurvedUISettings::get_RingFlipVertical()
extern void CurvedUISettings_get_RingFlipVertical_mDB3E42B6983142E071C5E8307C15AB322349862E (void);
// 0x00000D86 System.Void CurvedUI.CurvedUISettings::set_RingFlipVertical(System.Boolean)
extern void CurvedUISettings_set_RingFlipVertical_m32081215315DF813D0F07EC24A2994976141F5D7 (void);
// 0x00000D87 System.Boolean CurvedUI.CurvedUISettings::get_PreserveAspect()
extern void CurvedUISettings_get_PreserveAspect_m26AE14C6CF90717F8DF7168D1ED4ED9E10E143D0 (void);
// 0x00000D88 System.Void CurvedUI.CurvedUISettings::set_PreserveAspect(System.Boolean)
extern void CurvedUISettings_set_PreserveAspect_mA5E4C3FA17394FF86E555D06E98B6081A9CCA5B5 (void);
// 0x00000D89 System.Boolean CurvedUI.CurvedUISettings::get_Interactable()
extern void CurvedUISettings_get_Interactable_m4B895A6868BEE4E82E6D94DB41FCA246DA563EDF (void);
// 0x00000D8A System.Void CurvedUI.CurvedUISettings::set_Interactable(System.Boolean)
extern void CurvedUISettings_set_Interactable_mEED95FCD49ECD36F3CEFB2FFFC8E60E861E0D936 (void);
// 0x00000D8B System.Boolean CurvedUI.CurvedUISettings::get_ForceUseBoxCollider()
extern void CurvedUISettings_get_ForceUseBoxCollider_m36FD603A67636DCDA54E6D01E8E54B1BA90ABC0F (void);
// 0x00000D8C System.Void CurvedUI.CurvedUISettings::set_ForceUseBoxCollider(System.Boolean)
extern void CurvedUISettings_set_ForceUseBoxCollider_mCFF6881EDD5C8EFA701D87293D079FEFC2C28E1C (void);
// 0x00000D8D System.Boolean CurvedUI.CurvedUISettings::get_BlocksRaycasts()
extern void CurvedUISettings_get_BlocksRaycasts_m31E74082A1F09E01C342D9C3FE8FD3979ACEAE94 (void);
// 0x00000D8E System.Void CurvedUI.CurvedUISettings::set_BlocksRaycasts(System.Boolean)
extern void CurvedUISettings_set_BlocksRaycasts_mA13BB0F17078B73476F5AF5A118255E84F8D0F8E (void);
// 0x00000D8F System.Boolean CurvedUI.CurvedUISettings::get_RaycastMyLayerOnly()
extern void CurvedUISettings_get_RaycastMyLayerOnly_mEDFAB015DF898866585C9C998BEC5369C0646054 (void);
// 0x00000D90 System.Void CurvedUI.CurvedUISettings::set_RaycastMyLayerOnly(System.Boolean)
extern void CurvedUISettings_set_RaycastMyLayerOnly_m3CB69A45B1B6D091FA936BBB5255AB41A9C3BB6B (void);
// 0x00000D91 System.Void CurvedUI.CurvedUISettings::SetAllChildrenDirty(System.Boolean)
extern void CurvedUISettings_SetAllChildrenDirty_mA93BB6B6F8F1642A66B52FE66043D47586D76FE4 (void);
// 0x00000D92 System.Boolean CurvedUI.CurvedUISettings::get_PointingAtCanvas()
extern void CurvedUISettings_get_PointingAtCanvas_mAB6014E2CC9A183D27192E4AFDFB0DD99AE3EFC1 (void);
// 0x00000D93 System.Void CurvedUI.CurvedUISettings::Click()
extern void CurvedUISettings_Click_mDB49A7CCF7C07F1F5F72AC7D097BBB9EE30D472B (void);
// 0x00000D94 CurvedUIInputModule/CUIControlMethod CurvedUI.CurvedUISettings::get_ControlMethod()
extern void CurvedUISettings_get_ControlMethod_m9E222781CCE562D3EE938484C7AA45690D3DC1C0 (void);
// 0x00000D95 System.Void CurvedUI.CurvedUISettings::set_ControlMethod(CurvedUIInputModule/CUIControlMethod)
extern void CurvedUISettings_set_ControlMethod_mB3F050144BEA6F463989F42D9889C31695EC91DF (void);
// 0x00000D96 System.Collections.Generic.List`1<UnityEngine.GameObject> CurvedUI.CurvedUISettings::GetObjectsUnderPointer()
extern void CurvedUISettings_GetObjectsUnderPointer_mEA3244063C0DC120A27905EABF83A427FA52E846 (void);
// 0x00000D97 System.Collections.Generic.List`1<UnityEngine.GameObject> CurvedUI.CurvedUISettings::GetObjectsUnderScreenPos(UnityEngine.Vector2,UnityEngine.Camera)
extern void CurvedUISettings_GetObjectsUnderScreenPos_mB7E0F07652C86AFF90E298F65E20C7ED447B4C1F (void);
// 0x00000D98 System.Collections.Generic.List`1<UnityEngine.GameObject> CurvedUI.CurvedUISettings::GetObjectsHitByRay(UnityEngine.Ray)
extern void CurvedUISettings_GetObjectsHitByRay_m0A10944D3F647034ABD7D9E17C3C6DAEB205D8B6 (void);
// 0x00000D99 System.Boolean CurvedUI.CurvedUISettings::get_GazeUseTimedClick()
extern void CurvedUISettings_get_GazeUseTimedClick_mE3456FA58CDEAEF0C6A863B6A0C559EC71E9F0DA (void);
// 0x00000D9A System.Void CurvedUI.CurvedUISettings::set_GazeUseTimedClick(System.Boolean)
extern void CurvedUISettings_set_GazeUseTimedClick_m7F8302872A9A453E23A484482313DCB66DBC1165 (void);
// 0x00000D9B System.Single CurvedUI.CurvedUISettings::get_GazeClickTimer()
extern void CurvedUISettings_get_GazeClickTimer_mF336D35CEE0E6C75ACCEEA613BD420A091BD35FF (void);
// 0x00000D9C System.Void CurvedUI.CurvedUISettings::set_GazeClickTimer(System.Single)
extern void CurvedUISettings_set_GazeClickTimer_m107DACAA98C26C4EB0730A44ABBB3D40ACFC51A6 (void);
// 0x00000D9D System.Single CurvedUI.CurvedUISettings::get_GazeClickTimerDelay()
extern void CurvedUISettings_get_GazeClickTimerDelay_m2E1760EDE7D2B1E0AC6F1F9A87ED4786DBB67F24 (void);
// 0x00000D9E System.Void CurvedUI.CurvedUISettings::set_GazeClickTimerDelay(System.Single)
extern void CurvedUISettings_set_GazeClickTimerDelay_mECB610FE95147A122DE50E1B45B4F67266EC620E (void);
// 0x00000D9F System.Single CurvedUI.CurvedUISettings::get_GazeTimerProgress()
extern void CurvedUISettings_get_GazeTimerProgress_m7C5A8B3A32F6B034E4606D9F6F06464D64D71D7A (void);
// 0x00000DA0 System.Void CurvedUI.CurvedUISettings::.ctor()
extern void CurvedUISettings__ctor_mFFF15E7A20C1ED9793F1EA9441F05B3819B58394 (void);
// 0x00000DA1 System.Boolean CurvedUI.CurvedUIVertexEffect::get_tesselationRequired()
extern void CurvedUIVertexEffect_get_tesselationRequired_m08E71987CA7C3C05F44AC4C6DC39BFA70851A822 (void);
// 0x00000DA2 System.Void CurvedUI.CurvedUIVertexEffect::set_tesselationRequired(System.Boolean)
extern void CurvedUIVertexEffect_set_tesselationRequired_m45D6AC6D425C55CC7DA5033FC390DEEA5AAEB071 (void);
// 0x00000DA3 System.Void CurvedUI.CurvedUIVertexEffect::Awake()
extern void CurvedUIVertexEffect_Awake_mD37D2931E5D1CE8436BC2F77FDAFB0A497D0F954 (void);
// 0x00000DA4 System.Void CurvedUI.CurvedUIVertexEffect::OnEnable()
extern void CurvedUIVertexEffect_OnEnable_m804472989514EC99F3DDA634E9C1E359BEA24DE8 (void);
// 0x00000DA5 System.Void CurvedUI.CurvedUIVertexEffect::Start()
extern void CurvedUIVertexEffect_Start_m52083F15545A43741C25E60AE0528783FF41971A (void);
// 0x00000DA6 System.Void CurvedUI.CurvedUIVertexEffect::OnDisable()
extern void CurvedUIVertexEffect_OnDisable_mF5171DF40BF679BF19FAC7BEB8FD266709D08172 (void);
// 0x00000DA7 System.Void CurvedUI.CurvedUIVertexEffect::TesselationRequiredCallback()
extern void CurvedUIVertexEffect_TesselationRequiredCallback_m620E6430212B9F8979023FB06849099F7CF67018 (void);
// 0x00000DA8 System.Void CurvedUI.CurvedUIVertexEffect::FontTextureRebuiltCallback(UnityEngine.Font)
extern void CurvedUIVertexEffect_FontTextureRebuiltCallback_mD093F6EDE5570D99ED4DCE982B112E73A3676381 (void);
// 0x00000DA9 System.Void CurvedUI.CurvedUIVertexEffect::LateUpdate()
extern void CurvedUIVertexEffect_LateUpdate_mB7BD5259F9E1E10D6A61074DE250EF8E10CEF87C (void);
// 0x00000DAA System.Void CurvedUI.CurvedUIVertexEffect::ModifyMesh(UnityEngine.UI.VertexHelper)
extern void CurvedUIVertexEffect_ModifyMesh_m69D2C18CD67BADEE56E1CF324FFAD37DAC77A8F1 (void);
// 0x00000DAB System.Void CurvedUI.CurvedUIVertexEffect::ModifyTMPMesh(System.Collections.Generic.List`1<UnityEngine.UIVertex>&)
extern void CurvedUIVertexEffect_ModifyTMPMesh_m723B6C3763093A9742C276D1CA6E06778A00784A (void);
// 0x00000DAC System.Boolean CurvedUI.CurvedUIVertexEffect::ShouldModify()
extern void CurvedUIVertexEffect_ShouldModify_m385CDF9926572D0FACA0172CD23E5D9FE885980A (void);
// 0x00000DAD System.Void CurvedUI.CurvedUIVertexEffect::CheckTextFontMaterial()
extern void CurvedUIVertexEffect_CheckTextFontMaterial_m2D71DE03244DBC24E10036EACC5ECC3E8EF7AE52 (void);
// 0x00000DAE CurvedUI.CurvedUISettings CurvedUI.CurvedUIVertexEffect::FindParentSettings(System.Boolean)
extern void CurvedUIVertexEffect_FindParentSettings_m7181A7C532DAFB430FCB6AE96853C9921A615EEE (void);
// 0x00000DAF UnityEngine.UIVertex CurvedUI.CurvedUIVertexEffect::CurveVertex(UnityEngine.UIVertex,System.Single,System.Single,UnityEngine.Vector2)
extern void CurvedUIVertexEffect_CurveVertex_mC6912A723DEAF6D098B4CA3A099897FE657C46F8 (void);
// 0x00000DB0 System.Void CurvedUI.CurvedUIVertexEffect::TesselateGeometry(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void CurvedUIVertexEffect_TesselateGeometry_mCB606693E7EC0C4FC9CC7CD50A52D7AAFF6A53E9 (void);
// 0x00000DB1 System.Void CurvedUI.CurvedUIVertexEffect::ModifyQuad(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Int32,UnityEngine.Vector2)
extern void CurvedUIVertexEffect_ModifyQuad_mB5AF951AFC70A67588CFE401B06F3EB8C41680CD (void);
// 0x00000DB2 System.Void CurvedUI.CurvedUIVertexEffect::TrisToQuads(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void CurvedUIVertexEffect_TrisToQuads_m5FADD57BE7342676E2FF447DD81E875DA1458C6A (void);
// 0x00000DB3 UnityEngine.UIVertex CurvedUI.CurvedUIVertexEffect::TesselateQuad(System.Single,System.Single)
extern void CurvedUIVertexEffect_TesselateQuad_m366A2E193ECEC00E97F4E6E9491B6A9933B01852 (void);
// 0x00000DB4 System.Void CurvedUI.CurvedUIVertexEffect::SetDirty()
extern void CurvedUIVertexEffect_SetDirty_mE48F20338F6D45E5808C5C16563096EB21821856 (void);
// 0x00000DB5 System.Boolean CurvedUI.CurvedUIVertexEffect::get_TesselationRequired()
extern void CurvedUIVertexEffect_get_TesselationRequired_m1B90903536388AB253BD17B7A6AB74D0301FD81F (void);
// 0x00000DB6 System.Void CurvedUI.CurvedUIVertexEffect::set_TesselationRequired(System.Boolean)
extern void CurvedUIVertexEffect_set_TesselationRequired_m7FFD8CD9F89603EA21F0999757255F5559C45DD4 (void);
// 0x00000DB7 System.Boolean CurvedUI.CurvedUIVertexEffect::get_CurvingRequired()
extern void CurvedUIVertexEffect_get_CurvingRequired_m134E5F264829E3B5EBD7EF69FFE4D4427CA3E5AF (void);
// 0x00000DB8 System.Void CurvedUI.CurvedUIVertexEffect::set_CurvingRequired(System.Boolean)
extern void CurvedUIVertexEffect_set_CurvingRequired_m41D1342720881EFB3527EE0A536B3EAB67EDD1B4 (void);
// 0x00000DB9 System.Void CurvedUI.CurvedUIVertexEffect::.ctor()
extern void CurvedUIVertexEffect__ctor_m17509362682E544585262DDAD2179D78450F8EFB (void);
// 0x00000DBA System.Void CurvedUI.CurvedUIViveController::.ctor()
extern void CurvedUIViveController__ctor_m8091F5C90E023A32E5753E4029A43AE7E0A18C3F (void);
// 0x00000DBB System.Void CurvedUI.ViveInputEvent::.ctor(System.Object,System.IntPtr)
extern void ViveInputEvent__ctor_m632425778123A6B068C61427CC8B5EC5CC046FEA (void);
// 0x00000DBC System.Void CurvedUI.ViveInputEvent::Invoke(System.Object,CurvedUI.ViveInputArgs)
extern void ViveInputEvent_Invoke_mCA1790FC2D22BFCE56B60C319A2483B665148F75 (void);
// 0x00000DBD System.IAsyncResult CurvedUI.ViveInputEvent::BeginInvoke(System.Object,CurvedUI.ViveInputArgs,System.AsyncCallback,System.Object)
extern void ViveInputEvent_BeginInvoke_m11064F45527733AE5F2D67400681C37F884EE189 (void);
// 0x00000DBE System.Void CurvedUI.ViveInputEvent::EndInvoke(System.IAsyncResult)
extern void ViveInputEvent_EndInvoke_m6FAF2D6B6F9A44BF204825BB724CE4185C6CEC36 (void);
// 0x00000DBF System.Void CurvedUI.ViveEvent::.ctor(System.Object,System.IntPtr)
extern void ViveEvent__ctor_m4E4D6CD824208EC3C89FFC672AACA472C615AB6D (void);
// 0x00000DC0 System.Void CurvedUI.ViveEvent::Invoke(System.Object)
extern void ViveEvent_Invoke_mB15BF3283CA0BAD82245A745921BE469D9A9974D (void);
// 0x00000DC1 System.IAsyncResult CurvedUI.ViveEvent::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void ViveEvent_BeginInvoke_mBD19E358FC6580DD3D8F62F208FB499B6C7779CA (void);
// 0x00000DC2 System.Void CurvedUI.ViveEvent::EndInvoke(System.IAsyncResult)
extern void ViveEvent_EndInvoke_mEAA7F601BF9A8E622D57039F25591C1F3B99B369 (void);
// 0x00000DC3 System.Void CurvedUI.CurvedUITMP::Start()
extern void CurvedUITMP_Start_m1BE56EA01AB1B8A3C93C054DC669C5026D6ACE1E (void);
// 0x00000DC4 System.Void CurvedUI.CurvedUITMP::OnEnable()
extern void CurvedUITMP_OnEnable_m737F9D46AC239D8F7A6FDC78BFC9BD51C28BFC41 (void);
// 0x00000DC5 System.Void CurvedUI.CurvedUITMP::OnDisable()
extern void CurvedUITMP_OnDisable_m7795FC16D377B51EA3BAC5769C2C8F183DB169CC (void);
// 0x00000DC6 System.Void CurvedUI.CurvedUITMP::OnDestroy()
extern void CurvedUITMP_OnDestroy_mF27E2E9A826FEA627E1A5B7348A251C5378B1C66 (void);
// 0x00000DC7 System.Void CurvedUI.CurvedUITMP::LateUpdate()
extern void CurvedUITMP_LateUpdate_m778AF8EF130C4D784F13F13DAD29CCF4BDF9C125 (void);
// 0x00000DC8 System.Void CurvedUI.CurvedUITMP::CreateUIVertexList(UnityEngine.Mesh)
extern void CurvedUITMP_CreateUIVertexList_m1F9577511B96E2D79C4CBD1330F48063BFEDA728 (void);
// 0x00000DC9 System.Void CurvedUI.CurvedUITMP::GetUIVertexFromMesh(UnityEngine.UIVertex&,System.Int32)
extern void CurvedUITMP_GetUIVertexFromMesh_m57183D6A1DBE9738A53CA5BE0CDDEAA2E847A436 (void);
// 0x00000DCA System.Void CurvedUI.CurvedUITMP::FillMeshWithUIVertexList(UnityEngine.Mesh,System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void CurvedUITMP_FillMeshWithUIVertexList_m341D2F7F640BBF278B2A2F8CBF0CDE6AD35A3CE4 (void);
// 0x00000DCB System.Void CurvedUI.CurvedUITMP::FindTMP()
extern void CurvedUITMP_FindTMP_mFACBD48AE2925379E501DECCC7A203630D1EF3FA (void);
// 0x00000DCC System.Void CurvedUI.CurvedUITMP::FindSubmeshes()
extern void CurvedUITMP_FindSubmeshes_mC64BD7183A342209EA54542B617D177E9F41CFEE (void);
// 0x00000DCD System.Boolean CurvedUI.CurvedUITMP::ShouldTesselate()
extern void CurvedUITMP_ShouldTesselate_mDBEA54B4612CCE6818B9B3417601D1D157DCE2DE (void);
// 0x00000DCE System.Void CurvedUI.CurvedUITMP::TMPTextChangedCallback(System.Object)
extern void CurvedUITMP_TMPTextChangedCallback_m6102698DD858BFCB0872B0D4C0781A0D3487D934 (void);
// 0x00000DCF System.Void CurvedUI.CurvedUITMP::TesselationRequiredCallback()
extern void CurvedUITMP_TesselationRequiredCallback_m883BCE664FDF049FC51A8833A517E59E9A92D974 (void);
// 0x00000DD0 System.Void CurvedUI.CurvedUITMP::.ctor()
extern void CurvedUITMP__ctor_mB5FB4AF340667B1A0705A225F9F4F2F34614D77D (void);
// 0x00000DD1 System.Void CurvedUI.CurvedUITMPInputFieldCaret::Awake()
extern void CurvedUITMPInputFieldCaret_Awake_m37D5B28657ACF2865189287B0F385EC96CB9E227 (void);
// 0x00000DD2 System.Void CurvedUI.CurvedUITMPInputFieldCaret::Update()
extern void CurvedUITMPInputFieldCaret_Update_m1732770BE61A7FE8BDF47D615A66870FA726F036 (void);
// 0x00000DD3 System.Void CurvedUI.CurvedUITMPInputFieldCaret::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void CurvedUITMPInputFieldCaret_OnSelect_mB4B67E5CEA6E8624C4972BCA983A052843290D03 (void);
// 0x00000DD4 System.Void CurvedUI.CurvedUITMPInputFieldCaret::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern void CurvedUITMPInputFieldCaret_OnDeselect_mBF8A14A16DBE7C1921D1A88FA4EC913DF623D6AF (void);
// 0x00000DD5 System.Collections.IEnumerator CurvedUI.CurvedUITMPInputFieldCaret::CaretBlinker()
extern void CurvedUITMPInputFieldCaret_CaretBlinker_mCE5460202CFB5609B10401374AA7E30ADAAB250B (void);
// 0x00000DD6 System.Void CurvedUI.CurvedUITMPInputFieldCaret::CreateCaret()
extern void CurvedUITMPInputFieldCaret_CreateCaret_mAEAD784D5650AC57DE14B46A1C5C06549B781E17 (void);
// 0x00000DD7 System.Void CurvedUI.CurvedUITMPInputFieldCaret::UpdateCaret()
extern void CurvedUITMPInputFieldCaret_UpdateCaret_m387C4C599DB34D7F97D3BE3D673FE7CA2DD63745 (void);
// 0x00000DD8 UnityEngine.Vector2 CurvedUI.CurvedUITMPInputFieldCaret::GetLocalPositionInText(System.Int32)
extern void CurvedUITMPInputFieldCaret_GetLocalPositionInText_m2CBB990A9AFC75561F2A4F7E7D704FC7DA17D6E9 (void);
// 0x00000DD9 System.Void CurvedUI.CurvedUITMPInputFieldCaret::CheckAndConvertMask()
extern void CurvedUITMPInputFieldCaret_CheckAndConvertMask_m8BEA6B95DC990F6ECD646BB84D02C1323935B8F3 (void);
// 0x00000DDA UnityEngine.Color CurvedUI.CurvedUITMPInputFieldCaret::get_CaretColor()
extern void CurvedUITMPInputFieldCaret_get_CaretColor_m0B1A30AB4E7D9180E418E91BE722E7C493E2C5B3 (void);
// 0x00000DDB System.Void CurvedUI.CurvedUITMPInputFieldCaret::set_CaretColor(UnityEngine.Color)
extern void CurvedUITMPInputFieldCaret_set_CaretColor_m139F5BCA647DBD2E9A652ACBCC004A3A2DEC0E7E (void);
// 0x00000DDC UnityEngine.Color CurvedUI.CurvedUITMPInputFieldCaret::get_SelectionColor()
extern void CurvedUITMPInputFieldCaret_get_SelectionColor_mC17554DB7CE4F1963F75CAA209FC5E1FFCCB580E (void);
// 0x00000DDD System.Void CurvedUI.CurvedUITMPInputFieldCaret::set_SelectionColor(UnityEngine.Color)
extern void CurvedUITMPInputFieldCaret_set_SelectionColor_m9E3862E7A15164F95C0DBE2B97246DB13AC99905 (void);
// 0x00000DDE System.Single CurvedUI.CurvedUITMPInputFieldCaret::get_CaretBlinkRate()
extern void CurvedUITMPInputFieldCaret_get_CaretBlinkRate_m80A49018D407C1D03CB447796266B2EADAD0E9B5 (void);
// 0x00000DDF System.Void CurvedUI.CurvedUITMPInputFieldCaret::set_CaretBlinkRate(System.Single)
extern void CurvedUITMPInputFieldCaret_set_CaretBlinkRate_mD3A7D152AC19FEA941995849C279A1EE09867257 (void);
// 0x00000DE0 System.Void CurvedUI.CurvedUITMPInputFieldCaret::.ctor()
extern void CurvedUITMPInputFieldCaret__ctor_mDF9A498C1B99D38F5859DE58122597C7644D4EA1 (void);
// 0x00000DE1 System.Void CurvedUI.CurvedUITMPInputFieldCaret/<CaretBlinker>d__10::.ctor(System.Int32)
extern void U3CCaretBlinkerU3Ed__10__ctor_m0672D5EA4DFF81957D859899F96AC00C03742AAF (void);
// 0x00000DE2 System.Void CurvedUI.CurvedUITMPInputFieldCaret/<CaretBlinker>d__10::System.IDisposable.Dispose()
extern void U3CCaretBlinkerU3Ed__10_System_IDisposable_Dispose_m755CF49521B55B700882B83C41E873E163423ECF (void);
// 0x00000DE3 System.Boolean CurvedUI.CurvedUITMPInputFieldCaret/<CaretBlinker>d__10::MoveNext()
extern void U3CCaretBlinkerU3Ed__10_MoveNext_m3213C25289AB7479A93A27755562135C5668AF18 (void);
// 0x00000DE4 System.Object CurvedUI.CurvedUITMPInputFieldCaret/<CaretBlinker>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCaretBlinkerU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7D11CE3F3556DD5D7F1BE36E95C18F09F8EC2D7 (void);
// 0x00000DE5 System.Void CurvedUI.CurvedUITMPInputFieldCaret/<CaretBlinker>d__10::System.Collections.IEnumerator.Reset()
extern void U3CCaretBlinkerU3Ed__10_System_Collections_IEnumerator_Reset_mA51EAF61D997DEDC7522E9CAB2E1A8EA1375E2E4 (void);
// 0x00000DE6 System.Object CurvedUI.CurvedUITMPInputFieldCaret/<CaretBlinker>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CCaretBlinkerU3Ed__10_System_Collections_IEnumerator_get_Current_mCD80F8F62871C6414C4252EFF6DE75B029A8C2A1 (void);
// 0x00000DE7 System.Void CurvedUI.CurvedUITMPSubmesh::UpdateSubmesh(System.Boolean,System.Boolean)
extern void CurvedUITMPSubmesh_UpdateSubmesh_mC34E6DC08013F0BF07F5255969C9FB0AF7D07D9A (void);
// 0x00000DE8 System.Void CurvedUI.CurvedUITMPSubmesh::.ctor()
extern void CurvedUITMPSubmesh__ctor_m07C154FE7BF5110D1DC6BBE1A4AAAC51916BAAC0 (void);
// 0x00000DE9 System.Void CurvedUI.OptionalDependencyAttribute::.ctor(System.String,System.String)
extern void OptionalDependencyAttribute__ctor_m494B803BF32C02FB5ABE78EEE7FFB0FD3360FD92 (void);
static Il2CppMethodPointer s_methodPointers[3561] = 
{
	CUI_CameraRotationOnButtonHeld_Start_m30296B8C7411400158611D7529DA36871CA1C08A,
	CUI_CameraRotationOnButtonHeld__ctor_m8171E5BE364721612E199DEC96FC29B55B2C85F7,
	CUI_MoveAlong_Start_mED9D864632FB694902CF7A79840C0DBD62276B6C,
	CUI_MoveAlong_Update_m9A2DC00C717B9D5AD62CC9C39B6520ED21ED0697,
	CUI_MoveAlong__ctor_m7DB256B85D07BB4EBA7BF57D94C4FFC2C0773DAC,
	CUI_MoveHeartbeat_Start_m1B77A93B8C10B65EF9DD39E375344B2C0706A070,
	CUI_MoveHeartbeat_Update_mCE8CA7B51BA29FACAC94747FCAB835939D708F2C,
	CUI_MoveHeartbeat__ctor_mCF476DDD167DA09AA9DE860EC05B3DC656D3B7BE,
	CUI_ShowParentCoordinates_Start_mE18C4A26580030AEAB52BA3C9383FB7FF1400229,
	CUI_ShowParentCoordinates_Update_m57ABED7A44EF3044621912AD32E63D6E0F326848,
	CUI_ShowParentCoordinates__ctor_mA672F201FA44A897396BB6CE9E0AFED694B02D94,
	CUI_rotation_anim_Start_m0CAEB6FEC4FC7B02DD1860698E01E337D85342B5,
	CUI_rotation_anim_Update_m4CC2C3F01E8AD5D0FBD92E4114E2729942E32FD7,
	CUI_rotation_anim__ctor_m3DF66001A411C57DDB9B29CC904CC9B1B73D207E,
	CUI_touchpad_Awake_m20432FA4CB5FAEF5ED50CC322658D664223CC2D4,
	CUI_touchpad_MoveDotOnTouchpadAxisChanged_m331119E1F7FBC1ADC238F9D25D87F526B42E3B6B,
	CUI_touchpad__ctor_m214DEE14E28C03DF6BEBB98F281030E961E57622,
	CurvedUIInputModule_Awake_m459F6CE5FCA6237742503D156E33FB307BB75A89,
	CurvedUIInputModule_Start_m40A7CE503CF7F076EC4C870C3DA74CF133DDEEE1,
	CurvedUIInputModule_Update_m361E8DA033404BEA21194F20B291B1CAE84FCC4C,
	CurvedUIInputModule_Process_m111FD300F4935C6B216D4F34890A0D1BEE0409C0,
	CurvedUIInputModule_ProcessGaze_m62BA5A85D37E3C4BAB8B8692E64A0B8B16712765,
	CurvedUIInputModule_ProcessCustomRayController_m1CD280750DAF41039906B20351137839779D8C34,
	CurvedUIInputModule_GetMousePointerEventData_mE9B50FB0C07545577E80E8F6D2ED84086075422C,
	CurvedUIInputModule_CustomRayFramePressedState_m1431D59909516A341EA9D3E2FB1B147B661D5782,
	CurvedUIInputModule_ProcessViveControllers_mD9D820D27E1ADCB7AF2237D0ABB52027BD30CDD1,
	CurvedUIInputModule_ProcessOculusVRController_m21223AA14E4C5AD193596AB7DFBFFADB994C826E,
	CurvedUIInputModule_ProcessSteamVR2Controllers_m5EEDE3D7AAB8A885E3C703B783368D7E4CE3E009,
	CurvedUIInputModule_ProcessUnityXRController_m8CEACBEC138A601094325A6F40B6A4CC6746160F,
	NULL,
	CurvedUIInputModule_get_Instance_mC9DDA54886BF85942DC1550EA2E0402870DB2A6B,
	CurvedUIInputModule_set_Instance_m51EF0229D6836DF3FC59FA0B0691C5EEE4D6D12A,
	CurvedUIInputModule_get_ControlMethod_mC512EF503B901ADFB66F30F116688DE6A7DFCC4C,
	CurvedUIInputModule_set_ControlMethod_mABF3F935318DCA34E13DD3D3C3191660B6023F89,
	CurvedUIInputModule_get_RaycastLayerMask_mAEEA2CC7326CA09868F82C89312548481D0B2298,
	CurvedUIInputModule_set_RaycastLayerMask_mB5424866EC6399E035CCF596C292A232D2EDA333,
	CurvedUIInputModule_get_UsedHand_m79EBF036F5EDD2BAAAE61122B1157551C529C8D0,
	CurvedUIInputModule_set_UsedHand_m009D73A0B802C0E268900E01C0B0C279C9CF80CB,
	CurvedUIInputModule_get_ControllerTransform_m3784A551AD38C88CFA1BB4CA46D6D367FDAAC94A,
	CurvedUIInputModule_get_ControllerPointingDirection_mC6CC7A3EFF829A3967F557EB52EE5B5AAB00CD3F,
	CurvedUIInputModule_get_ControllerPointingOrigin_m0AD6C220EDE79AC6D03E3D66A8322080B34D5720,
	CurvedUIInputModule_get_PointerTransformOverride_m1E3D241ACFC1D6E757E8AA210BBBD230175C0073,
	CurvedUIInputModule_set_PointerTransformOverride_mD46F1D5865E0CCC7D4FF3A7F676EEC8AF0EE80E9,
	CurvedUIInputModule_get_CurrentPointedAt_m9E544751C0B26B9A6455D018597B7B7E600F9154,
	CurvedUIInputModule_get_EventCamera_m55A6F530658A878D1A470143193927B2ABF8FFD8,
	CurvedUIInputModule_set_EventCamera_mFA4958069001525D55C1A7D9C508C146E4CF2CE8,
	CurvedUIInputModule_GetEventRay_m5597EB634A051D1B0FAD09B52BCE14A83E01AA58,
	CurvedUIInputModule_get_CustomControllerRay_mD775162A32166B7A27EED89C4AD3D0733351A363,
	CurvedUIInputModule_set_CustomControllerRay_m286E2D79D8A2E646CA98C35A9CF05BE5AEE19313,
	CurvedUIInputModule_get_CustomControllerButtonState_mE0C18DB1EB150E5964A9EAF418E24C05C79ECC2A,
	CurvedUIInputModule_set_CustomControllerButtonState_mBA5989AB6711974989B046E2870161B59AECDE3E,
	CurvedUIInputModule_get_CustomControllerButtonDown_mA5EA5E83C71D5DE523EDBBDFF63DD7483C2537F7,
	CurvedUIInputModule_set_CustomControllerButtonDown_m57603955FB8A6508B80FB3EDA44C08C51FDD8464,
	CurvedUIInputModule_get_WorldSpaceMouseInCanvasSpace_mE3B7299C01A132F0B367A229F83849142947B90A,
	CurvedUIInputModule_set_WorldSpaceMouseInCanvasSpace_m592AF804859C8A76EE6186EEA766CDB11574C2EF,
	CurvedUIInputModule_get_WorldSpaceMouseInCanvasSpaceDelta_m8353EE3480DB7777F92F4FF16FD3A41BE65C5E14,
	CurvedUIInputModule_get_WorldSpaceMouseSensitivity_m47FD779EED3A1C4C4DFC020254DF87520F776D44,
	CurvedUIInputModule_set_WorldSpaceMouseSensitivity_mB4EEBB97515A2F87A5110A6AFC29739845999582,
	CurvedUIInputModule_get_GazeUseTimedClick_m970ABA37DE32C3DF93F5759AB277484E8CA303A9,
	CurvedUIInputModule_set_GazeUseTimedClick_m208635934C547289449741B5090E98950F528944,
	CurvedUIInputModule_get_GazeClickTimer_mE2C0614954C9956711F286589B74FE10BAB9C21C,
	CurvedUIInputModule_set_GazeClickTimer_m787E445F77BAE33B13B0AC75982C68C98D1F37A8,
	CurvedUIInputModule_get_GazeClickTimerDelay_mF176D90CBFEB45FEB704F83EBD421459CDE3AC1D,
	CurvedUIInputModule_set_GazeClickTimerDelay_m08937B805F85A87F2755D13A99614EE148F3D533,
	CurvedUIInputModule_get_GazeTimerProgress_m0B25A06E1E5F9C52D7B71D380C86C70DBE110AB4,
	CurvedUIInputModule_get_GazeTimedClickProgressImage_m1B8B52C8DFECDEABF937662AF5568BB25788C012,
	CurvedUIInputModule_set_GazeTimedClickProgressImage_m39FB7BD7B8C2CB36120FE9910AFF01EFBDC26172,
	CurvedUIInputModule__ctor_m97851764FD907046199A7B0FB2181224D0153259,
	CurvedUIInputModule__cctor_mABCC99D3E956CD146CF7562F29222DB7E9D10E03,
	UnityEventFloat__ctor_mFF740EC4E18228A93E8F8B0FFAF5517C3C436522,
	UnityEventInt__ctor_mA0D74C4DA52EDEE0B64277B447BA1497B85589C1,
	UnityEventBool__ctor_m750449D2D0FC1AE986D7695E90625E5951435B5F,
	UnityEventString__ctor_m05DAE652CAD13F1AD4ECBC288E11380EC61B3EDF,
	UnityEventByteArray__ctor_m3E4AAE33E671513548067B0C29F65212D3E5405F,
	UnityEventFloatArray__ctor_m360DF59A91ED20BDEFAC68A7F985A1DB53163987,
	UnityEventTexture__ctor_mF6B125236323AD42B8AF7BC65546B89FF77629AE,
	UnityEventTexture2D__ctor_m8604648B368E08DD20E834BE7277BD05C5232422,
	UnityEventRenderTexture__ctor_m46862E0E8722739A2D6C008F6B0D191E30EFCC66,
	UnityEventWebcamTexture__ctor_mB672C8E341F209D2BD6DF002DAAD75CE6E894FBA,
	UnityEventClass__ctor_m55E05F3FC82A4B4BD632C6CA0B4A686390301DC6,
	Loom_get_maxThreads_m45F4C5CDCCA9EA30915641C7D48A8A1B312CDD2A,
	Loom_get_Current_mB4398B65D91C82F7B1700229D366D233DE6D5292,
	Loom_Awake_mDC841F2F83D833F3CE27C3A8CD7CBEAF0830D006,
	Loom_Initialize_mC9A387246A6232763DD1DDAE10CA8F3BC6DF44F4,
	Loom_QueueOnMainThread_mC6481FC8BB376138713AFE2B0F847B989FD04799,
	Loom_QueueOnMainThread_m52456F68B754CFBAF8ED1EC2831B60DAD50B8C79,
	Loom_RunAsync_mF19ED64A5D75CBD0C9150286CEB6EB97CD3A8CF5,
	Loom_RunAction_mCD028185DE1B5145E0FC75CAE05C40AB02DEE520,
	Loom_OnDisable_mF74455FCA2989CBF239B670FAFBA486C56C17CAB,
	Loom_Start_m3D61391AD1D7486F7955ABFDADD02DDEC12582F8,
	Loom_Update_m250490A6D28C0384C57E1D62E4E2E11E9DD5B8F5,
	Loom__ctor_m6E368A4B8D9DF24AAD415BD8C82218416C6E8CAF,
	Loom__cctor_m27488AF9914F4DAB513FC3566F38776051FF5C85,
	U3CU3Ec__cctor_mB83C3D68BCDA1E58351A40057BE43CCCB0E30702,
	U3CU3Ec__ctor_m0EE64F19B66C14511BE1D90EE7D8C21B22C5DF2E,
	U3CU3Ec_U3CUpdateU3Eb__22_0_mA9D277BC34D1B562E09AD8D99662EC0172BFF73A,
	EnergySavingManager_getGyroChanged_m07F73DF70A51F47EE86F9A69F4607DD802388864,
	EnergySavingManager_getAnyKeyChanged_m2AB138D86D9DCA1A9BEDE38F43D2D0701F77830B,
	EnergySavingManager_getTouchChanged_mC720722117AACE00171FC8994C13764F728039F2,
	EnergySavingManager_Awake_m90F344C979D39343AB6B36A56204280485089C77,
	EnergySavingManager_Start_m8455DC19313A7FF0EE1817EA99EDF6F04E31D4DB,
	EnergySavingManager_Update_mFBC12CCE7FA59B98DEB51178E648F142A88E430F,
	EnergySavingManager_Action_ReloadScene_m4EF9676A6BD46BDADDAAB7454A4B49A943C6D6B4,
	EnergySavingManager_DelayReloadScene_mAEB56F9BDDABE85C234AB20E51B7AC97ECF89DF8,
	EnergySavingManager_Action_QuitApp_mA06AE0E144A29BF58C5F142106AA5ADB425BB775,
	EnergySavingManager_DelayQuitApp_mEBF1F5293C9099BF208F9F39C9FC3F74AEAC8CD8,
	EnergySavingManager__ctor_m0A9D34308EB71860EF4748DB70E3032036A526EB,
	U3CDelayReloadSceneU3Ed__40__ctor_m378410F6A5C31E902391874A3B9E5575B3B47B20,
	U3CDelayReloadSceneU3Ed__40_System_IDisposable_Dispose_m3B3AA69F100D170E2074348E2D09995D587E2255,
	U3CDelayReloadSceneU3Ed__40_MoveNext_mEDAC1CD91D991D6237B37DCF66AD265EF183AA2D,
	U3CDelayReloadSceneU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0596AA92419A6E79BDE7EF5C75FD5B91836ECD10,
	U3CDelayReloadSceneU3Ed__40_System_Collections_IEnumerator_Reset_m4C23331135E8E94BE05C1C7FF40BA5BAB83F6D4A,
	U3CDelayReloadSceneU3Ed__40_System_Collections_IEnumerator_get_Current_m2F19C025F98F1EF3C47A3E00BD88A14E1160EAB4,
	U3CDelayQuitAppU3Ed__42__ctor_m2982F7BD45D512C22B8071F2A16CD7BC0AEC5D3E,
	U3CDelayQuitAppU3Ed__42_System_IDisposable_Dispose_m4ED41B74ADAA3AF5E127CD18D01851957230A57D,
	U3CDelayQuitAppU3Ed__42_MoveNext_mB96130378F55019EF308216C5027E466F724D3DE,
	U3CDelayQuitAppU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB3215365FC516F5429FC10B1470F7AF8DF2FCC8,
	U3CDelayQuitAppU3Ed__42_System_Collections_IEnumerator_Reset_m10AEA39667252F8C0366EE02D14D0FFEB53C87E6,
	U3CDelayQuitAppU3Ed__42_System_Collections_IEnumerator_get_Current_m3AEFD1FB81A442FD1677D5653F954536DCA624D8,
	FPSManager_Update_m0522EFA7F4F133BC59618DFA4DFAC12C2087969C,
	FPSManager_Action_SetFPS_mBC76DAE4286ABF5D9BD9EF6CA162D8686071F564,
	FPSManager__ctor_m2E6A21D5CEDD1BA8884A37F175EA3D71BA883093,
	DemoObjectAnimation_Start_m61B26A3EEFEE316B23A692EB9819B0936A33B5AD,
	DemoObjectAnimation_Update_m0BCB030740300DA20DF73C47E1C680BD50CAD216,
	DemoObjectAnimation__ctor_m4E17FDCB4EBA99D7C84551E9D42244E8AEF105C6,
	DemoScreenshot_Reset_mBE57397437DFA75D8960EFB2962650F67307C8EA,
	DemoScreenshot_Update_mC462597259A047ED1AE56828BDDE993DC74E42AD,
	DemoScreenshot_SaveScreenshot_mBA35D8AD7F8B3F7BEF357653309DDF826C3BB955,
	DemoScreenshot_SaveJPG_mDD3390BF87787F36FDDFB828F593EDE5B99E320B,
	DemoScreenshot_SaveScreenshotPano_mB0F592F9B03FE248F5A8AE1B95B06535936F1964,
	DemoScreenshot__ctor_mBA024C4EDF18B31CCA84DC824CADE928BA036570,
	U3CSaveJPGU3Ed__10__ctor_m80B017FB7621082559EE1234D41D7B212334D6AD,
	U3CSaveJPGU3Ed__10_System_IDisposable_Dispose_m6242FD59BB6674148EF1EE49B9BAD2989B3A891E,
	U3CSaveJPGU3Ed__10_MoveNext_mA8014B6A1489057D93EAA6D564968F0B67A47905,
	U3CSaveJPGU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9A0AAB82531970F1329EA3AA22821C7522D0527F,
	U3CSaveJPGU3Ed__10_System_Collections_IEnumerator_Reset_m48FAB3D5B99C2D5767FBC8C88D78078B971E8D46,
	U3CSaveJPGU3Ed__10_System_Collections_IEnumerator_get_Current_m366D57B80A453196657D362CA20A1D7773F1F179,
	U3CSaveScreenshotPanoU3Ed__11__ctor_m8FAF53D59B7743762915EFC238292666D39F461D,
	U3CSaveScreenshotPanoU3Ed__11_System_IDisposable_Dispose_mCA343FD70613D0F0B5FBFFD4CC01D208CB2A7ECE,
	U3CSaveScreenshotPanoU3Ed__11_MoveNext_m6D5CEB4D0CB57C64E1DF0323266D39FEBFD0D899,
	U3CSaveScreenshotPanoU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3291B88A4B56AC7F8B4A38146C4265838FCD4058,
	U3CSaveScreenshotPanoU3Ed__11_System_Collections_IEnumerator_Reset_m1E37A2C1B0FFB2103292655CE06EAB86055855E5,
	U3CSaveScreenshotPanoU3Ed__11_System_Collections_IEnumerator_get_Current_m4831A82DF401B704EF1149B943E6457A760BDBB7,
	FillCameraBackground_Start_m83A28EE7F248150FB6DF327464F96375F753CE70,
	FillCameraBackground_LateUpdate_mE11227CEA4E886E71D218E049DE251E022233995,
	FillCameraBackground__ctor_m1DD54F12BB631FD917CF7069BE7A4799137A6168,
	GyroController_Awake_m9DE28977F11F876B392510E0A106EFF171DF6670,
	GyroController_Start_m74034C95FD00F13D038ABC9B1E7D20DD54E315FF,
	GyroController_EnableGyro_m4E6CA669420E086E9D1798093E40D1D1565827F9,
	GyroController_Update_m7DC451D70AC3512B5EC4A9A77EDE97D546545841,
	GyroController_AimingOffset_mCFC1D9E3581EFEC7A69D889E90F45553A92CAB4F,
	GyroController_MouseDragRot_mC021B1138BDCFAE31801668F2C6F359AB41CEC94,
	GyroController_TouchDragRot_mAEB8AA78851F7B15648A929FCC5267CCFE09A41C,
	GyroController__ctor_m8F76C3F9F945BDE364299A3C3E23A92E44B57442,
	_UnityEventFloat__ctor_m3C7D96090897BCAB60F0D082A4AE1D263BC41C83,
	PointerEvents_Start_mEAE7F0645485260A2956330EFF57358846B59763,
	PointerEvents_Update_mB5E4941825808D9B099210CE3A95C551CF3E30CA,
	PointerEvents_OnPointerDown_m72CD1D5401C5938F6E2A221C94C772D99E8F5AEF,
	PointerEvents_OnPointerUp_mC2DA25B8BC3E0C4C3EBC979491114093EC70E986,
	PointerEvents_OnPointerEnter_m16550B1C48E09257AB2C0555F508344E48BDF6D5,
	PointerEvents_OnPointerExit_m6A148D6CDAE4DF94D188B53AEED69DDF6C4F30CD,
	PointerEvents__ctor_mC7FF3430D7AAAEBCDA95C0723A59B2C3FE292441,
	TouchVisualization_Start_mE98CFC665943969CA267FA257A8DD00D675AF833,
	TouchVisualization_Update_m073D885C35E8E17C453FFDE621516C80CF31F585,
	TouchVisualization__ctor_m73E55042EACC2476F91AD811F756D3D529C7EEE7,
	OffScreenTrigger_Start_mFA3F5D962CC09261A9ED9F97304303A5D16DEDD8,
	OffScreenTrigger_Update_m8B50798E9369E64B38A1B5D8418576BBA8F2EB9E,
	OffScreenTrigger_Action_offscreen_m309DEA55FA4E1AC8763EBC43F489816678358F87,
	OffScreenTrigger__ctor_mEF41D1BAEF64AE177D8B860AEE3E39FF5CD6189E,
	WorldToScreenSpace_Start_m02CD9588E96DE188C6C7AF4BA467CF7E4AE44D4E,
	WorldToScreenSpace_Update_mA51732AEF18A583B410F4C5AFB11B51C7C0EFD0A,
	WorldToScreenSpace_InvokeOnScreen_m2518C7AA233A5C5526E909F9E1568C6B0EF8DA20,
	WorldToScreenSpace_InvokeOffScreen_m032EEC8A29DD2B163EB6811DE9444C8D40E2DDF7,
	WorldToScreenSpace_OnDisable_mBD6D5A879F6180931A4898C1253825496882F1E1,
	WorldToScreenSpace_OnEnable_m6DD74E6045A5E0AB9CBC4961187462E0814BEE52,
	WorldToScreenSpace_DebugTest_m333E5F31F445884C07702D81B95F89ED658517F9,
	WorldToScreenSpace__ctor_m697DF27F1AB8AEA6D847524FD22698F489C395E5,
	ZoomManager_Start_m5564B07802ECD1890FD37C118660F9341DB70A78,
	ZoomManager_Update_mFFB0DB68EDECC1CEE73F6E9E61F6FD357D479AC3,
	ZoomManager_GestureZoom_m27472A4680B46B9EE14AFBDEC415E026A0A4DB5A,
	ZoomManager_ZoomIn_mF9DED265600855232D21C366D41872E02BA0A10A,
	ZoomManager_ZoomOut_m30DE34CC9DECAF76CE557B460C65890B50F00BA5,
	ZoomManager__ctor_m2DC1B15361F9233ED42BE447B6FED5D57C6DC1FE,
	SceneManager_helper_Action_LoadSceneName_mE45650E0ED0D6B19B9622967E9F9EA814756ADE7,
	SceneManager_helper_Action_Quit_mAA22B68E3E7D071D9B1F083DE0BB4F806741DFD3,
	SceneManager_helper__ctor_mD59AEDC0E2B8F355D19F9385C530AE69F190FF8A,
	TargetProjectionMatrix_Action_UpdateFOV_m15AB3F37BA03F650029246E7BB3E932318982552,
	TargetProjectionMatrix_Action_SetAllowUpdate_mB1330AEA23BB4D0A116F9806F0EC2667C1A62E1F,
	TargetProjectionMatrix_Action_SetForceDisableUpdate_mDF1165DE2FCCD1B816C77EC184733E61B3D5AED6,
	TargetProjectionMatrix_UpdateProjectionMatrixLoop_m9BBD15A81609B32704AD6F714CBB2FFF25F0F02B,
	TargetProjectionMatrix_Start_m4859CF1A76FB4A432DCD561251C907A6D22BE2BC,
	TargetProjectionMatrix__ctor_mFCA533A6831AC695D313F3B6600804F52D226894,
	U3CUpdateProjectionMatrixLoopU3Ed__10__ctor_m0C230674C5D40700A51231AB15CD4362B42129F5,
	U3CUpdateProjectionMatrixLoopU3Ed__10_System_IDisposable_Dispose_mCC990D854F67E1FEAB8C5942350714360CAA6C4B,
	U3CUpdateProjectionMatrixLoopU3Ed__10_MoveNext_m60419720F9995FAD7FF38AEB23CF2CD5660607D3,
	U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3DC7F959E369D83651E676D2FFCF66AE85A2C36,
	U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_Reset_m70A89E669D02A74E6DAABC101346075B8F0F3F25,
	U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_get_Current_m7071709350C18754652B239E532208352E7E4601,
	WebcamManager_Action_useFrontCam_m96E70AB022B4E8FAC541A9D225DC85B01A3D0F62,
	WebcamManager_RestartWebcam_m606BBE4978CDDE71EFC322393B370501657C3288,
	WebcamManager_get_WebCamTexture_mDAA40C51424298F9C63A01CA51B61BECC6C9407F,
	WebcamManager_get_IsFlipped_mC198CF5C254EAD69D7F688DDCF103C9C2839C2C6,
	WebcamManager_Start_m19A031E1F1F000DD3083AB2A9D97D0CA740AFBC0,
	WebcamManager_Update_m70BFF30AF71CB1ECE20E1905878A8D0C5A63DE81,
	WebcamManager_OnApplicationQuit_mBE25BE76FD98A3709C1010E6D506E06BE9FA5A63,
	WebcamManager_CalculateBackgroundQuad_mA707C37E7AF9177F385C64094F4CD2DA99F6B6F7,
	WebcamManager_Action_StopWebcam_m3B387E8BCA377260DF46C7189AD053032E173C1A,
	WebcamManager_Action_StartWebcam_mE2758C0B9DC4BF39E7BBA1405C726E8C54C24198,
	WebcamManager_OnEnable_mF52E952C81D735CB23AC17B768CBDEC35E0B4A47,
	WebcamManager_OnDisable_mF7D0A167F8E6D6765DFFB1DB4658F0D43E1BD88A,
	WebcamManager_initAndWaitForWebCamTexture_m6475CE2D9148E36CF76C11720BB8EBC75C3E8494,
	WebcamManager__ctor_m1C5A3E28015B0ACB88EAA02428040165C9B0BD76,
	U3CRestartWebcamU3Ed__8__ctor_m28872094CCAA094B43F22696A627F91DB681702E,
	U3CRestartWebcamU3Ed__8_System_IDisposable_Dispose_m64D07C3BFF909EE7CCCBBF13483490CDFD984122,
	U3CRestartWebcamU3Ed__8_MoveNext_m0073A78F7D1343C48FDB671B463ED829CDA3C822,
	U3CRestartWebcamU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA6D2C7491AB5B6855FC00F3F90253FC3DB64DCC3,
	U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_Reset_mA3A746F18872D6F97B3CC5A4977A1F692BA7E8EA,
	U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_get_Current_m0D001B2B663BA976A2D539566D366D6BCB620327,
	U3CinitAndWaitForWebCamTextureU3Ed__34__ctor_m52FA2EEEB153A88A5AA5BAA71AB02EA1EF8C4D7E,
	U3CinitAndWaitForWebCamTextureU3Ed__34_System_IDisposable_Dispose_m2727360BB45FFE1C4D81B8AF9B1B04400CD74F47,
	U3CinitAndWaitForWebCamTextureU3Ed__34_MoveNext_m42785171471996265574A92AFC79AFFB0ADEFE23,
	U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03936395905AC4CD42FCADDBCFDC81AA99AB1180,
	U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_Reset_m40B8DC7783ACD7CDF9D80B46263092CF7CCD2C02,
	U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_get_Current_m1B82795B8517450D9B2E0420375A1E3D234BE130,
	AudioDecoder_Start_m4A47FE23BB46592FEFB8827CBC0ECE4ABE8ECDC0,
	AudioDecoder_Action_ProcessData_m4E6B0BDDDE0AAB0C6CE8A9AD25E72347FDDEF241,
	AudioDecoder_get_Volume_m474CC6458E9DBF6C3E05B1A20D89D37A3FFA0EDB,
	AudioDecoder_set_Volume_mC3D8A106DE0D8B8804AC0BF45D0F1AA3187337FF,
	AudioDecoder_ProcessAudioData_mCD60D45A102AC845C95F710E97ED9AC84764608D,
	AudioDecoder_CreateClip_mD97DF673B8DDAA7643B53CE9FEB39BAB86BE5689,
	AudioDecoder_OnAudioRead_m1794D32AD1D7DB0ECA7AF488FD848B26C2F30915,
	AudioDecoder_OnAudioSetPosition_m6188583CB8F1CB8E8C3B1BA6283B6B259709B919,
	AudioDecoder_ToFloatArray_m8E8BA27CA0333C1DAA52FD0067109FC5AFA24439,
	AudioDecoder__ctor_m0BBC264F0BE17A77E373B38972CC77292FC76DB4,
	U3CProcessAudioDataU3Ed__19__ctor_mE0848B84AE84FDE79B6FE343E3D683EAA49E7970,
	U3CProcessAudioDataU3Ed__19_System_IDisposable_Dispose_mD25A519AED3454A362024DFC8D2ED00C90718B84,
	U3CProcessAudioDataU3Ed__19_MoveNext_mC6B474AD5047D98A69C3D5BA6F10AC0F22AABBD5,
	U3CProcessAudioDataU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD07EFA8A3BBF9FB0487C83D2C8F5CFCB6BC8D8F4,
	U3CProcessAudioDataU3Ed__19_System_Collections_IEnumerator_Reset_m1AC29B6F730B329545A2840B093F62B10215D6E4,
	U3CProcessAudioDataU3Ed__19_System_Collections_IEnumerator_get_Current_m7D17C6D80A40F7A84F86140A78B47A3A680A64D6,
	AudioEncoder_Start_mC6E27FB33C1A93F4E0AAEA967F0330D62D494ACA,
	AudioEncoder_OnAudioFilterRead_m4B02FEBA050FF299DFEF08DB2EB85BCF67A62F0F,
	AudioEncoder_SenderCOR_m927CA7B10E5E48E0E83B7E0E88BBA863ADD50878,
	AudioEncoder_FloatToInt16_mA700BD6EF5E0C453B7039DCC969472464FBBA851,
	AudioEncoder_EncodeBytes_m5F714A2D8AFBBA5277E66BBFC6D8BA03B1882A26,
	AudioEncoder_OnEnable_m11999C31B52870FC6809BF7AD22834E2A09479A7,
	AudioEncoder_OnDisable_mD3A71D2BD0DF0FD4345D1E6CBAB7058A370408B1,
	AudioEncoder__ctor_mE955CC6B5EC97CA461D6523FF7A89131C07B8A9B,
	U3CSenderCORU3Ed__22__ctor_mA784695FBBA952AEDA2F1836217E9B9532533B30,
	U3CSenderCORU3Ed__22_System_IDisposable_Dispose_mFC00E9099AB61671A87DC369640809AB166C9B93,
	U3CSenderCORU3Ed__22_MoveNext_m6DCC893FF65CBDC6CA8E93EF0366A9E428171743,
	U3CSenderCORU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79A57D218C729C4CBB8F88B7771BF63A351DD582,
	U3CSenderCORU3Ed__22_System_Collections_IEnumerator_Reset_m47EF41E4C31BD26B8B93922AD1CFBEEC2D43F057,
	U3CSenderCORU3Ed__22_System_Collections_IEnumerator_get_Current_m3CD170DF647398653FD6BC8C76779BF24305CFB5,
	FMPCStreamDecoder_Reset_mD516E2837782C0998AA13DBD160B69CA36765313,
	FMPCStreamDecoder_Start_mD154DAB53A3D993DCAA6643C3065480826BFD594,
	FMPCStreamDecoder_Update_mB513E3EBCEAE9931F4EC96A44DA39C3A7B28E245,
	FMPCStreamDecoder_Action_ProcessPointCloudData_m9AE4E232B791A9A13EB1EB6FD7B2432340B78F50,
	FMPCStreamDecoder_ProcessImageData_m462C8F6C2E0CFFE88CC4C7348CFDFC53B95382E0,
	FMPCStreamDecoder_OnDisable_mF185D10601FCC5F39D5939E2D27665B48F7CEC88,
	FMPCStreamDecoder_Action_ProcessImage_m9DF4F10AC003E9D116CA53446641969BE0D2D757,
	FMPCStreamDecoder__ctor_mE90CEB60B0B3E3E41F5699E046603FCCD8FE257E,
	U3CU3Ec__DisplayClass28_0__ctor_m65D7C3C6C9BADEA6DCADB46D1CC86A9AA1B6B45B,
	U3CU3Ec__DisplayClass28_1__ctor_m1B611E718B7F8879DAE5C1D87C4217673B75BF80,
	U3CU3Ec__DisplayClass28_1_U3CProcessImageDataU3Eb__0_m6E156A0A7C817E9FA82241C2F69668A787A58D2C,
	U3CProcessImageDataU3Ed__28__ctor_m65E9267CA3923A1ED5273964813B34E414E6C1A8,
	U3CProcessImageDataU3Ed__28_System_IDisposable_Dispose_mB132AB81D7B361538A451E56DE6288BB00B415BC,
	U3CProcessImageDataU3Ed__28_MoveNext_m8636F87D9C47E6BD28A5381F5A4DDD8D45869B63,
	U3CProcessImageDataU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1CB91832319CBA52A62C70735673C95257DC1CA,
	U3CProcessImageDataU3Ed__28_System_Collections_IEnumerator_Reset_mFEDBB74A6DDD44A49D4215E3BA8D403DE3B85009,
	U3CProcessImageDataU3Ed__28_System_Collections_IEnumerator_get_Current_m24D648EBC336DA7161885D872B2308FDA4EE1EE5,
	FMPCStreamEncoder_Reset_m060C641D90DB243600DE6F6C690517E0EAB94279,
	FMPCStreamEncoder_CheckResolution_m9383BE69C4C0D139BD3231004E2DD69E8F14735A,
	FMPCStreamEncoder_OnRenderImage_mFDBC0F48C15582A61E93094FF1D09F0C7356AC03,
	FMPCStreamEncoder_LateUpdate_mEE1717ABC183EECB1B06F8014F8CB2FA77F33906,
	FMPCStreamEncoder_RequestTextureUpdate_m3BE507F6BB313CD894712B96CA114C9E68695689,
	FMPCStreamEncoder_get_SupportsAsyncGPUReadback_m54B799AF9C5A6523897FC4034DF29F3947B2F612,
	FMPCStreamEncoder_get_GetStreamTexture_m6170C92092A4E1BF2A8EDC55ECB8A9E3D776B9B1,
	FMPCStreamEncoder_ProcessCapturedTexture_m93ED4CDD6B12A4634112D011E1DF57FADB5623F9,
	FMPCStreamEncoder_ProcessCapturedTextureCOR_m06E07C7C35D38473F939CE051118329E708FE270,
	FMPCStreamEncoder_ProcessCapturedTextureGPUReadbackCOR_m17EC58D224FFFA01491BB4B14C7139CCD8CBF25B,
	FMPCStreamEncoder_EncodeBytes_mE685726FB5710F700D1DE50896F43383AE8B15F1,
	FMPCStreamEncoder__ctor_m30EDCF533329D9546E6A091E4B1CB062A078A8E1,
	U3CProcessCapturedTextureCORU3Ed__42__ctor_m15FFF09B7B159B87D28F5D33B144A2746818A1AD,
	U3CProcessCapturedTextureCORU3Ed__42_System_IDisposable_Dispose_m7729875E21C169DB7607CD7AA78FDCAA64EABD79,
	U3CProcessCapturedTextureCORU3Ed__42_MoveNext_mCA2A0F715D2889BDE77782D83D3DCB81EC59249E,
	U3CProcessCapturedTextureCORU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EDA088CC58BB139147565B228C736438F3D1535,
	U3CProcessCapturedTextureCORU3Ed__42_System_Collections_IEnumerator_Reset_m2CF3235A57BBD0185F62E97C5BB4B8D3FCE928E7,
	U3CProcessCapturedTextureCORU3Ed__42_System_Collections_IEnumerator_get_Current_m86506782F7D1CAA62F474DD37C2B8E4C39092B76,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43__ctor_mFD7B06F8C9C3225C53B4A570A00B4B411A0E194F,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_IDisposable_Dispose_m5AD15B51BE7D919588C1A8F64512C4D51D1B8614,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_MoveNext_mE382A215BD6B180696A1D6AC5F4131B82E716112,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD91424E3299A4B2722284CC4EEC2EF72A011B5F,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_IEnumerator_Reset_mDD289D3618486F23BC4AFFC98D55B0E0BAB41D63,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_IEnumerator_get_Current_m16B83FFBF70421CFEB212367B9AA144BAAE65173,
	U3CU3Ec__DisplayClass44_0__ctor_m2935EC52C8AAD44F8F55CA8EDB1ACDC824F63DEB,
	U3CU3Ec__DisplayClass44_1__ctor_mFA7BDA937A6B95CF50F55E89B53ED590D4F62E2B,
	U3CU3Ec__DisplayClass44_1_U3CEncodeBytesU3Eb__0_mEB50BEDB1F85AD02C80A1DE03D3D53BCB185F5F2,
	U3CEncodeBytesU3Ed__44__ctor_mB362F7B2E399D20CF9B1258BCDD198F8182C8678,
	U3CEncodeBytesU3Ed__44_System_IDisposable_Dispose_mC9163EBABB38DCA93DFFCA7EA69202082F854504,
	U3CEncodeBytesU3Ed__44_MoveNext_m412619D11160EBE0E6CE0CDFBDC855F347648ED0,
	U3CEncodeBytesU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFD0D1F5ADBBBF396B19D6D50AF3D49D56BC87EA0,
	U3CEncodeBytesU3Ed__44_System_Collections_IEnumerator_Reset_m0A6EC7E76BB5A9824356A83320ABC41C4BABC3AB,
	U3CEncodeBytesU3Ed__44_System_Collections_IEnumerator_get_Current_mD8957775782E2CDED6842E4BAC06BBFC114D72F2,
	GameViewDecoder_get_ReceivedTexture_mF67BA0855D0FCC63FDBA97286C6488DDCEFBA090,
	GameViewDecoder_Reset_mAB4D75283F20BB464F83A051C94DAE18FA94ABEB,
	GameViewDecoder_Start_m95CF1EA78DC41B6399E3B92DC7CEA639126CF794,
	GameViewDecoder_Action_ProcessImageData_m50EFF638F1E082E0A9F0FB2A4367F55E03DF26C7,
	GameViewDecoder_ProcessImageData_mF85E9BC44B1BF094D87F8FBFEA767AC4AD540658,
	GameViewDecoder_OnDisable_m768F881636B7601865898A1AC4BDEE99E29D639A,
	GameViewDecoder_Action_ProcessMJPEGData_mB9D368B96F984D54AB204F98F1F66E50218091D2,
	GameViewDecoder_parseStreamBuffer_m34A1C9414FE4C26E4117E64E0F78034E7E3D06A1,
	GameViewDecoder_searchPicture_mE9318AA1AF44DBA04ED25636A3EAA2CEC47EEB24,
	GameViewDecoder_parsePicture_m00E4058E8261EE7C7971F4DA44AC0B42D1149EA6,
	GameViewDecoder__ctor_m546F57E09CC15846414235547ED2BA8593D6F793,
	U3CU3Ec__DisplayClass26_0__ctor_m7131F0235948BECBB597F6170FDD17508A1D665D,
	U3CU3Ec__DisplayClass26_1__ctor_m51A8AACEE71945705181A2E3DF4D527AF3D44B71,
	U3CU3Ec__DisplayClass26_1_U3CProcessImageDataU3Eb__0_mD3668D812B3812EDA9AF374399C48844028C4EE9,
	U3CProcessImageDataU3Ed__26__ctor_mFF005381DEAFA85697691DB01D2E96A04A2F3F9A,
	U3CProcessImageDataU3Ed__26_System_IDisposable_Dispose_m104AD1D5B329FBA939C13D10D821C5A3DF5A5E40,
	U3CProcessImageDataU3Ed__26_MoveNext_mA877162B987ECC684487FD6AC82A8B31518C916A,
	U3CProcessImageDataU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB862E09B3F79BDDD1138B058AD495CA9A1655B0,
	U3CProcessImageDataU3Ed__26_System_Collections_IEnumerator_Reset_m4C616E8E673C04E08EE0A9CC111811BF6CFF216B,
	U3CProcessImageDataU3Ed__26_System_Collections_IEnumerator_get_Current_m17341C31260029C5D3893A16A2469E24CA2ACE59,
	GameViewEncoder_get_SupportsAsyncGPUReadback_mB7EABCD177286341C3DBBE73AF2C8E5B69E0FF51,
	GameViewEncoder_get_GetStreamTexture_m66B84400A91D49DC346FA5E38C21A9B074D196D5,
	GameViewEncoder_get_brightness_m8540D0E2F6AB680C4A7E5AFD81A8AD6B9DA5F9EE,
	GameViewEncoder_CaptureModeUpdate_m01E3DA6667F83361E464F3A6E601FECDABCCB1B9,
	GameViewEncoder_Reset_m8148C23BE2C200581407E7E576C1854E7B891E2E,
	GameViewEncoder_Start_m746BDB5B15E9F0EC7362F72FB57E1B0BF1DDADB4,
	GameViewEncoder_Update_m07CC0A29DAE467DE84589D865850F820D6D31AFF,
	GameViewEncoder_CheckResolution_m72CD6F887C77B9BCCD7267F8ECCB40D97AED9E4E,
	GameViewEncoder_ProcessCapturedTexture_m065D42F559FFF10D37572C314956B8A19033462E,
	GameViewEncoder_ProcessCapturedTextureCOR_m2F3C75858DCFF5FCEA1F75266CAD7C78D49FE216,
	GameViewEncoder_ProcessCapturedTextureGPUReadbackCOR_mE20F256550ECCD02E1E544B2C81517D1C717BAB1,
	GameViewEncoder_OnRenderImage_mFED3A2E5E855AF3CCCBE6EA0E37D636E652EDE02,
	GameViewEncoder_RenderTextureRefresh_m10F6FD6FBEF772E8038B944E220F1DD2DE566779,
	GameViewEncoder_Action_UpdateTexture_m287EA9A33302BC256F33E8D42BC3661C42DE0D4B,
	GameViewEncoder_RequestTextureUpdate_mBFF2C8B375914C55D3556EEEDF976C4C2F5DE232,
	GameViewEncoder_SenderCOR_m01EDCB53B6A4252CE4F4E8159142FE1F53E43FC9,
	GameViewEncoder_EncodeBytes_m158CD186834EE71D24D6B29734DF224EE223578A,
	GameViewEncoder_OnEnable_m5B2591C89FD745F4BC47047415072184EA7E02AC,
	GameViewEncoder_OnDisable_m7646E72D204E45C9E244CEB1DC00F8A21ED6DDC4,
	GameViewEncoder_OnApplicationQuit_m988AC9DD29A59AD833AE133E37EF398AA2517474,
	GameViewEncoder_OnDestroy_mF53329415142B1049240B81229046284AF820D9A,
	GameViewEncoder_StopAll_mF8AB30BFE36976B1CE20BF32B84DD1C2BBB14307,
	GameViewEncoder_StartAll_m592905167330D9598AAE2264583EF0FCA4099F6C,
	GameViewEncoder__ctor_mA630790521E624EE1DD73BD0288BE5A2E9DA547E,
	U3CProcessCapturedTextureCORU3Ed__69__ctor_mF66269C3B7A0ED10EBF8E094D3BFC0F060D86A5D,
	U3CProcessCapturedTextureCORU3Ed__69_System_IDisposable_Dispose_m086E685B3E9C1E36DB2E6EA87C0C3873C87A84E9,
	U3CProcessCapturedTextureCORU3Ed__69_MoveNext_mE44F2FE3DBF6358C95020E8377AA8C7E297F09A9,
	U3CProcessCapturedTextureCORU3Ed__69_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE181181BB0EC7ECF873706E1D34BF0BB10D5068B,
	U3CProcessCapturedTextureCORU3Ed__69_System_Collections_IEnumerator_Reset_m70A1896DD9795F94E98749F40D58746320E05B45,
	U3CProcessCapturedTextureCORU3Ed__69_System_Collections_IEnumerator_get_Current_mC4C6E62B6906AC9420398225007300BBB7C310A2,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70__ctor_m22AC16A56D048F7390728187024E563C144A1D70,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_IDisposable_Dispose_m2D881E772C31EF5A3E5B524982ADF7327A31F266,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_MoveNext_m215773F333C547E2B3B2F08BBAC4A24A06677BAD,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE3DAB2EEEC093449434F76C34C21918E5522966,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_IEnumerator_Reset_mD7D3F0A050DBDB59AA97F0CA55B7060AC65E20BD,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_IEnumerator_get_Current_m66E2867825D436E43BBA5A1EFCF6F874341651E7,
	U3CRenderTextureRefreshU3Ed__72__ctor_m441A7A26F202683B4AF7A01A23EE9F91E26C1427,
	U3CRenderTextureRefreshU3Ed__72_System_IDisposable_Dispose_mCDCD7312CCE933380F22BD1B245DB76E3328A4BF,
	U3CRenderTextureRefreshU3Ed__72_MoveNext_m9005B964D5851EE2716DCC2498292CAE27DC690A,
	U3CRenderTextureRefreshU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC69DB3D7AA8EC1B9A61D2276C6A2AE960D8C8409,
	U3CRenderTextureRefreshU3Ed__72_System_Collections_IEnumerator_Reset_m521F9BE351ACF085DB25746D7471F7D97E2008AF,
	U3CRenderTextureRefreshU3Ed__72_System_Collections_IEnumerator_get_Current_mFC45BFE4ED404F1977AA77690A0502BCEC32A087,
	U3CSenderCORU3Ed__75__ctor_mB1D44F3E580E178C4525AE4F1E965AB6537AAF63,
	U3CSenderCORU3Ed__75_System_IDisposable_Dispose_mF5251E7986A74799D5234C699F5F5DE2018DCAB8,
	U3CSenderCORU3Ed__75_MoveNext_m19B5394EDA9F584DE255EC49DD84741814E7E291,
	U3CSenderCORU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64FDB59383175778B5F08CD111AC0152C8762D94,
	U3CSenderCORU3Ed__75_System_Collections_IEnumerator_Reset_m26A8498C71A09594D18B15654843CA89C017AD38,
	U3CSenderCORU3Ed__75_System_Collections_IEnumerator_get_Current_mDA5B591513A1F48F3C6657B0F7D2D13BF3216660,
	U3CU3Ec__DisplayClass76_0__ctor_m425A9ED39A7D6CE5AC79A49DEDEA5897C81AEC44,
	U3CU3Ec__DisplayClass76_1__ctor_m757856454F0951330CF023FC4DA89C76A1BC6770,
	U3CU3Ec__DisplayClass76_1_U3CEncodeBytesU3Eb__0_mF2BCCD0900D22AE1E459932C9B15412536F4153B,
	U3CEncodeBytesU3Ed__76__ctor_m05FFBA45D64E7B0C2A417B23A3BB88952C899118,
	U3CEncodeBytesU3Ed__76_System_IDisposable_Dispose_m3A445380FE83AC6C121EAB423FCC0DC76F24AF14,
	U3CEncodeBytesU3Ed__76_MoveNext_m727982AC08D4850C26E458ED9F15EA89B2F7F6F0,
	U3CEncodeBytesU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE8F2F18944E9749DCE685168852188B1D0A3DBF,
	U3CEncodeBytesU3Ed__76_System_Collections_IEnumerator_Reset_m0D2CAE677BE939A31EE1CCC3057CEBDBD72E4927,
	U3CEncodeBytesU3Ed__76_System_Collections_IEnumerator_get_Current_m44B4F166663727A00D3269D1E5D5FA6FA101BBFA,
	MicEncoder_Start_m53DA7A39A7AF825CBA08B2C2842AA3E7DEA56EBB,
	MicEncoder_CaptureMic_mCF075DE7D509C2F50F85DCA752A6DFC890211ABE,
	MicEncoder_FloatToInt16_m555E6262FB2F33514A6098636DE6020521914608,
	MicEncoder_AddMicData_m5324698332010F054884AA42792D1B375262CF3C,
	MicEncoder_SenderCOR_mF9DDA52D68EA3C7AC1762793C3D03072960F212F,
	MicEncoder_EncodeBytes_m0DB78BA240940B5AE3F421E4DB87A33C84DFD796,
	MicEncoder_OnEnable_m3A80338C5ED4F15066E95D41A1C9003E6B3A2FF7,
	MicEncoder_OnDisable_m8FB151163C744039B233BA89C2281D19EC521B1B,
	MicEncoder_StartAll_m25BB3583F7F42AD43A06E256B40638616A36A786,
	MicEncoder_StopAll_m3D277EADD613EFCC2EF6A886515FB27523B85BC2,
	MicEncoder__ctor_m6E490823DE9B0DA48D2D815A3FB147EFF78145DD,
	U3CCaptureMicU3Ed__25__ctor_m0BCAE3452B6D482727E672861BAF162325F90E00,
	U3CCaptureMicU3Ed__25_System_IDisposable_Dispose_mA5BA2DDBCA658E05BC4BA83D41DF209FFDAC0DFE,
	U3CCaptureMicU3Ed__25_MoveNext_mC5874582F40EDE1F6408B7ACADFD24270CB24A87,
	U3CCaptureMicU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10322B18AC361696176C1C18CF4F29B2B1C28DE0,
	U3CCaptureMicU3Ed__25_System_Collections_IEnumerator_Reset_mE23C4A8F1B189131CCD122043EDB2C0E2657B29B,
	U3CCaptureMicU3Ed__25_System_Collections_IEnumerator_get_Current_m9D74DBD73471CE428391634DF4C6AF4F92D3DD62,
	U3CSenderCORU3Ed__28__ctor_m98F7DD97280427E4F47C7C505A04704BAECD7F70,
	U3CSenderCORU3Ed__28_System_IDisposable_Dispose_mCAC6350DD4E60EF80637FCF114A1D4A5D5829BDD,
	U3CSenderCORU3Ed__28_MoveNext_mACBBC85A20BC627B8DF11957F481D723567A59F7,
	U3CSenderCORU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EEE62547FE206388298E76F5ECE2EE1609537A9,
	U3CSenderCORU3Ed__28_System_Collections_IEnumerator_Reset_m5314ED9761438DE960AD963E34FE3254896D624B,
	U3CSenderCORU3Ed__28_System_Collections_IEnumerator_get_Current_m96B53EFB88DE6AC60DD986DCAC39165906A78261,
	TextureEncoder_set_SetStreamWebCamTexture_m4BC52C1DABB16CBE607EA898EFC75060868344EF,
	TextureEncoder_get_GetStreamTexture_mC67E1EC94C8AE2A119EA2CE176F72B91B8225A4E,
	TextureEncoder_get_GetPreviewTexture_mC1E7EA60F737994ACC02B93A80893C81723BEB39,
	TextureEncoder_get_SupportsAsyncGPUReadback_m0C8E70073EA5FD168956CE663BCE5AD953BFD7F4,
	TextureEncoder_get_StreamWidth_m39F28ED4CB09C702003D8A0FEB6FBD4EB374AB23,
	TextureEncoder_get_StreamHeight_m3C7F58AAF640D6D5A8A4DA6A7E794CD8DDC8B4F8,
	TextureEncoder_Start_m9C6BED8F944D06A581B6BC95842A8245B9C9B551,
	TextureEncoder_Action_StreamTexture_m9CDAF27783BE37BCBAA8A816BFB70733C91E07DD,
	TextureEncoder_Action_StreamTexture_m49279A48FAA4555DC1E9D2FDC4EB3ED386454BBF,
	TextureEncoder_Action_StreamWebcamTexture_m657DC6148B1452DEF3C168053F0C909A2FAF01E9,
	TextureEncoder_Action_StreamRenderTexture_m822E546F729527FC3B50363B385CA7EBCE06B8E0,
	TextureEncoder_ProcessCapturedTextureGPUReadbackCOR_m9F3891B57BC8A6F67F89A7B55896CB9240E1003E,
	TextureEncoder_RequestTextureUpdate_m18AEFAA45404F0ADA2195C0C0C5C788760D9A72C,
	TextureEncoder_SenderCOR_mE21722767888B08DDC3D884C94134624EB3509B5,
	TextureEncoder_EncodeBytes_mAC3DABEB4A2B62CE1F7C6B652D2CD0126F926E42,
	TextureEncoder_OnEnable_m16D12003083336A653CB29C05414E2F6FB866344,
	TextureEncoder_OnDisable_m843744E50FFB53AC2A4151CF23CFBF6B9E642FF7,
	TextureEncoder_OnApplicationQuit_m407C24B90AAB5E4F382AA10C42EECC61A6A7F0E9,
	TextureEncoder_OnDestroy_m419DA1C2B096F89A50EB84C06898F5C6D2BBD0C9,
	TextureEncoder_StopAll_mC655C735A170A941D3B511B8CAFA7A2D1A892164,
	TextureEncoder_StartAll_m568278F6524B4862DEBC9733143C515883AFE16E,
	TextureEncoder__ctor_m3E23694B97D5B1226833C9CB674B97E3AAD7841B,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49__ctor_m7A7777671665613076FF3AA9D0634827F0CE1D1D,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_IDisposable_Dispose_mC244647E21C1622C33C49437AA73760B07EA733B,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_MoveNext_mCC7C1D079ECD84A73980C682B79EA37EE42CFFB8,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96C42559F165E59586219E1387AB2CB4D6BCA73D,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_Reset_m765AE58387A0C89F17588BFF91919AEC413081FB,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_get_Current_m792B625630F599B92747F7863B39D0C1F7A87FA7,
	U3CSenderCORU3Ed__51__ctor_mA77EC35DD25ABA9B996DA8F9C5B0EDD5903A18FA,
	U3CSenderCORU3Ed__51_System_IDisposable_Dispose_m83C50F3984BDE74629E8ACBF069BB0A0AA333753,
	U3CSenderCORU3Ed__51_MoveNext_m80A1E315C02D774471163852BC228E65D95C5D69,
	U3CSenderCORU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8322C0C2E7D89D2C7143F8EF17A2A6E09A2F2A6,
	U3CSenderCORU3Ed__51_System_Collections_IEnumerator_Reset_m8DF573C5E0A33310CB292F0C8B8E3A2AB46F659A,
	U3CSenderCORU3Ed__51_System_Collections_IEnumerator_get_Current_mDBCD80BACCBECF12F5C336C8003EB0C3BB48A9F5,
	U3CU3Ec__DisplayClass52_0__ctor_mC913D0C8AE158CA693E67A5CCABC2512C3245FF4,
	U3CU3Ec__DisplayClass52_0_U3CEncodeBytesU3Eb__0_m4C4569E76CED51732F353063F2BB0EDFCCC3A091,
	U3CEncodeBytesU3Ed__52__ctor_m466C96E960C64C1A08D40CAFA7B15519B3BF7222,
	U3CEncodeBytesU3Ed__52_System_IDisposable_Dispose_mD120CA1771B0C5533F78D015D0754FE83065A0D1,
	U3CEncodeBytesU3Ed__52_MoveNext_mD170B8A76DDDCB34603A5E040260C6A9F876381F,
	U3CEncodeBytesU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A775392B9DC6011391FC9DB18C0606286964374,
	U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_Reset_m5CC4264F59DB6C3D9A916AE8A5FEADDAF55A15A2,
	U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_get_Current_m4890B1605C611D741CCA98D73B9980C33FD36A6E,
	ConnectionDebugText_Start_m7CAE9BA91D2E70145138045CA505FCCFA372F2DD,
	ConnectionDebugText_Update_m3D64F82A50F59B71539DB801D24F1EB62D1A1D32,
	ConnectionDebugText__ctor_mEEB98F27DD2F5EDC426081AEADAA9D60B69D1A09,
	NetworkActionClient_Awake_mF86F12BB0EB2C0EF4E670722513C042B52BEDE02,
	NetworkActionClient_Action_SetIP_mDEE71162A2E4D57187A5240809994E9CCE969574,
	NetworkActionClient_Action_SetServerListenPort_m1E9A6A0E8AD34EFEC01D38E363D625DBB012AFB7,
	NetworkActionClient_GetCurrentMS_m12D70F8F88D64DD0BEBEF85CB607A181768677C4,
	NetworkActionClient_Action_ReceivedDataDebug_mE04C41A0848D0B38F375F541A159F48DCC1EF574,
	NetworkActionClient_Action_RpcSend_m8A035AFD85297132135EBD46E86EDADDF5084F33,
	NetworkActionClient_Action_RpcSend_mCB092BFD39392CD0A1856F3A73524D767673ADDB,
	NetworkActionClient_Start_m4A1FF36E770264CBE95B9F7E604714DEFF1AA682,
	NetworkActionClient_Update_m99FF45575FE161D9C7BEEF7EF7B1253054CBE3F4,
	NetworkActionClient_DataByteArrayToByteLength_mDB3A6E8595BC1EDA3DBBF56D7704AA3D5644CCC2,
	NetworkActionClient_ReadDataByteSize_mE20E1A2DE4A38D4D67041DC0D4A29B4922AB8500,
	NetworkActionClient_ReadDataByteArray_mB33409027DAEE5CF4E83BD1157C356F60AA8020D,
	NetworkActionClient_ProcessReceivedData_m39981BD8F7B743980CDCD3E5E99678C6225BB39D,
	NetworkActionClient_RelaunchApp_m169BC583FDCCD8B38DE27077A5715E78C0FE72BC,
	NetworkActionClient_Action_Disconnect_mE4A373855D69A45C99DCDAB6EE408611242F772D,
	NetworkActionClient_Action_ConnectServer_m0707006053220BF330715FF8184510BBED094A6E,
	NetworkActionClient_ConnectServer_mAD0257352347719F65E80A8DE0040CDD4D06D940,
	NetworkActionClient_MainThreadSender_m622A0D7B47BE01980014D36BBBB09F334EA899C2,
	NetworkActionClient_Sender_m7D5CD6518E541625486056C65D3F5BBF233C6F4D,
	NetworkActionClient_ClientEndConnect_m709415FE0F577342490EEF98D339B45BD4F6697F,
	NetworkActionClient_LOGWARNING_m4D60F0A371D528121BF77655970CF2CF344D3A40,
	NetworkActionClient_OnApplicationQuit_m44F85D653C3F25F58872540A5D26098DECF10F3C,
	NetworkActionClient_OnDestroy_m0B27CA4A88F6392F93D3D8F4C9C40E7E64E59EA5,
	NetworkActionClient_CheckPause_m43B9B63A68CF59691FFD5DF0ED7E1B57A5364219,
	NetworkActionClient_OnApplicationFocus_m0BBEBBEF98676E7090487970BBEE99A3E2845D53,
	NetworkActionClient_OnApplicationPause_mFE5C243713B06223E5412BA6581203466FC39818,
	NetworkActionClient_OnDisable_m79FD8899EA70F695A5D8F8D089EA093BA6F3B07B,
	NetworkActionClient_OnEnable_m9BB68941F0A63901AB9B39532EFB316646A22124,
	NetworkActionClient__ctor_mBDE3C62F1A79B999AEF997D2650892A68D41D3AC,
	NetworkActionClient_U3CConnectServerU3Eb__45_0_m442C6B6C470CC656027DF70E73DAF478E089B6CB,
	NetworkActionClient_U3CConnectServerU3Eb__45_1_m179A541B8267512D610B9B300D41484E26284764,
	U3CRelaunchAppU3Ed__42__ctor_m8B105F3327D8A632F1D7C90BF50014DEEFC06FCD,
	U3CRelaunchAppU3Ed__42_System_IDisposable_Dispose_mEEBC05919BF40AB942F11E926772F7EFEB21CD57,
	U3CRelaunchAppU3Ed__42_MoveNext_mC32F576854618400F8E044D411CB5D20947ACDB2,
	U3CRelaunchAppU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC26F400B84818EC3460756DE11E735D785BFB94,
	U3CRelaunchAppU3Ed__42_System_Collections_IEnumerator_Reset_m53A63EDFC749C27FB3BB9933690050F0D04B574F,
	U3CRelaunchAppU3Ed__42_System_Collections_IEnumerator_get_Current_mF304273D01DC3101989FBD106DCF13E98709989E,
	U3CConnectServerU3Ed__45__ctor_m5B85B336E097535503ECCF6231A0F090E60D8CA8,
	U3CConnectServerU3Ed__45_System_IDisposable_Dispose_mBE9FA24FBCB924B9DDFAF289C121D5B147256051,
	U3CConnectServerU3Ed__45_MoveNext_m9778BCC1EE6904540ACB90BBA216ED0B289A15BF,
	U3CConnectServerU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m272B27177D53C32FE40E38BE30C757C7C01BBDFE,
	U3CConnectServerU3Ed__45_System_Collections_IEnumerator_Reset_m6EC4EBEF2DB4BBDA8AE731F223D52D673ADD8979,
	U3CConnectServerU3Ed__45_System_Collections_IEnumerator_get_Current_mBD22DE68B0C630F4024EF16188DE45274A2504AB,
	U3CMainThreadSenderU3Ed__46__ctor_mC82359B67D71E32828C89B6142552FA23EE3A05D,
	U3CMainThreadSenderU3Ed__46_System_IDisposable_Dispose_m93673DC46CDF23BBA139D59DFAA42E801556DB10,
	U3CMainThreadSenderU3Ed__46_MoveNext_mCC037827C5AF8E0A43CBA41DF17A7D2B21447C14,
	U3CMainThreadSenderU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74012AD9BC7B95B1B3325C8B6DCA643B95A64804,
	U3CMainThreadSenderU3Ed__46_System_Collections_IEnumerator_Reset_m8EBA6BD61B072F3C906E725CA12D79F20807C3F2,
	U3CMainThreadSenderU3Ed__46_System_Collections_IEnumerator_get_Current_m447C88C8948FDDD82F61202F9CCCDF3FD4EC4C54,
	NetworkActionDemo_Action_ProcessRpcMessage_m8501BA238EA0A9E619F99E5EB08237CD57B0836A,
	NetworkActionDemo_Rpc_SetInt_mD7B23D1FD6E57602B08B99D085DA6E965527FFA2,
	NetworkActionDemo_Rpc_SetFloat_m76560C28EE5B5CC05112D8CBF3E1C44D184F9CEA,
	NetworkActionDemo_Rpc_SetBool_mBCF20801C526B002022BDEA2D5F878EED04CBBA7,
	NetworkActionDemo_Rpc_SetString_mAB43CBDC892B99BDF9FD4C5BE4F653F8BB79267C,
	NetworkActionDemo_Rpc_SetVector3_m545A3340C1C473F4509513FBFABF9852C814A8FA,
	NetworkActionDemo_Rpc_SetVector3Random_mA35F3592067C14205A552EA2D07D80FBD0EBA5C5,
	NetworkActionDemo_Cmd_ServerSendByte_mFCFA36F6546980E972712BD53B57BD0B78A63BA7,
	NetworkActionDemo_Action_ProcessByteData_m5E9BF37DFC0FEE3FE1AA26F2B1F81E248AC34B05,
	NetworkActionDemo_Action_SetColorRed_m9E972F4F1099CF47AAC07B6F73681CC75E1FBC77,
	NetworkActionDemo_Action_SetColorGreen_mD2000946F4D41999E2D479A8DACB774006D75A0B,
	NetworkActionDemo_Action_SetInt_mABB30D07F97761700F2868AF6EA5A2A8B7EF1F67,
	NetworkActionDemo_Action_SetFloat_mF77DA627B5D1C7D4887D46AA1E2304D8C9CC8458,
	NetworkActionDemo_Action_SetBool_m6EC9B1110DE48B131681806E2E63ED41AD333A43,
	NetworkActionDemo_Action_SetString_m622E1BF7AF69DEE4FFBC226F1F1FA663EE26A8FE,
	NetworkActionDemo_Action_SetVector3_m23C12C7C41C048EA60D6665F129098701E69BC46,
	NetworkActionDemo__ctor_m40995801EE857AEB2237BFB9013AAB19F1D08C75,
	NetworkActionServer_Awake_m356A33F0BA9A90FAF51BC7C88C09A65452C96E53,
	NetworkActionServer_Action_AddCmd_m83242BB96084BEAEF19EE0D2CB720FB61A0B1631,
	NetworkActionServer_Action_AddCmd_m9FD8414C42AD2F633FEE631322FB67625BA7A6B5,
	NetworkActionServer_NetworkServerStart_mD5F977E58A6487DB3F3ED9C09334F9B67AB5A6EE,
	NetworkActionServer_UdpReceiveCallback_m52BACCFA05E25B803F8B54211B3E4E1CF4139711,
	NetworkActionServer_StopServerListener_m85C3A8EBEFEB3FC487A52E5452309961BC38CF64,
	NetworkActionServer_Start_m682905EB145AD1A34E7FA5BEF3D5F5F69FA05FCF,
	NetworkActionServer_Update_m3F95CBDF6493E6C0A1822D3ABE7D99C685746BAA,
	NetworkActionServer_initServer_mE1FC9057F31F33924D260DF9C25E20F2871DBA93,
	NetworkActionServer_AcceptCallback_m7567880E91AD3285FD87DE77EB4EDE4E23048AD1,
	NetworkActionServer_senderCOR_mD36F0F619872E82608780159A44632485206BADF,
	NetworkActionServer_Sender_m6FE9A162D8AE0FC33674212A17F42C08592EA96F,
	NetworkActionServer_byteLengthToDataByteArray_m6FF0525B5A68E990D64FC55C5E01D634E20A9F87,
	NetworkActionServer_StreamWrite_m4A55CBE3D53E2A9728CF42F2DA22DA2F7C9F2F63,
	NetworkActionServer_RemoveClientConnection_mC84BE472290CF9E442B2A299F9113D039BD93D02,
	NetworkActionServer_LOG_m3D73FA1C73E2E79B5B095368E1803D968B5935C2,
	NetworkActionServer_StopAll_m8976E24CB6C40438CE55C8CA805CD1DD9EEA2A30,
	NetworkActionServer_OnApplicationQuit_m64996CDE16C7BFAE0A511D6191F7610A97C70DCA,
	NetworkActionServer_OnDisable_mBA6DD57A03D7BF7B6615619B02C2754F487009C3,
	NetworkActionServer_OnDestroy_m2C56360714457E29A209A290B6137729686BBB5B,
	NetworkActionServer_OnEnable_m1FC9C76AEC36EB27E17F8153BF0798737E78406E,
	NetworkActionServer__ctor_m23D3033A23672D90E3B4AD3CDC1253B1E94A8037,
	NetworkActionServer_U3CNetworkServerStartU3Eb__27_0_m587120E8A38D4C97DDF732BC8D2AB35182FBC073,
	NetworkActionServer_U3CinitServerU3Eb__35_0_mAE1A30AAC7632B132A79EBB213D02321E61FDF36,
	NetworkActionServer_U3CinitServerU3Eb__35_1_mCF9490B0D57AF521FE01D6B0F0B6A37580D12CC3,
	U3CNetworkServerStartU3Ed__27__ctor_mCB2C852278E9F451A4E26CE1B413FB5110475993,
	U3CNetworkServerStartU3Ed__27_System_IDisposable_Dispose_m1CA80B998FA446E973ADFFCA18F495F5E298666E,
	U3CNetworkServerStartU3Ed__27_MoveNext_m4B7415CB64FAC61529468363166627B9BB9DC05A,
	U3CNetworkServerStartU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF7728EA245BC2307839F1B4A4149C65B8A951316,
	U3CNetworkServerStartU3Ed__27_System_Collections_IEnumerator_Reset_mAAE9C1A8BBD88F66F9DE95EEB2E0E3B9E81F8581,
	U3CNetworkServerStartU3Ed__27_System_Collections_IEnumerator_get_Current_m60B9EC98A4E541B38ED4166F2F1831DA0A61ACC7,
	U3CinitServerU3Ed__35__ctor_m769D62C303CE7EC079538C0F8DBFB7BBEBDC95DB,
	U3CinitServerU3Ed__35_System_IDisposable_Dispose_m4A3E42FCC0B216893BF2FA0E85C42C10CF0A8550,
	U3CinitServerU3Ed__35_MoveNext_mE2DABB48FB243774BAA4A09FC74E9689EC8D03FD,
	U3CinitServerU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74370DDB23F94786FF901839AD1A30ABA66A50E3,
	U3CinitServerU3Ed__35_System_Collections_IEnumerator_Reset_m3F1034C5ED02A11B8DEFA1FF9D6D715A02B7A43A,
	U3CinitServerU3Ed__35_System_Collections_IEnumerator_get_Current_m5837691CFA955995184267142FE100572A14D95D,
	U3CU3Ec__DisplayClass37_0__ctor_m93F546C886B1313964FB4A16327E7224C2CE1355,
	U3CU3Ec__DisplayClass37_0_U3CsenderCORU3Eb__0_m50C79F2F5E3EE434E4AFB98B1859E0568C04F3A5,
	U3CsenderCORU3Ed__37__ctor_mBD331865F5DA308269214092CA652A6BECA978BE,
	U3CsenderCORU3Ed__37_System_IDisposable_Dispose_m46CCC2693D726470C1805ABBAD43E96EF5D38451,
	U3CsenderCORU3Ed__37_MoveNext_m8D7AD01909F0EC4C36DA16A7514F66E2970713A6,
	U3CsenderCORU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6E3F7FAFACD4AEEACAED1D538BB2E5824FF0C8B1,
	U3CsenderCORU3Ed__37_System_Collections_IEnumerator_Reset_m42E68D766184FABAC0C3CF196255CB692C47B129,
	U3CsenderCORU3Ed__37_System_Collections_IEnumerator_get_Current_m3312A4E6B021CC2099E75F2DA5E9E9742C578C0E,
	NetworkActions_Debug_Action_TextUpdate_m3376DFDE12FFDF9C3DFBA04B6E0E4DAC4AE573F5,
	NetworkActions_Debug_Start_mDFF34ACB60CF14594C8B901DE50AF7D52E75D5E3,
	NetworkActions_Debug_Update_mDE40BF3BBD4BD0A5081217DB24056EF97A581AED,
	NetworkActions_Debug__ctor_m0617F64BCF20752A7F572A5B13DAE17F0DB4D602,
	NetworkDiscovery_LocalIPAddress_mBF16033A02B182501A303D10A25F26BB630861EF,
	NetworkDiscovery_SetStreamingPort_m91C7B677B575BA6D21E680CAA731314DA46F6788,
	NetworkDiscovery_Action_SetIsStreaming_mB0A8D7AF81C9A43CBF3CD5FC257BCEA58E1D9D97,
	NetworkDiscovery_Start_mC650727A1681B08D76685999C290594895EAA96A,
	NetworkDiscovery_Update_m89EBD95C2B788F298A2FF2993BC1A57875085663,
	NetworkDiscovery_Action_StartServer_mC9463914F5654F8D0E1A76F8AB06CD51660F270F,
	NetworkDiscovery_Action_StartClient_mE1F22FE54ECBFDCF3E23277424F972D92531779A,
	NetworkDiscovery_NetworkServerStart_m18B5868E5008D8585C7E334F79B6F81C456B5E7A,
	NetworkDiscovery_UdpReceiveCallback_m6FD06145036B677B99A72ADC0863139846DD18B0,
	NetworkDiscovery_NetworkClientStart_m0C87658DDD08D23DC867C26D9765B4696E42672C,
	NetworkDiscovery_DebugLog_mD963FEC19FBF9F632F01969256410242A53CD7EE,
	NetworkDiscovery_OnApplicationQuit_m8BE1B8FA5CED4BC79DEB83417FE01B60CDE1F029,
	NetworkDiscovery_OnDisable_mDFA183A01F541C092A1373C7B1780624A1EFDF4E,
	NetworkDiscovery_OnDestroy_mD71C97A283A1F7F72A9DC0673869D18B70F3C870,
	NetworkDiscovery_OnEnable_mFE481254F2E5D27A919B62CF9AA494E5AD15502C,
	NetworkDiscovery_StartAll_mF49F9F2CC070E3287A4077982435C22B81A8F8E6,
	NetworkDiscovery_StopAll_mBCF2798C9C1DDA1F94F673D6BCB6AC7D202E7D6E,
	NetworkDiscovery__ctor_m959DBEF0B48CBFBB84AFAD83DACD48F4E8A7A605,
	NetworkDiscovery_U3CNetworkServerStartU3Eb__25_0_m69B3F6347256B019D2E3152F9D3A491EB8531419,
	U3CU3Ec__DisplayClass25_0__ctor_mD3CC686CEF342354C80F0FD8A679CEECF8D18E1B,
	U3CU3Ec__DisplayClass25_0_U3CNetworkServerStartU3Eb__1_mFA14316C9EC26A8BF498812C744EC0DA36D5861E,
	U3CNetworkServerStartU3Ed__25__ctor_m9E6CA950392E1C360F05B286DE6ADC815EBBF241,
	U3CNetworkServerStartU3Ed__25_System_IDisposable_Dispose_mDEFD1D08EF3DBB6ECEEE820BB5E8F4AE844534FA,
	U3CNetworkServerStartU3Ed__25_MoveNext_m9707563A7EE49CDDA2142550E33F9E155F33FF94,
	U3CNetworkServerStartU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75754473C27926F5FB4A444A1F25D332861E2769,
	U3CNetworkServerStartU3Ed__25_System_Collections_IEnumerator_Reset_m3379482282EF79E2E7746C979E713167F99EB205,
	U3CNetworkServerStartU3Ed__25_System_Collections_IEnumerator_get_Current_mB8AE6FB23F56DC8F37F185A99FF1BAA7A76727A8,
	U3CU3Ec__DisplayClass28_0__ctor_m8E57EF5811074A63AC563CD86D9093C44DE49EE1,
	U3CU3Ec__DisplayClass28_0_U3CNetworkClientStartU3Eb__0_m93B70165AC94F452E7CE31B781A627650FBACEBC,
	U3CU3Ec__DisplayClass28_1__ctor_m50146F8B01451F08AA535F8542D55D24D58B4A86,
	U3CU3Ec__DisplayClass28_1_U3CNetworkClientStartU3Eb__1_mF41931373697E36560FD28CA411D762CC9BAB810,
	U3CNetworkClientStartU3Ed__28__ctor_mDCB1B215FA337B35B6B23CE5564442B6ED62A5BD,
	U3CNetworkClientStartU3Ed__28_System_IDisposable_Dispose_mB3863749BF1ABC4FB33E5A6E36F80506B34C24C3,
	U3CNetworkClientStartU3Ed__28_MoveNext_mD230EEE85BF15308CE925225DAB974BE0654FDB7,
	U3CNetworkClientStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m21EA8CB49CD8ABA66A584C5B28405068771A97D7,
	U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_Reset_mE6C03EC48AD7CD76E1B99BE573B7656B65B49C1B,
	U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_get_Current_m063DFE03B905450BF7EC4D8C2B51D2C6ECB182B2,
	FMNetwork_Demo_Action_ProcessStringData_m3D969B44A7E78D832F3889B52C56B9E52CC3E5FB,
	FMNetwork_Demo_Action_ProcessByteData_m673512FFA11D6BE5FFB46F2140829CC4C345CE54,
	FMNetwork_Demo_Action_ShowRawByteLength_m521940D61EE7A037670C3D7ACE4AE9624982B9F4,
	FMNetwork_Demo_Start_m90EE9BCFCC4988B92570EE13F156FEFB6E2EAAFD,
	FMNetwork_Demo_Action_SetTargetIP_m2E9DF0B615B1F903BE8E15A9DB21C39D346D79F0,
	FMNetwork_Demo_Action_SendRandomLargeFile_mE35B5499B725B7E1E9EBD4ACCBB71FDE07AD1D06,
	FMNetwork_Demo_Action_DemoReceivedLargeFile_m4E56CF31EC68A86365D60F17285D3D1B62E92DA1,
	FMNetwork_Demo_Update_m60ECFAE166BC35DCA788ECD757EE8683377E3984,
	FMNetwork_Demo_Action_SendByteToAll_m5056938F5DE5E34B349DA4530A33AA03FCD10184,
	FMNetwork_Demo_Action_SendByteToServer_m66C186E56C28D6759BC4755013E92EC0515EE269,
	FMNetwork_Demo_Action_SendByteToOthers_m8B68D9B4C3781CBC246841613C6DBA6F7321EADF,
	FMNetwork_Demo_Action_SendByteToTarget_m1F8D84944B2A7A17A6561FEBA1833E94A9B028E6,
	FMNetwork_Demo_Action_SendTextToAll_m29260CFECDCA40EA5D3EE7B0E8024A2675E586CB,
	FMNetwork_Demo_Action_SendTextToServer_m999D775A61F527A431B671738D02650E37D38C45,
	FMNetwork_Demo_Action_SendTextToOthers_mA5A23FA7923F26BD85BCF39689C19DE4A35ACCE8,
	FMNetwork_Demo_Action_SendTextToTarget_m3FF203C67A7E888BBC74224EBD60EF7A485BE017,
	FMNetwork_Demo_Action_SendRandomTextToAll_m18C744E4BD7038A24F70C2D0088878105FA5ECD1,
	FMNetwork_Demo_Action_SendRandomTextToServer_m383B330C05F1E0D6972BAC9C339A90AED7D1953F,
	FMNetwork_Demo_Action_SendRandomTextToOthers_m6041D78D1F03A3766E87803DAF7AA19419F0B174,
	FMNetwork_Demo_Action_SendRandomTextToTarget_m4EF8E2C5C6024BD23E61A33A21C65F94A2D82435,
	FMNetwork_Demo__ctor_m32525BDC5CB7720599BD9B312AC50C9D4E99BDEB,
	FMNetwork_DemoAnimation_Start_m55994DC6FECDEA65967B1F36CC7F18430FBA7DAF,
	FMNetwork_DemoAnimation_Update_m0F7ABF21F646A8E4903A4315E9DA4867B8A6A1BB,
	FMNetwork_DemoAnimation__ctor_m5BD90AB069BD702037121B8C550B74858192209E,
	FMClient_Action_AddPacket_m4384A8D2663748C0DAC53985D41EF579215B5231,
	FMClient_Action_AddPacket_m111859BF278F6E4B9982A2414D59E5D1F2F87049,
	FMClient_Action_AddPacket_m2E3494FA4D747670B5F15D8D518725E3693132C0,
	FMClient_Action_AddPacket_m5EB2125BA3CCE60516506CA78E36D1D68C2224E1,
	FMClient_Start_m1A8E277C432EB72A03A979A960A23FCA372FC8C5,
	FMClient_Update_m4BE2E1C9C244EDBE8FA9CAD154942958F8DBC3B6,
	FMClient_Action_StartClient_m0A5D15C0CCF7EE2FC756E0A96C8BCCAE6B6FC54B,
	FMClient_NetworkClientStart_m5C1D3F095A33D1FC774B8C6EC098F7721D7E31B4,
	FMClient_MainThreadSender_m6D18ADAB3294A9DEA6DF2D29C40F1F50BF6E99C8,
	FMClient_Sender_m25F98C9C7A0C3D7D0FF2D9858B4E9B9BF4660356,
	FMClient_DebugLog_m5D568908BAFB34296E19BAA3D9162453B3E5B0AF,
	FMClient_OnApplicationQuit_m00DD985C52D0FF12D5B91A51223E9EE363DFB020,
	FMClient_OnDisable_m371622F814BF5311D1A36FC8A639E6F35478E1D7,
	FMClient_OnDestroy_m5BAEE745A17764085C604385EAF4E9EA2D2BA696,
	FMClient_OnEnable_m8C2BDACC0D09C1C55C69C9DAD02189D62099264A,
	FMClient_StartAll_mE2B2ED1EA90BA6D9CF9503B0FD6B7BFF47E67400,
	FMClient_StopAll_mAD5CC012381EF05F81DE74FC20A2CC1BC32AB5D9,
	FMClient__ctor_m3368DB666B571C4CA0EF2CC98E7DD4767D97C087,
	FMClient_U3CNetworkClientStartU3Eb__28_0_mF1E5191AE17244C6DC39C9E713F0283479E83BC3,
	FMClient_U3CNetworkClientStartU3Eb__28_1_m59932624A282FFE56FAED1432B64DA442A8A659F,
	U3CNetworkClientStartU3Ed__28__ctor_m74FB441E0A122B39FA3E8BACE588F244CC3442C6,
	U3CNetworkClientStartU3Ed__28_System_IDisposable_Dispose_mDAF5B6EB73682E6DAD0467B3CC135597900FDA15,
	U3CNetworkClientStartU3Ed__28_MoveNext_mDFF3581CDC0511F260C7C998E8A09130E78EC5CC,
	U3CNetworkClientStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD38932EFA7E11FA248F7DCE962FD7BE819F7F151,
	U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_Reset_mAF78F0B9CF45FC45FDFF8CC2937CEE2789C18471,
	U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_get_Current_mE01B0407D66C75A6FB8EDA7C05DD36C2BD5ABC48,
	U3CMainThreadSenderU3Ed__30__ctor_mA1D1255D636432AF89AF3753663A87FF0D6CE9B5,
	U3CMainThreadSenderU3Ed__30_System_IDisposable_Dispose_m40BAFB1B370680F27AA552B07ABDBF86CE9AD75E,
	U3CMainThreadSenderU3Ed__30_MoveNext_m9F7A69D36F655852240D7388286395A0C8156143,
	U3CMainThreadSenderU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m16B157F33D163BDDBEC08E74BA62E7FCDF7FD29A,
	U3CMainThreadSenderU3Ed__30_System_Collections_IEnumerator_Reset_m9DD8471E6B7633F4DBA91DD16BFE95E879C46259,
	U3CMainThreadSenderU3Ed__30_System_Collections_IEnumerator_get_Current_m3A2D221196A83D49C0259B9A97B0E912C667B7D0,
	FMNetworkManager_LocalIPAddress_m2A21AA0317895E069D17566788F2873954084F54,
	FMNetworkManager_get_ReadLocalIPAddress_m7DCC079152B8447F43A53F32EF05E1152A879265,
	FMNetworkManager_Action_SendNetworkObjectTransform_m0C03C34EAE2F0021EBB5B2363A343AC88EC6325B,
	FMNetworkManager_EncodeTransformByte_m3A9DF15CAE923FDD8F22770C60CC0CDE077C367C,
	FMNetworkManager_DecodeByteToFloatArray_mC127030F2A1228A72386C10319A3708AFAEF764B,
	FMNetworkManager_Action_SyncNetworkObjectTransform_m03149D79DB7D9D17F45662668009A96ACFCDBDCE,
	FMNetworkManager_Action_InitAsServer_m0B1E52A07D5B6D78481F1A82DA5FE0B2F6430158,
	FMNetworkManager_Action_InitAsClient_mF3D72A59BAF7CAC0173117BFDC26744B20D55AD0,
	FMNetworkManager_Action_InitStereoPi_mAA947D2C0A9F98EE6B088D1E7D7D035741BFF248,
	FMNetworkManager_Init_m11D9E28E3100F1496F90BB647DC796D9DD71C6DB,
	FMNetworkManager_Awake_m1774513DDB5D216E4AACFCF43D6E65E33AC84A27,
	FMNetworkManager_Start_m320FE8165AD4EE5B71C9DC437E246D9FEFEF3989,
	FMNetworkManager_Update_mA5198BADACFAF5FBDBC10C4779BDD30A1938AE24,
	FMNetworkManager_Send_m8C2F12C1A7F5F268D037CEA720A4B3BCAE674E73,
	FMNetworkManager_Send_m4108634695E8995D0C3E1141BE2555A1039F34DB,
	FMNetworkManager_SendToAll_mDAE7E215456CAEC0E0AC88EA0B1AEACB3CECC826,
	FMNetworkManager_SendToServer_m8D48C24B1C491EDF21C76C04C86A24FCD623B1A3,
	FMNetworkManager_SendToOthers_m87549E3034721974B328A4E42A97E37405ED3AE0,
	FMNetworkManager_SendToAll_mBC96E0C6A1749ED039D5526E51511EE4F1476D57,
	FMNetworkManager_SendToServer_mB52C54BA4BABF484F45687BD0710F633EC79CF3F,
	FMNetworkManager_SendToOthers_m18812E19A37300606D07E6AD20D417F8DF21136E,
	FMNetworkManager_SendToTarget_m0008F7DE2509EA8A0EB49A176D1F1E30F274A205,
	FMNetworkManager_SendToTarget_m1B60D0F1FDA5826C5F7DACDF1B134106A8CBAC6A,
	FMNetworkManager_Send_mC88400D86C67E664044CA4DCD13B1823818A3E48,
	FMNetworkManager_Send_mFBEF41B525A0DE6B5677224BEF5112937DE982DC,
	FMNetworkManager_Action_ReloadScene_mD9AAC5E14852F3972B8A70E5F11378AC1363621B,
	FMNetworkManager__ctor_mCF6D22B4209B321D7860ECA779E5C924987D8CD1,
	FMServerSettings__ctor_mB88B438B37E13C1A9F610E2179A3437C2375D0A5,
	FMClientSettings__ctor_m10B41CF7BD14A31A54439D1DB8E3BE39CF2A879C,
	FMStereoPiSettings__ctor_mF731D9003D30D2816C10196E300E94551AA15CCD,
	FMServer_Action_CheckClientStatus_m08CA18AA90ACF9F85369E57FDA8746BAADF87C01,
	FMServer_Action_AddPacket_m0CF954C60BCB5AF4D9187886DF3579BD6991309B,
	FMServer_Action_AddPacket_mC41AAF3BB2D3D62A3251FEC256C862FE7285926D,
	FMServer_Action_AddPacket_m7ACAD0E45DC1B13F525C629F1E462EA69388C94E,
	FMServer_Action_AddPacket_mF24F6E7C93B2F1A4FE5E080D168C931F30C86AD1,
	FMServer_Action_AddPacket_mD6C916EA5636A65AF6C7FDE4329CDD9AA1DAA9B6,
	FMServer_Action_AddNetworkObjectPacket_mAE1BD8B7D3BEAEA1509D798BDE8D6F32C5AAB08A,
	FMServer_Start_mB8A6F846AD04910BB18CE17165A6FE5D39F54C8F,
	FMServer_Update_mDFE639F168331ED62B0D2DA16D1F07AF9C2BDD30,
	FMServer_Action_StartServer_mD183AA00CAA2FD6053AC436C2F2F337F0E03C691,
	FMServer_InitializeServerListener_m9E9268F9C4B46C9DDE416D3B36A8F9129FDEEDC6,
	FMServer_MulticastChecker_m96C7979AAE41349983F70C087E9A9C0910EB5A97,
	FMServer_MulticastCheckerCOR_mE2725B33D0CB15DCAAF488FCC012F15A320B9EE9,
	FMServer_NetworkServerStart_mADE0EFA3E23400B33FF8D8686241BB372317FB91,
	FMServer_UdpReceiveCallback_mC81B62C1A7D2BB0DF083D0C9CDC4782EB9326019,
	FMServer_MainThreadSender_m583131AE11A85995F22EA303AEA1E78F1CF5F6C7,
	FMServer_Sender_m75BBFF91484C9D7E2CB64034A9C40B29F2FCE51E,
	FMServer_DebugLog_m480EA7DD3B45EAB405F1370083664ECD87B0C703,
	FMServer_OnApplicationQuit_m2B06A75C93CF68FC6CB2CE67D8E82C2C2830A8F8,
	FMServer_OnDisable_mE066DD74919E26D3AE2B23280B495727955AA5D5,
	FMServer_OnDestroy_mA6687ACC0736265F3CA1A56B696CF7174DBFA2E2,
	FMServer_OnEnable_m527C733A41C0BE99DD39BEB3F7295E2752F61B2A,
	FMServer_StartAll_m387372109DC7ADCDC641ED6BCA6D799530A11BFA,
	FMServer_StopAll_m98E16C304EB7599AE0A96AEFDF889C9D34B4F88A,
	FMServer__ctor_m63CB3AF1617032C73B0D3AC8DA1EF801DB26343E,
	FMServer_U3CNetworkServerStartU3Eb__33_0_m94D4E67DD25C57BA594883C8BB8A3938F38A17EC,
	FMServer_U3CNetworkServerStartU3Eb__33_1_mAB86250B7255398C9E6DE59B2A66E6E5339D40C5,
	ConnectedClient_Send_m61BF16FE82C71CC32EF19977F327585599C03342,
	ConnectedClient_Close_m691799BBCE9CA16F7FDCE62885395B09F46D3DDF,
	ConnectedClient__ctor_mD3478E8BF36F6097F908D0052829192CB80F748B,
	U3CMulticastCheckerCORU3Ed__32__ctor_m22803B6B8723DE8AAD5DBDAA07727CC3ED4F697D,
	U3CMulticastCheckerCORU3Ed__32_System_IDisposable_Dispose_m3E6A6A5B9B8F959D5E3A97FE0C16DF116F3E1EA3,
	U3CMulticastCheckerCORU3Ed__32_MoveNext_m382F62F98DD9118E9DC32F45E7FC95C2ABDC830D,
	U3CMulticastCheckerCORU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50E18A0ED10006EC08B1212D96DD4108123F33B1,
	U3CMulticastCheckerCORU3Ed__32_System_Collections_IEnumerator_Reset_m16BCF26480237987CB13158B859DDA879F20DC2F,
	U3CMulticastCheckerCORU3Ed__32_System_Collections_IEnumerator_get_Current_m57CE0AA50FDC05977BD0BF3BB5D1F0030A95C980,
	U3CNetworkServerStartU3Ed__33__ctor_m003D95822BA086D2EBF20C01A8AF02ADB09A4792,
	U3CNetworkServerStartU3Ed__33_System_IDisposable_Dispose_mDD16B6E0A35425113C4F1F49E684BFD4F3954B4C,
	U3CNetworkServerStartU3Ed__33_MoveNext_m143763191D31EB48F7E3865A2A8C3B569C00668B,
	U3CNetworkServerStartU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57D8D888ABDDD6F8373014058D5946F23AE912B1,
	U3CNetworkServerStartU3Ed__33_System_Collections_IEnumerator_Reset_m987EF30AA6FAFB83981381D953E31ACFD3F8E791,
	U3CNetworkServerStartU3Ed__33_System_Collections_IEnumerator_get_Current_mE9B204506AB7045EE686A73EBFA38CAB50BC7C15,
	U3CMainThreadSenderU3Ed__35__ctor_mEEFF366163521219123028747B65BAD54CAE8413,
	U3CMainThreadSenderU3Ed__35_System_IDisposable_Dispose_mB11B958AC613C529041005C1E719367C7568DD70,
	U3CMainThreadSenderU3Ed__35_MoveNext_m9434EFEF081094ABE2E73A237FE7D00E4F2FF062,
	U3CMainThreadSenderU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5992ED54360CC40B22F777EB6D69E2038A69CD5,
	U3CMainThreadSenderU3Ed__35_System_Collections_IEnumerator_Reset_m5840A514453D43117ABBA39DC19A46B7ED52ACB5,
	U3CMainThreadSenderU3Ed__35_System_Collections_IEnumerator_get_Current_m12BA27A2837055EE2552D4AE931685EE06F03EA8,
	FMStereoPi_MulticastChecker_mA1B76F219B70818A47C318531F80DADDB8E4163F,
	FMStereoPi_NetworkServerStartTCP_mD09D99ADF2FE7161A475D5C62F73678B70F9CDF2,
	FMStereoPi_TCPReceiverCOR_m053F2533E08C08E7B2BAD0DDE160095E29FC58F5,
	FMStereoPi_NetworkClientStartUDP_m6F2FD2FE29DA7DFAB81AF847F517ADF42A4114D1,
	FMStereoPi_Action_StartClient_mF7F98EAF478F5C1AAFFDDA8F4A2152915B04F0A8,
	FMStereoPi_Action_StopClient_m29CE522A4A8474C36B42467E739F64CD3C623D1B,
	FMStereoPi_StartAll_m4A520CB05FF5DF8D90B04C8CD41727A4D5379DA9,
	FMStereoPi_StopAll_mC9F120C0C8BEF4243E0A51332288DDF50DB03E20,
	FMStereoPi_Start_m403748CA9441AD024F44C5CC053A59566C9540F4,
	FMStereoPi_Update_mB0CC2343CD4F7425C4AB7B2C99265417CBFCADE3,
	FMStereoPi_DebugLog_m4FEBEB55ECCA8F927D81F38563EE4892E6C7C331,
	FMStereoPi_OnApplicationQuit_m8E9703593C9E3FA30734380F8B17CC348358D388,
	FMStereoPi_OnDisable_mFE1831B6291DC460253CCEDA89A84D935965252F,
	FMStereoPi_OnDestroy_m002D26363D2CCDEC8CB03355EF4CBEED67BCBA09,
	FMStereoPi_OnEnable_m5B8CEA197848EAD2A2B0F5B78F4A1F2B22278E7B,
	FMStereoPi__ctor_mEA034CD53544599F4F56FC0F195FF963EACF43E3,
	FMStereoPi_U3CNetworkServerStartTCPU3Eb__17_0_mAD211109B9D9ACF6355D40D7D09D2E30C91A0926,
	FMStereoPi_U3CNetworkServerStartTCPU3Eb__17_1_mFF8E324C3E21AC1048101868450F80A7B6DE1F08,
	FMStereoPi_U3CNetworkClientStartUDPU3Eb__19_0_mE7E8B0756AAED4615D1A62351D1CF295303F8471,
	U3CNetworkServerStartTCPU3Ed__17__ctor_mE0314820D968F3A49C0E6FF5B01E26717284BB7B,
	U3CNetworkServerStartTCPU3Ed__17_System_IDisposable_Dispose_mBE078FF37D259E010A03A6484F4A340A40D74D71,
	U3CNetworkServerStartTCPU3Ed__17_MoveNext_mF489580D4B92A92371F48E2429809DD9396ADF51,
	U3CNetworkServerStartTCPU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C9EA44B0303A454404E8381F01F118C8D0F8459,
	U3CNetworkServerStartTCPU3Ed__17_System_Collections_IEnumerator_Reset_m5007E2C8482C4CEFE81CF74D936FF0A7C07DC93A,
	U3CNetworkServerStartTCPU3Ed__17_System_Collections_IEnumerator_get_Current_mBA9F0CCFC24599A08DA4B1E951E5DA06A9352D61,
	U3CU3Ec__DisplayClass18_0__ctor_m134CEAE04BE2FEDE9EFA11D7295E24769F381845,
	U3CU3Ec__DisplayClass18_0_U3CTCPReceiverCORU3Eb__0_m7283781305E022DC9A5972AA90D14BD4B8836F26,
	U3CTCPReceiverCORU3Ed__18__ctor_mF4813BCD2E45B641891847411481E574D7318350,
	U3CTCPReceiverCORU3Ed__18_System_IDisposable_Dispose_m5F6BB7A97097970C9825699DCB95B6661949D6C4,
	U3CTCPReceiverCORU3Ed__18_MoveNext_mB7A962FC45293826B6A1E343B9CCEB3A22092CD6,
	U3CTCPReceiverCORU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m17AA4BB212021D7D2CAF7D6A959D39168C8CFB6D,
	U3CTCPReceiverCORU3Ed__18_System_Collections_IEnumerator_Reset_mC2BB26FFA15EEABCC8A8BBC632FEB34BED63744D,
	U3CTCPReceiverCORU3Ed__18_System_Collections_IEnumerator_get_Current_mF22F2BC3DCB0963C48EC14EC3B817D90FF0A5222,
	U3CNetworkClientStartUDPU3Ed__19__ctor_mD2D6DE21BB1A7D4A054760914643543A4C996788,
	U3CNetworkClientStartUDPU3Ed__19_System_IDisposable_Dispose_mA53E5147A12C4F90CB390C510B94C5C8CF2AD80F,
	U3CNetworkClientStartUDPU3Ed__19_MoveNext_m6879179DAAB8AB37CDA04D0AE4B416D7C9C38F89,
	U3CNetworkClientStartUDPU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C5CF71E1386F8AAEC4A69379236610C2DFA13D1,
	U3CNetworkClientStartUDPU3Ed__19_System_Collections_IEnumerator_Reset_mD4BB877B6B3A6BD3870668A3EB513F92BFE49C20,
	U3CNetworkClientStartUDPU3Ed__19_System_Collections_IEnumerator_get_Current_m03523BCBE848983E26C0F44806CEE08CE2EBCAB4,
	LargeFileDecoder_Start_m8A75DDD0E127C5A2C1D41CF78D7E0581C23A6B8A,
	LargeFileDecoder_Action_ProcessData_m2B9A1325FBD809329402EC76F777888B38C7301F,
	LargeFileDecoder_ProcessData_m2311EAA9443AA6B4E6F167E9A6FE1557E2B90F13,
	LargeFileDecoder_OnDisable_mB84E52E9D02E42F62C5A4965ADDDEE00470F74C2,
	LargeFileDecoder__ctor_m459E40D94CDE806031571D17C42113AF188B95AD,
	U3CProcessDataU3Ed__9__ctor_m074890C9D66E75DC716549F4CD67963C57A144DD,
	U3CProcessDataU3Ed__9_System_IDisposable_Dispose_m41BB9C9CB32546C55B694EC712193613A175AFCA,
	U3CProcessDataU3Ed__9_MoveNext_m461DFDB2ACC7EA99F8724939CE646299E30FDEB3,
	U3CProcessDataU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m80D685CFA55F83AE0FA445CC53F0DF227830CD62,
	U3CProcessDataU3Ed__9_System_Collections_IEnumerator_Reset_m1F1159FD210E745B8C334BAC10F3DDF768937431,
	U3CProcessDataU3Ed__9_System_Collections_IEnumerator_get_Current_m1CAA9605310113BD0F7D8D4CD0FA4B3512883CFD,
	LargeFileEncoder_Start_m7E65B00CBB0B821A62B40FD630976A66B40E37F0,
	LargeFileEncoder_Action_SendLargeByte_mEE15729811CA3E004C3499B71D4DF53BA8B3BC9C,
	LargeFileEncoder_SenderCOR_m5891195A95AF73F12FC5F91F084C70A4E2FB12AC,
	LargeFileEncoder_OnDisable_m0E1D113CCC30D1E325B61C39FEDE23FC91214960,
	LargeFileEncoder_OnApplicationQuit_mAB23876EEFC606B77271E0F1DC72A114AFD8F0A8,
	LargeFileEncoder_OnDestroy_m21FE6842736295D83F8994DDBCCF7D599195036C,
	LargeFileEncoder_StopAll_mE4864F26B82CEDE3E91DD0000DD77F1469D2820B,
	LargeFileEncoder__ctor_m847C903529B79899D7B8E43C9F3F419671522678,
	U3CSenderCORU3Ed__10__ctor_mF0E6C0CAB4B6DE80E30E476987AB86150658F8AF,
	U3CSenderCORU3Ed__10_System_IDisposable_Dispose_m7AB699C9394D6D03DAA48AFD83D6A96CE218A760,
	U3CSenderCORU3Ed__10_MoveNext_m6B896C113925C0DF2CA3885486AD351188835E7F,
	U3CSenderCORU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE0B49284FA72DB77FE6D7F1172585E4AAA780929,
	U3CSenderCORU3Ed__10_System_Collections_IEnumerator_Reset_mA3BA306D19B562FCD486EA9B27E2A058387C3E47,
	U3CSenderCORU3Ed__10_System_Collections_IEnumerator_get_Current_m941E2DF11D6FB1E53992F88B8C61150B9CDA4AD7,
	FMWebSocketNetwork_debug_Action_SendStringAll_m1DEAB244EB097156BA0ED95AFC813CC74B00A74F,
	FMWebSocketNetwork_debug_Action_SendStringServer_mEEB8867AE92E5ADD9A2556A41F77D617569444B0,
	FMWebSocketNetwork_debug_Action_SendStringOthers_m2D6A70B7E39CE45F289FC7DD91DE4864E7F732F9,
	FMWebSocketNetwork_debug_Action_SendByteAll_m2598E2B9E9441787C94BC83D76420960EF216618,
	FMWebSocketNetwork_debug_Action_SendByteServer_m961EE3773CEDDCBE60BD1F40D0F083C611DB57A3,
	FMWebSocketNetwork_debug_Action_SendByteOthers_m4A0D3032899D5304524EC3D295AD5A43BC8C83ED,
	FMWebSocketNetwork_debug_Start_mC045C96F0436FD89E570A98F2314C364EF7BE75B,
	FMWebSocketNetwork_debug_Update_m2C5B56FB1E949E99651D72EBE8DCEFC75FA9E8DB,
	FMWebSocketNetwork_debug_Action_OnReceivedData_mF6FD58582305745F792EC448480F0196C09ED58C,
	FMWebSocketNetwork_debug_Action_OnReceivedData_m6DFB9721A0A7096CC1C43E2F2D80D2196F2A0B4D,
	FMWebSocketNetwork_debug__ctor_m5D47B374D10296F8DC4572962F98ED3805DE146E,
	FMSocketIOManager_Action_SetIP_m2DACCC3335E8356660E2237B840B9F7AA9BA91FD,
	FMSocketIOManager_Action_SetPort_mF207E3AB17AB8787D725758C63436AE96646363E,
	FMSocketIOManager_Action_SetSslEnabled_m7359E9F5AEE4FDB5046332F9BA4A61A4494BE750,
	FMSocketIOManager_Action_SetPortRequired_mDD3EBF257B3D8514085A4006FA3BBB2A809C71A7,
	FMSocketIOManager_Action_SetSocketIORequired_m95F544F9629A528CC4E6D0D81E48ADF2165DE884,
	FMSocketIOManager_DebugLog_m74E32AD81199FF02ADE1EB49542C0EF9F482C1B7,
	FMSocketIOManager_WaitForSocketIOConnected_m360334C5828064FD0709AFAB709D4BDAC631A368,
	FMSocketIOManager_OnReceivedData_m564C1DC559441046CE292BBD4BA0153BBA051A16,
	FMSocketIOManager_OnReceivedData_mEBCC6B0CAE3726D891E40355587465E4EB85C221,
	FMSocketIOManager_Action_OnReceivedData_m3E9526566AEB6029BDB8355E3EF0B8964D5D7AD2,
	FMSocketIOManager_Action_OnReceivedData_m7E8F8E69D260AFCBE4A52AF29DA87FDCDFF65F0D,
	FMSocketIOManager_Send_m6205B10070EE4CD2BFD94D6632B3A40F7A8BA8AF,
	FMSocketIOManager_Send_mED23DAD687C6CDF827EC72509E80AD1FB133A1D3,
	FMSocketIOManager_SendToAll_mD2F07E2B650AC398967074E0A003AB1C85A40EA5,
	FMSocketIOManager_SendToServer_m0AF01A6E5BE220B8A7C181D7FADD0E2A37CF5BA4,
	FMSocketIOManager_SendToOthers_mFE9DDC4124E403A03F56C252885FF53C68B625B0,
	FMSocketIOManager_SendToAll_mAAE7AEABC061961123564512FCC6194D24330862,
	FMSocketIOManager_SendToServer_m4E32E2CD464CC7A38A555B33DE629335F4CCB49C,
	FMSocketIOManager_SendToOthers_m60EFE030EC94F81844227F5048BA8858BD28DBED,
	FMSocketIOManager_Awake_m846693922495F75D5DB5B543338B310787BE9150,
	FMSocketIOManager_Start_m3D40AFB1E6D0412F215B85006D57A330E4E6C06C,
	FMSocketIOManager_Update_m207B5AD1BD2BF2AE0DAE93AA1019BB796F4EF12E,
	FMSocketIOManager_InitAsServer_mFDA2A1F200ED996ABEABA298D6054FB27F26A2FA,
	FMSocketIOManager_InitAsClient_m967E18EC3F19FFF2D144CC8B2C5BAEE3A88F1B6F,
	FMSocketIOManager_Init_mD898C19204626E0B606F98B597A09FFC55172E21,
	FMSocketIOManager_IsWebSocketConnected_m731F48B7C8B7CB6B3CE0CF12285010CFD4387B74,
	FMSocketIOManager_Connect_m4FE665F0538F4B298D73F0C5B9866E8D65FE3A54,
	FMSocketIOManager_Close_m05B4EACB3634D9DDC6CDCA06AE6BB58F32500B05,
	FMSocketIOManager_Emit_m3B409BCF830812E434D0399463E86CF45E8DA1B8,
	FMSocketIOManager_Emit_m5508330F8D3C823C18B864581A66B0C405AA0769,
	FMSocketIOManager_Emit_mA238F16091B6DF4D311BB008ECB2AF924744947B,
	FMSocketIOManager_Emit_mB8A406CDEB1FA2470249AB8B5ABCC1543E2C460F,
	FMSocketIOManager_On_m1CA9EFBEADF999D55BB444D54D283484675D3191,
	FMSocketIOManager_Off_m9D273652A01A3C28EC5945CD4A8A61E295BAE973,
	FMSocketIOManager_OnEnable_m7A0FFD7BB5BDC714DEE967C03FEE79F1F646B12B,
	FMSocketIOManager_OnDisable_m0B1FD36A0387CE0124317A519A49041A57B1D68A,
	FMSocketIOManager_OnApplicationQuit_mE556B12B563B66CA2DEBAFD23B1928CF96735426,
	FMSocketIOManager__ctor_m6AAE84CF5B41EA55E238E8F98AD4DA2B8C5B163F,
	FMSocketIOManager_U3CUpdateU3Eb__38_0_m6BB955FCFBDEBF552F3B375B5DCE96BFAAE4C163,
	SocketIOSettings__ctor_m130029042EAC430661F51E7B3EA51A0333FBFE7F,
	U3CWaitForSocketIOConnectedU3Ed__23__ctor_m502AB0FB1E28D8B7B6065D5DFC5D79E4A09BAF79,
	U3CWaitForSocketIOConnectedU3Ed__23_System_IDisposable_Dispose_m90274DE1F545F83A61400FA206297200FFA0D3B5,
	U3CWaitForSocketIOConnectedU3Ed__23_MoveNext_m37A863F5EBC5D3B0D6C86A04A7CE3BB24D74D046,
	U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9833FFFC5A7501B9152F7896E842E844F4A3C9BF,
	U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_IEnumerator_Reset_mB9ADDDD54A6DC5CC287AE4FA279F5E59703B52F1,
	U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_IEnumerator_get_Current_mCBE89DB927B76215B3D44313952F2631B63666C0,
	EventJson__ctor_m60EB3DC222BE234D639F6FB67192E04A04193959,
	SocketIOComponentWebGL_Awake_m50BE39A293703A9258D6B3AF626B6A8517C088AB,
	SocketIOComponentWebGL_DebugLog_mB13687BDF523632A2D11453F07430C9E7577FF4C,
	SocketIOComponentWebGL_IsWebSocketConnected_mA226D79FB19CEFF8AE9C3844FF3CFB39ADEC6D80,
	SocketIOComponentWebGL_IsReady_mADCBA75B51F59C384FE75BE47120C21F11F1984F,
	SocketIOComponentWebGL_Update_mA3D44CD4036F274062D738B726759AEE97E2B460,
	SocketIOComponentWebGL_Init_m067DD067D5744ED41CB780B3D8A8C24BE840BDD7,
	SocketIOComponentWebGL_OnConnected_m193A05D49B9C7E319E229ED907F6F99211CFE915,
	SocketIOComponentWebGL_AddSocketIO_mF235A09507C32C094D7EF86F15F50135A1E66510,
	SocketIOComponentWebGL_AddEventListeners_m5A923E46FBBD18541C674EE1ED49A4FFBCC061C0,
	SocketIOComponentWebGL_Connect_m12823E19822749676E6D964BCE08D2AAA3B58D08,
	SocketIOComponentWebGL_Close_m96CC53D8EC7C842CF8E1F1ED30A9C92185B9C217,
	SocketIOComponentWebGL_Emit_m49B2504BA984A6C8B9AF54E841624F73BC3676B5,
	SocketIOComponentWebGL_Emit_m74CEE3BF253C5AC0856E2386F83B3EF30713FBA0,
	SocketIOComponentWebGL_Emit_mE9AE1494A9C9F7F5F6C9B3A374B19868E985E38A,
	SocketIOComponentWebGL_Emit_m1E3275BC575D4D35CD3F52E9C41C2A472812D388,
	SocketIOComponentWebGL_On_mA0FCE11E6AEA321DE5C94A805ADAF2CC7D0F28AE,
	SocketIOComponentWebGL_Off_mF0D903B57858CA6D39C4751B19640075A5670F7C,
	SocketIOComponentWebGL_InvokeAck_m0CAC2FBC4FBA22C903D2F27D09F944DC6150167D,
	SocketIOComponentWebGL_OnOpen_mD42EEEBDC92EE6713841EC1CA2FA0A969B2E5AE9,
	SocketIOComponentWebGL_SetSocketID_m484428E423FF885CFB5FB2D0239B332A494798A6,
	SocketIOComponentWebGL_InvokeEventCallback_m3AC80C2526FCEFC82FC9766658DAE0E0CAC2F0EB,
	SocketIOComponentWebGL_RegOnOpen_m97190DA0DF7A6B942D5FE97219F53C0309F47835,
	SocketIOComponentWebGL_RegOnClose_m5AE9071D6FE06A7C40D551008DAECF7E7ECEFE8A,
	SocketIOComponentWebGL_RegOnMessage_m95B57268F4F7F573AB78F610C2E0A45B5CE60A4A,
	SocketIOComponentWebGL_RegOnError_mBCF0197900B581E456CB126C57D64045696B12FD,
	SocketIOComponentWebGL_RegWebSocketConnected_mA47A1A8F82DD2292E5EA4030714EE19F8600DD7D,
	SocketIOComponentWebGL_RegWebSocketDisconnected_m66BC9634595CBE9A2D5C3A38127F5781D30787CB,
	SocketIOComponentWebGL__ctor_m20A0C18230A26AC642E91DA545D56740319CA19F,
	MoveNegativeX_Update_m91CFD2F04AA9BE0AFD12189FE8E51ECAB596A78B,
	MoveNegativeX__ctor_m622D7002829201D9A92CE7B03BB9394B52DEA36D,
	FlameLightFlicker_Awake_m3353D02A27F32B952714E04BD2F773ED77DF4364,
	FlameLightFlicker_OnEnable_m6D3199C75399D0130BA06CB7FE50B4E204FDA931,
	FlameLightFlicker_SlowDisable_mF0AE0A601FD5B924F9E2D7771A7BE2AD6493EB8A,
	FlameLightFlicker_Update_mECDE467860E5D1072EA55D1F73E640BC8653B237,
	FlameLightFlicker__ctor_m35B7D3C879E6CD983DFA267005CABB7835CD9118,
	FlameEventInvoker_InvokeMaxBrightnessReachedIfNotAlreadyInvoked_m18098365975E0D8166D366E6F4EB75F601646418,
	FlameEventInvoker_InvokeBurnOutStartedIfNotAlreadyInvoked_m3E98D587E0346B441712C101F601A88335E7F2DF,
	FlameEventInvoker__ctor_m41966F388B668DE030D235CFE9D773D72BACD575,
	ActorCOMTransform_Update_m42CC68376DF28C96387BACFC64C48515AE9CE469,
	ActorCOMTransform__ctor_mB7B969737BB7416B0E66C1CD4B8FDF1E8A7186CD,
	ActorSpawner_Update_mBB452D66B51B50FE636D74E7FF957A10A1FE2A07,
	ActorSpawner__ctor_m48C1E536BA52A4F4EDE74F5B90749B4619F13C86,
	AddRandomVelocity_Update_m6584A3F85D2E61F6D420B0B911B3C61637008449,
	AddRandomVelocity__ctor_mC1CFE0D59EF94BA30BA6AAB81CA6FF0899F84DDF,
	Blinker_Awake_mC92D3CBC36B46F324C8197377BC40A434109C876,
	Blinker_Blink_m4082F3A19006F464F4C14105BED460D5A1A0E273,
	Blinker_LateUpdate_mEE40A5EDA3EAFBB9CAD39BC413CB921E15CF6CCB,
	Blinker__ctor_mAE1E307D31F5C3BB83001FBC5B2D61F827A69F3C,
	ColliderHighlighter_Awake_m5920B8F626704DB40821E9BB9A85FDA1E0BFE4B3,
	ColliderHighlighter_OnEnable_m6F1425D169D88CC4D90E0A8F144A05C55F7E03B6,
	ColliderHighlighter_OnDisable_m2780DDE2CFE790FF4B04AD45654BDB74DD752893,
	ColliderHighlighter_Solver_OnCollision_m8A90B739BAB8461A0179C2B4CD4EE87AB3DE8D30,
	ColliderHighlighter__ctor_mBE8B58BC252B389A907738D87DAD06F094A62587,
	CollisionEventHandler_Awake_m744FDE1AB7D3F6777399B5108896E5E2CC61FE93,
	CollisionEventHandler_OnEnable_m2D3F33422D785CEDFCDCF1012A0B37B5F3ACB9F6,
	CollisionEventHandler_OnDisable_m75B03CB65B3D0226C04FF77A99D488596144BC32,
	CollisionEventHandler_Solver_OnCollision_m484E685CBD1E502504C567FD2D5CCB6F0860D616,
	CollisionEventHandler_OnDrawGizmos_m7939D47B098838344A41DD08F547354E268E07A8,
	CollisionEventHandler__ctor_m92805BEBFF6E7114772EF8DFF4D5C244467A479B,
	DebugParticleFrames_Awake_mA5C0197560CB8F73D7472974A693020EA29B28B3,
	DebugParticleFrames_OnDrawGizmos_m51378254E7930D9050D3355F810A3BC15A4CBBED,
	DebugParticleFrames__ctor_mBE2F687FB03E680B1957BB4D2C9F19E43F3CF260,
	ExtrapolationCamera_Start_mCB1FD551CBFB047EEA0FA534CE64CA969640D51E,
	ExtrapolationCamera_FixedUpdate_m622D9ECE2C7F91E1C5A70068B94B7C3E03AF4523,
	ExtrapolationCamera_LateUpdate_mB90259A761A12F0D8301E75BC381A6C0B32B443D,
	ExtrapolationCamera_Teleport_m8A6D37587FD1DF2518851886F5BE70B897DE0B67,
	ExtrapolationCamera__ctor_mA4E8D57C9E25BC7F9DA5676D79A7FC5505073032,
	FPSDisplay_get_CurrentFPS_mA9019EF8EAA0384D37F2DE714D84E5413E783C72,
	FPSDisplay_get_FPSMedian_m6ACF1BBC790E915C8CE3D94C797CE4A4E8F116DD,
	FPSDisplay_get_FPSAverage_m52F2116EA4DD6DA75E483FE24D21321CF9F2D4C1,
	FPSDisplay_Start_mF1CE43CCE2AAB57ECD8D8B623B7E7358AB163C55,
	FPSDisplay_Update_m8740416265A05D0C56CA05EC0DE3A12E5132B028,
	FPSDisplay_ResetMedianAndAverage_m82840B2C8D0189EA211AE4B14A8C94B22D932F70,
	FPSDisplay__ctor_mC4AD184EF09FAF5C43D89D0F7C211B2754D95B18,
	ObiActorTeleport_Teleport_m83F48614A9D7AE20261E92546E28B7C65C26DA1D,
	ObiActorTeleport__ctor_mCC83730596EA6276332F31F582F5C8D1F08EE019,
	ObiParticleCounter_Awake_m42D6C0254A2B27439769A96FE50705D33EC62B98,
	ObiParticleCounter_OnEnable_mA9798F6382D0620125A95B047C974EBA660926CB,
	ObiParticleCounter_OnDisable_m3224433810D75B192BD03D399ADE656864BBD594,
	ObiParticleCounter_Solver_OnCollision_mC585B439D90C23BA5FE74E65D3195651DCAEF392,
	ObiParticleCounter__ctor_m3107B0DABE8BEA1C8EB84C5A8EEEDB3C3E4B1B52,
	ObjectDragger_OnMouseDown_mC96AA07AABD47631E097AEA08E9A81AE1506EEEE,
	ObjectDragger_OnMouseDrag_m7E0B861E5546C709DED1ACF6F33ED31D6004FA6E,
	ObjectDragger__ctor_m97083AF9B77C7E2554A3D0701C6FAEE5D2282A6C,
	ObjectLimit_Update_m6689D4679CA59887B038EE4BD3F2294753C57E38,
	ObjectLimit__ctor_m8F9F789DA4B949FECADA677D21A26E893A0A5F41,
	SlowmoToggler_Slowmo_mF683360609D33F0B08F26A1019AAAD2BA21C8404,
	SlowmoToggler__ctor_m17E4BB8E004DE90384ADF6074E74BC7C383E7001,
	WorldSpaceGravity_Awake_m20A0105B37EEF41CE69A528E59C13099276E4F2B,
	WorldSpaceGravity_Update_m9BFD6836B3B4FD9F671F9CC77622279143C1F72C,
	WorldSpaceGravity__ctor_m8A54BF21A4069DBE4B27EA67FDABDC1C26766539,
	CharacterControl2D_Awake_mBEDC7D4202A9C5DB77612B42672FEEA151A07DC5,
	CharacterControl2D_FixedUpdate_m45A35A8B2CD76109467D0CF2DC222087997E9977,
	CharacterControl2D__ctor_m49BE177A57EA0352D49F2DD04C6C31CEEB8A7999,
	CraneController_Start_mF01684DCC2B564B112A4368E334199C7C19AD74B,
	CraneController_Update_mD5DCB3A3C60F3F5CB5CCA7FC0D073F2882682A7D,
	CraneController__ctor_mF9E29CD9F6BCDFE7AA44E7028D0B14CE7FDBC8FF,
	CursorController_Start_m0B22294E05CD4E228C22A26E2FED8E074EA1A83F,
	CursorController_Update_m415ECCFB2C1F667CCD6477CAF4D3A04E472A6C1C,
	CursorController__ctor_m180053D2620A25532FBDEEAA4B88D3033623014C,
	GrapplingHook_Awake_m4989432FB5EE2AEFF0DDD46692168EE9474D6E2F,
	GrapplingHook_OnDestroy_m966B313F51BFD2A96FABACA9536796964167391D,
	GrapplingHook_LaunchHook_mBA12CEA6F36A0CC3C72257BADBBC5364804AA12C,
	GrapplingHook_AttachHook_m36E627486AE7D4866048929EA3937F0E72963C17,
	GrapplingHook_DetachHook_m3ABB9323C8F0CE50EF0E3992970BA87794F1E7E8,
	GrapplingHook_Update_m5BF248318CAC3A90D6860DF6E25F996E3B67DF83,
	GrapplingHook__ctor_m9F75928EE3CDBC81C3D7E273DD9A02384D44DA43,
	U3CAttachHookU3Ed__13__ctor_m9164753579B8C9146C64A515E50F24A7BE11B1B6,
	U3CAttachHookU3Ed__13_System_IDisposable_Dispose_m4F8F47E389A0787F2AFC015113D1573AEA67A929,
	U3CAttachHookU3Ed__13_MoveNext_mBA824C87D8787B680AB4AB73B1315EABE2E5D866,
	U3CAttachHookU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86BE01E073EC4AD283C3A0D22EEB1018E4C524A6,
	U3CAttachHookU3Ed__13_System_Collections_IEnumerator_Reset_mCDCEBF557360577CE90A5311F7F8E4B554AF72A4,
	U3CAttachHookU3Ed__13_System_Collections_IEnumerator_get_Current_mF88E8E06C4CF813A1865B3E1D8B8E8BBAE36D5EB,
	RobotArmController_Update_m840A4F091544CA7A4C5B70686142BAD5D8935285,
	RobotArmController__ctor_m6E744B8D725B51231442CDAC4E05AA6ED00EB32A,
	RopeBetweenTwoPoints_Start_m2A83ABEFB3C1A4DA1A4701FB8E413FFEA91239CD,
	RopeBetweenTwoPoints_Generate_mD8EFE81845FB64A55D2F4A533EF45E3CB6F6D567,
	RopeBetweenTwoPoints__ctor_m2C5314D4CBAF87AC5EC44276ABB46BA0693092DE,
	RopeNet_Awake_m91A7C469E5162B122FABB5098D57C835406A3FB1,
	RopeNet_CreateNet_m22482DF393195B29521F64D4A0A50ABDF1D31A8B,
	RopeNet_PinRope_m486DBEAA8B687109D5F807D831CE538CAFC92579,
	RopeNet_CreateRope_mB8050E63103CB60CAEF1B476F8F6A9975383CA16,
	RopeNet__ctor_mA68E7CE8CF66BF32ACE6A9A50D495593FCA07EA9,
	RopeSweepCut_Awake_mB8FA7A121C0BAED955C801DA084FA4A48B226993,
	RopeSweepCut_OnDestroy_mEC2478E3A69C3A562AF0191F9E5D55D3393CD2E9,
	RopeSweepCut_AddMouseLine_m59987D970ACC960BDD9A0819F2571C9A12FC9DAA,
	RopeSweepCut_DeleteMouseLine_m434A0B4FC5BD368673C8A7842601E0D71E06BB24,
	RopeSweepCut_LateUpdate_m17E67E5258C2589EB4853A75468372AD8DDB330C,
	RopeSweepCut_ProcessInput_mFC4D9F1EEB6821E3714EC1831578044C914A783E,
	RopeSweepCut_ScreenSpaceCut_mE46DA04B03ED57211F47D85664211A4F526D57EF,
	RopeSweepCut_SegmentSegmentIntersection_m781A2777BCDBB2C0D734BF253EC279F7DF9CEA60,
	RopeSweepCut__ctor_m97CC3593ACF0C65A59D7111387B4F292F95D360B,
	RopeTenser_Update_m5184BA8D81DA278AFCC867BD6DEB6C08EC998668,
	RopeTenser__ctor_mAA2EEFCA72E252AD76523321BE66F3FF29EC4CE7,
	RopeTensionColorizer_Awake_m6C6C0DFE55FCF5D59D8DBA7C630208CB8A8620E5,
	RopeTensionColorizer_OnDestroy_mC5888FA0C292EFAA830AC640E3470086BA254709,
	RopeTensionColorizer_Update_m690AD3894DF36C91918409F904A52B9AC743B9BA,
	RopeTensionColorizer__ctor_mF617811E5D9A9ACE5B957AF7FA3529C03FA54244,
	RuntimeRopeGenerator_MakeRope_m75FA53894436086130B97A840F5FDD2FC27B16AE,
	RuntimeRopeGenerator_AddPendulum_mC44C741BD87348055DED2613E4B3F2C1F036FF27,
	RuntimeRopeGenerator_RemovePendulum_m45255D311564674390956D3FE09428A818699C2A,
	RuntimeRopeGenerator_ChangeRopeLength_m5DD09878372434ADBF820AF657C4B20245638828,
	RuntimeRopeGenerator_UpdateTethers_mCD9D9C785979709408E1D779541D301E6BB8F37A,
	RuntimeRopeGenerator__ctor_mC6334710591DE477B4E1977384C18E199AC229BB,
	U3CMakeRopeU3Ed__2__ctor_mC9B96B6A48DE9C6A24651C61A20885D4B7BFAC2D,
	U3CMakeRopeU3Ed__2_System_IDisposable_Dispose_mAF88235C710602C1022A42A1C91CEAC0CFCB8204,
	U3CMakeRopeU3Ed__2_MoveNext_mD3BFD03EF7B85140E88BF2A029A1FBEEB5B83504,
	U3CMakeRopeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1E799A4BCA7AA45E2E66D688FC6DBE189FDFE596,
	U3CMakeRopeU3Ed__2_System_Collections_IEnumerator_Reset_m21ABBBA4A71228DA2105ABF24C336ED84D28D1C2,
	U3CMakeRopeU3Ed__2_System_Collections_IEnumerator_get_Current_m46764A418CA35B282FE4A27DE6EE40D497E8AE97,
	RuntimeRopeGeneratorUse_Start_m016745BFD195A82E4B89668A5271E64703A3000C,
	RuntimeRopeGeneratorUse_Update_mF5A466B58BA8D471831BA6EA5627D5B1E31422FA,
	RuntimeRopeGeneratorUse__ctor_mC97354E58383A34851903C4A577F2F792C853C21,
	U3CStartU3Ed__2__ctor_mB2576F12E41960219B8D4B925C0E50DF2A66FC9E,
	U3CStartU3Ed__2_System_IDisposable_Dispose_m443D3F88C18C90B75A79EA1C9056696B94DCC154,
	U3CStartU3Ed__2_MoveNext_m7175EACB83490273BDD9BD4FD91E1A4CBE7C38F7,
	U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E92812F9987D045E11CB2653D3BE2668B002811,
	U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_mF62F9BBE198D60E90417C026611B81DD3461E8DC,
	U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m555815E4C6D334F953A3D60A4858AF925B400F3D,
	SnakeController_Start_m46F63DB6E2B01EC690B7867EBB2818BF26DC279B,
	SnakeController_OnDestroy_m31645CE0FDC49E9F37A7551F944809D10D7F73C4,
	SnakeController_ResetSurfaceInfo_mC9A57457F8CC93AD82F820B172FDB936C891FE4C,
	SnakeController_AnalyzeContacts_m36CCC13638269AA4530D576BA471E01C10DCD2C1,
	SnakeController_Update_m1D28B620F00A46D61598472DA3B03BEFEC2E5681,
	SnakeController__ctor_m5DD187E2ABC5A7F126EBE19C2A8D461DB837B9AE,
	SpiralCurve_Awake_mA3D312955BD674577F25C169DDF76F8F8A9E01E3,
	SpiralCurve_Generate_mEE3D2D5D691C41EF442DECC262CDC897EBF3ABE4,
	SpiralCurve__ctor_m6E99F9930728ADE344E5EFB74E9162C21B018EB3,
	WrapRopeGameController_Awake_m045ED1CAC6F8ACD3FB6BFCB027C9E009C4E7B06A,
	WrapRopeGameController_OnEnable_m2E487E79BAC01C141875950CB5DA281840D041FD,
	WrapRopeGameController_OnDisable_m29CEF7B251D8261332ED77E112AF3D95E2C830DD,
	WrapRopeGameController_Update_m19FCF5F3E86097865448E1E2E5B9203BFAEF8E44,
	WrapRopeGameController_Solver_OnCollision_m1423CF5FAD56ACDCEB4B6FA20E715B493E78BCFC,
	WrapRopeGameController__ctor_mF05C3F3006F45A280C0CDA8258C9219CFC2B7B18,
	WrapRopePlayerController_Awake_m8BA41B1F43833149BA401D08A91F497A3C7F44B8,
	WrapRopePlayerController_Update_mBFD4983CF399B3D30F4BDA939FD77F68C7BCC73C,
	WrapRopePlayerController__ctor_m18B2CE68E53693F8BAF7B092199B6F21BE38CC3E,
	Wrappable_Awake_mBAD2A0564D165C4A1247B404B7B26309AE392C07,
	Wrappable_OnDestroy_m099CFA91817E95C96D6B758AE181F5EFA469FA51,
	Wrappable_Reset_mB1C37B1D99E5E783AA6D118957C4BC92668C8B9E,
	Wrappable_SetWrapped_m396EA26793C4C341226F87069F7634B2CEBCC6A3,
	Wrappable_IsWrapped_mD1C6830A339AB6839C919D5CE52894B61C55FC89,
	Wrappable__ctor_mFD07DD359F22A7AF5B3D6C174775313840A37D49,
	OvrAvatarTestDriver_GetMalibuControllerPose_mBBCCC1B71F759AD7ED527CD6CB1CD33B7CD3CA33,
	OvrAvatarTestDriver_GetControllerPose_mEFC942D9BC35972518EACB2D6842F0D75FBADD0D,
	OvrAvatarTestDriver_CalculateCurrentPose_mE9C968915332F48537EDDA7485C04B74D6FA44C7,
	OvrAvatarTestDriver_UpdateTransforms_mDEB955A238CE55F370AB6B4A638F75B56A41AD4C,
	OvrAvatarTestDriver__ctor_mEAEC601ACCF2713D049DE7D53EEA8BE13028E021,
	PoseEditHelper_OnDrawGizmos_mD91616EF51322BFE811A39179D58F27E403FEB2F,
	PoseEditHelper_DrawJoints_mE9240514D1F4D82C2CBEFC30675E6F9AB1F90A5E,
	PoseEditHelper__ctor_m14B364DCCE64D239A94E4EFFB94FA4021B649AD4,
	GazeTargetSpawner_get_IsVisible_m027FD3301AF746556139C75719A516C56B604F3A,
	GazeTargetSpawner_set_IsVisible_m656741C7D34BCC648A2FFFDD279DF9E749BC65EC,
	GazeTargetSpawner_Start_m54A65954DEDFB05BE7B5ED8797EB9E9BBB69E0FE,
	GazeTargetSpawner_OnValidate_m0AD5B928BAC51C8F8C897A28A6F5B5C0C813F759,
	GazeTargetSpawner__ctor_m0CCC682B97C7E54454D450E31E3AD10FFFB01609,
	RemoteLoopbackManager_Start_m3D88A609BEE0B233D3DB9FF31CECB9A02EA0622C,
	RemoteLoopbackManager_OnLocalAvatarPacketRecorded_mA6B085BFEB8A43C62BEA927F6897DA7D2703C509,
	RemoteLoopbackManager_Update_m8580875FA8F63D34323D1DFE1FAABFE202ACCD83,
	RemoteLoopbackManager_SendPacketData_mE1366A23BF9706263E81A72270EBFD0552CC6F2C,
	RemoteLoopbackManager_ReceivePacketData_m6C30CA3730DC2B8C3A369EAEB893CBC6A23E51E1,
	RemoteLoopbackManager__ctor_m3411382CDA89A7DFD4D0BF9F90B554A6908A52DB,
	PacketLatencyPair__ctor_m8689B42992068A72EC9825E4D0D5BEA59C6CA17B,
	SimulatedLatencySettings_NextValue_mE0F727E70EE772D2380E3B700425942729C05EC4,
	SimulatedLatencySettings__ctor_mDC4701382DCAB4B34E2AE9BD7ED27DCEC0306B6C,
	P2PManager__ctor_mB6608327AD13643BD03C5E68280FCD0B6125325A,
	P2PManager_ConnectTo_m17196C73BF4289B60E1A87A89E346C9829531277,
	P2PManager_Disconnect_m1D1F9D79468C213DE441570ED4063FBB3382B395,
	P2PManager_PeerConnectRequestCallback_mA2B238158FFFD34238FF04504BE0DDAA79F46CF3,
	P2PManager_ConnectionStateChangedCallback_m4830C291FF361D6925FA5CE9990C75E28EBC1C1D,
	P2PManager_SendAvatarUpdate_m30D3B0C339473423E8E32210B7DA3A0EB837BF17,
	P2PManager_GetRemotePackets_m927B54C77C08A0B750468DC6DF462AACE6080862,
	P2PManager_processAvatarPacket_mB70AAB480DC71E2334F9024B392D086135503DCB,
	P2PManager_PackByte_mE3BAC7A5FF8DFB90FD34D7D094FEB52186D752CB,
	P2PManager_ReadByte_m62246D13ACE98725DDF1392AC7ABD465CF7ADC39,
	P2PManager_PackFloat_m813A4ADC04B138338DC3D7C8B32F3DA23074C103,
	P2PManager_ReadFloat_m533B3071FB90DB8C6AD2C4425CB586A0C0FD4507,
	P2PManager_PackULong_mE1BEA769C74E4C31B85D620D213B42890E56B982,
	P2PManager_ReadULong_m2B0A88069265C6B0D7AD44AE3AD6E97AD4E0976A,
	P2PManager_PackUInt32_mBD348EA800F6189A135EA39BB1B46115B1F5668A,
	P2PManager_ReadUInt32_m9B4BA0EFDA8E5242C3E342C87E9E39227957B579,
	PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23,
	PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_checkInput_m3D97AFD4C743C977628FB1AD033A11D510245E5D,
	PlayerController_ToggleCamera_m5B3789403E84830A876162599A26AC0D60FE63BF,
	PlayerController_ToggleUI_mBF6001D97AEB40ED13E5D3B7D949A32A1A8EE767,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	RemotePlayer__ctor_m1D2EA078FE1DC4E73403B21D7276F151B485779B,
	RoomManager__ctor_mB80958CB1C8945EFECA6721A48CCD7FD4FD642C0,
	RoomManager_AcceptingInviteCallback_mF113E845BBE2E6274413197E3F690A4CB6D31E54,
	RoomManager_CheckForInvite_mA303ECFF0732D3E8A431B195A44489B63A69FE61,
	RoomManager_CreateRoom_m269D2F174277B1C17D40042717CECB404F62A00A,
	RoomManager_CreateAndJoinPrivateRoomCallback_m9D46166FC8AF5D761D1AA469ED7ABCC68206F75A,
	RoomManager_OnLaunchInviteWorkflowComplete_mD38D5E0FE0AE71861760C9E1D1CF0145CB9062AD,
	RoomManager_JoinExistingRoom_m9EBD9604CE6455B6883010469352032A5194D33A,
	RoomManager_JoinRoomCallback_m1595E1E4A1682F648B5EBB3F38F36CEFB92DFDD4,
	RoomManager_RoomUpdateCallback_m5E41C1379A957AFEDB593D5E3CE5332795FA6E13,
	RoomManager_LeaveCurrentRoom_mECB32B537948F588D0033D33487CC18D82901396,
	RoomManager_ProcessRoomData_m8B72D5D186602982ECCA901CD501C5090FEC554A,
	SocialPlatformManager_Update_mBA54B1BBAB8E2AFCD46CB881DF8F3CDC237502F4,
	SocialPlatformManager_Awake_mFA4B8BE84D03C963593DB6464796DC2575FEEF63,
	SocialPlatformManager_InitCallback_mEC7EF74DB7487D0ACA881942CD05575580983CCF,
	SocialPlatformManager_Start_m2590A29622024579EF9E7981569F3668B05C4B42,
	SocialPlatformManager_IsEntitledCallback_m4A11C466B9DCFEA08AB0F6D1C6719410242D4CA2,
	SocialPlatformManager_GetLoggedInUserCallback_m0073552C6096F9CFBB9AD5F02428D9EE468AF816,
	SocialPlatformManager_GetLoggedInUserFriendsAndRoomsCallback_m0715D8600656E2FFCB5E9AA93CC948171EEFE364,
	SocialPlatformManager_OnLocalAvatarPacketRecorded_m7C64D22280077B4CAC09D8506E18D56665C197C2,
	SocialPlatformManager_OnApplicationQuit_mEE92A60A70DEAB4FE2D6E4FF93F36BE01DA46E5F,
	SocialPlatformManager_AddUser_m66C4DF835C4E3CAB01E2C7F5E3DF15BE470DE684,
	SocialPlatformManager_LogOutputLine_m352A91FC8CCDA409B3667731204AA3FD819AFFAA,
	SocialPlatformManager_TerminateWithError_m681E54AD4A07EA68D7C6C64E85C9E71B38CA81EB,
	SocialPlatformManager_get_CurrentState_m3179BA8104D33F2C148B8335C9DDAB76F0B88E0E,
	SocialPlatformManager_get_MyID_m7E25BD62EF6B364A1BD93382A06A0B8BA12B9C74,
	SocialPlatformManager_get_MyOculusID_mCC5312E6B8395BE762012533A6AEC13966E6A4EA,
	SocialPlatformManager_TransitionToState_m175EA407E7A7EA8960B43092D59BFF3F490B144D,
	SocialPlatformManager_SetSphereColorForState_mD20681844CD61712C4AD977BEC7C05187CE31EE7,
	SocialPlatformManager_SetFloorColorForState_mFA400D5420C4E9EFEC67F866046BFF60086D41BE,
	SocialPlatformManager_MarkAllRemoteUsersAsNotInRoom_m0DC4041159E579F85969424B72400DF5D1CB3822,
	SocialPlatformManager_MarkRemoteUserInRoom_mE819D28052F78D02AE063DEB97595511D17FD0C2,
	SocialPlatformManager_ForgetRemoteUsersNotInRoom_mEA043618BBD474DBF2A7477A6CA1B8BFFB682EAE,
	SocialPlatformManager_LogOutput_m13794D4E7BF84D05CD292E7C7F25ED03A7C0AB37,
	SocialPlatformManager_IsUserInRoom_m01AFEE74247D50385D62456E3DA432689550BA60,
	SocialPlatformManager_AddRemoteUser_m0E67E5165F5A716D8D19B6AE53EB50C22F4915A4,
	SocialPlatformManager_RemoveRemoteUser_m63251A56F7060B8814B76FBA9351A2B099FBCD5B,
	SocialPlatformManager_UpdateVoiceData_mCF8F832AF0077482F63563E55AA367BA9AC5E7EC,
	SocialPlatformManager_MicFilter_m0FB5A641DE3104796155D242D62D10DFFB182F54,
	SocialPlatformManager_GetRemoteUser_m8770DF5599B8C4BC117AF4C35BB2B43E8C806288,
	SocialPlatformManager__ctor_mEB334D6BB761E5915EFC79D1CFA2DE61E1915700,
	SocialPlatformManager__cctor_mE631E0315F36815C9F3F6BF35F3E232824B92D92,
	VoipManager__ctor_mECBEE9E5787626F81F3487717805DA107A1AEB2D,
	VoipManager_ConnectTo_m8544850E93404C45736EA96AF0C1707F077283C8,
	VoipManager_Disconnect_m0652B9A829FAC11D7617211FC41B03217BC830C1,
	VoipManager_VoipConnectRequestCallback_m0B922EF366F6CD9902ED9298C96CD640B551B655,
	VoipManager_VoipStateChangedCallback_m82F3EC6D4F09BD39F4E5CEFF77A930FD83F503E5,
	GazeTarget__cctor_m63D70FB4A884154F1A873541498B2F5FD847CEAC,
	GazeTarget_Start_m573A7A8C6E6AE8114DD2942011CF6CFB672B3A7A,
	GazeTarget_Update_mEBBFC7C145132060351AC1F16A7AB0828ECF5F4D,
	GazeTarget_OnDestroy_mDE8B5B28EEB3452A56270E395875A49398F1BA84,
	GazeTarget_UpdateGazeTarget_mE12B5551370A1534E16B667052CE53B64ECAABD2,
	GazeTarget_CreateOvrGazeTarget_mBF5ADB60F51A0D7B854D124D6842655935E83EC1,
	GazeTarget__ctor_mC673116584E41E083DDA84E891F70F0F53613519,
	AvatarLayer__ctor_m76756963656B658651AD3BAD4D99F5F14A297529,
	PacketRecordSettings__ctor_m052D8B988CA60332A17788508A2F73B347514D4C,
	OvrAvatar__cctor_m02C03ED710DFE5A5F6B2895C21E3C1A95E070E15,
	OvrAvatar_OnDestroy_mC412A1B3D84723E215D7062BF9A176AACD226A72,
	OvrAvatar_AssetLoadedCallback_m70921F135999AAC208A86DC9AC5AA850A63515F7,
	OvrAvatar_CombinedMeshLoadedCallback_m421FEAE81DA1784E5EAA835E9BF2C03A017CEB49,
	OvrAvatar_AddSkinnedMeshRenderComponent_mB46FB768614CD12912521699A1FBFB3459F12119,
	OvrAvatar_AddSkinnedMeshRenderPBSComponent_mAD30ABDE44F4344B5AF02513AE2F5698381861FF,
	OvrAvatar_AddSkinnedMeshRenderPBSV2Component_m8531195959A66CD1C74B47EE78FC734D048497E3,
	OvrAvatar_GetRenderPart_m0D4B0495AE9C8D2334044275E7D0E9EC2D425A0C,
	OvrAvatar_GetRenderPartName_m04E97ECBD6A71336A0CB32FCB9078CC7DDD9CFF8,
	OvrAvatar_ConvertTransform_mC81F3B94DCE65CB4EE009963840332F32E7983A1,
	OvrAvatar_ConvertTransform_m21D8832CB2832797E104B022016A1EA739E1E147,
	OvrAvatar_CreateOvrAvatarTransform_m0741758157287F1C90255D82D7A9936EEB3A5FE7,
	OvrAvatar_CreateOvrGazeTarget_m967676E78CD412D13B2335E987285D81190198BB,
	OvrAvatar_BuildRenderComponents_m2D4D78E1719099DE258FCED2862D75ACE00989A2,
	NULL,
	OvrAvatar_UpdateCustomPoses_m96AC60583AA8BEB4F68C0FF1D520EC8F29A2DA32,
	OvrAvatar_UpdatePoseRoot_m2864E2FD383663A9A77551499E69F8F2DA374BEE,
	OvrAvatar_UpdateTransforms_mBE373C2F2D62BAE010C42580CC4A9E47BE63F1EE,
	OvrAvatar_OrderJoints_m207BB5F508A31EA26C0FFDBA9D25CA35983B45F0,
	OvrAvatar_AvatarSpecificationCallback_mEECAAAA1502EAF866D2D5ABB985B4A6025566FC4,
	OvrAvatar_Start_m4AB09F66F023610098F17600CC522620E3A1CE37,
	OvrAvatar_Update_mF15EAF54811291C91AD417940E5B0133B54B48BD,
	OvrAvatar_CreateInputState_m4A275E979A58BD232419463DB6F63EAC719F6F52,
	OvrAvatar_ShowControllers_m4D759FC08A9EA4CBC77C60CA09AE2E7F199311C1,
	OvrAvatar_ShowLeftController_mBA2711BBD63E19516B156586B4FDAD3ECEDFC5D0,
	OvrAvatar_ShowRightController_mA362260B0837A8408F13AADDD0547F6E1C90EF25,
	OvrAvatar_UpdateVoiceVisualization_m9FF25EA1FC1D9F596055875EDB6E5D92680F1712,
	OvrAvatar_RecordFrame_m0E1F196470490BD6D1B9A136CE22919DC0B3ED6C,
	OvrAvatar_RecordUnityFrame_mE29CEBA1AA8F7B081B1AEE21E2C4AA605895956C,
	OvrAvatar_RecordSDKFrame_m741C1C1B7CCF95205A635E733E5F721BFCC72F45,
	OvrAvatar_AddRenderParts_mD338D0A74FF393DF9C583D1C29D1ECC63877029C,
	OvrAvatar_RefreshBodyParts_m5FF1B92685C1690638FD726C4F7A434F4DFF57ED,
	OvrAvatar_GetBodyComponent_mB0E0D5918861985AD7CC4468D68505E193312915,
	OvrAvatar_GetHandTransform_m1165DA8669BF988BA908E700918D08F721B9A44B,
	OvrAvatar_GetPointingDirection_m729CF3681B6FBF203B99ECF4082F0B8BC75B2D2B,
	OvrAvatar_UpdateVoiceBehavior_m955A8CBBBD42069D9342B9935916B0455E11E154,
	OvrAvatar_IsValidMic_m28534EEB808B1F34EA16C1F9AC74846400CA2B63,
	OvrAvatar_InitPostLoad_m052DD75178E0312A881F5FCB0160347146D60BF7,
	OvrAvatar_ExpressiveGlobalInit_mE82993B820E52426286E4B3989536243BC7B1332,
	OvrAvatar_InitializeLights_m6C51C212514BD1F867E57EE9D6A7640F304FC9EE,
	OvrAvatar_CreateLightDirectional_m048B90BE869A4CF71DE15CA21881FD2657CB22F5,
	OvrAvatar_CreateLightPoint_m3DFFB4AC272AFE99ADFDE0589545A7ED9C649943,
	OvrAvatar_CreateLightSpot_m1ECA6DE74031B1843269B7C5CB5D788668F10338,
	OvrAvatar_UpdateExpressive_mFAC1796755CE934DAF447CC55DDBEA027DFFF28C,
	OvrAvatar_ConfigureHelpers_mB9B34DCC14A1997470F18AC0061F2BC5EDB30B86,
	OvrAvatar_WaitForMouthAudioSource_m4B9D1847BB4B32B21C5D28794A837FC3326219C2,
	OvrAvatar_DestroyHelperObjects_m5685285FD7FBD4DA075C66DE1B64C418D07FC129,
	OvrAvatar_CreateHelperObject_m414E59562DDAFB8654E4E2C356F690CE057EDE1C,
	OvrAvatar_UpdateVoiceData_m2C3C9A8DECB05BFA3253789D3BB1563C080218C6,
	OvrAvatar_UpdateVoiceData_m21182F84E276DD53D4D4D4B560F759A02BC9987B,
	OvrAvatar_UpdateFacewave_mD60684F907CC55D1D0ADFB1285E551B7708312B6,
	OvrAvatar__ctor_m3BD0FB8A94A434644B64D4F0AEC77300BE61E1C1,
	PacketEventArgs__ctor_m0D15EF1306EC9B22B3DE0C03199ACC05DB3FBFC2,
	U3CWaitForMouthAudioSourceU3Ed__137__ctor_m348BF982E093DEB28505198EE74C66C5C4EC91CD,
	U3CWaitForMouthAudioSourceU3Ed__137_System_IDisposable_Dispose_mEF1934995C5F6E9A73296732199E48298B0DD301,
	U3CWaitForMouthAudioSourceU3Ed__137_MoveNext_m304A31AF4083356D80E80C3B29886919DD7F8A6A,
	U3CWaitForMouthAudioSourceU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7158266C8CA3498E870069B16B95C9207B686BA7,
	U3CWaitForMouthAudioSourceU3Ed__137_System_Collections_IEnumerator_Reset_mEDBCFD5369F19AC4FB15794210B618DB424EBDA0,
	U3CWaitForMouthAudioSourceU3Ed__137_System_Collections_IEnumerator_get_Current_m2A982A178BFE62260BED95D4341B8A4BFF23E337,
	OvrAvatarAsset__ctor_m39F0A8F0CF014E13531C5F97B8A70731CC1138C3,
	OvrAvatarAssetMesh__ctor_m9CCEE6602802003978B059098F85B792B2FA462F,
	OvrAvatarAssetMesh_LoadSubmeshes_mC605617D9D31B1AB34245A5822BFF2DC49855FA7,
	OvrAvatarAssetMesh_LoadBlendShapes_m0499ED13EBE059D00048A4D0AAF2BBDC4D4CDB77,
	OvrAvatarAssetMesh_SetSkinnedBindPose_m833655AE4A7D9E909D6C0ED3DA78F9876185A40B,
	OvrAvatarAssetMesh_GetVertexAndIndexData_mEE29DB37A7DDA8086D2922A32B744E7514101661,
	OvrAvatarAssetMesh_CreateSkinnedMeshRendererOnObject_m71695AB5E91B1E2A78FD416FED69C6F26CE1EC51,
	OvrAvatarAssetTexture__ctor_m376B684F3BBCE8A5510C5573AFAF79396F5F4DEF,
	OvrAvatarBase_Update_m76690377EC334BEE5C0A8B22BB6E39F98226B2D7,
	OvrAvatarBase__ctor_mE5886D4BC00231C332068218A68AAEE3AF258250,
	OvrAvatarBody_GetNativeAvatarComponent_m350C8348D3AA56732F047EFA408B563B3256DBDA,
	OvrAvatarBody_Update_mFA3B50D702E9DC5BBDE34D4C4011465E98D6333E,
	OvrAvatarBody__ctor_m8F3B64CFEBDC1A537FA24AAAA154B96E81636D24,
	OvrAvatarComponent_SetOvrAvatarOwner_m49570F220F056450ECD604B205893AEC914F1243,
	OvrAvatarComponent_UpdateAvatar_m379B3EB66C76F958C6BEA47072B3F522609837CF,
	OvrAvatarComponent_UpdateActive_m5FB9CEE5C5A7C00B03E4694A93CBAA4D4CB7A7F1,
	OvrAvatarComponent_UpdateAvatarMaterial_m7AE0E007ADF57058775F3451C4DBDC9D7C914F72,
	OvrAvatarComponent_GetLoadedTexture_mBF2D9B2CFF2F9C22B661DC446C30EE924080DA7B,
	OvrAvatarComponent__ctor_m9F063BF4835CF289D94707841DD135026645900C,
	OvrAvatarComponent__cctor_mEF653C66360FB1ED2D912492152A07FD533363BC,
	OvrAvatarDriver_GetCurrentPose_mAAED7AE7B7BC0895E565AF3DEC2BB53842A0ECCA,
	NULL,
	OvrAvatarDriver_Start_mEE3CDA3C83A3C9EF9B9E9AC8E0527D43747A59F2,
	OvrAvatarDriver_UpdateTransformsFromPose_mFA51834F9EEC8A48DBC35051F7324011EBC6CD3E,
	OvrAvatarDriver_GetIsTrackedRemote_m656EE8484BA274FC3ADB3C592EF6D40D0B621454,
	OvrAvatarDriver__ctor_m8A450A986441B188BBCAEFEDF8333ACCAD64C7EA,
	ControllerPose_Interpolate_mFFCD926E9C586FA2A7BAE504FCEB42A231D31300,
	PoseFrame_Interpolate_mF56FB29ACBA999727C9C7C7CC80534F60FB927ED,
	OvrAvatarHand_Update_m1F53059E67DE74F863992E1F4CA5B4A40D298082,
	OvrAvatarHand__ctor_mDABA9708428E61124B640215227B83C857879F7B,
	OvrAvatarLocalDriver_GetMalibuControllerPose_mC41F7699C30BA32C9D609266E79CA0E0188E027E,
	OvrAvatarLocalDriver_GetControllerPose_m120C1D0A44C4EF7C865EB093876BD718687EC523,
	OvrAvatarLocalDriver_CalculateCurrentPose_m52CB4B974BAB9EB4B728620B7501B1823E121D51,
	OvrAvatarLocalDriver_UpdateTransforms_mCCF860D225CA81F365C4FFCD5C8F2BCA9975462D,
	OvrAvatarLocalDriver__ctor_m89C5BF97359084F41D9B973DA9D01815BFD57EBD,
	OvrAvatarMaterialManager_CreateTextureArrays_m4977F1D7A1834D0B557B8646C582324A566B4C2B,
	OvrAvatarMaterialManager_SetRenderer_mA11FDEE6BC73C5B68170F30B6CE713397B9E812A,
	OvrAvatarMaterialManager_OnCombinedMeshReady_m496ED8C56791DF79B950B75BF346FFA777D50F57,
	OvrAvatarMaterialManager_AddTextureIDToTextureManager_m585FA8EB055EDD9B0FF7E96CE977AC50F707DDB5,
	OvrAvatarMaterialManager_DeleteTextureSet_m0E185753E4AA75E85181E7CDD872B9581A08CCCB,
	OvrAvatarMaterialManager_InitTextureArrays_mD3FA9B2FBA114FB8CBED80A8E6F0C7C3D4B9F45D,
	OvrAvatarMaterialManager_ProcessTexturesWithMips_m029BCD34869335D62816718377CA75F15C8AAC07,
	OvrAvatarMaterialManager_SetMaterialPropertyBlock_m5E8587155BF7C951269D280170EE6A1D20199C33,
	OvrAvatarMaterialManager_ApplyMaterialPropertyBlock_mBFBBAB70543AD985D044D1B1261EB07B8226325B,
	OvrAvatarMaterialManager_GetComponentType_mC62C71B9DBA0F6A5B3E09BED485E18118DFB82A4,
	OvrAvatarMaterialManager_GetTextureIDForType_m9CD95EA1D320AEDBE52F290FEF8FDEBAFB1626AF,
	OvrAvatarMaterialManager_ValidateTextures_m34411C5A1DD8ED20357A53E77C500A9687D20924,
	OvrAvatarMaterialManager_RunLoadingAnimation_mDC16C0FD084B74B67F28E53111AF0C8EFA967BBE,
	OvrAvatarMaterialManager__ctor_m768DE47BEF2C881EA5071AFA38F8086C5D476E2E,
	OvrAvatarMaterialManager__cctor_m01A9BF6AD6CC70B929652B9D94DE52DAD037E3F9,
	AvatarMaterialConfig__ctor_m94DAF6BC95EAE1BEEFB2C2E7D2CAB0942F14AEDC,
	U3CRunLoadingAnimationU3Ed__49__ctor_m43C27C76F38D8F3F3F60BD229ACA2F5DE8A34A81,
	U3CRunLoadingAnimationU3Ed__49_System_IDisposable_Dispose_mE08FA819A41936B454B0DC7414239CE5E49BE750,
	U3CRunLoadingAnimationU3Ed__49_MoveNext_m37A6368254A8C97B72D2550D01F14CAAFC442121,
	U3CRunLoadingAnimationU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC75F672BB6BAD350DD052C15CA28E17A0E2C380,
	U3CRunLoadingAnimationU3Ed__49_System_Collections_IEnumerator_Reset_mE0F680342E7E1E8ED7EF4068256FD39CEC1C3CB9,
	U3CRunLoadingAnimationU3Ed__49_System_Collections_IEnumerator_get_Current_mA67FEEB470D478F15DB1A503103A714AB1794D33,
	OvrAvatarPacket_get_Duration_m296F69BE606B54FA4E43D7B7F20C96CE370C88EC,
	OvrAvatarPacket_get_FinalFrame_mCEA24FF57B6279986B3FE4FE568276E571D1E4FE,
	OvrAvatarPacket__ctor_mD52B967DF3F4460B58402F2F9B810ED42BEDB3BB,
	OvrAvatarPacket__ctor_mE1CAA121E13C21C0DB97398373A56538AB8778D6,
	OvrAvatarPacket__ctor_mE8DECB846A7BAC69B4D50720C471E08F19A00371,
	OvrAvatarPacket_AddFrame_m38AF69FAF2A01CC1F49F12ABDFB679C3210EF040,
	OvrAvatarPacket_GetPoseFrame_m6F949DB2D47B799213B7C68470222DC4ACDFF4DC,
	OvrAvatarPacket_Read_m066559D73CE8DCA5AB1C5D53EB6F39FFD4545681,
	OvrAvatarPacket_Write_m7642998DF5BEDE118D40278CCE95E7CEA7BA1FEE,
	BinaryWriterExtensions_Write_m446C798DF132C6F24343370DB33C59326A1E95AC,
	BinaryWriterExtensions_Write_m21975F2DA2BAFC07AF3CF90B6203DF169A4E157C,
	BinaryWriterExtensions_Write_mC7841D88FC4F08AB79DFE3C521E197302C6BE074,
	BinaryWriterExtensions_Write_m4C10A3AAA917ADAB2B414844E19A4DF29EB3F136,
	BinaryWriterExtensions_Write_m185C8626C0C25BE258AD731FFFA746C4D7EFC754,
	BinaryReaderExtensions_ReadPoseFrame_mFD945F0699DD814338CE03BDCA2211495254431E,
	BinaryReaderExtensions_ReadVector2_mE3C25910DDFCFAF8981CB5001DD5EDF3098671A5,
	BinaryReaderExtensions_ReadVector3_mB04805367268FC0013FF5824C402B6F65D3DBB80,
	BinaryReaderExtensions_ReadQuaternion_m9B69DBADD15BB59D47169D955EDA4AFBB0DF8BE3,
	BinaryReaderExtensions_ReadControllerPose_m26574F17311CB8A944640B3757455239CEA73C0E,
	OvrAvatarRemoteDriver_QueuePacket_m2E81DB0E70F4BFD20B3758C568BDD88562D4622F,
	OvrAvatarRemoteDriver_UpdateTransforms_m6B1FC6DFD64DE9AEB1FB6BCACB6CB88A38103D12,
	OvrAvatarRemoteDriver_UpdateFromSDKPacket_m528D84F57A02F1F2CBB2E69FC1C16DADB66C52B5,
	OvrAvatarRemoteDriver_UpdateFromUnityPacket_m26C3888D91024E4A5FE2F564B2D90DFCCAD9F10C,
	OvrAvatarRemoteDriver__ctor_mADF67945EB1D689ACBA4F4741BD94957D3386132,
	OvrAvatarRenderComponent_UpdateActive_m5DAFE0C6899E5AD32104CD4575CD253D42EC151C,
	OvrAvatarRenderComponent_CreateSkinnedMesh_mFE6D9DA8E5F1B95F0782E4490CD94A7576DDDF35,
	OvrAvatarRenderComponent_UpdateSkinnedMesh_mBD6812162FC47D2A39BEEE9233C049B6DAD4734B,
	OvrAvatarRenderComponent_CreateAvatarMaterial_m7CFCCA6394C8C11644BBB7F9240A77BB459C6126,
	OvrAvatarRenderComponent__ctor_m24877CDBB946D3B93B6D3F9E653C380C09160376,
	ovrAvatarComponent_Offsets__cctor_m79EA446A7D9C6A608AF45C62DF26A4AA27A0C779,
	ovrAvatarBodyComponent_Offsets__cctor_m37DCA998128851151E27F431221B6E7728F103B2,
	ovrAvatarMaterialLayerState_VectorEquals_m0F49E3B7839726FD77C50D6AB1749DC8C6BC85F3,
	ovrAvatarMaterialLayerState_Equals_m1C14E1DBA874C1083F95A564F30780DEF30E435E,
	ovrAvatarMaterialLayerState_GetHashCode_m3302E925482D7B9E2072339A1F3C3EF15F46B519,
	ovrAvatarMaterialState_VectorEquals_m8B0E482C9D79CB23F353C80DCAC9A9ECA0D0679B,
	ovrAvatarMaterialState_Equals_m293D1C2CBB4C8403F0D14E1A68DB14379FEF491D,
	ovrAvatarMaterialState_GetHashCode_m573CEF4EF15464DAF18046F8F0B7E56705B55740,
	ovrAvatarExpressiveParameters_VectorEquals_m12124A3A84669FEF3298F1AE3AF7707E6FE4292F,
	ovrAvatarExpressiveParameters_Equals_m501E5CF62F65200176CC04A883C7CD3CB29C0DB1,
	ovrAvatarExpressiveParameters_GetHashCode_mEA3158598F1C76B2480355833396802E41C4E75B,
	ovrAvatarPBSMaterialState_VectorEquals_mBE6A57EDA248DF344BCC3C204832D79266C1ABE9,
	ovrAvatarPBSMaterialState_Equals_m1C1A3BFBDC22AE8A5A6A3E28725926A9B34C3209,
	ovrAvatarPBSMaterialState_GetHashCode_m8D5F74AC0D6E2F3546275C51C5142D5572BFFA78,
	OvrAvatarAssetMaterial__ctor_m7544427A852734E8A5D47AB90DF220301B710B56,
	ovrAvatarBlendShapeParams_Offsets__cctor_m6D9041980A2912344E42DAF204B833E30235EC29,
	ovrAvatarVisemes_Offsets__cctor_m4E405CF2A255B832C38697BCE85951DB07A73168,
	ovrAvatarGazeTarget_Offsets__cctor_mFDD3A73DC15E156590CD887592D50048E4249316,
	ovrAvatarGazeTargets_Offsets__cctor_mD23B3BAF735FD7EC7FD1B4B7109AB4D04324FC46,
	ovrAvatarLight_Offsets__cctor_m521DE5242539E17683EE76311BC289EDE65E04AE,
	ovrAvatarLights_Offsets__cctor_mAAD61322FFEB720780FEA560BCC4E6888D7C6990,
	specificationCallback__ctor_m49430870F265EE01A85245F2A8CDF76EF07E1620,
	specificationCallback_Invoke_mE6CEDC7F76E008998E965BE52ED26ECDE7428914,
	specificationCallback_BeginInvoke_m39BC7E000A26EC2E691ADDFE8DB980B349237F80,
	specificationCallback_EndInvoke_m67BADEF6AAE9318D70EE75B34F1365205D9E8F1E,
	assetLoadedCallback__ctor_m98D354A6C2CC719D0463B26F84A0157EFFD0702D,
	assetLoadedCallback_Invoke_mACE414A8D1CE4671949BD46DA45D10CCD3785523,
	assetLoadedCallback_BeginInvoke_mFEBEB34E8B4A4DCDBAB0562B43C59D013B1B624E,
	assetLoadedCallback_EndInvoke_m7D155B49E64129ADEC1DAB24680462380465F5C0,
	combinedMeshLoadedCallback__ctor_m95CE9B5144E8DE923CE6ECF548704E880D70391D,
	combinedMeshLoadedCallback_Invoke_mF25554358775A40E62C19201FD51AFD6C7750656,
	combinedMeshLoadedCallback_BeginInvoke_m7B0251721E56C2D3CCD2F2CC13D29DDBB7787CB5,
	combinedMeshLoadedCallback_EndInvoke_mA989F9F3703FB365A6BC5D656EA016B78C3F7B47,
	OvrAvatarSDKManager_get_Instance_mB9B5FF8B8F1E78391247927F232F3F7383E70563,
	OvrAvatarSDKManager_Initialize_m5BD26AE5A66236ADADDFFBBEC2D22E93885AE0FF,
	OvrAvatarSDKManager_OnDestroy_m5380FD15A8AAA3F246DBD4373EF79C5C9DF1CEEC,
	OvrAvatarSDKManager_Update_m44DF588B43A11234E2FF4A64E6B87F651777367B,
	OvrAvatarSDKManager_IsAvatarSpecWaiting_mF531B80DACCF54019AFE42894710C6C0AA9EABF6,
	OvrAvatarSDKManager_IsAvatarLoading_mFEDFCD39684FAC17722B08023D9DA140A1C4322D,
	OvrAvatarSDKManager_AddLoadingAvatar_mF0590DEF0E1CF074E92085951129624B08C06FA0,
	OvrAvatarSDKManager_RemoveLoadingAvatar_m58DCEA6BC09480677D9134202DB39FBEB8945A0D,
	OvrAvatarSDKManager_RequestAvatarSpecification_mD355097C0CFA323AB0E6D2207D370300446EA9FF,
	OvrAvatarSDKManager_DispatchAvatarSpecificationRequest_m69F1994F001D5F38A64479A033D9C7CE56804969,
	OvrAvatarSDKManager_BeginLoadingAsset_mCC22D7727D70A5BD2A4B2CC5CD788C5480BA3663,
	OvrAvatarSDKManager_RegisterCombinedMeshCallback_mF1A9841550BFA90A55371808D502EA719BE41783,
	OvrAvatarSDKManager_GetAsset_m5327170071598FD893DFDE5A4272A30F5750C55C,
	OvrAvatarSDKManager_DeleteAssetFromCache_mB2BEEC55CFB786AF0B6EE172F2C75B7AAB2A97B3,
	OvrAvatarSDKManager_GetAppId_mDD2D0505570BF16344263D082F9EF0D419CD5EDA,
	OvrAvatarSDKManager_GetTextureCopyManager_mAD832C2A625A244AA199A83CF6729915A056625A,
	OvrAvatarSDKManager__ctor_m2D3B0B78DD42EDE0C78AF88A4C57AADFD7744338,
	AvatarSpecRequestParams__ctor_mC4E88966CA0552017DF34A410391946CFEF4021D,
	OvrAvatarSettings_get_AppID_m5077983A11AC7223C3C80C81C2C3B348B9231F86,
	OvrAvatarSettings_set_AppID_mFBF334F95276E3D8C9379870787EE2316E46CE9F,
	OvrAvatarSettings_get_MobileAppID_m422288C504386C1484BFED15CE1BEA3EDEB9E9EF,
	OvrAvatarSettings_set_MobileAppID_mB599A44E4BD07D5A6E5EA14180275EAC58AF1487,
	OvrAvatarSettings_get_Instance_m1F2D4D698A20F907854549C31911AEABCC5424EF,
	OvrAvatarSettings_set_Instance_mB47E146E75A592D2E0BB1961BD6BD50547CBD247,
	OvrAvatarSettings__ctor_mC20E3765ACEE15B37A1A00AF1941524AA325D8FD,
	OvrAvatarSkinnedMeshRenderComponent_Initialize_mC1AFEA820CD38750C5E43AE3AE1B507D1BE99406,
	OvrAvatarSkinnedMeshRenderComponent_UpdateSkinnedMeshRender_m1CE4439BF8D73BF8503272232D166AAFA545F81E,
	OvrAvatarSkinnedMeshRenderComponent_UpdateMeshMaterial_mA2EAD33553131CFF85ABE2A788DBB3EF9A286FB9,
	OvrAvatarSkinnedMeshRenderComponent__ctor_mAAFF60F8F7011F8CABF1893F3C200C1F2E29C637,
	OvrAvatarSkinnedMeshRenderPBSComponent_Initialize_m80494C16D424C44B683B2390A16FC600A5ADAB1F,
	OvrAvatarSkinnedMeshRenderPBSComponent_UpdateSkinnedMeshRenderPBS_mD3B0539E844751EA3FC12A8168FA15A544A43F12,
	OvrAvatarSkinnedMeshRenderPBSComponent__ctor_m09F2C6B04806341383164F13D46E3A3C59BF533B,
	OvrAvatarSkinnedMeshPBSV2RenderComponent_Initialize_m48F349C1F7D008718FEF0CCB0BBB6E14F962B3A4,
	OvrAvatarSkinnedMeshPBSV2RenderComponent_UpdateSkinnedMeshRender_m7D1981139187AA4E292360970620B34730AB7F5B,
	OvrAvatarSkinnedMeshPBSV2RenderComponent_InitializeSingleComponentMaterial_mB54505D8670BE8979C5C9081E4F127D064980DC8,
	OvrAvatarSkinnedMeshPBSV2RenderComponent_InitializeCombinedMaterial_m26D97507031F1B647035660C464E816C31073670,
	OvrAvatarSkinnedMeshPBSV2RenderComponent_SetMaterialTransparent_mAC9A398B2903E420EE6D4D4F55625AFD4550CED6,
	OvrAvatarSkinnedMeshPBSV2RenderComponent_SetMaterialOpaque_mE542ABED3552C07BA6B7289653AD43855073BA5D,
	OvrAvatarSkinnedMeshPBSV2RenderComponent__ctor_m6BF007FC10A10C8C9A1E52C06CA90C3203B62BEF,
	OvrAvatarTextureCopyManager__ctor_m0CCDBDF969C1BAF740B7BC3A84D809007A135113,
	OvrAvatarTextureCopyManager_Update_m737B583D618DC4073C3D7EF2CA9134680BC47CAD,
	OvrAvatarTextureCopyManager_GetTextureCount_m7E8EFA97005056B17B3ADDBF04E715C20AE287DA,
	OvrAvatarTextureCopyManager_CopyTexture_m78E65D5F8F1742D6E3859F2FD1305E8D75327AEC,
	OvrAvatarTextureCopyManager_CopyTexture_m41E7EBA84335D0E9146D9938FA32DD831EBC1E2D,
	OvrAvatarTextureCopyManager_AddTextureIDToTextureSet_mD9D20776722FD413D977173033B4772489DAB951,
	OvrAvatarTextureCopyManager_DeleteTextureSet_mDAD44978C2CFDC23C82CB1E176532483D15D6C91,
	OvrAvatarTextureCopyManager_DeleteTextureSetCoroutine_m7207FDEA225FFD902D1896643088801AA3C7E128,
	OvrAvatarTextureCopyManager_CheckFallbackTextureSet_m012EBD8463DABD4557C70C495A3EC89478BA5C62,
	OvrAvatarTextureCopyManager_InitFallbackTextureSet_m0713E1FD188454AC61EDDE1EC12EE2BEDE5C3A0E,
	CopyTextureParams__ctor_m1B3ADEEC9C61F14F8CA8EBCFF0FED7020C338DC2,
	TextureSet__ctor_mD82F34E402F4351FF6634FAF2614B45506EC8B8C,
	U3CDeleteTextureSetCoroutineU3Ed__24__ctor_m79238538B0AB6415A5C3E0847361F2043C061051,
	U3CDeleteTextureSetCoroutineU3Ed__24_System_IDisposable_Dispose_mC19B8CDE02AA7463AF3A312C1BDA6DCF40660D7E,
	U3CDeleteTextureSetCoroutineU3Ed__24_MoveNext_m5D84322450A11F76FAE8AC3D25744EFF9B3198A6,
	U3CDeleteTextureSetCoroutineU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3BB8FBE6FBCB81BC013B5E77224876F974568FF,
	U3CDeleteTextureSetCoroutineU3Ed__24_System_Collections_IEnumerator_Reset_m6A4C0016B310142BB80ECBD048150ADCA25E6EDD,
	U3CDeleteTextureSetCoroutineU3Ed__24_System_Collections_IEnumerator_get_Current_m1941004ECF1ADA168283893C759EA71E996784C1,
	OvrAvatarTouchController_Update_mD0090C21D424B18F7CE3E579DE7B7B5C3AAD85D3,
	OvrAvatarTouchController__ctor_m9FF70E0776D27496432CC9F96D9A28300A30E3B9,
	DebugUIBuilder_Awake_m403F18586F94598A6A54C5B1899CF76032848602,
	DebugUIBuilder_Show_mF284EB0ED0943893BD5799EBFCCB1DB6B192DC2E,
	DebugUIBuilder_Hide_m23BD4DC6DB4E714215B97C9A37417D70356FD0C2,
	DebugUIBuilder_Relayout_m3937EE57F0E4C6F007BE6CB4CF0940FBEC300C28,
	DebugUIBuilder_AddRect_m877BEB5A300FE474E3A148ABB8BA0C0FBB224EA9,
	DebugUIBuilder_AddButton_m2FA1F9C35EEDC9AD3BDFEA645B88E8ABAC9F9274,
	DebugUIBuilder_AddLabel_m6A98E248939BD65B054A4B4C4420D950F4811CAD,
	DebugUIBuilder_AddSlider_mC4E1DB56081E390B06F2B7D02C67BA683D84686F,
	DebugUIBuilder_AddDivider_m858451240D2EAE0D367F8145E2AC917929B15DB2,
	DebugUIBuilder_AddToggle_m3A79A54CC94114F24B771CD465F40B001CBFCAFD,
	DebugUIBuilder_AddToggle_mAA47BCD38AB222645D00709AFB422F4D5CF02DB8,
	DebugUIBuilder_AddRadio_m783EACB19FD91F15BD9BE7EDABCDD6B05D639639,
	DebugUIBuilder_AddTextField_m726C38770CC36156FC5B71739F63536EA55540A1,
	DebugUIBuilder_ToggleLaserPointer_m19D2F9E314DF85DEE576A53C5E719685B14A2B55,
	DebugUIBuilder__ctor_m95BD850859B88DCA361D481FC4A0C998DB46496E,
	OnClick__ctor_mC5739B4F2FE456031D0F032D2B1335A8C7E07F76,
	OnClick_Invoke_mDC64AF5D49F6C27A1C15E0695D0B9B8A51958AC9,
	OnClick_BeginInvoke_m26A6065C75CFBA6B575C90229E2689F93697C002,
	OnClick_EndInvoke_m9DC1A8BFEE9B5B1036401A570221ECB4C32AD697,
	OnToggleValueChange__ctor_mDED41EA7D290F566BBDE14F48F7DAB95A65513A3,
	OnToggleValueChange_Invoke_m7C6B23451E232EEEB6C2F95BAD41C5E5716303CE,
	OnToggleValueChange_BeginInvoke_m33F94F819A1EB1D23BC216963765EDB1740702C8,
	OnToggleValueChange_EndInvoke_mA11DE32406ADE7239AD2833535BF1C49DB4E292B,
	OnSlider__ctor_m748CDFE6C061BCC4EAB8C70EDEF6413DBC5748C8,
	OnSlider_Invoke_mC63EB553A881C3BD597451B10831BAD4EC03F5C9,
	OnSlider_BeginInvoke_mFA997752B26047A4D133433B32C14383D0E1F7DB,
	OnSlider_EndInvoke_mCC34E0F20749EC0287A3FDC4FDC354A88202FEB2,
	ActiveUpdate__ctor_mC45D2B4C2665FD2DC12090EAB9C0B5044514DF19,
	ActiveUpdate_Invoke_m769E06EC3AB44D98C5C452D5B2336809F87C1E18,
	ActiveUpdate_BeginInvoke_m720D31DDBFECDC31563B554DE0450793E969F877,
	ActiveUpdate_EndInvoke_mFA8C1E60854662BE8B396F7BC93367E235ACB707,
	U3CU3Ec__DisplayClass36_0__ctor_m236E036B67727BA8579DE1C608CD3ED374C1211D,
	U3CU3Ec__DisplayClass36_0_U3CAddButtonU3Eb__0_m5FF7DBCC3B8181548A527A9F7EF193DB9D0D327E,
	U3CU3Ec__DisplayClass38_0__ctor_mF607CC87B637181BABC35D98A59112CD9D091E56,
	U3CU3Ec__DisplayClass38_0_U3CAddSliderU3Eb__0_m0CE3BBB5E665B1DA0C1A3BDA8E627A6C77CA20F3,
	U3CU3Ec__DisplayClass40_0__ctor_mB01C29C346234077CBCF5D492C168E8F58A0BB7A,
	U3CU3Ec__DisplayClass40_0_U3CAddToggleU3Eb__0_mE1F178F830E6B71B8F71612362E3121C1B9A45A2,
	U3CU3Ec__DisplayClass41_0__ctor_m6527E632BD200CEC2C12AA6E5775DD1A9B9385A2,
	U3CU3Ec__DisplayClass41_0_U3CAddToggleU3Eb__0_m5CADE8ED54BB65D80A8CB46587DA461303E926D0,
	U3CU3Ec__DisplayClass42_0__ctor_m17DDADD259C7C0F83EAC16A3DC2527C61619CAF0,
	U3CU3Ec__DisplayClass42_0_U3CAddRadioU3Eb__0_mC9894607A1275C248AA27CC402078C91704EBC0A,
	HandedInputSelector_Start_m9F0E0845F78B86C943B82C6C00D00C2755379664,
	HandedInputSelector_Update_m4AD93E3F8125E142011798D2CABF248A3DFB73C6,
	HandedInputSelector_SetActiveController_m675CE04EEBD60BA97F10205CFCC8E0360A57640D,
	HandedInputSelector__ctor_mFB9B45C64E23FAA0F3AC5FC734673E1A4EC7B6FD,
	LaserPointer_set_laserBeamBehavior_mAEDF3DABF1D4C13A02570DFB7539EA32D96C3EB7,
	LaserPointer_get_laserBeamBehavior_mC452C41CF65C2D1E3D13990D519BCE2B5459F4CE,
	LaserPointer_Awake_mB3F93300A58061D247AAD53BA961CA67148670CA,
	LaserPointer_Start_mC859C4A4E40BCB5E1A39D8B6135D256178A1B0C1,
	LaserPointer_SetCursorStartDest_m58BE76CF5DC6A138D6F39812B510E58A8195CA18,
	LaserPointer_SetCursorRay_m73C5954CB43A8D590BF1FF9674846886C869E3CA,
	LaserPointer_LateUpdate_m922E6CC571CF3BF292D1E4A89ABA3DE721940E40,
	LaserPointer_UpdateLaserBeam_m2EC759248E0200A9EDC06D995886AED2C3BBE84D,
	LaserPointer_OnDisable_m386D5CB4ED7CC015BDD625F9BB173D48A44963BC,
	LaserPointer_OnInputFocusLost_m03F3165CDC0823985D85DBDECE16679C4A574EC1,
	LaserPointer_OnInputFocusAcquired_m253C322D76D9D1A00FD1041F30DE52EC189857D8,
	LaserPointer_OnDestroy_mA7F7B1BDDB6897671BC633CF81F15F3C2AC8A27E,
	LaserPointer__ctor_m9834E61B31A7B83B51FC2BEB7B9F59FCE781B66A,
	CharacterCameraConstraint__ctor_mB4775E6A3FD15170E7EDA1FF7083E59DC9D9356B,
	CharacterCameraConstraint_Awake_m11B961F3351B1DCF59D2F9F25F6129261B996D8C,
	CharacterCameraConstraint_OnEnable_mA51E71B397BB043BDCDBD57881301ACEDBB7A937,
	CharacterCameraConstraint_OnDisable_m0C85B3913ABB8DD8EF6AB5A38003925365C97700,
	CharacterCameraConstraint_CameraUpdate_m7D846796F0337138A375E39F69E31C68B0EBA41B,
	CharacterCameraConstraint_CheckCameraOverlapped_m7EF5BBE41ADD0F7610CE1CC6EE39CA631513C783,
	CharacterCameraConstraint_CheckCameraNearClipping_m3BD4B953D24FBF24CAB9E86824E600EAFE549483,
	LocomotionController_Start_mFF771607088B6AC1B9647FFD5262BD156C32990B,
	LocomotionController__ctor_m5AA73F4FF00769A834D5732FC4BD08BDD4634BDF,
	LocomotionTeleport_EnableMovement_m8ADC24447284A9EE7AEECCED581CC2B51326FDC9,
	LocomotionTeleport_EnableRotation_m1B04E1D2444E02AC4407BEE5362EF08441C2AFA5,
	LocomotionTeleport_get_CurrentState_mCEDC22238BFC0EB60D9DE2616698C71D0CEF0BA8,
	LocomotionTeleport_set_CurrentState_m6D752383FDB712A2347A0CAC2F7734E26274DA77,
	LocomotionTeleport_add_UpdateTeleportDestination_m5CF34E190EF7950C68B07AA59440C8904B000EDA,
	LocomotionTeleport_remove_UpdateTeleportDestination_m6D4B9E576EAFC2F0F8812B24237AE65840368C8D,
	LocomotionTeleport_OnUpdateTeleportDestination_m14192CB82A0C6B54908D63B87428912B984459D1,
	LocomotionTeleport_get_DestinationRotation_m713A8A847E529FFF3636C695730856FF0F591CC6,
	LocomotionTeleport_get_LocomotionController_mED79293C6EA765335D25527FC7111EF971257498,
	LocomotionTeleport_set_LocomotionController_m460991F216978FB103CB533BF9DEF4BC93DA814B,
	LocomotionTeleport_AimCollisionTest_m230A548C1CE75228EFA5CA239FA7171FB0FB4D3E,
	LocomotionTeleport_LogState_m2272C15F12A05D8C031A50FB08AE122C1FCA2EA7,
	LocomotionTeleport_CreateNewTeleportDestination_m985E3B33C931F465655EF6156CC75DC3B7A1D99D,
	LocomotionTeleport_DeactivateDestination_m4F8704E73CA946449DF98D49763F10EF733987A0,
	LocomotionTeleport_RecycleTeleportDestination_m6DA8327336475283A325074C859CD4D281BA9879,
	LocomotionTeleport_EnableMotion_m9ACD9EF8F110F90CD996939A058242FF89BCEA22,
	LocomotionTeleport_Awake_m9C80807870EFB91801F476BAE526C31D9E29E66E,
	LocomotionTeleport_OnEnable_mFCFB80918BF2D1AF964CB043299BBE69CC83FDB5,
	LocomotionTeleport_OnDisable_mA887B447817410EA3AC80DF9C6B2390D0B26788D,
	LocomotionTeleport_add_EnterStateReady_m694F17237FD2AEEDCADA5B5BF9D32F91E249BD39,
	LocomotionTeleport_remove_EnterStateReady_m02A5BF8752C71DC2A54CFA7325F737BC3B6099C1,
	LocomotionTeleport_ReadyStateCoroutine_mEF056F1254CED1ABD142E517BDEB2815AAE24A0C,
	LocomotionTeleport_add_EnterStateAim_m9EEEAB1100A4CC7635EF023A0078BCE95434C5A2,
	LocomotionTeleport_remove_EnterStateAim_mBBF9AA5970D87E22D59FC766F8278643C61B6250,
	LocomotionTeleport_add_UpdateAimData_mF17892665B70341AB40666AA39187787B9E060E5,
	LocomotionTeleport_remove_UpdateAimData_mBFAF399B9B0200FDC43ADEF36B4123F60941D24C,
	LocomotionTeleport_OnUpdateAimData_m76ED9D517FF45B8A7CE7757BD0E8D147892F90A8,
	LocomotionTeleport_add_ExitStateAim_m93E127BBB502440C22348EDF011EF6ED12E1E607,
	LocomotionTeleport_remove_ExitStateAim_m86CEE4BAF96D0CDAB9CEF69231DDB1A3FCE7964F,
	LocomotionTeleport_AimStateCoroutine_mC98C40FA612A3DC181B2A622B2AFC2B78C72D182,
	LocomotionTeleport_add_EnterStateCancelAim_m98AE021C2A1274CE289ADBDC891DCC027AB06668,
	LocomotionTeleport_remove_EnterStateCancelAim_mF547CBA5F10D4A7374318065008202266E14E729,
	LocomotionTeleport_CancelAimStateCoroutine_m89E30D85AE2200D46C6B6CEC487CA86309336DFE,
	LocomotionTeleport_add_EnterStatePreTeleport_m5436C07729E835E670F3D438A7EAC8743646071F,
	LocomotionTeleport_remove_EnterStatePreTeleport_m1BB7C07D45DA26BA3FC294749B845A6F04E9035D,
	LocomotionTeleport_PreTeleportStateCoroutine_m4D4467A0B6570EEE09C5366CF0B443665718D27C,
	LocomotionTeleport_add_EnterStateCancelTeleport_mE447D28CF848635B7BF086DA4CC7592E174225F5,
	LocomotionTeleport_remove_EnterStateCancelTeleport_mD9C6A70C33006402CC14DF75C06AA68EE0CB38B7,
	LocomotionTeleport_CancelTeleportStateCoroutine_m376EE91D4337D6E0DFE214FAF8277E4AD3A80F09,
	LocomotionTeleport_add_EnterStateTeleporting_m2939235BCF9A6D936A57686136AAF38FA38F7AE9,
	LocomotionTeleport_remove_EnterStateTeleporting_mE99129EE268F102CFBF05222E234285FD41EA602,
	LocomotionTeleport_TeleportingStateCoroutine_m29325603C91C678B7035FDA9AA324D657149A1C7,
	LocomotionTeleport_add_EnterStatePostTeleport_m6352237DD5B10EB9C90693107DD8C33A6CE538FA,
	LocomotionTeleport_remove_EnterStatePostTeleport_m9E53B5CBF3804AC61F9E38AEA246E2599341373D,
	LocomotionTeleport_PostTeleportStateCoroutine_m55EFBB7452FA0FEF1F0C025CE559516ED977AA96,
	LocomotionTeleport_add_Teleported_m8CAC3A947E40F5C79B38D5F059E94B75A6CE623A,
	LocomotionTeleport_remove_Teleported_m27E84C43ACE915F7D5ADA07A974F7A76530BBA3E,
	LocomotionTeleport_DoTeleport_m1477EB38D2FD6E2AD4A7553EB3237B18D40C0B0C,
	LocomotionTeleport_GetCharacterPosition_m30F7EB551F5787D2489CE2716284B848D2C11D26,
	LocomotionTeleport_GetHeadRotationY_m0F2078ED6649DCED3A8022BA2CD27BF02378639B,
	LocomotionTeleport_DoWarp_m2195B0DF360E9B04AD486855F66560DFC22EEB76,
	LocomotionTeleport__ctor_m9AFDEE21E452CC311C904A90D753CB815495A59C,
	AimData__ctor_mDEB139E72E07987C9EC05AFFACE3A5C393EE56B8,
	AimData_get_Points_m3169977C66C0D412F32CECB2B7FD471F65A191B2,
	AimData_set_Points_mDECAE723C539EE8F8513307662F0CCF743ED7BF4,
	AimData_Reset_mA22ED9AA08B2642374D760A48D198B98DA3D49E7,
	U3CReadyStateCoroutineU3Ed__52__ctor_m8DE1982B50214C0C02CF0920B2BD0C8503DD7BC6,
	U3CReadyStateCoroutineU3Ed__52_System_IDisposable_Dispose_m281149F5ECF711FBFD9262A09DA4339B2DB2F7FC,
	U3CReadyStateCoroutineU3Ed__52_MoveNext_mA3BE219EA412B9FF5022A2D51A8C98962618203D,
	U3CReadyStateCoroutineU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF049A6B9DF16EBDE92993801D9D70B3DF6724BB3,
	U3CReadyStateCoroutineU3Ed__52_System_Collections_IEnumerator_Reset_m57879FEF2CA0007DFA14EE7C948F128236C49F20,
	U3CReadyStateCoroutineU3Ed__52_System_Collections_IEnumerator_get_Current_m8CFB46F37C1A1C464F6C813BD5B61E5E4D60D65B,
	U3CAimStateCoroutineU3Ed__64__ctor_m58306B48D136424B1A697461F28CCD0112C0C386,
	U3CAimStateCoroutineU3Ed__64_System_IDisposable_Dispose_m1F7871364B779E25C381A3A485C5E8360E58BA33,
	U3CAimStateCoroutineU3Ed__64_MoveNext_mF0CCCD83D0E4DDD4864A079C21C7423C75D51811,
	U3CAimStateCoroutineU3Ed__64_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCEA3E5325E391A08ECF74573A3F1DA3F202A609F,
	U3CAimStateCoroutineU3Ed__64_System_Collections_IEnumerator_Reset_m430B643E53905F6ECA3AFC5D72DF1F1C7431E345,
	U3CAimStateCoroutineU3Ed__64_System_Collections_IEnumerator_get_Current_m800D35A57B1A8C7F13AB42BA27EC5C8C763D7EA8,
	U3CCancelAimStateCoroutineU3Ed__68__ctor_m1DDF58501076B8077E3441AF06D8A6A7F13B6747,
	U3CCancelAimStateCoroutineU3Ed__68_System_IDisposable_Dispose_mD7599C67A3F973C9B291989150D3639890B5BD84,
	U3CCancelAimStateCoroutineU3Ed__68_MoveNext_m0387258B4B35F7D9F037F52A915D247B2EBDF011,
	U3CCancelAimStateCoroutineU3Ed__68_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m422572D5779C1216188EDB8521144B4BD48335B0,
	U3CCancelAimStateCoroutineU3Ed__68_System_Collections_IEnumerator_Reset_m537442ED372D14EA04A5967D6B7078DC83C32107,
	U3CCancelAimStateCoroutineU3Ed__68_System_Collections_IEnumerator_get_Current_mCA030531FABF8A358DCC082E6F56B591B13DC194,
	U3CPreTeleportStateCoroutineU3Ed__72__ctor_m9FCD57D81CBFA88D2D1B4A777B29A236034CAF8E,
	U3CPreTeleportStateCoroutineU3Ed__72_System_IDisposable_Dispose_mBD12FF7C2306AAEF739BCCFF39B2C1F2C42D6ECD,
	U3CPreTeleportStateCoroutineU3Ed__72_MoveNext_mAB1EF7B08ADA03C55A05F1ADC5EF0628EAB6858B,
	U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m842BD3A40FAB08CE9ED7384F00A4035A6A8A6006,
	U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_IEnumerator_Reset_m8AA3FEA6ABDAC9A68A96DD7A90B1190B504ED09A,
	U3CPreTeleportStateCoroutineU3Ed__72_System_Collections_IEnumerator_get_Current_m1FC972F30DE3D7BAB7166BB72A107D0FD9E1E49E,
	U3CCancelTeleportStateCoroutineU3Ed__76__ctor_m6BBBA231C275F8B93FEFD97A2279B18FED30AFFF,
	U3CCancelTeleportStateCoroutineU3Ed__76_System_IDisposable_Dispose_mBB05318F123DC133C386E503BD026985038DF644,
	U3CCancelTeleportStateCoroutineU3Ed__76_MoveNext_m02B6031D1BB5B1B0662A91F6E9B78231CDC0CBAE,
	U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B45046196D18E8F11B7FFCA3836CDD0E069BFB6,
	U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_IEnumerator_Reset_m2FE09112ABFFCA00C122EFD44AE58DA551BD9817,
	U3CCancelTeleportStateCoroutineU3Ed__76_System_Collections_IEnumerator_get_Current_m888B89F4C478800F9FACD7F1EC137820F5094C57,
	U3CTeleportingStateCoroutineU3Ed__80__ctor_m1A3D09A30548B419B3E2083990FF4DA65B66A1C2,
	U3CTeleportingStateCoroutineU3Ed__80_System_IDisposable_Dispose_m52588F27903FD706F72A84E217008CDCEA1CE19C,
	U3CTeleportingStateCoroutineU3Ed__80_MoveNext_m01613F55E14EE215BD297AA0E61DFD1B400B786E,
	U3CTeleportingStateCoroutineU3Ed__80_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m59D9DB94A89EE8EC99B646C00995380363E17BA6,
	U3CTeleportingStateCoroutineU3Ed__80_System_Collections_IEnumerator_Reset_m87DE12198C0424F46FA9337E312A56AC85B48F34,
	U3CTeleportingStateCoroutineU3Ed__80_System_Collections_IEnumerator_get_Current_mD05407DB2BA285B804EC4E4DF432340D1424DF0D,
	U3CPostTeleportStateCoroutineU3Ed__84__ctor_m73CB3DEE73F665F765BA80FDF21B564AE8F22CAB,
	U3CPostTeleportStateCoroutineU3Ed__84_System_IDisposable_Dispose_mDDC6BB7ECF31923D9B8AA237DFDC162642BE3F5E,
	U3CPostTeleportStateCoroutineU3Ed__84_MoveNext_m5428FACDB4F6A7C6B9C2966106C9CA97A70CAFEC,
	U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50185F8995916556B5531091EAC1D796207EE4D6,
	U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_IEnumerator_Reset_m8E16BCCA8A3B018A2CAC418CDAA14DB7C9B3D897,
	U3CPostTeleportStateCoroutineU3Ed__84_System_Collections_IEnumerator_get_Current_mA97CEA8F012879C52B5756B734B3AA464439F83F,
	SimpleCapsuleWithStickMovement_add_CameraUpdated_m62D68F4E8BD481D3679FDB0979AE6CAC8DAFAC46,
	SimpleCapsuleWithStickMovement_remove_CameraUpdated_mA39A1F5D5F0CA44D0B6EB9E9208FD39B7D0D3E06,
	SimpleCapsuleWithStickMovement_add_PreCharacterMove_m682E5AAFBCDF0BF832F5447D69CC73DB39C096D2,
	SimpleCapsuleWithStickMovement_remove_PreCharacterMove_m57CBA41BD4CD9C1EA3864D4AD464C1255EA80B22,
	SimpleCapsuleWithStickMovement_Awake_m0C068EE45D2BEDB94F8D1D289C04BF7480443D2E,
	SimpleCapsuleWithStickMovement_Start_m517C18566428FBB777185B97264539BE48D5A0F5,
	SimpleCapsuleWithStickMovement_FixedUpdate_mE88BF998394A1EB4FCEBC8A04875FA26D3D6C1BD,
	SimpleCapsuleWithStickMovement_RotatePlayerToHMD_m762E683133240C6E945140A357920BC6ACB7D116,
	SimpleCapsuleWithStickMovement_StickMovement_m4550B6DF9A0272F282866822CC8F0E8AE32E1893,
	SimpleCapsuleWithStickMovement_SnapTurn_m6BA0E49AA935C048AC9E8D59C90CBF50E087B396,
	SimpleCapsuleWithStickMovement__ctor_m96AC7462B0905E8D35D74FCF0C54E3F96DDB68B9,
	TeleportAimHandler_OnEnable_mACD1F9393ADE87B704B4A4EAA298FBA36EDAC843,
	TeleportAimHandler_OnDisable_m61175BD9492F8C39A0BC8573FCAB06D008DF3961,
	NULL,
	TeleportAimHandler__ctor_m1763BE837F8F8FA3B3C34EBA78BE0D593F50A5E7,
	TeleportAimHandlerLaser_GetPoints_m5DE7FA7926409D22B8F5E329EC2A4935B95EC3A0,
	TeleportAimHandlerLaser__ctor_m0D6E559AC138E39BBBF5E12AD223F13571A32D9C,
	TeleportAimHandlerParabolic_GetPoints_m2AF261807D5A7F6FE7E14F20B473D985A93DD1C3,
	TeleportAimHandlerParabolic__ctor_mAB05081CBCE394C762E8B8F4F6CA12D3E9846A32,
	TeleportAimVisualLaser__ctor_mC232B12213106D0E44DD02D7EE16080182C5FCAC,
	TeleportAimVisualLaser_EnterAimState_mFBE7E0BB8190B822638EB470B2559E9A52B29832,
	TeleportAimVisualLaser_ExitAimState_m7076D541D8CFF0F24A2A19CBFE8C0EED448773D9,
	TeleportAimVisualLaser_Awake_mFC14E2C04A8ACFACA52131512BAE0FCDB425754A,
	TeleportAimVisualLaser_AddEventHandlers_m5E7A188CF65F15DEEE335F3B40C261EF1D1E0D10,
	TeleportAimVisualLaser_RemoveEventHandlers_mAEC2273E026F953D7B71945590F0E7ED65539415,
	TeleportAimVisualLaser_UpdateAimData_m8060F21ACBE191832A7247F4D750DF9A5A7C9984,
	TeleportDestination_get_IsValidDestination_m4A83CF1ABD625233373782FABAC9AC210A5FD151,
	TeleportDestination_set_IsValidDestination_m2A37F62F1E80ECB2FDE63D42471514A26B0ED0BA,
	TeleportDestination__ctor_m01B9174443C128BB3639D39F0D2EBD7283DEB972,
	TeleportDestination_OnEnable_mFB65AD58BD57263B4D14E91B6FA583DA9607EB91,
	TeleportDestination_TryDisableEventHandlers_m450B9F64A3EAB6059B85B7573377A574606A9062,
	TeleportDestination_OnDisable_m0D75681D1CD83894438624CD3A7878C95904F63F,
	TeleportDestination_add_Deactivated_m63745B4674198FDFA8D3CB45587CA14132840F9A,
	TeleportDestination_remove_Deactivated_mE2D6A7DCD9734262F34A8935272156A4B029A83A,
	TeleportDestination_OnDeactivated_mA84FB07520E0C6EDA28F13A6349B994236BF8B46,
	TeleportDestination_Recycle_m49B5F4B6776D55D76519AFA221B2881A51FED91E,
	TeleportDestination_UpdateTeleportDestination_m3FF1158B3A2BB73D914DC806E094D20F93A674B3,
	TeleportInputHandler__ctor_m54EE60B8D0F7935760E81AB820D4570094939F71,
	TeleportInputHandler_AddEventHandlers_m4609349778D031303E2767296E2B7E48E5E20FD2,
	TeleportInputHandler_RemoveEventHandlers_m739F90CC264BE61AC9316C2B1645BFE8BCBA0555,
	TeleportInputHandler_TeleportReadyCoroutine_m579E5F381DEA656B2A90444ADFA6DB5CB05BCD50,
	TeleportInputHandler_TeleportAimCoroutine_m82502080BCE2BD26E5BEBD89D54C3E57A4780B07,
	NULL,
	NULL,
	TeleportInputHandler_U3C_ctorU3Eb__2_0_m919CD5070F20E3A9E28CB8A95C9C9374663C52A1,
	TeleportInputHandler_U3C_ctorU3Eb__2_1_m2CE609639EC53A6A19E043A2E053F30FAE77D486,
	U3CTeleportReadyCoroutineU3Ed__5__ctor_m7C396BD2404ABCBF1CF83320ACF13FA1B621B4FB,
	U3CTeleportReadyCoroutineU3Ed__5_System_IDisposable_Dispose_mA2DFF1E0C765EA7B2F4BA2E1EF7721049E8EA046,
	U3CTeleportReadyCoroutineU3Ed__5_MoveNext_m5A64679B873617863C4B38AA8008C8CC1E564029,
	U3CTeleportReadyCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41A381D2B7B87AD5810E05A3C55A66FD7D8CADA2,
	U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m12F830ADD072802D84BE4B3231F43A12FA4F8E73,
	U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mF8243C3A677F944C06A59D21D17CC3AF8DD1B594,
	U3CTeleportAimCoroutineU3Ed__6__ctor_mE2F9BDC7C3225737B5ADD2AD7AAF0E449CFADE47,
	U3CTeleportAimCoroutineU3Ed__6_System_IDisposable_Dispose_m443089F5C3CB24CF2E58C43B6D940BB6A97E7059,
	U3CTeleportAimCoroutineU3Ed__6_MoveNext_m3D532A7ED8718469618989D091672C1E1E833FA0,
	U3CTeleportAimCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02583125A115ADE4CC8896C0108B7A2ADD9FCAFE,
	U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mE894BDC51B100595A10489704983F8922185CCF8,
	U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m28CB29942297ECF9C327DDDAB3D53AF8A77ABF05,
	TeleportInputHandlerHMD_get_Pointer_mCBC08A1A1B5D076AF31755E7771F3000034939FF,
	TeleportInputHandlerHMD_set_Pointer_m5F8552C6C93A90A8BA5E3473532D687559EB2E9D,
	TeleportInputHandlerHMD_GetIntention_m474B95CF0690765C9B5C7DCC8381385E9BF5FC6A,
	TeleportInputHandlerHMD_GetAimData_mBE4EAEA4E4AD483994731D9FE82D0491E131AA37,
	TeleportInputHandlerHMD__ctor_m3E2641D8040D2F44A53E2A184ECDDCA1ED6153C9,
	TeleportInputHandlerTouch_Start_m14F2576DDA7F7AA52A05AD9456703D1FF68F2387,
	TeleportInputHandlerTouch_GetIntention_m23F1835AE8064C76A1C2F9631B343C721DB6A993,
	TeleportInputHandlerTouch_GetAimData_m4155B513561DCB66F1180407A5AB8B2B5936E91B,
	TeleportInputHandlerTouch__ctor_mE4E2BF438B9790F07ECA23A6D77748104C707E31,
	TeleportOrientationHandler__ctor_m21A7BA2A330541DFEB4ADF80A53BE1046E65C10A,
	TeleportOrientationHandler_UpdateAimData_m9B52AC667A8BF78D509B49403962756A1714DD9B,
	TeleportOrientationHandler_AddEventHandlers_m2507C63FDF190C3BC81D2479EA8D488F32C6D4C6,
	TeleportOrientationHandler_RemoveEventHandlers_mB46ADA4F854BD6919ED74D88F027AB423FF522BF,
	TeleportOrientationHandler_UpdateOrientationCoroutine_m0CFA18DF96F986F14A52E6CC7EEFC90FA0C73B7B,
	NULL,
	NULL,
	TeleportOrientationHandler_GetLandingOrientation_m4CC4D8950AFCDAA13EB2244AD987B3225FEFAABA,
	TeleportOrientationHandler_U3C_ctorU3Eb__3_0_m3CA25FA37637FD4D799437E6037E2C278EC4B9A0,
	U3CUpdateOrientationCoroutineU3Ed__7__ctor_m5DFE7ADCE7BF42B372F2097C1C019F301BBE3DFE,
	U3CUpdateOrientationCoroutineU3Ed__7_System_IDisposable_Dispose_m159520706F62855F2F55F4686A47A89D093AD2B0,
	U3CUpdateOrientationCoroutineU3Ed__7_MoveNext_m2D766D50632FD169A27EF1E4EE39B50AAEF4512C,
	U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87C4EB920BA4BF331493243117342D5E99905F1C,
	U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m8957A7F5F8D1D9C630D84F24A7C2B66642CA9B95,
	U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mDB4E059717A313A10584DB05F88E813A45D9A606,
	TeleportOrientationHandler360_InitializeTeleportDestination_m6372AC3D398F0ABB35CDE87532ED8ACEE579351A,
	TeleportOrientationHandler360_UpdateTeleportDestination_mF98B570F0875BCE3DEF88E3EA2BDCF415C759E71,
	TeleportOrientationHandler360__ctor_m6FDD3AEEEB9FB23CEC3C6D7B6A453FF38428B49A,
	TeleportOrientationHandlerHMD_InitializeTeleportDestination_m507B4108A8B145144CC59BFB969FB37F3EDB88E9,
	TeleportOrientationHandlerHMD_UpdateTeleportDestination_mA3F3F034F1C248EBE74640A15F13D287D1C2AE2E,
	TeleportOrientationHandlerHMD__ctor_mED7328A59A1C729AC845F7A8410DD4BD5C633DE2,
	TeleportOrientationHandlerThumbstick_InitializeTeleportDestination_m4ABFA62C2B8CB74DE7829D0005E6C98FE32EB8F2,
	TeleportOrientationHandlerThumbstick_UpdateTeleportDestination_mBE2E2B2A0BCED3855974AAC1AAFBEE6F6081587B,
	TeleportOrientationHandlerThumbstick__ctor_mEAA7AC3ABA1CE6E0253CF38FB2CA3E4C318F59BF,
	TeleportPoint_Start_m30ECA69CD4353B458F2295BAEBB450CC01F59805,
	TeleportPoint_GetDestTransform_m721C2E343BE9B54FABF84CA15E207FD2390F7DBD,
	TeleportPoint_Update_mAF9BC964BBC534053D53659EF53DA1A09B22F484,
	TeleportPoint_OnLookAt_m2564753261F0D09670931BBF308955CB21B8302A,
	TeleportPoint__ctor_mA783018C82A82D1A80BA40DF577BC9D3BC245A6A,
	TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714,
	TeleportSupport_set_LocomotionTeleport_m718C84A5C4E1D039C61850BA424EFA71B5FF9327,
	TeleportSupport_OnEnable_mF890133242B29B518FDCBD5FEAFAF29298CBA609,
	TeleportSupport_OnDisable_m9C71979791E9FCD13837605745FA3C6CFF0B1409,
	TeleportSupport_LogEventHandler_m65943E6F024E9B73CE5D5ED5099B2A0164B3E2C5,
	TeleportSupport_AddEventHandlers_mE4502DF40E56A3B5061FB931FC4F21678BAE3E84,
	TeleportSupport_RemoveEventHandlers_mE7C77E2C141F41B7799C511FC1B8DEB699B6E8E1,
	TeleportSupport__ctor_m05529A4609A1DFC91979CE4AE1790639B372E754,
	TeleportTargetHandler__ctor_m3AE469213D738BE0ABD908BB5AA27AB6FF60240E,
	TeleportTargetHandler_AddEventHandlers_mFFEC0649545A9B1614D7114872C7AECEED6AE715,
	TeleportTargetHandler_RemoveEventHandlers_m3DC103164A1F0089BBC66E83C9E754B7F9C79849,
	TeleportTargetHandler_TargetAimCoroutine_m1CEA9B7FA57455F77D9639E1B62C205C515E959D,
	TeleportTargetHandler_ResetAimData_mA32665779CE5C63B71357211C714615BF9CD4DA2,
	NULL,
	TeleportTargetHandler_ConsiderDestination_m1A96940FCF8FD62A59948C3080FF813ABA4F8331,
	TeleportTargetHandler_U3C_ctorU3Eb__3_0_m6384FA377850643BB1088262AAFC6FFA8748F8F9,
	U3CTargetAimCoroutineU3Ed__7__ctor_m23D9D0D8A352A65E0D99985A676E454304BF8FE4,
	U3CTargetAimCoroutineU3Ed__7_System_IDisposable_Dispose_m21B111D2C0E7C1EB3CE98929690C0DD190716414,
	U3CTargetAimCoroutineU3Ed__7_MoveNext_m67B9E5EA47ACD29BD93DB469A85D596C9D94F69A,
	U3CTargetAimCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73510D945678774D27D2FEE03C45CF482A2EBBE6,
	U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m2BC8EDEB99F6EEEA6DB6EE8F703C99E5CC6A7301,
	U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mF0915E668ADBDEC27BF20ED85385A44114046F62,
	TeleportTargetHandlerNavMesh_Awake_m12528F076C3DFA64B7F8C2738B086CCBA390746A,
	TeleportTargetHandlerNavMesh_ConsiderTeleport_m437418CEDAA72639344228B31EA8BDC8A52AD169,
	TeleportTargetHandlerNavMesh_ConsiderDestination_m0095B7CAB6515175C4652DAFB166FB7851298DD8,
	TeleportTargetHandlerNavMesh_OnDrawGizmos_mBBC3AFFADE1EA512DEF62DE6FB277B82EE99E1DC,
	TeleportTargetHandlerNavMesh__ctor_m5BB4F183D1C263E465000903520478FBB0D2E990,
	TeleportTargetHandlerNode_ConsiderTeleport_m144CD7243791916F4D785F4E01AAAD729C1F1150,
	TeleportTargetHandlerNode__ctor_m66245B4B90DFAE3D51056B3DC1B825CF128E4145,
	TeleportTargetHandlerPhysical_ConsiderTeleport_m3F6B5CBCEFE9B53FB42E5A853B728740B30A879F,
	TeleportTargetHandlerPhysical__ctor_mED10B7430B7A3A1104C1EDFF1A2F7B4B4B007B4D,
	TeleportTransition_AddEventHandlers_m7945828C3E62F3C4EF094AC2B12950618720397F,
	TeleportTransition_RemoveEventHandlers_mBD20E23B4E42E8DE6D30126DA98CF0B744864546,
	NULL,
	TeleportTransition__ctor_m1A4530AB5DEEA55E946DF9A491B5145EFE239ACF,
	TeleportTransitionBlink_LocomotionTeleportOnEnterStateTeleporting_mCB87A486BE2733C4BCD2B57AEA4FAA69FED1D74D,
	TeleportTransitionBlink_BlinkCoroutine_m502BAE8615CE4CC76BFD91E15746263D10CF2E77,
	TeleportTransitionBlink__ctor_m709CADAB68439933BA41B5C318E4E60616F09932,
	U3CBlinkCoroutineU3Ed__4__ctor_m73115DAF8E2D89A49A9F6BD9AB03485AA7551BCD,
	U3CBlinkCoroutineU3Ed__4_System_IDisposable_Dispose_m6A66CCE325E70F5794414025EA6A7B8EABFAE259,
	U3CBlinkCoroutineU3Ed__4_MoveNext_mBEC5B507954BA0961920B4141D150885CE4D309B,
	U3CBlinkCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB31D456C6490D00742481C0F18817914FC76018,
	U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m6E21661229B022076FA60AF6C49A1980E56609E1,
	U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_mAA053761AFFEF4D53F8AC25C2BDA6DC4729996DF,
	TeleportTransitionInstant_LocomotionTeleportOnEnterStateTeleporting_m09B573BE4E14DD8DD571233C7F3CCA384A775A4B,
	TeleportTransitionInstant__ctor_m2596160D526DF43F58D24D83F33B43AEB3774DA4,
	TeleportTransitionWarp_LocomotionTeleportOnEnterStateTeleporting_m2D397B890161CCF004AF8C0AC064F28EAD6EFB8C,
	TeleportTransitionWarp_DoWarp_m12AF9CC51FC2784F1FCBCD37A9D0CBBDE2EEF117,
	TeleportTransitionWarp__ctor_mA2A7FA91C271FEC14A97EF24341CC25EF5C5639B,
	U3CDoWarpU3Ed__3__ctor_mA023019BAD546DB8BFCFC8326BF22566DC855B45,
	U3CDoWarpU3Ed__3_System_IDisposable_Dispose_mE361978D56E79BC8335160F0DA180CB96DCE29F9,
	U3CDoWarpU3Ed__3_MoveNext_m84CA338E50BBE257229411F56CD6B4D7F46015B1,
	U3CDoWarpU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94EA3502B1F9442D23A5EBF53483F6C85D484208,
	U3CDoWarpU3Ed__3_System_Collections_IEnumerator_Reset_mE692557C58D90117915B599FA91A88FC7D238200,
	U3CDoWarpU3Ed__3_System_Collections_IEnumerator_get_Current_mD1CE2712B5B0919B97EE26421E8DDC32F11E5314,
	NativeVideoPlayer_get_VideoPlayerClass_mDF6B6206956E93EEBC7AF18915278A2D6421B53E,
	NativeVideoPlayer_get_Activity_m57A6F34EE8FDF96D02CC8911A3A412FCD1E84AA1,
	NativeVideoPlayer_get_IsAvailable_m9EE0A75867D070F6DB6F146827B53778B1B15ECE,
	NativeVideoPlayer_get_IsPlaying_mEC08CBAC641A59742B7463B69DA7D259794238F9,
	NativeVideoPlayer_get_CurrentPlaybackState_mC8D05EA20BFC1762858006863DA54611CD8F768F,
	NativeVideoPlayer_get_Duration_m1DD26EE88BA047E7D07A5122433E8D53E70C295D,
	NativeVideoPlayer_get_VideoStereoMode_mA9EE9AC1F8865143D950B8DA844262FA4CEEB2AB,
	NativeVideoPlayer_get_VideoWidth_m0E4C6D08D48905A44F419C979943A14ABEADBC8A,
	NativeVideoPlayer_get_VideoHeight_m8E9DC31F1B90C05AE00C1BAC77AF7B96B3A38E66,
	NativeVideoPlayer_get_PlaybackPosition_m8A7BC6FB1BE4B1E5CABE2EFFF156FEB84F029A25,
	NativeVideoPlayer_set_PlaybackPosition_m5E361F1B165A814D829FA2E5A772855E804D0381,
	NativeVideoPlayer_PlayVideo_m369B2893EDF0A9C4D6CC3ABD8C39B87A5ADF878E,
	NativeVideoPlayer_Stop_m88F76F02AB21CDE313F7333C365AD3CA2D835D48,
	NativeVideoPlayer_Play_m1C55384A56E016C7F76701D9339BCDFDB2384278,
	NativeVideoPlayer_Pause_m85CEFB984199ED08E3F13D1E324BD5CB60FF5EC0,
	NativeVideoPlayer_SetPlaybackSpeed_m917A50C5444FD8BED5324AE2DDF2C3ED0C09D8CA,
	NativeVideoPlayer_SetLooping_m6A045BEABE99B0776A2A35F3A814B40FC7DF4B0C,
	NativeVideoPlayer_SetListenerRotation_mF2264AF57895244DAFFA22A75780D48B2A8CE177,
	NativeVideoPlayer__cctor_m2C57854907E5DD300D863EA75D72296752FD6C94,
	ButtonDownListener_add_onButtonDown_m6A369389838232FEEA1344146C1FD176F2ECE3E0,
	ButtonDownListener_remove_onButtonDown_m88F58309C99390BCEB654878D6C45519ADFB5A02,
	ButtonDownListener_OnPointerDown_mDC9FE990B96FBF2C6D980F8DBCB327E86BD04C9B,
	ButtonDownListener__ctor_mD2522D29DCA57136E34554C01F2C1DB18B6B4DD7,
	MediaPlayerImage_get_buttonType_m738EF74C0EDD4616EFC1E68DC56AC701B53DD2A8,
	MediaPlayerImage_set_buttonType_m86058D07781C3EE3FCEE2546DE6506CA31BB1874,
	MediaPlayerImage_OnPopulateMesh_mBFADD4CECF3314066C0377254FCB875E8C8F078F,
	MediaPlayerImage__ctor_mB86C91BF2885DC329EDD99F24916F9AD717B42D3,
	MoviePlayerSample_get_IsPlaying_mB7FE8B620130A38FB30BC09F7079E12084113CDF,
	MoviePlayerSample_set_IsPlaying_mB98C234AC3AC146C0F74729906F71A7E9F849CE6,
	MoviePlayerSample_get_Duration_m4A055DBAEAC1819C7091ACEF4E2B8DBE5EF4BAFF,
	MoviePlayerSample_set_Duration_m1B29A541BFA2385B82B3A793817338A24081ECBB,
	MoviePlayerSample_get_PlaybackPosition_m7B1A095968AFDF410EE031761D5BC5239328E00E,
	MoviePlayerSample_set_PlaybackPosition_mD8FC604C1C51A0577071A069BD3780DA303D4260,
	MoviePlayerSample_Awake_m096BC4D6D5E6B709F4BB2441B58E489540CAB06A,
	MoviePlayerSample_IsLocalVideo_m3B89DA94A65891DF9D5F3D6B8F7045A4B10897E9,
	MoviePlayerSample_UpdateShapeAndStereo_mA373D088B7381DB5A34923E79695184EC0AA1F0C,
	MoviePlayerSample_Start_m41862FCCB8907A26C31EC7C5B374DEDD661F61A9,
	MoviePlayerSample_Play_m64FB2763D4DCAC19B89BEE384C53488B702F84AB,
	MoviePlayerSample_Play_m43C298C6CA45290022B7F30B8CB648003469E44B,
	MoviePlayerSample_Pause_m1A8CFD8E325D63FFB48510A74E79F4CD7A07892B,
	MoviePlayerSample_SeekTo_mA2F91C759B810802D66038A151701C6AAC13B363,
	MoviePlayerSample_Update_m844225048CC9A23F2DD1AA0544BB61DD7D3867E5,
	MoviePlayerSample_SetPlaybackSpeed_mC9B71A0C6AD6851BE290D41ABB21689BE86BFE82,
	MoviePlayerSample_Stop_m869069EB49DB2E8DDC06D6833BA00905D0EC45CD,
	MoviePlayerSample_OnApplicationPause_mD8379876774437E10F757DE1A6CEF26245746263,
	MoviePlayerSample__ctor_mB1DFDA2F1E1E621CEDDE6898330836B240F1B7B7,
	U3CStartU3Ed__33__ctor_mFBD4AF6C95F797D0753DD1E6789AB7EFB4F3A4B3,
	U3CStartU3Ed__33_System_IDisposable_Dispose_mFD89D5A20E48873E01335A257B97824BE1B9EB56,
	U3CStartU3Ed__33_MoveNext_mD5A4E90D0615EF2D769C53930662CE60239B99F9,
	U3CStartU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BCA0FBB841D0017B1050286ADA36684C73A329D,
	U3CStartU3Ed__33_System_Collections_IEnumerator_Reset_m8ECDE46C41B27547EFB6B7E24E378DD06DD9107A,
	U3CStartU3Ed__33_System_Collections_IEnumerator_get_Current_m8D673748B930D29175C9D928B493B015EB8158A9,
	U3CU3Ec__DisplayClass34_0__ctor_m744287E1C5682D76E3E690286A071F39718E25A9,
	U3CU3Ec__DisplayClass34_0_U3CPlayU3Eb__0_mA90EE1D1E1F6E42A47F0488801D4A7734840FD4C,
	MoviePlayerSampleControls_Start_m625008A6C19107D6BE93FF40434BAF580E2C1D80,
	MoviePlayerSampleControls_OnPlayPauseClicked_mDFCC988FB43880C3A320FFC326517C5EEA554099,
	MoviePlayerSampleControls_OnFastForwardClicked_mDBE37296203D54341C0DFAA545CD2C37A3C6D36C,
	MoviePlayerSampleControls_OnRewindClicked_m890FFF45A080BEC11F3B0B3A11D492A1F6692C6E,
	MoviePlayerSampleControls_OnSeekBarMoved_m53704CFB9FDB6324FEA81FC2E7EA57DA12AE04B6,
	MoviePlayerSampleControls_Seek_mEC78032CC65FE67AA6A153DB42A4588955341EE5,
	MoviePlayerSampleControls_Update_m747FB37CA5F8CFBD485486E4517F4748489E38B3,
	MoviePlayerSampleControls_SetVisible_mB5C9350C361FC20DE05207E6CAB3D4805D28248F,
	MoviePlayerSampleControls__ctor_mB9AEEE8EBB60CE81FD9C5BA1C8154171C669BB7A,
	VectorUtil_ToVector_m5B5C2B6F166F7D975A94BAEA079D2328252A8E96,
	AppDeeplinkUI_Start_m775C152A115FF43C1AB34D97D9DC20A34FA705EA,
	AppDeeplinkUI_Update_m8D48F1E3305A2469B0935D4E829931DCB0D76713,
	AppDeeplinkUI_LaunchUnrealDeeplinkSample_mA5F8A1774DB5724E43C917EBE88431C6FE0D9236,
	AppDeeplinkUI_LaunchSelf_m2CA3C1FCE4414860CC1101B7D29B483B9AE45090,
	AppDeeplinkUI_LaunchOtherApp_mD63AA72CBA23EA78C7B91B1E4B59D9B7860B0823,
	AppDeeplinkUI__ctor_mA31F77CBBA609E0128E496E7D23C0FE41A5F94A6,
	CustomDebugUI_Awake_m312D4A67E8EA4824658432AE459FA21877975F71,
	CustomDebugUI_Start_m28DD234ADCAAC557207C669CE37554CD7C7CC833,
	CustomDebugUI_Update_mBF9B69FFE07A90FEA92DF170D99F185C952B043E,
	CustomDebugUI_AddTextField_m6CF8AA8D2CE855EF4B86EEF85E16E0F5AFC9F6FD,
	CustomDebugUI_RemoveFromCanvas_mC32F1E9CA35DFF39075ADC86BF7112A207C9F961,
	CustomDebugUI__ctor_m27BB96E142EA068E3029D709C3065D65DC9D2CFC,
	DebugUISample_Start_mF7AB8D2997FF58D02F5EB0FFA83D5B59DB982C7B,
	DebugUISample_TogglePressed_m2A267F4169FCDC7C8E0DD77FD85253225B6F381C,
	DebugUISample_RadioPressed_m7E36B100115C60405CC14822838E3ECD66418624,
	DebugUISample_SliderPressed_m6A03CFB38E2BAF3DC6E4C63A7DD2B895F2BD5F37,
	DebugUISample_Update_m7FA91668B8F852E7FDE02E7F8F82F3643D729C72,
	DebugUISample_LogButtonPressed_m8198BFFA8627BFEE6BAE268C801B5EE1C4B658F4,
	DebugUISample__ctor_mBD27AFA42BD0FA8EF120FCC4CFE624D2CD6B205B,
	DebugUISample_U3CStartU3Eb__2_0_mC3BA080F2C315CCC00B545B43B4EC960D6CEA6D3,
	DebugUISample_U3CStartU3Eb__2_1_m499D6E39FF2F002CD620BD3A790C87CF58BFBAE3,
	DebugUISample_U3CStartU3Eb__2_2_m1D0C6B17BCC797AB931BDD9128F04612252A4517,
	DebugUISample_U3CStartU3Eb__2_3_m148206FDC1F12C8E839B21019CFDFBCB7C320CD4,
	AnalyticsUI__ctor_m21788905A6C8F76C59714536A99939E5B74426A3,
	SampleUI_Start_m982A3C805909F26B7AA71851484369D105F0A2A5,
	SampleUI_Update_m3509E9A6966DBFBC160CFF3D96B508D1F854113F,
	SampleUI_GetText_m26599FA6C9771765EC9AD94F73BAAD00AEE049BD,
	SampleUI__ctor_mD3D7D360CE2BFF47B7799D39FBC3A99E1D5F211D,
	StartCrashlytics__ctor_m9C967A85082223F55E9138CB80F4107DE4A2C9BC,
	HandsActiveChecker_Awake_m789AB07810059246D886069A33494EDE150458DD,
	HandsActiveChecker_Update_m6C574E921E2E058430A4D6EFD50FB45F27F5526D,
	HandsActiveChecker_GetCenterEye_mEAD4DB3FC55FE5F6AA80E74EFFA4BFF053534D0E,
	HandsActiveChecker__ctor_m8875DA4F87EC8FE57A7587203AA1F695AEA6CBA4,
	U3CGetCenterEyeU3Ed__6__ctor_m99DAFAB56EEBC093A2EFF1857B7E41EDA623C071,
	U3CGetCenterEyeU3Ed__6_System_IDisposable_Dispose_m87282416027E1D142DA109AC2CAE4B8A202B7F25,
	U3CGetCenterEyeU3Ed__6_MoveNext_m8F91FCC289F307F81C3B0A1A6D16809AEAFFFA8A,
	U3CGetCenterEyeU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52AA44D25E603CBD76F6492C277DFA1502C50301,
	U3CGetCenterEyeU3Ed__6_System_Collections_IEnumerator_Reset_m60966632955FF20F0859533FE10315801CF3EFCC,
	U3CGetCenterEyeU3Ed__6_System_Collections_IEnumerator_get_Current_m45CF38A1CA12804B458CFC1C748D8A3490DF1854,
	CharacterCapsule_Update_mD56302C0703C800E264601DFCECAD70323B6D8E4,
	CharacterCapsule__ctor_m3517E2CB4B11230AC5AF2F46D6FCBBBD3F2FBC0E,
	LocomotionSampleSupport_get_TeleportController_m8E3B912620DEA5687BAC288FE3CE5F8116706216,
	LocomotionSampleSupport_Start_mCF86670570E36A40E41DC7613F8AFD7FAA740A89,
	LocomotionSampleSupport_Update_m031DA7C38919902529A0267E72B70BB5B294C1BB,
	LocomotionSampleSupport_Log_m0389971AE83EE5B9F1F560DD9692E55CB2AA2791,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocomotionSampleSupport_UpdateToggle_mD1A6755435B3A901645BDFA66781E84BB141B17A,
	LocomotionSampleSupport_SetupNonCap_mADDA8F8E1129D705CF311C52F138ACC60C3826B5,
	LocomotionSampleSupport_SetupTeleportDefaults_m5ACC86875EBD2D512ED55DB8211FB894F462B737,
	LocomotionSampleSupport_AddInstance_m585F0527D6C6C2351CC04DF1AD6AFDFE9252B09F,
	LocomotionSampleSupport_SetupNodeTeleport_m553B0B6C9BA5C94F976B0EA0828523B6C7FA8F79,
	LocomotionSampleSupport_SetupTwoStickTeleport_m0BBFB6EA935A3715A1D1677BC957562431346217,
	LocomotionSampleSupport_SetupWalkOnly_m1EE59A1C4972A91669F90AD573D410C26DBB370E,
	LocomotionSampleSupport_SetupLeftStrafeRightTeleport_mAF0F734749B4AEEEF9E7BA5B6131BA447174A7C5,
	LocomotionSampleSupport__ctor_m613DB25830E2B519F9C75665ABC4F75F3FBB3A44,
	OVROverlayCanvas_Start_m02FACA531257AF78493D111425986BDE67AB2991,
	OVROverlayCanvas_OnDestroy_mB9B73D9E5A1C61426EF8989A4BA55E64B920CB29,
	OVROverlayCanvas_OnEnable_m9FF98CCC4F3C096B72D990F0CF8FE6453D6FE7A1,
	OVROverlayCanvas_OnDisable_m9E0AE938543EDE56DB3FC961CB82FD14D6715961,
	OVROverlayCanvas_ShouldRender_mAB1021F516AA15A84D0F9FA33C9DAE329A8C7157,
	OVROverlayCanvas_Update_m6E50C6A065641B95F393BAB2C20ED325FCBBC750,
	OVROverlayCanvas_get_overlayEnabled_m9B539BAF0CDCCE9E1E5EBD918BE874E76F004BDF,
	OVROverlayCanvas_set_overlayEnabled_mDA991B436B2AB9BA5CBA3F2566352635795AD5F3,
	OVROverlayCanvas__ctor_m8B8FA48BC2B16C384FEBB9DA6B887942548C6539,
	OVROverlayCanvas__cctor_mC6F4BF85FBE55F37ED781CB002A4FFDFC0704915,
	AugmentedObject_Start_m8E0AD00236ACFDEAD69389D7467109BEE6B11B58,
	AugmentedObject_Update_m3A9B0C9619B59930AC5508F8FE2758CB0BD02E55,
	AugmentedObject_Grab_m459B031A6D80E520AE75A6A43DBEE1A087DC1CB2,
	AugmentedObject_Release_mAB1BE9AD51540219158AF0EFC23E4472C9B7BD3D,
	AugmentedObject_ToggleShadowType_mE9B7FDE7086D783F599CFE4164FFE16548F0E620,
	AugmentedObject__ctor_m1BEA4EB1EEF2E15B9D43AF499898A2DC25F6056D,
	BrushController_Start_mC8607F5C5E0D3C39318951CE99D90FBE562B9BCB,
	BrushController_Update_mD50E4CA311FB894AAA68222ACA91EBE6C30593E1,
	BrushController_Grab_mD6E7E5B9DC7580A384370CAC4A26AFF0379EE6CB,
	BrushController_Release_m3D7E2EFE180929EF8235DFE5CCB7DEDBF7FD9507,
	BrushController_FadeCameraClearColor_mF279AC0C77EA6F6F1758D99361413188B619AB05,
	BrushController_FadeSphere_mFE0E2D3B7CAB66CCBD2A263BF9E3C5234682E455,
	BrushController__ctor_m9257CBA4722EF69E6444F82933C2A2123AAE0CC6,
	U3CFadeCameraClearColorU3Ed__8__ctor_m71E38D8AED062F90AA945D9CC0540F9CDC001EE7,
	U3CFadeCameraClearColorU3Ed__8_System_IDisposable_Dispose_mB45544A56417DF7024AF0BF2FAC2892BEAA4CB57,
	U3CFadeCameraClearColorU3Ed__8_MoveNext_mADA92C9B3E3117DC738F397AA647A824FCC4BCB1,
	U3CFadeCameraClearColorU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CCA66E9BC227F6064FBD2B1EBDB29027B712EC6,
	U3CFadeCameraClearColorU3Ed__8_System_Collections_IEnumerator_Reset_m669F3DAFD50F24E439427435286DCBE5D5AAC116,
	U3CFadeCameraClearColorU3Ed__8_System_Collections_IEnumerator_get_Current_mD305219E1C23CE52D59AED4BFEF4C16461DAAAE1,
	U3CFadeSphereU3Ed__9__ctor_mFF2CC1B385DBDFF11A4C2EA5F29E290C231DFBE7,
	U3CFadeSphereU3Ed__9_System_IDisposable_Dispose_mBA8495222AE0CBF7E4A4569A41D5292D973A052B,
	U3CFadeSphereU3Ed__9_MoveNext_m3941BF83B9BCB6851DA0EAC9DBF79BC9052E9825,
	U3CFadeSphereU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09FCC5A8134E70A5773ABDBD3771EC06F0EFB03F,
	U3CFadeSphereU3Ed__9_System_Collections_IEnumerator_Reset_m38A51A10EA31A82FEC63C26F45E5AD2A52F86AA6,
	U3CFadeSphereU3Ed__9_System_Collections_IEnumerator_get_Current_m86BCBEDDEC870379C62644F1C8BF0CDB327A86C9,
	EnableUnpremultipliedAlpha_Start_m4926EC7506FF573EADA7ECC9CBB7D7E4DECAA67F,
	EnableUnpremultipliedAlpha__ctor_m72150D5AFC1B04755F8960E4A7E2211BC3E6BF0A,
	Flashlight_LateUpdate_mC96F82CE200FBCB574AE8EC09BC4A6794C3D8192,
	Flashlight_ToggleFlashlight_m2C0E6D9CB2AFB7BC18058A37692B42554F53E0AD,
	Flashlight_EnableFlashlight_mB6ED8C5FDB101A26CD2329430939DC65D087907E,
	Flashlight__ctor_m3126420E3768B6D3CFD0472251F924BE38E81671,
	FlashlightController_Start_mA7C1D7727D1DE21DCFD4FD4A1FD3A432DFAD5A56,
	FlashlightController_LateUpdate_m6B632F81EDC2907F75EB22CBCE2BC4ECCAA121CD,
	FlashlightController_FindHands_mBC00BF0B7AF22181B8586036875B7C9DB3C531C4,
	FlashlightController_AlignWithHand_mCE1A47A2AFD6232C0EA1D8D220E33454CD980E55,
	FlashlightController_AlignWithController_m124649B6FC7BDB21BCE91087F5AF313E83EAAB37,
	FlashlightController_Grab_m1C763114DEBABE1752067BB044130A9BB5A75175,
	FlashlightController_Release_m0B6874BDB6CBABE601DB5C94FAD11F11C6F4FE67,
	FlashlightController_FadeLighting_mE6DADFDB6FE28475B62BB3D04E99568CCC8052D8,
	FlashlightController__ctor_mB8B30045D45FF7336C3D1ED3C2343C40EA1EEA10,
	U3CFadeLightingU3Ed__17__ctor_m3178D02F76D3965BBFFA4F56F2F4243AF9B3B33A,
	U3CFadeLightingU3Ed__17_System_IDisposable_Dispose_m636516D26E0FC21793138E4235EAAFD9B87723BF,
	U3CFadeLightingU3Ed__17_MoveNext_m114DFA09D1CD0CF21952702777E86C16871EADAA,
	U3CFadeLightingU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD9E7E8152BD8F758715E45BC1885F51A63E26315,
	U3CFadeLightingU3Ed__17_System_Collections_IEnumerator_Reset_m532D658BF737F23E5DE75444F9DED652DF6FFC27,
	U3CFadeLightingU3Ed__17_System_Collections_IEnumerator_get_Current_mCCF09AD2D019507FE4A94EE11AFAE44708D1A70E,
	GrabObject_Grab_mB218C144950CC665DEF2C0D2621EC3BAB58F4DDB,
	GrabObject_Release_m87C19D984E37BBC51F11AE79362BCC463D9DF541,
	GrabObject_CursorPos_m712B6B88A9A282219C22F5AC6D91563EF6CF129A,
	GrabObject__ctor_mE34AA1EC033A0A74D6E8F881A20312EAD2E37316,
	GrabbedObject__ctor_mD71655859A58277437D2D41BF2D2E32ED45A3142,
	GrabbedObject_Invoke_mC5D11C66F1CA103D44AC0120309B8329C8C5AC12,
	GrabbedObject_BeginInvoke_m673B6B520980FE487E8E3698AC7C5E34E3999F7D,
	GrabbedObject_EndInvoke_mE288DA466CEF66AE67D22C3A85FC4FF0340AC220,
	ReleasedObject__ctor_mA1EB137D310B511053A0A1EB8F02C1A879C50E37,
	ReleasedObject_Invoke_mAD25C4E4B23562D2DC5EA00C154F670B80B2EC1E,
	ReleasedObject_BeginInvoke_mCF974E830722A67E3C0E157F95722386F5135C72,
	ReleasedObject_EndInvoke_m022CDE0C465D8F488CB44DF0F4537DC8D8F97EBF,
	SetCursorPosition__ctor_mFF389CC65BAD558CBA69A47B907658D65C53A63E,
	SetCursorPosition_Invoke_m15B4D29A4EFC2AA3C15ECF436299830A14AEA2F8,
	SetCursorPosition_BeginInvoke_mEC0A62FFCF845C61EB8FF6587D574FFEE524AFE5,
	SetCursorPosition_EndInvoke_m621EE977B55491598884A9DFBFAC6D7B247250AE,
	HandMeshMask_Awake_m507910DB71C1F770805606FBE79EC77CAE9E0896,
	HandMeshMask_Update_m38AC51F8B4FAD5EB72AF1C53379AD9230B4D6185,
	HandMeshMask_CreateHandMesh_m520E9CAE4BB1236F888D480F46994CDBC82BF3A7,
	HandMeshMask_AddKnuckleMesh_m7FE07234E6CE21E5C1930864ADFE9F4E5A878EC9,
	HandMeshMask_AddPalmMesh_m86D1CFFDA37877F6DDA84DEB1EB000ADE491CB5F,
	HandMeshMask_AddVertex_mFC6D3205D1524A29258D1E7C25257D6A3EB0DE2A,
	HandMeshMask__ctor_m37E35E530849032B1EF4024717644B8FC3723361,
	HandMeshUI_Start_m80C99FE9B881C46B86AE58C885846979489F8E2F,
	HandMeshUI_Update_mAC47D9C487ADFA21CB64CB5DA2735E72EB8A3345,
	HandMeshUI_SetSliderValue_m91E3D3B63E9A6677816037BB8DE109815A111FD5,
	HandMeshUI_CheckForHands_m5C8F16DA9E3D2B1906E4681B453190E184D9048D,
	HandMeshUI__ctor_m85C3A4E86D772F5799B0271350C9564E31FDFC89,
	ObjectManipulator_Start_mEBCDF18C339DB050547E7FAC14A140A2DEFEB4C9,
	ObjectManipulator_Update_m72C78567C8C72FC86BB069AD411212072EA8F03D,
	ObjectManipulator_GrabHoverObject_mBC43E1E7E210F0D296012D424536B74C00805A5E,
	ObjectManipulator_ReleaseObject_m02231C55BA814EDC06B3F938B7C2E8DEAB914E28,
	ObjectManipulator_StartDemo_m0754E0B57F00F054718FB66F9C646CB3882B26F9,
	ObjectManipulator_FindHoverObject_m3E6E90C275DF5252101B82C80D2CDBB5C22C4A37,
	ObjectManipulator_ManipulateObject_m229CCFCC09F5B4C39EFDE9E1D83993FB4675511C,
	ObjectManipulator_ClampGrabOffset_m53081A466E925B6FBBE4AFF4850DF53E4531B7A7,
	ObjectManipulator_ClampScale_mAD641AE21DC8D808B1E66B18906A701471035267,
	ObjectManipulator_CheckForDominantHand_m5492FB8645B7165F98D80E4F219962EA4DA4CBCD,
	ObjectManipulator_AssignInstructions_mA360EDC3C9EC6D0BAAF1C6D16618803917B3CC02,
	ObjectManipulator__ctor_mEEE59FC77CA6A41814536C9DFCB24192D5F21DE8,
	U3CStartDemoU3Ed__23__ctor_m93B55F7FFD26DDF5A738871EBEB2107BF95DE4C7,
	U3CStartDemoU3Ed__23_System_IDisposable_Dispose_m474A042BE09520BAFF40F9DAF52345B48C09EBD4,
	U3CStartDemoU3Ed__23_MoveNext_mD24727A203FF95967F58B93DAC73148D97E43ECF,
	U3CStartDemoU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m19A26D41F7FB9A11FB058C438E02F555FB51BA20,
	U3CStartDemoU3Ed__23_System_Collections_IEnumerator_Reset_m4291011401E9FEB2BEB0922EAEC7AEF435F7D49B,
	U3CStartDemoU3Ed__23_System_Collections_IEnumerator_get_Current_m804A4428AFF3C3D87F16FAD3D707FE4585EB405A,
	OverlayPassthrough_Start_m97C9F6B31E6315147C9E6EB5E796CA724229751A,
	OverlayPassthrough_Update_m5553EE278872B84F1DEB50AE402E7B512D997CD7,
	OverlayPassthrough__ctor_mF913DC5B9EA0FCDC65A0E34CAF78F74F5057B499,
	PassthroughBrush_OnDisable_m838B38AE602884CCB0BE04DE661EA60F012E47FB,
	PassthroughBrush_LateUpdate_m03306C8C4FA821AA69875F16A21EF9F9908DE954,
	PassthroughBrush_StartLine_mEDDC47E33545C96489E037E8B136D4CA579624BE,
	PassthroughBrush_UpdateLine_m508789A468A107D18B3C06A34E1A0A48FCA59FF0,
	PassthroughBrush_ClearLines_m684A85937EAF8304462A02BB40F5167779554BF2,
	PassthroughBrush_UndoInkLine_m23CD97DDBA483D18F39256132BDD62F5F0B2A63A,
	PassthroughBrush__ctor_mDEF0A84B7E60A4F9FE0D7584CA5F2F0D92A89311,
	PassthroughController_Start_m086EE35AA2F6701D88686F0A874F2D3AB9684631,
	PassthroughController_Update_mCE0679C2D8F409F507F99818B9FFB4467554FFD1,
	PassthroughController__ctor_mB542E27BD18002805C29FA8B8AE4E70101CC2B12,
	PassthroughProjectionSurface_Start_m1C289BDEF11167F3B6B3CC13CA3D2B0FB243A94B,
	PassthroughProjectionSurface_Update_m32347B174938842813CB23FF93C7DB52E17320AC,
	PassthroughProjectionSurface__ctor_m62D8E3BAA312AF5CA874323872CD7E14EA0251C4,
	PassthroughStyler_Start_m4092C1316E5C3612B264AF1E185992BBBE17C3C6,
	PassthroughStyler_Update_mDDBAB8B2F74B759734BAB13249701F84AE886D8F,
	PassthroughStyler_Grab_mC1D22409F8C813A9516E1BC8C38762359F008BE4,
	PassthroughStyler_Release_m64EC76A02BD96B9818C1838EE93B509F0D59A28A,
	PassthroughStyler_FadeToCurrentStyle_m89B40CBE7FC06A936547F98A3A11CE491E62F75C,
	PassthroughStyler_FadeToDefaultPassthrough_m9CDB8FB4E0D030CB89347B19B531763D9E35EC6D,
	PassthroughStyler_OnBrightnessChanged_mAD761CCF67D62E8B6C31BA3E430E388310FD3C8A,
	PassthroughStyler_OnContrastChanged_mD62F12B93AD84016661953D7AC62A910E794D97C,
	PassthroughStyler_OnPosterizeChanged_m56B23239FA40FA946248F57D7142C1EBCC10E112,
	PassthroughStyler_OnAlphaChanged_m2E29232FB7882E0CDD74227B63A6B3409EA71568,
	PassthroughStyler_ShowFullMenu_m63597CE4A8078251E6BA27225B375A4C493D310B,
	PassthroughStyler_Cursor_m7BA7DDF0FAD50949DC8C251499BB8000BF2FD16D,
	PassthroughStyler_DoColorDrag_m11B7DA9E8F21113859605E888C3BEB871684E2A5,
	PassthroughStyler_GetColorFromWheel_mA52723A4C04478A9E36FD6BEADA82662744EBEBE,
	PassthroughStyler__ctor_m5F97489280B446110F4C2BAAD9327E817821B130,
	U3CFadeToCurrentStyleU3Ed__19__ctor_mAE485AE3924BF8387544A21DBBDC07DCC91AA9BD,
	U3CFadeToCurrentStyleU3Ed__19_System_IDisposable_Dispose_mB7C7E02379F912796844ECB81A7ABAB5101237E4,
	U3CFadeToCurrentStyleU3Ed__19_MoveNext_m695EBBCC6271D725364D7EDA234F63636A989360,
	U3CFadeToCurrentStyleU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m62E9360956FC142D7DBC1E9381A9FBDD782771BA,
	U3CFadeToCurrentStyleU3Ed__19_System_Collections_IEnumerator_Reset_mB5C2710DD59CC9A98A6208DB18E20E1779BE046F,
	U3CFadeToCurrentStyleU3Ed__19_System_Collections_IEnumerator_get_Current_mAA54D3F70B51CFA5841047A0C02E509EBCA0E1B2,
	U3CFadeToDefaultPassthroughU3Ed__20__ctor_mD5734AE35E978B6016AC4BAAF5BD2885854E7289,
	U3CFadeToDefaultPassthroughU3Ed__20_System_IDisposable_Dispose_m098A05EF49FFD658B51912059ADA457D2E5BCF4E,
	U3CFadeToDefaultPassthroughU3Ed__20_MoveNext_m2E27578A23C55A27E901446E92898D8B8C50843E,
	U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53DC68C1CCCDF30EC9CB9DB49C519C931805FABC,
	U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_IEnumerator_Reset_m67B84B18ED37EF94A7E28DC9D942BF28EE813ED8,
	U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_IEnumerator_get_Current_m271ADFE5A502CB0CE024D87718FCE1B1BD5220BC,
	PassthroughSurface_Start_m7E506149753FD8679C9CB718543978CA53CE1F74,
	PassthroughSurface__ctor_m6B56B0E135BDB59C84145F3D6DC79877B2C65090,
	SPPquad_Start_m092C0AF73D6B9CD60ED91D3DAF859FDAA4EF5896,
	SPPquad_Grab_m8B4D385FEA16E6FFBD1A49860DB3B6FB13295A17,
	SPPquad_Release_mE8CA2CE6FBABBFA6CFBA60A468981092C0914A63,
	SPPquad__ctor_mE270AE3BB58A6EBD5FB9C47F99428A242A79CC41,
	SceneSampler_Awake_m632DFE6F8657193318C2BD87E035227C40EB8507,
	SceneSampler_Update_m3D06DA05BAACAA52006AF75D138BCD5D4923207E,
	SceneSampler__ctor_m613B46CE973907D623D017AD04DAF9B991B61C75,
	SelectivePassthroughExperience_Update_m0DA48E563BEB1B36C67E476AF5745FCB13B88903,
	SelectivePassthroughExperience__ctor_m312145A2A5A8560CE04653F4F4799F843525FD62,
	StartMenu_Start_m38A6B8AD8F7E61759540CF792C58672CFD13ED46,
	StartMenu_LoadScene_mF7663ABFF602CD53F3A26C076E6A8FA200347F97,
	StartMenu__ctor_mF07C92F5B046EB38E3933ABD69D96918F29EB952,
	U3CU3Ec__DisplayClass3_0__ctor_mB4BEA21B02CB620F151FB8C1D57C0384883E9130,
	U3CU3Ec__DisplayClass3_0_U3CStartU3Eb__0_mFE41F96EFC3E7314714C2E5F6392280A788A23D2,
	ClientManager_Start_mC42606D276CB33FA9190F652B50D1AB2E605117A,
	ClientManager_SwitchFireExtinguisher_m9382E50BCFD1CE3D905DF3C14F5F22720F20A8A9,
	ClientManager_Update_m457E07414871AE4B7E376EF218D5116E9D24AE4E,
	ClientManager__ctor_mAC4A063EAA17352DF73913CB522DBF39A1D102BE,
	ClientManager__cctor_m783D8D02E2732C18649B3403B4D88D5D53294988,
	ClientManagerUI_Init_m6C53E13B2F02099F38BC949E8DA1956D2D801883,
	ClientManagerUI_BackToClientSceneFromTestingScene_m9A0AD57DDE8E135099847DC26A18DCAA316E8309,
	ClientManagerUI_OpenBlueTooth_m4A97600EC257F2F9884BB6D08E4A979FF76817C7,
	ClientManagerUI_OpenClientMenu_m14B2EDE4C2D24F20603BDC77E3D13FE5CF847484,
	ClientManagerUI_OpenFireType_mD13E0EC82B70E981ACCC96DCA5352F7C99D14CA7,
	ClientManagerUI_CloseFireType_mD553092AE12FA204756BC165A64F9165A7DFF758,
	ClientManagerUI_OpenFireExtinguisherType_m8066C08243415F8AC293F7A0540DA287DD3EF325,
	ClientManagerUI_CloseFireExtinguisherType_mAC1580F518620259C194AB1604EDFCC95548BD91,
	ClientManagerUI_OpenTutorial_m01CB6179A76D1D8D9694E7B4E887CE822DBE50D1,
	ClientManagerUI_CloseTutorial_m13599DC43B9C293807DCA313AEB5C640767F84D1,
	ClientManagerUI_OpenTesting_m3A2D646838908E5C8F0CA21D157B7B965727BDA1,
	ClientManagerUI_CloseTesting_m44966651F90226A1CC9ED04F1AFBD3EF43F45F9E,
	ClientManagerUI__ctor_m0F2083392B0A1FA6BE1F9D922C4C1D8850BB535A,
	U3CInitU3Ed__8__ctor_m41B4C9710FAE4F482DF33EC5FA05FABF988D2B0F,
	U3CInitU3Ed__8_System_IDisposable_Dispose_mB01922B66FFA86C9A8562B6F33DA653BC6CED2B6,
	U3CInitU3Ed__8_MoveNext_m03A6410A1F945E81B99CBA03AC6C0521FE766897,
	U3CInitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB2CF953FC40BC2D53DCEE0EF2E1DF56F084C2CC,
	U3CInitU3Ed__8_System_Collections_IEnumerator_Reset_m254BEF0ECFA3EBCD31C16644667216173E97DECE,
	U3CInitU3Ed__8_System_Collections_IEnumerator_get_Current_mA2B1581C289A8F80B83E0FE21203EDF1B3A635B1,
	Panel_BlueToothConnection_add_EHOpenClientMenu_mC24D329E99943235A495CC07AEEF901E76F98B88,
	Panel_BlueToothConnection_remove_EHOpenClientMenu_mF55F6586F1CFC94DB1B4F5081273DEA0D037C8AA,
	Panel_BlueToothConnection_OnEnable_m590C1B77FD677A79026E89CA0BB3AA0A748DEB63,
	Panel_BlueToothConnection_Init_m159BEFC5DC9E7514C77B8A3D6566BC7D24F68648,
	Panel_BlueToothConnection_Update_m85E43DDF1BE68C1A26F0A52C1148A73A8E7A31CA,
	Panel_BlueToothConnection_BlueToothConectionProgress_m1CEC15509A0D8493312DA3DCCDDF699BE0A8C0DB,
	Panel_BlueToothConnection__ctor_m71C905D020E5B93A8889BD09C5514AF9D8916BFB,
	U3CBlueToothConectionProgressU3Ed__11__ctor_m01BF3E098DA933CA379066D0423721A73DC0E3E8,
	U3CBlueToothConectionProgressU3Ed__11_System_IDisposable_Dispose_mD936704747FF3F0FB42DDFF456BFD3A01F1157FF,
	U3CBlueToothConectionProgressU3Ed__11_MoveNext_mBA888AB1F584DCEF3626224FE0601EBD69981BAC,
	U3CBlueToothConectionProgressU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m298ADFBA0EFEE51DCBC7EC12DCFB53ED45FC4584,
	U3CBlueToothConectionProgressU3Ed__11_System_Collections_IEnumerator_Reset_mFAEEE761A5A341D3BDC2B2B591CBC98E3CB57BB3,
	U3CBlueToothConectionProgressU3Ed__11_System_Collections_IEnumerator_get_Current_m182D5CCB96349DC360874DFDB3EF56A7EB2F58DE,
	Panel_ClientMenu_add_EHOpenFireType_m1F12490A13502E3B33CE46710DF47F045FEF137B,
	Panel_ClientMenu_remove_EHOpenFireType_m93D0788D4BAD47F36DE5509745F4FF436CBA08F0,
	Panel_ClientMenu_add_EHOpenFireExtinguisherType_mFD8A7BC7FC3BC9C85467388872878CA91FB15D6D,
	Panel_ClientMenu_remove_EHOpenFireExtinguisherType_mDE2FA761FAF765F0FF960E5CDD4FBE30330018B5,
	Panel_ClientMenu_add_EHOpenTutorial_m971A7ABECC662140214B88DFF3611BF5FDBD64D8,
	Panel_ClientMenu_remove_EHOpenTutorial_m62DAAD720BF64A88A3DF207BFE03CAEFA375C696,
	Panel_ClientMenu_add_EHOpenTesting_m002F7F6D3B9CC2E15E43F8206C4277632AA1B01C,
	Panel_ClientMenu_remove_EHOpenTesting_m4E6987D627D99CD1768D77770F70955546157081,
	Panel_ClientMenu_OnEnable_m778760271D7701D67863F5CFB616DA43EDE88601,
	Panel_ClientMenu_Init_m603E66406B70FC964330598B17937B6E656B29A0,
	Panel_ClientMenu_IntroductionMenu_m07093709B05A05ED082D86D7400209B8587DCD49,
	Panel_ClientMenu_MainMenu_m96BF5BBBF95D5AFF4BF1CA4B2D47A4DE789842D4,
	Panel_ClientMenu_FireType_m0679EC5126DF795BCE9209E7240D75B9D8204138,
	Panel_ClientMenu_FireExtinguisherType_m2C4CB776ABD2DA08D00B2BAE8299086FAC994351,
	Panel_ClientMenu_Tutorial_mB8B8E134DD25350747B57DE2405437CD8DBB940F,
	Panel_ClientMenu_EHTesting_m32D55FB90B8386DF0A11C99E6A44254EB023A8E6,
	Panel_ClientMenu__ctor_m73279859DDBD31F9D04716E5162ABF31C6B955DB,
	Panel_ClientMenu_U3COnEnableU3Eb__18_0_mB4E022B442BB744420D71FCBE4A13242395FAB3C,
	Panel_ClientMenu_U3COnEnableU3Eb__18_1_m2C9FDA45A3E576181439CF52059A1936A791547F,
	Panel_ClientMenu_U3COnEnableU3Eb__18_2_m2FBDCE613F6F3689D7D839549CAAD405B57AE7E1,
	Panel_ClientMenu_U3COnEnableU3Eb__18_3_mE7E8545C5AD1B5184135BD5023B6ED120930F85D,
	Panel_ClientMenu_U3COnEnableU3Eb__18_4_m04A18B1F32CDB7DCEBAB12C729A009221EAF5935,
	Panel_ClientMenu_U3COnEnableU3Eb__18_5_m6BD88BB52DA83623A0707E72FAC03E47A4E25401,
	Panel_FireExtinguisherTypeIntroduction_add_EHCloseFireExtinguisherType_m70250C7EA75648C9AD19CED9865F6BD05DBCD9E2,
	Panel_FireExtinguisherTypeIntroduction_remove_EHCloseFireExtinguisherType_mACBA17461D0B9A616F5954AB2337288C58703885,
	Panel_FireExtinguisherTypeIntroduction_OnEnable_m19FDB7FBC77A6FAFE16A78D25337F205E37912D2,
	Panel_FireExtinguisherTypeIntroduction_Init_mF660D630B576FA5E567CB533C8EC18D2659D51D2,
	Panel_FireExtinguisherTypeIntroduction_FireTypeChange_m198387A1168F8EFAEA975455D531FFF6DFE52FAE,
	Panel_FireExtinguisherTypeIntroduction_BackToMenu_m3216871D862A632EBDD1BF4BE9F53D8B6888C727,
	Panel_FireExtinguisherTypeIntroduction__ctor_m6E2532AAD8E47F84798C582DE0A4AA5F740BBFC1,
	Panel_FireExtinguisherTypeIntroduction_U3COnEnableU3Eb__13_0_m4E9A97BA43505B731F79688B2CE2A5FE50C537F9,
	FireExtinguisherType__ctor_mD1FE9920CE475DB069329C5A41B44149478AA0E7,
	Board__ctor_mCAF81E135B44EEBE92F9AA73F20075CFE14F887C,
	U3CU3Ec__DisplayClass13_0__ctor_m2243666E2E1AC6C1E64C85AF82F518A309784B80,
	U3CU3Ec__DisplayClass13_0_U3COnEnableU3Eb__1_mBB6AEF8FCEB5E9F87E1B5F9D92DB026C7C802F41,
	U3CBackToMenuU3Ed__17__ctor_m5EB2B50782BFBDDB3F59135355A8BFA1D63AB42B,
	U3CBackToMenuU3Ed__17_System_IDisposable_Dispose_mA3C89B679718D465E60E5854B721EB3C750329FD,
	U3CBackToMenuU3Ed__17_MoveNext_m424F51A3C15F4C1C888940EC36960A6EB3CDD352,
	U3CBackToMenuU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B1D697678AC7EB50CF9A5CDB9D343BDF75B0002,
	U3CBackToMenuU3Ed__17_System_Collections_IEnumerator_Reset_m2CF6EDD8F56899E97B535669FC523E3C450987B3,
	U3CBackToMenuU3Ed__17_System_Collections_IEnumerator_get_Current_mF51C96CD8F5704CFA827F29A8866F5249F35EAC8,
	Panel_FireTypeIntroduction_add_EHCloseFireType_mB3F6C66ED424FBF5BC76A541036AE96BA6708DC4,
	Panel_FireTypeIntroduction_remove_EHCloseFireType_m6086A58798CCBA3DD40C487BA58D89E7E6AFA325,
	Panel_FireTypeIntroduction_OnEnable_m712837B975684B938FDC19BE225D0678ACBB9BF7,
	Panel_FireTypeIntroduction_Init_m14419696CAE5E61A5B222C53838ADE9410D19ED2,
	Panel_FireTypeIntroduction_FireTypeChange_mD83F23C885F8BF5937B1D59DC41D1570F23E01C4,
	Panel_FireTypeIntroduction_BackToMenu_m0252E7C01B7F4C807E5D454B2043A929FD3E1EC2,
	Panel_FireTypeIntroduction__ctor_m9F02DA2EAD0012CD612826BD885B20D6053756A8,
	Panel_FireTypeIntroduction_U3COnEnableU3Eb__11_0_m889CCEEBE3DA47462F470F54215FBA5B894F853E,
	FireType__ctor_m5BC653B019BF5411AD9F45315AD28FAAD5AC0431,
	Board__ctor_m56730F9EC20145A28995C56D68B85A48F97F18CF,
	U3CU3Ec__DisplayClass11_0__ctor_m647E1ADC4584DAEC22E9AEA545B4B12B48355200,
	U3CU3Ec__DisplayClass11_0_U3COnEnableU3Eb__1_m477ED89C12CA61B5DD88CD455725B3DC725A9C40,
	U3CBackToMenuU3Ed__15__ctor_m139127FED6B85A1E2CB003B51642B4098183BE3D,
	U3CBackToMenuU3Ed__15_System_IDisposable_Dispose_m71E53129DA703A82C8871C459B6AE736FADDD831,
	U3CBackToMenuU3Ed__15_MoveNext_m28D7989BBCFD6524F111C75D4A1F00B3F208CAA3,
	U3CBackToMenuU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C1C0E7F1E742F622CB4E35C20D338626B932722,
	U3CBackToMenuU3Ed__15_System_Collections_IEnumerator_Reset_m60C93D4119AFFC64CEB5FEC3E20CC9379CBA5E4C,
	U3CBackToMenuU3Ed__15_System_Collections_IEnumerator_get_Current_m107D38B598E9D32DDF6F23BBCB8E200AFFFE1E19,
	Panel_IPConnection_add_EHOpenBlueToothPanel_m3ED6F64311871D89F10B9EF42429BF85D6760499,
	Panel_IPConnection_remove_EHOpenBlueToothPanel_m6BB351BA431529054767EB1281816F0E404B94E0,
	Panel_IPConnection_OnEnable_mE1F1EE0BD5B7FD857AEAFE8DB3DA8BA380FB5AAC,
	Panel_IPConnection_Init_mB21EA518D0B459C8E1167910724C3348EB49492F,
	Panel_IPConnection_ClickOK_m7A517CCEA0EB87B7C4ABF501BEB6F0487BD8508B,
	Panel_IPConnection_Connect_m6FFBCFB8F9FB0E28BB8969BEE095651044A6FE87,
	Panel_IPConnection_ClickNum_m0F0B841A7F27332F4B3EC2F8E3CD769CA58F8AD3,
	Panel_IPConnection_ClickDot_mC937D7AFD68AD24C8AC03BEE496888682A143A67,
	Panel_IPConnection_ClickBack_mB95E51D2297CA3271B6418350D2AB61D4D76658A,
	Panel_IPConnection__ctor_m99B7D2ED0535A4F80AC172F1F297FC7AF04CC4AB,
	Panel_IPConnection_U3COnEnableU3Eb__11_0_m3C5686BB46BA275284584E76DCBEDF8EF1451475,
	Panel_IPConnection_U3COnEnableU3Eb__11_1_m81F252BD267551CF8D7EE5241DAB2C12F8E7223F,
	Panel_IPConnection_U3COnEnableU3Eb__11_2_mDBE36F5D073C32BEFDED1028961E4D453DAB9AC0,
	U3CU3Ec__DisplayClass11_0__ctor_m862EBF53643A74C164649D0568AB88499EB41D22,
	U3CU3Ec__DisplayClass11_0_U3COnEnableU3Eb__3_m2EF425A819D06D3BE34DE0AD9E2C4821C9A95641,
	U3CConnectU3Ed__14__ctor_mE7DEB2DA2327BC1920528E104F0D0BA5C48573F5,
	U3CConnectU3Ed__14_System_IDisposable_Dispose_m0A30C01F2EEF3B71D9A39FF87D1CD7896FDDC864,
	U3CConnectU3Ed__14_MoveNext_m707D3F0466D6A62179BB27BC3C4DD2AAD5A53B63,
	U3CConnectU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC83A6B4013CD244022DD6AA926A5D413F22FF2CD,
	U3CConnectU3Ed__14_System_Collections_IEnumerator_Reset_m0C8BFAE71C8ADA2430D49E7B97F3D131FE99F6DB,
	U3CConnectU3Ed__14_System_Collections_IEnumerator_get_Current_m263BCD911AB490ABB52DF1F4A06EF7C5BFBEB5E7,
	Panel_Testing_add_EHCloseTesting_m2D5A085B194DB435F2CD3AE5DF271BE6BD21E796,
	Panel_Testing_remove_EHCloseTesting_mAB689057627A6B4532E65E42BAC749739137927B,
	Panel_Testing_OnEnable_m5AE6508B54EFE6E20A32570AC06A447818713209,
	Panel_Testing_Init_m4AB1F57E93BF112755A26D3E665D87CF6A70D642,
	Panel_Testing_OnClick_mF0DBA53075F52F805EB82AE13308104E2589F353,
	Panel_Testing_BackToMenu_m4E93FB0049B08CBBFA8F3A0D75319A8D62FA3C6A,
	Panel_Testing__ctor_mA2DB7C1012EFB7FBE8FE9B61339B66D5A5C01735,
	Panel_Testing_U3COnEnableU3Eb__6_0_mFF330C7C4BA29DE4FB0C10C06F658BC4D16A8729,
	ChooseScene__ctor_m8261B0B1064E4D1412AA754C64669AEC0855E8F5,
	U3CU3Ec__DisplayClass6_0__ctor_m9048AD7F7DE3C4F70B4809AE8F2268401CD70404,
	U3CU3Ec__DisplayClass6_0_U3COnEnableU3Eb__1_mC724D04C5473CA9EE224D1C2B0925EA84E625581,
	Panel_Tutorial_add_EHCloseTutorial_m885D08A61B99EB73A3FF57B152596CB8BE26E1E5,
	Panel_Tutorial_remove_EHCloseTutorial_m00F5190BA6D5819D570BBD2ABD18FC453D7B8565,
	Panel_Tutorial_OnEnable_m199629279E7E08D74AF33E10DA4EB0FD0333A79F,
	Panel_Tutorial_Init_m0BBF57DCE10DEB22A12E9A8978E786D70EE72FD0,
	Panel_Tutorial_ChangeState_m1E717D0BD6B6F5248B444ED05B5A8F1146400BEE,
	Panel_Tutorial_PlayVoice_mA2D92771119D88CCF5F749138B4F202495F67353,
	Panel_Tutorial_StateProgress_m10B81AF1913FD7BDB15D65334FD964958E74096E,
	Panel_Tutorial_Update_m32A4FAF3ED38EF69BCE809D60D1C6B8338AB76FF,
	Panel_Tutorial_BackToMenu_mA17165D63698E0F8FC4C44176BEA2082406740B5,
	Panel_Tutorial__ctor_m6636F4237C157E2A38FCB778DB778AABCDC7EE3C,
	Panel_Tutorial_U3COnEnableU3Eb__21_0_m92BC49055BA1DCB362B73000DA924C0DCB7DFC7A,
	ProgressState__ctor_mBAE105CDFC15FA7A0BDF2ECE7B81D6769C29AB64,
	U3CStateProgressU3Ed__25__ctor_mA55D19F99728EF1C1DC364BDAB2F2C31A735B198,
	U3CStateProgressU3Ed__25_System_IDisposable_Dispose_mB5E9634F972993FBC4733B03F2050AF58AD4177F,
	U3CStateProgressU3Ed__25_MoveNext_m4B096EAB8612BB5B50A50B197789F8F401E15555,
	U3CStateProgressU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43A691FF45B15C1411322E182C2ADAA28D4B186A,
	U3CStateProgressU3Ed__25_System_Collections_IEnumerator_Reset_mFE7E1FCB2BD5C8673D69A691B1E204FFA3E28778,
	U3CStateProgressU3Ed__25_System_Collections_IEnumerator_get_Current_mEDD9C00E13CBB77795F3063C422A4FB40EAC5011,
	U3CBackToMenuU3Ed__28__ctor_mCFD9249726D23513A5E1F3A8B11AEE464D91A587,
	U3CBackToMenuU3Ed__28_System_IDisposable_Dispose_m903836E31DFC665AFB87CB8428790301EE84C46B,
	U3CBackToMenuU3Ed__28_MoveNext_mE08361449256D33A91C795408E9C1D8973CE6B21,
	U3CBackToMenuU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920BD50906164146F37AC70F04EA7E69A0AE63E9,
	U3CBackToMenuU3Ed__28_System_Collections_IEnumerator_Reset_mC2F4113D63E94175EE29C9594D7DA81D7F2C3D24,
	U3CBackToMenuU3Ed__28_System_Collections_IEnumerator_get_Current_mA88ADCDC8A8824B6EBC8A91C80FE0233CA3E9F8D,
	Panel_Extinguishing_OnEnable_m8E47320B61134B757C0A19CCEBF0220924C7209D,
	Panel_Extinguishing_BackToMainScene_m91E877EDFDA971F2008E02F8DC09FAEB412D5756,
	Panel_Extinguishing_Update_m9A5D322E37D657F9D8A715DF137E8BE748301F8B,
	Panel_Extinguishing__ctor_m47D881C0C3A6A4256BFD6C69C822A4105DB7D9BD,
	Panel_Extinguishing_U3COnEnableU3Eb__17_0_mA022839CEC499F7ADD7B69BDC79101E2801D7A8E,
	U3CBackToMainSceneU3Ed__19__ctor_m91F54967A8EB4D4D06942E47E8704DC149F9E2AB,
	U3CBackToMainSceneU3Ed__19_System_IDisposable_Dispose_m087200B439C68408557830A216F518216C8F3919,
	U3CBackToMainSceneU3Ed__19_MoveNext_m7D00D08C05599A9066DD3E044129949081A09ACE,
	U3CBackToMainSceneU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2851117CF1AF75D37B4B679D90AD29294B674C5F,
	U3CBackToMainSceneU3Ed__19_System_Collections_IEnumerator_Reset_m4E818025912DA085DF1BEFF5B655F8FCC2371901,
	U3CBackToMainSceneU3Ed__19_System_Collections_IEnumerator_get_Current_mB52FA104A877B14A3D25F2B772B3F9FE1FEFB683,
	Scene_Manager_Start_mFC00DAC848A94FB84F305C1A6C259141E173E0E1,
	Scene_Manager_SwitchFireExtinguisher_m687233F02853ED82EA456057A53E171AF759CF16,
	Scene_Manager_Update_mD499C044AEF444A2B1DC5FB953859CCEB7521674,
	Scene_Manager__ctor_m1AF5F8B64587F0C59862025A9D14510A934C2015,
	Scene_Manager__cctor_m799737852231F58FB038939198E585B3BD13F33D,
	Panel_AllInOne_add_EHCloseAllInOne_m65626A3BB4CF4110B13AD9BBB334F32B219DC1CE,
	Panel_AllInOne_remove_EHCloseAllInOne_m5B22F1C0B00A5570A7E0EAF938E93B2A36721978,
	Panel_AllInOne_OnEnable_m98520E69B161718260E45FF3F2170F1D3229BDFB,
	Panel_AllInOne_OnDisable_m6BA22BE851DD56B5A11D10EC1B14206E3FD3C15A,
	Panel_AllInOne_Init_mA7E93524FC07F607E1AA76537FECFF40A744BAA4,
	Panel_AllInOne_Action_ProcessStringData_m7F8D84C8E7F161C4DC16372C71E7CF2FDD46BF88,
	Panel_AllInOne_Update_m25A1A47C37B24A2F057EDE53AB404743A2D8E289,
	Panel_AllInOne_BackToMenu_m7CA1B61CC39A504F5E417999690C0E78264E118D,
	Panel_AllInOne_Sync_m03C1D2BADAAF7AB5B1DC95E0EEEAFD51A2FBA312,
	Panel_AllInOne_QuitSync_m0182216EFDF5799F2A7CDD2B580319864F88C2C6,
	Panel_AllInOne_GetLocalIPAddress_m1F69E8243C29FB15AE5BA484F85AF87A1771C26D,
	Panel_AllInOne__ctor_mAFA60210C320A40F899129CE6E7EAA428DA88876,
	Panel_AllInOne__cctor_mD4751A913D4CDE552D8D4C53DE6723052D47EE4B,
	Panel_AllInOne_U3COnEnableU3Eb__13_0_mB0DE17C829A8F30591B8921BA811DC6747BB5ADE,
	Panel_AllInOne_U3COnEnableU3Eb__13_1_m5273AC4E0B79BDE9B5DDE8A9861FCFC10F04DF31,
	Panel_AllInOne_U3COnEnableU3Eb__13_2_m62CD6D18DBA99ED8C2825A06792AB7B6E85ACC88,
	Panel_DataRecord_add_EHCloseDataRecord_mCC095FF201C79024C6A9A708A1C44EACF59E7300,
	Panel_DataRecord_remove_EHCloseDataRecord_m2D4FA24071B451858F508706C42B362BAED456E0,
	Panel_DataRecord_OnEnable_m0EB29226BCCD094DC5A61A71EDAC201EB14D8B9B,
	Panel_DataRecord_BackToMenu_mB34DF035DFAD9DA4A82BE2EA8B3FF73D7A37757B,
	Panel_DataRecord_FireExtinguisherSearchOnclick_m6A7C2B7F4D1F766F1B130E10DC39073F17F73618,
	Panel_DataRecord_FFireExtinguisherList_Search_mF9C8651EF88A1E56FCC4496C805658BEB2E4CF31,
	Panel_DataRecord_MatchData_m1C114B94972458986BE3493565751067E2CD7BB7,
	Panel_DataRecord_FirstPage_m29EBB0B6675E973EB6B7EDA3FF64BD936F2B5D72,
	Panel_DataRecord_PreviousPage_mF3B2E215F6FDEFB9FD349BCBC692C570A9DCFB86,
	Panel_DataRecord_LastPage_mF1E01CE4B09086C99B4B0053662D24AA55D67B31,
	Panel_DataRecord_NextPage_mB1C464EA751F3E732AD155C35DAD3F0C95C015A6,
	Panel_DataRecord_ClickTargetPages_m62FCA4EECFDDB7E17F5658788ED7F7488AFD51C2,
	Panel_DataRecord_UpdatePages_m18599772233E25EB66A582A5F6A9018FBBCDD822,
	Panel_DataRecord__ctor_m3F15910DDA46CD577C38697D9BD04946833D0EC5,
	Panel_DataRecord_U3COnEnableU3Eb__16_0_m1883757F8FF433363F935BCAE5590F33623D4D25,
	Panel_DataRecord_U3COnEnableU3Eb__16_1_m8B301FE4DFB5A9CE47BE3F35A6A0F07D8DA180C1,
	Panel_DataRecord_U3COnEnableU3Eb__16_2_mDBA317E7E07BBDFB7D10F53EF1889A428DFBF732,
	U3CU3Ec__DisplayClass16_0__ctor_m139BC4BF7153CE6AFA2AC7333A74FAE293CE5085,
	U3CU3Ec__DisplayClass16_0_U3COnEnableU3Eb__3_mD49AAA39C69F4306EB14BC49B6C5FC161D026EB1,
	U3CFFireExtinguisherList_SearchU3Ed__19__ctor_mBCA4A19B22A5B11DCCF7A7BBFFD12B0A7562D534,
	U3CFFireExtinguisherList_SearchU3Ed__19_System_IDisposable_Dispose_mB051B9257BE2AF4D4FC150AF518E010CBC5237FA,
	U3CFFireExtinguisherList_SearchU3Ed__19_MoveNext_m34B0882E56905E9A4E5B61184CA11F63536569C6,
	U3CFFireExtinguisherList_SearchU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67ABDD9443F6AC3AED551BE4DBEA19FCA75CF4C4,
	U3CFFireExtinguisherList_SearchU3Ed__19_System_Collections_IEnumerator_Reset_mC634A246C61D7726D687D683459D5901493C2745,
	U3CFFireExtinguisherList_SearchU3Ed__19_System_Collections_IEnumerator_get_Current_m162E99DD6AC81019B428119B3EFA7EC7D31BA58B,
	Panel_Login_add_EHOpenMenuPanel_mA55C094873CC99345C4F40C8F7EDB9541AF77731,
	Panel_Login_remove_EHOpenMenuPanel_mF22F74E438CA98B34685FF2A445BA625657E8ED4,
	Panel_Login_OnEnable_mB6942C8447CD4A7D17E325B7A11F975BC9930438,
	Panel_Login_LoginByAccount_m7DE03DBC4C99459C3677E409EDA3C2D4FAE0CA98,
	Panel_Login_FLogin_m7E10800D23B853E3FF8941EF7D2D45A506497B13,
	Panel_Login_LoginByVisitor_m7DD64051F4AA7B6E76FC80BAFB096E86E654C956,
	Panel_Login_TgVisiableValueChanged_mCE428C5C500E9E0C0A466C3AA52E8E125A43440C,
	Panel_Login__ctor_m410877F3F0DCCBB46E49E88A282EDCC839A942BF,
	Panel_Login_U3COnEnableU3Eb__11_0_m449882B2262B8F72F74FC1A327AADA39FD712C78,
	Panel_Login_U3COnEnableU3Eb__11_1_m0A5E76704B164BFCD49DACAE3EF61D5EAFD03CA4,
	Panel_Login_U3COnEnableU3Eb__11_2_m5846FDF9FE97714F6154CAE91C4A2CE6D7DEF7CC,
	U3CU3Ec__cctor_m298E7007DFF3A763829014FD6FD236B01B28F4F2,
	U3CU3Ec__ctor_mD003A3E6913A88E6C9322515B1B59BD79BD16BC5,
	U3CU3Ec_U3CLoginByAccountU3Eb__12_0_mE26C298150EAA90B81500564C597AB866EB72BCB,
	U3CFLoginU3Ed__13__ctor_mC079873B18D6FEEFD59DBD8EC972CC4CC64C4E11,
	U3CFLoginU3Ed__13_System_IDisposable_Dispose_m1E3549457085DC8E8C2372032C0D8DDB3275C43D,
	U3CFLoginU3Ed__13_MoveNext_m15003411CFB4951967A274DC35EBFEFA7C327D58,
	U3CFLoginU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DCA039700C52215A58CB57A4686196D533F1DDE,
	U3CFLoginU3Ed__13_System_Collections_IEnumerator_Reset_mC697FD5C29652BB1F47A13AC9520CBF51A5D5DBA,
	U3CFLoginU3Ed__13_System_Collections_IEnumerator_get_Current_m2E7950E9FC70F044040FA77916E340F848F7B10B,
	Panel_Menu_add_EHOpenAllInOne_m37C17885A25E4A6D83FFFF330F3E37F6BECCB8E2,
	Panel_Menu_remove_EHOpenAllInOne_m5B1F6549F8BB499F8240B1898AE6B4A985075731,
	Panel_Menu_add_EHOpenDataRecord_m193BE0C8E56955ABE74A747F3579592A2416965A,
	Panel_Menu_remove_EHOpenDataRecord_m48067132DBD959AFB0944EB6BC19489EC9E050EE,
	Panel_Menu_add_EHLogout_mAA883EE7947DB7A0540919530C59FF5846FEDC93,
	Panel_Menu_remove_EHLogout_mE8801C4FE6750C6F747CF72EC0164D2FF66D7119,
	Panel_Menu_OnEnable_m4614286D00F7F2EAF49D7A3CB5B1ED2558BD5DC6,
	Panel_Menu_AllInOneMode_m13398C5A706C218E5BE2870624799E09C3C92FF0,
	Panel_Menu_DataRecordMode_m6F6D1206D1D7B645C547990AAEB4C1E506165CD9,
	Panel_Menu_Logout_m42D572E495ABBA21FD32DFDCF7D464EE67ADE698,
	Panel_Menu__ctor_m6931C94624415A9739EF8576FEF93EB5AB8F2CC9,
	Panel_Menu_U3COnEnableU3Eb__12_0_m8CBDF70120EEB65BD89746A115275E3AF3A86AEF,
	Panel_Menu_U3COnEnableU3Eb__12_1_m275C6C788C72B9CE1CF75D720D66581F68D7214B,
	Panel_Menu_U3COnEnableU3Eb__12_2_mAF2A27194E2557D0D8FAF139D2DD0FD62F13B37D,
	ServerManager_Start_mF7EF750999CD42D9B6EF0BCC056CAC5C4D49FB88,
	ServerManager_GetLocalIPAddress_m8D97795E01D54DCB6F5234D09648A77CE155F6F4,
	ServerManager__ctor_m023716C9C9B41DDA0833CEB210D5F70CD919DFD0,
	ServerManagerUI_Init_mDCFE72456B0F6DB27B7FFEBC7F200422F25EE23F,
	ServerManagerUI_EventOpenMenu_m862BD21B1C41A02A3EC56B6C849F2CD6FD5A192C,
	ServerManagerUI_EventOpenAllInOne_m4B5B93774BAB0D0F99E375B317B5297C6423B02F,
	ServerManagerUI_EventOpenDataRecord_m3C42AE3C3F077C2D88079C129DEB95601D606A1A,
	ServerManagerUI_EventLogout_mE50C0004EB72022F33618CA19F5192B54105BEA3,
	ServerManagerUI_EventBackToMenu_m00165244790CC8711D3DCDD50583E651FA4C2B1F,
	ServerManagerUI__ctor_m76B67313410C36300DA69229040F19E9C5FF1DFD,
	U3CInitU3Ed__4__ctor_m96FAED00B123A4C00B5EF446FCDC08A68379DF49,
	U3CInitU3Ed__4_System_IDisposable_Dispose_mC8CFAEBABC78B67AD5FB8DA056A5D20F6D807142,
	U3CInitU3Ed__4_MoveNext_mE9566C1EB60855ABD5C0B78367C5D1A52E72A38B,
	U3CInitU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED67422E32CABAC4657637A811C9DD6C6689B4D0,
	U3CInitU3Ed__4_System_Collections_IEnumerator_Reset_mAD7BB77764B97952953510C1E0453EE78593E9D4,
	U3CInitU3Ed__4_System_Collections_IEnumerator_get_Current_mEA9B9FE6F9C30ECB95B87241FEA66918AD8BACE8,
	AutoRotate_Update_mB64C3E4C500CA7ACF369E2C1519DA8D65D8A12AB,
	AutoRotate__ctor_m734E0E82503C1250A1C45AE292672C28607D856B,
	FollowObjectPosition_Update_m7A2163E64DB2AD26BC3E23625D9F24EB85D387A2,
	FollowObjectPosition__ctor_mDB60D885D341F0A006F8C900C9C5AAB9D982F271,
	ParticleController_Start_m7DB7ECDC47B6BE9A938A0D900D2F9A8CA95EC13D,
	ParticleController_OnEnable_m850F8E3BF25F6D582589561FAB0BD33A389E04E4,
	ParticleController_Update_m2C837B5D6C99AA727449C4458B373AB5D18E216F,
	ParticleController_Play_m9704E5ADC97C236761DD608674EDA1CFB8509B43,
	ParticleController_Stop_m7C4FD910BCEB4620A4271F33C19B8EDAD77FD9B0,
	ParticleController__ctor_mFE7357596D5D39DD236F3BFCBFF71AFAC99C64AB,
	RayCasting_Start_m6A5ED478C3E7F039CDA628E841C6FA686F503167,
	RayCasting_Update_m76D59315B4FE0288779D7A2B8B26C978A99D900D,
	RayCasting__ctor_mE28827612C030EEE4023A42F70549D3DBF065B7C,
	UGUI_Hover_OnEnable_mEB168FB1D752666CCEAEE84CDB8C238698D6B1A7,
	UGUI_Hover_OnPointerEnter_m79805FD893214753331E0B1BFB147FD3DEF11663,
	UGUI_Hover_OnPointerExit_mC42813F3611FB896CC6F81E298331C91992D394B,
	UGUI_Hover__ctor_mADB45CE18CE14848C928960151B948AD5B37EE47,
	UserIDCheck_Action_ProcessStringData_m58224527A6C5E2B39B5B543DCD5CD82F8349AFD5,
	UserIDCheck__ctor_m26FE5C64E8FC139E3CF6DC162DB567F498C56668,
	UserIDCheck__cctor_mF2B76C2FD991CB46030C16B5FCE0AE4E47FB60A2,
	HandsVisualization_Update_mD8418A158C986348A57598128279C52D796BEFA1,
	HandsVisualization_ChangeState_m11849B5F36C8FCF74B1EBA9E7322097FA75BCD83,
	HandsVisualization__ctor_m7DA8877C57583BE4C14A0B3637519BE71018A1CE,
	LaserRay_Start_mDB9F96A87B00625A79A0B339009550A51C44FB08,
	LaserRay_Update_m738D5DC746623A29ACA1D457A6F1A92013502B6F,
	LaserRay_RayHit_m43A03C4D7F3CDDC088ECEF07EDC0DC1C2184FD50,
	LaserRay__ctor_m4197B860737EBC446089CF358FF58CC8597B641E,
	ControlState__cctor_mBDB237564AD51DED7F4D37EA9D696E057AB9CABE,
	VRIO_Haptics_mDEDCBBE17224C75F57744B84333F4FFA6933907D,
	VRIO_Throw_m85BEEC3343521C3CE9072B2B686B3D38EF9099A3,
	U3CHapticsU3Ed__0__ctor_mCD6188E048F28ED1D22E38A31DB31CD4B81E8C16,
	U3CHapticsU3Ed__0_MoveNext_m9373353F1A7C1785E118156DCE0D27A8C52E0E52,
	U3CHapticsU3Ed__0_SetStateMachine_m80A4CFDD96530258F3A3865E6A5E3B39614DCB25,
	VRPlatformSetting_Awake_m6FF9D5546D60BE40314F1E0AD5EE966759961B72,
	VRPlatformSetting__ctor_mC91EA019BBD199BF9AE9BF19B120E34FDACFFAD4,
	Console_get_SizeOffset_mAE766FF6C2A1E64A441FCF18ECE95A1CFBB7AAF4,
	Console_set_SizeOffset_mF0EE6F76811BA9C00E05C04974999BAFF9AB4DB5,
	Console_get_ShowConsole_mD4411F1872A10C1BF9AFFB752FD60913EF3E47B8,
	Console_set_ShowConsole_m254D008826892EE2D8424DEFC2FFDB73B1356D76,
	Console_OnEnable_m1D5D0ECDE0C01A0794DED111B7A590EB21503982,
	Console_OnDisable_mAABB8E1D554880D111759181281854138BD7FB1D,
	Console_Update_m06834FCB3E2B451247AF9ECB932AFAC75CA188B3,
	Console_OnGUI_mE245A80CEC1003471364B9B0A1BC9174E2466183,
	Console_ConsoleWindow_m77D6AACFEE7F91006B06D7C251C6253617E1D79B,
	Console_HandleLog_mFA2325313E8A2A5641A334CD98FFBB17DE3A1DE2,
	Console_ExportMessages_m2EA7177DEC60DCBEED44D3530EE5E46F9BEA5709,
	Console__ctor_m9F86FDBEB005BCE8604A3F5135BD3DCECF1074AF,
	Console__cctor_mB3B14CE8294B4B55C36E3C511660704222DDA3FE,
	API_Login_API_mB88E9E47FD413402B94CE8BE21389466FB2A4B5F,
	API_FireExtinguisher_API_mD22898A0EAC074FC1546B479D06B2278B2C401D8,
	API_FireExtinguisherList_API_mFA38695002692D9E8747788F682970C4C56BAB7D,
	API_Firemen_API_m762BD6FE4ABE7B7C8C4A5B8373238C707AF72303,
	API_FiremenList_API_mC39E64B49DBDD88EE45FBA624B4EE1FCE5086C4E,
	API_FiremenDetailList_API_m5CE8000DBC8E6D079E6E67B5E438201C3FF9E460,
	API_SelectVideo_m184ECF99E47F8594D6626BAAB24BCF495F4C7090,
	API_CheckVideo_m233867B761183D99AA04442B1751FCC48E65EF94,
	API_UploadVideo_m2207B4F85113E40A7256BA9063FEEA4FFD29EE1D,
	API__cctor_m5C950070AE71A037928544CE4FA2D65FFF7DF380,
	Login__ctor_m7F1F98F28179603D8698EEF6572D0B723F663657,
	User__ctor_mE64BFA90B5E315FC33D9170235F8464961688482,
	FireExtinguisher__ctor_mAE78FF7ADA50E81072520B1196A961BCA92677D6,
	FireExtinguisherList__ctor_m6A7C6B895C88D4DA3A491FAE85080851E5ECEA07,
	FiremenInsertResponse__ctor_mB2FAF27217C8DF3D791EB19A9AF078DBABD97F09,
	FiremenDetailed__ctor_m925E2B7169510C77D0930187C5073295AC387FED,
	FiremenDetailedList__ctor_m68474C4E0190F3935A780202B9C05F768DCB6382,
	Firemen__ctor_m2A881E00E190694E376F29AFAB795912F863F86E,
	FiremenList__ctor_m419026132BDB8BAC0E8A0C3670406E3E8D1E1D00,
	RequestData__ctor_m5B29AF3CB727501125FBD1BCA00EB3195E578DFB,
	Search__ctor_m9F1D82CBD4FBA64A53CF46E06C22BB18B90979A5,
	U3CLogin_APIU3Ed__10__ctor_m5A371B2FC2C44AAF0A86334AEA7CC1C17F8E587F,
	U3CLogin_APIU3Ed__10_System_IDisposable_Dispose_m1E36A717738B5EFC60E33438811C8F3DD1B68943,
	U3CLogin_APIU3Ed__10_MoveNext_m80887A731F456AD7C423425A86052CF733017AD0,
	U3CLogin_APIU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B1309DF06DFD499B36DEE1B77CE53F99252B372,
	U3CLogin_APIU3Ed__10_System_Collections_IEnumerator_Reset_m5CF1BAC5246C0E187E1D4CF1E2BA812CABB0651F,
	U3CLogin_APIU3Ed__10_System_Collections_IEnumerator_get_Current_m5E377DF1E518E6FB8A68CC55770A9A837D710941,
	U3CFireExtinguisher_APIU3Ed__14__ctor_m6E37AD2479D669253DB457C8174636523EE9E51E,
	U3CFireExtinguisher_APIU3Ed__14_System_IDisposable_Dispose_mE48CDB272A08C4F57B9C6C8354E3B49EB5D3AE29,
	U3CFireExtinguisher_APIU3Ed__14_MoveNext_m8C5DA611CCA33D4491D9A8E87745266F179EDBF2,
	U3CFireExtinguisher_APIU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC5259F54744D75AAFAA3F2A160AFFB5258F7A840,
	U3CFireExtinguisher_APIU3Ed__14_System_Collections_IEnumerator_Reset_m559439118F928FC49F8B4FA087E2D5AECDD93EDB,
	U3CFireExtinguisher_APIU3Ed__14_System_Collections_IEnumerator_get_Current_mA8757CA3F093FF2DF205838526B4432E234F56A0,
	U3CFireExtinguisherList_APIU3Ed__15__ctor_mAD02ADC58EBFFE16FFF6825C95393285F6F4E051,
	U3CFireExtinguisherList_APIU3Ed__15_System_IDisposable_Dispose_m5839073769B867DFF93CC20F6B6894472F533C09,
	U3CFireExtinguisherList_APIU3Ed__15_MoveNext_m6B02FD0CAC2F0D364DF0FEECA66A0F68BC513C82,
	U3CFireExtinguisherList_APIU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC0C5E92A3D4397B2B001594290C0F010CD62390A,
	U3CFireExtinguisherList_APIU3Ed__15_System_Collections_IEnumerator_Reset_m10D2D1A4FE103557CE313347C9359BC5B69B098D,
	U3CFireExtinguisherList_APIU3Ed__15_System_Collections_IEnumerator_get_Current_mEE38B5D80B82DCC2CD39863EC5488A0E7D41A82E,
	U3CFiremen_APIU3Ed__19__ctor_m8C5EC8A86E21946157DF2EFD0AD3E535A8FDDF90,
	U3CFiremen_APIU3Ed__19_System_IDisposable_Dispose_mE41130179346E193D46C65D75958EBC602B05865,
	U3CFiremen_APIU3Ed__19_MoveNext_m101508CE3EA4CEA57B12AADA3278BB3D7581EB9E,
	U3CFiremen_APIU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7CA8E7652383684AA3EE9278831691B45481B57,
	U3CFiremen_APIU3Ed__19_System_Collections_IEnumerator_Reset_m7CA0FE9DE77579C146500F74667E5E1948E687B0,
	U3CFiremen_APIU3Ed__19_System_Collections_IEnumerator_get_Current_m4A00CF3770D67C7616305FCCBA0DD461C4002B13,
	U3CFiremenList_APIU3Ed__20__ctor_m39C6253B3A0205A47293C746609DDE86F82F39CE,
	U3CFiremenList_APIU3Ed__20_System_IDisposable_Dispose_mD13C41F413CEA831D55090AEBA3A998ADB5273F0,
	U3CFiremenList_APIU3Ed__20_MoveNext_m5AE9CE7E4F074FFDD918DFADD96F52F9800045AF,
	U3CFiremenList_APIU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m80F7E984F20A3C7030CF100F7DB4656ACFEFEF9A,
	U3CFiremenList_APIU3Ed__20_System_Collections_IEnumerator_Reset_m7E9DF3ACADE55D44B84E3CFDEBF7C251580FACCB,
	U3CFiremenList_APIU3Ed__20_System_Collections_IEnumerator_get_Current_m45E78CBB8D455374A390226B32BAD3A8A690CEBD,
	U3CFiremenDetailList_APIU3Ed__21__ctor_mAC930F79937AFEE1FB31FFE94BF8561B5EEE594E,
	U3CFiremenDetailList_APIU3Ed__21_System_IDisposable_Dispose_mD08FCFAA218F78E704EE1575143FEC4987C45231,
	U3CFiremenDetailList_APIU3Ed__21_MoveNext_mEBD2F2F524EC645D5D20FAC2DCCCBC03344008A6,
	U3CFiremenDetailList_APIU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3B244E3C064F290DFB1B86CB92A536BC63F61F1,
	U3CFiremenDetailList_APIU3Ed__21_System_Collections_IEnumerator_Reset_mEFF9BE445E82C3385FC3E37AE8ADD1B82E1DF6E0,
	U3CFiremenDetailList_APIU3Ed__21_System_Collections_IEnumerator_get_Current_m2C9725029111E95E8B88F47C0A6730E3C47E1510,
	U3CSelectVideoU3Ed__30__ctor_mBBA963469CCD6FF9B6EF87EE8F313AE0FD19D26A,
	U3CSelectVideoU3Ed__30_System_IDisposable_Dispose_mD1053B5B6FA17801CBE26915CFC79B3AA48766E6,
	U3CSelectVideoU3Ed__30_MoveNext_mE0B8B307E8CF9B0508E6533FF9B660333FF7B240,
	U3CSelectVideoU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF3D7A6217D0104B0E45456632F6D9E9B2BB37B1,
	U3CSelectVideoU3Ed__30_System_Collections_IEnumerator_Reset_mB93214CC946EBDE373EA3EEE748CCCA94F0E2CE4,
	U3CSelectVideoU3Ed__30_System_Collections_IEnumerator_get_Current_mAC954C4889366ABDB27CF8BD34E23F03D9140153,
	U3CCheckVideoU3Ed__31__ctor_m1BE34ADBC6D6B756257DE8E006216A712C85FF95,
	U3CCheckVideoU3Ed__31_System_IDisposable_Dispose_m22951DB866AA82D4826A581A8CA3A2A450818B24,
	U3CCheckVideoU3Ed__31_MoveNext_mC9F001B04F13F86AA21F6AD9E96ADFFF56704E70,
	U3CCheckVideoU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m18BF553D353ACE9AA61693EDA687DF94E73EE4CB,
	U3CCheckVideoU3Ed__31_System_Collections_IEnumerator_Reset_mB7DD11826ECFFA05FFC4558E5CD8BD8CBF65BF40,
	U3CCheckVideoU3Ed__31_System_Collections_IEnumerator_get_Current_mEE0F174E52399BE7BE5E8FC7896254D2CCEBCF35,
	U3CUploadVideoU3Ed__32__ctor_m3F82494FAD51A1744988E92C31B6DAA5865E4E39,
	U3CUploadVideoU3Ed__32_System_IDisposable_Dispose_m198334FE7081A64C780AB3F40289802E20D7ACD7,
	U3CUploadVideoU3Ed__32_MoveNext_m92535121B50DCC8B841EAC1BFDD0389672EBF3FF,
	U3CUploadVideoU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3CE180FC3EB271F3727E401E57BE13C92BF0B88,
	U3CUploadVideoU3Ed__32_System_Collections_IEnumerator_Reset_m5197E35D8E4DA480777B92F26FED6A6120CC9796,
	U3CUploadVideoU3Ed__32_System_Collections_IEnumerator_get_Current_m764EA69BB57540EE73138B73F3F3F3F977B18DFF,
	APIHelper_Init_m9BE28A0AA4EF3293CB7FA78561872573A92112B2,
	APIHelper_Login_mCA1B3060A6BA7184B8FF9949CAF575F9E32CA7F0,
	APIHelper_Logout_m6541E32B40679060BAFB83857657CC12457F9B7A,
	APIHelper_GetUserData_m2BD2B8CC63E978945051A99D2239C3E5F830ED51,
	APIHelper_FireExtinguisher_Insert_m8FDF32147D2E7C3C81A280DEF0646623D0945CCF,
	APIHelper_FireExtinguisherList_Search_m1B24D39D5DE004F6A9211C73AAD8D1A8AA7B202C,
	APIHelper_Firemen_Insert_mBE4236003F4BBD2379402F98B800C3EEA1231B51,
	APIHelper_FiremenList_Search_m122E771393E463AF5EEE431A0F3C294FDFEC23A1,
	APIHelper_FiremenDetailList_Search_mD22CBF6F95A9A938865A85401880944D452B58E0,
	APIHelper__cctor_mA921E7652C0F8B07F71AB0D06690877B5998C2F3,
	U3CLoginU3Ed__2__ctor_m0D04E9BF7397D44770472F91B12F8637CDB631EA,
	U3CLoginU3Ed__2_System_IDisposable_Dispose_m3A65362A650183875DF9B61A839F5400226CDC16,
	U3CLoginU3Ed__2_MoveNext_m257954EA96E2AE5DAE8F7FECC1D3AACE5C0C458C,
	U3CLoginU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m843480DC9090DC568AA3A02092C6DEFE0A0D4762,
	U3CLoginU3Ed__2_System_Collections_IEnumerator_Reset_m2A23CF954AD85C566998F85FD206085FBC9A1C9A,
	U3CLoginU3Ed__2_System_Collections_IEnumerator_get_Current_m0DD5DD3C2B599755AE2034BD3252FA233AA8A1A3,
	U3CFireExtinguisher_InsertU3Ed__5__ctor_m8F43656725CCCEAD050D72F2217CDD178702E270,
	U3CFireExtinguisher_InsertU3Ed__5_System_IDisposable_Dispose_mB1883EF22AB69E048F8158069CFACA08D6D80033,
	U3CFireExtinguisher_InsertU3Ed__5_MoveNext_mC4F5B4C21E860EB52782178C13D47FAE1A527F38,
	U3CFireExtinguisher_InsertU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAFBA099A1B758D7B78DE1F69136F6A531802FAC1,
	U3CFireExtinguisher_InsertU3Ed__5_System_Collections_IEnumerator_Reset_mA32F5A9A452A20176846DB08EE957BD3EF4D7847,
	U3CFireExtinguisher_InsertU3Ed__5_System_Collections_IEnumerator_get_Current_m11D7AEF50426AF5B415D6A2F2184402ABDED735A,
	U3CFireExtinguisherList_SearchU3Ed__6__ctor_m8B83EB57DB1B6F24A05DF05A397738FC29E94848,
	U3CFireExtinguisherList_SearchU3Ed__6_System_IDisposable_Dispose_m2C717BC1DD54BA2CB06C0E19D9BAA5EF79B303BD,
	U3CFireExtinguisherList_SearchU3Ed__6_MoveNext_m85B4E3FDE96FD14A79FFC77C08F185E900257987,
	U3CFireExtinguisherList_SearchU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2622088E65A2D1D8669017AE3A57454D9C284FC9,
	U3CFireExtinguisherList_SearchU3Ed__6_System_Collections_IEnumerator_Reset_m14F5AA453C1EF93BD9259DF45DEE0150B2B5F98D,
	U3CFireExtinguisherList_SearchU3Ed__6_System_Collections_IEnumerator_get_Current_mCA1CAB7CD498E26A7D8A8B225FC545E364E2EA67,
	U3CFiremen_InsertU3Ed__7__ctor_mD08B037037AB223208CBB69E39302F0AF002A00D,
	U3CFiremen_InsertU3Ed__7_System_IDisposable_Dispose_mAFFC436912F6B03F1CD9ED0113002948035FBA9B,
	U3CFiremen_InsertU3Ed__7_MoveNext_m5D74BF7CE972A863902D5EBBAADA2D2A0553292A,
	U3CFiremen_InsertU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m304114036223ED98A053CBB1FE9820272B2254D7,
	U3CFiremen_InsertU3Ed__7_System_Collections_IEnumerator_Reset_m83176A07D2F9054B7674EBD2F3FEA3DB51BF455F,
	U3CFiremen_InsertU3Ed__7_System_Collections_IEnumerator_get_Current_mCCBA54FA3C9CCD69BC671C704AB43CA7EEDEA7A9,
	U3CFiremenList_SearchU3Ed__8__ctor_mED2B29AE3E7DB3158EB2CE62BE11D2D340004816,
	U3CFiremenList_SearchU3Ed__8_System_IDisposable_Dispose_m63529BAA60B667EC7AC9EAF4A0B69181BF65F326,
	U3CFiremenList_SearchU3Ed__8_MoveNext_mDFE28F3A188E71CF39F7E21A16C834B1F394273E,
	U3CFiremenList_SearchU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A01DD4641B64B68990C01041C746A4DD4965D29,
	U3CFiremenList_SearchU3Ed__8_System_Collections_IEnumerator_Reset_m1C9157B81C1E20582C0FDFA5AE6F65A316DAAB29,
	U3CFiremenList_SearchU3Ed__8_System_Collections_IEnumerator_get_Current_mCFFAAC9A4DF161AA75E664F45BC504BF367564A4,
	U3CFiremenDetailList_SearchU3Ed__9__ctor_m48E3561794F6F154FAD67C35C37CD84756565984,
	U3CFiremenDetailList_SearchU3Ed__9_System_IDisposable_Dispose_mBD69026A306F5B6B18720BBB79D83F51074BAECA,
	U3CFiremenDetailList_SearchU3Ed__9_MoveNext_mABB9FA607BEB19F011F52CC9FF73AA4E5A93E42F,
	U3CFiremenDetailList_SearchU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8AF08EC7D09DF8B616A430DCF3D021CD0720D5B,
	U3CFiremenDetailList_SearchU3Ed__9_System_Collections_IEnumerator_Reset_m60C2AED1F904C9F214DF4D2BC0E1891A1E9EB910,
	U3CFiremenDetailList_SearchU3Ed__9_System_Collections_IEnumerator_get_Current_mB396A6841F15B386A9E248BF2157F7A65891E834,
	Sample_Start_m17A1B560310479D6145D865A647BD612404C85AD,
	Sample_LoginOnclick_m4BC19770D8779D9DE535E74F30C8536740BDDD36,
	Sample_LogoutOnclick_m5B31C85FFF312831931213811C93AF0303A061AA,
	Sample_FireExtinguisherOnclick_m6027A01965E34F9963CEDFCFC5BB82797BC4FA1F,
	Sample_FireExtinguisherSearchOnclick_m6F12DB56A89A7C84A758E16DCDE4B401B5CB1F0D,
	Sample_FiremenOnclick_mAC88FF47ACF2E3D9B548C27CAB988A4204CA5694,
	Sample_FiremenSearchOnclick_mF4E379D0ACD259EAC0985DF3AC0950F4921EBECF,
	Sample_FiremenDetailSearchOnclick_m7AB32715B1EA558A4FA59CED5ABAB7D568F8392D,
	Sample__ctor_mA8231634864E6A7574C766EB7929D3B175E9DFDD,
	Sample_U3CLoginOnclickU3Eb__2_0_mF31B1B56CB2B8C586A2EB6CCD88C6AEE84090523,
	Util_Run_m7D124C4D8E9934C0E8888D47FFE217DD39370831,
	Util_Encrypt_mAD930D9A854DE4776504B0CB05CB1E23AD0E3F03,
	Util_Decrypt_mF902BCA1E0CD412BDA35080CC1826D7EBBD0E67D,
	Util_GetTimeStamp_mA1486B0B832C4FE31A60D97CE7922A415F3D9B4D,
	Util_GetTimeStamp_m3CA5281EC0FBFEBB622AEA11091945A6C3D8AAED,
	Util_ToMD5_m42E2FAD12D61F9A131F6E58B1D2D068638618D1B,
	Util_GetSerialNumber_m102F3075F6B492063DE286FEA38692CB571D34E9,
	Machine__ctor_m296C002DE86C45F85F38604A3C4B2A6028146A22,
	ColorGrabbable_get_Highlight_mEE9A5BC6077D6415C0F5AAEBF9268BCEDA9192CB,
	ColorGrabbable_set_Highlight_mF9C67E77A7CAB0FF638BDF7EE4C359530267191F,
	ColorGrabbable_UpdateColor_m4D60081FC49D8BD81C4B63B35405B144232F8C60,
	ColorGrabbable_GrabBegin_m756D3F58E950E5B54A1B7C4E79F2BD99AE630108,
	ColorGrabbable_GrabEnd_mCBB05FAD02AA291052245779AD795CEDA97F960F,
	ColorGrabbable_Awake_mC26EA500134341FD38388600111FF6E5668A8832,
	ColorGrabbable_SetColor_m1EA4F276B7EFDBD7EE35D598963E888307850540,
	ColorGrabbable__ctor_mCC1D8EDB3DB7A4DBEE8D15AC5A361A1B102EEA34,
	ColorGrabbable__cctor_mF2135B0A5F041846D69DE4C6085F38F57A9BFE97,
	DistanceGrabbable_get_InRange_m056971DE194AD0A8B85A8E8E3E82F76711FAF8EA,
	DistanceGrabbable_set_InRange_m0EFDE0426D5819B04621E607860A63531577586D,
	DistanceGrabbable_get_Targeted_m5D48533FA91BE16785B21D7881202E27DA1A8D0A,
	DistanceGrabbable_set_Targeted_m9D01FAEC07F2BBD49B6DA12F1A014D2AFDBDD0E0,
	DistanceGrabbable_Start_m0F13F152E7FCB05A41B8E0E8B6699462531AF358,
	DistanceGrabbable_RefreshCrosshair_m5461E3A1849F56BC79A9BEC50F63EE0F5C8C08A9,
	DistanceGrabbable__ctor_mB69087D8AA20E7C90FAA564AC6D963C5606CDE22,
	DistanceGrabber_get_UseSpherecast_mF25C99BCAF0C6E6CA18A4C42E4AB34BF8BFC6D5F,
	DistanceGrabber_set_UseSpherecast_mE0BAA3127799C6328433698FA578D4E71D8AC86F,
	DistanceGrabber_Start_m775855E7B9023538B7E56155A6597519A953ED70,
	DistanceGrabber_Update_m960BB0DB05A12AE7386926698E0153ED5BEAEAEE,
	DistanceGrabber_GrabBegin_m77865480609755B1282A5A546DC6A53FA63B49CF,
	DistanceGrabber_MoveGrabbedObject_m6B815535F8AAC860485491CCA49CD8046802F0A9,
	DistanceGrabber_HitInfoToGrabbable_m6A5994E443368A6D81B8FDABD2202A42D581AF19,
	DistanceGrabber_FindTarget_m1F30C7639B39EEB998BC94B7AD6483A235F162EC,
	DistanceGrabber_FindTargetWithSpherecast_m561C09222DCCD4C52DF037E1AFD95D6EEFF3645D,
	DistanceGrabber_GrabVolumeEnable_m5AE9B41CBEB5A5EBC1D2F7945B875794FD93A405,
	DistanceGrabber_OffhandGrabbed_m31A56380ABE7DB5847799670467539468AF085DB,
	DistanceGrabber__ctor_m80196B68D9FB9CDE30E2CEC5690F4BB97740A366,
	GrabManager_OnTriggerEnter_m6EF4F094BEAE1A7A31854F98826B3FBB1B74E91F,
	GrabManager_OnTriggerExit_mC52984A8FECA9E7098E2615B0DAFEFAEA92997F7,
	GrabManager__ctor_m5A60AC8D6E026A5A4FDAB55DAC7CEDCDD2913376,
	GrabbableCrosshair_Start_mFE636535DBC51AC58F63DDB687E6D1875D63FD97,
	GrabbableCrosshair_SetState_m5D2604BA1418ED7C18E0CFB823CAA172EA1B23C6,
	GrabbableCrosshair_Update_m9A0A89691DDE8B292D735158EB3C6EDC852DC979,
	GrabbableCrosshair__ctor_m59EFA7290C1E0CFC102C201C1303868B7AE1ED0D,
	PauseOnInputLoss_Start_m309D5D446C65347D01220D6965E848AE62DB809D,
	PauseOnInputLoss_OnInputFocusLost_mAC19B6B6F2BD7435BD93C83448F67F22A86ECEB2,
	PauseOnInputLoss_OnInputFocusAcquired_m21D3CFB7EF526E324CBF8F43F6B2102EACDEC057,
	PauseOnInputLoss__ctor_m0648631C18A65BFA8306CBD7C10288D6CB81EC25,
	BoneCapsuleTriggerLogic_OnDisable_m78D90F38B9238A03909E14C79AC447972BE23702,
	BoneCapsuleTriggerLogic_Update_mA3BC03489B0871D073AD4CF06850AD26768D4A24,
	BoneCapsuleTriggerLogic_OnTriggerEnter_m711C97FA1E316AB56D8084519BE087A638497C39,
	BoneCapsuleTriggerLogic_OnTriggerExit_m5E0F1A427B4A13D901592A0CA13FDC839C6C7C65,
	BoneCapsuleTriggerLogic_CleanUpDeadColliders_m93077C24AD19895958A85B39B597DDAE5EFC90B1,
	BoneCapsuleTriggerLogic__ctor_m11A006B3E2E5D97ACC8F7908F57598EB8A7FADBD,
	ButtonController_get_ValidToolTagsMask_mC1A23D42383354957AC3ED2EE4E0E443E4BD7EE8,
	ButtonController_get_LocalButtonDirection_mCD9B2BA21A5208ACF45B9DE2348951CED9A2F597,
	ButtonController_get_CurrentButtonState_m97D3281E1AA62F0B28B9FC13D5C6D97E8898374A,
	ButtonController_set_CurrentButtonState_mDDD60059979F7FCE78D320A3D034ED7550F076C1,
	ButtonController_Awake_m9A642360ACD027C8D3C3B39598D5D7483AB9D199,
	ButtonController_FireInteractionEventsOnDepth_m8532030AB88C8754A41BA83EFE29ED352DE838C9,
	ButtonController_UpdateCollisionDepth_mD5E62BC8315DE7342BBD20473AEE3FDD65788A3D,
	ButtonController_GetUpcomingStateNearField_m4377557E72D77C2AA2D3FE1DEAE6F5067F54D12E,
	ButtonController_ForceResetButton_m82BCDD2D684499C11C329CB5AD25C9DAD23AEE9E,
	ButtonController_IsValidContact_mF9BEA01F40D5CF760D0D26E15603D51B0BE2C59E,
	ButtonController_PassEntryTest_m1CC5A6460FD676ADC06C3D8C003D3E90784028B0,
	ButtonController_PassPerpTest_mCCEFC8E20AAF9FBE58E5BEDCE554CEF048EC6196,
	ButtonController__ctor_m64221E61350B4251820CAA250163801C4AAC1781,
	ButtonTriggerZone_get_Collider_mA97511B633C8FA23B1E1B864FE94DF7771424CCA,
	ButtonTriggerZone_set_Collider_mB7AE7E1CA02A2EB268DD49032D0F33472F443AB5,
	ButtonTriggerZone_get_ParentInteractable_m14EDBF5B2E361C7B71513E0FD4D10EA87D388693,
	ButtonTriggerZone_set_ParentInteractable_m6BF1259D2ED0DF4B565C08D00C2A867074C03A53,
	ButtonTriggerZone_get_CollisionDepth_m066AACBCB480A4D05E1F9910AAB71F3EB4F90558,
	ButtonTriggerZone_Awake_mAEDDFC443C2337ECC22046203921081412A8253C,
	ButtonTriggerZone__ctor_mE84EA0EF8CC850618DF29DCF722071F662F46D35,
	NULL,
	NULL,
	NULL,
	ColliderZoneArgs__ctor_m2F17926EE45B385F3291ABB057D0DB1C46E7C721,
	HandsManager_get_RightHand_mE9F238C463B7E656776BAD73EFE3EF3EDF4479B3,
	HandsManager_set_RightHand_m007F2D3739DF6176DA4F7E496C36EEB66A2437B6,
	HandsManager_get_RightHandSkeleton_mBCF403959F6ABB1D38A64EDFB2EF5410A71BA9D8,
	HandsManager_set_RightHandSkeleton_m9F1BE77E728E2B0ADE142380CA4EA62589754159,
	HandsManager_get_RightHandSkeletonRenderer_mEBEE32D6AB8B63D8DA808FDBB8FC20F6E066D28C,
	HandsManager_set_RightHandSkeletonRenderer_mE330C23C803564C20D05F29CCA79AA575E32D3F1,
	HandsManager_get_RightHandMesh_mA1B64CFD74E89E1F8A3A59C040AC9AA732837C6D,
	HandsManager_set_RightHandMesh_m8B308743A2F158A112E5C0EBB6A54A1D19451DBD,
	HandsManager_get_RightHandMeshRenderer_mE2D447583D12CB6E51360CD78BD25B85C33D613E,
	HandsManager_set_RightHandMeshRenderer_mD2468B3A5B1C6EA97F07D6314DA50969C600380E,
	HandsManager_get_LeftHand_m53807ABE4939BD2D9260E268C6674B7011713BA8,
	HandsManager_set_LeftHand_m32768139F21ADDBCD87E2833D2BB30DAE5B59F4F,
	HandsManager_get_LeftHandSkeleton_m44A310FFC70C3CB410D2DC2F235F57B1B58FF2D2,
	HandsManager_set_LeftHandSkeleton_mC76A55A9DCF670FC60B34CC92DD81027E7735B08,
	HandsManager_get_LeftHandSkeletonRenderer_m8FD29F034B1EC81684F4C74A8301596395011B48,
	HandsManager_set_LeftHandSkeletonRenderer_m1C5A2A642C6614C040085355D8214E16D934D28D,
	HandsManager_get_LeftHandMesh_m16A9E535B1B520F309D0886704A3201648B16CDD,
	HandsManager_set_LeftHandMesh_m56E8B4E036DA7AACB922228B4FE27FCC272278E3,
	HandsManager_get_LeftHandMeshRenderer_mDE3580A395755759374279284C9650D59D09CFF2,
	HandsManager_set_LeftHandMeshRenderer_m102CF222EBA2577F468099C726325DF3B779FF64,
	HandsManager_get_Instance_m749E74CBE6267511C58A4033FB6FDF3C8D03AF1F,
	HandsManager_set_Instance_m0F4B8CD87D1B95FB7F64D3BD246FAEC5ACBD7F53,
	HandsManager_Awake_m7D236135CCB8A2F47F442B2EE89281B985ABA02A,
	HandsManager_Update_m3AAFA9F8B8B994326B7695807BA0D38FF592C2A3,
	HandsManager_FindSkeletonVisualGameObjects_m31E51747D167E74132150423190EC4AAC13B1E53,
	HandsManager_SwitchVisualization_mFF5FEEE41ACCB620EC9E98BA8364A343CE7F5584,
	HandsManager_SetToCurrentVisualMode_m183D5AEF5523A39C0054A34E304FE88E25E9E277,
	HandsManager_GetCapsulesPerBone_mC98582D7BF3248388FA2A5153586EDCAF029D8EB,
	HandsManager_IsInitialized_m607714995C91CC61C6672A91A631EAC7FA934917,
	HandsManager__ctor_mF764E9D2BE5BB64D933E7B6925E0F549EA1341C6,
	U3CFindSkeletonVisualGameObjectsU3Ed__52__ctor_mB55CA2BF6CE22AC93FB1D1E0324FA76F2895B6BB,
	U3CFindSkeletonVisualGameObjectsU3Ed__52_System_IDisposable_Dispose_mBB9AC6B3987FBE05D17229FC65772D6AEF1AE5C5,
	U3CFindSkeletonVisualGameObjectsU3Ed__52_MoveNext_mDB2E7404E01F1E9764DFD7C7E69BE594B4130CFA,
	U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53507F454464337DB141FA4B5B17D2ABBD2C0FE4,
	U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_IEnumerator_Reset_m38AAC961B881CFB50A5DD5D622E430A3F56E6416,
	U3CFindSkeletonVisualGameObjectsU3Ed__52_System_Collections_IEnumerator_get_Current_m1534C6E698DEA0AE22B6DB0F7352E4815D614CD3,
	Interactable_get_ProximityCollider_mF6F604B5AC84CDF8FDA76502CF97731CC056FF08,
	Interactable_get_ContactCollider_m4735130C7B2B2FE783C186EBC1A482C7E29D3873,
	Interactable_get_ActionCollider_mBAF9B046536B533C7BA2226926BBC50E17B6726D,
	Interactable_get_ValidToolTagsMask_m85DABBD0A6116F184E84F198CAC92C26121F6DF6,
	Interactable_add_ProximityZoneEvent_m2EFA9B8880CE03E14E54AAF2282D089F2E4B3FA0,
	Interactable_remove_ProximityZoneEvent_m314D6AC5B0CAC5A5FF915B383F9C4E24A44C0183,
	Interactable_OnProximityZoneEvent_mDED9C800E85CF54A06FB3CCA1DB2EF51ECF15A1F,
	Interactable_add_ContactZoneEvent_m284AEAF4458BB62779A75C591F261091518B3481,
	Interactable_remove_ContactZoneEvent_m96A59B1DA482F032CF5D490918466CF68A1A5D02,
	Interactable_OnContactZoneEvent_mF346FBAB8E4F53A26B148E8126759D64A70B5E73,
	Interactable_add_ActionZoneEvent_m303D57C7749AD0BE641D5031D90F4A1053B9B438,
	Interactable_remove_ActionZoneEvent_m4E8476203B9F028436EBD7F9E29DD629BE44FF2D,
	Interactable_OnActionZoneEvent_mA4413FFF1DD8593420D456C9A8EEA3487D1CB9DE,
	NULL,
	Interactable_Awake_m673622B78CF87568C609EDD039F0D947CF4E9F32,
	Interactable_OnDestroy_m24FA62B42CADA4737F199EFB99C057CCD0ADFB94,
	Interactable__ctor_m38A75FA2BE69E84CE6E111E03319FA303714B4D6,
	InteractableStateArgsEvent__ctor_mD35D9AB2D5844DC8C2246F8AD4EA36A0B09308DE,
	InteractableStateArgs__ctor_m3C317F0382FE5F267D662DDCFABE336F06862624,
	InteractableRegistry_get_Interactables_mA04622D45767EF063B111AC2407C5B0A46DE07B9,
	InteractableRegistry_RegisterInteractable_m3D376455E898032EF5A1C68D44E0A14AE631CB84,
	InteractableRegistry_UnregisterInteractable_m7DF572861A04C4D9D3C14627C2A367F2AEFE5E0A,
	InteractableRegistry__ctor_mF51128E4BD939E58FF4E97F8F43DF49290548B80,
	InteractableRegistry__cctor_m2C0C6016085C12A509690BDD8585897D6576D023,
	InteractableToolsCreator_Awake_m52A416198A9987B601BFB7B4DA2D4F81763787D5,
	InteractableToolsCreator_AttachToolsToHands_m494AA7A9F37A70A890E0E386D44474483AAB9169,
	InteractableToolsCreator_AttachToolToHandTransform_m76F801FCEFADB36E154C683F39DAC5A38CD6F1D5,
	InteractableToolsCreator__ctor_m3B28F7ECE7DC21C19A7F090418D93B4A90D279A4,
	U3CAttachToolsToHandsU3Ed__3__ctor_m734EDB11729722D3E2FF780175D03056A3FB6B59,
	U3CAttachToolsToHandsU3Ed__3_System_IDisposable_Dispose_m465E039D390579A8A71EDEA3E92B1B21759BBFF9,
	U3CAttachToolsToHandsU3Ed__3_MoveNext_mED0C488063607CCD70041DB0782687049723B1E0,
	U3CAttachToolsToHandsU3Ed__3_U3CU3Em__Finally1_m8A2BD809ED53D8BAB20DB018A676FEC81B1C6206,
	U3CAttachToolsToHandsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9D3D39C28404672F886B0E4DF9571ADEFD737D,
	U3CAttachToolsToHandsU3Ed__3_System_Collections_IEnumerator_Reset_mF4B96290820469D2C97EEF77C56725A051EE2A5B,
	U3CAttachToolsToHandsU3Ed__3_System_Collections_IEnumerator_get_Current_m1BC783E9E2AEAC011D84DDF68154B2A50B83686F,
	InteractableToolsInputRouter_get_Instance_m688D5ADD47AD768092EA65094D4763D31CA37DC7,
	InteractableToolsInputRouter_RegisterInteractableTool_m0427DDDC21A231B5E405F2D2B2C91470C99F0656,
	InteractableToolsInputRouter_UnregisterInteractableTool_m10F7F03FBF73F6D75A7D9FB96564C26190BCF96A,
	InteractableToolsInputRouter_Update_mFFABA2B1A23E61F20573C7589FF38352FBF5B09B,
	InteractableToolsInputRouter_UpdateToolsAndEnableState_mDC4F03CF1C73759E460B7C55D69DF2953B928516,
	InteractableToolsInputRouter_UpdateTools_m497363491848B42184F5CD40662A510E5236FE5F,
	InteractableToolsInputRouter_ToggleToolsEnableState_mCB03C3A19E2CF7287E61B7BD684523AF54DECAAF,
	InteractableToolsInputRouter__ctor_m1F12838905D93E459F495D3417F16A3347BC7359,
	FingerTipPokeTool_get_ToolTags_mBD6F55F53B1190B9D1A07EE70C58B117C35DC144,
	FingerTipPokeTool_get_ToolInputState_m5135949E46396699EFB438DCEF083077CA58A42D,
	FingerTipPokeTool_get_IsFarFieldTool_m19F2F230707836E77DACA32EA3295BB3836E38FE,
	FingerTipPokeTool_get_EnableState_mAFAC89BBE30FA63FD0361CB2C73F546A6421A81A,
	FingerTipPokeTool_set_EnableState_m53263FC9F17DBD554E665FE970FFFB8660840772,
	FingerTipPokeTool_Initialize_mF6EAF229A254AAA628C55FD25DFC45F6DB52603F,
	FingerTipPokeTool_AttachTriggerLogic_m806CC48725DBC7E41BE57F20DA730BEB3C96EE1F,
	FingerTipPokeTool_Update_m2C3F38F5A27DC330842FE5B82FCA1C850EE3D85B,
	FingerTipPokeTool_UpdateAverageVelocity_mC1BFD7068F92A60898578833DB93CF37BCA75E38,
	FingerTipPokeTool_CheckAndUpdateScale_mDE1DDF48D29BA291BCE6EDB0EDB6CDD04E803207,
	FingerTipPokeTool_GetNextIntersectingObjects_mA962AC37DB3CBF382E81F163B450CC69A42E156B,
	FingerTipPokeTool_FocusOnInteractable_mBA11E0AA0F742ADA5FC0859146C644DEA5D51AE7,
	FingerTipPokeTool_DeFocus_mEA2E9F73BCF74755786061B17A11B663BB01474C,
	FingerTipPokeTool__ctor_m0793D77F8FC85C10E5943D692BEC9AC367F68173,
	U3CAttachTriggerLogicU3Ed__21__ctor_mCC871AFFBBB476D21423473C54CBDEBD2F14CE9D,
	U3CAttachTriggerLogicU3Ed__21_System_IDisposable_Dispose_mBBABBA85272B6862FE1453CBD314E245B6EA24E7,
	U3CAttachTriggerLogicU3Ed__21_MoveNext_m4D36E1F74B87AF1B2AE443A494AC3BD5616CD4FF,
	U3CAttachTriggerLogicU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91F1B4E86C3AF75E8070FF63A39EBFF0539499B5,
	U3CAttachTriggerLogicU3Ed__21_System_Collections_IEnumerator_Reset_m550408ED48447F61859BFBD490115411547A6856,
	U3CAttachTriggerLogicU3Ed__21_System_Collections_IEnumerator_get_Current_mB807F3B3CCBDAF22F0233E0A7A6CDB2DE46DC36E,
	FingerTipPokeToolView_get_InteractableTool_mBB4D33BE156B9EE4FBFEB6D3558E3E04B8ADB51F,
	FingerTipPokeToolView_set_InteractableTool_mDF529210D8F673D622B4427BBDC28106C80FF9AB,
	FingerTipPokeToolView_get_EnableState_m4C69AA39F36971BCB98909949405B1B0766A7391,
	FingerTipPokeToolView_set_EnableState_m5D51EB5315EB2FBF3FE5F9B471C9B1313905820F,
	FingerTipPokeToolView_get_ToolActivateState_mE74D50A247203ABF3902B02AC10F46172A64155B,
	FingerTipPokeToolView_set_ToolActivateState_m5D27DF2EB537D9BA0E4DBA30FA1A339E7EC45EFF,
	FingerTipPokeToolView_get_SphereRadius_mA3C7336BF367853C422F0AFEB012A77CFB627BD8,
	FingerTipPokeToolView_set_SphereRadius_mAD1E864E25672BE5A87327B53BEF716C3AF34275,
	FingerTipPokeToolView_Awake_m5F066BEF3A88699EBF8BD848574BD8CDCFDFDC24,
	FingerTipPokeToolView_SetFocusedInteractable_m5B02B5A8DECB19168DF581C171368E46F36D9409,
	FingerTipPokeToolView__ctor_mAF5D655B9724611A68D1A582B2473D035D308784,
	InteractableCollisionInfo__ctor_m7348491A5EAFD2DB5F4B9702C512F9244B6EAC91,
	InteractableTool_get_ToolTransform_m205F590619ED079C0A72EA22A89EE3927842DCA7,
	InteractableTool_get_IsRightHandedTool_m737F795BBD30E1EC82B68A7EF69CD222FB31541A,
	InteractableTool_set_IsRightHandedTool_m465A3CE11D7F4A97C4D2B0FC69F90922C886D53D,
	NULL,
	NULL,
	NULL,
	InteractableTool_get_Velocity_mAC1D35D388D176A5F972A883516180AC66B8090F,
	InteractableTool_set_Velocity_m42BE2E5966AF9A78A1C157A35B123926EFB1814B,
	InteractableTool_get_InteractionPosition_mE0FB722D4A4A52B1ACFB6D35CAFB876AA01C2111,
	InteractableTool_set_InteractionPosition_m7D1E65ACF3D7C8D81E733557B4B97C858B11FAE2,
	InteractableTool_GetCurrentIntersectingObjects_m6F4274996E629AF558DAE685B712356444DE3147,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InteractableTool_GetFirstCurrentCollisionInfo_m128C0E18F5FF4B752ECAA380CEAD01A0C35D4C40,
	InteractableTool_ClearAllCurrentCollisionInfos_mB2B670C225F4D7E9ECA1FE8F32A4591C794D3F95,
	InteractableTool_UpdateCurrentCollisionsBasedOnDepth_m0C7062FF22E200B4D7E8E315BC862B9D47698F9A,
	InteractableTool_UpdateLatestCollisionData_m464888128C71919DDBB2C6C7688EAF0740634250,
	InteractableTool__ctor_m53710FF617E85E4274C1AAA0653FF167C28A3722,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PinchStateModule_get_PinchUpAndDownOnFocusedObject_m6A5427560A3F4A2372BC64C89FD513CB60D6E345,
	PinchStateModule_get_PinchSteadyOnFocusedObject_mE211E69F60B0867D44633FA5B25FFDFEDE2E5D13,
	PinchStateModule_get_PinchDownOnFocusedObject_m1D2C7ED77EFB4833B1921792A3735636BA207109,
	PinchStateModule__ctor_m06DE10F3EADE5A00F264E444C105C23A0F4D6891,
	PinchStateModule_UpdateState_mE6EC067C8424F21E0C1553703E69E5B25E56DCBE,
	RayTool_get_ToolTags_mF79256339F1A375AF576B2E2029537F43EE2C0B7,
	RayTool_get_ToolInputState_m54478A71F7ED3FE7E08BE631D08037E4A6F76D89,
	RayTool_get_IsFarFieldTool_mF690CF4ED00E55B933E58EE8B46737D215F4C6A7,
	RayTool_get_EnableState_mF4A05EDD10F5FE793D780CF3745D6881ED3B3D3D,
	RayTool_set_EnableState_mE27327551A200506EC3FA1185A10F9392AAAC4C9,
	RayTool_Initialize_m6C629CDBF7127645DCD9CABC894F1D1327FC036C,
	RayTool_OnDestroy_m8332B404479AA0DA187DF0C424576FB9262442C1,
	RayTool_Update_mFF995DE32795EB15C757CFD5CB8201786B0F62DC,
	RayTool_GetRayCastOrigin_m4AE3575A3D65B27E3809814C3415E39FC5489B47,
	RayTool_GetNextIntersectingObjects_m8BA14292395263E433DD82B9F64080C42A5A4821,
	RayTool_HasRayReleasedInteractable_m9B7A76FC8F15B453D91980F1D118EFEF432BA5CA,
	RayTool_FindTargetInteractable_m4D0BD21D1425CD76DD0FA675ECBF7E3AA4AAB685,
	RayTool_FindPrimaryRaycastHit_mB6C7E211ADEC012A379E02BBC2D60B5B567CB8F8,
	RayTool_FindInteractableViaConeTest_mEDB0AE5A0EF2B9FF895B7D08EE6B50A5DD1AD5CF,
	RayTool_FocusOnInteractable_m535AB705865AB75D40842C9B5587E17732DDD2F9,
	RayTool_DeFocus_m085E103A8A047F7067906C31DACE15ACE126C6CE,
	RayTool__ctor_mC1D1E10E1C6B15267986E22E04C6A23966CA681B,
	RayToolView_get_EnableState_m464E298E5F0DDCF469868DA3F7A07C1BC74FCDE7,
	RayToolView_set_EnableState_m0C757E1F42C28C763F57C93F4670ED3DA3B511A5,
	RayToolView_get_ToolActivateState_m5DE8AD80488ACB610C8A00467984095998DB4723,
	RayToolView_set_ToolActivateState_m382F731A347E5EF495984B48AB4EE7941FD0E99B,
	RayToolView_Awake_m8D4B4C38C29579601A9E21E8BE3A2E4E95246985,
	RayToolView_get_InteractableTool_m6678005A9883BB94200F353DF981AE211CFFE44A,
	RayToolView_set_InteractableTool_m0E3083D4DD470BB252858CF5D401E248705FD46B,
	RayToolView_SetFocusedInteractable_mE7F566BFFD744607512ACBBDC070D22D737A1E64,
	RayToolView_Update_mC0702F11E7C9A01B8E14CDD5C607E15B2B1882D7,
	RayToolView_GetPointOnBezierCurve_m714E7DE6A245136E03757F982C68847DBF0F55E4,
	RayToolView__ctor_m729843F49E716782D52EEB4D3A725EAE9E7F59D9,
	DistanceGrabberSample_get_UseSpherecast_m1940F73780371F34046759ED81DDBEDBFCA99FAC,
	DistanceGrabberSample_set_UseSpherecast_mE18E95F373903F263003BD244ED69D60E4F17C74,
	DistanceGrabberSample_get_AllowGrabThroughWalls_m0E2C0AE5522B8066A49EE17D4ED8901796CEB3F2,
	DistanceGrabberSample_set_AllowGrabThroughWalls_m47024AC8A8622EB0582E9FBBD92AFAE9FDC70186,
	DistanceGrabberSample_Start_m17A8BC2B0CD5CA1D6A19E5AFAA95B07239A6EFDC,
	DistanceGrabberSample_ToggleSphereCasting_mA150837732BE9CA29DB37AAA3D21D0956F198488,
	DistanceGrabberSample_ToggleGrabThroughWalls_mCEBEA385FE68A84650D3849E5B20D729FE663A74,
	DistanceGrabberSample__ctor_m91EB15189BB6C76FE3D70960D66F80C3F474CE39,
	ControllerBoxController_Awake_m10CE6D278450B3090C9F6FEBE087CA23CC9FFD8F,
	ControllerBoxController_StartStopStateChanged_mD500AA96C8E03F97F611D5EA54F954C933753451,
	ControllerBoxController_DecreaseSpeedStateChanged_m3229EACD0C33045DC2CB25A0BF6120F0838F0EE0,
	ControllerBoxController_IncreaseSpeedStateChanged_mEFE12568E8F140F770F50AF38395E0DB22C9788B,
	ControllerBoxController_SmokeButtonStateChanged_m886E38B2844CF61820F76E9724FDCCA2C61CA372,
	ControllerBoxController_WhistleButtonStateChanged_m28F2550607F6321A43FFB7891295EBBCCC90179A,
	ControllerBoxController_ReverseButtonStateChanged_m8183F155BD2B5E5F76680E51DA08B88B315AE0FA,
	ControllerBoxController_SwitchVisualization_m5D7DB78DF80500F6C712B626B834CEC6BD2D59D0,
	ControllerBoxController_GoMoo_mFBEDA2C31525584CD95F0C59E73B5A10E9FCD509,
	ControllerBoxController__ctor_mE2B5C31F1DA7DC4A076BB4715E72787A1FB3705C,
	CowController_Start_mC4B6534879CC6708AFC5D442E5282DFE915173D7,
	CowController_PlayMooSound_m6EC41C83F14928C33AAB8C96D1DB6F10E70B5104,
	CowController_GoMooCowGo_m0F2CD8AAFA84A5E77222026981CE7BE734090D96,
	CowController__ctor_m4EC07EAB37300BBD79EDBAA4073CB736455CBF28,
	PanelHMDFollower_Awake_m2CF549A1B7AD1994E135D0C3A64B3AAE4BE1C91C,
	PanelHMDFollower_Update_m78849650838D5B71CC1A8AF283E9A26DF5E78E17,
	PanelHMDFollower_CalculateIdealAnchorPosition_m74F280D8BC9291C194EB4FC9817A4150ABC944E2,
	PanelHMDFollower_LerpToHMD_mD8B9F448F88D9F598368D4B930DFA7BC19A843BB,
	PanelHMDFollower__ctor_mDFD1C555C729E0009E9FC989048FA5806267EC72,
	U3CLerpToHMDU3Ed__13__ctor_m2718D92561980DA3B2F04C2D102AB928155D0AAA,
	U3CLerpToHMDU3Ed__13_System_IDisposable_Dispose_m6F79FD9B76A0633FBC96538B385DB07AF57C90DC,
	U3CLerpToHMDU3Ed__13_MoveNext_m9C0F7AA018E489420080E85A9C327248E2301DC4,
	U3CLerpToHMDU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD58D03CC073A72DDD7B0EC4188CB4614E92A68AF,
	U3CLerpToHMDU3Ed__13_System_Collections_IEnumerator_Reset_mA09722794732F819AA5D31CB2AF5BCB0C2FD817A,
	U3CLerpToHMDU3Ed__13_System_Collections_IEnumerator_get_Current_mF39A3910A6D633D8600FF5B154BC7B6764772E9C,
	SelectionCylinder_get_CurrSelectionState_mEEA0EACE12CE6A5F1BDAA7E59A7EC5D77D56FAEA,
	SelectionCylinder_set_CurrSelectionState_m960B75182FF50885FD248F74A05F7D33BA3D67D3,
	SelectionCylinder_Awake_m3C9863696F1825751F817545A99016F0840CC0EF,
	SelectionCylinder_OnDestroy_mB8F6CF93EB637C30627D4851B5F0290D0D730A49,
	SelectionCylinder_AffectSelectionColor_m98DE925B474211ACCCCB6085DDB3A90399E4DCA3,
	SelectionCylinder__ctor_m19D204E84FD5989ECFE8FBF1480C7FCF758CFE92,
	SelectionCylinder__cctor_m3809D35D3304F30A86CCECB1D957BE393A8685F5,
	TrackSegment_get_StartDistance_m7754F5C2A4BE8A74DB09CE3CED8D2598630B0D7B,
	TrackSegment_set_StartDistance_mCAF42BD5254AFCFFE3001A6E20070939A6F20195,
	TrackSegment_get_GridSize_m3D99EF429B98F214239CA9CA54A076BBB7FE92E3,
	TrackSegment_set_GridSize_m9823CF02AE0029655CBEAFF47200B88A76BD2E85,
	TrackSegment_get_SubDivCount_mE962363663526F01DC7D77F360A5E97A5E8D333E,
	TrackSegment_set_SubDivCount_m208543971918F3C42B26627EB3BFC845A034B19A,
	TrackSegment_get_Type_mB16F5DB9C4FD3AF8FA91CF5880D5DD48AA8681FF,
	TrackSegment_get_EndPose_m0F2F98AAF1A5C6C067720CA8067C3FD47D43876A,
	TrackSegment_get_Radius_mB458D02292A84615CA9BFEA5F03C1059268D6BE6,
	TrackSegment_setGridSize_mC74C3F3285B5ABD033CF68BDB0770DE56F16E990,
	TrackSegment_get_SegmentLength_m63FFFA4AD5055C552C805872637BC6839AC55F39,
	TrackSegment_Awake_m9650E6E117E2253990A03F82515ED14E990DDA97,
	TrackSegment_UpdatePose_m97FBB83914AB8D3CB49D9D85467DEB7431248B53,
	TrackSegment_Update_m108D239B881A8A7A1729E74C2EF814E2376D5DB5,
	TrackSegment_OnDisable_mB9B6EB483265C1B1BCF980EA6F58ED15D06ABB0A,
	TrackSegment_DrawDebugLines_m37FEF6E5D1842508645C8F8FEB28BABDA03D7253,
	TrackSegment_RegenerateTrackAndMesh_m4841B12BDC519C7862B4901565B44A9B6787AD5C,
	TrackSegment__ctor_mC6FA53B0963D2B395BCE338C1EC39985B28C4F96,
	TrainButtonVisualController_Awake_m84F6745B2D689E89F2AAF1ECA67B2FC59FEC4F14,
	TrainButtonVisualController_OnDestroy_m297A762E98F73EF8414F095856CE387C438D1798,
	TrainButtonVisualController_OnEnable_m0564F0AD8EDBCF9FAFE21A3D15EB7CCC7C65DE97,
	TrainButtonVisualController_OnDisable_m24A294D4F2E5C301D05DE52F29EDA4400EC831C5,
	TrainButtonVisualController_ActionOrInContactZoneStayEvent_m7AF8B0FD00D7F603EC903094DC343CC42BF7ABD0,
	TrainButtonVisualController_InteractableStateChanged_mCF09476B0142274EAFE2C5ADE762C28633BD23EF,
	TrainButtonVisualController_PlaySound_m65FC0A9207B130533020BEA24C3F291415B02249,
	TrainButtonVisualController_StopResetLerping_m9ADCE7C308758261CC14032402BEED4F21EE4B4B,
	TrainButtonVisualController_LerpToOldPosition_m95E0582BA249685039EEA46A3AD1BDD57CCE3B68,
	TrainButtonVisualController_ResetPosition_m9232F6767751D6404FFFCB0944D9FE7926F02B0B,
	TrainButtonVisualController__ctor_m67AA3D848961906B2E5F20150B0C82462B7E190F,
	U3CResetPositionU3Ed__26__ctor_m5635000743474AB03A282A54DAA034BE871944D8,
	U3CResetPositionU3Ed__26_System_IDisposable_Dispose_mFD4D0FB62BBFDC93E0DEAD7EA68430B7A4ACAF69,
	U3CResetPositionU3Ed__26_MoveNext_m5E72132EBEF7DB0236E6E8EBD6632A16E0F748E0,
	U3CResetPositionU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BE3FF62F4373A00DD3E8BBCBB2036F6761E547B,
	U3CResetPositionU3Ed__26_System_Collections_IEnumerator_Reset_mFAB28539B7B0AF0B783CD738C41D9DC51A1E8853,
	U3CResetPositionU3Ed__26_System_Collections_IEnumerator_get_Current_m84A8CF3B9E09A2B224C5C4969FBB701BB9E8FF7B,
	TrainCar_get_DistanceBehindParentScaled_m373B41D22495598A3431F0C5FC5017D442208A2F,
	TrainCar_Awake_m05D4446FBB4ADAF94AECC0466BBF94055909C8F0,
	TrainCar_UpdatePosition_m338A73EAC14EC679C9F5E00DEED605CEA2BDE432,
	TrainCar__ctor_m086A9601B0ADD3B4FD4118437F6DED5539A25486,
	TrainCarBase_get_Distance_mA33D6D4848DFD0820D2C75F9E1F2A76DC755E8A6,
	TrainCarBase_set_Distance_m4C3DF32DF27C6B14AB536FA6A983A8AD056B55CB,
	TrainCarBase_get_Scale_m5DBF114FDC5DBE97AD3C5BC2712624BBADF3F5D5,
	TrainCarBase_set_Scale_mAF09262F53F500D9ED7DA7829847172C5410ED61,
	TrainCarBase_Awake_m37A702A4A3B24463E19C126F64F2D89F11A9A502,
	TrainCarBase_UpdatePose_m0A67C10B69FB1D9A986E883B1CDC382B571085BE,
	TrainCarBase_UpdateCarPosition_m2B2E8799969C239937C47D4615D2101E6F9133A1,
	TrainCarBase_RotateCarWheels_m965650C8E46E761835CC060092CB6F4B69252858,
	NULL,
	TrainCarBase__ctor_mF505ADDC2083B90F7A3F45C89C32025DD0002ED8,
	TrainCarBase__cctor_m9F34E6EE6F8D87A0B97D210D6550DE0CC0755706,
	TrainCrossingController_Awake_mC8448F974906D7A72F4FB52C18CC6B00887E120A,
	TrainCrossingController_OnDestroy_m5913750A3AF404FB4E8FC54EA209A020A231A5D4,
	TrainCrossingController_CrossingButtonStateChanged_mE31580E5F7428418ECBE6FF15AB6BE20D73D998D,
	TrainCrossingController_Update_m580817DC6CF5C8E2EB48A214F1F84EE51EFBB736,
	TrainCrossingController_ActivateTrainCrossing_m85B47BC1A24078D2E973B527337EEB61794E7849,
	TrainCrossingController_AnimateCrossing_m8AE7CFD19C0FF9120FA3040DFD02538E8A297C65,
	TrainCrossingController_AffectMaterials_m6401A1A3E5B6E85E11681CF60FA492BB4BA510FA,
	TrainCrossingController_ToggleLightObjects_m4B93FC82AC5CA62BC1F0755839F5AE2D3983C38C,
	TrainCrossingController__ctor_m0EA7B850A7E56F174B1E49FF0513392222383826,
	U3CAnimateCrossingU3Ed__15__ctor_mFAA4EB62DD8CF2E800A17FB70610D210D4572B2C,
	U3CAnimateCrossingU3Ed__15_System_IDisposable_Dispose_m15B4C628F2391E4765AAD7E5FBEB1ED37163CF6C,
	U3CAnimateCrossingU3Ed__15_MoveNext_m9E22324ADCB019B88C3415E7B1087C2E5E277572,
	U3CAnimateCrossingU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E5821A766805B7DA85AF5CDF5B3E79C9007A97D,
	U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_Reset_m1221B7705FF6866AB85D415E8D00A92F1B40B0C3,
	U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_get_Current_mDF8E56DBC547B33C024A5EB10750F3D8B075A412,
	TrainLocomotive_Start_m596B010A45C501F05A1BE9CE412138AB82498900,
	TrainLocomotive_Update_mC64296C895306B462D399E315CCFE6106979396A,
	TrainLocomotive_UpdatePosition_mF409B3053EC449F8F57AB7B971A9053C642C0D1A,
	TrainLocomotive_StartStopStateChanged_m37D2604A94869E9ABAE686C05C06DA8039D5EAFC,
	TrainLocomotive_StartStopTrain_m2983A1995E2F95A8642F03A8105C0B489F9C8037,
	TrainLocomotive_PlayEngineSound_mBBE7F6F9988F707FFC60EE195E18C7C7C37C05CE,
	TrainLocomotive_UpdateDistance_m18B8F2E7593CBA725B2A7B5034AF46279D35A321,
	TrainLocomotive_DecreaseSpeedStateChanged_m58EE2750F1CC037AB7C44A209C5D7A3D6FDCB9A1,
	TrainLocomotive_IncreaseSpeedStateChanged_m4DEB392A7FA443870D2A849A663EE6E3E740A22B,
	TrainLocomotive_UpdateSmokeEmissionBasedOnSpeed_mABC70306F1FBF3D00E0BA0901760D44793F9D3EF,
	TrainLocomotive_GetCurrentSmokeEmission_mCBF588B02E4B7CB77E4A4EEE98FA557C6C09A267,
	TrainLocomotive_SmokeButtonStateChanged_m2E6E404156BF780272ED69C4A2D7D07FF94E01EC,
	TrainLocomotive_WhistleButtonStateChanged_m2B8FF22070F53567B9DB0EF8F021411531CCF240,
	TrainLocomotive_ReverseButtonStateChanged_mDC7C10B6A595AABCF8351143917C1CDCFEA74A98,
	TrainLocomotive__ctor_mF249EAA95B7C51F5D9493892D960C0E8460AF909,
	U3CStartStopTrainU3Ed__34__ctor_m39BEC907A4658AB1DC65D214A86D35F6E246795A,
	U3CStartStopTrainU3Ed__34_System_IDisposable_Dispose_m6A0A898733F46F849ACEF0984B4C8938E5A88B20,
	U3CStartStopTrainU3Ed__34_MoveNext_m02D96704D3CA82F8BECFC12A487D9C4AC7903FA1,
	U3CStartStopTrainU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26D7E65A695D870236E43D9C2E2D46D88EA33065,
	U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_Reset_mE5B9500E4605106C99B8718D3105A15B0CB73A87,
	U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_get_Current_mEF6808D31C6E8D74E845C551B8618F3902FC469E,
	TrainTrack_get_TrackLength_m2527E507114E21D65C843667ECF4F42CBA3F7FCA,
	TrainTrack_set_TrackLength_m32ED334511B32330D895514C07B3BED3381F02E5,
	TrainTrack_Awake_mF48A851C3560D8D9ADA00466789E9571E49047B4,
	TrainTrack_GetSegment_m357BC8C004D29CAD4A3CA54E7896EB1A9C7F6B4D,
	TrainTrack_Regenerate_m69F9C3D51A88D5FC844056750E4B233C06B427D6,
	TrainTrack_SetScale_mA57F06F4C3421BB4EB73D3311A661210F8FE7416,
	TrainTrack__ctor_mB9A2233319F4A20CD3719E44B1A65AF41FA2CB38,
	Pose__ctor_mB4BBD3D8EA8C633539F804F43FB086FB99C168E2,
	Pose__ctor_m8C52CA7D6BF59F3F82C09B76750DE7AF776F06C5,
	WindmillBladesController_get_IsMoving_m5B2902A40908BBD84B5478EDA121ED00DBDECF56,
	WindmillBladesController_set_IsMoving_mAFFB89C8CAA4C34CA91560EE2188E80B3B66897A,
	WindmillBladesController_Start_m68D68EA86272066DDC385AF8B8FDC2307AE6D4CA,
	WindmillBladesController_Update_m6D72C558EA8D91B66F86A4B977F32B78C6A19C9A,
	WindmillBladesController_SetMoveState_m7E9FF42447DFDD4DC710B07309C4ECB6A9027B3F,
	WindmillBladesController_LerpToSpeed_mEC59BAB1ADEBDA2D4051ADE4DF879F255B80C50A,
	WindmillBladesController_PlaySoundDelayed_m5C9465C96F6157B1A20D4F7F5A7D4F307D10D0FA,
	WindmillBladesController_PlaySound_mBFB8F38297A3551225D2E57F4285B58E9EF1EA63,
	WindmillBladesController__ctor_m9ACCD7787AFD75BF05A2D1282EAC87C3E8EFAF50,
	U3CLerpToSpeedU3Ed__17__ctor_mC566935DE291006EA7DBEFE0365E202472FD9E7B,
	U3CLerpToSpeedU3Ed__17_System_IDisposable_Dispose_m47D668CCF11A5E20E41EE17981808A194A891934,
	U3CLerpToSpeedU3Ed__17_MoveNext_mEA106649270155724FF7B4F3557200EB0C46C649,
	U3CLerpToSpeedU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0BCE854267BF1FBEBD932AD293ECE426245E39C3,
	U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_Reset_mDB45CD8EC190F294BD562D4090ADB533E5EBF9F7,
	U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_get_Current_m81542027D8F2618D5B750EF7344E51128D3A6EA3,
	U3CPlaySoundDelayedU3Ed__18__ctor_m43BFCD0BBEAFA46AF3EC55B65A2F55BF43541166,
	U3CPlaySoundDelayedU3Ed__18_System_IDisposable_Dispose_m3978D8C006709205406E26F305AAD051EDA0CB61,
	U3CPlaySoundDelayedU3Ed__18_MoveNext_m5CE5E650B1870D226841E3D4EA3A269EC73C7493,
	U3CPlaySoundDelayedU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DE8A8951E14A7AB0C941F512740278C4DF7841A,
	U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_Reset_mD0C04D5F54AAD444CE77C15B853BE79F5504160F,
	U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_get_Current_mC1F5FCA59AD560DFD85357BAC5AD0581AE393182,
	WindmillController_Awake_m819B04397F7D5A549DB64C972D9B35A0B54DCA77,
	WindmillController_OnEnable_mD0EBB3C707F79AB7CFB88EE7CE33355A24311BD0,
	WindmillController_OnDisable_mDAF50F89AB116A19F75AD91C088CF0B58F441365,
	WindmillController_StartStopStateChanged_m662296A93D8F31FC2608D8C3E89D1E3714B9561F,
	WindmillController_Update_mA8A7025A970E66000E92C858A97B07D1A73650C2,
	WindmillController__ctor_m2AE15DFF314EC3FE467821FB5F2EB1EE0F19EABC,
	OVROverlaySample_Start_m4266C95062A3A7F9DB99739F13F1767E8E135CDC,
	OVROverlaySample_Update_mE6B42EE1FD90945F73BB8A1E71C98D6FE45F8BAB,
	OVROverlaySample_ActivateWorldGeo_mE88CC5B6F95FF940DF64CC3E30A163299A0028AB,
	OVROverlaySample_ActivateOVROverlay_m0D8B1F2CE350FC1D6530827588176FED1E8B0E52,
	OVROverlaySample_ActivateNone_mA8A0AC683DA78AACC6DBB04E1BFCCAA41CC83D66,
	OVROverlaySample_TriggerLoad_mA4868A61FDAFE908F1178A852FAAC4DA4A614B42,
	OVROverlaySample_WaitforOVROverlay_mE2A11AA7618CAF92461A97CFFF9BF0163B14D463,
	OVROverlaySample_TriggerUnload_m6B1496A0AF1064FF3E131ED27D4BE41916FEAACA,
	OVROverlaySample_CameraAndRenderTargetSetup_mF5D8804C1E12AE7E92207ACE4AE38C9C4B12005B,
	OVROverlaySample_SimulateLevelLoad_m5792B7A4F133CB3538E9A9CF021C1EED354E47C4,
	OVROverlaySample_ClearObjects_m0B71842496007E962955E65310B2A4B15653E446,
	OVROverlaySample_RadioPressed_m67B72653B224FFE4E86C34D625F9B442D32BD19F,
	OVROverlaySample__ctor_mB44CEBA69BEAF1A6A58A099C565D834246303FEC,
	OVROverlaySample_U3CStartU3Eb__24_0_mBD67EE6633B4F08B23C48B5757BF037F011FA444,
	OVROverlaySample_U3CStartU3Eb__24_1_m9C707473B8AD3A3E9A2A5DE4CBB97FE7067691B9,
	OVROverlaySample_U3CStartU3Eb__24_2_m4676B4B0549094AF080DCCCC880399B388366734,
	U3CWaitforOVROverlayU3Ed__30__ctor_m2AB6E29E1D1F5472C16F001263DCA536B3140AB4,
	U3CWaitforOVROverlayU3Ed__30_System_IDisposable_Dispose_m1650460A7C7917454B04CB2BE659585291CBEA40,
	U3CWaitforOVROverlayU3Ed__30_MoveNext_mEC25CDDFCBF7B22EEB0CD01AE64AF805A2A11534,
	U3CWaitforOVROverlayU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m615D0B2569205ABB59D00AC9BA50160C1554A6F2,
	U3CWaitforOVROverlayU3Ed__30_System_Collections_IEnumerator_Reset_mEEB99599B3CF4FC0F90F891156B2FDE7157003F3,
	U3CWaitforOVROverlayU3Ed__30_System_Collections_IEnumerator_get_Current_mD516520BDEAB4043256A81E49B7366A842120F09,
	Hand_Awake_m8481AE5F78D80F6D46DEB65B2B9D0182A56DF09F,
	Hand_Start_m9705EEDEA171DF9AF0D8E107F3E1D42EF11BC79B,
	Hand_OnDestroy_mD3C5A20383810964D0D12F41E40C185DE7AAA046,
	Hand_Update_mB561E1D8A8122B9C20C5C6A00B9A43F952A09F2A,
	Hand_UpdateCapTouchStates_mD04F599C0EB3A207AE420014E51B65DA66F5A49E,
	Hand_LateUpdate_mA7932BE84EA2D5A09A4637502E4E52CA4321B8E3,
	Hand_OnInputFocusLost_m8C6FE8A97BA23153CEFD5D8AB561781E43AB6E02,
	Hand_OnInputFocusAcquired_m57F1BE51C79CB9B267C1866F851C75E6B848FA1C,
	Hand_InputValueRateChange_m76AA4719D87E5D5C3BC14DBE98B145551F685784,
	Hand_UpdateAnimStates_m513AAE4141F73A2CF86B65C1E5C88804AC288D6E,
	Hand_CollisionEnable_m23E8B46BA537F57146805D86CE15089DB121554E,
	Hand__ctor_m7990A68D0B224E873A13E2ABFF28659D667951AE,
	U3CU3Ec__cctor_m722784DF370910842E77B13ABF29607D0DA4DA74,
	U3CU3Ec__ctor_m29502B502F048DD5DE6F011936D3ACF3E92D116E,
	U3CU3Ec_U3CStartU3Eb__28_0_m982333D324DA5B0E763CCC459B17E62E62201591,
	HandPose_get_AllowPointing_mC5B4C9CDBFCF4883AC67C1637C2D1FA5C5553D3D,
	HandPose_get_AllowThumbsUp_mAE1D1913C2FEE204F4A74A8E40135873C9DB6615,
	HandPose_get_PoseId_m676CB369677442E624811201F99B12E4F5BB8F8E,
	HandPose__ctor_mB1A903B2059D5FB6676ACBD06E475480BBC767C9,
	TouchController_Update_m19376C0269F927CC0CFD041646CEA78E2748D6C1,
	TouchController_OnInputFocusLost_m09A7F448B045C56328B5E92B8DE149BA7ADA065A,
	TouchController_OnInputFocusAcquired_m7B23CB3B2CAD8C70AE1D8711D16F06F3E9ADA4A9,
	TouchController__ctor_mBE4D0026D782C0768D1B286B617197245B28D9C6,
	AvatarLogger_Log_mD82AB03540DFED158D2A47B7234DA2E82F83641A,
	AvatarLogger_Log_mB15C4AE1EBEE9DCDFBE0E0582B56CC2A57B18568,
	AvatarLogger_LogWarning_m4D65766C75422BD60E328AE048685147FBC8235A,
	AvatarLogger_LogError_m228ECB3375BEABA9539FB9A9C3946537D36E9991,
	AvatarLogger_LogError_m5C5E5B23A65ECCA310F45407126FE8CDE7F300B8,
	CAPI_ovrAvatar_InitializeAndroidUnity_mC3AFCAAC97FB003250BE0CB0DEE4E38598698655,
	CAPI_Initialize_mCFE421CB2BBBD32A30DBAC812779F11DB9C789A9,
	CAPI_Shutdown_m5213FFC230918B7C139643CEBEE90F4B54D5EC85,
	CAPI_ovrAvatar_Shutdown_m641D8D674098A24109719418021C44EB43BDF9B6,
	CAPI_ovrAvatarMessage_Pop_mB9AFAFA67F16C9A90444772AC3D999C137B36FC5,
	CAPI_ovrAvatarMessage_GetType_m300CECDD197F9D8CF189F992685F2985ACD0230C,
	CAPI_ovrAvatarMessage_GetAvatarSpecification_mF47B0AB5074AAA64F9B1EEF29BAFF0BEA81FEB29,
	CAPI_ovrAvatarMessage_GetAvatarSpecification_Native_m3BBB41279950D874A51A87D4432E0995A25F1A63,
	CAPI_ovrAvatarMessage_GetAssetLoaded_m16C066A389042915A5FD37CB1EF9BBE00EA54F30,
	CAPI_ovrAvatarMessage_GetAssetLoaded_Native_mA44E113C8F42B99305017AFDC5FEA1B2877B4746,
	CAPI_ovrAvatarMessage_Free_m6C11CF04C0C33DE6956DF293F664401140918F83,
	CAPI_ovrAvatarSpecificationRequest_Create_m4922625DE77E33F591119E7840C5C8F553C5F02F,
	CAPI_ovrAvatarSpecificationRequest_Destroy_m22BB42A3C3D22344241058550A686F6D2D756708,
	CAPI_ovrAvatarSpecificationRequest_SetCombineMeshes_m4506CA79C68CAFA97627F706EEE2E88E778A1207,
	CAPI_ovrAvatarSpecificationRequest_SetLookAndFeelVersion_m3E892769BC7318EB5F4735A1538EF9FCCBDCB0A1,
	CAPI_ovrAvatarSpecificationRequest_SetLevelOfDetail_mEE3431DC85D80B5560C6C5F323D3F4B33B3F2F0D,
	CAPI_ovrAvatar_RequestAvatarSpecification_m8A6F491E57EB844E56AC8227EC00C3623127B906,
	CAPI_ovrAvatar_RequestAvatarSpecificationFromSpecRequest_m854EFCB6926682E600936B5092A6B271479A45FC,
	CAPI_ovrAvatarSpecificationRequest_SetFallbackLookAndFeelVersion_m965D436E837C7FA927AC5C95BD61D45670D40C34,
	CAPI_ovrAvatarSpecificationRequest_SetExpressiveFlag_m8AD558362CA25158047625D276CF0BE1518DB1CB,
	CAPI_ovrAvatar_Create_mFF4CDAC619F6B0F5DF351B7BF7A91A23DEC5DC2F,
	CAPI_ovrAvatar_Destroy_m6279128A1650962A75521A3D767A5114481262B9,
	CAPI_ovrAvatarPose_UpdateBody_m5451FF570370967D2CA5CF5A2FD4AD7B387002A2,
	CAPI_ovrAvatarPose_UpdateVoiceVisualization_m83B3B8C7786ECDC63592A5AC61B009BBD07BF8A0,
	CAPI_ovrAvatarPose_UpdateVoiceVisualization_Native_m86657348A18C3D4DE40B30EE8AAA19414CCB758A,
	CAPI_ovrAvatarPose_UpdateHands_m11125DF72FFEA7B81DC9423AB489E897BA98AEC7,
	CAPI_ovrAvatarPose_UpdateHandsWithType_m47D2DD93341719D6315370F0F66FAD3CE3FAC079,
	CAPI_ovrAvatarPose_Finalize_m894288410291C8D1B0FF176AC7D7A03803614444,
	CAPI_ovrAvatar_SetLeftControllerVisibility_m8E28B5C24659C6746F4107B0107242E8B746BB77,
	CAPI_ovrAvatar_SetRightControllerVisibility_m305EE4BE5E299ED15646AE6DF3DE5DCE857F8032,
	CAPI_ovrAvatar_SetLeftHandVisibility_m74D741849CE137EF10E74309DF4604C5EF537B45,
	CAPI_ovrAvatar_SetRightHandVisibility_m44DA3611F78B67D6939891D3A5D9E43E866D8611,
	CAPI_ovrAvatarComponent_Count_m83DAD58C3C43AB1A1F8C22A35BC338444567787F,
	CAPI_ovrAvatarComponent_Get_m856C62C27353C0E8048F8F016A5CB8CB881BA22C,
	CAPI_ovrAvatarComponent_Get_mA52630A153E9A4EE8D01FEE7046EE90F5948066C,
	CAPI_ovrAvatarComponent_Get_Native_mCA8B12981816470E2C357D5F51C61AB713F4164D,
	CAPI_ovrAvatarPose_GetBaseComponent_mFB79C5989BA57CA1165701730AFBC929D27833D9,
	CAPI_ovrAvatarPose_GetBaseComponent_Native_m9DBDAF1292E05A652CD67C0277FA71B0EB4E26C9,
	NULL,
	CAPI_ovrAvatarPose_GetBodyComponent_mEDD41CF57AF83FCBF9E79B025743106444A7DF96,
	CAPI_ovrAvatarPose_GetBodyComponent_Native_m3CD40DE7FC868648986D9DF7F041F8920E57515D,
	CAPI_ovrAvatarPose_GetLeftControllerComponent_m5E9AC67844EF05B994D699B554CCA1DCBD489BEB,
	CAPI_ovrAvatarPose_GetLeftControllerComponent_Native_m4789DF168842A0C009153634F763A720302C7F76,
	CAPI_ovrAvatarPose_GetRightControllerComponent_mCB3AB6B9A1DE307070973A8C91B764B67DAEDB27,
	CAPI_ovrAvatarPose_GetRightControllerComponent_Native_mE2471D94A0BCB0D26ABFEC8E72074AF277558ED9,
	CAPI_ovrAvatarPose_GetLeftHandComponent_m56C5861D621E221D3EA6E0E4E5105851433B556C,
	CAPI_ovrAvatarPose_GetLeftHandComponent_Native_mB5EC966B7623A7E46EB6D3AD85D4823A43300CDE,
	CAPI_ovrAvatarPose_GetRightHandComponent_m65EC0025B2F955F0F1288F5DB2CF4A74127EB792,
	CAPI_ovrAvatarPose_GetRightHandComponent_Native_mC3139C0C1DDF6EC0D160E18205790D4F6B70AFA4,
	CAPI_ovrAvatarAsset_BeginLoading_m412A64C9C59E2F0EBCFE02927961BA9D4CB901AF,
	CAPI_ovrAvatarAsset_BeginLoadingLOD_m5F2AC7457EA9F2F1842ADA810250ABF92E51DED2,
	CAPI_ovrAvatarAsset_GetType_m0D9E2212F900FB45AA11875EE3C48E7184BE0947,
	CAPI_ovrAvatarAsset_GetMeshData_m7787644F34AC7F6596CD317AB4147235824C0E3D,
	CAPI_ovrAvatarAsset_GetCombinedMeshData_m1F7138A785791EE1610F61CA3D62239EAA76C707,
	CAPI_ovrAvatarAsset_GetCombinedMeshData_Native_m1DED67A93FC3B775CC3935B87709A01302F47DC3,
	CAPI_ovrAvatarAsset_GetMeshData_Native_m68F79C0D03B495C23B201641F4FD26ABB873E78C,
	CAPI_ovrAvatarAsset_GetMeshBlendShapeCount_m5AE3A4FA1113F89436DFA080FB319A0EF529B032,
	CAPI_ovrAvatarAsset_GetMeshBlendShapeName_mEA5EBB824098910900EC38EDAE6EE2E68DE5B2E5,
	CAPI_ovrAvatarAsset_GetSubmeshCount_mA73ED35AC9BE04E6B9988AB8F305C4ACD6684A11,
	CAPI_ovrAvatarAsset_GetSubmeshLastIndex_m715D60734E002A939C148A4354E4E46178A48AD6,
	CAPI_ovrAvatarAsset_GetMeshBlendShapeVertices_mA17001F2BD4CA9DECE4CF348E7EB31266C4F1AC0,
	CAPI_ovrAvatarAsset_GetAvatar_mE78CF7692CF3E6EAF03B065480FA89A23600C58E,
	CAPI_ovrAvatarAsset_GetCombinedMeshIDs_mA167C983CFB41E0A85A84BD6711C35FEE9D9DC2B,
	CAPI_ovrAvatarAsset_GetCombinedMeshIDs_Native_m3CC1814E3DE7FA14C1A711B36CF20525FAD5DF9E,
	CAPI_ovrAvatar_GetCombinedMeshAlphaData_m21EBC47B8A242FAFF5595F97DA1D7B68923FF837,
	CAPI_ovrAvatar_GetCombinedMeshAlphaData_Native_mCF90F067F0A3C586D8A3ED5F60F89B957E303CB1,
	CAPI_ovrAvatarAsset_GetTextureData_mAAB3144F5EE4FF98B35828798D84B1B5EB65B526,
	CAPI_ovrAvatarAsset_GetTextureData_Native_m923758A280773BE3AB3C1FEE8DA33194EA2011EC,
	CAPI_ovrAvatarAsset_GetMaterialData_Native_m151A2936E08570427FFF09A12FCD3E31274CDE78,
	CAPI_ovrAvatarAsset_GetMaterialState_m180F9D6F3C0CE06C402DCDCD10CA5C7557D0E963,
	CAPI_ovrAvatarRenderPart_GetType_m73C2BBEA895BA843BF7FA5CA68150EADDF70229A,
	CAPI_ovrAvatarRenderPart_GetSkinnedMeshRender_mE35AE01E81CB778BBAE15AC9D104086A4A2D9E53,
	CAPI_ovrAvatarRenderPart_GetSkinnedMeshRender_Native_m9774E96A201192F53EF698299568523E5A1B8862,
	CAPI_ovrAvatarSkinnedMeshRender_GetTransform_m35FCA048E97B96A46ACE9A123391B59AEA77F508,
	CAPI_ovrAvatarSkinnedMeshRenderPBS_GetTransform_mAB0C72AA6EC962A5EF82DD03546DF0AE870698B4,
	CAPI_ovrAvatarSkinnedMeshRenderPBSV2_GetTransform_m410DD5215D3D73B227B855451463068DEE3FF098,
	CAPI_ovrAvatarSkinnedMeshRender_GetVisibilityMask_m93046DB38EC0F5544C0DC06812A59A3E502BCE5F,
	CAPI_ovrAvatarSkinnedMeshRender_MaterialStateChanged_mCFFA92521C1E4CA16D6EAED8459C77B7FEC352CF,
	CAPI_ovrAvatarSkinnedMeshRenderPBSV2_MaterialStateChanged_m4E6D330334A76173F99B67A94A0ED70CA1739BE3,
	CAPI_ovrAvatarSkinnedMeshRenderPBS_GetVisibilityMask_m51CF15349045242C26C0518EA4F8AEA73D480028,
	CAPI_ovrAvatarSkinnedMeshRenderPBSV2_GetVisibilityMask_mB20C93D31AB725CDA7F9020977A1613B21F84CF2,
	CAPI_ovrAvatarSkinnedMeshRender_GetMaterialState_mE60F44C8BCE3F7E6AD540B2B8847BE65BC76A8EA,
	CAPI_ovrAvatarSkinnedMeshRenderPBSV2_GetPBSMaterialState_m1DF341E631EE792107E84C25645596523D4BAF12,
	CAPI_ovrAvatar_GetExpressiveParameters_mE5A33D5D5625B3F07CC1884886A1C1198C8301F6,
	CAPI_ovrAvatarSkinnedMeshRender_GetDirtyJoints_m015B56A89B28DF47A1FDC94A30EAF1235536F235,
	CAPI_ovrAvatarSkinnedMeshRenderPBS_GetDirtyJoints_m2C663BD3285C21DA9C672DFB61949630DB050237,
	CAPI_ovrAvatarSkinnedMeshRenderPBSV2_GetDirtyJoints_m01644BC1D48FDAC32F9754A56BE34BCF348E7026,
	CAPI_ovrAvatarSkinnedMeshRender_GetJointTransform_m649E628F1A8FFFFD0842F53B7E834F4371DE334C,
	CAPI_ovrAvatar_SetActionUnitOnsetSpeed_m1C970DE451D337915A13251B250D636261DE7106,
	CAPI_ovrAvatar_SetActionUnitFalloffSpeed_m1C6645B4C0628E9E468A3E319BEC2CC0C0D79039,
	CAPI_ovrAvatar_SetVisemeMultiplier_m925D7D67D82CB100C4081B767AF603A3A99D2EE5,
	CAPI_ovrAvatarSkinnedMeshRenderPBS_GetJointTransform_mB3D9A366D251BEEF09448E6055918657679924C3,
	CAPI_ovrAvatarSkinnedMeshRenderPBSV2_GetJointTransform_m2D30C3004C92C1A25A9E825F11A7C7465AB40CD6,
	CAPI_ovrAvatarSkinnedMeshRenderPBS_GetAlbedoTextureAssetID_m3A4E055B2D93CE5EA7333C2A4C64AD40032BB83B,
	CAPI_ovrAvatarSkinnedMeshRenderPBS_GetSurfaceTextureAssetID_m5934F61E6459781AB3BCAE08CA103E17D45ED240,
	CAPI_ovrAvatarRenderPart_GetSkinnedMeshRenderPBS_mCF50C6A154A9366C0A82DFE3BFC8E3E7FB88A68F,
	CAPI_ovrAvatarRenderPart_GetSkinnedMeshRenderPBS_Native_m54E4803DEF35DE27C47D6E24604429F65B415AF2,
	CAPI_ovrAvatarRenderPart_GetSkinnedMeshRenderPBSV2_m45D0F9A059342C06596F9993D7422501E3089BF9,
	CAPI_ovrAvatarRenderPart_GetSkinnedMeshRenderPBSV2_Native_m109C808BBB07AEAAFF04FDB6B204D886943721F2,
	CAPI_ovrAvatarSkinnedMeshRender_GetBlendShapeParams_m6BEF2EDE46D67787CBADF5204AB66F160A96BF95,
	CAPI_ovrAvatarSkinnedMeshRender_GetBlendShapeParams_Native_m0849E1CB05483EFC139298EE8950F40E99EB5C87,
	CAPI_ovrAvatarRenderPart_GetProjectorRender_mE6A8741B119852271D39374B25D29418756D01A4,
	CAPI_ovrAvatar_GetBodyPBSMaterialStates_mD93F87EC0B218CE87531BCAE3EAE330CA7A54DD2,
	CAPI_ovrAvatar_GetBodyPBSMaterialStates_Native_m3F059EC2600CA680759A0530230BA61C6282F456,
	CAPI_ovrAvatarRenderPart_GetProjectorRender_Native_mCA06C0BC0D537583DFF90E3AD26FE9F8E2169FB8,
	CAPI_ovrAvatar_GetReferencedAssetCount_m4FADDE26FABA9C1AD3D832AB71D3947A7C6B5D6C,
	CAPI_ovrAvatar_GetReferencedAsset_m946755160D5D17674A8B1EABD526537C17D58261,
	CAPI_ovrAvatar_SetLeftHandGesture_mFE13D368CE500F7C6112B30F1E8D119C42045569,
	CAPI_ovrAvatar_SetRightHandGesture_m77AFC95EE0D2BB6329AB8D15D48545A0617EC57D,
	CAPI_ovrAvatar_SetLeftHandCustomGesture_m2304B758B028F976631B41F42A960CD93608618F,
	CAPI_ovrAvatar_SetRightHandCustomGesture_m2D456833A8A31FB8257581B33EAD3125508807C1,
	CAPI_ovrAvatar_UpdatePoseFromPacket_m843F6ACC15BAE43CAD6C8369129BFDB234130817,
	CAPI_ovrAvatarPacket_BeginRecording_m563474205D13D2C81DF0EBA0E4FA859A8B6545CB,
	CAPI_ovrAvatarPacket_EndRecording_mC19C294C14694BC8FE5DD778538C32B35F6EFFF6,
	CAPI_ovrAvatarPacket_GetSize_m8911AECB2FF7CAB7B521CE22BB8A3AF6F189691A,
	CAPI_ovrAvatarPacket_GetDurationSeconds_mAF67CD5A9731348400DE5D5B0D142C9B4B80101B,
	CAPI_ovrAvatarPacket_Free_mDD18D124EC797E80A397A6309196A34B69AF38F7,
	CAPI_ovrAvatarPacket_Write_m3FA7BCDE2E9C17A173F4546F292B6F500E373F90,
	CAPI_ovrAvatarPacket_Read_mC585E78E3DDE9F704F62A80A173D371BCBD01F6A,
	CAPI_ovrAvatar_SetInternalForceASTCTextures_m179DB4365DE27697CEB2C1CE7DCF681470B8AE48,
	CAPI_ovrAvatar_SetForceASTCTextures_m740DAB9EE4F7A772EE075650E8C6E81754EA5B56,
	CAPI_ovrAvatar_OverrideExpressiveLogic_m5DC0F50ED696C8408D5550CAA58D852E1F06D0F6,
	CAPI_ovrAvatar_OverrideExpressiveLogic_Native_mE7C62285999472B0BB3E19487049B9D6447FD718,
	CAPI_ovrAvatar_SetVisemes_m9AEE1056FB54DAA7458CEB6F1E62DADE3BF309C7,
	CAPI_ovrAvatar_SetVisemes_Native_mAA20508E980D4A1FA17AC9CA3B9CDA46E92F4D16,
	CAPI_ovrAvatar_UpdateWorldTransform_m6295F3809955F0BE82959AA6E1B02947ABBE6F26,
	CAPI_ovrAvatar_UpdateGazeTargets_m2427C132C152AC9F96755A4CB55933B4FCA3B173,
	CAPI_ovrAvatar_UpdateGazeTargets_Native_mDE1B61A465C0B35D56ADD7E17A76534A2D4BE74E,
	CAPI_ovrAvatar_RemoveGazeTargets_m33FE86B4F58B30F059B05DE3F2B8CDD6FEFA73D8,
	CAPI_ovrAvatar_UpdateLights_mCDA09C2977A0E65A2A47850E146552E742B5FAFD,
	CAPI_ovrAvatar_UpdateLights_Native_m551EA55815F41323BAAB0A4033E8DAD2677547D9,
	CAPI_ovrAvatar_RemoveLights_m0F5E1BB5D7EC370C7464E9B28068310BFF7964F0,
	CAPI_LoggingCallback_mE9CB96D4CEB343BBBBAA0ACD39A6281C11D4110B,
	CAPI_ovrAvatar_RegisterLoggingCallback_mC3CB369A24FBB4E09D8609FC0FA9822788DFB371,
	CAPI_ovrAvatar_SetLoggingLevel_m9E569FD2FF24290A14C61169D4D25352C2E0C2EA,
	CAPI_ovrAvatar_GetDebugTransforms_Native_mEC12C23FCDB91C82B2C82CA6AF845F02B8C3D5F0,
	CAPI_ovrAvatar_GetDebugLines_Native_mCE0C0453BEC741E27AFD02BD843DF51F0143E5EE,
	CAPI_ovrAvatar_DrawDebugLines_mE5E484E9686916F4B31C49FF4E5633C26C845D92,
	CAPI_ovrAvatar_SetDebugDrawContext_mC4B961A7059F3574401E7D332B79A4DFBB73FE95,
	CAPI_SendEvent_m02C69575F737F9751E1143A8041F884E3AA7B123,
	CAPI__ovrp_GetVersion_m4EBDC24710D40A75EC52A5C970A36F0810F0EE40,
	CAPI_ovrp_GetVersion_mA0A03FA3CA5C3C3C8F42CE0B889D099B5224FB70,
	CAPI__ctor_mBE1FA2A63058548F6EDD1569CFCC07A75C778512,
	CAPI__cctor_mE274F449E306322E410B64038426400CCB646766,
	LoggingDelegate__ctor_mC675B66814B9B9686DB64204977D33F783791245,
	LoggingDelegate_Invoke_m33D2B930F9EBF07BC2FCA8E9C05C053E011394CD,
	LoggingDelegate_BeginInvoke_mC00109CB6D9B7D2B923275339A8001FABB3FDC06,
	LoggingDelegate_EndInvoke_mA9ACBF14A04F2B84FD884DB9F00DDE480A0119E3,
	OVRP_1_30_0_ovrp_SendEvent2_mC8B91A032C7E71967F8841B996384EA3150F943E,
	OVRP_1_30_0__cctor_m3418A2000FF05F393EE1604666B66467E83787E9,
	ObiCharacter_Start_m9AF9959BA6BB669EC44CC6A19F2C4921A8D3EC12,
	ObiCharacter_Move_mE36529E342431BC2E3F82ED9221503AF7F957934,
	ObiCharacter_ScaleCapsuleForCrouching_mBEE0D314BF3D424DEC1B2D4249AC6F1F1B577A51,
	ObiCharacter_PreventStandingInLowHeadroom_mE8D5660997F99D5B260E7CCC9BF758FFF4FDFEDD,
	ObiCharacter_UpdateAnimator_m0133BC89517D199811A84D3308D99A39508A6163,
	ObiCharacter_HandleAirborneMovement_m2237FCB54D63F3C66E8DC444AE9485868A07E604,
	ObiCharacter_HandleGroundedMovement_m665A36CA6A0EAD5D0C62AA11737F2A6308EEEDC6,
	ObiCharacter_ApplyExtraTurnRotation_m0D9CC7D6882FDB7888330B062F69EE0FA0667BF5,
	ObiCharacter_OnAnimatorMove_mE0C7A78FB55C063DF34E2BC3FF6F84125B63AB6A,
	ObiCharacter_CheckGroundStatus_m21B58AEB9AB71B4D7885687B1C5979A9CB9CF45E,
	ObiCharacter__ctor_m16255C98358F19058CDB9991C96F04FE19EA7A93,
	SampleCharacterController_Start_mE5EAF1421DBAD9FFAC2D550E04332581CDFFE4BC,
	SampleCharacterController_FixedUpdate_m1AC87DDCCC039856B18C977E02E0CD2D80F1A524,
	SampleCharacterController__ctor_m8605568614106AD773DE856FE8AC1968C23A4262,
	ColorFromPhase_Awake_m8EE051CBD4CAF6406768C2D6941DCABCD6CFD2B9,
	ColorFromPhase_LateUpdate_m56F07E6A796032EB6DD06CE10C57090983DAA1AB,
	ColorFromPhase__ctor_mECC2D22AC69AC7C21BC64BF585AB14C65E7A2415,
	ColorFromVelocity_Awake_mBA50845A66FAE55EB4F4637C1BF190EE329FAEB5,
	ColorFromVelocity_OnEnable_m60CE9B091B8725925F60E867BE08F1F137E2AB9D,
	ColorFromVelocity_LateUpdate_m4A026CE0BCACBCA70F219D29A3EEFA7EDE33C2A3,
	ColorFromVelocity__ctor_mD26A1B2F148FC8BF6F57EDEA8179E0B2D8CD8FFC,
	ColorRandomizer_Start_mF174C12B77903483554BBCE5D4A4D82118DD4651,
	ColorRandomizer__ctor_mF1141F2D29657D595C863D186B46C6B3CD672A85,
	LookAroundCamera_Awake_mE90B4191F1288756CA34EEC0C21C1998251B0E8E,
	LookAroundCamera_LookAt_mEF29B91315C1AD0156E505C73EF9931CDD6E7718,
	LookAroundCamera_UpdateShot_m8C764716D01D701607A13430043C9E5B87A50597,
	LookAroundCamera_LateUpdate_m18FD061FDD268DE063DEC79BAD72F08432F1A7A8,
	LookAroundCamera__ctor_m52D5B9733D32E65A768D29868D4B07EB63988D12,
	CameraShot__ctor_mD9736DEA6A3F14DF47920666D00E1ABA5899E179,
	MoveAndRotate_Start_m81492AAA391819DF463B1781F3F647B8F50E8E1D,
	MoveAndRotate_FixedUpdate_mC9020498CC29B200215037EB0EA7508367CCD8BD,
	MoveAndRotate__ctor_mC46EA5D888532267ADC051A68F468CD7FD5D2130,
	Vector3andSpace__ctor_m5653715EE0EAB2271B055D2729CCF8046D2CE78B,
	OAVAShaderCompatibilitySO__ctor_mAA228997C3D85FC960519BA027203240D4666FED,
	ShaderProperty__ctor_m997A0C6F6C2D131D775B6A226E0ACDD2EE0418D4,
	TouchedTheFlame_TouchedTheFlameOuch_mFE81C59E0E5287A3CAA04B019DBD8CC6F1470101,
	TouchedTheFlame__ctor_m0CBCFA2771B2A381DC1C67233DC22559E28656BE,
	FireTrigger_FixedUpdate_mBCEE37C16BF24A28A54CF51E24B04600AF4A19FF,
	FireTrigger_Update_mD64612F52DEB2CF78148B944D09FEE88D6454857,
	FireTrigger_StartTrigger_mBB9BFB3C0051D4EE8FFEE09FC1DA9799C585090F,
	FireTrigger_TriggerUpdate_m1A8E5FEFFDABDDFE168EEDE4696065B581E8C2DA,
	FireTrigger_ProcessCollidedObject_mA9075C2917D7F435CF97BBEBB7FF054CB3608CCA,
	FireTrigger_OnDrawGizmos_mFFE3214519A5BEB4E056352AB36C1CBC16E84395,
	FireTrigger__ctor_mB2770FCA6AC51839D36D2503D0052C97840DFA45,
	FlameCollisionCallbacks_TryToTriggerCollisionEvents_mF9FC6337910EF35C16B540B56CF3422CB2492E3A,
	FlameCollisionCallbacks_TriggerCollisionEvents_mEF1AC1BBB87A64687E135BABAEABF9421ABA5AB1,
	FlameCollisionCallbacks_HasCallback_mED2E81D6527F9D0E60797F82781618678B4695A2,
	FlameCollisionCallbacks__ctor_mAADF8621641E35A5EE3FD114DC50EFD5D2917BB8,
	FlameEngine_get_instance_m232F83C18F4BF627FF52595F12EEBEEDC7BED039,
	FlameEngine_Awake_mB83AD523C7DF9DC72017C3D61FE21B9DC351FB3D,
	FlameEngine_Start_m2FC007786DF6696258A1B99A57F58048F88D3C60,
	FlameEngine_PauseFlames_mB0A734A1FB4E2441EC6FA51914F456B354E0D24A,
	FlameEngine_ResumeFlames_mBF39CD2758FA544479946BB41F626A8268EB6D13,
	FlameEngine_GetFireVFX_mE24A9AEDF3C29D56409654670F31584033AE287B,
	FlameEngine_GetFireVFX_m53705E8AF3FDEF1104DD5E6C5403109B97D367C0,
	FlameEngine_GetFireVFX_m69BA741535A64C23EB6383F80A8D4FB9B59B1B70,
	FlameEngine_GetCompatibleShaders_mF63E2716C709437A56D3904CA2D0DEC441340032,
	FlameEngine_GetCollisionCallbacks_mE751FB06B4E80AC17C32C3814ED3B8C43E3121E6,
	FlameEngine_MaskInstanceAndSpawnAPrefabIfNecessary_mD68F04E4A694A289E7D98E23E1283B116DEDDEFE,
	FlameEngine__ctor_mC09FE9AE4EDA5AB337C4742C54C18242C8F0D852,
	IgnisUnityTerrain_Start_m88AFB9A25BAE4C0339FE86579D00EA87280EF87D,
	IgnisUnityTerrain_UpdateTrees_mBE389708FD27AD2EF36286E47ECAC19ACA6151D2,
	IgnisUnityTerrain_OnApplicationQuit_m96822240263D3AFCA2E5A19F6A402454EFF99384,
	IgnisUnityTerrain__ctor_m78BAB8EFA3A0784197F2A8C6D9F02B22C63B8F57,
	VegetationStudioProTreeUnMasker_Update_m7C5E38F4C2C81583D8A5A236B279B829A689906D,
	VegetationStudioProTreeUnMasker_Unmask_m409D219E096A8B31DDB90A775F9CC6DFECC7D011,
	VegetationStudioProTreeUnMasker__ctor_m14BA2E2B1787670C66AD9BB8E16FDF02623C282C,
	WindRetrieve_Start_mD03B31BCFB340E75D2C07FECEBF1473F22DC13C6,
	WindRetrieve_Update_m0E8C775E0EAC5C4C5715EB8A09617FDF0A161ECC,
	WindRetrieve_OnUse_m5CB76729B104977E482C03905B587D34E58A2621,
	WindRetrieve_GetCurrentWindVelocity_mCA7856B1773745D48B1EBB3A3A4DB184F75C4BB7,
	WindRetrieve_SetupParticleSystem_mCFC2952D9F5CF86F4856C91423DC9BC9E9A501B6,
	WindRetrieve__ctor_m51453E6D716020DB7E56B64805648210B14051FF,
	DebugFlammableVFXBox_OnEnable_m2A97FA2CB574ADF2F7E72561084FD558CFEA2549,
	DebugFlammableVFXBox_Awake_m8F38894B51CD3F2B461EB6B587224F45276483E9,
	DebugFlammableVFXBox_Update_m4C4B6EF690528C5669F5357D3F6B5AFDD8F4D545,
	DebugFlammableVFXBox_SetMeshOnFire_mACC70574A1C2A3C06B286990EBD9508F4A7743D2,
	DebugFlammableVFXBox_SetBoxesOnFire_mD58E7A45CE8559D31C29B843F238E5D146B2BE27,
	DebugFlammableVFXBox_SetupFireConstants_mB205435CEFAEF77F77BF7D8B056B24E845BA9D20,
	DebugFlammableVFXBox_GenerateColliderFromMesh_m7D882D4D3F5208593475FF94CF2E66691076A89B,
	DebugFlammableVFXBox_OnDisable_mDC14E163FA38D1457074E6610BEB415F7D12805B,
	DebugFlammableVFXBox_Clean_mEA5DC66AA0B341289F8740567CA10E3D0F5EFB06,
	DebugFlammableVFXBox_OnDestroy_m5ABF1B4209B5562DEA57E7359FABBFC327F26991,
	DebugFlammableVFXBox__ctor_m56A59DF73ED4E23F407FA86A55A2BBE45EF1A372,
	FlameTriggerCallbacks_TriggerEvents_m1D14BDFEC8B93791C11629AB221986F72A8F5A95,
	FlameTriggerCallbacks__ctor_mBD0C4EE1F92B4C697C26A109E48D1F50BC5FC076,
	FlammableObject_Awake_m45C7971EA8DDC0680EDD3A46090FBE479CC6EBCA,
	FlammableObject_Start_m42C1FBD14CA817C984D85784DB65052304AEC2EB,
	FlammableObject_OnEnable_mE9BB40E9E8696B84455B8AF2B3822A91BE76F765,
	FlammableObject_FlammableOnStart_m7F775E152727CAD1220E50978D2183DF1D561CB4,
	FlammableObject_OnDisable_m27395D3C9343BE0C48E9D5CF13736B13C0C361C7,
	FlammableObject_Update_m69D6CC4DF6BE79E76F585E1CE7C84272A0674868,
	FlammableObject_SaveOriginalMaterialShaderProperties_mD8AE9957F20310D6A7A51C00E67F82BD827CB0AA,
	FlammableObject_ResetMaterialFromIgnis_mA78F56E637D438EBCA9DDDCA54CFAFEB9C56658E,
	FlammableObject_ResetObj_mC85841A0F6D78680D9ED5641EA9A8B22197E95EE,
	FlammableObject_SetOnFireFromCenter_m4894E67CA373F34B98D290EDFFAEC3CA88C64AAE,
	FlammableObject_TryToSetOnFire_mFBB99527ED1C876673D3210D86B53495C95F4B1D,
	FlammableObject_TryToSetOnFireIgniteProgressIncrease_m63C2B4944FA48F824743020C4093A0FCF4F0928E,
	FlammableObject_CountExtinguishTime_m56EF944C3DB03B7A9C185A79E5B0043F89D1C161,
	FlammableObject_IncrementalExtinguish_mAC7A854C82E30EB85F09CF3CD44B39A148C4DADD,
	FlammableObject_UpdateLights_m769B0B7FF81D40563012014EE070B5185F62CA3E,
	FlammableObject_UpdateShaders_mBAB5DC9D231FED6DC79861179EA3DF7156693800,
	FlammableObject_SetupShaders_m4DEF8C57CF77FDE5F81A22E810E1ACC23160D011,
	FlammableObject_SetupIgnisShader_mB664F34BC4F6EB4D879291C09752CCDB3C8FECAA,
	FlammableObject_KeywordsEnableCompatibleShaders_mB3495D8ADD368A113956CE189752052674B2A5EE,
	FlammableObject_GenerateColliderFromRenderer_m09FFD402BB2688EFBE619376A7F8152DB4FAF915,
	FlammableObject_CalculateApproxSize_mE2059C5D4707FE78A107F6C49A825AC93D80D5DE,
	FlammableObject_UpdateSupportedThirdPartyShaders_m8993F249B00A11DAB289DFF73E998DB6213AF6BC,
	FlammableObject_UpdateCompatibleShaders_m7B653977AACA388BA72F160BF8AFDDA30B63218D,
	FlammableObject_UpdateIgnisShader_mC1DB0891850339334A487D47E25E77395A4B43A6,
	FlammableObject_UpdateVFX_m99F99F9436AE0DFECAF8C92648E9F49285A6205B,
	FlammableObject_UpdateSFX_mE66C7252C15BC1E27903B826F67407A91AB32451,
	FlammableObject_CheckForRemove_m31CACD93DF6C9FE5C827F3F20AA1F162309CBC89,
	FlammableObject_InvokeCallbacks_m7ACD7BEE58C98D5E3BB645E2C1961076263B4A88,
	FlammableObject_SetupFireConstants_m4FD7FDF391DF216206D4FBB6C454BE98C851E9F9,
	FlammableObject_SetupSFX_m6B4C3D4E7E4789F812FD8928312992BD9CD8E822,
	FlammableObject_SetBoxesOnFire_m0FAC7AFE84B99C27286CA85D9A132E90B0281F42,
	FlammableObject_SetMeshOnFire_mF03C2122F3255F6ADABA698462B2DF0630045D3A,
	FlammableObject_Clamp0360Vector_m0A07E8258FE6DAB7B09C27ECB83F790786F87501,
	FlammableObject_Clamp0360_mFC0609E277E664BBA3579FA0C8E8AA068EAC7955,
	FlammableObject_hasBurnedOut_m234E66CC1CAE01CAF4E59454A2A631DAFB095CF6,
	FlammableObject_IsExtinguished_m5DC8F4CEAE06D50B2319565E694676EDF7D2AF62,
	FlammableObject_GetFireOrigin_m09A97E79B5F722AA946652E138414322DE6D6EF6,
	FlammableObject_GetPutOutRadius_mE7B990B7BEF3CA14E21CFB87C4F47AB2DBF2DB46,
	FlammableObject_GetPutOutCenter_m21606B2FBD804D2ED1E159EB2B0BCD44DC9C00EF,
	FlammableObject_GetCurrentIgnitionProgress_m892956237F5EF0291B0CCE09398C569271D3A376,
	FlammableObject_GetObjectApproxSize_m000F4CF12F5D5D96EAD83CE40A3E462B9F84A465,
	FlammableObject_OnDrawGizmosSelected_mE5283B21685FD5AB0929FE3603D4FF268036BAC0,
	FlammableObject_OnDestroy_m71B01388C0CD8E3031220AAB70568FBB38A4D096,
	FlammableObject__ctor_m19AA98D4F8219F10A428EC7D212EA47674BC8E79,
	U3CU3Ec__DisplayClass93_0__ctor_mEA21044435E4031CFC46DACBEE1D64B22DDC532F,
	U3CU3Ec__DisplayClass93_0_U3CSaveOriginalMaterialShaderPropertiesU3Eb__0_mF956C54131A3050A5E7401F85F5E938BBA5CFA37,
	U3CCountExtinguishTimeU3Ed__99__ctor_m4F83971CA9EB1399569059D5EB6702D6E34987D6,
	U3CCountExtinguishTimeU3Ed__99_System_IDisposable_Dispose_mCD81AF4B8263D699430454DFD2B7B0A9BC8DECB5,
	U3CCountExtinguishTimeU3Ed__99_MoveNext_m702D4BDA4D99ACE25937D87B58BA7152A58EBF73,
	U3CCountExtinguishTimeU3Ed__99_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3FB046241BBBF34C5AFE8FC6A2122C3D44F4DC2,
	U3CCountExtinguishTimeU3Ed__99_System_Collections_IEnumerator_Reset_m20C35A263EE2772A01687E0BB13AF22F49B63DD3,
	U3CCountExtinguishTimeU3Ed__99_System_Collections_IEnumerator_get_Current_m64793AB448EC8B3A4347CE70FC4C445A9BA4601B,
	NULL,
	ParticleExtinguish_Start_m59DE92F510A2E7547DDDE47632A027A6D0B92E5C,
	ParticleExtinguish_OnParticleCollision_mF63191C5C36BE863771817DBB3852EB4EC75BEE2,
	ParticleExtinguish__ctor_mED2BCD1315492224F7E7E3A810DEFA60F4522760,
	ParticleIgnite_Start_m0084E50FD65BB31D87BBA6F3B84F131D4E1229B3,
	ParticleIgnite_OnParticleCollision_mF8BC7A48F17F6A2C9483316126F21121DC493A1C,
	ParticleIgnite_RemoveFromCollidedAlready_m968F866681F10C528DF33C6D9C8260C294372F14,
	ParticleIgnite__ctor_m52214D921BD903D5BB9BDEF22538B0BE2ED1F617,
	U3CRemoveFromCollidedAlreadyU3Ed__6__ctor_mFDD28DF1ECC09EA61E51D594DC28D8DF42E9D2F3,
	U3CRemoveFromCollidedAlreadyU3Ed__6_System_IDisposable_Dispose_mE31A8FF4F0ACC0AFA00D055D269E0399BE2F0DAF,
	U3CRemoveFromCollidedAlreadyU3Ed__6_MoveNext_mE7BD4427C78BB8664CBA314342BE5549655BA55B,
	U3CRemoveFromCollidedAlreadyU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA64809CA13AFF7EB6703583ED7479A588D55896C,
	U3CRemoveFromCollidedAlreadyU3Ed__6_System_Collections_IEnumerator_Reset_mA87BDB84E77AEE03978D11B43AC960D5418A5A43,
	U3CRemoveFromCollidedAlreadyU3Ed__6_System_Collections_IEnumerator_get_Current_m061736B34E6F21EDAC65B47E34F6B7E1306F357C,
	RaycastExtinguish_Start_m7B4FC13CC0CA04F43ADDF2AA3FBF6C8822E1BB6C,
	RaycastExtinguish_CastRayCastExtinguish_m0204E6BA7DC82FF30A1E80981B942F2307BE95F1,
	RaycastExtinguish_OnDrawGizmos_m8523DCD9EC1007041D41C3D242D65C23BD9862DC,
	RaycastExtinguish__ctor_m9F60D3FDFD023FC0CF5D2A6FFB754CE083D759FF,
	RaycastIgnite_Start_mA01C95A9938A2B7E171B784A38E691997BB69C3A,
	RaycastIgnite_CastRayCastIgnite_mBA13CA9FF2F2FFCFB5E14B23F4B8C3FF3B2203D8,
	RaycastIgnite_OnDrawGizmos_m3B20CFC26A8F117F13D32FC7AB8CDEE45F2AE2E7,
	RaycastIgnite__ctor_m9DEFA75DA71FE93D54053097AE41FC9F30432D56,
	SimpleInteractWithFire_OnCollisionWithFire_mA0474A5EF2C55BB0C93365FD093781B0F8DAF8C1,
	SimpleInteractWithFire__ctor_m1DEA5D394218C906CCEBBA2F5AB4B643D7AAA1F5,
	SphereExtinguish_Start_mA72ADA696F1334A4552CF11F0D89CAA8DEAF047F,
	SphereExtinguish_SphereExtinguishCast_m3ECD64F5568A6165E4B991A206E3D49D6EB932D8,
	SphereExtinguish_OnDrawGizmos_mCC133325FD07E391D1F428008B09D51255F4A425,
	SphereExtinguish__ctor_m61D7D03D4ABDFB2978FCFBFD21640753AF0D955D,
	SphereIgnite_Start_m14F1D8166457FEF9441E333B5E6D480E9549FE39,
	SphereIgnite_SphereIgniteCast_m1F89159B934161E66090917A5C95E048B9BD704E,
	SphereIgnite_OnDrawGizmos_m12D675F95799C439B55698B6D28022171056EC71,
	SphereIgnite__ctor_m5D7AF51286E6D2859050FD4BEA278F669100F164,
	Ack__ctor_mBF0834E0C661F166666626C9EC86CE21A2804B1D,
	Ack_Invoke_m89254CC3600C1650B96AE76EFD3768063D77DFD7,
	Ack_ToString_m16FCC4670E1B9E865E24EECDA3E2AF86FD28D93E,
	SocketIOEvent_get_name_m272C6F0E0DEF3D858CEDB3CDF477998F04549424,
	SocketIOEvent_set_name_m8BDBF4BC266B74318C1A96E812A0BE9B459F0D9C,
	SocketIOEvent_get_data_m9CB85C9ACC0C2E158E9A5CCFFFEB8BC29FC8CA3F,
	SocketIOEvent_set_data_m839254B4D3783F185A6E3C8C14EAE51E8D0DC37E,
	SocketIOEvent__ctor_mE6C1131BC31A5D69E2119E7531D6DF78D2CAA885,
	SocketIOEvent__ctor_m7E06764CA0D91050034BB07C9BFD70FD174121F0,
	Decoder_Decode_mDFEB1B2336FAC59EA8DB38524052CF14C05A7B6D,
	Decoder__ctor_m19A6D5F428AED1E51DBF942839A255389890EE7D,
	Encoder_Encode_m4DA14E756547AD1F2E0E2C36875A77F8B9193E03,
	Encoder__ctor_m729ABCFE68F9DBADE61E68466382CF4BCD97B38B,
	Parser_Parse_m667792F77B19DBDC740C8B137DD0B067FA81A0A0,
	Parser_ParseData_mA7F9344DD12AF4AFCF42147B34FF6B8B36806A90,
	Parser__ctor_mB2E752671FC0E2DF3DB9A45E5E1D6A3F086D722F,
	SocketOpenData__ctor_mA8D183CCF76745B81A81ABA0E588F3BF5E0F005F,
	SocketIOComponent_get_socket_m367C734B9ACBA229C724C1D4240E555D4664F169,
	SocketIOComponent_get_sid_mA875D0C9A2D45CC4B14CE73997AF1867D43F09B7,
	SocketIOComponent_set_sid_m42AAE7199C4F652EB567C24FFB5C65B13281D911,
	SocketIOComponent_get_IsConnected_m1D1C2E6B5A577639AE5EBA14B39E67BB2D644264,
	SocketIOComponent_IsWebSocketConnected_mF53DB7E4707B517C639F4C2C74D01F2C7C9F1A98,
	SocketIOComponent_IsWebSocketConnected_mD65E312F201968123CA302BFB556828518C174F3,
	SocketIOComponent_DebugLog_m3063F4764FD156A7590B4C89BF5E3D8814492185,
	SocketIOComponent_Awake_mBFDCEE46A46027FD2FC07956389711158B55240C,
	SocketIOComponent_Init_m40A774ED935DEA19B992F158A5335EA78016497C,
	SocketIOComponent_Update_mDE380B53C01AB137E140ED9A7430663E43B08758,
	SocketIOComponent_OnDestroy_m627F9AEAB5EB8A9B585601EB511BA942A66E91A6,
	SocketIOComponent_OnApplicationQuit_m58B848E579E407B8A3389D8A48EE6FE9C25B9BFA,
	SocketIOComponent_Connect_mE2788920BFE2928F200A21D5DE1D8CCB5A1DB4D6,
	SocketIOComponent_Close_m8A6924B82A28E90D42E019B2F8290C47EA673363,
	SocketIOComponent_On_m0AB7A1F85759D816361F14F410E7D4AA756138B3,
	SocketIOComponent_Off_m9776C7EFA2162ABCE0C193D653D1E98653BBFECC,
	SocketIOComponent_EmitRaw_mD9D7EED787262C0B154EEE20C8DD6DDF894CE74F,
	SocketIOComponent_Emit_m64685E9C78955DBE3A5B27EC3CDDC56A5CF2BC81,
	SocketIOComponent_Emit_mE4FD86E57B3D1830CEDA8A73D7C2842A8BF2D5CC,
	SocketIOComponent_Emit_m8852CB355765564C0D81BE95B1AAA735B4AD24BD,
	SocketIOComponent_Emit_m63D8F481FE027021B0D6E8D2360F8EF134409023,
	SocketIOComponent_RunSocketThread_mC53A7E22576A749CD789D185123CC4E499C91133,
	SocketIOComponent_RunPingThread_m621A5F071F04044F5FA0381C80179B4AA4384F42,
	SocketIOComponent_EmitMessage_m7D0DE2FA418CD31701E031B36B366CD5D60E0242,
	SocketIOComponent_EmitClose_m4DCAAABD40D2D184AD072D06B39D1BB37D0300E6,
	SocketIOComponent_EmitPacket_m175B3D4FAF69CE1D1ABC33A4B5836ECF99507070,
	SocketIOComponent_OnOpen_mC64D6102C5EE4A8CDDD197E35670E7D2F95A473A,
	SocketIOComponent_OnMessage_mD0677197828A697F1EBB8405B050C907389B7AF8,
	SocketIOComponent_HandleOpen_m1512658D684F88914E03B6F050CBB7BF6F12A049,
	SocketIOComponent_HandlePing_mF9FA61F30A71E763E6D39089D1AB5E1B665BA9C7,
	SocketIOComponent_HandlePong_m4DD49D832A4593DC45048CA37AB5969F0A451516,
	SocketIOComponent_HandleMessage_mF486AF4664986DF46695243106CC89C6ACB07649,
	SocketIOComponent_OnError_mAAF0EFDB490349F96531F8C8AA48AE59481B9B62,
	SocketIOComponent_OnClose_m3039A4D69424B5B5233B363AC66B85E94030D415,
	SocketIOComponent_EmitEvent_m0DF7622C4CCAD145F8111E6143BEECBF75B3F9A1,
	SocketIOComponent_EmitEvent_mA5885DC30396687139226ACC9DE555A8FB801486,
	SocketIOComponent_InvokeAck_m2020DB72159487F841D333B689FDE3EB69E58CC1,
	SocketIOComponent__ctor_m80FAF8C0660DFBA10028C06CB4E975A10840EE2A,
	SocketIOException__ctor_m81A53CF1903AABC1E6DC951EF776851F32D2B489,
	SocketIOException__ctor_mF497A66832B0596F89128D0026A5A13F800ED8D3,
	SocketIOException__ctor_m10C3C06C82077E5134F4122D3A2C3362D67B9EC0,
	Packet__ctor_m9A00FE88F1B12E9F00DD47DE6A0900366FF28D05,
	Packet__ctor_mF24B9D858C39BA5D2CB4608FC9F57A7623244742,
	Packet__ctor_m2850E29D47F0A751A7EAD52D73AEA6DE605D3644,
	CUI_AnimateCurvedFillOnStart_Update_m08AA0297F31CC864744503C421800174399595DD,
	CUI_AnimateCurvedFillOnStart__ctor_mAC391C896C24CEEBC43FFF03C068D80925C7FAFA,
	CUI_CameraController_Awake_m5DD0FB6DF39DD842FE5F97D53DFAE3DAEA66BA15,
	CUI_CameraController__ctor_mFC4F6F33C6C40D621C936C7FDDE8B2365667130B,
	CUI_ChangeColor_ChangeColorToBlue_m990E7CDD451A8EA703A45D9D27E0413309FD6FD5,
	CUI_ChangeColor_ChangeColorToCyan_m61C429462F1F1B95CD5D8CAFB8B4CA6E5ED2CDED,
	CUI_ChangeColor_ChangeColorToWhite_m4BE3FCFA006656B69E2B9193762311DF214C439A,
	CUI_ChangeColor__ctor_m43ED965886F679B7209A689AFE501087262F8DA0,
	CUI_ChangeValueOnHold_Update_mDC750B878125B8DC7892BC2D7CBF1A81E6C3CD9B,
	CUI_ChangeValueOnHold_ChangeVal_mE87124B16F2997632DED6C5DDC46A4B2158EEB2D,
	CUI_ChangeValueOnHold_OnPointerEnter_mEFA0A76F2F750F0692A7834605604022AD55A888,
	CUI_ChangeValueOnHold_OnPointerExit_m1EE64AB5303BEEB47F68E2086CA9FCB6BE09A6F7,
	CUI_ChangeValueOnHold__ctor_mE077C29B46DAE7C33DA52BE53388088DCEDCFB58,
	CUI_DragBetweenCanvases_OnBeginDrag_m1289AE15330551287F1C34261453B5D7FB42B1C7,
	CUI_DragBetweenCanvases_OnDrag_m0BF8FF5F781972B5F024EF6013E1758F89AC5E35,
	CUI_DragBetweenCanvases_OnEndDrag_m2F5193266203F6706CA6C914AB5A4245E347EA0A,
	CUI_DragBetweenCanvases_RaycastPosition_mDA21C9D23250E1E9409E3B297E9B0C019A77749D,
	CUI_DragBetweenCanvases__ctor_m14E2E548261AD01450A25955D268ABD2490A10A0,
	CUI_GunController_Update_m36E60BDB2F0D7EE14D26341EFD3D836FA626BEF0,
	CUI_GunController__ctor_m138EF56C55DB13887EBAF19D7F413D2EF2C6BC91,
	U3CU3Ec__cctor_mBEA2F417A0C3CD57FD8DE25221561DECE3BA4A82,
	U3CU3Ec__ctor_mCC2AE7B34B6FE1D5236FA2A09B19D187DA9A7E78,
	U3CU3Ec_U3CUpdateU3Eb__2_0_m6D7CEA45743E007494D891DD7FA27FDE3C7A0594,
	CUI_GunMovement_Start_m1F5A96BA9E13BA21599CE013B956B3F55120B0D1,
	CUI_GunMovement_Update_m64C72287EE449B6F1C5BB323755043C99CBB115B,
	CUI_GunMovement__ctor_mCFE763C95F72212AA989ECCC00789C5D583F5446,
	CUI_OrientOnCurvedSpace_Awake_m4F777FAE1C0B7F3BEE5450542F034E26774B07FC,
	CUI_OrientOnCurvedSpace_Update_m331D55EB981ED973C38BDA30DF1BEE8A925E3F05,
	CUI_OrientOnCurvedSpace__ctor_mF7488BACB1FED15FC6A8185061FFE0FCCFAADFF3,
	CUI_PerlinNoisePosition_Start_mED61FFCB2F81200D3D5282D08D331C4F6B19C28A,
	CUI_PerlinNoisePosition_Update_m28D8742BEEAE9B82062FB171EC4E54A4F7CCABE1,
	CUI_PerlinNoisePosition__ctor_m30164467BDB01269D7F0B7CD3607ACFB3CBB24FD,
	CUI_PerlinNoiseRotation_Start_m6B033CBA1B875C9B1D83B6E75FD0EE6FA4C9AE68,
	CUI_PerlinNoiseRotation_Update_m2D8C8B962425E685B5DBAD2FFDCADC0CC79BA362,
	CUI_PerlinNoiseRotation__ctor_mE75B964A1634FB4179FA17FC536FAE9E5FEE7F7E,
	CUI_PickImageFromSet_PickThis_mF039517CAC67B443A8A82B4038CD7FEDAC907973,
	CUI_PickImageFromSet__ctor_m133C98C8E9926644CC433240609F460FBDD9FA95,
	CUI_PickImageFromSet__cctor_m3543BFCF3C1A2EEA62783DD276822FB614B66624,
	CUI_RaycastToCanvas_Start_mA1FD0F71147F660A151EB14CD885DE707AAE3D60,
	CUI_RaycastToCanvas_Update_mC4E15CAA713DDB0C609E1678F64AA5A2645B730F,
	CUI_RaycastToCanvas__ctor_mD9D224C47F32D9096B2FCE21B1F5EBB55F2B568B,
	CUI_RiseChildrenOverTime_Start_m86A7C7A590E268885769EBD1E9F7F7B20E502E9D,
	CUI_RiseChildrenOverTime_Update_m0E0C3F589ADFEC43E00DAC36C340DE1EB1281773,
	CUI_RiseChildrenOverTime__ctor_m73D8A39EC92DBAB8546FB45B52A90AD5ABB9E0D7,
	CUI_TMPChecker_Start_mF5D746C1E08CFE2A2462266B61703B7B2B98992F,
	CUI_TMPChecker__ctor_m1ECD982CA173EC821ED5FD8EE9EC013DE0F99698,
	CUI_ViveButtonState__ctor_mDF5D552AE93D653B7ED7338FC5CB54F845FBEDB0,
	CUI_ViveHapticPulse_Start_m38DFDBFDF1DFBB7552ACF5867F884049052F147E,
	CUI_ViveHapticPulse_SetPulseStrength_mBE06B9AAC0C9FEB505DF3164AEABB96D323DCDE0,
	CUI_ViveHapticPulse_TriggerPulse_m9F802EDFED42CC8A58FB9E73030FA5A5E5286C2F,
	CUI_ViveHapticPulse__ctor_mEAA39CCB1A0D73FDF6707BB9C2CA5A9C6FE9B57E,
	CUI_WorldSpaceCursorFollow_Start_mA7D5D85E4DDC9611B2CD88D11421B3D96151B347,
	CUI_WorldSpaceCursorFollow_Update_m79D6751B0C7D605B1536E943B950F0CA9CCC8F6D,
	CUI_WorldSpaceCursorFollow__ctor_m2D2DCC066E501FE413B0A9F98DC149B063793433,
	CUI_WorldSpaceMouseMultipleCanvases_Update_mCA4C8A5D3D8D9AE46FD6C4DC92CD2DBBB06A5350,
	CUI_WorldSpaceMouseMultipleCanvases__ctor_m90AFE943803DBB76DACF6A8944C85622B50A8133,
	CUI_draggable_OnBeginDrag_m99EF0FF2C391569B16201090A58274797FBFDA4E,
	CUI_draggable_OnDrag_m9BB0E276CE35CED133007835C3DCFC803AC4B824,
	CUI_draggable_OnEndDrag_m2A370C66EB011D24669A944F5DD306CBDE30E126,
	CUI_draggable_LateUpdate_mA3956CF0D0018C63E9C48E6B56365FBFB7F17DDB,
	CUI_draggable_RaycastPosition_m08D4CB319883C3E692116DCB0F8901E78DDBC2DE,
	CUI_draggable__ctor_mC2EBF640917826503F7B9B9F173F51240A63121B,
	CUI_inventory_paralax_Start_m3CC700D87DEBCD19CC0BE97E4A6BE11877D50BEC,
	CUI_inventory_paralax_Update_m5AAA9CBB78D1F6B34A5C916BEEEA0236A2F9D8CE,
	CUI_inventory_paralax__ctor_m869020A7B07AE6E7E6993FC22049332BD07E9583,
	CurvedUIEventSystem_Awake_mFC9A77154DAB17EC7E0A84FCB511067E3DBFA09C,
	CurvedUIEventSystem_OnApplicationFocus_mECAD0CE88C8C50B820D8DBE5609108F466DAF8D5,
	CurvedUIEventSystem__ctor_m516C0FF4FECFDA5B335CA49F92F05A599D46893F,
	CurvedUIExtensionMethods_AlmostEqual_m06D0CC3D545F8E2F991AF6C505B87F9E3BD84D12,
	CurvedUIExtensionMethods_Remap_mAEAC70B7C6CEFBC52700BF24A5E2B2D6CD28B658,
	CurvedUIExtensionMethods_RemapAndClamp_m1187F4DD5070953163F8A7BBFAC08686430E4703,
	CurvedUIExtensionMethods_Remap_mB5DFE445630877414F1B509AC16816C261624397,
	CurvedUIExtensionMethods_Remap_m4560F0F56FD7C1BBF4C19CCDBAD340F86F6BE2F2,
	CurvedUIExtensionMethods_Clamp_m473B57BE855F895390DA6586C4160A552BDACF2F,
	CurvedUIExtensionMethods_Clamp_m4A60B368AFAD3F893EFAE3EA80A0A434E8639278,
	CurvedUIExtensionMethods_Abs_m90B46839FD8F89DC2A4352A372F427411FBA4546,
	CurvedUIExtensionMethods_Abs_m4BF5805D5915CEAA1BA364FCA10CC8973B1EFD6C,
	CurvedUIExtensionMethods_ToInt_m6547A81D340623ABBB687DCD8E264D390E288B8F,
	CurvedUIExtensionMethods_FloorToInt_mA2B4F18C3554AE79045B0881C2914AF8C587370F,
	CurvedUIExtensionMethods_CeilToInt_m2685A18F5EAB3E9B86A1DD2740D5D9E74ADDEE88,
	CurvedUIExtensionMethods_ModifyX_mC831B7C37D9752B8E96BE55600586CCAE8C17C06,
	CurvedUIExtensionMethods_ModifyY_m0E892B4897F1156F337C8C736A3F374651AD3506,
	CurvedUIExtensionMethods_ModifyZ_m20137F0418097EBB0B00ECAB7828AA64B4551F7B,
	CurvedUIExtensionMethods_ModifyVectorX_m352F2CFE5094BAC31B1D6D627CED77F1A94AFEDA,
	CurvedUIExtensionMethods_ModifyVectorY_mA4E4BE861A802E16B5E6990B31F92DC646EFA6F8,
	CurvedUIExtensionMethods_ResetTransform_m0608AF599B60590872F013DCBC9D797B9A48933F,
	NULL,
	NULL,
	CurvedUIHandSwitcher_SwitchHandTo_m06020E832646BA1137A4A9B6CA70F195C8300AA1,
	CurvedUIHandSwitcher__ctor_m38A284E5E6CD80B19AF76CDAD4BA09DB119AA4BB,
	CurvedUIInputFieldCaret_Awake_m60173B3FA374FB03FAEB7B13AF89CD6218BD0747,
	CurvedUIInputFieldCaret_Update_m8077F0C95EE6DD0CFF895BB12CA4D45E79CEDA3F,
	CurvedUIInputFieldCaret_OnSelect_m58BDC6053D24E037AB3344EC99A59B4C22462272,
	CurvedUIInputFieldCaret_OnDeselect_mA110C3E453C57E54F1C43F74AEE39F66DF0E327F,
	CurvedUIInputFieldCaret_CaretBlinker_mDFE5E6834175B6A6427649AA2B4A9C5C2D6A21FD,
	CurvedUIInputFieldCaret_CreateCaret_m10BBEEC4F7A370B81FD4F891A044CFA3910F85C9,
	CurvedUIInputFieldCaret_UpdateCaret_mA9F8F426EC3520D114B47566B1E9514E9AB8FDF2,
	CurvedUIInputFieldCaret_GetLocalPositionInText_mAB42F91F0D6A0DAAEC400A8CC517B1D0136B0D2A,
	CurvedUIInputFieldCaret_get_CaretColor_mC87E54D95B1E5839203366268B0A0DD72A9103E6,
	CurvedUIInputFieldCaret_set_CaretColor_mAB74E1DF8F92B69CCFB4D2CC83C24B7551F25EAE,
	CurvedUIInputFieldCaret_get_SelectionColor_m4BC57166AC1E16AB0215DCB05E4B15AE90A9BBD4,
	CurvedUIInputFieldCaret_set_SelectionColor_mCDE6CDB94219BB3F228DFB370655085C46BCA27C,
	CurvedUIInputFieldCaret_get_CaretBlinkRate_m9A52EF65060B50207D6AB2B2F057416065957A36,
	CurvedUIInputFieldCaret_set_CaretBlinkRate_mC1AB3F74A449A8E40EEEFF5303F5AD381270FBB2,
	CurvedUIInputFieldCaret__ctor_mA1AA9A03A3349ECB68E432EA474AB87499436CD0,
	U3CCaretBlinkerU3Ed__11__ctor_m67381478BF9345D3E969C25962E451EF91315AF5,
	U3CCaretBlinkerU3Ed__11_System_IDisposable_Dispose_m018D6335DD56C7358CBAB7268D29E25C06496FD7,
	U3CCaretBlinkerU3Ed__11_MoveNext_mCBC54332F7307B856664B6B6CDCCD59A233835BE,
	U3CCaretBlinkerU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD634726565727B1AECD173A723AC9192716DE83,
	U3CCaretBlinkerU3Ed__11_System_Collections_IEnumerator_Reset_m98980809C0CEA288CA6DD8AE67E8C6CD25C55D7D,
	U3CCaretBlinkerU3Ed__11_System_Collections_IEnumerator_get_Current_m74515E26D1D8C92C52973FE874CCE1F7D22F14BA,
	CurvedUILaserBeam_Update_m239D2A72903FC76A3D9DEBCD936C537B9C38D090,
	CurvedUILaserBeam__ctor_m9CAB1D1EDC73F95346EAA6766F293645F9EEEE56,
	U3CU3Ec__cctor_m668D5A445F20CC34FCD5F1C04FD94163FD179076,
	U3CU3Ec__ctor_m8E2F66F69E455F9CFF7731C0A756FCDDF1303A67,
	U3CU3Ec_U3CUpdateU3Eb__3_0_m654CBA718A9C57D9969BB08F75670F5D5D29507E,
	CurvedUIPhysicsRaycaster__ctor_m394C48FBB1CEBD81E60B9C1E5D4061985E9E1ED2,
	CurvedUIPhysicsRaycaster_Raycast_mEAB5959326AC74D61C700F93A59FAECE5BCB6343,
	CurvedUIPhysicsRaycaster_get_CompoundEventMask_m88799FEB4593AC9BED95923EFFDBA97642FF9C86,
	CurvedUIPhysicsRaycaster_get_eventCamera_m09B513DE7529B45E877935D50F1409040BE7058F,
	CurvedUIPhysicsRaycaster_get_Depth_m57F505637677A1A00FAAA33558B0FA6ECDAFCF4C,
	CurvedUIPhysicsRaycaster_get_sortOrderPriority_mA831B0E1AC052CCA88F4148BBFEB3085933E6801,
	CurvedUIPointerEventData__ctor_mC97436B80566CB27E88CD2D0CF5B3C20753E6A9C,
	CurvedUIRaycaster_Awake_m41A71B7D05E239792E111C3AC3EECB501E6CF0F4,
	CurvedUIRaycaster_Start_m7830A9CFAD30ECB0C9B2AD9364D1FFD142FDAA7B,
	CurvedUIRaycaster_Update_mF15776991BA44552DF39FBE063DA0AD88297F5BD,
	CurvedUIRaycaster_ProcessGazeTimedClick_mB9A9B264031551841D452A70E26328AB0C817FF2,
	CurvedUIRaycaster_ResetGazeTimedClick_m843C7C84068CB9C07C3A23D7D523F06D53C7C100,
	CurvedUIRaycaster_Raycast_m64586E1A925EE17353C4E4223DCEDCD15320D54A,
	CurvedUIRaycaster_RaycastToCyllinderCanvas_m4EB80FA872167544AB7A1C280068B90BE7E24D45,
	CurvedUIRaycaster_RaycastToCyllinderVerticalCanvas_m57FF4C55C28EF9BF69F8780FB8094BA84E8D2233,
	CurvedUIRaycaster_RaycastToRingCanvas_mB516E9C001D62B88ABA55A7DAA1CAB45FC50A427,
	CurvedUIRaycaster_RaycastToSphereCanvas_mEFE28824AFEC714493F5AFD1817F87A2481C9369,
	CurvedUIRaycaster_FlatRaycast_mE77907450A3E3BCAA6281F73937BD8F7A14F9845,
	CurvedUIRaycaster_FlatRaycastAndSort_mA8B7682956D651F95471FCA6471902B70E6BAE9E,
	CurvedUIRaycaster_CreateCollider_mA8BF560052053B5FC6077E85CCD79CFD7F629ACF,
	CurvedUIRaycaster_SetupMeshColliderUsingMesh_m0286CC32AE1A28B97D1D01692E09118FAEC705C9,
	CurvedUIRaycaster_CreateConvexCyllinderCollider_mA543C686BE160F470551D7CCE6C76C7B980CA8D8,
	CurvedUIRaycaster_CreateCyllinderColliderMesh_m17DE884386AF2F807B4ABCA686A00AEF128B281D,
	CurvedUIRaycaster_CreateSphereColliderMesh_m5DAB4225D7D0416B57F0358800294F290DDDED2F,
	CurvedUIRaycaster_IsInLayerMask_m75DB04891FF16B4F91DF95CF2EC229394805B1CC,
	CurvedUIRaycaster_GetRaycastLayerMask_m29B6E80D367057356377A91DD0D8B87F6D7E0111,
	CurvedUIRaycaster_get_GazeProgressImage_mDF2E8876BCD902EDCC1D2E0F7A7BA5FFCF55BAEE,
	CurvedUIRaycaster_AngleSigned_mC5B773C402C43DEFC4951587BE44F3233A8D2B2E,
	CurvedUIRaycaster_ShouldStartDrag_m9F9C98980D20F76E4750C64F8CF3B0500F4A152E,
	CurvedUIRaycaster_ProcessMove_mB380654DFD5C7D00493C843630F2EDEDCB861554,
	CurvedUIRaycaster_UpdateSelectedObjects_m2AA12EFC33A3889BC1B7ECCDD029E5B9C045FBF8,
	CurvedUIRaycaster_HandlePointerExitAndEnter_mB6AE6B4BB0C035B746E5EB993C1FF7AB5F639541,
	CurvedUIRaycaster_FindCommonRoot_mEE9AFC04C85BFEDC6124D9007009B144AFE6371F,
	CurvedUIRaycaster_GetScreenSpacePointByRay_m29D009CB48063DFFD8A7E47596463B1B551812A4,
	CurvedUIRaycaster_CheckEventCamera_m6613B2556B486767F0208408EC2ABBE0EA8DBA20,
	CurvedUIRaycaster_get_PointingAtCanvas_mB7403942A71A3ABD3342F50BB898520AACAE386A,
	CurvedUIRaycaster_RebuildCollider_mDA76326696DBCB66EC1E9BAA302F3638A400695E,
	CurvedUIRaycaster_GetObjectsUnderPointer_mF213CEEBAA90A773CA0DDD71D00C4541830A30E0,
	CurvedUIRaycaster_GetObjectsUnderScreenPos_m87646F59031FE530555FC38F1D20466A981CB3BA,
	CurvedUIRaycaster_GetObjectsHitByRay_m148D933758579396D11CD5CC933FB2BE81B64A47,
	CurvedUIRaycaster_Click_m4E0FB818486A780429EDC714D10F3599CB87ECAB,
	CurvedUIRaycaster_ModifyQuad_mFB23B1772220A4612677D0630C593686613A9A9C,
	CurvedUIRaycaster_TesselateQuad_mDAF3602A35B00586A317F20DD8A9DC977E9684B3,
	CurvedUIRaycaster__ctor_m9BE516992572C4816332C16F25B4832CB8E8E6EB,
	CurvedUIRaycaster__cctor_m3F85146FA9FC4ACEDD66D97EF10E1DA8B74E24D4,
	U3CU3Ec__cctor_mF56774A508D9974A40386403C590C0017029B878,
	U3CU3Ec__ctor_m60DF2B930C5ACFFF5BFB160ED5B355B7E6E88C27,
	U3CU3Ec_U3CUpdateU3Eb__21_0_mFC514F6FCED2666E408C30CEBCC7F3C165E9CBEC,
	U3CU3Ec_U3CFlatRaycastAndSortU3Eb__32_0_mEBDCF8ED8EE7BB8827893E5A1CEFE1A59B16CB40,
	U3CU3Ec_U3CGetObjectsHitByRayU3Eb__55_0_m72535D85748212A6B27ABDB9950DBE256B0F4D90,
	CurvedUISettings_Awake_m33E15DA403B2389FAD7FCA4969F395DC454B9A25,
	CurvedUISettings_Start_m88CF93D4DAB4941172D8F02B882F7A734BD9AB5A,
	CurvedUISettings_OnEnable_mC0C81E95DB6033E6A9AF8AA7FC60F32C8E57E03F,
	CurvedUISettings_OnDisable_m2B6EAA803350D9B9A7889B5FFF936C368586FDE6,
	CurvedUISettings_Update_m3AFCC97F3B4A410A9CFF600E7E4C95DF837DBFFF,
	CurvedUISettings_SetUIAngle_mEE7C7B911E083ECB9F1E86755C2E24E90232A4F6,
	CurvedUISettings_CanvasToCyllinder_m3E67A2527A7C582B69C61B88CD31199F42629823,
	CurvedUISettings_CanvasToCyllinderVertical_mFB603527BF61C0D300DFA49B696A72F05D9BA1EE,
	CurvedUISettings_CanvasToRing_m0C25C1E32C66C19C9467E858A00A4A61E8C43DB7,
	CurvedUISettings_CanvasToSphere_m929920AF2E6469BB5ED5D21D1852259E140E6978,
	CurvedUISettings_get_RectTransform_m9D237DC354456882D1C0E94C3DB66B1CC6BEC35B,
	CurvedUISettings_AddEffectToChildren_m36C3BAF8A9433A04E534A058EA886504280D9C32,
	CurvedUISettings_VertexPositionToCurvedCanvas_mA5680842C563023029933E089D58BCDEF5F6510B,
	CurvedUISettings_CanvasToCurvedCanvas_m1658CCBFF20B993B94670E4DE4A938DB4A39F4EC,
	CurvedUISettings_CanvasToCurvedCanvasNormal_m9CFF358CC25FA358156071F046E1C364C36E72E6,
	CurvedUISettings_RaycastToCanvasSpace_mE7642ABF453446669890DC049B36AE70FC295614,
	CurvedUISettings_GetCyllinderRadiusInCanvasSpace_mFBCDDD132AFE0B8E308F0757B5EAB21294038855,
	CurvedUISettings_GetTesslationSize_mA5A866B283EF76F227141A4208D18AD0874E12E6,
	CurvedUISettings_GetSegmentsByAngle_m9F396F0EE0559F7576566E04468270FBD4D90569,
	CurvedUISettings_get_BaseCircleSegments_m76D735CBE7DF620256E7E34B1EFAD4924B838FCB,
	CurvedUISettings_get_Angle_m1EE2996AFD63A8450C903341A72939708998E5A0,
	CurvedUISettings_set_Angle_mFBED67A77D1C8C618C440A1F7F9E21A6705D2733,
	CurvedUISettings_get_Quality_mE6527143BD0F8AEC406A617F17DAA435B371496E,
	CurvedUISettings_set_Quality_m166319A9FAB48C490BF767FA6FBBC926BABDD957,
	CurvedUISettings_get_Shape_m6BE04B6D15EC541E67CBBAA98F79D12A3DA99978,
	CurvedUISettings_set_Shape_mB9C7A429B780CF018761B95A145F3D0BFB408972,
	CurvedUISettings_get_VerticalAngle_mBEFF9D6FE2D5F11421A2FD99D42A8E775B7478C9,
	CurvedUISettings_set_VerticalAngle_mE4B06780F9F9BC51A3EABE86207CFAE552249513,
	CurvedUISettings_get_RingFill_m9349A724E071D560F52F6079A71535B93992B65D,
	CurvedUISettings_set_RingFill_m6DE600750159292FA24758C5ED896AC88403285B,
	CurvedUISettings_get_SavedRadius_m17969A1CFEFAED639C2017AD3BD35E189FE39C50,
	CurvedUISettings_get_RingExternalDiameter_m9FB3A641AB503C081C1B28A5DB7EE2EB7073FB24,
	CurvedUISettings_set_RingExternalDiameter_m0C8C0B2B0BF0A4899A298A12F2B4FA925A301D68,
	CurvedUISettings_get_RingFlipVertical_mDB3E42B6983142E071C5E8307C15AB322349862E,
	CurvedUISettings_set_RingFlipVertical_m32081215315DF813D0F07EC24A2994976141F5D7,
	CurvedUISettings_get_PreserveAspect_m26AE14C6CF90717F8DF7168D1ED4ED9E10E143D0,
	CurvedUISettings_set_PreserveAspect_mA5E4C3FA17394FF86E555D06E98B6081A9CCA5B5,
	CurvedUISettings_get_Interactable_m4B895A6868BEE4E82E6D94DB41FCA246DA563EDF,
	CurvedUISettings_set_Interactable_mEED95FCD49ECD36F3CEFB2FFFC8E60E861E0D936,
	CurvedUISettings_get_ForceUseBoxCollider_m36FD603A67636DCDA54E6D01E8E54B1BA90ABC0F,
	CurvedUISettings_set_ForceUseBoxCollider_mCFF6881EDD5C8EFA701D87293D079FEFC2C28E1C,
	CurvedUISettings_get_BlocksRaycasts_m31E74082A1F09E01C342D9C3FE8FD3979ACEAE94,
	CurvedUISettings_set_BlocksRaycasts_mA13BB0F17078B73476F5AF5A118255E84F8D0F8E,
	CurvedUISettings_get_RaycastMyLayerOnly_mEDFAB015DF898866585C9C998BEC5369C0646054,
	CurvedUISettings_set_RaycastMyLayerOnly_m3CB69A45B1B6D091FA936BBB5255AB41A9C3BB6B,
	CurvedUISettings_SetAllChildrenDirty_mA93BB6B6F8F1642A66B52FE66043D47586D76FE4,
	CurvedUISettings_get_PointingAtCanvas_mAB6014E2CC9A183D27192E4AFDFB0DD99AE3EFC1,
	CurvedUISettings_Click_mDB49A7CCF7C07F1F5F72AC7D097BBB9EE30D472B,
	CurvedUISettings_get_ControlMethod_m9E222781CCE562D3EE938484C7AA45690D3DC1C0,
	CurvedUISettings_set_ControlMethod_mB3F050144BEA6F463989F42D9889C31695EC91DF,
	CurvedUISettings_GetObjectsUnderPointer_mEA3244063C0DC120A27905EABF83A427FA52E846,
	CurvedUISettings_GetObjectsUnderScreenPos_mB7E0F07652C86AFF90E298F65E20C7ED447B4C1F,
	CurvedUISettings_GetObjectsHitByRay_m0A10944D3F647034ABD7D9E17C3C6DAEB205D8B6,
	CurvedUISettings_get_GazeUseTimedClick_mE3456FA58CDEAEF0C6A863B6A0C559EC71E9F0DA,
	CurvedUISettings_set_GazeUseTimedClick_m7F8302872A9A453E23A484482313DCB66DBC1165,
	CurvedUISettings_get_GazeClickTimer_mF336D35CEE0E6C75ACCEEA613BD420A091BD35FF,
	CurvedUISettings_set_GazeClickTimer_m107DACAA98C26C4EB0730A44ABBB3D40ACFC51A6,
	CurvedUISettings_get_GazeClickTimerDelay_m2E1760EDE7D2B1E0AC6F1F9A87ED4786DBB67F24,
	CurvedUISettings_set_GazeClickTimerDelay_mECB610FE95147A122DE50E1B45B4F67266EC620E,
	CurvedUISettings_get_GazeTimerProgress_m7C5A8B3A32F6B034E4606D9F6F06464D64D71D7A,
	CurvedUISettings__ctor_mFFF15E7A20C1ED9793F1EA9441F05B3819B58394,
	CurvedUIVertexEffect_get_tesselationRequired_m08E71987CA7C3C05F44AC4C6DC39BFA70851A822,
	CurvedUIVertexEffect_set_tesselationRequired_m45D6AC6D425C55CC7DA5033FC390DEEA5AAEB071,
	CurvedUIVertexEffect_Awake_mD37D2931E5D1CE8436BC2F77FDAFB0A497D0F954,
	CurvedUIVertexEffect_OnEnable_m804472989514EC99F3DDA634E9C1E359BEA24DE8,
	CurvedUIVertexEffect_Start_m52083F15545A43741C25E60AE0528783FF41971A,
	CurvedUIVertexEffect_OnDisable_mF5171DF40BF679BF19FAC7BEB8FD266709D08172,
	CurvedUIVertexEffect_TesselationRequiredCallback_m620E6430212B9F8979023FB06849099F7CF67018,
	CurvedUIVertexEffect_FontTextureRebuiltCallback_mD093F6EDE5570D99ED4DCE982B112E73A3676381,
	CurvedUIVertexEffect_LateUpdate_mB7BD5259F9E1E10D6A61074DE250EF8E10CEF87C,
	CurvedUIVertexEffect_ModifyMesh_m69D2C18CD67BADEE56E1CF324FFAD37DAC77A8F1,
	CurvedUIVertexEffect_ModifyTMPMesh_m723B6C3763093A9742C276D1CA6E06778A00784A,
	CurvedUIVertexEffect_ShouldModify_m385CDF9926572D0FACA0172CD23E5D9FE885980A,
	CurvedUIVertexEffect_CheckTextFontMaterial_m2D71DE03244DBC24E10036EACC5ECC3E8EF7AE52,
	CurvedUIVertexEffect_FindParentSettings_m7181A7C532DAFB430FCB6AE96853C9921A615EEE,
	CurvedUIVertexEffect_CurveVertex_mC6912A723DEAF6D098B4CA3A099897FE657C46F8,
	CurvedUIVertexEffect_TesselateGeometry_mCB606693E7EC0C4FC9CC7CD50A52D7AAFF6A53E9,
	CurvedUIVertexEffect_ModifyQuad_mB5AF951AFC70A67588CFE401B06F3EB8C41680CD,
	CurvedUIVertexEffect_TrisToQuads_m5FADD57BE7342676E2FF447DD81E875DA1458C6A,
	CurvedUIVertexEffect_TesselateQuad_m366A2E193ECEC00E97F4E6E9491B6A9933B01852,
	CurvedUIVertexEffect_SetDirty_mE48F20338F6D45E5808C5C16563096EB21821856,
	CurvedUIVertexEffect_get_TesselationRequired_m1B90903536388AB253BD17B7A6AB74D0301FD81F,
	CurvedUIVertexEffect_set_TesselationRequired_m7FFD8CD9F89603EA21F0999757255F5559C45DD4,
	CurvedUIVertexEffect_get_CurvingRequired_m134E5F264829E3B5EBD7EF69FFE4D4427CA3E5AF,
	CurvedUIVertexEffect_set_CurvingRequired_m41D1342720881EFB3527EE0A536B3EAB67EDD1B4,
	CurvedUIVertexEffect__ctor_m17509362682E544585262DDAD2179D78450F8EFB,
	CurvedUIViveController__ctor_m8091F5C90E023A32E5753E4029A43AE7E0A18C3F,
	ViveInputEvent__ctor_m632425778123A6B068C61427CC8B5EC5CC046FEA,
	ViveInputEvent_Invoke_mCA1790FC2D22BFCE56B60C319A2483B665148F75,
	ViveInputEvent_BeginInvoke_m11064F45527733AE5F2D67400681C37F884EE189,
	ViveInputEvent_EndInvoke_m6FAF2D6B6F9A44BF204825BB724CE4185C6CEC36,
	ViveEvent__ctor_m4E4D6CD824208EC3C89FFC672AACA472C615AB6D,
	ViveEvent_Invoke_mB15BF3283CA0BAD82245A745921BE469D9A9974D,
	ViveEvent_BeginInvoke_mBD19E358FC6580DD3D8F62F208FB499B6C7779CA,
	ViveEvent_EndInvoke_mEAA7F601BF9A8E622D57039F25591C1F3B99B369,
	CurvedUITMP_Start_m1BE56EA01AB1B8A3C93C054DC669C5026D6ACE1E,
	CurvedUITMP_OnEnable_m737F9D46AC239D8F7A6FDC78BFC9BD51C28BFC41,
	CurvedUITMP_OnDisable_m7795FC16D377B51EA3BAC5769C2C8F183DB169CC,
	CurvedUITMP_OnDestroy_mF27E2E9A826FEA627E1A5B7348A251C5378B1C66,
	CurvedUITMP_LateUpdate_m778AF8EF130C4D784F13F13DAD29CCF4BDF9C125,
	CurvedUITMP_CreateUIVertexList_m1F9577511B96E2D79C4CBD1330F48063BFEDA728,
	CurvedUITMP_GetUIVertexFromMesh_m57183D6A1DBE9738A53CA5BE0CDDEAA2E847A436,
	CurvedUITMP_FillMeshWithUIVertexList_m341D2F7F640BBF278B2A2F8CBF0CDE6AD35A3CE4,
	CurvedUITMP_FindTMP_mFACBD48AE2925379E501DECCC7A203630D1EF3FA,
	CurvedUITMP_FindSubmeshes_mC64BD7183A342209EA54542B617D177E9F41CFEE,
	CurvedUITMP_ShouldTesselate_mDBEA54B4612CCE6818B9B3417601D1D157DCE2DE,
	CurvedUITMP_TMPTextChangedCallback_m6102698DD858BFCB0872B0D4C0781A0D3487D934,
	CurvedUITMP_TesselationRequiredCallback_m883BCE664FDF049FC51A8833A517E59E9A92D974,
	CurvedUITMP__ctor_mB5FB4AF340667B1A0705A225F9F4F2F34614D77D,
	CurvedUITMPInputFieldCaret_Awake_m37D5B28657ACF2865189287B0F385EC96CB9E227,
	CurvedUITMPInputFieldCaret_Update_m1732770BE61A7FE8BDF47D615A66870FA726F036,
	CurvedUITMPInputFieldCaret_OnSelect_mB4B67E5CEA6E8624C4972BCA983A052843290D03,
	CurvedUITMPInputFieldCaret_OnDeselect_mBF8A14A16DBE7C1921D1A88FA4EC913DF623D6AF,
	CurvedUITMPInputFieldCaret_CaretBlinker_mCE5460202CFB5609B10401374AA7E30ADAAB250B,
	CurvedUITMPInputFieldCaret_CreateCaret_mAEAD784D5650AC57DE14B46A1C5C06549B781E17,
	CurvedUITMPInputFieldCaret_UpdateCaret_m387C4C599DB34D7F97D3BE3D673FE7CA2DD63745,
	CurvedUITMPInputFieldCaret_GetLocalPositionInText_m2CBB990A9AFC75561F2A4F7E7D704FC7DA17D6E9,
	CurvedUITMPInputFieldCaret_CheckAndConvertMask_m8BEA6B95DC990F6ECD646BB84D02C1323935B8F3,
	CurvedUITMPInputFieldCaret_get_CaretColor_m0B1A30AB4E7D9180E418E91BE722E7C493E2C5B3,
	CurvedUITMPInputFieldCaret_set_CaretColor_m139F5BCA647DBD2E9A652ACBCC004A3A2DEC0E7E,
	CurvedUITMPInputFieldCaret_get_SelectionColor_mC17554DB7CE4F1963F75CAA209FC5E1FFCCB580E,
	CurvedUITMPInputFieldCaret_set_SelectionColor_m9E3862E7A15164F95C0DBE2B97246DB13AC99905,
	CurvedUITMPInputFieldCaret_get_CaretBlinkRate_m80A49018D407C1D03CB447796266B2EADAD0E9B5,
	CurvedUITMPInputFieldCaret_set_CaretBlinkRate_mD3A7D152AC19FEA941995849C279A1EE09867257,
	CurvedUITMPInputFieldCaret__ctor_mDF9A498C1B99D38F5859DE58122597C7644D4EA1,
	U3CCaretBlinkerU3Ed__10__ctor_m0672D5EA4DFF81957D859899F96AC00C03742AAF,
	U3CCaretBlinkerU3Ed__10_System_IDisposable_Dispose_m755CF49521B55B700882B83C41E873E163423ECF,
	U3CCaretBlinkerU3Ed__10_MoveNext_m3213C25289AB7479A93A27755562135C5668AF18,
	U3CCaretBlinkerU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7D11CE3F3556DD5D7F1BE36E95C18F09F8EC2D7,
	U3CCaretBlinkerU3Ed__10_System_Collections_IEnumerator_Reset_mA51EAF61D997DEDC7522E9CAB2E1A8EA1375E2E4,
	U3CCaretBlinkerU3Ed__10_System_Collections_IEnumerator_get_Current_mCD80F8F62871C6414C4252EFF6DE75B029A8C2A1,
	CurvedUITMPSubmesh_UpdateSubmesh_mC34E6DC08013F0BF07F5255969C9FB0AF7D07D9A,
	CurvedUITMPSubmesh__ctor_m07C154FE7BF5110D1DC6BBE1A4AAAC51916BAAC0,
	OptionalDependencyAttribute__ctor_m494B803BF32C02FB5ABE78EEE7FFB0FD3360FD92,
};
extern void ovrAvatarMaterialLayerState_Equals_m1C14E1DBA874C1083F95A564F30780DEF30E435E_AdjustorThunk (void);
extern void ovrAvatarMaterialLayerState_GetHashCode_m3302E925482D7B9E2072339A1F3C3EF15F46B519_AdjustorThunk (void);
extern void ovrAvatarMaterialState_Equals_m293D1C2CBB4C8403F0D14E1A68DB14379FEF491D_AdjustorThunk (void);
extern void ovrAvatarMaterialState_GetHashCode_m573CEF4EF15464DAF18046F8F0B7E56705B55740_AdjustorThunk (void);
extern void ovrAvatarExpressiveParameters_Equals_m501E5CF62F65200176CC04A883C7CD3CB29C0DB1_AdjustorThunk (void);
extern void ovrAvatarExpressiveParameters_GetHashCode_mEA3158598F1C76B2480355833396802E41C4E75B_AdjustorThunk (void);
extern void ovrAvatarPBSMaterialState_Equals_m1C1A3BFBDC22AE8A5A6A3E28725926A9B34C3209_AdjustorThunk (void);
extern void ovrAvatarPBSMaterialState_GetHashCode_m8D5F74AC0D6E2F3546275C51C5142D5572BFFA78_AdjustorThunk (void);
extern void AvatarSpecRequestParams__ctor_mC4E88966CA0552017DF34A410391946CFEF4021D_AdjustorThunk (void);
extern void CopyTextureParams__ctor_m1B3ADEEC9C61F14F8CA8EBCFF0FED7020C338DC2_AdjustorThunk (void);
extern void TextureSet__ctor_mD82F34E402F4351FF6634FAF2614B45506EC8B8C_AdjustorThunk (void);
extern void CameraShot__ctor_mD9736DEA6A3F14DF47920666D00E1ABA5899E179_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x0600050A, ovrAvatarMaterialLayerState_Equals_m1C14E1DBA874C1083F95A564F30780DEF30E435E_AdjustorThunk },
	{ 0x0600050B, ovrAvatarMaterialLayerState_GetHashCode_m3302E925482D7B9E2072339A1F3C3EF15F46B519_AdjustorThunk },
	{ 0x0600050D, ovrAvatarMaterialState_Equals_m293D1C2CBB4C8403F0D14E1A68DB14379FEF491D_AdjustorThunk },
	{ 0x0600050E, ovrAvatarMaterialState_GetHashCode_m573CEF4EF15464DAF18046F8F0B7E56705B55740_AdjustorThunk },
	{ 0x06000510, ovrAvatarExpressiveParameters_Equals_m501E5CF62F65200176CC04A883C7CD3CB29C0DB1_AdjustorThunk },
	{ 0x06000511, ovrAvatarExpressiveParameters_GetHashCode_mEA3158598F1C76B2480355833396802E41C4E75B_AdjustorThunk },
	{ 0x06000513, ovrAvatarPBSMaterialState_Equals_m1C1A3BFBDC22AE8A5A6A3E28725926A9B34C3209_AdjustorThunk },
	{ 0x06000514, ovrAvatarPBSMaterialState_GetHashCode_m8D5F74AC0D6E2F3546275C51C5142D5572BFFA78_AdjustorThunk },
	{ 0x06000539, AvatarSpecRequestParams__ctor_mC4E88966CA0552017DF34A410391946CFEF4021D_AdjustorThunk },
	{ 0x06000559, CopyTextureParams__ctor_m1B3ADEEC9C61F14F8CA8EBCFF0FED7020C338DC2_AdjustorThunk },
	{ 0x0600055A, TextureSet__ctor_mD82F34E402F4351FF6634FAF2614B45506EC8B8C_AdjustorThunk },
	{ 0x06000BF3, CameraShot__ctor_mD9736DEA6A3F14DF47920666D00E1ABA5899E179_AdjustorThunk },
};
static const int32_t s_InvokerIndices[3561] = 
{
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1986,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	2581,
	3965,
	4055,
	4055,
	4055,
	4055,
	-1,
	6183,
	6104,
	6176,
	6100,
	3971,
	3277,
	3965,
	3271,
	3983,
	4051,
	4051,
	3983,
	3287,
	3983,
	3983,
	3287,
	2615,
	6191,
	6106,
	6195,
	6109,
	6195,
	6109,
	4049,
	3354,
	4049,
	4023,
	3324,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	4023,
	3983,
	3287,
	4055,
	6207,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	6176,
	6183,
	4055,
	6207,
	6104,
	5777,
	6004,
	6104,
	4055,
	4055,
	4055,
	4055,
	6207,
	6207,
	4055,
	2988,
	4017,
	4017,
	4017,
	4055,
	4055,
	4055,
	1802,
	1440,
	1802,
	1440,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	3271,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	2588,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4017,
	4055,
	3324,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	3324,
	3320,
	3320,
	3983,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3320,
	3983,
	3983,
	4017,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	3287,
	4023,
	3324,
	2588,
	4055,
	3287,
	3271,
	2588,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	1971,
	3983,
	2261,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	3287,
	2588,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	1976,
	4055,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	2588,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	4055,
	4055,
	3287,
	2588,
	4055,
	3287,
	3287,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4017,
	3983,
	4023,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3983,
	3983,
	1976,
	3983,
	4055,
	4055,
	3983,
	2588,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	3983,
	2261,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3983,
	3983,
	4017,
	3965,
	3965,
	4055,
	1230,
	3287,
	3287,
	3287,
	2588,
	4055,
	3983,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	3287,
	3271,
	3965,
	3287,
	3287,
	3287,
	4055,
	4055,
	2417,
	2403,
	3271,
	3287,
	3983,
	4055,
	4055,
	3983,
	3983,
	4055,
	3287,
	3287,
	4055,
	4055,
	4055,
	3320,
	3320,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3271,
	3324,
	3320,
	3287,
	3356,
	4055,
	4055,
	3287,
	4055,
	4055,
	3271,
	3324,
	3320,
	3287,
	3356,
	4055,
	4055,
	3287,
	3287,
	3983,
	3287,
	4055,
	4055,
	4055,
	3983,
	3287,
	3983,
	4055,
	1816,
	1245,
	3271,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	4055,
	4055,
	4055,
	3983,
	3271,
	3320,
	4055,
	4055,
	4055,
	4055,
	3983,
	3287,
	3983,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3287,
	3287,
	4055,
	3287,
	4055,
	3287,
	4055,
	3271,
	3271,
	3271,
	3271,
	3287,
	3287,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1971,
	1971,
	1976,
	1976,
	4055,
	4055,
	4055,
	3983,
	3983,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	3983,
	4055,
	2588,
	1449,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1971,
	1971,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	1976,
	1976,
	1232,
	1232,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	1971,
	1971,
	1976,
	1976,
	3248,
	1971,
	4055,
	4055,
	4055,
	4055,
	4055,
	3983,
	3983,
	3287,
	3983,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	3983,
	1450,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	3287,
	2588,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	3287,
	2588,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	3287,
	3287,
	3320,
	3320,
	3320,
	3287,
	3983,
	3287,
	3249,
	3287,
	3287,
	1971,
	1971,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4017,
	4055,
	4055,
	3287,
	1976,
	1976,
	1245,
	1976,
	1976,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	3287,
	4017,
	4017,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	3287,
	1976,
	1976,
	1245,
	1976,
	1976,
	3287,
	4055,
	3287,
	3287,
	4055,
	4055,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1976,
	4055,
	4055,
	4055,
	4055,
	1976,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	2026,
	4055,
	4023,
	4023,
	4023,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1976,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3320,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3983,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	442,
	1464,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	2020,
	238,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1030,
	1984,
	4055,
	3324,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	1981,
	1976,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1976,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4017,
	4055,
	3517,
	3517,
	4055,
	3273,
	4055,
	4055,
	3287,
	4055,
	4017,
	3320,
	4055,
	4055,
	4055,
	4055,
	1976,
	4055,
	3287,
	3287,
	4055,
	4055,
	4023,
	4055,
	4055,
	3272,
	3272,
	3287,
	3287,
	822,
	4055,
	1222,
	1276,
	1549,
	1284,
	1618,
	1210,
	1422,
	1198,
	1374,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	4017,
	4055,
	3287,
	3287,
	3272,
	3287,
	3287,
	4055,
	3287,
	4055,
	4055,
	3287,
	4055,
	3287,
	3287,
	3287,
	1976,
	4055,
	1942,
	3287,
	6104,
	6176,
	6177,
	6183,
	6100,
	6207,
	6109,
	6207,
	6101,
	6207,
	6104,
	6047,
	6101,
	6101,
	1971,
	4906,
	6002,
	4055,
	6207,
	4055,
	3272,
	3272,
	3287,
	3287,
	6207,
	4055,
	4055,
	4055,
	4055,
	1320,
	4055,
	4055,
	4055,
	6207,
	4055,
	3287,
	3273,
	1452,
	1453,
	359,
	5519,
	5576,
	5761,
	5794,
	5800,
	5388,
	4055,
	-1,
	4055,
	4808,
	5653,
	5772,
	3273,
	4055,
	4055,
	5798,
	3320,
	3320,
	3320,
	3287,
	4055,
	4055,
	4055,
	1262,
	4055,
	3879,
	1440,
	1184,
	4055,
	4017,
	4055,
	6207,
	6207,
	4936,
	4618,
	4298,
	4055,
	4055,
	3983,
	4055,
	655,
	1971,
	1971,
	4055,
	4055,
	3287,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	1209,
	1215,
	1952,
	1951,
	255,
	2588,
	1946,
	4055,
	4055,
	3880,
	4055,
	4055,
	3287,
	3273,
	1971,
	1987,
	6002,
	4055,
	6207,
	4123,
	3273,
	4055,
	3273,
	6195,
	4055,
	5390,
	5391,
	4055,
	4055,
	3517,
	3517,
	4055,
	3273,
	4055,
	4055,
	3287,
	4055,
	1948,
	4055,
	4055,
	1232,
	4055,
	4055,
	5936,
	1424,
	3287,
	2588,
	4055,
	6207,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4023,
	4123,
	4055,
	3422,
	1245,
	2034,
	3520,
	6004,
	3287,
	5782,
	5779,
	5778,
	5773,
	5781,
	6151,
	6075,
	6082,
	6025,
	6150,
	1816,
	3273,
	3273,
	3273,
	4055,
	1971,
	616,
	447,
	1450,
	4055,
	6207,
	6207,
	5673,
	2887,
	3965,
	5673,
	2887,
	3965,
	5673,
	2887,
	3965,
	5673,
	2887,
	3965,
	1946,
	6207,
	6207,
	6207,
	6207,
	6207,
	6207,
	1973,
	3273,
	1010,
	3287,
	1973,
	3287,
	1022,
	3287,
	1973,
	3273,
	1010,
	3287,
	6183,
	4017,
	4055,
	4055,
	4017,
	4017,
	3271,
	3271,
	3425,
	3425,
	1207,
	1954,
	2582,
	3272,
	3983,
	3983,
	4055,
	81,
	6183,
	6104,
	6183,
	6104,
	6183,
	6104,
	4055,
	470,
	1243,
	1816,
	4055,
	916,
	1239,
	4055,
	24,
	1243,
	1951,
	1951,
	3287,
	3287,
	4055,
	4055,
	4055,
	3965,
	276,
	3426,
	1196,
	3271,
	1465,
	3271,
	3271,
	432,
	1979,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1971,
	1021,
	1449,
	225,
	2581,
	1021,
	647,
	644,
	1449,
	3320,
	4055,
	1973,
	4055,
	1450,
	3287,
	1973,
	3287,
	1022,
	3287,
	1973,
	3324,
	1046,
	3287,
	1973,
	4017,
	1450,
	2887,
	4055,
	4055,
	4055,
	3324,
	4055,
	3320,
	4055,
	3320,
	4055,
	3320,
	4055,
	4055,
	3271,
	4055,
	3271,
	3965,
	4055,
	4055,
	1310,
	3287,
	4055,
	2029,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4017,
	2797,
	4055,
	4055,
	899,
	899,
	3965,
	3271,
	3287,
	3287,
	889,
	3991,
	3983,
	3287,
	736,
	3287,
	4055,
	4055,
	3287,
	2001,
	4055,
	4055,
	4055,
	3287,
	3287,
	3983,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3983,
	3287,
	3287,
	3983,
	3287,
	3287,
	3983,
	3287,
	3287,
	3983,
	3287,
	3287,
	3983,
	3287,
	3287,
	3983,
	3287,
	3287,
	4055,
	4051,
	3991,
	2028,
	4055,
	4055,
	3983,
	3287,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3287,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	3287,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	4017,
	3320,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	4055,
	889,
	4055,
	4055,
	4055,
	3983,
	3983,
	3965,
	3207,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	3287,
	3965,
	3207,
	4055,
	4055,
	3965,
	3207,
	4055,
	4055,
	3287,
	4055,
	4055,
	3983,
	4055,
	4055,
	1468,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3983,
	4055,
	4055,
	4055,
	3983,
	3287,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3983,
	4055,
	1584,
	2152,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	1584,
	2152,
	4055,
	4055,
	1584,
	4055,
	1584,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	6178,
	6178,
	6195,
	6195,
	6176,
	6177,
	6176,
	6176,
	6176,
	6177,
	6101,
	5361,
	6207,
	6207,
	6207,
	6112,
	6109,
	6105,
	6207,
	3287,
	3287,
	3287,
	4055,
	3965,
	3271,
	3287,
	4055,
	4017,
	3320,
	3966,
	3272,
	3966,
	3272,
	4055,
	2887,
	4055,
	3983,
	1976,
	4055,
	4055,
	3272,
	4055,
	3324,
	4055,
	3320,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3324,
	3272,
	4055,
	3320,
	4055,
	6092,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1449,
	1971,
	4055,
	4055,
	3287,
	1245,
	3324,
	4055,
	4055,
	4055,
	3287,
	3287,
	3287,
	3287,
	4055,
	4055,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	3983,
	4055,
	4055,
	6104,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1979,
	4055,
	4055,
	1450,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4017,
	4055,
	4017,
	3320,
	4055,
	6207,
	4055,
	4055,
	3271,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	1432,
	991,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	3320,
	4055,
	4055,
	4055,
	4055,
	1976,
	3271,
	3271,
	4055,
	992,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	3356,
	4055,
	1973,
	3271,
	1006,
	3287,
	1973,
	4055,
	1450,
	3287,
	1973,
	3356,
	1057,
	3287,
	4055,
	4055,
	4055,
	418,
	3271,
	1308,
	4055,
	4055,
	4055,
	1206,
	4055,
	4055,
	4055,
	4055,
	1261,
	4055,
	3983,
	2026,
	1261,
	1651,
	1641,
	4055,
	3287,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	3356,
	3356,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	2594,
	2594,
	3324,
	3324,
	3324,
	3324,
	3320,
	3356,
	3320,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	3271,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4055,
	6207,
	3983,
	4055,
	1976,
	1976,
	1976,
	1976,
	1976,
	1976,
	1976,
	1976,
	1976,
	1976,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3287,
	4055,
	4055,
	4055,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	4055,
	3287,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	3320,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3287,
	4055,
	4055,
	3287,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	3320,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3287,
	4055,
	4055,
	4055,
	3983,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3287,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	4055,
	3271,
	3271,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	3271,
	4055,
	4055,
	6207,
	3287,
	3287,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	3983,
	4055,
	6207,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	4055,
	3271,
	1022,
	3287,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3287,
	4055,
	4055,
	1022,
	4055,
	3287,
	4055,
	4055,
	4055,
	3320,
	6207,
	4055,
	3287,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3983,
	4055,
	3983,
	1976,
	1976,
	1976,
	1976,
	1976,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	3287,
	4055,
	6207,
	4055,
	3271,
	4055,
	4055,
	4055,
	4055,
	4055,
	6207,
	6100,
	5741,
	4055,
	4055,
	3287,
	4055,
	4055,
	4000,
	3305,
	4017,
	3320,
	4055,
	4055,
	4055,
	4055,
	3271,
	1242,
	4055,
	4055,
	6207,
	6004,
	6004,
	6004,
	6004,
	6004,
	6004,
	4763,
	4340,
	5562,
	6207,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	6207,
	5128,
	6207,
	6183,
	5128,
	5128,
	5128,
	5128,
	5128,
	6207,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	6207,
	6004,
	6004,
	6177,
	5946,
	6004,
	6183,
	4055,
	4017,
	3320,
	4055,
	1976,
	2029,
	4055,
	3224,
	4055,
	6207,
	4017,
	3320,
	4017,
	3320,
	4055,
	4055,
	4055,
	4017,
	3320,
	4055,
	4055,
	4055,
	1303,
	6007,
	1487,
	1487,
	3320,
	3287,
	4055,
	3287,
	3287,
	4055,
	4055,
	3271,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	4055,
	3965,
	4051,
	3965,
	3271,
	4055,
	1199,
	1230,
	97,
	4055,
	1556,
	1556,
	1556,
	4055,
	3983,
	3287,
	3983,
	3287,
	3965,
	4055,
	4055,
	3983,
	3983,
	3965,
	875,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	3983,
	3287,
	6183,
	6104,
	4055,
	4055,
	3983,
	4055,
	4055,
	5557,
	4017,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	3983,
	3983,
	3965,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	1230,
	4055,
	4055,
	4055,
	4055,
	433,
	6183,
	6104,
	6104,
	4055,
	6207,
	4055,
	1451,
	1979,
	4055,
	3271,
	4055,
	4017,
	4055,
	3983,
	4055,
	3983,
	6183,
	3287,
	3287,
	4055,
	1554,
	1554,
	1979,
	4055,
	3965,
	3965,
	4017,
	4017,
	3320,
	4055,
	3983,
	4055,
	4055,
	4055,
	3983,
	1976,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	3287,
	4017,
	3320,
	4017,
	3320,
	4023,
	3324,
	4055,
	3287,
	4055,
	1232,
	3983,
	4017,
	3320,
	3965,
	3965,
	4017,
	4051,
	3356,
	4051,
	3356,
	3983,
	3983,
	1976,
	4055,
	4017,
	3320,
	4055,
	3826,
	4055,
	4055,
	4055,
	4055,
	3983,
	3287,
	4017,
	3320,
	4017,
	3320,
	4017,
	4017,
	4017,
	4055,
	1976,
	3965,
	3965,
	4017,
	4017,
	3320,
	4055,
	4055,
	4055,
	4051,
	3983,
	2887,
	3983,
	1464,
	1464,
	1976,
	4055,
	4055,
	4017,
	3320,
	4017,
	3320,
	4055,
	3983,
	3287,
	3287,
	4055,
	4542,
	4055,
	4017,
	3320,
	4017,
	3320,
	4055,
	3287,
	3287,
	4055,
	4055,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4051,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3965,
	3271,
	4055,
	4055,
	3287,
	4055,
	6207,
	4023,
	3324,
	4023,
	3324,
	3965,
	3271,
	3965,
	3983,
	4023,
	3043,
	4023,
	4055,
	2009,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	3287,
	4055,
	4055,
	3983,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4023,
	4055,
	4055,
	4055,
	4023,
	3324,
	4023,
	3324,
	4055,
	1285,
	4055,
	4055,
	4055,
	4055,
	6207,
	4055,
	4055,
	3287,
	4055,
	4055,
	2594,
	1966,
	3320,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	2593,
	3039,
	4055,
	4055,
	4055,
	4055,
	4023,
	4055,
	4055,
	4055,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4023,
	3324,
	4055,
	2594,
	4055,
	3324,
	4055,
	4055,
	2026,
	4017,
	3320,
	4055,
	4055,
	2002,
	2594,
	1025,
	1979,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	1245,
	4055,
	3287,
	3287,
	3287,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1623,
	4055,
	3320,
	4055,
	6207,
	4055,
	2887,
	4017,
	4017,
	3965,
	4055,
	4055,
	4055,
	4055,
	4055,
	6104,
	5772,
	6104,
	6104,
	5772,
	6104,
	6207,
	6207,
	6207,
	6178,
	5934,
	6126,
	5960,
	6125,
	5960,
	6102,
	5959,
	6102,
	5755,
	5751,
	5751,
	6101,
	6102,
	5751,
	5755,
	5510,
	6102,
	5758,
	5754,
	5321,
	5340,
	4897,
	5756,
	5755,
	5755,
	5755,
	5755,
	5934,
	4891,
	5339,
	5510,
	5641,
	5960,
	-1,
	5641,
	5960,
	5641,
	5960,
	5641,
	5960,
	5641,
	5960,
	5641,
	5960,
	6101,
	5638,
	5934,
	6123,
	6124,
	5960,
	5960,
	5934,
	5510,
	5934,
	5473,
	5960,
	5960,
	6003,
	5511,
	5314,
	5079,
	6132,
	5960,
	5960,
	6122,
	5934,
	6129,
	5960,
	6133,
	6133,
	6133,
	5934,
	6048,
	6048,
	5934,
	5934,
	6122,
	6127,
	6121,
	5952,
	5952,
	5952,
	5799,
	5756,
	5756,
	5756,
	5799,
	5799,
	5952,
	5952,
	6130,
	5960,
	6131,
	5960,
	5749,
	5960,
	6128,
	6003,
	5511,
	5960,
	5934,
	5499,
	5751,
	5751,
	5321,
	5321,
	5331,
	6102,
	5960,
	5934,
	6064,
	6102,
	5175,
	5509,
	6109,
	6109,
	5757,
	5753,
	5759,
	5753,
	5758,
	6116,
	6102,
	5741,
	6117,
	6102,
	5741,
	6102,
	6104,
	6100,
	5960,
	5960,
	6207,
	6100,
	5187,
	6178,
	6183,
	4055,
	6207,
	1973,
	3273,
	1010,
	3287,
	5031,
	6207,
	4055,
	1305,
	3320,
	4055,
	3356,
	4055,
	2001,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	2029,
	4055,
	4055,
	4055,
	911,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	3287,
	4055,
	2887,
	4055,
	6183,
	4055,
	4055,
	4055,
	4055,
	1442,
	3983,
	2581,
	3983,
	3983,
	2588,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4017,
	4051,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	2028,
	2028,
	3983,
	1307,
	4055,
	4055,
	4055,
	3287,
	3287,
	3287,
	4055,
	1976,
	1976,
	1976,
	4055,
	1840,
	4055,
	4055,
	3287,
	2025,
	4055,
	4055,
	3099,
	3043,
	4017,
	4017,
	4051,
	4023,
	4051,
	4023,
	4023,
	4055,
	4055,
	4055,
	4055,
	2887,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3287,
	4055,
	3287,
	4055,
	4055,
	3287,
	2588,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1816,
	3287,
	3983,
	3983,
	3287,
	3983,
	3287,
	3287,
	1976,
	2588,
	4055,
	2588,
	4055,
	2588,
	2588,
	4055,
	4055,
	3983,
	3983,
	3287,
	4017,
	4017,
	2887,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	1976,
	1976,
	3287,
	3287,
	1976,
	1976,
	1245,
	3287,
	3287,
	1816,
	4055,
	3287,
	1976,
	1976,
	3287,
	4055,
	4055,
	3287,
	1976,
	1976,
	3287,
	3287,
	3287,
	4055,
	4055,
	3287,
	1976,
	4055,
	3271,
	251,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	4055,
	3287,
	3287,
	3287,
	3207,
	4055,
	4055,
	4055,
	6207,
	4055,
	2887,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	6207,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3324,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	3287,
	4055,
	3207,
	4055,
	4055,
	4055,
	4055,
	4055,
	3320,
	4055,
	5200,
	4540,
	4540,
	4537,
	4397,
	5213,
	5208,
	5932,
	6068,
	5940,
	5940,
	5940,
	5722,
	5722,
	5722,
	5712,
	5712,
	6104,
	-1,
	-1,
	3271,
	4055,
	4055,
	4055,
	3287,
	3287,
	3983,
	4055,
	4055,
	3085,
	3922,
	3224,
	3922,
	3224,
	4023,
	3324,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	4055,
	4055,
	6207,
	4055,
	2887,
	4055,
	1976,
	3965,
	3983,
	3965,
	3965,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	1976,
	1133,
	1133,
	1133,
	1133,
	1976,
	4609,
	4055,
	3287,
	2593,
	2593,
	3983,
	1523,
	3971,
	3983,
	1146,
	735,
	3287,
	3287,
	1976,
	5562,
	1559,
	4017,
	4017,
	4055,
	3983,
	1463,
	2590,
	4055,
	1235,
	1151,
	4055,
	6207,
	6207,
	4055,
	2887,
	1376,
	1376,
	4055,
	4055,
	4055,
	4055,
	4055,
	3271,
	3099,
	3099,
	3099,
	3099,
	3983,
	4055,
	3099,
	3099,
	3099,
	1559,
	4023,
	3088,
	3043,
	3965,
	3965,
	3271,
	4023,
	3324,
	3965,
	3271,
	3965,
	3271,
	4023,
	3324,
	4023,
	3965,
	3271,
	4017,
	3320,
	4017,
	3320,
	4017,
	3320,
	4017,
	3320,
	4017,
	3320,
	4017,
	3320,
	3320,
	4017,
	4055,
	3965,
	3271,
	3983,
	1463,
	2590,
	4017,
	3320,
	4023,
	3324,
	4023,
	3324,
	4023,
	4055,
	4017,
	3320,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	4055,
	3287,
	3207,
	4017,
	4055,
	2593,
	739,
	3287,
	1235,
	3287,
	1628,
	4055,
	4017,
	3320,
	4017,
	3320,
	4055,
	4055,
	1973,
	1986,
	657,
	3287,
	1973,
	3287,
	1022,
	3287,
	4055,
	4055,
	4055,
	4055,
	4055,
	3287,
	1648,
	1976,
	4055,
	4055,
	4017,
	3287,
	4055,
	4055,
	4055,
	4055,
	3287,
	3287,
	3983,
	4055,
	4055,
	3085,
	4055,
	3922,
	3224,
	3922,
	3224,
	4023,
	3324,
	4055,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	2001,
	4055,
	1976,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[2] = 
{
	{ 0x06000464, 5,  (void**)&SocialPlatformManager_MicFilter_m0FB5A641DE3104796155D242D62D10DFFB182F54_RuntimeMethod_var, 0 },
	{ 0x06000BC5, 0,  (void**)&CAPI_LoggingCallback_mE9CB96D4CEB343BBBBAA0ACD39A6281C11D4110B_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[13] = 
{
	{ 0x0600001E, { 0, 3 } },
	{ 0x06000484, { 3, 2 } },
	{ 0x0600070C, { 5, 5 } },
	{ 0x0600070D, { 10, 5 } },
	{ 0x0600070E, { 15, 1 } },
	{ 0x0600070F, { 16, 1 } },
	{ 0x06000710, { 17, 1 } },
	{ 0x06000711, { 18, 1 } },
	{ 0x06000712, { 19, 1 } },
	{ 0x06000713, { 20, 1 } },
	{ 0x06000B67, { 21, 1 } },
	{ 0x06000D14, { 22, 3 } },
	{ 0x06000D15, { 25, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[26] = 
{
	{ (Il2CppRGCTXDataType)2, 120 },
	{ (Il2CppRGCTXDataType)3, 29223 },
	{ (Il2CppRGCTXDataType)3, 28766 },
	{ (Il2CppRGCTXDataType)3, 29225 },
	{ (Il2CppRGCTXDataType)2, 236 },
	{ (Il2CppRGCTXDataType)3, 29400 },
	{ (Il2CppRGCTXDataType)1, 675 },
	{ (Il2CppRGCTXDataType)1, 346 },
	{ (Il2CppRGCTXDataType)2, 346 },
	{ (Il2CppRGCTXDataType)2, 675 },
	{ (Il2CppRGCTXDataType)3, 29499 },
	{ (Il2CppRGCTXDataType)3, 29486 },
	{ (Il2CppRGCTXDataType)3, 29503 },
	{ (Il2CppRGCTXDataType)3, 29501 },
	{ (Il2CppRGCTXDataType)3, 29505 },
	{ (Il2CppRGCTXDataType)3, 29490 },
	{ (Il2CppRGCTXDataType)3, 29489 },
	{ (Il2CppRGCTXDataType)3, 29492 },
	{ (Il2CppRGCTXDataType)3, 29491 },
	{ (Il2CppRGCTXDataType)3, 29493 },
	{ (Il2CppRGCTXDataType)3, 29494 },
	{ (Il2CppRGCTXDataType)1, 85 },
	{ (Il2CppRGCTXDataType)3, 29296 },
	{ (Il2CppRGCTXDataType)2, 119 },
	{ (Il2CppRGCTXDataType)3, 29222 },
	{ (Il2CppRGCTXDataType)3, 29003 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	3561,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	2,
	s_reversePInvokeIndices,
	13,
	s_rgctxIndices,
	26,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
