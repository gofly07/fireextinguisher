﻿using UnityEngine;
using System.Collections.Generic;

#if DKP_STEAMVR_2
using Valve.VR;
#endif

namespace DKP.VR
{
    public class HandsVisualization : MonoBehaviour
    {
        [Header("Anchor Info")]
        [SerializeField] private Hand hand = Hand.right;

        [Header("Model")]
        [SerializeField] private List<GameObject> fireExtinguishe = new List<GameObject>();
        [SerializeField] private DisplayMode displayMode = DisplayMode.Controller;
        [SerializeField] private MeshRenderer wire;

        public enum DisplayMode
        {
            Controller,
            Powder,
            CO2,
            Foam,
        }
        private OVRControllerHelper handPose;

        private void Update()
        {
            gameObject.transform.SetPositionAndRotation(handPose.transform.position, handPose.transform.rotation);
        }

        public void ChangeState(DisplayMode dm)
        {
            if (hand == Hand.left)
                handPose = VRPlatformSetting.instance.oVRLeft;
            else if (hand == Hand.right)
                handPose = VRPlatformSetting.instance.oVRRight;

            switch (dm)
            {
                case DisplayMode.Controller:
                    foreach (var i in fireExtinguishe)
                        i.gameObject.SetActive(false);
                    handPose.gameObject.SetActive(true);
                    wire.enabled = false;

                    break;
                case DisplayMode.Powder:
                    foreach (var i in fireExtinguishe)
                        i.gameObject.SetActive(false);
                    handPose.gameObject.SetActive(false);
                    fireExtinguishe[0].gameObject.SetActive(true);
                    wire.enabled = true;
                    break;
                case DisplayMode.CO2:
                    foreach (var i in fireExtinguishe)
                        i.gameObject.SetActive(false);
                    handPose.gameObject.SetActive(false);
                    fireExtinguishe[1].gameObject.SetActive(true);
                    wire.enabled = true;
                    break;
                case DisplayMode.Foam:
                    foreach (var i in fireExtinguishe)
                        i.gameObject.SetActive(false);
                    handPose.gameObject.SetActive(false);
                    fireExtinguishe[2].gameObject.SetActive(true);
                    wire.enabled = true;
                    break;
            }
        }
    }
}