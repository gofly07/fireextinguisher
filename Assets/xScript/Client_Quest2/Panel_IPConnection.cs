﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Panel_IPConnection : MonoBehaviour
{
    //[SerializeField] private FMNetworkManager fMNetworkManager;

    [Header("IP Paramenters")]
    [SerializeField] private InputField ifIP;
    [SerializeField] private Button btOK;
    [SerializeField] private RectTransform rtKeyboard;
    [SerializeField] private List<Button> btNumbers = new List<Button>();
    [SerializeField] private Button btDot;
    [SerializeField] private Button btBack;

    [Header("Connecting Paramenters")]
    [SerializeField] private Image imgLoading;
    [SerializeField] private Text txtRemind;

    public event EventHandler EHOpenBlueToothPanel;

    private void OnEnable()
    {
        Init();
        btOK.onClick.AddListener(delegate { ClickOK(); });

        foreach (var i in btNumbers)
            i.onClick.AddListener(delegate { ClickNum(i); });

        btDot.onClick.AddListener(delegate { ClickDot(); });
        btBack.onClick.AddListener(delegate { ClickBack(); });
    }

    private void Init()
    {
        rtKeyboard.gameObject.SetActive(true);
        imgLoading.gameObject.SetActive(false);
        txtRemind.gameObject.SetActive(false);
    }

    private void ClickOK()
    {
        StopAllCoroutines();
        StartCoroutine(Connect());
    }

    private IEnumerator Connect()
    {
        btOK.gameObject.SetActive(false);
        rtKeyboard.gameObject.SetActive(false);
        FMNetworkManager.instance.enabled = false;
        yield return null;

        //Fake Waitting...
        imgLoading.gameObject.SetActive(true);
        txtRemind.gameObject.SetActive(true);
        txtRemind.color = new Color32(57, 97, 121, 255);

        FMNetworkManager.instance.ClientSettings.ServerIP = ifIP.text;
        FMNetworkManager.instance.enabled = true;
        FMNetworkManager.instance.Action_InitAsClient();

        int waitTime = 3;
        while (true)
        {
            txtRemind.text = string.Format("等待連線...({0})", waitTime);
            yield return new WaitForSeconds(1);

            FMNetworkManager.instance.SendToServer(ifIP.text.ToString());//add

            waitTime--;
            //Sucess
            if (FMNetworkManager.instance.Client.FoundServer && FMNetworkManager.instance.Client.IsConnected && UserIDCheck.userID != "")
            {
                //Fake Waitting...
                imgLoading.gameObject.SetActive(false);
                txtRemind.color = Color.green;

                for (int i = 3; i >= 0; i--)
                {
                    txtRemind.text = string.Format("連線中({0})", i);
                    yield return new WaitForSeconds(1);
                }
                txtRemind.gameObject.SetActive(false);

                EHOpenBlueToothPanel(this, EventArgs.Empty);
                break;
            }
            //Failed
            else if (waitTime <= 0)
            {
                //Fake Waitting...
                imgLoading.gameObject.SetActive(false);
                txtRemind.color = Color.red;

                for (int i = 3; i >= 0; i--)
                {
                    txtRemind.text = string.Format("IP確認失敗，請重新輸入...({0})", i);
                    yield return new WaitForSeconds(1);
                }
                txtRemind.gameObject.SetActive(false);

                btOK.gameObject.SetActive(true);
                rtKeyboard.gameObject.SetActive(true);
                break;
            }
        }
    }

    private void ClickNum(Button num)
    {
        ifIP.text = ifIP.text + num.GetComponentInChildren<Text>().text;
    }

    private void ClickDot()
    {
        ifIP.text = ifIP.text + ".";
    }

    private void ClickBack()
    {
        if (ifIP.text.Length > 0)
            ifIP.text = ifIP.text.Remove(ifIP.text.Length - 1, 1);
    }
}
