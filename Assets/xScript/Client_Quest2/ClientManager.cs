﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static GPTT.Data.API;

public class ClientManager : MonoBehaviour
{
    [Header("Streaming Paramenters")]
    [SerializeField] private List<GameObject> dontDestroyObjects = new List<GameObject>();
    private GameObject temp;
    private static bool isHaveClone = false;
    [SerializeField] private Transform target;

    [Header("Fire Extinguisher Changer")]
    public DKP.VR.HandsVisualization handsVisualizationL;
    public DKP.VR.HandsVisualization handsVisualizationR;

    public static ClientManager instance = new ClientManager();

    private void Start()
    {
        instance = this;
        StartCoroutine(gameObject.GetComponent<ClientManagerUI>().Init());

        //第一次進入
        if (!isHaveClone)
        {
            foreach (var i in dontDestroyObjects)
            {
                temp = Instantiate(i);
                DontDestroyOnLoad(temp);
            }
            ClientManager.instance.SwitchFireExtinguisher(DKP.VR.HandsVisualization.DisplayMode.Controller);
            isHaveClone = true;
        }
        //從練習場景回來
        else if (isHaveClone)
        {
            gameObject.GetComponent<ClientManagerUI>().BackToClientSceneFromTestingScene();
            ClientManager.instance.SwitchFireExtinguisher(DKP.VR.HandsVisualization.DisplayMode.Powder);
            Destroy(GameObject.Find("LaserRay_l"));
        }
    }

    public void SwitchFireExtinguisher(DKP.VR.HandsVisualization.DisplayMode displayMode)
    {
        handsVisualizationL.ChangeState(displayMode);
        handsVisualizationR.ChangeState(displayMode);
    }

    private void Update()
    {
        if (FMNetworkManager.instance.isActiveAndEnabled)
        {
            FMNetworkManager.instance.gameObject.transform.SetPositionAndRotation(target.position, target.rotation);
            FMNetworkManager.instance.gameObject.transform.GetChild(1).GetComponent<Canvas>().worldCamera = target.GetComponent<Camera>();
        }
    }
}