﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32>
struct Dictionary_2_tCB10D3F0D8D28A1A6B54347C56E10FE9D9612C94;
// System.Collections.Generic.Dictionary`2<System.Int32Enum,FMTurboJpegWrapper.TjSize>
struct Dictionary_2_tB0D355938417F3178DFCA5B4EA676050F4274356;
// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32>
struct Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52;
// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>
struct Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F;
// System.Collections.Generic.IEqualityComparer`1<FMTurboJpegWrapper.TJPixelFormat>
struct IEqualityComparer_1_tE5A40B919ABAAFBC27F667312B0144BA69858BA1;
// System.Collections.Generic.IEqualityComparer`1<FMTurboJpegWrapper.TJSubsamplingOption>
struct IEqualityComparer_1_tA9B7CE202F080DFD2EFF38F00FBCFBDA3A289784;
// System.Collections.Generic.Dictionary`2/KeyCollection<FMTurboJpegWrapper.TJPixelFormat,System.Int32>
struct KeyCollection_t53AD29D554B2B25105722C0471BF534C39963B46;
// System.Collections.Generic.Dictionary`2/KeyCollection<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>
struct KeyCollection_tF2910FDBB72538DCF7DA3B2DFD5053E3BE438D36;
// System.Collections.Generic.Dictionary`2/ValueCollection<FMTurboJpegWrapper.TJPixelFormat,System.Int32>
struct ValueCollection_t8F188F924A1B7F912757B55DAAB9CC5DFC8A481C;
// System.Collections.Generic.Dictionary`2/ValueCollection<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>
struct ValueCollection_t5A9FDF14CB7A4811A97AC0E861423385D86451C7;
// System.Collections.Generic.Dictionary`2/Entry<FMTurboJpegWrapper.TJPixelFormat,System.Int32>[]
struct EntryU5BU5D_t2BD7EBCA86F7BE599F441A1F3BB456156F5F2455;
// System.Collections.Generic.Dictionary`2/Entry<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>[]
struct EntryU5BU5D_t50E4E76A38BFFDDF9DA00C5B1367C076F1BCC945;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8;
// FMTurboJpegWrapper.DecompressedImage
struct DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.InvalidOperationException
struct InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// System.ObjectDisposedException
struct ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// FMTurboJpegWrapper.TJCompressor
struct TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542;
// FMTurboJpegWrapper.TJDecompressor
struct TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GC_tD6F0377620BF01385965FD29272CF088A4309C0D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Marshal_tEBAFAE20369FCB1B38C49C4E27A8D8C2C4B55058_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TJPixelFormat_t74413E64CD0264BDB07C1776A05E5BA122A5F226_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TJSubsamplingOption_t8A94ECD53B69C6FF1DE00E6F939EC434246110BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral20F5317256971762AB556CA5842E20C59A160EA1;
IL2CPP_EXTERN_C String_t* _stringLiteralD525221FF38EAF1A30491622A0B39D5D960A7815;
IL2CPP_EXTERN_C String_t* _stringLiteralEF5C314A85CE0407A185D572BE5BB2C388BE9D17;
IL2CPP_EXTERN_C String_t* _stringLiteralF2901696E61B6DBCAC0495A6584065E29A70C5DA;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m70ACBD7D6FBC4F2E8319EAF1D1DEA10602E5224C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mB2214F2816AD1BA65732FFE1CF32C6BF2CF6289C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mDB083C6F06C633E1928B8B9FD1313D2BD55BE8CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TJCompressor_CheckOptionsCompatibilityAndThrow_m2B09B024F4D0D0C1F9B4800CED86E353E453032B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TJCompressor_Compress_m928E468B43ECEB3E6083AC8684F39347BCB90849_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TJDecompressor_Decompress_m39C66057ABFB7B90C028F1374BEF01D5452B3EC2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TJDecompressor_Decompress_m3DEC0F0E4A220B42DFD7D38D38B0389F44445C75_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TJDecompressor_Decompress_m80B39EB731429D27BACDFB17B8B977ADA76651CC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TurboJpegImport_TjDecompressHeader_m1C70E63777A15B304A78A5E262481DA984B152A5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TurboJpegImport_TjDecompress_m591B0598027516C484448BE035AB9E75D5B60471_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t8FF0D70ACBF69E95B9653C09B6A6F7D28EF77839 
{
public:

public:
};


// System.Object


// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32>
struct Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t2BD7EBCA86F7BE599F441A1F3BB456156F5F2455* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t53AD29D554B2B25105722C0471BF534C39963B46 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t8F188F924A1B7F912757B55DAAB9CC5DFC8A481C * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52, ___entries_1)); }
	inline EntryU5BU5D_t2BD7EBCA86F7BE599F441A1F3BB456156F5F2455* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t2BD7EBCA86F7BE599F441A1F3BB456156F5F2455** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t2BD7EBCA86F7BE599F441A1F3BB456156F5F2455* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52, ___keys_7)); }
	inline KeyCollection_t53AD29D554B2B25105722C0471BF534C39963B46 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t53AD29D554B2B25105722C0471BF534C39963B46 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t53AD29D554B2B25105722C0471BF534C39963B46 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52, ___values_8)); }
	inline ValueCollection_t8F188F924A1B7F912757B55DAAB9CC5DFC8A481C * get_values_8() const { return ___values_8; }
	inline ValueCollection_t8F188F924A1B7F912757B55DAAB9CC5DFC8A481C ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t8F188F924A1B7F912757B55DAAB9CC5DFC8A481C * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>
struct Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t50E4E76A38BFFDDF9DA00C5B1367C076F1BCC945* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tF2910FDBB72538DCF7DA3B2DFD5053E3BE438D36 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t5A9FDF14CB7A4811A97AC0E861423385D86451C7 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F, ___entries_1)); }
	inline EntryU5BU5D_t50E4E76A38BFFDDF9DA00C5B1367C076F1BCC945* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t50E4E76A38BFFDDF9DA00C5B1367C076F1BCC945** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t50E4E76A38BFFDDF9DA00C5B1367C076F1BCC945* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F, ___keys_7)); }
	inline KeyCollection_tF2910FDBB72538DCF7DA3B2DFD5053E3BE438D36 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tF2910FDBB72538DCF7DA3B2DFD5053E3BE438D36 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tF2910FDBB72538DCF7DA3B2DFD5053E3BE438D36 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F, ___values_8)); }
	inline ValueCollection_t5A9FDF14CB7A4811A97AC0E861423385D86451C7 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t5A9FDF14CB7A4811A97AC0E861423385D86451C7 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t5A9FDF14CB7A4811A97AC0E861423385D86451C7 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// FMExtensionMethods
struct FMExtensionMethods_t94AE7DDB5123D5E8FC90332214459AF364B1CAF9  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// FMTurboJpegWrapper.TJUtils
struct TJUtils_tA943F50277318B86BCFF8C2F67994FA81A1D4BA8  : public RuntimeObject
{
public:

public:
};


// FMTurboJpegWrapper.TurboJpegImport
struct TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A  : public RuntimeObject
{
public:

public:
};

struct TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32> FMTurboJpegWrapper.TurboJpegImport::PixelSizes
	Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * ___PixelSizes_0;
	// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize> FMTurboJpegWrapper.TurboJpegImport::MCUSizes
	Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * ___MCUSizes_1;
	// System.Boolean FMTurboJpegWrapper.TurboJpegImport::_LibraryFound
	bool ____LibraryFound_2;

public:
	inline static int32_t get_offset_of_PixelSizes_0() { return static_cast<int32_t>(offsetof(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_StaticFields, ___PixelSizes_0)); }
	inline Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * get_PixelSizes_0() const { return ___PixelSizes_0; }
	inline Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 ** get_address_of_PixelSizes_0() { return &___PixelSizes_0; }
	inline void set_PixelSizes_0(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * value)
	{
		___PixelSizes_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PixelSizes_0), (void*)value);
	}

	inline static int32_t get_offset_of_MCUSizes_1() { return static_cast<int32_t>(offsetof(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_StaticFields, ___MCUSizes_1)); }
	inline Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * get_MCUSizes_1() const { return ___MCUSizes_1; }
	inline Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F ** get_address_of_MCUSizes_1() { return &___MCUSizes_1; }
	inline void set_MCUSizes_1(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * value)
	{
		___MCUSizes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MCUSizes_1), (void*)value);
	}

	inline static int32_t get_offset_of__LibraryFound_2() { return static_cast<int32_t>(offsetof(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_StaticFields, ____LibraryFound_2)); }
	inline bool get__LibraryFound_2() const { return ____LibraryFound_2; }
	inline bool* get_address_of__LibraryFound_2() { return &____LibraryFound_2; }
	inline void set__LibraryFound_2(bool value)
	{
		____LibraryFound_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// FMTurboJpegWrapper.TjSize
struct TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 
{
public:
	// System.Int32 FMTurboJpegWrapper.TjSize::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_0;
	// System.Int32 FMTurboJpegWrapper.TjSize::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3, ___U3CWidthU3Ek__BackingField_0)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_0() const { return ___U3CWidthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_0() { return &___U3CWidthU3Ek__BackingField_0; }
	inline void set_U3CWidthU3Ek__BackingField_0(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3, ___U3CHeightU3Ek__BackingField_1)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_1() const { return ___U3CHeightU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_1() { return &___U3CHeightU3Ek__BackingField_1; }
	inline void set_U3CHeightU3Ek__BackingField_1(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_1 = value;
	}
};


// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.UInt64
struct UInt64_tEC57511B3E3CA2DBA1BEBD434C6983E31C943281 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_tEC57511B3E3CA2DBA1BEBD434C6983E31C943281, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// FMChromaSubsamplingOption
struct FMChromaSubsamplingOption_tD4AB367B5D544756EFD9AC7EFE65AA18AF801AB7 
{
public:
	// System.Int32 FMChromaSubsamplingOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FMChromaSubsamplingOption_tD4AB367B5D544756EFD9AC7EFE65AA18AF801AB7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Int32Enum
struct Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// FMTurboJpegWrapper.TJCompressor
struct TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542  : public RuntimeObject
{
public:
	// System.Object FMTurboJpegWrapper.TJCompressor::lock
	RuntimeObject * ___lock_0;
	// System.IntPtr FMTurboJpegWrapper.TJCompressor::compressorHandle
	intptr_t ___compressorHandle_1;
	// System.Boolean FMTurboJpegWrapper.TJCompressor::isDisposed
	bool ___isDisposed_2;

public:
	inline static int32_t get_offset_of_lock_0() { return static_cast<int32_t>(offsetof(TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542, ___lock_0)); }
	inline RuntimeObject * get_lock_0() const { return ___lock_0; }
	inline RuntimeObject ** get_address_of_lock_0() { return &___lock_0; }
	inline void set_lock_0(RuntimeObject * value)
	{
		___lock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lock_0), (void*)value);
	}

	inline static int32_t get_offset_of_compressorHandle_1() { return static_cast<int32_t>(offsetof(TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542, ___compressorHandle_1)); }
	inline intptr_t get_compressorHandle_1() const { return ___compressorHandle_1; }
	inline intptr_t* get_address_of_compressorHandle_1() { return &___compressorHandle_1; }
	inline void set_compressorHandle_1(intptr_t value)
	{
		___compressorHandle_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}
};


// FMTurboJpegWrapper.TJDecompressor
struct TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC  : public RuntimeObject
{
public:
	// System.Object FMTurboJpegWrapper.TJDecompressor::lock
	RuntimeObject * ___lock_0;
	// System.IntPtr FMTurboJpegWrapper.TJDecompressor::decompressorHandle
	intptr_t ___decompressorHandle_1;
	// System.Boolean FMTurboJpegWrapper.TJDecompressor::isDisposed
	bool ___isDisposed_2;

public:
	inline static int32_t get_offset_of_lock_0() { return static_cast<int32_t>(offsetof(TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC, ___lock_0)); }
	inline RuntimeObject * get_lock_0() const { return ___lock_0; }
	inline RuntimeObject ** get_address_of_lock_0() { return &___lock_0; }
	inline void set_lock_0(RuntimeObject * value)
	{
		___lock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lock_0), (void*)value);
	}

	inline static int32_t get_offset_of_decompressorHandle_1() { return static_cast<int32_t>(offsetof(TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC, ___decompressorHandle_1)); }
	inline intptr_t get_decompressorHandle_1() const { return ___decompressorHandle_1; }
	inline intptr_t* get_address_of_decompressorHandle_1() { return &___decompressorHandle_1; }
	inline void set_decompressorHandle_1(intptr_t value)
	{
		___decompressorHandle_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}
};


// FMTurboJpegWrapper.TJFlags
struct TJFlags_tC5108FCE11C6D4F23960030F270D79218DEFEB80 
{
public:
	// System.Int32 FMTurboJpegWrapper.TJFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TJFlags_tC5108FCE11C6D4F23960030F270D79218DEFEB80, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMTurboJpegWrapper.TJPixelFormat
struct TJPixelFormat_t74413E64CD0264BDB07C1776A05E5BA122A5F226 
{
public:
	// System.Int32 FMTurboJpegWrapper.TJPixelFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TJPixelFormat_t74413E64CD0264BDB07C1776A05E5BA122A5F226, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMTurboJpegWrapper.TJSubsamplingOption
struct TJSubsamplingOption_t8A94ECD53B69C6FF1DE00E6F939EC434246110BC 
{
public:
	// System.Int32 FMTurboJpegWrapper.TJSubsamplingOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TJSubsamplingOption_t8A94ECD53B69C6FF1DE00E6F939EC434246110BC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureFormat
struct TextureFormat_tBED5388A0445FE978F97B41D247275B036407932 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_tBED5388A0445FE978F97B41D247275B036407932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMTurboJpegWrapper.DecompressedImage
struct DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA  : public RuntimeObject
{
public:
	// FMTurboJpegWrapper.TJPixelFormat FMTurboJpegWrapper.DecompressedImage::<PixelFormat>k__BackingField
	int32_t ___U3CPixelFormatU3Ek__BackingField_0;
	// System.Int32 FMTurboJpegWrapper.DecompressedImage::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_1;
	// System.Int32 FMTurboJpegWrapper.DecompressedImage::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_2;
	// System.Int32 FMTurboJpegWrapper.DecompressedImage::<Stride>k__BackingField
	int32_t ___U3CStrideU3Ek__BackingField_3;
	// System.Byte[] FMTurboJpegWrapper.DecompressedImage::<Data>k__BackingField
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___U3CDataU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CPixelFormatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA, ___U3CPixelFormatU3Ek__BackingField_0)); }
	inline int32_t get_U3CPixelFormatU3Ek__BackingField_0() const { return ___U3CPixelFormatU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CPixelFormatU3Ek__BackingField_0() { return &___U3CPixelFormatU3Ek__BackingField_0; }
	inline void set_U3CPixelFormatU3Ek__BackingField_0(int32_t value)
	{
		___U3CPixelFormatU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA, ___U3CWidthU3Ek__BackingField_1)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_1() const { return ___U3CWidthU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_1() { return &___U3CWidthU3Ek__BackingField_1; }
	inline void set_U3CWidthU3Ek__BackingField_1(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA, ___U3CHeightU3Ek__BackingField_2)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_2() const { return ___U3CHeightU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_2() { return &___U3CHeightU3Ek__BackingField_2; }
	inline void set_U3CHeightU3Ek__BackingField_2(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStrideU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA, ___U3CStrideU3Ek__BackingField_3)); }
	inline int32_t get_U3CStrideU3Ek__BackingField_3() const { return ___U3CStrideU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CStrideU3Ek__BackingField_3() { return &___U3CStrideU3Ek__BackingField_3; }
	inline void set_U3CStrideU3Ek__BackingField_3(int32_t value)
	{
		___U3CStrideU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA, ___U3CDataU3Ek__BackingField_4)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_U3CDataU3Ek__BackingField_4() const { return ___U3CDataU3Ek__BackingField_4; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_U3CDataU3Ek__BackingField_4() { return &___U3CDataU3Ek__BackingField_4; }
	inline void set_U3CDataU3Ek__BackingField_4(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___U3CDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDataU3Ek__BackingField_4), (void*)value);
	}
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.InvalidOperationException
struct InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_actualValue_19), (void*)value);
	}
};

struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rangeMessage_18), (void*)value);
	}
};


// System.ObjectDisposedException
struct ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A  : public InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB
{
public:
	// System.String System.ObjectDisposedException::objectName
	String_t* ___objectName_17;

public:
	inline static int32_t get_offset_of_objectName_17() { return static_cast<int32_t>(offsetof(ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A, ___objectName_17)); }
	inline String_t* get_objectName_17() const { return ___objectName_17; }
	inline String_t** get_address_of_objectName_17() { return &___objectName_17; }
	inline void set_objectName_17(String_t* value)
	{
		___objectName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectName_17), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// !1 System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Item_mA931B0C8790EF07D8F8AC3F328DE919A753EF46E_gshared (Dictionary_2_tCB10D3F0D8D28A1A6B54347C56E10FE9D9612C94 * __this, int32_t ___key0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_mAD69B022F1D23A2262CA05C4D32C31DA1CF7FE92_gshared (Dictionary_2_tCB10D3F0D8D28A1A6B54347C56E10FE9D9612C94 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m966E300F7A36F73697CCDA7F3534BD90FFB029AF_gshared (Dictionary_2_tCB10D3F0D8D28A1A6B54347C56E10FE9D9612C94 * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,FMTurboJpegWrapper.TjSize>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C5364F1C9BCD07C8183AEF0C672C2F7B2C39E1B_gshared (Dictionary_2_tB0D355938417F3178DFCA5B4EA676050F4274356 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,FMTurboJpegWrapper.TjSize>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_mEC4061A7DDEE869B75E0C401AB7CBAD39BE8EEAF_gshared (Dictionary_2_tB0D355938417F3178DFCA5B4EA676050F4274356 * __this, int32_t ___key0, TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3  ___value1, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJCompressor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor__ctor_m2BE57DF352ADAB1D70B3CBB7C48615E46D102780 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, const RuntimeMethod* method);
// FMTurboJpegWrapper.TJSubsamplingOption FMExtensionMethods::GetTJSubsampligOption(FMChromaSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMExtensionMethods_GetTJSubsampligOption_m74FD4BA6471F91419E3E139DFD8815E7C2CC0D1F (int32_t ___Subsampling0, const RuntimeMethod* method);
// System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(UnityEngine.Texture2D,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TJCompressor_EncodeFMJPG_m04A1BB9FBAE993BE6DABC415BBB01574C24716A1 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___texture0, int32_t ___Quality1, int32_t ___TJSubsampling2, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJCompressor::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_Dispose_mEF9EFCA1BCC301303AECF2137C940FF92E152B85 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (RuntimeObject * ___message0, const RuntimeMethod* method);
// UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Texture2D_get_format_mCBCE13524A94042693822BDDE112990B25F4F8E4 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJDecompressor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor__ctor_m9DDCFB672B44DA49F610E0E5232F288EA50AAA34 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, const RuntimeMethod* method);
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::DecodeFMJPG(System.Byte[],UnityEngine.TextureFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * TJDecompressor_DecodeFMJPG_m6CC0B007AA65465F562ACE401F2D6129022341E1 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___jpegBuf0, int32_t ___format1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Width()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Width_mD99C0D51A23DFE6043BFA0124864922153892CAF_inline (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Height()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Height_m9BBC5D9D1CFF42D53D088026465248DFD6400D83_inline (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Byte[] FMTurboJpegWrapper.DecompressedImage::get_Data()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* DecompressedImage_get_Data_m5845759BA644DDCB0A686751B401A22149211BF9_inline (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::LoadRawTextureData(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_LoadRawTextureData_m93A620CC97332F351305E3A93AD11CB2E0EFDAF4 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_m3BB3975288119BA62ED9BE4243F7767EC2F88CA0 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJDecompressor::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Dispose_m918F5429F1097880B5945F8587E9B0017A37F2E1 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, const RuntimeMethod* method);
// System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(System.Byte[],System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TJCompressor_EncodeFMJPG_mB6286123E7EC12E8C34EFF1B0D772F97ACC9D561 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____RawTextureData0, int32_t ____width1, int32_t ____height2, int32_t ____format3, int32_t ___Quality4, int32_t ___TJSubsampling5, const RuntimeMethod* method);
// System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitCompress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t TurboJpegImport_TjInitCompress_m0E11173E0A9EC8190333E8810615BAA1260B7261 (const RuntimeMethod* method);
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Equality_mD94F3FE43A65684EFF984A7B95E70D2520C0AC73 (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJUtils::GetErrorAndThrow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJUtils_GetErrorAndThrow_m116A7140103D97D986F589FC565B881F9ED3E046 (const RuntimeMethod* method);
// System.Void System.Object::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Finalize_mC59C83CF4F7707E425FFA6362931C25D4C36676A (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.ObjectDisposedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectDisposedException__ctor_mE57C6A61713668708F9B3CEF060A8D006B1FE880 (ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A * __this, String_t* ___objectName0, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJCompressor::CheckOptionsCompatibilityAndThrow(FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TJPixelFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_CheckOptionsCompatibilityAndThrow_m2B09B024F4D0D0C1F9B4800CED86E353E453032B (int32_t ___subSamp0, int32_t ___srcFormat1, const RuntimeMethod* method);
// System.IntPtr System.IntPtr::op_Explicit(System.Void*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t IntPtr_op_Explicit_mBD40223EE90BDDF40A24C0F321D3398DEA300495 (void* ___value0, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjCompress2(System.IntPtr,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.IntPtr&,System.UInt64&,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjCompress2_m153EB5525C48045D5CF86EC1A74B66714C6DD65E (intptr_t ___handle0, intptr_t ___srcBuf1, int32_t ___width2, int32_t ___pitch3, int32_t ___height4, int32_t ___pixelFormat5, intptr_t* ___jpegBuf6, uint64_t* ___jpegSize7, int32_t ___jpegSubsamp8, int32_t ___jpegQual9, int32_t ___flags10, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Marshal_Copy_m057A8067BF7212A361510EA26B24022990A07AC0 (intptr_t ___source0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___destination1, int32_t ___startIndex2, int32_t ___length3, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TurboJpegImport::TjFree(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurboJpegImport_TjFree_mAEF709FF49044B6A1021411427D48CA7841F35FC (intptr_t ___buffer0, const RuntimeMethod* method);
// System.Byte[] UnityEngine.Texture2D::GetRawTextureData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* Texture2D_GetRawTextureData_m60C0B5EF034F31FE1824B31AC1DE71042E2ACB55 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, const RuntimeMethod* method);
// System.Byte[] FMTurboJpegWrapper.TJCompressor::Compress(System.Byte[],System.Int32,System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJSubsamplingOption,System.Int32,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TJCompressor_Compress_m928E468B43ECEB3E6083AC8684F39347BCB90849 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___srcBuf0, int32_t ___stride1, int32_t ___width2, int32_t ___height3, int32_t ___tjPixelFormat4, int32_t ___subSamp5, int32_t ___quality6, int32_t ___flags7, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_m3AEE1F76020B92B6C2742BCD05706DC5FD6F9CB2 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.GC::SuppressFinalize(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GC_SuppressFinalize_mEE880E988C6AF32AA2F67F2D62715281EAA41555 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Inequality_m212AF0E66AA81FEDC982B1C8A44ADDA24B995EB8 (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDestroy(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDestroy_m133702A671B7275E7B1C1F096A4DC2983AFC6184 (intptr_t ___handle0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66 (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90 (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitDecompress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t TurboJpegImport_TjInitDecompress_m249156F548208F11CE0EB14918ED99F1820AA97C (const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJDecompressor::GetImageInfo(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_GetImageInfo_m1BCAA6AC6AB869AACC1EAD4C29D9E01E6B27E5C3 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t* ___width3, int32_t* ___height4, int32_t* ___stride5, int32_t* ___bufSize6, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,System.IntPtr,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Decompress_m39C66057ABFB7B90C028F1374BEF01D5452B3EC2 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, intptr_t ___outBuf2, int32_t ___outBufSize3, int32_t ___destPixelFormat4, int32_t ___flags5, int32_t* ___width6, int32_t* ___height7, int32_t* ___stride8, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader_m1C70E63777A15B304A78A5E262481DA984B152A5 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32>::get_Item(!0)
inline int32_t Dictionary_2_get_Item_mDB083C6F06C633E1928B8B9FD1313D2BD55BE8CF (Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_mA931B0C8790EF07D8F8AC3F328DE919A753EF46E_gshared)(__this, ___key0, method);
}
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress_m591B0598027516C484448BE035AB9E75D5B60471 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method);
// System.Byte[] FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TJDecompressor_Decompress_m3A19D8ABEFBC56FEF69012A80F78D4480D070A2E (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t ___flags3, int32_t* ___width4, int32_t* ___height5, int32_t* ___stride6, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.DecompressedImage::.ctor(System.Int32,System.Int32,System.Int32,System.Byte[],FMTurboJpegWrapper.TJPixelFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DecompressedImage__ctor_mEA72381F39D50F44CB114784B95B02B9B10007B4 (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, int32_t ___width0, int32_t ___height1, int32_t ___stride2, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data3, int32_t ___pixelFormat4, const RuntimeMethod* method);
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * TJDecompressor_Decompress_m3DEC0F0E4A220B42DFD7D38D38B0389F44445C75 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t ___flags3, const RuntimeMethod* method);
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.Byte[],FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * TJDecompressor_Decompress_m80B39EB731429D27BACDFB17B8B977ADA76651CC (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___jpegBuf0, int32_t ___destPixelFormat1, int32_t ___flags2, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TjSize::set_Width(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TjSize_set_Width_m40DB05B6594E8FB81CD072C94EA99F619F12B648_inline (TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TjSize::set_Height(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TjSize_set_Height_mA6BFA2A673A5796D6E5FD2623EBA70D2EB90AD98_inline (TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TjSize::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6 (TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method);
// System.Int32 System.IntPtr::get_Size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t IntPtr_get_Size_mD8038A1C440DE87E685F4D879DC80A6704D9C77B (const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x86(System.IntPtr,System.IntPtr,System.UInt32,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader3_x86_m1CC33DD0404AF9F552013DA9CBE510B6BD9457F7 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint32_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x64(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader3_x64_mD6E45CE0657F44D84480954738D2FE6A833935B9 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x86(System.IntPtr,System.IntPtr,System.UInt32,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress2_x86_mA296A566DD1B56EF26610DC67299E2D58ADC8842 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint32_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x64(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress2_x64_m7D71BE1DAD8A4013D134CD0683E3300A3515BBFC (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32>::.ctor()
inline void Dictionary_2__ctor_mB2214F2816AD1BA65732FFE1CF32C6BF2CF6289C (Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 *, const RuntimeMethod*))Dictionary_2__ctor_mAD69B022F1D23A2262CA05C4D32C31DA1CF7FE92_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32>::Add(!0,!1)
inline void Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F (Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 *, int32_t, int32_t, const RuntimeMethod*))Dictionary_2_Add_m966E300F7A36F73697CCDA7F3534BD90FFB029AF_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>::.ctor()
inline void Dictionary_2__ctor_m70ACBD7D6FBC4F2E8319EAF1D1DEA10602E5224C (Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F *, const RuntimeMethod*))Dictionary_2__ctor_m2C5364F1C9BCD07C8183AEF0C672C2F7B2C39E1B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>::Add(!0,!1)
inline void Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F (Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * __this, int32_t ___key0, TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3  ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F *, int32_t, TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 , const RuntimeMethod*))Dictionary_2_Add_mEC4061A7DDEE869B75E0C401AB7CBAD39BE8EEAF_gshared)(__this, ___key0, ___value1, method);
}
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL tjInitCompress();
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL tjCompress2(intptr_t, intptr_t, int32_t, int32_t, int32_t, int32_t, intptr_t*, uint64_t*, int32_t, int32_t, int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL tjInitDecompress();
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL tjFree(intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL tjDestroy(intptr_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL tjDecompressHeader3(intptr_t, intptr_t, uint32_t, int32_t*, int32_t*, int32_t*, int32_t*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL tjDecompress2(intptr_t, intptr_t, uint32_t, intptr_t, int32_t, int32_t, int32_t, int32_t, int32_t);
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMTurboJpegWrapper.DecompressedImage::.ctor(System.Int32,System.Int32,System.Int32,System.Byte[],FMTurboJpegWrapper.TJPixelFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DecompressedImage__ctor_mEA72381F39D50F44CB114784B95B02B9B10007B4 (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, int32_t ___width0, int32_t ___height1, int32_t ___stride2, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data3, int32_t ___pixelFormat4, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		__this->set_U3CWidthU3Ek__BackingField_1(L_0);
		int32_t L_1 = ___height1;
		__this->set_U3CHeightU3Ek__BackingField_2(L_1);
		int32_t L_2 = ___stride2;
		__this->set_U3CStrideU3Ek__BackingField_3(L_2);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = ___data3;
		__this->set_U3CDataU3Ek__BackingField_4(L_3);
		int32_t L_4 = ___pixelFormat4;
		__this->set_U3CPixelFormatU3Ek__BackingField_0(L_4);
		return;
	}
}
// System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Width_mD99C0D51A23DFE6043BFA0124864922153892CAF (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CWidthU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Height_m9BBC5D9D1CFF42D53D088026465248DFD6400D83 (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CHeightU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Byte[] FMTurboJpegWrapper.DecompressedImage::get_Data()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* DecompressedImage_get_Data_m5845759BA644DDCB0A686751B401A22149211BF9 (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_U3CDataU3Ek__BackingField_4();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] FMExtensionMethods::FMEncodeToJPG(UnityEngine.Texture2D,System.Int32,FMChromaSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* FMExtensionMethods_FMEncodeToJPG_mF34C6EA42568D795E3907398B0805642671FC990 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___texture0, int32_t ___Quality1, int32_t ___Subsampling2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * V_0 = NULL;
	{
		TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * L_0 = (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 *)il2cpp_codegen_object_new(TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542_il2cpp_TypeInfo_var);
		TJCompressor__ctor_m2BE57DF352ADAB1D70B3CBB7C48615E46D102780(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * L_1 = V_0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_2 = ___texture0;
		int32_t L_3 = ___Quality1;
		int32_t L_4 = ___Subsampling2;
		int32_t L_5;
		L_5 = FMExtensionMethods_GetTJSubsampligOption_m74FD4BA6471F91419E3E139DFD8815E7C2CC0D1F(L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6;
		L_6 = TJCompressor_EncodeFMJPG_m04A1BB9FBAE993BE6DABC415BBB01574C24716A1(L_1, L_2, L_3, L_5, /*hidden argument*/NULL);
		TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * L_7 = V_0;
		NullCheck(L_7);
		TJCompressor_Dispose_mEF9EFCA1BCC301303AECF2137C940FF92E152B85(L_7, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void FMExtensionMethods::FMLoadJPG(UnityEngine.Texture2D,UnityEngine.Texture2D&,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FMExtensionMethods_FMLoadJPG_m6F80A4605FECD0D7A6FD7C9088363FD490A286BE (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___texture0, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** ___ref_texture1, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bytes2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral20F5317256971762AB556CA5842E20C59A160EA1);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * V_1 = NULL;
	int32_t G_B5_0 = 0;
	TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * G_B7_0 = NULL;
	TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * G_B6_0 = NULL;
	TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * G_B10_0 = NULL;
	TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * G_B9_0 = NULL;
	TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * G_B8_0 = NULL;
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = ___texture0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** L_1 = ___ref_texture1;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_2 = *((Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF **)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteral20F5317256971762AB556CA5842E20C59A160EA1, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_4 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0021;
		}
	}
	{
		G_B5_0 = 4;
		goto IL_0027;
	}

IL_0021:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_6 = ___texture0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = Texture2D_get_format_mCBCE13524A94042693822BDDE112990B25F4F8E4(L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
	}

IL_0027:
	{
		V_0 = G_B5_0;
		TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * L_8 = (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC *)il2cpp_codegen_object_new(TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC_il2cpp_TypeInfo_var);
		TJDecompressor__ctor_m9DDCFB672B44DA49F610E0E5232F288EA50AAA34(L_8, /*hidden argument*/NULL);
		TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * L_9 = L_8;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = ___bytes2;
		int32_t L_11 = V_0;
		NullCheck(L_9);
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_12;
		L_12 = TJDecompressor_DecodeFMJPG_m6CC0B007AA65465F562ACE401F2D6129022341E1(L_9, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_13 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_14;
		L_14 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_13, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		G_B6_0 = L_9;
		if (!L_14)
		{
			G_B7_0 = L_9;
			goto IL_0056;
		}
	}
	{
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_15 = V_1;
		NullCheck(L_15);
		int32_t L_16;
		L_16 = DecompressedImage_get_Width_mD99C0D51A23DFE6043BFA0124864922153892CAF_inline(L_15, /*hidden argument*/NULL);
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_17 = V_1;
		NullCheck(L_17);
		int32_t L_18;
		L_18 = DecompressedImage_get_Height_m9BBC5D9D1CFF42D53D088026465248DFD6400D83_inline(L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_20 = (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)il2cpp_codegen_object_new(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092(L_20, L_16, L_18, L_19, (bool)0, /*hidden argument*/NULL);
		___texture0 = L_20;
		G_B10_0 = G_B6_0;
		goto IL_008d;
	}

IL_0056:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_21 = ___texture0;
		NullCheck(L_21);
		int32_t L_22;
		L_22 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_21);
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_23 = V_1;
		NullCheck(L_23);
		int32_t L_24;
		L_24 = DecompressedImage_get_Width_mD99C0D51A23DFE6043BFA0124864922153892CAF_inline(L_23, /*hidden argument*/NULL);
		G_B8_0 = G_B7_0;
		if ((!(((uint32_t)L_22) == ((uint32_t)L_24))))
		{
			G_B9_0 = G_B7_0;
			goto IL_0072;
		}
	}
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_25 = ___texture0;
		NullCheck(L_25);
		int32_t L_26;
		L_26 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_25);
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_27 = V_1;
		NullCheck(L_27);
		int32_t L_28;
		L_28 = DecompressedImage_get_Height_m9BBC5D9D1CFF42D53D088026465248DFD6400D83_inline(L_27, /*hidden argument*/NULL);
		G_B9_0 = G_B8_0;
		if ((((int32_t)L_26) == ((int32_t)L_28)))
		{
			G_B10_0 = G_B8_0;
			goto IL_008d;
		}
	}

IL_0072:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_29 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_29, /*hidden argument*/NULL);
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_30 = V_1;
		NullCheck(L_30);
		int32_t L_31;
		L_31 = DecompressedImage_get_Width_mD99C0D51A23DFE6043BFA0124864922153892CAF_inline(L_30, /*hidden argument*/NULL);
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_32 = V_1;
		NullCheck(L_32);
		int32_t L_33;
		L_33 = DecompressedImage_get_Height_m9BBC5D9D1CFF42D53D088026465248DFD6400D83_inline(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_35 = (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)il2cpp_codegen_object_new(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092(L_35, L_31, L_33, L_34, (bool)0, /*hidden argument*/NULL);
		___texture0 = L_35;
		G_B10_0 = G_B9_0;
	}

IL_008d:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_36 = ___texture0;
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_37 = V_1;
		NullCheck(L_37);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_38;
		L_38 = DecompressedImage_get_Data_m5845759BA644DDCB0A686751B401A22149211BF9_inline(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		Texture2D_LoadRawTextureData_m93A620CC97332F351305E3A93AD11CB2E0EFDAF4(L_36, L_38, /*hidden argument*/NULL);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_39 = ___texture0;
		NullCheck(L_39);
		Texture2D_Apply_m3BB3975288119BA62ED9BE4243F7767EC2F88CA0(L_39, /*hidden argument*/NULL);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** L_40 = ___ref_texture1;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_41 = ___texture0;
		*((RuntimeObject **)L_40) = (RuntimeObject *)L_41;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_40, (void*)(RuntimeObject *)L_41);
		NullCheck(G_B10_0);
		TJDecompressor_Dispose_m918F5429F1097880B5945F8587E9B0017A37F2E1(G_B10_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMExtensionMethods::FMMatchResolution(UnityEngine.Texture2D,UnityEngine.Texture2D&,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FMExtensionMethods_FMMatchResolution_m5CA46D3AD35FE10810AE24C762DACAD0CB68C23A (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___texture0, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** ___ref_texture1, int32_t ____width2, int32_t ____height3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral20F5317256971762AB556CA5842E20C59A160EA1);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = ___texture0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** L_1 = ___ref_texture1;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_2 = *((Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF **)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteral20F5317256971762AB556CA5842E20C59A160EA1, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_4 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0021;
		}
	}
	{
		G_B5_0 = 4;
		goto IL_0027;
	}

IL_0021:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_6 = ___texture0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = Texture2D_get_format_mCBCE13524A94042693822BDDE112990B25F4F8E4(L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
	}

IL_0027:
	{
		V_0 = G_B5_0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_8 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_8, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_10 = ____width2;
		int32_t L_11 = ____height3;
		int32_t L_12 = V_0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_13 = (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)il2cpp_codegen_object_new(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092(L_13, L_10, L_11, L_12, (bool)0, /*hidden argument*/NULL);
		___texture0 = L_13;
		goto IL_0061;
	}

IL_003e:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_14 = ___texture0;
		NullCheck(L_14);
		int32_t L_15;
		L_15 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_14);
		int32_t L_16 = ____width2;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0050;
		}
	}
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_17 = ___texture0;
		NullCheck(L_17);
		int32_t L_18;
		L_18 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_17);
		int32_t L_19 = ____height3;
		if ((((int32_t)L_18) == ((int32_t)L_19)))
		{
			goto IL_0061;
		}
	}

IL_0050:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_20 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_20, /*hidden argument*/NULL);
		int32_t L_21 = ____width2;
		int32_t L_22 = ____height3;
		int32_t L_23 = V_0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_24 = (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)il2cpp_codegen_object_new(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092(L_24, L_21, L_22, L_23, (bool)0, /*hidden argument*/NULL);
		___texture0 = L_24;
	}

IL_0061:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** L_25 = ___ref_texture1;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_26 = ___texture0;
		*((RuntimeObject **)L_25) = (RuntimeObject *)L_26;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_25, (void*)(RuntimeObject *)L_26);
		return;
	}
}
// System.Byte[] FMExtensionMethods::FMRawTextureDataToJPG(System.Byte[],System.Int32,System.Int32,System.Int32,FMChromaSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* FMExtensionMethods_FMRawTextureDataToJPG_m85A2EA12E76A38D087B912D0CED0F084C3D9EE8A (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___RawTextureData0, int32_t ____width1, int32_t ____height2, int32_t ___Quality3, int32_t ___Subsampling4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B2_2 = NULL;
	TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B1_2 = NULL;
	TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * G_B1_3 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	int32_t G_B3_2 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B3_3 = NULL;
	TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * G_B3_4 = NULL;
	{
		TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * L_0 = (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 *)il2cpp_codegen_object_new(TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542_il2cpp_TypeInfo_var);
		TJCompressor__ctor_m2BE57DF352ADAB1D70B3CBB7C48615E46D102780(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * L_1 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = ___RawTextureData0;
		int32_t L_3 = ____width1;
		int32_t L_4 = ____height2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = ___RawTextureData0;
		NullCheck(L_5);
		int32_t L_6 = ____height2;
		int32_t L_7 = ____width1;
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if ((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length)))/(int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_6, (int32_t)L_7))))) == ((int32_t)3)))
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0017;
		}
	}
	{
		G_B3_0 = 7;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0018:
	{
		int32_t L_8 = ___Quality3;
		int32_t L_9 = ___Subsampling4;
		int32_t L_10;
		L_10 = FMExtensionMethods_GetTJSubsampligOption_m74FD4BA6471F91419E3E139DFD8815E7C2CC0D1F(L_9, /*hidden argument*/NULL);
		NullCheck(G_B3_4);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11;
		L_11 = TJCompressor_EncodeFMJPG_mB6286123E7EC12E8C34EFF1B0D772F97ACC9D561(G_B3_4, G_B3_3, G_B3_2, G_B3_1, G_B3_0, L_8, L_10, /*hidden argument*/NULL);
		TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * L_12 = V_0;
		NullCheck(L_12);
		TJCompressor_Dispose_mEF9EFCA1BCC301303AECF2137C940FF92E152B85(L_12, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void FMExtensionMethods::FMJPGToRawTextureData(System.Byte[],System.Byte[]&,System.Int32&,System.Int32&,UnityEngine.TextureFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FMExtensionMethods_FMJPGToRawTextureData_mA8B43E8A0D5B9712F4A85D74F72BE51D132CDBAD (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___JPGData0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** ____RawTextureData1, int32_t* ____width2, int32_t* ____height3, int32_t ____format4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * V_0 = NULL;
	{
		TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * L_0 = (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC *)il2cpp_codegen_object_new(TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC_il2cpp_TypeInfo_var);
		TJDecompressor__ctor_m9DDCFB672B44DA49F610E0E5232F288EA50AAA34(L_0, /*hidden argument*/NULL);
		TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * L_1 = L_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = ___JPGData0;
		int32_t L_3 = ____format4;
		NullCheck(L_1);
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_4;
		L_4 = TJDecompressor_DecodeFMJPG_m6CC0B007AA65465F562ACE401F2D6129022341E1(L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** L_5 = ____RawTextureData1;
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_6 = V_0;
		NullCheck(L_6);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7;
		L_7 = DecompressedImage_get_Data_m5845759BA644DDCB0A686751B401A22149211BF9_inline(L_6, /*hidden argument*/NULL);
		*((RuntimeObject **)L_5) = (RuntimeObject *)L_7;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_5, (void*)(RuntimeObject *)L_7);
		int32_t* L_8 = ____width2;
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10;
		L_10 = DecompressedImage_get_Width_mD99C0D51A23DFE6043BFA0124864922153892CAF_inline(L_9, /*hidden argument*/NULL);
		*((int32_t*)L_8) = (int32_t)L_10;
		int32_t* L_11 = ____height3;
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = DecompressedImage_get_Height_m9BBC5D9D1CFF42D53D088026465248DFD6400D83_inline(L_12, /*hidden argument*/NULL);
		*((int32_t*)L_11) = (int32_t)L_13;
		NullCheck(L_1);
		TJDecompressor_Dispose_m918F5429F1097880B5945F8587E9B0017A37F2E1(L_1, /*hidden argument*/NULL);
		return;
	}
}
// FMTurboJpegWrapper.TJSubsamplingOption FMExtensionMethods::GetTJSubsampligOption(FMChromaSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMExtensionMethods_GetTJSubsampligOption_m74FD4BA6471F91419E3E139DFD8815E7C2CC0D1F (int32_t ___Subsampling0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 2;
		int32_t L_0 = ___Subsampling0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_001e;
			}
			case 2:
			{
				goto IL_0022;
			}
			case 3:
			{
				goto IL_0026;
			}
		}
	}
	{
		goto IL_0028;
	}

IL_001a:
	{
		V_0 = 0;
		goto IL_0028;
	}

IL_001e:
	{
		V_0 = 1;
		goto IL_0028;
	}

IL_0022:
	{
		V_0 = 2;
		goto IL_0028;
	}

IL_0026:
	{
		V_0 = 3;
	}

IL_0028:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMTurboJpegWrapper.TJCompressor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor__ctor_m2BE57DF352ADAB1D70B3CBB7C48615E46D102780 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RuntimeObject_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_lock_0(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		intptr_t L_1;
		L_1 = TurboJpegImport_TjInitCompress_m0E11173E0A9EC8190333E8810615BAA1260B7261(/*hidden argument*/NULL);
		__this->set_compressorHandle_1((intptr_t)L_1);
		intptr_t L_2 = __this->get_compressorHandle_1();
		bool L_3;
		L_3 = IntPtr_op_Equality_mD94F3FE43A65684EFF984A7B95E70D2520C0AC73((intptr_t)L_2, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		TJUtils_GetErrorAndThrow_m116A7140103D97D986F589FC565B881F9ED3E046(/*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJCompressor::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_Finalize_m4F353B1CBF857C1F7248466A268B31FE72FE220D (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void FMTurboJpegWrapper.TJCompressor::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x10, FINALLY_0009);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0009;
	}

FINALLY_0009:
	{ // begin finally (depth: 1)
		Object_Finalize_mC59C83CF4F7707E425FFA6362931C25D4C36676A(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(9)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(9)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x10, IL_0010)
	}

IL_0010:
	{
		return;
	}
}
// System.Byte[] FMTurboJpegWrapper.TJCompressor::Compress(System.Byte[],System.Int32,System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJSubsamplingOption,System.Int32,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TJCompressor_Compress_m928E468B43ECEB3E6083AC8684F39347BCB90849 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___srcBuf0, int32_t ___stride1, int32_t ___width2, int32_t ___height3, int32_t ___tjPixelFormat4, int32_t ___subSamp5, int32_t ___quality6, int32_t ___flags7, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Marshal_tEBAFAE20369FCB1B38C49C4E27A8D8C2C4B55058_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TJCompressor_Compress_m928E468B43ECEB3E6083AC8684F39347BCB90849_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	uint64_t V_1 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	uint8_t* V_3 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_4 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_5 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A * L_1 = (ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A_il2cpp_TypeInfo_var)));
		ObjectDisposedException__ctor_mE57C6A61713668708F9B3CEF060A8D006B1FE880(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralD525221FF38EAF1A30491622A0B39D5D960A7815)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TJCompressor_Compress_m928E468B43ECEB3E6083AC8684F39347BCB90849_RuntimeMethod_var)));
	}

IL_0013:
	{
		int32_t L_2 = ___subSamp5;
		int32_t L_3 = ___tjPixelFormat4;
		TJCompressor_CheckOptionsCompatibilityAndThrow_m2B09B024F4D0D0C1F9B4800CED86E353E453032B(L_2, L_3, /*hidden argument*/NULL);
		V_0 = (intptr_t)(0);
		V_1 = ((int64_t)((int64_t)0));
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = ___srcBuf0;
				ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = L_4;
				V_4 = L_5;
				if (!L_5)
				{
					goto IL_0031;
				}
			}

IL_002b:
			{
				ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = V_4;
				NullCheck(L_6);
				if (((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length))))
				{
					goto IL_0036;
				}
			}

IL_0031:
			{
				V_3 = (uint8_t*)((uintptr_t)0);
				goto IL_0040;
			}

IL_0036:
			{
				ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = V_4;
				NullCheck(L_7);
				V_3 = (uint8_t*)((uintptr_t)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
			}

IL_0040:
			{
				intptr_t L_8 = __this->get_compressorHandle_1();
				uint8_t* L_9 = V_3;
				intptr_t L_10;
				L_10 = IntPtr_op_Explicit_mBD40223EE90BDDF40A24C0F321D3398DEA300495((void*)(void*)L_9, /*hidden argument*/NULL);
				int32_t L_11 = ___width2;
				int32_t L_12 = ___stride1;
				int32_t L_13 = ___height3;
				int32_t L_14 = ___tjPixelFormat4;
				int32_t L_15 = ___subSamp5;
				int32_t L_16 = ___quality6;
				int32_t L_17 = ___flags7;
				IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
				int32_t L_18;
				L_18 = TurboJpegImport_TjCompress2_m153EB5525C48045D5CF86EC1A74B66714C6DD65E((intptr_t)L_8, (intptr_t)L_10, L_11, L_12, L_13, L_14, (intptr_t*)(&V_0), (uint64_t*)(&V_1), L_15, L_16, L_17, /*hidden argument*/NULL);
				if ((!(((uint32_t)L_18) == ((uint32_t)(-1)))))
				{
					goto IL_0069;
				}
			}

IL_0064:
			{
				TJUtils_GetErrorAndThrow_m116A7140103D97D986F589FC565B881F9ED3E046(/*hidden argument*/NULL);
			}

IL_0069:
			{
				IL2CPP_LEAVE(0x6F, FINALLY_006b);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_006b;
		}

FINALLY_006b:
		{ // begin finally (depth: 2)
			V_4 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL;
			IL2CPP_END_FINALLY(107)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(107)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_JUMP_TBL(0x6F, IL_006f)
		}

IL_006f:
		{
			uint64_t L_19 = V_1;
			if ((uint64_t)(L_19) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), TJCompressor_Compress_m928E468B43ECEB3E6083AC8684F39347BCB90849_RuntimeMethod_var);
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_20 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((intptr_t)L_19));
			V_2 = L_20;
			intptr_t L_21 = V_0;
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_22 = V_2;
			uint64_t L_23 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_tEBAFAE20369FCB1B38C49C4E27A8D8C2C4B55058_il2cpp_TypeInfo_var);
			Marshal_Copy_m057A8067BF7212A361510EA26B24022990A07AC0((intptr_t)L_21, L_22, 0, ((int32_t)((int32_t)L_23)), /*hidden argument*/NULL);
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_24 = V_2;
			V_5 = L_24;
			IL2CPP_LEAVE(0x8D, FINALLY_0086);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0086;
	}

FINALLY_0086:
	{ // begin finally (depth: 1)
		intptr_t L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		TurboJpegImport_TjFree_mAEF709FF49044B6A1021411427D48CA7841F35FC((intptr_t)L_25, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(134)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(134)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x8D, IL_008d)
	}

IL_008d:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_26 = V_5;
		return L_26;
	}
}
// System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(UnityEngine.Texture2D,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TJCompressor_EncodeFMJPG_m04A1BB9FBAE993BE6DABC415BBB01574C24716A1 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___texture0, int32_t ___Quality1, int32_t ___TJSubsampling2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_0 = ___texture0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_0);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)3));
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_2 = ___texture0;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = Texture2D_get_format_mCBCE13524A94042693822BDDE112990B25F4F8E4(L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		int32_t L_4 = V_2;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)3)))
		{
			case 0:
			{
				goto IL_002d;
			}
			case 1:
			{
				goto IL_003a;
			}
			case 2:
			{
				goto IL_0054;
			}
		}
	}
	{
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)14))))
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0060;
	}

IL_002d:
	{
		V_0 = 0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_6 = ___texture0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_6);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_7, (int32_t)3));
		goto IL_0060;
	}

IL_003a:
	{
		V_0 = 7;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_8 = ___texture0;
		NullCheck(L_8);
		int32_t L_9;
		L_9 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_8);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)4));
		goto IL_0060;
	}

IL_0047:
	{
		V_0 = 8;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_10 = ___texture0;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_10);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_11, (int32_t)4));
		goto IL_0060;
	}

IL_0054:
	{
		V_0 = ((int32_t)10);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_12 = ___texture0;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_12);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)4));
	}

IL_0060:
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_14 = ___texture0;
		NullCheck(L_14);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_15;
		L_15 = Texture2D_GetRawTextureData_m60C0B5EF034F31FE1824B31AC1DE71042E2ACB55(L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_17 = ___texture0;
		NullCheck(L_17);
		int32_t L_18;
		L_18 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_17);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_19 = ___texture0;
		NullCheck(L_19);
		int32_t L_20;
		L_20 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_19);
		int32_t L_21 = V_0;
		int32_t L_22 = ___TJSubsampling2;
		int32_t L_23 = ___Quality1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_24;
		L_24 = TJCompressor_Compress_m928E468B43ECEB3E6083AC8684F39347BCB90849(__this, L_15, L_16, L_18, L_20, L_21, L_22, L_23, 2, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(System.Byte[],System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TJCompressor_EncodeFMJPG_mB6286123E7EC12E8C34EFF1B0D772F97ACC9D561 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____RawTextureData0, int32_t ____width1, int32_t ____height2, int32_t ____format3, int32_t ___Quality4, int32_t ___TJSubsampling5, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ____width1;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)3));
		int32_t L_1 = ____format3;
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = ____format3;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)7)))
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_002f;
			}
			case 2:
			{
				goto IL_0039;
			}
			case 3:
			{
				goto IL_0035;
			}
		}
	}
	{
		goto IL_0039;
	}

IL_0023:
	{
		int32_t L_3 = ____width1;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_3, (int32_t)3));
		goto IL_0039;
	}

IL_0029:
	{
		int32_t L_4 = ____width1;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_4, (int32_t)4));
		goto IL_0039;
	}

IL_002f:
	{
		int32_t L_5 = ____width1;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)4));
		goto IL_0039;
	}

IL_0035:
	{
		int32_t L_6 = ____width1;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_6, (int32_t)4));
	}

IL_0039:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = ____RawTextureData0;
		int32_t L_8 = V_0;
		int32_t L_9 = ____width1;
		int32_t L_10 = ____height2;
		int32_t L_11 = ____format3;
		int32_t L_12 = ___TJSubsampling5;
		int32_t L_13 = ___Quality4;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_14;
		L_14 = TJCompressor_Compress_m928E468B43ECEB3E6083AC8684F39347BCB90849(__this, L_7, L_8, L_9, L_10, L_11, L_12, L_13, 2, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void FMTurboJpegWrapper.TJCompressor::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_Dispose_mEF9EFCA1BCC301303AECF2137C940FF92E152B85 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GC_tD6F0377620BF01385965FD29272CF088A4309C0D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		RuntimeObject * L_1 = __this->get_lock_0();
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m3AEE1F76020B92B6C2742BCD05706DC5FD6F9CB2(L_2, /*hidden argument*/NULL);
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		{
			bool L_3 = __this->get_isDisposed_2();
			if (!L_3)
			{
				goto IL_0020;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}

IL_0020:
		{
			VirtActionInvoker1< bool >::Invoke(5 /* System.Void FMTurboJpegWrapper.TJCompressor::Dispose(System.Boolean) */, __this, (bool)1);
			IL2CPP_RUNTIME_CLASS_INIT(GC_tD6F0377620BF01385965FD29272CF088A4309C0D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_mEE880E988C6AF32AA2F67F2D62715281EAA41555(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		RuntimeObject * L_4 = V_0;
		Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A(L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x36, IL_0036)
	}

IL_0036:
	{
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJCompressor::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_Dispose_m0128E1435D98AB29E9BAEAB76F283D6AD4E1E3B0 (TJCompressor_t367DB7B397B22AC2D58283FB8915E8BBA433A542 * __this, bool ___callFromUserCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___callFromUserCode0;
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
	}

IL_000a:
	{
		intptr_t L_1 = __this->get_compressorHandle_1();
		bool L_2;
		L_2 = IntPtr_op_Inequality_m212AF0E66AA81FEDC982B1C8A44ADDA24B995EB8((intptr_t)L_1, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		intptr_t L_3 = __this->get_compressorHandle_1();
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		int32_t L_4;
		L_4 = TurboJpegImport_TjDestroy_m133702A671B7275E7B1C1F096A4DC2983AFC6184((intptr_t)L_3, /*hidden argument*/NULL);
		__this->set_compressorHandle_1((intptr_t)(0));
	}

IL_0033:
	{
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJCompressor::CheckOptionsCompatibilityAndThrow(FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TJPixelFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_CheckOptionsCompatibilityAndThrow_m2B09B024F4D0D0C1F9B4800CED86E353E453032B (int32_t ___subSamp0, int32_t ___srcFormat1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___srcFormat1;
		if ((!(((uint32_t)L_0) == ((uint32_t)6))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_1 = ___subSamp0;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_2 = 3;
		RuntimeObject * L_3 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TJSubsamplingOption_t8A94ECD53B69C6FF1DE00E6F939EC434246110BC_il2cpp_TypeInfo_var)), &L_2);
		int32_t L_4 = 6;
		RuntimeObject * L_5 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TJPixelFormat_t74413E64CD0264BDB07C1776A05E5BA122A5F226_il2cpp_TypeInfo_var)), &L_4);
		String_t* L_6;
		L_6 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralF2901696E61B6DBCAC0495A6584065E29A70C5DA)), L_3, L_5, /*hidden argument*/NULL);
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_7 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TJCompressor_CheckOptionsCompatibilityAndThrow_m2B09B024F4D0D0C1F9B4800CED86E353E453032B_RuntimeMethod_var)));
	}

IL_0024:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMTurboJpegWrapper.TJDecompressor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor__ctor_m9DDCFB672B44DA49F610E0E5232F288EA50AAA34 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RuntimeObject_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(L_0, /*hidden argument*/NULL);
		__this->set_lock_0(L_0);
		__this->set_decompressorHandle_1((intptr_t)(0));
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		intptr_t L_1;
		L_1 = TurboJpegImport_TjInitDecompress_m249156F548208F11CE0EB14918ED99F1820AA97C(/*hidden argument*/NULL);
		__this->set_decompressorHandle_1((intptr_t)L_1);
		intptr_t L_2 = __this->get_decompressorHandle_1();
		bool L_3;
		L_3 = IntPtr_op_Equality_mD94F3FE43A65684EFF984A7B95E70D2520C0AC73((intptr_t)L_2, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		TJUtils_GetErrorAndThrow_m116A7140103D97D986F589FC565B881F9ED3E046(/*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJDecompressor::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Finalize_mC383BAD6492DC0B469BEBC3E92DF1D849F8D68FE (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void FMTurboJpegWrapper.TJDecompressor::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x10, FINALLY_0009);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0009;
	}

FINALLY_0009:
	{ // begin finally (depth: 1)
		Object_Finalize_mC59C83CF4F7707E425FFA6362931C25D4C36676A(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(9)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(9)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x10, IL_0010)
	}

IL_0010:
	{
		return;
	}
}
// System.Byte[] FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* TJDecompressor_Decompress_m3A19D8ABEFBC56FEF69012A80F78D4480D070A2E (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t ___flags3, int32_t* ___width4, int32_t* ___height5, int32_t* ___stride6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B2_0 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B1_0 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B3_0 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B4_0 = NULL;
	{
		intptr_t L_0 = ___jpegBuf0;
		uint64_t L_1 = ___jpegBufSize1;
		int32_t L_2 = ___destPixelFormat2;
		int32_t* L_3 = ___width4;
		int32_t* L_4 = ___height5;
		int32_t* L_5 = ___stride6;
		TJDecompressor_GetImageInfo_m1BCAA6AC6AB869AACC1EAD4C29D9E01E6B27E5C3(__this, (intptr_t)L_0, L_1, L_2, (int32_t*)L_3, (int32_t*)L_4, (int32_t*)L_5, (int32_t*)(&V_0), /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_6);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8 = L_7;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9 = L_8;
		V_2 = L_9;
		G_B1_0 = L_8;
		if (!L_9)
		{
			G_B2_0 = L_8;
			goto IL_0021;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = V_2;
		NullCheck(L_10);
		G_B2_0 = G_B1_0;
		if (((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length))))
		{
			G_B3_0 = G_B1_0;
			goto IL_0026;
		}
	}

IL_0021:
	{
		V_1 = (uint8_t*)((uintptr_t)0);
		G_B4_0 = G_B2_0;
		goto IL_002f;
	}

IL_0026:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = V_2;
		NullCheck(L_11);
		V_1 = (uint8_t*)((uintptr_t)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		G_B4_0 = G_B3_0;
	}

IL_002f:
	{
		intptr_t L_12 = ___jpegBuf0;
		uint64_t L_13 = ___jpegBufSize1;
		uint8_t* L_14 = V_1;
		intptr_t L_15;
		L_15 = IntPtr_op_Explicit_mBD40223EE90BDDF40A24C0F321D3398DEA300495((void*)(void*)L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		int32_t L_17 = ___destPixelFormat2;
		int32_t L_18 = ___flags3;
		int32_t* L_19 = ___width4;
		int32_t* L_20 = ___height5;
		int32_t* L_21 = ___stride6;
		TJDecompressor_Decompress_m39C66057ABFB7B90C028F1374BEF01D5452B3EC2(__this, (intptr_t)L_12, L_13, (intptr_t)L_15, L_16, L_17, L_18, (int32_t*)L_19, (int32_t*)L_20, (int32_t*)L_21, /*hidden argument*/NULL);
		V_2 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL;
		return G_B4_0;
	}
}
// System.Void FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,System.IntPtr,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Decompress_m39C66057ABFB7B90C028F1374BEF01D5452B3EC2 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, intptr_t ___outBuf2, int32_t ___outBufSize3, int32_t ___destPixelFormat4, int32_t ___flags5, int32_t* ___width6, int32_t* ___height7, int32_t* ___stride8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mDB083C6F06C633E1928B8B9FD1313D2BD55BE8CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A * L_1 = (ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A_il2cpp_TypeInfo_var)));
		ObjectDisposedException__ctor_mE57C6A61713668708F9B3CEF060A8D006B1FE880(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralD525221FF38EAF1A30491622A0B39D5D960A7815)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TJDecompressor_Decompress_m39C66057ABFB7B90C028F1374BEF01D5452B3EC2_RuntimeMethod_var)));
	}

IL_0013:
	{
		intptr_t L_2 = __this->get_decompressorHandle_1();
		intptr_t L_3 = ___jpegBuf0;
		uint64_t L_4 = ___jpegBufSize1;
		int32_t* L_5 = ___width6;
		int32_t* L_6 = ___height7;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		int32_t L_7;
		L_7 = TurboJpegImport_TjDecompressHeader_m1C70E63777A15B304A78A5E262481DA984B152A5((intptr_t)L_2, (intptr_t)L_3, L_4, (int32_t*)L_5, (int32_t*)L_6, (int32_t*)(&V_0), (int32_t*)(&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)(-1)))))
		{
			goto IL_0030;
		}
	}
	{
		TJUtils_GetErrorAndThrow_m116A7140103D97D986F589FC565B881F9ED3E046(/*hidden argument*/NULL);
	}

IL_0030:
	{
		int32_t L_8 = ___destPixelFormat4;
		V_2 = L_8;
		int32_t* L_9 = ___stride8;
		int32_t* L_10 = ___width6;
		int32_t L_11 = *((int32_t*)L_10);
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_12 = ((TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_StaticFields*)il2cpp_codegen_static_fields_for(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var))->get_PixelSizes_0();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14;
		L_14 = Dictionary_2_get_Item_mDB083C6F06C633E1928B8B9FD1313D2BD55BE8CF(L_12, L_13, /*hidden argument*/Dictionary_2_get_Item_mDB083C6F06C633E1928B8B9FD1313D2BD55BE8CF_RuntimeMethod_var);
		*((int32_t*)L_9) = (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_11, (int32_t)L_14));
		int32_t* L_15 = ___stride8;
		int32_t L_16 = *((int32_t*)L_15);
		int32_t* L_17 = ___height7;
		int32_t L_18 = *((int32_t*)L_17);
		V_3 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_16, (int32_t)L_18));
		int32_t L_19 = ___outBufSize3;
		int32_t L_20 = V_3;
		if ((((int32_t)L_19) >= ((int32_t)L_20)))
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_21;
		L_21 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___outBufSize3), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_22 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_22, L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TJDecompressor_Decompress_m39C66057ABFB7B90C028F1374BEF01D5452B3EC2_RuntimeMethod_var)));
	}

IL_005f:
	{
		intptr_t L_23 = __this->get_decompressorHandle_1();
		intptr_t L_24 = ___jpegBuf0;
		uint64_t L_25 = ___jpegBufSize1;
		intptr_t L_26 = ___outBuf2;
		int32_t* L_27 = ___width6;
		int32_t L_28 = *((int32_t*)L_27);
		int32_t* L_29 = ___stride8;
		int32_t L_30 = *((int32_t*)L_29);
		int32_t* L_31 = ___height7;
		int32_t L_32 = *((int32_t*)L_31);
		int32_t L_33 = V_2;
		int32_t L_34 = ___flags5;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		int32_t L_35;
		L_35 = TurboJpegImport_TjDecompress_m591B0598027516C484448BE035AB9E75D5B60471((intptr_t)L_23, (intptr_t)L_24, L_25, (intptr_t)L_26, L_28, L_30, L_32, L_33, L_34, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_35) == ((uint32_t)(-1)))))
		{
			goto IL_0081;
		}
	}
	{
		TJUtils_GetErrorAndThrow_m116A7140103D97D986F589FC565B881F9ED3E046(/*hidden argument*/NULL);
	}

IL_0081:
	{
		return;
	}
}
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * TJDecompressor_Decompress_m3DEC0F0E4A220B42DFD7D38D38B0389F44445C75 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t ___flags3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_3 = NULL;
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A * L_1 = (ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A_il2cpp_TypeInfo_var)));
		ObjectDisposedException__ctor_mE57C6A61713668708F9B3CEF060A8D006B1FE880(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralD525221FF38EAF1A30491622A0B39D5D960A7815)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TJDecompressor_Decompress_m3DEC0F0E4A220B42DFD7D38D38B0389F44445C75_RuntimeMethod_var)));
	}

IL_0013:
	{
		intptr_t L_2 = ___jpegBuf0;
		uint64_t L_3 = ___jpegBufSize1;
		int32_t L_4 = ___destPixelFormat2;
		int32_t L_5 = ___flags3;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6;
		L_6 = TJDecompressor_Decompress_m3A19D8ABEFBC56FEF69012A80F78D4480D070A2E(__this, (intptr_t)L_2, L_3, L_4, L_5, (int32_t*)(&V_0), (int32_t*)(&V_1), (int32_t*)(&V_2), /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		int32_t L_9 = V_2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = V_3;
		int32_t L_11 = ___destPixelFormat2;
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_12 = (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA *)il2cpp_codegen_object_new(DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA_il2cpp_TypeInfo_var);
		DecompressedImage__ctor_mEA72381F39D50F44CB114784B95B02B9B10007B4(L_12, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.Byte[],FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * TJDecompressor_Decompress_m80B39EB731429D27BACDFB17B8B977ADA76651CC (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___jpegBuf0, int32_t ___destPixelFormat1, int32_t ___flags2, const RuntimeMethod* method)
{
	uint64_t V_0 = 0;
	uint8_t* V_1 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A * L_1 = (ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ObjectDisposedException_t29EF6F519F16BA477EC682F23E8344BB1E9A958A_il2cpp_TypeInfo_var)));
		ObjectDisposedException__ctor_mE57C6A61713668708F9B3CEF060A8D006B1FE880(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralD525221FF38EAF1A30491622A0B39D5D960A7815)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TJDecompressor_Decompress_m80B39EB731429D27BACDFB17B8B977ADA76651CC_RuntimeMethod_var)));
	}

IL_0013:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = ___jpegBuf0;
		NullCheck(L_2);
		V_0 = ((int64_t)((int64_t)((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length)))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = ___jpegBuf0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = L_3;
		V_2 = L_4;
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = V_2;
		NullCheck(L_5);
		if (((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length))))
		{
			goto IL_0027;
		}
	}

IL_0022:
	{
		V_1 = (uint8_t*)((uintptr_t)0);
		goto IL_0030;
	}

IL_0027:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = V_2;
		NullCheck(L_6);
		V_1 = (uint8_t*)((uintptr_t)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
	}

IL_0030:
	{
		uint8_t* L_7 = V_1;
		intptr_t L_8;
		L_8 = IntPtr_op_Explicit_mBD40223EE90BDDF40A24C0F321D3398DEA300495((void*)(void*)L_7, /*hidden argument*/NULL);
		uint64_t L_9 = V_0;
		int32_t L_10 = ___destPixelFormat1;
		int32_t L_11 = ___flags2;
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_12;
		L_12 = TJDecompressor_Decompress_m3DEC0F0E4A220B42DFD7D38D38B0389F44445C75(__this, (intptr_t)L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::DecodeFMJPG(System.Byte[],UnityEngine.TextureFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * TJDecompressor_DecodeFMJPG_m6CC0B007AA65465F562ACE401F2D6129022341E1 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___jpegBuf0, int32_t ___format1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___format1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)3)))
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_0026;
			}
			case 2:
			{
				goto IL_002e;
			}
		}
	}
	{
		int32_t L_1 = ___format1;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)14))))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_2 = ___format1;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)63))))
		{
			goto IL_0033;
		}
	}
	{
		goto IL_0035;
	}

IL_0022:
	{
		V_0 = 0;
		goto IL_0035;
	}

IL_0026:
	{
		V_0 = 7;
		goto IL_0035;
	}

IL_002a:
	{
		V_0 = 8;
		goto IL_0035;
	}

IL_002e:
	{
		V_0 = ((int32_t)10);
		goto IL_0035;
	}

IL_0033:
	{
		V_0 = 6;
	}

IL_0035:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = ___jpegBuf0;
		int32_t L_4 = V_0;
		DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * L_5;
		L_5 = TJDecompressor_Decompress_m80B39EB731429D27BACDFB17B8B977ADA76651CC(__this, L_3, L_4, 2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void FMTurboJpegWrapper.TJDecompressor::GetImageInfo(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_GetImageInfo_m1BCAA6AC6AB869AACC1EAD4C29D9E01E6B27E5C3 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t* ___width3, int32_t* ___height4, int32_t* ___stride5, int32_t* ___bufSize6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_mDB083C6F06C633E1928B8B9FD1313D2BD55BE8CF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		intptr_t L_0 = __this->get_decompressorHandle_1();
		intptr_t L_1 = ___jpegBuf0;
		uint64_t L_2 = ___jpegBufSize1;
		int32_t* L_3 = ___width3;
		int32_t* L_4 = ___height4;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		int32_t L_5;
		L_5 = TurboJpegImport_TjDecompressHeader_m1C70E63777A15B304A78A5E262481DA984B152A5((intptr_t)L_0, (intptr_t)L_1, L_2, (int32_t*)L_3, (int32_t*)L_4, (int32_t*)(&V_0), (int32_t*)(&V_1), /*hidden argument*/NULL);
		int32_t* L_6 = ___stride5;
		int32_t* L_7 = ___width3;
		int32_t L_8 = *((int32_t*)L_7);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_9 = ((TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_StaticFields*)il2cpp_codegen_static_fields_for(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var))->get_PixelSizes_0();
		int32_t L_10 = ___destPixelFormat2;
		NullCheck(L_9);
		int32_t L_11;
		L_11 = Dictionary_2_get_Item_mDB083C6F06C633E1928B8B9FD1313D2BD55BE8CF(L_9, L_10, /*hidden argument*/Dictionary_2_get_Item_mDB083C6F06C633E1928B8B9FD1313D2BD55BE8CF_RuntimeMethod_var);
		*((int32_t*)L_6) = (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_8, (int32_t)L_11));
		int32_t* L_12 = ___bufSize6;
		int32_t* L_13 = ___stride5;
		int32_t L_14 = *((int32_t*)L_13);
		int32_t* L_15 = ___height4;
		int32_t L_16 = *((int32_t*)L_15);
		*((int32_t*)L_12) = (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_14, (int32_t)L_16));
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJDecompressor::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Dispose_m918F5429F1097880B5945F8587E9B0017A37F2E1 (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GC_tD6F0377620BF01385965FD29272CF088A4309C0D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		RuntimeObject * L_1 = __this->get_lock_0();
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m3AEE1F76020B92B6C2742BCD05706DC5FD6F9CB2(L_2, /*hidden argument*/NULL);
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		{
			bool L_3 = __this->get_isDisposed_2();
			if (!L_3)
			{
				goto IL_0020;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}

IL_0020:
		{
			VirtActionInvoker1< bool >::Invoke(5 /* System.Void FMTurboJpegWrapper.TJDecompressor::Dispose(System.Boolean) */, __this, (bool)1);
			IL2CPP_RUNTIME_CLASS_INIT(GC_tD6F0377620BF01385965FD29272CF088A4309C0D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_mEE880E988C6AF32AA2F67F2D62715281EAA41555(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		RuntimeObject * L_4 = V_0;
		Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A(L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x36, IL_0036)
	}

IL_0036:
	{
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJDecompressor::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Dispose_m916E34446FE0C5DD2101267A42BC54C027153B1B (TJDecompressor_t6601A09ADCD527016E4B7B9B59E6079931220EAC * __this, bool ___callFromUserCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IntPtr_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___callFromUserCode0;
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
	}

IL_000a:
	{
		intptr_t L_1 = __this->get_decompressorHandle_1();
		bool L_2;
		L_2 = IntPtr_op_Inequality_m212AF0E66AA81FEDC982B1C8A44ADDA24B995EB8((intptr_t)L_1, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		intptr_t L_3 = __this->get_decompressorHandle_1();
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		int32_t L_4;
		L_4 = TurboJpegImport_TjDestroy_m133702A671B7275E7B1C1F096A4DC2983AFC6184((intptr_t)L_3, /*hidden argument*/NULL);
		__this->set_decompressorHandle_1((intptr_t)(0));
	}

IL_0033:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMTurboJpegWrapper.TJUtils::GetErrorAndThrow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJUtils_GetErrorAndThrow_m116A7140103D97D986F589FC565B881F9ED3E046 (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMTurboJpegWrapper.TjSize::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6 (TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___width0;
		TjSize_set_Width_m40DB05B6594E8FB81CD072C94EA99F619F12B648_inline((TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 *)__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height1;
		TjSize_set_Height_mA6BFA2A673A5796D6E5FD2623EBA70D2EB90AD98_inline((TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 *)__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6_AdjustorThunk (RuntimeObject * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * _thisAdjusted = reinterpret_cast<TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 *>(__this + _offset);
	TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6(_thisAdjusted, ___width0, ___height1, method);
}
// System.Void FMTurboJpegWrapper.TjSize::set_Width(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TjSize_set_Width_m40DB05B6594E8FB81CD072C94EA99F619F12B648 (TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CWidthU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void TjSize_set_Width_m40DB05B6594E8FB81CD072C94EA99F619F12B648_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * _thisAdjusted = reinterpret_cast<TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 *>(__this + _offset);
	TjSize_set_Width_m40DB05B6594E8FB81CD072C94EA99F619F12B648_inline(_thisAdjusted, ___value0, method);
}
// System.Void FMTurboJpegWrapper.TjSize::set_Height(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TjSize_set_Height_mA6BFA2A673A5796D6E5FD2623EBA70D2EB90AD98 (TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CHeightU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void TjSize_set_Height_mA6BFA2A673A5796D6E5FD2623EBA70D2EB90AD98_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * _thisAdjusted = reinterpret_cast<TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 *>(__this + _offset);
	TjSize_set_Height_mA6BFA2A673A5796D6E5FD2623EBA70D2EB90AD98_inline(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitCompress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t TurboJpegImport_TjInitCompress_m0E11173E0A9EC8190333E8810615BAA1260B7261 (const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_turbojpeg_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjInitCompress", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(tjInitCompress)();
	#else
	intptr_t returnValue = il2cppPInvokeFunc();
	#endif

	return returnValue;
}
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjCompress2(System.IntPtr,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.IntPtr&,System.UInt64&,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjCompress2_m153EB5525C48045D5CF86EC1A74B66714C6DD65E (intptr_t ___handle0, intptr_t ___srcBuf1, int32_t ___width2, int32_t ___pitch3, int32_t ___height4, int32_t ___pixelFormat5, intptr_t* ___jpegBuf6, uint64_t* ___jpegSize7, int32_t ___jpegSubsamp8, int32_t ___jpegQual9, int32_t ___flags10, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, int32_t, int32_t, int32_t, int32_t, intptr_t*, uint64_t*, int32_t, int32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_turbojpeg_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(intptr_t*) + sizeof(uint64_t*) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjCompress2", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjCompress2)(___handle0, ___srcBuf1, ___width2, ___pitch3, ___height4, ___pixelFormat5, ___jpegBuf6, ___jpegSize7, ___jpegSubsamp8, ___jpegQual9, ___flags10);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0, ___srcBuf1, ___width2, ___pitch3, ___height4, ___pixelFormat5, ___jpegBuf6, ___jpegSize7, ___jpegSubsamp8, ___jpegQual9, ___flags10);
	#endif

	return returnValue;
}
// System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitDecompress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t TurboJpegImport_TjInitDecompress_m249156F548208F11CE0EB14918ED99F1820AA97C (const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_turbojpeg_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjInitDecompress", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(tjInitDecompress)();
	#else
	intptr_t returnValue = il2cppPInvokeFunc();
	#endif

	return returnValue;
}
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader_m1C70E63777A15B304A78A5E262481DA984B152A5 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0;
		L_0 = IntPtr_get_Size_mD8038A1C440DE87E685F4D879DC80A6704D9C77B(/*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0031;
	}

IL_0010:
	{
		intptr_t L_3 = ___handle0;
		intptr_t L_4 = ___jpegBuf1;
		uint64_t L_5 = ___jpegSize2;
		int32_t* L_6 = ___width3;
		int32_t* L_7 = ___height4;
		int32_t* L_8 = ___jpegSubsamp5;
		int32_t* L_9 = ___jpegColorspace6;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		int32_t L_10;
		L_10 = TurboJpegImport_TjDecompressHeader3_x86_m1CC33DD0404AF9F552013DA9CBE510B6BD9457F7((intptr_t)L_3, (intptr_t)L_4, ((int32_t)((uint32_t)L_5)), (int32_t*)L_6, (int32_t*)L_7, (int32_t*)L_8, (int32_t*)L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0021:
	{
		intptr_t L_11 = ___handle0;
		intptr_t L_12 = ___jpegBuf1;
		uint64_t L_13 = ___jpegSize2;
		int32_t* L_14 = ___width3;
		int32_t* L_15 = ___height4;
		int32_t* L_16 = ___jpegSubsamp5;
		int32_t* L_17 = ___jpegColorspace6;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		int32_t L_18;
		L_18 = TurboJpegImport_TjDecompressHeader3_x64_mD6E45CE0657F44D84480954738D2FE6A833935B9((intptr_t)L_11, (intptr_t)L_12, L_13, (int32_t*)L_14, (int32_t*)L_15, (int32_t*)L_16, (int32_t*)L_17, /*hidden argument*/NULL);
		return L_18;
	}

IL_0031:
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_19 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_19, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralEF5C314A85CE0407A185D572BE5BB2C388BE9D17)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TurboJpegImport_TjDecompressHeader_m1C70E63777A15B304A78A5E262481DA984B152A5_RuntimeMethod_var)));
	}
}
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress_m591B0598027516C484448BE035AB9E75D5B60471 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0;
		L_0 = IntPtr_get_Size_mD8038A1C440DE87E685F4D879DC80A6704D9C77B(/*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0039;
	}

IL_0010:
	{
		intptr_t L_3 = ___handle0;
		intptr_t L_4 = ___jpegBuf1;
		uint64_t L_5 = ___jpegSize2;
		intptr_t L_6 = ___dstBuf3;
		int32_t L_7 = ___width4;
		int32_t L_8 = ___pitch5;
		int32_t L_9 = ___height6;
		int32_t L_10 = ___pixelFormat7;
		int32_t L_11 = ___flags8;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		int32_t L_12;
		L_12 = TurboJpegImport_TjDecompress2_x86_mA296A566DD1B56EF26610DC67299E2D58ADC8842((intptr_t)L_3, (intptr_t)L_4, ((int32_t)((uint32_t)L_5)), (intptr_t)L_6, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_0025:
	{
		intptr_t L_13 = ___handle0;
		intptr_t L_14 = ___jpegBuf1;
		uint64_t L_15 = ___jpegSize2;
		intptr_t L_16 = ___dstBuf3;
		int32_t L_17 = ___width4;
		int32_t L_18 = ___pitch5;
		int32_t L_19 = ___height6;
		int32_t L_20 = ___pixelFormat7;
		int32_t L_21 = ___flags8;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		int32_t L_22;
		L_22 = TurboJpegImport_TjDecompress2_x64_m7D71BE1DAD8A4013D134CD0683E3300A3515BBFC((intptr_t)L_13, (intptr_t)L_14, L_15, (intptr_t)L_16, L_17, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		return L_22;
	}

IL_0039:
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_23 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_23, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralEF5C314A85CE0407A185D572BE5BB2C388BE9D17)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&TurboJpegImport_TjDecompress_m591B0598027516C484448BE035AB9E75D5B60471_RuntimeMethod_var)));
	}
}
// System.Void FMTurboJpegWrapper.TurboJpegImport::TjFree(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurboJpegImport_TjFree_mAEF709FF49044B6A1021411427D48CA7841F35FC (intptr_t ___buffer0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_turbojpeg_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjFree", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
	reinterpret_cast<PInvokeFunc>(tjFree)(___buffer0);
	#else
	il2cppPInvokeFunc(___buffer0);
	#endif

}
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDestroy(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDestroy_m133702A671B7275E7B1C1F096A4DC2983AFC6184 (intptr_t ___handle0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_turbojpeg_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjDestroy", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjDestroy)(___handle0);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0);
	#endif

	return returnValue;
}
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x86(System.IntPtr,System.IntPtr,System.UInt32,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader3_x86_m1CC33DD0404AF9F552013DA9CBE510B6BD9457F7 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint32_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, uint32_t, int32_t*, int32_t*, int32_t*, int32_t*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_turbojpeg_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(uint32_t) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjDecompressHeader3", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjDecompressHeader3)(___handle0, ___jpegBuf1, ___jpegSize2, ___width3, ___height4, ___jpegSubsamp5, ___jpegColorspace6);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0, ___jpegBuf1, ___jpegSize2, ___width3, ___height4, ___jpegSubsamp5, ___jpegColorspace6);
	#endif

	return returnValue;
}
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x64(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader3_x64_mD6E45CE0657F44D84480954738D2FE6A833935B9 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, uint64_t, int32_t*, int32_t*, int32_t*, int32_t*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_turbojpeg_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(uint64_t) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjDecompressHeader3", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjDecompressHeader3)(___handle0, ___jpegBuf1, ___jpegSize2, ___width3, ___height4, ___jpegSubsamp5, ___jpegColorspace6);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0, ___jpegBuf1, ___jpegSize2, ___width3, ___height4, ___jpegSubsamp5, ___jpegColorspace6);
	#endif

	return returnValue;
}
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x86(System.IntPtr,System.IntPtr,System.UInt32,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress2_x86_mA296A566DD1B56EF26610DC67299E2D58ADC8842 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint32_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, uint32_t, intptr_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_turbojpeg_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(uint32_t) + sizeof(intptr_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjDecompress2", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjDecompress2)(___handle0, ___jpegBuf1, ___jpegSize2, ___dstBuf3, ___width4, ___pitch5, ___height6, ___pixelFormat7, ___flags8);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0, ___jpegBuf1, ___jpegSize2, ___dstBuf3, ___width4, ___pitch5, ___height6, ___pixelFormat7, ___flags8);
	#endif

	return returnValue;
}
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x64(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress2_x64_m7D71BE1DAD8A4013D134CD0683E3300A3515BBFC (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, uint64_t, intptr_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_turbojpeg_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(uint64_t) + sizeof(intptr_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjDecompress2", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_turbojpeg_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjDecompress2)(___handle0, ___jpegBuf1, ___jpegSize2, ___dstBuf3, ___width4, ___pitch5, ___height6, ___pixelFormat7, ___flags8);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0, ___jpegBuf1, ___jpegSize2, ___dstBuf3, ___width4, ___pitch5, ___height6, ___pixelFormat7, ___flags8);
	#endif

	return returnValue;
}
// System.Void FMTurboJpegWrapper.TurboJpegImport::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurboJpegImport__cctor_m6FFFAE012B029556B93B50A4476BDDE71F9D7850 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m70ACBD7D6FBC4F2E8319EAF1D1DEA10602E5224C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mB2214F2816AD1BA65732FFE1CF32C6BF2CF6289C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_0 = (Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 *)il2cpp_codegen_object_new(Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mB2214F2816AD1BA65732FFE1CF32C6BF2CF6289C(L_0, /*hidden argument*/Dictionary_2__ctor_mB2214F2816AD1BA65732FFE1CF32C6BF2CF6289C_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_1 = L_0;
		NullCheck(L_1);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_1, 0, 3, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_2 = L_1;
		NullCheck(L_2);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_2, 1, 3, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_3 = L_2;
		NullCheck(L_3);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_3, 2, 4, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_4 = L_3;
		NullCheck(L_4);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_4, 3, 4, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_5 = L_4;
		NullCheck(L_5);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_5, 4, 4, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_6 = L_5;
		NullCheck(L_6);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_6, 5, 4, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_7 = L_6;
		NullCheck(L_7);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_7, 6, 1, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_8 = L_7;
		NullCheck(L_8);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_8, 7, 4, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_9 = L_8;
		NullCheck(L_9);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_9, 8, 4, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_10 = L_9;
		NullCheck(L_10);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_10, ((int32_t)9), 4, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_11 = L_10;
		NullCheck(L_11);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_11, ((int32_t)10), 4, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		Dictionary_2_tF063024B5B2D38224E4E05CDD7D2193C79394C52 * L_12 = L_11;
		NullCheck(L_12);
		Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F(L_12, ((int32_t)11), 4, /*hidden argument*/Dictionary_2_Add_m2F248AE3A0C59078B9208E4866530084B650B10F_RuntimeMethod_var);
		((TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_StaticFields*)il2cpp_codegen_static_fields_for(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var))->set_PixelSizes_0(L_12);
		Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * L_13 = (Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F *)il2cpp_codegen_object_new(Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m70ACBD7D6FBC4F2E8319EAF1D1DEA10602E5224C(L_13, /*hidden argument*/Dictionary_2__ctor_m70ACBD7D6FBC4F2E8319EAF1D1DEA10602E5224C_RuntimeMethod_var);
		Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * L_14 = L_13;
		TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3  L_15;
		memset((&L_15), 0, sizeof(L_15));
		TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6((&L_15), 8, 8, /*hidden argument*/NULL);
		NullCheck(L_14);
		Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F(L_14, 3, L_15, /*hidden argument*/Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F_RuntimeMethod_var);
		Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * L_16 = L_14;
		TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3  L_17;
		memset((&L_17), 0, sizeof(L_17));
		TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6((&L_17), 8, 8, /*hidden argument*/NULL);
		NullCheck(L_16);
		Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F(L_16, 0, L_17, /*hidden argument*/Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F_RuntimeMethod_var);
		Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * L_18 = L_16;
		TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3  L_19;
		memset((&L_19), 0, sizeof(L_19));
		TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6((&L_19), ((int32_t)16), 8, /*hidden argument*/NULL);
		NullCheck(L_18);
		Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F(L_18, 1, L_19, /*hidden argument*/Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F_RuntimeMethod_var);
		Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * L_20 = L_18;
		TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3  L_21;
		memset((&L_21), 0, sizeof(L_21));
		TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6((&L_21), ((int32_t)16), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_20);
		Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F(L_20, 2, L_21, /*hidden argument*/Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F_RuntimeMethod_var);
		Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * L_22 = L_20;
		TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3  L_23;
		memset((&L_23), 0, sizeof(L_23));
		TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6((&L_23), 8, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_22);
		Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F(L_22, 4, L_23, /*hidden argument*/Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F_RuntimeMethod_var);
		Dictionary_2_t9337FA58561C662589BFAC354E7EEF2983CC392F * L_24 = L_22;
		TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3  L_25;
		memset((&L_25), 0, sizeof(L_25));
		TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6((&L_25), ((int32_t)32), 8, /*hidden argument*/NULL);
		NullCheck(L_24);
		Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F(L_24, 5, L_25, /*hidden argument*/Dictionary_2_Add_mF2ED06FDD9E4A7B22FAD85BB6190BB5C17CC3A0F_RuntimeMethod_var);
		((TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_StaticFields*)il2cpp_codegen_static_fields_for(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var))->set_MCUSizes_1(L_24);
		((TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_StaticFields*)il2cpp_codegen_static_fields_for(TurboJpegImport_tDA08952F1F6E79FD4F6281072D090E6BE265F56A_il2cpp_TypeInfo_var))->set__LibraryFound_2((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Width_mD99C0D51A23DFE6043BFA0124864922153892CAF_inline (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CWidthU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Height_m9BBC5D9D1CFF42D53D088026465248DFD6400D83_inline (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CHeightU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* DecompressedImage_get_Data_m5845759BA644DDCB0A686751B401A22149211BF9_inline (DecompressedImage_t92057EB8EC3A0EBCCF170CD0993DCB6E54C6A2FA * __this, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_U3CDataU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TjSize_set_Width_m40DB05B6594E8FB81CD072C94EA99F619F12B648_inline (TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CWidthU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void TjSize_set_Height_mA6BFA2A673A5796D6E5FD2623EBA70D2EB90AD98_inline (TjSize_t56134A6262D9A5CA6A257A0238C0D062894F9DF3 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CHeightU3Ek__BackingField_1(L_0);
		return;
	}
}
