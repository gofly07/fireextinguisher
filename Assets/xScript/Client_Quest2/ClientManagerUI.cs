using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ClientManagerUI : MonoBehaviour
{
    [SerializeField] private GameObject table;
    [SerializeField] private Panel_IPConnection panelIPConnection;
    [SerializeField] private Panel_BlueToothConnection panelBlueToothConnection;
    [SerializeField] private Panel_ClientMenu panelClientMenu;
    [SerializeField] private Panel_FireTypeIntroduction panelFireTypeIntroduction;
    [SerializeField] private Panel_FireExtinguisherTypeIntroduction panelFireExtinguisherTypeIntroduction;
    [SerializeField] private Panel_Tutorial panelTutorial;
    [SerializeField] private Panel_Testing panelTesting;

    public IEnumerator Init()
    {
        table.SetActive(false);

#if UNITY_EDITOR
        Debug.Log(GPTT.Data.Util.GetSerialNumber());
        GPTT.Data.APIHelper.Init();
        UserIDCheck.userID = "4";

        panelIPConnection.gameObject.SetActive(false);
        panelBlueToothConnection.gameObject.SetActive(true);
#elif UNITY_ANDROID
        panelIPConnection.gameObject.SetActive(true);
        panelBlueToothConnection.gameObject.SetActive(false);
#endif
        //panelIPConnection.gameObject.SetActive(false);
        //panelBlueToothConnection.gameObject.SetActive(true);

        panelClientMenu.gameObject.SetActive(false);
        panelFireTypeIntroduction.gameObject.SetActive(false);
        panelFireExtinguisherTypeIntroduction.gameObject.SetActive(false);
        panelTutorial.gameObject.SetActive(false);
        panelTesting.gameObject.SetActive(false);

        yield return null;

        panelIPConnection.EHOpenBlueToothPanel += OpenBlueTooth;
        panelBlueToothConnection.EHOpenClientMenu += OpenClientMenu;
        panelClientMenu.EHOpenFireType += OpenFireType;
        panelClientMenu.EHOpenFireExtinguisherType += OpenFireExtinguisherType;
        panelClientMenu.EHOpenTutorial += OpenTutorial;
        panelClientMenu.EHOpenTesting += OpenTesting;

        panelFireTypeIntroduction.EHCloseFireType += CloseFireType;
        panelFireExtinguisherTypeIntroduction.EHCloseFireExtinguisherType += CloseFireExtinguisherType;
        panelTutorial.EHCloseTutorial += CloseTutorial;
        panelTesting.EHCloseTesting += CloseTesting;
    }

    public void BackToClientSceneFromTestingScene()
    {
        table.SetActive(true);
        panelIPConnection.gameObject.SetActive(false);
        panelBlueToothConnection.gameObject.SetActive(false);
        panelTesting.gameObject.SetActive(true);
    }

    private void OpenBlueTooth(object sender, EventArgs e)
    {
        panelIPConnection.gameObject.SetActive(false);
        panelBlueToothConnection.gameObject.SetActive(true);
    }

    private void OpenClientMenu(object sender, EventArgs e)
    {
        table.SetActive(true);
        panelBlueToothConnection.gameObject.SetActive(false);
        panelClientMenu.gameObject.SetActive(true);
    }

    private void OpenFireType(object sender, EventArgs e)
    {
        panelClientMenu.gameObject.SetActive(false);
        panelFireTypeIntroduction.gameObject.SetActive(true);
    }

    private void CloseFireType(object sender, EventArgs e)
    {
        panelClientMenu.gameObject.SetActive(true);
        panelClientMenu.IntroductionMenu();
        panelFireTypeIntroduction.gameObject.SetActive(false);
    }

    private void OpenFireExtinguisherType(object sender, EventArgs e)
    {
        panelClientMenu.gameObject.SetActive(false);
        panelFireExtinguisherTypeIntroduction.gameObject.SetActive(true);
    }

    private void CloseFireExtinguisherType(object sender, EventArgs e)
    {
        panelClientMenu.gameObject.SetActive(true);
        panelClientMenu.IntroductionMenu();
        panelFireExtinguisherTypeIntroduction.gameObject.SetActive(false);
    }

    private void OpenTutorial(object sender, EventArgs e)
    {
        table.SetActive(false);
        panelClientMenu.gameObject.SetActive(false);
        panelTutorial.gameObject.SetActive(true);
    }

    private void CloseTutorial(object sender, EventArgs e)
    {
        table.SetActive(true);
        panelClientMenu.gameObject.SetActive(true);
        panelTutorial.gameObject.SetActive(false);
    }

    private void OpenTesting(object sender, EventArgs e)
    {
        panelTesting.gameObject.SetActive(true);
        panelClientMenu.gameObject.SetActive(false);
    }

    private void CloseTesting(object sender, EventArgs e)
    {
        panelClientMenu.gameObject.SetActive(true);
        panelTesting.gameObject.SetActive(false);
    }
}
