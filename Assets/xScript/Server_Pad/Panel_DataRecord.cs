﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using static GPTT.Data.API;

public class Panel_DataRecord : MonoBehaviour
{
    [Header("UIs")]
    [SerializeField] private Button btBack;

    [SerializeField] private Button btPP;
    [SerializeField] private Button btP;
    [SerializeField] private List<Button> btPages = new List<Button>();
    [SerializeField] private Button btN;
    [SerializeField] private Button btNN;
    [SerializeField] private Sprite sprNormal;
    [SerializeField] private Sprite sprPressed;
    [SerializeField] private Sprite sprDot;

    [Header("Data Cache")]
    [SerializeField] private List<GameObject> container = new List<GameObject>();
    [SerializeField] private List<FireExtinguisher> fireExtinguishers = new List<FireExtinguisher>();
    [SerializeField] private int pageCount = 0;
    [SerializeField] private int currentPage = 1;

    public event EventHandler EHCloseDataRecord;

    private void OnEnable()
    {
        btBack.onClick.AddListener(delegate { BackToMenu(); });

        btPP.onClick.AddListener(delegate { FirstPage(); });
        //btP.onClick.AddListener(delegate { PreviousPage(); });
        foreach (var i in btPages)
            i.onClick.AddListener(delegate { ClickTargetPages(i); });
        //btN.onClick.AddListener(delegate { NextPage(); });
        btNN.onClick.AddListener(delegate { LastPage(); });

        if (GPTT.Data.APIHelper.isLogin)
            StartCoroutine(test());
    }

    private IEnumerator test()
    {
        yield return null;
        Search s = new Search
        {
            limit = "10",
            page = "1",
            sort = "asc",
            sort_col = "id",
            userId = GPTT.Data.APIHelper.GetUserData().id
        };
        yield return(readPageCount(s));
        currentPage = pc;
        FireExtinguisherSearchOnclick(currentPage);
    }
    int pc = 1;
    private IEnumerator readPageCount(Search search, Action<User> onSuccess = null, Action<string> onError = null)
    {
        yield return GPTT.Data.API.FireExtinguisherList_API(search);

        if (GPTT.Data.API.ResponseMessage["responseCode"] == "200")
            pc = Convert.ToInt32(GPTT.Data.API.fireExtinguisherListData.last_page);
        else
        {
            Debug.Log(GPTT.Data.API.ResponseMessage["message"]);
            onError?.Invoke(GPTT.Data.API.ResponseMessage["message"]);
        }
    }

    private void BackToMenu()
    {
        EHCloseDataRecord(this, EventArgs.Empty);
    }

    private void FireExtinguisherSearchOnclick(int page)
    {
        StopAllCoroutines();
        //搜尋滅火器訓練紀錄列表
        Search search = new Search
        {
            limit = "10",
            page = page.ToString(),
            sort = "asc",
            sort_col = "id",
            userId = GPTT.Data.APIHelper.GetUserData().id
        };
        StartCoroutine(FFireExtinguisherList_Search(search));
    }
   
    private IEnumerator FFireExtinguisherList_Search(Search search, Action<User> onSuccess = null, Action<string> onError = null)
    {
        yield return GPTT.Data.API.FireExtinguisherList_API(search);

        if (GPTT.Data.API.ResponseMessage["responseCode"] == "200")
        {
            //Debug.Log("Data Count: " + API.fireExtinguisherListData.total);
            //Debug.Log("Page Count: " + API.fireExtinguisherListData.last_page);

            pageCount = Convert.ToInt32(GPTT.Data.API.fireExtinguisherListData.last_page);

            fireExtinguishers.Clear();

            foreach (FireExtinguisher data in GPTT.Data.API.fireExtinguisherListData.Data)
                fireExtinguishers.Add(data);

            MatchData(fireExtinguishers);
        }
        else
        {
            Debug.Log(GPTT.Data.API.ResponseMessage["message"]);
            onError?.Invoke(GPTT.Data.API.ResponseMessage["message"]);
        }
    }

    private void MatchData(List<FireExtinguisher> data)
    {
        foreach (var i in container)
        {
            i.transform.GetChild(0).GetComponent<Text>().text = "";
            i.transform.GetChild(1).GetComponent<Text>().text = "";
            i.transform.GetChild(2).GetComponent<Text>().text = "";
            i.transform.GetChild(3).GetComponent<Text>().text = "";
            i.transform.GetChild(4).GetComponent<Text>().text = "";
        }
        for (int i = 0; i < data.Count; i++)
        {
            container[i].transform.GetChild(0).GetComponent<Text>().text = data[i].startTime;
            container[i].transform.GetChild(1).GetComponent<Text>().text = data[i].endTime;
            container[i].transform.GetChild(2).GetComponent<Text>().text = data[i].totalTime;
            container[i].transform.GetChild(3).GetComponent<Text>().text = data[i].project;
            container[i].transform.GetChild(4).GetComponent<Text>().text = data[i].status;
        }
        UpdatePages();
    }

    private void FirstPage()
    {
        if (currentPage > 1)
        {
            currentPage = 1;
            FireExtinguisherSearchOnclick(currentPage);
        }
    }
    public void PreviousPage()
    {
        if (currentPage > 1)
        {
            currentPage = currentPage - 1;
            FireExtinguisherSearchOnclick(currentPage);
        }
    }
    private void LastPage()
    {
        if (currentPage < pageCount)
        {
            currentPage = pageCount;
            FireExtinguisherSearchOnclick(currentPage);
        }
    }
    public void NextPage()
    {
        if (currentPage < pageCount)
        {
            currentPage = currentPage + 1;
            FireExtinguisherSearchOnclick(currentPage);
        }
    }
    private void ClickTargetPages(Button bt)
    {
        currentPage = Convert.ToInt32(bt.name);
        FireExtinguisherSearchOnclick(currentPage);
    }
    private void UpdatePages()
    {
        foreach (var i in btPages)
        {
            i.enabled = false;
            i.image.sprite = sprDot;
            i.transform.GetChild(0).GetComponent<Text>().text = "";
            i.transform.GetChild(0).GetComponent<Text>().color = Color.white;
            i.transform.gameObject.name = "";
        }

        if (currentPage <= pageCount)
        {
            int tab = currentPage / 6;
            if (currentPage % 6 != 0)
                tab = tab + 1;

            int num = (6 * (tab - 1)) + 1;

            for (int i = 0; i < btPages.Count; i++)
            {
                if (pageCount >= (num + i))
                {
                    btPages[i].enabled = true;
                    btPages[i].image.sprite = sprNormal;
                    btPages[i].transform.GetChild(0).GetComponent<Text>().text = (num + i).ToString();
                    btPages[i].transform.GetChild(0).GetComponent<Text>().color = new Color32(89, 133, 158, 255);
                    btPages[i].transform.gameObject.name = (num + i).ToString();

                    if (currentPage == (num + i))
                    {
                        btPages[i].image.sprite = sprPressed;
                        btPages[i].transform.GetChild(0).GetComponent<Text>().color = Color.white;
                    }
                }
            }
        }
    }
}