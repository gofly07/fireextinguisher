﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Byte[] FMZipHelper::FMZipBytes(System.Byte[])
extern void FMZipHelper_FMZipBytes_mFD4D5CDF5908FCBC97214EFBF45BD25AE810C26E (void);
// 0x00000002 System.Byte[] FMZipHelper::FMUnzipBytes(System.Byte[])
extern void FMZipHelper_FMUnzipBytes_m2625C076A010F8B2D86DA45E95E45231C11313FA (void);
// 0x00000003 System.Byte[] FMZipHelper::ReadAllBytes(System.IO.BinaryReader)
extern void FMZipHelper_ReadAllBytes_m6AC058706643D287F7BB6AD9A4E793F0E58D8277 (void);
// 0x00000004 System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor()
extern void StreamDecodingException__ctor_m4F10EB31C40C7AC98DF71DCA43F9D59F7ABDBFED (void);
// 0x00000005 System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor(System.String)
extern void StreamDecodingException__ctor_mE34C08B6F04E109D9A4E0B2DA3DB7C67555C5CE1 (void);
// 0x00000006 System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void StreamDecodingException__ctor_m402438841B691D097F1B428835488DB9CF0D4826 (void);
// 0x00000007 System.Void FMETP.SharpZipLib.ValueOutOfRangeException::.ctor(System.String)
extern void ValueOutOfRangeException__ctor_mEA093740F1E941798A6138627AEF47268840D542 (void);
// 0x00000008 System.Void FMETP.SharpZipLib.ValueOutOfRangeException::.ctor()
extern void ValueOutOfRangeException__ctor_m92C0CD2C12020AC04BEDA8C8CDEAFAF675B0F5D2 (void);
// 0x00000009 System.Void FMETP.SharpZipLib.ValueOutOfRangeException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ValueOutOfRangeException__ctor_m8B9E2FE21F5C7D6E51F5FD3C15DA0747FA527C96 (void);
// 0x0000000A System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor()
extern void SharpZipBaseException__ctor_mEE9B481134AA334BBF8777D11D8001825E16A988 (void);
// 0x0000000B System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor(System.String)
extern void SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914 (void);
// 0x0000000C System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void SharpZipBaseException__ctor_m8A2F854EAC4E1806C22166372CE8BD8A7DA0502D (void);
// 0x0000000D System.Void FMETP.SharpZipLib.Checksum.Crc32::.ctor()
extern void Crc32__ctor_m69D0E66F79D9E4A6937BA772C21E6D3EDA9B6F01 (void);
// 0x0000000E System.Void FMETP.SharpZipLib.Checksum.Crc32::Reset()
extern void Crc32_Reset_m6CB0530F210EBBEE3AA26D0EB48753B858A9AA55 (void);
// 0x0000000F System.Int64 FMETP.SharpZipLib.Checksum.Crc32::get_Value()
extern void Crc32_get_Value_m624503DF9F1044753BE1CB1AD489D7DA5035587B (void);
// 0x00000010 System.Void FMETP.SharpZipLib.Checksum.Crc32::Update(System.Int32)
extern void Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A (void);
// 0x00000011 System.Void FMETP.SharpZipLib.Checksum.Crc32::Update(System.ArraySegment`1<System.Byte>)
extern void Crc32_Update_m6521C75BD473D1869DA1C3F3D00C936F07D3957D (void);
// 0x00000012 System.Void FMETP.SharpZipLib.Checksum.Crc32::.cctor()
extern void Crc32__cctor_m28D994FACF6F0B95FD78D6FC2F9284222B0CEF69 (void);
// 0x00000013 System.Void FMETP.SharpZipLib.Checksum.Adler32::.ctor()
extern void Adler32__ctor_m83B2F2CA75BAC4A379FC88436032D5E4CD15EADF (void);
// 0x00000014 System.Void FMETP.SharpZipLib.Checksum.Adler32::Reset()
extern void Adler32_Reset_mA0BDC81A3B31E939D321A24040D47BBCEACECC22 (void);
// 0x00000015 System.Int64 FMETP.SharpZipLib.Checksum.Adler32::get_Value()
extern void Adler32_get_Value_mB072B09485C331979F68992167A098E33F9C26EB (void);
// 0x00000016 System.Void FMETP.SharpZipLib.Checksum.Adler32::Update(System.ArraySegment`1<System.Byte>)
extern void Adler32_Update_m97D8416919DC739C90BEE5CE6F50C85521246A58 (void);
// 0x00000017 System.Void FMETP.SharpZipLib.Checksum.Adler32::.cctor()
extern void Adler32__cctor_m2C0D47239F5F537E57B30315489B79E9895614C8 (void);
// 0x00000018 System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::AttemptRead()
extern void InflaterDynHeader_AttemptRead_m4593F6F078B40FEC075F5760125F157BD564AC53 (void);
// 0x00000019 System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::.ctor(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern void InflaterDynHeader__ctor_mE2911B9C7C908A0691E08CBCDCFB9573644BCBD9 (void);
// 0x0000001A System.Collections.Generic.IEnumerable`1<System.Boolean> FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::CreateStateMachine()
extern void InflaterDynHeader_CreateStateMachine_mDFFFFAC859E480605B8D903AF0985F656B0FDE1C (void);
// 0x0000001B FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::get_LiteralLengthTree()
extern void InflaterDynHeader_get_LiteralLengthTree_m79581D6BBB306802E0B9BCD492B4D4285509F9F2 (void);
// 0x0000001C FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::get_DistanceTree()
extern void InflaterDynHeader_get_DistanceTree_m4F6820D7E6C0EB2A9A37807FC2241A66361E05F6 (void);
// 0x0000001D System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::.cctor()
extern void InflaterDynHeader__cctor_m5EBE9FC83B3C055E1F094B6DE01A772F6977CB32 (void);
// 0x0000001E System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::.ctor(System.Int32)
extern void U3CCreateStateMachineU3Ed__7__ctor_mDA5BCC7D91C9FE2D8B24060145C553D67CBC0A2B (void);
// 0x0000001F System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.IDisposable.Dispose()
extern void U3CCreateStateMachineU3Ed__7_System_IDisposable_Dispose_mF395E22DD873E589246058B5A071F32C85D1708B (void);
// 0x00000020 System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::MoveNext()
extern void U3CCreateStateMachineU3Ed__7_MoveNext_mC8FF14CD284CC4D2E34F84F2379C483DCB0FC93B (void);
// 0x00000021 System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.Generic.IEnumerator<System.Boolean>.get_Current()
extern void U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_BooleanU3E_get_Current_m6DA70CEC026BE9529FE9D72164DE32F2A3173C5D (void);
// 0x00000022 System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_Reset_m0C8764730A6F04009E62C81D5B02CFE577620BA6 (void);
// 0x00000023 System.Object FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_get_Current_m0B8E932CEA10979633EBB79CDBB0D348140C0E51 (void);
// 0x00000024 System.Collections.Generic.IEnumerator`1<System.Boolean> FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.Generic.IEnumerable<System.Boolean>.GetEnumerator()
extern void U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumerableU3CSystem_BooleanU3E_GetEnumerator_m167460EFE831503FB0E0A7B18C3612A68229266F (void);
// 0x00000025 System.Collections.IEnumerator FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.IEnumerable.GetEnumerator()
extern void U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerable_GetEnumerator_m3950AE6E42EC05FF91763BAB01DC13EE64EED335 (void);
// 0x00000026 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterConstants::.cctor()
extern void DeflaterConstants__cctor_m3168234B4911EEA27420B684ECB3824C838F8DE4 (void);
// 0x00000027 System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.cctor()
extern void InflaterHuffmanTree__cctor_mC7178D83B2CC2CD1FFAFC67BD5C575E6913F1E69 (void);
// 0x00000028 System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.ctor(System.Collections.Generic.IList`1<System.Byte>)
extern void InflaterHuffmanTree__ctor_m8979A23B855128F705F710C36B32AB863EADF1D5 (void);
// 0x00000029 System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::BuildTree(System.Collections.Generic.IList`1<System.Byte>)
extern void InflaterHuffmanTree_BuildTree_mB50BCB289AD6E0A6F2D346F25AF26AFAE424EAD9 (void);
// 0x0000002A System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::GetSymbol(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern void InflaterHuffmanTree_GetSymbol_m0F2DAE53C30D2A81AC3ED21E476F2D3451728442 (void);
// 0x0000002B System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::.ctor(System.Int32)
extern void PendingBuffer__ctor_mD43BCBDDD48F118DE0DA5A277D973B569C0D6041 (void);
// 0x0000002C System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::Reset()
extern void PendingBuffer_Reset_mC6B583113FB3D8C9D12FA178435E7CAA2E1327BB (void);
// 0x0000002D System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteShort(System.Int32)
extern void PendingBuffer_WriteShort_mFD2AE37F8EC3E199F8E16E360B785246C287552B (void);
// 0x0000002E System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteBlock(System.Byte[],System.Int32,System.Int32)
extern void PendingBuffer_WriteBlock_m139A76BB21F2AA07C21B3B701F651A7B24903840 (void);
// 0x0000002F System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::get_BitCount()
extern void PendingBuffer_get_BitCount_mB4A917AF510467CF020B8E173785361118CF9F41 (void);
// 0x00000030 System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::AlignToByte()
extern void PendingBuffer_AlignToByte_m17AE6E041B765E2F686F17560EE7D22E763271D4 (void);
// 0x00000031 System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteBits(System.Int32,System.Int32)
extern void PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD (void);
// 0x00000032 System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteShortMSB(System.Int32)
extern void PendingBuffer_WriteShortMSB_m7597C12105E7CEE947A8CFDE511DD1CB805B3CD9 (void);
// 0x00000033 System.Boolean FMETP.SharpZipLib.Zip.Compression.PendingBuffer::get_IsFlushed()
extern void PendingBuffer_get_IsFlushed_m64096BA3F7B56951266958E6233A77C74844D569 (void);
// 0x00000034 System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::Flush(System.Byte[],System.Int32,System.Int32)
extern void PendingBuffer_Flush_m828B34DC184E3CFC8595E381C70B0068CBD5CDFA (void);
// 0x00000035 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterPending,System.Boolean)
extern void DeflaterEngine__ctor_m1A08402452F08A53C08973444EBAB7435D5CC37A (void);
// 0x00000036 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::Deflate(System.Boolean,System.Boolean)
extern void DeflaterEngine_Deflate_m9DD2CC475173BB34AE655813C1EDE6999B261099 (void);
// 0x00000037 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SetInput(System.Byte[],System.Int32,System.Int32)
extern void DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014 (void);
// 0x00000038 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::NeedsInput()
extern void DeflaterEngine_NeedsInput_mDDBEA2E842B74A55BB42B0DD6790EE4D5A8ED5A0 (void);
// 0x00000039 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::Reset()
extern void DeflaterEngine_Reset_mC49C3D98C62110B5453E75554DF402BE6C776CCE (void);
// 0x0000003A System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::ResetAdler()
extern void DeflaterEngine_ResetAdler_mFFE9B0AFB523F170C8523221606BFF85713DA012 (void);
// 0x0000003B System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::get_Adler()
extern void DeflaterEngine_get_Adler_m778B43EAEE4DE749764D60C3A970BFFCB36814CB (void);
// 0x0000003C System.Int64 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::get_TotalIn()
extern void DeflaterEngine_get_TotalIn_mC27F3CA9F01B1C1974B8DCFF91D7DE9B816F26FD (void);
// 0x0000003D System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::set_Strategy(FMETP.SharpZipLib.Zip.Compression.DeflateStrategy)
extern void DeflaterEngine_set_Strategy_m49FD868FE9D1242660EA59FD2C03881C34FF68AB (void);
// 0x0000003E System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SetLevel(System.Int32)
extern void DeflaterEngine_SetLevel_m2EB416322DD10370B059672DB7C652B8ED1A54DC (void);
// 0x0000003F System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::FillWindow()
extern void DeflaterEngine_FillWindow_m6F85F39A3609EFAA52BEA5CDBE91FFCC81363544 (void);
// 0x00000040 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::UpdateHash()
extern void DeflaterEngine_UpdateHash_m5D5500B13BFF64919559AE7022C08A9F9A5C8D09 (void);
// 0x00000041 System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::InsertString()
extern void DeflaterEngine_InsertString_m5D0EB65FCC71B2A30AD3699AFB65378EAFDF1B2A (void);
// 0x00000042 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SlideWindow()
extern void DeflaterEngine_SlideWindow_mDBA845C625026D2D96B768ED9754DD79D5954F11 (void);
// 0x00000043 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::FindLongestMatch(System.Int32)
extern void DeflaterEngine_FindLongestMatch_m1835826D27ABAB7A5A5D59A6E774233045C8245B (void);
// 0x00000044 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateStored(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateStored_mCA925305F6F3CC7515FE55DD3ABBF515DC038F41 (void);
// 0x00000045 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateFast(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateFast_mBF7F0E5FE39F8D6E4008A05387BE7D37CC1BD0EE (void);
// 0x00000046 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateSlow(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateSlow_m7954800AA1BE94A4BEF695D92CFEC124209018A3 (void);
// 0x00000047 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterPending::.ctor()
extern void DeflaterPending__ctor_m5F119C4BFC420BEB34433D4F5422C464B159B6A7 (void);
// 0x00000048 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::.cctor()
extern void DeflaterHuffman__cctor_m723E4AA3A5F7F92EBFF90A4CF9A14AD20052410E (void);
// 0x00000049 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterPending)
extern void DeflaterHuffman__ctor_m2B19A99445EFEB9B74D2153F80E7F4DC5792F4E1 (void);
// 0x0000004A System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Reset()
extern void DeflaterHuffman_Reset_m695094C3E44BCC5197F58D2CF7F0CCAA51AB8599 (void);
// 0x0000004B System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::SendAllTrees(System.Int32)
extern void DeflaterHuffman_SendAllTrees_m012E9E03627932FA1908F0B9A93872973802A748 (void);
// 0x0000004C System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::CompressBlock()
extern void DeflaterHuffman_CompressBlock_m59A757C733FEAA3CE8288F098417E05A31E35D78 (void);
// 0x0000004D System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushStoredBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void DeflaterHuffman_FlushStoredBlock_m9E4ABC1CFA682400BB19AEA57450D550B7F450D8 (void);
// 0x0000004E System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void DeflaterHuffman_FlushBlock_m3480AE4CB9E565601FE9618FAD10F25972C19AAA (void);
// 0x0000004F System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::IsFull()
extern void DeflaterHuffman_IsFull_mAC09A062E0A78CCD17FAE0260C22B7F79374BA42 (void);
// 0x00000050 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyLit(System.Int32)
extern void DeflaterHuffman_TallyLit_m054CCF260A35DB550E760EC54483DBCAE9DC699A (void);
// 0x00000051 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyDist(System.Int32,System.Int32)
extern void DeflaterHuffman_TallyDist_mAE5F24A91BE5C005AB7F8669A06D274B815099D6 (void);
// 0x00000052 System.Int16 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::BitReverse(System.Int32)
extern void DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B (void);
// 0x00000053 System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Lcode(System.Int32)
extern void DeflaterHuffman_Lcode_m366E178D7EAF0A531BC9970CA28376AD2186D528 (void);
// 0x00000054 System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Dcode(System.Int32)
extern void DeflaterHuffman_Dcode_m606D9F263A5BB3ABAC73BD88317213EA3728CD1A (void);
// 0x00000055 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman,System.Int32,System.Int32,System.Int32)
extern void Tree__ctor_mC175FAE853657848BE82C5A0C72FC6418B9C81B3 (void);
// 0x00000056 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::Reset()
extern void Tree_Reset_m88D2AC4D14EE34A72036C8440EE3ADFCDAB75698 (void);
// 0x00000057 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteSymbol(System.Int32)
extern void Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA (void);
// 0x00000058 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::SetStaticCodes(System.Int16[],System.Byte[])
extern void Tree_SetStaticCodes_m0527B8E8E0520CA78EF84CB97DA604434F6C63EA (void);
// 0x00000059 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildCodes()
extern void Tree_BuildCodes_mA2357168306F9D5F06255D9ED89B1C646039FBDC (void);
// 0x0000005A System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildTree()
extern void Tree_BuildTree_m5FF35C4C61B273025E9B99F2B6C17AFDF1C917E4 (void);
// 0x0000005B System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::GetEncodedLength()
extern void Tree_GetEncodedLength_m54044E0BBD9F68EFA7B3B2EE99BB131B45993997 (void);
// 0x0000005C System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::CalcBLFreq(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
extern void Tree_CalcBLFreq_m81CFB0D0DE3A835DF438203762616C9ECBA4B7AC (void);
// 0x0000005D System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteTree(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
extern void Tree_WriteTree_mAE4872807EDE476D00318D47C89ED01F9E95882A (void);
// 0x0000005E System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildLength(System.Int32[])
extern void Tree_BuildLength_m85D4045DCC50CC1D520AD0FC0C9486F28BC9F0CD (void);
// 0x0000005F System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::.ctor(System.Int32,System.Boolean)
extern void Deflater__ctor_m698267A3FB8383CEDAA02AA133076DF05411CDA8 (void);
// 0x00000060 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Reset()
extern void Deflater_Reset_m84EBDB6798E67F0321AEF102C98B90355A38D74F (void);
// 0x00000061 System.Int64 FMETP.SharpZipLib.Zip.Compression.Deflater::get_TotalIn()
extern void Deflater_get_TotalIn_m361FC4798E6EDA1D0A1E980CFDD628B211815A62 (void);
// 0x00000062 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Flush()
extern void Deflater_Flush_mCC6C370B48AC60F627C8A37E0363D76C951E4B07 (void);
// 0x00000063 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Finish()
extern void Deflater_Finish_mBB8D02EF15BCCC6A0DCD0560C80BC7F707824F60 (void);
// 0x00000064 System.Boolean FMETP.SharpZipLib.Zip.Compression.Deflater::get_IsFinished()
extern void Deflater_get_IsFinished_m84E8F8D8DE052B1016C5E57958E60F271146D446 (void);
// 0x00000065 System.Boolean FMETP.SharpZipLib.Zip.Compression.Deflater::get_IsNeedingInput()
extern void Deflater_get_IsNeedingInput_mB8C863FBF533F2C8BD42C9728907E092EDAEA899 (void);
// 0x00000066 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern void Deflater_SetInput_mD20D223304B17FE2D9637620A767625C0A3619A8 (void);
// 0x00000067 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetLevel(System.Int32)
extern void Deflater_SetLevel_m05D7A3C79B57A8AF9C72613CD730EC90FAB4CE4D (void);
// 0x00000068 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetStrategy(FMETP.SharpZipLib.Zip.Compression.DeflateStrategy)
extern void Deflater_SetStrategy_mAC8771AF03A22E52E55CC5353F334E831F51B2D0 (void);
// 0x00000069 System.Int32 FMETP.SharpZipLib.Zip.Compression.Deflater::Deflate(System.Byte[],System.Int32,System.Int32)
extern void Deflater_Deflate_m4B828E32CABE4976218147C102256B124DBB3A9C (void);
// 0x0000006A System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
extern void Inflater__ctor_m203485CA9DDAD7143A3459BD6BA7D479FA7B1C69 (void);
// 0x0000006B System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::Reset()
extern void Inflater_Reset_mCBF0E4780E8532B200538A1D7601C6EEEE9DA97C (void);
// 0x0000006C System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeHeader()
extern void Inflater_DecodeHeader_mB7913D25A9125D416A703F502EFAB4707D10C92C (void);
// 0x0000006D System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeDict()
extern void Inflater_DecodeDict_m653948520B90CBEFF564A8C76DF83D897026373E (void);
// 0x0000006E System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeHuffman()
extern void Inflater_DecodeHuffman_mAB4BC68E10CE0BC0F737716E1B52EA85096BC740 (void);
// 0x0000006F System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeChksum()
extern void Inflater_DecodeChksum_m82FFF42FBBBDFD86E8EBE560EEB21E7EA9B49DCC (void);
// 0x00000070 System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::Decode()
extern void Inflater_Decode_m58AF8D0040DD4AA68E0E477F77674C329E2D01CC (void);
// 0x00000071 System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern void Inflater_SetInput_mEF7B47FADA151D32D6020DC409B36CF51A22F849 (void);
// 0x00000072 System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[],System.Int32,System.Int32)
extern void Inflater_Inflate_m21F470F4D5A79E52EDA46572AEB52DD878A3C424 (void);
// 0x00000073 System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingInput()
extern void Inflater_get_IsNeedingInput_mD762AD35E335CCC57F14DC3304BF50A845676F16 (void);
// 0x00000074 System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingDictionary()
extern void Inflater_get_IsNeedingDictionary_mC165F0D2C014EECB04B5CD95E1875610942D4155 (void);
// 0x00000075 System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsFinished()
extern void Inflater_get_IsFinished_m90C7D1B230EEEB2150FA15482E9F6A97AF168812 (void);
// 0x00000076 System.Int64 FMETP.SharpZipLib.Zip.Compression.Inflater::get_TotalOut()
extern void Inflater_get_TotalOut_m24899EBBA3839BC2A41C59359686D6CB758576F0 (void);
// 0x00000077 System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::get_RemainingInput()
extern void Inflater_get_RemainingInput_m1D9B9068C5E2A64081E9CA6F3AA11FDE926B6102 (void);
// 0x00000078 System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::.cctor()
extern void Inflater__cctor_m3C243F5B53527B60D563D5CF4B499CC1D825A65E (void);
// 0x00000079 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,FMETP.SharpZipLib.Zip.Compression.Deflater,System.Int32)
extern void DeflaterOutputStream__ctor_mB41165AE84240A90382E4B53F11B299A4CDD58A1 (void);
// 0x0000007A System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish()
extern void DeflaterOutputStream_Finish_m0B586B7D62DFF8763BA25CB632AA7DD4F2EC2499 (void);
// 0x0000007B System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_IsStreamOwner()
extern void DeflaterOutputStream_get_IsStreamOwner_m3023CE9D69403AC66C712F5CC82EB88D8B395E61 (void);
// 0x0000007C System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::EncryptBlock(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_EncryptBlock_m6C13E0059BA831A5AE1863991316BDE10F96ACF9 (void);
// 0x0000007D System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate()
extern void DeflaterOutputStream_Deflate_m8300F455B747EED583C35BE29A3D84A71FF42B7B (void);
// 0x0000007E System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate(System.Boolean)
extern void DeflaterOutputStream_Deflate_mFD09C1AC5EC2990148643BDA9FDED4A88E515B0B (void);
// 0x0000007F System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanRead()
extern void DeflaterOutputStream_get_CanRead_m036DB4EA3070204EC64FECAA891CA131BCF57DCF (void);
// 0x00000080 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanSeek()
extern void DeflaterOutputStream_get_CanSeek_m64D0AD4B961EF54B7029ED70BB36B159740BC1DB (void);
// 0x00000081 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanWrite()
extern void DeflaterOutputStream_get_CanWrite_m2069B75439F0BCC32E24DB5EFC3FED753D5DC97A (void);
// 0x00000082 System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Length()
extern void DeflaterOutputStream_get_Length_mCA97C224C22168134C2DDABD2B4B3405CA44E126 (void);
// 0x00000083 System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Position()
extern void DeflaterOutputStream_get_Position_mD75B65440EEDED8F83F2E071C04FADA083436F11 (void);
// 0x00000084 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::set_Position(System.Int64)
extern void DeflaterOutputStream_set_Position_mDF47ADFCA9BF73AD629D520EA8563A6C2795E95F (void);
// 0x00000085 System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void DeflaterOutputStream_Seek_mD6DE8991D472B16453F824CDC50160028B5DDD68 (void);
// 0x00000086 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::ReadByte()
extern void DeflaterOutputStream_ReadByte_mACE3AF54BD8F42F3FD2290D30A854F9CC0FAC88A (void);
// 0x00000087 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_Read_m8480D2238C5FE7F4D0B13A6752ACFEC62FBE86FE (void);
// 0x00000088 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Flush()
extern void DeflaterOutputStream_Flush_m141FAC28B8AAC682115B212F4A82756D216A6BE9 (void);
// 0x00000089 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Dispose(System.Boolean)
extern void DeflaterOutputStream_Dispose_m0859A0D7B542948EA2174A3934D9D8A0E41348CE (void);
// 0x0000008A System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::GetAuthCodeIfAES()
extern void DeflaterOutputStream_GetAuthCodeIfAES_mF293FA59B514BCD46C50C97764D534CB1191EB54 (void);
// 0x0000008B System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::WriteByte(System.Byte)
extern void DeflaterOutputStream_WriteByte_m6A1E6EC2B811A4FA74D2BA8C932D7F27CCF7A457 (void);
// 0x0000008C System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_Write_m3B8E53BB694B9FEFBDAA9FF03BE50F4A41BDF12A (void);
// 0x0000008D System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.cctor()
extern void DeflaterOutputStream__cctor_mC050D00F80DCFFC143B164FC42E88A868EAA222C (void);
// 0x0000008E System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::PeekBits(System.Int32)
extern void StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6 (void);
// 0x0000008F System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::TryGetBits(System.Int32,System.Int32&,System.Int32)
extern void StreamManipulator_TryGetBits_mC4C8072E3A5991B94049F9C3F9382E2D5B0B9D96 (void);
// 0x00000090 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::TryGetBits(System.Int32,System.Byte[]&,System.Int32)
extern void StreamManipulator_TryGetBits_mEB43579CCD99015132323748F193722297150632 (void);
// 0x00000091 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::DropBits(System.Int32)
extern void StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1 (void);
// 0x00000092 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBits()
extern void StreamManipulator_get_AvailableBits_m7FA2836285F2C2BA6B74A8AA135D770DB9030A7B (void);
// 0x00000093 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBytes()
extern void StreamManipulator_get_AvailableBytes_m4993F192D46EF3A516A6E364BDE5E952D8281457 (void);
// 0x00000094 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SkipToByteBoundary()
extern void StreamManipulator_SkipToByteBoundary_m7C36C130332D5995F8938FFC6F8A45478B77A47C (void);
// 0x00000095 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_IsNeedingInput()
extern void StreamManipulator_get_IsNeedingInput_m962D2BBB56BD8BAC834E2A12897C13A7D4DCE456 (void);
// 0x00000096 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::CopyBytes(System.Byte[],System.Int32,System.Int32)
extern void StreamManipulator_CopyBytes_m6FA30492A0A8534309A1538CF230500658CA3C79 (void);
// 0x00000097 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::Reset()
extern void StreamManipulator_Reset_m7379C771C7C78B44A155BAD54952808BD4F0B5EA (void);
// 0x00000098 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SetInput(System.Byte[],System.Int32,System.Int32)
extern void StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A (void);
// 0x00000099 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::.ctor()
extern void StreamManipulator__ctor_mC16675A2CEAB3EE9F281DC7F5B59D4DF5DC77069 (void);
// 0x0000009A System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
extern void InflaterInputBuffer__ctor_m497E472D25D2E3D02803F5561C2A1C80477EBE26 (void);
// 0x0000009B System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
extern void InflaterInputBuffer_get_Available_m7BDBC3F8048C19BF8C9D3E253BBFDA8601A107DE (void);
// 0x0000009C System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
extern void InflaterInputBuffer_set_Available_mB9B0F342C4B3FB90CCFC0F34B4C5CEDDB7104981 (void);
// 0x0000009D System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(FMETP.SharpZipLib.Zip.Compression.Inflater)
extern void InflaterInputBuffer_SetInflaterInput_m84191E11B0A3E51A5F01CC1CA537E8915FB9243F (void);
// 0x0000009E System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
extern void InflaterInputBuffer_Fill_mD6D37972021807E5F4615E4D33276A13D2D64925 (void);
// 0x0000009F System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputBuffer_ReadClearTextBuffer_m91A4E800CE258802DA172CC80C03A08E919411A5 (void);
// 0x000000A0 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
extern void InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894 (void);
// 0x000000A1 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,FMETP.SharpZipLib.Zip.Compression.Inflater,System.Int32)
extern void InflaterInputStream__ctor_m152F154E3EB66C5B8E82F049C1ED5B37E52A19F8 (void);
// 0x000000A2 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_IsStreamOwner()
extern void InflaterInputStream_get_IsStreamOwner_m3C551CED06177186C1D848E92B2E4B8F2C879814 (void);
// 0x000000A3 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Fill()
extern void InflaterInputStream_Fill_m1DE4E5EC14785EFD25C866FBD31A46A1144445F2 (void);
// 0x000000A4 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanRead()
extern void InflaterInputStream_get_CanRead_m901AA598D0CC2468E7E5B3CE585CEEFA939105A4 (void);
// 0x000000A5 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanSeek()
extern void InflaterInputStream_get_CanSeek_mCB0CB60D7E7E2158874E2C7628079B18B3AA2759 (void);
// 0x000000A6 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanWrite()
extern void InflaterInputStream_get_CanWrite_m61C79F95230F6844B064AAB2D218466D42D9AB35 (void);
// 0x000000A7 System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Length()
extern void InflaterInputStream_get_Length_mE1ED6BA8DC45ECE8669A532BDADF82426AA14EEF (void);
// 0x000000A8 System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Position()
extern void InflaterInputStream_get_Position_m1E7B0E273E2BC810F840C5CC8B70FF60A68D19E4 (void);
// 0x000000A9 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::set_Position(System.Int64)
extern void InflaterInputStream_set_Position_mBC3541E547A6D58CB4C318F765D9D5584C850071 (void);
// 0x000000AA System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Flush()
extern void InflaterInputStream_Flush_m2B3D20377D1F08B4B69BC0ED964F488E107DA881 (void);
// 0x000000AB System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void InflaterInputStream_Seek_m44A365F774ED907FB953EE6964F02C492556933A (void);
// 0x000000AC System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputStream_Write_m9B9B4D2C82868E7F074E04AA712DE2F617CAC2BF (void);
// 0x000000AD System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::WriteByte(System.Byte)
extern void InflaterInputStream_WriteByte_m0D8A47F5A21C9369611A324A154A2B8AAB8A9134 (void);
// 0x000000AE System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Dispose(System.Boolean)
extern void InflaterInputStream_Dispose_mE2742F1199772B431DE0600042D5611EB77432E1 (void);
// 0x000000AF System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputStream_Read_mC36B703875162B5FAB4DC4F47A05AAD596331F50 (void);
// 0x000000B0 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Write(System.Int32)
extern void OutputWindow_Write_m8E87F3E22FAA5D8B122FB61BD4DF14D07F2E42C6 (void);
// 0x000000B1 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::SlowRepeat(System.Int32,System.Int32,System.Int32)
extern void OutputWindow_SlowRepeat_m12DFD2153B9406457D3D8DDCF4C8CB5C8B78E9FD (void);
// 0x000000B2 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Repeat(System.Int32,System.Int32)
extern void OutputWindow_Repeat_mD7467DB942F68DACD17A6A466A2F7974AD2969A5 (void);
// 0x000000B3 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyStored(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator,System.Int32)
extern void OutputWindow_CopyStored_m7676CBB69C573A09CED8B5EB799DC3C8759E6BDF (void);
// 0x000000B4 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetFreeSpace()
extern void OutputWindow_GetFreeSpace_m8FDF473C07DD2A7E667C0D4F9A52657DDD06B99B (void);
// 0x000000B5 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetAvailable()
extern void OutputWindow_GetAvailable_m00EBEEB3AAAEB4C919C63615591FAF21BB60BBEC (void);
// 0x000000B6 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyOutput(System.Byte[],System.Int32,System.Int32)
extern void OutputWindow_CopyOutput_m256CF0DA13981FB04C8D0DB6538E622E11FBEA34 (void);
// 0x000000B7 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Reset()
extern void OutputWindow_Reset_mB97240B42A56D72E549B1967E7646C715C8899E4 (void);
// 0x000000B8 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::.ctor()
extern void OutputWindow__ctor_mA9477B707FFBACD81303FAF7FE2F7C658990FF30 (void);
// 0x000000B9 System.Void FMETP.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream)
extern void GZipInputStream__ctor_mC075983580E627945130BC69DE408678F68542FA (void);
// 0x000000BA System.Void FMETP.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream,System.Int32)
extern void GZipInputStream__ctor_m826EE21BB68D245D97F7BC8BD658370543038BD0 (void);
// 0x000000BB System.Int32 FMETP.SharpZipLib.GZip.GZipInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void GZipInputStream_Read_mC933C5AF086B6DE9150D47B384F08DFA7FBB56A1 (void);
// 0x000000BC System.Boolean FMETP.SharpZipLib.GZip.GZipInputStream::ReadHeader()
extern void GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F (void);
// 0x000000BD System.Void FMETP.SharpZipLib.GZip.GZipInputStream::ReadFooter()
extern void GZipInputStream_ReadFooter_m5B658299CE668EC4346D6EA9571AD17532B9AC45 (void);
// 0x000000BE System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream)
extern void GZipOutputStream__ctor_m6E95590355B8E138591499632751259F020161A6 (void);
// 0x000000BF System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream,System.Int32)
extern void GZipOutputStream__ctor_mCD36645CB0B363E3E72E92182F18C6ED80873E28 (void);
// 0x000000C0 System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void GZipOutputStream_Write_mEDC538022944DBACC0349CAADF293F28D5714CC1 (void);
// 0x000000C1 System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::Dispose(System.Boolean)
extern void GZipOutputStream_Dispose_m765DA62FBF2FC4CCC406731A3EAC26AD1061406D (void);
// 0x000000C2 System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::Finish()
extern void GZipOutputStream_Finish_mF46A4E0A1E918AE80042EA7CCA80044F0D65CA25 (void);
// 0x000000C3 System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::WriteHeader()
extern void GZipOutputStream_WriteHeader_mADDFD2A440ACC6DE89CD4CA229C496D790BE0C55 (void);
// 0x000000C4 System.Void FMETP.SharpZipLib.GZip.GZipException::.ctor()
extern void GZipException__ctor_m91D8DC139F8C46AD9006D1E34323B5280AA6D684 (void);
// 0x000000C5 System.Void FMETP.SharpZipLib.GZip.GZipException::.ctor(System.String)
extern void GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB (void);
// 0x000000C6 System.Void FMETP.SharpZipLib.GZip.GZipException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void GZipException__ctor_mBFACA4B8DAEDC713836A45D82EA0FE360B5FD818 (void);
static Il2CppMethodPointer s_methodPointers[198] = 
{
	FMZipHelper_FMZipBytes_mFD4D5CDF5908FCBC97214EFBF45BD25AE810C26E,
	FMZipHelper_FMUnzipBytes_m2625C076A010F8B2D86DA45E95E45231C11313FA,
	FMZipHelper_ReadAllBytes_m6AC058706643D287F7BB6AD9A4E793F0E58D8277,
	StreamDecodingException__ctor_m4F10EB31C40C7AC98DF71DCA43F9D59F7ABDBFED,
	StreamDecodingException__ctor_mE34C08B6F04E109D9A4E0B2DA3DB7C67555C5CE1,
	StreamDecodingException__ctor_m402438841B691D097F1B428835488DB9CF0D4826,
	ValueOutOfRangeException__ctor_mEA093740F1E941798A6138627AEF47268840D542,
	ValueOutOfRangeException__ctor_m92C0CD2C12020AC04BEDA8C8CDEAFAF675B0F5D2,
	ValueOutOfRangeException__ctor_m8B9E2FE21F5C7D6E51F5FD3C15DA0747FA527C96,
	SharpZipBaseException__ctor_mEE9B481134AA334BBF8777D11D8001825E16A988,
	SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914,
	SharpZipBaseException__ctor_m8A2F854EAC4E1806C22166372CE8BD8A7DA0502D,
	Crc32__ctor_m69D0E66F79D9E4A6937BA772C21E6D3EDA9B6F01,
	Crc32_Reset_m6CB0530F210EBBEE3AA26D0EB48753B858A9AA55,
	Crc32_get_Value_m624503DF9F1044753BE1CB1AD489D7DA5035587B,
	Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A,
	Crc32_Update_m6521C75BD473D1869DA1C3F3D00C936F07D3957D,
	Crc32__cctor_m28D994FACF6F0B95FD78D6FC2F9284222B0CEF69,
	Adler32__ctor_m83B2F2CA75BAC4A379FC88436032D5E4CD15EADF,
	Adler32_Reset_mA0BDC81A3B31E939D321A24040D47BBCEACECC22,
	Adler32_get_Value_mB072B09485C331979F68992167A098E33F9C26EB,
	Adler32_Update_m97D8416919DC739C90BEE5CE6F50C85521246A58,
	Adler32__cctor_m2C0D47239F5F537E57B30315489B79E9895614C8,
	InflaterDynHeader_AttemptRead_m4593F6F078B40FEC075F5760125F157BD564AC53,
	InflaterDynHeader__ctor_mE2911B9C7C908A0691E08CBCDCFB9573644BCBD9,
	InflaterDynHeader_CreateStateMachine_mDFFFFAC859E480605B8D903AF0985F656B0FDE1C,
	InflaterDynHeader_get_LiteralLengthTree_m79581D6BBB306802E0B9BCD492B4D4285509F9F2,
	InflaterDynHeader_get_DistanceTree_m4F6820D7E6C0EB2A9A37807FC2241A66361E05F6,
	InflaterDynHeader__cctor_m5EBE9FC83B3C055E1F094B6DE01A772F6977CB32,
	U3CCreateStateMachineU3Ed__7__ctor_mDA5BCC7D91C9FE2D8B24060145C553D67CBC0A2B,
	U3CCreateStateMachineU3Ed__7_System_IDisposable_Dispose_mF395E22DD873E589246058B5A071F32C85D1708B,
	U3CCreateStateMachineU3Ed__7_MoveNext_mC8FF14CD284CC4D2E34F84F2379C483DCB0FC93B,
	U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_BooleanU3E_get_Current_m6DA70CEC026BE9529FE9D72164DE32F2A3173C5D,
	U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_Reset_m0C8764730A6F04009E62C81D5B02CFE577620BA6,
	U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_get_Current_m0B8E932CEA10979633EBB79CDBB0D348140C0E51,
	U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumerableU3CSystem_BooleanU3E_GetEnumerator_m167460EFE831503FB0E0A7B18C3612A68229266F,
	U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerable_GetEnumerator_m3950AE6E42EC05FF91763BAB01DC13EE64EED335,
	DeflaterConstants__cctor_m3168234B4911EEA27420B684ECB3824C838F8DE4,
	InflaterHuffmanTree__cctor_mC7178D83B2CC2CD1FFAFC67BD5C575E6913F1E69,
	InflaterHuffmanTree__ctor_m8979A23B855128F705F710C36B32AB863EADF1D5,
	InflaterHuffmanTree_BuildTree_mB50BCB289AD6E0A6F2D346F25AF26AFAE424EAD9,
	InflaterHuffmanTree_GetSymbol_m0F2DAE53C30D2A81AC3ED21E476F2D3451728442,
	PendingBuffer__ctor_mD43BCBDDD48F118DE0DA5A277D973B569C0D6041,
	PendingBuffer_Reset_mC6B583113FB3D8C9D12FA178435E7CAA2E1327BB,
	PendingBuffer_WriteShort_mFD2AE37F8EC3E199F8E16E360B785246C287552B,
	PendingBuffer_WriteBlock_m139A76BB21F2AA07C21B3B701F651A7B24903840,
	PendingBuffer_get_BitCount_mB4A917AF510467CF020B8E173785361118CF9F41,
	PendingBuffer_AlignToByte_m17AE6E041B765E2F686F17560EE7D22E763271D4,
	PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD,
	PendingBuffer_WriteShortMSB_m7597C12105E7CEE947A8CFDE511DD1CB805B3CD9,
	PendingBuffer_get_IsFlushed_m64096BA3F7B56951266958E6233A77C74844D569,
	PendingBuffer_Flush_m828B34DC184E3CFC8595E381C70B0068CBD5CDFA,
	DeflaterEngine__ctor_m1A08402452F08A53C08973444EBAB7435D5CC37A,
	DeflaterEngine_Deflate_m9DD2CC475173BB34AE655813C1EDE6999B261099,
	DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014,
	DeflaterEngine_NeedsInput_mDDBEA2E842B74A55BB42B0DD6790EE4D5A8ED5A0,
	DeflaterEngine_Reset_mC49C3D98C62110B5453E75554DF402BE6C776CCE,
	DeflaterEngine_ResetAdler_mFFE9B0AFB523F170C8523221606BFF85713DA012,
	DeflaterEngine_get_Adler_m778B43EAEE4DE749764D60C3A970BFFCB36814CB,
	DeflaterEngine_get_TotalIn_mC27F3CA9F01B1C1974B8DCFF91D7DE9B816F26FD,
	DeflaterEngine_set_Strategy_m49FD868FE9D1242660EA59FD2C03881C34FF68AB,
	DeflaterEngine_SetLevel_m2EB416322DD10370B059672DB7C652B8ED1A54DC,
	DeflaterEngine_FillWindow_m6F85F39A3609EFAA52BEA5CDBE91FFCC81363544,
	DeflaterEngine_UpdateHash_m5D5500B13BFF64919559AE7022C08A9F9A5C8D09,
	DeflaterEngine_InsertString_m5D0EB65FCC71B2A30AD3699AFB65378EAFDF1B2A,
	DeflaterEngine_SlideWindow_mDBA845C625026D2D96B768ED9754DD79D5954F11,
	DeflaterEngine_FindLongestMatch_m1835826D27ABAB7A5A5D59A6E774233045C8245B,
	DeflaterEngine_DeflateStored_mCA925305F6F3CC7515FE55DD3ABBF515DC038F41,
	DeflaterEngine_DeflateFast_mBF7F0E5FE39F8D6E4008A05387BE7D37CC1BD0EE,
	DeflaterEngine_DeflateSlow_m7954800AA1BE94A4BEF695D92CFEC124209018A3,
	DeflaterPending__ctor_m5F119C4BFC420BEB34433D4F5422C464B159B6A7,
	DeflaterHuffman__cctor_m723E4AA3A5F7F92EBFF90A4CF9A14AD20052410E,
	DeflaterHuffman__ctor_m2B19A99445EFEB9B74D2153F80E7F4DC5792F4E1,
	DeflaterHuffman_Reset_m695094C3E44BCC5197F58D2CF7F0CCAA51AB8599,
	DeflaterHuffman_SendAllTrees_m012E9E03627932FA1908F0B9A93872973802A748,
	DeflaterHuffman_CompressBlock_m59A757C733FEAA3CE8288F098417E05A31E35D78,
	DeflaterHuffman_FlushStoredBlock_m9E4ABC1CFA682400BB19AEA57450D550B7F450D8,
	DeflaterHuffman_FlushBlock_m3480AE4CB9E565601FE9618FAD10F25972C19AAA,
	DeflaterHuffman_IsFull_mAC09A062E0A78CCD17FAE0260C22B7F79374BA42,
	DeflaterHuffman_TallyLit_m054CCF260A35DB550E760EC54483DBCAE9DC699A,
	DeflaterHuffman_TallyDist_mAE5F24A91BE5C005AB7F8669A06D274B815099D6,
	DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B,
	DeflaterHuffman_Lcode_m366E178D7EAF0A531BC9970CA28376AD2186D528,
	DeflaterHuffman_Dcode_m606D9F263A5BB3ABAC73BD88317213EA3728CD1A,
	Tree__ctor_mC175FAE853657848BE82C5A0C72FC6418B9C81B3,
	Tree_Reset_m88D2AC4D14EE34A72036C8440EE3ADFCDAB75698,
	Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA,
	Tree_SetStaticCodes_m0527B8E8E0520CA78EF84CB97DA604434F6C63EA,
	Tree_BuildCodes_mA2357168306F9D5F06255D9ED89B1C646039FBDC,
	Tree_BuildTree_m5FF35C4C61B273025E9B99F2B6C17AFDF1C917E4,
	Tree_GetEncodedLength_m54044E0BBD9F68EFA7B3B2EE99BB131B45993997,
	Tree_CalcBLFreq_m81CFB0D0DE3A835DF438203762616C9ECBA4B7AC,
	Tree_WriteTree_mAE4872807EDE476D00318D47C89ED01F9E95882A,
	Tree_BuildLength_m85D4045DCC50CC1D520AD0FC0C9486F28BC9F0CD,
	Deflater__ctor_m698267A3FB8383CEDAA02AA133076DF05411CDA8,
	Deflater_Reset_m84EBDB6798E67F0321AEF102C98B90355A38D74F,
	Deflater_get_TotalIn_m361FC4798E6EDA1D0A1E980CFDD628B211815A62,
	Deflater_Flush_mCC6C370B48AC60F627C8A37E0363D76C951E4B07,
	Deflater_Finish_mBB8D02EF15BCCC6A0DCD0560C80BC7F707824F60,
	Deflater_get_IsFinished_m84E8F8D8DE052B1016C5E57958E60F271146D446,
	Deflater_get_IsNeedingInput_mB8C863FBF533F2C8BD42C9728907E092EDAEA899,
	Deflater_SetInput_mD20D223304B17FE2D9637620A767625C0A3619A8,
	Deflater_SetLevel_m05D7A3C79B57A8AF9C72613CD730EC90FAB4CE4D,
	Deflater_SetStrategy_mAC8771AF03A22E52E55CC5353F334E831F51B2D0,
	Deflater_Deflate_m4B828E32CABE4976218147C102256B124DBB3A9C,
	Inflater__ctor_m203485CA9DDAD7143A3459BD6BA7D479FA7B1C69,
	Inflater_Reset_mCBF0E4780E8532B200538A1D7601C6EEEE9DA97C,
	Inflater_DecodeHeader_mB7913D25A9125D416A703F502EFAB4707D10C92C,
	Inflater_DecodeDict_m653948520B90CBEFF564A8C76DF83D897026373E,
	Inflater_DecodeHuffman_mAB4BC68E10CE0BC0F737716E1B52EA85096BC740,
	Inflater_DecodeChksum_m82FFF42FBBBDFD86E8EBE560EEB21E7EA9B49DCC,
	Inflater_Decode_m58AF8D0040DD4AA68E0E477F77674C329E2D01CC,
	Inflater_SetInput_mEF7B47FADA151D32D6020DC409B36CF51A22F849,
	Inflater_Inflate_m21F470F4D5A79E52EDA46572AEB52DD878A3C424,
	Inflater_get_IsNeedingInput_mD762AD35E335CCC57F14DC3304BF50A845676F16,
	Inflater_get_IsNeedingDictionary_mC165F0D2C014EECB04B5CD95E1875610942D4155,
	Inflater_get_IsFinished_m90C7D1B230EEEB2150FA15482E9F6A97AF168812,
	Inflater_get_TotalOut_m24899EBBA3839BC2A41C59359686D6CB758576F0,
	Inflater_get_RemainingInput_m1D9B9068C5E2A64081E9CA6F3AA11FDE926B6102,
	Inflater__cctor_m3C243F5B53527B60D563D5CF4B499CC1D825A65E,
	DeflaterOutputStream__ctor_mB41165AE84240A90382E4B53F11B299A4CDD58A1,
	DeflaterOutputStream_Finish_m0B586B7D62DFF8763BA25CB632AA7DD4F2EC2499,
	DeflaterOutputStream_get_IsStreamOwner_m3023CE9D69403AC66C712F5CC82EB88D8B395E61,
	DeflaterOutputStream_EncryptBlock_m6C13E0059BA831A5AE1863991316BDE10F96ACF9,
	DeflaterOutputStream_Deflate_m8300F455B747EED583C35BE29A3D84A71FF42B7B,
	DeflaterOutputStream_Deflate_mFD09C1AC5EC2990148643BDA9FDED4A88E515B0B,
	DeflaterOutputStream_get_CanRead_m036DB4EA3070204EC64FECAA891CA131BCF57DCF,
	DeflaterOutputStream_get_CanSeek_m64D0AD4B961EF54B7029ED70BB36B159740BC1DB,
	DeflaterOutputStream_get_CanWrite_m2069B75439F0BCC32E24DB5EFC3FED753D5DC97A,
	DeflaterOutputStream_get_Length_mCA97C224C22168134C2DDABD2B4B3405CA44E126,
	DeflaterOutputStream_get_Position_mD75B65440EEDED8F83F2E071C04FADA083436F11,
	DeflaterOutputStream_set_Position_mDF47ADFCA9BF73AD629D520EA8563A6C2795E95F,
	DeflaterOutputStream_Seek_mD6DE8991D472B16453F824CDC50160028B5DDD68,
	DeflaterOutputStream_ReadByte_mACE3AF54BD8F42F3FD2290D30A854F9CC0FAC88A,
	DeflaterOutputStream_Read_m8480D2238C5FE7F4D0B13A6752ACFEC62FBE86FE,
	DeflaterOutputStream_Flush_m141FAC28B8AAC682115B212F4A82756D216A6BE9,
	DeflaterOutputStream_Dispose_m0859A0D7B542948EA2174A3934D9D8A0E41348CE,
	DeflaterOutputStream_GetAuthCodeIfAES_mF293FA59B514BCD46C50C97764D534CB1191EB54,
	DeflaterOutputStream_WriteByte_m6A1E6EC2B811A4FA74D2BA8C932D7F27CCF7A457,
	DeflaterOutputStream_Write_m3B8E53BB694B9FEFBDAA9FF03BE50F4A41BDF12A,
	DeflaterOutputStream__cctor_mC050D00F80DCFFC143B164FC42E88A868EAA222C,
	StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6,
	StreamManipulator_TryGetBits_mC4C8072E3A5991B94049F9C3F9382E2D5B0B9D96,
	StreamManipulator_TryGetBits_mEB43579CCD99015132323748F193722297150632,
	StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1,
	StreamManipulator_get_AvailableBits_m7FA2836285F2C2BA6B74A8AA135D770DB9030A7B,
	StreamManipulator_get_AvailableBytes_m4993F192D46EF3A516A6E364BDE5E952D8281457,
	StreamManipulator_SkipToByteBoundary_m7C36C130332D5995F8938FFC6F8A45478B77A47C,
	StreamManipulator_get_IsNeedingInput_m962D2BBB56BD8BAC834E2A12897C13A7D4DCE456,
	StreamManipulator_CopyBytes_m6FA30492A0A8534309A1538CF230500658CA3C79,
	StreamManipulator_Reset_m7379C771C7C78B44A155BAD54952808BD4F0B5EA,
	StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A,
	StreamManipulator__ctor_mC16675A2CEAB3EE9F281DC7F5B59D4DF5DC77069,
	InflaterInputBuffer__ctor_m497E472D25D2E3D02803F5561C2A1C80477EBE26,
	InflaterInputBuffer_get_Available_m7BDBC3F8048C19BF8C9D3E253BBFDA8601A107DE,
	InflaterInputBuffer_set_Available_mB9B0F342C4B3FB90CCFC0F34B4C5CEDDB7104981,
	InflaterInputBuffer_SetInflaterInput_m84191E11B0A3E51A5F01CC1CA537E8915FB9243F,
	InflaterInputBuffer_Fill_mD6D37972021807E5F4615E4D33276A13D2D64925,
	InflaterInputBuffer_ReadClearTextBuffer_m91A4E800CE258802DA172CC80C03A08E919411A5,
	InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894,
	InflaterInputStream__ctor_m152F154E3EB66C5B8E82F049C1ED5B37E52A19F8,
	InflaterInputStream_get_IsStreamOwner_m3C551CED06177186C1D848E92B2E4B8F2C879814,
	InflaterInputStream_Fill_m1DE4E5EC14785EFD25C866FBD31A46A1144445F2,
	InflaterInputStream_get_CanRead_m901AA598D0CC2468E7E5B3CE585CEEFA939105A4,
	InflaterInputStream_get_CanSeek_mCB0CB60D7E7E2158874E2C7628079B18B3AA2759,
	InflaterInputStream_get_CanWrite_m61C79F95230F6844B064AAB2D218466D42D9AB35,
	InflaterInputStream_get_Length_mE1ED6BA8DC45ECE8669A532BDADF82426AA14EEF,
	InflaterInputStream_get_Position_m1E7B0E273E2BC810F840C5CC8B70FF60A68D19E4,
	InflaterInputStream_set_Position_mBC3541E547A6D58CB4C318F765D9D5584C850071,
	InflaterInputStream_Flush_m2B3D20377D1F08B4B69BC0ED964F488E107DA881,
	InflaterInputStream_Seek_m44A365F774ED907FB953EE6964F02C492556933A,
	InflaterInputStream_Write_m9B9B4D2C82868E7F074E04AA712DE2F617CAC2BF,
	InflaterInputStream_WriteByte_m0D8A47F5A21C9369611A324A154A2B8AAB8A9134,
	InflaterInputStream_Dispose_mE2742F1199772B431DE0600042D5611EB77432E1,
	InflaterInputStream_Read_mC36B703875162B5FAB4DC4F47A05AAD596331F50,
	OutputWindow_Write_m8E87F3E22FAA5D8B122FB61BD4DF14D07F2E42C6,
	OutputWindow_SlowRepeat_m12DFD2153B9406457D3D8DDCF4C8CB5C8B78E9FD,
	OutputWindow_Repeat_mD7467DB942F68DACD17A6A466A2F7974AD2969A5,
	OutputWindow_CopyStored_m7676CBB69C573A09CED8B5EB799DC3C8759E6BDF,
	OutputWindow_GetFreeSpace_m8FDF473C07DD2A7E667C0D4F9A52657DDD06B99B,
	OutputWindow_GetAvailable_m00EBEEB3AAAEB4C919C63615591FAF21BB60BBEC,
	OutputWindow_CopyOutput_m256CF0DA13981FB04C8D0DB6538E622E11FBEA34,
	OutputWindow_Reset_mB97240B42A56D72E549B1967E7646C715C8899E4,
	OutputWindow__ctor_mA9477B707FFBACD81303FAF7FE2F7C658990FF30,
	GZipInputStream__ctor_mC075983580E627945130BC69DE408678F68542FA,
	GZipInputStream__ctor_m826EE21BB68D245D97F7BC8BD658370543038BD0,
	GZipInputStream_Read_mC933C5AF086B6DE9150D47B384F08DFA7FBB56A1,
	GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F,
	GZipInputStream_ReadFooter_m5B658299CE668EC4346D6EA9571AD17532B9AC45,
	GZipOutputStream__ctor_m6E95590355B8E138591499632751259F020161A6,
	GZipOutputStream__ctor_mCD36645CB0B363E3E72E92182F18C6ED80873E28,
	GZipOutputStream_Write_mEDC538022944DBACC0349CAADF293F28D5714CC1,
	GZipOutputStream_Dispose_m765DA62FBF2FC4CCC406731A3EAC26AD1061406D,
	GZipOutputStream_Finish_mF46A4E0A1E918AE80042EA7CCA80044F0D65CA25,
	GZipOutputStream_WriteHeader_mADDFD2A440ACC6DE89CD4CA229C496D790BE0C55,
	GZipException__ctor_m91D8DC139F8C46AD9006D1E34323B5280AA6D684,
	GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB,
	GZipException__ctor_mBFACA4B8DAEDC713836A45D82EA0FE360B5FD818,
};
static const int32_t s_InvokerIndices[198] = 
{
	6004,
	6004,
	6004,
	4055,
	3287,
	1982,
	3287,
	4055,
	1982,
	4055,
	3287,
	1982,
	4055,
	4055,
	3966,
	3271,
	3106,
	6207,
	4055,
	4055,
	3966,
	3106,
	6207,
	4017,
	3287,
	3983,
	3983,
	3983,
	6207,
	3271,
	4055,
	4017,
	4017,
	4055,
	3983,
	3983,
	3983,
	6207,
	6207,
	3287,
	3287,
	2417,
	3271,
	4055,
	3271,
	1230,
	3965,
	4055,
	1802,
	3271,
	4017,
	963,
	1979,
	1573,
	1230,
	4017,
	4055,
	4055,
	3965,
	3966,
	3271,
	3271,
	4055,
	4055,
	3965,
	4055,
	2869,
	1573,
	1573,
	1573,
	4055,
	6207,
	3287,
	4055,
	3271,
	4055,
	835,
	835,
	4017,
	2869,
	1521,
	5920,
	5932,
	5932,
	833,
	4055,
	3271,
	1976,
	4055,
	4055,
	3965,
	3287,
	3287,
	3287,
	1837,
	4055,
	3966,
	4055,
	4055,
	4017,
	4017,
	1230,
	3271,
	3271,
	963,
	3320,
	4055,
	4017,
	4017,
	4017,
	4017,
	4017,
	1230,
	963,
	4017,
	4017,
	4017,
	3966,
	3965,
	6207,
	1242,
	4055,
	4017,
	1230,
	4055,
	3320,
	4017,
	4017,
	4017,
	3966,
	3966,
	3272,
	1421,
	3965,
	963,
	4055,
	3320,
	4055,
	3320,
	1230,
	6207,
	2403,
	1089,
	1089,
	3271,
	3965,
	3965,
	4055,
	4017,
	963,
	4055,
	1230,
	4055,
	1971,
	3965,
	3271,
	3287,
	4055,
	963,
	3965,
	1242,
	4017,
	4055,
	4017,
	4017,
	4017,
	3966,
	3966,
	3272,
	4055,
	1421,
	1230,
	3320,
	3320,
	963,
	3271,
	1190,
	1802,
	1375,
	3965,
	3965,
	963,
	4055,
	4055,
	3287,
	1971,
	963,
	4017,
	4055,
	3287,
	1971,
	1230,
	3320,
	4055,
	4055,
	4055,
	3287,
	1982,
};
extern const CustomAttributesCacheGenerator g_FMSharpZip_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_FMSharpZip_CodeGenModule;
const Il2CppCodeGenModule g_FMSharpZip_CodeGenModule = 
{
	"FMSharpZip.dll",
	198,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_FMSharpZip_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
