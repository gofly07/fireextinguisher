﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net;
using System.Net.Sockets;

public class Panel_AllInOne : MonoBehaviour
{
    //[SerializeField] private FMNetworkManager fMNetworkManager;

    [Header("Sync View Paramenters")]
    [SerializeField] private RectTransform syncView;
    [SerializeField] private Button btQuitSync;

    [Header("IP Paramenters")]
    [SerializeField] private Text txtLocalIP;
    [SerializeField] private Text txtIPText;
    [SerializeField] private Image imgIPOutLine;

    [Header("Main Paramenters")]
    [SerializeField] private Image imgLoading;
    [SerializeField] private Image imgLogo;
    [SerializeField] private Text txtRemind;
    [SerializeField] private Button btSync;
    [SerializeField] private Button btBack;
  
    public event EventHandler EHCloseAllInOne;

    private void OnEnable()
    {
        Init();
        btBack.onClick.AddListener(delegate { BackToMenu(); });
        btSync.onClick.AddListener(delegate { Sync(); });
        btQuitSync.onClick.AddListener(delegate { QuitSync(); });
    }

    private void OnDisable()
    {
        FMNetworkManager.instance.enabled = false;
        FMNetworkManager.instance.UIStatus.text = "Debug";
    }

    private void Init()
    {
        syncView.gameObject.SetActive(false);

        txtLocalIP.gameObject.SetActive(true);
        txtIPText.gameObject.SetActive(true);
        imgIPOutLine.gameObject.SetActive(true);
        txtLocalIP.text = GetLocalIPAddress();

        imgLoading.gameObject.SetActive(true);
        imgLogo.gameObject.SetActive(false);
        txtRemind.gameObject.SetActive(true);
        txtRemind.text = "等待一體機連線";
        txtRemind.color = Color.gray;

        btSync.gameObject.SetActive(false);

        FMNetworkManager.instance.enabled = true;
        FMNetworkManager.instance.Action_InitAsServer();

        InvokeRepeating("Updating", 0, 1);
    }
    
    public void Action_ProcessStringData(string _string)
    {
        if (_string == GetLocalIPAddress())
        {
            FMNetworkManager.instance.SendToOthers(GPTT.Data.APIHelper.GetUserData().id);
            swsw = true;
        }
        Debug.Log("Received  string: " + _string);
    }

    public static bool swsw = false;

    private void Updating()
    {
        if (FMNetworkManager.instance.NetworkType == FMNetworkType.Server && FMNetworkManager.instance.isActiveAndEnabled &&
            FMNetworkManager.instance.ServerSettings.ConnectionCount == 1 && FMNetworkManager.instance.Server.ConnectionCount == 1 &&
            swsw)
        {
            txtLocalIP.gameObject.SetActive(false);
            txtIPText.gameObject.SetActive(false);
            imgIPOutLine.gameObject.SetActive(false);

            imgLoading.gameObject.SetActive(false);
            imgLogo.gameObject.SetActive(true);
            txtRemind.gameObject.SetActive(true);
            txtRemind.text = "連線成功";
            txtRemind.color = Color.green;

            btSync.gameObject.SetActive(true);

            CancelInvoke();
        }
        else
        {
            // syncView.gameObject.SetActive(false);
            //
            // txtLocalIP.gameObject.SetActive(true);
            // txtIPText.gameObject.SetActive(true);
            // imgIPOutLine.gameObject.SetActive(true);
            // txtLocalIP.text = GetLocalIPAddress();
            //
            // imgLoading.gameObject.SetActive(true);
            // imgLogo.gameObject.SetActive(false);
            // txtRemind.gameObject.SetActive(true);
            // txtRemind.text = "等待一體機連線";
            // txtRemind.color = Color.gray;
            //
            // btSync.gameObject.SetActive(false);
        }
    }

    private void BackToMenu()
    {
        EHCloseAllInOne(this, EventArgs.Empty);
        FMNetworkManager.instance.enabled = false;
        FMNetworkManager.instance.UIStatus.text = "Debug";
    }
    
    private void Sync()
    {
        syncView.gameObject.SetActive(true);
    }

    private void QuitSync()
    {
        syncView.gameObject.SetActive(false);

        if (FMNetworkManager.instance.NetworkType != FMNetworkType.Server || !FMNetworkManager.instance.isActiveAndEnabled ||
            FMNetworkManager.instance.ServerSettings.ConnectionCount != 1 || FMNetworkManager.instance.Server.ConnectionCount != 1 ||
            !swsw)
        {
            syncView.gameObject.SetActive(false);

            txtLocalIP.gameObject.SetActive(true);
            txtIPText.gameObject.SetActive(true);
            imgIPOutLine.gameObject.SetActive(true);
            txtLocalIP.text = GetLocalIPAddress();

            imgLoading.gameObject.SetActive(true);
            imgLogo.gameObject.SetActive(false);
            txtRemind.gameObject.SetActive(true);
            txtRemind.text = "等待一體機連線";
            txtRemind.color = Color.gray;

            btSync.gameObject.SetActive(false);
        }
    }

    private string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
                return ip.ToString();
        }
        throw new System.Exception("No network adapters with an IPv4 address in the system!");
    }
}
