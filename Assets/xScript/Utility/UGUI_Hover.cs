using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UGUI_Hover : MonoBehaviour, IPointerEnterHandler ,IPointerExitHandler, IPointerDownHandler
{
    [SerializeField] private Sprite spNormal;
    [SerializeField] private Sprite spHover;
    [SerializeField] private Sprite spPress;
    //[SerializeField] private Color32 colorNormal = Color.white;
    //[SerializeField] private Color32 colorHover = new Color32(57, 98, 121, 255);

    [SerializeField] private Image imgTarget;
    //[SerializeField] private Text txtTarget;

    private void OnEnable()
    {
        imgTarget.sprite = spNormal;
        //txtTarget.color = colorNormal;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        imgTarget.sprite = spHover;
        //txtTarget.color = colorHover;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        imgTarget.sprite = spNormal;
        //txtTarget.color = colorNormal;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        imgTarget.sprite = spPress;
    }

}
