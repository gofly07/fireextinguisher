using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UGUI_HoverText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Color32 colorNormal = Color.white;
    [SerializeField] private Color32 colorHover = new Color32(57, 98, 121, 255);
    [SerializeField] private Text txtTarget;
    private void OnEnable()
    {
        txtTarget.color = colorNormal;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        txtTarget.color = colorHover;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        txtTarget.color = colorNormal;
    }
}
