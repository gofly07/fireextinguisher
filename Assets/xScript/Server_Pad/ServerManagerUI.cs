using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class ServerManagerUI : MonoBehaviour
{

    [SerializeField] private Panel_Login panelLogin;
    [SerializeField] private Panel_Menu panelMenu;
    [SerializeField] private Panel_AllInOne panelAllInOne;
    [SerializeField] private Panel_DataRecord panelDataRecord;

    public IEnumerator Init()
    {
        panelLogin.gameObject.SetActive(true);
        panelMenu.gameObject.SetActive(false);
        panelAllInOne.gameObject.SetActive(false);
        panelDataRecord.gameObject.SetActive(false);

        yield return null;

        panelLogin.EHOpenMenuPanel += EventOpenMenu;
        panelMenu.EHOpenAllInOne += EventOpenAllInOne;
        panelMenu.EHOpenDataRecord += EventOpenDataRecord;
        panelMenu.EHLogout += EventLogout;

        panelAllInOne.EHCloseAllInOne += EventBackToMenu;
        panelDataRecord.EHCloseDataRecord += EventBackToMenu;
    }

    private void EventOpenMenu(object sender, EventArgs e)
    {
        panelLogin.gameObject.SetActive(false);
        panelMenu.gameObject.SetActive(true);
    }

    private void EventOpenAllInOne(object sender, EventArgs e)
    {
        panelMenu.gameObject.SetActive(false);
        panelAllInOne.gameObject.SetActive(true);
    }

    private void EventOpenDataRecord(object sender, EventArgs e)
    {
        panelMenu.gameObject.SetActive(false);
        panelDataRecord.gameObject.SetActive(true);
    }

    private void EventLogout(object sender, EventArgs e)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //Application.Quit();
    }

    private void EventBackToMenu(object sender, EventArgs e)
    {
        panelMenu.gameObject.SetActive(true);
        panelAllInOne.gameObject.SetActive(false);
        panelDataRecord.gameObject.SetActive(false);
    }

}
