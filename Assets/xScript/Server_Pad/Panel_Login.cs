﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;
using static GPTT.Data.API;
using System.Text.RegularExpressions;

public class Panel_Login : MonoBehaviour
{
    [SerializeField] private InputField ifAccount;
    [SerializeField] private InputField ifPassword;

    [SerializeField] private Toggle tgVisiable;
    [SerializeField] private Button btLoginByAccount;
    [SerializeField] private Button btLoginByVisitor;

    [SerializeField] private Sprite spVisiable;
    [SerializeField] private Sprite spInvisiable;

    [SerializeField] private RectTransform errorIcon;
    public event EventHandler EHOpenMenuPanel;

    private bool isAccountMathRuler, isAccountCharMathCount, isAccountHaveContinuousDot;

    private void OnEnable()
    {
        btLoginByAccount.onClick.AddListener(delegate { LoginByAccount(); });
        btLoginByVisitor.onClick.AddListener(delegate { LoginByVisitor(); });
        tgVisiable.onValueChanged.AddListener(delegate { TgVisiableValueChanged(tgVisiable); });
        ifAccount.onEndEdit.AddListener(delegate { AccountVerification(); });
        errorIcon.gameObject.SetActive(false);

        isAccountMathRuler = false;
        isAccountCharMathCount = false;
        isAccountHaveContinuousDot = false;

        //Debug.Log(GPTT.Data.Util.GetSerialNumber());
        GPTT.Data.APIHelper.Init();
    }

    private void AccountVerification()
    {
        if (ifAccount.text == "" || ifAccount.text == String.Empty)
        {
            errorIcon.gameObject.SetActive(false);
            return;
        }

        isAccountMathRuler = Regex.IsMatch(ifAccount.text, @"^[a-zA-Z0-9][a-zA-Z0-9\.]+@[a-zA-Z0-9\.]+\.[a-zA-Z0-9]{2,3}$");
        isAccountCharMathCount = Regex.IsMatch(ifAccount.text, @"^.{1,30}$");
        isAccountHaveContinuousDot = DetectAccountHave2Dot();

        if (isAccountMathRuler && isAccountCharMathCount && !isAccountHaveContinuousDot)
            errorIcon.gameObject.SetActive(false);
        else
            ShowAccountError();
    }

    private void ShowAccountError()
    {
        errorIcon.gameObject.SetActive(true);

        if (ifAccount.text.Substring(0, 1) == "@" || ifAccount.text.Substring(0, 1) == ".")
        {
            errorIcon.GetComponent<Text>().text = "第一個字元必須是英文字母 (a-z)、數字 (0-9)。";
            return;
        }

        if(isAccountHaveContinuousDot)
        {
            errorIcon.GetComponent<Text>().text = "不能使用連續的小數點。";
            return;
        }

        if (!isAccountMathRuler)
        {
            errorIcon.GetComponent<Text>().text = "不能使用英文字母 (a-z)、數字 (0-9) 、小數點 (.)以外的字元。";
            return;
        }

        if (!isAccountCharMathCount)
        {
            errorIcon.GetComponent<Text>().text = "長度必須小於30個字元。";
            return;
        }
    }

    private bool DetectAccountHave2Dot()
    {
        for (int i = 0; i < ifAccount.text.Length; i++)
        {
            if (ifAccount.text[i].Equals('.') && ifAccount.text[i + 1].Equals('.'))
                return true;
        }

        return false;
    }

    private void LoginByAccount()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            if (isAccountMathRuler && isAccountCharMathCount && !isAccountHaveContinuousDot)
            {
                //登入
                Login user = new Login
                {
                    account = ifAccount.text,
                    pwd = GPTT.Data.Util.ToMD5(ifPassword.text)
                };
                StartCoroutine(FLogin(user, delegate (User user) { }));
            }
        }
        else if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            errorIcon.gameObject.SetActive(true);
            errorIcon.GetComponent<Text>().text = "連線異常。";
        }
    }

    private IEnumerator FLogin(Login login, Action<User> onSuccess = null, Action<string> onError = null)
    {
        yield return GPTT.Data.API.Login_API(login);

        if (GPTT.Data.API.ResponseMessage["responseCode"] == "200")
        {
            EHOpenMenuPanel(this, EventArgs.Empty);
            Debug.Log(UserData.id + " " + UserData.name + " " + UserData.email + " " + UserData.unit);
            GPTT.Data.APIHelper.isLogin = true;
            onSuccess?.Invoke(UserData);
        }
        else
        {
            errorIcon.gameObject.SetActive(true);
            errorIcon.GetComponent<Text>().text = "帳號或密碼錯誤。";
            Debug.Log(GPTT.Data.API.ResponseMessage["message"]);
            onError?.Invoke(GPTT.Data.API.ResponseMessage["message"]);
        }
    }

    private void LoginByVisitor()
    {
        //EHOpenMenuPanel(this, EventArgs.Empty);
    }

    private void TgVisiableValueChanged(Toggle tg)
    {
        if (tg.isOn)
        {
            ifPassword.contentType = InputField.ContentType.Standard;
            ifPassword.ForceLabelUpdate();
            tgVisiable.GetComponent<Image>().sprite = spInvisiable;
            tgVisiable.GetComponent<RectTransform>().sizeDelta = new Vector2(40, 35);
        }
        else if (!tg.isOn)
        {
            ifPassword.contentType = InputField.ContentType.Password;
            ifPassword.ForceLabelUpdate();
            tgVisiable.GetComponent<Image>().sprite = spVisiable;
            tgVisiable.GetComponent<RectTransform>().sizeDelta = new Vector2(40, 24);
        }
    }

}