﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Comparison`1<Obi.ObiStructuralElement>
struct Comparison_1_tD125F39287A56230AD2F8E94903C7C682E966A75;
// System.Converter`2<Obi.IBounded,Obi.Triangle>
struct Converter_2_tA5CFCDD2DDF09EC21725B24DD40CB2E6C928F1F2;
// System.Func`2<UnityEngine.Vector3Int,System.Single>
struct Func_2_t60C15697969143DDD791767187830BED28B48143;
// System.Func`2<Obi.ObiSolver/ParticleInActor,System.Boolean>
struct Func_2_t8E696E949C725E6CEB283DBB48AC377CC3AF687E;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2DC97C7D486BF9E077C2BC2E517E434F393AA76E;
// System.Collections.Generic.IList`1<System.Single>
struct IList_1_tE2CB81875EDCE22646E13306A3501DF8022F4959;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<Obi.ObiActor>
struct List_1_t817AE5D45DC7F38B962828B6735F7715EB2D865B;
// System.Collections.Generic.List`1<Obi.ObiBendConstraintsBatch>
struct List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314;
// System.Collections.Generic.List`1<Obi.ObiChainConstraintsBatch>
struct List_1_t3996F24BACDCEF0ED723B57F053B3194023B0416;
// System.Collections.Generic.List`1<Obi.ObiDistanceConstraintsBatch>
struct List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A;
// System.Collections.Generic.List`1<Obi.ObiParticleGroup>
struct List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF;
// System.Collections.Generic.List`1<Obi.ObiStretchShearConstraintsBatch>
struct List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC;
// System.Collections.Generic.List`1<Obi.ObiStructuralElement>
struct List_1_tC53BC7C82E27162EC05E16FB1845BC506EE2A03B;
// System.Collections.Generic.List`1<Obi.ObiWingedPoint>
struct List_1_tA9940460E284B6579F43F2ED432B34ADA9EEDFB3;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// Obi.ObiConstraints`1<Obi.ObiBendConstraintsBatch>
struct ObiConstraints_1_t1A77FB453F8651AB273DB0DE943C8AF5AF981F82;
// Obi.ObiConstraints`1<Obi.ObiChainConstraintsBatch>
struct ObiConstraints_1_tC13134EAD7E5A4205F5834810FB89F47972C220E;
// Obi.ObiConstraints`1<Obi.ObiDistanceConstraintsBatch>
struct ObiConstraints_1_tDDB54978FB9AEE6BD8B391B96A13174F2E9966ED;
// Obi.ObiConstraints`1<Obi.ObiStretchShearConstraintsBatch>
struct ObiConstraints_1_t9491848D3D98BFF80E3C3AAF710BCE40F4F51610;
// Obi.ObiConstraints`1<System.Object>
struct ObiConstraints_1_tCEA141EDE83195DC4D0CD9725268B012EE741A3C;
// Obi.ObiInterpolator`1<UnityEngine.Color>
struct ObiInterpolator_1_t177396E77AAB9C278648D43781F9B09CD8A93B0E;
// Obi.ObiInterpolator`1<System.Int32>
struct ObiInterpolator_1_t9FEC24FCAE22DF8DBD57E606B60E5112FC864723;
// Obi.ObiInterpolator`1<System.Single>
struct ObiInterpolator_1_t38066B7EA7D2875D2AB087D6EE41C1F88680E9A1;
// Obi.ObiInterpolator`1<UnityEngine.Vector3>
struct ObiInterpolator_1_t762F3E1965E8012492DC51EA1BE1789DAA8944B2;
// Obi.ObiList`1<Oni/Contact>
struct ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3;
// Obi.ObiPathDataChannelIdentity`1<UnityEngine.Color>
struct ObiPathDataChannelIdentity_1_tB099F9C735620BE0BE1A9FE2208986F0128A2104;
// Obi.ObiPathDataChannelIdentity`1<System.Int32>
struct ObiPathDataChannelIdentity_1_tB49FD768113A83C96CCE65F0A01E480360D13F86;
// Obi.ObiPathDataChannelIdentity`1<System.Single>
struct ObiPathDataChannelIdentity_1_t472CD690CECDE58D7B1E3020921399E15844875C;
// Obi.ObiPathDataChannelIdentity`1<UnityEngine.Vector3>
struct ObiPathDataChannelIdentity_1_t58918EE3CAC1576B050AFB8CF1148BD9E5F7B230;
// Obi.PriorityQueue`1<Obi.VoxelPathFinder/TargetVoxel>
struct PriorityQueue_1_t0510E3608062DAF6C39185E22673183229DB4D03;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>
struct ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF;
// System.Collections.Generic.List`1<System.Int32>[]
struct List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C;
// UnityEngine.Camera[]
struct CameraU5BU5D_tAF84B9EC9AF40F1B6294BCEBA82A1AD123A9D001;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// Obi.IObiConstraints[]
struct IObiConstraintsU5BU5D_t4021995930E99D99E580CBA120F9AC0526C8032F;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// Obi.ObiBendConstraintsBatch[]
struct ObiBendConstraintsBatchU5BU5D_t31407D52D83618D9F947C6C72D1EC164E89AF3AF;
// Obi.ObiDistanceConstraintsBatch[]
struct ObiDistanceConstraintsBatchU5BU5D_t1F0BF8CB91EFA8638F726455614BAAC2A2E45701;
// Obi.ObiParticleGroup[]
struct ObiParticleGroupU5BU5D_t161012A1D746285DC87BD0A4C053405A002BF30D;
// Obi.ObiStretchShearConstraintsBatch[]
struct ObiStretchShearConstraintsBatchU5BU5D_tA7239A77B085F5EE77A70F4039A95915715B5665;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t33BCF5C401A053481BF4098B8D32E6C5DD53B9FD;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.Vector3Int[]
struct Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;
// Obi.MeshVoxelizer/Voxel[]
struct VoxelU5BU5D_tA0FDC07F9F3BB9F86147BF6E063718D1BAAD3E55;
// Obi.ObiSolver/ParticleInActor[]
struct ParticleInActorU5BU5D_t3EB732B2921D698AC54DD3C56E5D31A0513E8707;
// Oni/Contact[]
struct ContactU5BU5D_t0EA4D0F4F28544AEC9172BDBCC872BD763772C2C;
// System.Boolean[0...,0...,0...]
struct BooleanU5BU2CU2CU5D_tB8F56EB24D5C42F188A3C72BFF48D9AC9B78E58B;
// UnityEngine.Vector3Int[0...,0...,0...]
struct Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.EventArgs
struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// Obi.IBendConstraintsBatchImpl
struct IBendConstraintsBatchImpl_t1D10E0D23CB79EC0D6986070A2C79AE48BEE1932;
// Obi.IBounded
struct IBounded_tD15153C6212B0516F2F6B5BD9CB4A8D81658FB1E;
// Obi.IChainConstraintsBatchImpl
struct IChainConstraintsBatchImpl_t7349BC548F09B7277520676E11FAA8FEF5EE3DDE;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// Obi.IDistanceConstraintsBatchImpl
struct IDistanceConstraintsBatchImpl_t9A0A9B3E5C4BD680ACE74EB79CA490A7119DF2ED;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// Obi.IObiBackend
struct IObiBackend_t845592D87D7F3521C2341EA829726D728FB0904B;
// Obi.ISolverImpl
struct ISolverImpl_t058083E010450A10D71A5776169CE99C4B668FC6;
// Obi.IStretchShearConstraintsBatchImpl
struct IStretchShearConstraintsBatchImpl_t121B66A56F98D6655A407ED0E361D848E1CB99B1;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// Obi.MeshVoxelizer
struct MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// Obi.ObiActor
struct ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6;
// Obi.ObiActorBlueprint
struct ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD;
// Obi.ObiAerodynamicConstraintsData
struct ObiAerodynamicConstraintsData_t0C7593E41FF1ECD984BD4D9FF8C84C96DDBA5563;
// Obi.ObiBendConstraintsBatch
struct ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79;
// Obi.ObiBendConstraintsData
struct ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED;
// Obi.ObiBendTwistConstraintsData
struct ObiBendTwistConstraintsData_t681D2A3964FDCF4E12E05CED3F7C62E640440CEE;
// Obi.ObiChainConstraintsBatch
struct ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E;
// Obi.ObiChainConstraintsData
struct ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0;
// Obi.ObiCollisionMaterial
struct ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F;
// Obi.ObiColorDataChannel
struct ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2;
// Obi.ObiConstraintsBatch
struct ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127;
// Obi.ObiDistanceConstraintsBatch
struct ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE;
// Obi.ObiDistanceConstraintsData
struct ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB;
// Obi.ObiMassDataChannel
struct ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996;
// Obi.ObiNativeFloatList
struct ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311;
// Obi.ObiNativeInt4List
struct ObiNativeInt4List_t77B698CA82EEEFCB8C86017B00EABC6A4C3C0344;
// Obi.ObiNativeIntList
struct ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13;
// Obi.ObiNativeQuaternionList
struct ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4;
// Obi.ObiNativeVector2List
struct ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD;
// Obi.ObiNativeVector3List
struct ObiNativeVector3List_t91CCFD3FCE3DFE0A243C7A6EB5C3117F2CE8C8D7;
// Obi.ObiNativeVector4List
struct ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1;
// Obi.ObiNormalDataChannel
struct ObiNormalDataChannel_tA3F5F99B539B56E0703FA0603B89DCC345AC3296;
// Obi.ObiParticleGroup
struct ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE;
// Obi.ObiPath
struct ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459;
// Obi.ObiPhaseDataChannel
struct ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670;
// Obi.ObiPinConstraintsData
struct ObiPinConstraintsData_t55BA2D5374F110C343BB649D24477242C2961DDC;
// Obi.ObiPointsDataChannel
struct ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507;
// Obi.ObiRodBlueprint
struct ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597;
// Obi.ObiRope
struct ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464;
// Obi.ObiRopeBlueprint
struct ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B;
// Obi.ObiRopeBlueprintBase
struct ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687;
// Obi.ObiRotationalMassDataChannel
struct ObiRotationalMassDataChannel_tC062F8C628B38CBE7F635761D65A2BA348541E4B;
// Obi.ObiShapeMatchingConstraintsData
struct ObiShapeMatchingConstraintsData_t5CC282CC712A59B303FB502754F122D4C14801C8;
// Obi.ObiSkinConstraintsData
struct ObiSkinConstraintsData_t7F610D65CCE6B7699A70337524422428212F7353;
// Obi.ObiSolver
struct ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634;
// Obi.ObiStretchShearConstraintsBatch
struct ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B;
// Obi.ObiStretchShearConstraintsData
struct ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC;
// Obi.ObiStructuralElement
struct ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759;
// Obi.ObiTetherConstraintsData
struct ObiTetherConstraintsData_t071A4A85316CD0F23DA05223C15E3EE1B28626CE;
// Obi.ObiThicknessDataChannel
struct ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C;
// Obi.ObiVolumeConstraintsData
struct ObiVolumeConstraintsData_t12899715EEA78BB09F3BAC3CCC6EB012AF0E4D26;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// Obi.PathControlPointEvent
struct PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Obi.VoxelDistanceField
struct VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA;
// Obi.VoxelPathFinder
struct VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF;
// Obi.CoroutineJob/ProgressInfo
struct ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74;
// Obi.ObiActor/ActorBlueprintCallback
struct ActorBlueprintCallback_t9A80D17AA5E1DB3DB84D2C1638CD6D7002AE13C0;
// Obi.ObiActor/ActorCallback
struct ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF;
// Obi.ObiActor/ActorStepCallback
struct ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D;
// Obi.ObiActorBlueprint/BlueprintCallback
struct BlueprintCallback_t6040FFC664749D28E3A88E72308B277F69B9D3A6;
// Obi.ObiRodBlueprint/<CreateChainConstraints>d__6
struct U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872;
// Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4
struct U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46;
// Obi.ObiRodBlueprint/<Initialize>d__3
struct U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C;
// Obi.ObiRope/<>c
struct U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102;
// Obi.ObiRope/ObiRopeTornEventArgs
struct ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A;
// Obi.ObiRope/RopeTornCallback
struct RopeTornCallback_t7F7EC536306AFEEE07B7A037C3CC566AD1AF8D58;
// Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4
struct U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683;
// Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3
struct U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32;
// Obi.ObiRopeBlueprint/<Initialize>d__2
struct U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7;
// Obi.ObiRopeBlueprintBase/<Initialize>d__17
struct U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8;
// Obi.ObiSolver/<>c
struct U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16;
// Obi.ObiSolver/CollisionCallback
struct CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB;
// Obi.ObiSolver/ObiCollisionEventArgs
struct ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5;
// Obi.ObiSolver/ParticleInActor
struct ParticleInActor_t4D445CDF7A9DB2D80BEF82DBEEF606E8B61291BA;
// Obi.ObiSolver/SolverCallback
struct SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A;
// Obi.ObiSolver/SolverStepCallback
struct SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0;
// Obi.ObiStitcher/Stitch
struct Stitch_tFC29C4BD6C0E9EC2F51B4658C215F27B4E0D7201;
// Obi.ObiTriangleMeshContainer/<>c
struct U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232;
// Obi.ObiUtils/<BilateralInterleaved>d__40
struct U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB;
// Obi.VoxelDistanceField/<JumpFlood>d__5
struct U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401;
// Obi.VoxelPathFinder/<>c
struct U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816;
// Obi.VoxelPathFinder/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E;

IL2CPP_EXTERN_C RuntimeClass* ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral000FEC8A07B6CA30D8852E9E25C3EE48061B49FA;
IL2CPP_EXTERN_C String_t* _stringLiteral8BF94FD2D8D349E76B64F4A8FEA9E6A331A7DB94;
IL2CPP_EXTERN_C String_t* _stringLiteral903817815321EE2F94F9CC0B49F39AA16CC286AB;
IL2CPP_EXTERN_C String_t* _stringLiteralBDE3CEDBCC90855A8BBDCB3FE9B094E5B6AB184B;
IL2CPP_EXTERN_C String_t* _stringLiteralD368B3509E2BA452C46794E1AC89E84E3642498F;
IL2CPP_EXTERN_C String_t* _stringLiteralEACB9D9A15A8E3CD5021DA42030BE1FE13E7ED38;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m073B5521BD7EB6A522C1981D63905CEB40AE6FD2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m3F5E61F87B1504EBA4E43452FE85068B76A29181_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m9DF9BE8ACA130E6DB8F004222C3FC66DA116FC4D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiConstraints_1_AddBatch_m72B86FE6FB5147CD79A80B53085BC52B930199CB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiConstraints_1_AddBatch_m7FA684039BC7B6D3CF623C4EED798390D3A88D49_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiConstraints_1_AddBatch_mF16FEB2CD535743D507C3E2F63782E8ED0FCFD11_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiList_1__ctor_m61E6FC08889B646E0B516B5BBD472DBB1D334AFB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiPathDataChannelIdentity_1_GetAtMu_m169B9F9D1859E1C2E829CCC554D1DF4421230498_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m8B106EC62D22296800CE67482EE3F81810C06907_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_m394694E12A8EAEEB4EA80649BEA9BBE861C306FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mD6AC7761E802DE2F6F0CADB0A5682051E6E4669C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m3EC8612B472DCDC0A5BA07CF265742F7ADB074CC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CInitializeU3Ed__17_System_Collections_IEnumerator_Reset_m28AA9E89D0D2B4BD55BE44CC152105CAB3FE46A7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m7A4B24373876BDE2453048631F2B963E386E7D12_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CInitializeU3Ed__3_System_Collections_IEnumerator_Reset_m70CE6B4088FBF17D28720288E2F251679B22BD11_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6;
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;
struct Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E, ____items_1)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get__items_1() const { return ____items_1; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_StaticFields, ____emptyArray_5)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____items_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Obi.ObiBendConstraintsBatch>
struct List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObiBendConstraintsBatchU5BU5D_t31407D52D83618D9F947C6C72D1EC164E89AF3AF* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314, ____items_1)); }
	inline ObiBendConstraintsBatchU5BU5D_t31407D52D83618D9F947C6C72D1EC164E89AF3AF* get__items_1() const { return ____items_1; }
	inline ObiBendConstraintsBatchU5BU5D_t31407D52D83618D9F947C6C72D1EC164E89AF3AF** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObiBendConstraintsBatchU5BU5D_t31407D52D83618D9F947C6C72D1EC164E89AF3AF* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObiBendConstraintsBatchU5BU5D_t31407D52D83618D9F947C6C72D1EC164E89AF3AF* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314_StaticFields, ____emptyArray_5)); }
	inline ObiBendConstraintsBatchU5BU5D_t31407D52D83618D9F947C6C72D1EC164E89AF3AF* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObiBendConstraintsBatchU5BU5D_t31407D52D83618D9F947C6C72D1EC164E89AF3AF** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObiBendConstraintsBatchU5BU5D_t31407D52D83618D9F947C6C72D1EC164E89AF3AF* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Obi.ObiDistanceConstraintsBatch>
struct List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObiDistanceConstraintsBatchU5BU5D_t1F0BF8CB91EFA8638F726455614BAAC2A2E45701* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A, ____items_1)); }
	inline ObiDistanceConstraintsBatchU5BU5D_t1F0BF8CB91EFA8638F726455614BAAC2A2E45701* get__items_1() const { return ____items_1; }
	inline ObiDistanceConstraintsBatchU5BU5D_t1F0BF8CB91EFA8638F726455614BAAC2A2E45701** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObiDistanceConstraintsBatchU5BU5D_t1F0BF8CB91EFA8638F726455614BAAC2A2E45701* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObiDistanceConstraintsBatchU5BU5D_t1F0BF8CB91EFA8638F726455614BAAC2A2E45701* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A_StaticFields, ____emptyArray_5)); }
	inline ObiDistanceConstraintsBatchU5BU5D_t1F0BF8CB91EFA8638F726455614BAAC2A2E45701* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObiDistanceConstraintsBatchU5BU5D_t1F0BF8CB91EFA8638F726455614BAAC2A2E45701** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObiDistanceConstraintsBatchU5BU5D_t1F0BF8CB91EFA8638F726455614BAAC2A2E45701* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Obi.ObiParticleGroup>
struct List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObiParticleGroupU5BU5D_t161012A1D746285DC87BD0A4C053405A002BF30D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF, ____items_1)); }
	inline ObiParticleGroupU5BU5D_t161012A1D746285DC87BD0A4C053405A002BF30D* get__items_1() const { return ____items_1; }
	inline ObiParticleGroupU5BU5D_t161012A1D746285DC87BD0A4C053405A002BF30D** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObiParticleGroupU5BU5D_t161012A1D746285DC87BD0A4C053405A002BF30D* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObiParticleGroupU5BU5D_t161012A1D746285DC87BD0A4C053405A002BF30D* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF_StaticFields, ____emptyArray_5)); }
	inline ObiParticleGroupU5BU5D_t161012A1D746285DC87BD0A4C053405A002BF30D* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObiParticleGroupU5BU5D_t161012A1D746285DC87BD0A4C053405A002BF30D** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObiParticleGroupU5BU5D_t161012A1D746285DC87BD0A4C053405A002BF30D* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Obi.ObiStretchShearConstraintsBatch>
struct List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObiStretchShearConstraintsBatchU5BU5D_tA7239A77B085F5EE77A70F4039A95915715B5665* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC, ____items_1)); }
	inline ObiStretchShearConstraintsBatchU5BU5D_tA7239A77B085F5EE77A70F4039A95915715B5665* get__items_1() const { return ____items_1; }
	inline ObiStretchShearConstraintsBatchU5BU5D_tA7239A77B085F5EE77A70F4039A95915715B5665** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObiStretchShearConstraintsBatchU5BU5D_tA7239A77B085F5EE77A70F4039A95915715B5665* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObiStretchShearConstraintsBatchU5BU5D_tA7239A77B085F5EE77A70F4039A95915715B5665* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC_StaticFields, ____emptyArray_5)); }
	inline ObiStretchShearConstraintsBatchU5BU5D_tA7239A77B085F5EE77A70F4039A95915715B5665* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObiStretchShearConstraintsBatchU5BU5D_tA7239A77B085F5EE77A70F4039A95915715B5665** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObiStretchShearConstraintsBatchU5BU5D_tA7239A77B085F5EE77A70F4039A95915715B5665* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Single>
struct List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____items_1)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get__items_1() const { return ____items_1; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_StaticFields, ____emptyArray_5)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____items_1)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// Obi.ObiConstraints`1<Obi.ObiBendConstraintsBatch>
struct ObiConstraints_1_t1A77FB453F8651AB273DB0DE943C8AF5AF981F82  : public RuntimeObject
{
public:
	// Obi.ObiSolver Obi.ObiConstraints`1::m_Solver
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___m_Solver_0;
	// System.Collections.Generic.List`1<T> Obi.ObiConstraints`1::batches
	List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314 * ___batches_1;

public:
	inline static int32_t get_offset_of_m_Solver_0() { return static_cast<int32_t>(offsetof(ObiConstraints_1_t1A77FB453F8651AB273DB0DE943C8AF5AF981F82, ___m_Solver_0)); }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * get_m_Solver_0() const { return ___m_Solver_0; }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 ** get_address_of_m_Solver_0() { return &___m_Solver_0; }
	inline void set_m_Solver_0(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * value)
	{
		___m_Solver_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Solver_0), (void*)value);
	}

	inline static int32_t get_offset_of_batches_1() { return static_cast<int32_t>(offsetof(ObiConstraints_1_t1A77FB453F8651AB273DB0DE943C8AF5AF981F82, ___batches_1)); }
	inline List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314 * get_batches_1() const { return ___batches_1; }
	inline List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314 ** get_address_of_batches_1() { return &___batches_1; }
	inline void set_batches_1(List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314 * value)
	{
		___batches_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___batches_1), (void*)value);
	}
};


// Obi.ObiConstraints`1<Obi.ObiChainConstraintsBatch>
struct ObiConstraints_1_tC13134EAD7E5A4205F5834810FB89F47972C220E  : public RuntimeObject
{
public:
	// Obi.ObiSolver Obi.ObiConstraints`1::m_Solver
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___m_Solver_0;
	// System.Collections.Generic.List`1<T> Obi.ObiConstraints`1::batches
	List_1_t3996F24BACDCEF0ED723B57F053B3194023B0416 * ___batches_1;

public:
	inline static int32_t get_offset_of_m_Solver_0() { return static_cast<int32_t>(offsetof(ObiConstraints_1_tC13134EAD7E5A4205F5834810FB89F47972C220E, ___m_Solver_0)); }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * get_m_Solver_0() const { return ___m_Solver_0; }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 ** get_address_of_m_Solver_0() { return &___m_Solver_0; }
	inline void set_m_Solver_0(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * value)
	{
		___m_Solver_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Solver_0), (void*)value);
	}

	inline static int32_t get_offset_of_batches_1() { return static_cast<int32_t>(offsetof(ObiConstraints_1_tC13134EAD7E5A4205F5834810FB89F47972C220E, ___batches_1)); }
	inline List_1_t3996F24BACDCEF0ED723B57F053B3194023B0416 * get_batches_1() const { return ___batches_1; }
	inline List_1_t3996F24BACDCEF0ED723B57F053B3194023B0416 ** get_address_of_batches_1() { return &___batches_1; }
	inline void set_batches_1(List_1_t3996F24BACDCEF0ED723B57F053B3194023B0416 * value)
	{
		___batches_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___batches_1), (void*)value);
	}
};


// Obi.ObiConstraints`1<Obi.ObiDistanceConstraintsBatch>
struct ObiConstraints_1_tDDB54978FB9AEE6BD8B391B96A13174F2E9966ED  : public RuntimeObject
{
public:
	// Obi.ObiSolver Obi.ObiConstraints`1::m_Solver
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___m_Solver_0;
	// System.Collections.Generic.List`1<T> Obi.ObiConstraints`1::batches
	List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A * ___batches_1;

public:
	inline static int32_t get_offset_of_m_Solver_0() { return static_cast<int32_t>(offsetof(ObiConstraints_1_tDDB54978FB9AEE6BD8B391B96A13174F2E9966ED, ___m_Solver_0)); }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * get_m_Solver_0() const { return ___m_Solver_0; }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 ** get_address_of_m_Solver_0() { return &___m_Solver_0; }
	inline void set_m_Solver_0(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * value)
	{
		___m_Solver_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Solver_0), (void*)value);
	}

	inline static int32_t get_offset_of_batches_1() { return static_cast<int32_t>(offsetof(ObiConstraints_1_tDDB54978FB9AEE6BD8B391B96A13174F2E9966ED, ___batches_1)); }
	inline List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A * get_batches_1() const { return ___batches_1; }
	inline List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A ** get_address_of_batches_1() { return &___batches_1; }
	inline void set_batches_1(List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A * value)
	{
		___batches_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___batches_1), (void*)value);
	}
};


// Obi.ObiConstraints`1<Obi.ObiStretchShearConstraintsBatch>
struct ObiConstraints_1_t9491848D3D98BFF80E3C3AAF710BCE40F4F51610  : public RuntimeObject
{
public:
	// Obi.ObiSolver Obi.ObiConstraints`1::m_Solver
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___m_Solver_0;
	// System.Collections.Generic.List`1<T> Obi.ObiConstraints`1::batches
	List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC * ___batches_1;

public:
	inline static int32_t get_offset_of_m_Solver_0() { return static_cast<int32_t>(offsetof(ObiConstraints_1_t9491848D3D98BFF80E3C3AAF710BCE40F4F51610, ___m_Solver_0)); }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * get_m_Solver_0() const { return ___m_Solver_0; }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 ** get_address_of_m_Solver_0() { return &___m_Solver_0; }
	inline void set_m_Solver_0(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * value)
	{
		___m_Solver_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Solver_0), (void*)value);
	}

	inline static int32_t get_offset_of_batches_1() { return static_cast<int32_t>(offsetof(ObiConstraints_1_t9491848D3D98BFF80E3C3AAF710BCE40F4F51610, ___batches_1)); }
	inline List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC * get_batches_1() const { return ___batches_1; }
	inline List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC ** get_address_of_batches_1() { return &___batches_1; }
	inline void set_batches_1(List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC * value)
	{
		___batches_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___batches_1), (void*)value);
	}
};


// Obi.ObiList`1<Oni/Contact>
struct ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3  : public RuntimeObject
{
public:
	// T[] Obi.ObiList`1::data
	ContactU5BU5D_t0EA4D0F4F28544AEC9172BDBCC872BD763772C2C* ___data_0;
	// System.Int32 Obi.ObiList`1::count
	int32_t ___count_1;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3, ___data_0)); }
	inline ContactU5BU5D_t0EA4D0F4F28544AEC9172BDBCC872BD763772C2C* get_data_0() const { return ___data_0; }
	inline ContactU5BU5D_t0EA4D0F4F28544AEC9172BDBCC872BD763772C2C** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ContactU5BU5D_t0EA4D0F4F28544AEC9172BDBCC872BD763772C2C* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_0), (void*)value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}
};


// Obi.ObiPathDataChannel`2<UnityEngine.Color,UnityEngine.Color>
struct ObiPathDataChannel_2_t81E3117D8F660A0253E9EBD50BBD50A92F4BB8CE  : public RuntimeObject
{
public:
	// Obi.ObiInterpolator`1<U> Obi.ObiPathDataChannel`2::interpolator
	RuntimeObject* ___interpolator_0;
	// System.Boolean Obi.ObiPathDataChannel`2::dirty
	bool ___dirty_1;
	// System.Collections.Generic.List`1<T> Obi.ObiPathDataChannel`2::data
	List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * ___data_2;

public:
	inline static int32_t get_offset_of_interpolator_0() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t81E3117D8F660A0253E9EBD50BBD50A92F4BB8CE, ___interpolator_0)); }
	inline RuntimeObject* get_interpolator_0() const { return ___interpolator_0; }
	inline RuntimeObject** get_address_of_interpolator_0() { return &___interpolator_0; }
	inline void set_interpolator_0(RuntimeObject* value)
	{
		___interpolator_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interpolator_0), (void*)value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t81E3117D8F660A0253E9EBD50BBD50A92F4BB8CE, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t81E3117D8F660A0253E9EBD50BBD50A92F4BB8CE, ___data_2)); }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * get_data_2() const { return ___data_2; }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_2), (void*)value);
	}
};


// Obi.ObiPathDataChannel`2<System.Int32,System.Int32>
struct ObiPathDataChannel_2_t5BC70F53CBFF7F0F5FA5BD3DCFED49FE33E5700A  : public RuntimeObject
{
public:
	// Obi.ObiInterpolator`1<U> Obi.ObiPathDataChannel`2::interpolator
	RuntimeObject* ___interpolator_0;
	// System.Boolean Obi.ObiPathDataChannel`2::dirty
	bool ___dirty_1;
	// System.Collections.Generic.List`1<T> Obi.ObiPathDataChannel`2::data
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___data_2;

public:
	inline static int32_t get_offset_of_interpolator_0() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t5BC70F53CBFF7F0F5FA5BD3DCFED49FE33E5700A, ___interpolator_0)); }
	inline RuntimeObject* get_interpolator_0() const { return ___interpolator_0; }
	inline RuntimeObject** get_address_of_interpolator_0() { return &___interpolator_0; }
	inline void set_interpolator_0(RuntimeObject* value)
	{
		___interpolator_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interpolator_0), (void*)value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t5BC70F53CBFF7F0F5FA5BD3DCFED49FE33E5700A, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t5BC70F53CBFF7F0F5FA5BD3DCFED49FE33E5700A, ___data_2)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_data_2() const { return ___data_2; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_2), (void*)value);
	}
};


// Obi.ObiPathDataChannel`2<Obi.ObiWingedPoint,UnityEngine.Vector3>
struct ObiPathDataChannel_2_tEB1A7322E5FCEC0ECC8D096114FD0CD6FE92AB92  : public RuntimeObject
{
public:
	// Obi.ObiInterpolator`1<U> Obi.ObiPathDataChannel`2::interpolator
	RuntimeObject* ___interpolator_0;
	// System.Boolean Obi.ObiPathDataChannel`2::dirty
	bool ___dirty_1;
	// System.Collections.Generic.List`1<T> Obi.ObiPathDataChannel`2::data
	List_1_tA9940460E284B6579F43F2ED432B34ADA9EEDFB3 * ___data_2;

public:
	inline static int32_t get_offset_of_interpolator_0() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_tEB1A7322E5FCEC0ECC8D096114FD0CD6FE92AB92, ___interpolator_0)); }
	inline RuntimeObject* get_interpolator_0() const { return ___interpolator_0; }
	inline RuntimeObject** get_address_of_interpolator_0() { return &___interpolator_0; }
	inline void set_interpolator_0(RuntimeObject* value)
	{
		___interpolator_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interpolator_0), (void*)value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_tEB1A7322E5FCEC0ECC8D096114FD0CD6FE92AB92, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_tEB1A7322E5FCEC0ECC8D096114FD0CD6FE92AB92, ___data_2)); }
	inline List_1_tA9940460E284B6579F43F2ED432B34ADA9EEDFB3 * get_data_2() const { return ___data_2; }
	inline List_1_tA9940460E284B6579F43F2ED432B34ADA9EEDFB3 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(List_1_tA9940460E284B6579F43F2ED432B34ADA9EEDFB3 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_2), (void*)value);
	}
};


// Obi.ObiPathDataChannel`2<System.Single,System.Single>
struct ObiPathDataChannel_2_t0A3CDBB8C1B1657FA8C13C92E6C5E2C2527C071E  : public RuntimeObject
{
public:
	// Obi.ObiInterpolator`1<U> Obi.ObiPathDataChannel`2::interpolator
	RuntimeObject* ___interpolator_0;
	// System.Boolean Obi.ObiPathDataChannel`2::dirty
	bool ___dirty_1;
	// System.Collections.Generic.List`1<T> Obi.ObiPathDataChannel`2::data
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___data_2;

public:
	inline static int32_t get_offset_of_interpolator_0() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t0A3CDBB8C1B1657FA8C13C92E6C5E2C2527C071E, ___interpolator_0)); }
	inline RuntimeObject* get_interpolator_0() const { return ___interpolator_0; }
	inline RuntimeObject** get_address_of_interpolator_0() { return &___interpolator_0; }
	inline void set_interpolator_0(RuntimeObject* value)
	{
		___interpolator_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interpolator_0), (void*)value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t0A3CDBB8C1B1657FA8C13C92E6C5E2C2527C071E, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t0A3CDBB8C1B1657FA8C13C92E6C5E2C2527C071E, ___data_2)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_data_2() const { return ___data_2; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_2), (void*)value);
	}
};


// Obi.ObiPathDataChannel`2<UnityEngine.Vector3,UnityEngine.Vector3>
struct ObiPathDataChannel_2_t5C31D471EBD01623446ECFF76AD0A33B63C022FC  : public RuntimeObject
{
public:
	// Obi.ObiInterpolator`1<U> Obi.ObiPathDataChannel`2::interpolator
	RuntimeObject* ___interpolator_0;
	// System.Boolean Obi.ObiPathDataChannel`2::dirty
	bool ___dirty_1;
	// System.Collections.Generic.List`1<T> Obi.ObiPathDataChannel`2::data
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___data_2;

public:
	inline static int32_t get_offset_of_interpolator_0() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t5C31D471EBD01623446ECFF76AD0A33B63C022FC, ___interpolator_0)); }
	inline RuntimeObject* get_interpolator_0() const { return ___interpolator_0; }
	inline RuntimeObject** get_address_of_interpolator_0() { return &___interpolator_0; }
	inline void set_interpolator_0(RuntimeObject* value)
	{
		___interpolator_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___interpolator_0), (void*)value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t5C31D471EBD01623446ECFF76AD0A33B63C022FC, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(ObiPathDataChannel_2_t5C31D471EBD01623446ECFF76AD0A33B63C022FC, ___data_2)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_data_2() const { return ___data_2; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_2), (void*)value);
	}
};


// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>
struct ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_1), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.EventArgs
struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA  : public RuntimeObject
{
public:

public:
};

struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields, ___Empty_0)); }
	inline EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_0), (void*)value);
	}
};


// Obi.ObiConstraintsBatch
struct ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> Obi.ObiConstraintsBatch::m_IDs
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___m_IDs_0;
	// System.Collections.Generic.List`1<System.Int32> Obi.ObiConstraintsBatch::m_IDToIndex
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___m_IDToIndex_1;
	// System.Int32 Obi.ObiConstraintsBatch::m_ConstraintCount
	int32_t ___m_ConstraintCount_2;
	// System.Int32 Obi.ObiConstraintsBatch::m_ActiveConstraintCount
	int32_t ___m_ActiveConstraintCount_3;
	// System.Int32 Obi.ObiConstraintsBatch::m_InitialActiveConstraintCount
	int32_t ___m_InitialActiveConstraintCount_4;
	// Obi.ObiNativeIntList Obi.ObiConstraintsBatch::particleIndices
	ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * ___particleIndices_5;
	// Obi.ObiNativeFloatList Obi.ObiConstraintsBatch::lambdas
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___lambdas_6;

public:
	inline static int32_t get_offset_of_m_IDs_0() { return static_cast<int32_t>(offsetof(ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127, ___m_IDs_0)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_m_IDs_0() const { return ___m_IDs_0; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_m_IDs_0() { return &___m_IDs_0; }
	inline void set_m_IDs_0(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___m_IDs_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_IDs_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IDToIndex_1() { return static_cast<int32_t>(offsetof(ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127, ___m_IDToIndex_1)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_m_IDToIndex_1() const { return ___m_IDToIndex_1; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_m_IDToIndex_1() { return &___m_IDToIndex_1; }
	inline void set_m_IDToIndex_1(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___m_IDToIndex_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_IDToIndex_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ConstraintCount_2() { return static_cast<int32_t>(offsetof(ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127, ___m_ConstraintCount_2)); }
	inline int32_t get_m_ConstraintCount_2() const { return ___m_ConstraintCount_2; }
	inline int32_t* get_address_of_m_ConstraintCount_2() { return &___m_ConstraintCount_2; }
	inline void set_m_ConstraintCount_2(int32_t value)
	{
		___m_ConstraintCount_2 = value;
	}

	inline static int32_t get_offset_of_m_ActiveConstraintCount_3() { return static_cast<int32_t>(offsetof(ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127, ___m_ActiveConstraintCount_3)); }
	inline int32_t get_m_ActiveConstraintCount_3() const { return ___m_ActiveConstraintCount_3; }
	inline int32_t* get_address_of_m_ActiveConstraintCount_3() { return &___m_ActiveConstraintCount_3; }
	inline void set_m_ActiveConstraintCount_3(int32_t value)
	{
		___m_ActiveConstraintCount_3 = value;
	}

	inline static int32_t get_offset_of_m_InitialActiveConstraintCount_4() { return static_cast<int32_t>(offsetof(ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127, ___m_InitialActiveConstraintCount_4)); }
	inline int32_t get_m_InitialActiveConstraintCount_4() const { return ___m_InitialActiveConstraintCount_4; }
	inline int32_t* get_address_of_m_InitialActiveConstraintCount_4() { return &___m_InitialActiveConstraintCount_4; }
	inline void set_m_InitialActiveConstraintCount_4(int32_t value)
	{
		___m_InitialActiveConstraintCount_4 = value;
	}

	inline static int32_t get_offset_of_particleIndices_5() { return static_cast<int32_t>(offsetof(ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127, ___particleIndices_5)); }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * get_particleIndices_5() const { return ___particleIndices_5; }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 ** get_address_of_particleIndices_5() { return &___particleIndices_5; }
	inline void set_particleIndices_5(ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * value)
	{
		___particleIndices_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particleIndices_5), (void*)value);
	}

	inline static int32_t get_offset_of_lambdas_6() { return static_cast<int32_t>(offsetof(ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127, ___lambdas_6)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_lambdas_6() const { return ___lambdas_6; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_lambdas_6() { return &___lambdas_6; }
	inline void set_lambdas_6(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___lambdas_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lambdas_6), (void*)value);
	}
};


// Obi.ObiPath
struct ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Obi.ObiPath::m_Names
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___m_Names_0;
	// Obi.ObiPointsDataChannel Obi.ObiPath::m_Points
	ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507 * ___m_Points_1;
	// Obi.ObiNormalDataChannel Obi.ObiPath::m_Normals
	ObiNormalDataChannel_tA3F5F99B539B56E0703FA0603B89DCC345AC3296 * ___m_Normals_2;
	// Obi.ObiColorDataChannel Obi.ObiPath::m_Colors
	ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2 * ___m_Colors_3;
	// Obi.ObiThicknessDataChannel Obi.ObiPath::m_Thickness
	ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C * ___m_Thickness_4;
	// Obi.ObiMassDataChannel Obi.ObiPath::m_Masses
	ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996 * ___m_Masses_5;
	// Obi.ObiRotationalMassDataChannel Obi.ObiPath::m_RotationalMasses
	ObiRotationalMassDataChannel_tC062F8C628B38CBE7F635761D65A2BA348541E4B * ___m_RotationalMasses_6;
	// Obi.ObiPhaseDataChannel Obi.ObiPath::m_Filters
	ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670 * ___m_Filters_7;
	// System.Boolean Obi.ObiPath::m_Closed
	bool ___m_Closed_8;
	// System.Boolean Obi.ObiPath::dirty
	bool ___dirty_9;
	// System.Collections.Generic.List`1<System.Single> Obi.ObiPath::m_ArcLengthTable
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___m_ArcLengthTable_11;
	// System.Single Obi.ObiPath::m_TotalSplineLenght
	float ___m_TotalSplineLenght_12;
	// UnityEngine.Events.UnityEvent Obi.ObiPath::OnPathChanged
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnPathChanged_13;
	// Obi.PathControlPointEvent Obi.ObiPath::OnControlPointAdded
	PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE * ___OnControlPointAdded_14;
	// Obi.PathControlPointEvent Obi.ObiPath::OnControlPointRemoved
	PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE * ___OnControlPointRemoved_15;
	// Obi.PathControlPointEvent Obi.ObiPath::OnControlPointRenamed
	PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE * ___OnControlPointRenamed_16;

public:
	inline static int32_t get_offset_of_m_Names_0() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_Names_0)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_m_Names_0() const { return ___m_Names_0; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_m_Names_0() { return &___m_Names_0; }
	inline void set_m_Names_0(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___m_Names_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Names_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Points_1() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_Points_1)); }
	inline ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507 * get_m_Points_1() const { return ___m_Points_1; }
	inline ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507 ** get_address_of_m_Points_1() { return &___m_Points_1; }
	inline void set_m_Points_1(ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507 * value)
	{
		___m_Points_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Points_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Normals_2() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_Normals_2)); }
	inline ObiNormalDataChannel_tA3F5F99B539B56E0703FA0603B89DCC345AC3296 * get_m_Normals_2() const { return ___m_Normals_2; }
	inline ObiNormalDataChannel_tA3F5F99B539B56E0703FA0603B89DCC345AC3296 ** get_address_of_m_Normals_2() { return &___m_Normals_2; }
	inline void set_m_Normals_2(ObiNormalDataChannel_tA3F5F99B539B56E0703FA0603B89DCC345AC3296 * value)
	{
		___m_Normals_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Normals_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Colors_3() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_Colors_3)); }
	inline ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2 * get_m_Colors_3() const { return ___m_Colors_3; }
	inline ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2 ** get_address_of_m_Colors_3() { return &___m_Colors_3; }
	inline void set_m_Colors_3(ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2 * value)
	{
		___m_Colors_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Colors_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Thickness_4() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_Thickness_4)); }
	inline ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C * get_m_Thickness_4() const { return ___m_Thickness_4; }
	inline ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C ** get_address_of_m_Thickness_4() { return &___m_Thickness_4; }
	inline void set_m_Thickness_4(ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C * value)
	{
		___m_Thickness_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Thickness_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Masses_5() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_Masses_5)); }
	inline ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996 * get_m_Masses_5() const { return ___m_Masses_5; }
	inline ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996 ** get_address_of_m_Masses_5() { return &___m_Masses_5; }
	inline void set_m_Masses_5(ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996 * value)
	{
		___m_Masses_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Masses_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_RotationalMasses_6() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_RotationalMasses_6)); }
	inline ObiRotationalMassDataChannel_tC062F8C628B38CBE7F635761D65A2BA348541E4B * get_m_RotationalMasses_6() const { return ___m_RotationalMasses_6; }
	inline ObiRotationalMassDataChannel_tC062F8C628B38CBE7F635761D65A2BA348541E4B ** get_address_of_m_RotationalMasses_6() { return &___m_RotationalMasses_6; }
	inline void set_m_RotationalMasses_6(ObiRotationalMassDataChannel_tC062F8C628B38CBE7F635761D65A2BA348541E4B * value)
	{
		___m_RotationalMasses_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RotationalMasses_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Filters_7() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_Filters_7)); }
	inline ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670 * get_m_Filters_7() const { return ___m_Filters_7; }
	inline ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670 ** get_address_of_m_Filters_7() { return &___m_Filters_7; }
	inline void set_m_Filters_7(ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670 * value)
	{
		___m_Filters_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Filters_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Closed_8() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_Closed_8)); }
	inline bool get_m_Closed_8() const { return ___m_Closed_8; }
	inline bool* get_address_of_m_Closed_8() { return &___m_Closed_8; }
	inline void set_m_Closed_8(bool value)
	{
		___m_Closed_8 = value;
	}

	inline static int32_t get_offset_of_dirty_9() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___dirty_9)); }
	inline bool get_dirty_9() const { return ___dirty_9; }
	inline bool* get_address_of_dirty_9() { return &___dirty_9; }
	inline void set_dirty_9(bool value)
	{
		___dirty_9 = value;
	}

	inline static int32_t get_offset_of_m_ArcLengthTable_11() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_ArcLengthTable_11)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_m_ArcLengthTable_11() const { return ___m_ArcLengthTable_11; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_m_ArcLengthTable_11() { return &___m_ArcLengthTable_11; }
	inline void set_m_ArcLengthTable_11(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___m_ArcLengthTable_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ArcLengthTable_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_TotalSplineLenght_12() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___m_TotalSplineLenght_12)); }
	inline float get_m_TotalSplineLenght_12() const { return ___m_TotalSplineLenght_12; }
	inline float* get_address_of_m_TotalSplineLenght_12() { return &___m_TotalSplineLenght_12; }
	inline void set_m_TotalSplineLenght_12(float value)
	{
		___m_TotalSplineLenght_12 = value;
	}

	inline static int32_t get_offset_of_OnPathChanged_13() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___OnPathChanged_13)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnPathChanged_13() const { return ___OnPathChanged_13; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnPathChanged_13() { return &___OnPathChanged_13; }
	inline void set_OnPathChanged_13(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnPathChanged_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPathChanged_13), (void*)value);
	}

	inline static int32_t get_offset_of_OnControlPointAdded_14() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___OnControlPointAdded_14)); }
	inline PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE * get_OnControlPointAdded_14() const { return ___OnControlPointAdded_14; }
	inline PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE ** get_address_of_OnControlPointAdded_14() { return &___OnControlPointAdded_14; }
	inline void set_OnControlPointAdded_14(PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE * value)
	{
		___OnControlPointAdded_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnControlPointAdded_14), (void*)value);
	}

	inline static int32_t get_offset_of_OnControlPointRemoved_15() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___OnControlPointRemoved_15)); }
	inline PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE * get_OnControlPointRemoved_15() const { return ___OnControlPointRemoved_15; }
	inline PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE ** get_address_of_OnControlPointRemoved_15() { return &___OnControlPointRemoved_15; }
	inline void set_OnControlPointRemoved_15(PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE * value)
	{
		___OnControlPointRemoved_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnControlPointRemoved_15), (void*)value);
	}

	inline static int32_t get_offset_of_OnControlPointRenamed_16() { return static_cast<int32_t>(offsetof(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459, ___OnControlPointRenamed_16)); }
	inline PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE * get_OnControlPointRenamed_16() const { return ___OnControlPointRenamed_16; }
	inline PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE ** get_address_of_OnControlPointRenamed_16() { return &___OnControlPointRenamed_16; }
	inline void set_OnControlPointRenamed_16(PathControlPointEvent_tE514EB7D0869B1523B239A57657BE5D44A93FBFE * value)
	{
		___OnControlPointRenamed_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnControlPointRenamed_16), (void*)value);
	}
};


// Obi.ObiStructuralElement
struct ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiStructuralElement::particle1
	int32_t ___particle1_0;
	// System.Int32 Obi.ObiStructuralElement::particle2
	int32_t ___particle2_1;
	// System.Single Obi.ObiStructuralElement::restLength
	float ___restLength_2;
	// System.Single Obi.ObiStructuralElement::constraintForce
	float ___constraintForce_3;
	// System.Single Obi.ObiStructuralElement::tearResistance
	float ___tearResistance_4;

public:
	inline static int32_t get_offset_of_particle1_0() { return static_cast<int32_t>(offsetof(ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759, ___particle1_0)); }
	inline int32_t get_particle1_0() const { return ___particle1_0; }
	inline int32_t* get_address_of_particle1_0() { return &___particle1_0; }
	inline void set_particle1_0(int32_t value)
	{
		___particle1_0 = value;
	}

	inline static int32_t get_offset_of_particle2_1() { return static_cast<int32_t>(offsetof(ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759, ___particle2_1)); }
	inline int32_t get_particle2_1() const { return ___particle2_1; }
	inline int32_t* get_address_of_particle2_1() { return &___particle2_1; }
	inline void set_particle2_1(int32_t value)
	{
		___particle2_1 = value;
	}

	inline static int32_t get_offset_of_restLength_2() { return static_cast<int32_t>(offsetof(ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759, ___restLength_2)); }
	inline float get_restLength_2() const { return ___restLength_2; }
	inline float* get_address_of_restLength_2() { return &___restLength_2; }
	inline void set_restLength_2(float value)
	{
		___restLength_2 = value;
	}

	inline static int32_t get_offset_of_constraintForce_3() { return static_cast<int32_t>(offsetof(ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759, ___constraintForce_3)); }
	inline float get_constraintForce_3() const { return ___constraintForce_3; }
	inline float* get_address_of_constraintForce_3() { return &___constraintForce_3; }
	inline void set_constraintForce_3(float value)
	{
		___constraintForce_3 = value;
	}

	inline static int32_t get_offset_of_tearResistance_4() { return static_cast<int32_t>(offsetof(ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759, ___tearResistance_4)); }
	inline float get_tearResistance_4() const { return ___tearResistance_4; }
	inline float* get_address_of_tearResistance_4() { return &___tearResistance_4; }
	inline void set_tearResistance_4(float value)
	{
		___tearResistance_4 = value;
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Obi.VoxelDistanceField
struct VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA  : public RuntimeObject
{
public:
	// UnityEngine.Vector3Int[0...,0...,0...] Obi.VoxelDistanceField::distanceField
	Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* ___distanceField_0;
	// Obi.MeshVoxelizer Obi.VoxelDistanceField::voxelizer
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * ___voxelizer_1;

public:
	inline static int32_t get_offset_of_distanceField_0() { return static_cast<int32_t>(offsetof(VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA, ___distanceField_0)); }
	inline Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* get_distanceField_0() const { return ___distanceField_0; }
	inline Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9** get_address_of_distanceField_0() { return &___distanceField_0; }
	inline void set_distanceField_0(Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* value)
	{
		___distanceField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___distanceField_0), (void*)value);
	}

	inline static int32_t get_offset_of_voxelizer_1() { return static_cast<int32_t>(offsetof(VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA, ___voxelizer_1)); }
	inline MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * get_voxelizer_1() const { return ___voxelizer_1; }
	inline MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D ** get_address_of_voxelizer_1() { return &___voxelizer_1; }
	inline void set_voxelizer_1(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * value)
	{
		___voxelizer_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___voxelizer_1), (void*)value);
	}
};


// Obi.VoxelPathFinder
struct VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF  : public RuntimeObject
{
public:
	// Obi.MeshVoxelizer Obi.VoxelPathFinder::voxelizer
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * ___voxelizer_0;
	// System.Boolean[0...,0...,0...] Obi.VoxelPathFinder::closed
	BooleanU5BU2CU2CU5D_tB8F56EB24D5C42F188A3C72BFF48D9AC9B78E58B* ___closed_1;
	// Obi.PriorityQueue`1<Obi.VoxelPathFinder/TargetVoxel> Obi.VoxelPathFinder::open
	PriorityQueue_1_t0510E3608062DAF6C39185E22673183229DB4D03 * ___open_2;

public:
	inline static int32_t get_offset_of_voxelizer_0() { return static_cast<int32_t>(offsetof(VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF, ___voxelizer_0)); }
	inline MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * get_voxelizer_0() const { return ___voxelizer_0; }
	inline MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D ** get_address_of_voxelizer_0() { return &___voxelizer_0; }
	inline void set_voxelizer_0(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * value)
	{
		___voxelizer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___voxelizer_0), (void*)value);
	}

	inline static int32_t get_offset_of_closed_1() { return static_cast<int32_t>(offsetof(VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF, ___closed_1)); }
	inline BooleanU5BU2CU2CU5D_tB8F56EB24D5C42F188A3C72BFF48D9AC9B78E58B* get_closed_1() const { return ___closed_1; }
	inline BooleanU5BU2CU2CU5D_tB8F56EB24D5C42F188A3C72BFF48D9AC9B78E58B** get_address_of_closed_1() { return &___closed_1; }
	inline void set_closed_1(BooleanU5BU2CU2CU5D_tB8F56EB24D5C42F188A3C72BFF48D9AC9B78E58B* value)
	{
		___closed_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___closed_1), (void*)value);
	}

	inline static int32_t get_offset_of_open_2() { return static_cast<int32_t>(offsetof(VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF, ___open_2)); }
	inline PriorityQueue_1_t0510E3608062DAF6C39185E22673183229DB4D03 * get_open_2() const { return ___open_2; }
	inline PriorityQueue_1_t0510E3608062DAF6C39185E22673183229DB4D03 ** get_address_of_open_2() { return &___open_2; }
	inline void set_open_2(PriorityQueue_1_t0510E3608062DAF6C39185E22673183229DB4D03 * value)
	{
		___open_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___open_2), (void*)value);
	}
};


// Obi.CoroutineJob/ProgressInfo
struct ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74  : public RuntimeObject
{
public:
	// System.String Obi.CoroutineJob/ProgressInfo::userReadableInfo
	String_t* ___userReadableInfo_0;
	// System.Single Obi.CoroutineJob/ProgressInfo::progress
	float ___progress_1;

public:
	inline static int32_t get_offset_of_userReadableInfo_0() { return static_cast<int32_t>(offsetof(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74, ___userReadableInfo_0)); }
	inline String_t* get_userReadableInfo_0() const { return ___userReadableInfo_0; }
	inline String_t** get_address_of_userReadableInfo_0() { return &___userReadableInfo_0; }
	inline void set_userReadableInfo_0(String_t* value)
	{
		___userReadableInfo_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___userReadableInfo_0), (void*)value);
	}

	inline static int32_t get_offset_of_progress_1() { return static_cast<int32_t>(offsetof(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74, ___progress_1)); }
	inline float get_progress_1() const { return ___progress_1; }
	inline float* get_address_of_progress_1() { return &___progress_1; }
	inline void set_progress_1(float value)
	{
		___progress_1 = value;
	}
};


// Obi.ObiRodBlueprint/<CreateChainConstraints>d__6
struct U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Obi.ObiRodBlueprint Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::<>4__this
	ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * ___U3CU3E4__this_2;
	// Obi.ObiChainConstraintsBatch Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::<batch>5__1
	ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * ___U3CbatchU3E5__1_3;
	// System.Int32[] Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::<indices>5__2
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___U3CindicesU3E5__2_4;
	// System.Int32 Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::<i>5__3
	int32_t ___U3CiU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872, ___U3CU3E4__this_2)); }
	inline ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbatchU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872, ___U3CbatchU3E5__1_3)); }
	inline ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * get_U3CbatchU3E5__1_3() const { return ___U3CbatchU3E5__1_3; }
	inline ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E ** get_address_of_U3CbatchU3E5__1_3() { return &___U3CbatchU3E5__1_3; }
	inline void set_U3CbatchU3E5__1_3(ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * value)
	{
		___U3CbatchU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbatchU3E5__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CindicesU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872, ___U3CindicesU3E5__2_4)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_U3CindicesU3E5__2_4() const { return ___U3CindicesU3E5__2_4; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_U3CindicesU3E5__2_4() { return &___U3CindicesU3E5__2_4; }
	inline void set_U3CindicesU3E5__2_4(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___U3CindicesU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CindicesU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872, ___U3CiU3E5__3_5)); }
	inline int32_t get_U3CiU3E5__3_5() const { return ___U3CiU3E5__3_5; }
	inline int32_t* get_address_of_U3CiU3E5__3_5() { return &___U3CiU3E5__3_5; }
	inline void set_U3CiU3E5__3_5(int32_t value)
	{
		___U3CiU3E5__3_5 = value;
	}
};


// Obi.ObiRodBlueprint/<Initialize>d__3
struct U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiRodBlueprint/<Initialize>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiRodBlueprint/<Initialize>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Obi.ObiRodBlueprint Obi.ObiRodBlueprint/<Initialize>d__3::<>4__this
	ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Obi.ObiRodBlueprint/<Initialize>d__3::<particlePositions>5__1
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___U3CparticlePositionsU3E5__1_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Obi.ObiRodBlueprint/<Initialize>d__3::<particleNormals>5__2
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___U3CparticleNormalsU3E5__2_4;
	// System.Collections.Generic.List`1<System.Single> Obi.ObiRodBlueprint/<Initialize>d__3::<particleThicknesses>5__3
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___U3CparticleThicknessesU3E5__3_5;
	// System.Collections.Generic.List`1<System.Single> Obi.ObiRodBlueprint/<Initialize>d__3::<particleInvMasses>5__4
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___U3CparticleInvMassesU3E5__4_6;
	// System.Collections.Generic.List`1<System.Single> Obi.ObiRodBlueprint/<Initialize>d__3::<particleInvRotationalMasses>5__5
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___U3CparticleInvRotationalMassesU3E5__5_7;
	// System.Collections.Generic.List`1<System.Int32> Obi.ObiRodBlueprint/<Initialize>d__3::<particleFilters>5__6
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___U3CparticleFiltersU3E5__6_8;
	// System.Collections.Generic.List`1<UnityEngine.Color> Obi.ObiRodBlueprint/<Initialize>d__3::<particleColors>5__7
	List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * ___U3CparticleColorsU3E5__7_9;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> Obi.ObiRodBlueprint/<Initialize>d__3::<lengthTable>5__8
	ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * ___U3ClengthTableU3E5__8_10;
	// System.Int32 Obi.ObiRodBlueprint/<Initialize>d__3::<spans>5__9
	int32_t ___U3CspansU3E5__9_11;
	// System.Int32 Obi.ObiRodBlueprint/<Initialize>d__3::<numSegments>5__10
	int32_t ___U3CnumSegmentsU3E5__10_12;
	// System.Collections.IEnumerator Obi.ObiRodBlueprint/<Initialize>d__3::<dc>5__11
	RuntimeObject* ___U3CdcU3E5__11_13;
	// System.Collections.IEnumerator Obi.ObiRodBlueprint/<Initialize>d__3::<bc>5__12
	RuntimeObject* ___U3CbcU3E5__12_14;
	// System.Collections.IEnumerator Obi.ObiRodBlueprint/<Initialize>d__3::<cc>5__13
	RuntimeObject* ___U3CccU3E5__13_15;
	// System.Int32 Obi.ObiRodBlueprint/<Initialize>d__3::<i>5__14
	int32_t ___U3CiU3E5__14_16;
	// System.Int32 Obi.ObiRodBlueprint/<Initialize>d__3::<firstArcLengthSample>5__15
	int32_t ___U3CfirstArcLengthSampleU3E5__15_17;
	// System.Int32 Obi.ObiRodBlueprint/<Initialize>d__3::<lastArcLengthSample>5__16
	int32_t ___U3ClastArcLengthSampleU3E5__16_18;
	// System.Single Obi.ObiRodBlueprint/<Initialize>d__3::<upToSpanLength>5__17
	float ___U3CupToSpanLengthU3E5__17_19;
	// System.Single Obi.ObiRodBlueprint/<Initialize>d__3::<spanLength>5__18
	float ___U3CspanLengthU3E5__18_20;
	// System.Int32 Obi.ObiRodBlueprint/<Initialize>d__3::<particlesInSpan>5__19
	int32_t ___U3CparticlesInSpanU3E5__19_21;
	// System.Single Obi.ObiRodBlueprint/<Initialize>d__3::<distance>5__20
	float ___U3CdistanceU3E5__20_22;
	// System.Int32 Obi.ObiRodBlueprint/<Initialize>d__3::<j>5__21
	int32_t ___U3CjU3E5__21_23;
	// System.Single Obi.ObiRodBlueprint/<Initialize>d__3::<mu>5__22
	float ___U3CmuU3E5__22_24;
	// System.Int32 Obi.ObiRodBlueprint/<Initialize>d__3::<i>5__23
	int32_t ___U3CiU3E5__23_25;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CU3E4__this_2)); }
	inline ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticlePositionsU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CparticlePositionsU3E5__1_3)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_U3CparticlePositionsU3E5__1_3() const { return ___U3CparticlePositionsU3E5__1_3; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_U3CparticlePositionsU3E5__1_3() { return &___U3CparticlePositionsU3E5__1_3; }
	inline void set_U3CparticlePositionsU3E5__1_3(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___U3CparticlePositionsU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticlePositionsU3E5__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticleNormalsU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CparticleNormalsU3E5__2_4)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_U3CparticleNormalsU3E5__2_4() const { return ___U3CparticleNormalsU3E5__2_4; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_U3CparticleNormalsU3E5__2_4() { return &___U3CparticleNormalsU3E5__2_4; }
	inline void set_U3CparticleNormalsU3E5__2_4(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___U3CparticleNormalsU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticleNormalsU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticleThicknessesU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CparticleThicknessesU3E5__3_5)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_U3CparticleThicknessesU3E5__3_5() const { return ___U3CparticleThicknessesU3E5__3_5; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_U3CparticleThicknessesU3E5__3_5() { return &___U3CparticleThicknessesU3E5__3_5; }
	inline void set_U3CparticleThicknessesU3E5__3_5(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___U3CparticleThicknessesU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticleThicknessesU3E5__3_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticleInvMassesU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CparticleInvMassesU3E5__4_6)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_U3CparticleInvMassesU3E5__4_6() const { return ___U3CparticleInvMassesU3E5__4_6; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_U3CparticleInvMassesU3E5__4_6() { return &___U3CparticleInvMassesU3E5__4_6; }
	inline void set_U3CparticleInvMassesU3E5__4_6(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___U3CparticleInvMassesU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticleInvMassesU3E5__4_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticleInvRotationalMassesU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CparticleInvRotationalMassesU3E5__5_7)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_U3CparticleInvRotationalMassesU3E5__5_7() const { return ___U3CparticleInvRotationalMassesU3E5__5_7; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_U3CparticleInvRotationalMassesU3E5__5_7() { return &___U3CparticleInvRotationalMassesU3E5__5_7; }
	inline void set_U3CparticleInvRotationalMassesU3E5__5_7(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___U3CparticleInvRotationalMassesU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticleInvRotationalMassesU3E5__5_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticleFiltersU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CparticleFiltersU3E5__6_8)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_U3CparticleFiltersU3E5__6_8() const { return ___U3CparticleFiltersU3E5__6_8; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_U3CparticleFiltersU3E5__6_8() { return &___U3CparticleFiltersU3E5__6_8; }
	inline void set_U3CparticleFiltersU3E5__6_8(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___U3CparticleFiltersU3E5__6_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticleFiltersU3E5__6_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticleColorsU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CparticleColorsU3E5__7_9)); }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * get_U3CparticleColorsU3E5__7_9() const { return ___U3CparticleColorsU3E5__7_9; }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E ** get_address_of_U3CparticleColorsU3E5__7_9() { return &___U3CparticleColorsU3E5__7_9; }
	inline void set_U3CparticleColorsU3E5__7_9(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * value)
	{
		___U3CparticleColorsU3E5__7_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticleColorsU3E5__7_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClengthTableU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3ClengthTableU3E5__8_10)); }
	inline ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * get_U3ClengthTableU3E5__8_10() const { return ___U3ClengthTableU3E5__8_10; }
	inline ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF ** get_address_of_U3ClengthTableU3E5__8_10() { return &___U3ClengthTableU3E5__8_10; }
	inline void set_U3ClengthTableU3E5__8_10(ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * value)
	{
		___U3ClengthTableU3E5__8_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClengthTableU3E5__8_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CspansU3E5__9_11() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CspansU3E5__9_11)); }
	inline int32_t get_U3CspansU3E5__9_11() const { return ___U3CspansU3E5__9_11; }
	inline int32_t* get_address_of_U3CspansU3E5__9_11() { return &___U3CspansU3E5__9_11; }
	inline void set_U3CspansU3E5__9_11(int32_t value)
	{
		___U3CspansU3E5__9_11 = value;
	}

	inline static int32_t get_offset_of_U3CnumSegmentsU3E5__10_12() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CnumSegmentsU3E5__10_12)); }
	inline int32_t get_U3CnumSegmentsU3E5__10_12() const { return ___U3CnumSegmentsU3E5__10_12; }
	inline int32_t* get_address_of_U3CnumSegmentsU3E5__10_12() { return &___U3CnumSegmentsU3E5__10_12; }
	inline void set_U3CnumSegmentsU3E5__10_12(int32_t value)
	{
		___U3CnumSegmentsU3E5__10_12 = value;
	}

	inline static int32_t get_offset_of_U3CdcU3E5__11_13() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CdcU3E5__11_13)); }
	inline RuntimeObject* get_U3CdcU3E5__11_13() const { return ___U3CdcU3E5__11_13; }
	inline RuntimeObject** get_address_of_U3CdcU3E5__11_13() { return &___U3CdcU3E5__11_13; }
	inline void set_U3CdcU3E5__11_13(RuntimeObject* value)
	{
		___U3CdcU3E5__11_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdcU3E5__11_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbcU3E5__12_14() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CbcU3E5__12_14)); }
	inline RuntimeObject* get_U3CbcU3E5__12_14() const { return ___U3CbcU3E5__12_14; }
	inline RuntimeObject** get_address_of_U3CbcU3E5__12_14() { return &___U3CbcU3E5__12_14; }
	inline void set_U3CbcU3E5__12_14(RuntimeObject* value)
	{
		___U3CbcU3E5__12_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbcU3E5__12_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3CccU3E5__13_15() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CccU3E5__13_15)); }
	inline RuntimeObject* get_U3CccU3E5__13_15() const { return ___U3CccU3E5__13_15; }
	inline RuntimeObject** get_address_of_U3CccU3E5__13_15() { return &___U3CccU3E5__13_15; }
	inline void set_U3CccU3E5__13_15(RuntimeObject* value)
	{
		___U3CccU3E5__13_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CccU3E5__13_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__14_16() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CiU3E5__14_16)); }
	inline int32_t get_U3CiU3E5__14_16() const { return ___U3CiU3E5__14_16; }
	inline int32_t* get_address_of_U3CiU3E5__14_16() { return &___U3CiU3E5__14_16; }
	inline void set_U3CiU3E5__14_16(int32_t value)
	{
		___U3CiU3E5__14_16 = value;
	}

	inline static int32_t get_offset_of_U3CfirstArcLengthSampleU3E5__15_17() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CfirstArcLengthSampleU3E5__15_17)); }
	inline int32_t get_U3CfirstArcLengthSampleU3E5__15_17() const { return ___U3CfirstArcLengthSampleU3E5__15_17; }
	inline int32_t* get_address_of_U3CfirstArcLengthSampleU3E5__15_17() { return &___U3CfirstArcLengthSampleU3E5__15_17; }
	inline void set_U3CfirstArcLengthSampleU3E5__15_17(int32_t value)
	{
		___U3CfirstArcLengthSampleU3E5__15_17 = value;
	}

	inline static int32_t get_offset_of_U3ClastArcLengthSampleU3E5__16_18() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3ClastArcLengthSampleU3E5__16_18)); }
	inline int32_t get_U3ClastArcLengthSampleU3E5__16_18() const { return ___U3ClastArcLengthSampleU3E5__16_18; }
	inline int32_t* get_address_of_U3ClastArcLengthSampleU3E5__16_18() { return &___U3ClastArcLengthSampleU3E5__16_18; }
	inline void set_U3ClastArcLengthSampleU3E5__16_18(int32_t value)
	{
		___U3ClastArcLengthSampleU3E5__16_18 = value;
	}

	inline static int32_t get_offset_of_U3CupToSpanLengthU3E5__17_19() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CupToSpanLengthU3E5__17_19)); }
	inline float get_U3CupToSpanLengthU3E5__17_19() const { return ___U3CupToSpanLengthU3E5__17_19; }
	inline float* get_address_of_U3CupToSpanLengthU3E5__17_19() { return &___U3CupToSpanLengthU3E5__17_19; }
	inline void set_U3CupToSpanLengthU3E5__17_19(float value)
	{
		___U3CupToSpanLengthU3E5__17_19 = value;
	}

	inline static int32_t get_offset_of_U3CspanLengthU3E5__18_20() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CspanLengthU3E5__18_20)); }
	inline float get_U3CspanLengthU3E5__18_20() const { return ___U3CspanLengthU3E5__18_20; }
	inline float* get_address_of_U3CspanLengthU3E5__18_20() { return &___U3CspanLengthU3E5__18_20; }
	inline void set_U3CspanLengthU3E5__18_20(float value)
	{
		___U3CspanLengthU3E5__18_20 = value;
	}

	inline static int32_t get_offset_of_U3CparticlesInSpanU3E5__19_21() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CparticlesInSpanU3E5__19_21)); }
	inline int32_t get_U3CparticlesInSpanU3E5__19_21() const { return ___U3CparticlesInSpanU3E5__19_21; }
	inline int32_t* get_address_of_U3CparticlesInSpanU3E5__19_21() { return &___U3CparticlesInSpanU3E5__19_21; }
	inline void set_U3CparticlesInSpanU3E5__19_21(int32_t value)
	{
		___U3CparticlesInSpanU3E5__19_21 = value;
	}

	inline static int32_t get_offset_of_U3CdistanceU3E5__20_22() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CdistanceU3E5__20_22)); }
	inline float get_U3CdistanceU3E5__20_22() const { return ___U3CdistanceU3E5__20_22; }
	inline float* get_address_of_U3CdistanceU3E5__20_22() { return &___U3CdistanceU3E5__20_22; }
	inline void set_U3CdistanceU3E5__20_22(float value)
	{
		___U3CdistanceU3E5__20_22 = value;
	}

	inline static int32_t get_offset_of_U3CjU3E5__21_23() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CjU3E5__21_23)); }
	inline int32_t get_U3CjU3E5__21_23() const { return ___U3CjU3E5__21_23; }
	inline int32_t* get_address_of_U3CjU3E5__21_23() { return &___U3CjU3E5__21_23; }
	inline void set_U3CjU3E5__21_23(int32_t value)
	{
		___U3CjU3E5__21_23 = value;
	}

	inline static int32_t get_offset_of_U3CmuU3E5__22_24() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CmuU3E5__22_24)); }
	inline float get_U3CmuU3E5__22_24() const { return ___U3CmuU3E5__22_24; }
	inline float* get_address_of_U3CmuU3E5__22_24() { return &___U3CmuU3E5__22_24; }
	inline void set_U3CmuU3E5__22_24(float value)
	{
		___U3CmuU3E5__22_24 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__23_25() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C, ___U3CiU3E5__23_25)); }
	inline int32_t get_U3CiU3E5__23_25() const { return ___U3CiU3E5__23_25; }
	inline int32_t* get_address_of_U3CiU3E5__23_25() { return &___U3CiU3E5__23_25; }
	inline void set_U3CiU3E5__23_25(int32_t value)
	{
		___U3CiU3E5__23_25 = value;
	}
};


// Obi.ObiRope/<>c
struct U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102_StaticFields
{
public:
	// Obi.ObiRope/<>c Obi.ObiRope/<>c::<>9
	U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102 * ___U3CU3E9_0;
	// System.Comparison`1<Obi.ObiStructuralElement> Obi.ObiRope/<>c::<>9__61_0
	Comparison_1_tD125F39287A56230AD2F8E94903C7C682E966A75 * ___U3CU3E9__61_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__61_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102_StaticFields, ___U3CU3E9__61_0_1)); }
	inline Comparison_1_tD125F39287A56230AD2F8E94903C7C682E966A75 * get_U3CU3E9__61_0_1() const { return ___U3CU3E9__61_0_1; }
	inline Comparison_1_tD125F39287A56230AD2F8E94903C7C682E966A75 ** get_address_of_U3CU3E9__61_0_1() { return &___U3CU3E9__61_0_1; }
	inline void set_U3CU3E9__61_0_1(Comparison_1_tD125F39287A56230AD2F8E94903C7C682E966A75 * value)
	{
		___U3CU3E9__61_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__61_0_1), (void*)value);
	}
};


// Obi.ObiRope/ObiRopeTornEventArgs
struct ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A  : public RuntimeObject
{
public:
	// Obi.ObiStructuralElement Obi.ObiRope/ObiRopeTornEventArgs::element
	ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759 * ___element_0;
	// System.Int32 Obi.ObiRope/ObiRopeTornEventArgs::particleIndex
	int32_t ___particleIndex_1;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A, ___element_0)); }
	inline ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759 * get_element_0() const { return ___element_0; }
	inline ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759 ** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759 * value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___element_0), (void*)value);
	}

	inline static int32_t get_offset_of_particleIndex_1() { return static_cast<int32_t>(offsetof(ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A, ___particleIndex_1)); }
	inline int32_t get_particleIndex_1() const { return ___particleIndex_1; }
	inline int32_t* get_address_of_particleIndex_1() { return &___particleIndex_1; }
	inline void set_particleIndex_1(int32_t value)
	{
		___particleIndex_1 = value;
	}
};


// Obi.ObiRopeBlueprint/<Initialize>d__2
struct U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiRopeBlueprint/<Initialize>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiRopeBlueprint/<Initialize>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Obi.ObiRopeBlueprint Obi.ObiRopeBlueprint/<Initialize>d__2::<>4__this
	ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Obi.ObiRopeBlueprint/<Initialize>d__2::<particlePositions>5__1
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___U3CparticlePositionsU3E5__1_3;
	// System.Collections.Generic.List`1<System.Single> Obi.ObiRopeBlueprint/<Initialize>d__2::<particleThicknesses>5__2
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___U3CparticleThicknessesU3E5__2_4;
	// System.Collections.Generic.List`1<System.Single> Obi.ObiRopeBlueprint/<Initialize>d__2::<particleInvMasses>5__3
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___U3CparticleInvMassesU3E5__3_5;
	// System.Collections.Generic.List`1<System.Int32> Obi.ObiRopeBlueprint/<Initialize>d__2::<particleFilters>5__4
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___U3CparticleFiltersU3E5__4_6;
	// System.Collections.Generic.List`1<UnityEngine.Color> Obi.ObiRopeBlueprint/<Initialize>d__2::<particleColors>5__5
	List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * ___U3CparticleColorsU3E5__5_7;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> Obi.ObiRopeBlueprint/<Initialize>d__2::<lengthTable>5__6
	ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * ___U3ClengthTableU3E5__6_8;
	// System.Int32 Obi.ObiRopeBlueprint/<Initialize>d__2::<spans>5__7
	int32_t ___U3CspansU3E5__7_9;
	// System.Int32 Obi.ObiRopeBlueprint/<Initialize>d__2::<numSegments>5__8
	int32_t ___U3CnumSegmentsU3E5__8_10;
	// System.Collections.IEnumerator Obi.ObiRopeBlueprint/<Initialize>d__2::<dc>5__9
	RuntimeObject* ___U3CdcU3E5__9_11;
	// System.Collections.IEnumerator Obi.ObiRopeBlueprint/<Initialize>d__2::<bc>5__10
	RuntimeObject* ___U3CbcU3E5__10_12;
	// System.Int32 Obi.ObiRopeBlueprint/<Initialize>d__2::<i>5__11
	int32_t ___U3CiU3E5__11_13;
	// System.Int32 Obi.ObiRopeBlueprint/<Initialize>d__2::<firstArcLengthSample>5__12
	int32_t ___U3CfirstArcLengthSampleU3E5__12_14;
	// System.Int32 Obi.ObiRopeBlueprint/<Initialize>d__2::<lastArcLengthSample>5__13
	int32_t ___U3ClastArcLengthSampleU3E5__13_15;
	// System.Single Obi.ObiRopeBlueprint/<Initialize>d__2::<upToSpanLength>5__14
	float ___U3CupToSpanLengthU3E5__14_16;
	// System.Single Obi.ObiRopeBlueprint/<Initialize>d__2::<spanLength>5__15
	float ___U3CspanLengthU3E5__15_17;
	// System.Int32 Obi.ObiRopeBlueprint/<Initialize>d__2::<particlesInSpan>5__16
	int32_t ___U3CparticlesInSpanU3E5__16_18;
	// System.Single Obi.ObiRopeBlueprint/<Initialize>d__2::<distance>5__17
	float ___U3CdistanceU3E5__17_19;
	// System.Int32 Obi.ObiRopeBlueprint/<Initialize>d__2::<j>5__18
	int32_t ___U3CjU3E5__18_20;
	// System.Single Obi.ObiRopeBlueprint/<Initialize>d__2::<mu>5__19
	float ___U3CmuU3E5__19_21;
	// System.Int32 Obi.ObiRopeBlueprint/<Initialize>d__2::<i>5__20
	int32_t ___U3CiU3E5__20_22;
	// System.Single[] Obi.ObiRopeBlueprint/<Initialize>d__2::<>s__21
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___U3CU3Es__21_23;
	// System.Int32 Obi.ObiRopeBlueprint/<Initialize>d__2::<>s__22
	int32_t ___U3CU3Es__22_24;
	// System.Single Obi.ObiRopeBlueprint/<Initialize>d__2::<length>5__23
	float ___U3ClengthU3E5__23_25;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CU3E4__this_2)); }
	inline ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticlePositionsU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CparticlePositionsU3E5__1_3)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_U3CparticlePositionsU3E5__1_3() const { return ___U3CparticlePositionsU3E5__1_3; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_U3CparticlePositionsU3E5__1_3() { return &___U3CparticlePositionsU3E5__1_3; }
	inline void set_U3CparticlePositionsU3E5__1_3(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___U3CparticlePositionsU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticlePositionsU3E5__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticleThicknessesU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CparticleThicknessesU3E5__2_4)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_U3CparticleThicknessesU3E5__2_4() const { return ___U3CparticleThicknessesU3E5__2_4; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_U3CparticleThicknessesU3E5__2_4() { return &___U3CparticleThicknessesU3E5__2_4; }
	inline void set_U3CparticleThicknessesU3E5__2_4(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___U3CparticleThicknessesU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticleThicknessesU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticleInvMassesU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CparticleInvMassesU3E5__3_5)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_U3CparticleInvMassesU3E5__3_5() const { return ___U3CparticleInvMassesU3E5__3_5; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_U3CparticleInvMassesU3E5__3_5() { return &___U3CparticleInvMassesU3E5__3_5; }
	inline void set_U3CparticleInvMassesU3E5__3_5(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___U3CparticleInvMassesU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticleInvMassesU3E5__3_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticleFiltersU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CparticleFiltersU3E5__4_6)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_U3CparticleFiltersU3E5__4_6() const { return ___U3CparticleFiltersU3E5__4_6; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_U3CparticleFiltersU3E5__4_6() { return &___U3CparticleFiltersU3E5__4_6; }
	inline void set_U3CparticleFiltersU3E5__4_6(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___U3CparticleFiltersU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticleFiltersU3E5__4_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparticleColorsU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CparticleColorsU3E5__5_7)); }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * get_U3CparticleColorsU3E5__5_7() const { return ___U3CparticleColorsU3E5__5_7; }
	inline List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E ** get_address_of_U3CparticleColorsU3E5__5_7() { return &___U3CparticleColorsU3E5__5_7; }
	inline void set_U3CparticleColorsU3E5__5_7(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * value)
	{
		___U3CparticleColorsU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparticleColorsU3E5__5_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClengthTableU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3ClengthTableU3E5__6_8)); }
	inline ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * get_U3ClengthTableU3E5__6_8() const { return ___U3ClengthTableU3E5__6_8; }
	inline ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF ** get_address_of_U3ClengthTableU3E5__6_8() { return &___U3ClengthTableU3E5__6_8; }
	inline void set_U3ClengthTableU3E5__6_8(ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * value)
	{
		___U3ClengthTableU3E5__6_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClengthTableU3E5__6_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CspansU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CspansU3E5__7_9)); }
	inline int32_t get_U3CspansU3E5__7_9() const { return ___U3CspansU3E5__7_9; }
	inline int32_t* get_address_of_U3CspansU3E5__7_9() { return &___U3CspansU3E5__7_9; }
	inline void set_U3CspansU3E5__7_9(int32_t value)
	{
		___U3CspansU3E5__7_9 = value;
	}

	inline static int32_t get_offset_of_U3CnumSegmentsU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CnumSegmentsU3E5__8_10)); }
	inline int32_t get_U3CnumSegmentsU3E5__8_10() const { return ___U3CnumSegmentsU3E5__8_10; }
	inline int32_t* get_address_of_U3CnumSegmentsU3E5__8_10() { return &___U3CnumSegmentsU3E5__8_10; }
	inline void set_U3CnumSegmentsU3E5__8_10(int32_t value)
	{
		___U3CnumSegmentsU3E5__8_10 = value;
	}

	inline static int32_t get_offset_of_U3CdcU3E5__9_11() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CdcU3E5__9_11)); }
	inline RuntimeObject* get_U3CdcU3E5__9_11() const { return ___U3CdcU3E5__9_11; }
	inline RuntimeObject** get_address_of_U3CdcU3E5__9_11() { return &___U3CdcU3E5__9_11; }
	inline void set_U3CdcU3E5__9_11(RuntimeObject* value)
	{
		___U3CdcU3E5__9_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdcU3E5__9_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbcU3E5__10_12() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CbcU3E5__10_12)); }
	inline RuntimeObject* get_U3CbcU3E5__10_12() const { return ___U3CbcU3E5__10_12; }
	inline RuntimeObject** get_address_of_U3CbcU3E5__10_12() { return &___U3CbcU3E5__10_12; }
	inline void set_U3CbcU3E5__10_12(RuntimeObject* value)
	{
		___U3CbcU3E5__10_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbcU3E5__10_12), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__11_13() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CiU3E5__11_13)); }
	inline int32_t get_U3CiU3E5__11_13() const { return ___U3CiU3E5__11_13; }
	inline int32_t* get_address_of_U3CiU3E5__11_13() { return &___U3CiU3E5__11_13; }
	inline void set_U3CiU3E5__11_13(int32_t value)
	{
		___U3CiU3E5__11_13 = value;
	}

	inline static int32_t get_offset_of_U3CfirstArcLengthSampleU3E5__12_14() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CfirstArcLengthSampleU3E5__12_14)); }
	inline int32_t get_U3CfirstArcLengthSampleU3E5__12_14() const { return ___U3CfirstArcLengthSampleU3E5__12_14; }
	inline int32_t* get_address_of_U3CfirstArcLengthSampleU3E5__12_14() { return &___U3CfirstArcLengthSampleU3E5__12_14; }
	inline void set_U3CfirstArcLengthSampleU3E5__12_14(int32_t value)
	{
		___U3CfirstArcLengthSampleU3E5__12_14 = value;
	}

	inline static int32_t get_offset_of_U3ClastArcLengthSampleU3E5__13_15() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3ClastArcLengthSampleU3E5__13_15)); }
	inline int32_t get_U3ClastArcLengthSampleU3E5__13_15() const { return ___U3ClastArcLengthSampleU3E5__13_15; }
	inline int32_t* get_address_of_U3ClastArcLengthSampleU3E5__13_15() { return &___U3ClastArcLengthSampleU3E5__13_15; }
	inline void set_U3ClastArcLengthSampleU3E5__13_15(int32_t value)
	{
		___U3ClastArcLengthSampleU3E5__13_15 = value;
	}

	inline static int32_t get_offset_of_U3CupToSpanLengthU3E5__14_16() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CupToSpanLengthU3E5__14_16)); }
	inline float get_U3CupToSpanLengthU3E5__14_16() const { return ___U3CupToSpanLengthU3E5__14_16; }
	inline float* get_address_of_U3CupToSpanLengthU3E5__14_16() { return &___U3CupToSpanLengthU3E5__14_16; }
	inline void set_U3CupToSpanLengthU3E5__14_16(float value)
	{
		___U3CupToSpanLengthU3E5__14_16 = value;
	}

	inline static int32_t get_offset_of_U3CspanLengthU3E5__15_17() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CspanLengthU3E5__15_17)); }
	inline float get_U3CspanLengthU3E5__15_17() const { return ___U3CspanLengthU3E5__15_17; }
	inline float* get_address_of_U3CspanLengthU3E5__15_17() { return &___U3CspanLengthU3E5__15_17; }
	inline void set_U3CspanLengthU3E5__15_17(float value)
	{
		___U3CspanLengthU3E5__15_17 = value;
	}

	inline static int32_t get_offset_of_U3CparticlesInSpanU3E5__16_18() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CparticlesInSpanU3E5__16_18)); }
	inline int32_t get_U3CparticlesInSpanU3E5__16_18() const { return ___U3CparticlesInSpanU3E5__16_18; }
	inline int32_t* get_address_of_U3CparticlesInSpanU3E5__16_18() { return &___U3CparticlesInSpanU3E5__16_18; }
	inline void set_U3CparticlesInSpanU3E5__16_18(int32_t value)
	{
		___U3CparticlesInSpanU3E5__16_18 = value;
	}

	inline static int32_t get_offset_of_U3CdistanceU3E5__17_19() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CdistanceU3E5__17_19)); }
	inline float get_U3CdistanceU3E5__17_19() const { return ___U3CdistanceU3E5__17_19; }
	inline float* get_address_of_U3CdistanceU3E5__17_19() { return &___U3CdistanceU3E5__17_19; }
	inline void set_U3CdistanceU3E5__17_19(float value)
	{
		___U3CdistanceU3E5__17_19 = value;
	}

	inline static int32_t get_offset_of_U3CjU3E5__18_20() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CjU3E5__18_20)); }
	inline int32_t get_U3CjU3E5__18_20() const { return ___U3CjU3E5__18_20; }
	inline int32_t* get_address_of_U3CjU3E5__18_20() { return &___U3CjU3E5__18_20; }
	inline void set_U3CjU3E5__18_20(int32_t value)
	{
		___U3CjU3E5__18_20 = value;
	}

	inline static int32_t get_offset_of_U3CmuU3E5__19_21() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CmuU3E5__19_21)); }
	inline float get_U3CmuU3E5__19_21() const { return ___U3CmuU3E5__19_21; }
	inline float* get_address_of_U3CmuU3E5__19_21() { return &___U3CmuU3E5__19_21; }
	inline void set_U3CmuU3E5__19_21(float value)
	{
		___U3CmuU3E5__19_21 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__20_22() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CiU3E5__20_22)); }
	inline int32_t get_U3CiU3E5__20_22() const { return ___U3CiU3E5__20_22; }
	inline int32_t* get_address_of_U3CiU3E5__20_22() { return &___U3CiU3E5__20_22; }
	inline void set_U3CiU3E5__20_22(int32_t value)
	{
		___U3CiU3E5__20_22 = value;
	}

	inline static int32_t get_offset_of_U3CU3Es__21_23() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CU3Es__21_23)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_U3CU3Es__21_23() const { return ___U3CU3Es__21_23; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_U3CU3Es__21_23() { return &___U3CU3Es__21_23; }
	inline void set_U3CU3Es__21_23(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___U3CU3Es__21_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Es__21_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Es__22_24() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3CU3Es__22_24)); }
	inline int32_t get_U3CU3Es__22_24() const { return ___U3CU3Es__22_24; }
	inline int32_t* get_address_of_U3CU3Es__22_24() { return &___U3CU3Es__22_24; }
	inline void set_U3CU3Es__22_24(int32_t value)
	{
		___U3CU3Es__22_24 = value;
	}

	inline static int32_t get_offset_of_U3ClengthU3E5__23_25() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7, ___U3ClengthU3E5__23_25)); }
	inline float get_U3ClengthU3E5__23_25() const { return ___U3ClengthU3E5__23_25; }
	inline float* get_address_of_U3ClengthU3E5__23_25() { return &___U3ClengthU3E5__23_25; }
	inline void set_U3ClengthU3E5__23_25(float value)
	{
		___U3ClengthU3E5__23_25 = value;
	}
};


// Obi.ObiRopeBlueprintBase/<Initialize>d__17
struct U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiRopeBlueprintBase/<Initialize>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiRopeBlueprintBase/<Initialize>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Obi.ObiRopeBlueprintBase Obi.ObiRopeBlueprintBase/<Initialize>d__17::<>4__this
	ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8, ___U3CU3E4__this_2)); }
	inline ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// Obi.ObiSolver/<>c
struct U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_StaticFields
{
public:
	// Obi.ObiSolver/<>c Obi.ObiSolver/<>c::<>9
	U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16 * ___U3CU3E9_0;
	// System.Func`2<Obi.ObiSolver/ParticleInActor,System.Boolean> Obi.ObiSolver/<>c::<>9__148_0
	Func_2_t8E696E949C725E6CEB283DBB48AC377CC3AF687E * ___U3CU3E9__148_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__148_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_StaticFields, ___U3CU3E9__148_0_1)); }
	inline Func_2_t8E696E949C725E6CEB283DBB48AC377CC3AF687E * get_U3CU3E9__148_0_1() const { return ___U3CU3E9__148_0_1; }
	inline Func_2_t8E696E949C725E6CEB283DBB48AC377CC3AF687E ** get_address_of_U3CU3E9__148_0_1() { return &___U3CU3E9__148_0_1; }
	inline void set_U3CU3E9__148_0_1(Func_2_t8E696E949C725E6CEB283DBB48AC377CC3AF687E * value)
	{
		___U3CU3E9__148_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__148_0_1), (void*)value);
	}
};


// Obi.ObiSolver/ParticleInActor
struct ParticleInActor_t4D445CDF7A9DB2D80BEF82DBEEF606E8B61291BA  : public RuntimeObject
{
public:
	// Obi.ObiActor Obi.ObiSolver/ParticleInActor::actor
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6 * ___actor_0;
	// System.Int32 Obi.ObiSolver/ParticleInActor::indexInActor
	int32_t ___indexInActor_1;

public:
	inline static int32_t get_offset_of_actor_0() { return static_cast<int32_t>(offsetof(ParticleInActor_t4D445CDF7A9DB2D80BEF82DBEEF606E8B61291BA, ___actor_0)); }
	inline ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6 * get_actor_0() const { return ___actor_0; }
	inline ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6 ** get_address_of_actor_0() { return &___actor_0; }
	inline void set_actor_0(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6 * value)
	{
		___actor_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actor_0), (void*)value);
	}

	inline static int32_t get_offset_of_indexInActor_1() { return static_cast<int32_t>(offsetof(ParticleInActor_t4D445CDF7A9DB2D80BEF82DBEEF606E8B61291BA, ___indexInActor_1)); }
	inline int32_t get_indexInActor_1() const { return ___indexInActor_1; }
	inline int32_t* get_address_of_indexInActor_1() { return &___indexInActor_1; }
	inline void set_indexInActor_1(int32_t value)
	{
		___indexInActor_1 = value;
	}
};


// Obi.ObiStitcher/Stitch
struct Stitch_tFC29C4BD6C0E9EC2F51B4658C215F27B4E0D7201  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiStitcher/Stitch::particleIndex1
	int32_t ___particleIndex1_0;
	// System.Int32 Obi.ObiStitcher/Stitch::particleIndex2
	int32_t ___particleIndex2_1;

public:
	inline static int32_t get_offset_of_particleIndex1_0() { return static_cast<int32_t>(offsetof(Stitch_tFC29C4BD6C0E9EC2F51B4658C215F27B4E0D7201, ___particleIndex1_0)); }
	inline int32_t get_particleIndex1_0() const { return ___particleIndex1_0; }
	inline int32_t* get_address_of_particleIndex1_0() { return &___particleIndex1_0; }
	inline void set_particleIndex1_0(int32_t value)
	{
		___particleIndex1_0 = value;
	}

	inline static int32_t get_offset_of_particleIndex2_1() { return static_cast<int32_t>(offsetof(Stitch_tFC29C4BD6C0E9EC2F51B4658C215F27B4E0D7201, ___particleIndex2_1)); }
	inline int32_t get_particleIndex2_1() const { return ___particleIndex2_1; }
	inline int32_t* get_address_of_particleIndex2_1() { return &___particleIndex2_1; }
	inline void set_particleIndex2_1(int32_t value)
	{
		___particleIndex2_1 = value;
	}
};


// Obi.ObiTriangleMeshContainer/<>c
struct U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_StaticFields
{
public:
	// Obi.ObiTriangleMeshContainer/<>c Obi.ObiTriangleMeshContainer/<>c::<>9
	U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * ___U3CU3E9_0;
	// System.Converter`2<Obi.IBounded,Obi.Triangle> Obi.ObiTriangleMeshContainer/<>c::<>9__6_0
	Converter_2_tA5CFCDD2DDF09EC21725B24DD40CB2E6C928F1F2 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Converter_2_tA5CFCDD2DDF09EC21725B24DD40CB2E6C928F1F2 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Converter_2_tA5CFCDD2DDF09EC21725B24DD40CB2E6C928F1F2 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Converter_2_tA5CFCDD2DDF09EC21725B24DD40CB2E6C928F1F2 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_0_1), (void*)value);
	}
};


// Obi.ObiUtils/<BilateralInterleaved>d__40
struct U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiUtils/<BilateralInterleaved>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiUtils/<BilateralInterleaved>d__40::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 Obi.ObiUtils/<BilateralInterleaved>d__40::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Int32 Obi.ObiUtils/<BilateralInterleaved>d__40::count
	int32_t ___count_3;
	// System.Int32 Obi.ObiUtils/<BilateralInterleaved>d__40::<>3__count
	int32_t ___U3CU3E3__count_4;
	// System.Int32 Obi.ObiUtils/<BilateralInterleaved>d__40::<i>5__1
	int32_t ___U3CiU3E5__1_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__count_4() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___U3CU3E3__count_4)); }
	inline int32_t get_U3CU3E3__count_4() const { return ___U3CU3E3__count_4; }
	inline int32_t* get_address_of_U3CU3E3__count_4() { return &___U3CU3E3__count_4; }
	inline void set_U3CU3E3__count_4(int32_t value)
	{
		___U3CU3E3__count_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___U3CiU3E5__1_5)); }
	inline int32_t get_U3CiU3E5__1_5() const { return ___U3CiU3E5__1_5; }
	inline int32_t* get_address_of_U3CiU3E5__1_5() { return &___U3CiU3E5__1_5; }
	inline void set_U3CiU3E5__1_5(int32_t value)
	{
		___U3CiU3E5__1_5 = value;
	}
};


// Obi.VoxelDistanceField/<JumpFlood>d__5
struct U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401  : public RuntimeObject
{
public:
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.VoxelDistanceField/<JumpFlood>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Obi.VoxelDistanceField Obi.VoxelDistanceField/<JumpFlood>d__5::<>4__this
	VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * ___U3CU3E4__this_2;
	// UnityEngine.Vector3Int[0...,0...,0...] Obi.VoxelDistanceField/<JumpFlood>d__5::<auxBuffer>5__1
	Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* ___U3CauxBufferU3E5__1_3;
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<size>5__2
	int32_t ___U3CsizeU3E5__2_4;
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<step>5__3
	int32_t ___U3CstepU3E5__3_5;
	// System.Single Obi.VoxelDistanceField/<JumpFlood>d__5::<numPasses>5__4
	float ___U3CnumPassesU3E5__4_6;
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<i>5__5
	int32_t ___U3CiU3E5__5_7;
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<x>5__6
	int32_t ___U3CxU3E5__6_8;
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<y>5__7
	int32_t ___U3CyU3E5__7_9;
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<z>5__8
	int32_t ___U3CzU3E5__8_10;
	// UnityEngine.Vector3Int[0...,0...,0...] Obi.VoxelDistanceField/<JumpFlood>d__5::<temp>5__9
	Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* ___U3CtempU3E5__9_11;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CU3E4__this_2)); }
	inline VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CauxBufferU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CauxBufferU3E5__1_3)); }
	inline Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* get_U3CauxBufferU3E5__1_3() const { return ___U3CauxBufferU3E5__1_3; }
	inline Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9** get_address_of_U3CauxBufferU3E5__1_3() { return &___U3CauxBufferU3E5__1_3; }
	inline void set_U3CauxBufferU3E5__1_3(Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* value)
	{
		___U3CauxBufferU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CauxBufferU3E5__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsizeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CsizeU3E5__2_4)); }
	inline int32_t get_U3CsizeU3E5__2_4() const { return ___U3CsizeU3E5__2_4; }
	inline int32_t* get_address_of_U3CsizeU3E5__2_4() { return &___U3CsizeU3E5__2_4; }
	inline void set_U3CsizeU3E5__2_4(int32_t value)
	{
		___U3CsizeU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CstepU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CstepU3E5__3_5)); }
	inline int32_t get_U3CstepU3E5__3_5() const { return ___U3CstepU3E5__3_5; }
	inline int32_t* get_address_of_U3CstepU3E5__3_5() { return &___U3CstepU3E5__3_5; }
	inline void set_U3CstepU3E5__3_5(int32_t value)
	{
		___U3CstepU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CnumPassesU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CnumPassesU3E5__4_6)); }
	inline float get_U3CnumPassesU3E5__4_6() const { return ___U3CnumPassesU3E5__4_6; }
	inline float* get_address_of_U3CnumPassesU3E5__4_6() { return &___U3CnumPassesU3E5__4_6; }
	inline void set_U3CnumPassesU3E5__4_6(float value)
	{
		___U3CnumPassesU3E5__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CiU3E5__5_7)); }
	inline int32_t get_U3CiU3E5__5_7() const { return ___U3CiU3E5__5_7; }
	inline int32_t* get_address_of_U3CiU3E5__5_7() { return &___U3CiU3E5__5_7; }
	inline void set_U3CiU3E5__5_7(int32_t value)
	{
		___U3CiU3E5__5_7 = value;
	}

	inline static int32_t get_offset_of_U3CxU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CxU3E5__6_8)); }
	inline int32_t get_U3CxU3E5__6_8() const { return ___U3CxU3E5__6_8; }
	inline int32_t* get_address_of_U3CxU3E5__6_8() { return &___U3CxU3E5__6_8; }
	inline void set_U3CxU3E5__6_8(int32_t value)
	{
		___U3CxU3E5__6_8 = value;
	}

	inline static int32_t get_offset_of_U3CyU3E5__7_9() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CyU3E5__7_9)); }
	inline int32_t get_U3CyU3E5__7_9() const { return ___U3CyU3E5__7_9; }
	inline int32_t* get_address_of_U3CyU3E5__7_9() { return &___U3CyU3E5__7_9; }
	inline void set_U3CyU3E5__7_9(int32_t value)
	{
		___U3CyU3E5__7_9 = value;
	}

	inline static int32_t get_offset_of_U3CzU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CzU3E5__8_10)); }
	inline int32_t get_U3CzU3E5__8_10() const { return ___U3CzU3E5__8_10; }
	inline int32_t* get_address_of_U3CzU3E5__8_10() { return &___U3CzU3E5__8_10; }
	inline void set_U3CzU3E5__8_10(int32_t value)
	{
		___U3CzU3E5__8_10 = value;
	}

	inline static int32_t get_offset_of_U3CtempU3E5__9_11() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CtempU3E5__9_11)); }
	inline Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* get_U3CtempU3E5__9_11() const { return ___U3CtempU3E5__9_11; }
	inline Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9** get_address_of_U3CtempU3E5__9_11() { return &___U3CtempU3E5__9_11; }
	inline void set_U3CtempU3E5__9_11(Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* value)
	{
		___U3CtempU3E5__9_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtempU3E5__9_11), (void*)value);
	}
};


// Obi.VoxelPathFinder/<>c
struct U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_StaticFields
{
public:
	// Obi.VoxelPathFinder/<>c Obi.VoxelPathFinder/<>c::<>9
	U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Vector3Int,System.Single> Obi.VoxelPathFinder/<>c::<>9__6_1
	Func_2_t60C15697969143DDD791767187830BED28B48143 * ___U3CU3E9__6_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_StaticFields, ___U3CU3E9__6_1_1)); }
	inline Func_2_t60C15697969143DDD791767187830BED28B48143 * get_U3CU3E9__6_1_1() const { return ___U3CU3E9__6_1_1; }
	inline Func_2_t60C15697969143DDD791767187830BED28B48143 ** get_address_of_U3CU3E9__6_1_1() { return &___U3CU3E9__6_1_1; }
	inline void set_U3CU3E9__6_1_1(Func_2_t60C15697969143DDD791767187830BED28B48143 * value)
	{
		___U3CU3E9__6_1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_1_1), (void*)value);
	}
};


// Obi.ObiPathDataChannelIdentity`1<UnityEngine.Color>
struct ObiPathDataChannelIdentity_1_tB099F9C735620BE0BE1A9FE2208986F0128A2104  : public ObiPathDataChannel_2_t81E3117D8F660A0253E9EBD50BBD50A92F4BB8CE
{
public:

public:
};


// Obi.ObiPathDataChannelIdentity`1<System.Int32>
struct ObiPathDataChannelIdentity_1_tB49FD768113A83C96CCE65F0A01E480360D13F86  : public ObiPathDataChannel_2_t5BC70F53CBFF7F0F5FA5BD3DCFED49FE33E5700A
{
public:

public:
};


// Obi.ObiPathDataChannelIdentity`1<System.Single>
struct ObiPathDataChannelIdentity_1_t472CD690CECDE58D7B1E3020921399E15844875C  : public ObiPathDataChannel_2_t0A3CDBB8C1B1657FA8C13C92E6C5E2C2527C071E
{
public:

public:
};


// Obi.ObiPathDataChannelIdentity`1<UnityEngine.Vector3>
struct ObiPathDataChannelIdentity_1_t58918EE3CAC1576B050AFB8CF1148BD9E5F7B230  : public ObiPathDataChannel_2_t5C31D471EBD01623446ECFF76AD0A33B63C022FC
{
public:

public:
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// Obi.ObiBendConstraintsBatch
struct ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79  : public ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127
{
public:
	// Obi.IBendConstraintsBatchImpl Obi.ObiBendConstraintsBatch::m_BatchImpl
	RuntimeObject* ___m_BatchImpl_7;
	// Obi.ObiNativeFloatList Obi.ObiBendConstraintsBatch::restBends
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___restBends_8;
	// Obi.ObiNativeVector2List Obi.ObiBendConstraintsBatch::bendingStiffnesses
	ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * ___bendingStiffnesses_9;
	// Obi.ObiNativeVector2List Obi.ObiBendConstraintsBatch::plasticity
	ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * ___plasticity_10;

public:
	inline static int32_t get_offset_of_m_BatchImpl_7() { return static_cast<int32_t>(offsetof(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79, ___m_BatchImpl_7)); }
	inline RuntimeObject* get_m_BatchImpl_7() const { return ___m_BatchImpl_7; }
	inline RuntimeObject** get_address_of_m_BatchImpl_7() { return &___m_BatchImpl_7; }
	inline void set_m_BatchImpl_7(RuntimeObject* value)
	{
		___m_BatchImpl_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BatchImpl_7), (void*)value);
	}

	inline static int32_t get_offset_of_restBends_8() { return static_cast<int32_t>(offsetof(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79, ___restBends_8)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_restBends_8() const { return ___restBends_8; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_restBends_8() { return &___restBends_8; }
	inline void set_restBends_8(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___restBends_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restBends_8), (void*)value);
	}

	inline static int32_t get_offset_of_bendingStiffnesses_9() { return static_cast<int32_t>(offsetof(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79, ___bendingStiffnesses_9)); }
	inline ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * get_bendingStiffnesses_9() const { return ___bendingStiffnesses_9; }
	inline ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD ** get_address_of_bendingStiffnesses_9() { return &___bendingStiffnesses_9; }
	inline void set_bendingStiffnesses_9(ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * value)
	{
		___bendingStiffnesses_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bendingStiffnesses_9), (void*)value);
	}

	inline static int32_t get_offset_of_plasticity_10() { return static_cast<int32_t>(offsetof(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79, ___plasticity_10)); }
	inline ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * get_plasticity_10() const { return ___plasticity_10; }
	inline ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD ** get_address_of_plasticity_10() { return &___plasticity_10; }
	inline void set_plasticity_10(ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * value)
	{
		___plasticity_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___plasticity_10), (void*)value);
	}
};


// Obi.ObiBendConstraintsData
struct ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED  : public ObiConstraints_1_t1A77FB453F8651AB273DB0DE943C8AF5AF981F82
{
public:

public:
};


// Obi.ObiChainConstraintsBatch
struct ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E  : public ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127
{
public:
	// Obi.IChainConstraintsBatchImpl Obi.ObiChainConstraintsBatch::m_BatchImpl
	RuntimeObject* ___m_BatchImpl_7;
	// Obi.ObiNativeIntList Obi.ObiChainConstraintsBatch::firstParticle
	ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * ___firstParticle_8;
	// Obi.ObiNativeIntList Obi.ObiChainConstraintsBatch::numParticles
	ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * ___numParticles_9;
	// Obi.ObiNativeVector2List Obi.ObiChainConstraintsBatch::lengths
	ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * ___lengths_10;

public:
	inline static int32_t get_offset_of_m_BatchImpl_7() { return static_cast<int32_t>(offsetof(ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E, ___m_BatchImpl_7)); }
	inline RuntimeObject* get_m_BatchImpl_7() const { return ___m_BatchImpl_7; }
	inline RuntimeObject** get_address_of_m_BatchImpl_7() { return &___m_BatchImpl_7; }
	inline void set_m_BatchImpl_7(RuntimeObject* value)
	{
		___m_BatchImpl_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BatchImpl_7), (void*)value);
	}

	inline static int32_t get_offset_of_firstParticle_8() { return static_cast<int32_t>(offsetof(ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E, ___firstParticle_8)); }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * get_firstParticle_8() const { return ___firstParticle_8; }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 ** get_address_of_firstParticle_8() { return &___firstParticle_8; }
	inline void set_firstParticle_8(ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * value)
	{
		___firstParticle_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstParticle_8), (void*)value);
	}

	inline static int32_t get_offset_of_numParticles_9() { return static_cast<int32_t>(offsetof(ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E, ___numParticles_9)); }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * get_numParticles_9() const { return ___numParticles_9; }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 ** get_address_of_numParticles_9() { return &___numParticles_9; }
	inline void set_numParticles_9(ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * value)
	{
		___numParticles_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numParticles_9), (void*)value);
	}

	inline static int32_t get_offset_of_lengths_10() { return static_cast<int32_t>(offsetof(ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E, ___lengths_10)); }
	inline ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * get_lengths_10() const { return ___lengths_10; }
	inline ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD ** get_address_of_lengths_10() { return &___lengths_10; }
	inline void set_lengths_10(ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * value)
	{
		___lengths_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lengths_10), (void*)value);
	}
};


// Obi.ObiChainConstraintsData
struct ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0  : public ObiConstraints_1_tC13134EAD7E5A4205F5834810FB89F47972C220E
{
public:

public:
};


// Obi.ObiDistanceConstraintsBatch
struct ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE  : public ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127
{
public:
	// Obi.IDistanceConstraintsBatchImpl Obi.ObiDistanceConstraintsBatch::m_BatchImpl
	RuntimeObject* ___m_BatchImpl_7;
	// Obi.ObiNativeFloatList Obi.ObiDistanceConstraintsBatch::restLengths
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___restLengths_8;
	// Obi.ObiNativeVector2List Obi.ObiDistanceConstraintsBatch::stiffnesses
	ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * ___stiffnesses_9;

public:
	inline static int32_t get_offset_of_m_BatchImpl_7() { return static_cast<int32_t>(offsetof(ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE, ___m_BatchImpl_7)); }
	inline RuntimeObject* get_m_BatchImpl_7() const { return ___m_BatchImpl_7; }
	inline RuntimeObject** get_address_of_m_BatchImpl_7() { return &___m_BatchImpl_7; }
	inline void set_m_BatchImpl_7(RuntimeObject* value)
	{
		___m_BatchImpl_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BatchImpl_7), (void*)value);
	}

	inline static int32_t get_offset_of_restLengths_8() { return static_cast<int32_t>(offsetof(ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE, ___restLengths_8)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_restLengths_8() const { return ___restLengths_8; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_restLengths_8() { return &___restLengths_8; }
	inline void set_restLengths_8(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___restLengths_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restLengths_8), (void*)value);
	}

	inline static int32_t get_offset_of_stiffnesses_9() { return static_cast<int32_t>(offsetof(ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE, ___stiffnesses_9)); }
	inline ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * get_stiffnesses_9() const { return ___stiffnesses_9; }
	inline ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD ** get_address_of_stiffnesses_9() { return &___stiffnesses_9; }
	inline void set_stiffnesses_9(ObiNativeVector2List_t6BCE64935660CB8B866A2E2B325CC0676F172AAD * value)
	{
		___stiffnesses_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stiffnesses_9), (void*)value);
	}
};


// Obi.ObiDistanceConstraintsData
struct ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB  : public ObiConstraints_1_tDDB54978FB9AEE6BD8B391B96A13174F2E9966ED
{
public:

public:
};


// Obi.ObiPointsDataChannel
struct ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507  : public ObiPathDataChannel_2_tEB1A7322E5FCEC0ECC8D096114FD0CD6FE92AB92
{
public:

public:
};


// Obi.ObiStretchShearConstraintsBatch
struct ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B  : public ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127
{
public:
	// Obi.IStretchShearConstraintsBatchImpl Obi.ObiStretchShearConstraintsBatch::m_BatchImpl
	RuntimeObject* ___m_BatchImpl_7;
	// Obi.ObiNativeIntList Obi.ObiStretchShearConstraintsBatch::orientationIndices
	ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * ___orientationIndices_8;
	// Obi.ObiNativeFloatList Obi.ObiStretchShearConstraintsBatch::restLengths
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___restLengths_9;
	// Obi.ObiNativeQuaternionList Obi.ObiStretchShearConstraintsBatch::restOrientations
	ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * ___restOrientations_10;
	// Obi.ObiNativeVector3List Obi.ObiStretchShearConstraintsBatch::stiffnesses
	ObiNativeVector3List_t91CCFD3FCE3DFE0A243C7A6EB5C3117F2CE8C8D7 * ___stiffnesses_11;

public:
	inline static int32_t get_offset_of_m_BatchImpl_7() { return static_cast<int32_t>(offsetof(ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B, ___m_BatchImpl_7)); }
	inline RuntimeObject* get_m_BatchImpl_7() const { return ___m_BatchImpl_7; }
	inline RuntimeObject** get_address_of_m_BatchImpl_7() { return &___m_BatchImpl_7; }
	inline void set_m_BatchImpl_7(RuntimeObject* value)
	{
		___m_BatchImpl_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BatchImpl_7), (void*)value);
	}

	inline static int32_t get_offset_of_orientationIndices_8() { return static_cast<int32_t>(offsetof(ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B, ___orientationIndices_8)); }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * get_orientationIndices_8() const { return ___orientationIndices_8; }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 ** get_address_of_orientationIndices_8() { return &___orientationIndices_8; }
	inline void set_orientationIndices_8(ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * value)
	{
		___orientationIndices_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___orientationIndices_8), (void*)value);
	}

	inline static int32_t get_offset_of_restLengths_9() { return static_cast<int32_t>(offsetof(ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B, ___restLengths_9)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_restLengths_9() const { return ___restLengths_9; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_restLengths_9() { return &___restLengths_9; }
	inline void set_restLengths_9(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___restLengths_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restLengths_9), (void*)value);
	}

	inline static int32_t get_offset_of_restOrientations_10() { return static_cast<int32_t>(offsetof(ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B, ___restOrientations_10)); }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * get_restOrientations_10() const { return ___restOrientations_10; }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 ** get_address_of_restOrientations_10() { return &___restOrientations_10; }
	inline void set_restOrientations_10(ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * value)
	{
		___restOrientations_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restOrientations_10), (void*)value);
	}

	inline static int32_t get_offset_of_stiffnesses_11() { return static_cast<int32_t>(offsetof(ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B, ___stiffnesses_11)); }
	inline ObiNativeVector3List_t91CCFD3FCE3DFE0A243C7A6EB5C3117F2CE8C8D7 * get_stiffnesses_11() const { return ___stiffnesses_11; }
	inline ObiNativeVector3List_t91CCFD3FCE3DFE0A243C7A6EB5C3117F2CE8C8D7 ** get_address_of_stiffnesses_11() { return &___stiffnesses_11; }
	inline void set_stiffnesses_11(ObiNativeVector3List_t91CCFD3FCE3DFE0A243C7A6EB5C3117F2CE8C8D7 * value)
	{
		___stiffnesses_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stiffnesses_11), (void*)value);
	}
};


// Obi.ObiStretchShearConstraintsData
struct ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC  : public ObiConstraints_1_t9491848D3D98BFF80E3C3AAF710BCE40F4F51610
{
public:

public:
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// Obi.SimplexCounts
struct SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA 
{
public:
	// System.Int32 Obi.SimplexCounts::pointCount
	int32_t ___pointCount_0;
	// System.Int32 Obi.SimplexCounts::edgeCount
	int32_t ___edgeCount_1;
	// System.Int32 Obi.SimplexCounts::triangleCount
	int32_t ___triangleCount_2;

public:
	inline static int32_t get_offset_of_pointCount_0() { return static_cast<int32_t>(offsetof(SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA, ___pointCount_0)); }
	inline int32_t get_pointCount_0() const { return ___pointCount_0; }
	inline int32_t* get_address_of_pointCount_0() { return &___pointCount_0; }
	inline void set_pointCount_0(int32_t value)
	{
		___pointCount_0 = value;
	}

	inline static int32_t get_offset_of_edgeCount_1() { return static_cast<int32_t>(offsetof(SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA, ___edgeCount_1)); }
	inline int32_t get_edgeCount_1() const { return ___edgeCount_1; }
	inline int32_t* get_address_of_edgeCount_1() { return &___edgeCount_1; }
	inline void set_edgeCount_1(int32_t value)
	{
		___edgeCount_1 = value;
	}

	inline static int32_t get_offset_of_triangleCount_2() { return static_cast<int32_t>(offsetof(SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA, ___triangleCount_2)); }
	inline int32_t get_triangleCount_2() const { return ___triangleCount_2; }
	inline int32_t* get_address_of_triangleCount_2() { return &___triangleCount_2; }
	inline void set_triangleCount_2(int32_t value)
	{
		___triangleCount_2 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2Int
struct Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 
{
public:
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}
};

struct Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields
{
public:
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___s_Right_7;

public:
	inline static int32_t get_offset_of_s_Zero_2() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Zero_2)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Zero_2() const { return ___s_Zero_2; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Zero_2() { return &___s_Zero_2; }
	inline void set_s_Zero_2(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Zero_2 = value;
	}

	inline static int32_t get_offset_of_s_One_3() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_One_3)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_One_3() const { return ___s_One_3; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_One_3() { return &___s_One_3; }
	inline void set_s_One_3(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_One_3 = value;
	}

	inline static int32_t get_offset_of_s_Up_4() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Up_4)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Up_4() const { return ___s_Up_4; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Up_4() { return &___s_Up_4; }
	inline void set_s_Up_4(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Up_4 = value;
	}

	inline static int32_t get_offset_of_s_Down_5() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Down_5)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Down_5() const { return ___s_Down_5; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Down_5() { return &___s_Down_5; }
	inline void set_s_Down_5(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Down_5 = value;
	}

	inline static int32_t get_offset_of_s_Left_6() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Left_6)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Left_6() const { return ___s_Left_6; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Left_6() { return &___s_Left_6; }
	inline void set_s_Left_6(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Left_6 = value;
	}

	inline static int32_t get_offset_of_s_Right_7() { return static_cast<int32_t>(offsetof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9_StaticFields, ___s_Right_7)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_s_Right_7() const { return ___s_Right_7; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_s_Right_7() { return &___s_Right_7; }
	inline void set_s_Right_7(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___s_Right_7 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector3Int
struct Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA 
{
public:
	// System.Int32 UnityEngine.Vector3Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector3Int::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.Vector3Int::m_Z
	int32_t ___m_Z_2;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}

	inline static int32_t get_offset_of_m_Z_2() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA, ___m_Z_2)); }
	inline int32_t get_m_Z_2() const { return ___m_Z_2; }
	inline int32_t* get_address_of_m_Z_2() { return &___m_Z_2; }
	inline void set_m_Z_2(int32_t value)
	{
		___m_Z_2 = value;
	}
};

struct Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields
{
public:
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Zero
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Zero_3;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_One
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_One_4;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Up
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Up_5;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Down
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Down_6;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Left
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Left_7;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Right
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Right_8;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Forward
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Forward_9;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Back
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Back_10;

public:
	inline static int32_t get_offset_of_s_Zero_3() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Zero_3)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Zero_3() const { return ___s_Zero_3; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Zero_3() { return &___s_Zero_3; }
	inline void set_s_Zero_3(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Zero_3 = value;
	}

	inline static int32_t get_offset_of_s_One_4() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_One_4)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_One_4() const { return ___s_One_4; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_One_4() { return &___s_One_4; }
	inline void set_s_One_4(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_One_4 = value;
	}

	inline static int32_t get_offset_of_s_Up_5() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Up_5)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Up_5() const { return ___s_Up_5; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Up_5() { return &___s_Up_5; }
	inline void set_s_Up_5(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Up_5 = value;
	}

	inline static int32_t get_offset_of_s_Down_6() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Down_6)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Down_6() const { return ___s_Down_6; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Down_6() { return &___s_Down_6; }
	inline void set_s_Down_6(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Down_6 = value;
	}

	inline static int32_t get_offset_of_s_Left_7() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Left_7)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Left_7() const { return ___s_Left_7; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Left_7() { return &___s_Left_7; }
	inline void set_s_Left_7(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Left_7 = value;
	}

	inline static int32_t get_offset_of_s_Right_8() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Right_8)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Right_8() const { return ___s_Right_8; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Right_8() { return &___s_Right_8; }
	inline void set_s_Right_8(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Right_8 = value;
	}

	inline static int32_t get_offset_of_s_Forward_9() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Forward_9)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Forward_9() const { return ___s_Forward_9; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Forward_9() { return &___s_Forward_9; }
	inline void set_s_Forward_9(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Forward_9 = value;
	}

	inline static int32_t get_offset_of_s_Back_10() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Back_10)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Back_10() const { return ___s_Back_10; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Back_10() { return &___s_Back_10; }
	inline void set_s_Back_10(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Back_10 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Obi.ObiSolver/ObiCollisionEventArgs
struct ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5  : public EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA
{
public:
	// Obi.ObiList`1<Oni/Contact> Obi.ObiSolver/ObiCollisionEventArgs::contacts
	ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3 * ___contacts_1;

public:
	inline static int32_t get_offset_of_contacts_1() { return static_cast<int32_t>(offsetof(ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5, ___contacts_1)); }
	inline ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3 * get_contacts_1() const { return ___contacts_1; }
	inline ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3 ** get_address_of_contacts_1() { return &___contacts_1; }
	inline void set_contacts_1(ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3 * value)
	{
		___contacts_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contacts_1), (void*)value);
	}
};


// Oni/ProfileInfo
struct ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A 
{
public:
	// System.Double Oni/ProfileInfo::start
	double ___start_0;
	// System.Double Oni/ProfileInfo::end
	double ___end_1;
	// System.UInt32 Oni/ProfileInfo::info
	uint32_t ___info_2;
	// System.Int32 Oni/ProfileInfo::pad
	int32_t ___pad_3;
	// System.String Oni/ProfileInfo::name
	String_t* ___name_4;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A, ___start_0)); }
	inline double get_start_0() const { return ___start_0; }
	inline double* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(double value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A, ___end_1)); }
	inline double get_end_1() const { return ___end_1; }
	inline double* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(double value)
	{
		___end_1 = value;
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A, ___info_2)); }
	inline uint32_t get_info_2() const { return ___info_2; }
	inline uint32_t* get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(uint32_t value)
	{
		___info_2 = value;
	}

	inline static int32_t get_offset_of_pad_3() { return static_cast<int32_t>(offsetof(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A, ___pad_3)); }
	inline int32_t get_pad_3() const { return ___pad_3; }
	inline int32_t* get_address_of_pad_3() { return &___pad_3; }
	inline void set_pad_3(int32_t value)
	{
		___pad_3 = value;
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Oni/ProfileInfo
struct ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_pinvoke
{
	double ___start_0;
	double ___end_1;
	uint32_t ___info_2;
	int32_t ___pad_3;
	char ___name_4[64];
};
// Native definition for COM marshalling of Oni/ProfileInfo
struct ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_com
{
	double ___start_0;
	double ___end_1;
	uint32_t ___info_2;
	int32_t ___pad_3;
	char ___name_4[64];
};

// Obi.Aabb
struct Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF 
{
public:
	// UnityEngine.Vector4 Obi.Aabb::min
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___min_0;
	// UnityEngine.Vector4 Obi.Aabb::max
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF, ___min_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_min_0() const { return ___min_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF, ___max_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_max_1() const { return ___max_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___max_1 = value;
	}
};


// UnityEngine.Bounds
struct Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// Obi.MeshVoxelizer
struct MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D  : public RuntimeObject
{
public:
	// UnityEngine.Mesh Obi.MeshVoxelizer::input
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___input_5;
	// Obi.MeshVoxelizer/Voxel[] Obi.MeshVoxelizer::voxels
	VoxelU5BU5D_tA0FDC07F9F3BB9F86147BF6E063718D1BAAD3E55* ___voxels_6;
	// System.Single Obi.MeshVoxelizer::voxelSize
	float ___voxelSize_7;
	// UnityEngine.Vector3Int Obi.MeshVoxelizer::resolution
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___resolution_8;
	// System.Collections.Generic.List`1<System.Int32>[] Obi.MeshVoxelizer::triangleIndices
	List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C* ___triangleIndices_9;
	// UnityEngine.Vector3Int Obi.MeshVoxelizer::origin
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___origin_10;

public:
	inline static int32_t get_offset_of_input_5() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___input_5)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_input_5() const { return ___input_5; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_input_5() { return &___input_5; }
	inline void set_input_5(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___input_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___input_5), (void*)value);
	}

	inline static int32_t get_offset_of_voxels_6() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___voxels_6)); }
	inline VoxelU5BU5D_tA0FDC07F9F3BB9F86147BF6E063718D1BAAD3E55* get_voxels_6() const { return ___voxels_6; }
	inline VoxelU5BU5D_tA0FDC07F9F3BB9F86147BF6E063718D1BAAD3E55** get_address_of_voxels_6() { return &___voxels_6; }
	inline void set_voxels_6(VoxelU5BU5D_tA0FDC07F9F3BB9F86147BF6E063718D1BAAD3E55* value)
	{
		___voxels_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___voxels_6), (void*)value);
	}

	inline static int32_t get_offset_of_voxelSize_7() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___voxelSize_7)); }
	inline float get_voxelSize_7() const { return ___voxelSize_7; }
	inline float* get_address_of_voxelSize_7() { return &___voxelSize_7; }
	inline void set_voxelSize_7(float value)
	{
		___voxelSize_7 = value;
	}

	inline static int32_t get_offset_of_resolution_8() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___resolution_8)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_resolution_8() const { return ___resolution_8; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_resolution_8() { return &___resolution_8; }
	inline void set_resolution_8(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___resolution_8 = value;
	}

	inline static int32_t get_offset_of_triangleIndices_9() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___triangleIndices_9)); }
	inline List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C* get_triangleIndices_9() const { return ___triangleIndices_9; }
	inline List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C** get_address_of_triangleIndices_9() { return &___triangleIndices_9; }
	inline void set_triangleIndices_9(List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C* value)
	{
		___triangleIndices_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___triangleIndices_9), (void*)value);
	}

	inline static int32_t get_offset_of_origin_10() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___origin_10)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_origin_10() const { return ___origin_10; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_origin_10() { return &___origin_10; }
	inline void set_origin_10(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___origin_10 = value;
	}
};

struct MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields
{
public:
	// UnityEngine.Vector3Int[] Obi.MeshVoxelizer::fullNeighborhood
	Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* ___fullNeighborhood_0;
	// UnityEngine.Vector3Int[] Obi.MeshVoxelizer::edgefaceNeighborhood
	Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* ___edgefaceNeighborhood_1;
	// UnityEngine.Vector3Int[] Obi.MeshVoxelizer::faceNeighborhood
	Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* ___faceNeighborhood_2;
	// UnityEngine.Vector3Int[] Obi.MeshVoxelizer::edgeNeighborhood
	Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* ___edgeNeighborhood_3;
	// UnityEngine.Vector3Int[] Obi.MeshVoxelizer::vertexNeighborhood
	Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* ___vertexNeighborhood_4;

public:
	inline static int32_t get_offset_of_fullNeighborhood_0() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields, ___fullNeighborhood_0)); }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* get_fullNeighborhood_0() const { return ___fullNeighborhood_0; }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D** get_address_of_fullNeighborhood_0() { return &___fullNeighborhood_0; }
	inline void set_fullNeighborhood_0(Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* value)
	{
		___fullNeighborhood_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fullNeighborhood_0), (void*)value);
	}

	inline static int32_t get_offset_of_edgefaceNeighborhood_1() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields, ___edgefaceNeighborhood_1)); }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* get_edgefaceNeighborhood_1() const { return ___edgefaceNeighborhood_1; }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D** get_address_of_edgefaceNeighborhood_1() { return &___edgefaceNeighborhood_1; }
	inline void set_edgefaceNeighborhood_1(Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* value)
	{
		___edgefaceNeighborhood_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___edgefaceNeighborhood_1), (void*)value);
	}

	inline static int32_t get_offset_of_faceNeighborhood_2() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields, ___faceNeighborhood_2)); }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* get_faceNeighborhood_2() const { return ___faceNeighborhood_2; }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D** get_address_of_faceNeighborhood_2() { return &___faceNeighborhood_2; }
	inline void set_faceNeighborhood_2(Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* value)
	{
		___faceNeighborhood_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___faceNeighborhood_2), (void*)value);
	}

	inline static int32_t get_offset_of_edgeNeighborhood_3() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields, ___edgeNeighborhood_3)); }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* get_edgeNeighborhood_3() const { return ___edgeNeighborhood_3; }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D** get_address_of_edgeNeighborhood_3() { return &___edgeNeighborhood_3; }
	inline void set_edgeNeighborhood_3(Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* value)
	{
		___edgeNeighborhood_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___edgeNeighborhood_3), (void*)value);
	}

	inline static int32_t get_offset_of_vertexNeighborhood_4() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields, ___vertexNeighborhood_4)); }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* get_vertexNeighborhood_4() const { return ___vertexNeighborhood_4; }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D** get_address_of_vertexNeighborhood_4() { return &___vertexNeighborhood_4; }
	inline void set_vertexNeighborhood_4(Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* value)
	{
		___vertexNeighborhood_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vertexNeighborhood_4), (void*)value);
	}
};


// Obi.ObiColorDataChannel
struct ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2  : public ObiPathDataChannelIdentity_1_tB099F9C735620BE0BE1A9FE2208986F0128A2104
{
public:

public:
};


// Obi.ObiMassDataChannel
struct ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996  : public ObiPathDataChannelIdentity_1_t472CD690CECDE58D7B1E3020921399E15844875C
{
public:

public:
};


// Obi.ObiNormalDataChannel
struct ObiNormalDataChannel_tA3F5F99B539B56E0703FA0603B89DCC345AC3296  : public ObiPathDataChannelIdentity_1_t58918EE3CAC1576B050AFB8CF1148BD9E5F7B230
{
public:

public:
};


// Obi.ObiPathFrame
struct ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 
{
public:
	// UnityEngine.Vector3 Obi.ObiPathFrame::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_0;
	// UnityEngine.Vector3 Obi.ObiPathFrame::tangent
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___tangent_1;
	// UnityEngine.Vector3 Obi.ObiPathFrame::normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___normal_2;
	// UnityEngine.Vector3 Obi.ObiPathFrame::binormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___binormal_3;
	// UnityEngine.Vector4 Obi.ObiPathFrame::color
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___color_4;
	// System.Single Obi.ObiPathFrame::thickness
	float ___thickness_5;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7, ___position_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_0() const { return ___position_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_tangent_1() { return static_cast<int32_t>(offsetof(ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7, ___tangent_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_tangent_1() const { return ___tangent_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_tangent_1() { return &___tangent_1; }
	inline void set_tangent_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___tangent_1 = value;
	}

	inline static int32_t get_offset_of_normal_2() { return static_cast<int32_t>(offsetof(ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7, ___normal_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_normal_2() const { return ___normal_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_normal_2() { return &___normal_2; }
	inline void set_normal_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___normal_2 = value;
	}

	inline static int32_t get_offset_of_binormal_3() { return static_cast<int32_t>(offsetof(ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7, ___binormal_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_binormal_3() const { return ___binormal_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_binormal_3() { return &___binormal_3; }
	inline void set_binormal_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___binormal_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7, ___color_4)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_color_4() const { return ___color_4; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___color_4 = value;
	}

	inline static int32_t get_offset_of_thickness_5() { return static_cast<int32_t>(offsetof(ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7, ___thickness_5)); }
	inline float get_thickness_5() const { return ___thickness_5; }
	inline float* get_address_of_thickness_5() { return &___thickness_5; }
	inline void set_thickness_5(float value)
	{
		___thickness_5 = value;
	}
};


// Obi.ObiPhaseDataChannel
struct ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670  : public ObiPathDataChannelIdentity_1_tB49FD768113A83C96CCE65F0A01E480360D13F86
{
public:

public:
};


// Obi.ObiRotationalMassDataChannel
struct ObiRotationalMassDataChannel_tC062F8C628B38CBE7F635761D65A2BA348541E4B  : public ObiPathDataChannelIdentity_1_t472CD690CECDE58D7B1E3020921399E15844875C
{
public:

public:
};


// Obi.ObiThicknessDataChannel
struct ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C  : public ObiPathDataChannelIdentity_1_t472CD690CECDE58D7B1E3020921399E15844875C
{
public:

public:
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Unity.Profiling.ProfilerMarker
struct ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 
{
public:
	// System.IntPtr Unity.Profiling.ProfilerMarker::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// UnityEngine.Space
struct Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.MeshVoxelizer/Voxel
struct Voxel_tDC7ED04FB9683A1CAF022400103FC1FD89F6CFE3 
{
public:
	// System.Int32 Obi.MeshVoxelizer/Voxel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Voxel_tDC7ED04FB9683A1CAF022400103FC1FD89F6CFE3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4
struct U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Obi.ObiRopeBlueprint Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::<>4__this
	ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * ___U3CU3E4__this_2;
	// System.Int32 Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::<i>5__1
	int32_t ___U3CiU3E5__1_3;
	// Obi.ObiBendConstraintsBatch Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::<batch>5__2
	ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * ___U3CbatchU3E5__2_4;
	// UnityEngine.Vector3Int Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::<indices>5__3
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___U3CindicesU3E5__3_5;
	// System.Single Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::<restBend>5__4
	float ___U3CrestBendU3E5__4_6;
	// Obi.ObiBendConstraintsBatch Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::<loopClosingBatch>5__5
	ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * ___U3CloopClosingBatchU3E5__5_7;
	// UnityEngine.Vector3Int Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::<indices>5__6
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___U3CindicesU3E5__6_8;
	// Obi.ObiBendConstraintsBatch Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::<loopClosingBatch2>5__7
	ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * ___U3CloopClosingBatch2U3E5__7_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683, ___U3CU3E4__this_2)); }
	inline ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683, ___U3CiU3E5__1_3)); }
	inline int32_t get_U3CiU3E5__1_3() const { return ___U3CiU3E5__1_3; }
	inline int32_t* get_address_of_U3CiU3E5__1_3() { return &___U3CiU3E5__1_3; }
	inline void set_U3CiU3E5__1_3(int32_t value)
	{
		___U3CiU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CbatchU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683, ___U3CbatchU3E5__2_4)); }
	inline ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * get_U3CbatchU3E5__2_4() const { return ___U3CbatchU3E5__2_4; }
	inline ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 ** get_address_of_U3CbatchU3E5__2_4() { return &___U3CbatchU3E5__2_4; }
	inline void set_U3CbatchU3E5__2_4(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * value)
	{
		___U3CbatchU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbatchU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CindicesU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683, ___U3CindicesU3E5__3_5)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_U3CindicesU3E5__3_5() const { return ___U3CindicesU3E5__3_5; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_U3CindicesU3E5__3_5() { return &___U3CindicesU3E5__3_5; }
	inline void set_U3CindicesU3E5__3_5(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___U3CindicesU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CrestBendU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683, ___U3CrestBendU3E5__4_6)); }
	inline float get_U3CrestBendU3E5__4_6() const { return ___U3CrestBendU3E5__4_6; }
	inline float* get_address_of_U3CrestBendU3E5__4_6() { return &___U3CrestBendU3E5__4_6; }
	inline void set_U3CrestBendU3E5__4_6(float value)
	{
		___U3CrestBendU3E5__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CloopClosingBatchU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683, ___U3CloopClosingBatchU3E5__5_7)); }
	inline ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * get_U3CloopClosingBatchU3E5__5_7() const { return ___U3CloopClosingBatchU3E5__5_7; }
	inline ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 ** get_address_of_U3CloopClosingBatchU3E5__5_7() { return &___U3CloopClosingBatchU3E5__5_7; }
	inline void set_U3CloopClosingBatchU3E5__5_7(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * value)
	{
		___U3CloopClosingBatchU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CloopClosingBatchU3E5__5_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CindicesU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683, ___U3CindicesU3E5__6_8)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_U3CindicesU3E5__6_8() const { return ___U3CindicesU3E5__6_8; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_U3CindicesU3E5__6_8() { return &___U3CindicesU3E5__6_8; }
	inline void set_U3CindicesU3E5__6_8(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___U3CindicesU3E5__6_8 = value;
	}

	inline static int32_t get_offset_of_U3CloopClosingBatch2U3E5__7_9() { return static_cast<int32_t>(offsetof(U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683, ___U3CloopClosingBatch2U3E5__7_9)); }
	inline ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * get_U3CloopClosingBatch2U3E5__7_9() const { return ___U3CloopClosingBatch2U3E5__7_9; }
	inline ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 ** get_address_of_U3CloopClosingBatch2U3E5__7_9() { return &___U3CloopClosingBatch2U3E5__7_9; }
	inline void set_U3CloopClosingBatch2U3E5__7_9(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * value)
	{
		___U3CloopClosingBatch2U3E5__7_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CloopClosingBatch2U3E5__7_9), (void*)value);
	}
};


// Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3
struct U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Obi.ObiRopeBlueprint Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::<>4__this
	ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * ___U3CU3E4__this_2;
	// System.Int32 Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::<i>5__1
	int32_t ___U3CiU3E5__1_3;
	// Obi.ObiDistanceConstraintsBatch Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::<batch>5__2
	ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * ___U3CbatchU3E5__2_4;
	// UnityEngine.Vector2Int Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::<indices>5__3
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___U3CindicesU3E5__3_5;
	// Obi.ObiDistanceConstraintsBatch Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::<loopClosingBatch>5__4
	ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * ___U3CloopClosingBatchU3E5__4_6;
	// UnityEngine.Vector2Int Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::<indices>5__5
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___U3CindicesU3E5__5_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32, ___U3CU3E4__this_2)); }
	inline ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32, ___U3CiU3E5__1_3)); }
	inline int32_t get_U3CiU3E5__1_3() const { return ___U3CiU3E5__1_3; }
	inline int32_t* get_address_of_U3CiU3E5__1_3() { return &___U3CiU3E5__1_3; }
	inline void set_U3CiU3E5__1_3(int32_t value)
	{
		___U3CiU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CbatchU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32, ___U3CbatchU3E5__2_4)); }
	inline ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * get_U3CbatchU3E5__2_4() const { return ___U3CbatchU3E5__2_4; }
	inline ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE ** get_address_of_U3CbatchU3E5__2_4() { return &___U3CbatchU3E5__2_4; }
	inline void set_U3CbatchU3E5__2_4(ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * value)
	{
		___U3CbatchU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbatchU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CindicesU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32, ___U3CindicesU3E5__3_5)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_U3CindicesU3E5__3_5() const { return ___U3CindicesU3E5__3_5; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_U3CindicesU3E5__3_5() { return &___U3CindicesU3E5__3_5; }
	inline void set_U3CindicesU3E5__3_5(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___U3CindicesU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CloopClosingBatchU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32, ___U3CloopClosingBatchU3E5__4_6)); }
	inline ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * get_U3CloopClosingBatchU3E5__4_6() const { return ___U3CloopClosingBatchU3E5__4_6; }
	inline ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE ** get_address_of_U3CloopClosingBatchU3E5__4_6() { return &___U3CloopClosingBatchU3E5__4_6; }
	inline void set_U3CloopClosingBatchU3E5__4_6(ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * value)
	{
		___U3CloopClosingBatchU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CloopClosingBatchU3E5__4_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CindicesU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32, ___U3CindicesU3E5__5_7)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_U3CindicesU3E5__5_7() const { return ___U3CindicesU3E5__5_7; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_U3CindicesU3E5__5_7() { return &___U3CindicesU3E5__5_7; }
	inline void set_U3CindicesU3E5__5_7(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___U3CindicesU3E5__5_7 = value;
	}
};


// Obi.ObiSolver/BackendType
struct BackendType_tAAE42F5C8A5562F98A883CED31D3FACFDA560A82 
{
public:
	// System.Int32 Obi.ObiSolver/BackendType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BackendType_tAAE42F5C8A5562F98A883CED31D3FACFDA560A82, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.ObiUtils/ParticleFlags
struct ParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F 
{
public:
	// System.Int32 Obi.ObiUtils/ParticleFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.ObiWingedPoint/TangentMode
struct TangentMode_t0BA421F50B5F67C29E2B60117562585752098CEE 
{
public:
	// System.Int32 Obi.ObiWingedPoint/TangentMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TangentMode_t0BA421F50B5F67C29E2B60117562585752098CEE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni/ConstraintType
struct ConstraintType_t9E2A1C0D5B41982E641515858185BA43DEE0F5CC 
{
public:
	// System.Int32 Oni/ConstraintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstraintType_t9E2A1C0D5B41982E641515858185BA43DEE0F5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni/Contact
struct Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756 
{
public:
	union
	{
		struct
		{
			// UnityEngine.Vector4 Oni/Contact::pointA
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___pointA_0;
			// UnityEngine.Vector4 Oni/Contact::pointB
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___pointB_1;
			// UnityEngine.Vector4 Oni/Contact::normal
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___normal_2;
			// UnityEngine.Vector4 Oni/Contact::tangent
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___tangent_3;
			// UnityEngine.Vector4 Oni/Contact::bitangent
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___bitangent_4;
			// System.Single Oni/Contact::distance
			float ___distance_5;
			// System.Single Oni/Contact::normalImpulse
			float ___normalImpulse_6;
			// System.Single Oni/Contact::tangentImpulse
			float ___tangentImpulse_7;
			// System.Single Oni/Contact::bitangentImpulse
			float ___bitangentImpulse_8;
			// System.Single Oni/Contact::stickImpulse
			float ___stickImpulse_9;
			// System.Single Oni/Contact::rollingFrictionImpulse
			float ___rollingFrictionImpulse_10;
			// System.Int32 Oni/Contact::bodyA
			int32_t ___bodyA_11;
			// System.Int32 Oni/Contact::bodyB
			int32_t ___bodyB_12;
		};
		uint8_t Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756__padding[144];
	};

public:
	inline static int32_t get_offset_of_pointA_0() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___pointA_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_pointA_0() const { return ___pointA_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_pointA_0() { return &___pointA_0; }
	inline void set_pointA_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___pointA_0 = value;
	}

	inline static int32_t get_offset_of_pointB_1() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___pointB_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_pointB_1() const { return ___pointB_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_pointB_1() { return &___pointB_1; }
	inline void set_pointB_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___pointB_1 = value;
	}

	inline static int32_t get_offset_of_normal_2() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___normal_2)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_normal_2() const { return ___normal_2; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_normal_2() { return &___normal_2; }
	inline void set_normal_2(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___normal_2 = value;
	}

	inline static int32_t get_offset_of_tangent_3() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___tangent_3)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_tangent_3() const { return ___tangent_3; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_tangent_3() { return &___tangent_3; }
	inline void set_tangent_3(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___tangent_3 = value;
	}

	inline static int32_t get_offset_of_bitangent_4() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bitangent_4)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_bitangent_4() const { return ___bitangent_4; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_bitangent_4() { return &___bitangent_4; }
	inline void set_bitangent_4(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___bitangent_4 = value;
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_normalImpulse_6() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___normalImpulse_6)); }
	inline float get_normalImpulse_6() const { return ___normalImpulse_6; }
	inline float* get_address_of_normalImpulse_6() { return &___normalImpulse_6; }
	inline void set_normalImpulse_6(float value)
	{
		___normalImpulse_6 = value;
	}

	inline static int32_t get_offset_of_tangentImpulse_7() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___tangentImpulse_7)); }
	inline float get_tangentImpulse_7() const { return ___tangentImpulse_7; }
	inline float* get_address_of_tangentImpulse_7() { return &___tangentImpulse_7; }
	inline void set_tangentImpulse_7(float value)
	{
		___tangentImpulse_7 = value;
	}

	inline static int32_t get_offset_of_bitangentImpulse_8() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bitangentImpulse_8)); }
	inline float get_bitangentImpulse_8() const { return ___bitangentImpulse_8; }
	inline float* get_address_of_bitangentImpulse_8() { return &___bitangentImpulse_8; }
	inline void set_bitangentImpulse_8(float value)
	{
		___bitangentImpulse_8 = value;
	}

	inline static int32_t get_offset_of_stickImpulse_9() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___stickImpulse_9)); }
	inline float get_stickImpulse_9() const { return ___stickImpulse_9; }
	inline float* get_address_of_stickImpulse_9() { return &___stickImpulse_9; }
	inline void set_stickImpulse_9(float value)
	{
		___stickImpulse_9 = value;
	}

	inline static int32_t get_offset_of_rollingFrictionImpulse_10() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___rollingFrictionImpulse_10)); }
	inline float get_rollingFrictionImpulse_10() const { return ___rollingFrictionImpulse_10; }
	inline float* get_address_of_rollingFrictionImpulse_10() { return &___rollingFrictionImpulse_10; }
	inline void set_rollingFrictionImpulse_10(float value)
	{
		___rollingFrictionImpulse_10 = value;
	}

	inline static int32_t get_offset_of_bodyA_11() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bodyA_11)); }
	inline int32_t get_bodyA_11() const { return ___bodyA_11; }
	inline int32_t* get_address_of_bodyA_11() { return &___bodyA_11; }
	inline void set_bodyA_11(int32_t value)
	{
		___bodyA_11 = value;
	}

	inline static int32_t get_offset_of_bodyB_12() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bodyB_12)); }
	inline int32_t get_bodyB_12() const { return ___bodyB_12; }
	inline int32_t* get_address_of_bodyB_12() { return &___bodyB_12; }
	inline void set_bodyB_12(int32_t value)
	{
		___bodyB_12 = value;
	}
};


// Oni/GridCell
struct GridCell_t909A29E34A16B3EA378BA3BE26180874A91F5D70 
{
public:
	// UnityEngine.Vector3 Oni/GridCell::center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___center_0;
	// UnityEngine.Vector3 Oni/GridCell::size
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___size_1;
	// System.Int32 Oni/GridCell::count
	int32_t ___count_2;

public:
	inline static int32_t get_offset_of_center_0() { return static_cast<int32_t>(offsetof(GridCell_t909A29E34A16B3EA378BA3BE26180874A91F5D70, ___center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_center_0() const { return ___center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_center_0() { return &___center_0; }
	inline void set_center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___center_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(GridCell_t909A29E34A16B3EA378BA3BE26180874A91F5D70, ___size_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_size_1() const { return ___size_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(GridCell_t909A29E34A16B3EA378BA3BE26180874A91F5D70, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}
};


// Oni/MaterialCombineMode
struct MaterialCombineMode_t6D9889E6299865FDA792A7E3A202B4FCC7E347CD 
{
public:
	// System.Int32 Oni/MaterialCombineMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaterialCombineMode_t6D9889E6299865FDA792A7E3A202B4FCC7E347CD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni/ProfileMask
struct ProfileMask_tA62CF5C1CE62FE5BA04842425ABD12094B674555 
{
public:
	// System.UInt32 Oni/ProfileMask::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProfileMask_tA62CF5C1CE62FE5BA04842425ABD12094B674555, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};


// Oni/ShapeType
struct ShapeType_tA2E1C053BF689089A60C921625DA69D7FE71AFE5 
{
public:
	// System.Int32 Oni/ShapeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShapeType_tA2E1C053BF689089A60C921625DA69D7FE71AFE5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.QueryShape/QueryType
struct QueryType_tB7ABA496561EDA432C6A12A5DDB8E54184413CD2 
{
public:
	// System.Int32 Obi.QueryShape/QueryType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueryType_tB7ABA496561EDA432C6A12A5DDB8E54184413CD2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.VoxelPathFinder/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E  : public RuntimeObject
{
public:
	// UnityEngine.Vector3Int Obi.VoxelPathFinder/<>c__DisplayClass7_0::end
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___end_0;
	// Obi.VoxelPathFinder Obi.VoxelPathFinder/<>c__DisplayClass7_0::<>4__this
	VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_end_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E, ___end_0)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_end_0() const { return ___end_0; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_end_0() { return &___end_0; }
	inline void set_end_0(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___end_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E, ___U3CU3E4__this_1)); }
	inline VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// Obi.VoxelPathFinder/TargetVoxel
struct TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 
{
public:
	// UnityEngine.Vector3Int Obi.VoxelPathFinder/TargetVoxel::coordinates
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___coordinates_0;
	// System.Single Obi.VoxelPathFinder/TargetVoxel::distance
	float ___distance_1;
	// System.Single Obi.VoxelPathFinder/TargetVoxel::heuristic
	float ___heuristic_2;

public:
	inline static int32_t get_offset_of_coordinates_0() { return static_cast<int32_t>(offsetof(TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564, ___coordinates_0)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_coordinates_0() const { return ___coordinates_0; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_coordinates_0() { return &___coordinates_0; }
	inline void set_coordinates_0(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___coordinates_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564, ___distance_1)); }
	inline float get_distance_1() const { return ___distance_1; }
	inline float* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(float value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_heuristic_2() { return static_cast<int32_t>(offsetof(TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564, ___heuristic_2)); }
	inline float get_heuristic_2() const { return ___heuristic_2; }
	inline float* get_address_of_heuristic_2() { return &___heuristic_2; }
	inline void set_heuristic_2(float value)
	{
		___heuristic_2 = value;
	}
};


// Oni/ConstraintParameters/EvaluationOrder
struct EvaluationOrder_tD7DE4467E3DE5F4E5FFC0117442FCA2E42BB8C59 
{
public:
	// System.Int32 Oni/ConstraintParameters/EvaluationOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EvaluationOrder_tD7DE4467E3DE5F4E5FFC0117442FCA2E42BB8C59, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni/SolverParameters/Interpolation
struct Interpolation_tC0EB8CECD4C0862D9C195BC65FC4B2220998E35F 
{
public:
	// System.Int32 Oni/SolverParameters/Interpolation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Interpolation_tC0EB8CECD4C0862D9C195BC65FC4B2220998E35F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni/SolverParameters/Mode
struct Mode_t9545552DF2C676020D2CAEFB2B3E7E3C22ABF0EC 
{
public:
	// System.Int32 Oni/SolverParameters/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t9545552DF2C676020D2CAEFB2B3E7E3C22ABF0EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// Obi.Triangle
struct Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 
{
public:
	// System.Int32 Obi.Triangle::i1
	int32_t ___i1_0;
	// System.Int32 Obi.Triangle::i2
	int32_t ___i2_1;
	// System.Int32 Obi.Triangle::i3
	int32_t ___i3_2;
	// Obi.Aabb Obi.Triangle::b
	Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  ___b_3;

public:
	inline static int32_t get_offset_of_i1_0() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___i1_0)); }
	inline int32_t get_i1_0() const { return ___i1_0; }
	inline int32_t* get_address_of_i1_0() { return &___i1_0; }
	inline void set_i1_0(int32_t value)
	{
		___i1_0 = value;
	}

	inline static int32_t get_offset_of_i2_1() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___i2_1)); }
	inline int32_t get_i2_1() const { return ___i2_1; }
	inline int32_t* get_address_of_i2_1() { return &___i2_1; }
	inline void set_i2_1(int32_t value)
	{
		___i2_1 = value;
	}

	inline static int32_t get_offset_of_i3_2() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___i3_2)); }
	inline int32_t get_i3_2() const { return ___i3_2; }
	inline int32_t* get_address_of_i3_2() { return &___i3_2; }
	inline void set_i3_2(int32_t value)
	{
		___i3_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___b_3)); }
	inline Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  get_b_3() const { return ___b_3; }
	inline Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF * get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  value)
	{
		___b_3 = value;
	}
};


// Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4
struct U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::particleNormals
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___particleNormals_2;
	// Obi.ObiRodBlueprint Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<>4__this
	ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * ___U3CU3E4__this_3;
	// Obi.ObiPathFrame Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<frame>5__1
	ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7  ___U3CframeU3E5__1_4;
	// System.Int32 Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<i>5__2
	int32_t ___U3CiU3E5__2_5;
	// Obi.ObiStretchShearConstraintsBatch Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<batch>5__3
	ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * ___U3CbatchU3E5__3_6;
	// UnityEngine.Vector2Int Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<indices>5__4
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___U3CindicesU3E5__4_7;
	// UnityEngine.Vector3 Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<d>5__5
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CdU3E5__5_8;
	// Obi.ObiStretchShearConstraintsBatch Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<loopClosingBatch>5__6
	ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * ___U3CloopClosingBatchU3E5__6_9;
	// UnityEngine.Vector2Int Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<indices>5__7
	Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___U3CindicesU3E5__7_10;
	// UnityEngine.Vector3 Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<d>5__8
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CdU3E5__8_11;
	// System.Single[] Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<>s__9
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___U3CU3Es__9_12;
	// System.Int32 Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<>s__10
	int32_t ___U3CU3Es__10_13;
	// System.Single Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::<length>5__11
	float ___U3ClengthU3E5__11_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_particleNormals_2() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___particleNormals_2)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_particleNormals_2() const { return ___particleNormals_2; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_particleNormals_2() { return &___particleNormals_2; }
	inline void set_particleNormals_2(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___particleNormals_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particleNormals_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CU3E4__this_3)); }
	inline ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CframeU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CframeU3E5__1_4)); }
	inline ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7  get_U3CframeU3E5__1_4() const { return ___U3CframeU3E5__1_4; }
	inline ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 * get_address_of_U3CframeU3E5__1_4() { return &___U3CframeU3E5__1_4; }
	inline void set_U3CframeU3E5__1_4(ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7  value)
	{
		___U3CframeU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CiU3E5__2_5)); }
	inline int32_t get_U3CiU3E5__2_5() const { return ___U3CiU3E5__2_5; }
	inline int32_t* get_address_of_U3CiU3E5__2_5() { return &___U3CiU3E5__2_5; }
	inline void set_U3CiU3E5__2_5(int32_t value)
	{
		___U3CiU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CbatchU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CbatchU3E5__3_6)); }
	inline ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * get_U3CbatchU3E5__3_6() const { return ___U3CbatchU3E5__3_6; }
	inline ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B ** get_address_of_U3CbatchU3E5__3_6() { return &___U3CbatchU3E5__3_6; }
	inline void set_U3CbatchU3E5__3_6(ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * value)
	{
		___U3CbatchU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbatchU3E5__3_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CindicesU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CindicesU3E5__4_7)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_U3CindicesU3E5__4_7() const { return ___U3CindicesU3E5__4_7; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_U3CindicesU3E5__4_7() { return &___U3CindicesU3E5__4_7; }
	inline void set_U3CindicesU3E5__4_7(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___U3CindicesU3E5__4_7 = value;
	}

	inline static int32_t get_offset_of_U3CdU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CdU3E5__5_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CdU3E5__5_8() const { return ___U3CdU3E5__5_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CdU3E5__5_8() { return &___U3CdU3E5__5_8; }
	inline void set_U3CdU3E5__5_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CdU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CloopClosingBatchU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CloopClosingBatchU3E5__6_9)); }
	inline ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * get_U3CloopClosingBatchU3E5__6_9() const { return ___U3CloopClosingBatchU3E5__6_9; }
	inline ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B ** get_address_of_U3CloopClosingBatchU3E5__6_9() { return &___U3CloopClosingBatchU3E5__6_9; }
	inline void set_U3CloopClosingBatchU3E5__6_9(ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * value)
	{
		___U3CloopClosingBatchU3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CloopClosingBatchU3E5__6_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CindicesU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CindicesU3E5__7_10)); }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  get_U3CindicesU3E5__7_10() const { return ___U3CindicesU3E5__7_10; }
	inline Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * get_address_of_U3CindicesU3E5__7_10() { return &___U3CindicesU3E5__7_10; }
	inline void set_U3CindicesU3E5__7_10(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  value)
	{
		___U3CindicesU3E5__7_10 = value;
	}

	inline static int32_t get_offset_of_U3CdU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CdU3E5__8_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CdU3E5__8_11() const { return ___U3CdU3E5__8_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CdU3E5__8_11() { return &___U3CdU3E5__8_11; }
	inline void set_U3CdU3E5__8_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CdU3E5__8_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3Es__9_12() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CU3Es__9_12)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_U3CU3Es__9_12() const { return ___U3CU3Es__9_12; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_U3CU3Es__9_12() { return &___U3CU3Es__9_12; }
	inline void set_U3CU3Es__9_12(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___U3CU3Es__9_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Es__9_12), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Es__10_13() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3CU3Es__10_13)); }
	inline int32_t get_U3CU3Es__10_13() const { return ___U3CU3Es__10_13; }
	inline int32_t* get_address_of_U3CU3Es__10_13() { return &___U3CU3Es__10_13; }
	inline void set_U3CU3Es__10_13(int32_t value)
	{
		___U3CU3Es__10_13 = value;
	}

	inline static int32_t get_offset_of_U3ClengthU3E5__11_14() { return static_cast<int32_t>(offsetof(U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46, ___U3ClengthU3E5__11_14)); }
	inline float get_U3ClengthU3E5__11_14() const { return ___U3ClengthU3E5__11_14; }
	inline float* get_address_of_U3ClengthU3E5__11_14() { return &___U3ClengthU3E5__11_14; }
	inline void set_U3ClengthU3E5__11_14(float value)
	{
		___U3ClengthU3E5__11_14 = value;
	}
};


// Oni/ConstraintParameters
struct ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 
{
public:
	// Oni/ConstraintParameters/EvaluationOrder Oni/ConstraintParameters::evaluationOrder
	int32_t ___evaluationOrder_0;
	// System.Int32 Oni/ConstraintParameters::iterations
	int32_t ___iterations_1;
	// System.Single Oni/ConstraintParameters::SORFactor
	float ___SORFactor_2;
	// System.Boolean Oni/ConstraintParameters::enabled
	bool ___enabled_3;

public:
	inline static int32_t get_offset_of_evaluationOrder_0() { return static_cast<int32_t>(offsetof(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22, ___evaluationOrder_0)); }
	inline int32_t get_evaluationOrder_0() const { return ___evaluationOrder_0; }
	inline int32_t* get_address_of_evaluationOrder_0() { return &___evaluationOrder_0; }
	inline void set_evaluationOrder_0(int32_t value)
	{
		___evaluationOrder_0 = value;
	}

	inline static int32_t get_offset_of_iterations_1() { return static_cast<int32_t>(offsetof(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22, ___iterations_1)); }
	inline int32_t get_iterations_1() const { return ___iterations_1; }
	inline int32_t* get_address_of_iterations_1() { return &___iterations_1; }
	inline void set_iterations_1(int32_t value)
	{
		___iterations_1 = value;
	}

	inline static int32_t get_offset_of_SORFactor_2() { return static_cast<int32_t>(offsetof(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22, ___SORFactor_2)); }
	inline float get_SORFactor_2() const { return ___SORFactor_2; }
	inline float* get_address_of_SORFactor_2() { return &___SORFactor_2; }
	inline void set_SORFactor_2(float value)
	{
		___SORFactor_2 = value;
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}
};


// Oni/SolverParameters
struct SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 
{
public:
	// Oni/SolverParameters/Mode Oni/SolverParameters::mode
	int32_t ___mode_0;
	// Oni/SolverParameters/Interpolation Oni/SolverParameters::interpolation
	int32_t ___interpolation_1;
	// UnityEngine.Vector3 Oni/SolverParameters::gravity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___gravity_2;
	// System.Single Oni/SolverParameters::damping
	float ___damping_3;
	// System.Single Oni/SolverParameters::maxAnisotropy
	float ___maxAnisotropy_4;
	// System.Single Oni/SolverParameters::sleepThreshold
	float ___sleepThreshold_5;
	// System.Single Oni/SolverParameters::collisionMargin
	float ___collisionMargin_6;
	// System.Single Oni/SolverParameters::maxDepenetration
	float ___maxDepenetration_7;
	// System.Single Oni/SolverParameters::continuousCollisionDetection
	float ___continuousCollisionDetection_8;
	// System.Single Oni/SolverParameters::shockPropagation
	float ___shockPropagation_9;
	// System.Int32 Oni/SolverParameters::surfaceCollisionIterations
	int32_t ___surfaceCollisionIterations_10;
	// System.Single Oni/SolverParameters::surfaceCollisionTolerance
	float ___surfaceCollisionTolerance_11;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_interpolation_1() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___interpolation_1)); }
	inline int32_t get_interpolation_1() const { return ___interpolation_1; }
	inline int32_t* get_address_of_interpolation_1() { return &___interpolation_1; }
	inline void set_interpolation_1(int32_t value)
	{
		___interpolation_1 = value;
	}

	inline static int32_t get_offset_of_gravity_2() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___gravity_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_gravity_2() const { return ___gravity_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_gravity_2() { return &___gravity_2; }
	inline void set_gravity_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___gravity_2 = value;
	}

	inline static int32_t get_offset_of_damping_3() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___damping_3)); }
	inline float get_damping_3() const { return ___damping_3; }
	inline float* get_address_of_damping_3() { return &___damping_3; }
	inline void set_damping_3(float value)
	{
		___damping_3 = value;
	}

	inline static int32_t get_offset_of_maxAnisotropy_4() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___maxAnisotropy_4)); }
	inline float get_maxAnisotropy_4() const { return ___maxAnisotropy_4; }
	inline float* get_address_of_maxAnisotropy_4() { return &___maxAnisotropy_4; }
	inline void set_maxAnisotropy_4(float value)
	{
		___maxAnisotropy_4 = value;
	}

	inline static int32_t get_offset_of_sleepThreshold_5() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___sleepThreshold_5)); }
	inline float get_sleepThreshold_5() const { return ___sleepThreshold_5; }
	inline float* get_address_of_sleepThreshold_5() { return &___sleepThreshold_5; }
	inline void set_sleepThreshold_5(float value)
	{
		___sleepThreshold_5 = value;
	}

	inline static int32_t get_offset_of_collisionMargin_6() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___collisionMargin_6)); }
	inline float get_collisionMargin_6() const { return ___collisionMargin_6; }
	inline float* get_address_of_collisionMargin_6() { return &___collisionMargin_6; }
	inline void set_collisionMargin_6(float value)
	{
		___collisionMargin_6 = value;
	}

	inline static int32_t get_offset_of_maxDepenetration_7() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___maxDepenetration_7)); }
	inline float get_maxDepenetration_7() const { return ___maxDepenetration_7; }
	inline float* get_address_of_maxDepenetration_7() { return &___maxDepenetration_7; }
	inline void set_maxDepenetration_7(float value)
	{
		___maxDepenetration_7 = value;
	}

	inline static int32_t get_offset_of_continuousCollisionDetection_8() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___continuousCollisionDetection_8)); }
	inline float get_continuousCollisionDetection_8() const { return ___continuousCollisionDetection_8; }
	inline float* get_address_of_continuousCollisionDetection_8() { return &___continuousCollisionDetection_8; }
	inline void set_continuousCollisionDetection_8(float value)
	{
		___continuousCollisionDetection_8 = value;
	}

	inline static int32_t get_offset_of_shockPropagation_9() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___shockPropagation_9)); }
	inline float get_shockPropagation_9() const { return ___shockPropagation_9; }
	inline float* get_address_of_shockPropagation_9() { return &___shockPropagation_9; }
	inline void set_shockPropagation_9(float value)
	{
		___shockPropagation_9 = value;
	}

	inline static int32_t get_offset_of_surfaceCollisionIterations_10() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___surfaceCollisionIterations_10)); }
	inline int32_t get_surfaceCollisionIterations_10() const { return ___surfaceCollisionIterations_10; }
	inline int32_t* get_address_of_surfaceCollisionIterations_10() { return &___surfaceCollisionIterations_10; }
	inline void set_surfaceCollisionIterations_10(int32_t value)
	{
		___surfaceCollisionIterations_10 = value;
	}

	inline static int32_t get_offset_of_surfaceCollisionTolerance_11() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___surfaceCollisionTolerance_11)); }
	inline float get_surfaceCollisionTolerance_11() const { return ___surfaceCollisionTolerance_11; }
	inline float* get_address_of_surfaceCollisionTolerance_11() { return &___surfaceCollisionTolerance_11; }
	inline void set_surfaceCollisionTolerance_11(float value)
	{
		___surfaceCollisionTolerance_11 = value;
	}
};


// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// Obi.ObiActorBlueprint
struct ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// Obi.ObiActorBlueprint/BlueprintCallback Obi.ObiActorBlueprint::OnBlueprintGenerate
	BlueprintCallback_t6040FFC664749D28E3A88E72308B277F69B9D3A6 * ___OnBlueprintGenerate_4;
	// System.Boolean Obi.ObiActorBlueprint::m_Empty
	bool ___m_Empty_5;
	// System.Int32 Obi.ObiActorBlueprint::m_ActiveParticleCount
	int32_t ___m_ActiveParticleCount_6;
	// System.Int32 Obi.ObiActorBlueprint::m_InitialActiveParticleCount
	int32_t ___m_InitialActiveParticleCount_7;
	// UnityEngine.Bounds Obi.ObiActorBlueprint::_bounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ____bounds_8;
	// UnityEngine.Vector3[] Obi.ObiActorBlueprint::positions
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___positions_9;
	// UnityEngine.Vector4[] Obi.ObiActorBlueprint::restPositions
	Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ___restPositions_10;
	// UnityEngine.Quaternion[] Obi.ObiActorBlueprint::orientations
	QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* ___orientations_11;
	// UnityEngine.Quaternion[] Obi.ObiActorBlueprint::restOrientations
	QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* ___restOrientations_12;
	// UnityEngine.Vector3[] Obi.ObiActorBlueprint::velocities
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___velocities_13;
	// UnityEngine.Vector3[] Obi.ObiActorBlueprint::angularVelocities
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___angularVelocities_14;
	// System.Single[] Obi.ObiActorBlueprint::invMasses
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___invMasses_15;
	// System.Single[] Obi.ObiActorBlueprint::invRotationalMasses
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___invRotationalMasses_16;
	// System.Int32[] Obi.ObiActorBlueprint::filters
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___filters_17;
	// UnityEngine.Vector3[] Obi.ObiActorBlueprint::principalRadii
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___principalRadii_18;
	// UnityEngine.Color[] Obi.ObiActorBlueprint::colors
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ___colors_19;
	// System.Int32[] Obi.ObiActorBlueprint::points
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___points_20;
	// System.Int32[] Obi.ObiActorBlueprint::edges
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___edges_21;
	// System.Int32[] Obi.ObiActorBlueprint::triangles
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___triangles_22;
	// Obi.ObiDistanceConstraintsData Obi.ObiActorBlueprint::distanceConstraintsData
	ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * ___distanceConstraintsData_23;
	// Obi.ObiBendConstraintsData Obi.ObiActorBlueprint::bendConstraintsData
	ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * ___bendConstraintsData_24;
	// Obi.ObiSkinConstraintsData Obi.ObiActorBlueprint::skinConstraintsData
	ObiSkinConstraintsData_t7F610D65CCE6B7699A70337524422428212F7353 * ___skinConstraintsData_25;
	// Obi.ObiTetherConstraintsData Obi.ObiActorBlueprint::tetherConstraintsData
	ObiTetherConstraintsData_t071A4A85316CD0F23DA05223C15E3EE1B28626CE * ___tetherConstraintsData_26;
	// Obi.ObiStretchShearConstraintsData Obi.ObiActorBlueprint::stretchShearConstraintsData
	ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * ___stretchShearConstraintsData_27;
	// Obi.ObiBendTwistConstraintsData Obi.ObiActorBlueprint::bendTwistConstraintsData
	ObiBendTwistConstraintsData_t681D2A3964FDCF4E12E05CED3F7C62E640440CEE * ___bendTwistConstraintsData_28;
	// Obi.ObiShapeMatchingConstraintsData Obi.ObiActorBlueprint::shapeMatchingConstraintsData
	ObiShapeMatchingConstraintsData_t5CC282CC712A59B303FB502754F122D4C14801C8 * ___shapeMatchingConstraintsData_29;
	// Obi.ObiAerodynamicConstraintsData Obi.ObiActorBlueprint::aerodynamicConstraintsData
	ObiAerodynamicConstraintsData_t0C7593E41FF1ECD984BD4D9FF8C84C96DDBA5563 * ___aerodynamicConstraintsData_30;
	// Obi.ObiChainConstraintsData Obi.ObiActorBlueprint::chainConstraintsData
	ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 * ___chainConstraintsData_31;
	// Obi.ObiVolumeConstraintsData Obi.ObiActorBlueprint::volumeConstraintsData
	ObiVolumeConstraintsData_t12899715EEA78BB09F3BAC3CCC6EB012AF0E4D26 * ___volumeConstraintsData_32;
	// System.Collections.Generic.List`1<Obi.ObiParticleGroup> Obi.ObiActorBlueprint::groups
	List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * ___groups_33;

public:
	inline static int32_t get_offset_of_OnBlueprintGenerate_4() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___OnBlueprintGenerate_4)); }
	inline BlueprintCallback_t6040FFC664749D28E3A88E72308B277F69B9D3A6 * get_OnBlueprintGenerate_4() const { return ___OnBlueprintGenerate_4; }
	inline BlueprintCallback_t6040FFC664749D28E3A88E72308B277F69B9D3A6 ** get_address_of_OnBlueprintGenerate_4() { return &___OnBlueprintGenerate_4; }
	inline void set_OnBlueprintGenerate_4(BlueprintCallback_t6040FFC664749D28E3A88E72308B277F69B9D3A6 * value)
	{
		___OnBlueprintGenerate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnBlueprintGenerate_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Empty_5() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___m_Empty_5)); }
	inline bool get_m_Empty_5() const { return ___m_Empty_5; }
	inline bool* get_address_of_m_Empty_5() { return &___m_Empty_5; }
	inline void set_m_Empty_5(bool value)
	{
		___m_Empty_5 = value;
	}

	inline static int32_t get_offset_of_m_ActiveParticleCount_6() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___m_ActiveParticleCount_6)); }
	inline int32_t get_m_ActiveParticleCount_6() const { return ___m_ActiveParticleCount_6; }
	inline int32_t* get_address_of_m_ActiveParticleCount_6() { return &___m_ActiveParticleCount_6; }
	inline void set_m_ActiveParticleCount_6(int32_t value)
	{
		___m_ActiveParticleCount_6 = value;
	}

	inline static int32_t get_offset_of_m_InitialActiveParticleCount_7() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___m_InitialActiveParticleCount_7)); }
	inline int32_t get_m_InitialActiveParticleCount_7() const { return ___m_InitialActiveParticleCount_7; }
	inline int32_t* get_address_of_m_InitialActiveParticleCount_7() { return &___m_InitialActiveParticleCount_7; }
	inline void set_m_InitialActiveParticleCount_7(int32_t value)
	{
		___m_InitialActiveParticleCount_7 = value;
	}

	inline static int32_t get_offset_of__bounds_8() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ____bounds_8)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get__bounds_8() const { return ____bounds_8; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of__bounds_8() { return &____bounds_8; }
	inline void set__bounds_8(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		____bounds_8 = value;
	}

	inline static int32_t get_offset_of_positions_9() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___positions_9)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_positions_9() const { return ___positions_9; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_positions_9() { return &___positions_9; }
	inline void set_positions_9(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___positions_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___positions_9), (void*)value);
	}

	inline static int32_t get_offset_of_restPositions_10() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___restPositions_10)); }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* get_restPositions_10() const { return ___restPositions_10; }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871** get_address_of_restPositions_10() { return &___restPositions_10; }
	inline void set_restPositions_10(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* value)
	{
		___restPositions_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restPositions_10), (void*)value);
	}

	inline static int32_t get_offset_of_orientations_11() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___orientations_11)); }
	inline QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* get_orientations_11() const { return ___orientations_11; }
	inline QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6** get_address_of_orientations_11() { return &___orientations_11; }
	inline void set_orientations_11(QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* value)
	{
		___orientations_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___orientations_11), (void*)value);
	}

	inline static int32_t get_offset_of_restOrientations_12() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___restOrientations_12)); }
	inline QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* get_restOrientations_12() const { return ___restOrientations_12; }
	inline QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6** get_address_of_restOrientations_12() { return &___restOrientations_12; }
	inline void set_restOrientations_12(QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* value)
	{
		___restOrientations_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restOrientations_12), (void*)value);
	}

	inline static int32_t get_offset_of_velocities_13() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___velocities_13)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_velocities_13() const { return ___velocities_13; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_velocities_13() { return &___velocities_13; }
	inline void set_velocities_13(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___velocities_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___velocities_13), (void*)value);
	}

	inline static int32_t get_offset_of_angularVelocities_14() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___angularVelocities_14)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_angularVelocities_14() const { return ___angularVelocities_14; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_angularVelocities_14() { return &___angularVelocities_14; }
	inline void set_angularVelocities_14(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___angularVelocities_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___angularVelocities_14), (void*)value);
	}

	inline static int32_t get_offset_of_invMasses_15() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___invMasses_15)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_invMasses_15() const { return ___invMasses_15; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_invMasses_15() { return &___invMasses_15; }
	inline void set_invMasses_15(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___invMasses_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invMasses_15), (void*)value);
	}

	inline static int32_t get_offset_of_invRotationalMasses_16() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___invRotationalMasses_16)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_invRotationalMasses_16() const { return ___invRotationalMasses_16; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_invRotationalMasses_16() { return &___invRotationalMasses_16; }
	inline void set_invRotationalMasses_16(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___invRotationalMasses_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invRotationalMasses_16), (void*)value);
	}

	inline static int32_t get_offset_of_filters_17() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___filters_17)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_filters_17() const { return ___filters_17; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_filters_17() { return &___filters_17; }
	inline void set_filters_17(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___filters_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filters_17), (void*)value);
	}

	inline static int32_t get_offset_of_principalRadii_18() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___principalRadii_18)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_principalRadii_18() const { return ___principalRadii_18; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_principalRadii_18() { return &___principalRadii_18; }
	inline void set_principalRadii_18(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___principalRadii_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___principalRadii_18), (void*)value);
	}

	inline static int32_t get_offset_of_colors_19() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___colors_19)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get_colors_19() const { return ___colors_19; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of_colors_19() { return &___colors_19; }
	inline void set_colors_19(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		___colors_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colors_19), (void*)value);
	}

	inline static int32_t get_offset_of_points_20() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___points_20)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_points_20() const { return ___points_20; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_points_20() { return &___points_20; }
	inline void set_points_20(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___points_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___points_20), (void*)value);
	}

	inline static int32_t get_offset_of_edges_21() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___edges_21)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_edges_21() const { return ___edges_21; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_edges_21() { return &___edges_21; }
	inline void set_edges_21(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___edges_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___edges_21), (void*)value);
	}

	inline static int32_t get_offset_of_triangles_22() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___triangles_22)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_triangles_22() const { return ___triangles_22; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_triangles_22() { return &___triangles_22; }
	inline void set_triangles_22(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___triangles_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___triangles_22), (void*)value);
	}

	inline static int32_t get_offset_of_distanceConstraintsData_23() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___distanceConstraintsData_23)); }
	inline ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * get_distanceConstraintsData_23() const { return ___distanceConstraintsData_23; }
	inline ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB ** get_address_of_distanceConstraintsData_23() { return &___distanceConstraintsData_23; }
	inline void set_distanceConstraintsData_23(ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * value)
	{
		___distanceConstraintsData_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___distanceConstraintsData_23), (void*)value);
	}

	inline static int32_t get_offset_of_bendConstraintsData_24() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___bendConstraintsData_24)); }
	inline ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * get_bendConstraintsData_24() const { return ___bendConstraintsData_24; }
	inline ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED ** get_address_of_bendConstraintsData_24() { return &___bendConstraintsData_24; }
	inline void set_bendConstraintsData_24(ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * value)
	{
		___bendConstraintsData_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bendConstraintsData_24), (void*)value);
	}

	inline static int32_t get_offset_of_skinConstraintsData_25() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___skinConstraintsData_25)); }
	inline ObiSkinConstraintsData_t7F610D65CCE6B7699A70337524422428212F7353 * get_skinConstraintsData_25() const { return ___skinConstraintsData_25; }
	inline ObiSkinConstraintsData_t7F610D65CCE6B7699A70337524422428212F7353 ** get_address_of_skinConstraintsData_25() { return &___skinConstraintsData_25; }
	inline void set_skinConstraintsData_25(ObiSkinConstraintsData_t7F610D65CCE6B7699A70337524422428212F7353 * value)
	{
		___skinConstraintsData_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skinConstraintsData_25), (void*)value);
	}

	inline static int32_t get_offset_of_tetherConstraintsData_26() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___tetherConstraintsData_26)); }
	inline ObiTetherConstraintsData_t071A4A85316CD0F23DA05223C15E3EE1B28626CE * get_tetherConstraintsData_26() const { return ___tetherConstraintsData_26; }
	inline ObiTetherConstraintsData_t071A4A85316CD0F23DA05223C15E3EE1B28626CE ** get_address_of_tetherConstraintsData_26() { return &___tetherConstraintsData_26; }
	inline void set_tetherConstraintsData_26(ObiTetherConstraintsData_t071A4A85316CD0F23DA05223C15E3EE1B28626CE * value)
	{
		___tetherConstraintsData_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tetherConstraintsData_26), (void*)value);
	}

	inline static int32_t get_offset_of_stretchShearConstraintsData_27() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___stretchShearConstraintsData_27)); }
	inline ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * get_stretchShearConstraintsData_27() const { return ___stretchShearConstraintsData_27; }
	inline ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC ** get_address_of_stretchShearConstraintsData_27() { return &___stretchShearConstraintsData_27; }
	inline void set_stretchShearConstraintsData_27(ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * value)
	{
		___stretchShearConstraintsData_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stretchShearConstraintsData_27), (void*)value);
	}

	inline static int32_t get_offset_of_bendTwistConstraintsData_28() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___bendTwistConstraintsData_28)); }
	inline ObiBendTwistConstraintsData_t681D2A3964FDCF4E12E05CED3F7C62E640440CEE * get_bendTwistConstraintsData_28() const { return ___bendTwistConstraintsData_28; }
	inline ObiBendTwistConstraintsData_t681D2A3964FDCF4E12E05CED3F7C62E640440CEE ** get_address_of_bendTwistConstraintsData_28() { return &___bendTwistConstraintsData_28; }
	inline void set_bendTwistConstraintsData_28(ObiBendTwistConstraintsData_t681D2A3964FDCF4E12E05CED3F7C62E640440CEE * value)
	{
		___bendTwistConstraintsData_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bendTwistConstraintsData_28), (void*)value);
	}

	inline static int32_t get_offset_of_shapeMatchingConstraintsData_29() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___shapeMatchingConstraintsData_29)); }
	inline ObiShapeMatchingConstraintsData_t5CC282CC712A59B303FB502754F122D4C14801C8 * get_shapeMatchingConstraintsData_29() const { return ___shapeMatchingConstraintsData_29; }
	inline ObiShapeMatchingConstraintsData_t5CC282CC712A59B303FB502754F122D4C14801C8 ** get_address_of_shapeMatchingConstraintsData_29() { return &___shapeMatchingConstraintsData_29; }
	inline void set_shapeMatchingConstraintsData_29(ObiShapeMatchingConstraintsData_t5CC282CC712A59B303FB502754F122D4C14801C8 * value)
	{
		___shapeMatchingConstraintsData_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shapeMatchingConstraintsData_29), (void*)value);
	}

	inline static int32_t get_offset_of_aerodynamicConstraintsData_30() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___aerodynamicConstraintsData_30)); }
	inline ObiAerodynamicConstraintsData_t0C7593E41FF1ECD984BD4D9FF8C84C96DDBA5563 * get_aerodynamicConstraintsData_30() const { return ___aerodynamicConstraintsData_30; }
	inline ObiAerodynamicConstraintsData_t0C7593E41FF1ECD984BD4D9FF8C84C96DDBA5563 ** get_address_of_aerodynamicConstraintsData_30() { return &___aerodynamicConstraintsData_30; }
	inline void set_aerodynamicConstraintsData_30(ObiAerodynamicConstraintsData_t0C7593E41FF1ECD984BD4D9FF8C84C96DDBA5563 * value)
	{
		___aerodynamicConstraintsData_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aerodynamicConstraintsData_30), (void*)value);
	}

	inline static int32_t get_offset_of_chainConstraintsData_31() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___chainConstraintsData_31)); }
	inline ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 * get_chainConstraintsData_31() const { return ___chainConstraintsData_31; }
	inline ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 ** get_address_of_chainConstraintsData_31() { return &___chainConstraintsData_31; }
	inline void set_chainConstraintsData_31(ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 * value)
	{
		___chainConstraintsData_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chainConstraintsData_31), (void*)value);
	}

	inline static int32_t get_offset_of_volumeConstraintsData_32() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___volumeConstraintsData_32)); }
	inline ObiVolumeConstraintsData_t12899715EEA78BB09F3BAC3CCC6EB012AF0E4D26 * get_volumeConstraintsData_32() const { return ___volumeConstraintsData_32; }
	inline ObiVolumeConstraintsData_t12899715EEA78BB09F3BAC3CCC6EB012AF0E4D26 ** get_address_of_volumeConstraintsData_32() { return &___volumeConstraintsData_32; }
	inline void set_volumeConstraintsData_32(ObiVolumeConstraintsData_t12899715EEA78BB09F3BAC3CCC6EB012AF0E4D26 * value)
	{
		___volumeConstraintsData_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___volumeConstraintsData_32), (void*)value);
	}

	inline static int32_t get_offset_of_groups_33() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___groups_33)); }
	inline List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * get_groups_33() const { return ___groups_33; }
	inline List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF ** get_address_of_groups_33() { return &___groups_33; }
	inline void set_groups_33(List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * value)
	{
		___groups_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groups_33), (void*)value);
	}
};


// Obi.ObiParticleGroup
struct ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Collections.Generic.List`1<System.Int32> Obi.ObiParticleGroup::particleIndices
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___particleIndices_4;
	// Obi.ObiActorBlueprint Obi.ObiParticleGroup::m_Blueprint
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * ___m_Blueprint_5;

public:
	inline static int32_t get_offset_of_particleIndices_4() { return static_cast<int32_t>(offsetof(ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE, ___particleIndices_4)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_particleIndices_4() const { return ___particleIndices_4; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_particleIndices_4() { return &___particleIndices_4; }
	inline void set_particleIndices_4(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___particleIndices_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particleIndices_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Blueprint_5() { return static_cast<int32_t>(offsetof(ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE, ___m_Blueprint_5)); }
	inline ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * get_m_Blueprint_5() const { return ___m_Blueprint_5; }
	inline ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD ** get_address_of_m_Blueprint_5() { return &___m_Blueprint_5; }
	inline void set_m_Blueprint_5(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * value)
	{
		___m_Blueprint_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Blueprint_5), (void*)value);
	}
};


// Obi.ObiRope/RopeTornCallback
struct RopeTornCallback_t7F7EC536306AFEEE07B7A037C3CC566AD1AF8D58  : public MulticastDelegate_t
{
public:

public:
};


// Obi.ObiSolver/CollisionCallback
struct CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB  : public MulticastDelegate_t
{
public:

public:
};


// Obi.ObiSolver/SolverCallback
struct SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A  : public MulticastDelegate_t
{
public:

public:
};


// Obi.ObiSolver/SolverStepCallback
struct SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// Obi.ObiRopeBlueprintBase
struct ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687  : public ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD
{
public:
	// Obi.ObiPath Obi.ObiRopeBlueprintBase::path
	ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * ___path_34;
	// System.Single Obi.ObiRopeBlueprintBase::thickness
	float ___thickness_35;
	// System.Single Obi.ObiRopeBlueprintBase::resolution
	float ___resolution_36;
	// System.Single Obi.ObiRopeBlueprintBase::m_InterParticleDistance
	float ___m_InterParticleDistance_37;
	// System.Int32 Obi.ObiRopeBlueprintBase::totalParticles
	int32_t ___totalParticles_38;
	// System.Single Obi.ObiRopeBlueprintBase::m_RestLength
	float ___m_RestLength_39;
	// System.Single[] Obi.ObiRopeBlueprintBase::restLengths
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___restLengths_40;

public:
	inline static int32_t get_offset_of_path_34() { return static_cast<int32_t>(offsetof(ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687, ___path_34)); }
	inline ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * get_path_34() const { return ___path_34; }
	inline ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 ** get_address_of_path_34() { return &___path_34; }
	inline void set_path_34(ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * value)
	{
		___path_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_34), (void*)value);
	}

	inline static int32_t get_offset_of_thickness_35() { return static_cast<int32_t>(offsetof(ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687, ___thickness_35)); }
	inline float get_thickness_35() const { return ___thickness_35; }
	inline float* get_address_of_thickness_35() { return &___thickness_35; }
	inline void set_thickness_35(float value)
	{
		___thickness_35 = value;
	}

	inline static int32_t get_offset_of_resolution_36() { return static_cast<int32_t>(offsetof(ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687, ___resolution_36)); }
	inline float get_resolution_36() const { return ___resolution_36; }
	inline float* get_address_of_resolution_36() { return &___resolution_36; }
	inline void set_resolution_36(float value)
	{
		___resolution_36 = value;
	}

	inline static int32_t get_offset_of_m_InterParticleDistance_37() { return static_cast<int32_t>(offsetof(ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687, ___m_InterParticleDistance_37)); }
	inline float get_m_InterParticleDistance_37() const { return ___m_InterParticleDistance_37; }
	inline float* get_address_of_m_InterParticleDistance_37() { return &___m_InterParticleDistance_37; }
	inline void set_m_InterParticleDistance_37(float value)
	{
		___m_InterParticleDistance_37 = value;
	}

	inline static int32_t get_offset_of_totalParticles_38() { return static_cast<int32_t>(offsetof(ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687, ___totalParticles_38)); }
	inline int32_t get_totalParticles_38() const { return ___totalParticles_38; }
	inline int32_t* get_address_of_totalParticles_38() { return &___totalParticles_38; }
	inline void set_totalParticles_38(int32_t value)
	{
		___totalParticles_38 = value;
	}

	inline static int32_t get_offset_of_m_RestLength_39() { return static_cast<int32_t>(offsetof(ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687, ___m_RestLength_39)); }
	inline float get_m_RestLength_39() const { return ___m_RestLength_39; }
	inline float* get_address_of_m_RestLength_39() { return &___m_RestLength_39; }
	inline void set_m_RestLength_39(float value)
	{
		___m_RestLength_39 = value;
	}

	inline static int32_t get_offset_of_restLengths_40() { return static_cast<int32_t>(offsetof(ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687, ___restLengths_40)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_restLengths_40() const { return ___restLengths_40; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_restLengths_40() { return &___restLengths_40; }
	inline void set_restLengths_40(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___restLengths_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restLengths_40), (void*)value);
	}
};


// Obi.ObiActor
struct ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Obi.ObiActor/ActorBlueprintCallback Obi.ObiActor::OnBlueprintLoaded
	ActorBlueprintCallback_t9A80D17AA5E1DB3DB84D2C1638CD6D7002AE13C0 * ___OnBlueprintLoaded_4;
	// Obi.ObiActor/ActorBlueprintCallback Obi.ObiActor::OnBlueprintUnloaded
	ActorBlueprintCallback_t9A80D17AA5E1DB3DB84D2C1638CD6D7002AE13C0 * ___OnBlueprintUnloaded_5;
	// Obi.ObiActor/ActorCallback Obi.ObiActor::OnPrepareFrame
	ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF * ___OnPrepareFrame_6;
	// Obi.ObiActor/ActorStepCallback Obi.ObiActor::OnPrepareStep
	ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * ___OnPrepareStep_7;
	// Obi.ObiActor/ActorStepCallback Obi.ObiActor::OnBeginStep
	ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * ___OnBeginStep_8;
	// Obi.ObiActor/ActorStepCallback Obi.ObiActor::OnSubstep
	ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * ___OnSubstep_9;
	// Obi.ObiActor/ActorStepCallback Obi.ObiActor::OnEndStep
	ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * ___OnEndStep_10;
	// Obi.ObiActor/ActorCallback Obi.ObiActor::OnInterpolate
	ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF * ___OnInterpolate_11;
	// System.Int32 Obi.ObiActor::m_ActiveParticleCount
	int32_t ___m_ActiveParticleCount_12;
	// System.Int32[] Obi.ObiActor::solverIndices
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___solverIndices_13;
	// System.Collections.Generic.List`1<System.Int32>[] Obi.ObiActor::solverBatchOffsets
	List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C* ___solverBatchOffsets_14;
	// Obi.ObiSolver Obi.ObiActor::m_Solver
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___m_Solver_15;
	// System.Boolean Obi.ObiActor::m_Loaded
	bool ___m_Loaded_16;
	// Obi.ObiActorBlueprint Obi.ObiActor::state
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * ___state_17;
	// Obi.ObiActorBlueprint Obi.ObiActor::m_BlueprintInstance
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * ___m_BlueprintInstance_18;
	// Obi.ObiPinConstraintsData Obi.ObiActor::m_PinConstraints
	ObiPinConstraintsData_t55BA2D5374F110C343BB649D24477242C2961DDC * ___m_PinConstraints_19;
	// Obi.ObiCollisionMaterial Obi.ObiActor::m_CollisionMaterial
	ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F * ___m_CollisionMaterial_20;
	// System.Boolean Obi.ObiActor::m_SurfaceCollisions
	bool ___m_SurfaceCollisions_21;

public:
	inline static int32_t get_offset_of_OnBlueprintLoaded_4() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___OnBlueprintLoaded_4)); }
	inline ActorBlueprintCallback_t9A80D17AA5E1DB3DB84D2C1638CD6D7002AE13C0 * get_OnBlueprintLoaded_4() const { return ___OnBlueprintLoaded_4; }
	inline ActorBlueprintCallback_t9A80D17AA5E1DB3DB84D2C1638CD6D7002AE13C0 ** get_address_of_OnBlueprintLoaded_4() { return &___OnBlueprintLoaded_4; }
	inline void set_OnBlueprintLoaded_4(ActorBlueprintCallback_t9A80D17AA5E1DB3DB84D2C1638CD6D7002AE13C0 * value)
	{
		___OnBlueprintLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnBlueprintLoaded_4), (void*)value);
	}

	inline static int32_t get_offset_of_OnBlueprintUnloaded_5() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___OnBlueprintUnloaded_5)); }
	inline ActorBlueprintCallback_t9A80D17AA5E1DB3DB84D2C1638CD6D7002AE13C0 * get_OnBlueprintUnloaded_5() const { return ___OnBlueprintUnloaded_5; }
	inline ActorBlueprintCallback_t9A80D17AA5E1DB3DB84D2C1638CD6D7002AE13C0 ** get_address_of_OnBlueprintUnloaded_5() { return &___OnBlueprintUnloaded_5; }
	inline void set_OnBlueprintUnloaded_5(ActorBlueprintCallback_t9A80D17AA5E1DB3DB84D2C1638CD6D7002AE13C0 * value)
	{
		___OnBlueprintUnloaded_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnBlueprintUnloaded_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnPrepareFrame_6() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___OnPrepareFrame_6)); }
	inline ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF * get_OnPrepareFrame_6() const { return ___OnPrepareFrame_6; }
	inline ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF ** get_address_of_OnPrepareFrame_6() { return &___OnPrepareFrame_6; }
	inline void set_OnPrepareFrame_6(ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF * value)
	{
		___OnPrepareFrame_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPrepareFrame_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnPrepareStep_7() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___OnPrepareStep_7)); }
	inline ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * get_OnPrepareStep_7() const { return ___OnPrepareStep_7; }
	inline ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D ** get_address_of_OnPrepareStep_7() { return &___OnPrepareStep_7; }
	inline void set_OnPrepareStep_7(ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * value)
	{
		___OnPrepareStep_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPrepareStep_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnBeginStep_8() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___OnBeginStep_8)); }
	inline ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * get_OnBeginStep_8() const { return ___OnBeginStep_8; }
	inline ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D ** get_address_of_OnBeginStep_8() { return &___OnBeginStep_8; }
	inline void set_OnBeginStep_8(ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * value)
	{
		___OnBeginStep_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnBeginStep_8), (void*)value);
	}

	inline static int32_t get_offset_of_OnSubstep_9() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___OnSubstep_9)); }
	inline ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * get_OnSubstep_9() const { return ___OnSubstep_9; }
	inline ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D ** get_address_of_OnSubstep_9() { return &___OnSubstep_9; }
	inline void set_OnSubstep_9(ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * value)
	{
		___OnSubstep_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSubstep_9), (void*)value);
	}

	inline static int32_t get_offset_of_OnEndStep_10() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___OnEndStep_10)); }
	inline ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * get_OnEndStep_10() const { return ___OnEndStep_10; }
	inline ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D ** get_address_of_OnEndStep_10() { return &___OnEndStep_10; }
	inline void set_OnEndStep_10(ActorStepCallback_t6674C19C2448A7BE282B7382FB749FD41D16FC8D * value)
	{
		___OnEndStep_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEndStep_10), (void*)value);
	}

	inline static int32_t get_offset_of_OnInterpolate_11() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___OnInterpolate_11)); }
	inline ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF * get_OnInterpolate_11() const { return ___OnInterpolate_11; }
	inline ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF ** get_address_of_OnInterpolate_11() { return &___OnInterpolate_11; }
	inline void set_OnInterpolate_11(ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF * value)
	{
		___OnInterpolate_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnInterpolate_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActiveParticleCount_12() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___m_ActiveParticleCount_12)); }
	inline int32_t get_m_ActiveParticleCount_12() const { return ___m_ActiveParticleCount_12; }
	inline int32_t* get_address_of_m_ActiveParticleCount_12() { return &___m_ActiveParticleCount_12; }
	inline void set_m_ActiveParticleCount_12(int32_t value)
	{
		___m_ActiveParticleCount_12 = value;
	}

	inline static int32_t get_offset_of_solverIndices_13() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___solverIndices_13)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_solverIndices_13() const { return ___solverIndices_13; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_solverIndices_13() { return &___solverIndices_13; }
	inline void set_solverIndices_13(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___solverIndices_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___solverIndices_13), (void*)value);
	}

	inline static int32_t get_offset_of_solverBatchOffsets_14() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___solverBatchOffsets_14)); }
	inline List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C* get_solverBatchOffsets_14() const { return ___solverBatchOffsets_14; }
	inline List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C** get_address_of_solverBatchOffsets_14() { return &___solverBatchOffsets_14; }
	inline void set_solverBatchOffsets_14(List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C* value)
	{
		___solverBatchOffsets_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___solverBatchOffsets_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_Solver_15() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___m_Solver_15)); }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * get_m_Solver_15() const { return ___m_Solver_15; }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 ** get_address_of_m_Solver_15() { return &___m_Solver_15; }
	inline void set_m_Solver_15(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * value)
	{
		___m_Solver_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Solver_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_Loaded_16() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___m_Loaded_16)); }
	inline bool get_m_Loaded_16() const { return ___m_Loaded_16; }
	inline bool* get_address_of_m_Loaded_16() { return &___m_Loaded_16; }
	inline void set_m_Loaded_16(bool value)
	{
		___m_Loaded_16 = value;
	}

	inline static int32_t get_offset_of_state_17() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___state_17)); }
	inline ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * get_state_17() const { return ___state_17; }
	inline ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD ** get_address_of_state_17() { return &___state_17; }
	inline void set_state_17(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * value)
	{
		___state_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___state_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_BlueprintInstance_18() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___m_BlueprintInstance_18)); }
	inline ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * get_m_BlueprintInstance_18() const { return ___m_BlueprintInstance_18; }
	inline ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD ** get_address_of_m_BlueprintInstance_18() { return &___m_BlueprintInstance_18; }
	inline void set_m_BlueprintInstance_18(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * value)
	{
		___m_BlueprintInstance_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BlueprintInstance_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_PinConstraints_19() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___m_PinConstraints_19)); }
	inline ObiPinConstraintsData_t55BA2D5374F110C343BB649D24477242C2961DDC * get_m_PinConstraints_19() const { return ___m_PinConstraints_19; }
	inline ObiPinConstraintsData_t55BA2D5374F110C343BB649D24477242C2961DDC ** get_address_of_m_PinConstraints_19() { return &___m_PinConstraints_19; }
	inline void set_m_PinConstraints_19(ObiPinConstraintsData_t55BA2D5374F110C343BB649D24477242C2961DDC * value)
	{
		___m_PinConstraints_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PinConstraints_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CollisionMaterial_20() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___m_CollisionMaterial_20)); }
	inline ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F * get_m_CollisionMaterial_20() const { return ___m_CollisionMaterial_20; }
	inline ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F ** get_address_of_m_CollisionMaterial_20() { return &___m_CollisionMaterial_20; }
	inline void set_m_CollisionMaterial_20(ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F * value)
	{
		___m_CollisionMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CollisionMaterial_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_SurfaceCollisions_21() { return static_cast<int32_t>(offsetof(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6, ___m_SurfaceCollisions_21)); }
	inline bool get_m_SurfaceCollisions_21() const { return ___m_SurfaceCollisions_21; }
	inline bool* get_address_of_m_SurfaceCollisions_21() { return &___m_SurfaceCollisions_21; }
	inline void set_m_SurfaceCollisions_21(bool value)
	{
		___m_SurfaceCollisions_21 = value;
	}
};


// Obi.ObiRodBlueprint
struct ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597  : public ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687
{
public:
	// System.Boolean Obi.ObiRodBlueprint::keepInitialShape
	bool ___keepInitialShape_41;

public:
	inline static int32_t get_offset_of_keepInitialShape_41() { return static_cast<int32_t>(offsetof(ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597, ___keepInitialShape_41)); }
	inline bool get_keepInitialShape_41() const { return ___keepInitialShape_41; }
	inline bool* get_address_of_keepInitialShape_41() { return &___keepInitialShape_41; }
	inline void set_keepInitialShape_41(bool value)
	{
		___keepInitialShape_41 = value;
	}
};


// Obi.ObiRopeBlueprint
struct ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B  : public ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687
{
public:
	// System.Int32 Obi.ObiRopeBlueprint::pooledParticles
	int32_t ___pooledParticles_41;

public:
	inline static int32_t get_offset_of_pooledParticles_41() { return static_cast<int32_t>(offsetof(ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B, ___pooledParticles_41)); }
	inline int32_t get_pooledParticles_41() const { return ___pooledParticles_41; }
	inline int32_t* get_address_of_pooledParticles_41() { return &___pooledParticles_41; }
	inline void set_pooledParticles_41(int32_t value)
	{
		___pooledParticles_41 = value;
	}
};


// Obi.ObiSolver
struct ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Obi.ObiSolver/CollisionCallback Obi.ObiSolver::OnCollision
	CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB * ___OnCollision_9;
	// Obi.ObiSolver/CollisionCallback Obi.ObiSolver::OnParticleCollision
	CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB * ___OnParticleCollision_10;
	// Obi.ObiSolver/SolverCallback Obi.ObiSolver::OnUpdateParameters
	SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * ___OnUpdateParameters_11;
	// Obi.ObiSolver/SolverCallback Obi.ObiSolver::OnPrepareFrame
	SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * ___OnPrepareFrame_12;
	// Obi.ObiSolver/SolverStepCallback Obi.ObiSolver::OnPrepareStep
	SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * ___OnPrepareStep_13;
	// Obi.ObiSolver/SolverStepCallback Obi.ObiSolver::OnBeginStep
	SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * ___OnBeginStep_14;
	// Obi.ObiSolver/SolverStepCallback Obi.ObiSolver::OnSubstep
	SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * ___OnSubstep_15;
	// Obi.ObiSolver/SolverCallback Obi.ObiSolver::OnEndStep
	SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * ___OnEndStep_16;
	// Obi.ObiSolver/SolverCallback Obi.ObiSolver::OnInterpolate
	SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * ___OnInterpolate_17;
	// System.Boolean Obi.ObiSolver::simulateWhenInvisible
	bool ___simulateWhenInvisible_18;
	// Obi.ISolverImpl Obi.ObiSolver::m_SolverImpl
	RuntimeObject* ___m_SolverImpl_19;
	// Obi.IObiBackend Obi.ObiSolver::m_SimulationBackend
	RuntimeObject* ___m_SimulationBackend_20;
	// Obi.ObiSolver/BackendType Obi.ObiSolver::m_Backend
	int32_t ___m_Backend_21;
	// Oni/SolverParameters Obi.ObiSolver::parameters
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6  ___parameters_22;
	// UnityEngine.Vector3 Obi.ObiSolver::gravity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___gravity_23;
	// UnityEngine.Space Obi.ObiSolver::gravitySpace
	int32_t ___gravitySpace_24;
	// System.Single Obi.ObiSolver::worldLinearInertiaScale
	float ___worldLinearInertiaScale_25;
	// System.Single Obi.ObiSolver::worldAngularInertiaScale
	float ___worldAngularInertiaScale_26;
	// System.Collections.Generic.List`1<Obi.ObiActor> Obi.ObiSolver::actors
	List_1_t817AE5D45DC7F38B962828B6735F7715EB2D865B * ___actors_27;
	// Obi.ObiSolver/ParticleInActor[] Obi.ObiSolver::m_ParticleToActor
	ParticleInActorU5BU5D_t3EB732B2921D698AC54DD3C56E5D31A0513E8707* ___m_ParticleToActor_28;
	// Obi.ObiNativeIntList Obi.ObiSolver::freeList
	ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * ___freeList_29;
	// System.Int32[] Obi.ObiSolver::activeParticles
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___activeParticles_30;
	// System.Int32 Obi.ObiSolver::activeParticleCount
	int32_t ___activeParticleCount_31;
	// System.Collections.Generic.List`1<System.Int32> Obi.ObiSolver::simplices
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___simplices_32;
	// System.Collections.Generic.List`1<System.Int32> Obi.ObiSolver::points
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___points_33;
	// System.Collections.Generic.List`1<System.Int32> Obi.ObiSolver::edges
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___edges_34;
	// System.Collections.Generic.List`1<System.Int32> Obi.ObiSolver::triangles
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___triangles_35;
	// Obi.SimplexCounts Obi.ObiSolver::m_SimplexCounts
	SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA  ___m_SimplexCounts_36;
	// System.Boolean Obi.ObiSolver::dirtyActiveParticles
	bool ___dirtyActiveParticles_37;
	// System.Boolean Obi.ObiSolver::dirtySimplices
	bool ___dirtySimplices_38;
	// System.Int32 Obi.ObiSolver::dirtyConstraints
	int32_t ___dirtyConstraints_39;
	// Obi.ObiSolver/ObiCollisionEventArgs Obi.ObiSolver::collisionArgs
	ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * ___collisionArgs_40;
	// Obi.ObiSolver/ObiCollisionEventArgs Obi.ObiSolver::particleCollisionArgs
	ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * ___particleCollisionArgs_41;
	// System.Int32 Obi.ObiSolver::m_contactCount
	int32_t ___m_contactCount_42;
	// System.Int32 Obi.ObiSolver::m_particleContactCount
	int32_t ___m_particleContactCount_43;
	// System.Single Obi.ObiSolver::m_MaxScale
	float ___m_MaxScale_44;
	// UnityEngine.Bounds Obi.ObiSolver::bounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___bounds_45;
	// UnityEngine.Plane[] Obi.ObiSolver::planes
	PlaneU5BU5D_t33BCF5C401A053481BF4098B8D32E6C5DD53B9FD* ___planes_46;
	// UnityEngine.Camera[] Obi.ObiSolver::sceneCameras
	CameraU5BU5D_tAF84B9EC9AF40F1B6294BCEBA82A1AD123A9D001* ___sceneCameras_47;
	// System.Boolean Obi.ObiSolver::isVisible
	bool ___isVisible_48;
	// Obi.IObiConstraints[] Obi.ObiSolver::m_Constraints
	IObiConstraintsU5BU5D_t4021995930E99D99E580CBA120F9AC0526C8032F* ___m_Constraints_49;
	// Oni/ConstraintParameters Obi.ObiSolver::distanceConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___distanceConstraintParameters_50;
	// Oni/ConstraintParameters Obi.ObiSolver::bendingConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___bendingConstraintParameters_51;
	// Oni/ConstraintParameters Obi.ObiSolver::particleCollisionConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___particleCollisionConstraintParameters_52;
	// Oni/ConstraintParameters Obi.ObiSolver::particleFrictionConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___particleFrictionConstraintParameters_53;
	// Oni/ConstraintParameters Obi.ObiSolver::collisionConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___collisionConstraintParameters_54;
	// Oni/ConstraintParameters Obi.ObiSolver::frictionConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___frictionConstraintParameters_55;
	// Oni/ConstraintParameters Obi.ObiSolver::skinConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___skinConstraintParameters_56;
	// Oni/ConstraintParameters Obi.ObiSolver::volumeConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___volumeConstraintParameters_57;
	// Oni/ConstraintParameters Obi.ObiSolver::shapeMatchingConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___shapeMatchingConstraintParameters_58;
	// Oni/ConstraintParameters Obi.ObiSolver::tetherConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___tetherConstraintParameters_59;
	// Oni/ConstraintParameters Obi.ObiSolver::pinConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___pinConstraintParameters_60;
	// Oni/ConstraintParameters Obi.ObiSolver::stitchConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___stitchConstraintParameters_61;
	// Oni/ConstraintParameters Obi.ObiSolver::densityConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___densityConstraintParameters_62;
	// Oni/ConstraintParameters Obi.ObiSolver::stretchShearConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___stretchShearConstraintParameters_63;
	// Oni/ConstraintParameters Obi.ObiSolver::bendTwistConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___bendTwistConstraintParameters_64;
	// Oni/ConstraintParameters Obi.ObiSolver::chainConstraintParameters
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  ___chainConstraintParameters_65;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_RigidbodyLinearVelocities
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_RigidbodyLinearVelocities_66;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_RigidbodyAngularVelocities
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_RigidbodyAngularVelocities_67;
	// UnityEngine.Color[] Obi.ObiSolver::m_Colors
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ___m_Colors_68;
	// Obi.ObiNativeInt4List Obi.ObiSolver::m_CellCoords
	ObiNativeInt4List_t77B698CA82EEEFCB8C86017B00EABC6A4C3C0344 * ___m_CellCoords_69;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_Positions
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_Positions_70;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_RestPositions
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_RestPositions_71;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_PrevPositions
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_PrevPositions_72;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_StartPositions
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_StartPositions_73;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_RenderablePositions
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_RenderablePositions_74;
	// Obi.ObiNativeQuaternionList Obi.ObiSolver::m_Orientations
	ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * ___m_Orientations_75;
	// Obi.ObiNativeQuaternionList Obi.ObiSolver::m_RestOrientations
	ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * ___m_RestOrientations_76;
	// Obi.ObiNativeQuaternionList Obi.ObiSolver::m_PrevOrientations
	ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * ___m_PrevOrientations_77;
	// Obi.ObiNativeQuaternionList Obi.ObiSolver::m_StartOrientations
	ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * ___m_StartOrientations_78;
	// Obi.ObiNativeQuaternionList Obi.ObiSolver::m_RenderableOrientations
	ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * ___m_RenderableOrientations_79;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_Velocities
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_Velocities_80;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_AngularVelocities
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_AngularVelocities_81;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_InvMasses
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_InvMasses_82;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_InvRotationalMasses
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_InvRotationalMasses_83;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_InvInertiaTensors
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_InvInertiaTensors_84;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_ExternalForces
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_ExternalForces_85;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_ExternalTorques
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_ExternalTorques_86;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_Wind
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_Wind_87;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_PositionDeltas
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_PositionDeltas_88;
	// Obi.ObiNativeQuaternionList Obi.ObiSolver::m_OrientationDeltas
	ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * ___m_OrientationDeltas_89;
	// Obi.ObiNativeIntList Obi.ObiSolver::m_PositionConstraintCounts
	ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * ___m_PositionConstraintCounts_90;
	// Obi.ObiNativeIntList Obi.ObiSolver::m_OrientationConstraintCounts
	ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * ___m_OrientationConstraintCounts_91;
	// Obi.ObiNativeIntList Obi.ObiSolver::m_CollisionMaterials
	ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * ___m_CollisionMaterials_92;
	// Obi.ObiNativeIntList Obi.ObiSolver::m_Phases
	ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * ___m_Phases_93;
	// Obi.ObiNativeIntList Obi.ObiSolver::m_Filters
	ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * ___m_Filters_94;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_Anisotropies
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_Anisotropies_95;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_PrincipalRadii
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_PrincipalRadii_96;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_Normals
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_Normals_97;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_Vorticities
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_Vorticities_98;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_FluidData
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_FluidData_99;
	// Obi.ObiNativeVector4List Obi.ObiSolver::m_UserData
	ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * ___m_UserData_100;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_SmoothingRadii
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_SmoothingRadii_101;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_Buoyancies
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_Buoyancies_102;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_RestDensities
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_RestDensities_103;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_Viscosities
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_Viscosities_104;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_SurfaceTension
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_SurfaceTension_105;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_VortConfinement
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_VortConfinement_106;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_AtmosphericDrag
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_AtmosphericDrag_107;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_AtmosphericPressure
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_AtmosphericPressure_108;
	// Obi.ObiNativeFloatList Obi.ObiSolver::m_Diffusion
	ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * ___m_Diffusion_109;

public:
	inline static int32_t get_offset_of_OnCollision_9() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___OnCollision_9)); }
	inline CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB * get_OnCollision_9() const { return ___OnCollision_9; }
	inline CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB ** get_address_of_OnCollision_9() { return &___OnCollision_9; }
	inline void set_OnCollision_9(CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB * value)
	{
		___OnCollision_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnCollision_9), (void*)value);
	}

	inline static int32_t get_offset_of_OnParticleCollision_10() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___OnParticleCollision_10)); }
	inline CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB * get_OnParticleCollision_10() const { return ___OnParticleCollision_10; }
	inline CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB ** get_address_of_OnParticleCollision_10() { return &___OnParticleCollision_10; }
	inline void set_OnParticleCollision_10(CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB * value)
	{
		___OnParticleCollision_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnParticleCollision_10), (void*)value);
	}

	inline static int32_t get_offset_of_OnUpdateParameters_11() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___OnUpdateParameters_11)); }
	inline SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * get_OnUpdateParameters_11() const { return ___OnUpdateParameters_11; }
	inline SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A ** get_address_of_OnUpdateParameters_11() { return &___OnUpdateParameters_11; }
	inline void set_OnUpdateParameters_11(SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * value)
	{
		___OnUpdateParameters_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnUpdateParameters_11), (void*)value);
	}

	inline static int32_t get_offset_of_OnPrepareFrame_12() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___OnPrepareFrame_12)); }
	inline SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * get_OnPrepareFrame_12() const { return ___OnPrepareFrame_12; }
	inline SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A ** get_address_of_OnPrepareFrame_12() { return &___OnPrepareFrame_12; }
	inline void set_OnPrepareFrame_12(SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * value)
	{
		___OnPrepareFrame_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPrepareFrame_12), (void*)value);
	}

	inline static int32_t get_offset_of_OnPrepareStep_13() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___OnPrepareStep_13)); }
	inline SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * get_OnPrepareStep_13() const { return ___OnPrepareStep_13; }
	inline SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 ** get_address_of_OnPrepareStep_13() { return &___OnPrepareStep_13; }
	inline void set_OnPrepareStep_13(SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * value)
	{
		___OnPrepareStep_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPrepareStep_13), (void*)value);
	}

	inline static int32_t get_offset_of_OnBeginStep_14() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___OnBeginStep_14)); }
	inline SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * get_OnBeginStep_14() const { return ___OnBeginStep_14; }
	inline SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 ** get_address_of_OnBeginStep_14() { return &___OnBeginStep_14; }
	inline void set_OnBeginStep_14(SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * value)
	{
		___OnBeginStep_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnBeginStep_14), (void*)value);
	}

	inline static int32_t get_offset_of_OnSubstep_15() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___OnSubstep_15)); }
	inline SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * get_OnSubstep_15() const { return ___OnSubstep_15; }
	inline SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 ** get_address_of_OnSubstep_15() { return &___OnSubstep_15; }
	inline void set_OnSubstep_15(SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * value)
	{
		___OnSubstep_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnSubstep_15), (void*)value);
	}

	inline static int32_t get_offset_of_OnEndStep_16() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___OnEndStep_16)); }
	inline SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * get_OnEndStep_16() const { return ___OnEndStep_16; }
	inline SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A ** get_address_of_OnEndStep_16() { return &___OnEndStep_16; }
	inline void set_OnEndStep_16(SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * value)
	{
		___OnEndStep_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEndStep_16), (void*)value);
	}

	inline static int32_t get_offset_of_OnInterpolate_17() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___OnInterpolate_17)); }
	inline SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * get_OnInterpolate_17() const { return ___OnInterpolate_17; }
	inline SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A ** get_address_of_OnInterpolate_17() { return &___OnInterpolate_17; }
	inline void set_OnInterpolate_17(SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * value)
	{
		___OnInterpolate_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnInterpolate_17), (void*)value);
	}

	inline static int32_t get_offset_of_simulateWhenInvisible_18() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___simulateWhenInvisible_18)); }
	inline bool get_simulateWhenInvisible_18() const { return ___simulateWhenInvisible_18; }
	inline bool* get_address_of_simulateWhenInvisible_18() { return &___simulateWhenInvisible_18; }
	inline void set_simulateWhenInvisible_18(bool value)
	{
		___simulateWhenInvisible_18 = value;
	}

	inline static int32_t get_offset_of_m_SolverImpl_19() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_SolverImpl_19)); }
	inline RuntimeObject* get_m_SolverImpl_19() const { return ___m_SolverImpl_19; }
	inline RuntimeObject** get_address_of_m_SolverImpl_19() { return &___m_SolverImpl_19; }
	inline void set_m_SolverImpl_19(RuntimeObject* value)
	{
		___m_SolverImpl_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SolverImpl_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_SimulationBackend_20() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_SimulationBackend_20)); }
	inline RuntimeObject* get_m_SimulationBackend_20() const { return ___m_SimulationBackend_20; }
	inline RuntimeObject** get_address_of_m_SimulationBackend_20() { return &___m_SimulationBackend_20; }
	inline void set_m_SimulationBackend_20(RuntimeObject* value)
	{
		___m_SimulationBackend_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SimulationBackend_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_Backend_21() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Backend_21)); }
	inline int32_t get_m_Backend_21() const { return ___m_Backend_21; }
	inline int32_t* get_address_of_m_Backend_21() { return &___m_Backend_21; }
	inline void set_m_Backend_21(int32_t value)
	{
		___m_Backend_21 = value;
	}

	inline static int32_t get_offset_of_parameters_22() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___parameters_22)); }
	inline SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6  get_parameters_22() const { return ___parameters_22; }
	inline SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 * get_address_of_parameters_22() { return &___parameters_22; }
	inline void set_parameters_22(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6  value)
	{
		___parameters_22 = value;
	}

	inline static int32_t get_offset_of_gravity_23() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___gravity_23)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_gravity_23() const { return ___gravity_23; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_gravity_23() { return &___gravity_23; }
	inline void set_gravity_23(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___gravity_23 = value;
	}

	inline static int32_t get_offset_of_gravitySpace_24() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___gravitySpace_24)); }
	inline int32_t get_gravitySpace_24() const { return ___gravitySpace_24; }
	inline int32_t* get_address_of_gravitySpace_24() { return &___gravitySpace_24; }
	inline void set_gravitySpace_24(int32_t value)
	{
		___gravitySpace_24 = value;
	}

	inline static int32_t get_offset_of_worldLinearInertiaScale_25() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___worldLinearInertiaScale_25)); }
	inline float get_worldLinearInertiaScale_25() const { return ___worldLinearInertiaScale_25; }
	inline float* get_address_of_worldLinearInertiaScale_25() { return &___worldLinearInertiaScale_25; }
	inline void set_worldLinearInertiaScale_25(float value)
	{
		___worldLinearInertiaScale_25 = value;
	}

	inline static int32_t get_offset_of_worldAngularInertiaScale_26() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___worldAngularInertiaScale_26)); }
	inline float get_worldAngularInertiaScale_26() const { return ___worldAngularInertiaScale_26; }
	inline float* get_address_of_worldAngularInertiaScale_26() { return &___worldAngularInertiaScale_26; }
	inline void set_worldAngularInertiaScale_26(float value)
	{
		___worldAngularInertiaScale_26 = value;
	}

	inline static int32_t get_offset_of_actors_27() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___actors_27)); }
	inline List_1_t817AE5D45DC7F38B962828B6735F7715EB2D865B * get_actors_27() const { return ___actors_27; }
	inline List_1_t817AE5D45DC7F38B962828B6735F7715EB2D865B ** get_address_of_actors_27() { return &___actors_27; }
	inline void set_actors_27(List_1_t817AE5D45DC7F38B962828B6735F7715EB2D865B * value)
	{
		___actors_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actors_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParticleToActor_28() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_ParticleToActor_28)); }
	inline ParticleInActorU5BU5D_t3EB732B2921D698AC54DD3C56E5D31A0513E8707* get_m_ParticleToActor_28() const { return ___m_ParticleToActor_28; }
	inline ParticleInActorU5BU5D_t3EB732B2921D698AC54DD3C56E5D31A0513E8707** get_address_of_m_ParticleToActor_28() { return &___m_ParticleToActor_28; }
	inline void set_m_ParticleToActor_28(ParticleInActorU5BU5D_t3EB732B2921D698AC54DD3C56E5D31A0513E8707* value)
	{
		___m_ParticleToActor_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleToActor_28), (void*)value);
	}

	inline static int32_t get_offset_of_freeList_29() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___freeList_29)); }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * get_freeList_29() const { return ___freeList_29; }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 ** get_address_of_freeList_29() { return &___freeList_29; }
	inline void set_freeList_29(ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * value)
	{
		___freeList_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freeList_29), (void*)value);
	}

	inline static int32_t get_offset_of_activeParticles_30() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___activeParticles_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_activeParticles_30() const { return ___activeParticles_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_activeParticles_30() { return &___activeParticles_30; }
	inline void set_activeParticles_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___activeParticles_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___activeParticles_30), (void*)value);
	}

	inline static int32_t get_offset_of_activeParticleCount_31() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___activeParticleCount_31)); }
	inline int32_t get_activeParticleCount_31() const { return ___activeParticleCount_31; }
	inline int32_t* get_address_of_activeParticleCount_31() { return &___activeParticleCount_31; }
	inline void set_activeParticleCount_31(int32_t value)
	{
		___activeParticleCount_31 = value;
	}

	inline static int32_t get_offset_of_simplices_32() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___simplices_32)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_simplices_32() const { return ___simplices_32; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_simplices_32() { return &___simplices_32; }
	inline void set_simplices_32(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___simplices_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___simplices_32), (void*)value);
	}

	inline static int32_t get_offset_of_points_33() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___points_33)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_points_33() const { return ___points_33; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_points_33() { return &___points_33; }
	inline void set_points_33(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___points_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___points_33), (void*)value);
	}

	inline static int32_t get_offset_of_edges_34() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___edges_34)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_edges_34() const { return ___edges_34; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_edges_34() { return &___edges_34; }
	inline void set_edges_34(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___edges_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___edges_34), (void*)value);
	}

	inline static int32_t get_offset_of_triangles_35() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___triangles_35)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_triangles_35() const { return ___triangles_35; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_triangles_35() { return &___triangles_35; }
	inline void set_triangles_35(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___triangles_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___triangles_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_SimplexCounts_36() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_SimplexCounts_36)); }
	inline SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA  get_m_SimplexCounts_36() const { return ___m_SimplexCounts_36; }
	inline SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA * get_address_of_m_SimplexCounts_36() { return &___m_SimplexCounts_36; }
	inline void set_m_SimplexCounts_36(SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA  value)
	{
		___m_SimplexCounts_36 = value;
	}

	inline static int32_t get_offset_of_dirtyActiveParticles_37() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___dirtyActiveParticles_37)); }
	inline bool get_dirtyActiveParticles_37() const { return ___dirtyActiveParticles_37; }
	inline bool* get_address_of_dirtyActiveParticles_37() { return &___dirtyActiveParticles_37; }
	inline void set_dirtyActiveParticles_37(bool value)
	{
		___dirtyActiveParticles_37 = value;
	}

	inline static int32_t get_offset_of_dirtySimplices_38() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___dirtySimplices_38)); }
	inline bool get_dirtySimplices_38() const { return ___dirtySimplices_38; }
	inline bool* get_address_of_dirtySimplices_38() { return &___dirtySimplices_38; }
	inline void set_dirtySimplices_38(bool value)
	{
		___dirtySimplices_38 = value;
	}

	inline static int32_t get_offset_of_dirtyConstraints_39() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___dirtyConstraints_39)); }
	inline int32_t get_dirtyConstraints_39() const { return ___dirtyConstraints_39; }
	inline int32_t* get_address_of_dirtyConstraints_39() { return &___dirtyConstraints_39; }
	inline void set_dirtyConstraints_39(int32_t value)
	{
		___dirtyConstraints_39 = value;
	}

	inline static int32_t get_offset_of_collisionArgs_40() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___collisionArgs_40)); }
	inline ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * get_collisionArgs_40() const { return ___collisionArgs_40; }
	inline ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 ** get_address_of_collisionArgs_40() { return &___collisionArgs_40; }
	inline void set_collisionArgs_40(ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * value)
	{
		___collisionArgs_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collisionArgs_40), (void*)value);
	}

	inline static int32_t get_offset_of_particleCollisionArgs_41() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___particleCollisionArgs_41)); }
	inline ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * get_particleCollisionArgs_41() const { return ___particleCollisionArgs_41; }
	inline ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 ** get_address_of_particleCollisionArgs_41() { return &___particleCollisionArgs_41; }
	inline void set_particleCollisionArgs_41(ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * value)
	{
		___particleCollisionArgs_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___particleCollisionArgs_41), (void*)value);
	}

	inline static int32_t get_offset_of_m_contactCount_42() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_contactCount_42)); }
	inline int32_t get_m_contactCount_42() const { return ___m_contactCount_42; }
	inline int32_t* get_address_of_m_contactCount_42() { return &___m_contactCount_42; }
	inline void set_m_contactCount_42(int32_t value)
	{
		___m_contactCount_42 = value;
	}

	inline static int32_t get_offset_of_m_particleContactCount_43() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_particleContactCount_43)); }
	inline int32_t get_m_particleContactCount_43() const { return ___m_particleContactCount_43; }
	inline int32_t* get_address_of_m_particleContactCount_43() { return &___m_particleContactCount_43; }
	inline void set_m_particleContactCount_43(int32_t value)
	{
		___m_particleContactCount_43 = value;
	}

	inline static int32_t get_offset_of_m_MaxScale_44() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_MaxScale_44)); }
	inline float get_m_MaxScale_44() const { return ___m_MaxScale_44; }
	inline float* get_address_of_m_MaxScale_44() { return &___m_MaxScale_44; }
	inline void set_m_MaxScale_44(float value)
	{
		___m_MaxScale_44 = value;
	}

	inline static int32_t get_offset_of_bounds_45() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___bounds_45)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_bounds_45() const { return ___bounds_45; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_bounds_45() { return &___bounds_45; }
	inline void set_bounds_45(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___bounds_45 = value;
	}

	inline static int32_t get_offset_of_planes_46() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___planes_46)); }
	inline PlaneU5BU5D_t33BCF5C401A053481BF4098B8D32E6C5DD53B9FD* get_planes_46() const { return ___planes_46; }
	inline PlaneU5BU5D_t33BCF5C401A053481BF4098B8D32E6C5DD53B9FD** get_address_of_planes_46() { return &___planes_46; }
	inline void set_planes_46(PlaneU5BU5D_t33BCF5C401A053481BF4098B8D32E6C5DD53B9FD* value)
	{
		___planes_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___planes_46), (void*)value);
	}

	inline static int32_t get_offset_of_sceneCameras_47() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___sceneCameras_47)); }
	inline CameraU5BU5D_tAF84B9EC9AF40F1B6294BCEBA82A1AD123A9D001* get_sceneCameras_47() const { return ___sceneCameras_47; }
	inline CameraU5BU5D_tAF84B9EC9AF40F1B6294BCEBA82A1AD123A9D001** get_address_of_sceneCameras_47() { return &___sceneCameras_47; }
	inline void set_sceneCameras_47(CameraU5BU5D_tAF84B9EC9AF40F1B6294BCEBA82A1AD123A9D001* value)
	{
		___sceneCameras_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sceneCameras_47), (void*)value);
	}

	inline static int32_t get_offset_of_isVisible_48() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___isVisible_48)); }
	inline bool get_isVisible_48() const { return ___isVisible_48; }
	inline bool* get_address_of_isVisible_48() { return &___isVisible_48; }
	inline void set_isVisible_48(bool value)
	{
		___isVisible_48 = value;
	}

	inline static int32_t get_offset_of_m_Constraints_49() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Constraints_49)); }
	inline IObiConstraintsU5BU5D_t4021995930E99D99E580CBA120F9AC0526C8032F* get_m_Constraints_49() const { return ___m_Constraints_49; }
	inline IObiConstraintsU5BU5D_t4021995930E99D99E580CBA120F9AC0526C8032F** get_address_of_m_Constraints_49() { return &___m_Constraints_49; }
	inline void set_m_Constraints_49(IObiConstraintsU5BU5D_t4021995930E99D99E580CBA120F9AC0526C8032F* value)
	{
		___m_Constraints_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Constraints_49), (void*)value);
	}

	inline static int32_t get_offset_of_distanceConstraintParameters_50() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___distanceConstraintParameters_50)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_distanceConstraintParameters_50() const { return ___distanceConstraintParameters_50; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_distanceConstraintParameters_50() { return &___distanceConstraintParameters_50; }
	inline void set_distanceConstraintParameters_50(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___distanceConstraintParameters_50 = value;
	}

	inline static int32_t get_offset_of_bendingConstraintParameters_51() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___bendingConstraintParameters_51)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_bendingConstraintParameters_51() const { return ___bendingConstraintParameters_51; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_bendingConstraintParameters_51() { return &___bendingConstraintParameters_51; }
	inline void set_bendingConstraintParameters_51(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___bendingConstraintParameters_51 = value;
	}

	inline static int32_t get_offset_of_particleCollisionConstraintParameters_52() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___particleCollisionConstraintParameters_52)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_particleCollisionConstraintParameters_52() const { return ___particleCollisionConstraintParameters_52; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_particleCollisionConstraintParameters_52() { return &___particleCollisionConstraintParameters_52; }
	inline void set_particleCollisionConstraintParameters_52(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___particleCollisionConstraintParameters_52 = value;
	}

	inline static int32_t get_offset_of_particleFrictionConstraintParameters_53() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___particleFrictionConstraintParameters_53)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_particleFrictionConstraintParameters_53() const { return ___particleFrictionConstraintParameters_53; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_particleFrictionConstraintParameters_53() { return &___particleFrictionConstraintParameters_53; }
	inline void set_particleFrictionConstraintParameters_53(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___particleFrictionConstraintParameters_53 = value;
	}

	inline static int32_t get_offset_of_collisionConstraintParameters_54() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___collisionConstraintParameters_54)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_collisionConstraintParameters_54() const { return ___collisionConstraintParameters_54; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_collisionConstraintParameters_54() { return &___collisionConstraintParameters_54; }
	inline void set_collisionConstraintParameters_54(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___collisionConstraintParameters_54 = value;
	}

	inline static int32_t get_offset_of_frictionConstraintParameters_55() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___frictionConstraintParameters_55)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_frictionConstraintParameters_55() const { return ___frictionConstraintParameters_55; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_frictionConstraintParameters_55() { return &___frictionConstraintParameters_55; }
	inline void set_frictionConstraintParameters_55(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___frictionConstraintParameters_55 = value;
	}

	inline static int32_t get_offset_of_skinConstraintParameters_56() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___skinConstraintParameters_56)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_skinConstraintParameters_56() const { return ___skinConstraintParameters_56; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_skinConstraintParameters_56() { return &___skinConstraintParameters_56; }
	inline void set_skinConstraintParameters_56(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___skinConstraintParameters_56 = value;
	}

	inline static int32_t get_offset_of_volumeConstraintParameters_57() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___volumeConstraintParameters_57)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_volumeConstraintParameters_57() const { return ___volumeConstraintParameters_57; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_volumeConstraintParameters_57() { return &___volumeConstraintParameters_57; }
	inline void set_volumeConstraintParameters_57(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___volumeConstraintParameters_57 = value;
	}

	inline static int32_t get_offset_of_shapeMatchingConstraintParameters_58() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___shapeMatchingConstraintParameters_58)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_shapeMatchingConstraintParameters_58() const { return ___shapeMatchingConstraintParameters_58; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_shapeMatchingConstraintParameters_58() { return &___shapeMatchingConstraintParameters_58; }
	inline void set_shapeMatchingConstraintParameters_58(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___shapeMatchingConstraintParameters_58 = value;
	}

	inline static int32_t get_offset_of_tetherConstraintParameters_59() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___tetherConstraintParameters_59)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_tetherConstraintParameters_59() const { return ___tetherConstraintParameters_59; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_tetherConstraintParameters_59() { return &___tetherConstraintParameters_59; }
	inline void set_tetherConstraintParameters_59(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___tetherConstraintParameters_59 = value;
	}

	inline static int32_t get_offset_of_pinConstraintParameters_60() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___pinConstraintParameters_60)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_pinConstraintParameters_60() const { return ___pinConstraintParameters_60; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_pinConstraintParameters_60() { return &___pinConstraintParameters_60; }
	inline void set_pinConstraintParameters_60(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___pinConstraintParameters_60 = value;
	}

	inline static int32_t get_offset_of_stitchConstraintParameters_61() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___stitchConstraintParameters_61)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_stitchConstraintParameters_61() const { return ___stitchConstraintParameters_61; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_stitchConstraintParameters_61() { return &___stitchConstraintParameters_61; }
	inline void set_stitchConstraintParameters_61(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___stitchConstraintParameters_61 = value;
	}

	inline static int32_t get_offset_of_densityConstraintParameters_62() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___densityConstraintParameters_62)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_densityConstraintParameters_62() const { return ___densityConstraintParameters_62; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_densityConstraintParameters_62() { return &___densityConstraintParameters_62; }
	inline void set_densityConstraintParameters_62(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___densityConstraintParameters_62 = value;
	}

	inline static int32_t get_offset_of_stretchShearConstraintParameters_63() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___stretchShearConstraintParameters_63)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_stretchShearConstraintParameters_63() const { return ___stretchShearConstraintParameters_63; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_stretchShearConstraintParameters_63() { return &___stretchShearConstraintParameters_63; }
	inline void set_stretchShearConstraintParameters_63(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___stretchShearConstraintParameters_63 = value;
	}

	inline static int32_t get_offset_of_bendTwistConstraintParameters_64() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___bendTwistConstraintParameters_64)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_bendTwistConstraintParameters_64() const { return ___bendTwistConstraintParameters_64; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_bendTwistConstraintParameters_64() { return &___bendTwistConstraintParameters_64; }
	inline void set_bendTwistConstraintParameters_64(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___bendTwistConstraintParameters_64 = value;
	}

	inline static int32_t get_offset_of_chainConstraintParameters_65() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___chainConstraintParameters_65)); }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  get_chainConstraintParameters_65() const { return ___chainConstraintParameters_65; }
	inline ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * get_address_of_chainConstraintParameters_65() { return &___chainConstraintParameters_65; }
	inline void set_chainConstraintParameters_65(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22  value)
	{
		___chainConstraintParameters_65 = value;
	}

	inline static int32_t get_offset_of_m_RigidbodyLinearVelocities_66() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_RigidbodyLinearVelocities_66)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_RigidbodyLinearVelocities_66() const { return ___m_RigidbodyLinearVelocities_66; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_RigidbodyLinearVelocities_66() { return &___m_RigidbodyLinearVelocities_66; }
	inline void set_m_RigidbodyLinearVelocities_66(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_RigidbodyLinearVelocities_66 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RigidbodyLinearVelocities_66), (void*)value);
	}

	inline static int32_t get_offset_of_m_RigidbodyAngularVelocities_67() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_RigidbodyAngularVelocities_67)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_RigidbodyAngularVelocities_67() const { return ___m_RigidbodyAngularVelocities_67; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_RigidbodyAngularVelocities_67() { return &___m_RigidbodyAngularVelocities_67; }
	inline void set_m_RigidbodyAngularVelocities_67(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_RigidbodyAngularVelocities_67 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RigidbodyAngularVelocities_67), (void*)value);
	}

	inline static int32_t get_offset_of_m_Colors_68() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Colors_68)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get_m_Colors_68() const { return ___m_Colors_68; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of_m_Colors_68() { return &___m_Colors_68; }
	inline void set_m_Colors_68(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		___m_Colors_68 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Colors_68), (void*)value);
	}

	inline static int32_t get_offset_of_m_CellCoords_69() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_CellCoords_69)); }
	inline ObiNativeInt4List_t77B698CA82EEEFCB8C86017B00EABC6A4C3C0344 * get_m_CellCoords_69() const { return ___m_CellCoords_69; }
	inline ObiNativeInt4List_t77B698CA82EEEFCB8C86017B00EABC6A4C3C0344 ** get_address_of_m_CellCoords_69() { return &___m_CellCoords_69; }
	inline void set_m_CellCoords_69(ObiNativeInt4List_t77B698CA82EEEFCB8C86017B00EABC6A4C3C0344 * value)
	{
		___m_CellCoords_69 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CellCoords_69), (void*)value);
	}

	inline static int32_t get_offset_of_m_Positions_70() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Positions_70)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_Positions_70() const { return ___m_Positions_70; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_Positions_70() { return &___m_Positions_70; }
	inline void set_m_Positions_70(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_Positions_70 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Positions_70), (void*)value);
	}

	inline static int32_t get_offset_of_m_RestPositions_71() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_RestPositions_71)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_RestPositions_71() const { return ___m_RestPositions_71; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_RestPositions_71() { return &___m_RestPositions_71; }
	inline void set_m_RestPositions_71(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_RestPositions_71 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RestPositions_71), (void*)value);
	}

	inline static int32_t get_offset_of_m_PrevPositions_72() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_PrevPositions_72)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_PrevPositions_72() const { return ___m_PrevPositions_72; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_PrevPositions_72() { return &___m_PrevPositions_72; }
	inline void set_m_PrevPositions_72(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_PrevPositions_72 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PrevPositions_72), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartPositions_73() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_StartPositions_73)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_StartPositions_73() const { return ___m_StartPositions_73; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_StartPositions_73() { return &___m_StartPositions_73; }
	inline void set_m_StartPositions_73(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_StartPositions_73 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StartPositions_73), (void*)value);
	}

	inline static int32_t get_offset_of_m_RenderablePositions_74() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_RenderablePositions_74)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_RenderablePositions_74() const { return ___m_RenderablePositions_74; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_RenderablePositions_74() { return &___m_RenderablePositions_74; }
	inline void set_m_RenderablePositions_74(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_RenderablePositions_74 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RenderablePositions_74), (void*)value);
	}

	inline static int32_t get_offset_of_m_Orientations_75() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Orientations_75)); }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * get_m_Orientations_75() const { return ___m_Orientations_75; }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 ** get_address_of_m_Orientations_75() { return &___m_Orientations_75; }
	inline void set_m_Orientations_75(ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * value)
	{
		___m_Orientations_75 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Orientations_75), (void*)value);
	}

	inline static int32_t get_offset_of_m_RestOrientations_76() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_RestOrientations_76)); }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * get_m_RestOrientations_76() const { return ___m_RestOrientations_76; }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 ** get_address_of_m_RestOrientations_76() { return &___m_RestOrientations_76; }
	inline void set_m_RestOrientations_76(ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * value)
	{
		___m_RestOrientations_76 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RestOrientations_76), (void*)value);
	}

	inline static int32_t get_offset_of_m_PrevOrientations_77() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_PrevOrientations_77)); }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * get_m_PrevOrientations_77() const { return ___m_PrevOrientations_77; }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 ** get_address_of_m_PrevOrientations_77() { return &___m_PrevOrientations_77; }
	inline void set_m_PrevOrientations_77(ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * value)
	{
		___m_PrevOrientations_77 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PrevOrientations_77), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartOrientations_78() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_StartOrientations_78)); }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * get_m_StartOrientations_78() const { return ___m_StartOrientations_78; }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 ** get_address_of_m_StartOrientations_78() { return &___m_StartOrientations_78; }
	inline void set_m_StartOrientations_78(ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * value)
	{
		___m_StartOrientations_78 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StartOrientations_78), (void*)value);
	}

	inline static int32_t get_offset_of_m_RenderableOrientations_79() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_RenderableOrientations_79)); }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * get_m_RenderableOrientations_79() const { return ___m_RenderableOrientations_79; }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 ** get_address_of_m_RenderableOrientations_79() { return &___m_RenderableOrientations_79; }
	inline void set_m_RenderableOrientations_79(ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * value)
	{
		___m_RenderableOrientations_79 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RenderableOrientations_79), (void*)value);
	}

	inline static int32_t get_offset_of_m_Velocities_80() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Velocities_80)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_Velocities_80() const { return ___m_Velocities_80; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_Velocities_80() { return &___m_Velocities_80; }
	inline void set_m_Velocities_80(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_Velocities_80 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Velocities_80), (void*)value);
	}

	inline static int32_t get_offset_of_m_AngularVelocities_81() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_AngularVelocities_81)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_AngularVelocities_81() const { return ___m_AngularVelocities_81; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_AngularVelocities_81() { return &___m_AngularVelocities_81; }
	inline void set_m_AngularVelocities_81(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_AngularVelocities_81 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AngularVelocities_81), (void*)value);
	}

	inline static int32_t get_offset_of_m_InvMasses_82() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_InvMasses_82)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_InvMasses_82() const { return ___m_InvMasses_82; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_InvMasses_82() { return &___m_InvMasses_82; }
	inline void set_m_InvMasses_82(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_InvMasses_82 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvMasses_82), (void*)value);
	}

	inline static int32_t get_offset_of_m_InvRotationalMasses_83() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_InvRotationalMasses_83)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_InvRotationalMasses_83() const { return ___m_InvRotationalMasses_83; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_InvRotationalMasses_83() { return &___m_InvRotationalMasses_83; }
	inline void set_m_InvRotationalMasses_83(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_InvRotationalMasses_83 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvRotationalMasses_83), (void*)value);
	}

	inline static int32_t get_offset_of_m_InvInertiaTensors_84() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_InvInertiaTensors_84)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_InvInertiaTensors_84() const { return ___m_InvInertiaTensors_84; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_InvInertiaTensors_84() { return &___m_InvInertiaTensors_84; }
	inline void set_m_InvInertiaTensors_84(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_InvInertiaTensors_84 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvInertiaTensors_84), (void*)value);
	}

	inline static int32_t get_offset_of_m_ExternalForces_85() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_ExternalForces_85)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_ExternalForces_85() const { return ___m_ExternalForces_85; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_ExternalForces_85() { return &___m_ExternalForces_85; }
	inline void set_m_ExternalForces_85(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_ExternalForces_85 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExternalForces_85), (void*)value);
	}

	inline static int32_t get_offset_of_m_ExternalTorques_86() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_ExternalTorques_86)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_ExternalTorques_86() const { return ___m_ExternalTorques_86; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_ExternalTorques_86() { return &___m_ExternalTorques_86; }
	inline void set_m_ExternalTorques_86(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_ExternalTorques_86 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExternalTorques_86), (void*)value);
	}

	inline static int32_t get_offset_of_m_Wind_87() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Wind_87)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_Wind_87() const { return ___m_Wind_87; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_Wind_87() { return &___m_Wind_87; }
	inline void set_m_Wind_87(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_Wind_87 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Wind_87), (void*)value);
	}

	inline static int32_t get_offset_of_m_PositionDeltas_88() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_PositionDeltas_88)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_PositionDeltas_88() const { return ___m_PositionDeltas_88; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_PositionDeltas_88() { return &___m_PositionDeltas_88; }
	inline void set_m_PositionDeltas_88(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_PositionDeltas_88 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PositionDeltas_88), (void*)value);
	}

	inline static int32_t get_offset_of_m_OrientationDeltas_89() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_OrientationDeltas_89)); }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * get_m_OrientationDeltas_89() const { return ___m_OrientationDeltas_89; }
	inline ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 ** get_address_of_m_OrientationDeltas_89() { return &___m_OrientationDeltas_89; }
	inline void set_m_OrientationDeltas_89(ObiNativeQuaternionList_tAB96F2897F7D3D41115AEEEE7B503BB0EC6477F4 * value)
	{
		___m_OrientationDeltas_89 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OrientationDeltas_89), (void*)value);
	}

	inline static int32_t get_offset_of_m_PositionConstraintCounts_90() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_PositionConstraintCounts_90)); }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * get_m_PositionConstraintCounts_90() const { return ___m_PositionConstraintCounts_90; }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 ** get_address_of_m_PositionConstraintCounts_90() { return &___m_PositionConstraintCounts_90; }
	inline void set_m_PositionConstraintCounts_90(ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * value)
	{
		___m_PositionConstraintCounts_90 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PositionConstraintCounts_90), (void*)value);
	}

	inline static int32_t get_offset_of_m_OrientationConstraintCounts_91() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_OrientationConstraintCounts_91)); }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * get_m_OrientationConstraintCounts_91() const { return ___m_OrientationConstraintCounts_91; }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 ** get_address_of_m_OrientationConstraintCounts_91() { return &___m_OrientationConstraintCounts_91; }
	inline void set_m_OrientationConstraintCounts_91(ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * value)
	{
		___m_OrientationConstraintCounts_91 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OrientationConstraintCounts_91), (void*)value);
	}

	inline static int32_t get_offset_of_m_CollisionMaterials_92() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_CollisionMaterials_92)); }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * get_m_CollisionMaterials_92() const { return ___m_CollisionMaterials_92; }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 ** get_address_of_m_CollisionMaterials_92() { return &___m_CollisionMaterials_92; }
	inline void set_m_CollisionMaterials_92(ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * value)
	{
		___m_CollisionMaterials_92 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CollisionMaterials_92), (void*)value);
	}

	inline static int32_t get_offset_of_m_Phases_93() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Phases_93)); }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * get_m_Phases_93() const { return ___m_Phases_93; }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 ** get_address_of_m_Phases_93() { return &___m_Phases_93; }
	inline void set_m_Phases_93(ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * value)
	{
		___m_Phases_93 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Phases_93), (void*)value);
	}

	inline static int32_t get_offset_of_m_Filters_94() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Filters_94)); }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * get_m_Filters_94() const { return ___m_Filters_94; }
	inline ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 ** get_address_of_m_Filters_94() { return &___m_Filters_94; }
	inline void set_m_Filters_94(ObiNativeIntList_tA1AE4AD27E8B05C94EFCD9047A57362986597D13 * value)
	{
		___m_Filters_94 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Filters_94), (void*)value);
	}

	inline static int32_t get_offset_of_m_Anisotropies_95() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Anisotropies_95)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_Anisotropies_95() const { return ___m_Anisotropies_95; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_Anisotropies_95() { return &___m_Anisotropies_95; }
	inline void set_m_Anisotropies_95(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_Anisotropies_95 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Anisotropies_95), (void*)value);
	}

	inline static int32_t get_offset_of_m_PrincipalRadii_96() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_PrincipalRadii_96)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_PrincipalRadii_96() const { return ___m_PrincipalRadii_96; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_PrincipalRadii_96() { return &___m_PrincipalRadii_96; }
	inline void set_m_PrincipalRadii_96(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_PrincipalRadii_96 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PrincipalRadii_96), (void*)value);
	}

	inline static int32_t get_offset_of_m_Normals_97() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Normals_97)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_Normals_97() const { return ___m_Normals_97; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_Normals_97() { return &___m_Normals_97; }
	inline void set_m_Normals_97(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_Normals_97 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Normals_97), (void*)value);
	}

	inline static int32_t get_offset_of_m_Vorticities_98() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Vorticities_98)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_Vorticities_98() const { return ___m_Vorticities_98; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_Vorticities_98() { return &___m_Vorticities_98; }
	inline void set_m_Vorticities_98(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_Vorticities_98 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Vorticities_98), (void*)value);
	}

	inline static int32_t get_offset_of_m_FluidData_99() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_FluidData_99)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_FluidData_99() const { return ___m_FluidData_99; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_FluidData_99() { return &___m_FluidData_99; }
	inline void set_m_FluidData_99(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_FluidData_99 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FluidData_99), (void*)value);
	}

	inline static int32_t get_offset_of_m_UserData_100() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_UserData_100)); }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * get_m_UserData_100() const { return ___m_UserData_100; }
	inline ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 ** get_address_of_m_UserData_100() { return &___m_UserData_100; }
	inline void set_m_UserData_100(ObiNativeVector4List_t1D2CEC09E4374E4868439571D9CCE925FBC1AAF1 * value)
	{
		___m_UserData_100 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UserData_100), (void*)value);
	}

	inline static int32_t get_offset_of_m_SmoothingRadii_101() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_SmoothingRadii_101)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_SmoothingRadii_101() const { return ___m_SmoothingRadii_101; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_SmoothingRadii_101() { return &___m_SmoothingRadii_101; }
	inline void set_m_SmoothingRadii_101(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_SmoothingRadii_101 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SmoothingRadii_101), (void*)value);
	}

	inline static int32_t get_offset_of_m_Buoyancies_102() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Buoyancies_102)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_Buoyancies_102() const { return ___m_Buoyancies_102; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_Buoyancies_102() { return &___m_Buoyancies_102; }
	inline void set_m_Buoyancies_102(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_Buoyancies_102 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Buoyancies_102), (void*)value);
	}

	inline static int32_t get_offset_of_m_RestDensities_103() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_RestDensities_103)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_RestDensities_103() const { return ___m_RestDensities_103; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_RestDensities_103() { return &___m_RestDensities_103; }
	inline void set_m_RestDensities_103(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_RestDensities_103 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RestDensities_103), (void*)value);
	}

	inline static int32_t get_offset_of_m_Viscosities_104() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Viscosities_104)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_Viscosities_104() const { return ___m_Viscosities_104; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_Viscosities_104() { return &___m_Viscosities_104; }
	inline void set_m_Viscosities_104(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_Viscosities_104 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Viscosities_104), (void*)value);
	}

	inline static int32_t get_offset_of_m_SurfaceTension_105() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_SurfaceTension_105)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_SurfaceTension_105() const { return ___m_SurfaceTension_105; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_SurfaceTension_105() { return &___m_SurfaceTension_105; }
	inline void set_m_SurfaceTension_105(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_SurfaceTension_105 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SurfaceTension_105), (void*)value);
	}

	inline static int32_t get_offset_of_m_VortConfinement_106() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_VortConfinement_106)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_VortConfinement_106() const { return ___m_VortConfinement_106; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_VortConfinement_106() { return &___m_VortConfinement_106; }
	inline void set_m_VortConfinement_106(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_VortConfinement_106 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VortConfinement_106), (void*)value);
	}

	inline static int32_t get_offset_of_m_AtmosphericDrag_107() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_AtmosphericDrag_107)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_AtmosphericDrag_107() const { return ___m_AtmosphericDrag_107; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_AtmosphericDrag_107() { return &___m_AtmosphericDrag_107; }
	inline void set_m_AtmosphericDrag_107(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_AtmosphericDrag_107 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AtmosphericDrag_107), (void*)value);
	}

	inline static int32_t get_offset_of_m_AtmosphericPressure_108() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_AtmosphericPressure_108)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_AtmosphericPressure_108() const { return ___m_AtmosphericPressure_108; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_AtmosphericPressure_108() { return &___m_AtmosphericPressure_108; }
	inline void set_m_AtmosphericPressure_108(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_AtmosphericPressure_108 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AtmosphericPressure_108), (void*)value);
	}

	inline static int32_t get_offset_of_m_Diffusion_109() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634, ___m_Diffusion_109)); }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * get_m_Diffusion_109() const { return ___m_Diffusion_109; }
	inline ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 ** get_address_of_m_Diffusion_109() { return &___m_Diffusion_109; }
	inline void set_m_Diffusion_109(ObiNativeFloatList_tA4863A5AB5A81299F4684E134F63FA0B7BBAA311 * value)
	{
		___m_Diffusion_109 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Diffusion_109), (void*)value);
	}
};

struct ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_StaticFields
{
public:
	// Unity.Profiling.ProfilerMarker Obi.ObiSolver::m_StateInterpolationPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___m_StateInterpolationPerfMarker_4;
	// Unity.Profiling.ProfilerMarker Obi.ObiSolver::m_UpdateVisibilityPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___m_UpdateVisibilityPerfMarker_5;
	// Unity.Profiling.ProfilerMarker Obi.ObiSolver::m_GetSolverBoundsPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___m_GetSolverBoundsPerfMarker_6;
	// Unity.Profiling.ProfilerMarker Obi.ObiSolver::m_TestBoundsPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___m_TestBoundsPerfMarker_7;
	// Unity.Profiling.ProfilerMarker Obi.ObiSolver::m_GetAllCamerasPerfMarker
	ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  ___m_GetAllCamerasPerfMarker_8;

public:
	inline static int32_t get_offset_of_m_StateInterpolationPerfMarker_4() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_StaticFields, ___m_StateInterpolationPerfMarker_4)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_m_StateInterpolationPerfMarker_4() const { return ___m_StateInterpolationPerfMarker_4; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_m_StateInterpolationPerfMarker_4() { return &___m_StateInterpolationPerfMarker_4; }
	inline void set_m_StateInterpolationPerfMarker_4(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___m_StateInterpolationPerfMarker_4 = value;
	}

	inline static int32_t get_offset_of_m_UpdateVisibilityPerfMarker_5() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_StaticFields, ___m_UpdateVisibilityPerfMarker_5)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_m_UpdateVisibilityPerfMarker_5() const { return ___m_UpdateVisibilityPerfMarker_5; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_m_UpdateVisibilityPerfMarker_5() { return &___m_UpdateVisibilityPerfMarker_5; }
	inline void set_m_UpdateVisibilityPerfMarker_5(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___m_UpdateVisibilityPerfMarker_5 = value;
	}

	inline static int32_t get_offset_of_m_GetSolverBoundsPerfMarker_6() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_StaticFields, ___m_GetSolverBoundsPerfMarker_6)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_m_GetSolverBoundsPerfMarker_6() const { return ___m_GetSolverBoundsPerfMarker_6; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_m_GetSolverBoundsPerfMarker_6() { return &___m_GetSolverBoundsPerfMarker_6; }
	inline void set_m_GetSolverBoundsPerfMarker_6(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___m_GetSolverBoundsPerfMarker_6 = value;
	}

	inline static int32_t get_offset_of_m_TestBoundsPerfMarker_7() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_StaticFields, ___m_TestBoundsPerfMarker_7)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_m_TestBoundsPerfMarker_7() const { return ___m_TestBoundsPerfMarker_7; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_m_TestBoundsPerfMarker_7() { return &___m_TestBoundsPerfMarker_7; }
	inline void set_m_TestBoundsPerfMarker_7(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___m_TestBoundsPerfMarker_7 = value;
	}

	inline static int32_t get_offset_of_m_GetAllCamerasPerfMarker_8() { return static_cast<int32_t>(offsetof(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_StaticFields, ___m_GetAllCamerasPerfMarker_8)); }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  get_m_GetAllCamerasPerfMarker_8() const { return ___m_GetAllCamerasPerfMarker_8; }
	inline ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1 * get_address_of_m_GetAllCamerasPerfMarker_8() { return &___m_GetAllCamerasPerfMarker_8; }
	inline void set_m_GetAllCamerasPerfMarker_8(ProfilerMarker_tAE86534C80C5D67768DB3B244D8D139A2E6495E1  value)
	{
		___m_GetAllCamerasPerfMarker_8 = value;
	}
};


// Obi.ObiRopeBase
struct ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7  : public ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6
{
public:
	// System.Boolean Obi.ObiRopeBase::m_SelfCollisions
	bool ___m_SelfCollisions_22;
	// System.Single Obi.ObiRopeBase::restLength_
	float ___restLength__23;
	// System.Collections.Generic.List`1<Obi.ObiStructuralElement> Obi.ObiRopeBase::elements
	List_1_tC53BC7C82E27162EC05E16FB1845BC506EE2A03B * ___elements_24;
	// Obi.ObiActor/ActorCallback Obi.ObiRopeBase::OnElementsGenerated
	ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF * ___OnElementsGenerated_25;

public:
	inline static int32_t get_offset_of_m_SelfCollisions_22() { return static_cast<int32_t>(offsetof(ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7, ___m_SelfCollisions_22)); }
	inline bool get_m_SelfCollisions_22() const { return ___m_SelfCollisions_22; }
	inline bool* get_address_of_m_SelfCollisions_22() { return &___m_SelfCollisions_22; }
	inline void set_m_SelfCollisions_22(bool value)
	{
		___m_SelfCollisions_22 = value;
	}

	inline static int32_t get_offset_of_restLength__23() { return static_cast<int32_t>(offsetof(ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7, ___restLength__23)); }
	inline float get_restLength__23() const { return ___restLength__23; }
	inline float* get_address_of_restLength__23() { return &___restLength__23; }
	inline void set_restLength__23(float value)
	{
		___restLength__23 = value;
	}

	inline static int32_t get_offset_of_elements_24() { return static_cast<int32_t>(offsetof(ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7, ___elements_24)); }
	inline List_1_tC53BC7C82E27162EC05E16FB1845BC506EE2A03B * get_elements_24() const { return ___elements_24; }
	inline List_1_tC53BC7C82E27162EC05E16FB1845BC506EE2A03B ** get_address_of_elements_24() { return &___elements_24; }
	inline void set_elements_24(List_1_tC53BC7C82E27162EC05E16FB1845BC506EE2A03B * value)
	{
		___elements_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___elements_24), (void*)value);
	}

	inline static int32_t get_offset_of_OnElementsGenerated_25() { return static_cast<int32_t>(offsetof(ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7, ___OnElementsGenerated_25)); }
	inline ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF * get_OnElementsGenerated_25() const { return ___OnElementsGenerated_25; }
	inline ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF ** get_address_of_OnElementsGenerated_25() { return &___OnElementsGenerated_25; }
	inline void set_OnElementsGenerated_25(ActorCallback_tAA3B372A3175143318D7A78E70D714FCCA749BCF * value)
	{
		___OnElementsGenerated_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnElementsGenerated_25), (void*)value);
	}
};


// Obi.ObiRope
struct ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464  : public ObiRopeBase_tF3D5BB7AF3961638A6C368259A916AF7AB9D05F7
{
public:
	// Obi.ObiRopeBlueprint Obi.ObiRope::m_RopeBlueprint
	ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * ___m_RopeBlueprint_26;
	// Obi.ObiRopeBlueprint Obi.ObiRope::m_RopeBlueprintInstance
	ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * ___m_RopeBlueprintInstance_27;
	// System.Boolean Obi.ObiRope::tearingEnabled
	bool ___tearingEnabled_28;
	// System.Single Obi.ObiRope::tearResistanceMultiplier
	float ___tearResistanceMultiplier_29;
	// System.Int32 Obi.ObiRope::tearRate
	int32_t ___tearRate_30;
	// System.Boolean Obi.ObiRope::_distanceConstraintsEnabled
	bool ____distanceConstraintsEnabled_31;
	// System.Single Obi.ObiRope::_stretchingScale
	float ____stretchingScale_32;
	// System.Single Obi.ObiRope::_stretchCompliance
	float ____stretchCompliance_33;
	// System.Single Obi.ObiRope::_maxCompression
	float ____maxCompression_34;
	// System.Boolean Obi.ObiRope::_bendConstraintsEnabled
	bool ____bendConstraintsEnabled_35;
	// System.Single Obi.ObiRope::_bendCompliance
	float ____bendCompliance_36;
	// System.Single Obi.ObiRope::_maxBending
	float ____maxBending_37;
	// System.Single Obi.ObiRope::_plasticYield
	float ____plasticYield_38;
	// System.Single Obi.ObiRope::_plasticCreep
	float ____plasticCreep_39;
	// Obi.ObiRope/RopeTornCallback Obi.ObiRope::OnRopeTorn
	RopeTornCallback_t7F7EC536306AFEEE07B7A037C3CC566AD1AF8D58 * ___OnRopeTorn_40;

public:
	inline static int32_t get_offset_of_m_RopeBlueprint_26() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ___m_RopeBlueprint_26)); }
	inline ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * get_m_RopeBlueprint_26() const { return ___m_RopeBlueprint_26; }
	inline ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B ** get_address_of_m_RopeBlueprint_26() { return &___m_RopeBlueprint_26; }
	inline void set_m_RopeBlueprint_26(ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * value)
	{
		___m_RopeBlueprint_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RopeBlueprint_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_RopeBlueprintInstance_27() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ___m_RopeBlueprintInstance_27)); }
	inline ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * get_m_RopeBlueprintInstance_27() const { return ___m_RopeBlueprintInstance_27; }
	inline ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B ** get_address_of_m_RopeBlueprintInstance_27() { return &___m_RopeBlueprintInstance_27; }
	inline void set_m_RopeBlueprintInstance_27(ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * value)
	{
		___m_RopeBlueprintInstance_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RopeBlueprintInstance_27), (void*)value);
	}

	inline static int32_t get_offset_of_tearingEnabled_28() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ___tearingEnabled_28)); }
	inline bool get_tearingEnabled_28() const { return ___tearingEnabled_28; }
	inline bool* get_address_of_tearingEnabled_28() { return &___tearingEnabled_28; }
	inline void set_tearingEnabled_28(bool value)
	{
		___tearingEnabled_28 = value;
	}

	inline static int32_t get_offset_of_tearResistanceMultiplier_29() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ___tearResistanceMultiplier_29)); }
	inline float get_tearResistanceMultiplier_29() const { return ___tearResistanceMultiplier_29; }
	inline float* get_address_of_tearResistanceMultiplier_29() { return &___tearResistanceMultiplier_29; }
	inline void set_tearResistanceMultiplier_29(float value)
	{
		___tearResistanceMultiplier_29 = value;
	}

	inline static int32_t get_offset_of_tearRate_30() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ___tearRate_30)); }
	inline int32_t get_tearRate_30() const { return ___tearRate_30; }
	inline int32_t* get_address_of_tearRate_30() { return &___tearRate_30; }
	inline void set_tearRate_30(int32_t value)
	{
		___tearRate_30 = value;
	}

	inline static int32_t get_offset_of__distanceConstraintsEnabled_31() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ____distanceConstraintsEnabled_31)); }
	inline bool get__distanceConstraintsEnabled_31() const { return ____distanceConstraintsEnabled_31; }
	inline bool* get_address_of__distanceConstraintsEnabled_31() { return &____distanceConstraintsEnabled_31; }
	inline void set__distanceConstraintsEnabled_31(bool value)
	{
		____distanceConstraintsEnabled_31 = value;
	}

	inline static int32_t get_offset_of__stretchingScale_32() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ____stretchingScale_32)); }
	inline float get__stretchingScale_32() const { return ____stretchingScale_32; }
	inline float* get_address_of__stretchingScale_32() { return &____stretchingScale_32; }
	inline void set__stretchingScale_32(float value)
	{
		____stretchingScale_32 = value;
	}

	inline static int32_t get_offset_of__stretchCompliance_33() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ____stretchCompliance_33)); }
	inline float get__stretchCompliance_33() const { return ____stretchCompliance_33; }
	inline float* get_address_of__stretchCompliance_33() { return &____stretchCompliance_33; }
	inline void set__stretchCompliance_33(float value)
	{
		____stretchCompliance_33 = value;
	}

	inline static int32_t get_offset_of__maxCompression_34() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ____maxCompression_34)); }
	inline float get__maxCompression_34() const { return ____maxCompression_34; }
	inline float* get_address_of__maxCompression_34() { return &____maxCompression_34; }
	inline void set__maxCompression_34(float value)
	{
		____maxCompression_34 = value;
	}

	inline static int32_t get_offset_of__bendConstraintsEnabled_35() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ____bendConstraintsEnabled_35)); }
	inline bool get__bendConstraintsEnabled_35() const { return ____bendConstraintsEnabled_35; }
	inline bool* get_address_of__bendConstraintsEnabled_35() { return &____bendConstraintsEnabled_35; }
	inline void set__bendConstraintsEnabled_35(bool value)
	{
		____bendConstraintsEnabled_35 = value;
	}

	inline static int32_t get_offset_of__bendCompliance_36() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ____bendCompliance_36)); }
	inline float get__bendCompliance_36() const { return ____bendCompliance_36; }
	inline float* get_address_of__bendCompliance_36() { return &____bendCompliance_36; }
	inline void set__bendCompliance_36(float value)
	{
		____bendCompliance_36 = value;
	}

	inline static int32_t get_offset_of__maxBending_37() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ____maxBending_37)); }
	inline float get__maxBending_37() const { return ____maxBending_37; }
	inline float* get_address_of__maxBending_37() { return &____maxBending_37; }
	inline void set__maxBending_37(float value)
	{
		____maxBending_37 = value;
	}

	inline static int32_t get_offset_of__plasticYield_38() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ____plasticYield_38)); }
	inline float get__plasticYield_38() const { return ____plasticYield_38; }
	inline float* get_address_of__plasticYield_38() { return &____plasticYield_38; }
	inline void set__plasticYield_38(float value)
	{
		____plasticYield_38 = value;
	}

	inline static int32_t get_offset_of__plasticCreep_39() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ____plasticCreep_39)); }
	inline float get__plasticCreep_39() const { return ____plasticCreep_39; }
	inline float* get_address_of__plasticCreep_39() { return &____plasticCreep_39; }
	inline void set__plasticCreep_39(float value)
	{
		____plasticCreep_39 = value;
	}

	inline static int32_t get_offset_of_OnRopeTorn_40() { return static_cast<int32_t>(offsetof(ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464, ___OnRopeTorn_40)); }
	inline RopeTornCallback_t7F7EC536306AFEEE07B7A037C3CC566AD1AF8D58 * get_OnRopeTorn_40() const { return ___OnRopeTorn_40; }
	inline RopeTornCallback_t7F7EC536306AFEEE07B7A037C3CC566AD1AF8D58 ** get_address_of_OnRopeTorn_40() { return &___OnRopeTorn_40; }
	inline void set_OnRopeTorn_40(RopeTornCallback_t7F7EC536306AFEEE07B7A037C3CC566AD1AF8D58 * value)
	{
		___OnRopeTorn_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRopeTorn_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  m_Items[1];

public:
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  m_Items[1];

public:
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  m_Items[1];

public:
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3Int[0...,0...,0...]
struct Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  m_Items[1];

public:
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		m_Items[index] = value;
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void Obi.ObiConstraints`1<System.Object>::AddBatch(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiConstraints_1_AddBatch_m9556C21A3470420258E7D6E18C48D81A7BF74333_gshared (ObiConstraints_1_tCEA141EDE83195DC4D0CD9725268B012EE741A3C * __this, RuntimeObject * ___batch0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C_gshared (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_gshared (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___item0, const RuntimeMethod* method);
// T Obi.ObiPathDataChannelIdentity`1<UnityEngine.Vector3>::GetAtMu(System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ObiPathDataChannelIdentity_1_GetAtMu_m169B9F9D1859E1C2E829CCC554D1DF4421230498_gshared (ObiPathDataChannelIdentity_1_t58918EE3CAC1576B050AFB8CF1148BD9E5F7B230 * __this, bool ___closed0, float ___mu1, const RuntimeMethod* method);
// T Obi.ObiPathDataChannelIdentity`1<System.Single>::GetAtMu(System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_gshared (ObiPathDataChannelIdentity_1_t472CD690CECDE58D7B1E3020921399E15844875C * __this, bool ___closed0, float ___mu1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Single>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_gshared (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * __this, float ___item0, const RuntimeMethod* method);
// T Obi.ObiPathDataChannelIdentity`1<System.Int32>::GetAtMu(System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB_gshared (ObiPathDataChannelIdentity_1_tB49FD768113A83C96CCE65F0A01E480360D13F86 * __this, bool ___closed0, float ___mu1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method);
// T Obi.ObiPathDataChannelIdentity`1<UnityEngine.Color>::GetAtMu(System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A_gshared (ObiPathDataChannelIdentity_1_tB099F9C735620BE0BE1A9FE2208986F0128A2104 * __this, bool ___closed0, float ___mu1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_gshared (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// !0 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F_gshared (ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_gshared_inline (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * __this, int32_t ___index0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_gshared_inline (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Obi.ObiList`1<Oni/Contact>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiList_1__ctor_m61E6FC08889B646E0B516B5BBD472DBB1D334AFB_gshared (ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Obi.ObiChainConstraintsData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiChainConstraintsData__ctor_m9D1C51412A1878F85D8C173777A60EC621A9FFDD (ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 * __this, const RuntimeMethod* method);
// System.Void Obi.ObiChainConstraintsBatch::.ctor(Obi.ObiChainConstraintsData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiChainConstraintsBatch__ctor_m8E8E6310A3DDA3527AFF7F0DF6A851F75B52AEDC (ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * __this, ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 * ___constraints0, const RuntimeMethod* method);
// System.Void Obi.ObiConstraints`1<Obi.ObiChainConstraintsBatch>::AddBatch(T)
inline void ObiConstraints_1_AddBatch_m7FA684039BC7B6D3CF623C4EED798390D3A88D49 (ObiConstraints_1_tC13134EAD7E5A4205F5834810FB89F47972C220E * __this, ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * ___batch0, const RuntimeMethod* method)
{
	((  void (*) (ObiConstraints_1_tC13134EAD7E5A4205F5834810FB89F47972C220E *, ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E *, const RuntimeMethod*))ObiConstraints_1_AddBatch_m9556C21A3470420258E7D6E18C48D81A7BF74333_gshared)(__this, ___batch0, method);
}
// System.Boolean Obi.ObiPath::get_Closed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856 (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// System.Void Obi.ObiChainConstraintsBatch::AddConstraint(System.Int32[],System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiChainConstraintsBatch_AddConstraint_m8615D25C5C8628CEE00BD28F9D2D465A635813CB (ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * __this, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___indices0, float ___restLength1, float ___stretchStiffness2, float ___compressionStiffness3, const RuntimeMethod* method);
// System.Int32 Obi.ObiConstraintsBatch::get_activeConstraintCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02 (ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127 * __this, const RuntimeMethod* method);
// System.Void Obi.ObiConstraintsBatch::set_activeConstraintCount(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A (ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void Obi.ObiStretchShearConstraintsData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiStretchShearConstraintsData__ctor_m81576073ED6B14D835150C4666A3588AEDBD29FC (ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * __this, const RuntimeMethod* method);
// System.Void Obi.ObiStretchShearConstraintsBatch::.ctor(Obi.ObiStretchShearConstraintsData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiStretchShearConstraintsBatch__ctor_m8927550DA897869840F27FAE27254EF2961AEFB7 (ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * __this, ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * ___constraints0, const RuntimeMethod* method);
// System.Void Obi.ObiConstraints`1<Obi.ObiStretchShearConstraintsBatch>::AddBatch(T)
inline void ObiConstraints_1_AddBatch_m72B86FE6FB5147CD79A80B53085BC52B930199CB (ObiConstraints_1_t9491848D3D98BFF80E3C3AAF710BCE40F4F51610 * __this, ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * ___batch0, const RuntimeMethod* method)
{
	((  void (*) (ObiConstraints_1_t9491848D3D98BFF80E3C3AAF710BCE40F4F51610 *, ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B *, const RuntimeMethod*))ObiConstraints_1_AddBatch_m9556C21A3470420258E7D6E18C48D81A7BF74333_gshared)(__this, ___batch0, method);
}
// System.Void Obi.ObiPathFrame::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiPathFrame_Reset_m6589829D261335ABE1D4733AD2BA58A2E8D5FF15 (ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<Obi.ObiStretchShearConstraintsBatch>::get_Item(System.Int32)
inline ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * List_1_get_Item_m073B5521BD7EB6A522C1981D63905CEB40AE6FD2_inline (List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * (*) (List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void UnityEngine.Vector2Int::.ctor(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2Int__ctor_mB2B73108B6DD3ADC1B515D7DD9116ECAC6833726_inline (Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector2Int::get_y()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline (Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector2Int::get_x()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline (Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Void Obi.ObiPathFrame::Transport(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiPathFrame_Transport_m0B1B3CA7069547BE12B49F7FDB4E67EEADC239D4 (ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___newPosition0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___newTangent1, float ___twist2, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, int32_t, const RuntimeMethod*))List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forward0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upwards1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702 (const RuntimeMethod* method);
// System.Void Obi.ObiStretchShearConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Int32,System.Single,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiStretchShearConstraintsBatch_AddConstraint_m3EDB080395E5167A460038CE0B00CEEC306C19D5 (ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * __this, Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___indices0, int32_t ___orientationIndex1, float ___restLength2, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___restOrientation3, const RuntimeMethod* method);
// System.Void Obi.CoroutineJob/ProgressInfo::.ctor(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1 (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * __this, String_t* ___userReadableInfo0, float ___progress1, const RuntimeMethod* method);
// System.Int32 Obi.ObiPath::get_ControlPointCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObiPath_get_ControlPointCount_m09869836E934AC2B253907636DDE6D72BA95D05F (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// System.Void Obi.ObiActorBlueprint::ClearParticleGroups(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiActorBlueprint_ClearParticleGroups_mBC589A70CC5E642C10FD126B482590D27FEC8B18 (ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * __this, bool ___saveImmediately0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (const RuntimeMethod* method);
// System.Int32 Obi.ObiUtils::MakeFilter(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3_inline (int32_t ___mask0, int32_t ___category1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E (const RuntimeMethod* method);
// System.Void Obi.ObiPath::InsertControlPoint(System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Int32,UnityEngine.Color,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiPath_InsertControlPoint_m978D7BDBC828882C6D3B1C85F029C6B1BC63BDDA (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, int32_t ___index0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___inTangentVector2, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___outTangentVector3, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___normal4, float ___mass5, float ___rotationalMass6, float ___thickness7, int32_t ___filter8, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color9, String_t* ___name10, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Matrix4x4_get_identity_mC91289718DDD3DDBE0A10551BDA59A446414A596 (const RuntimeMethod* method);
// System.Single Obi.ObiPath::RecalculateLenght(UnityEngine.Matrix4x4,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ObiPath_RecalculateLenght_mDF1CD450DBBA7B9823BA8090C340595338260C0E (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___referenceFrame0, float ___acc1, int32_t ___maxevals2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
inline void List_1__ctor_mF8F23D572031748AD428623AE16803455997E297 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor()
inline void List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, const RuntimeMethod*))List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
inline void List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1 (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E *, const RuntimeMethod*))List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_gshared)(__this, method);
}
// Obi.ObiPointsDataChannel Obi.ObiPath::get_points()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507 * ObiPath_get_points_m6F044F0A220D99C2C5184CB5FE1769BAED6FFD3C (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 Obi.ObiPointsDataChannel::GetPositionAtMu(System.Boolean,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ObiPointsDataChannel_GetPositionAtMu_m5DA1AF5F4A0C5EB6EDAE4AA729DAC149E3E17DF5 (ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507 * __this, bool ___closed0, float ___mu1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
inline void List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , const RuntimeMethod*))List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_gshared)(__this, ___item0, method);
}
// Obi.ObiNormalDataChannel Obi.ObiPath::get_normals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObiNormalDataChannel_tA3F5F99B539B56E0703FA0603B89DCC345AC3296 * ObiPath_get_normals_mD27D9F0689AB44FF831629FC2F0F7F2BC5EA08F6 (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// T Obi.ObiPathDataChannelIdentity`1<UnityEngine.Vector3>::GetAtMu(System.Boolean,System.Single)
inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ObiPathDataChannelIdentity_1_GetAtMu_m169B9F9D1859E1C2E829CCC554D1DF4421230498 (ObiPathDataChannelIdentity_1_t58918EE3CAC1576B050AFB8CF1148BD9E5F7B230 * __this, bool ___closed0, float ___mu1, const RuntimeMethod* method)
{
	return ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (ObiPathDataChannelIdentity_1_t58918EE3CAC1576B050AFB8CF1148BD9E5F7B230 *, bool, float, const RuntimeMethod*))ObiPathDataChannelIdentity_1_GetAtMu_m169B9F9D1859E1C2E829CCC554D1DF4421230498_gshared)(__this, ___closed0, ___mu1, method);
}
// Obi.ObiThicknessDataChannel Obi.ObiPath::get_thicknesses()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C * ObiPath_get_thicknesses_m435418A103FD1CC0D151EF3B75689969F0E23C8D (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// T Obi.ObiPathDataChannelIdentity`1<System.Single>::GetAtMu(System.Boolean,System.Single)
inline float ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3 (ObiPathDataChannelIdentity_1_t472CD690CECDE58D7B1E3020921399E15844875C * __this, bool ___closed0, float ___mu1, const RuntimeMethod* method)
{
	return ((  float (*) (ObiPathDataChannelIdentity_1_t472CD690CECDE58D7B1E3020921399E15844875C *, bool, float, const RuntimeMethod*))ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_gshared)(__this, ___closed0, ___mu1, method);
}
// System.Void System.Collections.Generic.List`1<System.Single>::Add(!0)
inline void List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * __this, float ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, float, const RuntimeMethod*))List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_gshared)(__this, ___item0, method);
}
// Obi.ObiMassDataChannel Obi.ObiPath::get_masses()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996 * ObiPath_get_masses_m563E313A7734C29F2476B68228BAB71731EBC326 (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// System.Single Obi.ObiUtils::MassToInvMass(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68_inline (float ___mass0, const RuntimeMethod* method);
// Obi.ObiRotationalMassDataChannel Obi.ObiPath::get_rotationalMasses()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObiRotationalMassDataChannel_tC062F8C628B38CBE7F635761D65A2BA348541E4B * ObiPath_get_rotationalMasses_m65A0959B14AEBD424FC35853DF6EBEE9A4D2648A (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// Obi.ObiPhaseDataChannel Obi.ObiPath::get_filters()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670 * ObiPath_get_filters_m6D02BA0F0D6E24496882A16B37C12B185D9CC028 (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// T Obi.ObiPathDataChannelIdentity`1<System.Int32>::GetAtMu(System.Boolean,System.Single)
inline int32_t ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB (ObiPathDataChannelIdentity_1_tB49FD768113A83C96CCE65F0A01E480360D13F86 * __this, bool ___closed0, float ___mu1, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ObiPathDataChannelIdentity_1_tB49FD768113A83C96CCE65F0A01E480360D13F86 *, bool, float, const RuntimeMethod*))ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB_gshared)(__this, ___closed0, ___mu1, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
inline void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared)(__this, ___item0, method);
}
// Obi.ObiColorDataChannel Obi.ObiPath::get_colors()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2 * ObiPath_get_colors_mB1315A257BB672D26575C3CEFD3FC642D65C8C6F (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// T Obi.ObiPathDataChannelIdentity`1<UnityEngine.Color>::GetAtMu(System.Boolean,System.Single)
inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A (ObiPathDataChannelIdentity_1_tB099F9C735620BE0BE1A9FE2208986F0128A2104 * __this, bool ___closed0, float ___mu1, const RuntimeMethod* method)
{
	return ((  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  (*) (ObiPathDataChannelIdentity_1_tB099F9C735620BE0BE1A9FE2208986F0128A2104 *, bool, float, const RuntimeMethod*))ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A_gshared)(__this, ___closed0, ___mu1, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
inline void List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E *, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 , const RuntimeMethod*))List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_gshared)(__this, ___item0, method);
}
// !0 System.Collections.Generic.List`1<Obi.ObiParticleGroup>::get_Item(System.Int32)
inline ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE * List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_inline (List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE * (*) (List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
inline void List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_gshared)(__this, method);
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single> Obi.ObiPath::get_ArcLengthTable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * ObiPath_get_ArcLengthTable_m473E96A0299C036CB1835FE04F20733B6D824C81 (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// System.Int32 Obi.ObiPath::GetSpanCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObiPath_GetSpanCount_m09199A6F5BB9A1619E0A8A7FB826FA1472B29623 (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// System.Int32 Obi.ObiPath::get_ArcLengthSamples()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObiPath_get_ArcLengthSamples_m3C36FC6DDE2FBD76AD4F46EB7C5A17FDC4B1D9BA (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// !0 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>::get_Item(System.Int32)
inline float ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F (ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  float (*) (ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF *, int32_t, const RuntimeMethod*))ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F_gshared)(__this, ___index0, method);
}
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E (float ___f0, const RuntimeMethod* method);
// System.Single Obi.ObiPath::GetMuAtLenght(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ObiPath_GetMuAtLenght_m9B36DC8C812923123B8D5F9D290BAD46CF555E6B (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, float ___length0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
inline int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline)(__this, method);
}
// System.Single Obi.ObiPath::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ObiPath_get_Length_m702A025AC3A2C08D2F9629728753916FA662A7E9 (ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32)
inline float List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_inline (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  float (*) (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, int32_t, const RuntimeMethod*))List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  Vector4_op_Implicit_mDCFA56E9D34979E1E2BFE6C2D61F1768D934A8EB (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector4_set_Item_m7552B288FF218CA023F0DFB971BBA30D0362006A (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_one_m9CDE5C456038B133ED94402673859EC37B1C1CCB (const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
inline int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline)(__this, ___index0, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_inline (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  (*) (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E *, int32_t, const RuntimeMethod*))List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_gshared_inline)(__this, ___index0, method);
}
// System.Void Obi.ObiRopeBlueprintBase::CreateSimplices(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiRopeBlueprintBase_CreateSimplices_mB064904BA09A240A79F509214EDD53AF206F9FDD (ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 * __this, int32_t ___numSegments0, const RuntimeMethod* method);
// System.Void Obi.ObiRope/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m4B4378CC0AB345511F93C07572A6368778B3AE8E (U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102 * __this, const RuntimeMethod* method);
// System.Int32 System.Single::CompareTo(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Single_CompareTo_m80B5B5A70A2343C3A8673F35635EBED4458109B4 (float* __this, float ___value0, const RuntimeMethod* method);
// System.Void Obi.ObiBendConstraintsData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiBendConstraintsData__ctor_mFE69ED43DBE4731FAAAF8C32658B04596342980A (ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * __this, const RuntimeMethod* method);
// System.Void Obi.ObiBendConstraintsBatch::.ctor(Obi.ObiBendConstraintsData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9 (ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * __this, ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * ___constraints0, const RuntimeMethod* method);
// System.Void Obi.ObiConstraints`1<Obi.ObiBendConstraintsBatch>::AddBatch(T)
inline void ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6 (ObiConstraints_1_t1A77FB453F8651AB273DB0DE943C8AF5AF981F82 * __this, ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * ___batch0, const RuntimeMethod* method)
{
	((  void (*) (ObiConstraints_1_t1A77FB453F8651AB273DB0DE943C8AF5AF981F82 *, ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 *, const RuntimeMethod*))ObiConstraints_1_AddBatch_m9556C21A3470420258E7D6E18C48D81A7BF74333_gshared)(__this, ___batch0, method);
}
// !0 System.Collections.Generic.List`1<Obi.ObiBendConstraintsBatch>::get_Item(System.Int32)
inline ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * List_1_get_Item_m9DF9BE8ACA130E6DB8F004222C3FC66DA116FC4D_inline (List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * (*) (List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void UnityEngine.Vector3Int::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector3Int::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Vector3Int_get_Item_mEF2FC37D6A65B456ABF7E75DE736A9286706A72D (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, int32_t ___index0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___v0, const RuntimeMethod* method);
// System.Single Obi.ObiUtils::RestBendingConstraint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float ObiUtils_RestBendingConstraint_mA2A2E588C9B5B6907DC2670CF3176A0441D95C78_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positionA0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positionB1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positionC2, const RuntimeMethod* method);
// System.Void Obi.ObiBendConstraintsBatch::AddConstraint(UnityEngine.Vector3Int,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiBendConstraintsBatch_AddConstraint_m59872A1ADDD39CF3FB2C303EA08FD4E6E9AFAA0C (ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___indices0, float ___restBend1, const RuntimeMethod* method);
// System.Void Obi.ObiDistanceConstraintsData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiDistanceConstraintsData__ctor_m10E8E535222BCC55670623BBB7DA36CAF7A2A95E (ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * __this, const RuntimeMethod* method);
// System.Void Obi.ObiDistanceConstraintsBatch::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiDistanceConstraintsBatch__ctor_mE47DA7819812295CB5E274C6A916E54E705A0EC3 (ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * __this, int32_t ___a0, const RuntimeMethod* method);
// System.Void Obi.ObiConstraints`1<Obi.ObiDistanceConstraintsBatch>::AddBatch(T)
inline void ObiConstraints_1_AddBatch_mF16FEB2CD535743D507C3E2F63782E8ED0FCFD11 (ObiConstraints_1_tDDB54978FB9AEE6BD8B391B96A13174F2E9966ED * __this, ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * ___batch0, const RuntimeMethod* method)
{
	((  void (*) (ObiConstraints_1_tDDB54978FB9AEE6BD8B391B96A13174F2E9966ED *, ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE *, const RuntimeMethod*))ObiConstraints_1_AddBatch_m9556C21A3470420258E7D6E18C48D81A7BF74333_gshared)(__this, ___batch0, method);
}
// !0 System.Collections.Generic.List`1<Obi.ObiDistanceConstraintsBatch>::get_Item(System.Int32)
inline ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * List_1_get_Item_m3F5E61F87B1504EBA4E43452FE85068B76A29181_inline (List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * (*) (List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void Obi.ObiDistanceConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiDistanceConstraintsBatch_AddConstraint_m118C1575A23FFAC933204048E9DD894F44CDD021 (ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * __this, Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  ___indices0, float ___restLength1, const RuntimeMethod* method);
// UnityEngine.Vector2Int UnityEngine.Vector2Int::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  Vector2Int_get_zero_m89F24AE488182BAB38B381B445B8868B3DD5A2AE (const RuntimeMethod* method);
// System.Void Obi.ObiSolver/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m2D8F2915236AB77A7C36A87C7761B5503DCA10F2 (U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void Obi.ObiList`1<Oni/Contact>::.ctor()
inline void ObiList_1__ctor_m61E6FC08889B646E0B516B5BBD472DBB1D334AFB (ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3 * __this, const RuntimeMethod* method)
{
	((  void (*) (ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3 *, const RuntimeMethod*))ObiList_1__ctor_m61E6FC08889B646E0B516B5BBD472DBB1D334AFB_gshared)(__this, method);
}
// System.Void System.EventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventArgs__ctor_m5ECB9A8ED0A9E2DBB1ED999BAC1CB44F4354E571 (EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * __this, const RuntimeMethod* method);
// System.Void Obi.ObiTriangleMeshContainer/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8 (U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * __this, const RuntimeMethod* method);
// System.Int32 System.Environment::get_CurrentManagedThreadId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D (const RuntimeMethod* method);
// System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<System.Object> Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method);
// System.Void Oni/ConstraintParameters::.ctor(System.Boolean,Oni/ConstraintParameters/EvaluationOrder,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566 (ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * __this, bool ___enabled0, int32_t ___order1, int32_t ___iterations2, const RuntimeMethod* method);
// System.Void Oni/SolverParameters::.ctor(Oni/SolverParameters/Interpolation,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3 (SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 * __this, int32_t ___interpolation0, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___gravity1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector3Int::get_x()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594 (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector3Int::get_y()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3 (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector3Int::get_z()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6 (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, const RuntimeMethod* method);
// Obi.MeshVoxelizer/Voxel Obi.MeshVoxelizer::get_Item(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MeshVoxelizer_get_Item_mC5F92567FD7C76447DF970069641190AE24C1356 (MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method);
// System.Int32 System.Array::GetLength(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB (RuntimeArray * __this, int32_t ___dimension0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::Max(System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_Max_mC299EEF1FD6084E41A182E2033790DB50DEF5B39 (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___values0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Log_mF7F3624FA030AB57AD8C1F4CAF084B2DCC99897A (float ___f0, float ___p1, const RuntimeMethod* method);
// System.Void Obi.VoxelDistanceField::JumpFloodPass(System.Int32,UnityEngine.Vector3Int[0...,0...,0...],UnityEngine.Vector3Int[0...,0...,0...])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoxelDistanceField_JumpFloodPass_m0CAE942EA7B7FB237160C57ABAC04D2F77198AC3 (VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * __this, int32_t ___stride0, Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* ___input1, Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* ___output2, const RuntimeMethod* method);
// System.Void Obi.VoxelPathFinder/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m7A494B57D1990F9D4FD4EF63281425ABEC97A888 (U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3Int::op_Equality(UnityEngine.Vector3Int,UnityEngine.Vector3Int)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3Int_op_Equality_m724847B7E7A484A1E6F598CEC2D77CDE8ECE49E7_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___lhs0, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3Int::op_Implicit(UnityEngine.Vector3Int)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3Int_op_Implicit_mD812DEDBDE886508E86FB3222BB9DDB4949B4475_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___v0, const RuntimeMethod* method);
// System.Single Obi.VoxelPathFinder/TargetVoxel::get_cost()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, const RuntimeMethod* method);
// System.Void Obi.VoxelPathFinder/TargetVoxel::.ctor(UnityEngine.Vector3Int,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___coordinates0, float ___distance1, float ___heuristic2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3Int::Equals(UnityEngine.Vector3Int)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3Int_Equals_m8BE683205BACD053B7EB560AB5B7EDE78B779C5F_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___other0, const RuntimeMethod* method);
// System.Boolean Obi.VoxelPathFinder/TargetVoxel::Equals(Obi.VoxelPathFinder/TargetVoxel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681 (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method);
// System.Int32 Obi.VoxelPathFinder/TargetVoxel::CompareTo(Obi.VoxelPathFinder/TargetVoxel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8 (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775 (float ___a0, float ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateChainConstraintsU3Ed__6__ctor_mCCD0039AE63FA7C9AF769CB6DA517174CFE80C80 (U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateChainConstraintsU3Ed__6_System_IDisposable_Dispose_m8D90A9C0F15B87BFEE0F460C6F3DA96A7595273C (U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCreateChainConstraintsU3Ed__6_MoveNext_m1D5D41CD0B98482AACC5F3425B74A62581E06E4A (U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiConstraints_1_AddBatch_m7FA684039BC7B6D3CF623C4EED798390D3A88D49_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	bool V_3 = false;
	int32_t G_B9_0 = 0;
	U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872 * G_B9_1 = NULL;
	int32_t G_B8_0 = 0;
	U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872 * G_B8_1 = NULL;
	int32_t G_B10_0 = 0;
	int32_t G_B10_1 = 0;
	U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872 * G_B10_2 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		goto IL_001b;
	}

IL_0014:
	{
		goto IL_013f;
	}

IL_0019:
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// chainConstraintsData = new ObiChainConstraintsData();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_3 = __this->get_U3CU3E4__this_2();
		ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 * L_4 = (ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 *)il2cpp_codegen_object_new(ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0_il2cpp_TypeInfo_var);
		ObiChainConstraintsData__ctor_m9D1C51412A1878F85D8C173777A60EC621A9FFDD(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_3)->set_chainConstraintsData_31(L_4);
		// var batch = new ObiChainConstraintsBatch();
		ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * L_5 = (ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E *)il2cpp_codegen_object_new(ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_il2cpp_TypeInfo_var);
		ObiChainConstraintsBatch__ctor_m8E8E6310A3DDA3527AFF7F0DF6A851F75B52AEDC(L_5, (ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 *)NULL, /*hidden argument*/NULL);
		__this->set_U3CbatchU3E5__1_3(L_5);
		// chainConstraintsData.AddBatch(batch);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_6 = __this->get_U3CU3E4__this_2();
		NullCheck(L_6);
		ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 * L_7 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_6)->get_chainConstraintsData_31();
		ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * L_8 = __this->get_U3CbatchU3E5__1_3();
		NullCheck(L_7);
		ObiConstraints_1_AddBatch_m7FA684039BC7B6D3CF623C4EED798390D3A88D49(L_7, L_8, /*hidden argument*/ObiConstraints_1_AddBatch_m7FA684039BC7B6D3CF623C4EED798390D3A88D49_RuntimeMethod_var);
		// int[] indices = new int[m_ActiveParticleCount + (path.Closed ? 1 : 0)];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_9 = __this->get_U3CU3E4__this_2();
		NullCheck(L_9);
		int32_t L_10 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_9)->get_m_ActiveParticleCount_6();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_11 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_12 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_11)->get_path_34();
		NullCheck(L_12);
		bool L_13;
		L_13 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_12, /*hidden argument*/NULL);
		G_B8_0 = L_10;
		G_B8_1 = __this;
		if (L_13)
		{
			G_B9_0 = L_10;
			G_B9_1 = __this;
			goto IL_0077;
		}
	}
	{
		G_B10_0 = 0;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_0078;
	}

IL_0077:
	{
		G_B10_0 = 1;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_0078:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_14 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)G_B10_1, (int32_t)G_B10_0)));
		NullCheck(G_B10_2);
		G_B10_2->set_U3CindicesU3E5__2_4(L_14);
		// for (int i = 0; i < m_ActiveParticleCount; ++i)
		__this->set_U3CiU3E5__3_5(0);
		goto IL_00af;
	}

IL_008c:
	{
		// indices[i] = i;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_15 = __this->get_U3CindicesU3E5__2_4();
		int32_t L_16 = __this->get_U3CiU3E5__3_5();
		int32_t L_17 = __this->get_U3CiU3E5__3_5();
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (int32_t)L_17);
		// for (int i = 0; i < m_ActiveParticleCount; ++i)
		int32_t L_18 = __this->get_U3CiU3E5__3_5();
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
		int32_t L_19 = V_1;
		__this->set_U3CiU3E5__3_5(L_19);
	}

IL_00af:
	{
		// for (int i = 0; i < m_ActiveParticleCount; ++i)
		int32_t L_20 = __this->get_U3CiU3E5__3_5();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_21 = __this->get_U3CU3E4__this_2();
		NullCheck(L_21);
		int32_t L_22 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_21)->get_m_ActiveParticleCount_6();
		V_2 = (bool)((((int32_t)L_20) < ((int32_t)L_22))? 1 : 0);
		bool L_23 = V_2;
		if (L_23)
		{
			goto IL_008c;
		}
	}
	{
		// if (path.Closed)
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_24 = __this->get_U3CU3E4__this_2();
		NullCheck(L_24);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_25 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_24)->get_path_34();
		NullCheck(L_25);
		bool L_26;
		L_26 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_25, /*hidden argument*/NULL);
		V_3 = L_26;
		bool L_27 = V_3;
		if (!L_27)
		{
			goto IL_00ed;
		}
	}
	{
		// indices[m_ActiveParticleCount] = 0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_28 = __this->get_U3CindicesU3E5__2_4();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_29 = __this->get_U3CU3E4__this_2();
		NullCheck(L_29);
		int32_t L_30 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_29)->get_m_ActiveParticleCount_6();
		NullCheck(L_28);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(L_30), (int32_t)0);
	}

IL_00ed:
	{
		// batch.AddConstraint(indices, m_InterParticleDistance, 1, 1);
		ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * L_31 = __this->get_U3CbatchU3E5__1_3();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_32 = __this->get_U3CindicesU3E5__2_4();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_33 = __this->get_U3CU3E4__this_2();
		NullCheck(L_33);
		float L_34 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_33)->get_m_InterParticleDistance_37();
		NullCheck(L_31);
		ObiChainConstraintsBatch_AddConstraint_m8615D25C5C8628CEE00BD28F9D2D465A635813CB(L_31, L_32, L_34, (1.0f), (1.0f), /*hidden argument*/NULL);
		// batch.activeConstraintCount++;
		ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * L_35 = __this->get_U3CbatchU3E5__1_3();
		ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E * L_36 = L_35;
		NullCheck(L_36);
		int32_t L_37;
		L_37 = ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02(L_36, /*hidden argument*/NULL);
		V_1 = L_37;
		int32_t L_38 = V_1;
		NullCheck(L_36);
		ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A(L_36, ((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1)), /*hidden argument*/NULL);
		// yield return 0;
		int32_t L_39 = 0;
		RuntimeObject * L_40 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_39);
		__this->set_U3CU3E2__current_1(L_40);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_013f:
	{
		__this->set_U3CU3E1__state_0((-1));
		// }
		return (bool)0;
	}
}
// System.Object Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateChainConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m15E671F86EEE570870E5A45BF66B72D282E1CBD5 (U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_m394694E12A8EAEEB4EA80649BEA9BBE861C306FB (U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_m394694E12A8EAEEB4EA80649BEA9BBE861C306FB_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiRodBlueprint/<CreateChainConstraints>d__6::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateChainConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_mC6B36F52F2460C2F7E972D940E2F69B588832FED (U3CCreateChainConstraintsU3Ed__6_t1B3E2B9C58B28112D852BCA67EAE7C7C5A0AB872 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateStretchShearConstraintsU3Ed__4__ctor_mDFFBA8618ED30102037B3C56180A9BEA9487C330 (U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateStretchShearConstraintsU3Ed__4_System_IDisposable_Dispose_m89DF329DFEB0C57A2E9159B524D238E75D784621 (U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCreateStretchShearConstraintsU3Ed__4_MoveNext_m35DE1D56E478B7D4BBA5ECF95C387263211AADF8 (U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m073B5521BD7EB6A522C1981D63905CEB40AE6FD2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiConstraints_1_AddBatch_m72B86FE6FB5147CD79A80B53085BC52B930199CB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral000FEC8A07B6CA30D8852E9E25C3EE48061B49FA);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		goto IL_001b;
	}

IL_0014:
	{
		goto IL_02ad;
	}

IL_0019:
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// stretchShearConstraintsData = new ObiStretchShearConstraintsData();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_3 = __this->get_U3CU3E4__this_3();
		ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * L_4 = (ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC *)il2cpp_codegen_object_new(ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC_il2cpp_TypeInfo_var);
		ObiStretchShearConstraintsData__ctor_m81576073ED6B14D835150C4666A3588AEDBD29FC(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_3)->set_stretchShearConstraintsData_27(L_4);
		// stretchShearConstraintsData.AddBatch(new ObiStretchShearConstraintsBatch());
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_5 = __this->get_U3CU3E4__this_3();
		NullCheck(L_5);
		ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * L_6 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_5)->get_stretchShearConstraintsData_27();
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_7 = (ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B *)il2cpp_codegen_object_new(ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_il2cpp_TypeInfo_var);
		ObiStretchShearConstraintsBatch__ctor_m8927550DA897869840F27FAE27254EF2961AEFB7(L_7, (ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC *)NULL, /*hidden argument*/NULL);
		NullCheck(L_6);
		ObiConstraints_1_AddBatch_m72B86FE6FB5147CD79A80B53085BC52B930199CB(L_6, L_7, /*hidden argument*/ObiConstraints_1_AddBatch_m72B86FE6FB5147CD79A80B53085BC52B930199CB_RuntimeMethod_var);
		// stretchShearConstraintsData.AddBatch(new ObiStretchShearConstraintsBatch());
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_8 = __this->get_U3CU3E4__this_3();
		NullCheck(L_8);
		ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * L_9 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_8)->get_stretchShearConstraintsData_27();
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_10 = (ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B *)il2cpp_codegen_object_new(ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_il2cpp_TypeInfo_var);
		ObiStretchShearConstraintsBatch__ctor_m8927550DA897869840F27FAE27254EF2961AEFB7(L_10, (ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC *)NULL, /*hidden argument*/NULL);
		NullCheck(L_9);
		ObiConstraints_1_AddBatch_m72B86FE6FB5147CD79A80B53085BC52B930199CB(L_9, L_10, /*hidden argument*/ObiConstraints_1_AddBatch_m72B86FE6FB5147CD79A80B53085BC52B930199CB_RuntimeMethod_var);
		// ObiPathFrame frame = new ObiPathFrame();
		ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 * L_11 = __this->get_address_of_U3CframeU3E5__1_4();
		il2cpp_codegen_initobj(L_11, sizeof(ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 ));
		// frame.Reset();
		ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 * L_12 = __this->get_address_of_U3CframeU3E5__1_4();
		ObiPathFrame_Reset_m6589829D261335ABE1D4733AD2BA58A2E8D5FF15((ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 *)L_12, /*hidden argument*/NULL);
		// for (int i = 0; i < totalParticles - 1; i++)
		__this->set_U3CiU3E5__2_5(0);
		goto IL_02e4;
	}

IL_0085:
	{
		// var batch = stretchShearConstraintsData.batches[i % 2] as ObiStretchShearConstraintsBatch;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_13 = __this->get_U3CU3E4__this_3();
		NullCheck(L_13);
		ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * L_14 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_13)->get_stretchShearConstraintsData_27();
		NullCheck(L_14);
		List_1_tA6427D1689287A7A2E8CEA26AE3EEB966A7B1FFC * L_15 = ((ObiConstraints_1_t9491848D3D98BFF80E3C3AAF710BCE40F4F51610 *)L_14)->get_batches_1();
		int32_t L_16 = __this->get_U3CiU3E5__2_5();
		NullCheck(L_15);
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_17;
		L_17 = List_1_get_Item_m073B5521BD7EB6A522C1981D63905CEB40AE6FD2_inline(L_15, ((int32_t)((int32_t)L_16%(int32_t)2)), /*hidden argument*/List_1_get_Item_m073B5521BD7EB6A522C1981D63905CEB40AE6FD2_RuntimeMethod_var);
		__this->set_U3CbatchU3E5__3_6(L_17);
		// Vector2Int indices = new Vector2Int(i, i + 1);
		int32_t L_18 = __this->get_U3CiU3E5__2_5();
		int32_t L_19 = __this->get_U3CiU3E5__2_5();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Vector2Int__ctor_mB2B73108B6DD3ADC1B515D7DD9116ECAC6833726_inline((&L_20), L_18, ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U3CindicesU3E5__4_7(L_20);
		// Vector3 d = positions[indices.y] - positions[indices.x];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_21 = __this->get_U3CU3E4__this_3();
		NullCheck(L_21);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_22 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_21)->get_positions_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_23 = __this->get_address_of_U3CindicesU3E5__4_7();
		int32_t L_24;
		L_24 = Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		int32_t L_25 = L_24;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_27 = __this->get_U3CU3E4__this_3();
		NullCheck(L_27);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_28 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_27)->get_positions_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_29 = __this->get_address_of_U3CindicesU3E5__4_7();
		int32_t L_30;
		L_30 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		int32_t L_31 = L_30;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_26, L_32, /*hidden argument*/NULL);
		__this->set_U3CdU3E5__5_8(L_33);
		// restLengths[i] = d.magnitude;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_34 = __this->get_U3CU3E4__this_3();
		NullCheck(L_34);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_35 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_34)->get_restLengths_40();
		int32_t L_36 = __this->get_U3CiU3E5__2_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_37 = __this->get_address_of_U3CdU3E5__5_8();
		float L_38;
		L_38 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_37, /*hidden argument*/NULL);
		NullCheck(L_35);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(L_36), (float)L_38);
		// frame.Transport(positions[indices.x], d.normalized, 0);
		ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 * L_39 = __this->get_address_of_U3CframeU3E5__1_4();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_40 = __this->get_U3CU3E4__this_3();
		NullCheck(L_40);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_41 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_40)->get_positions_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_42 = __this->get_address_of_U3CindicesU3E5__4_7();
		int32_t L_43;
		L_43 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_42, /*hidden argument*/NULL);
		NullCheck(L_41);
		int32_t L_44 = L_43;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_46 = __this->get_address_of_U3CdU3E5__5_8();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47;
		L_47 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_46, /*hidden argument*/NULL);
		ObiPathFrame_Transport_m0B1B3CA7069547BE12B49F7FDB4E67EEADC239D4((ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 *)L_39, L_45, L_47, (0.0f), /*hidden argument*/NULL);
		// orientations[i] = Quaternion.LookRotation(frame.tangent, particleNormals[indices.x]);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_48 = __this->get_U3CU3E4__this_3();
		NullCheck(L_48);
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_49 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_48)->get_orientations_11();
		int32_t L_50 = __this->get_U3CiU3E5__2_5();
		ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 * L_51 = __this->get_address_of_U3CframeU3E5__1_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52 = L_51->get_tangent_1();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_53 = __this->get_particleNormals_2();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_54 = __this->get_address_of_U3CindicesU3E5__4_7();
		int32_t L_55;
		L_55 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_54, /*hidden argument*/NULL);
		NullCheck(L_53);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_56;
		L_56 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_53, L_55, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_57;
		L_57 = Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1(L_52, L_56, /*hidden argument*/NULL);
		NullCheck(L_49);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(L_50), (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 )L_57);
		// restOrientations[i] = orientations[i];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_58 = __this->get_U3CU3E4__this_3();
		NullCheck(L_58);
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_59 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_58)->get_restOrientations_12();
		int32_t L_60 = __this->get_U3CiU3E5__2_5();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_61 = __this->get_U3CU3E4__this_3();
		NullCheck(L_61);
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_62 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_61)->get_orientations_11();
		int32_t L_63 = __this->get_U3CiU3E5__2_5();
		NullCheck(L_62);
		int32_t L_64 = L_63;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_59);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(L_60), (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 )L_65);
		// orientations[indices.y] = orientations[i];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_66 = __this->get_U3CU3E4__this_3();
		NullCheck(L_66);
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_67 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_66)->get_orientations_11();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_68 = __this->get_address_of_U3CindicesU3E5__4_7();
		int32_t L_69;
		L_69 = Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_68, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_70 = __this->get_U3CU3E4__this_3();
		NullCheck(L_70);
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_71 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_70)->get_orientations_11();
		int32_t L_72 = __this->get_U3CiU3E5__2_5();
		NullCheck(L_71);
		int32_t L_73 = L_72;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_74 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		NullCheck(L_67);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(L_69), (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 )L_74);
		// restOrientations[indices.y] = orientations[i];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_75 = __this->get_U3CU3E4__this_3();
		NullCheck(L_75);
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_76 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_75)->get_restOrientations_12();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_77 = __this->get_address_of_U3CindicesU3E5__4_7();
		int32_t L_78;
		L_78 = Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_77, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_79 = __this->get_U3CU3E4__this_3();
		NullCheck(L_79);
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_80 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_79)->get_orientations_11();
		int32_t L_81 = __this->get_U3CiU3E5__2_5();
		NullCheck(L_80);
		int32_t L_82 = L_81;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_76);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(L_78), (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 )L_83);
		// batch.AddConstraint(indices, indices.x, restLengths[i], Quaternion.identity);
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_84 = __this->get_U3CbatchU3E5__3_6();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  L_85 = __this->get_U3CindicesU3E5__4_7();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_86 = __this->get_address_of_U3CindicesU3E5__4_7();
		int32_t L_87;
		L_87 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_86, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_88 = __this->get_U3CU3E4__this_3();
		NullCheck(L_88);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_89 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_88)->get_restLengths_40();
		int32_t L_90 = __this->get_U3CiU3E5__2_5();
		NullCheck(L_89);
		int32_t L_91 = L_90;
		float L_92 = (L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_93;
		L_93 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		NullCheck(L_84);
		ObiStretchShearConstraintsBatch_AddConstraint_m3EDB080395E5167A460038CE0B00CEEC306C19D5(L_84, L_85, L_87, L_92, L_93, /*hidden argument*/NULL);
		// batch.activeConstraintCount++;
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_94 = __this->get_U3CbatchU3E5__3_6();
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_95 = L_94;
		NullCheck(L_95);
		int32_t L_96;
		L_96 = ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02(L_95, /*hidden argument*/NULL);
		V_1 = L_96;
		int32_t L_97 = V_1;
		NullCheck(L_95);
		ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A(L_95, ((int32_t)il2cpp_codegen_add((int32_t)L_97, (int32_t)1)), /*hidden argument*/NULL);
		// if (i % 500 == 0)
		int32_t L_98 = __this->get_U3CiU3E5__2_5();
		V_2 = (bool)((((int32_t)((int32_t)((int32_t)L_98%(int32_t)((int32_t)500)))) == ((int32_t)0))? 1 : 0);
		bool L_99 = V_2;
		if (!L_99)
		{
			goto IL_02b4;
		}
	}
	{
		// yield return new CoroutineJob.ProgressInfo("ObiRod: generating structural constraints...", i / (float)(totalParticles - 1));
		int32_t L_100 = __this->get_U3CiU3E5__2_5();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_101 = __this->get_U3CU3E4__this_3();
		NullCheck(L_101);
		int32_t L_102 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_101)->get_totalParticles_38();
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_103 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_103, _stringLiteral000FEC8A07B6CA30D8852E9E25C3EE48061B49FA, ((float)((float)((float)((float)L_100))/(float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_102, (int32_t)1)))))), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_103);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_02ad:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_02b4:
	{
		__this->set_U3CbatchU3E5__3_6((ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B *)NULL);
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_104 = __this->get_address_of_U3CindicesU3E5__4_7();
		il2cpp_codegen_initobj(L_104, sizeof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 ));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_105 = __this->get_address_of_U3CdU3E5__5_8();
		il2cpp_codegen_initobj(L_105, sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		// for (int i = 0; i < totalParticles - 1; i++)
		int32_t L_106 = __this->get_U3CiU3E5__2_5();
		V_1 = L_106;
		int32_t L_107 = V_1;
		__this->set_U3CiU3E5__2_5(((int32_t)il2cpp_codegen_add((int32_t)L_107, (int32_t)1)));
	}

IL_02e4:
	{
		// for (int i = 0; i < totalParticles - 1; i++)
		int32_t L_108 = __this->get_U3CiU3E5__2_5();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_109 = __this->get_U3CU3E4__this_3();
		NullCheck(L_109);
		int32_t L_110 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_109)->get_totalParticles_38();
		V_3 = (bool)((((int32_t)L_108) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_110, (int32_t)1))))? 1 : 0);
		bool L_111 = V_3;
		if (L_111)
		{
			goto IL_0085;
		}
	}
	{
		// if (path.Closed)
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_112 = __this->get_U3CU3E4__this_3();
		NullCheck(L_112);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_113 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_112)->get_path_34();
		NullCheck(L_113);
		bool L_114;
		L_114 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_113, /*hidden argument*/NULL);
		V_4 = L_114;
		bool L_115 = V_4;
		if (!L_115)
		{
			goto IL_04e0;
		}
	}
	{
		// var loopClosingBatch = new ObiStretchShearConstraintsBatch();
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_116 = (ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B *)il2cpp_codegen_object_new(ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_il2cpp_TypeInfo_var);
		ObiStretchShearConstraintsBatch__ctor_m8927550DA897869840F27FAE27254EF2961AEFB7(L_116, (ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC *)NULL, /*hidden argument*/NULL);
		__this->set_U3CloopClosingBatchU3E5__6_9(L_116);
		// stretchShearConstraintsData.AddBatch(loopClosingBatch);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_117 = __this->get_U3CU3E4__this_3();
		NullCheck(L_117);
		ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * L_118 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_117)->get_stretchShearConstraintsData_27();
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_119 = __this->get_U3CloopClosingBatchU3E5__6_9();
		NullCheck(L_118);
		ObiConstraints_1_AddBatch_m72B86FE6FB5147CD79A80B53085BC52B930199CB(L_118, L_119, /*hidden argument*/ObiConstraints_1_AddBatch_m72B86FE6FB5147CD79A80B53085BC52B930199CB_RuntimeMethod_var);
		// Vector2Int indices = new Vector2Int(m_ActiveParticleCount - 1, 0);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_120 = __this->get_U3CU3E4__this_3();
		NullCheck(L_120);
		int32_t L_121 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_120)->get_m_ActiveParticleCount_6();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  L_122;
		memset((&L_122), 0, sizeof(L_122));
		Vector2Int__ctor_mB2B73108B6DD3ADC1B515D7DD9116ECAC6833726_inline((&L_122), ((int32_t)il2cpp_codegen_subtract((int32_t)L_121, (int32_t)1)), 0, /*hidden argument*/NULL);
		__this->set_U3CindicesU3E5__7_10(L_122);
		// Vector3 d = positions[indices.y] - positions[indices.x];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_123 = __this->get_U3CU3E4__this_3();
		NullCheck(L_123);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_124 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_123)->get_positions_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_125 = __this->get_address_of_U3CindicesU3E5__7_10();
		int32_t L_126;
		L_126 = Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_125, /*hidden argument*/NULL);
		NullCheck(L_124);
		int32_t L_127 = L_126;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_128 = (L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_127));
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_129 = __this->get_U3CU3E4__this_3();
		NullCheck(L_129);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_130 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_129)->get_positions_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_131 = __this->get_address_of_U3CindicesU3E5__7_10();
		int32_t L_132;
		L_132 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_131, /*hidden argument*/NULL);
		NullCheck(L_130);
		int32_t L_133 = L_132;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_134 = (L_130)->GetAt(static_cast<il2cpp_array_size_t>(L_133));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_135;
		L_135 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_128, L_134, /*hidden argument*/NULL);
		__this->set_U3CdU3E5__8_11(L_135);
		// restLengths[m_ActiveParticleCount - 2] = d.magnitude;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_136 = __this->get_U3CU3E4__this_3();
		NullCheck(L_136);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_137 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_136)->get_restLengths_40();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_138 = __this->get_U3CU3E4__this_3();
		NullCheck(L_138);
		int32_t L_139 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_138)->get_m_ActiveParticleCount_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_140 = __this->get_address_of_U3CdU3E5__8_11();
		float L_141;
		L_141 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_140, /*hidden argument*/NULL);
		NullCheck(L_137);
		(L_137)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_139, (int32_t)2))), (float)L_141);
		// frame.Transport(positions[indices.x], d.normalized, 0);
		ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 * L_142 = __this->get_address_of_U3CframeU3E5__1_4();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_143 = __this->get_U3CU3E4__this_3();
		NullCheck(L_143);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_144 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_143)->get_positions_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_145 = __this->get_address_of_U3CindicesU3E5__7_10();
		int32_t L_146;
		L_146 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_145, /*hidden argument*/NULL);
		NullCheck(L_144);
		int32_t L_147 = L_146;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_148 = (L_144)->GetAt(static_cast<il2cpp_array_size_t>(L_147));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_149 = __this->get_address_of_U3CdU3E5__8_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_150;
		L_150 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_149, /*hidden argument*/NULL);
		ObiPathFrame_Transport_m0B1B3CA7069547BE12B49F7FDB4E67EEADC239D4((ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 *)L_142, L_148, L_150, (0.0f), /*hidden argument*/NULL);
		// orientations[m_ActiveParticleCount - 1] = Quaternion.LookRotation(frame.tangent, particleNormals[indices.x]);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_151 = __this->get_U3CU3E4__this_3();
		NullCheck(L_151);
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_152 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_151)->get_orientations_11();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_153 = __this->get_U3CU3E4__this_3();
		NullCheck(L_153);
		int32_t L_154 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_153)->get_m_ActiveParticleCount_6();
		ObiPathFrame_tD198560E591FA0179C981028241B15E2DAA020D7 * L_155 = __this->get_address_of_U3CframeU3E5__1_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_156 = L_155->get_tangent_1();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_157 = __this->get_particleNormals_2();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_158 = __this->get_address_of_U3CindicesU3E5__7_10();
		int32_t L_159;
		L_159 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_158, /*hidden argument*/NULL);
		NullCheck(L_157);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_160;
		L_160 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_157, L_159, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_161;
		L_161 = Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1(L_156, L_160, /*hidden argument*/NULL);
		NullCheck(L_152);
		(L_152)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_154, (int32_t)1))), (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 )L_161);
		// restOrientations[m_ActiveParticleCount - 1] = orientations[m_ActiveParticleCount - 1];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_162 = __this->get_U3CU3E4__this_3();
		NullCheck(L_162);
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_163 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_162)->get_restOrientations_12();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_164 = __this->get_U3CU3E4__this_3();
		NullCheck(L_164);
		int32_t L_165 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_164)->get_m_ActiveParticleCount_6();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_166 = __this->get_U3CU3E4__this_3();
		NullCheck(L_166);
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_167 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_166)->get_orientations_11();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_168 = __this->get_U3CU3E4__this_3();
		NullCheck(L_168);
		int32_t L_169 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_168)->get_m_ActiveParticleCount_6();
		NullCheck(L_167);
		int32_t L_170 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_169, (int32_t)1));
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_171 = (L_167)->GetAt(static_cast<il2cpp_array_size_t>(L_170));
		NullCheck(L_163);
		(L_163)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_165, (int32_t)1))), (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 )L_171);
		// loopClosingBatch.AddConstraint(indices, indices.x, restLengths[m_ActiveParticleCount - 2], Quaternion.identity);
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_172 = __this->get_U3CloopClosingBatchU3E5__6_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  L_173 = __this->get_U3CindicesU3E5__7_10();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_174 = __this->get_address_of_U3CindicesU3E5__7_10();
		int32_t L_175;
		L_175 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_174, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_176 = __this->get_U3CU3E4__this_3();
		NullCheck(L_176);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_177 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_176)->get_restLengths_40();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_178 = __this->get_U3CU3E4__this_3();
		NullCheck(L_178);
		int32_t L_179 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_178)->get_m_ActiveParticleCount_6();
		NullCheck(L_177);
		int32_t L_180 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_179, (int32_t)2));
		float L_181 = (L_177)->GetAt(static_cast<il2cpp_array_size_t>(L_180));
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_182;
		L_182 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		NullCheck(L_172);
		ObiStretchShearConstraintsBatch_AddConstraint_m3EDB080395E5167A460038CE0B00CEEC306C19D5(L_172, L_173, L_175, L_181, L_182, /*hidden argument*/NULL);
		// loopClosingBatch.activeConstraintCount++;
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_183 = __this->get_U3CloopClosingBatchU3E5__6_9();
		ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B * L_184 = L_183;
		NullCheck(L_184);
		int32_t L_185;
		L_185 = ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02(L_184, /*hidden argument*/NULL);
		V_1 = L_185;
		int32_t L_186 = V_1;
		NullCheck(L_184);
		ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A(L_184, ((int32_t)il2cpp_codegen_add((int32_t)L_186, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U3CloopClosingBatchU3E5__6_9((ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B *)NULL);
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_187 = __this->get_address_of_U3CindicesU3E5__7_10();
		il2cpp_codegen_initobj(L_187, sizeof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 ));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_188 = __this->get_address_of_U3CdU3E5__8_11();
		il2cpp_codegen_initobj(L_188, sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
	}

IL_04e0:
	{
		// m_RestLength = 0;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_189 = __this->get_U3CU3E4__this_3();
		NullCheck(L_189);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_189)->set_m_RestLength_39((0.0f));
		// foreach (float length in restLengths)
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_190 = __this->get_U3CU3E4__this_3();
		NullCheck(L_190);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_191 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_190)->get_restLengths_40();
		__this->set_U3CU3Es__9_12(L_191);
		__this->set_U3CU3Es__10_13(0);
		goto IL_0549;
	}

IL_050b:
	{
		// foreach (float length in restLengths)
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_192 = __this->get_U3CU3Es__9_12();
		int32_t L_193 = __this->get_U3CU3Es__10_13();
		NullCheck(L_192);
		int32_t L_194 = L_193;
		float L_195 = (L_192)->GetAt(static_cast<il2cpp_array_size_t>(L_194));
		__this->set_U3ClengthU3E5__11_14(L_195);
		// m_RestLength += length;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_196 = __this->get_U3CU3E4__this_3();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_197 = __this->get_U3CU3E4__this_3();
		NullCheck(L_197);
		float L_198 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_197)->get_m_RestLength_39();
		float L_199 = __this->get_U3ClengthU3E5__11_14();
		NullCheck(L_196);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_196)->set_m_RestLength_39(((float)il2cpp_codegen_add((float)L_198, (float)L_199)));
		int32_t L_200 = __this->get_U3CU3Es__10_13();
		__this->set_U3CU3Es__10_13(((int32_t)il2cpp_codegen_add((int32_t)L_200, (int32_t)1)));
	}

IL_0549:
	{
		// foreach (float length in restLengths)
		int32_t L_201 = __this->get_U3CU3Es__10_13();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_202 = __this->get_U3CU3Es__9_12();
		NullCheck(L_202);
		if ((((int32_t)L_201) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_202)->max_length))))))
		{
			goto IL_050b;
		}
	}
	{
		__this->set_U3CU3Es__9_12((SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)NULL);
		// }
		return (bool)0;
	}
}
// System.Object Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m48490361DF35F3911FA88CC9E8488B9976632C81 (U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m3EC8612B472DCDC0A5BA07CF265742F7ADB074CC (U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m3EC8612B472DCDC0A5BA07CF265742F7ADB074CC_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiRodBlueprint/<CreateStretchShearConstraints>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateStretchShearConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_mDF13C68A7A9199168F5434C5FDAE03DECED69AD9 (U3CCreateStretchShearConstraintsU3Ed__4_tFA2ADAC8A57CAE0BDB3D39F9FBB17FD4D1227E46 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiRodBlueprint/<Initialize>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__3__ctor_m41BDF4E1CF1181A4B67AA1298FF91C848705149D (U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.ObiRodBlueprint/<Initialize>d__3::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__3_System_IDisposable_Dispose_mAAB9B58959E17D6316D0376867E1ADEA76D9F1EC (U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiRodBlueprint/<Initialize>d__3::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CInitializeU3Ed__3_MoveNext_mF4E8C487EB520B7BB8AC9C68DC554D6ABB40D2B4 (U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathDataChannelIdentity_1_GetAtMu_m169B9F9D1859E1C2E829CCC554D1DF4421230498_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral903817815321EE2F94F9CC0B49F39AA16CC286AB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBDE3CEDBCC90855A8BBDCB3FE9B094E5B6AB184B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD368B3509E2BA452C46794E1AC89E84E3642498F);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	int32_t G_B20_0 = 0;
	int32_t G_B29_0 = 0;
	U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C * G_B29_1 = NULL;
	int32_t G_B28_0 = 0;
	U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C * G_B28_1 = NULL;
	int32_t G_B30_0 = 0;
	int32_t G_B30_1 = 0;
	U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C * G_B30_2 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0027;
			}
			case 1:
			{
				goto IL_0029;
			}
			case 2:
			{
				goto IL_002e;
			}
			case 3:
			{
				goto IL_0033;
			}
			case 4:
			{
				goto IL_0038;
			}
			case 5:
			{
				goto IL_003d;
			}
		}
	}
	{
		goto IL_0042;
	}

IL_0027:
	{
		goto IL_0044;
	}

IL_0029:
	{
		goto IL_070a;
	}

IL_002e:
	{
		goto IL_0aac;
	}

IL_0033:
	{
		goto IL_0b25;
	}

IL_0038:
	{
		goto IL_0b6a;
	}

IL_003d:
	{
		goto IL_0baf;
	}

IL_0042:
	{
		return (bool)0;
	}

IL_0044:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (path.ControlPointCount < 2)
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_3 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_2)->get_path_34();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = ObiPath_get_ControlPointCount_m09869836E934AC2B253907636DDE6D72BA95D05F(L_3, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_4) < ((int32_t)2))? 1 : 0);
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0131;
		}
	}
	{
		// ClearParticleGroups();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_6 = __this->get_U3CU3E4__this_2();
		NullCheck(L_6);
		ObiActorBlueprint_ClearParticleGroups_mBC589A70CC5E642C10FD126B482590D27FEC8B18(L_6, (bool)1, /*hidden argument*/NULL);
		// path.InsertControlPoint(0, Vector3.left, Vector3.left * 0.25f, Vector3.right * 0.25f, Vector3.up, DEFAULT_PARTICLE_MASS, DEFAULT_PARTICLE_ROTATIONAL_MASS, 1, ObiUtils.MakeFilter(ObiUtils.CollideWithEverything, 1), Color.white, "control point");
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_8 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_7)->get_path_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_10, (0.25f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_12, (0.25f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		int32_t L_15;
		L_15 = ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3_inline(((int32_t)65535), 1, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_16;
		L_16 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		NullCheck(L_8);
		ObiPath_InsertControlPoint_m978D7BDBC828882C6D3B1C85F029C6B1BC63BDDA(L_8, 0, L_9, L_11, L_13, L_14, (0.100000001f), (0.00999999978f), (1.0f), L_15, L_16, _stringLiteralD368B3509E2BA452C46794E1AC89E84E3642498F, /*hidden argument*/NULL);
		// path.InsertControlPoint(1, Vector3.right, Vector3.left * 0.25f, Vector3.right * 0.25f, Vector3.up, DEFAULT_PARTICLE_MASS, DEFAULT_PARTICLE_ROTATIONAL_MASS, 1, ObiUtils.MakeFilter(ObiUtils.CollideWithEverything, 1), Color.white, "control point");
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_17 = __this->get_U3CU3E4__this_2();
		NullCheck(L_17);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_18 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_17)->get_path_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_20, (0.25f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_22, (0.25f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24;
		L_24 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		int32_t L_25;
		L_25 = ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3_inline(((int32_t)65535), 1, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_26;
		L_26 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		NullCheck(L_18);
		ObiPath_InsertControlPoint_m978D7BDBC828882C6D3B1C85F029C6B1BC63BDDA(L_18, 1, L_19, L_21, L_23, L_24, (0.100000001f), (0.00999999978f), (1.0f), L_25, L_26, _stringLiteralD368B3509E2BA452C46794E1AC89E84E3642498F, /*hidden argument*/NULL);
	}

IL_0131:
	{
		// path.RecalculateLenght(Matrix4x4.identity, 0.00001f, 7);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_27 = __this->get_U3CU3E4__this_2();
		NullCheck(L_27);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_28 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_27)->get_path_34();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_29;
		L_29 = Matrix4x4_get_identity_mC91289718DDD3DDBE0A10551BDA59A446414A596(/*hidden argument*/NULL);
		NullCheck(L_28);
		float L_30;
		L_30 = ObiPath_RecalculateLenght_mDF1CD450DBBA7B9823BA8090C340595338260C0E(L_28, L_29, (9.99999975E-06f), 7, /*hidden argument*/NULL);
		// List<Vector3> particlePositions = new List<Vector3>();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_31 = (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *)il2cpp_codegen_object_new(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		List_1__ctor_mF8F23D572031748AD428623AE16803455997E297(L_31, /*hidden argument*/List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		__this->set_U3CparticlePositionsU3E5__1_3(L_31);
		// List<Vector3> particleNormals = new List<Vector3>();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_32 = (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *)il2cpp_codegen_object_new(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		List_1__ctor_mF8F23D572031748AD428623AE16803455997E297(L_32, /*hidden argument*/List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		__this->set_U3CparticleNormalsU3E5__2_4(L_32);
		// List<float> particleThicknesses = new List<float>();
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_33 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)il2cpp_codegen_object_new(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_il2cpp_TypeInfo_var);
		List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C(L_33, /*hidden argument*/List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C_RuntimeMethod_var);
		__this->set_U3CparticleThicknessesU3E5__3_5(L_33);
		// List<float> particleInvMasses = new List<float>();
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_34 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)il2cpp_codegen_object_new(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_il2cpp_TypeInfo_var);
		List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C(L_34, /*hidden argument*/List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C_RuntimeMethod_var);
		__this->set_U3CparticleInvMassesU3E5__4_6(L_34);
		// List<float> particleInvRotationalMasses = new List<float>();
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_35 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)il2cpp_codegen_object_new(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_il2cpp_TypeInfo_var);
		List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C(L_35, /*hidden argument*/List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C_RuntimeMethod_var);
		__this->set_U3CparticleInvRotationalMassesU3E5__5_7(L_35);
		// List<int> particleFilters = new List<int>();
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_36 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)il2cpp_codegen_object_new(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD(L_36, /*hidden argument*/List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		__this->set_U3CparticleFiltersU3E5__6_8(L_36);
		// List<Color> particleColors = new List<Color>();
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_37 = (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E *)il2cpp_codegen_object_new(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_il2cpp_TypeInfo_var);
		List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1(L_37, /*hidden argument*/List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_RuntimeMethod_var);
		__this->set_U3CparticleColorsU3E5__7_9(L_37);
		// if (!path.Closed)
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_38 = __this->get_U3CU3E4__this_2();
		NullCheck(L_38);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_39 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_38)->get_path_34();
		NullCheck(L_39);
		bool L_40;
		L_40 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_39, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_40) == ((int32_t)0))? 1 : 0);
		bool L_41 = V_2;
		if (!L_41)
		{
			goto IL_033a;
		}
	}
	{
		// particlePositions.Add(path.points.GetPositionAtMu(path.Closed, 0));
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_42 = __this->get_U3CparticlePositionsU3E5__1_3();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_43 = __this->get_U3CU3E4__this_2();
		NullCheck(L_43);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_44 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_43)->get_path_34();
		NullCheck(L_44);
		ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507 * L_45;
		L_45 = ObiPath_get_points_m6F044F0A220D99C2C5184CB5FE1769BAED6FFD3C(L_44, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_46 = __this->get_U3CU3E4__this_2();
		NullCheck(L_46);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_47 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_46)->get_path_34();
		NullCheck(L_47);
		bool L_48;
		L_48 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_47, /*hidden argument*/NULL);
		NullCheck(L_45);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49;
		L_49 = ObiPointsDataChannel_GetPositionAtMu_m5DA1AF5F4A0C5EB6EDAE4AA729DAC149E3E17DF5(L_45, L_48, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_42);
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_42, L_49, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// particleNormals.Add(path.normals.GetAtMu(path.Closed, 0));
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_50 = __this->get_U3CparticleNormalsU3E5__2_4();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_51 = __this->get_U3CU3E4__this_2();
		NullCheck(L_51);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_52 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_51)->get_path_34();
		NullCheck(L_52);
		ObiNormalDataChannel_tA3F5F99B539B56E0703FA0603B89DCC345AC3296 * L_53;
		L_53 = ObiPath_get_normals_mD27D9F0689AB44FF831629FC2F0F7F2BC5EA08F6(L_52, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_54 = __this->get_U3CU3E4__this_2();
		NullCheck(L_54);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_55 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_54)->get_path_34();
		NullCheck(L_55);
		bool L_56;
		L_56 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_55, /*hidden argument*/NULL);
		NullCheck(L_53);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_57;
		L_57 = ObiPathDataChannelIdentity_1_GetAtMu_m169B9F9D1859E1C2E829CCC554D1DF4421230498(L_53, L_56, (0.0f), /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_m169B9F9D1859E1C2E829CCC554D1DF4421230498_RuntimeMethod_var);
		NullCheck(L_50);
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_50, L_57, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// particleThicknesses.Add(path.thicknesses.GetAtMu(path.Closed, 0));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_58 = __this->get_U3CparticleThicknessesU3E5__3_5();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_59 = __this->get_U3CU3E4__this_2();
		NullCheck(L_59);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_60 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_59)->get_path_34();
		NullCheck(L_60);
		ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C * L_61;
		L_61 = ObiPath_get_thicknesses_m435418A103FD1CC0D151EF3B75689969F0E23C8D(L_60, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_62 = __this->get_U3CU3E4__this_2();
		NullCheck(L_62);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_63 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_62)->get_path_34();
		NullCheck(L_63);
		bool L_64;
		L_64 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_63, /*hidden argument*/NULL);
		NullCheck(L_61);
		float L_65;
		L_65 = ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3(L_61, L_64, (0.0f), /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		NullCheck(L_58);
		List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A(L_58, L_65, /*hidden argument*/List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		// particleInvMasses.Add(ObiUtils.MassToInvMass(path.masses.GetAtMu(path.Closed, 0)));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_66 = __this->get_U3CparticleInvMassesU3E5__4_6();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_67 = __this->get_U3CU3E4__this_2();
		NullCheck(L_67);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_68 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_67)->get_path_34();
		NullCheck(L_68);
		ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996 * L_69;
		L_69 = ObiPath_get_masses_m563E313A7734C29F2476B68228BAB71731EBC326(L_68, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_70 = __this->get_U3CU3E4__this_2();
		NullCheck(L_70);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_71 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_70)->get_path_34();
		NullCheck(L_71);
		bool L_72;
		L_72 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_71, /*hidden argument*/NULL);
		NullCheck(L_69);
		float L_73;
		L_73 = ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3(L_69, L_72, (0.0f), /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		float L_74;
		L_74 = ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68_inline(L_73, /*hidden argument*/NULL);
		NullCheck(L_66);
		List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A(L_66, L_74, /*hidden argument*/List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		// particleInvRotationalMasses.Add(ObiUtils.MassToInvMass(path.rotationalMasses.GetAtMu(path.Closed, 0)));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_75 = __this->get_U3CparticleInvRotationalMassesU3E5__5_7();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_76 = __this->get_U3CU3E4__this_2();
		NullCheck(L_76);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_77 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_76)->get_path_34();
		NullCheck(L_77);
		ObiRotationalMassDataChannel_tC062F8C628B38CBE7F635761D65A2BA348541E4B * L_78;
		L_78 = ObiPath_get_rotationalMasses_m65A0959B14AEBD424FC35853DF6EBEE9A4D2648A(L_77, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_79 = __this->get_U3CU3E4__this_2();
		NullCheck(L_79);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_80 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_79)->get_path_34();
		NullCheck(L_80);
		bool L_81;
		L_81 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_80, /*hidden argument*/NULL);
		NullCheck(L_78);
		float L_82;
		L_82 = ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3(L_78, L_81, (0.0f), /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		float L_83;
		L_83 = ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68_inline(L_82, /*hidden argument*/NULL);
		NullCheck(L_75);
		List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A(L_75, L_83, /*hidden argument*/List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		// particleFilters.Add(path.filters.GetAtMu(path.Closed, 0));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_84 = __this->get_U3CparticleFiltersU3E5__6_8();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_85 = __this->get_U3CU3E4__this_2();
		NullCheck(L_85);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_86 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_85)->get_path_34();
		NullCheck(L_86);
		ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670 * L_87;
		L_87 = ObiPath_get_filters_m6D02BA0F0D6E24496882A16B37C12B185D9CC028(L_86, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_88 = __this->get_U3CU3E4__this_2();
		NullCheck(L_88);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_89 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_88)->get_path_34();
		NullCheck(L_89);
		bool L_90;
		L_90 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_89, /*hidden argument*/NULL);
		NullCheck(L_87);
		int32_t L_91;
		L_91 = ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB(L_87, L_90, (0.0f), /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB_RuntimeMethod_var);
		NullCheck(L_84);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_84, L_91, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// particleColors.Add(path.colors.GetAtMu(path.Closed, 0));
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_92 = __this->get_U3CparticleColorsU3E5__7_9();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_93 = __this->get_U3CU3E4__this_2();
		NullCheck(L_93);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_94 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_93)->get_path_34();
		NullCheck(L_94);
		ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2 * L_95;
		L_95 = ObiPath_get_colors_mB1315A257BB672D26575C3CEFD3FC642D65C8C6F(L_94, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_96 = __this->get_U3CU3E4__this_2();
		NullCheck(L_96);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_97 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_96)->get_path_34();
		NullCheck(L_97);
		bool L_98;
		L_98 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_97, /*hidden argument*/NULL);
		NullCheck(L_95);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_99;
		L_99 = ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A(L_95, L_98, (0.0f), /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A_RuntimeMethod_var);
		NullCheck(L_92);
		List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C(L_92, L_99, /*hidden argument*/List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_RuntimeMethod_var);
	}

IL_033a:
	{
		// groups[0].particleIndices.Clear();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_100 = __this->get_U3CU3E4__this_2();
		NullCheck(L_100);
		List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * L_101 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_100)->get_groups_33();
		NullCheck(L_101);
		ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE * L_102;
		L_102 = List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_inline(L_101, 0, /*hidden argument*/List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var);
		NullCheck(L_102);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_103 = L_102->get_particleIndices_4();
		NullCheck(L_103);
		List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A(L_103, /*hidden argument*/List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var);
		// groups[0].particleIndices.Add(0);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_104 = __this->get_U3CU3E4__this_2();
		NullCheck(L_104);
		List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * L_105 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_104)->get_groups_33();
		NullCheck(L_105);
		ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE * L_106;
		L_106 = List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_inline(L_105, 0, /*hidden argument*/List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var);
		NullCheck(L_106);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_107 = L_106->get_particleIndices_4();
		NullCheck(L_107);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_107, 0, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// ReadOnlyCollection<float> lengthTable = path.ArcLengthTable;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_108 = __this->get_U3CU3E4__this_2();
		NullCheck(L_108);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_109 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_108)->get_path_34();
		NullCheck(L_109);
		ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * L_110;
		L_110 = ObiPath_get_ArcLengthTable_m473E96A0299C036CB1835FE04F20733B6D824C81(L_109, /*hidden argument*/NULL);
		__this->set_U3ClengthTableU3E5__8_10(L_110);
		// int spans = path.GetSpanCount();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_111 = __this->get_U3CU3E4__this_2();
		NullCheck(L_111);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_112 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_111)->get_path_34();
		NullCheck(L_112);
		int32_t L_113;
		L_113 = ObiPath_GetSpanCount_m09199A6F5BB9A1619E0A8A7FB826FA1472B29623(L_112, /*hidden argument*/NULL);
		__this->set_U3CspansU3E5__9_11(L_113);
		// for (int i = 0; i < spans; i++)
		__this->set_U3CiU3E5__14_16(0);
		goto IL_0722;
	}

IL_03ab:
	{
		// int firstArcLengthSample = i * (path.ArcLengthSamples + 1);
		int32_t L_114 = __this->get_U3CiU3E5__14_16();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_115 = __this->get_U3CU3E4__this_2();
		NullCheck(L_115);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_116 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_115)->get_path_34();
		NullCheck(L_116);
		int32_t L_117;
		L_117 = ObiPath_get_ArcLengthSamples_m3C36FC6DDE2FBD76AD4F46EB7C5A17FDC4B1D9BA(L_116, /*hidden argument*/NULL);
		__this->set_U3CfirstArcLengthSampleU3E5__15_17(((int32_t)il2cpp_codegen_multiply((int32_t)L_114, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_117, (int32_t)1)))));
		// int lastArcLengthSample = (i + 1) * (path.ArcLengthSamples + 1);
		int32_t L_118 = __this->get_U3CiU3E5__14_16();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_119 = __this->get_U3CU3E4__this_2();
		NullCheck(L_119);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_120 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_119)->get_path_34();
		NullCheck(L_120);
		int32_t L_121;
		L_121 = ObiPath_get_ArcLengthSamples_m3C36FC6DDE2FBD76AD4F46EB7C5A17FDC4B1D9BA(L_120, /*hidden argument*/NULL);
		__this->set_U3ClastArcLengthSampleU3E5__16_18(((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_118, (int32_t)1)), (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_121, (int32_t)1)))));
		// float upToSpanLength = lengthTable[firstArcLengthSample];
		ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * L_122 = __this->get_U3ClengthTableU3E5__8_10();
		int32_t L_123 = __this->get_U3CfirstArcLengthSampleU3E5__15_17();
		NullCheck(L_122);
		float L_124;
		L_124 = ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F(L_122, L_123, /*hidden argument*/ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F_RuntimeMethod_var);
		__this->set_U3CupToSpanLengthU3E5__17_19(L_124);
		// float spanLength = lengthTable[lastArcLengthSample] - upToSpanLength;
		ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * L_125 = __this->get_U3ClengthTableU3E5__8_10();
		int32_t L_126 = __this->get_U3ClastArcLengthSampleU3E5__16_18();
		NullCheck(L_125);
		float L_127;
		L_127 = ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F(L_125, L_126, /*hidden argument*/ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F_RuntimeMethod_var);
		float L_128 = __this->get_U3CupToSpanLengthU3E5__17_19();
		__this->set_U3CspanLengthU3E5__18_20(((float)il2cpp_codegen_subtract((float)L_127, (float)L_128)));
		// int particlesInSpan = 1 + Mathf.FloorToInt(spanLength / thickness * resolution);
		float L_129 = __this->get_U3CspanLengthU3E5__18_20();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_130 = __this->get_U3CU3E4__this_2();
		NullCheck(L_130);
		float L_131 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_130)->get_thickness_35();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_132 = __this->get_U3CU3E4__this_2();
		NullCheck(L_132);
		float L_133 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_132)->get_resolution_36();
		int32_t L_134;
		L_134 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)((float)((float)L_129/(float)L_131)), (float)L_133)), /*hidden argument*/NULL);
		__this->set_U3CparticlesInSpanU3E5__19_21(((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)L_134)));
		// float distance = spanLength / particlesInSpan;
		float L_135 = __this->get_U3CspanLengthU3E5__18_20();
		int32_t L_136 = __this->get_U3CparticlesInSpanU3E5__19_21();
		__this->set_U3CdistanceU3E5__20_22(((float)((float)L_135/(float)((float)((float)L_136)))));
		// for (int j = 0; j < particlesInSpan; ++j)
		__this->set_U3CjU3E5__21_23(0);
		goto IL_0636;
	}

IL_046c:
	{
		// float mu = path.GetMuAtLenght(upToSpanLength + distance * (j + 1));
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_137 = __this->get_U3CU3E4__this_2();
		NullCheck(L_137);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_138 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_137)->get_path_34();
		float L_139 = __this->get_U3CupToSpanLengthU3E5__17_19();
		float L_140 = __this->get_U3CdistanceU3E5__20_22();
		int32_t L_141 = __this->get_U3CjU3E5__21_23();
		NullCheck(L_138);
		float L_142;
		L_142 = ObiPath_GetMuAtLenght_m9B36DC8C812923123B8D5F9D290BAD46CF555E6B(L_138, ((float)il2cpp_codegen_add((float)L_139, (float)((float)il2cpp_codegen_multiply((float)L_140, (float)((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_141, (int32_t)1)))))))), /*hidden argument*/NULL);
		__this->set_U3CmuU3E5__22_24(L_142);
		// particlePositions.Add(path.points.GetPositionAtMu(path.Closed, mu));
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_143 = __this->get_U3CparticlePositionsU3E5__1_3();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_144 = __this->get_U3CU3E4__this_2();
		NullCheck(L_144);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_145 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_144)->get_path_34();
		NullCheck(L_145);
		ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507 * L_146;
		L_146 = ObiPath_get_points_m6F044F0A220D99C2C5184CB5FE1769BAED6FFD3C(L_145, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_147 = __this->get_U3CU3E4__this_2();
		NullCheck(L_147);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_148 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_147)->get_path_34();
		NullCheck(L_148);
		bool L_149;
		L_149 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_148, /*hidden argument*/NULL);
		float L_150 = __this->get_U3CmuU3E5__22_24();
		NullCheck(L_146);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_151;
		L_151 = ObiPointsDataChannel_GetPositionAtMu_m5DA1AF5F4A0C5EB6EDAE4AA729DAC149E3E17DF5(L_146, L_149, L_150, /*hidden argument*/NULL);
		NullCheck(L_143);
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_143, L_151, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// particleNormals.Add(path.normals.GetAtMu(path.Closed, mu));
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_152 = __this->get_U3CparticleNormalsU3E5__2_4();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_153 = __this->get_U3CU3E4__this_2();
		NullCheck(L_153);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_154 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_153)->get_path_34();
		NullCheck(L_154);
		ObiNormalDataChannel_tA3F5F99B539B56E0703FA0603B89DCC345AC3296 * L_155;
		L_155 = ObiPath_get_normals_mD27D9F0689AB44FF831629FC2F0F7F2BC5EA08F6(L_154, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_156 = __this->get_U3CU3E4__this_2();
		NullCheck(L_156);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_157 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_156)->get_path_34();
		NullCheck(L_157);
		bool L_158;
		L_158 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_157, /*hidden argument*/NULL);
		float L_159 = __this->get_U3CmuU3E5__22_24();
		NullCheck(L_155);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_160;
		L_160 = ObiPathDataChannelIdentity_1_GetAtMu_m169B9F9D1859E1C2E829CCC554D1DF4421230498(L_155, L_158, L_159, /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_m169B9F9D1859E1C2E829CCC554D1DF4421230498_RuntimeMethod_var);
		NullCheck(L_152);
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_152, L_160, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// particleThicknesses.Add(path.thicknesses.GetAtMu(path.Closed, mu));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_161 = __this->get_U3CparticleThicknessesU3E5__3_5();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_162 = __this->get_U3CU3E4__this_2();
		NullCheck(L_162);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_163 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_162)->get_path_34();
		NullCheck(L_163);
		ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C * L_164;
		L_164 = ObiPath_get_thicknesses_m435418A103FD1CC0D151EF3B75689969F0E23C8D(L_163, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_165 = __this->get_U3CU3E4__this_2();
		NullCheck(L_165);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_166 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_165)->get_path_34();
		NullCheck(L_166);
		bool L_167;
		L_167 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_166, /*hidden argument*/NULL);
		float L_168 = __this->get_U3CmuU3E5__22_24();
		NullCheck(L_164);
		float L_169;
		L_169 = ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3(L_164, L_167, L_168, /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		NullCheck(L_161);
		List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A(L_161, L_169, /*hidden argument*/List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		// particleInvMasses.Add(ObiUtils.MassToInvMass(path.masses.GetAtMu(path.Closed, mu)));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_170 = __this->get_U3CparticleInvMassesU3E5__4_6();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_171 = __this->get_U3CU3E4__this_2();
		NullCheck(L_171);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_172 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_171)->get_path_34();
		NullCheck(L_172);
		ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996 * L_173;
		L_173 = ObiPath_get_masses_m563E313A7734C29F2476B68228BAB71731EBC326(L_172, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_174 = __this->get_U3CU3E4__this_2();
		NullCheck(L_174);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_175 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_174)->get_path_34();
		NullCheck(L_175);
		bool L_176;
		L_176 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_175, /*hidden argument*/NULL);
		float L_177 = __this->get_U3CmuU3E5__22_24();
		NullCheck(L_173);
		float L_178;
		L_178 = ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3(L_173, L_176, L_177, /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		float L_179;
		L_179 = ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68_inline(L_178, /*hidden argument*/NULL);
		NullCheck(L_170);
		List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A(L_170, L_179, /*hidden argument*/List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		// particleInvRotationalMasses.Add(ObiUtils.MassToInvMass(path.rotationalMasses.GetAtMu(path.Closed, mu)));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_180 = __this->get_U3CparticleInvRotationalMassesU3E5__5_7();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_181 = __this->get_U3CU3E4__this_2();
		NullCheck(L_181);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_182 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_181)->get_path_34();
		NullCheck(L_182);
		ObiRotationalMassDataChannel_tC062F8C628B38CBE7F635761D65A2BA348541E4B * L_183;
		L_183 = ObiPath_get_rotationalMasses_m65A0959B14AEBD424FC35853DF6EBEE9A4D2648A(L_182, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_184 = __this->get_U3CU3E4__this_2();
		NullCheck(L_184);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_185 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_184)->get_path_34();
		NullCheck(L_185);
		bool L_186;
		L_186 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_185, /*hidden argument*/NULL);
		float L_187 = __this->get_U3CmuU3E5__22_24();
		NullCheck(L_183);
		float L_188;
		L_188 = ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3(L_183, L_186, L_187, /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		float L_189;
		L_189 = ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68_inline(L_188, /*hidden argument*/NULL);
		NullCheck(L_180);
		List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A(L_180, L_189, /*hidden argument*/List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		// particleFilters.Add(path.filters.GetAtMu(path.Closed, mu));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_190 = __this->get_U3CparticleFiltersU3E5__6_8();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_191 = __this->get_U3CU3E4__this_2();
		NullCheck(L_191);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_192 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_191)->get_path_34();
		NullCheck(L_192);
		ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670 * L_193;
		L_193 = ObiPath_get_filters_m6D02BA0F0D6E24496882A16B37C12B185D9CC028(L_192, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_194 = __this->get_U3CU3E4__this_2();
		NullCheck(L_194);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_195 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_194)->get_path_34();
		NullCheck(L_195);
		bool L_196;
		L_196 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_195, /*hidden argument*/NULL);
		float L_197 = __this->get_U3CmuU3E5__22_24();
		NullCheck(L_193);
		int32_t L_198;
		L_198 = ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB(L_193, L_196, L_197, /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB_RuntimeMethod_var);
		NullCheck(L_190);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_190, L_198, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// particleColors.Add(path.colors.GetAtMu(path.Closed, mu));
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_199 = __this->get_U3CparticleColorsU3E5__7_9();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_200 = __this->get_U3CU3E4__this_2();
		NullCheck(L_200);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_201 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_200)->get_path_34();
		NullCheck(L_201);
		ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2 * L_202;
		L_202 = ObiPath_get_colors_mB1315A257BB672D26575C3CEFD3FC642D65C8C6F(L_201, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_203 = __this->get_U3CU3E4__this_2();
		NullCheck(L_203);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_204 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_203)->get_path_34();
		NullCheck(L_204);
		bool L_205;
		L_205 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_204, /*hidden argument*/NULL);
		float L_206 = __this->get_U3CmuU3E5__22_24();
		NullCheck(L_202);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_207;
		L_207 = ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A(L_202, L_205, L_206, /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A_RuntimeMethod_var);
		NullCheck(L_199);
		List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C(L_199, L_207, /*hidden argument*/List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_RuntimeMethod_var);
		// for (int j = 0; j < particlesInSpan; ++j)
		int32_t L_208 = __this->get_U3CjU3E5__21_23();
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_208, (int32_t)1));
		int32_t L_209 = V_3;
		__this->set_U3CjU3E5__21_23(L_209);
	}

IL_0636:
	{
		// for (int j = 0; j < particlesInSpan; ++j)
		int32_t L_210 = __this->get_U3CjU3E5__21_23();
		int32_t L_211 = __this->get_U3CparticlesInSpanU3E5__19_21();
		V_4 = (bool)((((int32_t)L_210) < ((int32_t)L_211))? 1 : 0);
		bool L_212 = V_4;
		if (L_212)
		{
			goto IL_046c;
		}
	}
	{
		// if (!(path.Closed && i == spans - 1))
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_213 = __this->get_U3CU3E4__this_2();
		NullCheck(L_213);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_214 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_213)->get_path_34();
		NullCheck(L_214);
		bool L_215;
		L_215 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_214, /*hidden argument*/NULL);
		if (!L_215)
		{
			goto IL_0674;
		}
	}
	{
		int32_t L_216 = __this->get_U3CiU3E5__14_16();
		int32_t L_217 = __this->get_U3CspansU3E5__9_11();
		G_B20_0 = ((((int32_t)((((int32_t)L_216) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_217, (int32_t)1))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0675;
	}

IL_0674:
	{
		G_B20_0 = 1;
	}

IL_0675:
	{
		V_5 = (bool)G_B20_0;
		bool L_218 = V_5;
		if (!L_218)
		{
			goto IL_06d0;
		}
	}
	{
		// groups[i + 1].particleIndices.Clear();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_219 = __this->get_U3CU3E4__this_2();
		NullCheck(L_219);
		List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * L_220 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_219)->get_groups_33();
		int32_t L_221 = __this->get_U3CiU3E5__14_16();
		NullCheck(L_220);
		ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE * L_222;
		L_222 = List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_inline(L_220, ((int32_t)il2cpp_codegen_add((int32_t)L_221, (int32_t)1)), /*hidden argument*/List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var);
		NullCheck(L_222);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_223 = L_222->get_particleIndices_4();
		NullCheck(L_223);
		List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A(L_223, /*hidden argument*/List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var);
		// groups[i + 1].particleIndices.Add(particlePositions.Count - 1);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_224 = __this->get_U3CU3E4__this_2();
		NullCheck(L_224);
		List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * L_225 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_224)->get_groups_33();
		int32_t L_226 = __this->get_U3CiU3E5__14_16();
		NullCheck(L_225);
		ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE * L_227;
		L_227 = List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_inline(L_225, ((int32_t)il2cpp_codegen_add((int32_t)L_226, (int32_t)1)), /*hidden argument*/List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var);
		NullCheck(L_227);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_228 = L_227->get_particleIndices_4();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_229 = __this->get_U3CparticlePositionsU3E5__1_3();
		NullCheck(L_229);
		int32_t L_230;
		L_230 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_229, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		NullCheck(L_228);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_228, ((int32_t)il2cpp_codegen_subtract((int32_t)L_230, (int32_t)1)), /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
	}

IL_06d0:
	{
		// if (i % 100 == 0)
		int32_t L_231 = __this->get_U3CiU3E5__14_16();
		V_6 = (bool)((((int32_t)((int32_t)((int32_t)L_231%(int32_t)((int32_t)100)))) == ((int32_t)0))? 1 : 0);
		bool L_232 = V_6;
		if (!L_232)
		{
			goto IL_0711;
		}
	}
	{
		// yield return new CoroutineJob.ProgressInfo("ObiRope: generating particles...", i / (float)spans);
		int32_t L_233 = __this->get_U3CiU3E5__14_16();
		int32_t L_234 = __this->get_U3CspansU3E5__9_11();
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_235 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_235, _stringLiteral903817815321EE2F94F9CC0B49F39AA16CC286AB, ((float)((float)((float)((float)L_233))/(float)((float)((float)L_234)))), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_235);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_070a:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0711:
	{
		// for (int i = 0; i < spans; i++)
		int32_t L_236 = __this->get_U3CiU3E5__14_16();
		V_3 = L_236;
		int32_t L_237 = V_3;
		__this->set_U3CiU3E5__14_16(((int32_t)il2cpp_codegen_add((int32_t)L_237, (int32_t)1)));
	}

IL_0722:
	{
		// for (int i = 0; i < spans; i++)
		int32_t L_238 = __this->get_U3CiU3E5__14_16();
		int32_t L_239 = __this->get_U3CspansU3E5__9_11();
		V_7 = (bool)((((int32_t)L_238) < ((int32_t)L_239))? 1 : 0);
		bool L_240 = V_7;
		if (L_240)
		{
			goto IL_03ab;
		}
	}
	{
		// m_ActiveParticleCount = particlePositions.Count;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_241 = __this->get_U3CU3E4__this_2();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_242 = __this->get_U3CparticlePositionsU3E5__1_3();
		NullCheck(L_242);
		int32_t L_243;
		L_243 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_242, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		NullCheck(L_241);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_241)->set_m_ActiveParticleCount_6(L_243);
		// totalParticles = m_ActiveParticleCount;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_244 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_245 = __this->get_U3CU3E4__this_2();
		NullCheck(L_245);
		int32_t L_246 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_245)->get_m_ActiveParticleCount_6();
		NullCheck(L_244);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_244)->set_totalParticles_38(L_246);
		// int numSegments = m_ActiveParticleCount - (path.Closed ? 0 : 1);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_247 = __this->get_U3CU3E4__this_2();
		NullCheck(L_247);
		int32_t L_248 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_247)->get_m_ActiveParticleCount_6();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_249 = __this->get_U3CU3E4__this_2();
		NullCheck(L_249);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_250 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_249)->get_path_34();
		NullCheck(L_250);
		bool L_251;
		L_251 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_250, /*hidden argument*/NULL);
		G_B28_0 = L_248;
		G_B28_1 = __this;
		if (L_251)
		{
			G_B29_0 = L_248;
			G_B29_1 = __this;
			goto IL_0786;
		}
	}
	{
		G_B30_0 = 1;
		G_B30_1 = G_B28_0;
		G_B30_2 = G_B28_1;
		goto IL_0787;
	}

IL_0786:
	{
		G_B30_0 = 0;
		G_B30_1 = G_B29_0;
		G_B30_2 = G_B29_1;
	}

IL_0787:
	{
		NullCheck(G_B30_2);
		G_B30_2->set_U3CnumSegmentsU3E5__10_12(((int32_t)il2cpp_codegen_subtract((int32_t)G_B30_1, (int32_t)G_B30_0)));
		// if (numSegments > 0)
		int32_t L_252 = __this->get_U3CnumSegmentsU3E5__10_12();
		V_8 = (bool)((((int32_t)L_252) > ((int32_t)0))? 1 : 0);
		bool L_253 = V_8;
		if (!L_253)
		{
			goto IL_07c1;
		}
	}
	{
		// m_InterParticleDistance = path.Length / (float)numSegments;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_254 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_255 = __this->get_U3CU3E4__this_2();
		NullCheck(L_255);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_256 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_255)->get_path_34();
		NullCheck(L_256);
		float L_257;
		L_257 = ObiPath_get_Length_m702A025AC3A2C08D2F9629728753916FA662A7E9(L_256, /*hidden argument*/NULL);
		int32_t L_258 = __this->get_U3CnumSegmentsU3E5__10_12();
		NullCheck(L_254);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_254)->set_m_InterParticleDistance_37(((float)((float)L_257/(float)((float)((float)L_258)))));
		goto IL_07d1;
	}

IL_07c1:
	{
		// m_InterParticleDistance = 0;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_259 = __this->get_U3CU3E4__this_2();
		NullCheck(L_259);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_259)->set_m_InterParticleDistance_37((0.0f));
	}

IL_07d1:
	{
		// positions = new Vector3[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_260 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_261 = __this->get_U3CU3E4__this_2();
		NullCheck(L_261);
		int32_t L_262 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_261)->get_totalParticles_38();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_263 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_262);
		NullCheck(L_260);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_260)->set_positions_9(L_263);
		// orientations = new Quaternion[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_264 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_265 = __this->get_U3CU3E4__this_2();
		NullCheck(L_265);
		int32_t L_266 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_265)->get_totalParticles_38();
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_267 = (QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6*)(QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6*)SZArrayNew(QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6_il2cpp_TypeInfo_var, (uint32_t)L_266);
		NullCheck(L_264);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_264)->set_orientations_11(L_267);
		// velocities = new Vector3[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_268 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_269 = __this->get_U3CU3E4__this_2();
		NullCheck(L_269);
		int32_t L_270 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_269)->get_totalParticles_38();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_271 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_270);
		NullCheck(L_268);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_268)->set_velocities_13(L_271);
		// angularVelocities = new Vector3[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_272 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_273 = __this->get_U3CU3E4__this_2();
		NullCheck(L_273);
		int32_t L_274 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_273)->get_totalParticles_38();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_275 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_274);
		NullCheck(L_272);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_272)->set_angularVelocities_14(L_275);
		// invMasses = new float[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_276 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_277 = __this->get_U3CU3E4__this_2();
		NullCheck(L_277);
		int32_t L_278 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_277)->get_totalParticles_38();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_279 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_278);
		NullCheck(L_276);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_276)->set_invMasses_15(L_279);
		// invRotationalMasses = new float[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_280 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_281 = __this->get_U3CU3E4__this_2();
		NullCheck(L_281);
		int32_t L_282 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_281)->get_totalParticles_38();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_283 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_282);
		NullCheck(L_280);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_280)->set_invRotationalMasses_16(L_283);
		// principalRadii = new Vector3[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_284 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_285 = __this->get_U3CU3E4__this_2();
		NullCheck(L_285);
		int32_t L_286 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_285)->get_totalParticles_38();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_287 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_286);
		NullCheck(L_284);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_284)->set_principalRadii_18(L_287);
		// filters = new int[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_288 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_289 = __this->get_U3CU3E4__this_2();
		NullCheck(L_289);
		int32_t L_290 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_289)->get_totalParticles_38();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_291 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)L_290);
		NullCheck(L_288);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_288)->set_filters_17(L_291);
		// restPositions = new Vector4[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_292 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_293 = __this->get_U3CU3E4__this_2();
		NullCheck(L_293);
		int32_t L_294 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_293)->get_totalParticles_38();
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_295 = (Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871*)(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871*)SZArrayNew(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871_il2cpp_TypeInfo_var, (uint32_t)L_294);
		NullCheck(L_292);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_292)->set_restPositions_10(L_295);
		// restOrientations = new Quaternion[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_296 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_297 = __this->get_U3CU3E4__this_2();
		NullCheck(L_297);
		int32_t L_298 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_297)->get_totalParticles_38();
		QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* L_299 = (QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6*)(QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6*)SZArrayNew(QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6_il2cpp_TypeInfo_var, (uint32_t)L_298);
		NullCheck(L_296);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_296)->set_restOrientations_12(L_299);
		// colors = new Color[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_300 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_301 = __this->get_U3CU3E4__this_2();
		NullCheck(L_301);
		int32_t L_302 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_301)->get_totalParticles_38();
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_303 = (ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)SZArrayNew(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var, (uint32_t)L_302);
		NullCheck(L_300);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_300)->set_colors_19(L_303);
		// restLengths = new float[totalParticles];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_304 = __this->get_U3CU3E4__this_2();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_305 = __this->get_U3CU3E4__this_2();
		NullCheck(L_305);
		int32_t L_306 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_305)->get_totalParticles_38();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_307 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_306);
		NullCheck(L_304);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_304)->set_restLengths_40(L_307);
		// for (int i = 0; i < m_ActiveParticleCount; i++)
		__this->set_U3CiU3E5__23_25(0);
		goto IL_0ac4;
	}

IL_0921:
	{
		// invMasses[i] = particleInvMasses[i];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_308 = __this->get_U3CU3E4__this_2();
		NullCheck(L_308);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_309 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_308)->get_invMasses_15();
		int32_t L_310 = __this->get_U3CiU3E5__23_25();
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_311 = __this->get_U3CparticleInvMassesU3E5__4_6();
		int32_t L_312 = __this->get_U3CiU3E5__23_25();
		NullCheck(L_311);
		float L_313;
		L_313 = List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_inline(L_311, L_312, /*hidden argument*/List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_RuntimeMethod_var);
		NullCheck(L_309);
		(L_309)->SetAt(static_cast<il2cpp_array_size_t>(L_310), (float)L_313);
		// invRotationalMasses[i] = particleInvRotationalMasses[i];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_314 = __this->get_U3CU3E4__this_2();
		NullCheck(L_314);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_315 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_314)->get_invRotationalMasses_16();
		int32_t L_316 = __this->get_U3CiU3E5__23_25();
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_317 = __this->get_U3CparticleInvRotationalMassesU3E5__5_7();
		int32_t L_318 = __this->get_U3CiU3E5__23_25();
		NullCheck(L_317);
		float L_319;
		L_319 = List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_inline(L_317, L_318, /*hidden argument*/List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_RuntimeMethod_var);
		NullCheck(L_315);
		(L_315)->SetAt(static_cast<il2cpp_array_size_t>(L_316), (float)L_319);
		// positions[i] = particlePositions[i];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_320 = __this->get_U3CU3E4__this_2();
		NullCheck(L_320);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_321 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_320)->get_positions_9();
		int32_t L_322 = __this->get_U3CiU3E5__23_25();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_323 = __this->get_U3CparticlePositionsU3E5__1_3();
		int32_t L_324 = __this->get_U3CiU3E5__23_25();
		NullCheck(L_323);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_325;
		L_325 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_323, L_324, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		NullCheck(L_321);
		(L_321)->SetAt(static_cast<il2cpp_array_size_t>(L_322), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_325);
		// restPositions[i] = positions[i];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_326 = __this->get_U3CU3E4__this_2();
		NullCheck(L_326);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_327 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_326)->get_restPositions_10();
		int32_t L_328 = __this->get_U3CiU3E5__23_25();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_329 = __this->get_U3CU3E4__this_2();
		NullCheck(L_329);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_330 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_329)->get_positions_9();
		int32_t L_331 = __this->get_U3CiU3E5__23_25();
		NullCheck(L_330);
		int32_t L_332 = L_331;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_333 = (L_330)->GetAt(static_cast<il2cpp_array_size_t>(L_332));
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_334;
		L_334 = Vector4_op_Implicit_mDCFA56E9D34979E1E2BFE6C2D61F1768D934A8EB(L_333, /*hidden argument*/NULL);
		NullCheck(L_327);
		(L_327)->SetAt(static_cast<il2cpp_array_size_t>(L_328), (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )L_334);
		// restPositions[i][3] = 1; // activate rest position.
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_335 = __this->get_U3CU3E4__this_2();
		NullCheck(L_335);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_336 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_335)->get_restPositions_10();
		int32_t L_337 = __this->get_U3CiU3E5__23_25();
		NullCheck(L_336);
		Vector4_set_Item_m7552B288FF218CA023F0DFB971BBA30D0362006A((Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 *)((L_336)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_337))), 3, (1.0f), /*hidden argument*/NULL);
		// principalRadii[i] = Vector3.one * particleThicknesses[i] * thickness;
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_338 = __this->get_U3CU3E4__this_2();
		NullCheck(L_338);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_339 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_338)->get_principalRadii_18();
		int32_t L_340 = __this->get_U3CiU3E5__23_25();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_341;
		L_341 = Vector3_get_one_m9CDE5C456038B133ED94402673859EC37B1C1CCB(/*hidden argument*/NULL);
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_342 = __this->get_U3CparticleThicknessesU3E5__3_5();
		int32_t L_343 = __this->get_U3CiU3E5__23_25();
		NullCheck(L_342);
		float L_344;
		L_344 = List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_inline(L_342, L_343, /*hidden argument*/List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_345;
		L_345 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_341, L_344, /*hidden argument*/NULL);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_346 = __this->get_U3CU3E4__this_2();
		NullCheck(L_346);
		float L_347 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_346)->get_thickness_35();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_348;
		L_348 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_345, L_347, /*hidden argument*/NULL);
		NullCheck(L_339);
		(L_339)->SetAt(static_cast<il2cpp_array_size_t>(L_340), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_348);
		// filters[i] = particleFilters[i];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_349 = __this->get_U3CU3E4__this_2();
		NullCheck(L_349);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_350 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_349)->get_filters_17();
		int32_t L_351 = __this->get_U3CiU3E5__23_25();
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_352 = __this->get_U3CparticleFiltersU3E5__6_8();
		int32_t L_353 = __this->get_U3CiU3E5__23_25();
		NullCheck(L_352);
		int32_t L_354;
		L_354 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_352, L_353, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_350);
		(L_350)->SetAt(static_cast<il2cpp_array_size_t>(L_351), (int32_t)L_354);
		// colors[i] = particleColors[i];
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_355 = __this->get_U3CU3E4__this_2();
		NullCheck(L_355);
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_356 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_355)->get_colors_19();
		int32_t L_357 = __this->get_U3CiU3E5__23_25();
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_358 = __this->get_U3CparticleColorsU3E5__7_9();
		int32_t L_359 = __this->get_U3CiU3E5__23_25();
		NullCheck(L_358);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_360;
		L_360 = List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_inline(L_358, L_359, /*hidden argument*/List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_RuntimeMethod_var);
		NullCheck(L_356);
		(L_356)->SetAt(static_cast<il2cpp_array_size_t>(L_357), (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 )L_360);
		// if (i % 100 == 0)
		int32_t L_361 = __this->get_U3CiU3E5__23_25();
		V_9 = (bool)((((int32_t)((int32_t)((int32_t)L_361%(int32_t)((int32_t)100)))) == ((int32_t)0))? 1 : 0);
		bool L_362 = V_9;
		if (!L_362)
		{
			goto IL_0ab3;
		}
	}
	{
		// yield return new CoroutineJob.ProgressInfo("ObiRod: generating particles...", i / (float)m_ActiveParticleCount);
		int32_t L_363 = __this->get_U3CiU3E5__23_25();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_364 = __this->get_U3CU3E4__this_2();
		NullCheck(L_364);
		int32_t L_365 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_364)->get_m_ActiveParticleCount_6();
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_366 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_366, _stringLiteralBDE3CEDBCC90855A8BBDCB3FE9B094E5B6AB184B, ((float)((float)((float)((float)L_363))/(float)((float)((float)L_365)))), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_366);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0aac:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0ab3:
	{
		// for (int i = 0; i < m_ActiveParticleCount; i++)
		int32_t L_367 = __this->get_U3CiU3E5__23_25();
		V_3 = L_367;
		int32_t L_368 = V_3;
		__this->set_U3CiU3E5__23_25(((int32_t)il2cpp_codegen_add((int32_t)L_368, (int32_t)1)));
	}

IL_0ac4:
	{
		// for (int i = 0; i < m_ActiveParticleCount; i++)
		int32_t L_369 = __this->get_U3CiU3E5__23_25();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_370 = __this->get_U3CU3E4__this_2();
		NullCheck(L_370);
		int32_t L_371 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_370)->get_m_ActiveParticleCount_6();
		V_10 = (bool)((((int32_t)L_369) < ((int32_t)L_371))? 1 : 0);
		bool L_372 = V_10;
		if (L_372)
		{
			goto IL_0921;
		}
	}
	{
		// CreateSimplices(numSegments);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_373 = __this->get_U3CU3E4__this_2();
		int32_t L_374 = __this->get_U3CnumSegmentsU3E5__10_12();
		NullCheck(L_373);
		ObiRopeBlueprintBase_CreateSimplices_mB064904BA09A240A79F509214EDD53AF206F9FDD(L_373, L_374, /*hidden argument*/NULL);
		// IEnumerator dc = CreateStretchShearConstraints(particleNormals);
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_375 = __this->get_U3CU3E4__this_2();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_376 = __this->get_U3CparticleNormalsU3E5__2_4();
		NullCheck(L_375);
		RuntimeObject* L_377;
		L_377 = VirtFuncInvoker1< RuntimeObject*, List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * >::Invoke(18 /* System.Collections.IEnumerator Obi.ObiRodBlueprint::CreateStretchShearConstraints(System.Collections.Generic.List`1<UnityEngine.Vector3>) */, L_375, L_376);
		__this->set_U3CdcU3E5__11_13(L_377);
		goto IL_0b2c;
	}

IL_0b0b:
	{
		// yield return dc.Current;
		RuntimeObject* L_378 = __this->get_U3CdcU3E5__11_13();
		NullCheck(L_378);
		RuntimeObject * L_379;
		L_379 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_378);
		__this->set_U3CU3E2__current_1(L_379);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_0b25:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0b2c:
	{
		// while (dc.MoveNext())
		RuntimeObject* L_380 = __this->get_U3CdcU3E5__11_13();
		NullCheck(L_380);
		bool L_381;
		L_381 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_380);
		V_11 = L_381;
		bool L_382 = V_11;
		if (L_382)
		{
			goto IL_0b0b;
		}
	}
	{
		// IEnumerator bc = CreateBendTwistConstraints();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_383 = __this->get_U3CU3E4__this_2();
		NullCheck(L_383);
		RuntimeObject* L_384;
		L_384 = VirtFuncInvoker0< RuntimeObject* >::Invoke(19 /* System.Collections.IEnumerator Obi.ObiRodBlueprint::CreateBendTwistConstraints() */, L_383);
		__this->set_U3CbcU3E5__12_14(L_384);
		goto IL_0b71;
	}

IL_0b50:
	{
		// yield return bc.Current;
		RuntimeObject* L_385 = __this->get_U3CbcU3E5__12_14();
		NullCheck(L_385);
		RuntimeObject * L_386;
		L_386 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_385);
		__this->set_U3CU3E2__current_1(L_386);
		__this->set_U3CU3E1__state_0(4);
		return (bool)1;
	}

IL_0b6a:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0b71:
	{
		// while (bc.MoveNext())
		RuntimeObject* L_387 = __this->get_U3CbcU3E5__12_14();
		NullCheck(L_387);
		bool L_388;
		L_388 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_387);
		V_12 = L_388;
		bool L_389 = V_12;
		if (L_389)
		{
			goto IL_0b50;
		}
	}
	{
		// IEnumerator cc = CreateChainConstraints();
		ObiRodBlueprint_t71343406010E59BAB94CD4D083B31C61E9E27597 * L_390 = __this->get_U3CU3E4__this_2();
		NullCheck(L_390);
		RuntimeObject* L_391;
		L_391 = VirtFuncInvoker0< RuntimeObject* >::Invoke(20 /* System.Collections.IEnumerator Obi.ObiRodBlueprint::CreateChainConstraints() */, L_390);
		__this->set_U3CccU3E5__13_15(L_391);
		goto IL_0bb6;
	}

IL_0b95:
	{
		// yield return cc.Current;
		RuntimeObject* L_392 = __this->get_U3CccU3E5__13_15();
		NullCheck(L_392);
		RuntimeObject * L_393;
		L_393 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_392);
		__this->set_U3CU3E2__current_1(L_393);
		__this->set_U3CU3E1__state_0(5);
		return (bool)1;
	}

IL_0baf:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0bb6:
	{
		// while (cc.MoveNext())
		RuntimeObject* L_394 = __this->get_U3CccU3E5__13_15();
		NullCheck(L_394);
		bool L_395;
		L_395 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_394);
		V_13 = L_395;
		bool L_396 = V_13;
		if (L_396)
		{
			goto IL_0b95;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object Obi.ObiRodBlueprint/<Initialize>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitializeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD199EE52973224E0C633E71C904164111168B9B7 (U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiRodBlueprint/<Initialize>d__3::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__3_System_Collections_IEnumerator_Reset_m70CE6B4088FBF17D28720288E2F251679B22BD11 (U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CInitializeU3Ed__3_System_Collections_IEnumerator_Reset_m70CE6B4088FBF17D28720288E2F251679B22BD11_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiRodBlueprint/<Initialize>d__3::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitializeU3Ed__3_System_Collections_IEnumerator_get_Current_m6B81FD82D37E5ED58B0B639232283B9902ED448B (U3CInitializeU3Ed__3_t977C268E07915B0DDAFE12C6ECBA644987046B2C * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiRope/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m1EB58583F6224716B470370411E9FAA7866AD049 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102 * L_0 = (U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102 *)il2cpp_codegen_object_new(U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m4B4378CC0AB345511F93C07572A6368778B3AE8E(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Obi.ObiRope/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m4B4378CC0AB345511F93C07572A6368778B3AE8E (U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Obi.ObiRope/<>c::<ApplyTearing>b__61_0(Obi.ObiStructuralElement,Obi.ObiStructuralElement)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3CApplyTearingU3Eb__61_0_mB850523B03651A79FDF5AA49504936E26D51D959 (U3CU3Ec_t9C7AD3F39A70616290A8D0FC924D2A12B1E63102 * __this, ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759 * ___x0, ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759 * ___y1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// return x.constraintForce.CompareTo(y.constraintForce);
		ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759 * L_0 = ___x0;
		NullCheck(L_0);
		float* L_1 = L_0->get_address_of_constraintForce_3();
		ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759 * L_2 = ___y1;
		NullCheck(L_2);
		float L_3 = L_2->get_constraintForce_3();
		int32_t L_4;
		L_4 = Single_CompareTo_m80B5B5A70A2343C3A8673F35635EBED4458109B4((float*)L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		// });
		int32_t L_5 = V_0;
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiRope/ObiRopeTornEventArgs::.ctor(Obi.ObiStructuralElement,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiRopeTornEventArgs__ctor_m9FDD4BA5BA9BEFAB7A96904A52289FCEA3624DF8 (ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * __this, ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759 * ___element0, int32_t ___particle1, const RuntimeMethod* method)
{
	{
		// public ObiRopeTornEventArgs(ObiStructuralElement element, int particle)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.element = element;
		ObiStructuralElement_t147EEA2CD4E7D8B12D83D1693EFB47416016A759 * L_0 = ___element0;
		__this->set_element_0(L_0);
		// this.particleIndex = particle;
		int32_t L_1 = ___particle1;
		__this->set_particleIndex_1(L_1);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiRope/RopeTornCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RopeTornCallback__ctor_m74940736BB0FE86E5D9978DE57B83F6514334BEE (RopeTornCallback_t7F7EC536306AFEEE07B7A037C3CC566AD1AF8D58 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Obi.ObiRope/RopeTornCallback::Invoke(Obi.ObiRope,Obi.ObiRope/ObiRopeTornEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RopeTornCallback_Invoke_mA260D7064D0816B8431CD1EFF00C0D5056B6CC55 (RopeTornCallback_t7F7EC536306AFEEE07B7A037C3CC566AD1AF8D58 * __this, ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 * ___rope0, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * ___tearInfo1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 *, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___rope0, ___tearInfo1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 *, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___rope0, ___tearInfo1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * >::Invoke(targetMethod, ___rope0, ___tearInfo1);
					else
						GenericVirtActionInvoker1< ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * >::Invoke(targetMethod, ___rope0, ___tearInfo1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___rope0, ___tearInfo1);
					else
						VirtActionInvoker1< ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___rope0, ___tearInfo1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 *, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___rope0, ___tearInfo1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 *, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * >::Invoke(targetMethod, targetThis, ___rope0, ___tearInfo1);
					else
						GenericVirtActionInvoker2< ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 *, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * >::Invoke(targetMethod, targetThis, ___rope0, ___tearInfo1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 *, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___rope0, ___tearInfo1);
					else
						VirtActionInvoker2< ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 *, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___rope0, ___tearInfo1);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 *, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___rope0, ___tearInfo1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 *, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___rope0, ___tearInfo1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Obi.ObiRope/RopeTornCallback::BeginInvoke(Obi.ObiRope,Obi.ObiRope/ObiRopeTornEventArgs,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RopeTornCallback_BeginInvoke_mFC33BEC1937BA04BC48653AFA9D75E530B95525C (RopeTornCallback_t7F7EC536306AFEEE07B7A037C3CC566AD1AF8D58 * __this, ObiRope_t42CB2BF81FCA209413C53D6C7530CCC3A06A6464 * ___rope0, ObiRopeTornEventArgs_tBCD778A667F0C58B625578169799D92364ECA68A * ___tearInfo1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___rope0;
	__d_args[1] = ___tearInfo1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);;
}
// System.Void Obi.ObiRope/RopeTornCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RopeTornCallback_EndInvoke_m9E58F646BB4EDF02833D0D8A97BF71746B3C5712 (RopeTornCallback_t7F7EC536306AFEEE07B7A037C3CC566AD1AF8D58 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateBendingConstraintsU3Ed__4__ctor_mF09A262B24850675B97784C745083E957AC70DDB (U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateBendingConstraintsU3Ed__4_System_IDisposable_Dispose_m1ACE661B7DC059891776A5AD8632DD995321EBA3 (U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCreateBendingConstraintsU3Ed__4_MoveNext_mBF7AB4425E651E4E8AE9B8FF0406A4DE65085F8E (U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m9DF9BE8ACA130E6DB8F004222C3FC66DA116FC4D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEACB9D9A15A8E3CD5021DA42030BE1FE13E7ED38);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		goto IL_001b;
	}

IL_0014:
	{
		goto IL_01c0;
	}

IL_0019:
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// bendConstraintsData = new ObiBendConstraintsData();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_3 = __this->get_U3CU3E4__this_2();
		ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * L_4 = (ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED *)il2cpp_codegen_object_new(ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED_il2cpp_TypeInfo_var);
		ObiBendConstraintsData__ctor_mFE69ED43DBE4731FAAAF8C32658B04596342980A(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_3)->set_bendConstraintsData_24(L_4);
		// bendConstraintsData.AddBatch(new ObiBendConstraintsBatch());
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * L_6 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_5)->get_bendConstraintsData_24();
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_7 = (ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 *)il2cpp_codegen_object_new(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_il2cpp_TypeInfo_var);
		ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9(L_7, (ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED *)NULL, /*hidden argument*/NULL);
		NullCheck(L_6);
		ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6(L_6, L_7, /*hidden argument*/ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6_RuntimeMethod_var);
		// bendConstraintsData.AddBatch(new ObiBendConstraintsBatch());
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_8 = __this->get_U3CU3E4__this_2();
		NullCheck(L_8);
		ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * L_9 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_8)->get_bendConstraintsData_24();
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_10 = (ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 *)il2cpp_codegen_object_new(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_il2cpp_TypeInfo_var);
		ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9(L_10, (ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED *)NULL, /*hidden argument*/NULL);
		NullCheck(L_9);
		ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6(L_9, L_10, /*hidden argument*/ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6_RuntimeMethod_var);
		// bendConstraintsData.AddBatch(new ObiBendConstraintsBatch());
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_11 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * L_12 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_11)->get_bendConstraintsData_24();
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_13 = (ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 *)il2cpp_codegen_object_new(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_il2cpp_TypeInfo_var);
		ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9(L_13, (ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED *)NULL, /*hidden argument*/NULL);
		NullCheck(L_12);
		ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6(L_12, L_13, /*hidden argument*/ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6_RuntimeMethod_var);
		// for (int i = 0; i < totalParticles - 2; i++)
		__this->set_U3CiU3E5__1_3(0);
		goto IL_01eb;
	}

IL_0084:
	{
		// var batch = bendConstraintsData.batches[i % 3] as ObiBendConstraintsBatch;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_14 = __this->get_U3CU3E4__this_2();
		NullCheck(L_14);
		ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * L_15 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_14)->get_bendConstraintsData_24();
		NullCheck(L_15);
		List_1_t2D69A7773F017ACB9111AC6D827B4A42239C9314 * L_16 = ((ObiConstraints_1_t1A77FB453F8651AB273DB0DE943C8AF5AF981F82 *)L_15)->get_batches_1();
		int32_t L_17 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_16);
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_18;
		L_18 = List_1_get_Item_m9DF9BE8ACA130E6DB8F004222C3FC66DA116FC4D_inline(L_16, ((int32_t)((int32_t)L_17%(int32_t)3)), /*hidden argument*/List_1_get_Item_m9DF9BE8ACA130E6DB8F004222C3FC66DA116FC4D_RuntimeMethod_var);
		__this->set_U3CbatchU3E5__2_4(L_18);
		// Vector3Int indices = new Vector3Int(i, i + 2, i + 1);
		int32_t L_19 = __this->get_U3CiU3E5__1_3();
		int32_t L_20 = __this->get_U3CiU3E5__1_3();
		int32_t L_21 = __this->get_U3CiU3E5__1_3();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_22;
		memset((&L_22), 0, sizeof(L_22));
		Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline((&L_22), L_19, ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)2)), ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U3CindicesU3E5__3_5(L_22);
		// float restBend = ObiUtils.RestBendingConstraint(restPositions[indices[0]], restPositions[indices[1]], restPositions[indices[2]]);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_23 = __this->get_U3CU3E4__this_2();
		NullCheck(L_23);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_24 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_23)->get_restPositions_10();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_25 = __this->get_address_of_U3CindicesU3E5__3_5();
		int32_t L_26;
		L_26 = Vector3Int_get_Item_mEF2FC37D6A65B456ABF7E75DE736A9286706A72D((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_25, 0, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_27 = L_26;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_28 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		L_29 = Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B(L_28, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_30 = __this->get_U3CU3E4__this_2();
		NullCheck(L_30);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_31 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_30)->get_restPositions_10();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_32 = __this->get_address_of_U3CindicesU3E5__3_5();
		int32_t L_33;
		L_33 = Vector3Int_get_Item_mEF2FC37D6A65B456ABF7E75DE736A9286706A72D((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_32, 1, /*hidden argument*/NULL);
		NullCheck(L_31);
		int32_t L_34 = L_33;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_35 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36;
		L_36 = Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B(L_35, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_37 = __this->get_U3CU3E4__this_2();
		NullCheck(L_37);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_38 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_37)->get_restPositions_10();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_39 = __this->get_address_of_U3CindicesU3E5__3_5();
		int32_t L_40;
		L_40 = Vector3Int_get_Item_mEF2FC37D6A65B456ABF7E75DE736A9286706A72D((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_39, 2, /*hidden argument*/NULL);
		NullCheck(L_38);
		int32_t L_41 = L_40;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_42 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_43;
		L_43 = Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B(L_42, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		float L_44;
		L_44 = ObiUtils_RestBendingConstraint_mA2A2E588C9B5B6907DC2670CF3176A0441D95C78_inline(L_29, L_36, L_43, /*hidden argument*/NULL);
		__this->set_U3CrestBendU3E5__4_6(L_44);
		// batch.AddConstraint(indices, restBend);
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_45 = __this->get_U3CbatchU3E5__2_4();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_46 = __this->get_U3CindicesU3E5__3_5();
		float L_47 = __this->get_U3CrestBendU3E5__4_6();
		NullCheck(L_45);
		ObiBendConstraintsBatch_AddConstraint_m59872A1ADDD39CF3FB2C303EA08FD4E6E9AFAA0C(L_45, L_46, L_47, /*hidden argument*/NULL);
		// if (i < m_ActiveParticleCount - 2)
		int32_t L_48 = __this->get_U3CiU3E5__1_3();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_49 = __this->get_U3CU3E4__this_2();
		NullCheck(L_49);
		int32_t L_50 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_49)->get_m_ActiveParticleCount_6();
		V_1 = (bool)((((int32_t)L_48) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_50, (int32_t)2))))? 1 : 0);
		bool L_51 = V_1;
		if (!L_51)
		{
			goto IL_017e;
		}
	}
	{
		// batch.activeConstraintCount++;
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_52 = __this->get_U3CbatchU3E5__2_4();
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_53 = L_52;
		NullCheck(L_53);
		int32_t L_54;
		L_54 = ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02(L_53, /*hidden argument*/NULL);
		V_2 = L_54;
		int32_t L_55 = V_2;
		NullCheck(L_53);
		ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A(L_53, ((int32_t)il2cpp_codegen_add((int32_t)L_55, (int32_t)1)), /*hidden argument*/NULL);
	}

IL_017e:
	{
		// if (i % 500 == 0)
		int32_t L_56 = __this->get_U3CiU3E5__1_3();
		V_3 = (bool)((((int32_t)((int32_t)((int32_t)L_56%(int32_t)((int32_t)500)))) == ((int32_t)0))? 1 : 0);
		bool L_57 = V_3;
		if (!L_57)
		{
			goto IL_01c7;
		}
	}
	{
		// yield return new CoroutineJob.ProgressInfo("ObiRope: generating structural constraints...", i / (float)(totalParticles - 2));
		int32_t L_58 = __this->get_U3CiU3E5__1_3();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_59 = __this->get_U3CU3E4__this_2();
		NullCheck(L_59);
		int32_t L_60 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_59)->get_totalParticles_38();
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_61 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_61, _stringLiteralEACB9D9A15A8E3CD5021DA42030BE1FE13E7ED38, ((float)((float)((float)((float)L_58))/(float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_60, (int32_t)2)))))), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_61);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_01c0:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_01c7:
	{
		__this->set_U3CbatchU3E5__2_4((ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 *)NULL);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_62 = __this->get_address_of_U3CindicesU3E5__3_5();
		il2cpp_codegen_initobj(L_62, sizeof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA ));
		// for (int i = 0; i < totalParticles - 2; i++)
		int32_t L_63 = __this->get_U3CiU3E5__1_3();
		V_2 = L_63;
		int32_t L_64 = V_2;
		__this->set_U3CiU3E5__1_3(((int32_t)il2cpp_codegen_add((int32_t)L_64, (int32_t)1)));
	}

IL_01eb:
	{
		// for (int i = 0; i < totalParticles - 2; i++)
		int32_t L_65 = __this->get_U3CiU3E5__1_3();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_66 = __this->get_U3CU3E4__this_2();
		NullCheck(L_66);
		int32_t L_67 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_66)->get_totalParticles_38();
		V_4 = (bool)((((int32_t)L_65) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_67, (int32_t)2))))? 1 : 0);
		bool L_68 = V_4;
		if (L_68)
		{
			goto IL_0084;
		}
	}
	{
		// if (path.Closed)
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_69 = __this->get_U3CU3E4__this_2();
		NullCheck(L_69);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_70 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_69)->get_path_34();
		NullCheck(L_70);
		bool L_71;
		L_71 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_70, /*hidden argument*/NULL);
		V_5 = L_71;
		bool L_72 = V_5;
		if (!L_72)
		{
			goto IL_031e;
		}
	}
	{
		// var loopClosingBatch = new ObiBendConstraintsBatch();
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_73 = (ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 *)il2cpp_codegen_object_new(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_il2cpp_TypeInfo_var);
		ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9(L_73, (ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED *)NULL, /*hidden argument*/NULL);
		__this->set_U3CloopClosingBatchU3E5__5_7(L_73);
		// bendConstraintsData.AddBatch(loopClosingBatch);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_74 = __this->get_U3CU3E4__this_2();
		NullCheck(L_74);
		ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * L_75 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_74)->get_bendConstraintsData_24();
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_76 = __this->get_U3CloopClosingBatchU3E5__5_7();
		NullCheck(L_75);
		ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6(L_75, L_76, /*hidden argument*/ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6_RuntimeMethod_var);
		// Vector3Int indices = new Vector3Int(m_ActiveParticleCount - 2, 0, m_ActiveParticleCount - 1);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_77 = __this->get_U3CU3E4__this_2();
		NullCheck(L_77);
		int32_t L_78 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_77)->get_m_ActiveParticleCount_6();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_79 = __this->get_U3CU3E4__this_2();
		NullCheck(L_79);
		int32_t L_80 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_79)->get_m_ActiveParticleCount_6();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_81;
		memset((&L_81), 0, sizeof(L_81));
		Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline((&L_81), ((int32_t)il2cpp_codegen_subtract((int32_t)L_78, (int32_t)2)), 0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_80, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U3CindicesU3E5__6_8(L_81);
		// loopClosingBatch.AddConstraint(indices, 0);
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_82 = __this->get_U3CloopClosingBatchU3E5__5_7();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_83 = __this->get_U3CindicesU3E5__6_8();
		NullCheck(L_82);
		ObiBendConstraintsBatch_AddConstraint_m59872A1ADDD39CF3FB2C303EA08FD4E6E9AFAA0C(L_82, L_83, (0.0f), /*hidden argument*/NULL);
		// loopClosingBatch.activeConstraintCount++;
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_84 = __this->get_U3CloopClosingBatchU3E5__5_7();
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_85 = L_84;
		NullCheck(L_85);
		int32_t L_86;
		L_86 = ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02(L_85, /*hidden argument*/NULL);
		V_2 = L_86;
		int32_t L_87 = V_2;
		NullCheck(L_85);
		ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A(L_85, ((int32_t)il2cpp_codegen_add((int32_t)L_87, (int32_t)1)), /*hidden argument*/NULL);
		// var loopClosingBatch2 = new ObiBendConstraintsBatch();
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_88 = (ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 *)il2cpp_codegen_object_new(ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_il2cpp_TypeInfo_var);
		ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9(L_88, (ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED *)NULL, /*hidden argument*/NULL);
		__this->set_U3CloopClosingBatch2U3E5__7_9(L_88);
		// bendConstraintsData.AddBatch(loopClosingBatch2);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_89 = __this->get_U3CU3E4__this_2();
		NullCheck(L_89);
		ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * L_90 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_89)->get_bendConstraintsData_24();
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_91 = __this->get_U3CloopClosingBatch2U3E5__7_9();
		NullCheck(L_90);
		ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6(L_90, L_91, /*hidden argument*/ObiConstraints_1_AddBatch_m9B2156C9E3DB9643986A6FD8363EB96540F85FD6_RuntimeMethod_var);
		// indices = new Vector3Int(m_ActiveParticleCount - 1, 1, 0);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_92 = __this->get_U3CU3E4__this_2();
		NullCheck(L_92);
		int32_t L_93 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_92)->get_m_ActiveParticleCount_6();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_94;
		memset((&L_94), 0, sizeof(L_94));
		Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline((&L_94), ((int32_t)il2cpp_codegen_subtract((int32_t)L_93, (int32_t)1)), 1, 0, /*hidden argument*/NULL);
		__this->set_U3CindicesU3E5__6_8(L_94);
		// loopClosingBatch2.AddConstraint(indices, 0);
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_95 = __this->get_U3CloopClosingBatch2U3E5__7_9();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_96 = __this->get_U3CindicesU3E5__6_8();
		NullCheck(L_95);
		ObiBendConstraintsBatch_AddConstraint_m59872A1ADDD39CF3FB2C303EA08FD4E6E9AFAA0C(L_95, L_96, (0.0f), /*hidden argument*/NULL);
		// loopClosingBatch2.activeConstraintCount++;
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_97 = __this->get_U3CloopClosingBatch2U3E5__7_9();
		ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 * L_98 = L_97;
		NullCheck(L_98);
		int32_t L_99;
		L_99 = ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02(L_98, /*hidden argument*/NULL);
		V_2 = L_99;
		int32_t L_100 = V_2;
		NullCheck(L_98);
		ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A(L_98, ((int32_t)il2cpp_codegen_add((int32_t)L_100, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U3CloopClosingBatchU3E5__5_7((ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 *)NULL);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_101 = __this->get_address_of_U3CindicesU3E5__6_8();
		il2cpp_codegen_initobj(L_101, sizeof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA ));
		__this->set_U3CloopClosingBatch2U3E5__7_9((ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79 *)NULL);
	}

IL_031e:
	{
		// }
		return (bool)0;
	}
}
// System.Object Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateBendingConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD5D1A5BD31642785B698F9B9734EBE8775F45FA6 (U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m8B106EC62D22296800CE67482EE3F81810C06907 (U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m8B106EC62D22296800CE67482EE3F81810C06907_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiRopeBlueprint/<CreateBendingConstraints>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateBendingConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m9475B499E2444C3687CB7A86ADA02EFA9627693A (U3CCreateBendingConstraintsU3Ed__4_t1A89A0F317DE8DB7E152406418C43DAFC5DF2683 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateDistanceConstraintsU3Ed__3__ctor_mC2881BA9A2C3B5FFD584032842FC4D8016768534 (U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_m1CE3F312F7B66A397DE190A3983DA6CB907431EC (U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCreateDistanceConstraintsU3Ed__3_MoveNext_mD9AAE5B282EF03644F559C1BC11856CE97E7F88A (U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m3F5E61F87B1504EBA4E43452FE85068B76A29181_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiConstraints_1_AddBatch_mF16FEB2CD535743D507C3E2F63782E8ED0FCFD11_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEACB9D9A15A8E3CD5021DA42030BE1FE13E7ED38);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		goto IL_001b;
	}

IL_0014:
	{
		goto IL_01d4;
	}

IL_0019:
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// distanceConstraintsData = new ObiDistanceConstraintsData();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_3 = __this->get_U3CU3E4__this_2();
		ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * L_4 = (ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB *)il2cpp_codegen_object_new(ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB_il2cpp_TypeInfo_var);
		ObiDistanceConstraintsData__ctor_m10E8E535222BCC55670623BBB7DA36CAF7A2A95E(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_3)->set_distanceConstraintsData_23(L_4);
		// distanceConstraintsData.AddBatch(new ObiDistanceConstraintsBatch());
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * L_6 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_5)->get_distanceConstraintsData_23();
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_7 = (ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE *)il2cpp_codegen_object_new(ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_il2cpp_TypeInfo_var);
		ObiDistanceConstraintsBatch__ctor_mE47DA7819812295CB5E274C6A916E54E705A0EC3(L_7, 0, /*hidden argument*/NULL);
		NullCheck(L_6);
		ObiConstraints_1_AddBatch_mF16FEB2CD535743D507C3E2F63782E8ED0FCFD11(L_6, L_7, /*hidden argument*/ObiConstraints_1_AddBatch_mF16FEB2CD535743D507C3E2F63782E8ED0FCFD11_RuntimeMethod_var);
		// distanceConstraintsData.AddBatch(new ObiDistanceConstraintsBatch());
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_8 = __this->get_U3CU3E4__this_2();
		NullCheck(L_8);
		ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * L_9 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_8)->get_distanceConstraintsData_23();
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_10 = (ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE *)il2cpp_codegen_object_new(ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_il2cpp_TypeInfo_var);
		ObiDistanceConstraintsBatch__ctor_mE47DA7819812295CB5E274C6A916E54E705A0EC3(L_10, 0, /*hidden argument*/NULL);
		NullCheck(L_9);
		ObiConstraints_1_AddBatch_mF16FEB2CD535743D507C3E2F63782E8ED0FCFD11(L_9, L_10, /*hidden argument*/ObiConstraints_1_AddBatch_mF16FEB2CD535743D507C3E2F63782E8ED0FCFD11_RuntimeMethod_var);
		// for (int i = 0; i < totalParticles - 1; i++)
		__this->set_U3CiU3E5__1_3(0);
		goto IL_01f3;
	}

IL_006d:
	{
		// var batch = distanceConstraintsData.batches[i % 2] as ObiDistanceConstraintsBatch;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_11 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * L_12 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_11)->get_distanceConstraintsData_23();
		NullCheck(L_12);
		List_1_t5EE7BEFE060CE680419D725E327D37E34A831E6A * L_13 = ((ObiConstraints_1_tDDB54978FB9AEE6BD8B391B96A13174F2E9966ED *)L_12)->get_batches_1();
		int32_t L_14 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_13);
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_15;
		L_15 = List_1_get_Item_m3F5E61F87B1504EBA4E43452FE85068B76A29181_inline(L_13, ((int32_t)((int32_t)L_14%(int32_t)2)), /*hidden argument*/List_1_get_Item_m3F5E61F87B1504EBA4E43452FE85068B76A29181_RuntimeMethod_var);
		__this->set_U3CbatchU3E5__2_4(L_15);
		// if (i < m_ActiveParticleCount - 1)
		int32_t L_16 = __this->get_U3CiU3E5__1_3();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_17 = __this->get_U3CU3E4__this_2();
		NullCheck(L_17);
		int32_t L_18 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_17)->get_m_ActiveParticleCount_6();
		V_1 = (bool)((((int32_t)L_16) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)1))))? 1 : 0);
		bool L_19 = V_1;
		if (!L_19)
		{
			goto IL_015d;
		}
	}
	{
		// Vector2Int indices = new Vector2Int(i, i + 1);
		int32_t L_20 = __this->get_U3CiU3E5__1_3();
		int32_t L_21 = __this->get_U3CiU3E5__1_3();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  L_22;
		memset((&L_22), 0, sizeof(L_22));
		Vector2Int__ctor_mB2B73108B6DD3ADC1B515D7DD9116ECAC6833726_inline((&L_22), L_20, ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U3CindicesU3E5__3_5(L_22);
		// restLengths[i] = Vector3.Distance(positions[indices.x], positions[indices.y]);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_23 = __this->get_U3CU3E4__this_2();
		NullCheck(L_23);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_24 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_23)->get_restLengths_40();
		int32_t L_25 = __this->get_U3CiU3E5__1_3();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_26 = __this->get_U3CU3E4__this_2();
		NullCheck(L_26);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_27 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_26)->get_positions_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_28 = __this->get_address_of_U3CindicesU3E5__3_5();
		int32_t L_29;
		L_29 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		int32_t L_30 = L_29;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_32 = __this->get_U3CU3E4__this_2();
		NullCheck(L_32);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_33 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_32)->get_positions_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_34 = __this->get_address_of_U3CindicesU3E5__3_5();
		int32_t L_35;
		L_35 = Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		int32_t L_36 = L_35;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		float L_38;
		L_38 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_31, L_37, /*hidden argument*/NULL);
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_25), (float)L_38);
		// batch.AddConstraint(indices, restLengths[i]);
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_39 = __this->get_U3CbatchU3E5__2_4();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  L_40 = __this->get_U3CindicesU3E5__3_5();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_41 = __this->get_U3CU3E4__this_2();
		NullCheck(L_41);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_42 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_41)->get_restLengths_40();
		int32_t L_43 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_42);
		int32_t L_44 = L_43;
		float L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		NullCheck(L_39);
		ObiDistanceConstraintsBatch_AddConstraint_m118C1575A23FFAC933204048E9DD894F44CDD021(L_39, L_40, L_45, /*hidden argument*/NULL);
		// batch.activeConstraintCount++;
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_46 = __this->get_U3CbatchU3E5__2_4();
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_47 = L_46;
		NullCheck(L_47);
		int32_t L_48;
		L_48 = ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02(L_47, /*hidden argument*/NULL);
		V_2 = L_48;
		int32_t L_49 = V_2;
		NullCheck(L_47);
		ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A(L_47, ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1)), /*hidden argument*/NULL);
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_50 = __this->get_address_of_U3CindicesU3E5__3_5();
		il2cpp_codegen_initobj(L_50, sizeof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 ));
		goto IL_0192;
	}

IL_015d:
	{
		// restLengths[i] = m_InterParticleDistance;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_51 = __this->get_U3CU3E4__this_2();
		NullCheck(L_51);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_52 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_51)->get_restLengths_40();
		int32_t L_53 = __this->get_U3CiU3E5__1_3();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_54 = __this->get_U3CU3E4__this_2();
		NullCheck(L_54);
		float L_55 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_54)->get_m_InterParticleDistance_37();
		NullCheck(L_52);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(L_53), (float)L_55);
		// batch.AddConstraint(Vector2Int.zero, 0);
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_56 = __this->get_U3CbatchU3E5__2_4();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  L_57;
		L_57 = Vector2Int_get_zero_m89F24AE488182BAB38B381B445B8868B3DD5A2AE(/*hidden argument*/NULL);
		NullCheck(L_56);
		ObiDistanceConstraintsBatch_AddConstraint_m118C1575A23FFAC933204048E9DD894F44CDD021(L_56, L_57, (0.0f), /*hidden argument*/NULL);
	}

IL_0192:
	{
		// if (i % 500 == 0)
		int32_t L_58 = __this->get_U3CiU3E5__1_3();
		V_3 = (bool)((((int32_t)((int32_t)((int32_t)L_58%(int32_t)((int32_t)500)))) == ((int32_t)0))? 1 : 0);
		bool L_59 = V_3;
		if (!L_59)
		{
			goto IL_01db;
		}
	}
	{
		// yield return new CoroutineJob.ProgressInfo("ObiRope: generating structural constraints...", i / (float)(totalParticles - 1));
		int32_t L_60 = __this->get_U3CiU3E5__1_3();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_61 = __this->get_U3CU3E4__this_2();
		NullCheck(L_61);
		int32_t L_62 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_61)->get_totalParticles_38();
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_63 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_63, _stringLiteralEACB9D9A15A8E3CD5021DA42030BE1FE13E7ED38, ((float)((float)((float)((float)L_60))/(float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_62, (int32_t)1)))))), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_63);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_01d4:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_01db:
	{
		__this->set_U3CbatchU3E5__2_4((ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE *)NULL);
		// for (int i = 0; i < totalParticles - 1; i++)
		int32_t L_64 = __this->get_U3CiU3E5__1_3();
		V_2 = L_64;
		int32_t L_65 = V_2;
		__this->set_U3CiU3E5__1_3(((int32_t)il2cpp_codegen_add((int32_t)L_65, (int32_t)1)));
	}

IL_01f3:
	{
		// for (int i = 0; i < totalParticles - 1; i++)
		int32_t L_66 = __this->get_U3CiU3E5__1_3();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_67 = __this->get_U3CU3E4__this_2();
		NullCheck(L_67);
		int32_t L_68 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_67)->get_totalParticles_38();
		V_4 = (bool)((((int32_t)L_66) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_68, (int32_t)1))))? 1 : 0);
		bool L_69 = V_4;
		if (L_69)
		{
			goto IL_006d;
		}
	}
	{
		// if (path.Closed)
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_70 = __this->get_U3CU3E4__this_2();
		NullCheck(L_70);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_71 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_70)->get_path_34();
		NullCheck(L_71);
		bool L_72;
		L_72 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_71, /*hidden argument*/NULL);
		V_5 = L_72;
		bool L_73 = V_5;
		if (!L_73)
		{
			goto IL_0310;
		}
	}
	{
		// var loopClosingBatch = new ObiDistanceConstraintsBatch();
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_74 = (ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE *)il2cpp_codegen_object_new(ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_il2cpp_TypeInfo_var);
		ObiDistanceConstraintsBatch__ctor_mE47DA7819812295CB5E274C6A916E54E705A0EC3(L_74, 0, /*hidden argument*/NULL);
		__this->set_U3CloopClosingBatchU3E5__4_6(L_74);
		// distanceConstraintsData.AddBatch(loopClosingBatch);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_75 = __this->get_U3CU3E4__this_2();
		NullCheck(L_75);
		ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * L_76 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_75)->get_distanceConstraintsData_23();
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_77 = __this->get_U3CloopClosingBatchU3E5__4_6();
		NullCheck(L_76);
		ObiConstraints_1_AddBatch_mF16FEB2CD535743D507C3E2F63782E8ED0FCFD11(L_76, L_77, /*hidden argument*/ObiConstraints_1_AddBatch_mF16FEB2CD535743D507C3E2F63782E8ED0FCFD11_RuntimeMethod_var);
		// Vector2Int indices = new Vector2Int(m_ActiveParticleCount - 1, 0);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_78 = __this->get_U3CU3E4__this_2();
		NullCheck(L_78);
		int32_t L_79 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_78)->get_m_ActiveParticleCount_6();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  L_80;
		memset((&L_80), 0, sizeof(L_80));
		Vector2Int__ctor_mB2B73108B6DD3ADC1B515D7DD9116ECAC6833726_inline((&L_80), ((int32_t)il2cpp_codegen_subtract((int32_t)L_79, (int32_t)1)), 0, /*hidden argument*/NULL);
		__this->set_U3CindicesU3E5__5_7(L_80);
		// restLengths[m_ActiveParticleCount - 2] = Vector3.Distance(positions[indices.x], positions[indices.y]);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_81 = __this->get_U3CU3E4__this_2();
		NullCheck(L_81);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_82 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_81)->get_restLengths_40();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_83 = __this->get_U3CU3E4__this_2();
		NullCheck(L_83);
		int32_t L_84 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_83)->get_m_ActiveParticleCount_6();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_85 = __this->get_U3CU3E4__this_2();
		NullCheck(L_85);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_86 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_85)->get_positions_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_87 = __this->get_address_of_U3CindicesU3E5__5_7();
		int32_t L_88;
		L_88 = Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_87, /*hidden argument*/NULL);
		NullCheck(L_86);
		int32_t L_89 = L_88;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_90 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_89));
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_91 = __this->get_U3CU3E4__this_2();
		NullCheck(L_91);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_92 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_91)->get_positions_9();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_93 = __this->get_address_of_U3CindicesU3E5__5_7();
		int32_t L_94;
		L_94 = Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline((Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 *)L_93, /*hidden argument*/NULL);
		NullCheck(L_92);
		int32_t L_95 = L_94;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_96 = (L_92)->GetAt(static_cast<il2cpp_array_size_t>(L_95));
		float L_97;
		L_97 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_90, L_96, /*hidden argument*/NULL);
		NullCheck(L_82);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_84, (int32_t)2))), (float)L_97);
		// loopClosingBatch.AddConstraint(indices, restLengths[m_ActiveParticleCount - 2]);
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_98 = __this->get_U3CloopClosingBatchU3E5__4_6();
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9  L_99 = __this->get_U3CindicesU3E5__5_7();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_100 = __this->get_U3CU3E4__this_2();
		NullCheck(L_100);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_101 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_100)->get_restLengths_40();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_102 = __this->get_U3CU3E4__this_2();
		NullCheck(L_102);
		int32_t L_103 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_102)->get_m_ActiveParticleCount_6();
		NullCheck(L_101);
		int32_t L_104 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_103, (int32_t)2));
		float L_105 = (L_101)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		NullCheck(L_98);
		ObiDistanceConstraintsBatch_AddConstraint_m118C1575A23FFAC933204048E9DD894F44CDD021(L_98, L_99, L_105, /*hidden argument*/NULL);
		// loopClosingBatch.activeConstraintCount++;
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_106 = __this->get_U3CloopClosingBatchU3E5__4_6();
		ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE * L_107 = L_106;
		NullCheck(L_107);
		int32_t L_108;
		L_108 = ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02(L_107, /*hidden argument*/NULL);
		V_2 = L_108;
		int32_t L_109 = V_2;
		NullCheck(L_107);
		ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A(L_107, ((int32_t)il2cpp_codegen_add((int32_t)L_109, (int32_t)1)), /*hidden argument*/NULL);
		__this->set_U3CloopClosingBatchU3E5__4_6((ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE *)NULL);
		Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * L_110 = __this->get_address_of_U3CindicesU3E5__5_7();
		il2cpp_codegen_initobj(L_110, sizeof(Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 ));
	}

IL_0310:
	{
		// }
		return (bool)0;
	}
}
// System.Object Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m74A1152B6135923B3BAACBAC1F2F5932512D8046 (U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mD6AC7761E802DE2F6F0CADB0A5682051E6E4669C (U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mD6AC7761E802DE2F6F0CADB0A5682051E6E4669C_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiRopeBlueprint/<CreateDistanceConstraints>d__3::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8EF851B496CA7FD1CC7F31747EEF8484A7FCBB15 (U3CCreateDistanceConstraintsU3Ed__3_tDEF721875AC432A5347D23A5C09FFEC79F31CC32 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiRopeBlueprint/<Initialize>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__2__ctor_mE9943407AC4BCDD21A5F1BB4759F285BAE447D8D (U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.ObiRopeBlueprint/<Initialize>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__2_System_IDisposable_Dispose_m97A6F5247B0287130D582F2BB4E544062A689B13 (U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiRopeBlueprint/<Initialize>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CInitializeU3Ed__2_MoveNext_m0A32F555A52037AFECFE0D01A86660C18222209F (U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral903817815321EE2F94F9CC0B49F39AA16CC286AB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD368B3509E2BA452C46794E1AC89E84E3642498F);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	bool V_12 = false;
	int32_t G_B19_0 = 0;
	int32_t G_B28_0 = 0;
	U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7 * G_B28_1 = NULL;
	int32_t G_B27_0 = 0;
	U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7 * G_B27_1 = NULL;
	int32_t G_B29_0 = 0;
	int32_t G_B29_1 = 0;
	U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7 * G_B29_2 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0025;
			}
			case 2:
			{
				goto IL_002a;
			}
			case 3:
			{
				goto IL_002f;
			}
			case 4:
			{
				goto IL_0034;
			}
		}
	}
	{
		goto IL_0039;
	}

IL_0023:
	{
		goto IL_003b;
	}

IL_0025:
	{
		goto IL_0607;
	}

IL_002a:
	{
		goto IL_0926;
	}

IL_002f:
	{
		goto IL_0999;
	}

IL_0034:
	{
		goto IL_09de;
	}

IL_0039:
	{
		return (bool)0;
	}

IL_003b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (path.ControlPointCount < 2)
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_3 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_2)->get_path_34();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = ObiPath_get_ControlPointCount_m09869836E934AC2B253907636DDE6D72BA95D05F(L_3, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_4) < ((int32_t)2))? 1 : 0);
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0128;
		}
	}
	{
		// ClearParticleGroups();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_6 = __this->get_U3CU3E4__this_2();
		NullCheck(L_6);
		ObiActorBlueprint_ClearParticleGroups_mBC589A70CC5E642C10FD126B482590D27FEC8B18(L_6, (bool)1, /*hidden argument*/NULL);
		// path.InsertControlPoint(0, Vector3.left, Vector3.left * 0.25f, Vector3.right * 0.25f, Vector3.up, DEFAULT_PARTICLE_MASS, 1, 1, ObiUtils.MakeFilter(ObiUtils.CollideWithEverything,1), Color.white, "control point");
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_8 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_7)->get_path_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_10, (0.25f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_12, (0.25f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		int32_t L_15;
		L_15 = ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3_inline(((int32_t)65535), 1, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_16;
		L_16 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		NullCheck(L_8);
		ObiPath_InsertControlPoint_m978D7BDBC828882C6D3B1C85F029C6B1BC63BDDA(L_8, 0, L_9, L_11, L_13, L_14, (0.100000001f), (1.0f), (1.0f), L_15, L_16, _stringLiteralD368B3509E2BA452C46794E1AC89E84E3642498F, /*hidden argument*/NULL);
		// path.InsertControlPoint(1, Vector3.right, Vector3.left * 0.25f, Vector3.right * 0.25f, Vector3.up, DEFAULT_PARTICLE_MASS, 1, 1, ObiUtils.MakeFilter(ObiUtils.CollideWithEverything, 1), Color.white, "control point");
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_17 = __this->get_U3CU3E4__this_2();
		NullCheck(L_17);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_18 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_17)->get_path_34();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_20, (0.25f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_22, (0.25f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24;
		L_24 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		int32_t L_25;
		L_25 = ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3_inline(((int32_t)65535), 1, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_26;
		L_26 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		NullCheck(L_18);
		ObiPath_InsertControlPoint_m978D7BDBC828882C6D3B1C85F029C6B1BC63BDDA(L_18, 1, L_19, L_21, L_23, L_24, (0.100000001f), (1.0f), (1.0f), L_25, L_26, _stringLiteralD368B3509E2BA452C46794E1AC89E84E3642498F, /*hidden argument*/NULL);
	}

IL_0128:
	{
		// path.RecalculateLenght(Matrix4x4.identity, 0.00001f, 7);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_27 = __this->get_U3CU3E4__this_2();
		NullCheck(L_27);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_28 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_27)->get_path_34();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_29;
		L_29 = Matrix4x4_get_identity_mC91289718DDD3DDBE0A10551BDA59A446414A596(/*hidden argument*/NULL);
		NullCheck(L_28);
		float L_30;
		L_30 = ObiPath_RecalculateLenght_mDF1CD450DBBA7B9823BA8090C340595338260C0E(L_28, L_29, (9.99999975E-06f), 7, /*hidden argument*/NULL);
		// List<Vector3> particlePositions = new List<Vector3>();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_31 = (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *)il2cpp_codegen_object_new(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		List_1__ctor_mF8F23D572031748AD428623AE16803455997E297(L_31, /*hidden argument*/List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		__this->set_U3CparticlePositionsU3E5__1_3(L_31);
		// List<float> particleThicknesses = new List<float>();
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_32 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)il2cpp_codegen_object_new(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_il2cpp_TypeInfo_var);
		List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C(L_32, /*hidden argument*/List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C_RuntimeMethod_var);
		__this->set_U3CparticleThicknessesU3E5__2_4(L_32);
		// List<float> particleInvMasses = new List<float>();
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_33 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)il2cpp_codegen_object_new(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_il2cpp_TypeInfo_var);
		List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C(L_33, /*hidden argument*/List_1__ctor_m893CC03BA01C82718A6ED996182C83EA91A0E74C_RuntimeMethod_var);
		__this->set_U3CparticleInvMassesU3E5__3_5(L_33);
		// List<int> particleFilters = new List<int>();
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_34 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)il2cpp_codegen_object_new(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD(L_34, /*hidden argument*/List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		__this->set_U3CparticleFiltersU3E5__4_6(L_34);
		// List<Color> particleColors = new List<Color>();
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_35 = (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E *)il2cpp_codegen_object_new(List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E_il2cpp_TypeInfo_var);
		List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1(L_35, /*hidden argument*/List_1__ctor_mB2F7CCB002CAC8ADE6AAC80A37E8AA25DEA75AE1_RuntimeMethod_var);
		__this->set_U3CparticleColorsU3E5__5_7(L_35);
		// if (!path.Closed)
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_36 = __this->get_U3CU3E4__this_2();
		NullCheck(L_36);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_37 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_36)->get_path_34();
		NullCheck(L_37);
		bool L_38;
		L_38 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_37, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_38) == ((int32_t)0))? 1 : 0);
		bool L_39 = V_2;
		if (!L_39)
		{
			goto IL_02aa;
		}
	}
	{
		// particlePositions.Add(path.points.GetPositionAtMu(path.Closed, 0));
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_40 = __this->get_U3CparticlePositionsU3E5__1_3();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_41 = __this->get_U3CU3E4__this_2();
		NullCheck(L_41);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_42 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_41)->get_path_34();
		NullCheck(L_42);
		ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507 * L_43;
		L_43 = ObiPath_get_points_m6F044F0A220D99C2C5184CB5FE1769BAED6FFD3C(L_42, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_44 = __this->get_U3CU3E4__this_2();
		NullCheck(L_44);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_45 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_44)->get_path_34();
		NullCheck(L_45);
		bool L_46;
		L_46 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_45, /*hidden argument*/NULL);
		NullCheck(L_43);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47;
		L_47 = ObiPointsDataChannel_GetPositionAtMu_m5DA1AF5F4A0C5EB6EDAE4AA729DAC149E3E17DF5(L_43, L_46, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_40);
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_40, L_47, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// particleThicknesses.Add(path.thicknesses.GetAtMu(path.Closed, 0));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_48 = __this->get_U3CparticleThicknessesU3E5__2_4();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_49 = __this->get_U3CU3E4__this_2();
		NullCheck(L_49);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_50 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_49)->get_path_34();
		NullCheck(L_50);
		ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C * L_51;
		L_51 = ObiPath_get_thicknesses_m435418A103FD1CC0D151EF3B75689969F0E23C8D(L_50, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_52 = __this->get_U3CU3E4__this_2();
		NullCheck(L_52);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_53 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_52)->get_path_34();
		NullCheck(L_53);
		bool L_54;
		L_54 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_53, /*hidden argument*/NULL);
		NullCheck(L_51);
		float L_55;
		L_55 = ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3(L_51, L_54, (0.0f), /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		NullCheck(L_48);
		List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A(L_48, L_55, /*hidden argument*/List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		// particleInvMasses.Add(ObiUtils.MassToInvMass(path.masses.GetAtMu(path.Closed, 0)));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_56 = __this->get_U3CparticleInvMassesU3E5__3_5();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_57 = __this->get_U3CU3E4__this_2();
		NullCheck(L_57);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_58 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_57)->get_path_34();
		NullCheck(L_58);
		ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996 * L_59;
		L_59 = ObiPath_get_masses_m563E313A7734C29F2476B68228BAB71731EBC326(L_58, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_60 = __this->get_U3CU3E4__this_2();
		NullCheck(L_60);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_61 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_60)->get_path_34();
		NullCheck(L_61);
		bool L_62;
		L_62 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_61, /*hidden argument*/NULL);
		NullCheck(L_59);
		float L_63;
		L_63 = ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3(L_59, L_62, (0.0f), /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		float L_64;
		L_64 = ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68_inline(L_63, /*hidden argument*/NULL);
		NullCheck(L_56);
		List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A(L_56, L_64, /*hidden argument*/List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		// particleFilters.Add(path.filters.GetAtMu(path.Closed, 0));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_65 = __this->get_U3CparticleFiltersU3E5__4_6();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_66 = __this->get_U3CU3E4__this_2();
		NullCheck(L_66);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_67 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_66)->get_path_34();
		NullCheck(L_67);
		ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670 * L_68;
		L_68 = ObiPath_get_filters_m6D02BA0F0D6E24496882A16B37C12B185D9CC028(L_67, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_69 = __this->get_U3CU3E4__this_2();
		NullCheck(L_69);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_70 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_69)->get_path_34();
		NullCheck(L_70);
		bool L_71;
		L_71 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_70, /*hidden argument*/NULL);
		NullCheck(L_68);
		int32_t L_72;
		L_72 = ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB(L_68, L_71, (0.0f), /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB_RuntimeMethod_var);
		NullCheck(L_65);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_65, L_72, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// particleColors.Add(path.colors.GetAtMu(path.Closed, 0));
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_73 = __this->get_U3CparticleColorsU3E5__5_7();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_74 = __this->get_U3CU3E4__this_2();
		NullCheck(L_74);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_75 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_74)->get_path_34();
		NullCheck(L_75);
		ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2 * L_76;
		L_76 = ObiPath_get_colors_mB1315A257BB672D26575C3CEFD3FC642D65C8C6F(L_75, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_77 = __this->get_U3CU3E4__this_2();
		NullCheck(L_77);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_78 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_77)->get_path_34();
		NullCheck(L_78);
		bool L_79;
		L_79 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_78, /*hidden argument*/NULL);
		NullCheck(L_76);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_80;
		L_80 = ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A(L_76, L_79, (0.0f), /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A_RuntimeMethod_var);
		NullCheck(L_73);
		List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C(L_73, L_80, /*hidden argument*/List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_RuntimeMethod_var);
	}

IL_02aa:
	{
		// groups[0].particleIndices.Clear();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_81 = __this->get_U3CU3E4__this_2();
		NullCheck(L_81);
		List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * L_82 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_81)->get_groups_33();
		NullCheck(L_82);
		ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE * L_83;
		L_83 = List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_inline(L_82, 0, /*hidden argument*/List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var);
		NullCheck(L_83);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_84 = L_83->get_particleIndices_4();
		NullCheck(L_84);
		List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A(L_84, /*hidden argument*/List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var);
		// groups[0].particleIndices.Add(0);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_85 = __this->get_U3CU3E4__this_2();
		NullCheck(L_85);
		List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * L_86 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_85)->get_groups_33();
		NullCheck(L_86);
		ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE * L_87;
		L_87 = List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_inline(L_86, 0, /*hidden argument*/List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var);
		NullCheck(L_87);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_88 = L_87->get_particleIndices_4();
		NullCheck(L_88);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_88, 0, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// ReadOnlyCollection<float> lengthTable = path.ArcLengthTable;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_89 = __this->get_U3CU3E4__this_2();
		NullCheck(L_89);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_90 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_89)->get_path_34();
		NullCheck(L_90);
		ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * L_91;
		L_91 = ObiPath_get_ArcLengthTable_m473E96A0299C036CB1835FE04F20733B6D824C81(L_90, /*hidden argument*/NULL);
		__this->set_U3ClengthTableU3E5__6_8(L_91);
		// int spans = path.GetSpanCount();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_92 = __this->get_U3CU3E4__this_2();
		NullCheck(L_92);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_93 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_92)->get_path_34();
		NullCheck(L_93);
		int32_t L_94;
		L_94 = ObiPath_GetSpanCount_m09199A6F5BB9A1619E0A8A7FB826FA1472B29623(L_93, /*hidden argument*/NULL);
		__this->set_U3CspansU3E5__7_9(L_94);
		// for (int i = 0; i < spans; i++)
		__this->set_U3CiU3E5__11_13(0);
		goto IL_061f;
	}

IL_031b:
	{
		// int firstArcLengthSample = i * (path.ArcLengthSamples + 1);
		int32_t L_95 = __this->get_U3CiU3E5__11_13();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_96 = __this->get_U3CU3E4__this_2();
		NullCheck(L_96);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_97 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_96)->get_path_34();
		NullCheck(L_97);
		int32_t L_98;
		L_98 = ObiPath_get_ArcLengthSamples_m3C36FC6DDE2FBD76AD4F46EB7C5A17FDC4B1D9BA(L_97, /*hidden argument*/NULL);
		__this->set_U3CfirstArcLengthSampleU3E5__12_14(((int32_t)il2cpp_codegen_multiply((int32_t)L_95, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_98, (int32_t)1)))));
		// int lastArcLengthSample = (i + 1) * (path.ArcLengthSamples + 1);
		int32_t L_99 = __this->get_U3CiU3E5__11_13();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_100 = __this->get_U3CU3E4__this_2();
		NullCheck(L_100);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_101 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_100)->get_path_34();
		NullCheck(L_101);
		int32_t L_102;
		L_102 = ObiPath_get_ArcLengthSamples_m3C36FC6DDE2FBD76AD4F46EB7C5A17FDC4B1D9BA(L_101, /*hidden argument*/NULL);
		__this->set_U3ClastArcLengthSampleU3E5__13_15(((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_99, (int32_t)1)), (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_102, (int32_t)1)))));
		// float upToSpanLength = lengthTable[firstArcLengthSample];
		ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * L_103 = __this->get_U3ClengthTableU3E5__6_8();
		int32_t L_104 = __this->get_U3CfirstArcLengthSampleU3E5__12_14();
		NullCheck(L_103);
		float L_105;
		L_105 = ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F(L_103, L_104, /*hidden argument*/ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F_RuntimeMethod_var);
		__this->set_U3CupToSpanLengthU3E5__14_16(L_105);
		// float spanLength = lengthTable[lastArcLengthSample] - upToSpanLength;
		ReadOnlyCollection_1_t74E1A81B990B8745BDF3F07868906F980E1C01DF * L_106 = __this->get_U3ClengthTableU3E5__6_8();
		int32_t L_107 = __this->get_U3ClastArcLengthSampleU3E5__13_15();
		NullCheck(L_106);
		float L_108;
		L_108 = ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F(L_106, L_107, /*hidden argument*/ReadOnlyCollection_1_get_Item_m1D4127E94B47BFF14791421852AD688B4614F14F_RuntimeMethod_var);
		float L_109 = __this->get_U3CupToSpanLengthU3E5__14_16();
		__this->set_U3CspanLengthU3E5__15_17(((float)il2cpp_codegen_subtract((float)L_108, (float)L_109)));
		// int particlesInSpan = 1 + Mathf.FloorToInt(spanLength / thickness * resolution);
		float L_110 = __this->get_U3CspanLengthU3E5__15_17();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_111 = __this->get_U3CU3E4__this_2();
		NullCheck(L_111);
		float L_112 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_111)->get_thickness_35();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_113 = __this->get_U3CU3E4__this_2();
		NullCheck(L_113);
		float L_114 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_113)->get_resolution_36();
		int32_t L_115;
		L_115 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)((float)((float)L_110/(float)L_112)), (float)L_114)), /*hidden argument*/NULL);
		__this->set_U3CparticlesInSpanU3E5__16_18(((int32_t)il2cpp_codegen_add((int32_t)1, (int32_t)L_115)));
		// float distance = spanLength / particlesInSpan;
		float L_116 = __this->get_U3CspanLengthU3E5__15_17();
		int32_t L_117 = __this->get_U3CparticlesInSpanU3E5__16_18();
		__this->set_U3CdistanceU3E5__17_19(((float)((float)L_116/(float)((float)((float)L_117)))));
		// for (int j = 0; j < particlesInSpan; ++j)
		__this->set_U3CjU3E5__18_20(0);
		goto IL_0533;
	}

IL_03dc:
	{
		// float mu = path.GetMuAtLenght(upToSpanLength + distance * (j + 1));
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_118 = __this->get_U3CU3E4__this_2();
		NullCheck(L_118);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_119 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_118)->get_path_34();
		float L_120 = __this->get_U3CupToSpanLengthU3E5__14_16();
		float L_121 = __this->get_U3CdistanceU3E5__17_19();
		int32_t L_122 = __this->get_U3CjU3E5__18_20();
		NullCheck(L_119);
		float L_123;
		L_123 = ObiPath_GetMuAtLenght_m9B36DC8C812923123B8D5F9D290BAD46CF555E6B(L_119, ((float)il2cpp_codegen_add((float)L_120, (float)((float)il2cpp_codegen_multiply((float)L_121, (float)((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_122, (int32_t)1)))))))), /*hidden argument*/NULL);
		__this->set_U3CmuU3E5__19_21(L_123);
		// particlePositions.Add(path.points.GetPositionAtMu(path.Closed, mu));
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_124 = __this->get_U3CparticlePositionsU3E5__1_3();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_125 = __this->get_U3CU3E4__this_2();
		NullCheck(L_125);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_126 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_125)->get_path_34();
		NullCheck(L_126);
		ObiPointsDataChannel_t9829A9AF7262C2BE30176BB03658B553BC840507 * L_127;
		L_127 = ObiPath_get_points_m6F044F0A220D99C2C5184CB5FE1769BAED6FFD3C(L_126, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_128 = __this->get_U3CU3E4__this_2();
		NullCheck(L_128);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_129 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_128)->get_path_34();
		NullCheck(L_129);
		bool L_130;
		L_130 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_129, /*hidden argument*/NULL);
		float L_131 = __this->get_U3CmuU3E5__19_21();
		NullCheck(L_127);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_132;
		L_132 = ObiPointsDataChannel_GetPositionAtMu_m5DA1AF5F4A0C5EB6EDAE4AA729DAC149E3E17DF5(L_127, L_130, L_131, /*hidden argument*/NULL);
		NullCheck(L_124);
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_124, L_132, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// particleThicknesses.Add(path.thicknesses.GetAtMu(path.Closed, mu));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_133 = __this->get_U3CparticleThicknessesU3E5__2_4();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_134 = __this->get_U3CU3E4__this_2();
		NullCheck(L_134);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_135 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_134)->get_path_34();
		NullCheck(L_135);
		ObiThicknessDataChannel_tF22C0D268717A52A98161EE6B0B9BB8A9EC74F6C * L_136;
		L_136 = ObiPath_get_thicknesses_m435418A103FD1CC0D151EF3B75689969F0E23C8D(L_135, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_137 = __this->get_U3CU3E4__this_2();
		NullCheck(L_137);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_138 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_137)->get_path_34();
		NullCheck(L_138);
		bool L_139;
		L_139 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_138, /*hidden argument*/NULL);
		float L_140 = __this->get_U3CmuU3E5__19_21();
		NullCheck(L_136);
		float L_141;
		L_141 = ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3(L_136, L_139, L_140, /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		NullCheck(L_133);
		List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A(L_133, L_141, /*hidden argument*/List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		// particleInvMasses.Add(ObiUtils.MassToInvMass(path.masses.GetAtMu(path.Closed, mu)));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_142 = __this->get_U3CparticleInvMassesU3E5__3_5();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_143 = __this->get_U3CU3E4__this_2();
		NullCheck(L_143);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_144 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_143)->get_path_34();
		NullCheck(L_144);
		ObiMassDataChannel_tB319AF62196CE425E0876DDE0BDEC6E24BF73996 * L_145;
		L_145 = ObiPath_get_masses_m563E313A7734C29F2476B68228BAB71731EBC326(L_144, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_146 = __this->get_U3CU3E4__this_2();
		NullCheck(L_146);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_147 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_146)->get_path_34();
		NullCheck(L_147);
		bool L_148;
		L_148 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_147, /*hidden argument*/NULL);
		float L_149 = __this->get_U3CmuU3E5__19_21();
		NullCheck(L_145);
		float L_150;
		L_150 = ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3(L_145, L_148, L_149, /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mA5E9E5420D27962125605C5B809F7EA005989AF3_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		float L_151;
		L_151 = ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68_inline(L_150, /*hidden argument*/NULL);
		NullCheck(L_142);
		List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A(L_142, L_151, /*hidden argument*/List_1_Add_m0968C6D3DE1DE9278833352DD095B9ABCE91CD0A_RuntimeMethod_var);
		// particleFilters.Add(path.filters.GetAtMu(path.Closed, mu));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_152 = __this->get_U3CparticleFiltersU3E5__4_6();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_153 = __this->get_U3CU3E4__this_2();
		NullCheck(L_153);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_154 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_153)->get_path_34();
		NullCheck(L_154);
		ObiPhaseDataChannel_tB5F72C23612EC59CE0F6C11CB6BE8FD85A8C5670 * L_155;
		L_155 = ObiPath_get_filters_m6D02BA0F0D6E24496882A16B37C12B185D9CC028(L_154, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_156 = __this->get_U3CU3E4__this_2();
		NullCheck(L_156);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_157 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_156)->get_path_34();
		NullCheck(L_157);
		bool L_158;
		L_158 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_157, /*hidden argument*/NULL);
		float L_159 = __this->get_U3CmuU3E5__19_21();
		NullCheck(L_155);
		int32_t L_160;
		L_160 = ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB(L_155, L_158, L_159, /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mD54E80C95DA82C0370D74AE29442BFCE020400FB_RuntimeMethod_var);
		NullCheck(L_152);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_152, L_160, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		// particleColors.Add(path.colors.GetAtMu(path.Closed, mu));
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_161 = __this->get_U3CparticleColorsU3E5__5_7();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_162 = __this->get_U3CU3E4__this_2();
		NullCheck(L_162);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_163 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_162)->get_path_34();
		NullCheck(L_163);
		ObiColorDataChannel_t7B648E488D6FEE47A425854C29CFA89DFB75ADE2 * L_164;
		L_164 = ObiPath_get_colors_mB1315A257BB672D26575C3CEFD3FC642D65C8C6F(L_163, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_165 = __this->get_U3CU3E4__this_2();
		NullCheck(L_165);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_166 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_165)->get_path_34();
		NullCheck(L_166);
		bool L_167;
		L_167 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_166, /*hidden argument*/NULL);
		float L_168 = __this->get_U3CmuU3E5__19_21();
		NullCheck(L_164);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_169;
		L_169 = ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A(L_164, L_167, L_168, /*hidden argument*/ObiPathDataChannelIdentity_1_GetAtMu_mC1BA7D522304E1F48AFF0F3332EADC6B5E68014A_RuntimeMethod_var);
		NullCheck(L_161);
		List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C(L_161, L_169, /*hidden argument*/List_1_Add_m77863248BCE82A014C9B7DFE859F39BF87ACE24C_RuntimeMethod_var);
		// for (int j = 0; j < particlesInSpan; ++j)
		int32_t L_170 = __this->get_U3CjU3E5__18_20();
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_170, (int32_t)1));
		int32_t L_171 = V_3;
		__this->set_U3CjU3E5__18_20(L_171);
	}

IL_0533:
	{
		// for (int j = 0; j < particlesInSpan; ++j)
		int32_t L_172 = __this->get_U3CjU3E5__18_20();
		int32_t L_173 = __this->get_U3CparticlesInSpanU3E5__16_18();
		V_4 = (bool)((((int32_t)L_172) < ((int32_t)L_173))? 1 : 0);
		bool L_174 = V_4;
		if (L_174)
		{
			goto IL_03dc;
		}
	}
	{
		// if (!(path.Closed && i == spans - 1))
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_175 = __this->get_U3CU3E4__this_2();
		NullCheck(L_175);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_176 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_175)->get_path_34();
		NullCheck(L_176);
		bool L_177;
		L_177 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_176, /*hidden argument*/NULL);
		if (!L_177)
		{
			goto IL_0571;
		}
	}
	{
		int32_t L_178 = __this->get_U3CiU3E5__11_13();
		int32_t L_179 = __this->get_U3CspansU3E5__7_9();
		G_B19_0 = ((((int32_t)((((int32_t)L_178) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_179, (int32_t)1))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0572;
	}

IL_0571:
	{
		G_B19_0 = 1;
	}

IL_0572:
	{
		V_5 = (bool)G_B19_0;
		bool L_180 = V_5;
		if (!L_180)
		{
			goto IL_05cd;
		}
	}
	{
		// groups[i + 1].particleIndices.Clear();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_181 = __this->get_U3CU3E4__this_2();
		NullCheck(L_181);
		List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * L_182 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_181)->get_groups_33();
		int32_t L_183 = __this->get_U3CiU3E5__11_13();
		NullCheck(L_182);
		ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE * L_184;
		L_184 = List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_inline(L_182, ((int32_t)il2cpp_codegen_add((int32_t)L_183, (int32_t)1)), /*hidden argument*/List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var);
		NullCheck(L_184);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_185 = L_184->get_particleIndices_4();
		NullCheck(L_185);
		List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A(L_185, /*hidden argument*/List_1_Clear_m508B72E5229FAE7042D99A04555F66F10C597C7A_RuntimeMethod_var);
		// groups[i + 1].particleIndices.Add(particlePositions.Count - 1);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_186 = __this->get_U3CU3E4__this_2();
		NullCheck(L_186);
		List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * L_187 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_186)->get_groups_33();
		int32_t L_188 = __this->get_U3CiU3E5__11_13();
		NullCheck(L_187);
		ObiParticleGroup_t6BDED5BDE5228AFEFCEA29721FC1E629DB81ABAE * L_189;
		L_189 = List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_inline(L_187, ((int32_t)il2cpp_codegen_add((int32_t)L_188, (int32_t)1)), /*hidden argument*/List_1_get_Item_mDF455C041FD38439DDED75269429D898CE5F692C_RuntimeMethod_var);
		NullCheck(L_189);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_190 = L_189->get_particleIndices_4();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_191 = __this->get_U3CparticlePositionsU3E5__1_3();
		NullCheck(L_191);
		int32_t L_192;
		L_192 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_191, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		NullCheck(L_190);
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_190, ((int32_t)il2cpp_codegen_subtract((int32_t)L_192, (int32_t)1)), /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
	}

IL_05cd:
	{
		// if (i % 100 == 0)
		int32_t L_193 = __this->get_U3CiU3E5__11_13();
		V_6 = (bool)((((int32_t)((int32_t)((int32_t)L_193%(int32_t)((int32_t)100)))) == ((int32_t)0))? 1 : 0);
		bool L_194 = V_6;
		if (!L_194)
		{
			goto IL_060e;
		}
	}
	{
		// yield return new CoroutineJob.ProgressInfo("ObiRope: generating particles...", i / (float)spans);
		int32_t L_195 = __this->get_U3CiU3E5__11_13();
		int32_t L_196 = __this->get_U3CspansU3E5__7_9();
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_197 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_197, _stringLiteral903817815321EE2F94F9CC0B49F39AA16CC286AB, ((float)((float)((float)((float)L_195))/(float)((float)((float)L_196)))), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_197);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0607:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_060e:
	{
		// for (int i = 0; i < spans; i++)
		int32_t L_198 = __this->get_U3CiU3E5__11_13();
		V_3 = L_198;
		int32_t L_199 = V_3;
		__this->set_U3CiU3E5__11_13(((int32_t)il2cpp_codegen_add((int32_t)L_199, (int32_t)1)));
	}

IL_061f:
	{
		// for (int i = 0; i < spans; i++)
		int32_t L_200 = __this->get_U3CiU3E5__11_13();
		int32_t L_201 = __this->get_U3CspansU3E5__7_9();
		V_7 = (bool)((((int32_t)L_200) < ((int32_t)L_201))? 1 : 0);
		bool L_202 = V_7;
		if (L_202)
		{
			goto IL_031b;
		}
	}
	{
		// m_ActiveParticleCount = particlePositions.Count;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_203 = __this->get_U3CU3E4__this_2();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_204 = __this->get_U3CparticlePositionsU3E5__1_3();
		NullCheck(L_204);
		int32_t L_205;
		L_205 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_204, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		NullCheck(L_203);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_203)->set_m_ActiveParticleCount_6(L_205);
		// totalParticles = m_ActiveParticleCount + pooledParticles;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_206 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_207 = __this->get_U3CU3E4__this_2();
		NullCheck(L_207);
		int32_t L_208 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_207)->get_m_ActiveParticleCount_6();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_209 = __this->get_U3CU3E4__this_2();
		NullCheck(L_209);
		int32_t L_210 = L_209->get_pooledParticles_41();
		NullCheck(L_206);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_206)->set_totalParticles_38(((int32_t)il2cpp_codegen_add((int32_t)L_208, (int32_t)L_210)));
		// int numSegments = m_ActiveParticleCount - (path.Closed ? 0 : 1);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_211 = __this->get_U3CU3E4__this_2();
		NullCheck(L_211);
		int32_t L_212 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_211)->get_m_ActiveParticleCount_6();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_213 = __this->get_U3CU3E4__this_2();
		NullCheck(L_213);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_214 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_213)->get_path_34();
		NullCheck(L_214);
		bool L_215;
		L_215 = ObiPath_get_Closed_mB5C448BE6271345235E3303EAA2CA32A77E92856(L_214, /*hidden argument*/NULL);
		G_B27_0 = L_212;
		G_B27_1 = __this;
		if (L_215)
		{
			G_B28_0 = L_212;
			G_B28_1 = __this;
			goto IL_068f;
		}
	}
	{
		G_B29_0 = 1;
		G_B29_1 = G_B27_0;
		G_B29_2 = G_B27_1;
		goto IL_0690;
	}

IL_068f:
	{
		G_B29_0 = 0;
		G_B29_1 = G_B28_0;
		G_B29_2 = G_B28_1;
	}

IL_0690:
	{
		NullCheck(G_B29_2);
		G_B29_2->set_U3CnumSegmentsU3E5__8_10(((int32_t)il2cpp_codegen_subtract((int32_t)G_B29_1, (int32_t)G_B29_0)));
		// if (numSegments > 0)
		int32_t L_216 = __this->get_U3CnumSegmentsU3E5__8_10();
		V_8 = (bool)((((int32_t)L_216) > ((int32_t)0))? 1 : 0);
		bool L_217 = V_8;
		if (!L_217)
		{
			goto IL_06ca;
		}
	}
	{
		// m_InterParticleDistance = path.Length / (float)numSegments;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_218 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_219 = __this->get_U3CU3E4__this_2();
		NullCheck(L_219);
		ObiPath_tDB2DC69FA3DD19BC128AB0C4D687C36AC13E7459 * L_220 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_219)->get_path_34();
		NullCheck(L_220);
		float L_221;
		L_221 = ObiPath_get_Length_m702A025AC3A2C08D2F9629728753916FA662A7E9(L_220, /*hidden argument*/NULL);
		int32_t L_222 = __this->get_U3CnumSegmentsU3E5__8_10();
		NullCheck(L_218);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_218)->set_m_InterParticleDistance_37(((float)((float)L_221/(float)((float)((float)L_222)))));
		goto IL_06da;
	}

IL_06ca:
	{
		// m_InterParticleDistance = 0;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_223 = __this->get_U3CU3E4__this_2();
		NullCheck(L_223);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_223)->set_m_InterParticleDistance_37((0.0f));
	}

IL_06da:
	{
		// positions = new Vector3[totalParticles];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_224 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_225 = __this->get_U3CU3E4__this_2();
		NullCheck(L_225);
		int32_t L_226 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_225)->get_totalParticles_38();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_227 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_226);
		NullCheck(L_224);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_224)->set_positions_9(L_227);
		// restPositions = new Vector4[totalParticles];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_228 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_229 = __this->get_U3CU3E4__this_2();
		NullCheck(L_229);
		int32_t L_230 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_229)->get_totalParticles_38();
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_231 = (Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871*)(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871*)SZArrayNew(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871_il2cpp_TypeInfo_var, (uint32_t)L_230);
		NullCheck(L_228);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_228)->set_restPositions_10(L_231);
		// velocities = new Vector3[totalParticles];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_232 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_233 = __this->get_U3CU3E4__this_2();
		NullCheck(L_233);
		int32_t L_234 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_233)->get_totalParticles_38();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_235 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_234);
		NullCheck(L_232);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_232)->set_velocities_13(L_235);
		// invMasses = new float[totalParticles];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_236 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_237 = __this->get_U3CU3E4__this_2();
		NullCheck(L_237);
		int32_t L_238 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_237)->get_totalParticles_38();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_239 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_238);
		NullCheck(L_236);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_236)->set_invMasses_15(L_239);
		// principalRadii = new Vector3[totalParticles];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_240 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_241 = __this->get_U3CU3E4__this_2();
		NullCheck(L_241);
		int32_t L_242 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_241)->get_totalParticles_38();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_243 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_242);
		NullCheck(L_240);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_240)->set_principalRadii_18(L_243);
		// filters = new int[totalParticles];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_244 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_245 = __this->get_U3CU3E4__this_2();
		NullCheck(L_245);
		int32_t L_246 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_245)->get_totalParticles_38();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_247 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)L_246);
		NullCheck(L_244);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_244)->set_filters_17(L_247);
		// colors = new Color[totalParticles];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_248 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_249 = __this->get_U3CU3E4__this_2();
		NullCheck(L_249);
		int32_t L_250 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_249)->get_totalParticles_38();
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_251 = (ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)SZArrayNew(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var, (uint32_t)L_250);
		NullCheck(L_248);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_248)->set_colors_19(L_251);
		// restLengths = new float[totalParticles];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_252 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_253 = __this->get_U3CU3E4__this_2();
		NullCheck(L_253);
		int32_t L_254 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_253)->get_totalParticles_38();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_255 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_254);
		NullCheck(L_252);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_252)->set_restLengths_40(L_255);
		// for (int i = 0; i < m_ActiveParticleCount; i++)
		__this->set_U3CiU3E5__20_22(0);
		goto IL_093e;
	}

IL_07be:
	{
		// invMasses[i] = particleInvMasses[i];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_256 = __this->get_U3CU3E4__this_2();
		NullCheck(L_256);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_257 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_256)->get_invMasses_15();
		int32_t L_258 = __this->get_U3CiU3E5__20_22();
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_259 = __this->get_U3CparticleInvMassesU3E5__3_5();
		int32_t L_260 = __this->get_U3CiU3E5__20_22();
		NullCheck(L_259);
		float L_261;
		L_261 = List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_inline(L_259, L_260, /*hidden argument*/List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_RuntimeMethod_var);
		NullCheck(L_257);
		(L_257)->SetAt(static_cast<il2cpp_array_size_t>(L_258), (float)L_261);
		// positions[i] = particlePositions[i];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_262 = __this->get_U3CU3E4__this_2();
		NullCheck(L_262);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_263 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_262)->get_positions_9();
		int32_t L_264 = __this->get_U3CiU3E5__20_22();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_265 = __this->get_U3CparticlePositionsU3E5__1_3();
		int32_t L_266 = __this->get_U3CiU3E5__20_22();
		NullCheck(L_265);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_267;
		L_267 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_265, L_266, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		NullCheck(L_263);
		(L_263)->SetAt(static_cast<il2cpp_array_size_t>(L_264), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_267);
		// restPositions[i] = positions[i];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_268 = __this->get_U3CU3E4__this_2();
		NullCheck(L_268);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_269 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_268)->get_restPositions_10();
		int32_t L_270 = __this->get_U3CiU3E5__20_22();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_271 = __this->get_U3CU3E4__this_2();
		NullCheck(L_271);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_272 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_271)->get_positions_9();
		int32_t L_273 = __this->get_U3CiU3E5__20_22();
		NullCheck(L_272);
		int32_t L_274 = L_273;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_275 = (L_272)->GetAt(static_cast<il2cpp_array_size_t>(L_274));
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_276;
		L_276 = Vector4_op_Implicit_mDCFA56E9D34979E1E2BFE6C2D61F1768D934A8EB(L_275, /*hidden argument*/NULL);
		NullCheck(L_269);
		(L_269)->SetAt(static_cast<il2cpp_array_size_t>(L_270), (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )L_276);
		// restPositions[i][3] = 1; // activate rest position.
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_277 = __this->get_U3CU3E4__this_2();
		NullCheck(L_277);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_278 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_277)->get_restPositions_10();
		int32_t L_279 = __this->get_U3CiU3E5__20_22();
		NullCheck(L_278);
		Vector4_set_Item_m7552B288FF218CA023F0DFB971BBA30D0362006A((Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 *)((L_278)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_279))), 3, (1.0f), /*hidden argument*/NULL);
		// principalRadii[i] = Vector3.one * particleThicknesses[i] * thickness;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_280 = __this->get_U3CU3E4__this_2();
		NullCheck(L_280);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_281 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_280)->get_principalRadii_18();
		int32_t L_282 = __this->get_U3CiU3E5__20_22();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_283;
		L_283 = Vector3_get_one_m9CDE5C456038B133ED94402673859EC37B1C1CCB(/*hidden argument*/NULL);
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_284 = __this->get_U3CparticleThicknessesU3E5__2_4();
		int32_t L_285 = __this->get_U3CiU3E5__20_22();
		NullCheck(L_284);
		float L_286;
		L_286 = List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_inline(L_284, L_285, /*hidden argument*/List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_287;
		L_287 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_283, L_286, /*hidden argument*/NULL);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_288 = __this->get_U3CU3E4__this_2();
		NullCheck(L_288);
		float L_289 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_288)->get_thickness_35();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_290;
		L_290 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_287, L_289, /*hidden argument*/NULL);
		NullCheck(L_281);
		(L_281)->SetAt(static_cast<il2cpp_array_size_t>(L_282), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_290);
		// filters[i] = particleFilters[i];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_291 = __this->get_U3CU3E4__this_2();
		NullCheck(L_291);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_292 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_291)->get_filters_17();
		int32_t L_293 = __this->get_U3CiU3E5__20_22();
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_294 = __this->get_U3CparticleFiltersU3E5__4_6();
		int32_t L_295 = __this->get_U3CiU3E5__20_22();
		NullCheck(L_294);
		int32_t L_296;
		L_296 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_294, L_295, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		NullCheck(L_292);
		(L_292)->SetAt(static_cast<il2cpp_array_size_t>(L_293), (int32_t)L_296);
		// colors[i] = particleColors[i];
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_297 = __this->get_U3CU3E4__this_2();
		NullCheck(L_297);
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_298 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_297)->get_colors_19();
		int32_t L_299 = __this->get_U3CiU3E5__20_22();
		List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * L_300 = __this->get_U3CparticleColorsU3E5__5_7();
		int32_t L_301 = __this->get_U3CiU3E5__20_22();
		NullCheck(L_300);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_302;
		L_302 = List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_inline(L_300, L_301, /*hidden argument*/List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_RuntimeMethod_var);
		NullCheck(L_298);
		(L_298)->SetAt(static_cast<il2cpp_array_size_t>(L_299), (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 )L_302);
		// if (i % 100 == 0)
		int32_t L_303 = __this->get_U3CiU3E5__20_22();
		V_9 = (bool)((((int32_t)((int32_t)((int32_t)L_303%(int32_t)((int32_t)100)))) == ((int32_t)0))? 1 : 0);
		bool L_304 = V_9;
		if (!L_304)
		{
			goto IL_092d;
		}
	}
	{
		// yield return new CoroutineJob.ProgressInfo("ObiRope: generating particles...", i / (float)m_ActiveParticleCount);
		int32_t L_305 = __this->get_U3CiU3E5__20_22();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_306 = __this->get_U3CU3E4__this_2();
		NullCheck(L_306);
		int32_t L_307 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_306)->get_m_ActiveParticleCount_6();
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_308 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_308, _stringLiteral903817815321EE2F94F9CC0B49F39AA16CC286AB, ((float)((float)((float)((float)L_305))/(float)((float)((float)L_307)))), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_308);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0926:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_092d:
	{
		// for (int i = 0; i < m_ActiveParticleCount; i++)
		int32_t L_309 = __this->get_U3CiU3E5__20_22();
		V_3 = L_309;
		int32_t L_310 = V_3;
		__this->set_U3CiU3E5__20_22(((int32_t)il2cpp_codegen_add((int32_t)L_310, (int32_t)1)));
	}

IL_093e:
	{
		// for (int i = 0; i < m_ActiveParticleCount; i++)
		int32_t L_311 = __this->get_U3CiU3E5__20_22();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_312 = __this->get_U3CU3E4__this_2();
		NullCheck(L_312);
		int32_t L_313 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_312)->get_m_ActiveParticleCount_6();
		V_10 = (bool)((((int32_t)L_311) < ((int32_t)L_313))? 1 : 0);
		bool L_314 = V_10;
		if (L_314)
		{
			goto IL_07be;
		}
	}
	{
		// CreateSimplices(numSegments);
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_315 = __this->get_U3CU3E4__this_2();
		int32_t L_316 = __this->get_U3CnumSegmentsU3E5__8_10();
		NullCheck(L_315);
		ObiRopeBlueprintBase_CreateSimplices_mB064904BA09A240A79F509214EDD53AF206F9FDD(L_315, L_316, /*hidden argument*/NULL);
		// IEnumerator dc = CreateDistanceConstraints();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_317 = __this->get_U3CU3E4__this_2();
		NullCheck(L_317);
		RuntimeObject* L_318;
		L_318 = VirtFuncInvoker0< RuntimeObject* >::Invoke(18 /* System.Collections.IEnumerator Obi.ObiRopeBlueprint::CreateDistanceConstraints() */, L_317);
		__this->set_U3CdcU3E5__9_11(L_318);
		goto IL_09a0;
	}

IL_097f:
	{
		// yield return dc.Current;
		RuntimeObject* L_319 = __this->get_U3CdcU3E5__9_11();
		NullCheck(L_319);
		RuntimeObject * L_320;
		L_320 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_319);
		__this->set_U3CU3E2__current_1(L_320);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_0999:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_09a0:
	{
		// while (dc.MoveNext())
		RuntimeObject* L_321 = __this->get_U3CdcU3E5__9_11();
		NullCheck(L_321);
		bool L_322;
		L_322 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_321);
		V_11 = L_322;
		bool L_323 = V_11;
		if (L_323)
		{
			goto IL_097f;
		}
	}
	{
		// IEnumerator bc = CreateBendingConstraints();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_324 = __this->get_U3CU3E4__this_2();
		NullCheck(L_324);
		RuntimeObject* L_325;
		L_325 = VirtFuncInvoker0< RuntimeObject* >::Invoke(19 /* System.Collections.IEnumerator Obi.ObiRopeBlueprint::CreateBendingConstraints() */, L_324);
		__this->set_U3CbcU3E5__10_12(L_325);
		goto IL_09e5;
	}

IL_09c4:
	{
		// yield return bc.Current;
		RuntimeObject* L_326 = __this->get_U3CbcU3E5__10_12();
		NullCheck(L_326);
		RuntimeObject * L_327;
		L_327 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_326);
		__this->set_U3CU3E2__current_1(L_327);
		__this->set_U3CU3E1__state_0(4);
		return (bool)1;
	}

IL_09de:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_09e5:
	{
		// while (bc.MoveNext())
		RuntimeObject* L_328 = __this->get_U3CbcU3E5__10_12();
		NullCheck(L_328);
		bool L_329;
		L_329 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_328);
		V_12 = L_329;
		bool L_330 = V_12;
		if (L_330)
		{
			goto IL_09c4;
		}
	}
	{
		// m_RestLength = 0;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_331 = __this->get_U3CU3E4__this_2();
		NullCheck(L_331);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_331)->set_m_RestLength_39((0.0f));
		// foreach (float length in restLengths)
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_332 = __this->get_U3CU3E4__this_2();
		NullCheck(L_332);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_333 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_332)->get_restLengths_40();
		__this->set_U3CU3Es__21_23(L_333);
		__this->set_U3CU3Es__22_24(0);
		goto IL_0a5f;
	}

IL_0a21:
	{
		// foreach (float length in restLengths)
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_334 = __this->get_U3CU3Es__21_23();
		int32_t L_335 = __this->get_U3CU3Es__22_24();
		NullCheck(L_334);
		int32_t L_336 = L_335;
		float L_337 = (L_334)->GetAt(static_cast<il2cpp_array_size_t>(L_336));
		__this->set_U3ClengthU3E5__23_25(L_337);
		// m_RestLength += length;
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_338 = __this->get_U3CU3E4__this_2();
		ObiRopeBlueprint_t98F61AAFCFF4861F44E842BDA5142DAA87FE333B * L_339 = __this->get_U3CU3E4__this_2();
		NullCheck(L_339);
		float L_340 = ((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_339)->get_m_RestLength_39();
		float L_341 = __this->get_U3ClengthU3E5__23_25();
		NullCheck(L_338);
		((ObiRopeBlueprintBase_t43FBE4F0190EDE09ABEEDF24962681F5A515C687 *)L_338)->set_m_RestLength_39(((float)il2cpp_codegen_add((float)L_340, (float)L_341)));
		int32_t L_342 = __this->get_U3CU3Es__22_24();
		__this->set_U3CU3Es__22_24(((int32_t)il2cpp_codegen_add((int32_t)L_342, (int32_t)1)));
	}

IL_0a5f:
	{
		// foreach (float length in restLengths)
		int32_t L_343 = __this->get_U3CU3Es__22_24();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_344 = __this->get_U3CU3Es__21_23();
		NullCheck(L_344);
		if ((((int32_t)L_343) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_344)->max_length))))))
		{
			goto IL_0a21;
		}
	}
	{
		__this->set_U3CU3Es__21_23((SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)NULL);
		// }
		return (bool)0;
	}
}
// System.Object Obi.ObiRopeBlueprint/<Initialize>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAF2CC89A62774D134AF088ADE0119C979EB1F72 (U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiRopeBlueprint/<Initialize>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m7A4B24373876BDE2453048631F2B963E386E7D12 (U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m7A4B24373876BDE2453048631F2B963E386E7D12_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiRopeBlueprint/<Initialize>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m16BDC9B0E8C7DB5818965339EC2F1EFEFEF37DD9 (U3CInitializeU3Ed__2_tE2B906185A572484A3367ED83CFBA516A9646BA7 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiRopeBlueprintBase/<Initialize>d__17::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__17__ctor_m52F67707D008A9F9B010B0538ECEE7A5CC69F2EB (U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.ObiRopeBlueprintBase/<Initialize>d__17::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__17_System_IDisposable_Dispose_mF0B5E8B1440934FC4C19F4F949EACFE581448839 (U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiRopeBlueprintBase/<Initialize>d__17::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CInitializeU3Ed__17_MoveNext_m22EAFC8213CAC2C43D4012E007F6CE8431DD155D (U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_0030;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		// protected override IEnumerator Initialize() { yield return null; }
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0030:
	{
		__this->set_U3CU3E1__state_0((-1));
		// protected override IEnumerator Initialize() { yield return null; }
		return (bool)0;
	}
}
// System.Object Obi.ObiRopeBlueprintBase/<Initialize>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitializeU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9DD4C0990CD4B35386F56E79FF03AA864B1D6C21 (U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiRopeBlueprintBase/<Initialize>d__17::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__17_System_Collections_IEnumerator_Reset_m28AA9E89D0D2B4BD55BE44CC152105CAB3FE46A7 (U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CInitializeU3Ed__17_System_Collections_IEnumerator_Reset_m28AA9E89D0D2B4BD55BE44CC152105CAB3FE46A7_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiRopeBlueprintBase/<Initialize>d__17::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitializeU3Ed__17_System_Collections_IEnumerator_get_Current_mC609C587F1D824ADC1D1CD66C9D51AB95F222BAA (U3CInitializeU3Ed__17_t74A247E83DE8443FEAAA0ADB4125848C3E4745C8 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiSolver/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m89EAB0A60F1D2D1667AB8DC5FFD689526BA4D3DE (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16 * L_0 = (U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16 *)il2cpp_codegen_object_new(U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m2D8F2915236AB77A7C36A87C7761B5503DCA10F2(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Obi.ObiSolver/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m2D8F2915236AB77A7C36A87C7761B5503DCA10F2 (U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Obi.ObiSolver/<>c::<get_allocParticleCount>b__148_0(Obi.ObiSolver/ParticleInActor)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3Cget_allocParticleCountU3Eb__148_0_mDB5D6EA59542D260F8AA00864066AC567AF3613B (U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16 * __this, ParticleInActor_t4D445CDF7A9DB2D80BEF82DBEEF606E8B61291BA * ___s0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		// get { return particleToActor.Count(s => s != null && s.actor != null); }
		ParticleInActor_t4D445CDF7A9DB2D80BEF82DBEEF606E8B61291BA * L_0 = ___s0;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		ParticleInActor_t4D445CDF7A9DB2D80BEF82DBEEF606E8B61291BA * L_1 = ___s0;
		NullCheck(L_1);
		ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6 * L_2 = L_1->get_actor_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 0;
	}

IL_0012:
	{
		return (bool)G_B3_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiSolver/CollisionCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionCallback__ctor_mA5A4EE9E910214B425B42DD40A2671EB949D661E (CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Obi.ObiSolver/CollisionCallback::Invoke(Obi.ObiSolver,Obi.ObiSolver/ObiCollisionEventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionCallback_Invoke_m941AD708CC00FD8EA47569A5B061BB862B96F98C (CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB * __this, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___solver0, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * ___contacts1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___solver0, ___contacts1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___solver0, ___contacts1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * >::Invoke(targetMethod, ___solver0, ___contacts1);
					else
						GenericVirtActionInvoker1< ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * >::Invoke(targetMethod, ___solver0, ___contacts1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___solver0, ___contacts1);
					else
						VirtActionInvoker1< ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___solver0, ___contacts1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___solver0, ___contacts1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * >::Invoke(targetMethod, targetThis, ___solver0, ___contacts1);
					else
						GenericVirtActionInvoker2< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * >::Invoke(targetMethod, targetThis, ___solver0, ___contacts1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___solver0, ___contacts1);
					else
						VirtActionInvoker2< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___solver0, ___contacts1);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___solver0, ___contacts1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___solver0, ___contacts1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Obi.ObiSolver/CollisionCallback::BeginInvoke(Obi.ObiSolver,Obi.ObiSolver/ObiCollisionEventArgs,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CollisionCallback_BeginInvoke_m0A88695225D3C02615D754DC211FD3EE3468BFAB (CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB * __this, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___solver0, ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * ___contacts1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___solver0;
	__d_args[1] = ___contacts1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);;
}
// System.Void Obi.ObiSolver/CollisionCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CollisionCallback_EndInvoke_m46F561EE559B214E9D38C29ABCADAB3336B18A8A (CollisionCallback_t03DCBA24C60E9BFDAB207590B65D472FF18C89DB * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiSolver/ObiCollisionEventArgs::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiCollisionEventArgs__ctor_m346881EB85AB69845A91659208E75756E4EF6D17 (ObiCollisionEventArgs_tF1EE3DB033C7BDE026FD0FD240C089A5B427D0D5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiList_1__ctor_m61E6FC08889B646E0B516B5BBD472DBB1D334AFB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public ObiList<Oni.Contact> contacts = new ObiList<Oni.Contact>();  /**< collision contacts.*/
		ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3 * L_0 = (ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3 *)il2cpp_codegen_object_new(ObiList_1_t8D98047B2EE611918DD4336DE69AEBB9622FD0E3_il2cpp_TypeInfo_var);
		ObiList_1__ctor_m61E6FC08889B646E0B516B5BBD472DBB1D334AFB(L_0, /*hidden argument*/ObiList_1__ctor_m61E6FC08889B646E0B516B5BBD472DBB1D334AFB_RuntimeMethod_var);
		__this->set_contacts_1(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_il2cpp_TypeInfo_var);
		EventArgs__ctor_m5ECB9A8ED0A9E2DBB1ED999BAC1CB44F4354E571(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiSolver/ParticleInActor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleInActor__ctor_mAA089A9B7EBB63E8671D8AAB97FD0836220D7EEA (ParticleInActor_t4D445CDF7A9DB2D80BEF82DBEEF606E8B61291BA * __this, const RuntimeMethod* method)
{
	{
		// public ParticleInActor()
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.actor = null;
		__this->set_actor_0((ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6 *)NULL);
		// this.indexInActor = -1;
		__this->set_indexInActor_1((-1));
		// }
		return;
	}
}
// System.Void Obi.ObiSolver/ParticleInActor::.ctor(Obi.ObiActor,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleInActor__ctor_m3829A4666F38432208F2811BDE98CD7A8C6B25F3 (ParticleInActor_t4D445CDF7A9DB2D80BEF82DBEEF606E8B61291BA * __this, ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6 * ___actor0, int32_t ___indexInActor1, const RuntimeMethod* method)
{
	{
		// public ParticleInActor(ObiActor actor, int indexInActor)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.actor = actor;
		ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6 * L_0 = ___actor0;
		__this->set_actor_0(L_0);
		// this.indexInActor = indexInActor;
		int32_t L_1 = ___indexInActor1;
		__this->set_indexInActor_1(L_1);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiSolver/SolverCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SolverCallback__ctor_mE4083F1B124E6A1FFB21E69EE2AEE7318B28AAD7 (SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Obi.ObiSolver/SolverCallback::Invoke(Obi.ObiSolver)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SolverCallback_Invoke_m45F6B3AC4BD61E22AAA076A7DBA5544E5C72F057 (SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * __this, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___solver0, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___solver0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___solver0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___solver0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___solver0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___solver0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___solver0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___solver0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * >::Invoke(targetMethod, targetThis, ___solver0);
					else
						GenericVirtActionInvoker1< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * >::Invoke(targetMethod, targetThis, ___solver0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___solver0);
					else
						VirtActionInvoker1< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___solver0);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___solver0, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___solver0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Obi.ObiSolver/SolverCallback::BeginInvoke(Obi.ObiSolver,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* SolverCallback_BeginInvoke_mD89642A3E1F37C4DC118EA6CACD1B6D68CD83A08 (SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * __this, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___solver0, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___solver0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);;
}
// System.Void Obi.ObiSolver/SolverCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SolverCallback_EndInvoke_m395BC54EFE0998B85005E9E4F66858F146C931F2 (SolverCallback_t32109BE30AA1EFC3607C1BF16C2368D6234CEF4A * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiSolver/SolverStepCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SolverStepCallback__ctor_m2079BE6990F358A8A5837AE21480E58DD4D0EEA3 (SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Obi.ObiSolver/SolverStepCallback::Invoke(Obi.ObiSolver,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SolverStepCallback_Invoke_m68DD32CB29FBD1EC423CC5A72702D2CB05715754 (SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * __this, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___solver0, float ___stepTime1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, float, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___solver0, ___stepTime1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, float, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___solver0, ___stepTime1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< float >::Invoke(targetMethod, ___solver0, ___stepTime1);
					else
						GenericVirtActionInvoker1< float >::Invoke(targetMethod, ___solver0, ___stepTime1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< float >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___solver0, ___stepTime1);
					else
						VirtActionInvoker1< float >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___solver0, ___stepTime1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, float, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___solver0, ___stepTime1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, float >::Invoke(targetMethod, targetThis, ___solver0, ___stepTime1);
					else
						GenericVirtActionInvoker2< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, float >::Invoke(targetMethod, targetThis, ___solver0, ___stepTime1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, float >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___solver0, ___stepTime1);
					else
						VirtActionInvoker2< ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, float >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___solver0, ___stepTime1);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, float, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___solver0, ___stepTime1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 *, float, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___solver0, ___stepTime1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Obi.ObiSolver/SolverStepCallback::BeginInvoke(Obi.ObiSolver,System.Single,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* SolverStepCallback_BeginInvoke_m434058E191E84C9A47950451E2B46A1F8F957F40 (SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * __this, ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___solver0, float ___stepTime1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___solver0;
	__d_args[1] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___stepTime1);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);;
}
// System.Void Obi.ObiSolver/SolverStepCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SolverStepCallback_EndInvoke_mF3E0E0DE67ABA86472824000ACA507AB3352A533 (SolverStepCallback_tAA0716CB90BD68AF40CD869A51504C52651B23C0 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiStitcher/Stitch::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stitch__ctor_mA9EBE5BCBF80CAFB90725EF8F4B9E8EA693A9800 (Stitch_tFC29C4BD6C0E9EC2F51B4658C215F27B4E0D7201 * __this, int32_t ___particleIndex10, int32_t ___particleIndex21, const RuntimeMethod* method)
{
	{
		// public Stitch(int particleIndex1, int particleIndex2)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.particleIndex1 = particleIndex1;
		int32_t L_0 = ___particleIndex10;
		__this->set_particleIndex1_0(L_0);
		// this.particleIndex2 = particleIndex2;
		int32_t L_1 = ___particleIndex21;
		__this->set_particleIndex2_1(L_1);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiTriangleMeshContainer/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m78E18BF63E0FC93B04931361A400A550DF8E42FE (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * L_0 = (U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 *)il2cpp_codegen_object_new(U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Obi.ObiTriangleMeshContainer/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8 (U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// Obi.Triangle Obi.ObiTriangleMeshContainer/<>c::<GetOrCreateTriangleMesh>b__6_0(Obi.IBounded)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8  U3CU3Ec_U3CGetOrCreateTriangleMeshU3Eb__6_0_m55F60D5CC64D104ED8E0B37F968D4479C3124E42 (U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * __this, RuntimeObject* ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Triangle[] tris = Array.ConvertAll(t, x => (Triangle)x);
		RuntimeObject* L_0 = ___x0;
		return ((*(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 *)((Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 *)UnBox(L_0, Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_il2cpp_TypeInfo_var))));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBilateralInterleavedU3Ed__40_System_IDisposable_Dispose_m8831F3EF2CA071AF13BD9D630BBA7F87306BD89C (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiUtils/<BilateralInterleaved>d__40::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CBilateralInterleavedU3Ed__40_MoveNext_m77C2C019B232853A6CC9102657015E05AC815D61 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_001d;
			}
			case 2:
			{
				goto IL_001f;
			}
		}
	}
	{
		goto IL_0021;
	}

IL_001b:
	{
		goto IL_0023;
	}

IL_001d:
	{
		goto IL_006e;
	}

IL_001f:
	{
		goto IL_0091;
	}

IL_0021:
	{
		return (bool)0;
	}

IL_0023:
	{
		__this->set_U3CU3E1__state_0((-1));
		// for (int i = 0; i < count; ++i)
		__this->set_U3CiU3E5__1_5(0);
		goto IL_00a9;
	}

IL_0034:
	{
		// if (i % 2 != 0)
		int32_t L_2 = __this->get_U3CiU3E5__1_5();
		V_1 = (bool)((!(((uint32_t)((int32_t)((int32_t)L_2%(int32_t)2))) <= ((uint32_t)0)))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_0077;
		}
	}
	{
		// yield return count - (count % 2) - i;
		int32_t L_4 = __this->get_count_3();
		int32_t L_5 = __this->get_count_3();
		int32_t L_6 = __this->get_U3CiU3E5__1_5();
		int32_t L_7 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)((int32_t)((int32_t)L_5%(int32_t)2)))), (int32_t)L_6));
		RuntimeObject * L_8 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_7);
		__this->set_U3CU3E2__current_1(L_8);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_006e:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_0098;
	}

IL_0077:
	{
		// else yield return i;
		int32_t L_9 = __this->get_U3CiU3E5__1_5();
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_10);
		__this->set_U3CU3E2__current_1(L_11);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0091:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0098:
	{
		// for (int i = 0; i < count; ++i)
		int32_t L_12 = __this->get_U3CiU3E5__1_5();
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
		int32_t L_13 = V_2;
		__this->set_U3CiU3E5__1_5(L_13);
	}

IL_00a9:
	{
		// for (int i = 0; i < count; ++i)
		int32_t L_14 = __this->get_U3CiU3E5__1_5();
		int32_t L_15 = __this->get_count_3();
		V_3 = (bool)((((int32_t)L_14) < ((int32_t)L_15))? 1 : 0);
		bool L_16 = V_3;
		if (L_16)
		{
			goto IL_0034;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9E1918A629F5BD2E500DBD47B57BEA70DA7126 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_get_Current_m82E938AAC7D70CD2629E214DDAC1F71A6BB51474 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * L_3 = (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB *)il2cpp_codegen_object_new(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_il2cpp_TypeInfo_var);
		U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * L_4 = V_0;
		int32_t L_5 = __this->get_U3CU3E3__count_4();
		NullCheck(L_4);
		L_4->set_count_3(L_5);
		U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mBF01BD41B11190F2A9C48BE6A1A9FB04EF151CF6 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Oni/ConstraintParameters::.ctor(System.Boolean,Oni/ConstraintParameters/EvaluationOrder,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566 (ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * __this, bool ___enabled0, int32_t ___order1, int32_t ___iterations2, const RuntimeMethod* method)
{
	{
		// this.enabled = enabled;
		bool L_0 = ___enabled0;
		__this->set_enabled_3(L_0);
		// this.iterations = iterations;
		int32_t L_1 = ___iterations2;
		__this->set_iterations_1(L_1);
		// this.evaluationOrder = order;
		int32_t L_2 = ___order1;
		__this->set_evaluationOrder_0(L_2);
		// this.SORFactor = 1;
		__this->set_SORFactor_2((1.0f));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566_AdjustorThunk (RuntimeObject * __this, bool ___enabled0, int32_t ___order1, int32_t ___iterations2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * _thisAdjusted = reinterpret_cast<ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 *>(__this + _offset);
	ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566(_thisAdjusted, ___enabled0, ___order1, ___iterations2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Oni/ProfileInfo
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_pinvoke(const ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A& unmarshaled, ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_pinvoke& marshaled)
{
	marshaled.___start_0 = unmarshaled.get_start_0();
	marshaled.___end_1 = unmarshaled.get_end_1();
	marshaled.___info_2 = unmarshaled.get_info_2();
	marshaled.___pad_3 = unmarshaled.get_pad_3();
	il2cpp_codegen_marshal_string_fixed(unmarshaled.get_name_4(), (char*)&marshaled.___name_4, 64);
}
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_pinvoke_back(const ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_pinvoke& marshaled, ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A& unmarshaled)
{
	double unmarshaled_start_temp_0 = 0.0;
	unmarshaled_start_temp_0 = marshaled.___start_0;
	unmarshaled.set_start_0(unmarshaled_start_temp_0);
	double unmarshaled_end_temp_1 = 0.0;
	unmarshaled_end_temp_1 = marshaled.___end_1;
	unmarshaled.set_end_1(unmarshaled_end_temp_1);
	uint32_t unmarshaled_info_temp_2 = 0;
	unmarshaled_info_temp_2 = marshaled.___info_2;
	unmarshaled.set_info_2(unmarshaled_info_temp_2);
	int32_t unmarshaled_pad_temp_3 = 0;
	unmarshaled_pad_temp_3 = marshaled.___pad_3;
	unmarshaled.set_pad_3(unmarshaled_pad_temp_3);
	unmarshaled.set_name_4(il2cpp_codegen_marshal_string_result(marshaled.___name_4));
}
// Conversion method for clean up from marshalling of: Oni/ProfileInfo
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_pinvoke_cleanup(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Oni/ProfileInfo
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_com(const ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A& unmarshaled, ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_com& marshaled)
{
	marshaled.___start_0 = unmarshaled.get_start_0();
	marshaled.___end_1 = unmarshaled.get_end_1();
	marshaled.___info_2 = unmarshaled.get_info_2();
	marshaled.___pad_3 = unmarshaled.get_pad_3();
	il2cpp_codegen_marshal_string_fixed(unmarshaled.get_name_4(), (char*)&marshaled.___name_4, 64);
}
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_com_back(const ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_com& marshaled, ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A& unmarshaled)
{
	double unmarshaled_start_temp_0 = 0.0;
	unmarshaled_start_temp_0 = marshaled.___start_0;
	unmarshaled.set_start_0(unmarshaled_start_temp_0);
	double unmarshaled_end_temp_1 = 0.0;
	unmarshaled_end_temp_1 = marshaled.___end_1;
	unmarshaled.set_end_1(unmarshaled_end_temp_1);
	uint32_t unmarshaled_info_temp_2 = 0;
	unmarshaled_info_temp_2 = marshaled.___info_2;
	unmarshaled.set_info_2(unmarshaled_info_temp_2);
	int32_t unmarshaled_pad_temp_3 = 0;
	unmarshaled_pad_temp_3 = marshaled.___pad_3;
	unmarshaled.set_pad_3(unmarshaled_pad_temp_3);
	unmarshaled.set_name_4(il2cpp_codegen_marshal_string_result(marshaled.___name_4));
}
// Conversion method for clean up from marshalling of: Oni/ProfileInfo
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_com_cleanup(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Oni/SolverParameters::.ctor(Oni/SolverParameters/Interpolation,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3 (SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 * __this, int32_t ___interpolation0, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___gravity1, const RuntimeMethod* method)
{
	{
		// this.mode = Mode.Mode3D;
		__this->set_mode_0(0);
		// this.gravity = gravity;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_0 = ___gravity1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B(L_0, /*hidden argument*/NULL);
		__this->set_gravity_2(L_1);
		// this.interpolation = interpolation;
		int32_t L_2 = ___interpolation0;
		__this->set_interpolation_1(L_2);
		// damping = 0;
		__this->set_damping_3((0.0f));
		// shockPropagation = 0;
		__this->set_shockPropagation_9((0.0f));
		// surfaceCollisionIterations = 8;
		__this->set_surfaceCollisionIterations_10(8);
		// surfaceCollisionTolerance = 0.005f;
		__this->set_surfaceCollisionTolerance_11((0.00499999989f));
		// maxAnisotropy = 3;
		__this->set_maxAnisotropy_4((3.0f));
		// maxDepenetration = 10;
		__this->set_maxDepenetration_7((10.0f));
		// sleepThreshold = 0.0005f;
		__this->set_sleepThreshold_5((0.000500000024f));
		// collisionMargin = 0.02f;
		__this->set_collisionMargin_6((0.0199999996f));
		// continuousCollisionDetection = 1;
		__this->set_continuousCollisionDetection_8((1.0f));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3_AdjustorThunk (RuntimeObject * __this, int32_t ___interpolation0, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___gravity1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 * _thisAdjusted = reinterpret_cast<SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 *>(__this + _offset);
	SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3(_thisAdjusted, ___interpolation0, ___gravity1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CJumpFloodU3Ed__5__ctor_m9A42F34DE8C7C452C8A53F17D6D572BC2453C654 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CJumpFloodU3Ed__5_System_IDisposable_Dispose_m0A919D1AE0822BB1475E5FE78DA4060A236911E6 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.VoxelDistanceField/<JumpFlood>d__5::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CJumpFloodU3Ed__5_MoveNext_mCF3E269446E8A96E7F313896B714A111CBBD3FA3 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8BF94FD2D8D349E76B64F4A8FEA9E6A331A7DB94);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_001d;
			}
			case 2:
			{
				goto IL_0022;
			}
		}
	}
	{
		goto IL_0027;
	}

IL_001b:
	{
		goto IL_0029;
	}

IL_001d:
	{
		goto IL_0291;
	}

IL_0022:
	{
		goto IL_034f;
	}

IL_0027:
	{
		return (bool)0;
	}

IL_0029:
	{
		__this->set_U3CU3E1__state_0((-1));
		// distanceField = new Vector3Int[voxelizer.resolution.x,
		//                                voxelizer.resolution.y,
		//                                voxelizer.resolution.z];
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_2 = __this->get_U3CU3E4__this_2();
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_4 = L_3->get_voxelizer_1();
		NullCheck(L_4);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_5 = L_4->get_address_of_resolution_8();
		int32_t L_6;
		L_6 = Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_5, /*hidden argument*/NULL);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_8 = L_7->get_voxelizer_1();
		NullCheck(L_8);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_9 = L_8->get_address_of_resolution_8();
		int32_t L_10;
		L_10 = Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_9, /*hidden argument*/NULL);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_11 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_12 = L_11->get_voxelizer_1();
		NullCheck(L_12);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_13 = L_12->get_address_of_resolution_8();
		int32_t L_14;
		L_14 = Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_13, /*hidden argument*/NULL);
		il2cpp_array_size_t L_16[] = { (il2cpp_array_size_t)L_6, (il2cpp_array_size_t)L_10, (il2cpp_array_size_t)L_14 };
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_15 = (Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9*)GenArrayNew(Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9_il2cpp_TypeInfo_var, L_16);
		NullCheck(L_2);
		L_2->set_distanceField_0(L_15);
		// Vector3Int[,,] auxBuffer = new Vector3Int[voxelizer.resolution.x,
		//                                           voxelizer.resolution.y,
		//                                           voxelizer.resolution.z];
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_17 = __this->get_U3CU3E4__this_2();
		NullCheck(L_17);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_18 = L_17->get_voxelizer_1();
		NullCheck(L_18);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_19 = L_18->get_address_of_resolution_8();
		int32_t L_20;
		L_20 = Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_19, /*hidden argument*/NULL);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_21 = __this->get_U3CU3E4__this_2();
		NullCheck(L_21);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_22 = L_21->get_voxelizer_1();
		NullCheck(L_22);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_23 = L_22->get_address_of_resolution_8();
		int32_t L_24;
		L_24 = Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_23, /*hidden argument*/NULL);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_25 = __this->get_U3CU3E4__this_2();
		NullCheck(L_25);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_26 = L_25->get_voxelizer_1();
		NullCheck(L_26);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_27 = L_26->get_address_of_resolution_8();
		int32_t L_28;
		L_28 = Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_27, /*hidden argument*/NULL);
		il2cpp_array_size_t L_30[] = { (il2cpp_array_size_t)L_20, (il2cpp_array_size_t)L_24, (il2cpp_array_size_t)L_28 };
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_29 = (Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9*)GenArrayNew(Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9_il2cpp_TypeInfo_var, L_30);
		__this->set_U3CauxBufferU3E5__1_3(L_29);
		// for (int x = 0; x < distanceField.GetLength(0); ++x)
		__this->set_U3CxU3E5__6_8(0);
		goto IL_01f0;
	}

IL_00d6:
	{
		// for (int y = 0; y < distanceField.GetLength(1); ++y)
		__this->set_U3CyU3E5__7_9(0);
		goto IL_01be;
	}

IL_00e2:
	{
		// for (int z = 0; z < distanceField.GetLength(2); ++z)
		__this->set_U3CzU3E5__8_10(0);
		goto IL_018e;
	}

IL_00ee:
	{
		// if (voxelizer[x, y, z] == MeshVoxelizer.Voxel.Boundary)
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_31 = __this->get_U3CU3E4__this_2();
		NullCheck(L_31);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_32 = L_31->get_voxelizer_1();
		int32_t L_33 = __this->get_U3CxU3E5__6_8();
		int32_t L_34 = __this->get_U3CyU3E5__7_9();
		int32_t L_35 = __this->get_U3CzU3E5__8_10();
		NullCheck(L_32);
		int32_t L_36;
		L_36 = MeshVoxelizer_get_Item_mC5F92567FD7C76447DF970069641190AE24C1356(L_32, L_33, L_34, L_35, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_36) == ((int32_t)2))? 1 : 0);
		bool L_37 = V_1;
		if (!L_37)
		{
			goto IL_0153;
		}
	}
	{
		// distanceField[x, y, z] = new Vector3Int(x, y, z);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_38 = __this->get_U3CU3E4__this_2();
		NullCheck(L_38);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_39 = L_38->get_distanceField_0();
		int32_t L_40 = __this->get_U3CxU3E5__6_8();
		int32_t L_41 = __this->get_U3CyU3E5__7_9();
		int32_t L_42 = __this->get_U3CzU3E5__8_10();
		int32_t L_43 = __this->get_U3CxU3E5__6_8();
		int32_t L_44 = __this->get_U3CyU3E5__7_9();
		int32_t L_45 = __this->get_U3CzU3E5__8_10();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_46;
		memset((&L_46), 0, sizeof(L_46));
		Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline((&L_46), L_43, L_44, L_45, /*hidden argument*/NULL);
		NullCheck(L_39);
		(L_39)->SetAt(L_40, L_41, L_42, L_46);
		goto IL_017d;
	}

IL_0153:
	{
		// distanceField[x, y, z] = new Vector3Int(-1, -1, -1);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_47 = __this->get_U3CU3E4__this_2();
		NullCheck(L_47);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_48 = L_47->get_distanceField_0();
		int32_t L_49 = __this->get_U3CxU3E5__6_8();
		int32_t L_50 = __this->get_U3CyU3E5__7_9();
		int32_t L_51 = __this->get_U3CzU3E5__8_10();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_52;
		memset((&L_52), 0, sizeof(L_52));
		Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline((&L_52), (-1), (-1), (-1), /*hidden argument*/NULL);
		NullCheck(L_48);
		(L_48)->SetAt(L_49, L_50, L_51, L_52);
	}

IL_017d:
	{
		// for (int z = 0; z < distanceField.GetLength(2); ++z)
		int32_t L_53 = __this->get_U3CzU3E5__8_10();
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_53, (int32_t)1));
		int32_t L_54 = V_2;
		__this->set_U3CzU3E5__8_10(L_54);
	}

IL_018e:
	{
		// for (int z = 0; z < distanceField.GetLength(2); ++z)
		int32_t L_55 = __this->get_U3CzU3E5__8_10();
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_56 = __this->get_U3CU3E4__this_2();
		NullCheck(L_56);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_57 = L_56->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_57);
		int32_t L_58;
		L_58 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_57, 2, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_55) < ((int32_t)L_58))? 1 : 0);
		bool L_59 = V_3;
		if (L_59)
		{
			goto IL_00ee;
		}
	}
	{
		// for (int y = 0; y < distanceField.GetLength(1); ++y)
		int32_t L_60 = __this->get_U3CyU3E5__7_9();
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_60, (int32_t)1));
		int32_t L_61 = V_2;
		__this->set_U3CyU3E5__7_9(L_61);
	}

IL_01be:
	{
		// for (int y = 0; y < distanceField.GetLength(1); ++y)
		int32_t L_62 = __this->get_U3CyU3E5__7_9();
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_63 = __this->get_U3CU3E4__this_2();
		NullCheck(L_63);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_64 = L_63->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_64);
		int32_t L_65;
		L_65 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_64, 1, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_62) < ((int32_t)L_65))? 1 : 0);
		bool L_66 = V_4;
		if (L_66)
		{
			goto IL_00e2;
		}
	}
	{
		// for (int x = 0; x < distanceField.GetLength(0); ++x)
		int32_t L_67 = __this->get_U3CxU3E5__6_8();
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_67, (int32_t)1));
		int32_t L_68 = V_2;
		__this->set_U3CxU3E5__6_8(L_68);
	}

IL_01f0:
	{
		// for (int x = 0; x < distanceField.GetLength(0); ++x)
		int32_t L_69 = __this->get_U3CxU3E5__6_8();
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_70 = __this->get_U3CU3E4__this_2();
		NullCheck(L_70);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_71 = L_70->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_71);
		int32_t L_72;
		L_72 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_71, 0, /*hidden argument*/NULL);
		V_5 = (bool)((((int32_t)L_69) < ((int32_t)L_72))? 1 : 0);
		bool L_73 = V_5;
		if (L_73)
		{
			goto IL_00d6;
		}
	}
	{
		// int size = Mathf.Max(distanceField.GetLength(0),
		//                      distanceField.GetLength(1),
		//                      distanceField.GetLength(2));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_74 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)3);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_75 = L_74;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_76 = __this->get_U3CU3E4__this_2();
		NullCheck(L_76);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_77 = L_76->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_77);
		int32_t L_78;
		L_78 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_77, 0, /*hidden argument*/NULL);
		NullCheck(L_75);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)L_78);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_79 = L_75;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_80 = __this->get_U3CU3E4__this_2();
		NullCheck(L_80);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_81 = L_80->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_81);
		int32_t L_82;
		L_82 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_81, 1, /*hidden argument*/NULL);
		NullCheck(L_79);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_82);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_83 = L_79;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_84 = __this->get_U3CU3E4__this_2();
		NullCheck(L_84);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_85 = L_84->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_85);
		int32_t L_86;
		L_86 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_85, 2, /*hidden argument*/NULL);
		NullCheck(L_83);
		(L_83)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)L_86);
		int32_t L_87;
		L_87 = Mathf_Max_mC299EEF1FD6084E41A182E2033790DB50DEF5B39(L_83, /*hidden argument*/NULL);
		__this->set_U3CsizeU3E5__2_4(L_87);
		// int step = (int)(size / 2.0f);
		int32_t L_88 = __this->get_U3CsizeU3E5__2_4();
		__this->set_U3CstepU3E5__3_5(il2cpp_codegen_cast_double_to_int<int32_t>(((float)((float)((float)((float)L_88))/(float)(2.0f)))));
		// yield return new CoroutineJob.ProgressInfo("Generating voxel distance field...",0);
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_89 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_89, _stringLiteral8BF94FD2D8D349E76B64F4A8FEA9E6A331A7DB94, (0.0f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_89);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0291:
	{
		__this->set_U3CU3E1__state_0((-1));
		// float numPasses = (int) Mathf.Log(size, 2);
		int32_t L_90 = __this->get_U3CsizeU3E5__2_4();
		float L_91;
		L_91 = Mathf_Log_mF7F3624FA030AB57AD8C1F4CAF084B2DCC99897A(((float)((float)L_90)), (2.0f), /*hidden argument*/NULL);
		__this->set_U3CnumPassesU3E5__4_6(((float)((float)il2cpp_codegen_cast_double_to_int<int32_t>(L_91))));
		// int i = 0;
		__this->set_U3CiU3E5__5_7(0);
		goto IL_035e;
	}

IL_02bd:
	{
		// JumpFloodPass(step, distanceField, auxBuffer);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_92 = __this->get_U3CU3E4__this_2();
		int32_t L_93 = __this->get_U3CstepU3E5__3_5();
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_94 = __this->get_U3CU3E4__this_2();
		NullCheck(L_94);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_95 = L_94->get_distanceField_0();
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_96 = __this->get_U3CauxBufferU3E5__1_3();
		NullCheck(L_92);
		VoxelDistanceField_JumpFloodPass_m0CAE942EA7B7FB237160C57ABAC04D2F77198AC3(L_92, L_93, L_95, L_96, /*hidden argument*/NULL);
		// step /= 2;
		int32_t L_97 = __this->get_U3CstepU3E5__3_5();
		__this->set_U3CstepU3E5__3_5(((int32_t)((int32_t)L_97/(int32_t)2)));
		// Vector3Int[,,] temp = distanceField;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_98 = __this->get_U3CU3E4__this_2();
		NullCheck(L_98);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_99 = L_98->get_distanceField_0();
		__this->set_U3CtempU3E5__9_11(L_99);
		// distanceField = auxBuffer;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_100 = __this->get_U3CU3E4__this_2();
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_101 = __this->get_U3CauxBufferU3E5__1_3();
		NullCheck(L_100);
		L_100->set_distanceField_0(L_101);
		// auxBuffer = temp;
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_102 = __this->get_U3CtempU3E5__9_11();
		__this->set_U3CauxBufferU3E5__1_3(L_102);
		// yield return new CoroutineJob.ProgressInfo("Generating voxel distance field...", ++i / numPasses);
		int32_t L_103 = __this->get_U3CiU3E5__5_7();
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_103, (int32_t)1));
		int32_t L_104 = V_2;
		__this->set_U3CiU3E5__5_7(L_104);
		int32_t L_105 = V_2;
		float L_106 = __this->get_U3CnumPassesU3E5__4_6();
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_107 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_107, _stringLiteral8BF94FD2D8D349E76B64F4A8FEA9E6A331A7DB94, ((float)((float)((float)((float)L_105))/(float)L_106)), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_107);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_034f:
	{
		__this->set_U3CU3E1__state_0((-1));
		__this->set_U3CtempU3E5__9_11((Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9*)NULL);
	}

IL_035e:
	{
		// while (step >= 1)
		int32_t L_108 = __this->get_U3CstepU3E5__3_5();
		V_6 = (bool)((((int32_t)((((int32_t)L_108) < ((int32_t)1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_109 = V_6;
		if (L_109)
		{
			goto IL_02bd;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CJumpFloodU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF414A3DC08D65DC419082B4635288E1F7E02FC0 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22_RuntimeMethod_var)));
	}
}
// System.Object Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_get_Current_m0850F7B63FF4B6EB05FB8A60525AB3BC48509D18 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.VoxelPathFinder/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m2E9966CB04100D59E489C39A64FAC67F8FD97311 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * L_0 = (U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 *)il2cpp_codegen_object_new(U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m7A494B57D1990F9D4FD4EF63281425ABEC97A888(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Obi.VoxelPathFinder/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m7A494B57D1990F9D4FD4EF63281425ABEC97A888 (U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Obi.VoxelPathFinder/<>c::<FindClosestNonEmptyVoxel>b__6_1(UnityEngine.Vector3Int)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CU3Ec_U3CFindClosestNonEmptyVoxelU3Eb__6_1_mF780311AA35327276D353A5C472EBABD63FB6D10 (U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___c0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// return 0;
		V_0 = (0.0f);
		goto IL_0009;
	}

IL_0009:
	{
		// });
		float L_0 = V_0;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.VoxelPathFinder/<>c__DisplayClass7_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass7_0__ctor_mCCDBC132E254A9EA49862CDDDB93E94F182AF58C (U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Obi.VoxelPathFinder/<>c__DisplayClass7_0::<FindPath>b__0(Obi.VoxelPathFinder/TargetVoxel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__0_m0CBCD40678980CB309FD296C8DCA140B2368C158 (U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___v0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// return v.coordinates == end;
		TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  L_0 = ___v0;
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_1 = L_0.get_coordinates_0();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_2 = __this->get_end_0();
		bool L_3;
		L_3 = Vector3Int_op_Equality_m724847B7E7A484A1E6F598CEC2D77CDE8ECE49E7_inline(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0015;
	}

IL_0015:
	{
		// },
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Single Obi.VoxelPathFinder/<>c__DisplayClass7_0::<FindPath>b__1(UnityEngine.Vector3Int)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__1_m93C19DF998EC9A85989D9980D3C89F8EB7724361 (U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___c0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// return Vector3.Distance(c, end) * voxelizer.voxelSize;
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_0 = ___c0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3Int_op_Implicit_mD812DEDBDE886508E86FB3222BB9DDB4949B4475_inline(L_0, /*hidden argument*/NULL);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_2 = __this->get_end_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3Int_op_Implicit_mD812DEDBDE886508E86FB3222BB9DDB4949B4475_inline(L_2, /*hidden argument*/NULL);
		float L_4;
		L_4 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_1, L_3, /*hidden argument*/NULL);
		VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF * L_5 = __this->get_U3CU3E4__this_1();
		NullCheck(L_5);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_6 = L_5->get_voxelizer_0();
		NullCheck(L_6);
		float L_7 = L_6->get_voxelSize_7();
		V_0 = ((float)il2cpp_codegen_multiply((float)L_4, (float)L_7));
		goto IL_002b;
	}

IL_002b:
	{
		// });
		float L_8 = V_0;
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Obi.VoxelPathFinder/TargetVoxel::get_cost()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// get { return distance + heuristic; }
		float L_0 = __this->get_distance_1();
		float L_1 = __this->get_heuristic_2();
		V_0 = ((float)il2cpp_codegen_add((float)L_0, (float)L_1));
		goto IL_0011;
	}

IL_0011:
	{
		// get { return distance + heuristic; }
		float L_2 = V_0;
		return L_2;
	}
}
IL2CPP_EXTERN_C  float TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * _thisAdjusted = reinterpret_cast<TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *>(__this + _offset);
	float _returnValue;
	_returnValue = TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F(_thisAdjusted, method);
	return _returnValue;
}
// System.Void Obi.VoxelPathFinder/TargetVoxel::.ctor(UnityEngine.Vector3Int,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___coordinates0, float ___distance1, float ___heuristic2, const RuntimeMethod* method)
{
	{
		// this.coordinates = coordinates;
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_0 = ___coordinates0;
		__this->set_coordinates_0(L_0);
		// this.distance = distance;
		float L_1 = ___distance1;
		__this->set_distance_1(L_1);
		// this.heuristic = heuristic;
		float L_2 = ___heuristic2;
		__this->set_heuristic_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF_AdjustorThunk (RuntimeObject * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___coordinates0, float ___distance1, float ___heuristic2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * _thisAdjusted = reinterpret_cast<TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *>(__this + _offset);
	TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF(_thisAdjusted, ___coordinates0, ___distance1, ___heuristic2, method);
}
// System.Boolean Obi.VoxelPathFinder/TargetVoxel::Equals(Obi.VoxelPathFinder/TargetVoxel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681 (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// return this.coordinates.Equals(other.coordinates);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_0 = __this->get_address_of_coordinates_0();
		TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  L_1 = ___other0;
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_2 = L_1.get_coordinates_0();
		bool L_3;
		L_3 = Vector3Int_Equals_m8BE683205BACD053B7EB560AB5B7EDE78B779C5F_inline((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0015;
	}

IL_0015:
	{
		// }
		bool L_4 = V_0;
		return L_4;
	}
}
IL2CPP_EXTERN_C  bool TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681_AdjustorThunk (RuntimeObject * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * _thisAdjusted = reinterpret_cast<TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *>(__this + _offset);
	bool _returnValue;
	_returnValue = TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681(_thisAdjusted, ___other0, method);
	return _returnValue;
}
// System.Int32 Obi.VoxelPathFinder/TargetVoxel::CompareTo(Obi.VoxelPathFinder/TargetVoxel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8 (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		// return this.cost.CompareTo(other.cost);
		float L_0;
		L_0 = TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F((TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F((TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *)(&___other0), /*hidden argument*/NULL);
		int32_t L_2;
		L_2 = Single_CompareTo_m80B5B5A70A2343C3A8673F35635EBED4458109B4((float*)(&V_0), L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		// }
		int32_t L_3 = V_1;
		return L_3;
	}
}
IL2CPP_EXTERN_C  int32_t TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8_AdjustorThunk (RuntimeObject * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * _thisAdjusted = reinterpret_cast<TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8(_thisAdjusted, ___other0, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2Int__ctor_mB2B73108B6DD3ADC1B515D7DD9116ECAC6833726_inline (Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___x0;
		__this->set_m_X_0(L_0);
		int32_t L_1 = ___y1;
		__this->set_m_Y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_inline (Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Y_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_inline (Vector2Int_tF49F5C2443670DE126D9EC8DBE81D8F480EAA6E9 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_X_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3_inline (int32_t ___mask0, int32_t ___category1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// return (mask << 16) | (1 << category);
		int32_t L_0 = ___mask0;
		int32_t L_1 = ___category1;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0<<(int32_t)((int32_t)16)))|(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))))));
		goto IL_000f;
	}

IL_000f:
	{
		// }
		int32_t L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68_inline (float ___mass0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// return 1.0f / Mathf.Max(mass, 0.00001f);
		float L_0 = ___mass0;
		float L_1;
		L_1 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_0, (9.99999975E-06f), /*hidden argument*/NULL);
		V_0 = ((float)((float)(1.0f)/(float)L_1));
		goto IL_0015;
	}

IL_0015:
	{
		// }
		float L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___x0;
		__this->set_m_X_0(L_0);
		int32_t L_1 = ___y1;
		__this->set_m_Y_1(L_1);
		int32_t L_2 = ___z2;
		__this->set_m_Z_2(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float ObiUtils_RestBendingConstraint_mA2A2E588C9B5B6907DC2670CF3176A0441D95C78_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positionA0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positionB1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positionC2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		// Vector3 center = (positionA + positionB + positionC) / 3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___positionA0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ___positionB1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_0, L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___positionC2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_2, L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline(L_4, (3.0f), /*hidden argument*/NULL);
		V_0 = L_5;
		// return (positionC - center).magnitude;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___positionC2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9;
		L_9 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), /*hidden argument*/NULL);
		V_2 = L_9;
		goto IL_002b;
	}

IL_002b:
	{
		// }
		float L_10 = V_2;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3Int_op_Equality_m724847B7E7A484A1E6F598CEC2D77CDE8ECE49E7_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___lhs0, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0;
		L_0 = Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___lhs0), /*hidden argument*/NULL);
		int32_t L_1;
		L_1 = Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___rhs1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_2;
		L_2 = Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___lhs0), /*hidden argument*/NULL);
		int32_t L_3;
		L_3 = Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___rhs1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_4;
		L_4 = Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___lhs0), /*hidden argument*/NULL);
		int32_t L_5;
		L_5 = Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___rhs1), /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0034;
	}

IL_0033:
	{
		G_B4_0 = 0;
	}

IL_0034:
	{
		V_0 = (bool)G_B4_0;
		goto IL_0037;
	}

IL_0037:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3Int_op_Implicit_mD812DEDBDE886508E86FB3222BB9DDB4949B4475_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___v0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0;
		L_0 = Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___v0), /*hidden argument*/NULL);
		int32_t L_1;
		L_1 = Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___v0), /*hidden argument*/NULL);
		int32_t L_2;
		L_2 = Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___v0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_3), ((float)((float)L_0)), ((float)((float)L_1)), ((float)((float)L_2)), /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3Int_Equals_m8BE683205BACD053B7EB560AB5B7EDE78B779C5F_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_0 = (*(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)__this);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_1 = ___other0;
		bool L_2;
		L_2 = Vector3Int_op_Equality_m724847B7E7A484A1E6F598CEC2D77CDE8ECE49E7_inline(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)L_2, (int32_t)L_3);
		return (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float List_1_get_Item_mE747DE332539CAC0473E9C10EC3657830A3BBF97_gshared_inline (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_2 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)__this->get__items_1();
		int32_t L_3 = ___index0;
		float L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)L_2, (int32_t)L_3);
		return (float)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get__items_1();
		int32_t L_3 = ___index0;
		int32_t L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)L_2, (int32_t)L_3);
		return (int32_t)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  List_1_get_Item_m44FF1F2058A782761654B36B8879FF59BAD708F4_gshared_inline (List_1_t6ECA266B0EAC70596EFCED9D1D6E42D5E8A7E18E * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_2 = (ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)L_2, (int32_t)L_3);
		return (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_7/(float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
