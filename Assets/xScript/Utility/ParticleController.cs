using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ParticleController : MonoBehaviour
{
    private List<ParticleSystem> particleSystems = new List<ParticleSystem>();
   
    private void Start()
    {
        particleSystems = gameObject.GetComponentsInChildren<ParticleSystem>().ToList();
        Stop();
    }

    private void OnEnable()
    {
        Stop();
    }

    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.RawButton.X) || Input.GetKeyDown(KeyCode.H))
            Play();
        else if (OVRInput.GetUp(OVRInput.RawButton.X) || Input.GetKeyDown(KeyCode.L))
            Stop();
    }

    public void Play()
    {
        foreach (var i in particleSystems)
            i.Play();
    }

    public void Stop()
    {
        foreach (var i in particleSystems)
            i.Stop();
    }

}
