using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCasting : MonoBehaviour
{
    public Collider hitCollider;
    [SerializeField] private LayerMask allowMask;

    [Header("LineRenderer")]
    [SerializeField] private Material matRay;
    [SerializeField] private float lineWidth = 0.001f;
    [SerializeField] private bool isHead = true;
    private LineRenderer lr;

    private void Start()
    {
        if (!isHead)
        {
            lr = gameObject.GetComponent<LineRenderer>();
            lr.enabled = false;
            // lr.material = matRay;
            // lr.startWidth = lineWidth;
            // lr.endWidth = lineWidth;
            // lr.positionCount = 2;
        }
    }

    private void Update()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, allowMask))
        {
            Debug.DrawLine(transform.localPosition, transform.TransformPoint(Vector3.forward * Vector3.Distance(hit.point, transform.position)), Color.red);
            hitCollider = hit.collider;

            if (!isHead)
            {
               // lr.enabled = true;
               // lr.SetPosition(0, transform.position);
               // lr.SetPosition(1, transform.TransformPoint(Vector3.forward * Vector3.Distance(hit.point, transform.position)));
            }
        }
        else
        {
            hitCollider = null;

            if (!isHead)
            {
               // lr.enabled = false;
            }
        }
    }
}
