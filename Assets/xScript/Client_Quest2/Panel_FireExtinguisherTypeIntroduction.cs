﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using static GPTT.Data.API;

public class Panel_FireExtinguisherTypeIntroduction : MonoBehaviour
{
    [SerializeField] private Board board;

    [SerializeField] private List<FireExtinguisherType> fireExtinguisherTypes = new List<FireExtinguisherType>();
    [SerializeField] private Button btBack;

    public event EventHandler EHCloseFireExtinguisherType;

    [System.Serializable]
    private class FireExtinguisherType
    {
        public Toggle tgType;
        public string strTitle;
        public string strSubTitle;
        [TextArea]
        public string strDescription;
        public bool isClick = false;
    }

    [System.Serializable]
    private class Board
    {
        public RectTransform rectBoard;
        public Text txtBoardTitle;
        public Text txtSubTitle;
        public Text txtBoardBody;
    }

    //**
    private FireExtinguisher fireExtinguisher = new FireExtinguisher();
    private DateTime _startTime;
    private DateTime _endTime;
    //**

    private void OnEnable()
    {
        //**
        fireExtinguisher = new FireExtinguisher();
        _startTime = DateTime.Now;
        fireExtinguisher.startTime = _startTime.ToString("yyyy-MM-dd HH:mm:ss");
        //**

        Init();
        foreach (var i in fireExtinguisherTypes)
        {
            i.isClick = false;
            var imgs = i.tgType.GetComponentsInChildren<Image>();
            foreach (var j in imgs)
            {
                if (j != i.tgType.image)
                    j.enabled = false;
            }
            i.tgType.onValueChanged.AddListener(delegate { FireTypeChange(i.tgType); });
        }
        btBack.onClick.AddListener(delegate { StartCoroutine(BackToMenu()); });
        click = false;
    }

    private void Init()
    {
        board.rectBoard.gameObject.SetActive(false);
    }

    private void FireTypeChange(Toggle t)
    {
        foreach (var i in fireExtinguisherTypes)
        {
            if (i.tgType == t)
            {
                if (t.isOn)
                {
                    board.rectBoard.gameObject.SetActive(true);

                    board.txtBoardTitle.text = i.strTitle;
                    board.txtSubTitle.text = i.strSubTitle;
                    board.txtBoardBody.text = i.strDescription;

                    var imgs = i.tgType.GetComponentsInChildren<Image>();
                    t.gameObject.GetComponent<AudioSource>().Play();
                    i.isClick = true;
                    foreach (var j in imgs)
                    {
                        if (j != i.tgType.image)
                            j.enabled = true;
                    }
                }
                else if (!t.isOn)
                {
                    board.rectBoard.gameObject.SetActive(false);

                    var imgs = i.tgType.GetComponentsInChildren<Image>();
                    t.gameObject.GetComponent<AudioSource>().Stop();
                    foreach (var j in imgs)
                    {
                        if (j != i.tgType.image)
                            j.enabled = false;
                    }
                }
            }
        }
    }

    bool click = false;
    private IEnumerator BackToMenu()
    {
        if (!click)
        {
            click = true;
#if !UNITY_EDITOR
            //**
            _endTime = DateTime.Now;
            fireExtinguisher.endTime = _endTime.ToString("yyyy-MM-dd HH:mm:ss");

            var diff = _endTime.Subtract(_startTime);
            var res = String.Format("{0}h {1}m {2}s", diff.Hours, diff.Minutes, diff.Seconds);
            fireExtinguisher.totalTime = res;

            fireExtinguisher.project = "火災類型與滅火器-滅火器類型";

            List<bool> p = new List<bool>();
            foreach (var i in fireExtinguisherTypes)
                p.Add(i.isClick);

            if (p.Contains(false))
                fireExtinguisher.status = "未完成";
            else
                fireExtinguisher.status = "已完成";

            fireExtinguisher.userId = UserIDCheck.userID;

            yield return (GPTT.Data.APIHelper.FireExtinguisher_Insert(fireExtinguisher));
            //**
#endif
            yield return null;
            EHCloseFireExtinguisherType(this, EventArgs.Empty);
        }
    }
}
