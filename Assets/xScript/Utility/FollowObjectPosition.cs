﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObjectPosition : MonoBehaviour
{
    public Transform target;
    public Vector3 fixPos = new Vector3(-0.05f, 0.1f, 0);

    // Update is called once per frame
    void Update()
    {
        if (target == null)
            return;

        gameObject.transform.position = target.transform.TransformPoint(fixPos);
        gameObject.transform.rotation = target.transform.rotation;
    }
}
