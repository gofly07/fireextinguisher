﻿using UnityEngine;

namespace DKP.VR
{
    public class LaserRay : MonoBehaviour
    {
        #region DECLARE

        public Vector3 hitpos = new Vector3();
        [Header("Anchor Info")]
        public Hand hand = Hand.right;

        private OVRControllerHelper handPose;
        public bool hideWhenNotAimingAtCanvas = false;

        [Header("LineRenderer")]
        [SerializeField] private float lineWidth = 0.001f;
        [SerializeField] private Material matRay;
        [SerializeField] private GameObject sprDot;
        [SerializeField] private LayerMask allowMask;
        [SerializeField] public bool rayEnable = false;
        private LineRenderer lr;
        #endregion

        #region PRIVATE

        private void Start()
        {
            if (hand == Hand.left)
                handPose = VRPlatformSetting.instance.oVRLeft;
            else if (hand == Hand.right)
                handPose = VRPlatformSetting.instance.oVRRight;

            lr = gameObject.GetComponent<LineRenderer>();
            lr.material = matRay;
            lr.startWidth = lineWidth;
            lr.endWidth = lineWidth;
            lr.positionCount = 2;
            sprDot.SetActive(false);
        }

        private void Update()
        {
            gameObject.transform.position = handPose.transform.position;
            gameObject.transform.rotation = handPose.transform.rotation;

            if (ControlState.rightHandTouching)
            {
                lr.enabled = false;
                sprDot.SetActive(false);
                rayEnable = false;
                return;
            }

            //lr.enabled = true;
            //sprDot.SetActive(true);
            rayEnable = true;
            RayHit();
        }

        private void RayHit()
        {
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, allowMask))
            {
                lr.enabled = true;
                lr.SetPosition(0, transform.position);
                lr.SetPosition(1, transform.TransformPoint(Vector3.forward * Vector3.Distance(hit.point, transform.position)));
                //sprDot.SetActive(true);
                sprDot.transform.position = hit.point;
                hitpos = hit.point;
                Quaternion newRotation = Quaternion.LookRotation(hit.normal);
                sprDot.transform.rotation = newRotation;

                CurvedUIInputModule.CustomControllerRay = ray;
                CurvedUIInputModule.CustomControllerButtonState = OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, handPose.m_controller);

                ControlState.rayCasting = true;

            }
            else
            {
                lr.enabled = true;
                lr.SetPosition(0, transform.position);
                lr.SetPosition(1, transform.TransformPoint(Vector3.forward * 3));
            }
            //else if (hideWhenNotAimingAtCanvas)
            //{
            //    lr.enabled = false;
            //    sprDot.SetActive(false);
            //    rayEnable = false;
            //    ControlState.rayCasting = false;
            //}
        }

        #endregion
    }
}