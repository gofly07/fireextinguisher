﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.InteropServices.ComVisibleAttribute
struct ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.InteropServices.ComVisibleAttribute::_val
	bool ____val_0;

public:
	inline static int32_t get_offset_of__val_0() { return static_cast<int32_t>(offsetof(ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A, ____val_0)); }
	inline bool get__val_0() const { return ____val_0; }
	inline bool* get_address_of__val_0() { return &____val_0; }
	inline void set__val_0(bool value)
	{
		____val_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Diagnostics.DebuggerBrowsableState
struct DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091 
{
public:
	// System.Int32 System.Diagnostics.DebuggerBrowsableState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggerBrowsableState System.Diagnostics.DebuggerBrowsableAttribute::state
	int32_t ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerBrowsableAttribute::.ctor(System.Diagnostics.DebuggerBrowsableState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5 (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * __this, int32_t ___state0, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.ComVisibleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172 (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * __this, bool ___visibility0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
static void websocketU2Dsharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[0];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x77\x65\x62\x73\x6F\x63\x6B\x65\x74\x2D\x73\x68\x61\x72\x70\x2E\x64\x6C\x6C"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[1];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[2];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[3];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper("\x41\x20\x43\x23\x20\x69\x6D\x70\x6C\x65\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x57\x65\x62\x53\x6F\x63\x6B\x65\x74\x20\x70\x72\x6F\x74\x6F\x63\x6F\x6C\x20\x63\x6C\x69\x65\x6E\x74\x20\x61\x6E\x64\x20\x73\x65\x72\x76\x65\x72"), NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[4];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x77\x65\x62\x73\x6F\x63\x6B\x65\x74\x2D\x73\x68\x61\x72\x70"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[5];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[6];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[7];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[8];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[9];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x73\x74\x61\x2E\x62\x6C\x6F\x63\x6B\x68\x65\x61\x64"), NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[10];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_compress_m4358278284A560842E882535CAEE02997945C3DA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_decompress_mDE27860D3912BD63403E7BF996F153F59FD92D3D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_decompress_m2F24DFAF850F9B35C3FD4E0A1A3E7FF6C9C5AF14(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_decompressToArray_mEB8B011B30C5A46E00DA7F87520D6019B8242A11(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Append_m0E57723715B401B8746CE5C9749899C97D164541(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Compress_mE2A46ECF97EAAEBF6113AE9E870D50CAC040AB6F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Contains_mF267D8AEC1CA95308565F0B4A52350ABAF786A2E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Contains_mF267D8AEC1CA95308565F0B4A52350ABAF786A2E____anyOf1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Contains_m17E1757D14770EB91B608CE2815E7BA960DA2313(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Contains_m170EAA539632CF3112285D07DD04669E8F7EC866(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ContainsTwice_m4DD33BCD410B1D870759892F8B6D7D77AD927BD2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_CopyTo_mB436FB49B8616F60E26EFEB3C6F0D723A4189FBA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Decompress_m5051854DFB667F2D5FC17DE828439EB1A67F9D00(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_DecompressToArray_mAA1406B8850D2404435700563124565AEBBE8F03(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Emit_m53CC51B43FBF1FC6AED0C878BE0DC02D26EF220B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Emit_m965ABF62E5ECC5E012DD85C3EE0A7B2CAA7C7948(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_EqualsWith_mB93057EE05D870A4CFA31EA72B15A632D54EE6E5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_GetAbsolutePath_m719787C61869F48F573EE93861A56430E98AEB17(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_GetCookies_mE81477DBE254503ADF2F5B3173D4D96EEC7D4E4A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_GetMessage_m53797B74D6828C7E9D1B91D575F6159AD431C03E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_GetUTF8EncodedBytes_mB9035E3F22A387B3717298A96D687EDC5AFB71B2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_GetValue_mAF45AD216AC35C5D1BA224BEAAD6A6FF83DDE044(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_InternalToByteArray_m3CCD4BFE6D64214EDB0D5C07514DDBD636A2303F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_InternalToByteArray_mD8ECE713D31534241B3A20C070AECD1275D2D2B6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsCompressionExtension_m2D95C896B134964E2BCC94AF1FD73399888B03E9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsControl_mD6C47A8942EC85383A1FA21D750B3420A3A600E2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsData_mD1FDD951BEAC505088F40E7E96A4EC37DAA1B921(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsData_m0D314D4CE1CB0481957905FA9EF5DD8E08B33A0D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsReserved_mB14EE3EC1D539A9C9C35652B3BE120E97A5F96EB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsSupported_m274DFE67596E1C20FE078D43462675E1471E0BC2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsText_m4BF1ADE56AD743FBE48AD8F85DC2586F980D2164(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsToken_m181B2CFD2601FFEADB4BCCE758E0DAD9F158C2DE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ReadBytes_m46DBC394CDA9BD532F01E92390A9DACBF2B92C2D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ReadBytes_m0F3560D4CFEC3F4E2F01A6AB4F8CF19BD301BDD3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ReadBytesAsync_m16D0BD06630A2B20F3A59700665CA0A5328E736F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ReadBytesAsync_m4A84EB6C86EDB4A46262CF97D20258137A7A5FCF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Reverse_mCA306A4C0B475114A5217669A945EF1978C67EC4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_SplitHeaderValue_m01EE9C6D8102DB88FA9937B7EC8AF3CF4004B67A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_SplitHeaderValue_m01EE9C6D8102DB88FA9937B7EC8AF3CF4004B67A____separators1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToByteArray_mD20CDE09F09523A7A54F5489D9A42326FDCC561D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToExtensionString_m1A4430E0A6CD59742837AB50919C7008230C0E39(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToExtensionString_m1A4430E0A6CD59742837AB50919C7008230C0E39____parameters1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToList_m9BDDD3572D9F684B673305CC3500C90A26FFC7AA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToUInt16_mF963B0EB55BD1C1B24DF8FCB77070EB8AE1908CB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToUInt64_m8875AD2574EA9F437EB6499A833BBB87A8A07CB4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_TryCreateWebSocketUri_m3944CC7FC2F49F59DE819B4E18877FE84C9CB42A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_TryGetUTF8DecodedString_m07ED759625CB1683806215FDC4EA050F3F41E062(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_TryGetUTF8EncodedBytes_m9CA6A8F40D22157CE7096C2C0C90EEAF35D18CB5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Unquote_m430FB83166985AC03BF2BD8283CAEDBE21F66602(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Upgrades_m672B32A90886DEF7430195058BDD9B85E0F955F9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_WriteBytes_mC92631F017F5C1290CB883FBAECE35156B2ED308(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsEnclosedIn_mC4AD7A067E1070CDD25235847375ECA14647D8F3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsHostOrder_m82CC168E47DC97D8E61D971E684A97AC7F748AF2(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsNullOrEmpty_mEB4CFD1426630064F5435FADB81B48B26161793E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsPredefinedScheme_m274F02E1D5677ED6112F71F299C6562BA2AFD5C0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_MaybeUri_m9183FD39642FB589BE7AE769C8146A6A19386B11(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_SubArray_m3EE8DD51103A031A9967A3A29B4DC6FD7CA28DE3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_SubArray_mB3FCB98AA131E275E724E51383CE9D8195958137(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToHostOrder_mFC9D0D1F9DC21733829F38C35C3B819DD90A9CBC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToString_mD66D5879C7E7EE3E24A2A07C0F86A5698D75D07C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToUri_m36BFA928AEF6A14DB081BFEF75143B68665F2F88(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t502C290D482AE07C146468D19001D28D2FFBA1AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass59_0_tED6F22C5EBE723DA1CC46AC9913FA3CBE27F0573_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass60_0_t642F25F809DB0D9CCE1581856CB88DC9AB43D4D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass60_1_tA0BE8C6B39285DF556AAD89BC27B1747625741D6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62__ctor_mD0F6C1B1B13A73C577FF6F04AA2EF11BF7C895F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_IDisposable_Dispose_m13B0D952747EF8A3E7E734C9FE4A9B6E4E7ABB05(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m303D4DF29F9239C771BA12712ABC5AE8D4CBE13C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_Reset_m48C8B244C0B1648685FBD53A216EE5004BD258D3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_get_Current_mFB4110BE04322EC29F00261128176708E13C50A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m20D4ECB95C3C084914B659CF5E262D913718F626(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerable_GetEnumerator_mA5D81BE65336ED51944619BE3CAC8FA0FF695C95(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_OnClose(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_OnError(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_OnMessage(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_OnOpen(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket__ctor_m922DA968DBD07A0529B3BE9CE576188FD0B5C094____protocols1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_add_OnClose_mA8DE5F76FD7433CDAD33CC2D8812D7001B750A66(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_remove_OnClose_m2AE23DB4E743F675A549A0E71E3558FF7A2F3DBB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_add_OnError_m362EE37535D15C3B900BAD60249F10707EA6729E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_remove_OnError_mD2F241B0D17251C16DF1A40E533E5F20AB19E651(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_add_OnMessage_mFB8C1D5D98E13FCCB1653ABE35704A532B5988A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_remove_OnMessage_m455B56B3B9360D764DD5A5BB635ECAFF6EB54D07(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_add_OnOpen_m40F1299BBE18CDD3880D5111CAFD058C2D02A874(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_remove_OnOpen_m30290C709FC3C8F8DDC36076F97F87E04D7DDD2C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_U3CopenU3Eb__146_0_mD7B092980016BCF1CBBC93B23FDD9B18AC9CA2F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t4D43DE0F6A3B4A635F0BB341122D267D40FB6718_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass174_0_t1B72AC8F0EFD1FF8B4E6BF92EB39825E08050CE0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass176_0_t26CD9F37DDAA301F37DE34755DA896B4E8B699FC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass177_0_t77007918364F22D2EBDD3451C748292AB77491CF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__25__ctor_m7290C117BBBD824041032B6A783AE4AF32226A43(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__25_System_IDisposable_Dispose_mFDCDAD41B87D1F55DAE4A68FE3767B22A28C5F54(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mA7B801C8652EAE220C45112BB0883B24421B41B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m413CE860B0CE2F51C4F2E31AB83F860327B58A54(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m2CCE50DF3B959BC0B0D59F873D0DB781453FE120(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass67_0_t569E092299CA369FD9664DCFFEC8155BCE00A3C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass67_1_tCB6EE6F04FDE0AB3D1A2099078D08463607563D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass71_0_tEC28BFC3911203E685E25A7572AB4D78A4D50139_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass73_0_t4AA3B4687A64288E5E8D09F7D514D2CD4805FB9D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass75_0_tFDCFD4A18A8D1130C1065B4BC23C8B102B01234E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass77_0_t63957EF4D98541B64E18FABCA19C0D89BF153B6F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass84_0_t02ACBC7A71DCF4C4A09594F18F3456287BA6B95A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__86__ctor_mFDDD212B6E6A6FFAB4FD0CA6D9541929DE36E06A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__86_System_IDisposable_Dispose_mDA8B0BA2DB9C194B5932895D77173C70CF63D01D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__86_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m3EA1B7BF4D4B7576E0EFB264B5919542FB74CD33(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_Reset_m79C1B3397472E5979B328686C1169427742B14B4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_get_Current_mC579C33994AC27CEEAD74CEC09B3D6F517560A20(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_tCA77B1AE8FF6E043C965009A529D4F4A6A049EBE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_1_t0E049A755CC8C97AE4873C3C14EFCD68EF4229AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CookieCollection_tB27ED92A2C7D86E898CFA90FD60E1B758FD8AB26_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void WebHeaderCollection_t0AF23B0B026F25C80B084844A98DF30BDABE6FFD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A * tmp = (ComVisibleAttribute_tCE3DF5E341F3ECE4C81FE85C15B3D782AB302A2A *)cache->attributes[0];
		ComVisibleAttribute__ctor_mBDE8E12A0233C07B98D6D5103511F4DD5B1FC172(tmp, true, NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void HttpHeaderType_t5C8594F632954E75AA65DE48AED89827F8B7DB03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void U3CU3Ec_tB4D483EB29987D65A7D2CB676AA78EE82BB77E32_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_websocketU2Dsharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_websocketU2Dsharp_AttributeGenerators[118] = 
{
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t502C290D482AE07C146468D19001D28D2FFBA1AB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass59_0_tED6F22C5EBE723DA1CC46AC9913FA3CBE27F0573_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass60_0_t642F25F809DB0D9CCE1581856CB88DC9AB43D4D5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass60_1_tA0BE8C6B39285DF556AAD89BC27B1747625741D6_CustomAttributesCacheGenerator,
	U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator,
	U3CU3Ec_t4D43DE0F6A3B4A635F0BB341122D267D40FB6718_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass174_0_t1B72AC8F0EFD1FF8B4E6BF92EB39825E08050CE0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass176_0_t26CD9F37DDAA301F37DE34755DA896B4E8B699FC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass177_0_t77007918364F22D2EBDD3451C748292AB77491CF_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass67_0_t569E092299CA369FD9664DCFFEC8155BCE00A3C1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass67_1_tCB6EE6F04FDE0AB3D1A2099078D08463607563D4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass71_0_tEC28BFC3911203E685E25A7572AB4D78A4D50139_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass73_0_t4AA3B4687A64288E5E8D09F7D514D2CD4805FB9D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass75_0_tFDCFD4A18A8D1130C1065B4BC23C8B102B01234E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass77_0_t63957EF4D98541B64E18FABCA19C0D89BF153B6F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass84_0_t02ACBC7A71DCF4C4A09594F18F3456287BA6B95A_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_tCA77B1AE8FF6E043C965009A529D4F4A6A049EBE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_1_t0E049A755CC8C97AE4873C3C14EFCD68EF4229AA_CustomAttributesCacheGenerator,
	CookieCollection_tB27ED92A2C7D86E898CFA90FD60E1B758FD8AB26_CustomAttributesCacheGenerator,
	WebHeaderCollection_t0AF23B0B026F25C80B084844A98DF30BDABE6FFD_CustomAttributesCacheGenerator,
	HttpHeaderType_t5C8594F632954E75AA65DE48AED89827F8B7DB03_CustomAttributesCacheGenerator,
	U3CU3Ec_tB4D483EB29987D65A7D2CB676AA78EE82BB77E32_CustomAttributesCacheGenerator,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_OnClose,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_OnError,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_OnMessage,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_OnOpen,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_compress_m4358278284A560842E882535CAEE02997945C3DA,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_decompress_mDE27860D3912BD63403E7BF996F153F59FD92D3D,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_decompress_m2F24DFAF850F9B35C3FD4E0A1A3E7FF6C9C5AF14,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_decompressToArray_mEB8B011B30C5A46E00DA7F87520D6019B8242A11,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Append_m0E57723715B401B8746CE5C9749899C97D164541,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Compress_mE2A46ECF97EAAEBF6113AE9E870D50CAC040AB6F,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Contains_mF267D8AEC1CA95308565F0B4A52350ABAF786A2E,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Contains_m17E1757D14770EB91B608CE2815E7BA960DA2313,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Contains_m170EAA539632CF3112285D07DD04669E8F7EC866,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ContainsTwice_m4DD33BCD410B1D870759892F8B6D7D77AD927BD2,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_CopyTo_mB436FB49B8616F60E26EFEB3C6F0D723A4189FBA,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Decompress_m5051854DFB667F2D5FC17DE828439EB1A67F9D00,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_DecompressToArray_mAA1406B8850D2404435700563124565AEBBE8F03,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Emit_m53CC51B43FBF1FC6AED0C878BE0DC02D26EF220B,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Emit_m965ABF62E5ECC5E012DD85C3EE0A7B2CAA7C7948,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_EqualsWith_mB93057EE05D870A4CFA31EA72B15A632D54EE6E5,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_GetAbsolutePath_m719787C61869F48F573EE93861A56430E98AEB17,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_GetCookies_mE81477DBE254503ADF2F5B3173D4D96EEC7D4E4A,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_GetMessage_m53797B74D6828C7E9D1B91D575F6159AD431C03E,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_GetUTF8EncodedBytes_mB9035E3F22A387B3717298A96D687EDC5AFB71B2,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_GetValue_mAF45AD216AC35C5D1BA224BEAAD6A6FF83DDE044,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_InternalToByteArray_m3CCD4BFE6D64214EDB0D5C07514DDBD636A2303F,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_InternalToByteArray_mD8ECE713D31534241B3A20C070AECD1275D2D2B6,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsCompressionExtension_m2D95C896B134964E2BCC94AF1FD73399888B03E9,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsControl_mD6C47A8942EC85383A1FA21D750B3420A3A600E2,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsData_mD1FDD951BEAC505088F40E7E96A4EC37DAA1B921,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsData_m0D314D4CE1CB0481957905FA9EF5DD8E08B33A0D,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsReserved_mB14EE3EC1D539A9C9C35652B3BE120E97A5F96EB,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsSupported_m274DFE67596E1C20FE078D43462675E1471E0BC2,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsText_m4BF1ADE56AD743FBE48AD8F85DC2586F980D2164,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsToken_m181B2CFD2601FFEADB4BCCE758E0DAD9F158C2DE,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ReadBytes_m46DBC394CDA9BD532F01E92390A9DACBF2B92C2D,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ReadBytes_m0F3560D4CFEC3F4E2F01A6AB4F8CF19BD301BDD3,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ReadBytesAsync_m16D0BD06630A2B20F3A59700665CA0A5328E736F,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ReadBytesAsync_m4A84EB6C86EDB4A46262CF97D20258137A7A5FCF,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Reverse_mCA306A4C0B475114A5217669A945EF1978C67EC4,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_SplitHeaderValue_m01EE9C6D8102DB88FA9937B7EC8AF3CF4004B67A,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToByteArray_mD20CDE09F09523A7A54F5489D9A42326FDCC561D,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToExtensionString_m1A4430E0A6CD59742837AB50919C7008230C0E39,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToList_m9BDDD3572D9F684B673305CC3500C90A26FFC7AA,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToUInt16_mF963B0EB55BD1C1B24DF8FCB77070EB8AE1908CB,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToUInt64_m8875AD2574EA9F437EB6499A833BBB87A8A07CB4,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_TryCreateWebSocketUri_m3944CC7FC2F49F59DE819B4E18877FE84C9CB42A,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_TryGetUTF8DecodedString_m07ED759625CB1683806215FDC4EA050F3F41E062,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_TryGetUTF8EncodedBytes_m9CA6A8F40D22157CE7096C2C0C90EEAF35D18CB5,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Unquote_m430FB83166985AC03BF2BD8283CAEDBE21F66602,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Upgrades_m672B32A90886DEF7430195058BDD9B85E0F955F9,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_WriteBytes_mC92631F017F5C1290CB883FBAECE35156B2ED308,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsEnclosedIn_mC4AD7A067E1070CDD25235847375ECA14647D8F3,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsHostOrder_m82CC168E47DC97D8E61D971E684A97AC7F748AF2,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsNullOrEmpty_mEB4CFD1426630064F5435FADB81B48B26161793E,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_IsPredefinedScheme_m274F02E1D5677ED6112F71F299C6562BA2AFD5C0,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_MaybeUri_m9183FD39642FB589BE7AE769C8146A6A19386B11,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_SubArray_m3EE8DD51103A031A9967A3A29B4DC6FD7CA28DE3,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_SubArray_mB3FCB98AA131E275E724E51383CE9D8195958137,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToHostOrder_mFC9D0D1F9DC21733829F38C35C3B819DD90A9CBC,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToString_mD66D5879C7E7EE3E24A2A07C0F86A5698D75D07C,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToUri_m36BFA928AEF6A14DB081BFEF75143B68665F2F88,
	U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62__ctor_mD0F6C1B1B13A73C577FF6F04AA2EF11BF7C895F4,
	U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_IDisposable_Dispose_m13B0D952747EF8A3E7E734C9FE4A9B6E4E7ABB05,
	U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m303D4DF29F9239C771BA12712ABC5AE8D4CBE13C,
	U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_Reset_m48C8B244C0B1648685FBD53A216EE5004BD258D3,
	U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_get_Current_mFB4110BE04322EC29F00261128176708E13C50A0,
	U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m20D4ECB95C3C084914B659CF5E262D913718F626,
	U3CSplitHeaderValueU3Ed__62_tDFC5A9A096BE9CEE5CC5C29DB3CA3930081A861C_CustomAttributesCacheGenerator_U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerable_GetEnumerator_mA5D81BE65336ED51944619BE3CAC8FA0FF695C95,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_add_OnClose_mA8DE5F76FD7433CDAD33CC2D8812D7001B750A66,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_remove_OnClose_m2AE23DB4E743F675A549A0E71E3558FF7A2F3DBB,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_add_OnError_m362EE37535D15C3B900BAD60249F10707EA6729E,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_remove_OnError_mD2F241B0D17251C16DF1A40E533E5F20AB19E651,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_add_OnMessage_mFB8C1D5D98E13FCCB1653ABE35704A532B5988A6,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_remove_OnMessage_m455B56B3B9360D764DD5A5BB635ECAFF6EB54D07,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_add_OnOpen_m40F1299BBE18CDD3880D5111CAFD058C2D02A874,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_remove_OnOpen_m30290C709FC3C8F8DDC36076F97F87E04D7DDD2C,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket_U3CopenU3Eb__146_0_mD7B092980016BCF1CBBC93B23FDD9B18AC9CA2F6,
	U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__25__ctor_m7290C117BBBD824041032B6A783AE4AF32226A43,
	U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__25_System_IDisposable_Dispose_mFDCDAD41B87D1F55DAE4A68FE3767B22A28C5F54,
	U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mA7B801C8652EAE220C45112BB0883B24421B41B1,
	U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m413CE860B0CE2F51C4F2E31AB83F860327B58A54,
	U3CGetEnumeratorU3Ed__25_t14423A6754B24BBF73071EC39F4190DE6886427E_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m2CCE50DF3B959BC0B0D59F873D0DB781453FE120,
	U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__86__ctor_mFDDD212B6E6A6FFAB4FD0CA6D9541929DE36E06A,
	U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__86_System_IDisposable_Dispose_mDA8B0BA2DB9C194B5932895D77173C70CF63D01D,
	U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__86_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m3EA1B7BF4D4B7576E0EFB264B5919542FB74CD33,
	U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_Reset_m79C1B3397472E5979B328686C1169427742B14B4,
	U3CGetEnumeratorU3Ed__86_t70E65D270ACD3FB473901EF91FA815CFEA06663A_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_get_Current_mC579C33994AC27CEEAD74CEC09B3D6F517560A20,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_Contains_mF267D8AEC1CA95308565F0B4A52350ABAF786A2E____anyOf1,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_SplitHeaderValue_m01EE9C6D8102DB88FA9937B7EC8AF3CF4004B67A____separators1,
	Ext_t4BB938D3ADB951E9850315EE88577857944C1927_CustomAttributesCacheGenerator_Ext_ToExtensionString_m1A4430E0A6CD59742837AB50919C7008230C0E39____parameters1,
	WebSocket_t394CF710163C960EE4C3651F707CC820634A5906_CustomAttributesCacheGenerator_WebSocket__ctor_m922DA968DBD07A0529B3BE9CE576188FD0B5C094____protocols1,
	websocketU2Dsharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
