using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Panel_ClientMenu : MonoBehaviour
{
    [Header("Main Main")]
    [SerializeField] private Button btIntroduction;
    [SerializeField] private Button btTutorial;
    [SerializeField] private Button btTesting;

    [Header("Introduction Menu")]
    [SerializeField] private Button btFireType;
    [SerializeField] private Button btFireExtinguisherType;
    [SerializeField] private Button btBack;

    public event EventHandler EHOpenFireType;
    public event EventHandler EHOpenFireExtinguisherType;
    public event EventHandler EHOpenTutorial;
    public event EventHandler EHOpenTesting;

    private void OnEnable()
    {
        Init();
        btIntroduction.onClick.AddListener(delegate { IntroductionMenu(); });
        btTutorial.onClick.AddListener(delegate { Tutorial(); });
        btTesting.onClick.AddListener(delegate { EHTesting(); });

        btFireType.onClick.AddListener(delegate { FireType(); });
        btFireExtinguisherType.onClick.AddListener(delegate { FireExtinguisherType(); });
        btBack.onClick.AddListener(delegate { MainMenu(); });
    }

    private void Init()
    {
        btIntroduction.gameObject.SetActive(true);
        btTutorial.gameObject.SetActive(true);
        btTesting.gameObject.SetActive(true);

        btFireType.gameObject.SetActive(false);
        btFireExtinguisherType.gameObject.SetActive(false);
        btBack.gameObject.SetActive(false);
    }

    public void IntroductionMenu()
    {
        btIntroduction.gameObject.SetActive(false);
        btTutorial.gameObject.SetActive(false);
        btTesting.gameObject.SetActive(false);

        btFireType.gameObject.SetActive(true);
        btFireExtinguisherType.gameObject.SetActive(true);
        btBack.gameObject.SetActive(true);
    }

    private void MainMenu()
    {
        btIntroduction.gameObject.SetActive(true);
        btTutorial.gameObject.SetActive(true);
        btTesting.gameObject.SetActive(true);

        btFireType.gameObject.SetActive(false);
        btFireExtinguisherType.gameObject.SetActive(false);
        btBack.gameObject.SetActive(false);
    }

    private void FireType()
    {
        EHOpenFireType(this, EventArgs.Empty);
    }

    private void FireExtinguisherType()
    {
        EHOpenFireExtinguisherType(this, EventArgs.Empty);
    }

    private void Tutorial()
    {
        EHOpenTutorial(this, EventArgs.Empty);
    }

    private void EHTesting()
    {
        EHOpenTesting(this, EventArgs.Empty);
    }
}
