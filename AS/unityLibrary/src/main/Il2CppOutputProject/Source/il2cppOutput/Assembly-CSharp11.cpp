﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`1<UnityEngine.Rendering.AsyncGPUReadbackRequest>
struct Action_1_t542D0A6987D7110F66453B06D83AFE1D24208526;
// System.Action`1<System.Object>
struct Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC;
// System.Action`1<LocomotionTeleport/AimData>
struct Action_1_t92198758A8B104AAC58CEBF1192A89AFDC67850B;
// System.Action`3<UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion>
struct Action_3_tA4F94900F976E775A483E76A95AB2A443C164EC2;
// System.Action`4<System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Quaternion>>
struct Action_4_t9426C491AA37FE7E7E24FF3395FFAC5A60AAC07F;
// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo>
struct AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,OVRPassthroughLayer/PassthroughMeshInstance>
struct Dictionary_2_t60275C105022D515270F8E8181130D8B1020CB74;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task>
struct Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8;
// System.Func`1<System.Threading.Tasks.Task/ContingentProperties>
struct Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B;
// System.Collections.Generic.LinkedListNode`1<System.Single>
struct LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3;
// System.Collections.Generic.LinkedList`1<System.Single>
struct LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t458734AF850139150AB40DFB2B5D1BCE23178235;
// System.Collections.Generic.List`1<UnityEngine.UI.Button>
struct List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Collections.Generic.List`1<GPTT.Data.API/FireExtinguisher>
struct List_1_tE9A16AED8ACE3B4F11793B96E0914894721D031D;
// System.Collections.Generic.List`1<Loom/DelayedQueueItem>
struct List_1_t30C4A5C6EF7CF16239D523CD578A04147253133E;
// System.Collections.Generic.List`1<OVRPassthroughLayer/DeferredPassthroughMeshAddition>
struct List_1_t3B0820C8464D06CB2C98E3D7C75B224815E79323;
// System.Predicate`1<System.Object>
struct Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB;
// System.Predicate`1<System.Threading.Tasks.Task>
struct Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// UnityEngine.Events.UnityEvent`1<System.Byte[]>
struct UnityEvent_1_tF65557EFA1FC9CFCD88E86FB4B11D5367C18D14B;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t32063FE815890FF672DF76288FAC4ABE089B899F;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_tA38C18F6D88709B30F107C43E0669847172879D5;
// UnityEngine.Resolution[]
struct ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.Texture[]
struct TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150;
// OculusSampleFramework.TrainCarBase[]
struct TrainCarBaseU5BU5D_tEA9FA40EA871BECB324EAE0EA10A02C42C3FA5AA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E;
// UnityEngine.WebCamTexture[]
struct WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// OculusSampleFramework.ButtonController
struct ButtonController_t8B501737B40B4D8F465F215CAC449B627A8EF000;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasGroup
struct CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// System.Threading.ContextCallback
struct ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// System.EventArgs
struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA;
// System.EventHandler
struct EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B;
// System.Exception
struct Exception_t;
// System.Threading.ExecutionContext
struct ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.Gradient
struct Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tAE063F84A60E1058FCA4E3EA9F555D3462641F7D;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// System.Security.Principal.IPrincipal
struct IPrincipal_t850ACE1F48327B64F266DD2C6FD8C5F56E4889E2;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.UI.InputField
struct InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0;
// OculusSampleFramework.InteractableTool
struct InteractableTool_t3492A009F902935C1291A66C2D9E53DDB96B296C;
// System.Threading.InternalThread
struct InternalThread_t12B78B27503AE19E9122E212419A66843BF746EB;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// System.LocalDataStoreHolder
struct LocalDataStoreHolder_tF51C9DD735A89132114AE47E3EB51C11D0FED146;
// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t6CC44D0584911B6A6C6823115F858FC34AB4A80A;
// LocomotionController
struct LocomotionController_tCA625CD0A5F52DDB33ECB2FAC435471C0C079719;
// LocomotionTeleport
struct LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MeshRenderer
struct MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.MulticastDelegate
struct MulticastDelegate_t;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// OVRCameraRig
struct OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517;
// OVROverlay
struct OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7;
// OVRPassthroughLayer
struct OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB;
// Obi.ObiCollider
struct ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9;
// Obi.ObiColliderHandle
struct ObiColliderHandle_tE9A3F736A020E2D6B47BF185EEA23173D8B723F8;
// Obi.ObiCollisionMaterial
struct ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F;
// Obi.ObiDistanceField
struct ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482;
// Obi.ObiRigidbodyBase
struct ObiRigidbodyBase_tA95EE97A5497D66D571ABA3D52491F363471272A;
// Obi.ObiShapeTracker
struct ObiShapeTracker_tFA2C80FDB58BB42DD59DBA239AE4A916FAAC7730;
// Obi.ObiSolver
struct ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// Panel_AllInOne
struct Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D;
// Panel_DataRecord
struct Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57;
// Panel_Login
struct Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41;
// Panel_Menu
struct Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E;
// PassthroughStyler
struct PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// OculusSampleFramework.Pose
struct Pose_t8062E0F070422627AB6D1693718F8D8C38DBA742;
// UnityEngine.UI.RawImage
struct RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.RenderTexture
struct RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// RuntimeRopeGenerator
struct RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488;
// RuntimeRopeGeneratorUse
struct RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// OculusSampleFramework.SelectionCylinder
struct SelectionCylinder_tD00E5523E40CEE87C221B093CB293B99706AE041;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t6B7334CE017AF595535507519400AC02D688DC3C;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1;
// ServerManagerUI
struct ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.Threading.Tasks.StackGuard
struct StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D;
// StartMenu
struct StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3;
// System.String
struct String_t;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069;
// TargetProjectionMatrix
struct TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F;
// System.Threading.Tasks.Task
struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60;
// System.Threading.Tasks.TaskFactory
struct TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D;
// TeleportAimHandler
struct TeleportAimHandler_tC1C6D09FC478B420E7C892ECCB6E625F537D3BBD;
// TeleportDestination
struct TeleportDestination_t1DCC2BDB675ACE76D915FE7251B4CB6C487811DA;
// TeleportInputHandler
struct TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D;
// TeleportOrientationHandler
struct TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D;
// TeleportSupport
struct TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121;
// TeleportTargetHandler
struct TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273;
// TeleportTransitionBlink
struct TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977;
// TeleportTransitionWarp
struct TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// TextureEncoder
struct TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA;
// System.Threading.Thread
struct Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414;
// UnityEngine.UI.Toggle
struct Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E;
// OculusSampleFramework.TrainButtonVisualController
struct TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151;
// OculusSampleFramework.TrainCrossingController
struct TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37;
// OculusSampleFramework.TrainLocomotive
struct TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331;
// OculusSampleFramework.TrainTrack
struct TrainTrack_t7BDC9E56629804928D04D3C4F7FF03D0F3409424;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEventByteArray
struct UnityEventByteArray_tD5103CBD7F77D5C7683025D9BCE91819B3E37F16;
// UnityEventWebcamTexture
struct UnityEventWebcamTexture_t73539047423B07150669F124020AA49AF9260D66;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// System.Threading.WaitCallback
struct WaitCallback_t82C85517E973DCC6390AFB0BC3C2276F3328A319;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// UnityEngine.WebCamTexture
struct WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62;
// WebcamManager
struct WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973;
// OculusSampleFramework.WindmillBladesController
struct WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// LocomotionTeleport/AimData
struct AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// PassthroughStyler/<FadeToDefaultPassthrough>d__20
struct U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732;
// RemoteLoopbackManager/PacketLatencyPair
struct PacketLatencyPair_tB00B8B1B83E2801DA568D38A169B959F1FBCE25D;
// RemoteLoopbackManager/SimulatedLatencySettings
struct SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B;
// RuntimeRopeGenerator/<MakeRope>d__2
struct U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7;
// RuntimeRopeGeneratorUse/<Start>d__2
struct U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA;
// ServerManagerUI/<Init>d__4
struct U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2;
// StartMenu/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t27B782281E85B4B9B9C194DA1B73C3535D5CC46D;
// TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10
struct U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD;
// System.Threading.Tasks.Task/ContingentProperties
struct ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0;
// TeleportInputHandler/<TeleportAimCoroutine>d__6
struct U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7;
// TeleportInputHandler/<TeleportReadyCoroutine>d__5
struct U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848;
// TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7
struct U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5;
// TeleportTargetHandler/<TargetAimCoroutine>d__7
struct U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF;
// TeleportTransitionBlink/<BlinkCoroutine>d__4
struct U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A;
// TeleportTransitionWarp/<DoWarp>d__3
struct U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935;
// TextureEncoder/<>c__DisplayClass52_0
struct U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0;
// TextureEncoder/<EncodeBytes>d__52
struct U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3;
// TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49
struct U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5;
// TextureEncoder/<SenderCOR>d__51
struct U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E;
// OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26
struct U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4;
// OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15
struct U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79;
// OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34
struct U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723;
// GPTT.Data.Util/Machine
struct Machine_tD44F48548F85571C82CE7DF59CEDDFBB4DADF082;
// DKP.VR.VRIO/<Haptics>d__0
struct U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF;
// WebcamManager/<RestartWebcam>d__8
struct U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06;
// WebcamManager/<initAndWaitForWebCamTexture>d__34
struct U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D;
// OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17
struct U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D;
// OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18
struct U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9;

IL2CPP_EXTERN_C RuntimeClass* Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OVRInput_t3C43263053F2510BDF75588657A71B87702767FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD;
IL2CPP_EXTERN_C String_t* _stringLiteral3F55171AED7650EF04E95A77E6E60F9E11DEBDCE;
IL2CPP_EXTERN_C String_t* _stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncGPUReadbackRequest_GetData_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mA5D2215016D842E07C2BFC1C268B22D24E9FDDBD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mF00E8C45202F9019CB44F46C3EEC111CCE67B9E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisYieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mEC62BDE3C6F344BF4321FC8C373E9E160AFE0277_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedListNode_1_get_Value_m9BA93F2A1CA19916497B4D273C24114F10B694BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_AddLast_m4DD91ACDAC1A8D61399FC65E1A38601D1B95D6C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_RemoveFirst_m7DDA60CBD00B0A5D88BA887D4F2575C038E32756_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1__ctor_m1D81D06D2B8525B2D4C1EE8CA5D423E890F97E4F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_get_Count_m3002AA56275E54B52256A83C262FCD1F1A39E9BC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_get_First_m60113671B7FFA2E250B7EB5C490DF4B5D5C10F52_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeArray_1_ToArray_m13D8B79ED269C75D10BE015EC81844C30603B838_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_mB9EAE3168E00BA12AA7E1233A4A0007FD12BB9E7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ServerManagerUI_EventBackToMenu_m00165244790CC8711D3DCDD50583E651FA4C2B1F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ServerManagerUI_EventLogout_mE50C0004EB72022F33618CA19F5192B54105BEA3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ServerManagerUI_EventOpenAllInOne_m4B5B93774BAB0D0F99E375B317B5297C6423B02F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ServerManagerUI_EventOpenDataRecord_m3C42AE3C3F077C2D88079C129DEB95601D606A1A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ServerManagerUI_EventOpenMenu_m862BD21B1C41A02A3EC56B6C849F2CD6FD5A192C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_Reset_m1221B7705FF6866AB85D415E8D00A92F1B40B0C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m6E21661229B022076FA60AF6C49A1980E56609E1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDoWarpU3Ed__3_System_Collections_IEnumerator_Reset_mE692557C58D90117915B599FA91A88FC7D238200_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_Reset_m5CC4264F59DB6C3D9A916AE8A5FEADDAF55A15A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_IEnumerator_Reset_m67B84B18ED37EF94A7E28DC9D942BF28EE813ED8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CInitU3Ed__4_System_Collections_IEnumerator_Reset_mAD7BB77764B97952953510C1E0453EE78593E9D4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_Reset_mDB45CD8EC190F294BD562D4090ADB533E5EBF9F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CMakeRopeU3Ed__2_System_Collections_IEnumerator_Reset_m21ABBBA4A71228DA2105ABF24C336ED84D28D1C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_Reset_mD0C04D5F54AAD444CE77C15B853BE79F5504160F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_Reset_m765AE58387A0C89F17588BFF91919AEC413081FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CResetPositionU3Ed__26_System_Collections_IEnumerator_Reset_mFAB28539B7B0AF0B783CD738C41D9DC51A1E8853_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_Reset_mA3A746F18872D6F97B3CC5A4977A1F692BA7E8EA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSenderCORU3Ed__51_System_Collections_IEnumerator_Reset_m8DF573C5E0A33310CB292F0C8B8E3A2AB46F659A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_Reset_mE5B9500E4605106C99B8718D3105A15B0CB73A87_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_mF62F9BBE198D60E90417C026611B81DD3461E8DC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m2BC8EDEB99F6EEEA6DB6EE8F703C99E5CC6A7301_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mE894BDC51B100595A10489704983F8922185CCF8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m12F830ADD072802D84BE4B3231F43A12FA4F8E73_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass52_0_U3CEncodeBytesU3Eb__0_m4C4569E76CED51732F353063F2BB0EDFCCC3A091_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m8957A7F5F8D1D9C630D84F24A7C2B66642CA9B95_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_Reset_m70A89E669D02A74E6DAABC101346075B8F0F3F25_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_Reset_m40B8DC7783ACD7CDF9D80B46263092CF7CCD2C02_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_Invoke_m470057E7D3B20C1038D24F229A27EF3DEDD8E60D_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 ;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
struct WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E;
struct WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Collections.Generic.LinkedListNode`1<System.Single>
struct LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::list
	LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::next
	LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * ___next_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::prev
	LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * ___prev_2;
	// T System.Collections.Generic.LinkedListNode`1::item
	float ___item_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3, ___list_0)); }
	inline LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * get_list_0() const { return ___list_0; }
	inline LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3, ___next_1)); }
	inline LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * get_next_1() const { return ___next_1; }
	inline LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___next_1), (void*)value);
	}

	inline static int32_t get_offset_of_prev_2() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3, ___prev_2)); }
	inline LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * get_prev_2() const { return ___prev_2; }
	inline LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 ** get_address_of_prev_2() { return &___prev_2; }
	inline void set_prev_2(LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * value)
	{
		___prev_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prev_2), (void*)value);
	}

	inline static int32_t get_offset_of_item_3() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3, ___item_3)); }
	inline float get_item_3() const { return ___item_3; }
	inline float* get_address_of_item_3() { return &___item_3; }
	inline void set_item_3(float value)
	{
		___item_3 = value;
	}
};


// System.Collections.Generic.LinkedList`1<System.Single>
struct LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::head
	LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * ___head_0;
	// System.Int32 System.Collections.Generic.LinkedList`1::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Generic.LinkedList`1::version
	int32_t ___version_2;
	// System.Object System.Collections.Generic.LinkedList`1::_syncRoot
	RuntimeObject * ____syncRoot_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::_siInfo
	SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ____siInfo_4;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27, ___head_0)); }
	inline LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * get_head_0() const { return ___head_0; }
	inline LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___head_0), (void*)value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}

	inline static int32_t get_offset_of__siInfo_4() { return static_cast<int32_t>(offsetof(LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27, ____siInfo_4)); }
	inline SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * get__siInfo_4() const { return ____siInfo_4; }
	inline SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 ** get_address_of__siInfo_4() { return &____siInfo_4; }
	inline void set__siInfo_4(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * value)
	{
		____siInfo_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____siInfo_4), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____items_1)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct CriticalFinalizerObject_tA3367C832FFE7434EB3C15C7136AF25524150997  : public RuntimeObject
{
public:

public:
};


// System.EventArgs
struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA  : public RuntimeObject
{
public:

public:
};

struct EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA_StaticFields, ___Empty_0)); }
	inline EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_tBCAACA538A5195B6D6C8DFCC3524A2A4A67FD8BA * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_0), (void*)value);
	}
};


// RuntimeRopeGenerator
struct RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488  : public RuntimeObject
{
public:
	// Obi.ObiSolver RuntimeRopeGenerator::solver
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * ___solver_0;
	// System.Int32 RuntimeRopeGenerator::pinnedParticle
	int32_t ___pinnedParticle_1;

public:
	inline static int32_t get_offset_of_solver_0() { return static_cast<int32_t>(offsetof(RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488, ___solver_0)); }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * get_solver_0() const { return ___solver_0; }
	inline ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 ** get_address_of_solver_0() { return &___solver_0; }
	inline void set_solver_0(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634 * value)
	{
		___solver_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___solver_0), (void*)value);
	}

	inline static int32_t get_offset_of_pinnedParticle_1() { return static_cast<int32_t>(offsetof(RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488, ___pinnedParticle_1)); }
	inline int32_t get_pinnedParticle_1() const { return ___pinnedParticle_1; }
	inline int32_t* get_address_of_pinnedParticle_1() { return &___pinnedParticle_1; }
	inline void set_pinnedParticle_1(int32_t value)
	{
		___pinnedParticle_1 = value;
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// RemoteLoopbackManager/PacketLatencyPair
struct PacketLatencyPair_tB00B8B1B83E2801DA568D38A169B959F1FBCE25D  : public RuntimeObject
{
public:
	// System.Byte[] RemoteLoopbackManager/PacketLatencyPair::PacketData
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___PacketData_0;
	// System.Single RemoteLoopbackManager/PacketLatencyPair::FakeLatency
	float ___FakeLatency_1;

public:
	inline static int32_t get_offset_of_PacketData_0() { return static_cast<int32_t>(offsetof(PacketLatencyPair_tB00B8B1B83E2801DA568D38A169B959F1FBCE25D, ___PacketData_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_PacketData_0() const { return ___PacketData_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_PacketData_0() { return &___PacketData_0; }
	inline void set_PacketData_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___PacketData_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PacketData_0), (void*)value);
	}

	inline static int32_t get_offset_of_FakeLatency_1() { return static_cast<int32_t>(offsetof(PacketLatencyPair_tB00B8B1B83E2801DA568D38A169B959F1FBCE25D, ___FakeLatency_1)); }
	inline float get_FakeLatency_1() const { return ___FakeLatency_1; }
	inline float* get_address_of_FakeLatency_1() { return &___FakeLatency_1; }
	inline void set_FakeLatency_1(float value)
	{
		___FakeLatency_1 = value;
	}
};


// RemoteLoopbackManager/SimulatedLatencySettings
struct SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B  : public RuntimeObject
{
public:
	// System.Single RemoteLoopbackManager/SimulatedLatencySettings::FakeLatencyMax
	float ___FakeLatencyMax_0;
	// System.Single RemoteLoopbackManager/SimulatedLatencySettings::FakeLatencyMin
	float ___FakeLatencyMin_1;
	// System.Single RemoteLoopbackManager/SimulatedLatencySettings::LatencyWeight
	float ___LatencyWeight_2;
	// System.Int32 RemoteLoopbackManager/SimulatedLatencySettings::MaxSamples
	int32_t ___MaxSamples_3;
	// System.Single RemoteLoopbackManager/SimulatedLatencySettings::AverageWindow
	float ___AverageWindow_4;
	// System.Single RemoteLoopbackManager/SimulatedLatencySettings::LatencySum
	float ___LatencySum_5;
	// System.Collections.Generic.LinkedList`1<System.Single> RemoteLoopbackManager/SimulatedLatencySettings::LatencyValues
	LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * ___LatencyValues_6;

public:
	inline static int32_t get_offset_of_FakeLatencyMax_0() { return static_cast<int32_t>(offsetof(SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B, ___FakeLatencyMax_0)); }
	inline float get_FakeLatencyMax_0() const { return ___FakeLatencyMax_0; }
	inline float* get_address_of_FakeLatencyMax_0() { return &___FakeLatencyMax_0; }
	inline void set_FakeLatencyMax_0(float value)
	{
		___FakeLatencyMax_0 = value;
	}

	inline static int32_t get_offset_of_FakeLatencyMin_1() { return static_cast<int32_t>(offsetof(SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B, ___FakeLatencyMin_1)); }
	inline float get_FakeLatencyMin_1() const { return ___FakeLatencyMin_1; }
	inline float* get_address_of_FakeLatencyMin_1() { return &___FakeLatencyMin_1; }
	inline void set_FakeLatencyMin_1(float value)
	{
		___FakeLatencyMin_1 = value;
	}

	inline static int32_t get_offset_of_LatencyWeight_2() { return static_cast<int32_t>(offsetof(SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B, ___LatencyWeight_2)); }
	inline float get_LatencyWeight_2() const { return ___LatencyWeight_2; }
	inline float* get_address_of_LatencyWeight_2() { return &___LatencyWeight_2; }
	inline void set_LatencyWeight_2(float value)
	{
		___LatencyWeight_2 = value;
	}

	inline static int32_t get_offset_of_MaxSamples_3() { return static_cast<int32_t>(offsetof(SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B, ___MaxSamples_3)); }
	inline int32_t get_MaxSamples_3() const { return ___MaxSamples_3; }
	inline int32_t* get_address_of_MaxSamples_3() { return &___MaxSamples_3; }
	inline void set_MaxSamples_3(int32_t value)
	{
		___MaxSamples_3 = value;
	}

	inline static int32_t get_offset_of_AverageWindow_4() { return static_cast<int32_t>(offsetof(SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B, ___AverageWindow_4)); }
	inline float get_AverageWindow_4() const { return ___AverageWindow_4; }
	inline float* get_address_of_AverageWindow_4() { return &___AverageWindow_4; }
	inline void set_AverageWindow_4(float value)
	{
		___AverageWindow_4 = value;
	}

	inline static int32_t get_offset_of_LatencySum_5() { return static_cast<int32_t>(offsetof(SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B, ___LatencySum_5)); }
	inline float get_LatencySum_5() const { return ___LatencySum_5; }
	inline float* get_address_of_LatencySum_5() { return &___LatencySum_5; }
	inline void set_LatencySum_5(float value)
	{
		___LatencySum_5 = value;
	}

	inline static int32_t get_offset_of_LatencyValues_6() { return static_cast<int32_t>(offsetof(SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B, ___LatencyValues_6)); }
	inline LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * get_LatencyValues_6() const { return ___LatencyValues_6; }
	inline LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 ** get_address_of_LatencyValues_6() { return &___LatencyValues_6; }
	inline void set_LatencyValues_6(LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * value)
	{
		___LatencyValues_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LatencyValues_6), (void*)value);
	}
};


// RuntimeRopeGeneratorUse/<Start>d__2
struct U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA  : public RuntimeObject
{
public:
	// System.Int32 RuntimeRopeGeneratorUse/<Start>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RuntimeRopeGeneratorUse/<Start>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RuntimeRopeGeneratorUse RuntimeRopeGeneratorUse/<Start>d__2::<>4__this
	RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA, ___U3CU3E4__this_2)); }
	inline RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ServerManagerUI/<Init>d__4
struct U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2  : public RuntimeObject
{
public:
	// System.Int32 ServerManagerUI/<Init>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ServerManagerUI/<Init>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ServerManagerUI ServerManagerUI/<Init>d__4::<>4__this
	ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2, ___U3CU3E4__this_2)); }
	inline ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// StartMenu/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t27B782281E85B4B9B9C194DA1B73C3535D5CC46D  : public RuntimeObject
{
public:
	// System.Int32 StartMenu/<>c__DisplayClass3_0::sceneIndex
	int32_t ___sceneIndex_0;
	// StartMenu StartMenu/<>c__DisplayClass3_0::<>4__this
	StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_sceneIndex_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t27B782281E85B4B9B9C194DA1B73C3535D5CC46D, ___sceneIndex_0)); }
	inline int32_t get_sceneIndex_0() const { return ___sceneIndex_0; }
	inline int32_t* get_address_of_sceneIndex_0() { return &___sceneIndex_0; }
	inline void set_sceneIndex_0(int32_t value)
	{
		___sceneIndex_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t27B782281E85B4B9B9C194DA1B73C3535D5CC46D, ___U3CU3E4__this_1)); }
	inline StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// TeleportInputHandler/<TeleportReadyCoroutine>d__5
struct U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848  : public RuntimeObject
{
public:
	// System.Int32 TeleportInputHandler/<TeleportReadyCoroutine>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TeleportInputHandler/<TeleportReadyCoroutine>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TeleportInputHandler TeleportInputHandler/<TeleportReadyCoroutine>d__5::<>4__this
	TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848, ___U3CU3E4__this_2)); }
	inline TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7
struct U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5  : public RuntimeObject
{
public:
	// System.Int32 TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TeleportOrientationHandler TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::<>4__this
	TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5, ___U3CU3E4__this_2)); }
	inline TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// TeleportTransitionBlink/<BlinkCoroutine>d__4
struct U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A  : public RuntimeObject
{
public:
	// System.Int32 TeleportTransitionBlink/<BlinkCoroutine>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TeleportTransitionBlink/<BlinkCoroutine>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TeleportTransitionBlink TeleportTransitionBlink/<BlinkCoroutine>d__4::<>4__this
	TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977 * ___U3CU3E4__this_2;
	// System.Single TeleportTransitionBlink/<BlinkCoroutine>d__4::<elapsedTime>5__1
	float ___U3CelapsedTimeU3E5__1_3;
	// System.Single TeleportTransitionBlink/<BlinkCoroutine>d__4::<teleportTime>5__2
	float ___U3CteleportTimeU3E5__2_4;
	// System.Boolean TeleportTransitionBlink/<BlinkCoroutine>d__4::<teleported>5__3
	bool ___U3CteleportedU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A, ___U3CU3E4__this_2)); }
	inline TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A, ___U3CelapsedTimeU3E5__1_3)); }
	inline float get_U3CelapsedTimeU3E5__1_3() const { return ___U3CelapsedTimeU3E5__1_3; }
	inline float* get_address_of_U3CelapsedTimeU3E5__1_3() { return &___U3CelapsedTimeU3E5__1_3; }
	inline void set_U3CelapsedTimeU3E5__1_3(float value)
	{
		___U3CelapsedTimeU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CteleportTimeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A, ___U3CteleportTimeU3E5__2_4)); }
	inline float get_U3CteleportTimeU3E5__2_4() const { return ___U3CteleportTimeU3E5__2_4; }
	inline float* get_address_of_U3CteleportTimeU3E5__2_4() { return &___U3CteleportTimeU3E5__2_4; }
	inline void set_U3CteleportTimeU3E5__2_4(float value)
	{
		___U3CteleportTimeU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CteleportedU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A, ___U3CteleportedU3E5__3_5)); }
	inline bool get_U3CteleportedU3E5__3_5() const { return ___U3CteleportedU3E5__3_5; }
	inline bool* get_address_of_U3CteleportedU3E5__3_5() { return &___U3CteleportedU3E5__3_5; }
	inline void set_U3CteleportedU3E5__3_5(bool value)
	{
		___U3CteleportedU3E5__3_5 = value;
	}
};


// TextureEncoder/<>c__DisplayClass52_0
struct U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0  : public RuntimeObject
{
public:
	// System.Boolean TextureEncoder/<>c__DisplayClass52_0::AsyncEncoding
	bool ___AsyncEncoding_0;
	// TextureEncoder TextureEncoder/<>c__DisplayClass52_0::<>4__this
	TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_AsyncEncoding_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0, ___AsyncEncoding_0)); }
	inline bool get_AsyncEncoding_0() const { return ___AsyncEncoding_0; }
	inline bool* get_address_of_AsyncEncoding_0() { return &___AsyncEncoding_0; }
	inline void set_AsyncEncoding_0(bool value)
	{
		___AsyncEncoding_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0, ___U3CU3E4__this_1)); }
	inline TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// TextureEncoder/<EncodeBytes>d__52
struct U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3  : public RuntimeObject
{
public:
	// System.Int32 TextureEncoder/<EncodeBytes>d__52::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TextureEncoder/<EncodeBytes>d__52::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TextureEncoder TextureEncoder/<EncodeBytes>d__52::<>4__this
	TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * ___U3CU3E4__this_2;
	// System.Int32 TextureEncoder/<EncodeBytes>d__52::<_length>5__1
	int32_t ___U3C_lengthU3E5__1_3;
	// System.Int32 TextureEncoder/<EncodeBytes>d__52::<_offset>5__2
	int32_t ___U3C_offsetU3E5__2_4;
	// System.Byte[] TextureEncoder/<EncodeBytes>d__52::<_meta_label>5__3
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___U3C_meta_labelU3E5__3_5;
	// System.Byte[] TextureEncoder/<EncodeBytes>d__52::<_meta_id>5__4
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___U3C_meta_idU3E5__4_6;
	// System.Byte[] TextureEncoder/<EncodeBytes>d__52::<_meta_length>5__5
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___U3C_meta_lengthU3E5__5_7;
	// System.Int32 TextureEncoder/<EncodeBytes>d__52::<chunks>5__6
	int32_t ___U3CchunksU3E5__6_8;
	// TextureEncoder/<>c__DisplayClass52_0 TextureEncoder/<EncodeBytes>d__52::<>8__7
	U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * ___U3CU3E8__7_9;
	// System.Single TextureEncoder/<EncodeBytes>d__52::<diff>5__8
	float ___U3CdiffU3E5__8_10;
	// System.Int32 TextureEncoder/<EncodeBytes>d__52::<i>5__9
	int32_t ___U3CiU3E5__9_11;
	// System.Int32 TextureEncoder/<EncodeBytes>d__52::<SendByteLength>5__10
	int32_t ___U3CSendByteLengthU3E5__10_12;
	// System.Byte[] TextureEncoder/<EncodeBytes>d__52::<_meta_offset>5__11
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___U3C_meta_offsetU3E5__11_13;
	// System.Byte[] TextureEncoder/<EncodeBytes>d__52::<SendByte>5__12
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___U3CSendByteU3E5__12_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3CU3E4__this_2)); }
	inline TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3C_lengthU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3C_lengthU3E5__1_3)); }
	inline int32_t get_U3C_lengthU3E5__1_3() const { return ___U3C_lengthU3E5__1_3; }
	inline int32_t* get_address_of_U3C_lengthU3E5__1_3() { return &___U3C_lengthU3E5__1_3; }
	inline void set_U3C_lengthU3E5__1_3(int32_t value)
	{
		___U3C_lengthU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3C_offsetU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3C_offsetU3E5__2_4)); }
	inline int32_t get_U3C_offsetU3E5__2_4() const { return ___U3C_offsetU3E5__2_4; }
	inline int32_t* get_address_of_U3C_offsetU3E5__2_4() { return &___U3C_offsetU3E5__2_4; }
	inline void set_U3C_offsetU3E5__2_4(int32_t value)
	{
		___U3C_offsetU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3C_meta_labelU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3C_meta_labelU3E5__3_5)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_U3C_meta_labelU3E5__3_5() const { return ___U3C_meta_labelU3E5__3_5; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_U3C_meta_labelU3E5__3_5() { return &___U3C_meta_labelU3E5__3_5; }
	inline void set_U3C_meta_labelU3E5__3_5(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___U3C_meta_labelU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3C_meta_labelU3E5__3_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3C_meta_idU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3C_meta_idU3E5__4_6)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_U3C_meta_idU3E5__4_6() const { return ___U3C_meta_idU3E5__4_6; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_U3C_meta_idU3E5__4_6() { return &___U3C_meta_idU3E5__4_6; }
	inline void set_U3C_meta_idU3E5__4_6(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___U3C_meta_idU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3C_meta_idU3E5__4_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3C_meta_lengthU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3C_meta_lengthU3E5__5_7)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_U3C_meta_lengthU3E5__5_7() const { return ___U3C_meta_lengthU3E5__5_7; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_U3C_meta_lengthU3E5__5_7() { return &___U3C_meta_lengthU3E5__5_7; }
	inline void set_U3C_meta_lengthU3E5__5_7(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___U3C_meta_lengthU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3C_meta_lengthU3E5__5_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CchunksU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3CchunksU3E5__6_8)); }
	inline int32_t get_U3CchunksU3E5__6_8() const { return ___U3CchunksU3E5__6_8; }
	inline int32_t* get_address_of_U3CchunksU3E5__6_8() { return &___U3CchunksU3E5__6_8; }
	inline void set_U3CchunksU3E5__6_8(int32_t value)
	{
		___U3CchunksU3E5__6_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3E8__7_9() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3CU3E8__7_9)); }
	inline U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * get_U3CU3E8__7_9() const { return ___U3CU3E8__7_9; }
	inline U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 ** get_address_of_U3CU3E8__7_9() { return &___U3CU3E8__7_9; }
	inline void set_U3CU3E8__7_9(U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * value)
	{
		___U3CU3E8__7_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E8__7_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdiffU3E5__8_10() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3CdiffU3E5__8_10)); }
	inline float get_U3CdiffU3E5__8_10() const { return ___U3CdiffU3E5__8_10; }
	inline float* get_address_of_U3CdiffU3E5__8_10() { return &___U3CdiffU3E5__8_10; }
	inline void set_U3CdiffU3E5__8_10(float value)
	{
		___U3CdiffU3E5__8_10 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__9_11() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3CiU3E5__9_11)); }
	inline int32_t get_U3CiU3E5__9_11() const { return ___U3CiU3E5__9_11; }
	inline int32_t* get_address_of_U3CiU3E5__9_11() { return &___U3CiU3E5__9_11; }
	inline void set_U3CiU3E5__9_11(int32_t value)
	{
		___U3CiU3E5__9_11 = value;
	}

	inline static int32_t get_offset_of_U3CSendByteLengthU3E5__10_12() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3CSendByteLengthU3E5__10_12)); }
	inline int32_t get_U3CSendByteLengthU3E5__10_12() const { return ___U3CSendByteLengthU3E5__10_12; }
	inline int32_t* get_address_of_U3CSendByteLengthU3E5__10_12() { return &___U3CSendByteLengthU3E5__10_12; }
	inline void set_U3CSendByteLengthU3E5__10_12(int32_t value)
	{
		___U3CSendByteLengthU3E5__10_12 = value;
	}

	inline static int32_t get_offset_of_U3C_meta_offsetU3E5__11_13() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3C_meta_offsetU3E5__11_13)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_U3C_meta_offsetU3E5__11_13() const { return ___U3C_meta_offsetU3E5__11_13; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_U3C_meta_offsetU3E5__11_13() { return &___U3C_meta_offsetU3E5__11_13; }
	inline void set_U3C_meta_offsetU3E5__11_13(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___U3C_meta_offsetU3E5__11_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3C_meta_offsetU3E5__11_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSendByteU3E5__12_14() { return static_cast<int32_t>(offsetof(U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3, ___U3CSendByteU3E5__12_14)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_U3CSendByteU3E5__12_14() const { return ___U3CSendByteU3E5__12_14; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_U3CSendByteU3E5__12_14() { return &___U3CSendByteU3E5__12_14; }
	inline void set_U3CSendByteU3E5__12_14(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___U3CSendByteU3E5__12_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSendByteU3E5__12_14), (void*)value);
	}
};


// TextureEncoder/<SenderCOR>d__51
struct U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E  : public RuntimeObject
{
public:
	// System.Int32 TextureEncoder/<SenderCOR>d__51::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TextureEncoder/<SenderCOR>d__51::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TextureEncoder TextureEncoder/<SenderCOR>d__51::<>4__this
	TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E, ___U3CU3E4__this_2)); }
	inline TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26
struct U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4  : public RuntimeObject
{
public:
	// System.Int32 OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// OculusSampleFramework.TrainButtonVisualController OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::<>4__this
	TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151 * ___U3CU3E4__this_2;
	// System.Single OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::<startTime>5__1
	float ___U3CstartTimeU3E5__1_3;
	// System.Single OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::<endTime>5__2
	float ___U3CendTimeU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4, ___U3CU3E4__this_2)); }
	inline TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4, ___U3CstartTimeU3E5__1_3)); }
	inline float get_U3CstartTimeU3E5__1_3() const { return ___U3CstartTimeU3E5__1_3; }
	inline float* get_address_of_U3CstartTimeU3E5__1_3() { return &___U3CstartTimeU3E5__1_3; }
	inline void set_U3CstartTimeU3E5__1_3(float value)
	{
		___U3CstartTimeU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CendTimeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4, ___U3CendTimeU3E5__2_4)); }
	inline float get_U3CendTimeU3E5__2_4() const { return ___U3CendTimeU3E5__2_4; }
	inline float* get_address_of_U3CendTimeU3E5__2_4() { return &___U3CendTimeU3E5__2_4; }
	inline void set_U3CendTimeU3E5__2_4(float value)
	{
		___U3CendTimeU3E5__2_4 = value;
	}
};


// GPTT.Data.Util/Machine
struct Machine_tD44F48548F85571C82CE7DF59CEDDFBB4DADF082  : public RuntimeObject
{
public:
	// System.String GPTT.Data.Util/Machine::MachineToken
	String_t* ___MachineToken_0;
	// System.String GPTT.Data.Util/Machine::MachineKey
	String_t* ___MachineKey_1;
	// System.String GPTT.Data.Util/Machine::MachineIV
	String_t* ___MachineIV_2;

public:
	inline static int32_t get_offset_of_MachineToken_0() { return static_cast<int32_t>(offsetof(Machine_tD44F48548F85571C82CE7DF59CEDDFBB4DADF082, ___MachineToken_0)); }
	inline String_t* get_MachineToken_0() const { return ___MachineToken_0; }
	inline String_t** get_address_of_MachineToken_0() { return &___MachineToken_0; }
	inline void set_MachineToken_0(String_t* value)
	{
		___MachineToken_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MachineToken_0), (void*)value);
	}

	inline static int32_t get_offset_of_MachineKey_1() { return static_cast<int32_t>(offsetof(Machine_tD44F48548F85571C82CE7DF59CEDDFBB4DADF082, ___MachineKey_1)); }
	inline String_t* get_MachineKey_1() const { return ___MachineKey_1; }
	inline String_t** get_address_of_MachineKey_1() { return &___MachineKey_1; }
	inline void set_MachineKey_1(String_t* value)
	{
		___MachineKey_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MachineKey_1), (void*)value);
	}

	inline static int32_t get_offset_of_MachineIV_2() { return static_cast<int32_t>(offsetof(Machine_tD44F48548F85571C82CE7DF59CEDDFBB4DADF082, ___MachineIV_2)); }
	inline String_t* get_MachineIV_2() const { return ___MachineIV_2; }
	inline String_t** get_address_of_MachineIV_2() { return &___MachineIV_2; }
	inline void set_MachineIV_2(String_t* value)
	{
		___MachineIV_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MachineIV_2), (void*)value);
	}
};


// WebcamManager/<RestartWebcam>d__8
struct U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06  : public RuntimeObject
{
public:
	// System.Int32 WebcamManager/<RestartWebcam>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WebcamManager/<RestartWebcam>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// WebcamManager WebcamManager/<RestartWebcam>d__8::<>4__this
	WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06, ___U3CU3E4__this_2)); }
	inline WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// WebcamManager/<initAndWaitForWebCamTexture>d__34
struct U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D  : public RuntimeObject
{
public:
	// System.Int32 WebcamManager/<initAndWaitForWebCamTexture>d__34::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object WebcamManager/<initAndWaitForWebCamTexture>d__34::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// WebcamManager WebcamManager/<initAndWaitForWebCamTexture>d__34::<>4__this
	WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * ___U3CU3E4__this_2;
	// System.Int32 WebcamManager/<initAndWaitForWebCamTexture>d__34::<i>5__1
	int32_t ___U3CiU3E5__1_3;
	// System.Int32 WebcamManager/<initAndWaitForWebCamTexture>d__34::<i>5__2
	int32_t ___U3CiU3E5__2_4;
	// UnityEngine.GameObject[] WebcamManager/<initAndWaitForWebCamTexture>d__34::<>s__3
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___U3CU3Es__3_5;
	// System.Int32 WebcamManager/<initAndWaitForWebCamTexture>d__34::<>s__4
	int32_t ___U3CU3Es__4_6;
	// UnityEngine.GameObject WebcamManager/<initAndWaitForWebCamTexture>d__34::<obj>5__5
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CobjU3E5__5_7;
	// System.Int32 WebcamManager/<initAndWaitForWebCamTexture>d__34::<initFrameCount>5__6
	int32_t ___U3CinitFrameCountU3E5__6_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D, ___U3CU3E4__this_2)); }
	inline WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D, ___U3CiU3E5__1_3)); }
	inline int32_t get_U3CiU3E5__1_3() const { return ___U3CiU3E5__1_3; }
	inline int32_t* get_address_of_U3CiU3E5__1_3() { return &___U3CiU3E5__1_3; }
	inline void set_U3CiU3E5__1_3(int32_t value)
	{
		___U3CiU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D, ___U3CiU3E5__2_4)); }
	inline int32_t get_U3CiU3E5__2_4() const { return ___U3CiU3E5__2_4; }
	inline int32_t* get_address_of_U3CiU3E5__2_4() { return &___U3CiU3E5__2_4; }
	inline void set_U3CiU3E5__2_4(int32_t value)
	{
		___U3CiU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Es__3_5() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D, ___U3CU3Es__3_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_U3CU3Es__3_5() const { return ___U3CU3Es__3_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_U3CU3Es__3_5() { return &___U3CU3Es__3_5; }
	inline void set_U3CU3Es__3_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___U3CU3Es__3_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Es__3_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Es__4_6() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D, ___U3CU3Es__4_6)); }
	inline int32_t get_U3CU3Es__4_6() const { return ___U3CU3Es__4_6; }
	inline int32_t* get_address_of_U3CU3Es__4_6() { return &___U3CU3Es__4_6; }
	inline void set_U3CU3Es__4_6(int32_t value)
	{
		___U3CU3Es__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CobjU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D, ___U3CobjU3E5__5_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CobjU3E5__5_7() const { return ___U3CobjU3E5__5_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CobjU3E5__5_7() { return &___U3CobjU3E5__5_7; }
	inline void set_U3CobjU3E5__5_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CobjU3E5__5_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CobjU3E5__5_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CinitFrameCountU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D, ___U3CinitFrameCountU3E5__6_8)); }
	inline int32_t get_U3CinitFrameCountU3E5__6_8() const { return ___U3CinitFrameCountU3E5__6_8; }
	inline int32_t* get_address_of_U3CinitFrameCountU3E5__6_8() { return &___U3CinitFrameCountU3E5__6_8; }
	inline void set_U3CinitFrameCountU3E5__6_8(int32_t value)
	{
		___U3CinitFrameCountU3E5__6_8 = value;
	}
};


// OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17
struct U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D  : public RuntimeObject
{
public:
	// System.Int32 OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::goalSpeed
	float ___goalSpeed_2;
	// OculusSampleFramework.WindmillBladesController OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::<>4__this
	WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * ___U3CU3E4__this_3;
	// System.Single OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::<totalTime>5__1
	float ___U3CtotalTimeU3E5__1_4;
	// System.Single OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::<startSpeed>5__2
	float ___U3CstartSpeedU3E5__2_5;
	// System.Single OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::<diffSpeeds>5__3
	float ___U3CdiffSpeedsU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_goalSpeed_2() { return static_cast<int32_t>(offsetof(U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D, ___goalSpeed_2)); }
	inline float get_goalSpeed_2() const { return ___goalSpeed_2; }
	inline float* get_address_of_goalSpeed_2() { return &___goalSpeed_2; }
	inline void set_goalSpeed_2(float value)
	{
		___goalSpeed_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D, ___U3CU3E4__this_3)); }
	inline WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtotalTimeU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D, ___U3CtotalTimeU3E5__1_4)); }
	inline float get_U3CtotalTimeU3E5__1_4() const { return ___U3CtotalTimeU3E5__1_4; }
	inline float* get_address_of_U3CtotalTimeU3E5__1_4() { return &___U3CtotalTimeU3E5__1_4; }
	inline void set_U3CtotalTimeU3E5__1_4(float value)
	{
		___U3CtotalTimeU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CstartSpeedU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D, ___U3CstartSpeedU3E5__2_5)); }
	inline float get_U3CstartSpeedU3E5__2_5() const { return ___U3CstartSpeedU3E5__2_5; }
	inline float* get_address_of_U3CstartSpeedU3E5__2_5() { return &___U3CstartSpeedU3E5__2_5; }
	inline void set_U3CstartSpeedU3E5__2_5(float value)
	{
		___U3CstartSpeedU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CdiffSpeedsU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D, ___U3CdiffSpeedsU3E5__3_6)); }
	inline float get_U3CdiffSpeedsU3E5__3_6() const { return ___U3CdiffSpeedsU3E5__3_6; }
	inline float* get_address_of_U3CdiffSpeedsU3E5__3_6() { return &___U3CdiffSpeedsU3E5__3_6; }
	inline void set_U3CdiffSpeedsU3E5__3_6(float value)
	{
		___U3CdiffSpeedsU3E5__3_6 = value;
	}
};


// OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18
struct U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9  : public RuntimeObject
{
public:
	// System.Int32 OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.AudioClip OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::initial
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___initial_2;
	// UnityEngine.AudioClip OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::clip
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___clip_3;
	// System.Single OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::timeDelayAfterInitial
	float ___timeDelayAfterInitial_4;
	// OculusSampleFramework.WindmillBladesController OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::<>4__this
	WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * ___U3CU3E4__this_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_initial_2() { return static_cast<int32_t>(offsetof(U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9, ___initial_2)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_initial_2() const { return ___initial_2; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_initial_2() { return &___initial_2; }
	inline void set_initial_2(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___initial_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___initial_2), (void*)value);
	}

	inline static int32_t get_offset_of_clip_3() { return static_cast<int32_t>(offsetof(U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9, ___clip_3)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_clip_3() const { return ___clip_3; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_clip_3() { return &___clip_3; }
	inline void set_clip_3(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___clip_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clip_3), (void*)value);
	}

	inline static int32_t get_offset_of_timeDelayAfterInitial_4() { return static_cast<int32_t>(offsetof(U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9, ___timeDelayAfterInitial_4)); }
	inline float get_timeDelayAfterInitial_4() const { return ___timeDelayAfterInitial_4; }
	inline float* get_address_of_timeDelayAfterInitial_4() { return &___timeDelayAfterInitial_4; }
	inline void set_timeDelayAfterInitial_4(float value)
	{
		___timeDelayAfterInitial_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9, ___U3CU3E4__this_5)); }
	inline WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Byte[]>
struct UnityEvent_1_tF65557EFA1FC9CFCD88E86FB4B11D5367C18D14B  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tF65557EFA1FC9CFCD88E86FB4B11D5367C18D14B, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateMachine_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34, ___m_defaultContextAction_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_defaultContextAction_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.InteropServices.GCHandle
struct GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Mathf
struct Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194__padding[1];
	};

public:
};

struct Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_StaticFields
{
public:
	// System.Single UnityEngine.Mathf::Epsilon
	float ___Epsilon_0;

public:
	inline static int32_t get_offset_of_Epsilon_0() { return static_cast<int32_t>(offsetof(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_StaticFields, ___Epsilon_0)); }
	inline float get_Epsilon_0() const { return ___Epsilon_0; }
	inline float* get_address_of_Epsilon_0() { return &___Epsilon_0; }
	inline void set_Epsilon_0(float value)
	{
		___Epsilon_0 = value;
	}
};


// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C, ___m_task_0)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_marshaled_pinvoke
{
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_marshaled_com
{
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_0;
};

// System.Threading.Thread
struct Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414  : public CriticalFinalizerObject_tA3367C832FFE7434EB3C15C7136AF25524150997
{
public:
	// System.Threading.InternalThread System.Threading.Thread::internal_thread
	InternalThread_t12B78B27503AE19E9122E212419A66843BF746EB * ___internal_thread_6;
	// System.Object System.Threading.Thread::m_ThreadStartArg
	RuntimeObject * ___m_ThreadStartArg_7;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject * ___pending_exception_8;
	// System.Security.Principal.IPrincipal System.Threading.Thread::principal
	RuntimeObject* ___principal_9;
	// System.Int32 System.Threading.Thread::principal_version
	int32_t ___principal_version_10;
	// System.MulticastDelegate System.Threading.Thread::m_Delegate
	MulticastDelegate_t * ___m_Delegate_12;
	// System.Threading.ExecutionContext System.Threading.Thread::m_ExecutionContext
	ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * ___m_ExecutionContext_13;
	// System.Boolean System.Threading.Thread::m_ExecutionContextBelongsToOuterScope
	bool ___m_ExecutionContextBelongsToOuterScope_14;

public:
	inline static int32_t get_offset_of_internal_thread_6() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___internal_thread_6)); }
	inline InternalThread_t12B78B27503AE19E9122E212419A66843BF746EB * get_internal_thread_6() const { return ___internal_thread_6; }
	inline InternalThread_t12B78B27503AE19E9122E212419A66843BF746EB ** get_address_of_internal_thread_6() { return &___internal_thread_6; }
	inline void set_internal_thread_6(InternalThread_t12B78B27503AE19E9122E212419A66843BF746EB * value)
	{
		___internal_thread_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___internal_thread_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_ThreadStartArg_7() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___m_ThreadStartArg_7)); }
	inline RuntimeObject * get_m_ThreadStartArg_7() const { return ___m_ThreadStartArg_7; }
	inline RuntimeObject ** get_address_of_m_ThreadStartArg_7() { return &___m_ThreadStartArg_7; }
	inline void set_m_ThreadStartArg_7(RuntimeObject * value)
	{
		___m_ThreadStartArg_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ThreadStartArg_7), (void*)value);
	}

	inline static int32_t get_offset_of_pending_exception_8() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___pending_exception_8)); }
	inline RuntimeObject * get_pending_exception_8() const { return ___pending_exception_8; }
	inline RuntimeObject ** get_address_of_pending_exception_8() { return &___pending_exception_8; }
	inline void set_pending_exception_8(RuntimeObject * value)
	{
		___pending_exception_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pending_exception_8), (void*)value);
	}

	inline static int32_t get_offset_of_principal_9() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___principal_9)); }
	inline RuntimeObject* get_principal_9() const { return ___principal_9; }
	inline RuntimeObject** get_address_of_principal_9() { return &___principal_9; }
	inline void set_principal_9(RuntimeObject* value)
	{
		___principal_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___principal_9), (void*)value);
	}

	inline static int32_t get_offset_of_principal_version_10() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___principal_version_10)); }
	inline int32_t get_principal_version_10() const { return ___principal_version_10; }
	inline int32_t* get_address_of_principal_version_10() { return &___principal_version_10; }
	inline void set_principal_version_10(int32_t value)
	{
		___principal_version_10 = value;
	}

	inline static int32_t get_offset_of_m_Delegate_12() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___m_Delegate_12)); }
	inline MulticastDelegate_t * get_m_Delegate_12() const { return ___m_Delegate_12; }
	inline MulticastDelegate_t ** get_address_of_m_Delegate_12() { return &___m_Delegate_12; }
	inline void set_m_Delegate_12(MulticastDelegate_t * value)
	{
		___m_Delegate_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Delegate_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_ExecutionContext_13() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___m_ExecutionContext_13)); }
	inline ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * get_m_ExecutionContext_13() const { return ___m_ExecutionContext_13; }
	inline ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 ** get_address_of_m_ExecutionContext_13() { return &___m_ExecutionContext_13; }
	inline void set_m_ExecutionContext_13(ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * value)
	{
		___m_ExecutionContext_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExecutionContext_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_ExecutionContextBelongsToOuterScope_14() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___m_ExecutionContextBelongsToOuterScope_14)); }
	inline bool get_m_ExecutionContextBelongsToOuterScope_14() const { return ___m_ExecutionContextBelongsToOuterScope_14; }
	inline bool* get_address_of_m_ExecutionContextBelongsToOuterScope_14() { return &___m_ExecutionContextBelongsToOuterScope_14; }
	inline void set_m_ExecutionContextBelongsToOuterScope_14(bool value)
	{
		___m_ExecutionContextBelongsToOuterScope_14 = value;
	}
};

struct Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_StaticFields
{
public:
	// System.LocalDataStoreMgr System.Threading.Thread::s_LocalDataStoreMgr
	LocalDataStoreMgr_t6CC44D0584911B6A6C6823115F858FC34AB4A80A * ___s_LocalDataStoreMgr_0;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentCulture
	AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * ___s_asyncLocalCurrentCulture_4;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentUICulture
	AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * ___s_asyncLocalCurrentUICulture_5;

public:
	inline static int32_t get_offset_of_s_LocalDataStoreMgr_0() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_StaticFields, ___s_LocalDataStoreMgr_0)); }
	inline LocalDataStoreMgr_t6CC44D0584911B6A6C6823115F858FC34AB4A80A * get_s_LocalDataStoreMgr_0() const { return ___s_LocalDataStoreMgr_0; }
	inline LocalDataStoreMgr_t6CC44D0584911B6A6C6823115F858FC34AB4A80A ** get_address_of_s_LocalDataStoreMgr_0() { return &___s_LocalDataStoreMgr_0; }
	inline void set_s_LocalDataStoreMgr_0(LocalDataStoreMgr_t6CC44D0584911B6A6C6823115F858FC34AB4A80A * value)
	{
		___s_LocalDataStoreMgr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_LocalDataStoreMgr_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentCulture_4() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_StaticFields, ___s_asyncLocalCurrentCulture_4)); }
	inline AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * get_s_asyncLocalCurrentCulture_4() const { return ___s_asyncLocalCurrentCulture_4; }
	inline AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 ** get_address_of_s_asyncLocalCurrentCulture_4() { return &___s_asyncLocalCurrentCulture_4; }
	inline void set_s_asyncLocalCurrentCulture_4(AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * value)
	{
		___s_asyncLocalCurrentCulture_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_asyncLocalCurrentCulture_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentUICulture_5() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_StaticFields, ___s_asyncLocalCurrentUICulture_5)); }
	inline AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * get_s_asyncLocalCurrentUICulture_5() const { return ___s_asyncLocalCurrentUICulture_5; }
	inline AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 ** get_address_of_s_asyncLocalCurrentUICulture_5() { return &___s_asyncLocalCurrentUICulture_5; }
	inline void set_s_asyncLocalCurrentUICulture_5(AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * value)
	{
		___s_asyncLocalCurrentUICulture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_asyncLocalCurrentUICulture_5), (void*)value);
	}
};

struct Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_ThreadStaticFields
{
public:
	// System.LocalDataStoreHolder System.Threading.Thread::s_LocalDataStore
	LocalDataStoreHolder_tF51C9DD735A89132114AE47E3EB51C11D0FED146 * ___s_LocalDataStore_1;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentCulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___m_CurrentCulture_2;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentUICulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___m_CurrentUICulture_3;
	// System.Threading.Thread System.Threading.Thread::current_thread
	Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * ___current_thread_11;

public:
	inline static int32_t get_offset_of_s_LocalDataStore_1() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_ThreadStaticFields, ___s_LocalDataStore_1)); }
	inline LocalDataStoreHolder_tF51C9DD735A89132114AE47E3EB51C11D0FED146 * get_s_LocalDataStore_1() const { return ___s_LocalDataStore_1; }
	inline LocalDataStoreHolder_tF51C9DD735A89132114AE47E3EB51C11D0FED146 ** get_address_of_s_LocalDataStore_1() { return &___s_LocalDataStore_1; }
	inline void set_s_LocalDataStore_1(LocalDataStoreHolder_tF51C9DD735A89132114AE47E3EB51C11D0FED146 * value)
	{
		___s_LocalDataStore_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_LocalDataStore_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentCulture_2() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_ThreadStaticFields, ___m_CurrentCulture_2)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_m_CurrentCulture_2() const { return ___m_CurrentCulture_2; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_m_CurrentCulture_2() { return &___m_CurrentCulture_2; }
	inline void set_m_CurrentCulture_2(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___m_CurrentCulture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentCulture_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentUICulture_3() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_ThreadStaticFields, ___m_CurrentUICulture_3)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_m_CurrentUICulture_3() const { return ___m_CurrentUICulture_3; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_m_CurrentUICulture_3() { return &___m_CurrentUICulture_3; }
	inline void set_m_CurrentUICulture_3(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___m_CurrentUICulture_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentUICulture_3), (void*)value);
	}

	inline static int32_t get_offset_of_current_thread_11() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_ThreadStaticFields, ___current_thread_11)); }
	inline Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * get_current_thread_11() const { return ___current_thread_11; }
	inline Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 ** get_address_of_current_thread_11() { return &___current_thread_11; }
	inline void set_current_thread_11(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * value)
	{
		___current_thread_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_thread_11), (void*)value);
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:

public:
};


// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// System.Runtime.CompilerServices.YieldAwaitable
struct YieldAwaitable_t95CCA9EB9730CADF5A3BEF9845E12FF467F594FA 
{
public:
	union
	{
		struct
		{
		};
		uint8_t YieldAwaitable_t95CCA9EB9730CADF5A3BEF9845E12FF467F594FA__padding[1];
	};

public:
};


// UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/EmissionModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/EmissionModule
struct EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// UnityEngine.ParticleSystem/MainModule
struct MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter
struct YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE 
{
public:
	union
	{
		struct
		{
		};
		uint8_t YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE__padding[1];
	};

public:
};

struct YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_StaticFields
{
public:
	// System.Threading.WaitCallback System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter::s_waitCallbackRunAction
	WaitCallback_t82C85517E973DCC6390AFB0BC3C2276F3328A319 * ___s_waitCallbackRunAction_0;
	// System.Threading.SendOrPostCallback System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter::s_sendOrPostCallbackRunAction
	SendOrPostCallback_t6B7334CE017AF595535507519400AC02D688DC3C * ___s_sendOrPostCallbackRunAction_1;

public:
	inline static int32_t get_offset_of_s_waitCallbackRunAction_0() { return static_cast<int32_t>(offsetof(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_StaticFields, ___s_waitCallbackRunAction_0)); }
	inline WaitCallback_t82C85517E973DCC6390AFB0BC3C2276F3328A319 * get_s_waitCallbackRunAction_0() const { return ___s_waitCallbackRunAction_0; }
	inline WaitCallback_t82C85517E973DCC6390AFB0BC3C2276F3328A319 ** get_address_of_s_waitCallbackRunAction_0() { return &___s_waitCallbackRunAction_0; }
	inline void set_s_waitCallbackRunAction_0(WaitCallback_t82C85517E973DCC6390AFB0BC3C2276F3328A319 * value)
	{
		___s_waitCallbackRunAction_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_waitCallbackRunAction_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_sendOrPostCallbackRunAction_1() { return static_cast<int32_t>(offsetof(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_StaticFields, ___s_sendOrPostCallbackRunAction_1)); }
	inline SendOrPostCallback_t6B7334CE017AF595535507519400AC02D688DC3C * get_s_sendOrPostCallbackRunAction_1() const { return ___s_sendOrPostCallbackRunAction_1; }
	inline SendOrPostCallback_t6B7334CE017AF595535507519400AC02D688DC3C ** get_address_of_s_sendOrPostCallbackRunAction_1() { return &___s_sendOrPostCallbackRunAction_1; }
	inline void set_s_sendOrPostCallbackRunAction_1(SendOrPostCallback_t6B7334CE017AF595535507519400AC02D688DC3C * value)
	{
		___s_sendOrPostCallbackRunAction_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_sendOrPostCallbackRunAction_1), (void*)value);
	}
};


// System.Nullable`1<UnityEngine.Vector3>
struct Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 
{
public:
	// T System.Nullable`1::value
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258, ___value_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_value_0() const { return ___value_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// Unity.Collections.Allocator
struct Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Rendering.AsyncGPUReadbackRequest
struct AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA 
{
public:
	// System.IntPtr UnityEngine.Rendering.AsyncGPUReadbackRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Int32 UnityEngine.Rendering.AsyncGPUReadbackRequest::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};


// System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 
{
public:
	// System.Threading.SynchronizationContext System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_synchronizationContext
	SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * ___m_synchronizationContext_0;
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_coreState
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  ___m_coreState_1;
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.AsyncVoidMethodBuilder::m_task
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_synchronizationContext_0() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6, ___m_synchronizationContext_0)); }
	inline SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * get_m_synchronizationContext_0() const { return ___m_synchronizationContext_0; }
	inline SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 ** get_address_of_m_synchronizationContext_0() { return &___m_synchronizationContext_0; }
	inline void set_m_synchronizationContext_0(SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * value)
	{
		___m_synchronizationContext_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_synchronizationContext_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  value)
	{
		___m_coreState_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6, ___m_task_2)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_m_task_2() const { return ___m_task_2; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6_marshaled_pinvoke
{
	SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_pinvoke ___m_coreState_1;
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_2;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncVoidMethodBuilder
struct AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6_marshaled_com
{
	SynchronizationContext_t17D9365B5E0D30A0910A16FA4351C525232EF069 * ___m_synchronizationContext_0;
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_com ___m_coreState_1;
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_task_2;
};

// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// FMChromaSubsamplingOption
struct FMChromaSubsamplingOption_tD4AB367B5D544756EFD9AC7EFE65AA18AF801AB7 
{
public:
	// System.Int32 FMChromaSubsamplingOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FMChromaSubsamplingOption_tD4AB367B5D544756EFD9AC7EFE65AA18AF801AB7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMTextureType
struct FMTextureType_tD8905B500353D275CC876BE50E8AA183EB3D403C 
{
public:
	// System.Int32 FMTextureType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FMTextureType_tD8905B500353D275CC876BE50E8AA183EB3D403C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DKP.VR.Hand
struct Hand_t08DA34DAFEBA6D7322BE5EB9A048C97EE6E3F52F 
{
public:
	// System.Int32 DKP.VR.Hand::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Hand_t08DA34DAFEBA6D7322BE5EB9A048C97EE6E3F52F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RaycastHit
struct RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Point_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_UV_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// System.Threading.Tasks.Task
struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60  : public RuntimeObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_taskId
	int32_t ___m_taskId_4;
	// System.Object System.Threading.Tasks.Task::m_action
	RuntimeObject * ___m_action_5;
	// System.Object System.Threading.Tasks.Task::m_stateObject
	RuntimeObject * ___m_stateObject_6;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.Task::m_taskScheduler
	TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * ___m_taskScheduler_7;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::m_parent
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___m_parent_8;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_stateFlags
	int32_t ___m_stateFlags_9;
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_continuationObject
	RuntimeObject * ___m_continuationObject_28;
	// System.Threading.Tasks.Task/ContingentProperties modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_contingentProperties
	ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * ___m_contingentProperties_33;

public:
	inline static int32_t get_offset_of_m_taskId_4() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskId_4)); }
	inline int32_t get_m_taskId_4() const { return ___m_taskId_4; }
	inline int32_t* get_address_of_m_taskId_4() { return &___m_taskId_4; }
	inline void set_m_taskId_4(int32_t value)
	{
		___m_taskId_4 = value;
	}

	inline static int32_t get_offset_of_m_action_5() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_action_5)); }
	inline RuntimeObject * get_m_action_5() const { return ___m_action_5; }
	inline RuntimeObject ** get_address_of_m_action_5() { return &___m_action_5; }
	inline void set_m_action_5(RuntimeObject * value)
	{
		___m_action_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_action_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateObject_6() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateObject_6)); }
	inline RuntimeObject * get_m_stateObject_6() const { return ___m_stateObject_6; }
	inline RuntimeObject ** get_address_of_m_stateObject_6() { return &___m_stateObject_6; }
	inline void set_m_stateObject_6(RuntimeObject * value)
	{
		___m_stateObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_taskScheduler_7() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_taskScheduler_7)); }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * get_m_taskScheduler_7() const { return ___m_taskScheduler_7; }
	inline TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D ** get_address_of_m_taskScheduler_7() { return &___m_taskScheduler_7; }
	inline void set_m_taskScheduler_7(TaskScheduler_t74FBEEEDBDD5E0088FF0EEC18F45CD866B098D5D * value)
	{
		___m_taskScheduler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_taskScheduler_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_parent_8() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_parent_8)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_m_parent_8() const { return ___m_parent_8; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_m_parent_8() { return &___m_parent_8; }
	inline void set_m_parent_8(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___m_parent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parent_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateFlags_9() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_stateFlags_9)); }
	inline int32_t get_m_stateFlags_9() const { return ___m_stateFlags_9; }
	inline int32_t* get_address_of_m_stateFlags_9() { return &___m_stateFlags_9; }
	inline void set_m_stateFlags_9(int32_t value)
	{
		___m_stateFlags_9 = value;
	}

	inline static int32_t get_offset_of_m_continuationObject_28() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_continuationObject_28)); }
	inline RuntimeObject * get_m_continuationObject_28() const { return ___m_continuationObject_28; }
	inline RuntimeObject ** get_address_of_m_continuationObject_28() { return &___m_continuationObject_28; }
	inline void set_m_continuationObject_28(RuntimeObject * value)
	{
		___m_continuationObject_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_continuationObject_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_contingentProperties_33() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60, ___m_contingentProperties_33)); }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * get_m_contingentProperties_33() const { return ___m_contingentProperties_33; }
	inline ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 ** get_address_of_m_contingentProperties_33() { return &___m_contingentProperties_33; }
	inline void set_m_contingentProperties_33(ContingentProperties_t1E249C737B8B8644ED1D60EEFA101D326B199EA0 * value)
	{
		___m_contingentProperties_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_contingentProperties_33), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields
{
public:
	// System.Int32 System.Threading.Tasks.Task::s_taskIdCounter
	int32_t ___s_taskIdCounter_2;
	// System.Threading.Tasks.TaskFactory System.Threading.Tasks.Task::s_factory
	TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * ___s_factory_3;
	// System.Object System.Threading.Tasks.Task::s_taskCompletionSentinel
	RuntimeObject * ___s_taskCompletionSentinel_29;
	// System.Boolean System.Threading.Tasks.Task::s_asyncDebuggingEnabled
	bool ___s_asyncDebuggingEnabled_30;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_currentActiveTasks
	Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * ___s_currentActiveTasks_31;
	// System.Object System.Threading.Tasks.Task::s_activeTasksLock
	RuntimeObject * ___s_activeTasksLock_32;
	// System.Action`1<System.Object> System.Threading.Tasks.Task::s_taskCancelCallback
	Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * ___s_taskCancelCallback_34;
	// System.Func`1<System.Threading.Tasks.Task/ContingentProperties> System.Threading.Tasks.Task::s_createContingentProperties
	Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * ___s_createContingentProperties_35;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::s_completedTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___s_completedTask_36;
	// System.Predicate`1<System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_IsExceptionObservedByParentPredicate
	Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * ___s_IsExceptionObservedByParentPredicate_37;
	// System.Threading.ContextCallback System.Threading.Tasks.Task::s_ecCallback
	ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * ___s_ecCallback_38;
	// System.Predicate`1<System.Object> System.Threading.Tasks.Task::s_IsTaskContinuationNullPredicate
	Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * ___s_IsTaskContinuationNullPredicate_39;

public:
	inline static int32_t get_offset_of_s_taskIdCounter_2() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskIdCounter_2)); }
	inline int32_t get_s_taskIdCounter_2() const { return ___s_taskIdCounter_2; }
	inline int32_t* get_address_of_s_taskIdCounter_2() { return &___s_taskIdCounter_2; }
	inline void set_s_taskIdCounter_2(int32_t value)
	{
		___s_taskIdCounter_2 = value;
	}

	inline static int32_t get_offset_of_s_factory_3() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_factory_3)); }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * get_s_factory_3() const { return ___s_factory_3; }
	inline TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B ** get_address_of_s_factory_3() { return &___s_factory_3; }
	inline void set_s_factory_3(TaskFactory_t22D999A05A967C31A4B5FFBD08864809BF35EA3B * value)
	{
		___s_factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_factory_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCompletionSentinel_29() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCompletionSentinel_29)); }
	inline RuntimeObject * get_s_taskCompletionSentinel_29() const { return ___s_taskCompletionSentinel_29; }
	inline RuntimeObject ** get_address_of_s_taskCompletionSentinel_29() { return &___s_taskCompletionSentinel_29; }
	inline void set_s_taskCompletionSentinel_29(RuntimeObject * value)
	{
		___s_taskCompletionSentinel_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCompletionSentinel_29), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncDebuggingEnabled_30() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_asyncDebuggingEnabled_30)); }
	inline bool get_s_asyncDebuggingEnabled_30() const { return ___s_asyncDebuggingEnabled_30; }
	inline bool* get_address_of_s_asyncDebuggingEnabled_30() { return &___s_asyncDebuggingEnabled_30; }
	inline void set_s_asyncDebuggingEnabled_30(bool value)
	{
		___s_asyncDebuggingEnabled_30 = value;
	}

	inline static int32_t get_offset_of_s_currentActiveTasks_31() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_currentActiveTasks_31)); }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * get_s_currentActiveTasks_31() const { return ___s_currentActiveTasks_31; }
	inline Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 ** get_address_of_s_currentActiveTasks_31() { return &___s_currentActiveTasks_31; }
	inline void set_s_currentActiveTasks_31(Dictionary_2_tB758E2A2593CD827EFC041BE1F1BB4B68DE1C3E8 * value)
	{
		___s_currentActiveTasks_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_currentActiveTasks_31), (void*)value);
	}

	inline static int32_t get_offset_of_s_activeTasksLock_32() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_activeTasksLock_32)); }
	inline RuntimeObject * get_s_activeTasksLock_32() const { return ___s_activeTasksLock_32; }
	inline RuntimeObject ** get_address_of_s_activeTasksLock_32() { return &___s_activeTasksLock_32; }
	inline void set_s_activeTasksLock_32(RuntimeObject * value)
	{
		___s_activeTasksLock_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_activeTasksLock_32), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCancelCallback_34() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_taskCancelCallback_34)); }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * get_s_taskCancelCallback_34() const { return ___s_taskCancelCallback_34; }
	inline Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC ** get_address_of_s_taskCancelCallback_34() { return &___s_taskCancelCallback_34; }
	inline void set_s_taskCancelCallback_34(Action_1_tD9663D9715FAA4E62035CFCF1AD4D094EE7872DC * value)
	{
		___s_taskCancelCallback_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCancelCallback_34), (void*)value);
	}

	inline static int32_t get_offset_of_s_createContingentProperties_35() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_createContingentProperties_35)); }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * get_s_createContingentProperties_35() const { return ___s_createContingentProperties_35; }
	inline Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B ** get_address_of_s_createContingentProperties_35() { return &___s_createContingentProperties_35; }
	inline void set_s_createContingentProperties_35(Func_1_tBCF42601FA307876E83080BE4204110820F8BF3B * value)
	{
		___s_createContingentProperties_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_createContingentProperties_35), (void*)value);
	}

	inline static int32_t get_offset_of_s_completedTask_36() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_completedTask_36)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_s_completedTask_36() const { return ___s_completedTask_36; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_s_completedTask_36() { return &___s_completedTask_36; }
	inline void set_s_completedTask_36(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___s_completedTask_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_completedTask_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsExceptionObservedByParentPredicate_37() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsExceptionObservedByParentPredicate_37)); }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * get_s_IsExceptionObservedByParentPredicate_37() const { return ___s_IsExceptionObservedByParentPredicate_37; }
	inline Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD ** get_address_of_s_IsExceptionObservedByParentPredicate_37() { return &___s_IsExceptionObservedByParentPredicate_37; }
	inline void set_s_IsExceptionObservedByParentPredicate_37(Predicate_1_tC0DBBC8498BD1EE6ABFFAA5628024105FA7D11BD * value)
	{
		___s_IsExceptionObservedByParentPredicate_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsExceptionObservedByParentPredicate_37), (void*)value);
	}

	inline static int32_t get_offset_of_s_ecCallback_38() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_ecCallback_38)); }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * get_s_ecCallback_38() const { return ___s_ecCallback_38; }
	inline ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B ** get_address_of_s_ecCallback_38() { return &___s_ecCallback_38; }
	inline void set_s_ecCallback_38(ContextCallback_t93707E0430F4FF3E15E1FB5A4844BE89C657AE8B * value)
	{
		___s_ecCallback_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ecCallback_38), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsTaskContinuationNullPredicate_39() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_StaticFields, ___s_IsTaskContinuationNullPredicate_39)); }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * get_s_IsTaskContinuationNullPredicate_39() const { return ___s_IsTaskContinuationNullPredicate_39; }
	inline Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB ** get_address_of_s_IsTaskContinuationNullPredicate_39() { return &___s_IsTaskContinuationNullPredicate_39; }
	inline void set_s_IsTaskContinuationNullPredicate_39(Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * value)
	{
		___s_IsTaskContinuationNullPredicate_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsTaskContinuationNullPredicate_39), (void*)value);
	}
};

struct Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::t_currentTask
	Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * ___t_currentTask_0;
	// System.Threading.Tasks.StackGuard System.Threading.Tasks.Task::t_stackGuard
	StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * ___t_stackGuard_1;

public:
	inline static int32_t get_offset_of_t_currentTask_0() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_currentTask_0)); }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * get_t_currentTask_0() const { return ___t_currentTask_0; }
	inline Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 ** get_address_of_t_currentTask_0() { return &___t_currentTask_0; }
	inline void set_t_currentTask_0(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * value)
	{
		___t_currentTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_currentTask_0), (void*)value);
	}

	inline static int32_t get_offset_of_t_stackGuard_1() { return static_cast<int32_t>(offsetof(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_ThreadStaticFields, ___t_stackGuard_1)); }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * get_t_stackGuard_1() const { return ___t_stackGuard_1; }
	inline StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D ** get_address_of_t_stackGuard_1() { return &___t_stackGuard_1; }
	inline void set_t_stackGuard_1(StackGuard_t88E1EE4741AD02CA5FEA04A4EB2CC70F230E0E6D * value)
	{
		___t_stackGuard_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_stackGuard_1), (void*)value);
	}
};


// UnityEngine.TextureFormat
struct TextureFormat_tBED5388A0445FE978F97B41D247275B036407932 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_tBED5388A0445FE978F97B41D247275B036407932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureWrapMode
struct TextureWrapMode_t86DDA8206E4AA784A1218D0DE3C5F6826D7549EB 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapMode_t86DDA8206E4AA784A1218D0DE3C5F6826D7549EB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEventByteArray
struct UnityEventByteArray_tD5103CBD7F77D5C7683025D9BCE91819B3E37F16  : public UnityEvent_1_tF65557EFA1FC9CFCD88E86FB4B11D5367C18D14B
{
public:

public:
};


// UnityEngine.WebCamKind
struct WebCamKind_t27EA4C0DCCBC088C1C35CC9BB08F0BCF22A890F2 
{
public:
	// System.Int32 UnityEngine.WebCamKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WebCamKind_t27EA4C0DCCBC088C1C35CC9BB08F0BCF22A890F2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// LocomotionTeleport/AimCollisionTypes
struct AimCollisionTypes_t19461B238D5F6617A2C06CAA2481276A3B546DAB 
{
public:
	// System.Int32 LocomotionTeleport/AimCollisionTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AimCollisionTypes_t19461B238D5F6617A2C06CAA2481276A3B546DAB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// LocomotionTeleport/States
struct States_t676C5F34A4B2B2A616013FF0711FD0B64B406A30 
{
public:
	// System.Int32 LocomotionTeleport/States::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(States_t676C5F34A4B2B2A616013FF0711FD0B64B406A30, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// LocomotionTeleport/TeleportIntentions
struct TeleportIntentions_t3C1051D0F76523608F90B2657C5AD07AC92A9268 
{
public:
	// System.Int32 LocomotionTeleport/TeleportIntentions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TeleportIntentions_t3C1051D0F76523608F90B2657C5AD07AC92A9268, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRInput/Controller
struct Controller_tEEDA108639533B73057BAE8B95FE21725355C48F 
{
public:
	// System.Int32 OVRInput/Controller::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Controller_tEEDA108639533B73057BAE8B95FE21725355C48F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVROverlay/OverlayType
struct OverlayType_tA4C6425D93366BDC6B22B1B9780ED927FF5A8A91 
{
public:
	// System.Int32 OVROverlay/OverlayType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OverlayType_tA4C6425D93366BDC6B22B1B9780ED927FF5A8A91, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRPassthroughLayer/ColorMapEditorType
struct ColorMapEditorType_t85C46260EEE76B5FF61B48B5DDE649B6A5B6A243 
{
public:
	// System.Int32 OVRPassthroughLayer/ColorMapEditorType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMapEditorType_t85C46260EEE76B5FF61B48B5DDE649B6A5B6A243, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRPassthroughLayer/ProjectionSurfaceType
struct ProjectionSurfaceType_t38ED74A7818DB18AF544F2BEC4EBE79F5474627D 
{
public:
	// System.Int32 OVRPassthroughLayer/ProjectionSurfaceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProjectionSurfaceType_t38ED74A7818DB18AF544F2BEC4EBE79F5474627D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OVRPlugin/InsightPassthroughColorMapType
struct InsightPassthroughColorMapType_tFC50BE8F852456675505EC936C8389280143C042 
{
public:
	// System.Int32 OVRPlugin/InsightPassthroughColorMapType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InsightPassthroughColorMapType_tFC50BE8F852456675505EC936C8389280143C042, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// PassthroughStyler/<FadeToDefaultPassthrough>d__20
struct U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732  : public RuntimeObject
{
public:
	// System.Int32 PassthroughStyler/<FadeToDefaultPassthrough>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PassthroughStyler/<FadeToDefaultPassthrough>d__20::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single PassthroughStyler/<FadeToDefaultPassthrough>d__20::fadeTime
	float ___fadeTime_2;
	// PassthroughStyler PassthroughStyler/<FadeToDefaultPassthrough>d__20::<>4__this
	PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * ___U3CU3E4__this_3;
	// System.Single PassthroughStyler/<FadeToDefaultPassthrough>d__20::<timer>5__1
	float ___U3CtimerU3E5__1_4;
	// System.Single PassthroughStyler/<FadeToDefaultPassthrough>d__20::<brightness>5__2
	float ___U3CbrightnessU3E5__2_5;
	// System.Single PassthroughStyler/<FadeToDefaultPassthrough>d__20::<contrast>5__3
	float ___U3CcontrastU3E5__3_6;
	// System.Single PassthroughStyler/<FadeToDefaultPassthrough>d__20::<posterize>5__4
	float ___U3CposterizeU3E5__4_7;
	// UnityEngine.Color PassthroughStyler/<FadeToDefaultPassthrough>d__20::<edgeCol>5__5
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___U3CedgeColU3E5__5_8;
	// System.Single PassthroughStyler/<FadeToDefaultPassthrough>d__20::<normTimer>5__6
	float ___U3CnormTimerU3E5__6_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_fadeTime_2() { return static_cast<int32_t>(offsetof(U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732, ___fadeTime_2)); }
	inline float get_fadeTime_2() const { return ___fadeTime_2; }
	inline float* get_address_of_fadeTime_2() { return &___fadeTime_2; }
	inline void set_fadeTime_2(float value)
	{
		___fadeTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732, ___U3CU3E4__this_3)); }
	inline PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtimerU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732, ___U3CtimerU3E5__1_4)); }
	inline float get_U3CtimerU3E5__1_4() const { return ___U3CtimerU3E5__1_4; }
	inline float* get_address_of_U3CtimerU3E5__1_4() { return &___U3CtimerU3E5__1_4; }
	inline void set_U3CtimerU3E5__1_4(float value)
	{
		___U3CtimerU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CbrightnessU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732, ___U3CbrightnessU3E5__2_5)); }
	inline float get_U3CbrightnessU3E5__2_5() const { return ___U3CbrightnessU3E5__2_5; }
	inline float* get_address_of_U3CbrightnessU3E5__2_5() { return &___U3CbrightnessU3E5__2_5; }
	inline void set_U3CbrightnessU3E5__2_5(float value)
	{
		___U3CbrightnessU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CcontrastU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732, ___U3CcontrastU3E5__3_6)); }
	inline float get_U3CcontrastU3E5__3_6() const { return ___U3CcontrastU3E5__3_6; }
	inline float* get_address_of_U3CcontrastU3E5__3_6() { return &___U3CcontrastU3E5__3_6; }
	inline void set_U3CcontrastU3E5__3_6(float value)
	{
		___U3CcontrastU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CposterizeU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732, ___U3CposterizeU3E5__4_7)); }
	inline float get_U3CposterizeU3E5__4_7() const { return ___U3CposterizeU3E5__4_7; }
	inline float* get_address_of_U3CposterizeU3E5__4_7() { return &___U3CposterizeU3E5__4_7; }
	inline void set_U3CposterizeU3E5__4_7(float value)
	{
		___U3CposterizeU3E5__4_7 = value;
	}

	inline static int32_t get_offset_of_U3CedgeColU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732, ___U3CedgeColU3E5__5_8)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_U3CedgeColU3E5__5_8() const { return ___U3CedgeColU3E5__5_8; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_U3CedgeColU3E5__5_8() { return &___U3CedgeColU3E5__5_8; }
	inline void set_U3CedgeColU3E5__5_8(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___U3CedgeColU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CnormTimerU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732, ___U3CnormTimerU3E5__6_9)); }
	inline float get_U3CnormTimerU3E5__6_9() const { return ___U3CnormTimerU3E5__6_9; }
	inline float* get_address_of_U3CnormTimerU3E5__6_9() { return &___U3CnormTimerU3E5__6_9; }
	inline void set_U3CnormTimerU3E5__6_9(float value)
	{
		___U3CnormTimerU3E5__6_9 = value;
	}
};


// OculusSampleFramework.PinchStateModule/PinchState
struct PinchState_t0BCA2550BF63C405609CEA222111C1B7A3CC631F 
{
public:
	// System.Int32 OculusSampleFramework.PinchStateModule/PinchState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PinchState_t0BCA2550BF63C405609CEA222111C1B7A3CC631F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RuntimeRopeGenerator/<MakeRope>d__2
struct U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7  : public RuntimeObject
{
public:
	// System.Int32 RuntimeRopeGenerator/<MakeRope>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RuntimeRopeGenerator/<MakeRope>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Transform RuntimeRopeGenerator/<MakeRope>d__2::anchoredTo
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___anchoredTo_2;
	// UnityEngine.Vector3 RuntimeRopeGenerator/<MakeRope>d__2::attachmentOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___attachmentOffset_3;
	// System.Single RuntimeRopeGenerator/<MakeRope>d__2::ropeLength
	float ___ropeLength_4;
	// RuntimeRopeGenerator RuntimeRopeGenerator/<MakeRope>d__2::<>4__this
	RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * ___U3CU3E4__this_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_anchoredTo_2() { return static_cast<int32_t>(offsetof(U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7, ___anchoredTo_2)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_anchoredTo_2() const { return ___anchoredTo_2; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_anchoredTo_2() { return &___anchoredTo_2; }
	inline void set_anchoredTo_2(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___anchoredTo_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anchoredTo_2), (void*)value);
	}

	inline static int32_t get_offset_of_attachmentOffset_3() { return static_cast<int32_t>(offsetof(U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7, ___attachmentOffset_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_attachmentOffset_3() const { return ___attachmentOffset_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_attachmentOffset_3() { return &___attachmentOffset_3; }
	inline void set_attachmentOffset_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___attachmentOffset_3 = value;
	}

	inline static int32_t get_offset_of_ropeLength_4() { return static_cast<int32_t>(offsetof(U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7, ___ropeLength_4)); }
	inline float get_ropeLength_4() const { return ___ropeLength_4; }
	inline float* get_address_of_ropeLength_4() { return &___ropeLength_4; }
	inline void set_ropeLength_4(float value)
	{
		___ropeLength_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7, ___U3CU3E4__this_5)); }
	inline RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_5), (void*)value);
	}
};


// OculusSampleFramework.SelectionCylinder/SelectionState
struct SelectionState_t1ED50EF4F8ED5EDFD14D73EA6BEF2BAC58947A67 
{
public:
	// System.Int32 OculusSampleFramework.SelectionCylinder/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_t1ED50EF4F8ED5EDFD14D73EA6BEF2BAC58947A67, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// SocialPlatformManager/State
struct State_tB735C38369CD8ADDEC8761CE704DC1076C1C7C39 
{
public:
	// System.Int32 SocialPlatformManager/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tB735C38369CD8ADDEC8761CE704DC1076C1C7C39, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10
struct U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD  : public RuntimeObject
{
public:
	// System.Int32 TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TargetProjectionMatrix TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::<>4__this
	TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * ___U3CU3E4__this_2;
	// UnityEngine.Matrix4x4 TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::<rm>5__1
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___U3CrmU3E5__1_3;
	// System.Single TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::<aspect>5__2
	float ___U3CaspectU3E5__2_4;
	// System.Single TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::<matrixY>5__3
	float ___U3CmatrixYU3E5__3_5;
	// System.Single TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::<matrixX>5__4
	float ___U3CmatrixXU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD, ___U3CU3E4__this_2)); }
	inline TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrmU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD, ___U3CrmU3E5__1_3)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_U3CrmU3E5__1_3() const { return ___U3CrmU3E5__1_3; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_U3CrmU3E5__1_3() { return &___U3CrmU3E5__1_3; }
	inline void set_U3CrmU3E5__1_3(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___U3CrmU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CaspectU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD, ___U3CaspectU3E5__2_4)); }
	inline float get_U3CaspectU3E5__2_4() const { return ___U3CaspectU3E5__2_4; }
	inline float* get_address_of_U3CaspectU3E5__2_4() { return &___U3CaspectU3E5__2_4; }
	inline void set_U3CaspectU3E5__2_4(float value)
	{
		___U3CaspectU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixYU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD, ___U3CmatrixYU3E5__3_5)); }
	inline float get_U3CmatrixYU3E5__3_5() const { return ___U3CmatrixYU3E5__3_5; }
	inline float* get_address_of_U3CmatrixYU3E5__3_5() { return &___U3CmatrixYU3E5__3_5; }
	inline void set_U3CmatrixYU3E5__3_5(float value)
	{
		___U3CmatrixYU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixXU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD, ___U3CmatrixXU3E5__4_6)); }
	inline float get_U3CmatrixXU3E5__4_6() const { return ___U3CmatrixXU3E5__4_6; }
	inline float* get_address_of_U3CmatrixXU3E5__4_6() { return &___U3CmatrixXU3E5__4_6; }
	inline void set_U3CmatrixXU3E5__4_6(float value)
	{
		___U3CmatrixXU3E5__4_6 = value;
	}
};


// TeleportInputHandlerTouch/AimCapTouchButtons
struct AimCapTouchButtons_tC12ABE1B4441E9CEA94829FD366F28F5582F51BD 
{
public:
	// System.Int32 TeleportInputHandlerTouch/AimCapTouchButtons::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AimCapTouchButtons_tC12ABE1B4441E9CEA94829FD366F28F5582F51BD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TeleportInputHandlerTouch/InputModes
struct InputModes_t792D4AEF46B007AE05356729430E1BA993E0CD97 
{
public:
	// System.Int32 TeleportInputHandlerTouch/InputModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputModes_t792D4AEF46B007AE05356729430E1BA993E0CD97, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TeleportOrientationHandler/OrientationModes
struct OrientationModes_tC602DDEF74E8C16D61652A79A2755C32F4DE7BA5 
{
public:
	// System.Int32 TeleportOrientationHandler/OrientationModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OrientationModes_tC602DDEF74E8C16D61652A79A2755C32F4DE7BA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// TeleportTargetHandler/<TargetAimCoroutine>d__7
struct U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF  : public RuntimeObject
{
public:
	// System.Int32 TeleportTargetHandler/<TargetAimCoroutine>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TeleportTargetHandler/<TargetAimCoroutine>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TeleportTargetHandler TeleportTargetHandler/<TargetAimCoroutine>d__7::<>4__this
	TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 TeleportTargetHandler/<TargetAimCoroutine>d__7::<current>5__1
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CcurrentU3E5__1_3;
	// System.Int32 TeleportTargetHandler/<TargetAimCoroutine>d__7::<i>5__2
	int32_t ___U3CiU3E5__2_4;
	// UnityEngine.Vector3 TeleportTargetHandler/<TargetAimCoroutine>d__7::<adjustedPoint>5__3
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CadjustedPointU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF, ___U3CU3E4__this_2)); }
	inline TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcurrentU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF, ___U3CcurrentU3E5__1_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CcurrentU3E5__1_3() const { return ___U3CcurrentU3E5__1_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CcurrentU3E5__1_3() { return &___U3CcurrentU3E5__1_3; }
	inline void set_U3CcurrentU3E5__1_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CcurrentU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF, ___U3CiU3E5__2_4)); }
	inline int32_t get_U3CiU3E5__2_4() const { return ___U3CiU3E5__2_4; }
	inline int32_t* get_address_of_U3CiU3E5__2_4() { return &___U3CiU3E5__2_4; }
	inline void set_U3CiU3E5__2_4(int32_t value)
	{
		___U3CiU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CadjustedPointU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF, ___U3CadjustedPointU3E5__3_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CadjustedPointU3E5__3_5() const { return ___U3CadjustedPointU3E5__3_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CadjustedPointU3E5__3_5() { return &___U3CadjustedPointU3E5__3_5; }
	inline void set_U3CadjustedPointU3E5__3_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CadjustedPointU3E5__3_5 = value;
	}
};


// TeleportTransitionWarp/<DoWarp>d__3
struct U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935  : public RuntimeObject
{
public:
	// System.Int32 TeleportTransitionWarp/<DoWarp>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TeleportTransitionWarp/<DoWarp>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TeleportTransitionWarp TeleportTransitionWarp/<DoWarp>d__3::<>4__this
	TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 TeleportTransitionWarp/<DoWarp>d__3::<startPosition>5__1
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CstartPositionU3E5__1_3;
	// System.Single TeleportTransitionWarp/<DoWarp>d__3::<elapsedTime>5__2
	float ___U3CelapsedTimeU3E5__2_4;
	// System.Single TeleportTransitionWarp/<DoWarp>d__3::<t>5__3
	float ___U3CtU3E5__3_5;
	// System.Single TeleportTransitionWarp/<DoWarp>d__3::<pLerp>5__4
	float ___U3CpLerpU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935, ___U3CU3E4__this_2)); }
	inline TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CstartPositionU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935, ___U3CstartPositionU3E5__1_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CstartPositionU3E5__1_3() const { return ___U3CstartPositionU3E5__1_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CstartPositionU3E5__1_3() { return &___U3CstartPositionU3E5__1_3; }
	inline void set_U3CstartPositionU3E5__1_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CstartPositionU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935, ___U3CelapsedTimeU3E5__2_4)); }
	inline float get_U3CelapsedTimeU3E5__2_4() const { return ___U3CelapsedTimeU3E5__2_4; }
	inline float* get_address_of_U3CelapsedTimeU3E5__2_4() { return &___U3CelapsedTimeU3E5__2_4; }
	inline void set_U3CelapsedTimeU3E5__2_4(float value)
	{
		___U3CelapsedTimeU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935, ___U3CtU3E5__3_5)); }
	inline float get_U3CtU3E5__3_5() const { return ___U3CtU3E5__3_5; }
	inline float* get_address_of_U3CtU3E5__3_5() { return &___U3CtU3E5__3_5; }
	inline void set_U3CtU3E5__3_5(float value)
	{
		___U3CtU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CpLerpU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935, ___U3CpLerpU3E5__4_6)); }
	inline float get_U3CpLerpU3E5__4_6() const { return ___U3CpLerpU3E5__4_6; }
	inline float* get_address_of_U3CpLerpU3E5__4_6() { return &___U3CpLerpU3E5__4_6; }
	inline void set_U3CpLerpU3E5__4_6(float value)
	{
		___U3CpLerpU3E5__4_6 = value;
	}
};


// OculusSampleFramework.TrackSegment/SegmentType
struct SegmentType_t75B6DB130F4401E0851F6346681BD0DD1C0DFE3A 
{
public:
	// System.Int32 OculusSampleFramework.TrackSegment/SegmentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SegmentType_t75B6DB130F4401E0851F6346681BD0DD1C0DFE3A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15
struct U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79  : public RuntimeObject
{
public:
	// System.Int32 OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::animationLength
	float ___animationLength_2;
	// OculusSampleFramework.TrainCrossingController OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<>4__this
	TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 * ___U3CU3E4__this_3;
	// System.Single OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<animationEndTime>5__1
	float ___U3CanimationEndTimeU3E5__1_4;
	// System.Single OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<lightBlinkDuration>5__2
	float ___U3ClightBlinkDurationU3E5__2_5;
	// System.Single OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<lightBlinkStartTime>5__3
	float ___U3ClightBlinkStartTimeU3E5__3_6;
	// System.Single OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<lightBlinkEndTime>5__4
	float ___U3ClightBlinkEndTimeU3E5__4_7;
	// UnityEngine.Material OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<lightToBlinkOn>5__5
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___U3ClightToBlinkOnU3E5__5_8;
	// UnityEngine.Material OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<lightToBlinkOff>5__6
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___U3ClightToBlinkOffU3E5__6_9;
	// UnityEngine.Color OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<onColor>5__7
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___U3ConColorU3E5__7_10;
	// UnityEngine.Color OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<offColor>5__8
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___U3CoffColorU3E5__8_11;
	// System.Single OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<t>5__9
	float ___U3CtU3E5__9_12;
	// UnityEngine.Material OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::<temp>5__10
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___U3CtempU3E5__10_13;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_animationLength_2() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___animationLength_2)); }
	inline float get_animationLength_2() const { return ___animationLength_2; }
	inline float* get_address_of_animationLength_2() { return &___animationLength_2; }
	inline void set_animationLength_2(float value)
	{
		___animationLength_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3CU3E4__this_3)); }
	inline TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CanimationEndTimeU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3CanimationEndTimeU3E5__1_4)); }
	inline float get_U3CanimationEndTimeU3E5__1_4() const { return ___U3CanimationEndTimeU3E5__1_4; }
	inline float* get_address_of_U3CanimationEndTimeU3E5__1_4() { return &___U3CanimationEndTimeU3E5__1_4; }
	inline void set_U3CanimationEndTimeU3E5__1_4(float value)
	{
		___U3CanimationEndTimeU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3ClightBlinkDurationU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3ClightBlinkDurationU3E5__2_5)); }
	inline float get_U3ClightBlinkDurationU3E5__2_5() const { return ___U3ClightBlinkDurationU3E5__2_5; }
	inline float* get_address_of_U3ClightBlinkDurationU3E5__2_5() { return &___U3ClightBlinkDurationU3E5__2_5; }
	inline void set_U3ClightBlinkDurationU3E5__2_5(float value)
	{
		___U3ClightBlinkDurationU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3ClightBlinkStartTimeU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3ClightBlinkStartTimeU3E5__3_6)); }
	inline float get_U3ClightBlinkStartTimeU3E5__3_6() const { return ___U3ClightBlinkStartTimeU3E5__3_6; }
	inline float* get_address_of_U3ClightBlinkStartTimeU3E5__3_6() { return &___U3ClightBlinkStartTimeU3E5__3_6; }
	inline void set_U3ClightBlinkStartTimeU3E5__3_6(float value)
	{
		___U3ClightBlinkStartTimeU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3ClightBlinkEndTimeU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3ClightBlinkEndTimeU3E5__4_7)); }
	inline float get_U3ClightBlinkEndTimeU3E5__4_7() const { return ___U3ClightBlinkEndTimeU3E5__4_7; }
	inline float* get_address_of_U3ClightBlinkEndTimeU3E5__4_7() { return &___U3ClightBlinkEndTimeU3E5__4_7; }
	inline void set_U3ClightBlinkEndTimeU3E5__4_7(float value)
	{
		___U3ClightBlinkEndTimeU3E5__4_7 = value;
	}

	inline static int32_t get_offset_of_U3ClightToBlinkOnU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3ClightToBlinkOnU3E5__5_8)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_U3ClightToBlinkOnU3E5__5_8() const { return ___U3ClightToBlinkOnU3E5__5_8; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_U3ClightToBlinkOnU3E5__5_8() { return &___U3ClightToBlinkOnU3E5__5_8; }
	inline void set_U3ClightToBlinkOnU3E5__5_8(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___U3ClightToBlinkOnU3E5__5_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClightToBlinkOnU3E5__5_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3ClightToBlinkOffU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3ClightToBlinkOffU3E5__6_9)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_U3ClightToBlinkOffU3E5__6_9() const { return ___U3ClightToBlinkOffU3E5__6_9; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_U3ClightToBlinkOffU3E5__6_9() { return &___U3ClightToBlinkOffU3E5__6_9; }
	inline void set_U3ClightToBlinkOffU3E5__6_9(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___U3ClightToBlinkOffU3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ClightToBlinkOffU3E5__6_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3ConColorU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3ConColorU3E5__7_10)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_U3ConColorU3E5__7_10() const { return ___U3ConColorU3E5__7_10; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_U3ConColorU3E5__7_10() { return &___U3ConColorU3E5__7_10; }
	inline void set_U3ConColorU3E5__7_10(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___U3ConColorU3E5__7_10 = value;
	}

	inline static int32_t get_offset_of_U3CoffColorU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3CoffColorU3E5__8_11)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_U3CoffColorU3E5__8_11() const { return ___U3CoffColorU3E5__8_11; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_U3CoffColorU3E5__8_11() { return &___U3CoffColorU3E5__8_11; }
	inline void set_U3CoffColorU3E5__8_11(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___U3CoffColorU3E5__8_11 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__9_12() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3CtU3E5__9_12)); }
	inline float get_U3CtU3E5__9_12() const { return ___U3CtU3E5__9_12; }
	inline float* get_address_of_U3CtU3E5__9_12() { return &___U3CtU3E5__9_12; }
	inline void set_U3CtU3E5__9_12(float value)
	{
		___U3CtU3E5__9_12 = value;
	}

	inline static int32_t get_offset_of_U3CtempU3E5__10_13() { return static_cast<int32_t>(offsetof(U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79, ___U3CtempU3E5__10_13)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_U3CtempU3E5__10_13() const { return ___U3CtempU3E5__10_13; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_U3CtempU3E5__10_13() { return &___U3CtempU3E5__10_13; }
	inline void set_U3CtempU3E5__10_13(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___U3CtempU3E5__10_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtempU3E5__10_13), (void*)value);
	}
};


// OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34
struct U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723  : public RuntimeObject
{
public:
	// System.Int32 OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Boolean OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::startTrain
	bool ___startTrain_2;
	// OculusSampleFramework.TrainLocomotive OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<>4__this
	TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * ___U3CU3E4__this_3;
	// System.Single OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<endSpeed>5__1
	float ___U3CendSpeedU3E5__1_4;
	// System.Single OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<timePeriodForSpeedChange>5__2
	float ___U3CtimePeriodForSpeedChangeU3E5__2_5;
	// System.Single OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<startTime>5__3
	float ___U3CstartTimeU3E5__3_6;
	// System.Single OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<endTime>5__4
	float ___U3CendTimeU3E5__4_7;
	// System.Single OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<startSpeed>5__5
	float ___U3CstartSpeedU3E5__5_8;
	// UnityEngine.ParticleSystem/EmissionModule OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<emissionModule1>5__6
	EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  ___U3CemissionModule1U3E5__6_9;
	// UnityEngine.ParticleSystem/MainModule OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<mainModule>5__7
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  ___U3CmainModuleU3E5__7_10;
	// System.Single OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::<t>5__8
	float ___U3CtU3E5__8_11;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_startTrain_2() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___startTrain_2)); }
	inline bool get_startTrain_2() const { return ___startTrain_2; }
	inline bool* get_address_of_startTrain_2() { return &___startTrain_2; }
	inline void set_startTrain_2(bool value)
	{
		___startTrain_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CU3E4__this_3)); }
	inline TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CendSpeedU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CendSpeedU3E5__1_4)); }
	inline float get_U3CendSpeedU3E5__1_4() const { return ___U3CendSpeedU3E5__1_4; }
	inline float* get_address_of_U3CendSpeedU3E5__1_4() { return &___U3CendSpeedU3E5__1_4; }
	inline void set_U3CendSpeedU3E5__1_4(float value)
	{
		___U3CendSpeedU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CtimePeriodForSpeedChangeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CtimePeriodForSpeedChangeU3E5__2_5)); }
	inline float get_U3CtimePeriodForSpeedChangeU3E5__2_5() const { return ___U3CtimePeriodForSpeedChangeU3E5__2_5; }
	inline float* get_address_of_U3CtimePeriodForSpeedChangeU3E5__2_5() { return &___U3CtimePeriodForSpeedChangeU3E5__2_5; }
	inline void set_U3CtimePeriodForSpeedChangeU3E5__2_5(float value)
	{
		___U3CtimePeriodForSpeedChangeU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CstartTimeU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CstartTimeU3E5__3_6)); }
	inline float get_U3CstartTimeU3E5__3_6() const { return ___U3CstartTimeU3E5__3_6; }
	inline float* get_address_of_U3CstartTimeU3E5__3_6() { return &___U3CstartTimeU3E5__3_6; }
	inline void set_U3CstartTimeU3E5__3_6(float value)
	{
		___U3CstartTimeU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CendTimeU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CendTimeU3E5__4_7)); }
	inline float get_U3CendTimeU3E5__4_7() const { return ___U3CendTimeU3E5__4_7; }
	inline float* get_address_of_U3CendTimeU3E5__4_7() { return &___U3CendTimeU3E5__4_7; }
	inline void set_U3CendTimeU3E5__4_7(float value)
	{
		___U3CendTimeU3E5__4_7 = value;
	}

	inline static int32_t get_offset_of_U3CstartSpeedU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CstartSpeedU3E5__5_8)); }
	inline float get_U3CstartSpeedU3E5__5_8() const { return ___U3CstartSpeedU3E5__5_8; }
	inline float* get_address_of_U3CstartSpeedU3E5__5_8() { return &___U3CstartSpeedU3E5__5_8; }
	inline void set_U3CstartSpeedU3E5__5_8(float value)
	{
		___U3CstartSpeedU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CemissionModule1U3E5__6_9() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CemissionModule1U3E5__6_9)); }
	inline EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  get_U3CemissionModule1U3E5__6_9() const { return ___U3CemissionModule1U3E5__6_9; }
	inline EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * get_address_of_U3CemissionModule1U3E5__6_9() { return &___U3CemissionModule1U3E5__6_9; }
	inline void set_U3CemissionModule1U3E5__6_9(EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  value)
	{
		___U3CemissionModule1U3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CemissionModule1U3E5__6_9))->___m_ParticleSystem_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CmainModuleU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CmainModuleU3E5__7_10)); }
	inline MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  get_U3CmainModuleU3E5__7_10() const { return ___U3CmainModuleU3E5__7_10; }
	inline MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * get_address_of_U3CmainModuleU3E5__7_10() { return &___U3CmainModuleU3E5__7_10; }
	inline void set_U3CmainModuleU3E5__7_10(MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  value)
	{
		___U3CmainModuleU3E5__7_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CmainModuleU3E5__7_10))->___m_ParticleSystem_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CtU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723, ___U3CtU3E5__8_11)); }
	inline float get_U3CtU3E5__8_11() const { return ___U3CtU3E5__8_11; }
	inline float* get_address_of_U3CtU3E5__8_11() { return &___U3CtU3E5__8_11; }
	inline void set_U3CtU3E5__8_11(float value)
	{
		___U3CtU3E5__8_11 = value;
	}
};


// OculusSampleFramework.TrainLocomotive/EngineSoundState
struct EngineSoundState_t9C153C06331D3432B0BD63DE85BA6B17B3BB11F2 
{
public:
	// System.Int32 OculusSampleFramework.TrainLocomotive/EngineSoundState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EngineSoundState_t9C153C06331D3432B0BD63DE85BA6B17B3BB11F2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Collections.NativeArray`1<System.Byte>
struct NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * ___m_PCMReaderCallback_4;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * ___m_PCMSetPositionCallback_5;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_4() { return static_cast<int32_t>(offsetof(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE, ___m_PCMReaderCallback_4)); }
	inline PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * get_m_PCMReaderCallback_4() const { return ___m_PCMReaderCallback_4; }
	inline PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B ** get_address_of_m_PCMReaderCallback_4() { return &___m_PCMReaderCallback_4; }
	inline void set_m_PCMReaderCallback_4(PCMReaderCallback_t9CA1437D36509A9FAC5EDD8FF2BC3259C24D0E0B * value)
	{
		___m_PCMReaderCallback_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMReaderCallback_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_5() { return static_cast<int32_t>(offsetof(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE, ___m_PCMSetPositionCallback_5)); }
	inline PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * get_m_PCMSetPositionCallback_5() const { return ___m_PCMSetPositionCallback_5; }
	inline PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C ** get_address_of_m_PCMSetPositionCallback_5() { return &___m_PCMSetPositionCallback_5; }
	inline void set_m_PCMSetPositionCallback_5(PCMSetPositionCallback_tBDD99E7C0697687F1E7B06CDD5DE444A3709CF4C * value)
	{
		___m_PCMSetPositionCallback_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PCMSetPositionCallback_5), (void*)value);
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// UnityEngine.WebCamDevice
struct WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C 
{
public:
	// System.String UnityEngine.WebCamDevice::m_Name
	String_t* ___m_Name_0;
	// System.String UnityEngine.WebCamDevice::m_DepthCameraName
	String_t* ___m_DepthCameraName_1;
	// System.Int32 UnityEngine.WebCamDevice::m_Flags
	int32_t ___m_Flags_2;
	// UnityEngine.WebCamKind UnityEngine.WebCamDevice::m_Kind
	int32_t ___m_Kind_3;
	// UnityEngine.Resolution[] UnityEngine.WebCamDevice::m_Resolutions
	ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597* ___m_Resolutions_4;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_DepthCameraName_1() { return static_cast<int32_t>(offsetof(WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C, ___m_DepthCameraName_1)); }
	inline String_t* get_m_DepthCameraName_1() const { return ___m_DepthCameraName_1; }
	inline String_t** get_address_of_m_DepthCameraName_1() { return &___m_DepthCameraName_1; }
	inline void set_m_DepthCameraName_1(String_t* value)
	{
		___m_DepthCameraName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DepthCameraName_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_2() { return static_cast<int32_t>(offsetof(WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C, ___m_Flags_2)); }
	inline int32_t get_m_Flags_2() const { return ___m_Flags_2; }
	inline int32_t* get_address_of_m_Flags_2() { return &___m_Flags_2; }
	inline void set_m_Flags_2(int32_t value)
	{
		___m_Flags_2 = value;
	}

	inline static int32_t get_offset_of_m_Kind_3() { return static_cast<int32_t>(offsetof(WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C, ___m_Kind_3)); }
	inline int32_t get_m_Kind_3() const { return ___m_Kind_3; }
	inline int32_t* get_address_of_m_Kind_3() { return &___m_Kind_3; }
	inline void set_m_Kind_3(int32_t value)
	{
		___m_Kind_3 = value;
	}

	inline static int32_t get_offset_of_m_Resolutions_4() { return static_cast<int32_t>(offsetof(WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C, ___m_Resolutions_4)); }
	inline ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597* get_m_Resolutions_4() const { return ___m_Resolutions_4; }
	inline ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597** get_address_of_m_Resolutions_4() { return &___m_Resolutions_4; }
	inline void set_m_Resolutions_4(ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597* value)
	{
		___m_Resolutions_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Resolutions_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WebCamDevice
struct WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C_marshaled_pinvoke
{
	char* ___m_Name_0;
	char* ___m_DepthCameraName_1;
	int32_t ___m_Flags_2;
	int32_t ___m_Kind_3;
	Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 * ___m_Resolutions_4;
};
// Native definition for COM marshalling of UnityEngine.WebCamDevice
struct WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C_marshaled_com
{
	Il2CppChar* ___m_Name_0;
	Il2CppChar* ___m_DepthCameraName_1;
	int32_t ___m_Flags_2;
	int32_t ___m_Kind_3;
	Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 * ___m_Resolutions_4;
};

// LocomotionTeleport/AimData
struct AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA  : public RuntimeObject
{
public:
	// UnityEngine.RaycastHit LocomotionTeleport/AimData::TargetHitInfo
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  ___TargetHitInfo_0;
	// System.Boolean LocomotionTeleport/AimData::TargetValid
	bool ___TargetValid_1;
	// System.Nullable`1<UnityEngine.Vector3> LocomotionTeleport/AimData::Destination
	Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  ___Destination_2;
	// System.Single LocomotionTeleport/AimData::Radius
	float ___Radius_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> LocomotionTeleport/AimData::<Points>k__BackingField
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___U3CPointsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_TargetHitInfo_0() { return static_cast<int32_t>(offsetof(AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA, ___TargetHitInfo_0)); }
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  get_TargetHitInfo_0() const { return ___TargetHitInfo_0; }
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * get_address_of_TargetHitInfo_0() { return &___TargetHitInfo_0; }
	inline void set_TargetHitInfo_0(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  value)
	{
		___TargetHitInfo_0 = value;
	}

	inline static int32_t get_offset_of_TargetValid_1() { return static_cast<int32_t>(offsetof(AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA, ___TargetValid_1)); }
	inline bool get_TargetValid_1() const { return ___TargetValid_1; }
	inline bool* get_address_of_TargetValid_1() { return &___TargetValid_1; }
	inline void set_TargetValid_1(bool value)
	{
		___TargetValid_1 = value;
	}

	inline static int32_t get_offset_of_Destination_2() { return static_cast<int32_t>(offsetof(AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA, ___Destination_2)); }
	inline Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  get_Destination_2() const { return ___Destination_2; }
	inline Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 * get_address_of_Destination_2() { return &___Destination_2; }
	inline void set_Destination_2(Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  value)
	{
		___Destination_2 = value;
	}

	inline static int32_t get_offset_of_Radius_3() { return static_cast<int32_t>(offsetof(AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA, ___Radius_3)); }
	inline float get_Radius_3() const { return ___Radius_3; }
	inline float* get_address_of_Radius_3() { return &___Radius_3; }
	inline void set_Radius_3(float value)
	{
		___Radius_3 = value;
	}

	inline static int32_t get_offset_of_U3CPointsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA, ___U3CPointsU3Ek__BackingField_4)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_U3CPointsU3Ek__BackingField_4() const { return ___U3CPointsU3Ek__BackingField_4; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_U3CPointsU3Ek__BackingField_4() { return &___U3CPointsU3Ek__BackingField_4; }
	inline void set_U3CPointsU3Ek__BackingField_4(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___U3CPointsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPointsU3Ek__BackingField_4), (void*)value);
	}
};


// TeleportInputHandler/<TeleportAimCoroutine>d__6
struct U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7  : public RuntimeObject
{
public:
	// System.Int32 TeleportInputHandler/<TeleportAimCoroutine>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TeleportInputHandler/<TeleportAimCoroutine>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TeleportInputHandler TeleportInputHandler/<TeleportAimCoroutine>d__6::<>4__this
	TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * ___U3CU3E4__this_2;
	// LocomotionTeleport/TeleportIntentions TeleportInputHandler/<TeleportAimCoroutine>d__6::<intention>5__1
	int32_t ___U3CintentionU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7, ___U3CU3E4__this_2)); }
	inline TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CintentionU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7, ___U3CintentionU3E5__1_3)); }
	inline int32_t get_U3CintentionU3E5__1_3() const { return ___U3CintentionU3E5__1_3; }
	inline int32_t* get_address_of_U3CintentionU3E5__1_3() { return &___U3CintentionU3E5__1_3; }
	inline void set_U3CintentionU3E5__1_3(int32_t value)
	{
		___U3CintentionU3E5__1_3 = value;
	}
};


// TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49
struct U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5  : public RuntimeObject
{
public:
	// System.Int32 TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.RenderTexture TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::inputRenderTexture
	RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * ___inputRenderTexture_2;
	// TextureEncoder TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::<>4__this
	TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * ___U3CU3E4__this_3;
	// UnityEngine.Rendering.AsyncGPUReadbackRequest TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::<request>5__1
	AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA  ___U3CrequestU3E5__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_inputRenderTexture_2() { return static_cast<int32_t>(offsetof(U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5, ___inputRenderTexture_2)); }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * get_inputRenderTexture_2() const { return ___inputRenderTexture_2; }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** get_address_of_inputRenderTexture_2() { return &___inputRenderTexture_2; }
	inline void set_inputRenderTexture_2(RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * value)
	{
		___inputRenderTexture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputRenderTexture_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5, ___U3CU3E4__this_3)); }
	inline TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5, ___U3CrequestU3E5__1_4)); }
	inline AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA  get_U3CrequestU3E5__1_4() const { return ___U3CrequestU3E5__1_4; }
	inline AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA * get_address_of_U3CrequestU3E5__1_4() { return &___U3CrequestU3E5__1_4; }
	inline void set_U3CrequestU3E5__1_4(AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA  value)
	{
		___U3CrequestU3E5__1_4 = value;
	}
};


// DKP.VR.VRIO/<Haptics>d__0
struct U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF  : public RuntimeObject
{
public:
	// System.Int32 DKP.VR.VRIO/<Haptics>d__0::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncVoidMethodBuilder DKP.VR.VRIO/<Haptics>d__0::<>t__builder
	AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6  ___U3CU3Et__builder_1;
	// DKP.VR.Hand DKP.VR.VRIO/<Haptics>d__0::hand
	int32_t ___hand_2;
	// System.Runtime.CompilerServices.TaskAwaiter DKP.VR.VRIO/<Haptics>d__0::<>u__1
	TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  ___U3CU3Eu__1_3;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DKP.VR.VRIO/<Haptics>d__0::<>u__2
	YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  ___U3CU3Eu__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF, ___U3CU3Et__builder_1)); }
	inline AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_synchronizationContext_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Et__builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_hand_2() { return static_cast<int32_t>(offsetof(U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF, ___hand_2)); }
	inline int32_t get_hand_2() const { return ___hand_2; }
	inline int32_t* get_address_of_hand_2() { return &___hand_2; }
	inline void set_hand_2(int32_t value)
	{
		___hand_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF, ___U3CU3Eu__1_3)); }
	inline TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  value)
	{
		___U3CU3Eu__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CU3Eu__1_3))->___m_task_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CU3Eu__2_4() { return static_cast<int32_t>(offsetof(U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF, ___U3CU3Eu__2_4)); }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  get_U3CU3Eu__2_4() const { return ___U3CU3Eu__2_4; }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * get_address_of_U3CU3Eu__2_4() { return &___U3CU3Eu__2_4; }
	inline void set_U3CU3Eu__2_4(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  value)
	{
		___U3CU3Eu__2_4 = value;
	}
};


// System.Action`1<UnityEngine.Rendering.AsyncGPUReadbackRequest>
struct Action_1_t542D0A6987D7110F66453B06D83AFE1D24208526  : public MulticastDelegate_t
{
public:

public:
};


// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.EventHandler
struct EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B  : public MulticastDelegate_t
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.RenderTexture
struct RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.WebCamTexture
struct WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.AudioBehaviour
struct AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B  : public AudioBehaviour_tB44966D47AD43C50C7294AEE9B57574E55AACA4A
{
public:

public:
};


// LocomotionTeleport
struct LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean LocomotionTeleport::EnableMovementDuringReady
	bool ___EnableMovementDuringReady_4;
	// System.Boolean LocomotionTeleport::EnableMovementDuringAim
	bool ___EnableMovementDuringAim_5;
	// System.Boolean LocomotionTeleport::EnableMovementDuringPreTeleport
	bool ___EnableMovementDuringPreTeleport_6;
	// System.Boolean LocomotionTeleport::EnableMovementDuringPostTeleport
	bool ___EnableMovementDuringPostTeleport_7;
	// System.Boolean LocomotionTeleport::EnableRotationDuringReady
	bool ___EnableRotationDuringReady_8;
	// System.Boolean LocomotionTeleport::EnableRotationDuringAim
	bool ___EnableRotationDuringAim_9;
	// System.Boolean LocomotionTeleport::EnableRotationDuringPreTeleport
	bool ___EnableRotationDuringPreTeleport_10;
	// System.Boolean LocomotionTeleport::EnableRotationDuringPostTeleport
	bool ___EnableRotationDuringPostTeleport_11;
	// LocomotionTeleport/States LocomotionTeleport::<CurrentState>k__BackingField
	int32_t ___U3CCurrentStateU3Ek__BackingField_12;
	// TeleportAimHandler LocomotionTeleport::AimHandler
	TeleportAimHandler_tC1C6D09FC478B420E7C892ECCB6E625F537D3BBD * ___AimHandler_13;
	// TeleportDestination LocomotionTeleport::TeleportDestinationPrefab
	TeleportDestination_t1DCC2BDB675ACE76D915FE7251B4CB6C487811DA * ___TeleportDestinationPrefab_14;
	// System.Int32 LocomotionTeleport::TeleportDestinationLayer
	int32_t ___TeleportDestinationLayer_15;
	// System.Action`4<System.Boolean,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Quaternion>,System.Nullable`1<UnityEngine.Quaternion>> LocomotionTeleport::UpdateTeleportDestination
	Action_4_t9426C491AA37FE7E7E24FF3395FFAC5A60AAC07F * ___UpdateTeleportDestination_16;
	// TeleportInputHandler LocomotionTeleport::InputHandler
	TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * ___InputHandler_17;
	// LocomotionTeleport/TeleportIntentions LocomotionTeleport::CurrentIntention
	int32_t ___CurrentIntention_18;
	// System.Boolean LocomotionTeleport::IsPreTeleportRequested
	bool ___IsPreTeleportRequested_19;
	// System.Boolean LocomotionTeleport::IsTransitioning
	bool ___IsTransitioning_20;
	// System.Boolean LocomotionTeleport::IsPostTeleportRequested
	bool ___IsPostTeleportRequested_21;
	// TeleportDestination LocomotionTeleport::_teleportDestination
	TeleportDestination_t1DCC2BDB675ACE76D915FE7251B4CB6C487811DA * ____teleportDestination_22;
	// LocomotionController LocomotionTeleport::<LocomotionController>k__BackingField
	LocomotionController_tCA625CD0A5F52DDB33ECB2FAC435471C0C079719 * ___U3CLocomotionControllerU3Ek__BackingField_23;
	// LocomotionTeleport/AimCollisionTypes LocomotionTeleport::AimCollisionType
	int32_t ___AimCollisionType_24;
	// System.Boolean LocomotionTeleport::UseCharacterCollisionData
	bool ___UseCharacterCollisionData_25;
	// System.Single LocomotionTeleport::AimCollisionRadius
	float ___AimCollisionRadius_26;
	// System.Single LocomotionTeleport::AimCollisionHeight
	float ___AimCollisionHeight_27;
	// System.Action LocomotionTeleport::EnterStateReady
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___EnterStateReady_28;
	// System.Action LocomotionTeleport::EnterStateAim
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___EnterStateAim_29;
	// System.Action`1<LocomotionTeleport/AimData> LocomotionTeleport::UpdateAimData
	Action_1_t92198758A8B104AAC58CEBF1192A89AFDC67850B * ___UpdateAimData_30;
	// System.Action LocomotionTeleport::ExitStateAim
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___ExitStateAim_31;
	// System.Action LocomotionTeleport::EnterStateCancelAim
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___EnterStateCancelAim_32;
	// System.Action LocomotionTeleport::EnterStatePreTeleport
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___EnterStatePreTeleport_33;
	// System.Action LocomotionTeleport::EnterStateCancelTeleport
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___EnterStateCancelTeleport_34;
	// System.Action LocomotionTeleport::EnterStateTeleporting
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___EnterStateTeleporting_35;
	// System.Action LocomotionTeleport::EnterStatePostTeleport
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___EnterStatePostTeleport_36;
	// System.Action`3<UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion> LocomotionTeleport::Teleported
	Action_3_tA4F94900F976E775A483E76A95AB2A443C164EC2 * ___Teleported_37;

public:
	inline static int32_t get_offset_of_EnableMovementDuringReady_4() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnableMovementDuringReady_4)); }
	inline bool get_EnableMovementDuringReady_4() const { return ___EnableMovementDuringReady_4; }
	inline bool* get_address_of_EnableMovementDuringReady_4() { return &___EnableMovementDuringReady_4; }
	inline void set_EnableMovementDuringReady_4(bool value)
	{
		___EnableMovementDuringReady_4 = value;
	}

	inline static int32_t get_offset_of_EnableMovementDuringAim_5() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnableMovementDuringAim_5)); }
	inline bool get_EnableMovementDuringAim_5() const { return ___EnableMovementDuringAim_5; }
	inline bool* get_address_of_EnableMovementDuringAim_5() { return &___EnableMovementDuringAim_5; }
	inline void set_EnableMovementDuringAim_5(bool value)
	{
		___EnableMovementDuringAim_5 = value;
	}

	inline static int32_t get_offset_of_EnableMovementDuringPreTeleport_6() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnableMovementDuringPreTeleport_6)); }
	inline bool get_EnableMovementDuringPreTeleport_6() const { return ___EnableMovementDuringPreTeleport_6; }
	inline bool* get_address_of_EnableMovementDuringPreTeleport_6() { return &___EnableMovementDuringPreTeleport_6; }
	inline void set_EnableMovementDuringPreTeleport_6(bool value)
	{
		___EnableMovementDuringPreTeleport_6 = value;
	}

	inline static int32_t get_offset_of_EnableMovementDuringPostTeleport_7() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnableMovementDuringPostTeleport_7)); }
	inline bool get_EnableMovementDuringPostTeleport_7() const { return ___EnableMovementDuringPostTeleport_7; }
	inline bool* get_address_of_EnableMovementDuringPostTeleport_7() { return &___EnableMovementDuringPostTeleport_7; }
	inline void set_EnableMovementDuringPostTeleport_7(bool value)
	{
		___EnableMovementDuringPostTeleport_7 = value;
	}

	inline static int32_t get_offset_of_EnableRotationDuringReady_8() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnableRotationDuringReady_8)); }
	inline bool get_EnableRotationDuringReady_8() const { return ___EnableRotationDuringReady_8; }
	inline bool* get_address_of_EnableRotationDuringReady_8() { return &___EnableRotationDuringReady_8; }
	inline void set_EnableRotationDuringReady_8(bool value)
	{
		___EnableRotationDuringReady_8 = value;
	}

	inline static int32_t get_offset_of_EnableRotationDuringAim_9() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnableRotationDuringAim_9)); }
	inline bool get_EnableRotationDuringAim_9() const { return ___EnableRotationDuringAim_9; }
	inline bool* get_address_of_EnableRotationDuringAim_9() { return &___EnableRotationDuringAim_9; }
	inline void set_EnableRotationDuringAim_9(bool value)
	{
		___EnableRotationDuringAim_9 = value;
	}

	inline static int32_t get_offset_of_EnableRotationDuringPreTeleport_10() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnableRotationDuringPreTeleport_10)); }
	inline bool get_EnableRotationDuringPreTeleport_10() const { return ___EnableRotationDuringPreTeleport_10; }
	inline bool* get_address_of_EnableRotationDuringPreTeleport_10() { return &___EnableRotationDuringPreTeleport_10; }
	inline void set_EnableRotationDuringPreTeleport_10(bool value)
	{
		___EnableRotationDuringPreTeleport_10 = value;
	}

	inline static int32_t get_offset_of_EnableRotationDuringPostTeleport_11() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnableRotationDuringPostTeleport_11)); }
	inline bool get_EnableRotationDuringPostTeleport_11() const { return ___EnableRotationDuringPostTeleport_11; }
	inline bool* get_address_of_EnableRotationDuringPostTeleport_11() { return &___EnableRotationDuringPostTeleport_11; }
	inline void set_EnableRotationDuringPostTeleport_11(bool value)
	{
		___EnableRotationDuringPostTeleport_11 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___U3CCurrentStateU3Ek__BackingField_12)); }
	inline int32_t get_U3CCurrentStateU3Ek__BackingField_12() const { return ___U3CCurrentStateU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CCurrentStateU3Ek__BackingField_12() { return &___U3CCurrentStateU3Ek__BackingField_12; }
	inline void set_U3CCurrentStateU3Ek__BackingField_12(int32_t value)
	{
		___U3CCurrentStateU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_AimHandler_13() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___AimHandler_13)); }
	inline TeleportAimHandler_tC1C6D09FC478B420E7C892ECCB6E625F537D3BBD * get_AimHandler_13() const { return ___AimHandler_13; }
	inline TeleportAimHandler_tC1C6D09FC478B420E7C892ECCB6E625F537D3BBD ** get_address_of_AimHandler_13() { return &___AimHandler_13; }
	inline void set_AimHandler_13(TeleportAimHandler_tC1C6D09FC478B420E7C892ECCB6E625F537D3BBD * value)
	{
		___AimHandler_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AimHandler_13), (void*)value);
	}

	inline static int32_t get_offset_of_TeleportDestinationPrefab_14() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___TeleportDestinationPrefab_14)); }
	inline TeleportDestination_t1DCC2BDB675ACE76D915FE7251B4CB6C487811DA * get_TeleportDestinationPrefab_14() const { return ___TeleportDestinationPrefab_14; }
	inline TeleportDestination_t1DCC2BDB675ACE76D915FE7251B4CB6C487811DA ** get_address_of_TeleportDestinationPrefab_14() { return &___TeleportDestinationPrefab_14; }
	inline void set_TeleportDestinationPrefab_14(TeleportDestination_t1DCC2BDB675ACE76D915FE7251B4CB6C487811DA * value)
	{
		___TeleportDestinationPrefab_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TeleportDestinationPrefab_14), (void*)value);
	}

	inline static int32_t get_offset_of_TeleportDestinationLayer_15() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___TeleportDestinationLayer_15)); }
	inline int32_t get_TeleportDestinationLayer_15() const { return ___TeleportDestinationLayer_15; }
	inline int32_t* get_address_of_TeleportDestinationLayer_15() { return &___TeleportDestinationLayer_15; }
	inline void set_TeleportDestinationLayer_15(int32_t value)
	{
		___TeleportDestinationLayer_15 = value;
	}

	inline static int32_t get_offset_of_UpdateTeleportDestination_16() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___UpdateTeleportDestination_16)); }
	inline Action_4_t9426C491AA37FE7E7E24FF3395FFAC5A60AAC07F * get_UpdateTeleportDestination_16() const { return ___UpdateTeleportDestination_16; }
	inline Action_4_t9426C491AA37FE7E7E24FF3395FFAC5A60AAC07F ** get_address_of_UpdateTeleportDestination_16() { return &___UpdateTeleportDestination_16; }
	inline void set_UpdateTeleportDestination_16(Action_4_t9426C491AA37FE7E7E24FF3395FFAC5A60AAC07F * value)
	{
		___UpdateTeleportDestination_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UpdateTeleportDestination_16), (void*)value);
	}

	inline static int32_t get_offset_of_InputHandler_17() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___InputHandler_17)); }
	inline TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * get_InputHandler_17() const { return ___InputHandler_17; }
	inline TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D ** get_address_of_InputHandler_17() { return &___InputHandler_17; }
	inline void set_InputHandler_17(TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * value)
	{
		___InputHandler_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InputHandler_17), (void*)value);
	}

	inline static int32_t get_offset_of_CurrentIntention_18() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___CurrentIntention_18)); }
	inline int32_t get_CurrentIntention_18() const { return ___CurrentIntention_18; }
	inline int32_t* get_address_of_CurrentIntention_18() { return &___CurrentIntention_18; }
	inline void set_CurrentIntention_18(int32_t value)
	{
		___CurrentIntention_18 = value;
	}

	inline static int32_t get_offset_of_IsPreTeleportRequested_19() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___IsPreTeleportRequested_19)); }
	inline bool get_IsPreTeleportRequested_19() const { return ___IsPreTeleportRequested_19; }
	inline bool* get_address_of_IsPreTeleportRequested_19() { return &___IsPreTeleportRequested_19; }
	inline void set_IsPreTeleportRequested_19(bool value)
	{
		___IsPreTeleportRequested_19 = value;
	}

	inline static int32_t get_offset_of_IsTransitioning_20() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___IsTransitioning_20)); }
	inline bool get_IsTransitioning_20() const { return ___IsTransitioning_20; }
	inline bool* get_address_of_IsTransitioning_20() { return &___IsTransitioning_20; }
	inline void set_IsTransitioning_20(bool value)
	{
		___IsTransitioning_20 = value;
	}

	inline static int32_t get_offset_of_IsPostTeleportRequested_21() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___IsPostTeleportRequested_21)); }
	inline bool get_IsPostTeleportRequested_21() const { return ___IsPostTeleportRequested_21; }
	inline bool* get_address_of_IsPostTeleportRequested_21() { return &___IsPostTeleportRequested_21; }
	inline void set_IsPostTeleportRequested_21(bool value)
	{
		___IsPostTeleportRequested_21 = value;
	}

	inline static int32_t get_offset_of__teleportDestination_22() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ____teleportDestination_22)); }
	inline TeleportDestination_t1DCC2BDB675ACE76D915FE7251B4CB6C487811DA * get__teleportDestination_22() const { return ____teleportDestination_22; }
	inline TeleportDestination_t1DCC2BDB675ACE76D915FE7251B4CB6C487811DA ** get_address_of__teleportDestination_22() { return &____teleportDestination_22; }
	inline void set__teleportDestination_22(TeleportDestination_t1DCC2BDB675ACE76D915FE7251B4CB6C487811DA * value)
	{
		____teleportDestination_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____teleportDestination_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLocomotionControllerU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___U3CLocomotionControllerU3Ek__BackingField_23)); }
	inline LocomotionController_tCA625CD0A5F52DDB33ECB2FAC435471C0C079719 * get_U3CLocomotionControllerU3Ek__BackingField_23() const { return ___U3CLocomotionControllerU3Ek__BackingField_23; }
	inline LocomotionController_tCA625CD0A5F52DDB33ECB2FAC435471C0C079719 ** get_address_of_U3CLocomotionControllerU3Ek__BackingField_23() { return &___U3CLocomotionControllerU3Ek__BackingField_23; }
	inline void set_U3CLocomotionControllerU3Ek__BackingField_23(LocomotionController_tCA625CD0A5F52DDB33ECB2FAC435471C0C079719 * value)
	{
		___U3CLocomotionControllerU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLocomotionControllerU3Ek__BackingField_23), (void*)value);
	}

	inline static int32_t get_offset_of_AimCollisionType_24() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___AimCollisionType_24)); }
	inline int32_t get_AimCollisionType_24() const { return ___AimCollisionType_24; }
	inline int32_t* get_address_of_AimCollisionType_24() { return &___AimCollisionType_24; }
	inline void set_AimCollisionType_24(int32_t value)
	{
		___AimCollisionType_24 = value;
	}

	inline static int32_t get_offset_of_UseCharacterCollisionData_25() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___UseCharacterCollisionData_25)); }
	inline bool get_UseCharacterCollisionData_25() const { return ___UseCharacterCollisionData_25; }
	inline bool* get_address_of_UseCharacterCollisionData_25() { return &___UseCharacterCollisionData_25; }
	inline void set_UseCharacterCollisionData_25(bool value)
	{
		___UseCharacterCollisionData_25 = value;
	}

	inline static int32_t get_offset_of_AimCollisionRadius_26() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___AimCollisionRadius_26)); }
	inline float get_AimCollisionRadius_26() const { return ___AimCollisionRadius_26; }
	inline float* get_address_of_AimCollisionRadius_26() { return &___AimCollisionRadius_26; }
	inline void set_AimCollisionRadius_26(float value)
	{
		___AimCollisionRadius_26 = value;
	}

	inline static int32_t get_offset_of_AimCollisionHeight_27() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___AimCollisionHeight_27)); }
	inline float get_AimCollisionHeight_27() const { return ___AimCollisionHeight_27; }
	inline float* get_address_of_AimCollisionHeight_27() { return &___AimCollisionHeight_27; }
	inline void set_AimCollisionHeight_27(float value)
	{
		___AimCollisionHeight_27 = value;
	}

	inline static int32_t get_offset_of_EnterStateReady_28() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnterStateReady_28)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_EnterStateReady_28() const { return ___EnterStateReady_28; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_EnterStateReady_28() { return &___EnterStateReady_28; }
	inline void set_EnterStateReady_28(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___EnterStateReady_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnterStateReady_28), (void*)value);
	}

	inline static int32_t get_offset_of_EnterStateAim_29() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnterStateAim_29)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_EnterStateAim_29() const { return ___EnterStateAim_29; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_EnterStateAim_29() { return &___EnterStateAim_29; }
	inline void set_EnterStateAim_29(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___EnterStateAim_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnterStateAim_29), (void*)value);
	}

	inline static int32_t get_offset_of_UpdateAimData_30() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___UpdateAimData_30)); }
	inline Action_1_t92198758A8B104AAC58CEBF1192A89AFDC67850B * get_UpdateAimData_30() const { return ___UpdateAimData_30; }
	inline Action_1_t92198758A8B104AAC58CEBF1192A89AFDC67850B ** get_address_of_UpdateAimData_30() { return &___UpdateAimData_30; }
	inline void set_UpdateAimData_30(Action_1_t92198758A8B104AAC58CEBF1192A89AFDC67850B * value)
	{
		___UpdateAimData_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UpdateAimData_30), (void*)value);
	}

	inline static int32_t get_offset_of_ExitStateAim_31() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___ExitStateAim_31)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_ExitStateAim_31() const { return ___ExitStateAim_31; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_ExitStateAim_31() { return &___ExitStateAim_31; }
	inline void set_ExitStateAim_31(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___ExitStateAim_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ExitStateAim_31), (void*)value);
	}

	inline static int32_t get_offset_of_EnterStateCancelAim_32() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnterStateCancelAim_32)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_EnterStateCancelAim_32() const { return ___EnterStateCancelAim_32; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_EnterStateCancelAim_32() { return &___EnterStateCancelAim_32; }
	inline void set_EnterStateCancelAim_32(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___EnterStateCancelAim_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnterStateCancelAim_32), (void*)value);
	}

	inline static int32_t get_offset_of_EnterStatePreTeleport_33() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnterStatePreTeleport_33)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_EnterStatePreTeleport_33() const { return ___EnterStatePreTeleport_33; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_EnterStatePreTeleport_33() { return &___EnterStatePreTeleport_33; }
	inline void set_EnterStatePreTeleport_33(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___EnterStatePreTeleport_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnterStatePreTeleport_33), (void*)value);
	}

	inline static int32_t get_offset_of_EnterStateCancelTeleport_34() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnterStateCancelTeleport_34)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_EnterStateCancelTeleport_34() const { return ___EnterStateCancelTeleport_34; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_EnterStateCancelTeleport_34() { return &___EnterStateCancelTeleport_34; }
	inline void set_EnterStateCancelTeleport_34(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___EnterStateCancelTeleport_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnterStateCancelTeleport_34), (void*)value);
	}

	inline static int32_t get_offset_of_EnterStateTeleporting_35() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnterStateTeleporting_35)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_EnterStateTeleporting_35() const { return ___EnterStateTeleporting_35; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_EnterStateTeleporting_35() { return &___EnterStateTeleporting_35; }
	inline void set_EnterStateTeleporting_35(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___EnterStateTeleporting_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnterStateTeleporting_35), (void*)value);
	}

	inline static int32_t get_offset_of_EnterStatePostTeleport_36() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___EnterStatePostTeleport_36)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_EnterStatePostTeleport_36() const { return ___EnterStatePostTeleport_36; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_EnterStatePostTeleport_36() { return &___EnterStatePostTeleport_36; }
	inline void set_EnterStatePostTeleport_36(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___EnterStatePostTeleport_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnterStatePostTeleport_36), (void*)value);
	}

	inline static int32_t get_offset_of_Teleported_37() { return static_cast<int32_t>(offsetof(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206, ___Teleported_37)); }
	inline Action_3_tA4F94900F976E775A483E76A95AB2A443C164EC2 * get_Teleported_37() const { return ___Teleported_37; }
	inline Action_3_tA4F94900F976E775A483E76A95AB2A443C164EC2 ** get_address_of_Teleported_37() { return &___Teleported_37; }
	inline void set_Teleported_37(Action_3_tA4F94900F976E775A483E76A95AB2A443C164EC2 * value)
	{
		___Teleported_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Teleported_37), (void*)value);
	}
};


// Loom
struct Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 Loom::_count
	int32_t ____count_7;
	// System.Collections.Generic.List`1<System.Action> Loom::_actions
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ____actions_9;
	// System.Collections.Generic.List`1<Loom/DelayedQueueItem> Loom::_delayed
	List_1_t30C4A5C6EF7CF16239D523CD578A04147253133E * ____delayed_10;
	// System.Collections.Generic.List`1<Loom/DelayedQueueItem> Loom::_currentDelayed
	List_1_t30C4A5C6EF7CF16239D523CD578A04147253133E * ____currentDelayed_11;
	// System.Collections.Generic.List`1<System.Action> Loom::_currentActions
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ____currentActions_12;

public:
	inline static int32_t get_offset_of__count_7() { return static_cast<int32_t>(offsetof(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0, ____count_7)); }
	inline int32_t get__count_7() const { return ____count_7; }
	inline int32_t* get_address_of__count_7() { return &____count_7; }
	inline void set__count_7(int32_t value)
	{
		____count_7 = value;
	}

	inline static int32_t get_offset_of__actions_9() { return static_cast<int32_t>(offsetof(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0, ____actions_9)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get__actions_9() const { return ____actions_9; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of__actions_9() { return &____actions_9; }
	inline void set__actions_9(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		____actions_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____actions_9), (void*)value);
	}

	inline static int32_t get_offset_of__delayed_10() { return static_cast<int32_t>(offsetof(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0, ____delayed_10)); }
	inline List_1_t30C4A5C6EF7CF16239D523CD578A04147253133E * get__delayed_10() const { return ____delayed_10; }
	inline List_1_t30C4A5C6EF7CF16239D523CD578A04147253133E ** get_address_of__delayed_10() { return &____delayed_10; }
	inline void set__delayed_10(List_1_t30C4A5C6EF7CF16239D523CD578A04147253133E * value)
	{
		____delayed_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____delayed_10), (void*)value);
	}

	inline static int32_t get_offset_of__currentDelayed_11() { return static_cast<int32_t>(offsetof(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0, ____currentDelayed_11)); }
	inline List_1_t30C4A5C6EF7CF16239D523CD578A04147253133E * get__currentDelayed_11() const { return ____currentDelayed_11; }
	inline List_1_t30C4A5C6EF7CF16239D523CD578A04147253133E ** get_address_of__currentDelayed_11() { return &____currentDelayed_11; }
	inline void set__currentDelayed_11(List_1_t30C4A5C6EF7CF16239D523CD578A04147253133E * value)
	{
		____currentDelayed_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentDelayed_11), (void*)value);
	}

	inline static int32_t get_offset_of__currentActions_12() { return static_cast<int32_t>(offsetof(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0, ____currentActions_12)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get__currentActions_12() const { return ____currentActions_12; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of__currentActions_12() { return &____currentActions_12; }
	inline void set__currentActions_12(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		____currentActions_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentActions_12), (void*)value);
	}
};

struct Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_StaticFields
{
public:
	// System.Int32 Loom::processorCount
	int32_t ___processorCount_4;
	// System.Int32 Loom::numThreads
	int32_t ___numThreads_5;
	// Loom Loom::_current
	Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0 * ____current_6;
	// System.Boolean Loom::initialized
	bool ___initialized_8;

public:
	inline static int32_t get_offset_of_processorCount_4() { return static_cast<int32_t>(offsetof(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_StaticFields, ___processorCount_4)); }
	inline int32_t get_processorCount_4() const { return ___processorCount_4; }
	inline int32_t* get_address_of_processorCount_4() { return &___processorCount_4; }
	inline void set_processorCount_4(int32_t value)
	{
		___processorCount_4 = value;
	}

	inline static int32_t get_offset_of_numThreads_5() { return static_cast<int32_t>(offsetof(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_StaticFields, ___numThreads_5)); }
	inline int32_t get_numThreads_5() const { return ___numThreads_5; }
	inline int32_t* get_address_of_numThreads_5() { return &___numThreads_5; }
	inline void set_numThreads_5(int32_t value)
	{
		___numThreads_5 = value;
	}

	inline static int32_t get_offset_of__current_6() { return static_cast<int32_t>(offsetof(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_StaticFields, ____current_6)); }
	inline Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0 * get__current_6() const { return ____current_6; }
	inline Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0 ** get_address_of__current_6() { return &____current_6; }
	inline void set__current_6(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0 * value)
	{
		____current_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____current_6), (void*)value);
	}

	inline static int32_t get_offset_of_initialized_8() { return static_cast<int32_t>(offsetof(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_StaticFields, ___initialized_8)); }
	inline bool get_initialized_8() const { return ___initialized_8; }
	inline bool* get_address_of_initialized_8() { return &___initialized_8; }
	inline void set_initialized_8(bool value)
	{
		___initialized_8 = value;
	}
};


// OVRPassthroughLayer
struct OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// OVRPassthroughLayer/ProjectionSurfaceType OVRPassthroughLayer::projectionSurfaceType
	int32_t ___projectionSurfaceType_4;
	// OVROverlay/OverlayType OVRPassthroughLayer::overlayType
	int32_t ___overlayType_5;
	// System.Int32 OVRPassthroughLayer::compositionDepth
	int32_t ___compositionDepth_6;
	// System.Boolean OVRPassthroughLayer::hidden
	bool ___hidden_7;
	// System.Boolean OVRPassthroughLayer::overridePerLayerColorScaleAndOffset
	bool ___overridePerLayerColorScaleAndOffset_8;
	// UnityEngine.Vector4 OVRPassthroughLayer::colorScale
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___colorScale_9;
	// UnityEngine.Vector4 OVRPassthroughLayer::colorOffset
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___colorOffset_10;
	// OVRPassthroughLayer/ColorMapEditorType OVRPassthroughLayer::colorMapEditorType_
	int32_t ___colorMapEditorType__11;
	// UnityEngine.Gradient OVRPassthroughLayer::colorMapEditorGradient
	Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * ___colorMapEditorGradient_12;
	// UnityEngine.Gradient OVRPassthroughLayer::colorMapEditorGradientOld
	Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * ___colorMapEditorGradientOld_13;
	// System.Single OVRPassthroughLayer::colorMapEditorContrast
	float ___colorMapEditorContrast_14;
	// System.Single OVRPassthroughLayer::colorMapEditorContrast_
	float ___colorMapEditorContrast__15;
	// System.Single OVRPassthroughLayer::colorMapEditorBrightness
	float ___colorMapEditorBrightness_16;
	// System.Single OVRPassthroughLayer::colorMapEditorBrightness_
	float ___colorMapEditorBrightness__17;
	// System.Single OVRPassthroughLayer::colorMapEditorPosterize
	float ___colorMapEditorPosterize_18;
	// System.Single OVRPassthroughLayer::colorMapEditorPosterize_
	float ___colorMapEditorPosterize__19;
	// UnityEngine.GameObject OVRPassthroughLayer::auxGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___auxGameObject_20;
	// OVROverlay OVRPassthroughLayer::passthroughOverlay
	OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 * ___passthroughOverlay_21;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,OVRPassthroughLayer/PassthroughMeshInstance> OVRPassthroughLayer::surfaceGameObjects
	Dictionary_2_t60275C105022D515270F8E8181130D8B1020CB74 * ___surfaceGameObjects_22;
	// System.Collections.Generic.List`1<OVRPassthroughLayer/DeferredPassthroughMeshAddition> OVRPassthroughLayer::deferredSurfaceGameObjects
	List_1_t3B0820C8464D06CB2C98E3D7C75B224815E79323 * ___deferredSurfaceGameObjects_23;
	// System.Single OVRPassthroughLayer::textureOpacity_
	float ___textureOpacity__24;
	// System.Boolean OVRPassthroughLayer::edgeRenderingEnabled_
	bool ___edgeRenderingEnabled__25;
	// UnityEngine.Color OVRPassthroughLayer::edgeColor_
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___edgeColor__26;
	// OVRPlugin/InsightPassthroughColorMapType OVRPassthroughLayer::colorMapType
	int32_t ___colorMapType_27;
	// System.Byte[] OVRPassthroughLayer::colorMapData
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___colorMapData_28;
	// System.Runtime.InteropServices.GCHandle OVRPassthroughLayer::colorMapDataHandle
	GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  ___colorMapDataHandle_29;
	// System.Boolean OVRPassthroughLayer::styleDirty
	bool ___styleDirty_30;

public:
	inline static int32_t get_offset_of_projectionSurfaceType_4() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___projectionSurfaceType_4)); }
	inline int32_t get_projectionSurfaceType_4() const { return ___projectionSurfaceType_4; }
	inline int32_t* get_address_of_projectionSurfaceType_4() { return &___projectionSurfaceType_4; }
	inline void set_projectionSurfaceType_4(int32_t value)
	{
		___projectionSurfaceType_4 = value;
	}

	inline static int32_t get_offset_of_overlayType_5() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___overlayType_5)); }
	inline int32_t get_overlayType_5() const { return ___overlayType_5; }
	inline int32_t* get_address_of_overlayType_5() { return &___overlayType_5; }
	inline void set_overlayType_5(int32_t value)
	{
		___overlayType_5 = value;
	}

	inline static int32_t get_offset_of_compositionDepth_6() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___compositionDepth_6)); }
	inline int32_t get_compositionDepth_6() const { return ___compositionDepth_6; }
	inline int32_t* get_address_of_compositionDepth_6() { return &___compositionDepth_6; }
	inline void set_compositionDepth_6(int32_t value)
	{
		___compositionDepth_6 = value;
	}

	inline static int32_t get_offset_of_hidden_7() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___hidden_7)); }
	inline bool get_hidden_7() const { return ___hidden_7; }
	inline bool* get_address_of_hidden_7() { return &___hidden_7; }
	inline void set_hidden_7(bool value)
	{
		___hidden_7 = value;
	}

	inline static int32_t get_offset_of_overridePerLayerColorScaleAndOffset_8() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___overridePerLayerColorScaleAndOffset_8)); }
	inline bool get_overridePerLayerColorScaleAndOffset_8() const { return ___overridePerLayerColorScaleAndOffset_8; }
	inline bool* get_address_of_overridePerLayerColorScaleAndOffset_8() { return &___overridePerLayerColorScaleAndOffset_8; }
	inline void set_overridePerLayerColorScaleAndOffset_8(bool value)
	{
		___overridePerLayerColorScaleAndOffset_8 = value;
	}

	inline static int32_t get_offset_of_colorScale_9() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorScale_9)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_colorScale_9() const { return ___colorScale_9; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_colorScale_9() { return &___colorScale_9; }
	inline void set_colorScale_9(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___colorScale_9 = value;
	}

	inline static int32_t get_offset_of_colorOffset_10() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorOffset_10)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_colorOffset_10() const { return ___colorOffset_10; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_colorOffset_10() { return &___colorOffset_10; }
	inline void set_colorOffset_10(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___colorOffset_10 = value;
	}

	inline static int32_t get_offset_of_colorMapEditorType__11() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapEditorType__11)); }
	inline int32_t get_colorMapEditorType__11() const { return ___colorMapEditorType__11; }
	inline int32_t* get_address_of_colorMapEditorType__11() { return &___colorMapEditorType__11; }
	inline void set_colorMapEditorType__11(int32_t value)
	{
		___colorMapEditorType__11 = value;
	}

	inline static int32_t get_offset_of_colorMapEditorGradient_12() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapEditorGradient_12)); }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * get_colorMapEditorGradient_12() const { return ___colorMapEditorGradient_12; }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 ** get_address_of_colorMapEditorGradient_12() { return &___colorMapEditorGradient_12; }
	inline void set_colorMapEditorGradient_12(Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * value)
	{
		___colorMapEditorGradient_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorMapEditorGradient_12), (void*)value);
	}

	inline static int32_t get_offset_of_colorMapEditorGradientOld_13() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapEditorGradientOld_13)); }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * get_colorMapEditorGradientOld_13() const { return ___colorMapEditorGradientOld_13; }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 ** get_address_of_colorMapEditorGradientOld_13() { return &___colorMapEditorGradientOld_13; }
	inline void set_colorMapEditorGradientOld_13(Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * value)
	{
		___colorMapEditorGradientOld_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorMapEditorGradientOld_13), (void*)value);
	}

	inline static int32_t get_offset_of_colorMapEditorContrast_14() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapEditorContrast_14)); }
	inline float get_colorMapEditorContrast_14() const { return ___colorMapEditorContrast_14; }
	inline float* get_address_of_colorMapEditorContrast_14() { return &___colorMapEditorContrast_14; }
	inline void set_colorMapEditorContrast_14(float value)
	{
		___colorMapEditorContrast_14 = value;
	}

	inline static int32_t get_offset_of_colorMapEditorContrast__15() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapEditorContrast__15)); }
	inline float get_colorMapEditorContrast__15() const { return ___colorMapEditorContrast__15; }
	inline float* get_address_of_colorMapEditorContrast__15() { return &___colorMapEditorContrast__15; }
	inline void set_colorMapEditorContrast__15(float value)
	{
		___colorMapEditorContrast__15 = value;
	}

	inline static int32_t get_offset_of_colorMapEditorBrightness_16() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapEditorBrightness_16)); }
	inline float get_colorMapEditorBrightness_16() const { return ___colorMapEditorBrightness_16; }
	inline float* get_address_of_colorMapEditorBrightness_16() { return &___colorMapEditorBrightness_16; }
	inline void set_colorMapEditorBrightness_16(float value)
	{
		___colorMapEditorBrightness_16 = value;
	}

	inline static int32_t get_offset_of_colorMapEditorBrightness__17() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapEditorBrightness__17)); }
	inline float get_colorMapEditorBrightness__17() const { return ___colorMapEditorBrightness__17; }
	inline float* get_address_of_colorMapEditorBrightness__17() { return &___colorMapEditorBrightness__17; }
	inline void set_colorMapEditorBrightness__17(float value)
	{
		___colorMapEditorBrightness__17 = value;
	}

	inline static int32_t get_offset_of_colorMapEditorPosterize_18() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapEditorPosterize_18)); }
	inline float get_colorMapEditorPosterize_18() const { return ___colorMapEditorPosterize_18; }
	inline float* get_address_of_colorMapEditorPosterize_18() { return &___colorMapEditorPosterize_18; }
	inline void set_colorMapEditorPosterize_18(float value)
	{
		___colorMapEditorPosterize_18 = value;
	}

	inline static int32_t get_offset_of_colorMapEditorPosterize__19() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapEditorPosterize__19)); }
	inline float get_colorMapEditorPosterize__19() const { return ___colorMapEditorPosterize__19; }
	inline float* get_address_of_colorMapEditorPosterize__19() { return &___colorMapEditorPosterize__19; }
	inline void set_colorMapEditorPosterize__19(float value)
	{
		___colorMapEditorPosterize__19 = value;
	}

	inline static int32_t get_offset_of_auxGameObject_20() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___auxGameObject_20)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_auxGameObject_20() const { return ___auxGameObject_20; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_auxGameObject_20() { return &___auxGameObject_20; }
	inline void set_auxGameObject_20(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___auxGameObject_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___auxGameObject_20), (void*)value);
	}

	inline static int32_t get_offset_of_passthroughOverlay_21() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___passthroughOverlay_21)); }
	inline OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 * get_passthroughOverlay_21() const { return ___passthroughOverlay_21; }
	inline OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 ** get_address_of_passthroughOverlay_21() { return &___passthroughOverlay_21; }
	inline void set_passthroughOverlay_21(OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 * value)
	{
		___passthroughOverlay_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___passthroughOverlay_21), (void*)value);
	}

	inline static int32_t get_offset_of_surfaceGameObjects_22() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___surfaceGameObjects_22)); }
	inline Dictionary_2_t60275C105022D515270F8E8181130D8B1020CB74 * get_surfaceGameObjects_22() const { return ___surfaceGameObjects_22; }
	inline Dictionary_2_t60275C105022D515270F8E8181130D8B1020CB74 ** get_address_of_surfaceGameObjects_22() { return &___surfaceGameObjects_22; }
	inline void set_surfaceGameObjects_22(Dictionary_2_t60275C105022D515270F8E8181130D8B1020CB74 * value)
	{
		___surfaceGameObjects_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___surfaceGameObjects_22), (void*)value);
	}

	inline static int32_t get_offset_of_deferredSurfaceGameObjects_23() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___deferredSurfaceGameObjects_23)); }
	inline List_1_t3B0820C8464D06CB2C98E3D7C75B224815E79323 * get_deferredSurfaceGameObjects_23() const { return ___deferredSurfaceGameObjects_23; }
	inline List_1_t3B0820C8464D06CB2C98E3D7C75B224815E79323 ** get_address_of_deferredSurfaceGameObjects_23() { return &___deferredSurfaceGameObjects_23; }
	inline void set_deferredSurfaceGameObjects_23(List_1_t3B0820C8464D06CB2C98E3D7C75B224815E79323 * value)
	{
		___deferredSurfaceGameObjects_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deferredSurfaceGameObjects_23), (void*)value);
	}

	inline static int32_t get_offset_of_textureOpacity__24() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___textureOpacity__24)); }
	inline float get_textureOpacity__24() const { return ___textureOpacity__24; }
	inline float* get_address_of_textureOpacity__24() { return &___textureOpacity__24; }
	inline void set_textureOpacity__24(float value)
	{
		___textureOpacity__24 = value;
	}

	inline static int32_t get_offset_of_edgeRenderingEnabled__25() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___edgeRenderingEnabled__25)); }
	inline bool get_edgeRenderingEnabled__25() const { return ___edgeRenderingEnabled__25; }
	inline bool* get_address_of_edgeRenderingEnabled__25() { return &___edgeRenderingEnabled__25; }
	inline void set_edgeRenderingEnabled__25(bool value)
	{
		___edgeRenderingEnabled__25 = value;
	}

	inline static int32_t get_offset_of_edgeColor__26() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___edgeColor__26)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_edgeColor__26() const { return ___edgeColor__26; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_edgeColor__26() { return &___edgeColor__26; }
	inline void set_edgeColor__26(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___edgeColor__26 = value;
	}

	inline static int32_t get_offset_of_colorMapType_27() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapType_27)); }
	inline int32_t get_colorMapType_27() const { return ___colorMapType_27; }
	inline int32_t* get_address_of_colorMapType_27() { return &___colorMapType_27; }
	inline void set_colorMapType_27(int32_t value)
	{
		___colorMapType_27 = value;
	}

	inline static int32_t get_offset_of_colorMapData_28() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapData_28)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_colorMapData_28() const { return ___colorMapData_28; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_colorMapData_28() { return &___colorMapData_28; }
	inline void set_colorMapData_28(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___colorMapData_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorMapData_28), (void*)value);
	}

	inline static int32_t get_offset_of_colorMapDataHandle_29() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___colorMapDataHandle_29)); }
	inline GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  get_colorMapDataHandle_29() const { return ___colorMapDataHandle_29; }
	inline GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603 * get_address_of_colorMapDataHandle_29() { return &___colorMapDataHandle_29; }
	inline void set_colorMapDataHandle_29(GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  value)
	{
		___colorMapDataHandle_29 = value;
	}

	inline static int32_t get_offset_of_styleDirty_30() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB, ___styleDirty_30)); }
	inline bool get_styleDirty_30() const { return ___styleDirty_30; }
	inline bool* get_address_of_styleDirty_30() { return &___styleDirty_30; }
	inline void set_styleDirty_30(bool value)
	{
		___styleDirty_30 = value;
	}
};

struct OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB_StaticFields
{
public:
	// UnityEngine.Gradient OVRPassthroughLayer::colorMapNeutralGradient
	Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * ___colorMapNeutralGradient_31;

public:
	inline static int32_t get_offset_of_colorMapNeutralGradient_31() { return static_cast<int32_t>(offsetof(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB_StaticFields, ___colorMapNeutralGradient_31)); }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * get_colorMapNeutralGradient_31() const { return ___colorMapNeutralGradient_31; }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 ** get_address_of_colorMapNeutralGradient_31() { return &___colorMapNeutralGradient_31; }
	inline void set_colorMapNeutralGradient_31(Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * value)
	{
		___colorMapNeutralGradient_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorMapNeutralGradient_31), (void*)value);
	}
};


// Obi.ObiColliderBase
struct ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single Obi.ObiColliderBase::thickness
	float ___thickness_4;
	// Obi.ObiCollisionMaterial Obi.ObiColliderBase::material
	ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F * ___material_5;
	// System.Int32 Obi.ObiColliderBase::filter
	int32_t ___filter_6;
	// Obi.ObiColliderHandle Obi.ObiColliderBase::shapeHandle
	ObiColliderHandle_tE9A3F736A020E2D6B47BF185EEA23173D8B723F8 * ___shapeHandle_7;
	// System.IntPtr Obi.ObiColliderBase::oniCollider
	intptr_t ___oniCollider_8;
	// Obi.ObiRigidbodyBase Obi.ObiColliderBase::obiRigidbody
	ObiRigidbodyBase_tA95EE97A5497D66D571ABA3D52491F363471272A * ___obiRigidbody_9;
	// System.Boolean Obi.ObiColliderBase::wasUnityColliderEnabled
	bool ___wasUnityColliderEnabled_10;
	// System.Boolean Obi.ObiColliderBase::dirty
	bool ___dirty_11;
	// Obi.ObiShapeTracker Obi.ObiColliderBase::tracker
	ObiShapeTracker_tFA2C80FDB58BB42DD59DBA239AE4A916FAAC7730 * ___tracker_12;

public:
	inline static int32_t get_offset_of_thickness_4() { return static_cast<int32_t>(offsetof(ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4, ___thickness_4)); }
	inline float get_thickness_4() const { return ___thickness_4; }
	inline float* get_address_of_thickness_4() { return &___thickness_4; }
	inline void set_thickness_4(float value)
	{
		___thickness_4 = value;
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4, ___material_5)); }
	inline ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F * get_material_5() const { return ___material_5; }
	inline ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___material_5), (void*)value);
	}

	inline static int32_t get_offset_of_filter_6() { return static_cast<int32_t>(offsetof(ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4, ___filter_6)); }
	inline int32_t get_filter_6() const { return ___filter_6; }
	inline int32_t* get_address_of_filter_6() { return &___filter_6; }
	inline void set_filter_6(int32_t value)
	{
		___filter_6 = value;
	}

	inline static int32_t get_offset_of_shapeHandle_7() { return static_cast<int32_t>(offsetof(ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4, ___shapeHandle_7)); }
	inline ObiColliderHandle_tE9A3F736A020E2D6B47BF185EEA23173D8B723F8 * get_shapeHandle_7() const { return ___shapeHandle_7; }
	inline ObiColliderHandle_tE9A3F736A020E2D6B47BF185EEA23173D8B723F8 ** get_address_of_shapeHandle_7() { return &___shapeHandle_7; }
	inline void set_shapeHandle_7(ObiColliderHandle_tE9A3F736A020E2D6B47BF185EEA23173D8B723F8 * value)
	{
		___shapeHandle_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shapeHandle_7), (void*)value);
	}

	inline static int32_t get_offset_of_oniCollider_8() { return static_cast<int32_t>(offsetof(ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4, ___oniCollider_8)); }
	inline intptr_t get_oniCollider_8() const { return ___oniCollider_8; }
	inline intptr_t* get_address_of_oniCollider_8() { return &___oniCollider_8; }
	inline void set_oniCollider_8(intptr_t value)
	{
		___oniCollider_8 = value;
	}

	inline static int32_t get_offset_of_obiRigidbody_9() { return static_cast<int32_t>(offsetof(ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4, ___obiRigidbody_9)); }
	inline ObiRigidbodyBase_tA95EE97A5497D66D571ABA3D52491F363471272A * get_obiRigidbody_9() const { return ___obiRigidbody_9; }
	inline ObiRigidbodyBase_tA95EE97A5497D66D571ABA3D52491F363471272A ** get_address_of_obiRigidbody_9() { return &___obiRigidbody_9; }
	inline void set_obiRigidbody_9(ObiRigidbodyBase_tA95EE97A5497D66D571ABA3D52491F363471272A * value)
	{
		___obiRigidbody_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___obiRigidbody_9), (void*)value);
	}

	inline static int32_t get_offset_of_wasUnityColliderEnabled_10() { return static_cast<int32_t>(offsetof(ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4, ___wasUnityColliderEnabled_10)); }
	inline bool get_wasUnityColliderEnabled_10() const { return ___wasUnityColliderEnabled_10; }
	inline bool* get_address_of_wasUnityColliderEnabled_10() { return &___wasUnityColliderEnabled_10; }
	inline void set_wasUnityColliderEnabled_10(bool value)
	{
		___wasUnityColliderEnabled_10 = value;
	}

	inline static int32_t get_offset_of_dirty_11() { return static_cast<int32_t>(offsetof(ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4, ___dirty_11)); }
	inline bool get_dirty_11() const { return ___dirty_11; }
	inline bool* get_address_of_dirty_11() { return &___dirty_11; }
	inline void set_dirty_11(bool value)
	{
		___dirty_11 = value;
	}

	inline static int32_t get_offset_of_tracker_12() { return static_cast<int32_t>(offsetof(ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4, ___tracker_12)); }
	inline ObiShapeTracker_tFA2C80FDB58BB42DD59DBA239AE4A916FAAC7730 * get_tracker_12() const { return ___tracker_12; }
	inline ObiShapeTracker_tFA2C80FDB58BB42DD59DBA239AE4A916FAAC7730 ** get_address_of_tracker_12() { return &___tracker_12; }
	inline void set_tracker_12(ObiShapeTracker_tFA2C80FDB58BB42DD59DBA239AE4A916FAAC7730 * value)
	{
		___tracker_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tracker_12), (void*)value);
	}
};


// Panel_AllInOne
struct Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform Panel_AllInOne::syncView
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___syncView_4;
	// UnityEngine.UI.Button Panel_AllInOne::btQuitSync
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btQuitSync_5;
	// UnityEngine.UI.Text Panel_AllInOne::txtLocalIP
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtLocalIP_6;
	// UnityEngine.UI.Text Panel_AllInOne::txtIPText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtIPText_7;
	// UnityEngine.UI.Image Panel_AllInOne::imgIPOutLine
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___imgIPOutLine_8;
	// UnityEngine.UI.Image Panel_AllInOne::imgLoading
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___imgLoading_9;
	// UnityEngine.UI.Image Panel_AllInOne::imgLogo
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___imgLogo_10;
	// UnityEngine.UI.Text Panel_AllInOne::txtRemind
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___txtRemind_11;
	// UnityEngine.UI.Button Panel_AllInOne::btSync
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btSync_12;
	// UnityEngine.UI.Button Panel_AllInOne::btBack
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btBack_13;
	// System.EventHandler Panel_AllInOne::EHCloseAllInOne
	EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___EHCloseAllInOne_14;

public:
	inline static int32_t get_offset_of_syncView_4() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___syncView_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_syncView_4() const { return ___syncView_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_syncView_4() { return &___syncView_4; }
	inline void set_syncView_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___syncView_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___syncView_4), (void*)value);
	}

	inline static int32_t get_offset_of_btQuitSync_5() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___btQuitSync_5)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btQuitSync_5() const { return ___btQuitSync_5; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btQuitSync_5() { return &___btQuitSync_5; }
	inline void set_btQuitSync_5(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btQuitSync_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btQuitSync_5), (void*)value);
	}

	inline static int32_t get_offset_of_txtLocalIP_6() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___txtLocalIP_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtLocalIP_6() const { return ___txtLocalIP_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtLocalIP_6() { return &___txtLocalIP_6; }
	inline void set_txtLocalIP_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtLocalIP_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtLocalIP_6), (void*)value);
	}

	inline static int32_t get_offset_of_txtIPText_7() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___txtIPText_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtIPText_7() const { return ___txtIPText_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtIPText_7() { return &___txtIPText_7; }
	inline void set_txtIPText_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtIPText_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtIPText_7), (void*)value);
	}

	inline static int32_t get_offset_of_imgIPOutLine_8() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___imgIPOutLine_8)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_imgIPOutLine_8() const { return ___imgIPOutLine_8; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_imgIPOutLine_8() { return &___imgIPOutLine_8; }
	inline void set_imgIPOutLine_8(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___imgIPOutLine_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imgIPOutLine_8), (void*)value);
	}

	inline static int32_t get_offset_of_imgLoading_9() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___imgLoading_9)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_imgLoading_9() const { return ___imgLoading_9; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_imgLoading_9() { return &___imgLoading_9; }
	inline void set_imgLoading_9(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___imgLoading_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imgLoading_9), (void*)value);
	}

	inline static int32_t get_offset_of_imgLogo_10() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___imgLogo_10)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_imgLogo_10() const { return ___imgLogo_10; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_imgLogo_10() { return &___imgLogo_10; }
	inline void set_imgLogo_10(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___imgLogo_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___imgLogo_10), (void*)value);
	}

	inline static int32_t get_offset_of_txtRemind_11() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___txtRemind_11)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_txtRemind_11() const { return ___txtRemind_11; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_txtRemind_11() { return &___txtRemind_11; }
	inline void set_txtRemind_11(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___txtRemind_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___txtRemind_11), (void*)value);
	}

	inline static int32_t get_offset_of_btSync_12() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___btSync_12)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btSync_12() const { return ___btSync_12; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btSync_12() { return &___btSync_12; }
	inline void set_btSync_12(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btSync_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btSync_12), (void*)value);
	}

	inline static int32_t get_offset_of_btBack_13() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___btBack_13)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btBack_13() const { return ___btBack_13; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btBack_13() { return &___btBack_13; }
	inline void set_btBack_13(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btBack_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btBack_13), (void*)value);
	}

	inline static int32_t get_offset_of_EHCloseAllInOne_14() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D, ___EHCloseAllInOne_14)); }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * get_EHCloseAllInOne_14() const { return ___EHCloseAllInOne_14; }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B ** get_address_of_EHCloseAllInOne_14() { return &___EHCloseAllInOne_14; }
	inline void set_EHCloseAllInOne_14(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * value)
	{
		___EHCloseAllInOne_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EHCloseAllInOne_14), (void*)value);
	}
};

struct Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D_StaticFields
{
public:
	// System.Boolean Panel_AllInOne::swsw
	bool ___swsw_15;

public:
	inline static int32_t get_offset_of_swsw_15() { return static_cast<int32_t>(offsetof(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D_StaticFields, ___swsw_15)); }
	inline bool get_swsw_15() const { return ___swsw_15; }
	inline bool* get_address_of_swsw_15() { return &___swsw_15; }
	inline void set_swsw_15(bool value)
	{
		___swsw_15 = value;
	}
};


// Panel_DataRecord
struct Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Button Panel_DataRecord::btBack
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btBack_4;
	// UnityEngine.UI.Button Panel_DataRecord::btPP
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btPP_5;
	// UnityEngine.UI.Button Panel_DataRecord::btP
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btP_6;
	// System.Collections.Generic.List`1<UnityEngine.UI.Button> Panel_DataRecord::btPages
	List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * ___btPages_7;
	// UnityEngine.UI.Button Panel_DataRecord::btN
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btN_8;
	// UnityEngine.UI.Button Panel_DataRecord::btNN
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btNN_9;
	// UnityEngine.Sprite Panel_DataRecord::sprNormal
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___sprNormal_10;
	// UnityEngine.Sprite Panel_DataRecord::sprPressed
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___sprPressed_11;
	// UnityEngine.Sprite Panel_DataRecord::sprDot
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___sprDot_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Panel_DataRecord::container
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___container_13;
	// System.Collections.Generic.List`1<GPTT.Data.API/FireExtinguisher> Panel_DataRecord::fireExtinguishers
	List_1_tE9A16AED8ACE3B4F11793B96E0914894721D031D * ___fireExtinguishers_14;
	// System.Int32 Panel_DataRecord::pageCount
	int32_t ___pageCount_15;
	// System.Int32 Panel_DataRecord::currentPage
	int32_t ___currentPage_16;
	// System.EventHandler Panel_DataRecord::EHCloseDataRecord
	EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___EHCloseDataRecord_17;

public:
	inline static int32_t get_offset_of_btBack_4() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___btBack_4)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btBack_4() const { return ___btBack_4; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btBack_4() { return &___btBack_4; }
	inline void set_btBack_4(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btBack_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btBack_4), (void*)value);
	}

	inline static int32_t get_offset_of_btPP_5() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___btPP_5)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btPP_5() const { return ___btPP_5; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btPP_5() { return &___btPP_5; }
	inline void set_btPP_5(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btPP_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btPP_5), (void*)value);
	}

	inline static int32_t get_offset_of_btP_6() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___btP_6)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btP_6() const { return ___btP_6; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btP_6() { return &___btP_6; }
	inline void set_btP_6(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btP_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btP_6), (void*)value);
	}

	inline static int32_t get_offset_of_btPages_7() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___btPages_7)); }
	inline List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * get_btPages_7() const { return ___btPages_7; }
	inline List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E ** get_address_of_btPages_7() { return &___btPages_7; }
	inline void set_btPages_7(List_1_t21BC49A4390CA067C4050F19A1D58BE64AC2E89E * value)
	{
		___btPages_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btPages_7), (void*)value);
	}

	inline static int32_t get_offset_of_btN_8() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___btN_8)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btN_8() const { return ___btN_8; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btN_8() { return &___btN_8; }
	inline void set_btN_8(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btN_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btN_8), (void*)value);
	}

	inline static int32_t get_offset_of_btNN_9() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___btNN_9)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btNN_9() const { return ___btNN_9; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btNN_9() { return &___btNN_9; }
	inline void set_btNN_9(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btNN_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btNN_9), (void*)value);
	}

	inline static int32_t get_offset_of_sprNormal_10() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___sprNormal_10)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_sprNormal_10() const { return ___sprNormal_10; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_sprNormal_10() { return &___sprNormal_10; }
	inline void set_sprNormal_10(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___sprNormal_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sprNormal_10), (void*)value);
	}

	inline static int32_t get_offset_of_sprPressed_11() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___sprPressed_11)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_sprPressed_11() const { return ___sprPressed_11; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_sprPressed_11() { return &___sprPressed_11; }
	inline void set_sprPressed_11(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___sprPressed_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sprPressed_11), (void*)value);
	}

	inline static int32_t get_offset_of_sprDot_12() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___sprDot_12)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_sprDot_12() const { return ___sprDot_12; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_sprDot_12() { return &___sprDot_12; }
	inline void set_sprDot_12(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___sprDot_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sprDot_12), (void*)value);
	}

	inline static int32_t get_offset_of_container_13() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___container_13)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_container_13() const { return ___container_13; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_container_13() { return &___container_13; }
	inline void set_container_13(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___container_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___container_13), (void*)value);
	}

	inline static int32_t get_offset_of_fireExtinguishers_14() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___fireExtinguishers_14)); }
	inline List_1_tE9A16AED8ACE3B4F11793B96E0914894721D031D * get_fireExtinguishers_14() const { return ___fireExtinguishers_14; }
	inline List_1_tE9A16AED8ACE3B4F11793B96E0914894721D031D ** get_address_of_fireExtinguishers_14() { return &___fireExtinguishers_14; }
	inline void set_fireExtinguishers_14(List_1_tE9A16AED8ACE3B4F11793B96E0914894721D031D * value)
	{
		___fireExtinguishers_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fireExtinguishers_14), (void*)value);
	}

	inline static int32_t get_offset_of_pageCount_15() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___pageCount_15)); }
	inline int32_t get_pageCount_15() const { return ___pageCount_15; }
	inline int32_t* get_address_of_pageCount_15() { return &___pageCount_15; }
	inline void set_pageCount_15(int32_t value)
	{
		___pageCount_15 = value;
	}

	inline static int32_t get_offset_of_currentPage_16() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___currentPage_16)); }
	inline int32_t get_currentPage_16() const { return ___currentPage_16; }
	inline int32_t* get_address_of_currentPage_16() { return &___currentPage_16; }
	inline void set_currentPage_16(int32_t value)
	{
		___currentPage_16 = value;
	}

	inline static int32_t get_offset_of_EHCloseDataRecord_17() { return static_cast<int32_t>(offsetof(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57, ___EHCloseDataRecord_17)); }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * get_EHCloseDataRecord_17() const { return ___EHCloseDataRecord_17; }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B ** get_address_of_EHCloseDataRecord_17() { return &___EHCloseDataRecord_17; }
	inline void set_EHCloseDataRecord_17(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * value)
	{
		___EHCloseDataRecord_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EHCloseDataRecord_17), (void*)value);
	}
};


// Panel_Login
struct Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.InputField Panel_Login::ifAccount
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___ifAccount_4;
	// UnityEngine.UI.InputField Panel_Login::ifPassword
	InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * ___ifPassword_5;
	// UnityEngine.UI.Toggle Panel_Login::tgVisiable
	Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * ___tgVisiable_6;
	// UnityEngine.UI.Button Panel_Login::btLoginByAccount
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btLoginByAccount_7;
	// UnityEngine.UI.Button Panel_Login::btLoginByVisitor
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btLoginByVisitor_8;
	// UnityEngine.Sprite Panel_Login::spVisiable
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___spVisiable_9;
	// UnityEngine.Sprite Panel_Login::spInvisiable
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___spInvisiable_10;
	// UnityEngine.RectTransform Panel_Login::errorIcon
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___errorIcon_11;
	// System.EventHandler Panel_Login::EHOpenMenuPanel
	EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___EHOpenMenuPanel_12;

public:
	inline static int32_t get_offset_of_ifAccount_4() { return static_cast<int32_t>(offsetof(Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41, ___ifAccount_4)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_ifAccount_4() const { return ___ifAccount_4; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_ifAccount_4() { return &___ifAccount_4; }
	inline void set_ifAccount_4(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___ifAccount_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ifAccount_4), (void*)value);
	}

	inline static int32_t get_offset_of_ifPassword_5() { return static_cast<int32_t>(offsetof(Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41, ___ifPassword_5)); }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * get_ifPassword_5() const { return ___ifPassword_5; }
	inline InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 ** get_address_of_ifPassword_5() { return &___ifPassword_5; }
	inline void set_ifPassword_5(InputField_tB41A2814F31A3E9373D443EDEBBB2856006324D0 * value)
	{
		___ifPassword_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ifPassword_5), (void*)value);
	}

	inline static int32_t get_offset_of_tgVisiable_6() { return static_cast<int32_t>(offsetof(Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41, ___tgVisiable_6)); }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * get_tgVisiable_6() const { return ___tgVisiable_6; }
	inline Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E ** get_address_of_tgVisiable_6() { return &___tgVisiable_6; }
	inline void set_tgVisiable_6(Toggle_t68F5A84CDD2BBAEA866F42EB4E0C9F2B431D612E * value)
	{
		___tgVisiable_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tgVisiable_6), (void*)value);
	}

	inline static int32_t get_offset_of_btLoginByAccount_7() { return static_cast<int32_t>(offsetof(Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41, ___btLoginByAccount_7)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btLoginByAccount_7() const { return ___btLoginByAccount_7; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btLoginByAccount_7() { return &___btLoginByAccount_7; }
	inline void set_btLoginByAccount_7(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btLoginByAccount_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btLoginByAccount_7), (void*)value);
	}

	inline static int32_t get_offset_of_btLoginByVisitor_8() { return static_cast<int32_t>(offsetof(Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41, ___btLoginByVisitor_8)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btLoginByVisitor_8() const { return ___btLoginByVisitor_8; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btLoginByVisitor_8() { return &___btLoginByVisitor_8; }
	inline void set_btLoginByVisitor_8(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btLoginByVisitor_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btLoginByVisitor_8), (void*)value);
	}

	inline static int32_t get_offset_of_spVisiable_9() { return static_cast<int32_t>(offsetof(Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41, ___spVisiable_9)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_spVisiable_9() const { return ___spVisiable_9; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_spVisiable_9() { return &___spVisiable_9; }
	inline void set_spVisiable_9(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___spVisiable_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spVisiable_9), (void*)value);
	}

	inline static int32_t get_offset_of_spInvisiable_10() { return static_cast<int32_t>(offsetof(Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41, ___spInvisiable_10)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_spInvisiable_10() const { return ___spInvisiable_10; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_spInvisiable_10() { return &___spInvisiable_10; }
	inline void set_spInvisiable_10(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___spInvisiable_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spInvisiable_10), (void*)value);
	}

	inline static int32_t get_offset_of_errorIcon_11() { return static_cast<int32_t>(offsetof(Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41, ___errorIcon_11)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_errorIcon_11() const { return ___errorIcon_11; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_errorIcon_11() { return &___errorIcon_11; }
	inline void set_errorIcon_11(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___errorIcon_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___errorIcon_11), (void*)value);
	}

	inline static int32_t get_offset_of_EHOpenMenuPanel_12() { return static_cast<int32_t>(offsetof(Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41, ___EHOpenMenuPanel_12)); }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * get_EHOpenMenuPanel_12() const { return ___EHOpenMenuPanel_12; }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B ** get_address_of_EHOpenMenuPanel_12() { return &___EHOpenMenuPanel_12; }
	inline void set_EHOpenMenuPanel_12(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * value)
	{
		___EHOpenMenuPanel_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EHOpenMenuPanel_12), (void*)value);
	}
};


// Panel_Menu
struct Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Button Panel_Menu::btAllInOne
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btAllInOne_4;
	// UnityEngine.UI.Button Panel_Menu::btDataRecord
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btDataRecord_5;
	// UnityEngine.UI.Button Panel_Menu::btLogout
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___btLogout_6;
	// System.EventHandler Panel_Menu::EHOpenAllInOne
	EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___EHOpenAllInOne_7;
	// System.EventHandler Panel_Menu::EHOpenDataRecord
	EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___EHOpenDataRecord_8;
	// System.EventHandler Panel_Menu::EHLogout
	EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___EHLogout_9;

public:
	inline static int32_t get_offset_of_btAllInOne_4() { return static_cast<int32_t>(offsetof(Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412, ___btAllInOne_4)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btAllInOne_4() const { return ___btAllInOne_4; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btAllInOne_4() { return &___btAllInOne_4; }
	inline void set_btAllInOne_4(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btAllInOne_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btAllInOne_4), (void*)value);
	}

	inline static int32_t get_offset_of_btDataRecord_5() { return static_cast<int32_t>(offsetof(Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412, ___btDataRecord_5)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btDataRecord_5() const { return ___btDataRecord_5; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btDataRecord_5() { return &___btDataRecord_5; }
	inline void set_btDataRecord_5(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btDataRecord_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btDataRecord_5), (void*)value);
	}

	inline static int32_t get_offset_of_btLogout_6() { return static_cast<int32_t>(offsetof(Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412, ___btLogout_6)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_btLogout_6() const { return ___btLogout_6; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_btLogout_6() { return &___btLogout_6; }
	inline void set_btLogout_6(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___btLogout_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___btLogout_6), (void*)value);
	}

	inline static int32_t get_offset_of_EHOpenAllInOne_7() { return static_cast<int32_t>(offsetof(Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412, ___EHOpenAllInOne_7)); }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * get_EHOpenAllInOne_7() const { return ___EHOpenAllInOne_7; }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B ** get_address_of_EHOpenAllInOne_7() { return &___EHOpenAllInOne_7; }
	inline void set_EHOpenAllInOne_7(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * value)
	{
		___EHOpenAllInOne_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EHOpenAllInOne_7), (void*)value);
	}

	inline static int32_t get_offset_of_EHOpenDataRecord_8() { return static_cast<int32_t>(offsetof(Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412, ___EHOpenDataRecord_8)); }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * get_EHOpenDataRecord_8() const { return ___EHOpenDataRecord_8; }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B ** get_address_of_EHOpenDataRecord_8() { return &___EHOpenDataRecord_8; }
	inline void set_EHOpenDataRecord_8(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * value)
	{
		___EHOpenDataRecord_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EHOpenDataRecord_8), (void*)value);
	}

	inline static int32_t get_offset_of_EHLogout_9() { return static_cast<int32_t>(offsetof(Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412, ___EHLogout_9)); }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * get_EHLogout_9() const { return ___EHLogout_9; }
	inline EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B ** get_address_of_EHLogout_9() { return &___EHLogout_9; }
	inline void set_EHLogout_9(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * value)
	{
		___EHLogout_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EHLogout_9), (void*)value);
	}
};


// PassthroughStyler
struct PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// OVRInput/Controller PassthroughStyler::controllerHand
	int32_t ___controllerHand_4;
	// OVRPassthroughLayer PassthroughStyler::passthroughLayer
	OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * ___passthroughLayer_5;
	// System.Collections.IEnumerator PassthroughStyler::fadeIn
	RuntimeObject* ___fadeIn_6;
	// System.Collections.IEnumerator PassthroughStyler::fadeOut
	RuntimeObject* ___fadeOut_7;
	// UnityEngine.RectTransform[] PassthroughStyler::menuOptions
	RectTransformU5BU5D_tA38C18F6D88709B30F107C43E0669847172879D5* ___menuOptions_8;
	// UnityEngine.RectTransform PassthroughStyler::colorWheel
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___colorWheel_9;
	// UnityEngine.Texture2D PassthroughStyler::colorTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___colorTexture_10;
	// UnityEngine.Vector3 PassthroughStyler::cursorPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cursorPosition_11;
	// System.Boolean PassthroughStyler::settingColor
	bool ___settingColor_12;
	// UnityEngine.Color PassthroughStyler::savedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___savedColor_13;
	// System.Single PassthroughStyler::savedBrightness
	float ___savedBrightness_14;
	// System.Single PassthroughStyler::savedContrast
	float ___savedContrast_15;
	// System.Single PassthroughStyler::savedPosterize
	float ___savedPosterize_16;
	// UnityEngine.CanvasGroup PassthroughStyler::mainCanvas
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___mainCanvas_17;
	// UnityEngine.GameObject[] PassthroughStyler::compactObjects
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___compactObjects_18;

public:
	inline static int32_t get_offset_of_controllerHand_4() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___controllerHand_4)); }
	inline int32_t get_controllerHand_4() const { return ___controllerHand_4; }
	inline int32_t* get_address_of_controllerHand_4() { return &___controllerHand_4; }
	inline void set_controllerHand_4(int32_t value)
	{
		___controllerHand_4 = value;
	}

	inline static int32_t get_offset_of_passthroughLayer_5() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___passthroughLayer_5)); }
	inline OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * get_passthroughLayer_5() const { return ___passthroughLayer_5; }
	inline OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB ** get_address_of_passthroughLayer_5() { return &___passthroughLayer_5; }
	inline void set_passthroughLayer_5(OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * value)
	{
		___passthroughLayer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___passthroughLayer_5), (void*)value);
	}

	inline static int32_t get_offset_of_fadeIn_6() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___fadeIn_6)); }
	inline RuntimeObject* get_fadeIn_6() const { return ___fadeIn_6; }
	inline RuntimeObject** get_address_of_fadeIn_6() { return &___fadeIn_6; }
	inline void set_fadeIn_6(RuntimeObject* value)
	{
		___fadeIn_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fadeIn_6), (void*)value);
	}

	inline static int32_t get_offset_of_fadeOut_7() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___fadeOut_7)); }
	inline RuntimeObject* get_fadeOut_7() const { return ___fadeOut_7; }
	inline RuntimeObject** get_address_of_fadeOut_7() { return &___fadeOut_7; }
	inline void set_fadeOut_7(RuntimeObject* value)
	{
		___fadeOut_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fadeOut_7), (void*)value);
	}

	inline static int32_t get_offset_of_menuOptions_8() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___menuOptions_8)); }
	inline RectTransformU5BU5D_tA38C18F6D88709B30F107C43E0669847172879D5* get_menuOptions_8() const { return ___menuOptions_8; }
	inline RectTransformU5BU5D_tA38C18F6D88709B30F107C43E0669847172879D5** get_address_of_menuOptions_8() { return &___menuOptions_8; }
	inline void set_menuOptions_8(RectTransformU5BU5D_tA38C18F6D88709B30F107C43E0669847172879D5* value)
	{
		___menuOptions_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuOptions_8), (void*)value);
	}

	inline static int32_t get_offset_of_colorWheel_9() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___colorWheel_9)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_colorWheel_9() const { return ___colorWheel_9; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_colorWheel_9() { return &___colorWheel_9; }
	inline void set_colorWheel_9(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___colorWheel_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorWheel_9), (void*)value);
	}

	inline static int32_t get_offset_of_colorTexture_10() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___colorTexture_10)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_colorTexture_10() const { return ___colorTexture_10; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_colorTexture_10() { return &___colorTexture_10; }
	inline void set_colorTexture_10(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___colorTexture_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorTexture_10), (void*)value);
	}

	inline static int32_t get_offset_of_cursorPosition_11() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___cursorPosition_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cursorPosition_11() const { return ___cursorPosition_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cursorPosition_11() { return &___cursorPosition_11; }
	inline void set_cursorPosition_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cursorPosition_11 = value;
	}

	inline static int32_t get_offset_of_settingColor_12() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___settingColor_12)); }
	inline bool get_settingColor_12() const { return ___settingColor_12; }
	inline bool* get_address_of_settingColor_12() { return &___settingColor_12; }
	inline void set_settingColor_12(bool value)
	{
		___settingColor_12 = value;
	}

	inline static int32_t get_offset_of_savedColor_13() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___savedColor_13)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_savedColor_13() const { return ___savedColor_13; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_savedColor_13() { return &___savedColor_13; }
	inline void set_savedColor_13(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___savedColor_13 = value;
	}

	inline static int32_t get_offset_of_savedBrightness_14() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___savedBrightness_14)); }
	inline float get_savedBrightness_14() const { return ___savedBrightness_14; }
	inline float* get_address_of_savedBrightness_14() { return &___savedBrightness_14; }
	inline void set_savedBrightness_14(float value)
	{
		___savedBrightness_14 = value;
	}

	inline static int32_t get_offset_of_savedContrast_15() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___savedContrast_15)); }
	inline float get_savedContrast_15() const { return ___savedContrast_15; }
	inline float* get_address_of_savedContrast_15() { return &___savedContrast_15; }
	inline void set_savedContrast_15(float value)
	{
		___savedContrast_15 = value;
	}

	inline static int32_t get_offset_of_savedPosterize_16() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___savedPosterize_16)); }
	inline float get_savedPosterize_16() const { return ___savedPosterize_16; }
	inline float* get_address_of_savedPosterize_16() { return &___savedPosterize_16; }
	inline void set_savedPosterize_16(float value)
	{
		___savedPosterize_16 = value;
	}

	inline static int32_t get_offset_of_mainCanvas_17() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___mainCanvas_17)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_mainCanvas_17() const { return ___mainCanvas_17; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_mainCanvas_17() { return &___mainCanvas_17; }
	inline void set_mainCanvas_17(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___mainCanvas_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainCanvas_17), (void*)value);
	}

	inline static int32_t get_offset_of_compactObjects_18() { return static_cast<int32_t>(offsetof(PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32, ___compactObjects_18)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_compactObjects_18() const { return ___compactObjects_18; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_compactObjects_18() { return &___compactObjects_18; }
	inline void set_compactObjects_18(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___compactObjects_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___compactObjects_18), (void*)value);
	}
};


// RuntimeRopeGeneratorUse
struct RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Obi.ObiCollider RuntimeRopeGeneratorUse::pendulum
	ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9 * ___pendulum_4;
	// RuntimeRopeGenerator RuntimeRopeGeneratorUse::rg
	RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * ___rg_5;

public:
	inline static int32_t get_offset_of_pendulum_4() { return static_cast<int32_t>(offsetof(RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC, ___pendulum_4)); }
	inline ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9 * get_pendulum_4() const { return ___pendulum_4; }
	inline ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9 ** get_address_of_pendulum_4() { return &___pendulum_4; }
	inline void set_pendulum_4(ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9 * value)
	{
		___pendulum_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pendulum_4), (void*)value);
	}

	inline static int32_t get_offset_of_rg_5() { return static_cast<int32_t>(offsetof(RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC, ___rg_5)); }
	inline RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * get_rg_5() const { return ___rg_5; }
	inline RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 ** get_address_of_rg_5() { return &___rg_5; }
	inline void set_rg_5(RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * value)
	{
		___rg_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rg_5), (void*)value);
	}
};


// ServerManagerUI
struct ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Panel_Login ServerManagerUI::panelLogin
	Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41 * ___panelLogin_4;
	// Panel_Menu ServerManagerUI::panelMenu
	Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 * ___panelMenu_5;
	// Panel_AllInOne ServerManagerUI::panelAllInOne
	Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D * ___panelAllInOne_6;
	// Panel_DataRecord ServerManagerUI::panelDataRecord
	Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57 * ___panelDataRecord_7;

public:
	inline static int32_t get_offset_of_panelLogin_4() { return static_cast<int32_t>(offsetof(ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D, ___panelLogin_4)); }
	inline Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41 * get_panelLogin_4() const { return ___panelLogin_4; }
	inline Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41 ** get_address_of_panelLogin_4() { return &___panelLogin_4; }
	inline void set_panelLogin_4(Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41 * value)
	{
		___panelLogin_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___panelLogin_4), (void*)value);
	}

	inline static int32_t get_offset_of_panelMenu_5() { return static_cast<int32_t>(offsetof(ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D, ___panelMenu_5)); }
	inline Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 * get_panelMenu_5() const { return ___panelMenu_5; }
	inline Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 ** get_address_of_panelMenu_5() { return &___panelMenu_5; }
	inline void set_panelMenu_5(Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 * value)
	{
		___panelMenu_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___panelMenu_5), (void*)value);
	}

	inline static int32_t get_offset_of_panelAllInOne_6() { return static_cast<int32_t>(offsetof(ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D, ___panelAllInOne_6)); }
	inline Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D * get_panelAllInOne_6() const { return ___panelAllInOne_6; }
	inline Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D ** get_address_of_panelAllInOne_6() { return &___panelAllInOne_6; }
	inline void set_panelAllInOne_6(Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D * value)
	{
		___panelAllInOne_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___panelAllInOne_6), (void*)value);
	}

	inline static int32_t get_offset_of_panelDataRecord_7() { return static_cast<int32_t>(offsetof(ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D, ___panelDataRecord_7)); }
	inline Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57 * get_panelDataRecord_7() const { return ___panelDataRecord_7; }
	inline Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57 ** get_address_of_panelDataRecord_7() { return &___panelDataRecord_7; }
	inline void set_panelDataRecord_7(Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57 * value)
	{
		___panelDataRecord_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___panelDataRecord_7), (void*)value);
	}
};


// StartMenu
struct StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// OVROverlay StartMenu::overlay
	OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 * ___overlay_4;
	// OVROverlay StartMenu::text
	OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 * ___text_5;
	// OVRCameraRig StartMenu::vrRig
	OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * ___vrRig_6;

public:
	inline static int32_t get_offset_of_overlay_4() { return static_cast<int32_t>(offsetof(StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3, ___overlay_4)); }
	inline OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 * get_overlay_4() const { return ___overlay_4; }
	inline OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 ** get_address_of_overlay_4() { return &___overlay_4; }
	inline void set_overlay_4(OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 * value)
	{
		___overlay_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___overlay_4), (void*)value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3, ___text_5)); }
	inline OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 * get_text_5() const { return ___text_5; }
	inline OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(OVROverlay_t38325187DABEB39EACE0C5FA1102F149CB9B3BF7 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___text_5), (void*)value);
	}

	inline static int32_t get_offset_of_vrRig_6() { return static_cast<int32_t>(offsetof(StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3, ___vrRig_6)); }
	inline OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * get_vrRig_6() const { return ___vrRig_6; }
	inline OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 ** get_address_of_vrRig_6() { return &___vrRig_6; }
	inline void set_vrRig_6(OVRCameraRig_t743037A817D9B163CBAAA707F8B74E095D77A517 * value)
	{
		___vrRig_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vrRig_6), (void*)value);
	}
};


// TargetProjectionMatrix
struct TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Camera TargetProjectionMatrix::referenceCam
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___referenceCam_4;
	// UnityEngine.Camera TargetProjectionMatrix::targetCam
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___targetCam_5;
	// System.Boolean TargetProjectionMatrix::useCustomFOV
	bool ___useCustomFOV_6;
	// System.Single TargetProjectionMatrix::fov
	float ___fov_7;
	// System.Boolean TargetProjectionMatrix::maxFovAsReference
	bool ___maxFovAsReference_8;
	// System.Boolean TargetProjectionMatrix::allowUpdate
	bool ___allowUpdate_9;
	// System.Boolean TargetProjectionMatrix::ForceDisableUpdate
	bool ___ForceDisableUpdate_10;

public:
	inline static int32_t get_offset_of_referenceCam_4() { return static_cast<int32_t>(offsetof(TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F, ___referenceCam_4)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_referenceCam_4() const { return ___referenceCam_4; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_referenceCam_4() { return &___referenceCam_4; }
	inline void set_referenceCam_4(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___referenceCam_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___referenceCam_4), (void*)value);
	}

	inline static int32_t get_offset_of_targetCam_5() { return static_cast<int32_t>(offsetof(TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F, ___targetCam_5)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_targetCam_5() const { return ___targetCam_5; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_targetCam_5() { return &___targetCam_5; }
	inline void set_targetCam_5(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___targetCam_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetCam_5), (void*)value);
	}

	inline static int32_t get_offset_of_useCustomFOV_6() { return static_cast<int32_t>(offsetof(TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F, ___useCustomFOV_6)); }
	inline bool get_useCustomFOV_6() const { return ___useCustomFOV_6; }
	inline bool* get_address_of_useCustomFOV_6() { return &___useCustomFOV_6; }
	inline void set_useCustomFOV_6(bool value)
	{
		___useCustomFOV_6 = value;
	}

	inline static int32_t get_offset_of_fov_7() { return static_cast<int32_t>(offsetof(TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F, ___fov_7)); }
	inline float get_fov_7() const { return ___fov_7; }
	inline float* get_address_of_fov_7() { return &___fov_7; }
	inline void set_fov_7(float value)
	{
		___fov_7 = value;
	}

	inline static int32_t get_offset_of_maxFovAsReference_8() { return static_cast<int32_t>(offsetof(TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F, ___maxFovAsReference_8)); }
	inline bool get_maxFovAsReference_8() const { return ___maxFovAsReference_8; }
	inline bool* get_address_of_maxFovAsReference_8() { return &___maxFovAsReference_8; }
	inline void set_maxFovAsReference_8(bool value)
	{
		___maxFovAsReference_8 = value;
	}

	inline static int32_t get_offset_of_allowUpdate_9() { return static_cast<int32_t>(offsetof(TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F, ___allowUpdate_9)); }
	inline bool get_allowUpdate_9() const { return ___allowUpdate_9; }
	inline bool* get_address_of_allowUpdate_9() { return &___allowUpdate_9; }
	inline void set_allowUpdate_9(bool value)
	{
		___allowUpdate_9 = value;
	}

	inline static int32_t get_offset_of_ForceDisableUpdate_10() { return static_cast<int32_t>(offsetof(TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F, ___ForceDisableUpdate_10)); }
	inline bool get_ForceDisableUpdate_10() const { return ___ForceDisableUpdate_10; }
	inline bool* get_address_of_ForceDisableUpdate_10() { return &___ForceDisableUpdate_10; }
	inline void set_ForceDisableUpdate_10(bool value)
	{
		___ForceDisableUpdate_10 = value;
	}
};


// TeleportSupport
struct TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// LocomotionTeleport TeleportSupport::<LocomotionTeleport>k__BackingField
	LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * ___U3CLocomotionTeleportU3Ek__BackingField_4;
	// System.Boolean TeleportSupport::_eventsActive
	bool ____eventsActive_5;

public:
	inline static int32_t get_offset_of_U3CLocomotionTeleportU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121, ___U3CLocomotionTeleportU3Ek__BackingField_4)); }
	inline LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * get_U3CLocomotionTeleportU3Ek__BackingField_4() const { return ___U3CLocomotionTeleportU3Ek__BackingField_4; }
	inline LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 ** get_address_of_U3CLocomotionTeleportU3Ek__BackingField_4() { return &___U3CLocomotionTeleportU3Ek__BackingField_4; }
	inline void set_U3CLocomotionTeleportU3Ek__BackingField_4(LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * value)
	{
		___U3CLocomotionTeleportU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLocomotionTeleportU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of__eventsActive_5() { return static_cast<int32_t>(offsetof(TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121, ____eventsActive_5)); }
	inline bool get__eventsActive_5() const { return ____eventsActive_5; }
	inline bool* get_address_of__eventsActive_5() { return &____eventsActive_5; }
	inline void set__eventsActive_5(bool value)
	{
		____eventsActive_5 = value;
	}
};


// TextureEncoder
struct TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// FMTextureType TextureEncoder::TextureType
	int32_t ___TextureType_4;
	// UnityEngine.Texture2D TextureEncoder::StreamTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___StreamTexture_5;
	// UnityEngine.RenderTexture TextureEncoder::StreamRenderTexture
	RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * ___StreamRenderTexture_6;
	// UnityEngine.WebCamTexture TextureEncoder::StreamWebCamTexture
	WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * ___StreamWebCamTexture_7;
	// UnityEngine.RenderTexture TextureEncoder::StreamWebCamRenderTexture
	RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * ___StreamWebCamRenderTexture_8;
	// System.Single TextureEncoder::ResolutionScalar
	float ___ResolutionScalar_9;
	// System.Boolean TextureEncoder::FastMode
	bool ___FastMode_10;
	// System.Boolean TextureEncoder::AsyncMode
	bool ___AsyncMode_11;
	// System.Boolean TextureEncoder::GZipMode
	bool ___GZipMode_12;
	// System.Int32 TextureEncoder::Quality
	int32_t ___Quality_13;
	// FMChromaSubsamplingOption TextureEncoder::ChromaSubsampling
	int32_t ___ChromaSubsampling_14;
	// System.Single TextureEncoder::StreamFPS
	float ___StreamFPS_15;
	// System.Single TextureEncoder::interval
	float ___interval_16;
	// System.Boolean TextureEncoder::ignoreSimilarTexture
	bool ___ignoreSimilarTexture_17;
	// System.Int32 TextureEncoder::lastRawDataByte
	int32_t ___lastRawDataByte_18;
	// System.Int32 TextureEncoder::similarByteSizeThreshold
	int32_t ___similarByteSizeThreshold_19;
	// System.Boolean TextureEncoder::NeedUpdateTexture
	bool ___NeedUpdateTexture_20;
	// System.Boolean TextureEncoder::EncodingTexture
	bool ___EncodingTexture_21;
	// System.Boolean TextureEncoder::supportsAsyncGPUReadback
	bool ___supportsAsyncGPUReadback_22;
	// System.Boolean TextureEncoder::EnableAsyncGPUReadback
	bool ___EnableAsyncGPUReadback_23;
	// UnityEventByteArray TextureEncoder::OnDataByteReadyEvent
	UnityEventByteArray_tD5103CBD7F77D5C7683025D9BCE91819B3E37F16 * ___OnDataByteReadyEvent_24;
	// System.Int32 TextureEncoder::label
	int32_t ___label_25;
	// System.Int32 TextureEncoder::dataID
	int32_t ___dataID_26;
	// System.Int32 TextureEncoder::maxID
	int32_t ___maxID_27;
	// System.Int32 TextureEncoder::chunkSize
	int32_t ___chunkSize_28;
	// System.Single TextureEncoder::next
	float ___next_29;
	// System.Boolean TextureEncoder::stop
	bool ___stop_30;
	// System.Byte[] TextureEncoder::dataByte
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___dataByte_31;
	// System.Int32 TextureEncoder::dataLength
	int32_t ___dataLength_32;
	// System.Byte[] TextureEncoder::RawTextureData
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___RawTextureData_33;
	// System.Int32 TextureEncoder::streamWidth
	int32_t ___streamWidth_34;
	// System.Int32 TextureEncoder::streamHeight
	int32_t ___streamHeight_35;

public:
	inline static int32_t get_offset_of_TextureType_4() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___TextureType_4)); }
	inline int32_t get_TextureType_4() const { return ___TextureType_4; }
	inline int32_t* get_address_of_TextureType_4() { return &___TextureType_4; }
	inline void set_TextureType_4(int32_t value)
	{
		___TextureType_4 = value;
	}

	inline static int32_t get_offset_of_StreamTexture_5() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___StreamTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_StreamTexture_5() const { return ___StreamTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_StreamTexture_5() { return &___StreamTexture_5; }
	inline void set_StreamTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___StreamTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StreamTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_StreamRenderTexture_6() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___StreamRenderTexture_6)); }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * get_StreamRenderTexture_6() const { return ___StreamRenderTexture_6; }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** get_address_of_StreamRenderTexture_6() { return &___StreamRenderTexture_6; }
	inline void set_StreamRenderTexture_6(RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * value)
	{
		___StreamRenderTexture_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StreamRenderTexture_6), (void*)value);
	}

	inline static int32_t get_offset_of_StreamWebCamTexture_7() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___StreamWebCamTexture_7)); }
	inline WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * get_StreamWebCamTexture_7() const { return ___StreamWebCamTexture_7; }
	inline WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 ** get_address_of_StreamWebCamTexture_7() { return &___StreamWebCamTexture_7; }
	inline void set_StreamWebCamTexture_7(WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * value)
	{
		___StreamWebCamTexture_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StreamWebCamTexture_7), (void*)value);
	}

	inline static int32_t get_offset_of_StreamWebCamRenderTexture_8() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___StreamWebCamRenderTexture_8)); }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * get_StreamWebCamRenderTexture_8() const { return ___StreamWebCamRenderTexture_8; }
	inline RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 ** get_address_of_StreamWebCamRenderTexture_8() { return &___StreamWebCamRenderTexture_8; }
	inline void set_StreamWebCamRenderTexture_8(RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * value)
	{
		___StreamWebCamRenderTexture_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StreamWebCamRenderTexture_8), (void*)value);
	}

	inline static int32_t get_offset_of_ResolutionScalar_9() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___ResolutionScalar_9)); }
	inline float get_ResolutionScalar_9() const { return ___ResolutionScalar_9; }
	inline float* get_address_of_ResolutionScalar_9() { return &___ResolutionScalar_9; }
	inline void set_ResolutionScalar_9(float value)
	{
		___ResolutionScalar_9 = value;
	}

	inline static int32_t get_offset_of_FastMode_10() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___FastMode_10)); }
	inline bool get_FastMode_10() const { return ___FastMode_10; }
	inline bool* get_address_of_FastMode_10() { return &___FastMode_10; }
	inline void set_FastMode_10(bool value)
	{
		___FastMode_10 = value;
	}

	inline static int32_t get_offset_of_AsyncMode_11() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___AsyncMode_11)); }
	inline bool get_AsyncMode_11() const { return ___AsyncMode_11; }
	inline bool* get_address_of_AsyncMode_11() { return &___AsyncMode_11; }
	inline void set_AsyncMode_11(bool value)
	{
		___AsyncMode_11 = value;
	}

	inline static int32_t get_offset_of_GZipMode_12() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___GZipMode_12)); }
	inline bool get_GZipMode_12() const { return ___GZipMode_12; }
	inline bool* get_address_of_GZipMode_12() { return &___GZipMode_12; }
	inline void set_GZipMode_12(bool value)
	{
		___GZipMode_12 = value;
	}

	inline static int32_t get_offset_of_Quality_13() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___Quality_13)); }
	inline int32_t get_Quality_13() const { return ___Quality_13; }
	inline int32_t* get_address_of_Quality_13() { return &___Quality_13; }
	inline void set_Quality_13(int32_t value)
	{
		___Quality_13 = value;
	}

	inline static int32_t get_offset_of_ChromaSubsampling_14() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___ChromaSubsampling_14)); }
	inline int32_t get_ChromaSubsampling_14() const { return ___ChromaSubsampling_14; }
	inline int32_t* get_address_of_ChromaSubsampling_14() { return &___ChromaSubsampling_14; }
	inline void set_ChromaSubsampling_14(int32_t value)
	{
		___ChromaSubsampling_14 = value;
	}

	inline static int32_t get_offset_of_StreamFPS_15() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___StreamFPS_15)); }
	inline float get_StreamFPS_15() const { return ___StreamFPS_15; }
	inline float* get_address_of_StreamFPS_15() { return &___StreamFPS_15; }
	inline void set_StreamFPS_15(float value)
	{
		___StreamFPS_15 = value;
	}

	inline static int32_t get_offset_of_interval_16() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___interval_16)); }
	inline float get_interval_16() const { return ___interval_16; }
	inline float* get_address_of_interval_16() { return &___interval_16; }
	inline void set_interval_16(float value)
	{
		___interval_16 = value;
	}

	inline static int32_t get_offset_of_ignoreSimilarTexture_17() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___ignoreSimilarTexture_17)); }
	inline bool get_ignoreSimilarTexture_17() const { return ___ignoreSimilarTexture_17; }
	inline bool* get_address_of_ignoreSimilarTexture_17() { return &___ignoreSimilarTexture_17; }
	inline void set_ignoreSimilarTexture_17(bool value)
	{
		___ignoreSimilarTexture_17 = value;
	}

	inline static int32_t get_offset_of_lastRawDataByte_18() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___lastRawDataByte_18)); }
	inline int32_t get_lastRawDataByte_18() const { return ___lastRawDataByte_18; }
	inline int32_t* get_address_of_lastRawDataByte_18() { return &___lastRawDataByte_18; }
	inline void set_lastRawDataByte_18(int32_t value)
	{
		___lastRawDataByte_18 = value;
	}

	inline static int32_t get_offset_of_similarByteSizeThreshold_19() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___similarByteSizeThreshold_19)); }
	inline int32_t get_similarByteSizeThreshold_19() const { return ___similarByteSizeThreshold_19; }
	inline int32_t* get_address_of_similarByteSizeThreshold_19() { return &___similarByteSizeThreshold_19; }
	inline void set_similarByteSizeThreshold_19(int32_t value)
	{
		___similarByteSizeThreshold_19 = value;
	}

	inline static int32_t get_offset_of_NeedUpdateTexture_20() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___NeedUpdateTexture_20)); }
	inline bool get_NeedUpdateTexture_20() const { return ___NeedUpdateTexture_20; }
	inline bool* get_address_of_NeedUpdateTexture_20() { return &___NeedUpdateTexture_20; }
	inline void set_NeedUpdateTexture_20(bool value)
	{
		___NeedUpdateTexture_20 = value;
	}

	inline static int32_t get_offset_of_EncodingTexture_21() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___EncodingTexture_21)); }
	inline bool get_EncodingTexture_21() const { return ___EncodingTexture_21; }
	inline bool* get_address_of_EncodingTexture_21() { return &___EncodingTexture_21; }
	inline void set_EncodingTexture_21(bool value)
	{
		___EncodingTexture_21 = value;
	}

	inline static int32_t get_offset_of_supportsAsyncGPUReadback_22() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___supportsAsyncGPUReadback_22)); }
	inline bool get_supportsAsyncGPUReadback_22() const { return ___supportsAsyncGPUReadback_22; }
	inline bool* get_address_of_supportsAsyncGPUReadback_22() { return &___supportsAsyncGPUReadback_22; }
	inline void set_supportsAsyncGPUReadback_22(bool value)
	{
		___supportsAsyncGPUReadback_22 = value;
	}

	inline static int32_t get_offset_of_EnableAsyncGPUReadback_23() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___EnableAsyncGPUReadback_23)); }
	inline bool get_EnableAsyncGPUReadback_23() const { return ___EnableAsyncGPUReadback_23; }
	inline bool* get_address_of_EnableAsyncGPUReadback_23() { return &___EnableAsyncGPUReadback_23; }
	inline void set_EnableAsyncGPUReadback_23(bool value)
	{
		___EnableAsyncGPUReadback_23 = value;
	}

	inline static int32_t get_offset_of_OnDataByteReadyEvent_24() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___OnDataByteReadyEvent_24)); }
	inline UnityEventByteArray_tD5103CBD7F77D5C7683025D9BCE91819B3E37F16 * get_OnDataByteReadyEvent_24() const { return ___OnDataByteReadyEvent_24; }
	inline UnityEventByteArray_tD5103CBD7F77D5C7683025D9BCE91819B3E37F16 ** get_address_of_OnDataByteReadyEvent_24() { return &___OnDataByteReadyEvent_24; }
	inline void set_OnDataByteReadyEvent_24(UnityEventByteArray_tD5103CBD7F77D5C7683025D9BCE91819B3E37F16 * value)
	{
		___OnDataByteReadyEvent_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnDataByteReadyEvent_24), (void*)value);
	}

	inline static int32_t get_offset_of_label_25() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___label_25)); }
	inline int32_t get_label_25() const { return ___label_25; }
	inline int32_t* get_address_of_label_25() { return &___label_25; }
	inline void set_label_25(int32_t value)
	{
		___label_25 = value;
	}

	inline static int32_t get_offset_of_dataID_26() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___dataID_26)); }
	inline int32_t get_dataID_26() const { return ___dataID_26; }
	inline int32_t* get_address_of_dataID_26() { return &___dataID_26; }
	inline void set_dataID_26(int32_t value)
	{
		___dataID_26 = value;
	}

	inline static int32_t get_offset_of_maxID_27() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___maxID_27)); }
	inline int32_t get_maxID_27() const { return ___maxID_27; }
	inline int32_t* get_address_of_maxID_27() { return &___maxID_27; }
	inline void set_maxID_27(int32_t value)
	{
		___maxID_27 = value;
	}

	inline static int32_t get_offset_of_chunkSize_28() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___chunkSize_28)); }
	inline int32_t get_chunkSize_28() const { return ___chunkSize_28; }
	inline int32_t* get_address_of_chunkSize_28() { return &___chunkSize_28; }
	inline void set_chunkSize_28(int32_t value)
	{
		___chunkSize_28 = value;
	}

	inline static int32_t get_offset_of_next_29() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___next_29)); }
	inline float get_next_29() const { return ___next_29; }
	inline float* get_address_of_next_29() { return &___next_29; }
	inline void set_next_29(float value)
	{
		___next_29 = value;
	}

	inline static int32_t get_offset_of_stop_30() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___stop_30)); }
	inline bool get_stop_30() const { return ___stop_30; }
	inline bool* get_address_of_stop_30() { return &___stop_30; }
	inline void set_stop_30(bool value)
	{
		___stop_30 = value;
	}

	inline static int32_t get_offset_of_dataByte_31() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___dataByte_31)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_dataByte_31() const { return ___dataByte_31; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_dataByte_31() { return &___dataByte_31; }
	inline void set_dataByte_31(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___dataByte_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dataByte_31), (void*)value);
	}

	inline static int32_t get_offset_of_dataLength_32() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___dataLength_32)); }
	inline int32_t get_dataLength_32() const { return ___dataLength_32; }
	inline int32_t* get_address_of_dataLength_32() { return &___dataLength_32; }
	inline void set_dataLength_32(int32_t value)
	{
		___dataLength_32 = value;
	}

	inline static int32_t get_offset_of_RawTextureData_33() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___RawTextureData_33)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_RawTextureData_33() const { return ___RawTextureData_33; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_RawTextureData_33() { return &___RawTextureData_33; }
	inline void set_RawTextureData_33(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___RawTextureData_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RawTextureData_33), (void*)value);
	}

	inline static int32_t get_offset_of_streamWidth_34() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___streamWidth_34)); }
	inline int32_t get_streamWidth_34() const { return ___streamWidth_34; }
	inline int32_t* get_address_of_streamWidth_34() { return &___streamWidth_34; }
	inline void set_streamWidth_34(int32_t value)
	{
		___streamWidth_34 = value;
	}

	inline static int32_t get_offset_of_streamHeight_35() { return static_cast<int32_t>(offsetof(TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA, ___streamHeight_35)); }
	inline int32_t get_streamHeight_35() const { return ___streamHeight_35; }
	inline int32_t* get_address_of_streamHeight_35() { return &___streamHeight_35; }
	inline void set_streamHeight_35(int32_t value)
	{
		___streamHeight_35 = value;
	}
};


// OculusSampleFramework.TrainButtonVisualController
struct TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.MeshRenderer OculusSampleFramework.TrainButtonVisualController::_meshRenderer
	MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * ____meshRenderer_6;
	// UnityEngine.MeshRenderer OculusSampleFramework.TrainButtonVisualController::_glowRenderer
	MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * ____glowRenderer_7;
	// OculusSampleFramework.ButtonController OculusSampleFramework.TrainButtonVisualController::_buttonController
	ButtonController_t8B501737B40B4D8F465F215CAC449B627A8EF000 * ____buttonController_8;
	// UnityEngine.Color OculusSampleFramework.TrainButtonVisualController::_buttonContactColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ____buttonContactColor_9;
	// UnityEngine.Color OculusSampleFramework.TrainButtonVisualController::_buttonActionColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ____buttonActionColor_10;
	// UnityEngine.AudioSource OculusSampleFramework.TrainButtonVisualController::_audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ____audioSource_11;
	// UnityEngine.AudioClip OculusSampleFramework.TrainButtonVisualController::_actionSoundEffect
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ____actionSoundEffect_12;
	// UnityEngine.Transform OculusSampleFramework.TrainButtonVisualController::_buttonContactTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____buttonContactTransform_13;
	// System.Single OculusSampleFramework.TrainButtonVisualController::_contactMaxDisplacementDistance
	float ____contactMaxDisplacementDistance_14;
	// UnityEngine.Material OculusSampleFramework.TrainButtonVisualController::_buttonMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ____buttonMaterial_15;
	// UnityEngine.Color OculusSampleFramework.TrainButtonVisualController::_buttonDefaultColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ____buttonDefaultColor_16;
	// System.Int32 OculusSampleFramework.TrainButtonVisualController::_materialColorId
	int32_t ____materialColorId_17;
	// System.Boolean OculusSampleFramework.TrainButtonVisualController::_buttonInContactOrActionStates
	bool ____buttonInContactOrActionStates_18;
	// UnityEngine.Coroutine OculusSampleFramework.TrainButtonVisualController::_lerpToOldPositionCr
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____lerpToOldPositionCr_19;
	// UnityEngine.Vector3 OculusSampleFramework.TrainButtonVisualController::_oldPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____oldPosition_20;

public:
	inline static int32_t get_offset_of__meshRenderer_6() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____meshRenderer_6)); }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * get__meshRenderer_6() const { return ____meshRenderer_6; }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B ** get_address_of__meshRenderer_6() { return &____meshRenderer_6; }
	inline void set__meshRenderer_6(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * value)
	{
		____meshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____meshRenderer_6), (void*)value);
	}

	inline static int32_t get_offset_of__glowRenderer_7() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____glowRenderer_7)); }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * get__glowRenderer_7() const { return ____glowRenderer_7; }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B ** get_address_of__glowRenderer_7() { return &____glowRenderer_7; }
	inline void set__glowRenderer_7(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * value)
	{
		____glowRenderer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____glowRenderer_7), (void*)value);
	}

	inline static int32_t get_offset_of__buttonController_8() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____buttonController_8)); }
	inline ButtonController_t8B501737B40B4D8F465F215CAC449B627A8EF000 * get__buttonController_8() const { return ____buttonController_8; }
	inline ButtonController_t8B501737B40B4D8F465F215CAC449B627A8EF000 ** get_address_of__buttonController_8() { return &____buttonController_8; }
	inline void set__buttonController_8(ButtonController_t8B501737B40B4D8F465F215CAC449B627A8EF000 * value)
	{
		____buttonController_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buttonController_8), (void*)value);
	}

	inline static int32_t get_offset_of__buttonContactColor_9() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____buttonContactColor_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get__buttonContactColor_9() const { return ____buttonContactColor_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of__buttonContactColor_9() { return &____buttonContactColor_9; }
	inline void set__buttonContactColor_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		____buttonContactColor_9 = value;
	}

	inline static int32_t get_offset_of__buttonActionColor_10() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____buttonActionColor_10)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get__buttonActionColor_10() const { return ____buttonActionColor_10; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of__buttonActionColor_10() { return &____buttonActionColor_10; }
	inline void set__buttonActionColor_10(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		____buttonActionColor_10 = value;
	}

	inline static int32_t get_offset_of__audioSource_11() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____audioSource_11)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get__audioSource_11() const { return ____audioSource_11; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of__audioSource_11() { return &____audioSource_11; }
	inline void set__audioSource_11(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		____audioSource_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____audioSource_11), (void*)value);
	}

	inline static int32_t get_offset_of__actionSoundEffect_12() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____actionSoundEffect_12)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get__actionSoundEffect_12() const { return ____actionSoundEffect_12; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of__actionSoundEffect_12() { return &____actionSoundEffect_12; }
	inline void set__actionSoundEffect_12(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		____actionSoundEffect_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____actionSoundEffect_12), (void*)value);
	}

	inline static int32_t get_offset_of__buttonContactTransform_13() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____buttonContactTransform_13)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__buttonContactTransform_13() const { return ____buttonContactTransform_13; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__buttonContactTransform_13() { return &____buttonContactTransform_13; }
	inline void set__buttonContactTransform_13(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____buttonContactTransform_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buttonContactTransform_13), (void*)value);
	}

	inline static int32_t get_offset_of__contactMaxDisplacementDistance_14() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____contactMaxDisplacementDistance_14)); }
	inline float get__contactMaxDisplacementDistance_14() const { return ____contactMaxDisplacementDistance_14; }
	inline float* get_address_of__contactMaxDisplacementDistance_14() { return &____contactMaxDisplacementDistance_14; }
	inline void set__contactMaxDisplacementDistance_14(float value)
	{
		____contactMaxDisplacementDistance_14 = value;
	}

	inline static int32_t get_offset_of__buttonMaterial_15() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____buttonMaterial_15)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get__buttonMaterial_15() const { return ____buttonMaterial_15; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of__buttonMaterial_15() { return &____buttonMaterial_15; }
	inline void set__buttonMaterial_15(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		____buttonMaterial_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buttonMaterial_15), (void*)value);
	}

	inline static int32_t get_offset_of__buttonDefaultColor_16() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____buttonDefaultColor_16)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get__buttonDefaultColor_16() const { return ____buttonDefaultColor_16; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of__buttonDefaultColor_16() { return &____buttonDefaultColor_16; }
	inline void set__buttonDefaultColor_16(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		____buttonDefaultColor_16 = value;
	}

	inline static int32_t get_offset_of__materialColorId_17() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____materialColorId_17)); }
	inline int32_t get__materialColorId_17() const { return ____materialColorId_17; }
	inline int32_t* get_address_of__materialColorId_17() { return &____materialColorId_17; }
	inline void set__materialColorId_17(int32_t value)
	{
		____materialColorId_17 = value;
	}

	inline static int32_t get_offset_of__buttonInContactOrActionStates_18() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____buttonInContactOrActionStates_18)); }
	inline bool get__buttonInContactOrActionStates_18() const { return ____buttonInContactOrActionStates_18; }
	inline bool* get_address_of__buttonInContactOrActionStates_18() { return &____buttonInContactOrActionStates_18; }
	inline void set__buttonInContactOrActionStates_18(bool value)
	{
		____buttonInContactOrActionStates_18 = value;
	}

	inline static int32_t get_offset_of__lerpToOldPositionCr_19() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____lerpToOldPositionCr_19)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__lerpToOldPositionCr_19() const { return ____lerpToOldPositionCr_19; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__lerpToOldPositionCr_19() { return &____lerpToOldPositionCr_19; }
	inline void set__lerpToOldPositionCr_19(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____lerpToOldPositionCr_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lerpToOldPositionCr_19), (void*)value);
	}

	inline static int32_t get_offset_of__oldPosition_20() { return static_cast<int32_t>(offsetof(TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151, ____oldPosition_20)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__oldPosition_20() const { return ____oldPosition_20; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__oldPosition_20() { return &____oldPosition_20; }
	inline void set__oldPosition_20(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____oldPosition_20 = value;
	}
};


// OculusSampleFramework.TrainCarBase
struct TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform OculusSampleFramework.TrainCarBase::_frontWheels
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____frontWheels_7;
	// UnityEngine.Transform OculusSampleFramework.TrainCarBase::_rearWheels
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____rearWheels_8;
	// OculusSampleFramework.TrainTrack OculusSampleFramework.TrainCarBase::_trainTrack
	TrainTrack_t7BDC9E56629804928D04D3C4F7FF03D0F3409424 * ____trainTrack_9;
	// UnityEngine.Transform[] OculusSampleFramework.TrainCarBase::_individualWheels
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____individualWheels_10;
	// System.Single OculusSampleFramework.TrainCarBase::<Distance>k__BackingField
	float ___U3CDistanceU3Ek__BackingField_11;
	// System.Single OculusSampleFramework.TrainCarBase::scale
	float ___scale_12;
	// OculusSampleFramework.Pose OculusSampleFramework.TrainCarBase::_frontPose
	Pose_t8062E0F070422627AB6D1693718F8D8C38DBA742 * ____frontPose_13;
	// OculusSampleFramework.Pose OculusSampleFramework.TrainCarBase::_rearPose
	Pose_t8062E0F070422627AB6D1693718F8D8C38DBA742 * ____rearPose_14;

public:
	inline static int32_t get_offset_of__frontWheels_7() { return static_cast<int32_t>(offsetof(TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456, ____frontWheels_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__frontWheels_7() const { return ____frontWheels_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__frontWheels_7() { return &____frontWheels_7; }
	inline void set__frontWheels_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____frontWheels_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frontWheels_7), (void*)value);
	}

	inline static int32_t get_offset_of__rearWheels_8() { return static_cast<int32_t>(offsetof(TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456, ____rearWheels_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__rearWheels_8() const { return ____rearWheels_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__rearWheels_8() { return &____rearWheels_8; }
	inline void set__rearWheels_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____rearWheels_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rearWheels_8), (void*)value);
	}

	inline static int32_t get_offset_of__trainTrack_9() { return static_cast<int32_t>(offsetof(TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456, ____trainTrack_9)); }
	inline TrainTrack_t7BDC9E56629804928D04D3C4F7FF03D0F3409424 * get__trainTrack_9() const { return ____trainTrack_9; }
	inline TrainTrack_t7BDC9E56629804928D04D3C4F7FF03D0F3409424 ** get_address_of__trainTrack_9() { return &____trainTrack_9; }
	inline void set__trainTrack_9(TrainTrack_t7BDC9E56629804928D04D3C4F7FF03D0F3409424 * value)
	{
		____trainTrack_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____trainTrack_9), (void*)value);
	}

	inline static int32_t get_offset_of__individualWheels_10() { return static_cast<int32_t>(offsetof(TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456, ____individualWheels_10)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__individualWheels_10() const { return ____individualWheels_10; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__individualWheels_10() { return &____individualWheels_10; }
	inline void set__individualWheels_10(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____individualWheels_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____individualWheels_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDistanceU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456, ___U3CDistanceU3Ek__BackingField_11)); }
	inline float get_U3CDistanceU3Ek__BackingField_11() const { return ___U3CDistanceU3Ek__BackingField_11; }
	inline float* get_address_of_U3CDistanceU3Ek__BackingField_11() { return &___U3CDistanceU3Ek__BackingField_11; }
	inline void set_U3CDistanceU3Ek__BackingField_11(float value)
	{
		___U3CDistanceU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_scale_12() { return static_cast<int32_t>(offsetof(TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456, ___scale_12)); }
	inline float get_scale_12() const { return ___scale_12; }
	inline float* get_address_of_scale_12() { return &___scale_12; }
	inline void set_scale_12(float value)
	{
		___scale_12 = value;
	}

	inline static int32_t get_offset_of__frontPose_13() { return static_cast<int32_t>(offsetof(TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456, ____frontPose_13)); }
	inline Pose_t8062E0F070422627AB6D1693718F8D8C38DBA742 * get__frontPose_13() const { return ____frontPose_13; }
	inline Pose_t8062E0F070422627AB6D1693718F8D8C38DBA742 ** get_address_of__frontPose_13() { return &____frontPose_13; }
	inline void set__frontPose_13(Pose_t8062E0F070422627AB6D1693718F8D8C38DBA742 * value)
	{
		____frontPose_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frontPose_13), (void*)value);
	}

	inline static int32_t get_offset_of__rearPose_14() { return static_cast<int32_t>(offsetof(TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456, ____rearPose_14)); }
	inline Pose_t8062E0F070422627AB6D1693718F8D8C38DBA742 * get__rearPose_14() const { return ____rearPose_14; }
	inline Pose_t8062E0F070422627AB6D1693718F8D8C38DBA742 ** get_address_of__rearPose_14() { return &____rearPose_14; }
	inline void set__rearPose_14(Pose_t8062E0F070422627AB6D1693718F8D8C38DBA742 * value)
	{
		____rearPose_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rearPose_14), (void*)value);
	}
};

struct TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456_StaticFields
{
public:
	// UnityEngine.Vector3 OculusSampleFramework.TrainCarBase::OFFSET
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___OFFSET_4;

public:
	inline static int32_t get_offset_of_OFFSET_4() { return static_cast<int32_t>(offsetof(TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456_StaticFields, ___OFFSET_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_OFFSET_4() const { return ___OFFSET_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_OFFSET_4() { return &___OFFSET_4; }
	inline void set_OFFSET_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___OFFSET_4 = value;
	}
};


// OculusSampleFramework.TrainCrossingController
struct TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AudioSource OculusSampleFramework.TrainCrossingController::_audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ____audioSource_4;
	// UnityEngine.AudioClip[] OculusSampleFramework.TrainCrossingController::_crossingSounds
	AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* ____crossingSounds_5;
	// UnityEngine.MeshRenderer OculusSampleFramework.TrainCrossingController::_lightSide1Renderer
	MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * ____lightSide1Renderer_6;
	// UnityEngine.MeshRenderer OculusSampleFramework.TrainCrossingController::_lightSide2Renderer
	MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * ____lightSide2Renderer_7;
	// OculusSampleFramework.SelectionCylinder OculusSampleFramework.TrainCrossingController::_selectionCylinder
	SelectionCylinder_tD00E5523E40CEE87C221B093CB293B99706AE041 * ____selectionCylinder_8;
	// UnityEngine.Material OculusSampleFramework.TrainCrossingController::_lightsSide1Mat
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ____lightsSide1Mat_9;
	// UnityEngine.Material OculusSampleFramework.TrainCrossingController::_lightsSide2Mat
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ____lightsSide2Mat_10;
	// System.Int32 OculusSampleFramework.TrainCrossingController::_colorId
	int32_t ____colorId_11;
	// UnityEngine.Coroutine OculusSampleFramework.TrainCrossingController::_xingAnimationCr
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____xingAnimationCr_12;
	// OculusSampleFramework.InteractableTool OculusSampleFramework.TrainCrossingController::_toolInteractingWithMe
	InteractableTool_t3492A009F902935C1291A66C2D9E53DDB96B296C * ____toolInteractingWithMe_13;

public:
	inline static int32_t get_offset_of__audioSource_4() { return static_cast<int32_t>(offsetof(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37, ____audioSource_4)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get__audioSource_4() const { return ____audioSource_4; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of__audioSource_4() { return &____audioSource_4; }
	inline void set__audioSource_4(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		____audioSource_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____audioSource_4), (void*)value);
	}

	inline static int32_t get_offset_of__crossingSounds_5() { return static_cast<int32_t>(offsetof(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37, ____crossingSounds_5)); }
	inline AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* get__crossingSounds_5() const { return ____crossingSounds_5; }
	inline AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE** get_address_of__crossingSounds_5() { return &____crossingSounds_5; }
	inline void set__crossingSounds_5(AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* value)
	{
		____crossingSounds_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____crossingSounds_5), (void*)value);
	}

	inline static int32_t get_offset_of__lightSide1Renderer_6() { return static_cast<int32_t>(offsetof(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37, ____lightSide1Renderer_6)); }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * get__lightSide1Renderer_6() const { return ____lightSide1Renderer_6; }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B ** get_address_of__lightSide1Renderer_6() { return &____lightSide1Renderer_6; }
	inline void set__lightSide1Renderer_6(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * value)
	{
		____lightSide1Renderer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lightSide1Renderer_6), (void*)value);
	}

	inline static int32_t get_offset_of__lightSide2Renderer_7() { return static_cast<int32_t>(offsetof(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37, ____lightSide2Renderer_7)); }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * get__lightSide2Renderer_7() const { return ____lightSide2Renderer_7; }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B ** get_address_of__lightSide2Renderer_7() { return &____lightSide2Renderer_7; }
	inline void set__lightSide2Renderer_7(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * value)
	{
		____lightSide2Renderer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lightSide2Renderer_7), (void*)value);
	}

	inline static int32_t get_offset_of__selectionCylinder_8() { return static_cast<int32_t>(offsetof(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37, ____selectionCylinder_8)); }
	inline SelectionCylinder_tD00E5523E40CEE87C221B093CB293B99706AE041 * get__selectionCylinder_8() const { return ____selectionCylinder_8; }
	inline SelectionCylinder_tD00E5523E40CEE87C221B093CB293B99706AE041 ** get_address_of__selectionCylinder_8() { return &____selectionCylinder_8; }
	inline void set__selectionCylinder_8(SelectionCylinder_tD00E5523E40CEE87C221B093CB293B99706AE041 * value)
	{
		____selectionCylinder_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____selectionCylinder_8), (void*)value);
	}

	inline static int32_t get_offset_of__lightsSide1Mat_9() { return static_cast<int32_t>(offsetof(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37, ____lightsSide1Mat_9)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get__lightsSide1Mat_9() const { return ____lightsSide1Mat_9; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of__lightsSide1Mat_9() { return &____lightsSide1Mat_9; }
	inline void set__lightsSide1Mat_9(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		____lightsSide1Mat_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lightsSide1Mat_9), (void*)value);
	}

	inline static int32_t get_offset_of__lightsSide2Mat_10() { return static_cast<int32_t>(offsetof(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37, ____lightsSide2Mat_10)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get__lightsSide2Mat_10() const { return ____lightsSide2Mat_10; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of__lightsSide2Mat_10() { return &____lightsSide2Mat_10; }
	inline void set__lightsSide2Mat_10(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		____lightsSide2Mat_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lightsSide2Mat_10), (void*)value);
	}

	inline static int32_t get_offset_of__colorId_11() { return static_cast<int32_t>(offsetof(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37, ____colorId_11)); }
	inline int32_t get__colorId_11() const { return ____colorId_11; }
	inline int32_t* get_address_of__colorId_11() { return &____colorId_11; }
	inline void set__colorId_11(int32_t value)
	{
		____colorId_11 = value;
	}

	inline static int32_t get_offset_of__xingAnimationCr_12() { return static_cast<int32_t>(offsetof(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37, ____xingAnimationCr_12)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__xingAnimationCr_12() const { return ____xingAnimationCr_12; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__xingAnimationCr_12() { return &____xingAnimationCr_12; }
	inline void set__xingAnimationCr_12(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____xingAnimationCr_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____xingAnimationCr_12), (void*)value);
	}

	inline static int32_t get_offset_of__toolInteractingWithMe_13() { return static_cast<int32_t>(offsetof(TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37, ____toolInteractingWithMe_13)); }
	inline InteractableTool_t3492A009F902935C1291A66C2D9E53DDB96B296C * get__toolInteractingWithMe_13() const { return ____toolInteractingWithMe_13; }
	inline InteractableTool_t3492A009F902935C1291A66C2D9E53DDB96B296C ** get_address_of__toolInteractingWithMe_13() { return &____toolInteractingWithMe_13; }
	inline void set__toolInteractingWithMe_13(InteractableTool_t3492A009F902935C1291A66C2D9E53DDB96B296C * value)
	{
		____toolInteractingWithMe_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____toolInteractingWithMe_13), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// WebcamManager
struct WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.WebCamDevice[] WebcamManager::devices
	WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* ___devices_4;
	// UnityEngine.WebCamTexture[] WebcamManager::webCams
	WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983* ___webCams_5;
	// UnityEngine.Texture[] WebcamManager::textures
	TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* ___textures_6;
	// System.Int32 WebcamManager::TargetCamID
	int32_t ___TargetCamID_7;
	// System.Boolean WebcamManager::useFrontCam
	bool ___useFrontCam_8;
	// System.String WebcamManager::DeviceInfo
	String_t* ___DeviceInfo_9;
	// System.Boolean WebcamManager::canRestart
	bool ___canRestart_10;
	// UnityEngine.WebCamTexture WebcamManager::webCamTexture
	WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * ___webCamTexture_11;
	// UnityEngine.Material[] WebcamManager::materials
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ___materials_12;
	// UnityEngine.GameObject[] WebcamManager::targetMeshObjects
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___targetMeshObjects_13;
	// UnityEngine.GameObject WebcamManager::BackgroundQuad
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___BackgroundQuad_14;
	// System.Int32 WebcamManager::timeoutFrameCount
	int32_t ___timeoutFrameCount_15;
	// UnityEngine.Vector2 WebcamManager::requestResolution
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___requestResolution_16;
	// UnityEngine.Vector2 WebcamManager::textureResolution
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___textureResolution_17;
	// System.Boolean WebcamManager::isInitWaiting
	bool ___isInitWaiting_18;
	// System.Single WebcamManager::TextureRatio
	float ___TextureRatio_19;
	// System.Single WebcamManager::ScreenRatio
	float ___ScreenRatio_20;
	// UnityEngine.Quaternion WebcamManager::baseRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___baseRotation_21;
	// System.Boolean WebcamManager::isFlipped
	bool ___isFlipped_22;
	// UnityEventWebcamTexture WebcamManager::OnWebcamTextureReady
	UnityEventWebcamTexture_t73539047423B07150669F124020AA49AF9260D66 * ___OnWebcamTextureReady_23;

public:
	inline static int32_t get_offset_of_devices_4() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___devices_4)); }
	inline WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* get_devices_4() const { return ___devices_4; }
	inline WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E** get_address_of_devices_4() { return &___devices_4; }
	inline void set_devices_4(WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* value)
	{
		___devices_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___devices_4), (void*)value);
	}

	inline static int32_t get_offset_of_webCams_5() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___webCams_5)); }
	inline WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983* get_webCams_5() const { return ___webCams_5; }
	inline WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983** get_address_of_webCams_5() { return &___webCams_5; }
	inline void set_webCams_5(WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983* value)
	{
		___webCams_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___webCams_5), (void*)value);
	}

	inline static int32_t get_offset_of_textures_6() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___textures_6)); }
	inline TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* get_textures_6() const { return ___textures_6; }
	inline TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150** get_address_of_textures_6() { return &___textures_6; }
	inline void set_textures_6(TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* value)
	{
		___textures_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textures_6), (void*)value);
	}

	inline static int32_t get_offset_of_TargetCamID_7() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___TargetCamID_7)); }
	inline int32_t get_TargetCamID_7() const { return ___TargetCamID_7; }
	inline int32_t* get_address_of_TargetCamID_7() { return &___TargetCamID_7; }
	inline void set_TargetCamID_7(int32_t value)
	{
		___TargetCamID_7 = value;
	}

	inline static int32_t get_offset_of_useFrontCam_8() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___useFrontCam_8)); }
	inline bool get_useFrontCam_8() const { return ___useFrontCam_8; }
	inline bool* get_address_of_useFrontCam_8() { return &___useFrontCam_8; }
	inline void set_useFrontCam_8(bool value)
	{
		___useFrontCam_8 = value;
	}

	inline static int32_t get_offset_of_DeviceInfo_9() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___DeviceInfo_9)); }
	inline String_t* get_DeviceInfo_9() const { return ___DeviceInfo_9; }
	inline String_t** get_address_of_DeviceInfo_9() { return &___DeviceInfo_9; }
	inline void set_DeviceInfo_9(String_t* value)
	{
		___DeviceInfo_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceInfo_9), (void*)value);
	}

	inline static int32_t get_offset_of_canRestart_10() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___canRestart_10)); }
	inline bool get_canRestart_10() const { return ___canRestart_10; }
	inline bool* get_address_of_canRestart_10() { return &___canRestart_10; }
	inline void set_canRestart_10(bool value)
	{
		___canRestart_10 = value;
	}

	inline static int32_t get_offset_of_webCamTexture_11() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___webCamTexture_11)); }
	inline WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * get_webCamTexture_11() const { return ___webCamTexture_11; }
	inline WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 ** get_address_of_webCamTexture_11() { return &___webCamTexture_11; }
	inline void set_webCamTexture_11(WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * value)
	{
		___webCamTexture_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___webCamTexture_11), (void*)value);
	}

	inline static int32_t get_offset_of_materials_12() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___materials_12)); }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* get_materials_12() const { return ___materials_12; }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492** get_address_of_materials_12() { return &___materials_12; }
	inline void set_materials_12(MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* value)
	{
		___materials_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___materials_12), (void*)value);
	}

	inline static int32_t get_offset_of_targetMeshObjects_13() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___targetMeshObjects_13)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_targetMeshObjects_13() const { return ___targetMeshObjects_13; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_targetMeshObjects_13() { return &___targetMeshObjects_13; }
	inline void set_targetMeshObjects_13(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___targetMeshObjects_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetMeshObjects_13), (void*)value);
	}

	inline static int32_t get_offset_of_BackgroundQuad_14() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___BackgroundQuad_14)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_BackgroundQuad_14() const { return ___BackgroundQuad_14; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_BackgroundQuad_14() { return &___BackgroundQuad_14; }
	inline void set_BackgroundQuad_14(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___BackgroundQuad_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BackgroundQuad_14), (void*)value);
	}

	inline static int32_t get_offset_of_timeoutFrameCount_15() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___timeoutFrameCount_15)); }
	inline int32_t get_timeoutFrameCount_15() const { return ___timeoutFrameCount_15; }
	inline int32_t* get_address_of_timeoutFrameCount_15() { return &___timeoutFrameCount_15; }
	inline void set_timeoutFrameCount_15(int32_t value)
	{
		___timeoutFrameCount_15 = value;
	}

	inline static int32_t get_offset_of_requestResolution_16() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___requestResolution_16)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_requestResolution_16() const { return ___requestResolution_16; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_requestResolution_16() { return &___requestResolution_16; }
	inline void set_requestResolution_16(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___requestResolution_16 = value;
	}

	inline static int32_t get_offset_of_textureResolution_17() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___textureResolution_17)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_textureResolution_17() const { return ___textureResolution_17; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_textureResolution_17() { return &___textureResolution_17; }
	inline void set_textureResolution_17(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___textureResolution_17 = value;
	}

	inline static int32_t get_offset_of_isInitWaiting_18() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___isInitWaiting_18)); }
	inline bool get_isInitWaiting_18() const { return ___isInitWaiting_18; }
	inline bool* get_address_of_isInitWaiting_18() { return &___isInitWaiting_18; }
	inline void set_isInitWaiting_18(bool value)
	{
		___isInitWaiting_18 = value;
	}

	inline static int32_t get_offset_of_TextureRatio_19() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___TextureRatio_19)); }
	inline float get_TextureRatio_19() const { return ___TextureRatio_19; }
	inline float* get_address_of_TextureRatio_19() { return &___TextureRatio_19; }
	inline void set_TextureRatio_19(float value)
	{
		___TextureRatio_19 = value;
	}

	inline static int32_t get_offset_of_ScreenRatio_20() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___ScreenRatio_20)); }
	inline float get_ScreenRatio_20() const { return ___ScreenRatio_20; }
	inline float* get_address_of_ScreenRatio_20() { return &___ScreenRatio_20; }
	inline void set_ScreenRatio_20(float value)
	{
		___ScreenRatio_20 = value;
	}

	inline static int32_t get_offset_of_baseRotation_21() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___baseRotation_21)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_baseRotation_21() const { return ___baseRotation_21; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_baseRotation_21() { return &___baseRotation_21; }
	inline void set_baseRotation_21(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___baseRotation_21 = value;
	}

	inline static int32_t get_offset_of_isFlipped_22() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___isFlipped_22)); }
	inline bool get_isFlipped_22() const { return ___isFlipped_22; }
	inline bool* get_address_of_isFlipped_22() { return &___isFlipped_22; }
	inline void set_isFlipped_22(bool value)
	{
		___isFlipped_22 = value;
	}

	inline static int32_t get_offset_of_OnWebcamTextureReady_23() { return static_cast<int32_t>(offsetof(WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973, ___OnWebcamTextureReady_23)); }
	inline UnityEventWebcamTexture_t73539047423B07150669F124020AA49AF9260D66 * get_OnWebcamTextureReady_23() const { return ___OnWebcamTextureReady_23; }
	inline UnityEventWebcamTexture_t73539047423B07150669F124020AA49AF9260D66 ** get_address_of_OnWebcamTextureReady_23() { return &___OnWebcamTextureReady_23; }
	inline void set_OnWebcamTextureReady_23(UnityEventWebcamTexture_t73539047423B07150669F124020AA49AF9260D66 * value)
	{
		___OnWebcamTextureReady_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnWebcamTextureReady_23), (void*)value);
	}
};


// OculusSampleFramework.WindmillBladesController
struct WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AudioSource OculusSampleFramework.WindmillBladesController::_audioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ____audioSource_5;
	// UnityEngine.AudioClip OculusSampleFramework.WindmillBladesController::_windMillRotationSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ____windMillRotationSound_6;
	// UnityEngine.AudioClip OculusSampleFramework.WindmillBladesController::_windMillStartSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ____windMillStartSound_7;
	// UnityEngine.AudioClip OculusSampleFramework.WindmillBladesController::_windMillStopSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ____windMillStopSound_8;
	// System.Boolean OculusSampleFramework.WindmillBladesController::<IsMoving>k__BackingField
	bool ___U3CIsMovingU3Ek__BackingField_9;
	// System.Single OculusSampleFramework.WindmillBladesController::_currentSpeed
	float ____currentSpeed_10;
	// UnityEngine.Coroutine OculusSampleFramework.WindmillBladesController::_lerpSpeedCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____lerpSpeedCoroutine_11;
	// UnityEngine.Coroutine OculusSampleFramework.WindmillBladesController::_audioChangeCr
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____audioChangeCr_12;
	// UnityEngine.Quaternion OculusSampleFramework.WindmillBladesController::_originalRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ____originalRotation_13;
	// System.Single OculusSampleFramework.WindmillBladesController::_rotAngle
	float ____rotAngle_14;

public:
	inline static int32_t get_offset_of__audioSource_5() { return static_cast<int32_t>(offsetof(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3, ____audioSource_5)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get__audioSource_5() const { return ____audioSource_5; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of__audioSource_5() { return &____audioSource_5; }
	inline void set__audioSource_5(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		____audioSource_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____audioSource_5), (void*)value);
	}

	inline static int32_t get_offset_of__windMillRotationSound_6() { return static_cast<int32_t>(offsetof(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3, ____windMillRotationSound_6)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get__windMillRotationSound_6() const { return ____windMillRotationSound_6; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of__windMillRotationSound_6() { return &____windMillRotationSound_6; }
	inline void set__windMillRotationSound_6(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		____windMillRotationSound_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____windMillRotationSound_6), (void*)value);
	}

	inline static int32_t get_offset_of__windMillStartSound_7() { return static_cast<int32_t>(offsetof(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3, ____windMillStartSound_7)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get__windMillStartSound_7() const { return ____windMillStartSound_7; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of__windMillStartSound_7() { return &____windMillStartSound_7; }
	inline void set__windMillStartSound_7(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		____windMillStartSound_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____windMillStartSound_7), (void*)value);
	}

	inline static int32_t get_offset_of__windMillStopSound_8() { return static_cast<int32_t>(offsetof(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3, ____windMillStopSound_8)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get__windMillStopSound_8() const { return ____windMillStopSound_8; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of__windMillStopSound_8() { return &____windMillStopSound_8; }
	inline void set__windMillStopSound_8(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		____windMillStopSound_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____windMillStopSound_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsMovingU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3, ___U3CIsMovingU3Ek__BackingField_9)); }
	inline bool get_U3CIsMovingU3Ek__BackingField_9() const { return ___U3CIsMovingU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsMovingU3Ek__BackingField_9() { return &___U3CIsMovingU3Ek__BackingField_9; }
	inline void set_U3CIsMovingU3Ek__BackingField_9(bool value)
	{
		___U3CIsMovingU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of__currentSpeed_10() { return static_cast<int32_t>(offsetof(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3, ____currentSpeed_10)); }
	inline float get__currentSpeed_10() const { return ____currentSpeed_10; }
	inline float* get_address_of__currentSpeed_10() { return &____currentSpeed_10; }
	inline void set__currentSpeed_10(float value)
	{
		____currentSpeed_10 = value;
	}

	inline static int32_t get_offset_of__lerpSpeedCoroutine_11() { return static_cast<int32_t>(offsetof(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3, ____lerpSpeedCoroutine_11)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__lerpSpeedCoroutine_11() const { return ____lerpSpeedCoroutine_11; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__lerpSpeedCoroutine_11() { return &____lerpSpeedCoroutine_11; }
	inline void set__lerpSpeedCoroutine_11(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____lerpSpeedCoroutine_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lerpSpeedCoroutine_11), (void*)value);
	}

	inline static int32_t get_offset_of__audioChangeCr_12() { return static_cast<int32_t>(offsetof(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3, ____audioChangeCr_12)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__audioChangeCr_12() const { return ____audioChangeCr_12; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__audioChangeCr_12() { return &____audioChangeCr_12; }
	inline void set__audioChangeCr_12(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____audioChangeCr_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____audioChangeCr_12), (void*)value);
	}

	inline static int32_t get_offset_of__originalRotation_13() { return static_cast<int32_t>(offsetof(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3, ____originalRotation_13)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get__originalRotation_13() const { return ____originalRotation_13; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of__originalRotation_13() { return &____originalRotation_13; }
	inline void set__originalRotation_13(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		____originalRotation_13 = value;
	}

	inline static int32_t get_offset_of__rotAngle_14() { return static_cast<int32_t>(offsetof(WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3, ____rotAngle_14)); }
	inline float get__rotAngle_14() const { return ____rotAngle_14; }
	inline float* get_address_of__rotAngle_14() { return &____rotAngle_14; }
	inline void set__rotAngle_14(float value)
	{
		____rotAngle_14 = value;
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// Obi.ObiCollider
struct ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9  : public ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4
{
public:
	// UnityEngine.Collider Obi.ObiCollider::m_SourceCollider
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___m_SourceCollider_13;
	// Obi.ObiDistanceField Obi.ObiCollider::m_DistanceField
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482 * ___m_DistanceField_14;

public:
	inline static int32_t get_offset_of_m_SourceCollider_13() { return static_cast<int32_t>(offsetof(ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9, ___m_SourceCollider_13)); }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * get_m_SourceCollider_13() const { return ___m_SourceCollider_13; }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** get_address_of_m_SourceCollider_13() { return &___m_SourceCollider_13; }
	inline void set_m_SourceCollider_13(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		___m_SourceCollider_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SourceCollider_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_DistanceField_14() { return static_cast<int32_t>(offsetof(ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9, ___m_DistanceField_14)); }
	inline ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482 * get_m_DistanceField_14() const { return ___m_DistanceField_14; }
	inline ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482 ** get_address_of_m_DistanceField_14() { return &___m_DistanceField_14; }
	inline void set_m_DistanceField_14(ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482 * value)
	{
		___m_DistanceField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DistanceField_14), (void*)value);
	}
};


// TeleportAimHandler
struct TeleportAimHandler_tC1C6D09FC478B420E7C892ECCB6E625F537D3BBD  : public TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121
{
public:

public:
};


// TeleportInputHandler
struct TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D  : public TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121
{
public:
	// System.Action TeleportInputHandler::_startReadyAction
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ____startReadyAction_6;
	// System.Action TeleportInputHandler::_startAimAction
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ____startAimAction_7;

public:
	inline static int32_t get_offset_of__startReadyAction_6() { return static_cast<int32_t>(offsetof(TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D, ____startReadyAction_6)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get__startReadyAction_6() const { return ____startReadyAction_6; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of__startReadyAction_6() { return &____startReadyAction_6; }
	inline void set__startReadyAction_6(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		____startReadyAction_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____startReadyAction_6), (void*)value);
	}

	inline static int32_t get_offset_of__startAimAction_7() { return static_cast<int32_t>(offsetof(TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D, ____startAimAction_7)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get__startAimAction_7() const { return ____startAimAction_7; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of__startAimAction_7() { return &____startAimAction_7; }
	inline void set__startAimAction_7(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		____startAimAction_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____startAimAction_7), (void*)value);
	}
};


// TeleportOrientationHandler
struct TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D  : public TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121
{
public:
	// System.Action TeleportOrientationHandler::_updateOrientationAction
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ____updateOrientationAction_6;
	// System.Action`1<LocomotionTeleport/AimData> TeleportOrientationHandler::_updateAimDataAction
	Action_1_t92198758A8B104AAC58CEBF1192A89AFDC67850B * ____updateAimDataAction_7;
	// LocomotionTeleport/AimData TeleportOrientationHandler::AimData
	AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * ___AimData_8;

public:
	inline static int32_t get_offset_of__updateOrientationAction_6() { return static_cast<int32_t>(offsetof(TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D, ____updateOrientationAction_6)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get__updateOrientationAction_6() const { return ____updateOrientationAction_6; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of__updateOrientationAction_6() { return &____updateOrientationAction_6; }
	inline void set__updateOrientationAction_6(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		____updateOrientationAction_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____updateOrientationAction_6), (void*)value);
	}

	inline static int32_t get_offset_of__updateAimDataAction_7() { return static_cast<int32_t>(offsetof(TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D, ____updateAimDataAction_7)); }
	inline Action_1_t92198758A8B104AAC58CEBF1192A89AFDC67850B * get__updateAimDataAction_7() const { return ____updateAimDataAction_7; }
	inline Action_1_t92198758A8B104AAC58CEBF1192A89AFDC67850B ** get_address_of__updateAimDataAction_7() { return &____updateAimDataAction_7; }
	inline void set__updateAimDataAction_7(Action_1_t92198758A8B104AAC58CEBF1192A89AFDC67850B * value)
	{
		____updateAimDataAction_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____updateAimDataAction_7), (void*)value);
	}

	inline static int32_t get_offset_of_AimData_8() { return static_cast<int32_t>(offsetof(TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D, ___AimData_8)); }
	inline AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * get_AimData_8() const { return ___AimData_8; }
	inline AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA ** get_address_of_AimData_8() { return &___AimData_8; }
	inline void set_AimData_8(AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * value)
	{
		___AimData_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AimData_8), (void*)value);
	}
};


// TeleportTargetHandler
struct TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273  : public TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121
{
public:
	// UnityEngine.LayerMask TeleportTargetHandler::AimCollisionLayerMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___AimCollisionLayerMask_6;
	// LocomotionTeleport/AimData TeleportTargetHandler::AimData
	AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * ___AimData_7;
	// System.Action TeleportTargetHandler::_startAimAction
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ____startAimAction_8;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> TeleportTargetHandler::_aimPoints
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ____aimPoints_9;

public:
	inline static int32_t get_offset_of_AimCollisionLayerMask_6() { return static_cast<int32_t>(offsetof(TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273, ___AimCollisionLayerMask_6)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_AimCollisionLayerMask_6() const { return ___AimCollisionLayerMask_6; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_AimCollisionLayerMask_6() { return &___AimCollisionLayerMask_6; }
	inline void set_AimCollisionLayerMask_6(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___AimCollisionLayerMask_6 = value;
	}

	inline static int32_t get_offset_of_AimData_7() { return static_cast<int32_t>(offsetof(TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273, ___AimData_7)); }
	inline AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * get_AimData_7() const { return ___AimData_7; }
	inline AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA ** get_address_of_AimData_7() { return &___AimData_7; }
	inline void set_AimData_7(AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * value)
	{
		___AimData_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AimData_7), (void*)value);
	}

	inline static int32_t get_offset_of__startAimAction_8() { return static_cast<int32_t>(offsetof(TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273, ____startAimAction_8)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get__startAimAction_8() const { return ____startAimAction_8; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of__startAimAction_8() { return &____startAimAction_8; }
	inline void set__startAimAction_8(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		____startAimAction_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____startAimAction_8), (void*)value);
	}

	inline static int32_t get_offset_of__aimPoints_9() { return static_cast<int32_t>(offsetof(TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273, ____aimPoints_9)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get__aimPoints_9() const { return ____aimPoints_9; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of__aimPoints_9() { return &____aimPoints_9; }
	inline void set__aimPoints_9(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		____aimPoints_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____aimPoints_9), (void*)value);
	}
};


// TeleportTransition
struct TeleportTransition_tBF8EC872DEFA8AFCC167216EBA01D4642897C76D  : public TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121
{
public:

public:
};


// OculusSampleFramework.TrainLocomotive
struct TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331  : public TrainCarBase_tB4EDBE5E85CE292877DC28F3A001BA8C27F0D456
{
public:
	// System.Single OculusSampleFramework.TrainLocomotive::_initialSpeed
	float ____initialSpeed_19;
	// UnityEngine.GameObject OculusSampleFramework.TrainLocomotive::_startStopButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____startStopButton_20;
	// UnityEngine.GameObject OculusSampleFramework.TrainLocomotive::_decreaseSpeedButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____decreaseSpeedButton_21;
	// UnityEngine.GameObject OculusSampleFramework.TrainLocomotive::_increaseSpeedButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____increaseSpeedButton_22;
	// UnityEngine.GameObject OculusSampleFramework.TrainLocomotive::_smokeButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____smokeButton_23;
	// UnityEngine.GameObject OculusSampleFramework.TrainLocomotive::_whistleButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____whistleButton_24;
	// UnityEngine.GameObject OculusSampleFramework.TrainLocomotive::_reverseButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____reverseButton_25;
	// UnityEngine.AudioSource OculusSampleFramework.TrainLocomotive::_whistleAudioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ____whistleAudioSource_26;
	// UnityEngine.AudioClip OculusSampleFramework.TrainLocomotive::_whistleSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ____whistleSound_27;
	// UnityEngine.AudioSource OculusSampleFramework.TrainLocomotive::_engineAudioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ____engineAudioSource_28;
	// UnityEngine.AudioClip[] OculusSampleFramework.TrainLocomotive::_accelerationSounds
	AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* ____accelerationSounds_29;
	// UnityEngine.AudioClip[] OculusSampleFramework.TrainLocomotive::_decelerationSounds
	AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* ____decelerationSounds_30;
	// UnityEngine.AudioClip OculusSampleFramework.TrainLocomotive::_startUpSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ____startUpSound_31;
	// UnityEngine.AudioSource OculusSampleFramework.TrainLocomotive::_smokeStackAudioSource
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ____smokeStackAudioSource_32;
	// UnityEngine.AudioClip OculusSampleFramework.TrainLocomotive::_smokeSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ____smokeSound_33;
	// UnityEngine.ParticleSystem OculusSampleFramework.TrainLocomotive::_smoke1
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ____smoke1_34;
	// UnityEngine.ParticleSystem OculusSampleFramework.TrainLocomotive::_smoke2
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ____smoke2_35;
	// OculusSampleFramework.TrainCarBase[] OculusSampleFramework.TrainLocomotive::_childCars
	TrainCarBaseU5BU5D_tEA9FA40EA871BECB324EAE0EA10A02C42C3FA5AA* ____childCars_36;
	// System.Boolean OculusSampleFramework.TrainLocomotive::_isMoving
	bool ____isMoving_37;
	// System.Boolean OculusSampleFramework.TrainLocomotive::_reverse
	bool ____reverse_38;
	// System.Single OculusSampleFramework.TrainLocomotive::_currentSpeed
	float ____currentSpeed_39;
	// System.Single OculusSampleFramework.TrainLocomotive::_speedDiv
	float ____speedDiv_40;
	// System.Single OculusSampleFramework.TrainLocomotive::_standardRateOverTimeMultiplier
	float ____standardRateOverTimeMultiplier_41;
	// System.Int32 OculusSampleFramework.TrainLocomotive::_standardMaxParticles
	int32_t ____standardMaxParticles_42;
	// UnityEngine.Coroutine OculusSampleFramework.TrainLocomotive::_startStopTrainCr
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____startStopTrainCr_43;

public:
	inline static int32_t get_offset_of__initialSpeed_19() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____initialSpeed_19)); }
	inline float get__initialSpeed_19() const { return ____initialSpeed_19; }
	inline float* get_address_of__initialSpeed_19() { return &____initialSpeed_19; }
	inline void set__initialSpeed_19(float value)
	{
		____initialSpeed_19 = value;
	}

	inline static int32_t get_offset_of__startStopButton_20() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____startStopButton_20)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__startStopButton_20() const { return ____startStopButton_20; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__startStopButton_20() { return &____startStopButton_20; }
	inline void set__startStopButton_20(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____startStopButton_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____startStopButton_20), (void*)value);
	}

	inline static int32_t get_offset_of__decreaseSpeedButton_21() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____decreaseSpeedButton_21)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__decreaseSpeedButton_21() const { return ____decreaseSpeedButton_21; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__decreaseSpeedButton_21() { return &____decreaseSpeedButton_21; }
	inline void set__decreaseSpeedButton_21(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____decreaseSpeedButton_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____decreaseSpeedButton_21), (void*)value);
	}

	inline static int32_t get_offset_of__increaseSpeedButton_22() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____increaseSpeedButton_22)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__increaseSpeedButton_22() const { return ____increaseSpeedButton_22; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__increaseSpeedButton_22() { return &____increaseSpeedButton_22; }
	inline void set__increaseSpeedButton_22(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____increaseSpeedButton_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____increaseSpeedButton_22), (void*)value);
	}

	inline static int32_t get_offset_of__smokeButton_23() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____smokeButton_23)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__smokeButton_23() const { return ____smokeButton_23; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__smokeButton_23() { return &____smokeButton_23; }
	inline void set__smokeButton_23(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____smokeButton_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____smokeButton_23), (void*)value);
	}

	inline static int32_t get_offset_of__whistleButton_24() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____whistleButton_24)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__whistleButton_24() const { return ____whistleButton_24; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__whistleButton_24() { return &____whistleButton_24; }
	inline void set__whistleButton_24(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____whistleButton_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____whistleButton_24), (void*)value);
	}

	inline static int32_t get_offset_of__reverseButton_25() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____reverseButton_25)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__reverseButton_25() const { return ____reverseButton_25; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__reverseButton_25() { return &____reverseButton_25; }
	inline void set__reverseButton_25(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____reverseButton_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____reverseButton_25), (void*)value);
	}

	inline static int32_t get_offset_of__whistleAudioSource_26() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____whistleAudioSource_26)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get__whistleAudioSource_26() const { return ____whistleAudioSource_26; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of__whistleAudioSource_26() { return &____whistleAudioSource_26; }
	inline void set__whistleAudioSource_26(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		____whistleAudioSource_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____whistleAudioSource_26), (void*)value);
	}

	inline static int32_t get_offset_of__whistleSound_27() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____whistleSound_27)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get__whistleSound_27() const { return ____whistleSound_27; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of__whistleSound_27() { return &____whistleSound_27; }
	inline void set__whistleSound_27(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		____whistleSound_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____whistleSound_27), (void*)value);
	}

	inline static int32_t get_offset_of__engineAudioSource_28() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____engineAudioSource_28)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get__engineAudioSource_28() const { return ____engineAudioSource_28; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of__engineAudioSource_28() { return &____engineAudioSource_28; }
	inline void set__engineAudioSource_28(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		____engineAudioSource_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____engineAudioSource_28), (void*)value);
	}

	inline static int32_t get_offset_of__accelerationSounds_29() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____accelerationSounds_29)); }
	inline AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* get__accelerationSounds_29() const { return ____accelerationSounds_29; }
	inline AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE** get_address_of__accelerationSounds_29() { return &____accelerationSounds_29; }
	inline void set__accelerationSounds_29(AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* value)
	{
		____accelerationSounds_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____accelerationSounds_29), (void*)value);
	}

	inline static int32_t get_offset_of__decelerationSounds_30() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____decelerationSounds_30)); }
	inline AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* get__decelerationSounds_30() const { return ____decelerationSounds_30; }
	inline AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE** get_address_of__decelerationSounds_30() { return &____decelerationSounds_30; }
	inline void set__decelerationSounds_30(AudioClipU5BU5D_t9BA3E7C4B62164BA2E054F3A82CD6CE6EB273CBE* value)
	{
		____decelerationSounds_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____decelerationSounds_30), (void*)value);
	}

	inline static int32_t get_offset_of__startUpSound_31() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____startUpSound_31)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get__startUpSound_31() const { return ____startUpSound_31; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of__startUpSound_31() { return &____startUpSound_31; }
	inline void set__startUpSound_31(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		____startUpSound_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____startUpSound_31), (void*)value);
	}

	inline static int32_t get_offset_of__smokeStackAudioSource_32() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____smokeStackAudioSource_32)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get__smokeStackAudioSource_32() const { return ____smokeStackAudioSource_32; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of__smokeStackAudioSource_32() { return &____smokeStackAudioSource_32; }
	inline void set__smokeStackAudioSource_32(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		____smokeStackAudioSource_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____smokeStackAudioSource_32), (void*)value);
	}

	inline static int32_t get_offset_of__smokeSound_33() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____smokeSound_33)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get__smokeSound_33() const { return ____smokeSound_33; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of__smokeSound_33() { return &____smokeSound_33; }
	inline void set__smokeSound_33(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		____smokeSound_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____smokeSound_33), (void*)value);
	}

	inline static int32_t get_offset_of__smoke1_34() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____smoke1_34)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get__smoke1_34() const { return ____smoke1_34; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of__smoke1_34() { return &____smoke1_34; }
	inline void set__smoke1_34(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		____smoke1_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____smoke1_34), (void*)value);
	}

	inline static int32_t get_offset_of__smoke2_35() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____smoke2_35)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get__smoke2_35() const { return ____smoke2_35; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of__smoke2_35() { return &____smoke2_35; }
	inline void set__smoke2_35(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		____smoke2_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____smoke2_35), (void*)value);
	}

	inline static int32_t get_offset_of__childCars_36() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____childCars_36)); }
	inline TrainCarBaseU5BU5D_tEA9FA40EA871BECB324EAE0EA10A02C42C3FA5AA* get__childCars_36() const { return ____childCars_36; }
	inline TrainCarBaseU5BU5D_tEA9FA40EA871BECB324EAE0EA10A02C42C3FA5AA** get_address_of__childCars_36() { return &____childCars_36; }
	inline void set__childCars_36(TrainCarBaseU5BU5D_tEA9FA40EA871BECB324EAE0EA10A02C42C3FA5AA* value)
	{
		____childCars_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____childCars_36), (void*)value);
	}

	inline static int32_t get_offset_of__isMoving_37() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____isMoving_37)); }
	inline bool get__isMoving_37() const { return ____isMoving_37; }
	inline bool* get_address_of__isMoving_37() { return &____isMoving_37; }
	inline void set__isMoving_37(bool value)
	{
		____isMoving_37 = value;
	}

	inline static int32_t get_offset_of__reverse_38() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____reverse_38)); }
	inline bool get__reverse_38() const { return ____reverse_38; }
	inline bool* get_address_of__reverse_38() { return &____reverse_38; }
	inline void set__reverse_38(bool value)
	{
		____reverse_38 = value;
	}

	inline static int32_t get_offset_of__currentSpeed_39() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____currentSpeed_39)); }
	inline float get__currentSpeed_39() const { return ____currentSpeed_39; }
	inline float* get_address_of__currentSpeed_39() { return &____currentSpeed_39; }
	inline void set__currentSpeed_39(float value)
	{
		____currentSpeed_39 = value;
	}

	inline static int32_t get_offset_of__speedDiv_40() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____speedDiv_40)); }
	inline float get__speedDiv_40() const { return ____speedDiv_40; }
	inline float* get_address_of__speedDiv_40() { return &____speedDiv_40; }
	inline void set__speedDiv_40(float value)
	{
		____speedDiv_40 = value;
	}

	inline static int32_t get_offset_of__standardRateOverTimeMultiplier_41() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____standardRateOverTimeMultiplier_41)); }
	inline float get__standardRateOverTimeMultiplier_41() const { return ____standardRateOverTimeMultiplier_41; }
	inline float* get_address_of__standardRateOverTimeMultiplier_41() { return &____standardRateOverTimeMultiplier_41; }
	inline void set__standardRateOverTimeMultiplier_41(float value)
	{
		____standardRateOverTimeMultiplier_41 = value;
	}

	inline static int32_t get_offset_of__standardMaxParticles_42() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____standardMaxParticles_42)); }
	inline int32_t get__standardMaxParticles_42() const { return ____standardMaxParticles_42; }
	inline int32_t* get_address_of__standardMaxParticles_42() { return &____standardMaxParticles_42; }
	inline void set__standardMaxParticles_42(int32_t value)
	{
		____standardMaxParticles_42 = value;
	}

	inline static int32_t get_offset_of__startStopTrainCr_43() { return static_cast<int32_t>(offsetof(TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331, ____startStopTrainCr_43)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__startStopTrainCr_43() const { return ____startStopTrainCr_43; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__startStopTrainCr_43() { return &____startStopTrainCr_43; }
	inline void set__startStopTrainCr_43(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____startStopTrainCr_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____startStopTrainCr_43), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// TeleportTransitionBlink
struct TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977  : public TeleportTransition_tBF8EC872DEFA8AFCC167216EBA01D4642897C76D
{
public:
	// System.Single TeleportTransitionBlink::TransitionDuration
	float ___TransitionDuration_6;
	// System.Single TeleportTransitionBlink::TeleportDelay
	float ___TeleportDelay_7;
	// UnityEngine.AnimationCurve TeleportTransitionBlink::FadeLevels
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___FadeLevels_8;

public:
	inline static int32_t get_offset_of_TransitionDuration_6() { return static_cast<int32_t>(offsetof(TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977, ___TransitionDuration_6)); }
	inline float get_TransitionDuration_6() const { return ___TransitionDuration_6; }
	inline float* get_address_of_TransitionDuration_6() { return &___TransitionDuration_6; }
	inline void set_TransitionDuration_6(float value)
	{
		___TransitionDuration_6 = value;
	}

	inline static int32_t get_offset_of_TeleportDelay_7() { return static_cast<int32_t>(offsetof(TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977, ___TeleportDelay_7)); }
	inline float get_TeleportDelay_7() const { return ___TeleportDelay_7; }
	inline float* get_address_of_TeleportDelay_7() { return &___TeleportDelay_7; }
	inline void set_TeleportDelay_7(float value)
	{
		___TeleportDelay_7 = value;
	}

	inline static int32_t get_offset_of_FadeLevels_8() { return static_cast<int32_t>(offsetof(TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977, ___FadeLevels_8)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_FadeLevels_8() const { return ___FadeLevels_8; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_FadeLevels_8() { return &___FadeLevels_8; }
	inline void set_FadeLevels_8(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___FadeLevels_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FadeLevels_8), (void*)value);
	}
};


// TeleportTransitionWarp
struct TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37  : public TeleportTransition_tBF8EC872DEFA8AFCC167216EBA01D4642897C76D
{
public:
	// System.Single TeleportTransitionWarp::TransitionDuration
	float ___TransitionDuration_6;
	// UnityEngine.AnimationCurve TeleportTransitionWarp::PositionLerp
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___PositionLerp_7;

public:
	inline static int32_t get_offset_of_TransitionDuration_6() { return static_cast<int32_t>(offsetof(TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37, ___TransitionDuration_6)); }
	inline float get_TransitionDuration_6() const { return ___TransitionDuration_6; }
	inline float* get_address_of_TransitionDuration_6() { return &___TransitionDuration_6; }
	inline void set_TransitionDuration_6(float value)
	{
		___TransitionDuration_6 = value;
	}

	inline static int32_t get_offset_of_PositionLerp_7() { return static_cast<int32_t>(offsetof(TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37, ___PositionLerp_7)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_PositionLerp_7() const { return ___PositionLerp_7; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_PositionLerp_7() { return &___PositionLerp_7; }
	inline void set_PositionLerp_7(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___PositionLerp_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PositionLerp_7), (void*)value);
	}
};


// UnityEngine.UI.RawImage
struct RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___m_Texture_36;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___m_UVRect_37;

public:
	inline static int32_t get_offset_of_m_Texture_36() { return static_cast<int32_t>(offsetof(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A, ___m_Texture_36)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_m_Texture_36() const { return ___m_Texture_36; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_m_Texture_36() { return &___m_Texture_36; }
	inline void set_m_Texture_36(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___m_Texture_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Texture_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_UVRect_37() { return static_cast<int32_t>(offsetof(RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A, ___m_UVRect_37)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_m_UVRect_37() const { return ___m_UVRect_37; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_m_UVRect_37() { return &___m_UVRect_37; }
	inline void set_m_UVRect_37(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___m_UVRect_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C  m_Items[1];

public:
	inline WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_DepthCameraName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Resolutions_4), (void*)NULL);
		#endif
	}
	inline WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_DepthCameraName_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Resolutions_4), (void*)NULL);
		#endif
	}
};
// UnityEngine.WebCamTexture[]
struct WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * m_Items[1];

public:
	inline WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Texture[]
struct TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * m_Items[1];

public:
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Material_t8927C00353A72755313F046D0CE85178AE8218EE * m_Items[1];

public:
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};


// System.Int32 System.Collections.Generic.LinkedList`1<System.Single>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LinkedList_1_get_Count_m3002AA56275E54B52256A83C262FCD1F1A39E9BC_gshared_inline (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, const RuntimeMethod* method);
// System.Collections.Generic.LinkedListNode`1<!0> System.Collections.Generic.LinkedList`1<System.Single>::get_First()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * LinkedList_1_get_First_m60113671B7FFA2E250B7EB5C490DF4B5D5C10F52_gshared_inline (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.LinkedListNode`1<System.Single>::get_Value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float LinkedListNode_1_get_Value_m9BA93F2A1CA19916497B4D273C24114F10B694BD_gshared_inline (LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1<System.Single>::RemoveFirst()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1_RemoveFirst_m7DDA60CBD00B0A5D88BA887D4F2575C038E32756_gshared (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, const RuntimeMethod* method);
// System.Collections.Generic.LinkedListNode`1<!0> System.Collections.Generic.LinkedList`1<System.Single>::AddLast(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * LinkedList_1_AddLast_m4DD91ACDAC1A8D61399FC65E1A38601D1B95D6C3_gshared (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, float ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1<System.Single>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1__ctor_m1D81D06D2B8525B2D4C1EE8CA5D423E890F97E4F_gshared (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___item0, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mB9EAE3168E00BA12AA7E1233A4A0007FD12BB9E7_gshared_inline (Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_Invoke_m73C0FE7D4CDD8627332257E8503F2E9862E33C3E_gshared (UnityEvent_1_t32063FE815890FF672DF76288FAC4ABE089B899F * __this, RuntimeObject * ___arg00, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<!!0> UnityEngine.Rendering.AsyncGPUReadbackRequest::GetData<System.Byte>(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  AsyncGPUReadbackRequest_GetData_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mA5D2215016D842E07C2BFC1C268B22D24E9FDDBD_gshared (AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA * __this, int32_t ___layer0, const RuntimeMethod* method);
// !0[] Unity.Collections.NativeArray`1<System.Byte>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* NativeArray_1_ToArray_m13D8B79ED269C75D10BE015EC81844C30603B838_gshared (NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.TaskAwaiter,System.Object>(!!0&,!!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisRuntimeObject_mF3F724FDD4660E37EEA11B37B562AA2EB0FE6220_gshared (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * ___awaiter0, RuntimeObject ** ___stateMachine1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter,System.Object>(!!0&,!!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisYieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_TisRuntimeObject_m31C6431C5727670E96602B5B123E2512925EF140_gshared (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * ___awaiter0, RuntimeObject ** ___stateMachine1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// UnityEngine.Color OVRPassthroughLayer::get_edgeColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  OVRPassthroughLayer_get_edgeColor_mC1323150AAA1042A87A1E138E35863CAD05C9E49 (OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_Lerp_mC986D7F29103536908D76BD8FC59AA11DC33C197 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___a0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void OVRPassthroughLayer::set_edgeColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OVRPassthroughLayer_set_edgeColor_mB8D0F9ACAF8F3EB0F204798AE706CD8ADDBB8790 (OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// System.Void OVRPassthroughLayer::set_edgeRenderingEnabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OVRPassthroughLayer_set_edgeRenderingEnabled_mAD01AD97216755C868F1E46F1414CE68D82DE896 (OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.LinkedList`1<System.Single>::get_Count()
inline int32_t LinkedList_1_get_Count_m3002AA56275E54B52256A83C262FCD1F1A39E9BC_inline (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 *, const RuntimeMethod*))LinkedList_1_get_Count_m3002AA56275E54B52256A83C262FCD1F1A39E9BC_gshared_inline)(__this, method);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2 (float ___minInclusive0, float ___maxInclusive1, const RuntimeMethod* method);
// System.Collections.Generic.LinkedListNode`1<!0> System.Collections.Generic.LinkedList`1<System.Single>::get_First()
inline LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * LinkedList_1_get_First_m60113671B7FFA2E250B7EB5C490DF4B5D5C10F52_inline (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, const RuntimeMethod* method)
{
	return ((  LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * (*) (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 *, const RuntimeMethod*))LinkedList_1_get_First_m60113671B7FFA2E250B7EB5C490DF4B5D5C10F52_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.LinkedListNode`1<System.Single>::get_Value()
inline float LinkedListNode_1_get_Value_m9BA93F2A1CA19916497B4D273C24114F10B694BD_inline (LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * __this, const RuntimeMethod* method)
{
	return ((  float (*) (LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 *, const RuntimeMethod*))LinkedListNode_1_get_Value_m9BA93F2A1CA19916497B4D273C24114F10B694BD_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.LinkedList`1<System.Single>::RemoveFirst()
inline void LinkedList_1_RemoveFirst_m7DDA60CBD00B0A5D88BA887D4F2575C038E32756 (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 *, const RuntimeMethod*))LinkedList_1_RemoveFirst_m7DDA60CBD00B0A5D88BA887D4F2575C038E32756_gshared)(__this, method);
}
// System.Collections.Generic.LinkedListNode`1<!0> System.Collections.Generic.LinkedList`1<System.Single>::AddLast(!0)
inline LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * LinkedList_1_AddLast_m4DD91ACDAC1A8D61399FC65E1A38601D1B95D6C3 (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, float ___value0, const RuntimeMethod* method)
{
	return ((  LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * (*) (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 *, float, const RuntimeMethod*))LinkedList_1_AddLast_m4DD91ACDAC1A8D61399FC65E1A38601D1B95D6C3_gshared)(__this, ___value0, method);
}
// System.Void System.Collections.Generic.LinkedList`1<System.Single>::.ctor()
inline void LinkedList_1__ctor_m1D81D06D2B8525B2D4C1EE8CA5D423E890F97E4F (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 *, const RuntimeMethod*))LinkedList_1__ctor_m1D81D06D2B8525B2D4C1EE8CA5D423E890F97E4F_gshared)(__this, method);
}
// System.Void RuntimeRopeGenerator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeRopeGenerator__ctor_mC6334710591DE477B4E1977384C18E199AC229BB (RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Collections.IEnumerator RuntimeRopeGenerator::MakeRope(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RuntimeRopeGenerator_MakeRope_m75FA53894436086130B97A840F5FDD2FC27B16AE (RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___anchoredTo0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___attachmentOffset1, float ___ropeLength2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Void RuntimeRopeGenerator::AddPendulum(Obi.ObiCollider,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeRopeGenerator_AddPendulum_mC44C741BD87348055DED2613E4B3F2C1F036FF27 (RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * __this, ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9 * ___pendulum0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___attachmentOffset1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler__ctor_m9D4CC7B806AFF3B790E5E1A3B891F6775A146B78 (EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Panel_Login::add_EHOpenMenuPanel(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Panel_Login_add_EHOpenMenuPanel_mA55C094873CC99345C4F40C8F7EDB9541AF77731 (Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41 * __this, EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___value0, const RuntimeMethod* method);
// System.Void Panel_Menu::add_EHOpenAllInOne(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Panel_Menu_add_EHOpenAllInOne_m37C17885A25E4A6D83FFFF330F3E37F6BECCB8E2 (Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 * __this, EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___value0, const RuntimeMethod* method);
// System.Void Panel_Menu::add_EHOpenDataRecord(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Panel_Menu_add_EHOpenDataRecord_m193BE0C8E56955ABE74A747F3579592A2416965A (Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 * __this, EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___value0, const RuntimeMethod* method);
// System.Void Panel_Menu::add_EHLogout(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Panel_Menu_add_EHLogout_mAA883EE7947DB7A0540919530C59FF5846FEDC93 (Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 * __this, EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___value0, const RuntimeMethod* method);
// System.Void Panel_AllInOne::add_EHCloseAllInOne(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Panel_AllInOne_add_EHCloseAllInOne_m65626A3BB4CF4110B13AD9BBB334F32B219DC1CE (Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D * __this, EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___value0, const RuntimeMethod* method);
// System.Void Panel_DataRecord::add_EHCloseDataRecord(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Panel_DataRecord_add_EHCloseDataRecord_mCC095FF201C79024C6A9A708A1C44EACF59E7300 (Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57 * __this, EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * ___value0, const RuntimeMethod* method);
// System.Void StartMenu::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StartMenu_LoadScene_mF7663ABFF602CD53F3A26C076E6A8FA200347F97 (StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3 * __this, int32_t ___idx0, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Camera_get_projectionMatrix_mDB77E3A7F71CEF085797BCE58FAC78058C5D6756 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_fieldOfView()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_fieldOfView_mA9BA910800B2E33B572929CDA9A12CE596353920 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_aspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_aspect_mD0A1FC8F998473DA08866FF9CD61C02E6D5F4987 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Matrix4x4_set_Item_mE286A89718710DDF166DF6ACF8A480D15FE06B2F (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, int32_t ___row0, int32_t ___column1, float ___value2, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_fieldOfView_m138FE103CAC4B803F39E4CF579609A5C3FEB5E49 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_projectionMatrix_m3645AC49FC94726BDA07191C80299D8361D5C328 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// LocomotionTeleport TeleportSupport::get_LocomotionTeleport()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline (TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121 * __this, const RuntimeMethod* method);
// LocomotionTeleport/States LocomotionTeleport::get_CurrentState()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LocomotionTeleport_get_CurrentState_mCEDC22238BFC0EB60D9DE2616698C71D0CEF0BA8_inline (LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
inline void List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, int32_t, const RuntimeMethod*))List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline)(__this, ___index0, method);
}
// System.Collections.Generic.List`1<UnityEngine.Vector3> LocomotionTeleport/AimData::get_Points()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * AimData_get_Points_m3169977C66C0D412F32CECB2B7FD471F65A191B2_inline (AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
inline void List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , const RuntimeMethod*))List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_gshared)(__this, ___item0, method);
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
inline bool Nullable_1_get_HasValue_mB9EAE3168E00BA12AA7E1233A4A0007FD12BB9E7_inline (Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 *, const RuntimeMethod*))Nullable_1_get_HasValue_mB9EAE3168E00BA12AA7E1233A4A0007FD12BB9E7_gshared_inline)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
inline int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline)(__this, method);
}
// System.Void LocomotionTeleport::OnUpdateAimData(LocomotionTeleport/AimData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocomotionTeleport_OnUpdateAimData_m76ED9D517FF45B8A7CE7757BD0E8D147892F90A8 (LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * __this, AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * ___aimData0, const RuntimeMethod* method);
// System.Void LocomotionTeleport::DoTeleport()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocomotionTeleport_DoTeleport_m1477EB38D2FD6E2AD4A7553EB3237B18D40C0B0C (LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 LocomotionTeleport::GetCharacterPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  LocomotionTeleport_GetCharacterPosition_m30F7EB551F5787D2489CE2716284B848D2C11D26 (LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, float ___time0, const RuntimeMethod* method);
// System.Void LocomotionTeleport::DoWarp(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LocomotionTeleport_DoWarp_m2195B0DF360E9B04AD486855F66560DFC22EEB76 (LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startPos0, float ___positionPercent1, const RuntimeMethod* method);
// System.Byte[] FMExtensionMethods::FMRawTextureDataToJPG(System.Byte[],System.Int32,System.Int32,System.Int32,FMChromaSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* FMExtensionMethods_FMRawTextureDataToJPG_m85A2EA12E76A38D087B912D0CED0F084C3D9EE8A (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___RawTextureData0, int32_t ____width1, int32_t ____height2, int32_t ___Quality3, int32_t ___Subsampling4, const RuntimeMethod* method);
// System.Int32 Loom::get_maxThreads()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Loom_get_maxThreads_m45F4C5CDCCA9EA30915641C7D48A8A1B312CDD2A (const RuntimeMethod* method);
// System.Void TextureEncoder/<>c__DisplayClass52_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass52_0__ctor_mC913D0C8AE158CA693E67A5CCABC2512C3245FF4 (U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * __this, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Threading.Thread Loom::RunAsync(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * Loom_RunAsync_mF19ED64A5D75CBD0C9150286CEB6EB97CD3A8CF5 (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___a0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::Abs(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_Abs_mE46B08A540F26741910760E84ACB6AACD996C3C0 (int32_t ___value0, const RuntimeMethod* method);
// System.Byte[] FMZipHelper::FMZipBytes(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* FMZipHelper_FMZipBytes_mFD4D5CDF5908FCBC97214EFBF45BD25AE810C26E (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___RawBytes0, const RuntimeMethod* method);
// System.Byte[] System.BitConverter::GetBytes(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* BitConverter_GetBytes_m5C926FE938C878F7E4E95A5DED46C34DB1431D39 (int32_t ___value0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E (float ___f0, const RuntimeMethod* method);
// System.Void System.Buffer::BlockCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725 (RuntimeArray * ___src0, int32_t ___srcOffset1, RuntimeArray * ___dst2, int32_t ___dstOffset3, int32_t ___count4, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Byte[]>::Invoke(!0)
inline void UnityEvent_1_Invoke_m470057E7D3B20C1038D24F229A27EF3DEDD8E60D (UnityEvent_1_tF65557EFA1FC9CFCD88E86FB4B11D5367C18D14B * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___arg00, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tF65557EFA1FC9CFCD88E86FB4B11D5367C18D14B *, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, const RuntimeMethod*))UnityEvent_1_Invoke_m73C0FE7D4CDD8627332257E8503F2E9862E33C3E_gshared)(__this, ___arg00, method);
}
// UnityEngine.Rendering.AsyncGPUReadbackRequest UnityEngine.Rendering.AsyncGPUReadback::Request(UnityEngine.Texture,System.Int32,UnityEngine.TextureFormat,System.Action`1<UnityEngine.Rendering.AsyncGPUReadbackRequest>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA  AsyncGPUReadback_Request_m248D4E95EB755543253C06E28A71816381225B2D (Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___src0, int32_t ___mipIndex1, int32_t ___dstFormat2, Action_1_t542D0A6987D7110F66453B06D83AFE1D24208526 * ___callback3, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rendering.AsyncGPUReadbackRequest::get_done()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AsyncGPUReadbackRequest_get_done_m13E802E42592C58F5BD55E337F7EF2E29FA2D5AA (AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rendering.AsyncGPUReadbackRequest::get_hasError()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AsyncGPUReadbackRequest_get_hasError_m3DBBB101CE07C39D59FF8A7B96F4E96F3E8D5985 (AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA * __this, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<!!0> UnityEngine.Rendering.AsyncGPUReadbackRequest::GetData<System.Byte>(System.Int32)
inline NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  AsyncGPUReadbackRequest_GetData_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mA5D2215016D842E07C2BFC1C268B22D24E9FDDBD (AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA * __this, int32_t ___layer0, const RuntimeMethod* method)
{
	return ((  NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  (*) (AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA *, int32_t, const RuntimeMethod*))AsyncGPUReadbackRequest_GetData_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mA5D2215016D842E07C2BFC1C268B22D24E9FDDBD_gshared)(__this, ___layer0, method);
}
// !0[] Unity.Collections.NativeArray`1<System.Byte>::ToArray()
inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* NativeArray_1_ToArray_m13D8B79ED269C75D10BE015EC81844C30603B838 (NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785 * __this, const RuntimeMethod* method)
{
	return ((  ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* (*) (NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785 *, const RuntimeMethod*))NativeArray_1_ToArray_m13D8B79ED269C75D10BE015EC81844C30603B838_gshared)(__this, method);
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_realtimeSinceStartup_m5228CC1C1E57213D4148E965499072EA70D8C02B (const RuntimeMethod* method);
// System.Void TextureEncoder::RequestTextureUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextureEncoder_RequestTextureUpdate_m18AEFAA45404F0ADA2195C0C0C5C788760D9A72C (TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void OculusSampleFramework.TrainCrossingController::ToggleLightObjects(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrainCrossingController_ToggleLightObjects_m4B93FC82AC5CA62BC1F0755839F5AE2D3983C38C (TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 * __this, bool ___enableState0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetColor_m9DE63FCC5A31918F8A9A2E4FCED70C298677A7B4 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, int32_t ___nameID0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTimeMultiplier(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmissionModule_set_rateOverTimeMultiplier_m13A0F78D648A10145C2AE38A25A40E384791B961 (EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_maxParticles(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Single OculusSampleFramework.TrainLocomotive::PlayEngineSound(OculusSampleFramework.TrainLocomotive/EngineSoundState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TrainLocomotive_PlayEngineSound_mBBE7F6F9988F707FFC60EE195E18C7C7C37C05CE (TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * __this, int32_t ___engineSoundState0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_loop_mDD9FB746D8A7392472E5484EEF8D0A667993E3E0 (AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * __this, bool ___value0, const RuntimeMethod* method);
// System.Void OculusSampleFramework.TrainLocomotive::UpdateSmokeEmissionBasedOnSpeed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TrainLocomotive_UpdateSmokeEmissionBasedOnSpeed_mABC70306F1FBF3D00E0BA0901760D44793F9D3EF (TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Stop_m8CBF9268DE7B5A40952B4977462B857B5F5AFB9D (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method);
// System.Void OVRInput::SetControllerVibration(System.Single,System.Single,OVRInput/Controller)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OVRInput_SetControllerVibration_mB0CD60D364D545533E370E8BD2ED6AF43A69D728 (float ___frequency0, float ___amplitude1, int32_t ___controllerMask2, const RuntimeMethod* method);
// System.Threading.Tasks.Task System.Threading.Tasks.Task::Delay(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * Task_Delay_mD54722DBAF22507493263E9B1167A7F77EDDF80E (int32_t ___millisecondsDelay0, const RuntimeMethod* method);
// System.Runtime.CompilerServices.TaskAwaiter System.Threading.Tasks.Task::GetAwaiter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  Task_GetAwaiter_m1FF7528A8FE13F79207DFE970F642078EF6B1260 (Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * __this, const RuntimeMethod* method);
// System.Boolean System.Runtime.CompilerServices.TaskAwaiter::get_IsCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TaskAwaiter_get_IsCompleted_m6F97613C55E505B5664C3C0CFC4677D296EAA8BC (TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.TaskAwaiter,DKP.VR.VRIO/<Haptics>d__0>(!!0&,!!1&)
inline void AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mF00E8C45202F9019CB44F46C3EEC111CCE67B9E9 (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * ___awaiter0, U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF ** ___stateMachine1, const RuntimeMethod* method)
{
	((  void (*) (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *, TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *, U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF **, const RuntimeMethod*))AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisRuntimeObject_mF3F724FDD4660E37EEA11B37B562AA2EB0FE6220_gshared)(__this, ___awaiter0, ___stateMachine1, method);
}
// System.Void System.Runtime.CompilerServices.TaskAwaiter::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskAwaiter_GetResult_m578EEFEC4DD1AE5E77C899B8BAA3825EB79D1330 (TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * __this, const RuntimeMethod* method);
// System.Runtime.CompilerServices.YieldAwaitable System.Threading.Tasks.Task::Yield()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR YieldAwaitable_t95CCA9EB9730CADF5A3BEF9845E12FF467F594FA  Task_Yield_m1E79F65972D82906B8BBE9980C57E29538D3E94B (const RuntimeMethod* method);
// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter System.Runtime.CompilerServices.YieldAwaitable::GetAwaiter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  YieldAwaitable_GetAwaiter_m8AA7D8DCF790EB9BDBDD5F0D8BBA0404C6F7DCD8 (YieldAwaitable_t95CCA9EB9730CADF5A3BEF9845E12FF467F594FA * __this, const RuntimeMethod* method);
// System.Boolean System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter::get_IsCompleted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool YieldAwaiter_get_IsCompleted_mAB52777C6F31F3FBAD7E6A7CD90EDF40F220CBBC (YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::AwaitUnsafeOnCompleted<System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter,DKP.VR.VRIO/<Haptics>d__0>(!!0&,!!1&)
inline void AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisYieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mEC62BDE3C6F344BF4321FC8C373E9E160AFE0277 (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * ___awaiter0, U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF ** ___stateMachine1, const RuntimeMethod* method)
{
	((  void (*) (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *, YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE *, U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF **, const RuntimeMethod*))AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisYieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_TisRuntimeObject_m31C6431C5727670E96602B5B123E2512925EF140_gshared)(__this, ___awaiter0, ___stateMachine1, method);
}
// System.Void System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void YieldAwaiter_GetResult_mE9F670767330EAF32ED76882EB8B152FF62CCDBD (YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::SetException(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncVoidMethodBuilder_SetException_m16372173CEA3031B4CB9B8D15DA97C457F835155 (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, Exception_t * ___exception0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncVoidMethodBuilder::SetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncVoidMethodBuilder_SetResult_m901385B56EBE93E472A77EA48F61E4F498F3E00E (AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * __this, const RuntimeMethod* method);
// System.Void WebcamManager::Action_StopWebcam()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebcamManager_Action_StopWebcam_m3B387E8BCA377260DF46C7189AD053032E173C1A (WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator WebcamManager::initAndWaitForWebCamTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WebcamManager_initAndWaitForWebCamTexture_m6475CE2D9148E36CF76C11720BB8EBC75C3E8494 (WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * __this, const RuntimeMethod* method);
// UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* WebCamTexture_get_devices_m5E211AF8615AED8AAF97A669F41845FC85A0CD7C (const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.String UnityEngine.WebCamDevice::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* WebCamDevice_get_name_mD475CBF038076E5657D55D4DA43A7DC69CE6B6D4 (WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___values0, const RuntimeMethod* method);
// System.Void UnityEngine.WebCamTexture::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebCamTexture__ctor_m6819D615D58318888B7B90D47A7252A81894344F (WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD (float ___f0, const RuntimeMethod* method);
// System.Void UnityEngine.WebCamTexture::.ctor(System.String,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebCamTexture__ctor_m8369712442E77D9130CD2F76A3FA4846F74F16CB (WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * __this, String_t* ___deviceName0, int32_t ___requestedWidth1, int32_t ___requestedHeight2, int32_t ___requestedFPS3, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture_set_wrapMode_m1233D2DF48DC20996F8EE26E866D4BDD2AC8050D (Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebCamDevice_get_isFrontFacing_m43547AAF7B0729DB1962456A3EFF161B5E273E15 (WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebCamTexture_set_requestedFPS_m85821E34D74DDBF5B2FA8AF5D972BE0EE1667DCC (WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.WebCamTexture::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WebCamTexture_Play_m8527994B54606AE71602DB93195D2BA44CEDC2B1 (WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * Renderer_get_material_mE6B01125502D08EE0D6DFE2EAEC064AD9BB31804 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.RawImage>()
inline RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawImage_set_texture_m1D7BAE6CB629C36894B664D9F5D68CACA88B8D99 (RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.WebCamTexture::get_videoVerticallyMirrored()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WebCamTexture_get_videoVerticallyMirrored_m8ADBDC6A53EE4F84C6EEEE61B1B1A1E696C2B83D (WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_StopCoroutine_m5FF0476C9886FD8A3E6BA82BBE34B896CA279413 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___routine0, const RuntimeMethod* method);
// System.Boolean OculusSampleFramework.WindmillBladesController::get_IsMoving()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool WindmillBladesController_get_IsMoving_m5B2902A40908BBD84B5478EDA121ED00DBDECF56_inline (WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AudioClip::get_length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AudioClip_get_length_m2223F2281D853F847BE0048620BA6F61F26440E4 (AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator OculusSampleFramework.WindmillBladesController::PlaySoundDelayed(UnityEngine.AudioClip,UnityEngine.AudioClip,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WindmillBladesController_PlaySoundDelayed_m5C9465C96F6157B1A20D4F7F5A7D4F307D10D0FA (WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___initial0, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___clip1, float ___timeDelayAfterInitial2, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void OculusSampleFramework.WindmillBladesController::PlaySound(UnityEngine.AudioClip,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WindmillBladesController_PlaySound_mBFB8F38297A3551225D2E57F4285B58E9EF1EA63 (WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * __this, AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___clip0, bool ___loop1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PassthroughStyler/<FadeToDefaultPassthrough>d__20::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFadeToDefaultPassthroughU3Ed__20__ctor_mD5734AE35E978B6016AC4BAAF5BD2885854E7289 (U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void PassthroughStyler/<FadeToDefaultPassthrough>d__20::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFadeToDefaultPassthroughU3Ed__20_System_IDisposable_Dispose_m098A05EF49FFD658B51912059ADA457D2E5BCF4E (U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean PassthroughStyler/<FadeToDefaultPassthrough>d__20::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CFadeToDefaultPassthroughU3Ed__20_MoveNext_m2E27578A23C55A27E901446E92898D8B8C50843E (U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		goto IL_001b;
	}

IL_0014:
	{
		goto IL_01ab;
	}

IL_0019:
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// float timer = 0.0f;
		__this->set_U3CtimerU3E5__1_4((0.0f));
		// float brightness = passthroughLayer.colorMapEditorBrightness;
		PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * L_3 = __this->get_U3CU3E4__this_3();
		NullCheck(L_3);
		OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * L_4 = L_3->get_passthroughLayer_5();
		NullCheck(L_4);
		float L_5 = L_4->get_colorMapEditorBrightness_16();
		__this->set_U3CbrightnessU3E5__2_5(L_5);
		// float contrast = passthroughLayer.colorMapEditorContrast;
		PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * L_6 = __this->get_U3CU3E4__this_3();
		NullCheck(L_6);
		OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * L_7 = L_6->get_passthroughLayer_5();
		NullCheck(L_7);
		float L_8 = L_7->get_colorMapEditorContrast_14();
		__this->set_U3CcontrastU3E5__3_6(L_8);
		// float posterize = passthroughLayer.colorMapEditorPosterize;
		PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * L_9 = __this->get_U3CU3E4__this_3();
		NullCheck(L_9);
		OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * L_10 = L_9->get_passthroughLayer_5();
		NullCheck(L_10);
		float L_11 = L_10->get_colorMapEditorPosterize_18();
		__this->set_U3CposterizeU3E5__4_7(L_11);
		// Color edgeCol = passthroughLayer.edgeColor;
		PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * L_12 = __this->get_U3CU3E4__this_3();
		NullCheck(L_12);
		OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * L_13 = L_12->get_passthroughLayer_5();
		NullCheck(L_13);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_14;
		L_14 = OVRPassthroughLayer_get_edgeColor_mC1323150AAA1042A87A1E138E35863CAD05C9E49(L_13, /*hidden argument*/NULL);
		__this->set_U3CedgeColU3E5__5_8(L_14);
		goto IL_01b3;
	}

IL_008b:
	{
		// timer += Time.deltaTime;
		float L_15 = __this->get_U3CtimerU3E5__1_4();
		float L_16;
		L_16 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_U3CtimerU3E5__1_4(((float)il2cpp_codegen_add((float)L_15, (float)L_16)));
		// float normTimer = Mathf.Clamp01(timer / fadeTime);
		float L_17 = __this->get_U3CtimerU3E5__1_4();
		float L_18 = __this->get_fadeTime_2();
		float L_19;
		L_19 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)((float)L_17/(float)L_18)), /*hidden argument*/NULL);
		__this->set_U3CnormTimerU3E5__6_9(L_19);
		// passthroughLayer.colorMapEditorBrightness = Mathf.Lerp(brightness, 0.0f, normTimer);
		PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * L_20 = __this->get_U3CU3E4__this_3();
		NullCheck(L_20);
		OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * L_21 = L_20->get_passthroughLayer_5();
		float L_22 = __this->get_U3CbrightnessU3E5__2_5();
		float L_23 = __this->get_U3CnormTimerU3E5__6_9();
		float L_24;
		L_24 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_22, (0.0f), L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->set_colorMapEditorBrightness_16(L_24);
		// passthroughLayer.colorMapEditorContrast = Mathf.Lerp(contrast, 0.0f, normTimer);
		PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * L_25 = __this->get_U3CU3E4__this_3();
		NullCheck(L_25);
		OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * L_26 = L_25->get_passthroughLayer_5();
		float L_27 = __this->get_U3CcontrastU3E5__3_6();
		float L_28 = __this->get_U3CnormTimerU3E5__6_9();
		float L_29;
		L_29 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_27, (0.0f), L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		L_26->set_colorMapEditorContrast_14(L_29);
		// passthroughLayer.colorMapEditorPosterize = Mathf.Lerp(posterize, 0.0f, normTimer);
		PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * L_30 = __this->get_U3CU3E4__this_3();
		NullCheck(L_30);
		OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * L_31 = L_30->get_passthroughLayer_5();
		float L_32 = __this->get_U3CposterizeU3E5__4_7();
		float L_33 = __this->get_U3CnormTimerU3E5__6_9();
		float L_34;
		L_34 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_32, (0.0f), L_33, /*hidden argument*/NULL);
		NullCheck(L_31);
		L_31->set_colorMapEditorPosterize_18(L_34);
		// passthroughLayer.edgeColor = Color.Lerp(edgeCol, new Color(edgeCol.r, edgeCol.g, edgeCol.b, 0.0f), normTimer);
		PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * L_35 = __this->get_U3CU3E4__this_3();
		NullCheck(L_35);
		OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * L_36 = L_35->get_passthroughLayer_5();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_37 = __this->get_U3CedgeColU3E5__5_8();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_38 = __this->get_address_of_U3CedgeColU3E5__5_8();
		float L_39 = L_38->get_r_0();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_40 = __this->get_address_of_U3CedgeColU3E5__5_8();
		float L_41 = L_40->get_g_1();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_42 = __this->get_address_of_U3CedgeColU3E5__5_8();
		float L_43 = L_42->get_b_2();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_44;
		memset((&L_44), 0, sizeof(L_44));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_44), L_39, L_41, L_43, (0.0f), /*hidden argument*/NULL);
		float L_45 = __this->get_U3CnormTimerU3E5__6_9();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_46;
		L_46 = Color_Lerp_mC986D7F29103536908D76BD8FC59AA11DC33C197(L_37, L_44, L_45, /*hidden argument*/NULL);
		NullCheck(L_36);
		OVRPassthroughLayer_set_edgeColor_mB8D0F9ACAF8F3EB0F204798AE706CD8ADDBB8790(L_36, L_46, /*hidden argument*/NULL);
		// if (timer > fadeTime)
		float L_47 = __this->get_U3CtimerU3E5__1_4();
		float L_48 = __this->get_fadeTime_2();
		V_1 = (bool)((((float)L_47) > ((float)L_48))? 1 : 0);
		bool L_49 = V_1;
		if (!L_49)
		{
			goto IL_019b;
		}
	}
	{
		// passthroughLayer.edgeRenderingEnabled = false;
		PassthroughStyler_t6BC0ED3A919A3FB191574F198A108689F4E40D32 * L_50 = __this->get_U3CU3E4__this_3();
		NullCheck(L_50);
		OVRPassthroughLayer_t5E9DB341891664EC179D63B778948C649620EBCB * L_51 = L_50->get_passthroughLayer_5();
		NullCheck(L_51);
		OVRPassthroughLayer_set_edgeRenderingEnabled_mAD01AD97216755C868F1E46F1414CE68D82DE896(L_51, (bool)0, /*hidden argument*/NULL);
	}

IL_019b:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_01ab:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_01b3:
	{
		// while (timer <= fadeTime)
		float L_52 = __this->get_U3CtimerU3E5__1_4();
		float L_53 = __this->get_fadeTime_2();
		V_2 = (bool)((((int32_t)((!(((float)L_52) <= ((float)L_53)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_54 = V_2;
		if (L_54)
		{
			goto IL_008b;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object PassthroughStyler/<FadeToDefaultPassthrough>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m53DC68C1CCCDF30EC9CB9DB49C519C931805FABC (U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void PassthroughStyler/<FadeToDefaultPassthrough>d__20::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_IEnumerator_Reset_m67B84B18ED37EF94A7E28DC9D942BF28EE813ED8 (U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_IEnumerator_Reset_m67B84B18ED37EF94A7E28DC9D942BF28EE813ED8_RuntimeMethod_var)));
	}
}
// System.Object PassthroughStyler/<FadeToDefaultPassthrough>d__20::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CFadeToDefaultPassthroughU3Ed__20_System_Collections_IEnumerator_get_Current_m271ADFE5A502CB0CE024D87718FCE1B1BD5220BC (U3CFadeToDefaultPassthroughU3Ed__20_t5A2B942C5749915E9B9DD7D3865FCBEF0EFB9732 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RemoteLoopbackManager/PacketLatencyPair::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PacketLatencyPair__ctor_m8689B42992068A72EC9825E4D0D5BEA59C6CA17B (PacketLatencyPair_tB00B8B1B83E2801DA568D38A169B959F1FBCE25D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single RemoteLoopbackManager/SimulatedLatencySettings::NextValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SimulatedLatencySettings_NextValue_mE0F727E70EE772D2380E3B700425942729C05EC4 (SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedListNode_1_get_Value_m9BA93F2A1CA19916497B4D273C24114F10B694BD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_AddLast_m4DD91ACDAC1A8D61399FC65E1A38601D1B95D6C3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_RemoveFirst_m7DDA60CBD00B0A5D88BA887D4F2575C038E32756_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_get_Count_m3002AA56275E54B52256A83C262FCD1F1A39E9BC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_get_First_m60113671B7FFA2E250B7EB5C490DF4B5D5C10F52_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	float V_3 = 0.0f;
	{
		// AverageWindow = LatencySum / (float)LatencyValues.Count;
		float L_0 = __this->get_LatencySum_5();
		LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * L_1 = __this->get_LatencyValues_6();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = LinkedList_1_get_Count_m3002AA56275E54B52256A83C262FCD1F1A39E9BC_inline(L_1, /*hidden argument*/LinkedList_1_get_Count_m3002AA56275E54B52256A83C262FCD1F1A39E9BC_RuntimeMethod_var);
		__this->set_AverageWindow_4(((float)((float)L_0/(float)((float)((float)L_2)))));
		// float RandomLatency = UnityEngine.Random.Range(FakeLatencyMin, FakeLatencyMax);
		float L_3 = __this->get_FakeLatencyMin_1();
		float L_4 = __this->get_FakeLatencyMax_0();
		float L_5;
		L_5 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// float FakeLatency = AverageWindow * (1f - LatencyWeight) + LatencyWeight * RandomLatency;
		float L_6 = __this->get_AverageWindow_4();
		float L_7 = __this->get_LatencyWeight_2();
		float L_8 = __this->get_LatencyWeight_2();
		float L_9 = V_0;
		V_1 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_6, (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_9))));
		// if (LatencyValues.Count >= MaxSamples)
		LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * L_10 = __this->get_LatencyValues_6();
		NullCheck(L_10);
		int32_t L_11;
		L_11 = LinkedList_1_get_Count_m3002AA56275E54B52256A83C262FCD1F1A39E9BC_inline(L_10, /*hidden argument*/LinkedList_1_get_Count_m3002AA56275E54B52256A83C262FCD1F1A39E9BC_RuntimeMethod_var);
		int32_t L_12 = __this->get_MaxSamples_3();
		V_2 = (bool)((((int32_t)((((int32_t)L_11) < ((int32_t)L_12))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_13 = V_2;
		if (!L_13)
		{
			goto IL_008e;
		}
	}
	{
		// LatencySum -= LatencyValues.First.Value;
		float L_14 = __this->get_LatencySum_5();
		LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * L_15 = __this->get_LatencyValues_6();
		NullCheck(L_15);
		LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * L_16;
		L_16 = LinkedList_1_get_First_m60113671B7FFA2E250B7EB5C490DF4B5D5C10F52_inline(L_15, /*hidden argument*/LinkedList_1_get_First_m60113671B7FFA2E250B7EB5C490DF4B5D5C10F52_RuntimeMethod_var);
		NullCheck(L_16);
		float L_17;
		L_17 = LinkedListNode_1_get_Value_m9BA93F2A1CA19916497B4D273C24114F10B694BD_inline(L_16, /*hidden argument*/LinkedListNode_1_get_Value_m9BA93F2A1CA19916497B4D273C24114F10B694BD_RuntimeMethod_var);
		__this->set_LatencySum_5(((float)il2cpp_codegen_subtract((float)L_14, (float)L_17)));
		// LatencyValues.RemoveFirst();
		LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * L_18 = __this->get_LatencyValues_6();
		NullCheck(L_18);
		LinkedList_1_RemoveFirst_m7DDA60CBD00B0A5D88BA887D4F2575C038E32756(L_18, /*hidden argument*/LinkedList_1_RemoveFirst_m7DDA60CBD00B0A5D88BA887D4F2575C038E32756_RuntimeMethod_var);
	}

IL_008e:
	{
		// LatencySum += FakeLatency;
		float L_19 = __this->get_LatencySum_5();
		float L_20 = V_1;
		__this->set_LatencySum_5(((float)il2cpp_codegen_add((float)L_19, (float)L_20)));
		// LatencyValues.AddLast(FakeLatency);
		LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * L_21 = __this->get_LatencyValues_6();
		float L_22 = V_1;
		NullCheck(L_21);
		LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * L_23;
		L_23 = LinkedList_1_AddLast_m4DD91ACDAC1A8D61399FC65E1A38601D1B95D6C3(L_21, L_22, /*hidden argument*/LinkedList_1_AddLast_m4DD91ACDAC1A8D61399FC65E1A38601D1B95D6C3_RuntimeMethod_var);
		// return FakeLatency;
		float L_24 = V_1;
		V_3 = L_24;
		goto IL_00ad;
	}

IL_00ad:
	{
		// }
		float L_25 = V_3;
		return L_25;
	}
}
// System.Void RemoteLoopbackManager/SimulatedLatencySettings::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimulatedLatencySettings__ctor_mDC4701382DCAB4B34E2AE9BD7ED27DCEC0306B6C (SimulatedLatencySettings_t3C66FBDC0E1133AA2BAAF5E3BBEBB4B8B6A1894B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1__ctor_m1D81D06D2B8525B2D4C1EE8CA5D423E890F97E4F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float FakeLatencyMax = 0.25f; //250 ms max latency
		__this->set_FakeLatencyMax_0((0.25f));
		// public float FakeLatencyMin = 0.002f; //2ms min latency
		__this->set_FakeLatencyMin_1((0.00200000009f));
		// public float LatencyWeight = 0.25f;  // How much the latest sample impacts the current latency
		__this->set_LatencyWeight_2((0.25f));
		// public int MaxSamples = 4; //How many samples in our window
		__this->set_MaxSamples_3(4);
		// internal float AverageWindow = 0f;
		__this->set_AverageWindow_4((0.0f));
		// internal float LatencySum = 0f;
		__this->set_LatencySum_5((0.0f));
		// internal LinkedList<float> LatencyValues = new LinkedList<float>();
		LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * L_0 = (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 *)il2cpp_codegen_object_new(LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m1D81D06D2B8525B2D4C1EE8CA5D423E890F97E4F(L_0, /*hidden argument*/LinkedList_1__ctor_m1D81D06D2B8525B2D4C1EE8CA5D423E890F97E4F_RuntimeMethod_var);
		__this->set_LatencyValues_6(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RuntimeRopeGenerator/<MakeRope>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CMakeRopeU3Ed__2__ctor_mC9B96B6A48DE9C6A24651C61A20885D4B7BFAC2D (U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void RuntimeRopeGenerator/<MakeRope>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CMakeRopeU3Ed__2_System_IDisposable_Dispose_mAF88235C710602C1022A42A1C91CEAC0CFCB8204 (U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean RuntimeRopeGenerator/<MakeRope>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CMakeRopeU3Ed__2_MoveNext_mD3BFD03EF7B85140E88BF2A029A1FBEEB5B83504 (U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_0035;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return 0;
		int32_t L_3 = 0;
		RuntimeObject * L_4 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_3);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0035:
	{
		__this->set_U3CU3E1__state_0((-1));
		// }
		return (bool)0;
	}
}
// System.Object RuntimeRopeGenerator/<MakeRope>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CMakeRopeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1E799A4BCA7AA45E2E66D688FC6DBE189FDFE596 (U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void RuntimeRopeGenerator/<MakeRope>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CMakeRopeU3Ed__2_System_Collections_IEnumerator_Reset_m21ABBBA4A71228DA2105ABF24C336ED84D28D1C2 (U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CMakeRopeU3Ed__2_System_Collections_IEnumerator_Reset_m21ABBBA4A71228DA2105ABF24C336ED84D28D1C2_RuntimeMethod_var)));
	}
}
// System.Object RuntimeRopeGenerator/<MakeRope>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CMakeRopeU3Ed__2_System_Collections_IEnumerator_get_Current_m46764A418CA35B282FE4A27DE6EE40D497E8AE97 (U3CMakeRopeU3Ed__2_t0DE42ECBF2D14062FF21214E9BC8FAFFD574CCC7 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RuntimeRopeGeneratorUse/<Start>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__2__ctor_mB2576F12E41960219B8D4B925C0E50DF2A66FC9E (U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void RuntimeRopeGeneratorUse/<Start>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__2_System_IDisposable_Dispose_m443D3F88C18C90B75A79EA1C9056696B94DCC154 (U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean RuntimeRopeGeneratorUse/<Start>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartU3Ed__2_MoveNext_m7175EACB83490273BDD9BD4FD91E1A4CBE7C38F7 (U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_0064;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		// rg = new RuntimeRopeGenerator();
		RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC * L_3 = __this->get_U3CU3E4__this_2();
		RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * L_4 = (RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 *)il2cpp_codegen_object_new(RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488_il2cpp_TypeInfo_var);
		RuntimeRopeGenerator__ctor_mC6334710591DE477B4E1977384C18E199AC229BB(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_rg_5(L_4);
		// yield return rg.MakeRope(transform,Vector3.zero,1);
		RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * L_6 = L_5->get_rg_5();
		RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_6);
		RuntimeObject* L_10;
		L_10 = RuntimeRopeGenerator_MakeRope_m75FA53894436086130B97A840F5FDD2FC27B16AE(L_6, L_8, L_9, (1.0f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_10);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0064:
	{
		__this->set_U3CU3E1__state_0((-1));
		// rg.AddPendulum(pendulum,Vector3.up*0.5f);
		RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC * L_11 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		RuntimeRopeGenerator_t63B7C61886CD8951800281A509D37F0902AAA488 * L_12 = L_11->get_rg_5();
		RuntimeRopeGeneratorUse_tCD79C104B7E52840C4DD32A637FEC2713209ECEC * L_13 = __this->get_U3CU3E4__this_2();
		NullCheck(L_13);
		ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9 * L_14 = L_13->get_pendulum_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_15, (0.5f), /*hidden argument*/NULL);
		NullCheck(L_12);
		RuntimeRopeGenerator_AddPendulum_mC44C741BD87348055DED2613E4B3F2C1F036FF27(L_12, L_14, L_16, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object RuntimeRopeGeneratorUse/<Start>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E92812F9987D045E11CB2653D3BE2668B002811 (U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void RuntimeRopeGeneratorUse/<Start>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_mF62F9BBE198D60E90417C026611B81DD3461E8DC (U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_mF62F9BBE198D60E90417C026611B81DD3461E8DC_RuntimeMethod_var)));
	}
}
// System.Object RuntimeRopeGeneratorUse/<Start>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m555815E4C6D334F953A3D60A4858AF925B400F3D (U3CStartU3Ed__2_t71DEF2674912F8FFBCDB46FD689095D3BBB919CA * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ServerManagerUI/<Init>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitU3Ed__4__ctor_m96FAED00B123A4C00B5EF446FCDC08A68379DF49 (U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void ServerManagerUI/<Init>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitU3Ed__4_System_IDisposable_Dispose_mC8CFAEBABC78B67AD5FB8DA056A5D20F6D807142 (U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean ServerManagerUI/<Init>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CInitU3Ed__4_MoveNext_mE9566C1EB60855ABD5C0B78367C5D1A52E72A38B (U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ServerManagerUI_EventBackToMenu_m00165244790CC8711D3DCDD50583E651FA4C2B1F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ServerManagerUI_EventLogout_mE50C0004EB72022F33618CA19F5192B54105BEA3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ServerManagerUI_EventOpenAllInOne_m4B5B93774BAB0D0F99E375B317B5297C6423B02F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ServerManagerUI_EventOpenDataRecord_m3C42AE3C3F077C2D88079C129DEB95601D606A1A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ServerManagerUI_EventOpenMenu_m862BD21B1C41A02A3EC56B6C849F2CD6FD5A192C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_008c;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		// panelLogin.gameObject.SetActive(true);
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41 * L_4 = L_3->get_panelLogin_4();
		NullCheck(L_4);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)1, /*hidden argument*/NULL);
		// panelMenu.gameObject.SetActive(false);
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_6 = __this->get_U3CU3E4__this_2();
		NullCheck(L_6);
		Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 * L_7 = L_6->get_panelMenu_5();
		NullCheck(L_7);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8;
		L_8 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_8, (bool)0, /*hidden argument*/NULL);
		// panelAllInOne.gameObject.SetActive(false);
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_9 = __this->get_U3CU3E4__this_2();
		NullCheck(L_9);
		Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D * L_10 = L_9->get_panelAllInOne_6();
		NullCheck(L_10);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11;
		L_11 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_11, (bool)0, /*hidden argument*/NULL);
		// panelDataRecord.gameObject.SetActive(false);
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_12 = __this->get_U3CU3E4__this_2();
		NullCheck(L_12);
		Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57 * L_13 = L_12->get_panelDataRecord_7();
		NullCheck(L_13);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14;
		L_14 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_14, (bool)0, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_008c:
	{
		__this->set_U3CU3E1__state_0((-1));
		// panelLogin.EHOpenMenuPanel += EventOpenMenu;
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_15 = __this->get_U3CU3E4__this_2();
		NullCheck(L_15);
		Panel_Login_tD85BFCEAED72080ABAACAE2E46C2AD844F45AA41 * L_16 = L_15->get_panelLogin_4();
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_17 = __this->get_U3CU3E4__this_2();
		EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * L_18 = (EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B *)il2cpp_codegen_object_new(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B_il2cpp_TypeInfo_var);
		EventHandler__ctor_m9D4CC7B806AFF3B790E5E1A3B891F6775A146B78(L_18, L_17, (intptr_t)((intptr_t)ServerManagerUI_EventOpenMenu_m862BD21B1C41A02A3EC56B6C849F2CD6FD5A192C_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_16);
		Panel_Login_add_EHOpenMenuPanel_mA55C094873CC99345C4F40C8F7EDB9541AF77731(L_16, L_18, /*hidden argument*/NULL);
		// panelMenu.EHOpenAllInOne += EventOpenAllInOne;
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_19 = __this->get_U3CU3E4__this_2();
		NullCheck(L_19);
		Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 * L_20 = L_19->get_panelMenu_5();
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_21 = __this->get_U3CU3E4__this_2();
		EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * L_22 = (EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B *)il2cpp_codegen_object_new(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B_il2cpp_TypeInfo_var);
		EventHandler__ctor_m9D4CC7B806AFF3B790E5E1A3B891F6775A146B78(L_22, L_21, (intptr_t)((intptr_t)ServerManagerUI_EventOpenAllInOne_m4B5B93774BAB0D0F99E375B317B5297C6423B02F_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_20);
		Panel_Menu_add_EHOpenAllInOne_m37C17885A25E4A6D83FFFF330F3E37F6BECCB8E2(L_20, L_22, /*hidden argument*/NULL);
		// panelMenu.EHOpenDataRecord += EventOpenDataRecord;
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_23 = __this->get_U3CU3E4__this_2();
		NullCheck(L_23);
		Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 * L_24 = L_23->get_panelMenu_5();
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_25 = __this->get_U3CU3E4__this_2();
		EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * L_26 = (EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B *)il2cpp_codegen_object_new(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B_il2cpp_TypeInfo_var);
		EventHandler__ctor_m9D4CC7B806AFF3B790E5E1A3B891F6775A146B78(L_26, L_25, (intptr_t)((intptr_t)ServerManagerUI_EventOpenDataRecord_m3C42AE3C3F077C2D88079C129DEB95601D606A1A_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		Panel_Menu_add_EHOpenDataRecord_m193BE0C8E56955ABE74A747F3579592A2416965A(L_24, L_26, /*hidden argument*/NULL);
		// panelMenu.EHLogout += EventLogout;
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_27 = __this->get_U3CU3E4__this_2();
		NullCheck(L_27);
		Panel_Menu_t19223260B5D743C897718F325ED7DF36D2B3A412 * L_28 = L_27->get_panelMenu_5();
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_29 = __this->get_U3CU3E4__this_2();
		EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * L_30 = (EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B *)il2cpp_codegen_object_new(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B_il2cpp_TypeInfo_var);
		EventHandler__ctor_m9D4CC7B806AFF3B790E5E1A3B891F6775A146B78(L_30, L_29, (intptr_t)((intptr_t)ServerManagerUI_EventLogout_mE50C0004EB72022F33618CA19F5192B54105BEA3_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_28);
		Panel_Menu_add_EHLogout_mAA883EE7947DB7A0540919530C59FF5846FEDC93(L_28, L_30, /*hidden argument*/NULL);
		// panelAllInOne.EHCloseAllInOne += EventBackToMenu;
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_31 = __this->get_U3CU3E4__this_2();
		NullCheck(L_31);
		Panel_AllInOne_tD23320AA517F279558A80B6E7F9BE0869809C10D * L_32 = L_31->get_panelAllInOne_6();
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_33 = __this->get_U3CU3E4__this_2();
		EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * L_34 = (EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B *)il2cpp_codegen_object_new(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B_il2cpp_TypeInfo_var);
		EventHandler__ctor_m9D4CC7B806AFF3B790E5E1A3B891F6775A146B78(L_34, L_33, (intptr_t)((intptr_t)ServerManagerUI_EventBackToMenu_m00165244790CC8711D3DCDD50583E651FA4C2B1F_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_32);
		Panel_AllInOne_add_EHCloseAllInOne_m65626A3BB4CF4110B13AD9BBB334F32B219DC1CE(L_32, L_34, /*hidden argument*/NULL);
		// panelDataRecord.EHCloseDataRecord += EventBackToMenu;
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_35 = __this->get_U3CU3E4__this_2();
		NullCheck(L_35);
		Panel_DataRecord_t8AB241E0C9817515ADE30E639DEF58107FBCBD57 * L_36 = L_35->get_panelDataRecord_7();
		ServerManagerUI_t06659A6138840CB90906D828DA386FC6A644AA8D * L_37 = __this->get_U3CU3E4__this_2();
		EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B * L_38 = (EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B *)il2cpp_codegen_object_new(EventHandler_t084491E53EC706ACA0A15CA17488C075B4ECA44B_il2cpp_TypeInfo_var);
		EventHandler__ctor_m9D4CC7B806AFF3B790E5E1A3B891F6775A146B78(L_38, L_37, (intptr_t)((intptr_t)ServerManagerUI_EventBackToMenu_m00165244790CC8711D3DCDD50583E651FA4C2B1F_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_36);
		Panel_DataRecord_add_EHCloseDataRecord_mCC095FF201C79024C6A9A708A1C44EACF59E7300(L_36, L_38, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object ServerManagerUI/<Init>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED67422E32CABAC4657637A811C9DD6C6689B4D0 (U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void ServerManagerUI/<Init>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitU3Ed__4_System_Collections_IEnumerator_Reset_mAD7BB77764B97952953510C1E0453EE78593E9D4 (U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CInitU3Ed__4_System_Collections_IEnumerator_Reset_mAD7BB77764B97952953510C1E0453EE78593E9D4_RuntimeMethod_var)));
	}
}
// System.Object ServerManagerUI/<Init>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitU3Ed__4_System_Collections_IEnumerator_get_Current_mEA9B9FE6F9C30ECB95B87241FEA66918AD8BACE8 (U3CInitU3Ed__4_t3960C693DD1FA507B0FF08448C22C1D8BE277CB2 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void StartMenu/<>c__DisplayClass3_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0__ctor_mB4BEA21B02CB620F151FB8C1D57C0384883E9130 (U3CU3Ec__DisplayClass3_0_t27B782281E85B4B9B9C194DA1B73C3535D5CC46D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartMenu/<>c__DisplayClass3_0::<Start>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0_U3CStartU3Eb__0_mFE41F96EFC3E7314714C2E5F6392280A788A23D2 (U3CU3Ec__DisplayClass3_0_t27B782281E85B4B9B9C194DA1B73C3535D5CC46D * __this, const RuntimeMethod* method)
{
	{
		// DebugUIBuilder.instance.AddButton(Path.GetFileNameWithoutExtension(path), () => LoadScene(sceneIndex));
		StartMenu_t549376AD4E2045D8D42C75AF8C5ED808D56807F3 * L_0 = __this->get_U3CU3E4__this_1();
		int32_t L_1 = __this->get_sceneIndex_0();
		NullCheck(L_0);
		StartMenu_LoadScene_mF7663ABFF602CD53F3A26C076E6A8FA200347F97(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateProjectionMatrixLoopU3Ed__10__ctor_m0C230674C5D40700A51231AB15CD4362B42129F5 (U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateProjectionMatrixLoopU3Ed__10_System_IDisposable_Dispose_mCC990D854F67E1FEAB8C5942350714360CAA6C4B (U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CUpdateProjectionMatrixLoopU3Ed__10_MoveNext_m60419720F9995FAD7FF38AEB23CF2CD5660607D3 (U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	int32_t G_B13_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_001d;
			}
			case 2:
			{
				goto IL_001f;
			}
		}
	}
	{
		goto IL_0024;
	}

IL_001b:
	{
		goto IL_0026;
	}

IL_001d:
	{
		goto IL_004b;
	}

IL_001f:
	{
		goto IL_01c7;
	}

IL_0024:
	{
		return (bool)0;
	}

IL_0026:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_01db;
	}

IL_0033:
	{
		goto IL_0053;
	}

IL_0036:
	{
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_2 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_2, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_2);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_004b:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0053:
	{
		// while (!allowUpdate || ForceDisableUpdate)
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		bool L_4 = L_3->get_allowUpdate_9();
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		bool L_6 = L_5->get_ForceDisableUpdate_10();
		G_B13_0 = ((int32_t)(L_6));
		goto IL_006e;
	}

IL_006d:
	{
		G_B13_0 = 1;
	}

IL_006e:
	{
		V_1 = (bool)G_B13_0;
		bool L_7 = V_1;
		if (L_7)
		{
			goto IL_0036;
		}
	}
	{
		// Matrix4x4 rm = referenceCam.projectionMatrix;
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_8 = __this->get_U3CU3E4__this_2();
		NullCheck(L_8);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_9 = L_8->get_referenceCam_4();
		NullCheck(L_9);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_10;
		L_10 = Camera_get_projectionMatrix_mDB77E3A7F71CEF085797BCE58FAC78058C5D6756(L_9, /*hidden argument*/NULL);
		__this->set_U3CrmU3E5__1_3(L_10);
		// if (!useCustomFOV)
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_11 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		bool L_12 = L_11->get_useCustomFOV_6();
		V_2 = (bool)((((int32_t)L_12) == ((int32_t)0))? 1 : 0);
		bool L_13 = V_2;
		if (!L_13)
		{
			goto IL_00b7;
		}
	}
	{
		// fov = referenceCam.fieldOfView;
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_14 = __this->get_U3CU3E4__this_2();
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_15 = __this->get_U3CU3E4__this_2();
		NullCheck(L_15);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_16 = L_15->get_referenceCam_4();
		NullCheck(L_16);
		float L_17;
		L_17 = Camera_get_fieldOfView_mA9BA910800B2E33B572929CDA9A12CE596353920(L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_fov_7(L_17);
	}

IL_00b7:
	{
		// if (maxFovAsReference)
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_18 = __this->get_U3CU3E4__this_2();
		NullCheck(L_18);
		bool L_19 = L_18->get_maxFovAsReference_8();
		V_3 = L_19;
		bool L_20 = V_3;
		if (!L_20)
		{
			goto IL_0108;
		}
	}
	{
		// if (fov > referenceCam.fieldOfView)
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_21 = __this->get_U3CU3E4__this_2();
		NullCheck(L_21);
		float L_22 = L_21->get_fov_7();
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_23 = __this->get_U3CU3E4__this_2();
		NullCheck(L_23);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_24 = L_23->get_referenceCam_4();
		NullCheck(L_24);
		float L_25;
		L_25 = Camera_get_fieldOfView_mA9BA910800B2E33B572929CDA9A12CE596353920(L_24, /*hidden argument*/NULL);
		V_4 = (bool)((((float)L_22) > ((float)L_25))? 1 : 0);
		bool L_26 = V_4;
		if (!L_26)
		{
			goto IL_0107;
		}
	}
	{
		// fov = referenceCam.fieldOfView;
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_27 = __this->get_U3CU3E4__this_2();
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_28 = __this->get_U3CU3E4__this_2();
		NullCheck(L_28);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_29 = L_28->get_referenceCam_4();
		NullCheck(L_29);
		float L_30;
		L_30 = Camera_get_fieldOfView_mA9BA910800B2E33B572929CDA9A12CE596353920(L_29, /*hidden argument*/NULL);
		NullCheck(L_27);
		L_27->set_fov_7(L_30);
	}

IL_0107:
	{
	}

IL_0108:
	{
		// float aspect = referenceCam.aspect;
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_31 = __this->get_U3CU3E4__this_2();
		NullCheck(L_31);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_32 = L_31->get_referenceCam_4();
		NullCheck(L_32);
		float L_33;
		L_33 = Camera_get_aspect_mD0A1FC8F998473DA08866FF9CD61C02E6D5F4987(L_32, /*hidden argument*/NULL);
		__this->set_U3CaspectU3E5__2_4(L_33);
		// float matrixY = 1f / Mathf.Tan(fov / (2f * Mathf.Rad2Deg));
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_34 = __this->get_U3CU3E4__this_2();
		NullCheck(L_34);
		float L_35 = L_34->get_fov_7();
		float L_36;
		L_36 = tanf(((float)((float)L_35/(float)(114.59156f))));
		__this->set_U3CmatrixYU3E5__3_5(((float)((float)(1.0f)/(float)L_36)));
		// float matrixX = matrixY / aspect; // as matrixY IS the calculated fov ratio
		float L_37 = __this->get_U3CmatrixYU3E5__3_5();
		float L_38 = __this->get_U3CaspectU3E5__2_4();
		__this->set_U3CmatrixXU3E5__4_6(((float)((float)L_37/(float)L_38)));
		// rm[0, 0] = matrixX;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * L_39 = __this->get_address_of_U3CrmU3E5__1_3();
		float L_40 = __this->get_U3CmatrixXU3E5__4_6();
		Matrix4x4_set_Item_mE286A89718710DDF166DF6ACF8A480D15FE06B2F((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)L_39, 0, 0, L_40, /*hidden argument*/NULL);
		// rm[1, 1] = matrixY;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * L_41 = __this->get_address_of_U3CrmU3E5__1_3();
		float L_42 = __this->get_U3CmatrixYU3E5__3_5();
		Matrix4x4_set_Item_mE286A89718710DDF166DF6ACF8A480D15FE06B2F((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)L_41, 1, 1, L_42, /*hidden argument*/NULL);
		// targetCam.fieldOfView = fov;
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_43 = __this->get_U3CU3E4__this_2();
		NullCheck(L_43);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_44 = L_43->get_targetCam_5();
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_45 = __this->get_U3CU3E4__this_2();
		NullCheck(L_45);
		float L_46 = L_45->get_fov_7();
		NullCheck(L_44);
		Camera_set_fieldOfView_m138FE103CAC4B803F39E4CF579609A5C3FEB5E49(L_44, L_46, /*hidden argument*/NULL);
		// targetCam.projectionMatrix = rm;
		TargetProjectionMatrix_t40BA01B4A2B528E39AC1DFEC9EAE43BCC3CD523F * L_47 = __this->get_U3CU3E4__this_2();
		NullCheck(L_47);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_48 = L_47->get_targetCam_5();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_49 = __this->get_U3CrmU3E5__1_3();
		NullCheck(L_48);
		Camera_set_projectionMatrix_m3645AC49FC94726BDA07191C80299D8361D5C328(L_48, L_49, /*hidden argument*/NULL);
		// yield return new WaitForSeconds(0.005f);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_50 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_50, (0.00499999989f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_50);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_01c7:
	{
		__this->set_U3CU3E1__state_0((-1));
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * L_51 = __this->get_address_of_U3CrmU3E5__1_3();
		il2cpp_codegen_initobj(L_51, sizeof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 ));
	}

IL_01db:
	{
		// while (true)
		V_5 = (bool)1;
		goto IL_0033;
	}
}
// System.Object TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3DC7F959E369D83651E676D2FFCF66AE85A2C36 (U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_Reset_m70A89E669D02A74E6DAABC101346075B8F0F3F25 (U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_Reset_m70A89E669D02A74E6DAABC101346075B8F0F3F25_RuntimeMethod_var)));
	}
}
// System.Object TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_get_Current_m7071709350C18754652B239E532208352E7E4601 (U3CUpdateProjectionMatrixLoopU3Ed__10_t2348C0E897127234B082CA6DD9A430F2771D1BCD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TeleportInputHandler/<TeleportAimCoroutine>d__6::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportAimCoroutineU3Ed__6__ctor_mE2F9BDC7C3225737B5ADD2AD7AAF0E449CFADE47 (U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TeleportInputHandler/<TeleportAimCoroutine>d__6::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportAimCoroutineU3Ed__6_System_IDisposable_Dispose_m443089F5C3CB24CF2E58C43B6D940BB6A97E7059 (U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TeleportInputHandler/<TeleportAimCoroutine>d__6::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTeleportAimCoroutineU3Ed__6_MoveNext_m3D532A7ED8718469618989D091672C1E1E833FA0 (U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B13_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_005a;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		// LocomotionTeleport.TeleportIntentions intention = GetIntention();
		TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* LocomotionTeleport/TeleportIntentions TeleportInputHandler::GetIntention() */, L_3);
		__this->set_U3CintentionU3E5__1_3(L_4);
		goto IL_0073;
	}

IL_0033:
	{
		// LocomotionTeleport.CurrentIntention = intention;
		TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_6;
		L_6 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_5, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_U3CintentionU3E5__1_3();
		NullCheck(L_6);
		L_6->set_CurrentIntention_18(L_7);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_005a:
	{
		__this->set_U3CU3E1__state_0((-1));
		// intention = GetIntention();
		TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * L_8 = __this->get_U3CU3E4__this_2();
		NullCheck(L_8);
		int32_t L_9;
		L_9 = VirtFuncInvoker0< int32_t >::Invoke(8 /* LocomotionTeleport/TeleportIntentions TeleportInputHandler::GetIntention() */, L_8);
		__this->set_U3CintentionU3E5__1_3(L_9);
	}

IL_0073:
	{
		// while (intention == LocomotionTeleport.TeleportIntentions.Aim || intention == LocomotionTeleport.TeleportIntentions.PreTeleport)
		int32_t L_10 = __this->get_U3CintentionU3E5__1_3();
		if ((((int32_t)L_10) == ((int32_t)1)))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_11 = __this->get_U3CintentionU3E5__1_3();
		G_B13_0 = ((((int32_t)L_11) == ((int32_t)2))? 1 : 0);
		goto IL_0088;
	}

IL_0087:
	{
		G_B13_0 = 1;
	}

IL_0088:
	{
		V_1 = (bool)G_B13_0;
		bool L_12 = V_1;
		if (L_12)
		{
			goto IL_0033;
		}
	}
	{
		// LocomotionTeleport.CurrentIntention = intention;
		TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * L_13 = __this->get_U3CU3E4__this_2();
		NullCheck(L_13);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_14;
		L_14 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_13, /*hidden argument*/NULL);
		int32_t L_15 = __this->get_U3CintentionU3E5__1_3();
		NullCheck(L_14);
		L_14->set_CurrentIntention_18(L_15);
		// }
		return (bool)0;
	}
}
// System.Object TeleportInputHandler/<TeleportAimCoroutine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTeleportAimCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02583125A115ADE4CC8896C0108B7A2ADD9FCAFE (U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TeleportInputHandler/<TeleportAimCoroutine>d__6::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mE894BDC51B100595A10489704983F8922185CCF8 (U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_mE894BDC51B100595A10489704983F8922185CCF8_RuntimeMethod_var)));
	}
}
// System.Object TeleportInputHandler/<TeleportAimCoroutine>d__6::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTeleportAimCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m28CB29942297ECF9C327DDDAB3D53AF8A77ABF05 (U3CTeleportAimCoroutineU3Ed__6_tCB63F9E53975A1A5FAD660B3F2A2BBFDC62B24B7 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TeleportInputHandler/<TeleportReadyCoroutine>d__5::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportReadyCoroutineU3Ed__5__ctor_m7C396BD2404ABCBF1CF83320ACF13FA1B621B4FB (U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportReadyCoroutineU3Ed__5_System_IDisposable_Dispose_mA2DFF1E0C765EA7B2F4BA2E1EF7721049E8EA046 (U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TeleportInputHandler/<TeleportReadyCoroutine>d__5::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTeleportReadyCoroutineU3Ed__5_MoveNext_m5A64679B873617863C4B38AA8008C8CC1E564029 (U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_0033;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_003b;
	}

IL_0022:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0033:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_003b:
	{
		// while (GetIntention() != LocomotionTeleport.TeleportIntentions.Aim)
		TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = VirtFuncInvoker0< int32_t >::Invoke(8 /* LocomotionTeleport/TeleportIntentions TeleportInputHandler::GetIntention() */, L_3);
		V_1 = (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_5 = V_1;
		if (L_5)
		{
			goto IL_0022;
		}
	}
	{
		// LocomotionTeleport.CurrentIntention = LocomotionTeleport.TeleportIntentions.Aim;
		TeleportInputHandler_t578EF30A84A8A5B0538B7474C664F9EC28BF106D * L_6 = __this->get_U3CU3E4__this_2();
		NullCheck(L_6);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_7;
		L_7 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		L_7->set_CurrentIntention_18(1);
		// }
		return (bool)0;
	}
}
// System.Object TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTeleportReadyCoroutineU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41A381D2B7B87AD5810E05A3C55A66FD7D8CADA2 (U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m12F830ADD072802D84BE4B3231F43A12FA4F8E73 (U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_Reset_m12F830ADD072802D84BE4B3231F43A12FA4F8E73_RuntimeMethod_var)));
	}
}
// System.Object TeleportInputHandler/<TeleportReadyCoroutine>d__5::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTeleportReadyCoroutineU3Ed__5_System_Collections_IEnumerator_get_Current_mF8243C3A677F944C06A59D21D17CC3AF8DD1B594 (U3CTeleportReadyCoroutineU3Ed__5_tD24456DD09C4CDDB04F3F677F58ECE51D0582848 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateOrientationCoroutineU3Ed__7__ctor_m5DFE7ADCE7BF42B372F2097C1C019F301BBE3DFE (U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateOrientationCoroutineU3Ed__7_System_IDisposable_Dispose_m159520706F62855F2F55F4686A47A89D093AD2B0 (U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CUpdateOrientationCoroutineU3Ed__7_MoveNext_m2D766D50632FD169A27EF1E4EE39B50AAEF4512C (U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	int32_t G_B15_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_005f;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		// InitializeTeleportDestination();
		TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(8 /* System.Void TeleportOrientationHandler::InitializeTeleportDestination() */, L_3);
		goto IL_0067;
	}

IL_002e:
	{
		// if (AimData != null)
		TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D * L_4 = __this->get_U3CU3E4__this_2();
		NullCheck(L_4);
		AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * L_5 = L_4->get_AimData_8();
		V_1 = (bool)((!(((RuntimeObject*)(AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA *)L_5) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		// UpdateTeleportDestination();
		TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(9 /* System.Void TeleportOrientationHandler::UpdateTeleportDestination() */, L_7);
	}

IL_004f:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_005f:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0067:
	{
		// while (LocomotionTeleport.CurrentState == LocomotionTeleport.States.Aim || LocomotionTeleport.CurrentState == LocomotionTeleport.States.PreTeleport)
		TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D * L_8 = __this->get_U3CU3E4__this_2();
		NullCheck(L_8);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_9;
		L_9 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10;
		L_10 = LocomotionTeleport_get_CurrentState_mCEDC22238BFC0EB60D9DE2616698C71D0CEF0BA8_inline(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)1)))
		{
			goto IL_008f;
		}
	}
	{
		TeleportOrientationHandler_t2D984EDC757C7E5AD387D11F0720BE8679FCDA2D * L_11 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_12;
		L_12 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13;
		L_13 = LocomotionTeleport_get_CurrentState_mCEDC22238BFC0EB60D9DE2616698C71D0CEF0BA8_inline(L_12, /*hidden argument*/NULL);
		G_B15_0 = ((((int32_t)L_13) == ((int32_t)3))? 1 : 0);
		goto IL_0090;
	}

IL_008f:
	{
		G_B15_0 = 1;
	}

IL_0090:
	{
		V_2 = (bool)G_B15_0;
		bool L_14 = V_2;
		if (L_14)
		{
			goto IL_002e;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87C4EB920BA4BF331493243117342D5E99905F1C (U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m8957A7F5F8D1D9C630D84F24A7C2B66642CA9B95 (U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m8957A7F5F8D1D9C630D84F24A7C2B66642CA9B95_RuntimeMethod_var)));
	}
}
// System.Object TeleportOrientationHandler/<UpdateOrientationCoroutine>d__7::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CUpdateOrientationCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mDB4E059717A313A10584DB05F88E813A45D9A606 (U3CUpdateOrientationCoroutineU3Ed__7_t08EC88545E04719BD7CA409A02BB443CB86819E5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TeleportTargetHandler/<TargetAimCoroutine>d__7::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAimCoroutineU3Ed__7__ctor_m23D9D0D8A352A65E0D99985A676E454304BF8FE4 (U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TeleportTargetHandler/<TargetAimCoroutine>d__7::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAimCoroutineU3Ed__7_System_IDisposable_Dispose_m21B111D2C0E7C1EB3CE98929690C0DD190716414 (U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TeleportTargetHandler/<TargetAimCoroutine>d__7::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTargetAimCoroutineU3Ed__7_MoveNext_m67B9E5EA47ACD29BD93DB469A85D596C9D94F69A (U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Nullable_1_get_HasValue_mB9EAE3168E00BA12AA7E1233A4A0007FD12BB9E7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	bool V_4 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		goto IL_001b;
	}

IL_0014:
	{
		goto IL_01cf;
	}

IL_0019:
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_01e3;
	}

IL_0028:
	{
		// ResetAimData();
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(8 /* System.Void TeleportTargetHandler::ResetAimData() */, L_3);
		// var current = LocomotionTeleport.transform.position;
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_4 = __this->get_U3CU3E4__this_2();
		NullCheck(L_4);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_5;
		L_5 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		__this->set_U3CcurrentU3E5__1_3(L_7);
		// _aimPoints.Clear();
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_8 = __this->get_U3CU3E4__this_2();
		NullCheck(L_8);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_9 = L_8->get__aimPoints_9();
		NullCheck(L_9);
		List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702(L_9, /*hidden argument*/List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702_RuntimeMethod_var);
		// LocomotionTeleport.AimHandler.GetPoints(_aimPoints);
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_10 = __this->get_U3CU3E4__this_2();
		NullCheck(L_10);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_11;
		L_11 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		TeleportAimHandler_tC1C6D09FC478B420E7C892ECCB6E625F537D3BBD * L_12 = L_11->get_AimHandler_13();
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_13 = __this->get_U3CU3E4__this_2();
		NullCheck(L_13);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_14 = L_13->get__aimPoints_9();
		NullCheck(L_12);
		VirtActionInvoker1< List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * >::Invoke(8 /* System.Void TeleportAimHandler::GetPoints(System.Collections.Generic.List`1<UnityEngine.Vector3>) */, L_12, L_14);
		// for(int i = 0; i < _aimPoints.Count; i++)
		__this->set_U3CiU3E5__2_4(0);
		goto IL_0184;
	}

IL_008e:
	{
		// var adjustedPoint = _aimPoints[i];
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_15 = __this->get_U3CU3E4__this_2();
		NullCheck(L_15);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_16 = L_15->get__aimPoints_9();
		int32_t L_17 = __this->get_U3CiU3E5__2_4();
		NullCheck(L_16);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_16, L_17, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		__this->set_U3CadjustedPointU3E5__3_5(L_18);
		// AimData.TargetValid = ConsiderTeleport(current, ref adjustedPoint);
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_19 = __this->get_U3CU3E4__this_2();
		NullCheck(L_19);
		AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * L_20 = L_19->get_AimData_7();
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_21 = __this->get_U3CU3E4__this_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22 = __this->get_U3CcurrentU3E5__1_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_23 = __this->get_address_of_U3CadjustedPointU3E5__3_5();
		NullCheck(L_21);
		bool L_24;
		L_24 = VirtFuncInvoker2< bool, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * >::Invoke(9 /* System.Boolean TeleportTargetHandler::ConsiderTeleport(UnityEngine.Vector3,UnityEngine.Vector3&) */, L_21, L_22, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_23);
		NullCheck(L_20);
		L_20->set_TargetValid_1(L_24);
		// AimData.Points.Add(adjustedPoint);
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_25 = __this->get_U3CU3E4__this_2();
		NullCheck(L_25);
		AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * L_26 = L_25->get_AimData_7();
		NullCheck(L_26);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_27;
		L_27 = AimData_get_Points_m3169977C66C0D412F32CECB2B7FD471F65A191B2_inline(L_26, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28 = __this->get_U3CadjustedPointU3E5__3_5();
		NullCheck(L_27);
		List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_27, L_28, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
		// if (AimData.TargetValid)
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_29 = __this->get_U3CU3E4__this_2();
		NullCheck(L_29);
		AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * L_30 = L_29->get_AimData_7();
		NullCheck(L_30);
		bool L_31 = L_30->get_TargetValid_1();
		V_1 = L_31;
		bool L_32 = V_1;
		if (!L_32)
		{
			goto IL_014b;
		}
	}
	{
		// AimData.Destination = ConsiderDestination(adjustedPoint);
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_33 = __this->get_U3CU3E4__this_2();
		NullCheck(L_33);
		AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * L_34 = L_33->get_AimData_7();
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_35 = __this->get_U3CU3E4__this_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36 = __this->get_U3CadjustedPointU3E5__3_5();
		NullCheck(L_35);
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258  L_37;
		L_37 = VirtFuncInvoker1< Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 , Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(10 /* System.Nullable`1<UnityEngine.Vector3> TeleportTargetHandler::ConsiderDestination(UnityEngine.Vector3) */, L_35, L_36);
		NullCheck(L_34);
		L_34->set_Destination_2(L_37);
		// AimData.TargetValid = AimData.Destination.HasValue;
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_38 = __this->get_U3CU3E4__this_2();
		NullCheck(L_38);
		AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * L_39 = L_38->get_AimData_7();
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_40 = __this->get_U3CU3E4__this_2();
		NullCheck(L_40);
		AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * L_41 = L_40->get_AimData_7();
		NullCheck(L_41);
		Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 * L_42 = L_41->get_address_of_Destination_2();
		bool L_43;
		L_43 = Nullable_1_get_HasValue_mB9EAE3168E00BA12AA7E1233A4A0007FD12BB9E7_inline((Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 *)L_42, /*hidden argument*/Nullable_1_get_HasValue_mB9EAE3168E00BA12AA7E1233A4A0007FD12BB9E7_RuntimeMethod_var);
		NullCheck(L_39);
		L_39->set_TargetValid_1(L_43);
		// break;
		goto IL_01a3;
	}

IL_014b:
	{
		// current = _aimPoints[i];
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_44 = __this->get_U3CU3E4__this_2();
		NullCheck(L_44);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_45 = L_44->get__aimPoints_9();
		int32_t L_46 = __this->get_U3CiU3E5__2_4();
		NullCheck(L_45);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47;
		L_47 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_45, L_46, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		__this->set_U3CcurrentU3E5__1_3(L_47);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_48 = __this->get_address_of_U3CadjustedPointU3E5__3_5();
		il2cpp_codegen_initobj(L_48, sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		// for(int i = 0; i < _aimPoints.Count; i++)
		int32_t L_49 = __this->get_U3CiU3E5__2_4();
		V_2 = L_49;
		int32_t L_50 = V_2;
		__this->set_U3CiU3E5__2_4(((int32_t)il2cpp_codegen_add((int32_t)L_50, (int32_t)1)));
	}

IL_0184:
	{
		// for(int i = 0; i < _aimPoints.Count; i++)
		int32_t L_51 = __this->get_U3CiU3E5__2_4();
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_52 = __this->get_U3CU3E4__this_2();
		NullCheck(L_52);
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_53 = L_52->get__aimPoints_9();
		NullCheck(L_53);
		int32_t L_54;
		L_54 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_53, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		V_3 = (bool)((((int32_t)L_51) < ((int32_t)L_54))? 1 : 0);
		bool L_55 = V_3;
		if (L_55)
		{
			goto IL_008e;
		}
	}

IL_01a3:
	{
		// LocomotionTeleport.OnUpdateAimData(AimData);
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_56 = __this->get_U3CU3E4__this_2();
		NullCheck(L_56);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_57;
		L_57 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_56, /*hidden argument*/NULL);
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_58 = __this->get_U3CU3E4__this_2();
		NullCheck(L_58);
		AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * L_59 = L_58->get_AimData_7();
		NullCheck(L_57);
		LocomotionTeleport_OnUpdateAimData_m76ED9D517FF45B8A7CE7757BD0E8D147892F90A8(L_57, L_59, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_01cf:
	{
		__this->set_U3CU3E1__state_0((-1));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_60 = __this->get_address_of_U3CcurrentU3E5__1_3();
		il2cpp_codegen_initobj(L_60, sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
	}

IL_01e3:
	{
		// while (LocomotionTeleport.CurrentState == LocomotionTeleport.States.Aim)
		TeleportTargetHandler_t08585D582D3CDE6D7BECA85833479451FFC3D273 * L_61 = __this->get_U3CU3E4__this_2();
		NullCheck(L_61);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_62;
		L_62 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63;
		L_63 = LocomotionTeleport_get_CurrentState_mCEDC22238BFC0EB60D9DE2616698C71D0CEF0BA8_inline(L_62, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_63) == ((int32_t)1))? 1 : 0);
		bool L_64 = V_4;
		if (L_64)
		{
			goto IL_0028;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object TeleportTargetHandler/<TargetAimCoroutine>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTargetAimCoroutineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73510D945678774D27D2FEE03C45CF482A2EBBE6 (U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TeleportTargetHandler/<TargetAimCoroutine>d__7::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m2BC8EDEB99F6EEEA6DB6EE8F703C99E5CC6A7301 (U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_Reset_m2BC8EDEB99F6EEEA6DB6EE8F703C99E5CC6A7301_RuntimeMethod_var)));
	}
}
// System.Object TeleportTargetHandler/<TargetAimCoroutine>d__7::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTargetAimCoroutineU3Ed__7_System_Collections_IEnumerator_get_Current_mF0915E668ADBDEC27BF20ED85385A44114046F62 (U3CTargetAimCoroutineU3Ed__7_t105CA081A5F93C24A3D7607840940BE5D3A33AAF * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TeleportTransitionBlink/<BlinkCoroutine>d__4::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkCoroutineU3Ed__4__ctor_m73115DAF8E2D89A49A9F6BD9AB03485AA7551BCD (U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TeleportTransitionBlink/<BlinkCoroutine>d__4::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkCoroutineU3Ed__4_System_IDisposable_Dispose_m6A66CCE325E70F5794414025EA6A7B8EABFAE259 (U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TeleportTransitionBlink/<BlinkCoroutine>d__4::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CBlinkCoroutineU3Ed__4_MoveNext_mBEC5B507954BA0961920B4141D150885CE4D309B (U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	int32_t G_B12_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_0073;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		// LocomotionTeleport.IsTransitioning = true;
		TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977 * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_4;
		L_4 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_IsTransitioning_20((bool)1);
		// float elapsedTime = 0;
		__this->set_U3CelapsedTimeU3E5__1_3((0.0f));
		// var teleportTime = TransitionDuration * TeleportDelay;
		TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977 * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		float L_6 = L_5->get_TransitionDuration_6();
		TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977 * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		float L_8 = L_7->get_TeleportDelay_7();
		__this->set_U3CteleportTimeU3E5__2_4(((float)il2cpp_codegen_multiply((float)L_6, (float)L_8)));
		// var teleported = false;
		__this->set_U3CteleportedU3E5__3_5((bool)0);
		goto IL_00c7;
	}

IL_0062:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0073:
	{
		__this->set_U3CU3E1__state_0((-1));
		// elapsedTime += Time.deltaTime;
		float L_9 = __this->get_U3CelapsedTimeU3E5__1_3();
		float L_10;
		L_10 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_U3CelapsedTimeU3E5__1_3(((float)il2cpp_codegen_add((float)L_9, (float)L_10)));
		// if (!teleported && elapsedTime >= teleportTime)
		bool L_11 = __this->get_U3CteleportedU3E5__3_5();
		if (L_11)
		{
			goto IL_00a7;
		}
	}
	{
		float L_12 = __this->get_U3CelapsedTimeU3E5__1_3();
		float L_13 = __this->get_U3CteleportTimeU3E5__2_4();
		G_B12_0 = ((((int32_t)((!(((float)L_12) >= ((float)L_13)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00a8;
	}

IL_00a7:
	{
		G_B12_0 = 0;
	}

IL_00a8:
	{
		V_1 = (bool)G_B12_0;
		bool L_14 = V_1;
		if (!L_14)
		{
			goto IL_00c6;
		}
	}
	{
		// teleported = true;
		__this->set_U3CteleportedU3E5__3_5((bool)1);
		// LocomotionTeleport.DoTeleport();
		TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977 * L_15 = __this->get_U3CU3E4__this_2();
		NullCheck(L_15);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_16;
		L_16 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		LocomotionTeleport_DoTeleport_m1477EB38D2FD6E2AD4A7553EB3237B18D40C0B0C(L_16, /*hidden argument*/NULL);
	}

IL_00c6:
	{
	}

IL_00c7:
	{
		// while (elapsedTime < TransitionDuration)
		float L_17 = __this->get_U3CelapsedTimeU3E5__1_3();
		TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977 * L_18 = __this->get_U3CU3E4__this_2();
		NullCheck(L_18);
		float L_19 = L_18->get_TransitionDuration_6();
		V_2 = (bool)((((float)L_17) < ((float)L_19))? 1 : 0);
		bool L_20 = V_2;
		if (L_20)
		{
			goto IL_0062;
		}
	}
	{
		// LocomotionTeleport.IsTransitioning = false;
		TeleportTransitionBlink_t405541A5340FA52DC0FB59AD894186D86D33F977 * L_21 = __this->get_U3CU3E4__this_2();
		NullCheck(L_21);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_22;
		L_22 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		L_22->set_IsTransitioning_20((bool)0);
		// }
		return (bool)0;
	}
}
// System.Object TeleportTransitionBlink/<BlinkCoroutine>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBlinkCoroutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB31D456C6490D00742481C0F18817914FC76018 (U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TeleportTransitionBlink/<BlinkCoroutine>d__4::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m6E21661229B022076FA60AF6C49A1980E56609E1 (U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_Reset_m6E21661229B022076FA60AF6C49A1980E56609E1_RuntimeMethod_var)));
	}
}
// System.Object TeleportTransitionBlink/<BlinkCoroutine>d__4::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBlinkCoroutineU3Ed__4_System_Collections_IEnumerator_get_Current_mAA053761AFFEF4D53F8AC25C2BDA6DC4729996DF (U3CBlinkCoroutineU3Ed__4_t22431191674A306D5943B04EE01DDAB238A30A9A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TeleportTransitionWarp/<DoWarp>d__3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDoWarpU3Ed__3__ctor_mA023019BAD546DB8BFCFC8326BF22566DC855B45 (U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TeleportTransitionWarp/<DoWarp>d__3::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDoWarpU3Ed__3_System_IDisposable_Dispose_mE361978D56E79BC8335160F0DA180CB96DCE29F9 (U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TeleportTransitionWarp/<DoWarp>d__3::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDoWarpU3Ed__3_MoveNext_m84CA338E50BBE257229411F56CD6B4D7F46015B1 (U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		goto IL_001b;
	}

IL_0014:
	{
		goto IL_00cb;
	}

IL_0019:
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// LocomotionTeleport.IsTransitioning = true;
		TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_4;
		L_4 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->set_IsTransitioning_20((bool)1);
		// var startPosition = LocomotionTeleport.GetCharacterPosition();
		TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_6;
		L_6 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = LocomotionTeleport_GetCharacterPosition_m30F7EB551F5787D2489CE2716284B848D2C11D26(L_6, /*hidden argument*/NULL);
		__this->set_U3CstartPositionU3E5__1_3(L_7);
		// float elapsedTime = 0;
		__this->set_U3CelapsedTimeU3E5__2_4((0.0f));
		goto IL_00d3;
	}

IL_0057:
	{
		// elapsedTime += Time.deltaTime;
		float L_8 = __this->get_U3CelapsedTimeU3E5__2_4();
		float L_9;
		L_9 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_U3CelapsedTimeU3E5__2_4(((float)il2cpp_codegen_add((float)L_8, (float)L_9)));
		// var t = elapsedTime / TransitionDuration;
		float L_10 = __this->get_U3CelapsedTimeU3E5__2_4();
		TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * L_11 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		float L_12 = L_11->get_TransitionDuration_6();
		__this->set_U3CtU3E5__3_5(((float)((float)L_10/(float)L_12)));
		// var pLerp = PositionLerp.Evaluate(t);
		TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * L_13 = __this->get_U3CU3E4__this_2();
		NullCheck(L_13);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_14 = L_13->get_PositionLerp_7();
		float L_15 = __this->get_U3CtU3E5__3_5();
		NullCheck(L_14);
		float L_16;
		L_16 = AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A(L_14, L_15, /*hidden argument*/NULL);
		__this->set_U3CpLerpU3E5__4_6(L_16);
		// LocomotionTeleport.DoWarp(startPosition, pLerp);
		TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * L_17 = __this->get_U3CU3E4__this_2();
		NullCheck(L_17);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_18;
		L_18 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_17, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19 = __this->get_U3CstartPositionU3E5__1_3();
		float L_20 = __this->get_U3CpLerpU3E5__4_6();
		NullCheck(L_18);
		LocomotionTeleport_DoWarp_m2195B0DF360E9B04AD486855F66560DFC22EEB76(L_18, L_19, L_20, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00cb:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00d3:
	{
		// while (elapsedTime < TransitionDuration)
		float L_21 = __this->get_U3CelapsedTimeU3E5__2_4();
		TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * L_22 = __this->get_U3CU3E4__this_2();
		NullCheck(L_22);
		float L_23 = L_22->get_TransitionDuration_6();
		V_1 = (bool)((((float)L_21) < ((float)L_23))? 1 : 0);
		bool L_24 = V_1;
		if (L_24)
		{
			goto IL_0057;
		}
	}
	{
		// LocomotionTeleport.DoWarp(startPosition, 1.0f);
		TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * L_25 = __this->get_U3CU3E4__this_2();
		NullCheck(L_25);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_26;
		L_26 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_25, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27 = __this->get_U3CstartPositionU3E5__1_3();
		NullCheck(L_26);
		LocomotionTeleport_DoWarp_m2195B0DF360E9B04AD486855F66560DFC22EEB76(L_26, L_27, (1.0f), /*hidden argument*/NULL);
		// LocomotionTeleport.IsTransitioning = false;
		TeleportTransitionWarp_t8CCCAD91DA4969819A4D68DB925708D377323E37 * L_28 = __this->get_U3CU3E4__this_2();
		NullCheck(L_28);
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_29;
		L_29 = TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		L_29->set_IsTransitioning_20((bool)0);
		// }
		return (bool)0;
	}
}
// System.Object TeleportTransitionWarp/<DoWarp>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDoWarpU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94EA3502B1F9442D23A5EBF53483F6C85D484208 (U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TeleportTransitionWarp/<DoWarp>d__3::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDoWarpU3Ed__3_System_Collections_IEnumerator_Reset_mE692557C58D90117915B599FA91A88FC7D238200 (U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CDoWarpU3Ed__3_System_Collections_IEnumerator_Reset_mE692557C58D90117915B599FA91A88FC7D238200_RuntimeMethod_var)));
	}
}
// System.Object TeleportTransitionWarp/<DoWarp>d__3::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDoWarpU3Ed__3_System_Collections_IEnumerator_get_Current_mD1CE2712B5B0919B97EE26421E8DDC32F11E5314 (U3CDoWarpU3Ed__3_t4348B9455D74E7F0B4AFBE2D1FFD204E4906B935 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TextureEncoder/<>c__DisplayClass52_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass52_0__ctor_mC913D0C8AE158CA693E67A5CCABC2512C3245FF4 (U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TextureEncoder/<>c__DisplayClass52_0::<EncodeBytes>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass52_0_U3CEncodeBytesU3Eb__0_m4C4569E76CED51732F353063F2BB0EDFCCC3A091 (U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * __this, const RuntimeMethod* method)
{
	{
		// dataByte = RawTextureData.FMRawTextureDataToJPG(streamWidth, streamHeight, Quality, ChromaSubsampling);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_0 = __this->get_U3CU3E4__this_1();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_1 = __this->get_U3CU3E4__this_1();
		NullCheck(L_1);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = L_1->get_RawTextureData_33();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_3 = __this->get_U3CU3E4__this_1();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_streamWidth_34();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_5 = __this->get_U3CU3E4__this_1();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_streamHeight_35();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_7 = __this->get_U3CU3E4__this_1();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_Quality_13();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_9 = __this->get_U3CU3E4__this_1();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_ChromaSubsampling_14();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11;
		L_11 = FMExtensionMethods_FMRawTextureDataToJPG_m85A2EA12E76A38D087B912D0CED0F084C3D9EE8A(L_2, L_4, L_6, L_8, L_10, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_dataByte_31(L_11);
		// AsyncEncoding = false;
		__this->set_AsyncEncoding_0((bool)0);
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TextureEncoder/<EncodeBytes>d__52::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CEncodeBytesU3Ed__52__ctor_m466C96E960C64C1A08D40CAFA7B15519B3BF7222 (U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TextureEncoder/<EncodeBytes>d__52::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CEncodeBytesU3Ed__52_System_IDisposable_Dispose_mD120CA1771B0C5533F78D015D0754FE83065A0D1 (U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TextureEncoder/<EncodeBytes>d__52::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CEncodeBytesU3Ed__52_MoveNext_mD170B8A76DDDCB34603A5E040260C6A9F876381F (U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass52_0_U3CEncodeBytesU3Eb__0_m4C4569E76CED51732F353063F2BB0EDFCCC3A091_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_m470057E7D3B20C1038D24F229A27EF3DEDD8E60D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_4 = NULL;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	int32_t V_8 = 0;
	bool V_9 = false;
	bool V_10 = false;
	int32_t G_B10_0 = 0;
	U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3 * G_B29_0 = NULL;
	U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3 * G_B28_0 = NULL;
	int32_t G_B30_0 = 0;
	U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3 * G_B30_1 = NULL;
	int32_t G_B32_0 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B32_1 = NULL;
	int32_t G_B31_0 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B31_1 = NULL;
	int32_t G_B33_0 = 0;
	int32_t G_B33_1 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* G_B33_2 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_001d;
			}
			case 2:
			{
				goto IL_0022;
			}
		}
	}
	{
		goto IL_0027;
	}

IL_001b:
	{
		goto IL_0029;
	}

IL_001d:
	{
		goto IL_00b6;
	}

IL_0022:
	{
		goto IL_0132;
	}

IL_0027:
	{
		return (bool)0;
	}

IL_0029:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (FastMode)
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		bool L_3 = L_2->get_FastMode_10();
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_013d;
		}
	}
	{
		// if (AsyncMode && Loom.numThreads < Loom.maxThreads)
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		bool L_6 = L_5->get_AsyncMode_11();
		if (!L_6)
		{
			goto IL_005f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_StaticFields*)il2cpp_codegen_static_fields_for(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_il2cpp_TypeInfo_var))->get_numThreads_5();
		int32_t L_8;
		L_8 = Loom_get_maxThreads_m45F4C5CDCCA9EA30915641C7D48A8A1B312CDD2A(/*hidden argument*/NULL);
		G_B10_0 = ((((int32_t)L_7) < ((int32_t)L_8))? 1 : 0);
		goto IL_0060;
	}

IL_005f:
	{
		G_B10_0 = 0;
	}

IL_0060:
	{
		V_2 = (bool)G_B10_0;
		bool L_9 = V_2;
		if (!L_9)
		{
			goto IL_00d6;
		}
	}
	{
		U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * L_10 = (U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass52_0__ctor_mC913D0C8AE158CA693E67A5CCABC2512C3245FF4(L_10, /*hidden argument*/NULL);
		__this->set_U3CU3E8__7_9(L_10);
		U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * L_11 = __this->get_U3CU3E8__7_9();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_12 = __this->get_U3CU3E4__this_2();
		NullCheck(L_11);
		L_11->set_U3CU3E4__this_1(L_12);
		// bool AsyncEncoding = true;
		U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * L_13 = __this->get_U3CU3E8__7_9();
		NullCheck(L_13);
		L_13->set_AsyncEncoding_0((bool)1);
		// Loom.RunAsync(() =>
		// {
		//     dataByte = RawTextureData.FMRawTextureDataToJPG(streamWidth, streamHeight, Quality, ChromaSubsampling);
		//     AsyncEncoding = false;
		// });
		U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * L_14 = __this->get_U3CU3E8__7_9();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_15 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_15, L_14, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass52_0_U3CEncodeBytesU3Eb__0_m4C4569E76CED51732F353063F2BB0EDFCCC3A091_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Loom_tCC5BAE4709979649C71B7C0BB58EDC7C745542A0_il2cpp_TypeInfo_var);
		Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * L_16;
		L_16 = Loom_RunAsync_mF19ED64A5D75CBD0C9150286CEB6EB97CD3A8CF5(L_15, /*hidden argument*/NULL);
		goto IL_00bd;
	}

IL_00a6:
	{
		// while (AsyncEncoding) yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00b6:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00bd:
	{
		// while (AsyncEncoding) yield return null;
		U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 * L_17 = __this->get_U3CU3E8__7_9();
		NullCheck(L_17);
		bool L_18 = L_17->get_AsyncEncoding_0();
		V_3 = L_18;
		bool L_19 = V_3;
		if (L_19)
		{
			goto IL_00a6;
		}
	}
	{
		__this->set_U3CU3E8__7_9((U3CU3Ec__DisplayClass52_0_t5467CBA3A911DDF35A256699DC2B846F502037F0 *)NULL);
		goto IL_013a;
	}

IL_00d6:
	{
		// yield return dataByte = RawTextureData.FMRawTextureDataToJPG(streamWidth, streamHeight, Quality, ChromaSubsampling);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_20 = __this->get_U3CU3E4__this_2();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_21 = __this->get_U3CU3E4__this_2();
		NullCheck(L_21);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_22 = L_21->get_RawTextureData_33();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_23 = __this->get_U3CU3E4__this_2();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_streamWidth_34();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_25 = __this->get_U3CU3E4__this_2();
		NullCheck(L_25);
		int32_t L_26 = L_25->get_streamHeight_35();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_27 = __this->get_U3CU3E4__this_2();
		NullCheck(L_27);
		int32_t L_28 = L_27->get_Quality_13();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_29 = __this->get_U3CU3E4__this_2();
		NullCheck(L_29);
		int32_t L_30 = L_29->get_ChromaSubsampling_14();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_31;
		L_31 = FMExtensionMethods_FMRawTextureDataToJPG_m85A2EA12E76A38D087B912D0CED0F084C3D9EE8A(L_22, L_24, L_26, L_28, L_30, /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_32 = L_31;
		V_4 = L_32;
		NullCheck(L_20);
		L_20->set_dataByte_31(L_32);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_33 = V_4;
		__this->set_U3CU3E2__current_1((RuntimeObject *)L_33);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0132:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_013a:
	{
		goto IL_0186;
	}

IL_013d:
	{
		// dataByte = RawTextureData.FMRawTextureDataToJPG(streamWidth, streamHeight, Quality, ChromaSubsampling);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_34 = __this->get_U3CU3E4__this_2();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_35 = __this->get_U3CU3E4__this_2();
		NullCheck(L_35);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_36 = L_35->get_RawTextureData_33();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_37 = __this->get_U3CU3E4__this_2();
		NullCheck(L_37);
		int32_t L_38 = L_37->get_streamWidth_34();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_39 = __this->get_U3CU3E4__this_2();
		NullCheck(L_39);
		int32_t L_40 = L_39->get_streamHeight_35();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_41 = __this->get_U3CU3E4__this_2();
		NullCheck(L_41);
		int32_t L_42 = L_41->get_Quality_13();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_43 = __this->get_U3CU3E4__this_2();
		NullCheck(L_43);
		int32_t L_44 = L_43->get_ChromaSubsampling_14();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_45;
		L_45 = FMExtensionMethods_FMRawTextureDataToJPG_m85A2EA12E76A38D087B912D0CED0F084C3D9EE8A(L_36, L_38, L_40, L_42, L_44, /*hidden argument*/NULL);
		NullCheck(L_34);
		L_34->set_dataByte_31(L_45);
	}

IL_0186:
	{
		// if (ignoreSimilarTexture)
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_46 = __this->get_U3CU3E4__this_2();
		NullCheck(L_46);
		bool L_47 = L_46->get_ignoreSimilarTexture_17();
		V_5 = L_47;
		bool L_48 = V_5;
		if (!L_48)
		{
			goto IL_01e7;
		}
	}
	{
		// float diff = Mathf.Abs(lastRawDataByte - dataByte.Length);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_49 = __this->get_U3CU3E4__this_2();
		NullCheck(L_49);
		int32_t L_50 = L_49->get_lastRawDataByte_18();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_51 = __this->get_U3CU3E4__this_2();
		NullCheck(L_51);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_52 = L_51->get_dataByte_31();
		NullCheck(L_52);
		int32_t L_53;
		L_53 = Mathf_Abs_mE46B08A540F26741910760E84ACB6AACD996C3C0(((int32_t)il2cpp_codegen_subtract((int32_t)L_50, (int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_52)->max_length))))), /*hidden argument*/NULL);
		__this->set_U3CdiffU3E5__8_10(((float)((float)L_53)));
		// if (diff < similarByteSizeThreshold)
		float L_54 = __this->get_U3CdiffU3E5__8_10();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_55 = __this->get_U3CU3E4__this_2();
		NullCheck(L_55);
		int32_t L_56 = L_55->get_similarByteSizeThreshold_19();
		V_6 = (bool)((((float)L_54) < ((float)((float)((float)L_56))))? 1 : 0);
		bool L_57 = V_6;
		if (!L_57)
		{
			goto IL_01e6;
		}
	}
	{
		// EncodingTexture = false;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_58 = __this->get_U3CU3E4__this_2();
		NullCheck(L_58);
		L_58->set_EncodingTexture_21((bool)0);
		// yield break;
		return (bool)0;
	}

IL_01e6:
	{
	}

IL_01e7:
	{
		// lastRawDataByte = dataByte.Length;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_59 = __this->get_U3CU3E4__this_2();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_60 = __this->get_U3CU3E4__this_2();
		NullCheck(L_60);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_61 = L_60->get_dataByte_31();
		NullCheck(L_61);
		NullCheck(L_59);
		L_59->set_lastRawDataByte_18(((int32_t)((int32_t)(((RuntimeArray*)L_61)->max_length))));
		// if (GZipMode) dataByte = dataByte.FMZipBytes();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_62 = __this->get_U3CU3E4__this_2();
		NullCheck(L_62);
		bool L_63 = L_62->get_GZipMode_12();
		V_7 = L_63;
		bool L_64 = V_7;
		if (!L_64)
		{
			goto IL_022b;
		}
	}
	{
		// if (GZipMode) dataByte = dataByte.FMZipBytes();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_65 = __this->get_U3CU3E4__this_2();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_66 = __this->get_U3CU3E4__this_2();
		NullCheck(L_66);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_67 = L_66->get_dataByte_31();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_68;
		L_68 = FMZipHelper_FMZipBytes_mFD4D5CDF5908FCBC97214EFBF45BD25AE810C26E(L_67, /*hidden argument*/NULL);
		NullCheck(L_65);
		L_65->set_dataByte_31(L_68);
	}

IL_022b:
	{
		// dataLength = dataByte.Length;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_69 = __this->get_U3CU3E4__this_2();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_70 = __this->get_U3CU3E4__this_2();
		NullCheck(L_70);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_71 = L_70->get_dataByte_31();
		NullCheck(L_71);
		NullCheck(L_69);
		L_69->set_dataLength_32(((int32_t)((int32_t)(((RuntimeArray*)L_71)->max_length))));
		// int _length = dataByte.Length;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_72 = __this->get_U3CU3E4__this_2();
		NullCheck(L_72);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_73 = L_72->get_dataByte_31();
		NullCheck(L_73);
		__this->set_U3C_lengthU3E5__1_3(((int32_t)((int32_t)(((RuntimeArray*)L_73)->max_length))));
		// int _offset = 0;
		__this->set_U3C_offsetU3E5__2_4(0);
		// byte[] _meta_label = BitConverter.GetBytes(label);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_74 = __this->get_U3CU3E4__this_2();
		NullCheck(L_74);
		int32_t L_75 = L_74->get_label_25();
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_76;
		L_76 = BitConverter_GetBytes_m5C926FE938C878F7E4E95A5DED46C34DB1431D39(L_75, /*hidden argument*/NULL);
		__this->set_U3C_meta_labelU3E5__3_5(L_76);
		// byte[] _meta_id = BitConverter.GetBytes(dataID);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_77 = __this->get_U3CU3E4__this_2();
		NullCheck(L_77);
		int32_t L_78 = L_77->get_dataID_26();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_79;
		L_79 = BitConverter_GetBytes_m5C926FE938C878F7E4E95A5DED46C34DB1431D39(L_78, /*hidden argument*/NULL);
		__this->set_U3C_meta_idU3E5__4_6(L_79);
		// byte[] _meta_length = BitConverter.GetBytes(_length);
		int32_t L_80 = __this->get_U3C_lengthU3E5__1_3();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_81;
		L_81 = BitConverter_GetBytes_m5C926FE938C878F7E4E95A5DED46C34DB1431D39(L_80, /*hidden argument*/NULL);
		__this->set_U3C_meta_lengthU3E5__5_7(L_81);
		// int chunks = Mathf.FloorToInt(dataByte.Length / chunkSize);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_82 = __this->get_U3CU3E4__this_2();
		NullCheck(L_82);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_83 = L_82->get_dataByte_31();
		NullCheck(L_83);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_84 = __this->get_U3CU3E4__this_2();
		NullCheck(L_84);
		int32_t L_85 = L_84->get_chunkSize_28();
		int32_t L_86;
		L_86 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_83)->max_length)))/(int32_t)L_85)))), /*hidden argument*/NULL);
		__this->set_U3CchunksU3E5__6_8(L_86);
		// for (int i = 0; i <= chunks; i++)
		__this->set_U3CiU3E5__9_11(0);
		goto IL_041b;
	}

IL_02cb:
	{
		// int SendByteLength = (i == chunks) ? (_length % chunkSize + 18) : (chunkSize + 18);
		int32_t L_87 = __this->get_U3CiU3E5__9_11();
		int32_t L_88 = __this->get_U3CchunksU3E5__6_8();
		G_B28_0 = __this;
		if ((((int32_t)L_87) == ((int32_t)L_88)))
		{
			G_B29_0 = __this;
			goto IL_02eb;
		}
	}
	{
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_89 = __this->get_U3CU3E4__this_2();
		NullCheck(L_89);
		int32_t L_90 = L_89->get_chunkSize_28();
		G_B30_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_90, (int32_t)((int32_t)18)));
		G_B30_1 = G_B28_0;
		goto IL_0300;
	}

IL_02eb:
	{
		int32_t L_91 = __this->get_U3C_lengthU3E5__1_3();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_92 = __this->get_U3CU3E4__this_2();
		NullCheck(L_92);
		int32_t L_93 = L_92->get_chunkSize_28();
		G_B30_0 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)L_91%(int32_t)L_93)), (int32_t)((int32_t)18)));
		G_B30_1 = G_B29_0;
	}

IL_0300:
	{
		NullCheck(G_B30_1);
		G_B30_1->set_U3CSendByteLengthU3E5__10_12(G_B30_0);
		// byte[] _meta_offset = BitConverter.GetBytes(_offset);
		int32_t L_94 = __this->get_U3C_offsetU3E5__2_4();
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t8DCBA24B909F1B221372AF2B37C76DCF614BA654_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_95;
		L_95 = BitConverter_GetBytes_m5C926FE938C878F7E4E95A5DED46C34DB1431D39(L_94, /*hidden argument*/NULL);
		__this->set_U3C_meta_offsetU3E5__11_13(L_95);
		// byte[] SendByte = new byte[SendByteLength];
		int32_t L_96 = __this->get_U3CSendByteLengthU3E5__10_12();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_97 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_96);
		__this->set_U3CSendByteU3E5__12_14(L_97);
		// Buffer.BlockCopy(_meta_label, 0, SendByte, 0, 4);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_98 = __this->get_U3C_meta_labelU3E5__3_5();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_99 = __this->get_U3CSendByteU3E5__12_14();
		Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725((RuntimeArray *)(RuntimeArray *)L_98, 0, (RuntimeArray *)(RuntimeArray *)L_99, 0, 4, /*hidden argument*/NULL);
		// Buffer.BlockCopy(_meta_id, 0, SendByte, 4, 4);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_100 = __this->get_U3C_meta_idU3E5__4_6();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_101 = __this->get_U3CSendByteU3E5__12_14();
		Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725((RuntimeArray *)(RuntimeArray *)L_100, 0, (RuntimeArray *)(RuntimeArray *)L_101, 4, 4, /*hidden argument*/NULL);
		// Buffer.BlockCopy(_meta_length, 0, SendByte, 8, 4);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_102 = __this->get_U3C_meta_lengthU3E5__5_7();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_103 = __this->get_U3CSendByteU3E5__12_14();
		Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725((RuntimeArray *)(RuntimeArray *)L_102, 0, (RuntimeArray *)(RuntimeArray *)L_103, 8, 4, /*hidden argument*/NULL);
		// Buffer.BlockCopy(_meta_offset, 0, SendByte, 12, 4);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_104 = __this->get_U3C_meta_offsetU3E5__11_13();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_105 = __this->get_U3CSendByteU3E5__12_14();
		Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725((RuntimeArray *)(RuntimeArray *)L_104, 0, (RuntimeArray *)(RuntimeArray *)L_105, ((int32_t)12), 4, /*hidden argument*/NULL);
		// SendByte[16] = (byte)(GZipMode ? 1 : 0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_106 = __this->get_U3CSendByteU3E5__12_14();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_107 = __this->get_U3CU3E4__this_2();
		NullCheck(L_107);
		bool L_108 = L_107->get_GZipMode_12();
		G_B31_0 = ((int32_t)16);
		G_B31_1 = L_106;
		if (L_108)
		{
			G_B32_0 = ((int32_t)16);
			G_B32_1 = L_106;
			goto IL_0394;
		}
	}
	{
		G_B33_0 = 0;
		G_B33_1 = G_B31_0;
		G_B33_2 = G_B31_1;
		goto IL_0395;
	}

IL_0394:
	{
		G_B33_0 = 1;
		G_B33_1 = G_B32_0;
		G_B33_2 = G_B32_1;
	}

IL_0395:
	{
		NullCheck(G_B33_2);
		(G_B33_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B33_1), (uint8_t)((int32_t)((uint8_t)G_B33_0)));
		// SendByte[17] = (byte)0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_109 = __this->get_U3CSendByteU3E5__12_14();
		NullCheck(L_109);
		(L_109)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (uint8_t)0);
		// Buffer.BlockCopy(dataByte, _offset, SendByte, 18, SendByte.Length - 18);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_110 = __this->get_U3CU3E4__this_2();
		NullCheck(L_110);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_111 = L_110->get_dataByte_31();
		int32_t L_112 = __this->get_U3C_offsetU3E5__2_4();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_113 = __this->get_U3CSendByteU3E5__12_14();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_114 = __this->get_U3CSendByteU3E5__12_14();
		NullCheck(L_114);
		Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725((RuntimeArray *)(RuntimeArray *)L_111, L_112, (RuntimeArray *)(RuntimeArray *)L_113, ((int32_t)18), ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_114)->max_length))), (int32_t)((int32_t)18))), /*hidden argument*/NULL);
		// OnDataByteReadyEvent.Invoke(SendByte);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_115 = __this->get_U3CU3E4__this_2();
		NullCheck(L_115);
		UnityEventByteArray_tD5103CBD7F77D5C7683025D9BCE91819B3E37F16 * L_116 = L_115->get_OnDataByteReadyEvent_24();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_117 = __this->get_U3CSendByteU3E5__12_14();
		NullCheck(L_116);
		UnityEvent_1_Invoke_m470057E7D3B20C1038D24F229A27EF3DEDD8E60D(L_116, L_117, /*hidden argument*/UnityEvent_1_Invoke_m470057E7D3B20C1038D24F229A27EF3DEDD8E60D_RuntimeMethod_var);
		// _offset += chunkSize;
		int32_t L_118 = __this->get_U3C_offsetU3E5__2_4();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_119 = __this->get_U3CU3E4__this_2();
		NullCheck(L_119);
		int32_t L_120 = L_119->get_chunkSize_28();
		__this->set_U3C_offsetU3E5__2_4(((int32_t)il2cpp_codegen_add((int32_t)L_118, (int32_t)L_120)));
		__this->set_U3C_meta_offsetU3E5__11_13((ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL);
		__this->set_U3CSendByteU3E5__12_14((ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL);
		// for (int i = 0; i <= chunks; i++)
		int32_t L_121 = __this->get_U3CiU3E5__9_11();
		V_8 = L_121;
		int32_t L_122 = V_8;
		__this->set_U3CiU3E5__9_11(((int32_t)il2cpp_codegen_add((int32_t)L_122, (int32_t)1)));
	}

IL_041b:
	{
		// for (int i = 0; i <= chunks; i++)
		int32_t L_123 = __this->get_U3CiU3E5__9_11();
		int32_t L_124 = __this->get_U3CchunksU3E5__6_8();
		V_9 = (bool)((((int32_t)((((int32_t)L_123) > ((int32_t)L_124))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_125 = V_9;
		if (L_125)
		{
			goto IL_02cb;
		}
	}
	{
		// dataID++;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_126 = __this->get_U3CU3E4__this_2();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_127 = __this->get_U3CU3E4__this_2();
		NullCheck(L_127);
		int32_t L_128 = L_127->get_dataID_26();
		NullCheck(L_126);
		L_126->set_dataID_26(((int32_t)il2cpp_codegen_add((int32_t)L_128, (int32_t)1)));
		// if (dataID > maxID) dataID = 0;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_129 = __this->get_U3CU3E4__this_2();
		NullCheck(L_129);
		int32_t L_130 = L_129->get_dataID_26();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_131 = __this->get_U3CU3E4__this_2();
		NullCheck(L_131);
		int32_t L_132 = L_131->get_maxID_27();
		V_10 = (bool)((((int32_t)L_130) > ((int32_t)L_132))? 1 : 0);
		bool L_133 = V_10;
		if (!L_133)
		{
			goto IL_0477;
		}
	}
	{
		// if (dataID > maxID) dataID = 0;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_134 = __this->get_U3CU3E4__this_2();
		NullCheck(L_134);
		L_134->set_dataID_26(0);
	}

IL_0477:
	{
		// EncodingTexture = false;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_135 = __this->get_U3CU3E4__this_2();
		NullCheck(L_135);
		L_135->set_EncodingTexture_21((bool)0);
		// yield break;
		return (bool)0;
	}
}
// System.Object TextureEncoder/<EncodeBytes>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CEncodeBytesU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A775392B9DC6011391FC9DB18C0606286964374 (U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TextureEncoder/<EncodeBytes>d__52::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_Reset_m5CC4264F59DB6C3D9A916AE8A5FEADDAF55A15A2 (U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_Reset_m5CC4264F59DB6C3D9A916AE8A5FEADDAF55A15A2_RuntimeMethod_var)));
	}
}
// System.Object TextureEncoder/<EncodeBytes>d__52::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_get_Current_m4890B1605C611D741CCA98D73B9980C33FD36A6E (U3CEncodeBytesU3Ed__52_t6ED5B432A0AB52C8C0B3EC02DB9ED099105DE8A3 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49__ctor_m7A7777671665613076FF3AA9D0634827F0CE1D1D (U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_IDisposable_Dispose_mC244647E21C1622C33C49437AA73760B07EA733B (U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_MoveNext_mCC7C1D079ECD84A73980C682B79EA37EE42CFFB8 (U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncGPUReadbackRequest_GetData_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mA5D2215016D842E07C2BFC1C268B22D24E9FDDBD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeArray_1_ToArray_m13D8B79ED269C75D10BE015EC81844C30603B838_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_001d;
			}
			case 2:
			{
				goto IL_001f;
			}
		}
	}
	{
		goto IL_0021;
	}

IL_001b:
	{
		goto IL_0023;
	}

IL_001d:
	{
		goto IL_003f;
	}

IL_001f:
	{
		goto IL_006c;
	}

IL_0021:
	{
		return (bool)0;
	}

IL_0023:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_2 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_2, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_2);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_003f:
	{
		__this->set_U3CU3E1__state_0((-1));
		// AsyncGPUReadbackRequest request = AsyncGPUReadback.Request(inputRenderTexture, 0, TextureFormat.RGB24);
		RenderTexture_t5FE7A5B47EF962A0E8D7BEBA05E9FC87D49A1849 * L_3 = __this->get_inputRenderTexture_2();
		AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA  L_4;
		L_4 = AsyncGPUReadback_Request_m248D4E95EB755543253C06E28A71816381225B2D(L_3, 0, 3, (Action_1_t542D0A6987D7110F66453B06D83AFE1D24208526 *)NULL, /*hidden argument*/NULL);
		__this->set_U3CrequestU3E5__1_4(L_4);
		goto IL_0073;
	}

IL_005c:
	{
		// while (!request.done) yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_006c:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0073:
	{
		// while (!request.done) yield return null;
		AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA * L_5 = __this->get_address_of_U3CrequestU3E5__1_4();
		bool L_6;
		L_6 = AsyncGPUReadbackRequest_get_done_m13E802E42592C58F5BD55E337F7EF2E29FA2D5AA((AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA *)L_5, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		bool L_7 = V_1;
		if (L_7)
		{
			goto IL_005c;
		}
	}
	{
		// if (!request.hasError) RawTextureData = request.GetData<byte>().ToArray();
		AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA * L_8 = __this->get_address_of_U3CrequestU3E5__1_4();
		bool L_9;
		L_9 = AsyncGPUReadbackRequest_get_hasError_m3DBBB101CE07C39D59FF8A7B96F4E96F3E8D5985((AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA *)L_8, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		bool L_10 = V_2;
		if (!L_10)
		{
			goto IL_00b6;
		}
	}
	{
		// if (!request.hasError) RawTextureData = request.GetData<byte>().ToArray();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_11 = __this->get_U3CU3E4__this_3();
		AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA * L_12 = __this->get_address_of_U3CrequestU3E5__1_4();
		NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  L_13;
		L_13 = AsyncGPUReadbackRequest_GetData_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mA5D2215016D842E07C2BFC1C268B22D24E9FDDBD((AsyncGPUReadbackRequest_t30AF24AAE0D52605C959B8A0520867C21BDDE9FA *)L_12, 0, /*hidden argument*/AsyncGPUReadbackRequest_GetData_TisByte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_mA5D2215016D842E07C2BFC1C268B22D24E9FDDBD_RuntimeMethod_var);
		V_3 = L_13;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_14;
		L_14 = NativeArray_1_ToArray_m13D8B79ED269C75D10BE015EC81844C30603B838((NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785 *)(&V_3), /*hidden argument*/NativeArray_1_ToArray_m13D8B79ED269C75D10BE015EC81844C30603B838_RuntimeMethod_var);
		NullCheck(L_11);
		L_11->set_RawTextureData_33(L_14);
	}

IL_00b6:
	{
		// NeedUpdateTexture = true;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_15 = __this->get_U3CU3E4__this_3();
		NullCheck(L_15);
		L_15->set_NeedUpdateTexture_20((bool)1);
		// }
		return (bool)0;
	}
}
// System.Object TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96C42559F165E59586219E1387AB2CB4D6BCA73D (U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_Reset_m765AE58387A0C89F17588BFF91919AEC413081FB (U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_Reset_m765AE58387A0C89F17588BFF91919AEC413081FB_RuntimeMethod_var)));
	}
}
// System.Object TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_get_Current_m792B625630F599B92747F7863B39D0C1F7A87FA7 (U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_t538093C7D919AE13641E56B80041C8A4EEB003B5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TextureEncoder/<SenderCOR>d__51::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSenderCORU3Ed__51__ctor_mA77EC35DD25ABA9B996DA8F9C5B0EDD5903A18FA (U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void TextureEncoder/<SenderCOR>d__51::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSenderCORU3Ed__51_System_IDisposable_Dispose_m83C50F3984BDE74629E8ACBF069BB0A0AA333753 (U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean TextureEncoder/<SenderCOR>d__51::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSenderCORU3Ed__51_MoveNext_m80A1E315C02D774471163852BC228E65D95C5D69 (U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_001d;
			}
			case 2:
			{
				goto IL_0022;
			}
		}
	}
	{
		goto IL_0027;
	}

IL_001b:
	{
		goto IL_0029;
	}

IL_001d:
	{
		goto IL_00ba;
	}

IL_0022:
	{
		goto IL_00d2;
	}

IL_0027:
	{
		return (bool)0;
	}

IL_0029:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_00da;
	}

IL_0036:
	{
		// if (Time.realtimeSinceStartup > next)
		float L_2;
		L_2 = Time_get_realtimeSinceStartup_m5228CC1C1E57213D4148E965499072EA70D8C02B(/*hidden argument*/NULL);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		float L_4 = L_3->get_next_29();
		V_1 = (bool)((((float)L_2) > ((float)L_4))? 1 : 0);
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_00c2;
		}
	}
	{
		// if (StreamFPS > 0)
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_6 = __this->get_U3CU3E4__this_2();
		NullCheck(L_6);
		float L_7 = L_6->get_StreamFPS_15();
		V_2 = (bool)((((float)L_7) > ((float)(0.0f)))? 1 : 0);
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_00aa;
		}
	}
	{
		// interval = 1f / StreamFPS;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_9 = __this->get_U3CU3E4__this_2();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_10 = __this->get_U3CU3E4__this_2();
		NullCheck(L_10);
		float L_11 = L_10->get_StreamFPS_15();
		NullCheck(L_9);
		L_9->set_interval_16(((float)((float)(1.0f)/(float)L_11)));
		// next = Time.realtimeSinceStartup + interval;
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_12 = __this->get_U3CU3E4__this_2();
		float L_13;
		L_13 = Time_get_realtimeSinceStartup_m5228CC1C1E57213D4148E965499072EA70D8C02B(/*hidden argument*/NULL);
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_14 = __this->get_U3CU3E4__this_2();
		NullCheck(L_14);
		float L_15 = L_14->get_interval_16();
		NullCheck(L_12);
		L_12->set_next_29(((float)il2cpp_codegen_add((float)L_13, (float)L_15)));
		// RequestTextureUpdate();
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_16 = __this->get_U3CU3E4__this_2();
		NullCheck(L_16);
		TextureEncoder_RequestTextureUpdate_m18AEFAA45404F0ADA2195C0C0C5C788760D9A72C(L_16, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00ba:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00c2:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_00d2:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00da:
	{
		// while (!stop)
		TextureEncoder_tBC84618D51BF0225B79DD883D0ED6ACE633296DA * L_17 = __this->get_U3CU3E4__this_2();
		NullCheck(L_17);
		bool L_18 = L_17->get_stop_30();
		V_3 = (bool)((((int32_t)L_18) == ((int32_t)0))? 1 : 0);
		bool L_19 = V_3;
		if (L_19)
		{
			goto IL_0036;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object TextureEncoder/<SenderCOR>d__51::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSenderCORU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8322C0C2E7D89D2C7143F8EF17A2A6E09A2F2A6 (U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void TextureEncoder/<SenderCOR>d__51::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSenderCORU3Ed__51_System_Collections_IEnumerator_Reset_m8DF573C5E0A33310CB292F0C8B8E3A2AB46F659A (U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSenderCORU3Ed__51_System_Collections_IEnumerator_Reset_m8DF573C5E0A33310CB292F0C8B8E3A2AB46F659A_RuntimeMethod_var)));
	}
}
// System.Object TextureEncoder/<SenderCOR>d__51::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSenderCORU3Ed__51_System_Collections_IEnumerator_get_Current_mDBCD80BACCBECF12F5C336C8003EB0C3BB48A9F5 (U3CSenderCORU3Ed__51_tCECC543D3BCA65FB34A6865EBAEEBE326845597E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CResetPositionU3Ed__26__ctor_m5635000743474AB03A282A54DAA034BE871944D8 (U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CResetPositionU3Ed__26_System_IDisposable_Dispose_mFD4D0FB62BBFDC93E0DEAD7EA68430B7A4ACAF69 (U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CResetPositionU3Ed__26_MoveNext_m5E72132EBEF7DB0236E6E8EBD6632A16E0F748E0 (U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_0092;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		// var startTime = Time.time;
		float L_3;
		L_3 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_U3CstartTimeU3E5__1_3(L_3);
		// var endTime = Time.time + LERP_TO_OLD_POS_DURATION;
		float L_4;
		L_4 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_U3CendTimeU3E5__2_4(((float)il2cpp_codegen_add((float)L_4, (float)(1.0f))));
		goto IL_009a;
	}

IL_003e:
	{
		// transform.localPosition = Vector3.Lerp(transform.localPosition, _oldPosition,
		//   (Time.time - startTime) / LERP_TO_OLD_POS_DURATION);
		TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151 * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_5, /*hidden argument*/NULL);
		TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151 * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2(L_8, /*hidden argument*/NULL);
		TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151 * L_10 = __this->get_U3CU3E4__this_2();
		NullCheck(L_10);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = L_10->get__oldPosition_20();
		float L_12;
		L_12 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_13 = __this->get_U3CstartTimeU3E5__1_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_9, L_11, ((float)((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_13))/(float)(1.0f))), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_6, L_14, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0092:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_009a:
	{
		// while (Time.time < endTime)
		float L_15;
		L_15 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_16 = __this->get_U3CendTimeU3E5__2_4();
		V_1 = (bool)((((float)L_15) < ((float)L_16))? 1 : 0);
		bool L_17 = V_1;
		if (L_17)
		{
			goto IL_003e;
		}
	}
	{
		// transform.localPosition = _oldPosition;
		TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151 * L_18 = __this->get_U3CU3E4__this_2();
		NullCheck(L_18);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_18, /*hidden argument*/NULL);
		TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151 * L_20 = __this->get_U3CU3E4__this_2();
		NullCheck(L_20);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21 = L_20->get__oldPosition_20();
		NullCheck(L_19);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_19, L_21, /*hidden argument*/NULL);
		// _lerpToOldPositionCr = null;
		TrainButtonVisualController_t723A8AC2E0FF93B8D83FCEFA3856A745E9B0F151 * L_22 = __this->get_U3CU3E4__this_2();
		NullCheck(L_22);
		L_22->set__lerpToOldPositionCr_19((Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 *)NULL);
		// }
		return (bool)0;
	}
}
// System.Object OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CResetPositionU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BE3FF62F4373A00DD3E8BBCBB2036F6761E547B (U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CResetPositionU3Ed__26_System_Collections_IEnumerator_Reset_mFAB28539B7B0AF0B783CD738C41D9DC51A1E8853 (U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CResetPositionU3Ed__26_System_Collections_IEnumerator_Reset_mFAB28539B7B0AF0B783CD738C41D9DC51A1E8853_RuntimeMethod_var)));
	}
}
// System.Object OculusSampleFramework.TrainButtonVisualController/<ResetPosition>d__26::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CResetPositionU3Ed__26_System_Collections_IEnumerator_get_Current_m84A8CF3B9E09A2B224C5C4969FBB701BB9E8FF7B (U3CResetPositionU3Ed__26_t18EC641E47205F09E3BAB1CD58C23444594D98B4 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAnimateCrossingU3Ed__15__ctor_mFAA4EB62DD8CF2E800A17FB70610D210D4572B2C (U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAnimateCrossingU3Ed__15_System_IDisposable_Dispose_m15B4C628F2391E4765AAD7E5FBEB1ED37163CF6C (U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CAnimateCrossingU3Ed__15_MoveNext_m9E22324ADCB019B88C3415E7B1087C2E5E277572 (U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		goto IL_001b;
	}

IL_0014:
	{
		goto IL_01b7;
	}

IL_0019:
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// ToggleLightObjects(true);
		TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 * L_3 = __this->get_U3CU3E4__this_3();
		NullCheck(L_3);
		TrainCrossingController_ToggleLightObjects_m4B93FC82AC5CA62BC1F0755839F5AE2D3983C38C(L_3, (bool)1, /*hidden argument*/NULL);
		// float animationEndTime = Time.time + animationLength;
		float L_4;
		L_4 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_5 = __this->get_animationLength_2();
		__this->set_U3CanimationEndTimeU3E5__1_4(((float)il2cpp_codegen_add((float)L_4, (float)L_5)));
		// float lightBlinkDuration = animationLength * 0.1f;
		float L_6 = __this->get_animationLength_2();
		__this->set_U3ClightBlinkDurationU3E5__2_5(((float)il2cpp_codegen_multiply((float)L_6, (float)(0.100000001f))));
		// float lightBlinkStartTime = Time.time;
		float L_7;
		L_7 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_U3ClightBlinkStartTimeU3E5__3_6(L_7);
		// float lightBlinkEndTime = Time.time + lightBlinkDuration;
		float L_8;
		L_8 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_9 = __this->get_U3ClightBlinkDurationU3E5__2_5();
		__this->set_U3ClightBlinkEndTimeU3E5__4_7(((float)il2cpp_codegen_add((float)L_8, (float)L_9)));
		// Material lightToBlinkOn = _lightsSide1Mat;
		TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 * L_10 = __this->get_U3CU3E4__this_3();
		NullCheck(L_10);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_11 = L_10->get__lightsSide1Mat_9();
		__this->set_U3ClightToBlinkOnU3E5__5_8(L_11);
		// Material lightToBlinkOff = _lightsSide2Mat;
		TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 * L_12 = __this->get_U3CU3E4__this_3();
		NullCheck(L_12);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_13 = L_12->get__lightsSide2Mat_10();
		__this->set_U3ClightToBlinkOffU3E5__6_9(L_13);
		// Color onColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_14), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_U3ConColorU3E5__7_10(L_14);
		// Color offColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_15), (1.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_U3CoffColorU3E5__8_11(L_15);
		goto IL_01bf;
	}

IL_00d6:
	{
		// float t = (Time.time - lightBlinkStartTime) / lightBlinkDuration;
		float L_16;
		L_16 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_17 = __this->get_U3ClightBlinkStartTimeU3E5__3_6();
		float L_18 = __this->get_U3ClightBlinkDurationU3E5__2_5();
		__this->set_U3CtU3E5__9_12(((float)((float)((float)il2cpp_codegen_subtract((float)L_16, (float)L_17))/(float)L_18)));
		// lightToBlinkOn.SetColor(_colorId, Color.Lerp(offColor, onColor, t));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_19 = __this->get_U3ClightToBlinkOnU3E5__5_8();
		TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 * L_20 = __this->get_U3CU3E4__this_3();
		NullCheck(L_20);
		int32_t L_21 = L_20->get__colorId_11();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_22 = __this->get_U3CoffColorU3E5__8_11();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_23 = __this->get_U3ConColorU3E5__7_10();
		float L_24 = __this->get_U3CtU3E5__9_12();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_25;
		L_25 = Color_Lerp_mC986D7F29103536908D76BD8FC59AA11DC33C197(L_22, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_19);
		Material_SetColor_m9DE63FCC5A31918F8A9A2E4FCED70C298677A7B4(L_19, L_21, L_25, /*hidden argument*/NULL);
		// lightToBlinkOff.SetColor(_colorId, Color.Lerp(onColor, offColor, t));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_26 = __this->get_U3ClightToBlinkOffU3E5__6_9();
		TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 * L_27 = __this->get_U3CU3E4__this_3();
		NullCheck(L_27);
		int32_t L_28 = L_27->get__colorId_11();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_29 = __this->get_U3ConColorU3E5__7_10();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_30 = __this->get_U3CoffColorU3E5__8_11();
		float L_31 = __this->get_U3CtU3E5__9_12();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_32;
		L_32 = Color_Lerp_mC986D7F29103536908D76BD8FC59AA11DC33C197(L_29, L_30, L_31, /*hidden argument*/NULL);
		NullCheck(L_26);
		Material_SetColor_m9DE63FCC5A31918F8A9A2E4FCED70C298677A7B4(L_26, L_28, L_32, /*hidden argument*/NULL);
		// if (Time.time > lightBlinkEndTime)
		float L_33;
		L_33 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_34 = __this->get_U3ClightBlinkEndTimeU3E5__4_7();
		V_1 = (bool)((((float)L_33) > ((float)L_34))? 1 : 0);
		bool L_35 = V_1;
		if (!L_35)
		{
			goto IL_01a7;
		}
	}
	{
		// Material temp = lightToBlinkOn;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_36 = __this->get_U3ClightToBlinkOnU3E5__5_8();
		__this->set_U3CtempU3E5__10_13(L_36);
		// lightToBlinkOn = lightToBlinkOff;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_37 = __this->get_U3ClightToBlinkOffU3E5__6_9();
		__this->set_U3ClightToBlinkOnU3E5__5_8(L_37);
		// lightToBlinkOff = temp;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_38 = __this->get_U3CtempU3E5__10_13();
		__this->set_U3ClightToBlinkOffU3E5__6_9(L_38);
		// lightBlinkStartTime = Time.time;
		float L_39;
		L_39 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_U3ClightBlinkStartTimeU3E5__3_6(L_39);
		// lightBlinkEndTime = Time.time + lightBlinkDuration;
		float L_40;
		L_40 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_41 = __this->get_U3ClightBlinkDurationU3E5__2_5();
		__this->set_U3ClightBlinkEndTimeU3E5__4_7(((float)il2cpp_codegen_add((float)L_40, (float)L_41)));
		__this->set_U3CtempU3E5__10_13((Material_t8927C00353A72755313F046D0CE85178AE8218EE *)NULL);
	}

IL_01a7:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_01b7:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_01bf:
	{
		// while (Time.time < animationEndTime)
		float L_42;
		L_42 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_43 = __this->get_U3CanimationEndTimeU3E5__1_4();
		V_2 = (bool)((((float)L_42) < ((float)L_43))? 1 : 0);
		bool L_44 = V_2;
		if (L_44)
		{
			goto IL_00d6;
		}
	}
	{
		// ToggleLightObjects(false);
		TrainCrossingController_t8EAFD0499EF55DDAFA9727BF0F1331832E38ED37 * L_45 = __this->get_U3CU3E4__this_3();
		NullCheck(L_45);
		TrainCrossingController_ToggleLightObjects_m4B93FC82AC5CA62BC1F0755839F5AE2D3983C38C(L_45, (bool)0, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CAnimateCrossingU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E5821A766805B7DA85AF5CDF5B3E79C9007A97D (U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_Reset_m1221B7705FF6866AB85D415E8D00A92F1B40B0C3 (U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_Reset_m1221B7705FF6866AB85D415E8D00A92F1B40B0C3_RuntimeMethod_var)));
	}
}
// System.Object OculusSampleFramework.TrainCrossingController/<AnimateCrossing>d__15::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CAnimateCrossingU3Ed__15_System_Collections_IEnumerator_get_Current_mDF8E56DBC547B33C024A5EB10750F3D8B075A412 (U3CAnimateCrossingU3Ed__15_t03284E4EFB0B5DF867473E3CCA455DD255B7BE79 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartStopTrainU3Ed__34__ctor_m39BEC907A4658AB1DC65D214A86D35F6E246795A (U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartStopTrainU3Ed__34_System_IDisposable_Dispose_m6A0A898733F46F849ACEF0984B4C8938E5A88B20 (U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartStopTrainU3Ed__34_MoveNext_m02D96704D3CA82F8BECFC12A487D9C4AC7903FA1 (U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723 * G_B9_0 = NULL;
	U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723 * G_B8_0 = NULL;
	float G_B10_0 = 0.0f;
	U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723 * G_B10_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		goto IL_001b;
	}

IL_0014:
	{
		goto IL_01ca;
	}

IL_0019:
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// float endSpeed = startTrain ? _initialSpeed : 0.0f;
		bool L_3 = __this->get_startTrain_2();
		G_B8_0 = __this;
		if (L_3)
		{
			G_B9_0 = __this;
			goto IL_0033;
		}
	}
	{
		G_B10_0 = (0.0f);
		G_B10_1 = G_B8_0;
		goto IL_003e;
	}

IL_0033:
	{
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_4 = __this->get_U3CU3E4__this_3();
		NullCheck(L_4);
		float L_5 = L_4->get__initialSpeed_19();
		G_B10_0 = L_5;
		G_B10_1 = G_B9_0;
	}

IL_003e:
	{
		NullCheck(G_B10_1);
		G_B10_1->set_U3CendSpeedU3E5__1_4(G_B10_0);
		// var timePeriodForSpeedChange = 3.0f;
		__this->set_U3CtimePeriodForSpeedChangeU3E5__2_5((3.0f));
		// if (startTrain)
		bool L_6 = __this->get_startTrain_2();
		V_1 = L_6;
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_0100;
		}
	}
	{
		// _smoke1.Play();
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_8 = __this->get_U3CU3E4__this_3();
		NullCheck(L_8);
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_9 = L_8->get__smoke1_34();
		NullCheck(L_9);
		ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50(L_9, /*hidden argument*/NULL);
		// _isMoving = true;
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_10 = __this->get_U3CU3E4__this_3();
		NullCheck(L_10);
		L_10->set__isMoving_37((bool)1);
		// var emissionModule1 = _smoke1.emission;
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_11 = __this->get_U3CU3E4__this_3();
		NullCheck(L_11);
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_12 = L_11->get__smoke1_34();
		NullCheck(L_12);
		EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D  L_13;
		L_13 = ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1(L_12, /*hidden argument*/NULL);
		__this->set_U3CemissionModule1U3E5__6_9(L_13);
		// var mainModule = _smoke1.main;
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_14 = __this->get_U3CU3E4__this_3();
		NullCheck(L_14);
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_15 = L_14->get__smoke1_34();
		NullCheck(L_15);
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_16;
		L_16 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(L_15, /*hidden argument*/NULL);
		__this->set_U3CmainModuleU3E5__7_10(L_16);
		// emissionModule1.rateOverTimeMultiplier = _standardRateOverTimeMultiplier;
		EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * L_17 = __this->get_address_of_U3CemissionModule1U3E5__6_9();
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_18 = __this->get_U3CU3E4__this_3();
		NullCheck(L_18);
		float L_19 = L_18->get__standardRateOverTimeMultiplier_41();
		EmissionModule_set_rateOverTimeMultiplier_m13A0F78D648A10145C2AE38A25A40E384791B961((EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D *)L_17, L_19, /*hidden argument*/NULL);
		// mainModule.maxParticles = _standardMaxParticles;
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * L_20 = __this->get_address_of_U3CmainModuleU3E5__7_10();
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_21 = __this->get_U3CU3E4__this_3();
		NullCheck(L_21);
		int32_t L_22 = L_21->get__standardMaxParticles_42();
		MainModule_set_maxParticles_m6FD14B44787B44D38DEDAE4FCBE9596A86A0460D((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)L_20, L_22, /*hidden argument*/NULL);
		// timePeriodForSpeedChange = PlayEngineSound(EngineSoundState.Start);
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_23 = __this->get_U3CU3E4__this_3();
		NullCheck(L_23);
		float L_24;
		L_24 = TrainLocomotive_PlayEngineSound_mBBE7F6F9988F707FFC60EE195E18C7C7C37C05CE(L_23, 0, /*hidden argument*/NULL);
		__this->set_U3CtimePeriodForSpeedChangeU3E5__2_5(L_24);
		EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D * L_25 = __this->get_address_of_U3CemissionModule1U3E5__6_9();
		il2cpp_codegen_initobj(L_25, sizeof(EmissionModule_tE778D94F4003A96ECE3D8B670DDEDD2D557DE52D ));
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * L_26 = __this->get_address_of_U3CmainModuleU3E5__7_10();
		il2cpp_codegen_initobj(L_26, sizeof(MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B ));
		goto IL_0114;
	}

IL_0100:
	{
		// timePeriodForSpeedChange = PlayEngineSound(EngineSoundState.Stop);
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_27 = __this->get_U3CU3E4__this_3();
		NullCheck(L_27);
		float L_28;
		L_28 = TrainLocomotive_PlayEngineSound_mBBE7F6F9988F707FFC60EE195E18C7C7C37C05CE(L_27, 2, /*hidden argument*/NULL);
		__this->set_U3CtimePeriodForSpeedChangeU3E5__2_5(L_28);
	}

IL_0114:
	{
		// _engineAudioSource.loop = false;
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_29 = __this->get_U3CU3E4__this_3();
		NullCheck(L_29);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_30 = L_29->get__engineAudioSource_28();
		NullCheck(L_30);
		AudioSource_set_loop_mDD9FB746D8A7392472E5484EEF8D0A667993E3E0(L_30, (bool)0, /*hidden argument*/NULL);
		// timePeriodForSpeedChange = timePeriodForSpeedChange * 0.9f;
		float L_31 = __this->get_U3CtimePeriodForSpeedChangeU3E5__2_5();
		__this->set_U3CtimePeriodForSpeedChangeU3E5__2_5(((float)il2cpp_codegen_multiply((float)L_31, (float)(0.899999976f))));
		// float startTime = Time.time;
		float L_32;
		L_32 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		__this->set_U3CstartTimeU3E5__3_6(L_32);
		// float endTime = Time.time + timePeriodForSpeedChange;
		float L_33;
		L_33 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_34 = __this->get_U3CtimePeriodForSpeedChangeU3E5__2_5();
		__this->set_U3CendTimeU3E5__4_7(((float)il2cpp_codegen_add((float)L_33, (float)L_34)));
		// float startSpeed = _currentSpeed;
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_35 = __this->get_U3CU3E4__this_3();
		NullCheck(L_35);
		float L_36 = L_35->get__currentSpeed_39();
		__this->set_U3CstartSpeedU3E5__5_8(L_36);
		goto IL_01d2;
	}

IL_0168:
	{
		// float t = (Time.time - startTime) / timePeriodForSpeedChange;
		float L_37;
		L_37 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_38 = __this->get_U3CstartTimeU3E5__3_6();
		float L_39 = __this->get_U3CtimePeriodForSpeedChangeU3E5__2_5();
		__this->set_U3CtU3E5__8_11(((float)((float)((float)il2cpp_codegen_subtract((float)L_37, (float)L_38))/(float)L_39)));
		// _currentSpeed = startSpeed * (1.0f - t) + endSpeed * t;
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_40 = __this->get_U3CU3E4__this_3();
		float L_41 = __this->get_U3CstartSpeedU3E5__5_8();
		float L_42 = __this->get_U3CtU3E5__8_11();
		float L_43 = __this->get_U3CendSpeedU3E5__1_4();
		float L_44 = __this->get_U3CtU3E5__8_11();
		NullCheck(L_40);
		L_40->set__currentSpeed_39(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_41, (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_42)))), (float)((float)il2cpp_codegen_multiply((float)L_43, (float)L_44)))));
		// UpdateSmokeEmissionBasedOnSpeed();
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_45 = __this->get_U3CU3E4__this_3();
		NullCheck(L_45);
		TrainLocomotive_UpdateSmokeEmissionBasedOnSpeed_mABC70306F1FBF3D00E0BA0901760D44793F9D3EF(L_45, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_01ca:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_01d2:
	{
		// while (Time.time < endTime)
		float L_46;
		L_46 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		float L_47 = __this->get_U3CendTimeU3E5__4_7();
		V_2 = (bool)((((float)L_46) < ((float)L_47))? 1 : 0);
		bool L_48 = V_2;
		if (L_48)
		{
			goto IL_0168;
		}
	}
	{
		// _currentSpeed = endSpeed;
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_49 = __this->get_U3CU3E4__this_3();
		float L_50 = __this->get_U3CendSpeedU3E5__1_4();
		NullCheck(L_49);
		L_49->set__currentSpeed_39(L_50);
		// _startStopTrainCr = null;
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_51 = __this->get_U3CU3E4__this_3();
		NullCheck(L_51);
		L_51->set__startStopTrainCr_43((Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 *)NULL);
		// _isMoving = startTrain;
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_52 = __this->get_U3CU3E4__this_3();
		bool L_53 = __this->get_startTrain_2();
		NullCheck(L_52);
		L_52->set__isMoving_37(L_53);
		// if (!_isMoving)
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_54 = __this->get_U3CU3E4__this_3();
		NullCheck(L_54);
		bool L_55 = L_54->get__isMoving_37();
		V_3 = (bool)((((int32_t)L_55) == ((int32_t)0))? 1 : 0);
		bool L_56 = V_3;
		if (!L_56)
		{
			goto IL_0238;
		}
	}
	{
		// _smoke1.Stop();
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_57 = __this->get_U3CU3E4__this_3();
		NullCheck(L_57);
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_58 = L_57->get__smoke1_34();
		NullCheck(L_58);
		ParticleSystem_Stop_m8CBF9268DE7B5A40952B4977462B857B5F5AFB9D(L_58, /*hidden argument*/NULL);
		goto IL_0259;
	}

IL_0238:
	{
		// _engineAudioSource.loop = true;
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_59 = __this->get_U3CU3E4__this_3();
		NullCheck(L_59);
		AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * L_60 = L_59->get__engineAudioSource_28();
		NullCheck(L_60);
		AudioSource_set_loop_mDD9FB746D8A7392472E5484EEF8D0A667993E3E0(L_60, (bool)1, /*hidden argument*/NULL);
		// PlayEngineSound(EngineSoundState.AccelerateOrSetProperSpeed);
		TrainLocomotive_t2C268A830FCCA832C591095DC092DA488299C331 * L_61 = __this->get_U3CU3E4__this_3();
		NullCheck(L_61);
		float L_62;
		L_62 = TrainLocomotive_PlayEngineSound_mBBE7F6F9988F707FFC60EE195E18C7C7C37C05CE(L_61, 1, /*hidden argument*/NULL);
	}

IL_0259:
	{
		// }
		return (bool)0;
	}
}
// System.Object OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartStopTrainU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26D7E65A695D870236E43D9C2E2D46D88EA33065 (U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_Reset_mE5B9500E4605106C99B8718D3105A15B0CB73A87 (U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_Reset_mE5B9500E4605106C99B8718D3105A15B0CB73A87_RuntimeMethod_var)));
	}
}
// System.Object OculusSampleFramework.TrainLocomotive/<StartStopTrain>d__34::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartStopTrainU3Ed__34_System_Collections_IEnumerator_get_Current_mEF6808D31C6E8D74E845C551B8618F3902FC469E (U3CStartStopTrainU3Ed__34_t907E2CF7AFD4EF68917D60A52422A00185CBB723 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GPTT.Data.Util/Machine::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Machine__ctor_m296C002DE86C45F85F38604A3C4B2A6028146A22 (Machine_tD44F48548F85571C82CE7DF59CEDDFBB4DADF082 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DKP.VR.VRIO/<Haptics>d__0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHapticsU3Ed__0__ctor_mCD6188E048F28ED1D22E38A31DB31CD4B81E8C16 (U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DKP.VR.VRIO/<Haptics>d__0::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHapticsU3Ed__0_MoveNext_m9373353F1A7C1785E118156DCE0D27A8C52E0E52 (U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mF00E8C45202F9019CB44F46C3EEC111CCE67B9E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisYieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mEC62BDE3C6F344BF4321FC8C373E9E160AFE0277_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OVRInput_t3C43263053F2510BDF75588657A71B87702767FB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  V_2;
	memset((&V_2), 0, sizeof(V_2));
	U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF * V_3 = NULL;
	bool V_4 = false;
	TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  V_5;
	memset((&V_5), 0, sizeof(V_5));
	YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  V_6;
	memset((&V_6), 0, sizeof(V_6));
	YieldAwaitable_t95CCA9EB9730CADF5A3BEF9845E12FF467F594FA  V_7;
	memset((&V_7), 0, sizeof(V_7));
	Exception_t * V_8 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 5> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = V_0;
			switch (L_1)
			{
				case 0:
				{
					goto IL_001b;
				}
				case 1:
				{
					goto IL_001d;
				}
				case 2:
				{
					goto IL_0022;
				}
			}
		}

IL_0019:
		{
			goto IL_0027;
		}

IL_001b:
		{
			goto IL_0087;
		}

IL_001d:
		{
			goto IL_0125;
		}

IL_0022:
		{
			goto IL_019a;
		}

IL_0027:
		{
			// if (hand == Hand.left)
			int32_t L_2 = __this->get_hand_2();
			V_1 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
			bool L_3 = V_1;
			if (!L_3)
			{
				goto IL_00c2;
			}
		}

IL_0038:
		{
			// OVRInput.SetControllerVibration(0.125f, 0.125f, OVRInput.Controller.LTouch);
			IL2CPP_RUNTIME_CLASS_INIT(OVRInput_t3C43263053F2510BDF75588657A71B87702767FB_il2cpp_TypeInfo_var);
			OVRInput_SetControllerVibration_mB0CD60D364D545533E370E8BD2ED6AF43A69D728((0.125f), (0.125f), 1, /*hidden argument*/NULL);
			// await Task.Delay(100);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_il2cpp_TypeInfo_var);
			Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * L_4;
			L_4 = Task_Delay_mD54722DBAF22507493263E9B1167A7F77EDDF80E(((int32_t)100), /*hidden argument*/NULL);
			NullCheck(L_4);
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  L_5;
			L_5 = Task_GetAwaiter_m1FF7528A8FE13F79207DFE970F642078EF6B1260(L_4, /*hidden argument*/NULL);
			V_2 = L_5;
			bool L_6;
			L_6 = TaskAwaiter_get_IsCompleted_m6F97613C55E505B5664C3C0CFC4677D296EAA8BC((TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *)(&V_2), /*hidden argument*/NULL);
			if (L_6)
			{
				goto IL_00a3;
			}
		}

IL_0060:
		{
			int32_t L_7 = 0;
			V_0 = L_7;
			__this->set_U3CU3E1__state_0(L_7);
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  L_8 = V_2;
			__this->set_U3CU3Eu__1_3(L_8);
			V_3 = __this;
			AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * L_9 = __this->get_address_of_U3CU3Et__builder_1();
			AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mF00E8C45202F9019CB44F46C3EEC111CCE67B9E9((AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *)L_9, (TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *)(&V_2), (U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF **)(&V_3), /*hidden argument*/AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mF00E8C45202F9019CB44F46C3EEC111CCE67B9E9_RuntimeMethod_var);
			goto IL_01ef;
		}

IL_0087:
		{
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  L_10 = __this->get_U3CU3Eu__1_3();
			V_2 = L_10;
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * L_11 = __this->get_address_of_U3CU3Eu__1_3();
			il2cpp_codegen_initobj(L_11, sizeof(TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C ));
			int32_t L_12 = (-1);
			V_0 = L_12;
			__this->set_U3CU3E1__state_0(L_12);
		}

IL_00a3:
		{
			TaskAwaiter_GetResult_m578EEFEC4DD1AE5E77C899B8BAA3825EB79D1330((TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *)(&V_2), /*hidden argument*/NULL);
			// OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.LTouch);
			IL2CPP_RUNTIME_CLASS_INIT(OVRInput_t3C43263053F2510BDF75588657A71B87702767FB_il2cpp_TypeInfo_var);
			OVRInput_SetControllerVibration_mB0CD60D364D545533E370E8BD2ED6AF43A69D728((0.0f), (0.0f), 1, /*hidden argument*/NULL);
			goto IL_015c;
		}

IL_00c2:
		{
			// else if (hand == Hand.right)
			int32_t L_13 = __this->get_hand_2();
			V_4 = (bool)((((int32_t)L_13) == ((int32_t)1))? 1 : 0);
			bool L_14 = V_4;
			if (!L_14)
			{
				goto IL_015c;
			}
		}

IL_00d4:
		{
			// OVRInput.SetControllerVibration(0.125f, 0.125f, OVRInput.Controller.RTouch);
			IL2CPP_RUNTIME_CLASS_INIT(OVRInput_t3C43263053F2510BDF75588657A71B87702767FB_il2cpp_TypeInfo_var);
			OVRInput_SetControllerVibration_mB0CD60D364D545533E370E8BD2ED6AF43A69D728((0.125f), (0.125f), 2, /*hidden argument*/NULL);
			// await Task.Delay(100);
			IL2CPP_RUNTIME_CLASS_INIT(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_il2cpp_TypeInfo_var);
			Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60 * L_15;
			L_15 = Task_Delay_mD54722DBAF22507493263E9B1167A7F77EDDF80E(((int32_t)100), /*hidden argument*/NULL);
			NullCheck(L_15);
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  L_16;
			L_16 = Task_GetAwaiter_m1FF7528A8FE13F79207DFE970F642078EF6B1260(L_15, /*hidden argument*/NULL);
			V_5 = L_16;
			bool L_17;
			L_17 = TaskAwaiter_get_IsCompleted_m6F97613C55E505B5664C3C0CFC4677D296EAA8BC((TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *)(&V_5), /*hidden argument*/NULL);
			if (L_17)
			{
				goto IL_0142;
			}
		}

IL_00fd:
		{
			int32_t L_18 = 1;
			V_0 = L_18;
			__this->set_U3CU3E1__state_0(L_18);
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  L_19 = V_5;
			__this->set_U3CU3Eu__1_3(L_19);
			V_3 = __this;
			AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * L_20 = __this->get_address_of_U3CU3Et__builder_1();
			AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mF00E8C45202F9019CB44F46C3EEC111CCE67B9E9((AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *)L_20, (TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *)(&V_5), (U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF **)(&V_3), /*hidden argument*/AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisTaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mF00E8C45202F9019CB44F46C3EEC111CCE67B9E9_RuntimeMethod_var);
			goto IL_01ef;
		}

IL_0125:
		{
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C  L_21 = __this->get_U3CU3Eu__1_3();
			V_5 = L_21;
			TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C * L_22 = __this->get_address_of_U3CU3Eu__1_3();
			il2cpp_codegen_initobj(L_22, sizeof(TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C ));
			int32_t L_23 = (-1);
			V_0 = L_23;
			__this->set_U3CU3E1__state_0(L_23);
		}

IL_0142:
		{
			TaskAwaiter_GetResult_m578EEFEC4DD1AE5E77C899B8BAA3825EB79D1330((TaskAwaiter_t3780D365E9D10C2D6C4E76C78AA0CDF92B8F181C *)(&V_5), /*hidden argument*/NULL);
			// OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.RTouch);
			IL2CPP_RUNTIME_CLASS_INIT(OVRInput_t3C43263053F2510BDF75588657A71B87702767FB_il2cpp_TypeInfo_var);
			OVRInput_SetControllerVibration_mB0CD60D364D545533E370E8BD2ED6AF43A69D728((0.0f), (0.0f), 2, /*hidden argument*/NULL);
		}

IL_015c:
		{
			// await Task.Yield();
			IL2CPP_RUNTIME_CLASS_INIT(Task_t804B25CFE3FC13AAEE16C8FA3BF52513F2A8DB60_il2cpp_TypeInfo_var);
			YieldAwaitable_t95CCA9EB9730CADF5A3BEF9845E12FF467F594FA  L_24;
			L_24 = Task_Yield_m1E79F65972D82906B8BBE9980C57E29538D3E94B(/*hidden argument*/NULL);
			V_7 = L_24;
			YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  L_25;
			L_25 = YieldAwaitable_GetAwaiter_m8AA7D8DCF790EB9BDBDD5F0D8BBA0404C6F7DCD8((YieldAwaitable_t95CCA9EB9730CADF5A3BEF9845E12FF467F594FA *)(&V_7), /*hidden argument*/NULL);
			V_6 = L_25;
			bool L_26;
			L_26 = YieldAwaiter_get_IsCompleted_mAB52777C6F31F3FBAD7E6A7CD90EDF40F220CBBC((YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE *)(&V_6), /*hidden argument*/NULL);
			if (L_26)
			{
				goto IL_01b7;
			}
		}

IL_0175:
		{
			int32_t L_27 = 2;
			V_0 = L_27;
			__this->set_U3CU3E1__state_0(L_27);
			YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  L_28 = V_6;
			__this->set_U3CU3Eu__2_4(L_28);
			V_3 = __this;
			AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * L_29 = __this->get_address_of_U3CU3Et__builder_1();
			AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisYieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mEC62BDE3C6F344BF4321FC8C373E9E160AFE0277((AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *)L_29, (YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE *)(&V_6), (U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF **)(&V_3), /*hidden argument*/AsyncVoidMethodBuilder_AwaitUnsafeOnCompleted_TisYieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_TisU3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF_mEC62BDE3C6F344BF4321FC8C373E9E160AFE0277_RuntimeMethod_var);
			goto IL_01ef;
		}

IL_019a:
		{
			YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  L_30 = __this->get_U3CU3Eu__2_4();
			V_6 = L_30;
			YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * L_31 = __this->get_address_of_U3CU3Eu__2_4();
			il2cpp_codegen_initobj(L_31, sizeof(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE ));
			int32_t L_32 = (-1);
			V_0 = L_32;
			__this->set_U3CU3E1__state_0(L_32);
		}

IL_01b7:
		{
			YieldAwaiter_GetResult_mE9F670767330EAF32ED76882EB8B152FF62CCDBD((YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE *)(&V_6), /*hidden argument*/NULL);
			goto IL_01db;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_01c1;
		}
		throw e;
	}

CATCH_01c1:
	{ // begin catch(System.Exception)
		V_8 = ((Exception_t *)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t *));
		__this->set_U3CU3E1__state_0(((int32_t)-2));
		AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * L_33 = __this->get_address_of_U3CU3Et__builder_1();
		Exception_t * L_34 = V_8;
		AsyncVoidMethodBuilder_SetException_m16372173CEA3031B4CB9B8D15DA97C457F835155((AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *)L_33, L_34, /*hidden argument*/NULL);
		IL2CPP_POP_ACTIVE_EXCEPTION();
		goto IL_01ef;
	} // end catch (depth: 1)

IL_01db:
	{
		// }
		__this->set_U3CU3E1__state_0(((int32_t)-2));
		AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 * L_35 = __this->get_address_of_U3CU3Et__builder_1();
		AsyncVoidMethodBuilder_SetResult_m901385B56EBE93E472A77EA48F61E4F498F3E00E((AsyncVoidMethodBuilder_tA31C888168B27AABF7B0D9E7DF720547D4892DE6 *)L_35, /*hidden argument*/NULL);
	}

IL_01ef:
	{
		return;
	}
}
// System.Void DKP.VR.VRIO/<Haptics>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CHapticsU3Ed__0_SetStateMachine_m80A4CFDD96530258F3A3865E6A5E3B39614DCB25 (U3CHapticsU3Ed__0_t6A54D5C11497AE512B23E3904563D5DF5EFE27BF * __this, RuntimeObject* ___stateMachine0, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebcamManager/<RestartWebcam>d__8::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRestartWebcamU3Ed__8__ctor_m28872094CCAA094B43F22696A627F91DB681702E (U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void WebcamManager/<RestartWebcam>d__8::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRestartWebcamU3Ed__8_System_IDisposable_Dispose_m64D07C3BFF909EE7CCCBBF13483490CDFD984122 (U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean WebcamManager/<RestartWebcam>d__8::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CRestartWebcamU3Ed__8_MoveNext_m0073A78F7D1343C48FDB671B463ED829CDA3C822 (U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_001d;
			}
			case 2:
			{
				goto IL_001f;
			}
		}
	}
	{
		goto IL_0021;
	}

IL_001b:
	{
		goto IL_0023;
	}

IL_001d:
	{
		goto IL_005d;
	}

IL_001f:
	{
		goto IL_0080;
	}

IL_0021:
	{
		return (bool)0;
	}

IL_0023:
	{
		__this->set_U3CU3E1__state_0((-1));
		// canRestart = false;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		L_2->set_canRestart_10((bool)0);
		// Action_StopWebcam();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		WebcamManager_Action_StopWebcam_m3B387E8BCA377260DF46C7189AD053032E173C1A(L_3, /*hidden argument*/NULL);
		// yield return initAndWaitForWebCamTexture();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_4 = __this->get_U3CU3E4__this_2();
		NullCheck(L_4);
		RuntimeObject* L_5;
		L_5 = WebcamManager_initAndWaitForWebCamTexture_m6475CE2D9148E36CF76C11720BB8EBC75C3E8494(L_4, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_5);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_005d:
	{
		__this->set_U3CU3E1__state_0((-1));
		// canRestart = true;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_6 = __this->get_U3CU3E4__this_2();
		NullCheck(L_6);
		L_6->set_canRestart_10((bool)1);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0080:
	{
		__this->set_U3CU3E1__state_0((-1));
		// }
		return (bool)0;
	}
}
// System.Object WebcamManager/<RestartWebcam>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CRestartWebcamU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA6D2C7491AB5B6855FC00F3F90253FC3DB64DCC3 (U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void WebcamManager/<RestartWebcam>d__8::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_Reset_mA3A746F18872D6F97B3CC5A4977A1F692BA7E8EA (U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_Reset_mA3A746F18872D6F97B3CC5A4977A1F692BA7E8EA_RuntimeMethod_var)));
	}
}
// System.Object WebcamManager/<RestartWebcam>d__8::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_get_Current_m0D001B2B663BA976A2D539566D366D6BCB620327 (U3CRestartWebcamU3Ed__8_t7C04E68A8552A34A2D277E5149DE43524C70CE06 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void WebcamManager/<initAndWaitForWebCamTexture>d__34::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CinitAndWaitForWebCamTextureU3Ed__34__ctor_m52FA2EEEB153A88A5AA5BAA71AB02EA1EF8C4D7E (U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void WebcamManager/<initAndWaitForWebCamTexture>d__34::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CinitAndWaitForWebCamTextureU3Ed__34_System_IDisposable_Dispose_m2727360BB45FFE1C4D81B8AF9B1B04400CD74F47 (U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean WebcamManager/<initAndWaitForWebCamTexture>d__34::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CinitAndWaitForWebCamTextureU3Ed__34_MoveNext_m42785171471996265574A92AFC79AFFB0ADEFE23 (U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3F55171AED7650EF04E95A77E6E60F9E11DEBDCE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	int32_t V_5 = 0;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	bool V_11 = false;
	bool V_12 = false;
	bool V_13 = false;
	bool V_14 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001f;
			}
			case 1:
			{
				goto IL_0021;
			}
			case 2:
			{
				goto IL_0023;
			}
			case 3:
			{
				goto IL_0028;
			}
		}
	}
	{
		goto IL_002d;
	}

IL_001f:
	{
		goto IL_002f;
	}

IL_0021:
	{
		goto IL_0064;
	}

IL_0023:
	{
		goto IL_052b;
	}

IL_0028:
	{
		goto IL_05ba;
	}

IL_002d:
	{
		return (bool)0;
	}

IL_002f:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (isInitWaiting) yield break;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		bool L_3 = L_2->get_isInitWaiting_18();
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0048;
		}
	}
	{
		// if (isInitWaiting) yield break;
		return (bool)0;
	}

IL_0048:
	{
		// isInitWaiting = true;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		L_5->set_isInitWaiting_18((bool)1);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0064:
	{
		__this->set_U3CU3E1__state_0((-1));
		// devices = WebCamTexture.devices;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_6 = __this->get_U3CU3E4__this_2();
		WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* L_7;
		L_7 = WebCamTexture_get_devices_m5E211AF8615AED8AAF97A669F41845FC85A0CD7C(/*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_devices_4(L_7);
		// webCams = new WebCamTexture[devices.Length];
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_8 = __this->get_U3CU3E4__this_2();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_9 = __this->get_U3CU3E4__this_2();
		NullCheck(L_9);
		WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* L_10 = L_9->get_devices_4();
		NullCheck(L_10);
		WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983* L_11 = (WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983*)(WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983*)SZArrayNew(WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length))));
		NullCheck(L_8);
		L_8->set_webCams_5(L_11);
		// textures = new Texture[devices.Length];
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_12 = __this->get_U3CU3E4__this_2();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_13 = __this->get_U3CU3E4__this_2();
		NullCheck(L_13);
		WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* L_14 = L_13->get_devices_4();
		NullCheck(L_14);
		TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* L_15 = (TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150*)(TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150*)SZArrayNew(TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))));
		NullCheck(L_12);
		L_12->set_textures_6(L_15);
		// DeviceInfo = "";
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_16 = __this->get_U3CU3E4__this_2();
		NullCheck(L_16);
		L_16->set_DeviceInfo_9(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// for (int i = 0; i < devices.Length; i++)
		__this->set_U3CiU3E5__1_3(0);
		goto IL_0279;
	}

IL_00d1:
	{
		// DeviceInfo += "[" + i + "] name: " +devices[i].name + "\n";
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_17 = __this->get_U3CU3E4__this_2();
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)6);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_18;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_20 = __this->get_U3CU3E4__this_2();
		NullCheck(L_20);
		String_t* L_21 = L_20->get_DeviceInfo_9();
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_21);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_22 = L_19;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_23 = L_22;
		int32_t* L_24 = __this->get_address_of_U3CiU3E5__1_3();
		String_t* L_25;
		L_25 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_25);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_26 = L_23;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, _stringLiteral3F55171AED7650EF04E95A77E6E60F9E11DEBDCE);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3F55171AED7650EF04E95A77E6E60F9E11DEBDCE);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_27 = L_26;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_28 = __this->get_U3CU3E4__this_2();
		NullCheck(L_28);
		WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* L_29 = L_28->get_devices_4();
		int32_t L_30 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_29);
		String_t* L_31;
		L_31 = WebCamDevice_get_name_mD475CBF038076E5657D55D4DA43A7DC69CE6B6D4((WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_30))), /*hidden argument*/NULL);
		NullCheck(L_27);
		ArrayElementTypeCheck (L_27, L_31);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_31);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_32 = L_27;
		NullCheck(L_32);
		ArrayElementTypeCheck (L_32, _stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral00B28FF06B788B9B67C6B259800F404F9F3761FD);
		String_t* L_33;
		L_33 = String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9(L_32, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_DeviceInfo_9(L_33);
		// webCams[i] = new WebCamTexture();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_34 = __this->get_U3CU3E4__this_2();
		NullCheck(L_34);
		WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983* L_35 = L_34->get_webCams_5();
		int32_t L_36 = __this->get_U3CiU3E5__1_3();
		WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * L_37 = (WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 *)il2cpp_codegen_object_new(WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_m6819D615D58318888B7B90D47A7252A81894344F(L_37, /*hidden argument*/NULL);
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_37);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(L_36), (WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 *)L_37);
		// webCams[i] = new WebCamTexture(devices[i].name, Mathf.RoundToInt(requestResolution.x), Mathf.RoundToInt(requestResolution.y), 30);
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_38 = __this->get_U3CU3E4__this_2();
		NullCheck(L_38);
		WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983* L_39 = L_38->get_webCams_5();
		int32_t L_40 = __this->get_U3CiU3E5__1_3();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_41 = __this->get_U3CU3E4__this_2();
		NullCheck(L_41);
		WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* L_42 = L_41->get_devices_4();
		int32_t L_43 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_42);
		String_t* L_44;
		L_44 = WebCamDevice_get_name_mD475CBF038076E5657D55D4DA43A7DC69CE6B6D4((WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C *)((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_43))), /*hidden argument*/NULL);
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_45 = __this->get_U3CU3E4__this_2();
		NullCheck(L_45);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_46 = L_45->get_address_of_requestResolution_16();
		float L_47 = L_46->get_x_0();
		int32_t L_48;
		L_48 = Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD(L_47, /*hidden argument*/NULL);
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_49 = __this->get_U3CU3E4__this_2();
		NullCheck(L_49);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_50 = L_49->get_address_of_requestResolution_16();
		float L_51 = L_50->get_y_1();
		int32_t L_52;
		L_52 = Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD(L_51, /*hidden argument*/NULL);
		WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * L_53 = (WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 *)il2cpp_codegen_object_new(WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_m8369712442E77D9130CD2F76A3FA4846F74F16CB(L_53, L_44, L_48, L_52, ((int32_t)30), /*hidden argument*/NULL);
		NullCheck(L_39);
		ArrayElementTypeCheck (L_39, L_53);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(L_40), (WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 *)L_53);
		// textures[i] = webCams[i];
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_54 = __this->get_U3CU3E4__this_2();
		NullCheck(L_54);
		TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* L_55 = L_54->get_textures_6();
		int32_t L_56 = __this->get_U3CiU3E5__1_3();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_57 = __this->get_U3CU3E4__this_2();
		NullCheck(L_57);
		WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983* L_58 = L_57->get_webCams_5();
		int32_t L_59 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_58);
		int32_t L_60 = L_59;
		WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * L_61 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, L_61);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(L_56), (Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE *)L_61);
		// textures[i].wrapMode = TextureWrapMode.Repeat;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_62 = __this->get_U3CU3E4__this_2();
		NullCheck(L_62);
		TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* L_63 = L_62->get_textures_6();
		int32_t L_64 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_63);
		int32_t L_65 = L_64;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_66 = (L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		NullCheck(L_66);
		Texture_set_wrapMode_m1233D2DF48DC20996F8EE26E866D4BDD2AC8050D(L_66, 0, /*hidden argument*/NULL);
		// if (useFrontCam)
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_67 = __this->get_U3CU3E4__this_2();
		NullCheck(L_67);
		bool L_68 = L_67->get_useFrontCam_8();
		V_2 = L_68;
		bool L_69 = V_2;
		if (!L_69)
		{
			goto IL_022f;
		}
	}
	{
		// if (devices[i].isFrontFacing) TargetCamID = i;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_70 = __this->get_U3CU3E4__this_2();
		NullCheck(L_70);
		WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* L_71 = L_70->get_devices_4();
		int32_t L_72 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_71);
		bool L_73;
		L_73 = WebCamDevice_get_isFrontFacing_m43547AAF7B0729DB1962456A3EFF161B5E273E15((WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C *)((L_71)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_72))), /*hidden argument*/NULL);
		V_3 = L_73;
		bool L_74 = V_3;
		if (!L_74)
		{
			goto IL_022c;
		}
	}
	{
		// if (devices[i].isFrontFacing) TargetCamID = i;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_75 = __this->get_U3CU3E4__this_2();
		int32_t L_76 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_75);
		L_75->set_TargetCamID_7(L_76);
	}

IL_022c:
	{
		goto IL_0266;
	}

IL_022f:
	{
		// if (!devices[i].isFrontFacing) TargetCamID = i;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_77 = __this->get_U3CU3E4__this_2();
		NullCheck(L_77);
		WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* L_78 = L_77->get_devices_4();
		int32_t L_79 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_78);
		bool L_80;
		L_80 = WebCamDevice_get_isFrontFacing_m43547AAF7B0729DB1962456A3EFF161B5E273E15((WebCamDevice_t84AC34018729892FB910F4F146AB9820DD006A2C *)((L_78)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_79))), /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_80) == ((int32_t)0))? 1 : 0);
		bool L_81 = V_4;
		if (!L_81)
		{
			goto IL_0265;
		}
	}
	{
		// if (!devices[i].isFrontFacing) TargetCamID = i;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_82 = __this->get_U3CU3E4__this_2();
		int32_t L_83 = __this->get_U3CiU3E5__1_3();
		NullCheck(L_82);
		L_82->set_TargetCamID_7(L_83);
	}

IL_0265:
	{
	}

IL_0266:
	{
		// for (int i = 0; i < devices.Length; i++)
		int32_t L_84 = __this->get_U3CiU3E5__1_3();
		V_5 = L_84;
		int32_t L_85 = V_5;
		__this->set_U3CiU3E5__1_3(((int32_t)il2cpp_codegen_add((int32_t)L_85, (int32_t)1)));
	}

IL_0279:
	{
		// for (int i = 0; i < devices.Length; i++)
		int32_t L_86 = __this->get_U3CiU3E5__1_3();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_87 = __this->get_U3CU3E4__this_2();
		NullCheck(L_87);
		WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* L_88 = L_87->get_devices_4();
		NullCheck(L_88);
		V_6 = (bool)((((int32_t)L_86) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_88)->max_length)))))? 1 : 0);
		bool L_89 = V_6;
		if (L_89)
		{
			goto IL_00d1;
		}
	}
	{
		// if (TargetCamID > devices.Length - 1) TargetCamID = devices.Length - 1;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_90 = __this->get_U3CU3E4__this_2();
		NullCheck(L_90);
		int32_t L_91 = L_90->get_TargetCamID_7();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_92 = __this->get_U3CU3E4__this_2();
		NullCheck(L_92);
		WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* L_93 = L_92->get_devices_4();
		NullCheck(L_93);
		V_7 = (bool)((((int32_t)L_91) > ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_93)->max_length))), (int32_t)1))))? 1 : 0);
		bool L_94 = V_7;
		if (!L_94)
		{
			goto IL_02d3;
		}
	}
	{
		// if (TargetCamID > devices.Length - 1) TargetCamID = devices.Length - 1;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_95 = __this->get_U3CU3E4__this_2();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_96 = __this->get_U3CU3E4__this_2();
		NullCheck(L_96);
		WebCamDeviceU5BU5D_t5509CE66483F44F4D0DB82BF41F86C649CB7B70E* L_97 = L_96->get_devices_4();
		NullCheck(L_97);
		NullCheck(L_95);
		L_95->set_TargetCamID_7(((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_97)->max_length))), (int32_t)1)));
	}

IL_02d3:
	{
		// webCams[TargetCamID].requestedFPS = 30;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_98 = __this->get_U3CU3E4__this_2();
		NullCheck(L_98);
		WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983* L_99 = L_98->get_webCams_5();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_100 = __this->get_U3CU3E4__this_2();
		NullCheck(L_100);
		int32_t L_101 = L_100->get_TargetCamID_7();
		NullCheck(L_99);
		int32_t L_102 = L_101;
		WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * L_103 = (L_99)->GetAt(static_cast<il2cpp_array_size_t>(L_102));
		NullCheck(L_103);
		WebCamTexture_set_requestedFPS_m85821E34D74DDBF5B2FA8AF5D972BE0EE1667DCC(L_103, (30.0f), /*hidden argument*/NULL);
		// webCams[TargetCamID].Play();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_104 = __this->get_U3CU3E4__this_2();
		NullCheck(L_104);
		WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983* L_105 = L_104->get_webCams_5();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_106 = __this->get_U3CU3E4__this_2();
		NullCheck(L_106);
		int32_t L_107 = L_106->get_TargetCamID_7();
		NullCheck(L_105);
		int32_t L_108 = L_107;
		WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * L_109 = (L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		NullCheck(L_109);
		WebCamTexture_Play_m8527994B54606AE71602DB93195D2BA44CEDC2B1(L_109, /*hidden argument*/NULL);
		// for (int i = 0; i < materials.Length; i++) materials[i].mainTexture = textures[TargetCamID];
		__this->set_U3CiU3E5__2_4(0);
		goto IL_035c;
	}

IL_031b:
	{
		// for (int i = 0; i < materials.Length; i++) materials[i].mainTexture = textures[TargetCamID];
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_110 = __this->get_U3CU3E4__this_2();
		NullCheck(L_110);
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_111 = L_110->get_materials_12();
		int32_t L_112 = __this->get_U3CiU3E5__2_4();
		NullCheck(L_111);
		int32_t L_113 = L_112;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_114 = (L_111)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_115 = __this->get_U3CU3E4__this_2();
		NullCheck(L_115);
		TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* L_116 = L_115->get_textures_6();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_117 = __this->get_U3CU3E4__this_2();
		NullCheck(L_117);
		int32_t L_118 = L_117->get_TargetCamID_7();
		NullCheck(L_116);
		int32_t L_119 = L_118;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_120 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_119));
		NullCheck(L_114);
		Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5(L_114, L_120, /*hidden argument*/NULL);
		// for (int i = 0; i < materials.Length; i++) materials[i].mainTexture = textures[TargetCamID];
		int32_t L_121 = __this->get_U3CiU3E5__2_4();
		V_5 = L_121;
		int32_t L_122 = V_5;
		__this->set_U3CiU3E5__2_4(((int32_t)il2cpp_codegen_add((int32_t)L_122, (int32_t)1)));
	}

IL_035c:
	{
		// for (int i = 0; i < materials.Length; i++) materials[i].mainTexture = textures[TargetCamID];
		int32_t L_123 = __this->get_U3CiU3E5__2_4();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_124 = __this->get_U3CU3E4__this_2();
		NullCheck(L_124);
		MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* L_125 = L_124->get_materials_12();
		NullCheck(L_125);
		V_8 = (bool)((((int32_t)L_123) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_125)->max_length)))))? 1 : 0);
		bool L_126 = V_8;
		if (L_126)
		{
			goto IL_031b;
		}
	}
	{
		// if (BackgroundQuad != null) BackgroundQuad.GetComponent<Renderer>().material.mainTexture = textures[TargetCamID];
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_127 = __this->get_U3CU3E4__this_2();
		NullCheck(L_127);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_128 = L_127->get_BackgroundQuad_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_129;
		L_129 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_128, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		V_9 = L_129;
		bool L_130 = V_9;
		if (!L_130)
		{
			goto IL_03c0;
		}
	}
	{
		// if (BackgroundQuad != null) BackgroundQuad.GetComponent<Renderer>().material.mainTexture = textures[TargetCamID];
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_131 = __this->get_U3CU3E4__this_2();
		NullCheck(L_131);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_132 = L_131->get_BackgroundQuad_14();
		NullCheck(L_132);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_133;
		L_133 = GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466(L_132, /*hidden argument*/GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466_RuntimeMethod_var);
		NullCheck(L_133);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_134;
		L_134 = Renderer_get_material_mE6B01125502D08EE0D6DFE2EAEC064AD9BB31804(L_133, /*hidden argument*/NULL);
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_135 = __this->get_U3CU3E4__this_2();
		NullCheck(L_135);
		TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* L_136 = L_135->get_textures_6();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_137 = __this->get_U3CU3E4__this_2();
		NullCheck(L_137);
		int32_t L_138 = L_137->get_TargetCamID_7();
		NullCheck(L_136);
		int32_t L_139 = L_138;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_140 = (L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_139));
		NullCheck(L_134);
		Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5(L_134, L_140, /*hidden argument*/NULL);
	}

IL_03c0:
	{
		// foreach (GameObject obj in targetMeshObjects)
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_141 = __this->get_U3CU3E4__this_2();
		NullCheck(L_141);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_142 = L_141->get_targetMeshObjects_13();
		__this->set_U3CU3Es__3_5(L_142);
		__this->set_U3CU3Es__4_6(0);
		goto IL_048b;
	}

IL_03de:
	{
		// foreach (GameObject obj in targetMeshObjects)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_143 = __this->get_U3CU3Es__3_5();
		int32_t L_144 = __this->get_U3CU3Es__4_6();
		NullCheck(L_143);
		int32_t L_145 = L_144;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_146 = (L_143)->GetAt(static_cast<il2cpp_array_size_t>(L_145));
		__this->set_U3CobjU3E5__5_7(L_146);
		// if(obj.GetComponent<Renderer>() != null) obj.GetComponent<Renderer>().material.mainTexture = textures[TargetCamID];
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_147 = __this->get_U3CobjU3E5__5_7();
		NullCheck(L_147);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_148;
		L_148 = GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466(L_147, /*hidden argument*/GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_149;
		L_149 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_148, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		V_10 = L_149;
		bool L_150 = V_10;
		if (!L_150)
		{
			goto IL_0436;
		}
	}
	{
		// if(obj.GetComponent<Renderer>() != null) obj.GetComponent<Renderer>().material.mainTexture = textures[TargetCamID];
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_151 = __this->get_U3CobjU3E5__5_7();
		NullCheck(L_151);
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_152;
		L_152 = GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466(L_151, /*hidden argument*/GameObject_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_mD787758BED3337F182C18CC67C516C2A11B55466_RuntimeMethod_var);
		NullCheck(L_152);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_153;
		L_153 = Renderer_get_material_mE6B01125502D08EE0D6DFE2EAEC064AD9BB31804(L_152, /*hidden argument*/NULL);
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_154 = __this->get_U3CU3E4__this_2();
		NullCheck(L_154);
		TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* L_155 = L_154->get_textures_6();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_156 = __this->get_U3CU3E4__this_2();
		NullCheck(L_156);
		int32_t L_157 = L_156->get_TargetCamID_7();
		NullCheck(L_155);
		int32_t L_158 = L_157;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_159 = (L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_158));
		NullCheck(L_153);
		Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5(L_153, L_159, /*hidden argument*/NULL);
	}

IL_0436:
	{
		// if(obj.GetComponent<RawImage>() != null) obj.GetComponent<RawImage>().texture = textures[TargetCamID];
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_160 = __this->get_U3CobjU3E5__5_7();
		NullCheck(L_160);
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_161;
		L_161 = GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7(L_160, /*hidden argument*/GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_162;
		L_162 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_161, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		V_11 = L_162;
		bool L_163 = V_11;
		if (!L_163)
		{
			goto IL_0475;
		}
	}
	{
		// if(obj.GetComponent<RawImage>() != null) obj.GetComponent<RawImage>().texture = textures[TargetCamID];
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_164 = __this->get_U3CobjU3E5__5_7();
		NullCheck(L_164);
		RawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A * L_165;
		L_165 = GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7(L_164, /*hidden argument*/GameObject_GetComponent_TisRawImage_tFE280EF0C73AF19FE9AC24DB06501937DC2D6F1A_mDA64083A6BE0DE214BBF4660B7E7B7B571EBE4F7_RuntimeMethod_var);
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_166 = __this->get_U3CU3E4__this_2();
		NullCheck(L_166);
		TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* L_167 = L_166->get_textures_6();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_168 = __this->get_U3CU3E4__this_2();
		NullCheck(L_168);
		int32_t L_169 = L_168->get_TargetCamID_7();
		NullCheck(L_167);
		int32_t L_170 = L_169;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_171 = (L_167)->GetAt(static_cast<il2cpp_array_size_t>(L_170));
		NullCheck(L_165);
		RawImage_set_texture_m1D7BAE6CB629C36894B664D9F5D68CACA88B8D99(L_165, L_171, /*hidden argument*/NULL);
	}

IL_0475:
	{
		__this->set_U3CobjU3E5__5_7((GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL);
		int32_t L_172 = __this->get_U3CU3Es__4_6();
		__this->set_U3CU3Es__4_6(((int32_t)il2cpp_codegen_add((int32_t)L_172, (int32_t)1)));
	}

IL_048b:
	{
		// foreach (GameObject obj in targetMeshObjects)
		int32_t L_173 = __this->get_U3CU3Es__4_6();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_174 = __this->get_U3CU3Es__3_5();
		NullCheck(L_174);
		if ((((int32_t)L_173) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_174)->max_length))))))
		{
			goto IL_03de;
		}
	}
	{
		__this->set_U3CU3Es__3_5((GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642*)NULL);
		// if (textures.Length > 0)
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_175 = __this->get_U3CU3E4__this_2();
		NullCheck(L_175);
		TextureU5BU5D_t9DBF348F22539052ACB9387E8BB14A3AF2701150* L_176 = L_175->get_textures_6();
		NullCheck(L_176);
		V_12 = (bool)((!(((uint32_t)(((RuntimeArray*)L_176)->max_length)) <= ((uint32_t)0)))? 1 : 0);
		bool L_177 = V_12;
		if (!L_177)
		{
			goto IL_05aa;
		}
	}
	{
		// webCamTexture = webCams[TargetCamID];
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_178 = __this->get_U3CU3E4__this_2();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_179 = __this->get_U3CU3E4__this_2();
		NullCheck(L_179);
		WebCamTextureU5BU5D_tE6ADEA667639A815D2253C21E732809612174983* L_180 = L_179->get_webCams_5();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_181 = __this->get_U3CU3E4__this_2();
		NullCheck(L_181);
		int32_t L_182 = L_181->get_TargetCamID_7();
		NullCheck(L_180);
		int32_t L_183 = L_182;
		WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * L_184 = (L_180)->GetAt(static_cast<il2cpp_array_size_t>(L_183));
		NullCheck(L_178);
		L_178->set_webCamTexture_11(L_184);
		// int initFrameCount = 0;
		__this->set_U3CinitFrameCountU3E5__6_8(0);
		goto IL_0533;
	}

IL_04e9:
	{
		// if (initFrameCount > timeoutFrameCount) break;
		int32_t L_185 = __this->get_U3CinitFrameCountU3E5__6_8();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_186 = __this->get_U3CU3E4__this_2();
		NullCheck(L_186);
		int32_t L_187 = L_186->get_timeoutFrameCount_15();
		V_13 = (bool)((((int32_t)L_185) > ((int32_t)L_187))? 1 : 0);
		bool L_188 = V_13;
		if (!L_188)
		{
			goto IL_0505;
		}
	}
	{
		// if (initFrameCount > timeoutFrameCount) break;
		goto IL_0550;
	}

IL_0505:
	{
		// initFrameCount++;
		int32_t L_189 = __this->get_U3CinitFrameCountU3E5__6_8();
		V_5 = L_189;
		int32_t L_190 = V_5;
		__this->set_U3CinitFrameCountU3E5__6_8(((int32_t)il2cpp_codegen_add((int32_t)L_190, (int32_t)1)));
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_191 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_191, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_191);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_052b:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0533:
	{
		// while (webCamTexture.width <= 16)
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_192 = __this->get_U3CU3E4__this_2();
		NullCheck(L_192);
		WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * L_193 = L_192->get_webCamTexture_11();
		NullCheck(L_193);
		int32_t L_194;
		L_194 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_193);
		V_14 = (bool)((((int32_t)((((int32_t)L_194) > ((int32_t)((int32_t)16)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_195 = V_14;
		if (L_195)
		{
			goto IL_04e9;
		}
	}

IL_0550:
	{
		// textureResolution = new Vector2(webCamTexture.width, webCamTexture.height);
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_196 = __this->get_U3CU3E4__this_2();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_197 = __this->get_U3CU3E4__this_2();
		NullCheck(L_197);
		WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * L_198 = L_197->get_webCamTexture_11();
		NullCheck(L_198);
		int32_t L_199;
		L_199 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_198);
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_200 = __this->get_U3CU3E4__this_2();
		NullCheck(L_200);
		WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * L_201 = L_200->get_webCamTexture_11();
		NullCheck(L_201);
		int32_t L_202;
		L_202 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_201);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_203;
		memset((&L_203), 0, sizeof(L_203));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_203), ((float)((float)L_199)), ((float)((float)L_202)), /*hidden argument*/NULL);
		NullCheck(L_196);
		L_196->set_textureResolution_17(L_203);
		// isFlipped = webCamTexture.videoVerticallyMirrored;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_204 = __this->get_U3CU3E4__this_2();
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_205 = __this->get_U3CU3E4__this_2();
		NullCheck(L_205);
		WebCamTexture_t8E1DA1358E0E093A75FF35A336DD81B9EEC7AA62 * L_206 = L_205->get_webCamTexture_11();
		NullCheck(L_206);
		bool L_207;
		L_207 = WebCamTexture_get_videoVerticallyMirrored_m8ADBDC6A53EE4F84C6EEEE61B1B1A1E696C2B83D(L_206, /*hidden argument*/NULL);
		NullCheck(L_204);
		L_204->set_isFlipped_22(L_207);
		// isInitWaiting = false;
		WebcamManager_t70BB4B13511DE1D949A27FF1EF6237F113472973 * L_208 = __this->get_U3CU3E4__this_2();
		NullCheck(L_208);
		L_208->set_isInitWaiting_18((bool)0);
	}

IL_05aa:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_05ba:
	{
		__this->set_U3CU3E1__state_0((-1));
		// }
		return (bool)0;
	}
}
// System.Object WebcamManager/<initAndWaitForWebCamTexture>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03936395905AC4CD42FCADDBCFDC81AA99AB1180 (U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void WebcamManager/<initAndWaitForWebCamTexture>d__34::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_Reset_m40B8DC7783ACD7CDF9D80B46263092CF7CCD2C02 (U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_Reset_m40B8DC7783ACD7CDF9D80B46263092CF7CCD2C02_RuntimeMethod_var)));
	}
}
// System.Object WebcamManager/<initAndWaitForWebCamTexture>d__34::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_get_Current_m1B82795B8517450D9B2E0420375A1E3D234BE130 (U3CinitAndWaitForWebCamTextureU3Ed__34_t8135A25AB5EC245E5A92404E9002CA2A1AB5197D * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLerpToSpeedU3Ed__17__ctor_mC566935DE291006EA7DBEFE0365E202472FD9E7B (U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLerpToSpeedU3Ed__17_System_IDisposable_Dispose_m47D668CCF11A5E20E41EE17981808A194A891934 (U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CLerpToSpeedU3Ed__17_MoveNext_mEA106649270155724FF7B4F3557200EB0C46C649 (U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0019;
	}

IL_0012:
	{
		goto IL_001b;
	}

IL_0014:
	{
		goto IL_014e;
	}

IL_0019:
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// var totalTime = 0f;
		__this->set_U3CtotalTimeU3E5__1_4((0.0f));
		// var startSpeed = _currentSpeed;
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_3 = __this->get_U3CU3E4__this_3();
		NullCheck(L_3);
		float L_4 = L_3->get__currentSpeed_10();
		__this->set_U3CstartSpeedU3E5__2_5(L_4);
		// if (_audioChangeCr != null)
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_5 = __this->get_U3CU3E4__this_3();
		NullCheck(L_5);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_6 = L_5->get__audioChangeCr_12();
		V_1 = (bool)((!(((RuntimeObject*)(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 *)L_6) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_7 = V_1;
		if (!L_7)
		{
			goto IL_006a;
		}
	}
	{
		// StopCoroutine(_audioChangeCr);
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_8 = __this->get_U3CU3E4__this_3();
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_9 = __this->get_U3CU3E4__this_3();
		NullCheck(L_9);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_10 = L_9->get__audioChangeCr_12();
		NullCheck(L_8);
		MonoBehaviour_StopCoroutine_m5FF0476C9886FD8A3E6BA82BBE34B896CA279413(L_8, L_10, /*hidden argument*/NULL);
	}

IL_006a:
	{
		// if (IsMoving)
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_11 = __this->get_U3CU3E4__this_3();
		NullCheck(L_11);
		bool L_12;
		L_12 = WindmillBladesController_get_IsMoving_m5B2902A40908BBD84B5478EDA121ED00DBDECF56_inline(L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		bool L_13 = V_2;
		if (!L_13)
		{
			goto IL_00ca;
		}
	}
	{
		// _audioChangeCr = StartCoroutine(PlaySoundDelayed(_windMillStartSound,
		//   _windMillRotationSound, _windMillStartSound.length * 0.95f));
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_14 = __this->get_U3CU3E4__this_3();
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_15 = __this->get_U3CU3E4__this_3();
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_16 = __this->get_U3CU3E4__this_3();
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_17 = __this->get_U3CU3E4__this_3();
		NullCheck(L_17);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_18 = L_17->get__windMillStartSound_7();
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_19 = __this->get_U3CU3E4__this_3();
		NullCheck(L_19);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_20 = L_19->get__windMillRotationSound_6();
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_21 = __this->get_U3CU3E4__this_3();
		NullCheck(L_21);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_22 = L_21->get__windMillStartSound_7();
		NullCheck(L_22);
		float L_23;
		L_23 = AudioClip_get_length_m2223F2281D853F847BE0048620BA6F61F26440E4(L_22, /*hidden argument*/NULL);
		NullCheck(L_16);
		RuntimeObject* L_24;
		L_24 = WindmillBladesController_PlaySoundDelayed_m5C9465C96F6157B1A20D4F7F5A7D4F307D10D0FA(L_16, L_18, L_20, ((float)il2cpp_codegen_multiply((float)L_23, (float)(0.949999988f))), /*hidden argument*/NULL);
		NullCheck(L_15);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_25;
		L_25 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(L_15, L_24, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set__audioChangeCr_12(L_25);
		goto IL_00e4;
	}

IL_00ca:
	{
		// PlaySound(_windMillStopSound);
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_26 = __this->get_U3CU3E4__this_3();
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_27 = __this->get_U3CU3E4__this_3();
		NullCheck(L_27);
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_28 = L_27->get__windMillStopSound_8();
		NullCheck(L_26);
		WindmillBladesController_PlaySound_mBFB8F38297A3551225D2E57F4285B58E9EF1EA63(L_26, L_28, (bool)0, /*hidden argument*/NULL);
	}

IL_00e4:
	{
		// var diffSpeeds = Mathf.Abs(_currentSpeed - goalSpeed);
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_29 = __this->get_U3CU3E4__this_3();
		NullCheck(L_29);
		float L_30 = L_29->get__currentSpeed_10();
		float L_31 = __this->get_goalSpeed_2();
		float L_32;
		L_32 = fabsf(((float)il2cpp_codegen_subtract((float)L_30, (float)L_31)));
		__this->set_U3CdiffSpeedsU3E5__3_6(L_32);
		goto IL_0173;
	}

IL_0103:
	{
		// _currentSpeed = Mathf.Lerp(startSpeed, goalSpeed, totalTime / MAX_TIME);
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_33 = __this->get_U3CU3E4__this_3();
		float L_34 = __this->get_U3CstartSpeedU3E5__2_5();
		float L_35 = __this->get_goalSpeed_2();
		float L_36 = __this->get_U3CtotalTimeU3E5__1_4();
		float L_37;
		L_37 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_34, L_35, ((float)((float)L_36/(float)(1.0f))), /*hidden argument*/NULL);
		NullCheck(L_33);
		L_33->set__currentSpeed_10(L_37);
		// totalTime += Time.deltaTime;
		float L_38 = __this->get_U3CtotalTimeU3E5__1_4();
		float L_39;
		L_39 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_U3CtotalTimeU3E5__1_4(((float)il2cpp_codegen_add((float)L_38, (float)L_39)));
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_014e:
	{
		__this->set_U3CU3E1__state_0((-1));
		// diffSpeeds = Mathf.Abs(_currentSpeed - goalSpeed);
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_40 = __this->get_U3CU3E4__this_3();
		NullCheck(L_40);
		float L_41 = L_40->get__currentSpeed_10();
		float L_42 = __this->get_goalSpeed_2();
		float L_43;
		L_43 = fabsf(((float)il2cpp_codegen_subtract((float)L_41, (float)L_42)));
		__this->set_U3CdiffSpeedsU3E5__3_6(L_43);
	}

IL_0173:
	{
		// while (diffSpeeds > Mathf.Epsilon)
		float L_44 = __this->get_U3CdiffSpeedsU3E5__3_6();
		float L_45 = ((Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_StaticFields*)il2cpp_codegen_static_fields_for(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var))->get_Epsilon_0();
		V_3 = (bool)((((float)L_44) > ((float)L_45))? 1 : 0);
		bool L_46 = V_3;
		if (L_46)
		{
			goto IL_0103;
		}
	}
	{
		// _lerpSpeedCoroutine = null;
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_47 = __this->get_U3CU3E4__this_3();
		NullCheck(L_47);
		L_47->set__lerpSpeedCoroutine_11((Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 *)NULL);
		// }
		return (bool)0;
	}
}
// System.Object OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLerpToSpeedU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0BCE854267BF1FBEBD932AD293ECE426245E39C3 (U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_Reset_mDB45CD8EC190F294BD562D4090ADB533E5EBF9F7 (U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_Reset_mDB45CD8EC190F294BD562D4090ADB533E5EBF9F7_RuntimeMethod_var)));
	}
}
// System.Object OculusSampleFramework.WindmillBladesController/<LerpToSpeed>d__17::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CLerpToSpeedU3Ed__17_System_Collections_IEnumerator_get_Current_m81542027D8F2618D5B750EF7344E51128D3A6EA3 (U3CLerpToSpeedU3Ed__17_t9CDE768CAD150E4D7ED5A19EB0ECEEBD11A2451D * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPlaySoundDelayedU3Ed__18__ctor_m43BFCD0BBEAFA46AF3EC55B65A2F55BF43541166 (U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPlaySoundDelayedU3Ed__18_System_IDisposable_Dispose_m3978D8C006709205406E26F305AAD051EDA0CB61 (U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CPlaySoundDelayedU3Ed__18_MoveNext_m5CE5E650B1870D226841E3D4EA3A269EC73C7493 (U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_004d;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		// PlaySound(initial, false);
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_3 = __this->get_U3CU3E4__this_5();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_4 = __this->get_initial_2();
		NullCheck(L_3);
		WindmillBladesController_PlaySound_mBFB8F38297A3551225D2E57F4285B58E9EF1EA63(L_3, L_4, (bool)0, /*hidden argument*/NULL);
		// yield return new WaitForSeconds(timeDelayAfterInitial);
		float L_5 = __this->get_timeDelayAfterInitial_4();
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_6 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_6, L_5, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_6);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_004d:
	{
		__this->set_U3CU3E1__state_0((-1));
		// PlaySound(clip, true);
		WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * L_7 = __this->get_U3CU3E4__this_5();
		AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * L_8 = __this->get_clip_3();
		NullCheck(L_7);
		WindmillBladesController_PlaySound_mBFB8F38297A3551225D2E57F4285B58E9EF1EA63(L_7, L_8, (bool)1, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CPlaySoundDelayedU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DE8A8951E14A7AB0C941F512740278C4DF7841A (U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_Reset_mD0C04D5F54AAD444CE77C15B853BE79F5504160F (U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_Reset_mD0C04D5F54AAD444CE77C15B853BE79F5504160F_RuntimeMethod_var)));
	}
}
// System.Object OculusSampleFramework.WindmillBladesController/<PlaySoundDelayed>d__18::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CPlaySoundDelayedU3Ed__18_System_Collections_IEnumerator_get_Current_mC1F5FCA59AD560DFD85357BAC5AD0581AE393182 (U3CPlaySoundDelayedU3Ed__18_t8F0C89D2566AF3B264667814637857DF23D896A9 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * TeleportSupport_get_LocomotionTeleport_mC25D8C8701C7D25C8FBA30EB3C92527B8184A714_inline (TeleportSupport_t026B41AD8BFD67B4EB80376E64272DCC79979121 * __this, const RuntimeMethod* method)
{
	{
		// protected LocomotionTeleport LocomotionTeleport { get; private set; }
		LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * L_0 = __this->get_U3CLocomotionTeleportU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LocomotionTeleport_get_CurrentState_mCEDC22238BFC0EB60D9DE2616698C71D0CEF0BA8_inline (LocomotionTeleport_t727D11E3C6D163E075E04B98A06F630E61D13206 * __this, const RuntimeMethod* method)
{
	{
		// public States CurrentState { get; private set; }
		int32_t L_0 = __this->get_U3CCurrentStateU3Ek__BackingField_12();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * AimData_get_Points_m3169977C66C0D412F32CECB2B7FD471F65A191B2_inline (AimData_t4E1F88EE5D337AD83879336C4B72A4EC783E04BA * __this, const RuntimeMethod* method)
{
	{
		// public List<Vector3> Points { get; private set; }
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_0 = __this->get_U3CPointsU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool WindmillBladesController_get_IsMoving_m5B2902A40908BBD84B5478EDA121ED00DBDECF56_inline (WindmillBladesController_t71983948C596F91E28320A4EE77CF01AE7CE67A3 * __this, const RuntimeMethod* method)
{
	{
		// public bool IsMoving { get; private set; }
		bool L_0 = __this->get_U3CIsMovingU3Ek__BackingField_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LinkedList_1_get_Count_m3002AA56275E54B52256A83C262FCD1F1A39E9BC_gshared_inline (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_1();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * LinkedList_1_get_First_m60113671B7FFA2E250B7EB5C490DF4B5D5C10F52_gshared_inline (LinkedList_1_t918683DB486E2CE497E7DCBA078EBDAC50866B27 * __this, const RuntimeMethod* method)
{
	{
		LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * L_0 = (LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 *)__this->get_head_0();
		return (LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float LinkedListNode_1_get_Value_m9BA93F2A1CA19916497B4D273C24114F10B694BD_gshared_inline (LinkedListNode_1_tCC1C22983000617ED3F8E16F9846FA8DFB25BDD3 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = (float)__this->get_item_3();
		return (float)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)L_2, (int32_t)L_3);
		return (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mB9EAE3168E00BA12AA7E1233A4A0007FD12BB9E7_gshared_inline (Nullable_1_t1829213F3538788DF79B4659AFC9D6A9C90C3258 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return (bool)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
