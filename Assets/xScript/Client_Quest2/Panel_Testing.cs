using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class Panel_Testing : MonoBehaviour
{
    [SerializeField] private DKP.VR.LaserRay laserRay = new DKP.VR.LaserRay();
    [SerializeField] private List<ChooseScene> chooseScenes = new List<ChooseScene>();

    [SerializeField] private Button btBack;

    [SerializeField] private RectTransform content;
    public event EventHandler EHCloseTesting;

    [System.Serializable]
    private class ChooseScene
    {
        public string strSceneName;
        public Button btScene;
    }

    private void OnEnable()
    {
        Init();
        btBack.onClick.AddListener(delegate { BackToMenu(); });

        foreach (var i in chooseScenes)
            i.btScene.onClick.AddListener(delegate { OnClick(i); });
    }

    private void Init()
    {

    }

    private void OnClick(ChooseScene cs)
    {
        if (cs.strSceneName == "")
            return;

        UserIDCheck.roomName = cs.btScene.transform.GetChild(0).GetComponent<Text>().text;
        SceneManager.LoadScene(cs.strSceneName);
    }

    private void BackToMenu()
    {
        EHCloseTesting(this, EventArgs.Empty);
    }

    private void Update()
    {
        if (laserRay.rayEnable && laserRay.hitpos.x > -0.5f && laserRay.hitpos.x < 0.5f)
        {
            Vector2 primaryAxis = OVRInput.Get(OVRInput.RawAxis2D.RThumbstick);

            if (primaryAxis.y > 0)
                content.anchoredPosition = new Vector2(0, content.anchoredPosition.y - 500 * Time.deltaTime);
            else if (primaryAxis.y < 0)
                content.anchoredPosition = new Vector2(0, content.anchoredPosition.y + 500 * Time.deltaTime);
        }
    }
}
