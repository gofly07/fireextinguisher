﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace GPTT.Data
{
    public static class API : object
    {
        public static Dictionary<string, string> ResponseMessage = null;

        //API Server 主機 IP
        private static string API_URL = "https://bigxsafty.com";
        //API 登入
        private static string API_Login = "/firefighting-api/api/login";
        //API 滅火器新增資料
        private static string API_FireExtinguisher = "/firefighting-api/api/fireExtinguisher";
        //API 滅火器列表
        private static string API_FireExtinguisherList = "/firefighting-api/api/fireExtinguisher_list";
        //API 消防員共訓新增資料
        private static string API_Firemen = "/firefighting-api/api/firemen";
        //API 消防員共訓列表
        private static string API_FiremenList = "/firefighting-api/api/firemen_list";
        //API 消防員共訓歷程列表
        private static string API_FiremenDetailedList = "/firefighting-api/api/firemenDetailed_list";
        //API 消防員共訓影像上傳
        private static string API_FiremenVideoUpload = "/firefighting-api/api/firemen_video_upload";

        private static UnityWebRequest webRequest;

        /// <summary>
        /// USER登入
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public static IEnumerator Login_API(Login login)
        {
            //Util.Run();

            ResponseMessage = new Dictionary<string, string>();

            RequestData requestData = new RequestData
            {
                MachineToken = Util.MachineData.MachineToken,
                MachineSerialNumber = Util.GetSerialNumber(),
                Timestamp = Util.GetTimeStamp(),
                Data = Util.Encrypt(JsonUtility.ToJson(login))
            };

            string strJson = JsonUtility.ToJson(requestData);

            webRequest = UnityWebRequest.Put(API_URL + API_Login, strJson);
            webRequest.method = "POST";
            webRequest.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
            yield return webRequest.SendWebRequest();

            ResponseMessage.Add("responseCode", webRequest.responseCode.ToString());

            if (webRequest.responseCode == 200)
                UserData = JsonUtility.FromJson<User>(Util.Decrypt(webRequest.downloadHandler.text));
            else
                ResponseMessage.Add("message", webRequest.downloadHandler.text);

            webRequest.Abort();
        }

        [Serializable]
        public class Login
        {
            //帳號
            public string account;
            //密碼
            public string pwd;
        }

        [Serializable]
        public class User
        {
            //SQL Auto Increment
            public string id;
            //登入者姓名
            public string name;
            //登入者信箱
            public string email;
            //登入者單位
            public string unit;
        }

        public static User UserData = null;









        /// <summary>
        /// 滅火器訓練新增紀錄
        /// </summary>
        /// <param name="fireExtinguisher"></param>
        /// <returns></returns>
        public static IEnumerator FireExtinguisher_API(FireExtinguisher fireExtinguisher)
        {
            ResponseMessage = new Dictionary<string, string>();

            RequestData requestData = new RequestData
            {
                MachineToken = Util.MachineData.MachineToken,
                MachineSerialNumber = Util.GetSerialNumber(),
                Timestamp = Util.GetTimeStamp(),
                Data = Util.Encrypt(JsonUtility.ToJson(fireExtinguisher))
            };

            string strJson = JsonUtility.ToJson(requestData);

            webRequest = UnityWebRequest.Put(API_URL + API_FireExtinguisher, strJson);
            webRequest.method = "POST";
            webRequest.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
            yield return webRequest.SendWebRequest();

            ResponseMessage.Add("responseCode", webRequest.responseCode.ToString());
            ResponseMessage.Add("message", webRequest.downloadHandler.text);

            webRequest.Abort();
        }

        /// <summary>
        /// 滅火器訓練列表
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public static IEnumerator FireExtinguisherList_API(Search search)
        {
            ResponseMessage = new Dictionary<string, string>();

            RequestData requestData = new RequestData
            {
                MachineToken = Util.MachineData.MachineToken,
                MachineSerialNumber = Util.GetSerialNumber(),
                Timestamp = Util.GetTimeStamp(),
                Data = Util.Encrypt(JsonUtility.ToJson(search))
            };

            string strJson = JsonUtility.ToJson(requestData);

            webRequest = UnityWebRequest.Put(API_URL + API_FireExtinguisherList, strJson);
            webRequest.method = "POST";
            webRequest.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
            yield return webRequest.SendWebRequest();

            ResponseMessage.Add("responseCode", webRequest.responseCode.ToString());

            if (webRequest.responseCode == 200)
                fireExtinguisherListData = JsonUtility.FromJson<FireExtinguisherList>(Util.Decrypt(webRequest.downloadHandler.text));
            else
                ResponseMessage.Add("message", webRequest.downloadHandler.text);

            webRequest.Abort();
        }

        [Serializable]
        public class FireExtinguisher
        {
            //SQL Auto Increment
            public string id;
            //開始訓練時間
            public string startTime;
            //結束訓練時間
            public string endTime;
            //總共訓練時間
            public string totalTime;
            //訓練項目
            public string project;
            //是否通過
            public string status;
            //訓練者 SQL Auto Increment ID
            public string userId;
            //訓練者姓名
            public string userName;
        }

        [Serializable]
        public class FireExtinguisherList
        {
            //資料總筆數
            public string total;
            //最後一頁
            public string last_page;
            //Response 列表資料
            public FireExtinguisher[] Data;
        }

        public static FireExtinguisherList fireExtinguisherListData;











        /// <summary>
        /// 消防員訓練新增紀錄
        /// </summary>
        /// <param name="fireExtinguisher"></param>
        /// <returns></returns>
        public static IEnumerator Firemen_API(Firemen firemen)
        {
            ResponseMessage = new Dictionary<string, string>();
            Debug.Log(JsonUtility.ToJson(firemen));
            RequestData requestData = new RequestData
            {
                MachineToken = Util.MachineData.MachineToken,
                MachineSerialNumber = Util.GetSerialNumber(),
                Timestamp = Util.GetTimeStamp(),
                Data = Util.Encrypt(JsonUtility.ToJson(firemen))
            };

            string strJson = JsonUtility.ToJson(requestData);

            webRequest = UnityWebRequest.Put(API_URL + API_Firemen, strJson);
            webRequest.method = "POST";
            webRequest.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
            yield return webRequest.SendWebRequest();

            ResponseMessage.Add("responseCode", webRequest.responseCode.ToString());

            if (webRequest.responseCode == 200)
            {
                firemenInsertResponseData = JsonUtility.FromJson<FiremenInsertResponse>(Util.Decrypt(webRequest.downloadHandler.text));

                //開始搜尋影片上傳到server
                if (firemen.videoName != "")
                {
                    yield return SelectVideo(firemenInsertResponseData.firemenId, firemenInsertResponseData.obsPath, DateTime.Parse(firemen.videoName), 0);
                }
                
            }
            else
                ResponseMessage.Add("message", webRequest.downloadHandler.text);

            webRequest.Abort();
        }

        /// <summary>
        /// 消防員訓練列表
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public static IEnumerator FiremenList_API(Search search)
        {
            ResponseMessage = new Dictionary<string, string>();

            RequestData requestData = new RequestData
            {
                MachineToken = Util.MachineData.MachineToken,
                MachineSerialNumber = Util.GetSerialNumber(),
                Timestamp = Util.GetTimeStamp(),
                Data = Util.Encrypt(JsonUtility.ToJson(search))
            };

            string strJson = JsonUtility.ToJson(requestData);

            webRequest = UnityWebRequest.Put(API_URL + API_FiremenList, strJson);
            webRequest.method = "POST";
            webRequest.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
            yield return webRequest.SendWebRequest();

            ResponseMessage.Add("responseCode", webRequest.responseCode.ToString());

            if (webRequest.responseCode == 200)
                firemenListData = JsonUtility.FromJson<FiremenList>(Util.Decrypt(webRequest.downloadHandler.text));
            else
                ResponseMessage.Add("message", webRequest.downloadHandler.text);

            webRequest.Abort();
        }

        /// <summary>
        /// 消防員訓練歷程列表
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>
        public static IEnumerator FiremenDetailList_API(Search search)
        {
            ResponseMessage = new Dictionary<string, string>();

            RequestData requestData = new RequestData
            {
                MachineToken = Util.MachineData.MachineToken,
                MachineSerialNumber = Util.GetSerialNumber(),
                Timestamp = Util.GetTimeStamp(),
                Data = Util.Encrypt(JsonUtility.ToJson(search))
            };

            string strJson = JsonUtility.ToJson(requestData);

            webRequest = UnityWebRequest.Put(API_URL + API_FiremenDetailedList, strJson);
            webRequest.method = "POST";
            webRequest.SetRequestHeader("Content-Type", "application/json;charset=UTF-8");
            yield return webRequest.SendWebRequest();

            ResponseMessage.Add("responseCode", webRequest.responseCode.ToString());

            if (webRequest.responseCode == 200)
                firemenDetailedData = JsonUtility.FromJson<FiremenDetailedList>(Util.Decrypt(webRequest.downloadHandler.text));
            else
                ResponseMessage.Add("message", webRequest.downloadHandler.text);

            webRequest.Abort();
        }

        [Serializable]
        public class FiremenInsertResponse
        {
            //消防員共訓紀錄ID
            public string firemenId;
            //OBS存檔路徑
            public string obsPath;
        }

        public static FiremenInsertResponse firemenInsertResponseData;

        [Serializable]
        public class FiremenDetailed
        {
            //SQL Auto Increment
            public string id;
            //歷程時間
            public string trainingTime;
            //角色
            public string personnel;
            //姿勢
            public string posture;
            //行為
            public string behavior;
            //位置
            public string location;
            //訓練者 SQL Auto Increment ID
            public string userId;
            //訓練者姓名
            public string userName;
        }

        [Serializable]
        public class FiremenDetailedList
        {
            //資料總筆數
            public int total;
            //最後一頁
            public int last_page;
            //Response 列表資料
            public FiremenDetailed[] Data;
        }

        public static FiremenDetailedList firemenDetailedData;

        [Serializable]
        public class Firemen
        {
            //SQL Auto Increment
            public string id;
            //訓練日期
            public string trainingDate;
            //開始時間
            public string startTime;
            //結束時間
            public string endTime;
            //耗時
            public string totalTime;
            //模組
            public string modCategory;
            //地點
            public string place;
            //影片名稱
            public string videoName;
            //訓練者 SQL Auto Increment ID
            public string userId;
            //訓練者姓名
            public string userName;
            //歷程List
            public List<FiremenDetailed> detailedList = new List<FiremenDetailed>();
        }

        [Serializable]
        public class FiremenList
        {
            //資料總筆數
            public int total;
            //最後一頁
            public int last_page;
            //Response 列表資料
            public Firemen[] Data;
        }

        public static FiremenList firemenListData;










        /// <summary>
        /// 依時間搜尋影片 未收尋到就加一秒
        /// OBS存檔路徑設定於 D:/Uploads/
        /// OBS檔案名稱格式為 %CCYY-%MM-%DD-%hh-%mm-%ss 
        /// 在呼叫OBS開始錄影前先 DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        /// 最後呼叫API儲存時 將檔名給 firemen.videoName
        /// API程式會去將時間格式化為 yyyy-MM-dd-HH-mm-ss 去做影片搜尋
        /// 影片上傳成功後會刪除 Client端 影片
        /// </summary>
        public static IEnumerator SelectVideo(String firemenId, String obsPath, DateTime date, int x)
        {
            String videoName = "";
            if (x <= 30)
            {
                videoName = date.AddSeconds(x).ToString("yyyy-MM-dd-HH-mm-ss") + ".mp4";
                string path = "file://" + obsPath + videoName;
                yield return CheckVideo(path, videoName, firemenId, obsPath, date, x);
            }
        }

        //讀取影片是否存在
        public static IEnumerator CheckVideo(string path, string videoName, String firemenId, String obsPath, DateTime date, int x)
        {
            UnityWebRequest localFile = UnityWebRequest.Get(path);
            yield return localFile.SendWebRequest();
            if (localFile.isNetworkError || localFile.isHttpError)
            {
                Debug.Log("影片路徑抓取失敗" + path);
                yield return SelectVideo(firemenId, obsPath, date, x + 1);
            }
            else
            {
                Debug.Log("影片路徑抓取成功" + path);

                WWWForm form = new WWWForm();
                form.AddField("firemenId", firemenId);
                form.AddField("videoName", videoName);
                form.AddBinaryData("multipartFile", localFile.downloadHandler.data);

                yield return UploadVideo(form, obsPath + videoName); ;
            }
        }

        //上傳影片
        public static IEnumerator UploadVideo(WWWForm form, string videoPath)
        {
            ResponseMessage = new Dictionary<string, string>();

            UnityWebRequest webRequest = UnityWebRequest.Post(API_URL + API_FiremenVideoUpload, form);
            yield return webRequest.SendWebRequest();

            ResponseMessage.Add("responseCode", webRequest.responseCode.ToString());
            ResponseMessage.Add("message", webRequest.downloadHandler.text);

            if (webRequest.responseCode == 200)
                File.Delete(videoPath);

            webRequest.Abort();
        }










        [Serializable]
        public class RequestData
        {
            public String MachineToken;
            public String MachineSerialNumber;
            public long Timestamp;
            public String Data;
        }

        [Serializable]
        public class Search
        {
            public string limit;
            public string page;
            public string sort;
            public string sort_col;
            public string userId;
            public string firemenId;
        }
    }

}
