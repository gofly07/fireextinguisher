﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.ScreenCapture::CaptureScreenshot(System.String)
extern void ScreenCapture_CaptureScreenshot_mC755099095AD16869BE0345652DE2F64218A9E6A (void);
// 0x00000002 UnityEngine.Texture2D UnityEngine.ScreenCapture::CaptureScreenshotAsTexture()
extern void ScreenCapture_CaptureScreenshotAsTexture_m8C6C8E2E60314DCE72F06B1216F26B6F4EC6F962 (void);
// 0x00000003 System.Void UnityEngine.ScreenCapture::CaptureScreenshot(System.String,System.Int32,UnityEngine.ScreenCapture/StereoScreenCaptureMode)
extern void ScreenCapture_CaptureScreenshot_m856F202DB6C6CA5D98A5C0EE5ED41096E3CC66E7 (void);
// 0x00000004 UnityEngine.Texture2D UnityEngine.ScreenCapture::CaptureScreenshotAsTexture(System.Int32,UnityEngine.ScreenCapture/StereoScreenCaptureMode)
extern void ScreenCapture_CaptureScreenshotAsTexture_mDD92EED043DA9577C2C7F0FB6120CC35A5A4747B (void);
static Il2CppMethodPointer s_methodPointers[4] = 
{
	ScreenCapture_CaptureScreenshot_mC755099095AD16869BE0345652DE2F64218A9E6A,
	ScreenCapture_CaptureScreenshotAsTexture_m8C6C8E2E60314DCE72F06B1216F26B6F4EC6F962,
	ScreenCapture_CaptureScreenshot_m856F202DB6C6CA5D98A5C0EE5ED41096E3CC66E7,
	ScreenCapture_CaptureScreenshotAsTexture_mDD92EED043DA9577C2C7F0FB6120CC35A5A4747B,
};
static const int32_t s_InvokerIndices[4] = 
{
	6104,
	6183,
	5349,
	5542,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_ScreenCaptureModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_ScreenCaptureModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_ScreenCaptureModule_CodeGenModule = 
{
	"UnityEngine.ScreenCaptureModule.dll",
	4,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_ScreenCaptureModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
