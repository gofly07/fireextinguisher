﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.IO.MemoryStream WebSocketSharp.Ext::compress(System.IO.Stream)
extern void Ext_compress_m4358278284A560842E882535CAEE02997945C3DA (void);
// 0x00000002 System.Byte[] WebSocketSharp.Ext::decompress(System.Byte[])
extern void Ext_decompress_mDE27860D3912BD63403E7BF996F153F59FD92D3D (void);
// 0x00000003 System.IO.MemoryStream WebSocketSharp.Ext::decompress(System.IO.Stream)
extern void Ext_decompress_m2F24DFAF850F9B35C3FD4E0A1A3E7FF6C9C5AF14 (void);
// 0x00000004 System.Byte[] WebSocketSharp.Ext::decompressToArray(System.IO.Stream)
extern void Ext_decompressToArray_mEB8B011B30C5A46E00DA7F87520D6019B8242A11 (void);
// 0x00000005 System.Byte[] WebSocketSharp.Ext::Append(System.UInt16,System.String)
extern void Ext_Append_m0E57723715B401B8746CE5C9749899C97D164541 (void);
// 0x00000006 System.IO.Stream WebSocketSharp.Ext::Compress(System.IO.Stream,WebSocketSharp.CompressionMethod)
extern void Ext_Compress_mE2A46ECF97EAAEBF6113AE9E870D50CAC040AB6F (void);
// 0x00000007 System.Boolean WebSocketSharp.Ext::Contains(System.String,System.Char[])
extern void Ext_Contains_mF267D8AEC1CA95308565F0B4A52350ABAF786A2E (void);
// 0x00000008 System.Boolean WebSocketSharp.Ext::Contains(System.Collections.Specialized.NameValueCollection,System.String,System.String,System.StringComparison)
extern void Ext_Contains_m17E1757D14770EB91B608CE2815E7BA960DA2313 (void);
// 0x00000009 System.Boolean WebSocketSharp.Ext::Contains(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<T,System.Boolean>)
// 0x0000000A System.Boolean WebSocketSharp.Ext::ContainsTwice(System.String[])
extern void Ext_ContainsTwice_m4DD33BCD410B1D870759892F8B6D7D77AD927BD2 (void);
// 0x0000000B System.Void WebSocketSharp.Ext::CopyTo(System.IO.Stream,System.IO.Stream,System.Int32)
extern void Ext_CopyTo_mB436FB49B8616F60E26EFEB3C6F0D723A4189FBA (void);
// 0x0000000C System.Byte[] WebSocketSharp.Ext::Decompress(System.Byte[],WebSocketSharp.CompressionMethod)
extern void Ext_Decompress_m5051854DFB667F2D5FC17DE828439EB1A67F9D00 (void);
// 0x0000000D System.Byte[] WebSocketSharp.Ext::DecompressToArray(System.IO.Stream,WebSocketSharp.CompressionMethod)
extern void Ext_DecompressToArray_mAA1406B8850D2404435700563124565AEBBE8F03 (void);
// 0x0000000E System.Void WebSocketSharp.Ext::Emit(System.EventHandler,System.Object,System.EventArgs)
extern void Ext_Emit_m53CC51B43FBF1FC6AED0C878BE0DC02D26EF220B (void);
// 0x0000000F System.Void WebSocketSharp.Ext::Emit(System.EventHandler`1<TEventArgs>,System.Object,TEventArgs)
// 0x00000010 System.Boolean WebSocketSharp.Ext::EqualsWith(System.Int32,System.Char,System.Action`1<System.Int32>)
extern void Ext_EqualsWith_mB93057EE05D870A4CFA31EA72B15A632D54EE6E5 (void);
// 0x00000011 System.String WebSocketSharp.Ext::GetAbsolutePath(System.Uri)
extern void Ext_GetAbsolutePath_m719787C61869F48F573EE93861A56430E98AEB17 (void);
// 0x00000012 WebSocketSharp.Net.CookieCollection WebSocketSharp.Ext::GetCookies(System.Collections.Specialized.NameValueCollection,System.Boolean)
extern void Ext_GetCookies_mE81477DBE254503ADF2F5B3173D4D96EEC7D4E4A (void);
// 0x00000013 System.String WebSocketSharp.Ext::GetMessage(WebSocketSharp.CloseStatusCode)
extern void Ext_GetMessage_m53797B74D6828C7E9D1B91D575F6159AD431C03E (void);
// 0x00000014 System.Byte[] WebSocketSharp.Ext::GetUTF8EncodedBytes(System.String)
extern void Ext_GetUTF8EncodedBytes_mB9035E3F22A387B3717298A96D687EDC5AFB71B2 (void);
// 0x00000015 System.String WebSocketSharp.Ext::GetValue(System.String,System.Char,System.Boolean)
extern void Ext_GetValue_mAF45AD216AC35C5D1BA224BEAAD6A6FF83DDE044 (void);
// 0x00000016 System.Byte[] WebSocketSharp.Ext::InternalToByteArray(System.UInt16,WebSocketSharp.ByteOrder)
extern void Ext_InternalToByteArray_m3CCD4BFE6D64214EDB0D5C07514DDBD636A2303F (void);
// 0x00000017 System.Byte[] WebSocketSharp.Ext::InternalToByteArray(System.UInt64,WebSocketSharp.ByteOrder)
extern void Ext_InternalToByteArray_mD8ECE713D31534241B3A20C070AECD1275D2D2B6 (void);
// 0x00000018 System.Boolean WebSocketSharp.Ext::IsCompressionExtension(System.String,WebSocketSharp.CompressionMethod)
extern void Ext_IsCompressionExtension_m2D95C896B134964E2BCC94AF1FD73399888B03E9 (void);
// 0x00000019 System.Boolean WebSocketSharp.Ext::IsControl(System.Byte)
extern void Ext_IsControl_mD6C47A8942EC85383A1FA21D750B3420A3A600E2 (void);
// 0x0000001A System.Boolean WebSocketSharp.Ext::IsData(System.Byte)
extern void Ext_IsData_mD1FDD951BEAC505088F40E7E96A4EC37DAA1B921 (void);
// 0x0000001B System.Boolean WebSocketSharp.Ext::IsData(WebSocketSharp.Opcode)
extern void Ext_IsData_m0D314D4CE1CB0481957905FA9EF5DD8E08B33A0D (void);
// 0x0000001C System.Boolean WebSocketSharp.Ext::IsReserved(System.UInt16)
extern void Ext_IsReserved_mB14EE3EC1D539A9C9C35652B3BE120E97A5F96EB (void);
// 0x0000001D System.Boolean WebSocketSharp.Ext::IsSupported(System.Byte)
extern void Ext_IsSupported_m274DFE67596E1C20FE078D43462675E1471E0BC2 (void);
// 0x0000001E System.Boolean WebSocketSharp.Ext::IsText(System.String)
extern void Ext_IsText_m4BF1ADE56AD743FBE48AD8F85DC2586F980D2164 (void);
// 0x0000001F System.Boolean WebSocketSharp.Ext::IsToken(System.String)
extern void Ext_IsToken_m181B2CFD2601FFEADB4BCCE758E0DAD9F158C2DE (void);
// 0x00000020 System.Byte[] WebSocketSharp.Ext::ReadBytes(System.IO.Stream,System.Int32)
extern void Ext_ReadBytes_m46DBC394CDA9BD532F01E92390A9DACBF2B92C2D (void);
// 0x00000021 System.Byte[] WebSocketSharp.Ext::ReadBytes(System.IO.Stream,System.Int64,System.Int32)
extern void Ext_ReadBytes_m0F3560D4CFEC3F4E2F01A6AB4F8CF19BD301BDD3 (void);
// 0x00000022 System.Void WebSocketSharp.Ext::ReadBytesAsync(System.IO.Stream,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern void Ext_ReadBytesAsync_m16D0BD06630A2B20F3A59700665CA0A5328E736F (void);
// 0x00000023 System.Void WebSocketSharp.Ext::ReadBytesAsync(System.IO.Stream,System.Int64,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern void Ext_ReadBytesAsync_m4A84EB6C86EDB4A46262CF97D20258137A7A5FCF (void);
// 0x00000024 T[] WebSocketSharp.Ext::Reverse(T[])
// 0x00000025 System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharp.Ext::SplitHeaderValue(System.String,System.Char[])
extern void Ext_SplitHeaderValue_m01EE9C6D8102DB88FA9937B7EC8AF3CF4004B67A (void);
// 0x00000026 System.Byte[] WebSocketSharp.Ext::ToByteArray(System.IO.Stream)
extern void Ext_ToByteArray_mD20CDE09F09523A7A54F5489D9A42326FDCC561D (void);
// 0x00000027 System.String WebSocketSharp.Ext::ToExtensionString(WebSocketSharp.CompressionMethod,System.String[])
extern void Ext_ToExtensionString_m1A4430E0A6CD59742837AB50919C7008230C0E39 (void);
// 0x00000028 System.Collections.Generic.List`1<TSource> WebSocketSharp.Ext::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000029 System.UInt16 WebSocketSharp.Ext::ToUInt16(System.Byte[],WebSocketSharp.ByteOrder)
extern void Ext_ToUInt16_mF963B0EB55BD1C1B24DF8FCB77070EB8AE1908CB (void);
// 0x0000002A System.UInt64 WebSocketSharp.Ext::ToUInt64(System.Byte[],WebSocketSharp.ByteOrder)
extern void Ext_ToUInt64_m8875AD2574EA9F437EB6499A833BBB87A8A07CB4 (void);
// 0x0000002B System.Boolean WebSocketSharp.Ext::TryCreateWebSocketUri(System.String,System.Uri&,System.String&)
extern void Ext_TryCreateWebSocketUri_m3944CC7FC2F49F59DE819B4E18877FE84C9CB42A (void);
// 0x0000002C System.Boolean WebSocketSharp.Ext::TryGetUTF8DecodedString(System.Byte[],System.String&)
extern void Ext_TryGetUTF8DecodedString_m07ED759625CB1683806215FDC4EA050F3F41E062 (void);
// 0x0000002D System.Boolean WebSocketSharp.Ext::TryGetUTF8EncodedBytes(System.String,System.Byte[]&)
extern void Ext_TryGetUTF8EncodedBytes_m9CA6A8F40D22157CE7096C2C0C90EEAF35D18CB5 (void);
// 0x0000002E System.String WebSocketSharp.Ext::Unquote(System.String)
extern void Ext_Unquote_m430FB83166985AC03BF2BD8283CAEDBE21F66602 (void);
// 0x0000002F System.Boolean WebSocketSharp.Ext::Upgrades(System.Collections.Specialized.NameValueCollection,System.String)
extern void Ext_Upgrades_m672B32A90886DEF7430195058BDD9B85E0F955F9 (void);
// 0x00000030 System.Void WebSocketSharp.Ext::WriteBytes(System.IO.Stream,System.Byte[],System.Int32)
extern void Ext_WriteBytes_mC92631F017F5C1290CB883FBAECE35156B2ED308 (void);
// 0x00000031 System.Boolean WebSocketSharp.Ext::IsEnclosedIn(System.String,System.Char)
extern void Ext_IsEnclosedIn_mC4AD7A067E1070CDD25235847375ECA14647D8F3 (void);
// 0x00000032 System.Boolean WebSocketSharp.Ext::IsHostOrder(WebSocketSharp.ByteOrder)
extern void Ext_IsHostOrder_m82CC168E47DC97D8E61D971E684A97AC7F748AF2 (void);
// 0x00000033 System.Boolean WebSocketSharp.Ext::IsNullOrEmpty(System.String)
extern void Ext_IsNullOrEmpty_mEB4CFD1426630064F5435FADB81B48B26161793E (void);
// 0x00000034 System.Boolean WebSocketSharp.Ext::IsPredefinedScheme(System.String)
extern void Ext_IsPredefinedScheme_m274F02E1D5677ED6112F71F299C6562BA2AFD5C0 (void);
// 0x00000035 System.Boolean WebSocketSharp.Ext::MaybeUri(System.String)
extern void Ext_MaybeUri_m9183FD39642FB589BE7AE769C8146A6A19386B11 (void);
// 0x00000036 T[] WebSocketSharp.Ext::SubArray(T[],System.Int32,System.Int32)
// 0x00000037 T[] WebSocketSharp.Ext::SubArray(T[],System.Int64,System.Int64)
// 0x00000038 System.Byte[] WebSocketSharp.Ext::ToHostOrder(System.Byte[],WebSocketSharp.ByteOrder)
extern void Ext_ToHostOrder_mFC9D0D1F9DC21733829F38C35C3B819DD90A9CBC (void);
// 0x00000039 System.String WebSocketSharp.Ext::ToString(T[],System.String)
// 0x0000003A System.Uri WebSocketSharp.Ext::ToUri(System.String)
extern void Ext_ToUri_m36BFA928AEF6A14DB081BFEF75143B68665F2F88 (void);
// 0x0000003B System.Void WebSocketSharp.Ext::.cctor()
extern void Ext__cctor_mA75769ED37738922CD747FC3EDEB0697C14008FE (void);
// 0x0000003C System.Void WebSocketSharp.Ext/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m2BCF3D122CDD2BFF8754141B5B36329285CC24AB (void);
// 0x0000003D System.Boolean WebSocketSharp.Ext/<>c__DisplayClass21_0::<ContainsTwice>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass21_0_U3CContainsTwiceU3Eb__0_m655E85AD466C26675FAC6CAC1759DD9D6E5371B1 (void);
// 0x0000003E System.Void WebSocketSharp.Ext/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_m5C7BD90966D0F14250DDFA0EBC0538A546211E6C (void);
// 0x0000003F System.Void WebSocketSharp.Ext/<>c__DisplayClass59_0::<ReadBytesAsync>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass59_0_U3CReadBytesAsyncU3Eb__0_mE5DD53DD07EEC4B0FD280F7DB4E4B548B49837A6 (void);
// 0x00000040 System.Void WebSocketSharp.Ext/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_mB58148A9B0132C7D1222C7D91D40B89364793DA4 (void);
// 0x00000041 System.Void WebSocketSharp.Ext/<>c__DisplayClass60_0::<ReadBytesAsync>b__0(System.Int64)
extern void U3CU3Ec__DisplayClass60_0_U3CReadBytesAsyncU3Eb__0_m268BD3476574A3CEDFD50C6F3056A296820614F1 (void);
// 0x00000042 System.Void WebSocketSharp.Ext/<>c__DisplayClass60_1::.ctor()
extern void U3CU3Ec__DisplayClass60_1__ctor_mE471A74C1292C57D6782C110B0137F6334AD92F4 (void);
// 0x00000043 System.Void WebSocketSharp.Ext/<>c__DisplayClass60_1::<ReadBytesAsync>b__1(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass60_1_U3CReadBytesAsyncU3Eb__1_m3015741A1DCC71F30AAC0D7B64CCED91519EDE6D (void);
// 0x00000044 System.Void WebSocketSharp.Ext/<SplitHeaderValue>d__62::.ctor(System.Int32)
extern void U3CSplitHeaderValueU3Ed__62__ctor_mD0F6C1B1B13A73C577FF6F04AA2EF11BF7C895F4 (void);
// 0x00000045 System.Void WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.IDisposable.Dispose()
extern void U3CSplitHeaderValueU3Ed__62_System_IDisposable_Dispose_m13B0D952747EF8A3E7E734C9FE4A9B6E4E7ABB05 (void);
// 0x00000046 System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>d__62::MoveNext()
extern void U3CSplitHeaderValueU3Ed__62_MoveNext_m02893FAFFAEB3E74DB5CC21852617C485DD92765 (void);
// 0x00000047 System.String WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m303D4DF29F9239C771BA12712ABC5AE8D4CBE13C (void);
// 0x00000048 System.Void WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.Collections.IEnumerator.Reset()
extern void U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_Reset_m48C8B244C0B1648685FBD53A216EE5004BD258D3 (void);
// 0x00000049 System.Object WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.Collections.IEnumerator.get_Current()
extern void U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_get_Current_mFB4110BE04322EC29F00261128176708E13C50A0 (void);
// 0x0000004A System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m20D4ECB95C3C084914B659CF5E262D913718F626 (void);
// 0x0000004B System.Collections.IEnumerator WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.Collections.IEnumerable.GetEnumerator()
extern void U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerable_GetEnumerator_mA5D81BE65336ED51944619BE3CAC8FA0FF695C95 (void);
// 0x0000004C System.Void WebSocketSharp.MessageEventArgs::.ctor(WebSocketSharp.WebSocketFrame)
extern void MessageEventArgs__ctor_m2A1208F060211AE5B20D123ADB4817BC6CAECFEB (void);
// 0x0000004D System.Void WebSocketSharp.MessageEventArgs::.ctor(WebSocketSharp.Opcode,System.Byte[])
extern void MessageEventArgs__ctor_m7EEA211A3002A410FFD41650FFAD121E75A4446D (void);
// 0x0000004E System.String WebSocketSharp.MessageEventArgs::get_Data()
extern void MessageEventArgs_get_Data_m5A505CFE600EE15DC9041713B8C568D71C5CBB63 (void);
// 0x0000004F System.Void WebSocketSharp.MessageEventArgs::setData()
extern void MessageEventArgs_setData_m84E444D41FEF301DC7D1400933073D8421E10220 (void);
// 0x00000050 System.Void WebSocketSharp.CloseEventArgs::.ctor(WebSocketSharp.PayloadData,System.Boolean)
extern void CloseEventArgs__ctor_m2C6580973BCDEB0F0E4E8F2643C092423F4890C9 (void);
// 0x00000051 System.Void WebSocketSharp.ErrorEventArgs::.ctor(System.String,System.Exception)
extern void ErrorEventArgs__ctor_m049E3481288E6F50084D27EFA0431556A1E713B7 (void);
// 0x00000052 System.Void WebSocketSharp.WebSocket::.cctor()
extern void WebSocket__cctor_mBF1E757B75242B4625491BFEA5889B9ED7119F72 (void);
// 0x00000053 System.Void WebSocketSharp.WebSocket::.ctor(System.String,System.String[])
extern void WebSocket__ctor_m922DA968DBD07A0529B3BE9CE576188FD0B5C094 (void);
// 0x00000054 System.Boolean WebSocketSharp.WebSocket::get_HasMessage()
extern void WebSocket_get_HasMessage_m1FB96B036F1E3247BB2B598A6C41A1D525E2662B (void);
// 0x00000055 WebSocketSharp.WebSocketState WebSocketSharp.WebSocket::get_ReadyState()
extern void WebSocket_get_ReadyState_m8E4A7508979AB89D1326A902751E9EA57B603F85 (void);
// 0x00000056 WebSocketSharp.Net.ClientSslConfiguration WebSocketSharp.WebSocket::get_SslConfiguration()
extern void WebSocket_get_SslConfiguration_m9396292A9B1EF24F6B8604D19EEAD4F2E4904C2D (void);
// 0x00000057 System.Void WebSocketSharp.WebSocket::add_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern void WebSocket_add_OnClose_mA8DE5F76FD7433CDAD33CC2D8812D7001B750A66 (void);
// 0x00000058 System.Void WebSocketSharp.WebSocket::remove_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern void WebSocket_remove_OnClose_m2AE23DB4E743F675A549A0E71E3558FF7A2F3DBB (void);
// 0x00000059 System.Void WebSocketSharp.WebSocket::add_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern void WebSocket_add_OnError_m362EE37535D15C3B900BAD60249F10707EA6729E (void);
// 0x0000005A System.Void WebSocketSharp.WebSocket::remove_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern void WebSocket_remove_OnError_mD2F241B0D17251C16DF1A40E533E5F20AB19E651 (void);
// 0x0000005B System.Void WebSocketSharp.WebSocket::add_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern void WebSocket_add_OnMessage_mFB8C1D5D98E13FCCB1653ABE35704A532B5988A6 (void);
// 0x0000005C System.Void WebSocketSharp.WebSocket::remove_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern void WebSocket_remove_OnMessage_m455B56B3B9360D764DD5A5BB635ECAFF6EB54D07 (void);
// 0x0000005D System.Void WebSocketSharp.WebSocket::add_OnOpen(System.EventHandler)
extern void WebSocket_add_OnOpen_m40F1299BBE18CDD3880D5111CAFD058C2D02A874 (void);
// 0x0000005E System.Void WebSocketSharp.WebSocket::remove_OnOpen(System.EventHandler)
extern void WebSocket_remove_OnOpen_m30290C709FC3C8F8DDC36076F97F87E04D7DDD2C (void);
// 0x0000005F System.Boolean WebSocketSharp.WebSocket::checkHandshakeResponse(WebSocketSharp.HttpResponse,System.String&)
extern void WebSocket_checkHandshakeResponse_mF3BFD88D355C08B9D5464855C0ADE1E79FA57E94 (void);
// 0x00000060 System.Boolean WebSocketSharp.WebSocket::checkProtocols(System.String[],System.String&)
extern void WebSocket_checkProtocols_mA1A5E30205CA5BC405EDE62C9CC4AF9ACB6927BC (void);
// 0x00000061 System.Boolean WebSocketSharp.WebSocket::checkReceivedFrame(WebSocketSharp.WebSocketFrame,System.String&)
extern void WebSocket_checkReceivedFrame_mFCB84F26393F34C80D8789999C916BB35D273228 (void);
// 0x00000062 System.Void WebSocketSharp.WebSocket::close(System.UInt16,System.String)
extern void WebSocket_close_m02BDB543E56B74784B2ABF8D680C2739F2115D6F (void);
// 0x00000063 System.Void WebSocketSharp.WebSocket::close(WebSocketSharp.PayloadData,System.Boolean,System.Boolean,System.Boolean)
extern void WebSocket_close_m6B70D8C6C1E11AE19931E59FC4AE825CE05D248A (void);
// 0x00000064 System.Boolean WebSocketSharp.WebSocket::closeHandshake(WebSocketSharp.PayloadData,System.Boolean,System.Boolean,System.Boolean)
extern void WebSocket_closeHandshake_mB682AF7AB000E1F290047D6E51C23613A3FAAE81 (void);
// 0x00000065 System.Boolean WebSocketSharp.WebSocket::connect()
extern void WebSocket_connect_mB380C23832997519BAEBAB8E022041B11D2DCD64 (void);
// 0x00000066 System.String WebSocketSharp.WebSocket::createExtensions()
extern void WebSocket_createExtensions_m1F574ACD030DA089522EDCB7B2DEFAB409F4C9E5 (void);
// 0x00000067 WebSocketSharp.HttpRequest WebSocketSharp.WebSocket::createHandshakeRequest()
extern void WebSocket_createHandshakeRequest_mB333CABDE3F80FAC18C182B9A042FD9A17752ED0 (void);
// 0x00000068 System.Void WebSocketSharp.WebSocket::doHandshake()
extern void WebSocket_doHandshake_mBCA0FD27DAFED7A4D58F7F3C4BF29E3E44200F12 (void);
// 0x00000069 System.Void WebSocketSharp.WebSocket::enqueueToMessageEventQueue(WebSocketSharp.MessageEventArgs)
extern void WebSocket_enqueueToMessageEventQueue_m2092C8DD9DF898800BD52118FBABD5095D8D2B74 (void);
// 0x0000006A System.Void WebSocketSharp.WebSocket::error(System.String,System.Exception)
extern void WebSocket_error_m1B3A2F204039C8098B4189C783372BAC426DCA97 (void);
// 0x0000006B System.Void WebSocketSharp.WebSocket::fatal(System.String,System.Exception)
extern void WebSocket_fatal_mAF3B8099763885426D121C75F87AA439AF9C389D (void);
// 0x0000006C System.Void WebSocketSharp.WebSocket::fatal(System.String,System.UInt16)
extern void WebSocket_fatal_m1871F1D7F8439260ECDD9244DA4E91408219E88E (void);
// 0x0000006D System.Void WebSocketSharp.WebSocket::fatal(System.String,WebSocketSharp.CloseStatusCode)
extern void WebSocket_fatal_m5F3908C89DD054C87FA53194D3872B27383AD12B (void);
// 0x0000006E WebSocketSharp.Net.ClientSslConfiguration WebSocketSharp.WebSocket::getSslConfiguration()
extern void WebSocket_getSslConfiguration_mC57CE4497AC04BD614175EECD3B28731230A10B4 (void);
// 0x0000006F System.Void WebSocketSharp.WebSocket::init()
extern void WebSocket_init_m5AAB6FF2AE5BD979307FD5C6536A17799A2A4284 (void);
// 0x00000070 System.Void WebSocketSharp.WebSocket::message()
extern void WebSocket_message_mB69559458AC1A592384668C35066F239FB2DFE11 (void);
// 0x00000071 System.Void WebSocketSharp.WebSocket::messagec(WebSocketSharp.MessageEventArgs)
extern void WebSocket_messagec_mBB0B13944C5415C535CFB8897FA307325718C176 (void);
// 0x00000072 System.Void WebSocketSharp.WebSocket::open()
extern void WebSocket_open_mD227A699DB8632471E0FF473990D53FCA115F37A (void);
// 0x00000073 System.Boolean WebSocketSharp.WebSocket::processCloseFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processCloseFrame_m341AA7E1ADA14D05A71CA219C9196FA2A3DE4AB1 (void);
// 0x00000074 System.Void WebSocketSharp.WebSocket::processCookies(WebSocketSharp.Net.CookieCollection)
extern void WebSocket_processCookies_mCE65E3EBB7BD838AF1DFB09A9F3BD7FE41BA2052 (void);
// 0x00000075 System.Boolean WebSocketSharp.WebSocket::processDataFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processDataFrame_m38651FE664375395C21A4C006243C6C6893329B8 (void);
// 0x00000076 System.Boolean WebSocketSharp.WebSocket::processFragmentFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processFragmentFrame_m801AF044A538EE6FB7C0DA41F0B9B13A7D359A19 (void);
// 0x00000077 System.Boolean WebSocketSharp.WebSocket::processPingFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processPingFrame_m982A9E14B86942D6D731817259E582AF387E876D (void);
// 0x00000078 System.Boolean WebSocketSharp.WebSocket::processPongFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processPongFrame_m3FB60ECF4A021FA52CB1F32311FC059FEE2D2F12 (void);
// 0x00000079 System.Boolean WebSocketSharp.WebSocket::processReceivedFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processReceivedFrame_m6BA2094096E2CC13C5315E1AE61175B194330C5A (void);
// 0x0000007A System.Void WebSocketSharp.WebSocket::processSecWebSocketExtensionsServerHeader(System.String)
extern void WebSocket_processSecWebSocketExtensionsServerHeader_m09B3AA86D61AB2E4FDCA2E8C6B9B9F9119F66B56 (void);
// 0x0000007B System.Boolean WebSocketSharp.WebSocket::processUnsupportedFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processUnsupportedFrame_m69F5B0C2AFF2EF098E2F6EC5112F5BA87E828D34 (void);
// 0x0000007C System.Void WebSocketSharp.WebSocket::releaseClientResources()
extern void WebSocket_releaseClientResources_mD26F73F4794E873D4E1BFA93E69CB46A7F50BAF0 (void);
// 0x0000007D System.Void WebSocketSharp.WebSocket::releaseCommonResources()
extern void WebSocket_releaseCommonResources_m8253F95D7E5DA221D21A6F81080FA294B975C232 (void);
// 0x0000007E System.Void WebSocketSharp.WebSocket::releaseResources()
extern void WebSocket_releaseResources_mED7D3CE825EBA68EEC52FC579698DF0E8FF2C94C (void);
// 0x0000007F System.Void WebSocketSharp.WebSocket::releaseServerResources()
extern void WebSocket_releaseServerResources_m42383848291B6E3B4A2510D504BEFC63E5B3B8F1 (void);
// 0x00000080 System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream)
extern void WebSocket_send_m15FEBC3E51D73974E8B02498255420B14B102713 (void);
// 0x00000081 System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream,System.Boolean)
extern void WebSocket_send_m31A66C1F2D1EF9CDAF7566434F12DDC9D31475BB (void);
// 0x00000082 System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Fin,WebSocketSharp.Opcode,System.Byte[],System.Boolean)
extern void WebSocket_send_m19807526833D3940322EE9A13A3394FA12EA78B8 (void);
// 0x00000083 System.Boolean WebSocketSharp.WebSocket::sendBytes(System.Byte[])
extern void WebSocket_sendBytes_m662D431639A507188471C969D2A014BC2E2B6820 (void);
// 0x00000084 WebSocketSharp.HttpResponse WebSocketSharp.WebSocket::sendHandshakeRequest()
extern void WebSocket_sendHandshakeRequest_m4CC45304F37D568233914845F32FD9C1184C9C1A (void);
// 0x00000085 WebSocketSharp.HttpResponse WebSocketSharp.WebSocket::sendHttpRequest(WebSocketSharp.HttpRequest,System.Int32)
extern void WebSocket_sendHttpRequest_mBCC35A44A07989A813876A6264F23C300A08BF74 (void);
// 0x00000086 System.Void WebSocketSharp.WebSocket::sendProxyConnectRequest()
extern void WebSocket_sendProxyConnectRequest_m54F557B0AEE314593FD3B5409C4D227E84F47029 (void);
// 0x00000087 System.Void WebSocketSharp.WebSocket::setClientStream()
extern void WebSocket_setClientStream_m9B787B1BC3FE35905856C5F48AE3152F0DE7E2D8 (void);
// 0x00000088 System.Void WebSocketSharp.WebSocket::startReceiving()
extern void WebSocket_startReceiving_m3260E40714599330ECBF6F0E8C6E31E8FBD924CD (void);
// 0x00000089 System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketAcceptHeader(System.String)
extern void WebSocket_validateSecWebSocketAcceptHeader_m199824AF1843A047AA7C210193F2C74AD053F9A1 (void);
// 0x0000008A System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketExtensionsServerHeader(System.String)
extern void WebSocket_validateSecWebSocketExtensionsServerHeader_m6435E0E6DA97D34997C84B73038B1920C8CC00E5 (void);
// 0x0000008B System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketProtocolServerHeader(System.String)
extern void WebSocket_validateSecWebSocketProtocolServerHeader_mE24A209409042EF8B15016660E64209B2FA74386 (void);
// 0x0000008C System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketVersionServerHeader(System.String)
extern void WebSocket_validateSecWebSocketVersionServerHeader_m811EBF818F5C8DB13745DF989056F3B34C286714 (void);
// 0x0000008D System.String WebSocketSharp.WebSocket::CreateBase64Key()
extern void WebSocket_CreateBase64Key_mD771D577EB2DE12E0BF1C6E6041DC641484A3983 (void);
// 0x0000008E System.String WebSocketSharp.WebSocket::CreateResponseKey(System.String)
extern void WebSocket_CreateResponseKey_m8511A3E52E5FBD56856E3E545ACFEBDBF123F831 (void);
// 0x0000008F System.Void WebSocketSharp.WebSocket::Close()
extern void WebSocket_Close_mFF036D51A1F0FEFB339A36101B224E30964E6953 (void);
// 0x00000090 System.Void WebSocketSharp.WebSocket::Connect()
extern void WebSocket_Connect_mCA3D7ADB803DD8C421DC8EF5839B17BEDDB2210D (void);
// 0x00000091 System.Void WebSocketSharp.WebSocket::Send(System.String)
extern void WebSocket_Send_m8CB5F072D94C6E294E9EE0D6BCBCD0140338CB3A (void);
// 0x00000092 System.Void WebSocketSharp.WebSocket::System.IDisposable.Dispose()
extern void WebSocket_System_IDisposable_Dispose_m63D2EDDDD3758747F97FAB12531149D701212669 (void);
// 0x00000093 System.Void WebSocketSharp.WebSocket::<open>b__146_0(System.IAsyncResult)
extern void WebSocket_U3CopenU3Eb__146_0_mD7B092980016BCF1CBBC93B23FDD9B18AC9CA2F6 (void);
// 0x00000094 System.Void WebSocketSharp.WebSocket/<>c::.cctor()
extern void U3CU3Ec__cctor_mFF8A5C9CF05BF8A4829822D615AA786EB2C8299F (void);
// 0x00000095 System.Void WebSocketSharp.WebSocket/<>c::.ctor()
extern void U3CU3Ec__ctor_m117570B5819BC791785761A957D9253FCE5A7A30 (void);
// 0x00000096 System.Boolean WebSocketSharp.WebSocket/<>c::<checkProtocols>b__120_0(System.String)
extern void U3CU3Ec_U3CcheckProtocolsU3Eb__120_0_mFC7B8401775DFBA3D0E66F467E4424EA533CAF15 (void);
// 0x00000097 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass174_0::.ctor()
extern void U3CU3Ec__DisplayClass174_0__ctor_mE36EF473E335D521D4B0DB27B7EE0894C511D4E1 (void);
// 0x00000098 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass174_0::<startReceiving>b__0()
extern void U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__0_m8E24D6D9AE0C588A287C43371FC99377E42CA707 (void);
// 0x00000099 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass174_0::<startReceiving>b__1(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__1_mACC3030A2A69A3DC6335C77AF7C26CD579AE8673 (void);
// 0x0000009A System.Void WebSocketSharp.WebSocket/<>c__DisplayClass174_0::<startReceiving>b__2(System.Exception)
extern void U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__2_mC9E14CB1C0A921640461335B5AF22B6266DEE7AF (void);
// 0x0000009B System.Void WebSocketSharp.WebSocket/<>c__DisplayClass176_0::.ctor()
extern void U3CU3Ec__DisplayClass176_0__ctor_m684BBE2A9281D5413141C477D916D65FEEA0C8A4 (void);
// 0x0000009C System.Boolean WebSocketSharp.WebSocket/<>c__DisplayClass176_0::<validateSecWebSocketExtensionsServerHeader>b__0(System.String)
extern void U3CU3Ec__DisplayClass176_0_U3CvalidateSecWebSocketExtensionsServerHeaderU3Eb__0_m093F9B11367CA89CBDD22D7CBA6C0A8C96857719 (void);
// 0x0000009D System.Void WebSocketSharp.WebSocket/<>c__DisplayClass177_0::.ctor()
extern void U3CU3Ec__DisplayClass177_0__ctor_mEBAC1947E62BA2E35EB67691BE4F0D6910B152DA (void);
// 0x0000009E System.Boolean WebSocketSharp.WebSocket/<>c__DisplayClass177_0::<validateSecWebSocketProtocolServerHeader>b__0(System.String)
extern void U3CU3Ec__DisplayClass177_0_U3CvalidateSecWebSocketProtocolServerHeaderU3Eb__0_mB7F235C4E423B82FA02CF32AADD2756B21BFCFD0 (void);
// 0x0000009F System.Void WebSocketSharp.PayloadData::.cctor()
extern void PayloadData__cctor_m8EADEC6AE38490AED2274302C79C4797F43722DC (void);
// 0x000000A0 System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[])
extern void PayloadData__ctor_m658CBD26A46F9ACDDCB50C43B53CE3C85D3E0703 (void);
// 0x000000A1 System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[],System.Int64)
extern void PayloadData__ctor_m43C0C23A7B7E55573669050B1CEB6A7FCB806F21 (void);
// 0x000000A2 System.Void WebSocketSharp.PayloadData::.ctor(System.UInt16,System.String)
extern void PayloadData__ctor_m63AD7348411A3B5965AA4E72057D322CE1AE9EBC (void);
// 0x000000A3 System.UInt16 WebSocketSharp.PayloadData::get_Code()
extern void PayloadData_get_Code_m6F55D55E77C2567FB1749EE0E35CD45585C7CA58 (void);
// 0x000000A4 System.Boolean WebSocketSharp.PayloadData::get_HasReservedCode()
extern void PayloadData_get_HasReservedCode_mA2A90C33F1088C1E2A20A1FB6715EE19EE670900 (void);
// 0x000000A5 System.Byte[] WebSocketSharp.PayloadData::get_ApplicationData()
extern void PayloadData_get_ApplicationData_m8FFE92425A32E613CFBA980B9B49DBFD26D3FBD4 (void);
// 0x000000A6 System.UInt64 WebSocketSharp.PayloadData::get_Length()
extern void PayloadData_get_Length_mF27008252337038FAD3FE1FC88B810A4B4E51EC0 (void);
// 0x000000A7 System.Void WebSocketSharp.PayloadData::Mask(System.Byte[])
extern void PayloadData_Mask_m523717F5A2C66E440B31AFC386485DC99BC856AC (void);
// 0x000000A8 System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.PayloadData::GetEnumerator()
extern void PayloadData_GetEnumerator_m22C7B4773174B70E5FB2D51B33AFEECF2F67D682 (void);
// 0x000000A9 System.Byte[] WebSocketSharp.PayloadData::ToArray()
extern void PayloadData_ToArray_m480821B617EFC7F278807C27C7732EFA9EAAFB50 (void);
// 0x000000AA System.String WebSocketSharp.PayloadData::ToString()
extern void PayloadData_ToString_m7CAA9743A55C65714202109CDDD9BDCD5B0EB328 (void);
// 0x000000AB System.Collections.IEnumerator WebSocketSharp.PayloadData::System.Collections.IEnumerable.GetEnumerator()
extern void PayloadData_System_Collections_IEnumerable_GetEnumerator_m579797823AA7E5C219CD945047B717A634D4231A (void);
// 0x000000AC System.Void WebSocketSharp.PayloadData/<GetEnumerator>d__25::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__25__ctor_m7290C117BBBD824041032B6A783AE4AF32226A43 (void);
// 0x000000AD System.Void WebSocketSharp.PayloadData/<GetEnumerator>d__25::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__25_System_IDisposable_Dispose_mFDCDAD41B87D1F55DAE4A68FE3767B22A28C5F54 (void);
// 0x000000AE System.Boolean WebSocketSharp.PayloadData/<GetEnumerator>d__25::MoveNext()
extern void U3CGetEnumeratorU3Ed__25_MoveNext_m5BBA0C78B71EEC97413990316AD2D16A9662923D (void);
// 0x000000AF System.Byte WebSocketSharp.PayloadData/<GetEnumerator>d__25::System.Collections.Generic.IEnumerator<System.Byte>.get_Current()
extern void U3CGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mA7B801C8652EAE220C45112BB0883B24421B41B1 (void);
// 0x000000B0 System.Void WebSocketSharp.PayloadData/<GetEnumerator>d__25::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m413CE860B0CE2F51C4F2E31AB83F860327B58A54 (void);
// 0x000000B1 System.Object WebSocketSharp.PayloadData/<GetEnumerator>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m2CCE50DF3B959BC0B0D59F873D0DB781453FE120 (void);
// 0x000000B2 System.Void WebSocketSharp.WebSocketException::.ctor(System.String)
extern void WebSocketException__ctor_m33E614CB48AE162856CF66B43CFA3820B36E885D (void);
// 0x000000B3 System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode)
extern void WebSocketException__ctor_m8EBC29CC6D95F272F73D6B224BB63A55B8F5F26E (void);
// 0x000000B4 System.Void WebSocketSharp.WebSocketException::.ctor(System.String,System.Exception)
extern void WebSocketException__ctor_m0C5006A2D887D58AFBE107A79DEAE88E94AC03E0 (void);
// 0x000000B5 System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.Exception)
extern void WebSocketException__ctor_m6F648D792B0BEE233E71334EF5630D5DF9CDB38A (void);
// 0x000000B6 System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.String)
extern void WebSocketException__ctor_m1CF13CD49D70141B24D98DE5B3F40E50673876FD (void);
// 0x000000B7 System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.String,System.Exception)
extern void WebSocketException__ctor_mC0314C41BD7A079315E298C6F276A9BFFDC357F1 (void);
// 0x000000B8 WebSocketSharp.CloseStatusCode WebSocketSharp.WebSocketException::get_Code()
extern void WebSocketException_get_Code_m6303EF200458C42155DC8FC3F5CE54D032F7EB2A (void);
// 0x000000B9 System.Void WebSocketSharp.LogData::.ctor(WebSocketSharp.LogLevel,System.Diagnostics.StackFrame,System.String)
extern void LogData__ctor_mA4E490BB4AD085273333A0671EA9BDDA6AD65E99 (void);
// 0x000000BA System.String WebSocketSharp.LogData::ToString()
extern void LogData_ToString_mB912103F53201C69F5C7E3026E3AB9BFBB08CBEF (void);
// 0x000000BB System.Void WebSocketSharp.Logger::.ctor()
extern void Logger__ctor_m56612F787878F92C39B39A98D73C0C2A6E92449D (void);
// 0x000000BC System.Void WebSocketSharp.Logger::.ctor(WebSocketSharp.LogLevel,System.String,System.Action`2<WebSocketSharp.LogData,System.String>)
extern void Logger__ctor_m879D37E6E08DA95C7543A012E562FC852320F614 (void);
// 0x000000BD System.Void WebSocketSharp.Logger::defaultOutput(WebSocketSharp.LogData,System.String)
extern void Logger_defaultOutput_m638580E8604793C81D9995F218C501BBB846C4B8 (void);
// 0x000000BE System.Void WebSocketSharp.Logger::output(System.String,WebSocketSharp.LogLevel)
extern void Logger_output_m43C911A929E5AD6BBEAA34130FDABD747301EC2D (void);
// 0x000000BF System.Void WebSocketSharp.Logger::writeToFile(System.String,System.String)
extern void Logger_writeToFile_m45A3CA06576664B6B7F566AFBE3E52350D32E193 (void);
// 0x000000C0 System.Void WebSocketSharp.Logger::Debug(System.String)
extern void Logger_Debug_mEB54E96D44B38DB015BD3672FBF7C1AAA10D97CF (void);
// 0x000000C1 System.Void WebSocketSharp.Logger::Error(System.String)
extern void Logger_Error_mC7AB02F8B01230993938A6712BC95F7A84CCF9D4 (void);
// 0x000000C2 System.Void WebSocketSharp.Logger::Fatal(System.String)
extern void Logger_Fatal_m3CF912E03127C2731E44B2A5C85C95B48459738E (void);
// 0x000000C3 System.Void WebSocketSharp.Logger::Info(System.String)
extern void Logger_Info_m2A8010FF066CEEFA02AB38DE4F70A875AB9BFF8C (void);
// 0x000000C4 System.Void WebSocketSharp.Logger::Trace(System.String)
extern void Logger_Trace_mDE773047E49A9B4D519A5C6FDF27AC919283A4D0 (void);
// 0x000000C5 System.Void WebSocketSharp.Logger::Warn(System.String)
extern void Logger_Warn_mF60C556ADCEF121DBACF0BCD00F7E70D54383860 (void);
// 0x000000C6 System.Void WebSocketSharp.WebSocketFrame::.cctor()
extern void WebSocketFrame__cctor_m7B4EA50590BBFF37373A9D14260E7CD1A5C29915 (void);
// 0x000000C7 System.Void WebSocketSharp.WebSocketFrame::.ctor()
extern void WebSocketFrame__ctor_mF4DCB29F2C9613E8B2D2080FD4CC593E08898A61 (void);
// 0x000000C8 System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,System.Byte[],System.Boolean,System.Boolean)
extern void WebSocketFrame__ctor_mDB53820703CFD503AA14DFE9712CBF94CA726001 (void);
// 0x000000C9 System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.PayloadData,System.Boolean,System.Boolean)
extern void WebSocketFrame__ctor_mD648878987D949C7E722360DCB0FA9D39B40F428 (void);
// 0x000000CA System.UInt64 WebSocketSharp.WebSocketFrame::get_ExactPayloadLength()
extern void WebSocketFrame_get_ExactPayloadLength_mF73F2DE0850DEA5E5BFCE20BC67A2A335E15E70E (void);
// 0x000000CB System.Int32 WebSocketSharp.WebSocketFrame::get_ExtendedPayloadLengthWidth()
extern void WebSocketFrame_get_ExtendedPayloadLengthWidth_m8E2942AA9E36E857FB96109EB0B4660559B6E0F1 (void);
// 0x000000CC System.Boolean WebSocketSharp.WebSocketFrame::get_IsClose()
extern void WebSocketFrame_get_IsClose_mAA4D2AE49427CD9738D8AC66301180E1C6E85F9F (void);
// 0x000000CD System.Boolean WebSocketSharp.WebSocketFrame::get_IsCompressed()
extern void WebSocketFrame_get_IsCompressed_m14A768A52A2DA5B4C1AD969775FC33C7B73B2F19 (void);
// 0x000000CE System.Boolean WebSocketSharp.WebSocketFrame::get_IsContinuation()
extern void WebSocketFrame_get_IsContinuation_m777358323A7402D1511D59BA48D7DA4D8C578C87 (void);
// 0x000000CF System.Boolean WebSocketSharp.WebSocketFrame::get_IsData()
extern void WebSocketFrame_get_IsData_m30E6330D07BD119F57738F9F27E91EC1C9D73B07 (void);
// 0x000000D0 System.Boolean WebSocketSharp.WebSocketFrame::get_IsFinal()
extern void WebSocketFrame_get_IsFinal_m975B1AE82A1375D08440E08A34BC1B5974ACAF99 (void);
// 0x000000D1 System.Boolean WebSocketSharp.WebSocketFrame::get_IsFragment()
extern void WebSocketFrame_get_IsFragment_m08B7D5166D52EA3C48472D1161B3792887543B50 (void);
// 0x000000D2 System.Boolean WebSocketSharp.WebSocketFrame::get_IsMasked()
extern void WebSocketFrame_get_IsMasked_mAB2DED2695BD710FF78A9D96096BF93E26F59BDB (void);
// 0x000000D3 System.Boolean WebSocketSharp.WebSocketFrame::get_IsPing()
extern void WebSocketFrame_get_IsPing_m17308380EFBF26833097A3C8DC7E7F71463C88C0 (void);
// 0x000000D4 System.Boolean WebSocketSharp.WebSocketFrame::get_IsPong()
extern void WebSocketFrame_get_IsPong_mDF3FBB942DFDE9C0FEFAE6118DD3F3D40550BBCD (void);
// 0x000000D5 System.Boolean WebSocketSharp.WebSocketFrame::get_IsText()
extern void WebSocketFrame_get_IsText_mF6A84F510563FB5659789AC46CDD806DA06E0CF2 (void);
// 0x000000D6 System.UInt64 WebSocketSharp.WebSocketFrame::get_Length()
extern void WebSocketFrame_get_Length_m28FB305CEDD5F042715588559DF45C75190510CC (void);
// 0x000000D7 WebSocketSharp.Opcode WebSocketSharp.WebSocketFrame::get_Opcode()
extern void WebSocketFrame_get_Opcode_mECC7B1D5880DD55E7AB62364288408700D46123A (void);
// 0x000000D8 WebSocketSharp.PayloadData WebSocketSharp.WebSocketFrame::get_PayloadData()
extern void WebSocketFrame_get_PayloadData_mE6B1D54490BE8DD45F4475565374C4E6DBE93967 (void);
// 0x000000D9 WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv2()
extern void WebSocketFrame_get_Rsv2_m13644B12282AC45E5F04F80CE7230FA20BE52353 (void);
// 0x000000DA WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv3()
extern void WebSocketFrame_get_Rsv3_m3C0A8663FDD30A5AB46680A7ED378B97D6B4F117 (void);
// 0x000000DB System.Byte[] WebSocketSharp.WebSocketFrame::createMaskingKey()
extern void WebSocketFrame_createMaskingKey_mE03F37782469DBD5A70A9C5C63B2F27670C316A4 (void);
// 0x000000DC System.String WebSocketSharp.WebSocketFrame::dump(WebSocketSharp.WebSocketFrame)
extern void WebSocketFrame_dump_mAC4242054E54C3BFC69F1C9E3CA66A6FE6610540 (void);
// 0x000000DD System.String WebSocketSharp.WebSocketFrame::print(WebSocketSharp.WebSocketFrame)
extern void WebSocketFrame_print_m8C858FB381EA463C3979D70174B6D7DD62562A3D (void);
// 0x000000DE WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::processHeader(System.Byte[])
extern void WebSocketFrame_processHeader_mE573FD7E935A31249EECFBBF75199C364B3A854F (void);
// 0x000000DF System.Void WebSocketSharp.WebSocketFrame::readExtendedPayloadLengthAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readExtendedPayloadLengthAsync_m922290C927B427EAF1EC185BEE879BF7329D3FC6 (void);
// 0x000000E0 System.Void WebSocketSharp.WebSocketFrame::readHeaderAsync(System.IO.Stream,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readHeaderAsync_m4F195087FD1EC50D4D092400BF7838BE6B29E8F7 (void);
// 0x000000E1 System.Void WebSocketSharp.WebSocketFrame::readMaskingKeyAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readMaskingKeyAsync_m5F73FEA0B005B1C021A9D0F14D3783EBE3E38EF6 (void);
// 0x000000E2 System.Void WebSocketSharp.WebSocketFrame::readPayloadDataAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readPayloadDataAsync_m3965810AA59F5336E29916F110553F00031AB76D (void);
// 0x000000E3 System.String WebSocketSharp.WebSocketFrame::utf8Decode(System.Byte[])
extern void WebSocketFrame_utf8Decode_m72D693503AA6265E4BCEDD39F46B192EF3A330F6 (void);
// 0x000000E4 WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.PayloadData,System.Boolean)
extern void WebSocketFrame_CreateCloseFrame_mD637E8215EA93D3FA0BF7AD000F9FE1BBDAD8731 (void);
// 0x000000E5 WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePingFrame(System.Boolean)
extern void WebSocketFrame_CreatePingFrame_m7FB996F641AC3EE368827D689C9BD68D7FCED0EF (void);
// 0x000000E6 WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePongFrame(WebSocketSharp.PayloadData,System.Boolean)
extern void WebSocketFrame_CreatePongFrame_mBB4043327342566AE21805B02ED973108E1B57D1 (void);
// 0x000000E7 System.Void WebSocketSharp.WebSocketFrame::ReadFrameAsync(System.IO.Stream,System.Boolean,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_ReadFrameAsync_m9C0E785D48AB8DA1CE13C5728D775F80BFAC83FB (void);
// 0x000000E8 System.Void WebSocketSharp.WebSocketFrame::Unmask()
extern void WebSocketFrame_Unmask_m5C7F2FE3B40645E6A10BB581DD0AC0B265AFE172 (void);
// 0x000000E9 System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.WebSocketFrame::GetEnumerator()
extern void WebSocketFrame_GetEnumerator_mEF67D9D83EEAE0CECBE4963682C9B6FA5154DE7E (void);
// 0x000000EA System.String WebSocketSharp.WebSocketFrame::PrintToString(System.Boolean)
extern void WebSocketFrame_PrintToString_m4DAE2A73154CBF92CED95FB61199EA250C0C04CC (void);
// 0x000000EB System.Byte[] WebSocketSharp.WebSocketFrame::ToArray()
extern void WebSocketFrame_ToArray_m217A9E7E238584910BA05C707C45A237A6A5229B (void);
// 0x000000EC System.String WebSocketSharp.WebSocketFrame::ToString()
extern void WebSocketFrame_ToString_m99235962DF7944D08AA6E82215097C920ED79DF4 (void);
// 0x000000ED System.Collections.IEnumerator WebSocketSharp.WebSocketFrame::System.Collections.IEnumerable.GetEnumerator()
extern void WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_m487A358E47EAD2D7334216DFD26DD6FED191BDCC (void);
// 0x000000EE System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_0::.ctor()
extern void U3CU3Ec__DisplayClass67_0__ctor_mE6E1A585A035A12F4EAE82416222E607953A62F3 (void);
// 0x000000EF System.Action`4<System.String,System.String,System.String,System.String> WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_0::<dump>b__0()
extern void U3CU3Ec__DisplayClass67_0_U3CdumpU3Eb__0_m9768D92D3135F3085BCC4360EEB73207B21EF8B6 (void);
// 0x000000F0 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_1::.ctor()
extern void U3CU3Ec__DisplayClass67_1__ctor_m22AB30B37FD6134D7508AE21B40C83841E03ADD9 (void);
// 0x000000F1 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_1::<dump>b__1(System.String,System.String,System.String,System.String)
extern void U3CU3Ec__DisplayClass67_1_U3CdumpU3Eb__1_m8A3B62265AC42FF04D9EA593BCBC90ACD8E9D7B8 (void);
// 0x000000F2 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass71_0::.ctor()
extern void U3CU3Ec__DisplayClass71_0__ctor_m93F995D1AB05D60F418ED2988D4D8A692D255B46 (void);
// 0x000000F3 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass71_0::<readExtendedPayloadLengthAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass71_0_U3CreadExtendedPayloadLengthAsyncU3Eb__0_m3DC816E2F4CD686296CA4B35676A6A75D6CA5264 (void);
// 0x000000F4 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass73_0::.ctor()
extern void U3CU3Ec__DisplayClass73_0__ctor_mF299AFA95D2D5898364D62DDAAA84D8F98BDE4D6 (void);
// 0x000000F5 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass73_0::<readHeaderAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass73_0_U3CreadHeaderAsyncU3Eb__0_m18193F118E9CC413CB727A0CB34FAF56A6A30049 (void);
// 0x000000F6 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass75_0::.ctor()
extern void U3CU3Ec__DisplayClass75_0__ctor_mF0CC56940CC5BA9FB42CB68237BD70F33F4A7D48 (void);
// 0x000000F7 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass75_0::<readMaskingKeyAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass75_0_U3CreadMaskingKeyAsyncU3Eb__0_mBB2B7AAD10A5CABD0AE45EBDE22DF484FF6C8C51 (void);
// 0x000000F8 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass77_0::.ctor()
extern void U3CU3Ec__DisplayClass77_0__ctor_m21DDF022F43D7DD3C46CC55253138F5A72160A0C (void);
// 0x000000F9 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass77_0::<readPayloadDataAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass77_0_U3CreadPayloadDataAsyncU3Eb__0_mBEF25F96FBBEEB1D01DAB37A807F1CCFBE778A5B (void);
// 0x000000FA System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass84_0::.ctor()
extern void U3CU3Ec__DisplayClass84_0__ctor_m251DFE8F5FF2FA6B0AC3C1F60FB6131B76C39851 (void);
// 0x000000FB System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass84_0::<ReadFrameAsync>b__0(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__0_m2F441FFFCC21CF9D2AFA35D22C6172AF5A92DE0D (void);
// 0x000000FC System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass84_0::<ReadFrameAsync>b__1(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__1_m406798DAF939740176942C2A9726FEC2C05CFE60 (void);
// 0x000000FD System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass84_0::<ReadFrameAsync>b__2(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__2_mD6B77492AEBDF5D7D780A2FFB03CE4961F9D1FA3 (void);
// 0x000000FE System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass84_0::<ReadFrameAsync>b__3(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__3_mB95223F0107617D006EA1CCE46F6E8ACD90B3326 (void);
// 0x000000FF System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__86__ctor_mFDDD212B6E6A6FFAB4FD0CA6D9541929DE36E06A (void);
// 0x00000100 System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__86_System_IDisposable_Dispose_mDA8B0BA2DB9C194B5932895D77173C70CF63D01D (void);
// 0x00000101 System.Boolean WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::MoveNext()
extern void U3CGetEnumeratorU3Ed__86_MoveNext_m997E2491C32856DE993A6B2D9ABFC425831B0DC0 (void);
// 0x00000102 System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::System.Collections.Generic.IEnumerator<System.Byte>.get_Current()
extern void U3CGetEnumeratorU3Ed__86_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m3EA1B7BF4D4B7576E0EFB264B5919542FB74CD33 (void);
// 0x00000103 System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_Reset_m79C1B3397472E5979B328686C1169427742B14B4 (void);
// 0x00000104 System.Object WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_get_Current_mC579C33994AC27CEEAD74CEC09B3D6F517560A20 (void);
// 0x00000105 System.Void WebSocketSharp.HttpBase::.ctor(System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpBase__ctor_mF1E1C4A447ACA2BC996D4D90151192B843C18102 (void);
// 0x00000106 System.String WebSocketSharp.HttpBase::get_EntityBody()
extern void HttpBase_get_EntityBody_m3468AC17BE0108BED7DC6517AC98525F6FC2DD33 (void);
// 0x00000107 System.Collections.Specialized.NameValueCollection WebSocketSharp.HttpBase::get_Headers()
extern void HttpBase_get_Headers_m2C4961F0604F56C8C06511D5ECB7E4A05F522299 (void);
// 0x00000108 System.Version WebSocketSharp.HttpBase::get_ProtocolVersion()
extern void HttpBase_get_ProtocolVersion_m505377392BBA51CD8CB114D624A293AED38FDCC6 (void);
// 0x00000109 System.Byte[] WebSocketSharp.HttpBase::readEntityBody(System.IO.Stream,System.String)
extern void HttpBase_readEntityBody_m867C03EFBD465E67E553F743CE4664136F66224C (void);
// 0x0000010A System.String[] WebSocketSharp.HttpBase::readHeaders(System.IO.Stream,System.Int32)
extern void HttpBase_readHeaders_mAE0AC3D3FBB0BD9973D8311C9C52433FF93CE5ED (void);
// 0x0000010B T WebSocketSharp.HttpBase::Read(System.IO.Stream,System.Func`2<System.String[],T>,System.Int32)
// 0x0000010C System.Byte[] WebSocketSharp.HttpBase::ToByteArray()
extern void HttpBase_ToByteArray_m5365A340C7EF67272CC8AF332C7D45DDE060FAC7 (void);
// 0x0000010D System.Void WebSocketSharp.HttpBase/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m34215EDB0480034655DDE5DDA4C6ED370BFCBCA6 (void);
// 0x0000010E System.Void WebSocketSharp.HttpBase/<>c__DisplayClass13_0::<readHeaders>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass13_0_U3CreadHeadersU3Eb__0_m3520202656188C794A3FB269BF0A549B2558548B (void);
// 0x0000010F System.Void WebSocketSharp.HttpBase/<>c__DisplayClass14_0`1::.ctor()
// 0x00000110 System.Void WebSocketSharp.HttpBase/<>c__DisplayClass14_0`1::<Read>b__0(System.Object)
// 0x00000111 System.Void WebSocketSharp.HttpRequest::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpRequest__ctor_mE237958BCB1B4A9CDAFE1BE093012D028C507AD5 (void);
// 0x00000112 System.Void WebSocketSharp.HttpRequest::.ctor(System.String,System.String)
extern void HttpRequest__ctor_mCC851D0918B0C56F212BBB3FE492CB45E0985355 (void);
// 0x00000113 WebSocketSharp.HttpRequest WebSocketSharp.HttpRequest::CreateConnectRequest(System.Uri)
extern void HttpRequest_CreateConnectRequest_m74240951A912D803421AD0206B94F14CB945A005 (void);
// 0x00000114 WebSocketSharp.HttpRequest WebSocketSharp.HttpRequest::CreateWebSocketRequest(System.Uri)
extern void HttpRequest_CreateWebSocketRequest_m0F85CA0DF6563C88037201F285C0E30E3CDA4979 (void);
// 0x00000115 WebSocketSharp.HttpResponse WebSocketSharp.HttpRequest::GetResponse(System.IO.Stream,System.Int32)
extern void HttpRequest_GetResponse_mDFBE5D8DAE637EDA3CB80FEA51663C8C08C833A6 (void);
// 0x00000116 System.Void WebSocketSharp.HttpRequest::SetCookies(WebSocketSharp.Net.CookieCollection)
extern void HttpRequest_SetCookies_m213AEA59069FAA0095DDF1B37E862E16E3D33DE1 (void);
// 0x00000117 System.String WebSocketSharp.HttpRequest::ToString()
extern void HttpRequest_ToString_mCAE304871B470833EEF8513AE5A75043951443DE (void);
// 0x00000118 System.Void WebSocketSharp.HttpResponse::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpResponse__ctor_mF23A56537CB31E82D8A8DE42805D9EDBA728966D (void);
// 0x00000119 WebSocketSharp.Net.CookieCollection WebSocketSharp.HttpResponse::get_Cookies()
extern void HttpResponse_get_Cookies_m78CED25591E8F7688CBD8346D5DAD470E28B377E (void);
// 0x0000011A System.Boolean WebSocketSharp.HttpResponse::get_HasConnectionClose()
extern void HttpResponse_get_HasConnectionClose_m0A61095FC147EB6EC989DA847DFFF046B08CA1C0 (void);
// 0x0000011B System.Boolean WebSocketSharp.HttpResponse::get_IsProxyAuthenticationRequired()
extern void HttpResponse_get_IsProxyAuthenticationRequired_m0BD4DAC6C86A678D9D3F1317F2CFC219B7868D56 (void);
// 0x0000011C System.Boolean WebSocketSharp.HttpResponse::get_IsRedirect()
extern void HttpResponse_get_IsRedirect_m981833B2735F5824DE3E2BFFE25D2DE1985A4DC2 (void);
// 0x0000011D System.Boolean WebSocketSharp.HttpResponse::get_IsUnauthorized()
extern void HttpResponse_get_IsUnauthorized_mD5345B66E2271CBA89927E95F1B7CFD786699DC3 (void);
// 0x0000011E System.Boolean WebSocketSharp.HttpResponse::get_IsWebSocketResponse()
extern void HttpResponse_get_IsWebSocketResponse_m2378896FDF333E763308CBE7C93AA69D1C1148EC (void);
// 0x0000011F System.String WebSocketSharp.HttpResponse::get_StatusCode()
extern void HttpResponse_get_StatusCode_mCDF7D450E520712A4874022BC81F2D037343B290 (void);
// 0x00000120 WebSocketSharp.HttpResponse WebSocketSharp.HttpResponse::Parse(System.String[])
extern void HttpResponse_Parse_mBE4F6D080B1A5262ED43090C0BF5E8BC42868DE3 (void);
// 0x00000121 System.String WebSocketSharp.HttpResponse::ToString()
extern void HttpResponse_ToString_m07290A9C6F91DCA8AFDB3736F2A03CD30C83D518 (void);
// 0x00000122 System.Void WebSocketSharp.Net.Cookie::.cctor()
extern void Cookie__cctor_m15C8B451F36B03AEC908CB880CDCA388DCB1FBB0 (void);
// 0x00000123 System.Void WebSocketSharp.Net.Cookie::.ctor()
extern void Cookie__ctor_m88DA7F9F87C4B9E650EC0191012FBAEC7F0233E0 (void);
// 0x00000124 System.Void WebSocketSharp.Net.Cookie::.ctor(System.String,System.String)
extern void Cookie__ctor_m7E4DAF65E3AA9EF0E14D73412FC078C412B0B880 (void);
// 0x00000125 System.Void WebSocketSharp.Net.Cookie::.ctor(System.String,System.String,System.String,System.String)
extern void Cookie__ctor_m380FF0544D979D657BC7EF43EB4103CA1FE651C6 (void);
// 0x00000126 System.Void WebSocketSharp.Net.Cookie::set_MaxAge(System.Int32)
extern void Cookie_set_MaxAge_m755FA5D7A1EFAEE14727B8FC468C901784339C77 (void);
// 0x00000127 System.Void WebSocketSharp.Net.Cookie::set_SameSite(System.String)
extern void Cookie_set_SameSite_mFF91AE2B3DBAF8DAB4A7654EBC82A31848367C1D (void);
// 0x00000128 System.Void WebSocketSharp.Net.Cookie::set_Comment(System.String)
extern void Cookie_set_Comment_m477535E3C35E2971501C0BBE7D63D4C706E61DDA (void);
// 0x00000129 System.Void WebSocketSharp.Net.Cookie::set_CommentUri(System.Uri)
extern void Cookie_set_CommentUri_m0AEBFCA4E658B518F2A89E35FEA348E54173FE95 (void);
// 0x0000012A System.Void WebSocketSharp.Net.Cookie::set_Discard(System.Boolean)
extern void Cookie_set_Discard_mDA48275B8FDF1C12FC0788941D72E42D291D1076 (void);
// 0x0000012B System.Void WebSocketSharp.Net.Cookie::set_Domain(System.String)
extern void Cookie_set_Domain_mC28AC311DAA04DAEA84F28F34BF06FA7CC6906BA (void);
// 0x0000012C System.Boolean WebSocketSharp.Net.Cookie::get_Expired()
extern void Cookie_get_Expired_mF493025E38ECC26E3B32933671F0E887E8865132 (void);
// 0x0000012D System.DateTime WebSocketSharp.Net.Cookie::get_Expires()
extern void Cookie_get_Expires_m0E5335CFEAA0C9F83D78037347FA882FCC7196DC (void);
// 0x0000012E System.Void WebSocketSharp.Net.Cookie::set_Expires(System.DateTime)
extern void Cookie_set_Expires_m415F7BFDA83A47AB8AFDFB7BA4A6BE737EF250B7 (void);
// 0x0000012F System.Void WebSocketSharp.Net.Cookie::set_HttpOnly(System.Boolean)
extern void Cookie_set_HttpOnly_mC02BCDF89D7DFC618EDDB49C599E6087C3642945 (void);
// 0x00000130 System.String WebSocketSharp.Net.Cookie::get_Name()
extern void Cookie_get_Name_mB3EB068775AA77A2BFFD0479265C5409C7277BF5 (void);
// 0x00000131 System.String WebSocketSharp.Net.Cookie::get_Path()
extern void Cookie_get_Path_m8BC4E2E90A4DC80F0093B304E51D552AF9AEC7A4 (void);
// 0x00000132 System.Void WebSocketSharp.Net.Cookie::set_Path(System.String)
extern void Cookie_set_Path_m30E349842B509925BF90609FB6F35B867020DBEE (void);
// 0x00000133 System.Void WebSocketSharp.Net.Cookie::set_Port(System.String)
extern void Cookie_set_Port_mEA9B55A2042F45C1F0BD6B212A9860D7DA3A5FD5 (void);
// 0x00000134 System.Void WebSocketSharp.Net.Cookie::set_Secure(System.Boolean)
extern void Cookie_set_Secure_m0AE9EC8BAF6FC3ABDCFC5F08317A1D6B05BC5A93 (void);
// 0x00000135 System.Int32 WebSocketSharp.Net.Cookie::get_Version()
extern void Cookie_get_Version_mDB121464556942924F2BC6E2C0AF47B4417C156C (void);
// 0x00000136 System.Void WebSocketSharp.Net.Cookie::set_Version(System.Int32)
extern void Cookie_set_Version_mC8CE4EE6F5E9F620269191F2606269D6F4BD0581 (void);
// 0x00000137 System.Int32 WebSocketSharp.Net.Cookie::hash(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Cookie_hash_mE7E311788F5A7D031D26C2C377954B50B6A4E767 (void);
// 0x00000138 System.Void WebSocketSharp.Net.Cookie::init(System.String,System.String,System.String,System.String)
extern void Cookie_init_m73BA84C5EF3BA8F32E81D3053877A87AEB564236 (void);
// 0x00000139 System.Boolean WebSocketSharp.Net.Cookie::tryCreatePorts(System.String,System.Int32[]&)
extern void Cookie_tryCreatePorts_mFA866E4429B431A633BA748210EDC82DE43C34F6 (void);
// 0x0000013A System.Boolean WebSocketSharp.Net.Cookie::EqualsWithoutValue(WebSocketSharp.Net.Cookie)
extern void Cookie_EqualsWithoutValue_m6A17B401FE6A55564C3DFDC89425715AB7EDF2EF (void);
// 0x0000013B System.String WebSocketSharp.Net.Cookie::ToRequestString(System.Uri)
extern void Cookie_ToRequestString_m115F41177F4FA5388F06A6A7739D736C726A199D (void);
// 0x0000013C System.Boolean WebSocketSharp.Net.Cookie::TryCreate(System.String,System.String,WebSocketSharp.Net.Cookie&)
extern void Cookie_TryCreate_mF063A434AF1115C9B0E7E30213F356401CB26410 (void);
// 0x0000013D System.Boolean WebSocketSharp.Net.Cookie::Equals(System.Object)
extern void Cookie_Equals_mE045964281082E0D436A8D8771C7486421C3180F (void);
// 0x0000013E System.Int32 WebSocketSharp.Net.Cookie::GetHashCode()
extern void Cookie_GetHashCode_m157AF758B0ABDFD27664B292E01778B37F67F42F (void);
// 0x0000013F System.String WebSocketSharp.Net.Cookie::ToString()
extern void Cookie_ToString_m8567F449D809403B5851A7518D4467C1896B2630 (void);
// 0x00000140 System.Void WebSocketSharp.Net.CookieCollection::.ctor()
extern void CookieCollection__ctor_mFD618E3B4A4FE99CEF694330D7CA3E2E22BB4DF8 (void);
// 0x00000141 System.Collections.Generic.IEnumerable`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::get_Sorted()
extern void CookieCollection_get_Sorted_m25BA9DA6B9BD75257DAE7E394D569067DCA61F61 (void);
// 0x00000142 System.Int32 WebSocketSharp.Net.CookieCollection::get_Count()
extern void CookieCollection_get_Count_m62F0AAB4A4145624F147C3C2CF613B25F88C4ACB (void);
// 0x00000143 System.Boolean WebSocketSharp.Net.CookieCollection::get_IsReadOnly()
extern void CookieCollection_get_IsReadOnly_mAD9FA0A6CC1E5587FDE416A545D2BEC99AA625E7 (void);
// 0x00000144 System.Void WebSocketSharp.Net.CookieCollection::add(WebSocketSharp.Net.Cookie)
extern void CookieCollection_add_mEC7DAE8B1250B56C805B28174057819FF3453C95 (void);
// 0x00000145 System.Int32 WebSocketSharp.Net.CookieCollection::compareForSorted(WebSocketSharp.Net.Cookie,WebSocketSharp.Net.Cookie)
extern void CookieCollection_compareForSorted_mBE12999D8D1A2B578F3CD06F855ADB8221990E3F (void);
// 0x00000146 WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::parseRequest(System.String)
extern void CookieCollection_parseRequest_m4A411F7D1727A52D0E886A0A906E03FD1D0ED13C (void);
// 0x00000147 WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::parseResponse(System.String)
extern void CookieCollection_parseResponse_m7950E99147C1B8BC6CD9ACC6FA6FF62D5BBEF178 (void);
// 0x00000148 System.Int32 WebSocketSharp.Net.CookieCollection::search(WebSocketSharp.Net.Cookie)
extern void CookieCollection_search_m16DE9FDB551EF73EA37560AE89F48C33700A7BAA (void);
// 0x00000149 System.String WebSocketSharp.Net.CookieCollection::urlDecode(System.String,System.Text.Encoding)
extern void CookieCollection_urlDecode_m853FDCD39702ED0B55B258884556C9310122BCB1 (void);
// 0x0000014A WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::Parse(System.String,System.Boolean)
extern void CookieCollection_Parse_mD07834988B9F19106A76B324B3F3A3AA24EB243E (void);
// 0x0000014B System.Void WebSocketSharp.Net.CookieCollection::SetOrRemove(WebSocketSharp.Net.Cookie)
extern void CookieCollection_SetOrRemove_mEE0DA2FC4D4198C867774A2FAF59E397C11A1C91 (void);
// 0x0000014C System.Void WebSocketSharp.Net.CookieCollection::SetOrRemove(WebSocketSharp.Net.CookieCollection)
extern void CookieCollection_SetOrRemove_m29182CEE1E7C052C92DD628B937D205181A38314 (void);
// 0x0000014D System.Void WebSocketSharp.Net.CookieCollection::Add(WebSocketSharp.Net.Cookie)
extern void CookieCollection_Add_mF98A3231E80CA3DD5377E99282B140675B84DF5B (void);
// 0x0000014E System.Void WebSocketSharp.Net.CookieCollection::Clear()
extern void CookieCollection_Clear_m24C090FCC0DB43CE2AEF55E0865E868AA50826FB (void);
// 0x0000014F System.Boolean WebSocketSharp.Net.CookieCollection::Contains(WebSocketSharp.Net.Cookie)
extern void CookieCollection_Contains_mF13A9B55C4A167E101231F777B00813DD38E76E9 (void);
// 0x00000150 System.Void WebSocketSharp.Net.CookieCollection::CopyTo(WebSocketSharp.Net.Cookie[],System.Int32)
extern void CookieCollection_CopyTo_m3DA12CE3EACDF518A9917B9442EE7AF3A11F5B1B (void);
// 0x00000151 System.Collections.Generic.IEnumerator`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::GetEnumerator()
extern void CookieCollection_GetEnumerator_m84E24C1354F7324B477464B1A11EF80AE85F9A8A (void);
// 0x00000152 System.Boolean WebSocketSharp.Net.CookieCollection::Remove(WebSocketSharp.Net.Cookie)
extern void CookieCollection_Remove_mD0A12D0F5DA3D33F712E27D585C462B9A7115032 (void);
// 0x00000153 System.Collections.IEnumerator WebSocketSharp.Net.CookieCollection::System.Collections.IEnumerable.GetEnumerator()
extern void CookieCollection_System_Collections_IEnumerable_GetEnumerator_m218E6FCF4C3E21E1797EB91F812FA1FA37F15698 (void);
// 0x00000154 System.Void WebSocketSharp.Net.CookieException::.ctor(System.String,System.Exception)
extern void CookieException__ctor_m8139F97FAD9D781F74EA8495BF4EB94C708E3946 (void);
// 0x00000155 System.Void WebSocketSharp.Net.CookieException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException__ctor_m62C4075DD3BA5423EFEB71AAEEAFFEB4C633DCF5 (void);
// 0x00000156 System.Void WebSocketSharp.Net.CookieException::.ctor()
extern void CookieException__ctor_m0FB8DF9E315A43D7F600D54B8296305EDE2EE414 (void);
// 0x00000157 System.Void WebSocketSharp.Net.CookieException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException_GetObjectData_m6FDD567698C7C883A8C3BA8280E1DDD3F786F205 (void);
// 0x00000158 System.Void WebSocketSharp.Net.CookieException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m85AD28C193C6DCF9276301C15A75084B27F2D85D (void);
// 0x00000159 System.Void WebSocketSharp.Net.HttpUtility::.cctor()
extern void HttpUtility__cctor_mD22BA93238B775593BF8F689F3FA9C2B00C04454 (void);
// 0x0000015A System.Int32 WebSocketSharp.Net.HttpUtility::getNumber(System.Char)
extern void HttpUtility_getNumber_m63ACC9ADDF3028484EDA2B636D6730F044D28E8B (void);
// 0x0000015B System.Int32 WebSocketSharp.Net.HttpUtility::getNumber(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_getNumber_mF6BE53AE92A9F97A8216E4020B5F2384075F27CB (void);
// 0x0000015C System.Byte[] WebSocketSharp.Net.HttpUtility::urlDecodeToBytes(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_urlDecodeToBytes_m152FA907D38C421CE121213DEE0021290189FABA (void);
// 0x0000015D System.Text.Encoding WebSocketSharp.Net.HttpUtility::GetEncoding(System.String)
extern void HttpUtility_GetEncoding_m7164375040554DDDCF9B8F573280B74770839C3D (void);
// 0x0000015E System.String WebSocketSharp.Net.HttpUtility::UrlDecode(System.String,System.Text.Encoding)
extern void HttpUtility_UrlDecode_m869E3E819A24CC7E63F37877F8B3B67DD808A366 (void);
// 0x0000015F System.Void WebSocketSharp.Net.WebHeaderCollection::.cctor()
extern void WebHeaderCollection__cctor_m7F5F2F4AF0E77F1513B674EDF9F3286D5E4E9E3A (void);
// 0x00000160 System.Void WebSocketSharp.Net.WebHeaderCollection::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection__ctor_mBD30634606B3E0C5581C176919441A5C70E2E5C2 (void);
// 0x00000161 System.Void WebSocketSharp.Net.WebHeaderCollection::.ctor()
extern void WebHeaderCollection__ctor_mF3554DEA14F15A042C08248524EE2C934E285EEA (void);
// 0x00000162 System.String[] WebSocketSharp.Net.WebHeaderCollection::get_AllKeys()
extern void WebHeaderCollection_get_AllKeys_mEEB0483D2A39258F29270F91479E32CEB6F8CB0B (void);
// 0x00000163 System.Int32 WebSocketSharp.Net.WebHeaderCollection::get_Count()
extern void WebHeaderCollection_get_Count_mF12E1BB47A01CE22FACB6629FE54E4E537D0E468 (void);
// 0x00000164 System.Void WebSocketSharp.Net.WebHeaderCollection::add(System.String,System.String,System.Boolean)
extern void WebHeaderCollection_add_m2A458F3478235350040A50962A5DD28DD120E083 (void);
// 0x00000165 System.Void WebSocketSharp.Net.WebHeaderCollection::addWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_addWithoutCheckingName_mD7FC644E907E3FF9CF89DD87120C5067A3F1334F (void);
// 0x00000166 System.Void WebSocketSharp.Net.WebHeaderCollection::addWithoutCheckingNameAndRestricted(System.String,System.String)
extern void WebHeaderCollection_addWithoutCheckingNameAndRestricted_m59C29554CEE81D55DD26C3FE3AF717F4321516F1 (void);
// 0x00000167 WebSocketSharp.Net.HttpHeaderType WebSocketSharp.Net.WebHeaderCollection::checkHeaderType(System.String)
extern void WebHeaderCollection_checkHeaderType_m549D159072B231119F7FCE14ACBCC3324915227F (void);
// 0x00000168 System.String WebSocketSharp.Net.WebHeaderCollection::checkName(System.String)
extern void WebHeaderCollection_checkName_m0BA750B6B40BEF096AFD65FE501056D5F4ECA84D (void);
// 0x00000169 System.Void WebSocketSharp.Net.WebHeaderCollection::checkRestricted(System.String)
extern void WebHeaderCollection_checkRestricted_m8D2C7656C96B253B7C9616F57ED4EE7E07599136 (void);
// 0x0000016A System.Void WebSocketSharp.Net.WebHeaderCollection::checkState(System.Boolean)
extern void WebHeaderCollection_checkState_m5285E869C1FCB5429EAF75B992B01ABF8AC040FD (void);
// 0x0000016B System.String WebSocketSharp.Net.WebHeaderCollection::checkValue(System.String)
extern void WebHeaderCollection_checkValue_m27735441E4C79AF162613B0AE5353BA4C504D9D8 (void);
// 0x0000016C System.Void WebSocketSharp.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean)
extern void WebHeaderCollection_doWithCheckingState_m64BECEFFF321FF38AE923D98CE7AC0D4D7C416BE (void);
// 0x0000016D System.Void WebSocketSharp.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean,System.Boolean)
extern void WebHeaderCollection_doWithCheckingState_m29A4D470E35F4B3071989DAB32399553EF768CC0 (void);
// 0x0000016E System.Void WebSocketSharp.Net.WebHeaderCollection::doWithoutCheckingName(System.Action`2<System.String,System.String>,System.String,System.String)
extern void WebHeaderCollection_doWithoutCheckingName_m343275E3DF0BE57A5E500BC446ABFEA35FCB4572 (void);
// 0x0000016F WebSocketSharp.Net.HttpHeaderInfo WebSocketSharp.Net.WebHeaderCollection::getHeaderInfo(System.String)
extern void WebHeaderCollection_getHeaderInfo_m85BD8FEC8A49329DE3A786D9FB488422F4820465 (void);
// 0x00000170 System.Boolean WebSocketSharp.Net.WebHeaderCollection::isMultiValue(System.String,System.Boolean)
extern void WebHeaderCollection_isMultiValue_mAD21CC968915649A77C11340A81028767719F50F (void);
// 0x00000171 System.Boolean WebSocketSharp.Net.WebHeaderCollection::isRestricted(System.String,System.Boolean)
extern void WebHeaderCollection_isRestricted_mA4F76FADAAD1CD9D9F13C00C39A0CE3C1775BC7C (void);
// 0x00000172 System.Void WebSocketSharp.Net.WebHeaderCollection::setWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_setWithoutCheckingName_m8CEF4BCC834132C6D69CEC74B67F0E043ED789D5 (void);
// 0x00000173 System.Void WebSocketSharp.Net.WebHeaderCollection::InternalSet(System.String,System.Boolean)
extern void WebHeaderCollection_InternalSet_m55178F78DA7524E1F06391C2B32B18EF1CA11F6F (void);
// 0x00000174 System.Void WebSocketSharp.Net.WebHeaderCollection::InternalSet(System.String,System.String,System.Boolean)
extern void WebHeaderCollection_InternalSet_m645009FCE0E3C0A2D92B184C28EA53F0ADCC1EF9 (void);
// 0x00000175 System.Void WebSocketSharp.Net.WebHeaderCollection::Add(System.String,System.String)
extern void WebHeaderCollection_Add_m4227AF3B019BA3D385877A7BE09ECC9C6FD0800F (void);
// 0x00000176 System.String WebSocketSharp.Net.WebHeaderCollection::Get(System.Int32)
extern void WebHeaderCollection_Get_mFC808BE8F96997B303A6FA0106EC33F85DCED50F (void);
// 0x00000177 System.String WebSocketSharp.Net.WebHeaderCollection::Get(System.String)
extern void WebHeaderCollection_Get_m829CC5DF3940056A5EDB81C8231A5DA742D40086 (void);
// 0x00000178 System.Collections.IEnumerator WebSocketSharp.Net.WebHeaderCollection::GetEnumerator()
extern void WebHeaderCollection_GetEnumerator_m1BD1204EADAAAEC1E4763F573046D57180DA3739 (void);
// 0x00000179 System.String WebSocketSharp.Net.WebHeaderCollection::GetKey(System.Int32)
extern void WebHeaderCollection_GetKey_m72EBD9D41DEA41C80CBE0AC46347DE1090D66FDD (void);
// 0x0000017A System.Void WebSocketSharp.Net.WebHeaderCollection::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_GetObjectData_m30ED8F454609C10A131CF87799C5A1E78E7C63FC (void);
// 0x0000017B System.Void WebSocketSharp.Net.WebHeaderCollection::OnDeserialization(System.Object)
extern void WebHeaderCollection_OnDeserialization_mE8C99A2E493E224E200E81DEF50A27AA2D96F879 (void);
// 0x0000017C System.Void WebSocketSharp.Net.WebHeaderCollection::Set(System.String,System.String)
extern void WebHeaderCollection_Set_m82EFDA57BF408CD92C9A2F6D9901FCE9295189F5 (void);
// 0x0000017D System.String WebSocketSharp.Net.WebHeaderCollection::ToString()
extern void WebHeaderCollection_ToString_mCE15157D7C4E8EF223FEDE8AAEDA6F7049D98F30 (void);
// 0x0000017E System.Void WebSocketSharp.Net.WebHeaderCollection::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m67E90D77EE028DCBA04A7E3F882AE9760E6DCC4F (void);
// 0x0000017F System.Void WebSocketSharp.Net.HttpVersion::.cctor()
extern void HttpVersion__cctor_mF0429FDFD24104804D1B1E1C5D7FAF90BD310202 (void);
// 0x00000180 System.Void WebSocketSharp.Net.HttpHeaderInfo::.ctor(System.String,WebSocketSharp.Net.HttpHeaderType)
extern void HttpHeaderInfo__ctor_mD83936EFA9C348AA23D99D4CE8CD6BFFF36BDAEA (void);
// 0x00000181 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsMultiValueInRequest()
extern void HttpHeaderInfo_get_IsMultiValueInRequest_m7A0939EA4CD71DFB1E97F7B20ED9AE35D36BBE68 (void);
// 0x00000182 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsMultiValueInResponse()
extern void HttpHeaderInfo_get_IsMultiValueInResponse_m0F19920E9C0B4E4A09C093D51162BF76D6E35CE6 (void);
// 0x00000183 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsRequest()
extern void HttpHeaderInfo_get_IsRequest_mC3E72496719DD2260BB49C0C927E4B0B6426B6B2 (void);
// 0x00000184 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsResponse()
extern void HttpHeaderInfo_get_IsResponse_m6EC358FC3EFD8B556646892DAD7FE7D4589B0A15 (void);
// 0x00000185 System.String WebSocketSharp.Net.HttpHeaderInfo::get_Name()
extern void HttpHeaderInfo_get_Name_mA9CDE7D1B3743812B7487A6F773C3AC794F562FF (void);
// 0x00000186 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::IsMultiValue(System.Boolean)
extern void HttpHeaderInfo_IsMultiValue_m2DB10C22E6688693BC5D2B488508244004F3D0A9 (void);
// 0x00000187 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::IsRestricted(System.Boolean)
extern void HttpHeaderInfo_IsRestricted_m3E270AFBF9D7D779F5C66B31B59C1E44206AEB94 (void);
// 0x00000188 System.Void WebSocketSharp.Net.NetworkCredential::.cctor()
extern void NetworkCredential__cctor_mD463E6A84BACE220DD7925D1903F80CB7C404CDA (void);
// 0x00000189 System.String WebSocketSharp.Net.NetworkCredential::get_Domain()
extern void NetworkCredential_get_Domain_m478AD8467DF28093C4D8BA58518D6313D20DFC84 (void);
// 0x0000018A System.String WebSocketSharp.Net.NetworkCredential::get_Password()
extern void NetworkCredential_get_Password_m4627B42EC58CD13466A5113ACB0A5D61070215D4 (void);
// 0x0000018B System.String WebSocketSharp.Net.NetworkCredential::get_Username()
extern void NetworkCredential_get_Username_m71DAB9442F134A56292F3F78B161996AFDAC5F4A (void);
// 0x0000018C System.Void WebSocketSharp.Net.AuthenticationChallenge::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationChallenge__ctor_m89D9496D058F9250332CB26A2DE2FC7EA2AF684C (void);
// 0x0000018D WebSocketSharp.Net.AuthenticationChallenge WebSocketSharp.Net.AuthenticationChallenge::Parse(System.String)
extern void AuthenticationChallenge_Parse_m80D1D40F0474E706FBE376A3218B9C30434388EC (void);
// 0x0000018E System.String WebSocketSharp.Net.AuthenticationChallenge::ToBasicString()
extern void AuthenticationChallenge_ToBasicString_m171FD34EF4823FBE4ED6C58A45E8A8AAF8DB3C20 (void);
// 0x0000018F System.String WebSocketSharp.Net.AuthenticationChallenge::ToDigestString()
extern void AuthenticationChallenge_ToDigestString_mD8D18DE457D4675E058C28AF5576BF4E12C85FF1 (void);
// 0x00000190 System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.NetworkCredential)
extern void AuthenticationResponse__ctor_mC87CEBB424AEC0340941AD1EA34B760E2AD20DE9 (void);
// 0x00000191 System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.AuthenticationChallenge,WebSocketSharp.Net.NetworkCredential,System.UInt32)
extern void AuthenticationResponse__ctor_m5992FBF35171438C2E12BCACE2F28A341D38E1F4 (void);
// 0x00000192 System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection,WebSocketSharp.Net.NetworkCredential,System.UInt32)
extern void AuthenticationResponse__ctor_mD5B6147F1CDD6345F2EA3A78ACF9F2DD1EE36E82 (void);
// 0x00000193 System.UInt32 WebSocketSharp.Net.AuthenticationResponse::get_NonceCount()
extern void AuthenticationResponse_get_NonceCount_m94B4F34DF305DD982CDBEA058F2286A5E7558A9E (void);
// 0x00000194 System.String WebSocketSharp.Net.AuthenticationResponse::createA1(System.String,System.String,System.String)
extern void AuthenticationResponse_createA1_mB340A76DFB3F9BE2E916FD777DA59479B53D5F57 (void);
// 0x00000195 System.String WebSocketSharp.Net.AuthenticationResponse::createA1(System.String,System.String,System.String,System.String,System.String)
extern void AuthenticationResponse_createA1_m12719D7CA5E45860F11689E3997BAEE7EB74B575 (void);
// 0x00000196 System.String WebSocketSharp.Net.AuthenticationResponse::createA2(System.String,System.String)
extern void AuthenticationResponse_createA2_m54FC19F0852B8D5340AA3BC0AEB272633715D545 (void);
// 0x00000197 System.String WebSocketSharp.Net.AuthenticationResponse::createA2(System.String,System.String,System.String)
extern void AuthenticationResponse_createA2_mBE60DFC67FE7D99F173739CBFECBF50DA52FBE45 (void);
// 0x00000198 System.String WebSocketSharp.Net.AuthenticationResponse::hash(System.String)
extern void AuthenticationResponse_hash_m1706B5FA588B17BB60E127D0384DA79F157A8646 (void);
// 0x00000199 System.Void WebSocketSharp.Net.AuthenticationResponse::initAsDigest()
extern void AuthenticationResponse_initAsDigest_mEE65254A0F5EFAB5F105F4CA504DDA1A869E4AC6 (void);
// 0x0000019A System.String WebSocketSharp.Net.AuthenticationResponse::CreateRequestDigest(System.Collections.Specialized.NameValueCollection)
extern void AuthenticationResponse_CreateRequestDigest_m514F7A97B6D1160381D934A8EB6C869A405BA9D0 (void);
// 0x0000019B System.String WebSocketSharp.Net.AuthenticationResponse::ToBasicString()
extern void AuthenticationResponse_ToBasicString_mD959029EB3ED6FC588FE7FF14E15F3C22278BDD8 (void);
// 0x0000019C System.String WebSocketSharp.Net.AuthenticationResponse::ToDigestString()
extern void AuthenticationResponse_ToDigestString_m3D97A1F359A3A4883861CD4E6913F431B3625699 (void);
// 0x0000019D System.Void WebSocketSharp.Net.AuthenticationResponse/<>c::.cctor()
extern void U3CU3Ec__cctor_m46946CE0A1B74C21BC4C6F94827610A1DC4D0C77 (void);
// 0x0000019E System.Void WebSocketSharp.Net.AuthenticationResponse/<>c::.ctor()
extern void U3CU3Ec__ctor_m1A105A1AE604BFD24F03A5A1032FBD988C5C1481 (void);
// 0x0000019F System.Boolean WebSocketSharp.Net.AuthenticationResponse/<>c::<initAsDigest>b__24_0(System.String)
extern void U3CU3Ec_U3CinitAsDigestU3Eb__24_0_m9E735DA74FC654BEFED1E89CF3D71491D16FA4E0 (void);
// 0x000001A0 System.Void WebSocketSharp.Net.AuthenticationBase::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationBase__ctor_m510663A827778FF82B5AEECC3A405FD915FB1593 (void);
// 0x000001A1 WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.AuthenticationBase::get_Scheme()
extern void AuthenticationBase_get_Scheme_m8645EAA4B670F4E75F36C6CE750A9881FF3128B8 (void);
// 0x000001A2 System.String WebSocketSharp.Net.AuthenticationBase::CreateNonceValue()
extern void AuthenticationBase_CreateNonceValue_m11464AEB8E3CB43BFE2FB13A789B0937A657E9E4 (void);
// 0x000001A3 System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.AuthenticationBase::ParseParameters(System.String)
extern void AuthenticationBase_ParseParameters_m0232933D4FA6CBD78747BB3BCD535441B4EC1CCD (void);
// 0x000001A4 System.String WebSocketSharp.Net.AuthenticationBase::ToBasicString()
// 0x000001A5 System.String WebSocketSharp.Net.AuthenticationBase::ToDigestString()
// 0x000001A6 System.String WebSocketSharp.Net.AuthenticationBase::ToString()
extern void AuthenticationBase_ToString_m8AF7365991E64FEB697391182BC5607520FB78BF (void);
// 0x000001A7 System.Void WebSocketSharp.Net.ClientSslConfiguration::.ctor(System.String)
extern void ClientSslConfiguration__ctor_mF5EF97BB2DF00E97F0AB2F8FE2AD14274677A002 (void);
// 0x000001A8 System.Boolean WebSocketSharp.Net.ClientSslConfiguration::get_CheckCertificateRevocation()
extern void ClientSslConfiguration_get_CheckCertificateRevocation_mE359C515CAEA3A24D01D1F0B8E4DEBD098F5B283 (void);
// 0x000001A9 System.Security.Cryptography.X509Certificates.X509CertificateCollection WebSocketSharp.Net.ClientSslConfiguration::get_ClientCertificates()
extern void ClientSslConfiguration_get_ClientCertificates_m3181C612ACFEDC8563EA850F142571E7642CF186 (void);
// 0x000001AA System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.ClientSslConfiguration::get_ClientCertificateSelectionCallback()
extern void ClientSslConfiguration_get_ClientCertificateSelectionCallback_m5C0B2E0916F167F45D3AB856DB813186F6DA3F06 (void);
// 0x000001AB System.Security.Authentication.SslProtocols WebSocketSharp.Net.ClientSslConfiguration::get_EnabledSslProtocols()
extern void ClientSslConfiguration_get_EnabledSslProtocols_mD29188123A9C7173FFDF8FA65CB8705208FAFA31 (void);
// 0x000001AC System.Void WebSocketSharp.Net.ClientSslConfiguration::set_EnabledSslProtocols(System.Security.Authentication.SslProtocols)
extern void ClientSslConfiguration_set_EnabledSslProtocols_m9BCD39B5286C2D891F7AB5B667CC90D0456708CE (void);
// 0x000001AD System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.ClientSslConfiguration::get_ServerCertificateValidationCallback()
extern void ClientSslConfiguration_get_ServerCertificateValidationCallback_mFE5B87C6DFEA175F7E28624DE660EAC38E049A84 (void);
// 0x000001AE System.String WebSocketSharp.Net.ClientSslConfiguration::get_TargetHost()
extern void ClientSslConfiguration_get_TargetHost_m298AD821C4F8A0B9ACE825F9CCD511D713A2D14A (void);
// 0x000001AF System.Security.Cryptography.X509Certificates.X509Certificate WebSocketSharp.Net.ClientSslConfiguration::defaultSelectClientCertificate(System.Object,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern void ClientSslConfiguration_defaultSelectClientCertificate_mE7530A99129D711B5C87CDD733A3E8DE2CBBE943 (void);
// 0x000001B0 System.Boolean WebSocketSharp.Net.ClientSslConfiguration::defaultValidateServerCertificate(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void ClientSslConfiguration_defaultValidateServerCertificate_m833CBCFAD7450ED4D72691AEA244AF16FE04C11E (void);
static Il2CppMethodPointer s_methodPointers[432] = 
{
	Ext_compress_m4358278284A560842E882535CAEE02997945C3DA,
	Ext_decompress_mDE27860D3912BD63403E7BF996F153F59FD92D3D,
	Ext_decompress_m2F24DFAF850F9B35C3FD4E0A1A3E7FF6C9C5AF14,
	Ext_decompressToArray_mEB8B011B30C5A46E00DA7F87520D6019B8242A11,
	Ext_Append_m0E57723715B401B8746CE5C9749899C97D164541,
	Ext_Compress_mE2A46ECF97EAAEBF6113AE9E870D50CAC040AB6F,
	Ext_Contains_mF267D8AEC1CA95308565F0B4A52350ABAF786A2E,
	Ext_Contains_m17E1757D14770EB91B608CE2815E7BA960DA2313,
	NULL,
	Ext_ContainsTwice_m4DD33BCD410B1D870759892F8B6D7D77AD927BD2,
	Ext_CopyTo_mB436FB49B8616F60E26EFEB3C6F0D723A4189FBA,
	Ext_Decompress_m5051854DFB667F2D5FC17DE828439EB1A67F9D00,
	Ext_DecompressToArray_mAA1406B8850D2404435700563124565AEBBE8F03,
	Ext_Emit_m53CC51B43FBF1FC6AED0C878BE0DC02D26EF220B,
	NULL,
	Ext_EqualsWith_mB93057EE05D870A4CFA31EA72B15A632D54EE6E5,
	Ext_GetAbsolutePath_m719787C61869F48F573EE93861A56430E98AEB17,
	Ext_GetCookies_mE81477DBE254503ADF2F5B3173D4D96EEC7D4E4A,
	Ext_GetMessage_m53797B74D6828C7E9D1B91D575F6159AD431C03E,
	Ext_GetUTF8EncodedBytes_mB9035E3F22A387B3717298A96D687EDC5AFB71B2,
	Ext_GetValue_mAF45AD216AC35C5D1BA224BEAAD6A6FF83DDE044,
	Ext_InternalToByteArray_m3CCD4BFE6D64214EDB0D5C07514DDBD636A2303F,
	Ext_InternalToByteArray_mD8ECE713D31534241B3A20C070AECD1275D2D2B6,
	Ext_IsCompressionExtension_m2D95C896B134964E2BCC94AF1FD73399888B03E9,
	Ext_IsControl_mD6C47A8942EC85383A1FA21D750B3420A3A600E2,
	Ext_IsData_mD1FDD951BEAC505088F40E7E96A4EC37DAA1B921,
	Ext_IsData_m0D314D4CE1CB0481957905FA9EF5DD8E08B33A0D,
	Ext_IsReserved_mB14EE3EC1D539A9C9C35652B3BE120E97A5F96EB,
	Ext_IsSupported_m274DFE67596E1C20FE078D43462675E1471E0BC2,
	Ext_IsText_m4BF1ADE56AD743FBE48AD8F85DC2586F980D2164,
	Ext_IsToken_m181B2CFD2601FFEADB4BCCE758E0DAD9F158C2DE,
	Ext_ReadBytes_m46DBC394CDA9BD532F01E92390A9DACBF2B92C2D,
	Ext_ReadBytes_m0F3560D4CFEC3F4E2F01A6AB4F8CF19BD301BDD3,
	Ext_ReadBytesAsync_m16D0BD06630A2B20F3A59700665CA0A5328E736F,
	Ext_ReadBytesAsync_m4A84EB6C86EDB4A46262CF97D20258137A7A5FCF,
	NULL,
	Ext_SplitHeaderValue_m01EE9C6D8102DB88FA9937B7EC8AF3CF4004B67A,
	Ext_ToByteArray_mD20CDE09F09523A7A54F5489D9A42326FDCC561D,
	Ext_ToExtensionString_m1A4430E0A6CD59742837AB50919C7008230C0E39,
	NULL,
	Ext_ToUInt16_mF963B0EB55BD1C1B24DF8FCB77070EB8AE1908CB,
	Ext_ToUInt64_m8875AD2574EA9F437EB6499A833BBB87A8A07CB4,
	Ext_TryCreateWebSocketUri_m3944CC7FC2F49F59DE819B4E18877FE84C9CB42A,
	Ext_TryGetUTF8DecodedString_m07ED759625CB1683806215FDC4EA050F3F41E062,
	Ext_TryGetUTF8EncodedBytes_m9CA6A8F40D22157CE7096C2C0C90EEAF35D18CB5,
	Ext_Unquote_m430FB83166985AC03BF2BD8283CAEDBE21F66602,
	Ext_Upgrades_m672B32A90886DEF7430195058BDD9B85E0F955F9,
	Ext_WriteBytes_mC92631F017F5C1290CB883FBAECE35156B2ED308,
	Ext_IsEnclosedIn_mC4AD7A067E1070CDD25235847375ECA14647D8F3,
	Ext_IsHostOrder_m82CC168E47DC97D8E61D971E684A97AC7F748AF2,
	Ext_IsNullOrEmpty_mEB4CFD1426630064F5435FADB81B48B26161793E,
	Ext_IsPredefinedScheme_m274F02E1D5677ED6112F71F299C6562BA2AFD5C0,
	Ext_MaybeUri_m9183FD39642FB589BE7AE769C8146A6A19386B11,
	NULL,
	NULL,
	Ext_ToHostOrder_mFC9D0D1F9DC21733829F38C35C3B819DD90A9CBC,
	NULL,
	Ext_ToUri_m36BFA928AEF6A14DB081BFEF75143B68665F2F88,
	Ext__cctor_mA75769ED37738922CD747FC3EDEB0697C14008FE,
	U3CU3Ec__DisplayClass21_0__ctor_m2BCF3D122CDD2BFF8754141B5B36329285CC24AB,
	U3CU3Ec__DisplayClass21_0_U3CContainsTwiceU3Eb__0_m655E85AD466C26675FAC6CAC1759DD9D6E5371B1,
	U3CU3Ec__DisplayClass59_0__ctor_m5C7BD90966D0F14250DDFA0EBC0538A546211E6C,
	U3CU3Ec__DisplayClass59_0_U3CReadBytesAsyncU3Eb__0_mE5DD53DD07EEC4B0FD280F7DB4E4B548B49837A6,
	U3CU3Ec__DisplayClass60_0__ctor_mB58148A9B0132C7D1222C7D91D40B89364793DA4,
	U3CU3Ec__DisplayClass60_0_U3CReadBytesAsyncU3Eb__0_m268BD3476574A3CEDFD50C6F3056A296820614F1,
	U3CU3Ec__DisplayClass60_1__ctor_mE471A74C1292C57D6782C110B0137F6334AD92F4,
	U3CU3Ec__DisplayClass60_1_U3CReadBytesAsyncU3Eb__1_m3015741A1DCC71F30AAC0D7B64CCED91519EDE6D,
	U3CSplitHeaderValueU3Ed__62__ctor_mD0F6C1B1B13A73C577FF6F04AA2EF11BF7C895F4,
	U3CSplitHeaderValueU3Ed__62_System_IDisposable_Dispose_m13B0D952747EF8A3E7E734C9FE4A9B6E4E7ABB05,
	U3CSplitHeaderValueU3Ed__62_MoveNext_m02893FAFFAEB3E74DB5CC21852617C485DD92765,
	U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m303D4DF29F9239C771BA12712ABC5AE8D4CBE13C,
	U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_Reset_m48C8B244C0B1648685FBD53A216EE5004BD258D3,
	U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_get_Current_mFB4110BE04322EC29F00261128176708E13C50A0,
	U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m20D4ECB95C3C084914B659CF5E262D913718F626,
	U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerable_GetEnumerator_mA5D81BE65336ED51944619BE3CAC8FA0FF695C95,
	MessageEventArgs__ctor_m2A1208F060211AE5B20D123ADB4817BC6CAECFEB,
	MessageEventArgs__ctor_m7EEA211A3002A410FFD41650FFAD121E75A4446D,
	MessageEventArgs_get_Data_m5A505CFE600EE15DC9041713B8C568D71C5CBB63,
	MessageEventArgs_setData_m84E444D41FEF301DC7D1400933073D8421E10220,
	CloseEventArgs__ctor_m2C6580973BCDEB0F0E4E8F2643C092423F4890C9,
	ErrorEventArgs__ctor_m049E3481288E6F50084D27EFA0431556A1E713B7,
	WebSocket__cctor_mBF1E757B75242B4625491BFEA5889B9ED7119F72,
	WebSocket__ctor_m922DA968DBD07A0529B3BE9CE576188FD0B5C094,
	WebSocket_get_HasMessage_m1FB96B036F1E3247BB2B598A6C41A1D525E2662B,
	WebSocket_get_ReadyState_m8E4A7508979AB89D1326A902751E9EA57B603F85,
	WebSocket_get_SslConfiguration_m9396292A9B1EF24F6B8604D19EEAD4F2E4904C2D,
	WebSocket_add_OnClose_mA8DE5F76FD7433CDAD33CC2D8812D7001B750A66,
	WebSocket_remove_OnClose_m2AE23DB4E743F675A549A0E71E3558FF7A2F3DBB,
	WebSocket_add_OnError_m362EE37535D15C3B900BAD60249F10707EA6729E,
	WebSocket_remove_OnError_mD2F241B0D17251C16DF1A40E533E5F20AB19E651,
	WebSocket_add_OnMessage_mFB8C1D5D98E13FCCB1653ABE35704A532B5988A6,
	WebSocket_remove_OnMessage_m455B56B3B9360D764DD5A5BB635ECAFF6EB54D07,
	WebSocket_add_OnOpen_m40F1299BBE18CDD3880D5111CAFD058C2D02A874,
	WebSocket_remove_OnOpen_m30290C709FC3C8F8DDC36076F97F87E04D7DDD2C,
	WebSocket_checkHandshakeResponse_mF3BFD88D355C08B9D5464855C0ADE1E79FA57E94,
	WebSocket_checkProtocols_mA1A5E30205CA5BC405EDE62C9CC4AF9ACB6927BC,
	WebSocket_checkReceivedFrame_mFCB84F26393F34C80D8789999C916BB35D273228,
	WebSocket_close_m02BDB543E56B74784B2ABF8D680C2739F2115D6F,
	WebSocket_close_m6B70D8C6C1E11AE19931E59FC4AE825CE05D248A,
	WebSocket_closeHandshake_mB682AF7AB000E1F290047D6E51C23613A3FAAE81,
	WebSocket_connect_mB380C23832997519BAEBAB8E022041B11D2DCD64,
	WebSocket_createExtensions_m1F574ACD030DA089522EDCB7B2DEFAB409F4C9E5,
	WebSocket_createHandshakeRequest_mB333CABDE3F80FAC18C182B9A042FD9A17752ED0,
	WebSocket_doHandshake_mBCA0FD27DAFED7A4D58F7F3C4BF29E3E44200F12,
	WebSocket_enqueueToMessageEventQueue_m2092C8DD9DF898800BD52118FBABD5095D8D2B74,
	WebSocket_error_m1B3A2F204039C8098B4189C783372BAC426DCA97,
	WebSocket_fatal_mAF3B8099763885426D121C75F87AA439AF9C389D,
	WebSocket_fatal_m1871F1D7F8439260ECDD9244DA4E91408219E88E,
	WebSocket_fatal_m5F3908C89DD054C87FA53194D3872B27383AD12B,
	WebSocket_getSslConfiguration_mC57CE4497AC04BD614175EECD3B28731230A10B4,
	WebSocket_init_m5AAB6FF2AE5BD979307FD5C6536A17799A2A4284,
	WebSocket_message_mB69559458AC1A592384668C35066F239FB2DFE11,
	WebSocket_messagec_mBB0B13944C5415C535CFB8897FA307325718C176,
	WebSocket_open_mD227A699DB8632471E0FF473990D53FCA115F37A,
	WebSocket_processCloseFrame_m341AA7E1ADA14D05A71CA219C9196FA2A3DE4AB1,
	WebSocket_processCookies_mCE65E3EBB7BD838AF1DFB09A9F3BD7FE41BA2052,
	WebSocket_processDataFrame_m38651FE664375395C21A4C006243C6C6893329B8,
	WebSocket_processFragmentFrame_m801AF044A538EE6FB7C0DA41F0B9B13A7D359A19,
	WebSocket_processPingFrame_m982A9E14B86942D6D731817259E582AF387E876D,
	WebSocket_processPongFrame_m3FB60ECF4A021FA52CB1F32311FC059FEE2D2F12,
	WebSocket_processReceivedFrame_m6BA2094096E2CC13C5315E1AE61175B194330C5A,
	WebSocket_processSecWebSocketExtensionsServerHeader_m09B3AA86D61AB2E4FDCA2E8C6B9B9F9119F66B56,
	WebSocket_processUnsupportedFrame_m69F5B0C2AFF2EF098E2F6EC5112F5BA87E828D34,
	WebSocket_releaseClientResources_mD26F73F4794E873D4E1BFA93E69CB46A7F50BAF0,
	WebSocket_releaseCommonResources_m8253F95D7E5DA221D21A6F81080FA294B975C232,
	WebSocket_releaseResources_mED7D3CE825EBA68EEC52FC579698DF0E8FF2C94C,
	WebSocket_releaseServerResources_m42383848291B6E3B4A2510D504BEFC63E5B3B8F1,
	WebSocket_send_m15FEBC3E51D73974E8B02498255420B14B102713,
	WebSocket_send_m31A66C1F2D1EF9CDAF7566434F12DDC9D31475BB,
	WebSocket_send_m19807526833D3940322EE9A13A3394FA12EA78B8,
	WebSocket_sendBytes_m662D431639A507188471C969D2A014BC2E2B6820,
	WebSocket_sendHandshakeRequest_m4CC45304F37D568233914845F32FD9C1184C9C1A,
	WebSocket_sendHttpRequest_mBCC35A44A07989A813876A6264F23C300A08BF74,
	WebSocket_sendProxyConnectRequest_m54F557B0AEE314593FD3B5409C4D227E84F47029,
	WebSocket_setClientStream_m9B787B1BC3FE35905856C5F48AE3152F0DE7E2D8,
	WebSocket_startReceiving_m3260E40714599330ECBF6F0E8C6E31E8FBD924CD,
	WebSocket_validateSecWebSocketAcceptHeader_m199824AF1843A047AA7C210193F2C74AD053F9A1,
	WebSocket_validateSecWebSocketExtensionsServerHeader_m6435E0E6DA97D34997C84B73038B1920C8CC00E5,
	WebSocket_validateSecWebSocketProtocolServerHeader_mE24A209409042EF8B15016660E64209B2FA74386,
	WebSocket_validateSecWebSocketVersionServerHeader_m811EBF818F5C8DB13745DF989056F3B34C286714,
	WebSocket_CreateBase64Key_mD771D577EB2DE12E0BF1C6E6041DC641484A3983,
	WebSocket_CreateResponseKey_m8511A3E52E5FBD56856E3E545ACFEBDBF123F831,
	WebSocket_Close_mFF036D51A1F0FEFB339A36101B224E30964E6953,
	WebSocket_Connect_mCA3D7ADB803DD8C421DC8EF5839B17BEDDB2210D,
	WebSocket_Send_m8CB5F072D94C6E294E9EE0D6BCBCD0140338CB3A,
	WebSocket_System_IDisposable_Dispose_m63D2EDDDD3758747F97FAB12531149D701212669,
	WebSocket_U3CopenU3Eb__146_0_mD7B092980016BCF1CBBC93B23FDD9B18AC9CA2F6,
	U3CU3Ec__cctor_mFF8A5C9CF05BF8A4829822D615AA786EB2C8299F,
	U3CU3Ec__ctor_m117570B5819BC791785761A957D9253FCE5A7A30,
	U3CU3Ec_U3CcheckProtocolsU3Eb__120_0_mFC7B8401775DFBA3D0E66F467E4424EA533CAF15,
	U3CU3Ec__DisplayClass174_0__ctor_mE36EF473E335D521D4B0DB27B7EE0894C511D4E1,
	U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__0_m8E24D6D9AE0C588A287C43371FC99377E42CA707,
	U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__1_mACC3030A2A69A3DC6335C77AF7C26CD579AE8673,
	U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__2_mC9E14CB1C0A921640461335B5AF22B6266DEE7AF,
	U3CU3Ec__DisplayClass176_0__ctor_m684BBE2A9281D5413141C477D916D65FEEA0C8A4,
	U3CU3Ec__DisplayClass176_0_U3CvalidateSecWebSocketExtensionsServerHeaderU3Eb__0_m093F9B11367CA89CBDD22D7CBA6C0A8C96857719,
	U3CU3Ec__DisplayClass177_0__ctor_mEBAC1947E62BA2E35EB67691BE4F0D6910B152DA,
	U3CU3Ec__DisplayClass177_0_U3CvalidateSecWebSocketProtocolServerHeaderU3Eb__0_mB7F235C4E423B82FA02CF32AADD2756B21BFCFD0,
	PayloadData__cctor_m8EADEC6AE38490AED2274302C79C4797F43722DC,
	PayloadData__ctor_m658CBD26A46F9ACDDCB50C43B53CE3C85D3E0703,
	PayloadData__ctor_m43C0C23A7B7E55573669050B1CEB6A7FCB806F21,
	PayloadData__ctor_m63AD7348411A3B5965AA4E72057D322CE1AE9EBC,
	PayloadData_get_Code_m6F55D55E77C2567FB1749EE0E35CD45585C7CA58,
	PayloadData_get_HasReservedCode_mA2A90C33F1088C1E2A20A1FB6715EE19EE670900,
	PayloadData_get_ApplicationData_m8FFE92425A32E613CFBA980B9B49DBFD26D3FBD4,
	PayloadData_get_Length_mF27008252337038FAD3FE1FC88B810A4B4E51EC0,
	PayloadData_Mask_m523717F5A2C66E440B31AFC386485DC99BC856AC,
	PayloadData_GetEnumerator_m22C7B4773174B70E5FB2D51B33AFEECF2F67D682,
	PayloadData_ToArray_m480821B617EFC7F278807C27C7732EFA9EAAFB50,
	PayloadData_ToString_m7CAA9743A55C65714202109CDDD9BDCD5B0EB328,
	PayloadData_System_Collections_IEnumerable_GetEnumerator_m579797823AA7E5C219CD945047B717A634D4231A,
	U3CGetEnumeratorU3Ed__25__ctor_m7290C117BBBD824041032B6A783AE4AF32226A43,
	U3CGetEnumeratorU3Ed__25_System_IDisposable_Dispose_mFDCDAD41B87D1F55DAE4A68FE3767B22A28C5F54,
	U3CGetEnumeratorU3Ed__25_MoveNext_m5BBA0C78B71EEC97413990316AD2D16A9662923D,
	U3CGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mA7B801C8652EAE220C45112BB0883B24421B41B1,
	U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m413CE860B0CE2F51C4F2E31AB83F860327B58A54,
	U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m2CCE50DF3B959BC0B0D59F873D0DB781453FE120,
	WebSocketException__ctor_m33E614CB48AE162856CF66B43CFA3820B36E885D,
	WebSocketException__ctor_m8EBC29CC6D95F272F73D6B224BB63A55B8F5F26E,
	WebSocketException__ctor_m0C5006A2D887D58AFBE107A79DEAE88E94AC03E0,
	WebSocketException__ctor_m6F648D792B0BEE233E71334EF5630D5DF9CDB38A,
	WebSocketException__ctor_m1CF13CD49D70141B24D98DE5B3F40E50673876FD,
	WebSocketException__ctor_mC0314C41BD7A079315E298C6F276A9BFFDC357F1,
	WebSocketException_get_Code_m6303EF200458C42155DC8FC3F5CE54D032F7EB2A,
	LogData__ctor_mA4E490BB4AD085273333A0671EA9BDDA6AD65E99,
	LogData_ToString_mB912103F53201C69F5C7E3026E3AB9BFBB08CBEF,
	Logger__ctor_m56612F787878F92C39B39A98D73C0C2A6E92449D,
	Logger__ctor_m879D37E6E08DA95C7543A012E562FC852320F614,
	Logger_defaultOutput_m638580E8604793C81D9995F218C501BBB846C4B8,
	Logger_output_m43C911A929E5AD6BBEAA34130FDABD747301EC2D,
	Logger_writeToFile_m45A3CA06576664B6B7F566AFBE3E52350D32E193,
	Logger_Debug_mEB54E96D44B38DB015BD3672FBF7C1AAA10D97CF,
	Logger_Error_mC7AB02F8B01230993938A6712BC95F7A84CCF9D4,
	Logger_Fatal_m3CF912E03127C2731E44B2A5C85C95B48459738E,
	Logger_Info_m2A8010FF066CEEFA02AB38DE4F70A875AB9BFF8C,
	Logger_Trace_mDE773047E49A9B4D519A5C6FDF27AC919283A4D0,
	Logger_Warn_mF60C556ADCEF121DBACF0BCD00F7E70D54383860,
	WebSocketFrame__cctor_m7B4EA50590BBFF37373A9D14260E7CD1A5C29915,
	WebSocketFrame__ctor_mF4DCB29F2C9613E8B2D2080FD4CC593E08898A61,
	WebSocketFrame__ctor_mDB53820703CFD503AA14DFE9712CBF94CA726001,
	WebSocketFrame__ctor_mD648878987D949C7E722360DCB0FA9D39B40F428,
	WebSocketFrame_get_ExactPayloadLength_mF73F2DE0850DEA5E5BFCE20BC67A2A335E15E70E,
	WebSocketFrame_get_ExtendedPayloadLengthWidth_m8E2942AA9E36E857FB96109EB0B4660559B6E0F1,
	WebSocketFrame_get_IsClose_mAA4D2AE49427CD9738D8AC66301180E1C6E85F9F,
	WebSocketFrame_get_IsCompressed_m14A768A52A2DA5B4C1AD969775FC33C7B73B2F19,
	WebSocketFrame_get_IsContinuation_m777358323A7402D1511D59BA48D7DA4D8C578C87,
	WebSocketFrame_get_IsData_m30E6330D07BD119F57738F9F27E91EC1C9D73B07,
	WebSocketFrame_get_IsFinal_m975B1AE82A1375D08440E08A34BC1B5974ACAF99,
	WebSocketFrame_get_IsFragment_m08B7D5166D52EA3C48472D1161B3792887543B50,
	WebSocketFrame_get_IsMasked_mAB2DED2695BD710FF78A9D96096BF93E26F59BDB,
	WebSocketFrame_get_IsPing_m17308380EFBF26833097A3C8DC7E7F71463C88C0,
	WebSocketFrame_get_IsPong_mDF3FBB942DFDE9C0FEFAE6118DD3F3D40550BBCD,
	WebSocketFrame_get_IsText_mF6A84F510563FB5659789AC46CDD806DA06E0CF2,
	WebSocketFrame_get_Length_m28FB305CEDD5F042715588559DF45C75190510CC,
	WebSocketFrame_get_Opcode_mECC7B1D5880DD55E7AB62364288408700D46123A,
	WebSocketFrame_get_PayloadData_mE6B1D54490BE8DD45F4475565374C4E6DBE93967,
	WebSocketFrame_get_Rsv2_m13644B12282AC45E5F04F80CE7230FA20BE52353,
	WebSocketFrame_get_Rsv3_m3C0A8663FDD30A5AB46680A7ED378B97D6B4F117,
	WebSocketFrame_createMaskingKey_mE03F37782469DBD5A70A9C5C63B2F27670C316A4,
	WebSocketFrame_dump_mAC4242054E54C3BFC69F1C9E3CA66A6FE6610540,
	WebSocketFrame_print_m8C858FB381EA463C3979D70174B6D7DD62562A3D,
	WebSocketFrame_processHeader_mE573FD7E935A31249EECFBBF75199C364B3A854F,
	WebSocketFrame_readExtendedPayloadLengthAsync_m922290C927B427EAF1EC185BEE879BF7329D3FC6,
	WebSocketFrame_readHeaderAsync_m4F195087FD1EC50D4D092400BF7838BE6B29E8F7,
	WebSocketFrame_readMaskingKeyAsync_m5F73FEA0B005B1C021A9D0F14D3783EBE3E38EF6,
	WebSocketFrame_readPayloadDataAsync_m3965810AA59F5336E29916F110553F00031AB76D,
	WebSocketFrame_utf8Decode_m72D693503AA6265E4BCEDD39F46B192EF3A330F6,
	WebSocketFrame_CreateCloseFrame_mD637E8215EA93D3FA0BF7AD000F9FE1BBDAD8731,
	WebSocketFrame_CreatePingFrame_m7FB996F641AC3EE368827D689C9BD68D7FCED0EF,
	WebSocketFrame_CreatePongFrame_mBB4043327342566AE21805B02ED973108E1B57D1,
	WebSocketFrame_ReadFrameAsync_m9C0E785D48AB8DA1CE13C5728D775F80BFAC83FB,
	WebSocketFrame_Unmask_m5C7F2FE3B40645E6A10BB581DD0AC0B265AFE172,
	WebSocketFrame_GetEnumerator_mEF67D9D83EEAE0CECBE4963682C9B6FA5154DE7E,
	WebSocketFrame_PrintToString_m4DAE2A73154CBF92CED95FB61199EA250C0C04CC,
	WebSocketFrame_ToArray_m217A9E7E238584910BA05C707C45A237A6A5229B,
	WebSocketFrame_ToString_m99235962DF7944D08AA6E82215097C920ED79DF4,
	WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_m487A358E47EAD2D7334216DFD26DD6FED191BDCC,
	U3CU3Ec__DisplayClass67_0__ctor_mE6E1A585A035A12F4EAE82416222E607953A62F3,
	U3CU3Ec__DisplayClass67_0_U3CdumpU3Eb__0_m9768D92D3135F3085BCC4360EEB73207B21EF8B6,
	U3CU3Ec__DisplayClass67_1__ctor_m22AB30B37FD6134D7508AE21B40C83841E03ADD9,
	U3CU3Ec__DisplayClass67_1_U3CdumpU3Eb__1_m8A3B62265AC42FF04D9EA593BCBC90ACD8E9D7B8,
	U3CU3Ec__DisplayClass71_0__ctor_m93F995D1AB05D60F418ED2988D4D8A692D255B46,
	U3CU3Ec__DisplayClass71_0_U3CreadExtendedPayloadLengthAsyncU3Eb__0_m3DC816E2F4CD686296CA4B35676A6A75D6CA5264,
	U3CU3Ec__DisplayClass73_0__ctor_mF299AFA95D2D5898364D62DDAAA84D8F98BDE4D6,
	U3CU3Ec__DisplayClass73_0_U3CreadHeaderAsyncU3Eb__0_m18193F118E9CC413CB727A0CB34FAF56A6A30049,
	U3CU3Ec__DisplayClass75_0__ctor_mF0CC56940CC5BA9FB42CB68237BD70F33F4A7D48,
	U3CU3Ec__DisplayClass75_0_U3CreadMaskingKeyAsyncU3Eb__0_mBB2B7AAD10A5CABD0AE45EBDE22DF484FF6C8C51,
	U3CU3Ec__DisplayClass77_0__ctor_m21DDF022F43D7DD3C46CC55253138F5A72160A0C,
	U3CU3Ec__DisplayClass77_0_U3CreadPayloadDataAsyncU3Eb__0_mBEF25F96FBBEEB1D01DAB37A807F1CCFBE778A5B,
	U3CU3Ec__DisplayClass84_0__ctor_m251DFE8F5FF2FA6B0AC3C1F60FB6131B76C39851,
	U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__0_m2F441FFFCC21CF9D2AFA35D22C6172AF5A92DE0D,
	U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__1_m406798DAF939740176942C2A9726FEC2C05CFE60,
	U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__2_mD6B77492AEBDF5D7D780A2FFB03CE4961F9D1FA3,
	U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__3_mB95223F0107617D006EA1CCE46F6E8ACD90B3326,
	U3CGetEnumeratorU3Ed__86__ctor_mFDDD212B6E6A6FFAB4FD0CA6D9541929DE36E06A,
	U3CGetEnumeratorU3Ed__86_System_IDisposable_Dispose_mDA8B0BA2DB9C194B5932895D77173C70CF63D01D,
	U3CGetEnumeratorU3Ed__86_MoveNext_m997E2491C32856DE993A6B2D9ABFC425831B0DC0,
	U3CGetEnumeratorU3Ed__86_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m3EA1B7BF4D4B7576E0EFB264B5919542FB74CD33,
	U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_Reset_m79C1B3397472E5979B328686C1169427742B14B4,
	U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_get_Current_mC579C33994AC27CEEAD74CEC09B3D6F517560A20,
	HttpBase__ctor_mF1E1C4A447ACA2BC996D4D90151192B843C18102,
	HttpBase_get_EntityBody_m3468AC17BE0108BED7DC6517AC98525F6FC2DD33,
	HttpBase_get_Headers_m2C4961F0604F56C8C06511D5ECB7E4A05F522299,
	HttpBase_get_ProtocolVersion_m505377392BBA51CD8CB114D624A293AED38FDCC6,
	HttpBase_readEntityBody_m867C03EFBD465E67E553F743CE4664136F66224C,
	HttpBase_readHeaders_mAE0AC3D3FBB0BD9973D8311C9C52433FF93CE5ED,
	NULL,
	HttpBase_ToByteArray_m5365A340C7EF67272CC8AF332C7D45DDE060FAC7,
	U3CU3Ec__DisplayClass13_0__ctor_m34215EDB0480034655DDE5DDA4C6ED370BFCBCA6,
	U3CU3Ec__DisplayClass13_0_U3CreadHeadersU3Eb__0_m3520202656188C794A3FB269BF0A549B2558548B,
	NULL,
	NULL,
	HttpRequest__ctor_mE237958BCB1B4A9CDAFE1BE093012D028C507AD5,
	HttpRequest__ctor_mCC851D0918B0C56F212BBB3FE492CB45E0985355,
	HttpRequest_CreateConnectRequest_m74240951A912D803421AD0206B94F14CB945A005,
	HttpRequest_CreateWebSocketRequest_m0F85CA0DF6563C88037201F285C0E30E3CDA4979,
	HttpRequest_GetResponse_mDFBE5D8DAE637EDA3CB80FEA51663C8C08C833A6,
	HttpRequest_SetCookies_m213AEA59069FAA0095DDF1B37E862E16E3D33DE1,
	HttpRequest_ToString_mCAE304871B470833EEF8513AE5A75043951443DE,
	HttpResponse__ctor_mF23A56537CB31E82D8A8DE42805D9EDBA728966D,
	HttpResponse_get_Cookies_m78CED25591E8F7688CBD8346D5DAD470E28B377E,
	HttpResponse_get_HasConnectionClose_m0A61095FC147EB6EC989DA847DFFF046B08CA1C0,
	HttpResponse_get_IsProxyAuthenticationRequired_m0BD4DAC6C86A678D9D3F1317F2CFC219B7868D56,
	HttpResponse_get_IsRedirect_m981833B2735F5824DE3E2BFFE25D2DE1985A4DC2,
	HttpResponse_get_IsUnauthorized_mD5345B66E2271CBA89927E95F1B7CFD786699DC3,
	HttpResponse_get_IsWebSocketResponse_m2378896FDF333E763308CBE7C93AA69D1C1148EC,
	HttpResponse_get_StatusCode_mCDF7D450E520712A4874022BC81F2D037343B290,
	HttpResponse_Parse_mBE4F6D080B1A5262ED43090C0BF5E8BC42868DE3,
	HttpResponse_ToString_m07290A9C6F91DCA8AFDB3736F2A03CD30C83D518,
	Cookie__cctor_m15C8B451F36B03AEC908CB880CDCA388DCB1FBB0,
	Cookie__ctor_m88DA7F9F87C4B9E650EC0191012FBAEC7F0233E0,
	Cookie__ctor_m7E4DAF65E3AA9EF0E14D73412FC078C412B0B880,
	Cookie__ctor_m380FF0544D979D657BC7EF43EB4103CA1FE651C6,
	Cookie_set_MaxAge_m755FA5D7A1EFAEE14727B8FC468C901784339C77,
	Cookie_set_SameSite_mFF91AE2B3DBAF8DAB4A7654EBC82A31848367C1D,
	Cookie_set_Comment_m477535E3C35E2971501C0BBE7D63D4C706E61DDA,
	Cookie_set_CommentUri_m0AEBFCA4E658B518F2A89E35FEA348E54173FE95,
	Cookie_set_Discard_mDA48275B8FDF1C12FC0788941D72E42D291D1076,
	Cookie_set_Domain_mC28AC311DAA04DAEA84F28F34BF06FA7CC6906BA,
	Cookie_get_Expired_mF493025E38ECC26E3B32933671F0E887E8865132,
	Cookie_get_Expires_m0E5335CFEAA0C9F83D78037347FA882FCC7196DC,
	Cookie_set_Expires_m415F7BFDA83A47AB8AFDFB7BA4A6BE737EF250B7,
	Cookie_set_HttpOnly_mC02BCDF89D7DFC618EDDB49C599E6087C3642945,
	Cookie_get_Name_mB3EB068775AA77A2BFFD0479265C5409C7277BF5,
	Cookie_get_Path_m8BC4E2E90A4DC80F0093B304E51D552AF9AEC7A4,
	Cookie_set_Path_m30E349842B509925BF90609FB6F35B867020DBEE,
	Cookie_set_Port_mEA9B55A2042F45C1F0BD6B212A9860D7DA3A5FD5,
	Cookie_set_Secure_m0AE9EC8BAF6FC3ABDCFC5F08317A1D6B05BC5A93,
	Cookie_get_Version_mDB121464556942924F2BC6E2C0AF47B4417C156C,
	Cookie_set_Version_mC8CE4EE6F5E9F620269191F2606269D6F4BD0581,
	Cookie_hash_mE7E311788F5A7D031D26C2C377954B50B6A4E767,
	Cookie_init_m73BA84C5EF3BA8F32E81D3053877A87AEB564236,
	Cookie_tryCreatePorts_mFA866E4429B431A633BA748210EDC82DE43C34F6,
	Cookie_EqualsWithoutValue_m6A17B401FE6A55564C3DFDC89425715AB7EDF2EF,
	Cookie_ToRequestString_m115F41177F4FA5388F06A6A7739D736C726A199D,
	Cookie_TryCreate_mF063A434AF1115C9B0E7E30213F356401CB26410,
	Cookie_Equals_mE045964281082E0D436A8D8771C7486421C3180F,
	Cookie_GetHashCode_m157AF758B0ABDFD27664B292E01778B37F67F42F,
	Cookie_ToString_m8567F449D809403B5851A7518D4467C1896B2630,
	CookieCollection__ctor_mFD618E3B4A4FE99CEF694330D7CA3E2E22BB4DF8,
	CookieCollection_get_Sorted_m25BA9DA6B9BD75257DAE7E394D569067DCA61F61,
	CookieCollection_get_Count_m62F0AAB4A4145624F147C3C2CF613B25F88C4ACB,
	CookieCollection_get_IsReadOnly_mAD9FA0A6CC1E5587FDE416A545D2BEC99AA625E7,
	CookieCollection_add_mEC7DAE8B1250B56C805B28174057819FF3453C95,
	CookieCollection_compareForSorted_mBE12999D8D1A2B578F3CD06F855ADB8221990E3F,
	CookieCollection_parseRequest_m4A411F7D1727A52D0E886A0A906E03FD1D0ED13C,
	CookieCollection_parseResponse_m7950E99147C1B8BC6CD9ACC6FA6FF62D5BBEF178,
	CookieCollection_search_m16DE9FDB551EF73EA37560AE89F48C33700A7BAA,
	CookieCollection_urlDecode_m853FDCD39702ED0B55B258884556C9310122BCB1,
	CookieCollection_Parse_mD07834988B9F19106A76B324B3F3A3AA24EB243E,
	CookieCollection_SetOrRemove_mEE0DA2FC4D4198C867774A2FAF59E397C11A1C91,
	CookieCollection_SetOrRemove_m29182CEE1E7C052C92DD628B937D205181A38314,
	CookieCollection_Add_mF98A3231E80CA3DD5377E99282B140675B84DF5B,
	CookieCollection_Clear_m24C090FCC0DB43CE2AEF55E0865E868AA50826FB,
	CookieCollection_Contains_mF13A9B55C4A167E101231F777B00813DD38E76E9,
	CookieCollection_CopyTo_m3DA12CE3EACDF518A9917B9442EE7AF3A11F5B1B,
	CookieCollection_GetEnumerator_m84E24C1354F7324B477464B1A11EF80AE85F9A8A,
	CookieCollection_Remove_mD0A12D0F5DA3D33F712E27D585C462B9A7115032,
	CookieCollection_System_Collections_IEnumerable_GetEnumerator_m218E6FCF4C3E21E1797EB91F812FA1FA37F15698,
	CookieException__ctor_m8139F97FAD9D781F74EA8495BF4EB94C708E3946,
	CookieException__ctor_m62C4075DD3BA5423EFEB71AAEEAFFEB4C633DCF5,
	CookieException__ctor_m0FB8DF9E315A43D7F600D54B8296305EDE2EE414,
	CookieException_GetObjectData_m6FDD567698C7C883A8C3BA8280E1DDD3F786F205,
	CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m85AD28C193C6DCF9276301C15A75084B27F2D85D,
	HttpUtility__cctor_mD22BA93238B775593BF8F689F3FA9C2B00C04454,
	HttpUtility_getNumber_m63ACC9ADDF3028484EDA2B636D6730F044D28E8B,
	HttpUtility_getNumber_mF6BE53AE92A9F97A8216E4020B5F2384075F27CB,
	HttpUtility_urlDecodeToBytes_m152FA907D38C421CE121213DEE0021290189FABA,
	HttpUtility_GetEncoding_m7164375040554DDDCF9B8F573280B74770839C3D,
	HttpUtility_UrlDecode_m869E3E819A24CC7E63F37877F8B3B67DD808A366,
	WebHeaderCollection__cctor_m7F5F2F4AF0E77F1513B674EDF9F3286D5E4E9E3A,
	WebHeaderCollection__ctor_mBD30634606B3E0C5581C176919441A5C70E2E5C2,
	WebHeaderCollection__ctor_mF3554DEA14F15A042C08248524EE2C934E285EEA,
	WebHeaderCollection_get_AllKeys_mEEB0483D2A39258F29270F91479E32CEB6F8CB0B,
	WebHeaderCollection_get_Count_mF12E1BB47A01CE22FACB6629FE54E4E537D0E468,
	WebHeaderCollection_add_m2A458F3478235350040A50962A5DD28DD120E083,
	WebHeaderCollection_addWithoutCheckingName_mD7FC644E907E3FF9CF89DD87120C5067A3F1334F,
	WebHeaderCollection_addWithoutCheckingNameAndRestricted_m59C29554CEE81D55DD26C3FE3AF717F4321516F1,
	WebHeaderCollection_checkHeaderType_m549D159072B231119F7FCE14ACBCC3324915227F,
	WebHeaderCollection_checkName_m0BA750B6B40BEF096AFD65FE501056D5F4ECA84D,
	WebHeaderCollection_checkRestricted_m8D2C7656C96B253B7C9616F57ED4EE7E07599136,
	WebHeaderCollection_checkState_m5285E869C1FCB5429EAF75B992B01ABF8AC040FD,
	WebHeaderCollection_checkValue_m27735441E4C79AF162613B0AE5353BA4C504D9D8,
	WebHeaderCollection_doWithCheckingState_m64BECEFFF321FF38AE923D98CE7AC0D4D7C416BE,
	WebHeaderCollection_doWithCheckingState_m29A4D470E35F4B3071989DAB32399553EF768CC0,
	WebHeaderCollection_doWithoutCheckingName_m343275E3DF0BE57A5E500BC446ABFEA35FCB4572,
	WebHeaderCollection_getHeaderInfo_m85BD8FEC8A49329DE3A786D9FB488422F4820465,
	WebHeaderCollection_isMultiValue_mAD21CC968915649A77C11340A81028767719F50F,
	WebHeaderCollection_isRestricted_mA4F76FADAAD1CD9D9F13C00C39A0CE3C1775BC7C,
	WebHeaderCollection_setWithoutCheckingName_m8CEF4BCC834132C6D69CEC74B67F0E043ED789D5,
	WebHeaderCollection_InternalSet_m55178F78DA7524E1F06391C2B32B18EF1CA11F6F,
	WebHeaderCollection_InternalSet_m645009FCE0E3C0A2D92B184C28EA53F0ADCC1EF9,
	WebHeaderCollection_Add_m4227AF3B019BA3D385877A7BE09ECC9C6FD0800F,
	WebHeaderCollection_Get_mFC808BE8F96997B303A6FA0106EC33F85DCED50F,
	WebHeaderCollection_Get_m829CC5DF3940056A5EDB81C8231A5DA742D40086,
	WebHeaderCollection_GetEnumerator_m1BD1204EADAAAEC1E4763F573046D57180DA3739,
	WebHeaderCollection_GetKey_m72EBD9D41DEA41C80CBE0AC46347DE1090D66FDD,
	WebHeaderCollection_GetObjectData_m30ED8F454609C10A131CF87799C5A1E78E7C63FC,
	WebHeaderCollection_OnDeserialization_mE8C99A2E493E224E200E81DEF50A27AA2D96F879,
	WebHeaderCollection_Set_m82EFDA57BF408CD92C9A2F6D9901FCE9295189F5,
	WebHeaderCollection_ToString_mCE15157D7C4E8EF223FEDE8AAEDA6F7049D98F30,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m67E90D77EE028DCBA04A7E3F882AE9760E6DCC4F,
	HttpVersion__cctor_mF0429FDFD24104804D1B1E1C5D7FAF90BD310202,
	HttpHeaderInfo__ctor_mD83936EFA9C348AA23D99D4CE8CD6BFFF36BDAEA,
	HttpHeaderInfo_get_IsMultiValueInRequest_m7A0939EA4CD71DFB1E97F7B20ED9AE35D36BBE68,
	HttpHeaderInfo_get_IsMultiValueInResponse_m0F19920E9C0B4E4A09C093D51162BF76D6E35CE6,
	HttpHeaderInfo_get_IsRequest_mC3E72496719DD2260BB49C0C927E4B0B6426B6B2,
	HttpHeaderInfo_get_IsResponse_m6EC358FC3EFD8B556646892DAD7FE7D4589B0A15,
	HttpHeaderInfo_get_Name_mA9CDE7D1B3743812B7487A6F773C3AC794F562FF,
	HttpHeaderInfo_IsMultiValue_m2DB10C22E6688693BC5D2B488508244004F3D0A9,
	HttpHeaderInfo_IsRestricted_m3E270AFBF9D7D779F5C66B31B59C1E44206AEB94,
	NetworkCredential__cctor_mD463E6A84BACE220DD7925D1903F80CB7C404CDA,
	NetworkCredential_get_Domain_m478AD8467DF28093C4D8BA58518D6313D20DFC84,
	NetworkCredential_get_Password_m4627B42EC58CD13466A5113ACB0A5D61070215D4,
	NetworkCredential_get_Username_m71DAB9442F134A56292F3F78B161996AFDAC5F4A,
	AuthenticationChallenge__ctor_m89D9496D058F9250332CB26A2DE2FC7EA2AF684C,
	AuthenticationChallenge_Parse_m80D1D40F0474E706FBE376A3218B9C30434388EC,
	AuthenticationChallenge_ToBasicString_m171FD34EF4823FBE4ED6C58A45E8A8AAF8DB3C20,
	AuthenticationChallenge_ToDigestString_mD8D18DE457D4675E058C28AF5576BF4E12C85FF1,
	AuthenticationResponse__ctor_mC87CEBB424AEC0340941AD1EA34B760E2AD20DE9,
	AuthenticationResponse__ctor_m5992FBF35171438C2E12BCACE2F28A341D38E1F4,
	AuthenticationResponse__ctor_mD5B6147F1CDD6345F2EA3A78ACF9F2DD1EE36E82,
	AuthenticationResponse_get_NonceCount_m94B4F34DF305DD982CDBEA058F2286A5E7558A9E,
	AuthenticationResponse_createA1_mB340A76DFB3F9BE2E916FD777DA59479B53D5F57,
	AuthenticationResponse_createA1_m12719D7CA5E45860F11689E3997BAEE7EB74B575,
	AuthenticationResponse_createA2_m54FC19F0852B8D5340AA3BC0AEB272633715D545,
	AuthenticationResponse_createA2_mBE60DFC67FE7D99F173739CBFECBF50DA52FBE45,
	AuthenticationResponse_hash_m1706B5FA588B17BB60E127D0384DA79F157A8646,
	AuthenticationResponse_initAsDigest_mEE65254A0F5EFAB5F105F4CA504DDA1A869E4AC6,
	AuthenticationResponse_CreateRequestDigest_m514F7A97B6D1160381D934A8EB6C869A405BA9D0,
	AuthenticationResponse_ToBasicString_mD959029EB3ED6FC588FE7FF14E15F3C22278BDD8,
	AuthenticationResponse_ToDigestString_m3D97A1F359A3A4883861CD4E6913F431B3625699,
	U3CU3Ec__cctor_m46946CE0A1B74C21BC4C6F94827610A1DC4D0C77,
	U3CU3Ec__ctor_m1A105A1AE604BFD24F03A5A1032FBD988C5C1481,
	U3CU3Ec_U3CinitAsDigestU3Eb__24_0_m9E735DA74FC654BEFED1E89CF3D71491D16FA4E0,
	AuthenticationBase__ctor_m510663A827778FF82B5AEECC3A405FD915FB1593,
	AuthenticationBase_get_Scheme_m8645EAA4B670F4E75F36C6CE750A9881FF3128B8,
	AuthenticationBase_CreateNonceValue_m11464AEB8E3CB43BFE2FB13A789B0937A657E9E4,
	AuthenticationBase_ParseParameters_m0232933D4FA6CBD78747BB3BCD535441B4EC1CCD,
	NULL,
	NULL,
	AuthenticationBase_ToString_m8AF7365991E64FEB697391182BC5607520FB78BF,
	ClientSslConfiguration__ctor_mF5EF97BB2DF00E97F0AB2F8FE2AD14274677A002,
	ClientSslConfiguration_get_CheckCertificateRevocation_mE359C515CAEA3A24D01D1F0B8E4DEBD098F5B283,
	ClientSslConfiguration_get_ClientCertificates_m3181C612ACFEDC8563EA850F142571E7642CF186,
	ClientSslConfiguration_get_ClientCertificateSelectionCallback_m5C0B2E0916F167F45D3AB856DB813186F6DA3F06,
	ClientSslConfiguration_get_EnabledSslProtocols_mD29188123A9C7173FFDF8FA65CB8705208FAFA31,
	ClientSslConfiguration_set_EnabledSslProtocols_m9BCD39B5286C2D891F7AB5B667CC90D0456708CE,
	ClientSslConfiguration_get_ServerCertificateValidationCallback_mFE5B87C6DFEA175F7E28624DE660EAC38E049A84,
	ClientSslConfiguration_get_TargetHost_m298AD821C4F8A0B9ACE825F9CCD511D713A2D14A,
	ClientSslConfiguration_defaultSelectClientCertificate_mE7530A99129D711B5C87CDD733A3E8DE2CBBE943,
	ClientSslConfiguration_defaultValidateServerCertificate_m833CBCFAD7450ED4D72691AEA244AF16FE04C11E,
};
static const int32_t s_InvokerIndices[432] = 
{
	6004,
	6004,
	6004,
	6004,
	5540,
	5563,
	5653,
	4821,
	-1,
	6049,
	5359,
	5563,
	5563,
	5362,
	-1,
	5160,
	6004,
	5563,
	6000,
	6004,
	5117,
	5539,
	5546,
	5654,
	6051,
	6051,
	6051,
	6045,
	6051,
	6049,
	6049,
	5557,
	5123,
	4904,
	4598,
	-1,
	5562,
	6004,
	5572,
	-1,
	5455,
	5503,
	5181,
	5646,
	5646,
	6004,
	5653,
	5359,
	5649,
	6046,
	6049,
	6049,
	6049,
	-1,
	-1,
	5557,
	-1,
	6004,
	6207,
	4055,
	2869,
	4055,
	3287,
	4055,
	3272,
	4055,
	3287,
	3271,
	4055,
	4017,
	3983,
	4055,
	3983,
	3983,
	3983,
	3287,
	2000,
	3983,
	4055,
	1979,
	1976,
	6207,
	1976,
	4017,
	3964,
	3983,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	1549,
	5646,
	1549,
	1665,
	873,
	731,
	4017,
	3983,
	3983,
	4055,
	3287,
	1976,
	1976,
	1970,
	1970,
	3983,
	4055,
	4055,
	3287,
	4055,
	2887,
	3287,
	2887,
	2887,
	2887,
	2887,
	2887,
	3287,
	2887,
	4055,
	4055,
	4055,
	4055,
	1572,
	1134,
	733,
	2887,
	3983,
	1449,
	4055,
	4055,
	4055,
	2887,
	2887,
	2887,
	2887,
	6183,
	6004,
	4055,
	4055,
	3287,
	4055,
	3287,
	6207,
	4055,
	2887,
	4055,
	4055,
	3287,
	3287,
	4055,
	2887,
	4055,
	2887,
	6207,
	3287,
	1972,
	1665,
	3964,
	4017,
	3983,
	3966,
	3287,
	3983,
	3983,
	3983,
	3983,
	3271,
	4055,
	4017,
	4017,
	4055,
	3983,
	3287,
	3270,
	1976,
	1665,
	1665,
	1183,
	3964,
	1200,
	3983,
	4055,
	1200,
	5772,
	1971,
	5772,
	3287,
	3287,
	3287,
	3287,
	3287,
	3287,
	6207,
	4055,
	463,
	463,
	3966,
	3965,
	4017,
	4017,
	4017,
	4017,
	4017,
	4017,
	4017,
	4017,
	4017,
	4017,
	3966,
	4017,
	3983,
	4017,
	4017,
	6183,
	6004,
	6004,
	6004,
	4916,
	5362,
	4916,
	4916,
	6004,
	5563,
	6014,
	5563,
	4925,
	4055,
	3983,
	2593,
	3983,
	3983,
	3983,
	4055,
	3983,
	4055,
	856,
	4055,
	3287,
	4055,
	3287,
	4055,
	3287,
	4055,
	3287,
	4055,
	3287,
	3287,
	3287,
	3287,
	3271,
	4055,
	4017,
	4017,
	4055,
	3983,
	1976,
	3983,
	3983,
	3983,
	5562,
	5557,
	-1,
	3983,
	4055,
	3271,
	-1,
	-1,
	856,
	1976,
	6004,
	6004,
	1449,
	3287,
	3983,
	856,
	3983,
	4017,
	4017,
	4017,
	4017,
	4017,
	3983,
	6004,
	3983,
	6207,
	4055,
	1976,
	856,
	3271,
	3287,
	3287,
	3287,
	3320,
	3287,
	4017,
	3933,
	3239,
	3320,
	3983,
	3983,
	3287,
	3287,
	3320,
	3965,
	3271,
	4402,
	856,
	5646,
	2887,
	2588,
	5185,
	2887,
	3965,
	3983,
	4055,
	3983,
	3965,
	4017,
	3287,
	5485,
	6004,
	6004,
	2417,
	5562,
	5563,
	3287,
	3287,
	3287,
	4055,
	2887,
	1971,
	3983,
	2887,
	3983,
	1976,
	1982,
	4055,
	1982,
	1982,
	6207,
	5931,
	5024,
	5119,
	6004,
	5562,
	6207,
	1982,
	4055,
	3983,
	3965,
	1246,
	1976,
	1976,
	5936,
	6004,
	3287,
	3320,
	6004,
	858,
	439,
	1245,
	6004,
	5654,
	5654,
	1976,
	1979,
	1246,
	1976,
	2581,
	2588,
	3983,
	2581,
	1982,
	3287,
	1976,
	3983,
	1982,
	6207,
	1971,
	4017,
	4017,
	4017,
	4017,
	3983,
	2920,
	2920,
	6207,
	3983,
	3983,
	3983,
	1816,
	6004,
	3983,
	3983,
	3287,
	1242,
	814,
	3965,
	5128,
	4492,
	5562,
	5128,
	6004,
	4055,
	6004,
	3983,
	3983,
	6207,
	4055,
	2887,
	1816,
	3965,
	6183,
	6004,
	3983,
	3983,
	3983,
	3287,
	4017,
	3983,
	3983,
	3965,
	3271,
	3983,
	3983,
	4492,
	4821,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x06000009, { 0, 3 } },
	{ 0x0600000F, { 3, 1 } },
	{ 0x06000024, { 4, 1 } },
	{ 0x06000028, { 5, 2 } },
	{ 0x06000036, { 7, 1 } },
	{ 0x06000037, { 8, 1 } },
	{ 0x06000039, { 9, 1 } },
	{ 0x0600010B, { 10, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[15] = 
{
	{ (Il2CppRGCTXDataType)2, 2094 },
	{ (Il2CppRGCTXDataType)2, 2262 },
	{ (Il2CppRGCTXDataType)3, 9947 },
	{ (Il2CppRGCTXDataType)3, 9657 },
	{ (Il2CppRGCTXDataType)2, 4655 },
	{ (Il2CppRGCTXDataType)2, 2815 },
	{ (Il2CppRGCTXDataType)3, 13602 },
	{ (Il2CppRGCTXDataType)2, 4656 },
	{ (Il2CppRGCTXDataType)2, 4657 },
	{ (Il2CppRGCTXDataType)2, 156 },
	{ (Il2CppRGCTXDataType)2, 845 },
	{ (Il2CppRGCTXDataType)3, 64 },
	{ (Il2CppRGCTXDataType)3, 65 },
	{ (Il2CppRGCTXDataType)3, 9976 },
	{ (Il2CppRGCTXDataType)2, 177 },
};
extern const CustomAttributesCacheGenerator g_websocketU2Dsharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_websocketU2Dsharp_CodeGenModule;
const Il2CppCodeGenModule g_websocketU2Dsharp_CodeGenModule = 
{
	"websocket-sharp.dll",
	432,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	15,
	s_rgctxValues,
	NULL,
	g_websocketU2Dsharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
