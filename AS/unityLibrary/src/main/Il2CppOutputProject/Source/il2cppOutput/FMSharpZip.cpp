﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct InterfaceFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo>
struct AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162;
// System.Collections.Generic.IEnumerable`1<System.Boolean>
struct IEnumerable_1_t90AD96F2C518D6F04343C83B67B02220C715C642;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_tD64DA1873BBF65E545905171348E0241A3B706C0;
// System.Collections.Generic.IList`1<System.Byte>
struct IList_1_t15467A9C40BD12CE17BE6FF409B2EF0FDD26B9D6;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_tEF253D967DB628A9F8A389A9F2E4516871FD3725;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Int16[]
struct Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
// FMETP.SharpZipLib.Checksum.Adler32
struct Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB;
// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00;
// System.ArgumentNullException
struct ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8;
// System.IO.BinaryReader
struct BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128;
// System.IO.BinaryWriter
struct BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F;
// FMETP.SharpZipLib.Checksum.Crc32
struct Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3;
// System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98;
// System.Text.Decoder
struct Decoder_t91B2ED8AEC25AA24D23A00265203BE992B12C370;
// FMETP.SharpZipLib.Zip.Compression.Deflater
struct Deflater_tD13935AC79599366FDFE44A979912695AFA19A34;
// FMETP.SharpZipLib.Zip.Compression.DeflaterEngine
struct DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386;
// FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman
struct DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5;
// FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream
struct DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E;
// FMETP.SharpZipLib.Zip.Compression.DeflaterPending
struct DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B;
// System.Text.Encoder
struct Encoder_t5095F24D3B1D0F70D08762B980731B9F1ADEE56A;
// System.Text.Encoding
struct Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827;
// System.IO.EndOfStreamException
struct EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059;
// System.Exception
struct Exception_t;
// System.Threading.ExecutionContext
struct ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414;
// FMETP.SharpZipLib.GZip.GZipException
struct GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4;
// FMETP.SharpZipLib.GZip.GZipInputStream
struct GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9;
// FMETP.SharpZipLib.GZip.GZipOutputStream
struct GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t001D97000AA0178942D19FC52942472140472E5E;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t2A667D8777429024D8A3CB3D9AE29EA79FEA6176;
// System.Security.Principal.IPrincipal
struct IPrincipal_t850ACE1F48327B64F266DD2C6FD8C5F56E4889E2;
// FMETP.SharpZipLib.Zip.Compression.Inflater
struct Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7;
// FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader
struct InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA;
// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE;
// FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17;
// FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream
struct InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E;
// System.Threading.InternalThread
struct InternalThread_t12B78B27503AE19E9122E212419A66843BF746EB;
// System.InvalidOperationException
struct InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB;
// System.LocalDataStoreHolder
struct LocalDataStoreHolder_tF51C9DD735A89132114AE47E3EB51C11D0FED146;
// System.LocalDataStoreMgr
struct LocalDataStoreMgr_t6CC44D0584911B6A6C6823115F858FC34AB4A80A;
// System.IO.MemoryStream
struct MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C;
// System.MulticastDelegate
struct MulticastDelegate_t;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718;
// FMETP.SharpZipLib.Zip.Compression.PendingBuffer
struct PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1;
// FMETP.SharpZipLib.SharpZipBaseException
struct SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B;
// System.IO.Stream
struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB;
// FMETP.SharpZipLib.StreamDecodingException
struct StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C;
// FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0;
// System.String
struct String_t;
// System.Threading.Thread
struct Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414;
// System.Type
struct Type_t;
// FMETP.SharpZipLib.ValueOutOfRangeException
struct ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree
struct Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04;
// FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7
struct U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974;

IL2CPP_EXTERN_C RuntimeClass* Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Deflater_tD13935AC79599366FDFE44A979912695AFA19A34_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_t5AB6E9D20BDB8A993042228A58C871DF8C3BCE87_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICryptoTransform_t001D97000AA0178942D19FC52942472140472E5E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_t90AD96F2C518D6F04343C83B67B02220C715C642_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_tD64DA1873BBF65E545905171348E0241A3B706C0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IList_1_t15467A9C40BD12CE17BE6FF409B2EF0FDD26B9D6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____00C40B3F013EDA60390F2E849C4581815A9419E4_0_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____03B569C38E3CD6B720388919D43735A904012C52_1_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____373B494F210C656134C5728D551D4C97B013EB33_3_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____67C0E784F3654B008A81E2988588CF4956CCF3DA_4_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____6BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____79D521E6E3E55103005E9CC3FA43B3174FAF090F_6_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____89CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____D068832E6B13A623916709C1E0E25ADCBE7B455F_8_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____DB7C763C9670DD0F6ED34B75B3410A39D835F964_10_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____E727EF4792A349C485D893E60874475A54F24B97_11_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12_FieldInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0C5D82EC0DDFC2A751DB2B8E28F3256ABDE01270;
IL2CPP_EXTERN_C String_t* _stringLiteral0EF0E9609C4859EEAC4F853683B3C2A224D9AA13;
IL2CPP_EXTERN_C String_t* _stringLiteral146305E5A9EF2C098E8782ED66BAFB9A4F45E076;
IL2CPP_EXTERN_C String_t* _stringLiteral1956A49F1096CFA1A4F0007238FFD9DAF226E029;
IL2CPP_EXTERN_C String_t* _stringLiteral19735335CA05EBD60664E496D17C7EF387AC7C8B;
IL2CPP_EXTERN_C String_t* _stringLiteral1B51DC7F7A299FDF20D4111AA6D04EFD1D4FDD6B;
IL2CPP_EXTERN_C String_t* _stringLiteral1DBDFB2A21ED7BDE42E71C3049B7639D2C2DEC93;
IL2CPP_EXTERN_C String_t* _stringLiteral20B43C997D46773915E000B0A5DBB64316FDD2E8;
IL2CPP_EXTERN_C String_t* _stringLiteral2338B88AF89C2719503E11D89F92D58C2B27109F;
IL2CPP_EXTERN_C String_t* _stringLiteral245B50E2C13208F7C0CD450C27207D86AF461D75;
IL2CPP_EXTERN_C String_t* _stringLiteral2938DFAC96402A5DC952E0ADEA558EB6FFAC2C8C;
IL2CPP_EXTERN_C String_t* _stringLiteral30CAD18346B4AC106F4C27BDD5FF7775D7F71A38;
IL2CPP_EXTERN_C String_t* _stringLiteral30D44D3EB7B534CAF51AD514F18FE7DD626129FE;
IL2CPP_EXTERN_C String_t* _stringLiteral36919DB41559814DAA1900952B7D08BD1CA0C036;
IL2CPP_EXTERN_C String_t* _stringLiteral42AEC79DC2BD0CC7051BC6692BBFF6CFDFB5F434;
IL2CPP_EXTERN_C String_t* _stringLiteral4B80EC9460A3D933B23C9CCFF5E4011E47D0FF85;
IL2CPP_EXTERN_C String_t* _stringLiteral4EB4BE55A4E968B39CF168F95F155F0CE9E4A4BC;
IL2CPP_EXTERN_C String_t* _stringLiteral51F1DBAC2A477B647C00F35F5067C191B5F39517;
IL2CPP_EXTERN_C String_t* _stringLiteral52487EB2942AE06ED9A634DF79C35C26D9ADAEA2;
IL2CPP_EXTERN_C String_t* _stringLiteral553CED52ED1AB1E6F030EF57734B26F12CA2DE3A;
IL2CPP_EXTERN_C String_t* _stringLiteral5CA4B7D02E9487E750582AC773BCF704AFBABE6F;
IL2CPP_EXTERN_C String_t* _stringLiteral60165C068AEA0DE0A862E2324EB9140E987AEF22;
IL2CPP_EXTERN_C String_t* _stringLiteral64FB23415B98CBBAAE0C2B5B5CAD4E64AC7C92C3;
IL2CPP_EXTERN_C String_t* _stringLiteral656A09E37E7997B12AB93C019EA4CC35E11E437D;
IL2CPP_EXTERN_C String_t* _stringLiteral664F1CF5298604232E38438FFBAECBE325640454;
IL2CPP_EXTERN_C String_t* _stringLiteral693A080DEBFE98FE11083064E8A1C20EFF085780;
IL2CPP_EXTERN_C String_t* _stringLiteral71C99B122416C97FB9B0D52972951F4694129599;
IL2CPP_EXTERN_C String_t* _stringLiteral77B8FF6E0BFB09EB9D8A7585234E9561468125FD;
IL2CPP_EXTERN_C String_t* _stringLiteral7C8891184659AF0C1E4D415CD6AEC9C7D572EB95;
IL2CPP_EXTERN_C String_t* _stringLiteral7F602873ED0E988A95A87335349E7292B60E4F24;
IL2CPP_EXTERN_C String_t* _stringLiteral8FF23D129BAAFF88D5BFD6399DCAA2557F8F2030;
IL2CPP_EXTERN_C String_t* _stringLiteral906BBE5F7CD672B7E71DFD5F848785C3BC25F023;
IL2CPP_EXTERN_C String_t* _stringLiteral93A2E40752B3D03B73B603835B057B861A530EE9;
IL2CPP_EXTERN_C String_t* _stringLiteral9413892BFE723FDF603E2A7F4DCECB3703459A95;
IL2CPP_EXTERN_C String_t* _stringLiteral9807D224CD9C86357FB82B5C8337A0C24ED00CB5;
IL2CPP_EXTERN_C String_t* _stringLiteral99EF3F46FC3EBDEC21B57E38C06DA94FE17B8895;
IL2CPP_EXTERN_C String_t* _stringLiteralABA8F64A9F94A564D9AC678557D76653D0CD2628;
IL2CPP_EXTERN_C String_t* _stringLiteralB81349898E91060D24CC2168BA4D1A2744026AF9;
IL2CPP_EXTERN_C String_t* _stringLiteralBDA3F81E88BE1700D497DE0A5571831377B1C88F;
IL2CPP_EXTERN_C String_t* _stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65;
IL2CPP_EXTERN_C String_t* _stringLiteralC885E9B07C81BFE24C1773C611D9F1399145BDFB;
IL2CPP_EXTERN_C String_t* _stringLiteralCBC3EE4B235C41540E696A527B41C2EB3D741A62;
IL2CPP_EXTERN_C String_t* _stringLiteralCBDDDDE911E2F08DD3A4D52E8CFF5D244531824C;
IL2CPP_EXTERN_C String_t* _stringLiteralCCF12B74DC9FC35FD79F50B6BD9E5803766CD12B;
IL2CPP_EXTERN_C String_t* _stringLiteralD2C4AFE33DBE351005555CACEE117BC851996B97;
IL2CPP_EXTERN_C String_t* _stringLiteralD35E5EE2D5AC8610DB60F486ABC47D10280EB70E;
IL2CPP_EXTERN_C String_t* _stringLiteralD65228AEF331C4DDC9C591DAE27D7197C62B893F;
IL2CPP_EXTERN_C String_t* _stringLiteralDC7B5EAEC47EFC435ADE020F2A569BDBA22F89CC;
IL2CPP_EXTERN_C String_t* _stringLiteralDEAAC5E3A77D04DD8512744622F3D88ACFA2767D;
IL2CPP_EXTERN_C String_t* _stringLiteralE2342616D08840279278C2B09E3EEBDC0A0FEAE6;
IL2CPP_EXTERN_C String_t* _stringLiteralE2892E4FE328B349200CDDAF1DED7E765A3843B4;
IL2CPP_EXTERN_C String_t* _stringLiteralF0A45CCAC3B8CC663DC29BD756A86295833579C1;
IL2CPP_EXTERN_C String_t* _stringLiteralF286F33D4D4346AB086D0F76D7130EC74F9C4F29;
IL2CPP_EXTERN_C String_t* _stringLiteralF387CA61FFEFEF87BC52030A46267A254BF3224C;
IL2CPP_EXTERN_C String_t* _stringLiteralF5140C8A872A6E28C108AF6DAFC168839A3721CD;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeflaterEngine_Deflate_m9DD2CC475173BB34AE655813C1EDE6999B261099_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeflaterEngine_SetLevel_m2EB416322DD10370B059672DB7C652B8ED1A54DC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeflaterOutputStream_Deflate_mFD09C1AC5EC2990148643BDA9FDED4A88E515B0B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeflaterOutputStream_Finish_m0B586B7D62DFF8763BA25CB632AA7DD4F2EC2499_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeflaterOutputStream_ReadByte_mACE3AF54BD8F42F3FD2290D30A854F9CC0FAC88A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeflaterOutputStream_Read_m8480D2238C5FE7F4D0B13A6752ACFEC62FBE86FE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeflaterOutputStream_Seek_mD6DE8991D472B16453F824CDC50160028B5DDD68_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeflaterOutputStream__ctor_mB41165AE84240A90382E4B53F11B299A4CDD58A1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DeflaterOutputStream_set_Position_mDF47ADFCA9BF73AD629D520EA8563A6C2795E95F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Deflater_Deflate_m4B828E32CABE4976218147C102256B124DBB3A9C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Deflater_SetInput_mD20D223304B17FE2D9637620A767625C0A3619A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Deflater_SetLevel_m05D7A3C79B57A8AF9C72613CD730EC90FAB4CE4D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Deflater__ctor_m698267A3FB8383CEDAA02AA133076DF05411CDA8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FMZipHelper_FMZipBytes_mFD4D5CDF5908FCBC97214EFBF45BD25AE810C26E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GZipInputStream_ReadFooter_m5B658299CE668EC4346D6EA9571AD17532B9AC45_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GZipOutputStream_Write_mEDC538022944DBACC0349CAADF293F28D5714CC1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterDynHeader_get_DistanceTree_m4F6820D7E6C0EB2A9A37807FC2241A66361E05F6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterDynHeader_get_LiteralLengthTree_m79581D6BBB306802E0B9BCD492B4D4285509F9F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterHuffmanTree_GetSymbol_m0F2DAE53C30D2A81AC3ED21E476F2D3451728442_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterHuffmanTree__cctor_mC7178D83B2CC2CD1FFAFC67BD5C575E6913F1E69_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterInputBuffer_ReadClearTextBuffer_m91A4E800CE258802DA172CC80C03A08E919411A5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterInputStream_Fill_m1DE4E5EC14785EFD25C866FBD31A46A1144445F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterInputStream_Read_mC36B703875162B5FAB4DC4F47A05AAD596331F50_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterInputStream_Seek_m44A365F774ED907FB953EE6964F02C492556933A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterInputStream_WriteByte_m0D8A47F5A21C9369611A324A154A2B8AAB8A9134_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterInputStream_Write_m9B9B4D2C82868E7F074E04AA712DE2F617CAC2BF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterInputStream__ctor_m152F154E3EB66C5B8E82F049C1ED5B37E52A19F8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterInputStream_get_Length_mE1ED6BA8DC45ECE8669A532BDADF82426AA14EEF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InflaterInputStream_set_Position_mBC3541E547A6D58CB4C318F765D9D5584C850071_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Inflater_DecodeChksum_m82FFF42FBBBDFD86E8EBE560EEB21E7EA9B49DCC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Inflater_DecodeHeader_mB7913D25A9125D416A703F502EFAB4707D10C92C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Inflater_DecodeHuffman_mAB4BC68E10CE0BC0F737716E1B52EA85096BC740_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Inflater_Decode_m58AF8D0040DD4AA68E0E477F77674C329E2D01CC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Inflater_Inflate_m21F470F4D5A79E52EDA46572AEB52DD878A3C424_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* OutputWindow_CopyOutput_m256CF0DA13981FB04C8D0DB6538E622E11FBEA34_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* OutputWindow_Repeat_mD7467DB942F68DACD17A6A466A2F7974AD2969A5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* OutputWindow_Write_m8E87F3E22FAA5D8B122FB61BD4DF14D07F2E42C6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* StreamManipulator_CopyBytes_m6FA30492A0A8534309A1538CF230500658CA3C79_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Tree_BuildTree_m5FF35C4C61B273025E9B99F2B6C17AFDF1C917E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCreateStateMachineU3Ed__7_MoveNext_mC8FF14CD284CC4D2E34F84F2379C483DCB0FC93B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_Reset_m0C8764730A6F04009E62C81D5B02CFE577620BA6_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t191CF7EBD974253D236A4F524E83549ADA23B318 
{
public:

public:
};


// System.Object


// FMETP.SharpZipLib.Checksum.Adler32
struct Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB  : public RuntimeObject
{
public:
	// System.UInt32 FMETP.SharpZipLib.Checksum.Adler32::checkValue
	uint32_t ___checkValue_1;

public:
	inline static int32_t get_offset_of_checkValue_1() { return static_cast<int32_t>(offsetof(Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB, ___checkValue_1)); }
	inline uint32_t get_checkValue_1() const { return ___checkValue_1; }
	inline uint32_t* get_address_of_checkValue_1() { return &___checkValue_1; }
	inline void set_checkValue_1(uint32_t value)
	{
		___checkValue_1 = value;
	}
};

struct Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_StaticFields
{
public:
	// System.UInt32 FMETP.SharpZipLib.Checksum.Adler32::BASE
	uint32_t ___BASE_0;

public:
	inline static int32_t get_offset_of_BASE_0() { return static_cast<int32_t>(offsetof(Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_StaticFields, ___BASE_0)); }
	inline uint32_t get_BASE_0() const { return ___BASE_0; }
	inline uint32_t* get_address_of_BASE_0() { return &___BASE_0; }
	inline void set_BASE_0(uint32_t value)
	{
		___BASE_0 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.IO.BinaryReader
struct BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128  : public RuntimeObject
{
public:
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___m_stream_0;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___m_buffer_1;
	// System.Text.Decoder System.IO.BinaryReader::m_decoder
	Decoder_t91B2ED8AEC25AA24D23A00265203BE992B12C370 * ___m_decoder_2;
	// System.Byte[] System.IO.BinaryReader::m_charBytes
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___m_charBytes_3;
	// System.Char[] System.IO.BinaryReader::m_singleChar
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___m_singleChar_4;
	// System.Char[] System.IO.BinaryReader::m_charBuffer
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___m_charBuffer_5;
	// System.Int32 System.IO.BinaryReader::m_maxCharsSize
	int32_t ___m_maxCharsSize_6;
	// System.Boolean System.IO.BinaryReader::m_2BytesPerChar
	bool ___m_2BytesPerChar_7;
	// System.Boolean System.IO.BinaryReader::m_isMemoryStream
	bool ___m_isMemoryStream_8;
	// System.Boolean System.IO.BinaryReader::m_leaveOpen
	bool ___m_leaveOpen_9;

public:
	inline static int32_t get_offset_of_m_stream_0() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_stream_0)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_m_stream_0() const { return ___m_stream_0; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_m_stream_0() { return &___m_stream_0; }
	inline void set_m_stream_0(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___m_stream_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stream_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_buffer_1() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_buffer_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_m_buffer_1() const { return ___m_buffer_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_m_buffer_1() { return &___m_buffer_1; }
	inline void set_m_buffer_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___m_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_buffer_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_decoder_2() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_decoder_2)); }
	inline Decoder_t91B2ED8AEC25AA24D23A00265203BE992B12C370 * get_m_decoder_2() const { return ___m_decoder_2; }
	inline Decoder_t91B2ED8AEC25AA24D23A00265203BE992B12C370 ** get_address_of_m_decoder_2() { return &___m_decoder_2; }
	inline void set_m_decoder_2(Decoder_t91B2ED8AEC25AA24D23A00265203BE992B12C370 * value)
	{
		___m_decoder_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_decoder_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_charBytes_3() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_charBytes_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_m_charBytes_3() const { return ___m_charBytes_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_m_charBytes_3() { return &___m_charBytes_3; }
	inline void set_m_charBytes_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___m_charBytes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_charBytes_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_singleChar_4() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_singleChar_4)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_m_singleChar_4() const { return ___m_singleChar_4; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_m_singleChar_4() { return &___m_singleChar_4; }
	inline void set_m_singleChar_4(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___m_singleChar_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_singleChar_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_charBuffer_5() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_charBuffer_5)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_m_charBuffer_5() const { return ___m_charBuffer_5; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_m_charBuffer_5() { return &___m_charBuffer_5; }
	inline void set_m_charBuffer_5(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___m_charBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_charBuffer_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_maxCharsSize_6() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_maxCharsSize_6)); }
	inline int32_t get_m_maxCharsSize_6() const { return ___m_maxCharsSize_6; }
	inline int32_t* get_address_of_m_maxCharsSize_6() { return &___m_maxCharsSize_6; }
	inline void set_m_maxCharsSize_6(int32_t value)
	{
		___m_maxCharsSize_6 = value;
	}

	inline static int32_t get_offset_of_m_2BytesPerChar_7() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_2BytesPerChar_7)); }
	inline bool get_m_2BytesPerChar_7() const { return ___m_2BytesPerChar_7; }
	inline bool* get_address_of_m_2BytesPerChar_7() { return &___m_2BytesPerChar_7; }
	inline void set_m_2BytesPerChar_7(bool value)
	{
		___m_2BytesPerChar_7 = value;
	}

	inline static int32_t get_offset_of_m_isMemoryStream_8() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_isMemoryStream_8)); }
	inline bool get_m_isMemoryStream_8() const { return ___m_isMemoryStream_8; }
	inline bool* get_address_of_m_isMemoryStream_8() { return &___m_isMemoryStream_8; }
	inline void set_m_isMemoryStream_8(bool value)
	{
		___m_isMemoryStream_8 = value;
	}

	inline static int32_t get_offset_of_m_leaveOpen_9() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_leaveOpen_9)); }
	inline bool get_m_leaveOpen_9() const { return ___m_leaveOpen_9; }
	inline bool* get_address_of_m_leaveOpen_9() { return &___m_leaveOpen_9; }
	inline void set_m_leaveOpen_9(bool value)
	{
		___m_leaveOpen_9 = value;
	}
};


// System.IO.BinaryWriter
struct BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F  : public RuntimeObject
{
public:
	// System.IO.Stream System.IO.BinaryWriter::OutStream
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___OutStream_1;
	// System.Byte[] System.IO.BinaryWriter::_buffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____buffer_2;
	// System.Text.Encoding System.IO.BinaryWriter::_encoding
	Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * ____encoding_3;
	// System.Text.Encoder System.IO.BinaryWriter::_encoder
	Encoder_t5095F24D3B1D0F70D08762B980731B9F1ADEE56A * ____encoder_4;
	// System.Boolean System.IO.BinaryWriter::_leaveOpen
	bool ____leaveOpen_5;
	// System.Byte[] System.IO.BinaryWriter::_largeByteBuffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____largeByteBuffer_6;
	// System.Int32 System.IO.BinaryWriter::_maxChars
	int32_t ____maxChars_7;

public:
	inline static int32_t get_offset_of_OutStream_1() { return static_cast<int32_t>(offsetof(BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F, ___OutStream_1)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_OutStream_1() const { return ___OutStream_1; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_OutStream_1() { return &___OutStream_1; }
	inline void set_OutStream_1(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___OutStream_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OutStream_1), (void*)value);
	}

	inline static int32_t get_offset_of__buffer_2() { return static_cast<int32_t>(offsetof(BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F, ____buffer_2)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__buffer_2() const { return ____buffer_2; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__buffer_2() { return &____buffer_2; }
	inline void set__buffer_2(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____buffer_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buffer_2), (void*)value);
	}

	inline static int32_t get_offset_of__encoding_3() { return static_cast<int32_t>(offsetof(BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F, ____encoding_3)); }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * get__encoding_3() const { return ____encoding_3; }
	inline Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 ** get_address_of__encoding_3() { return &____encoding_3; }
	inline void set__encoding_3(Encoding_tE901442411E2E70039D2A4AE77FB81C3D6064827 * value)
	{
		____encoding_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____encoding_3), (void*)value);
	}

	inline static int32_t get_offset_of__encoder_4() { return static_cast<int32_t>(offsetof(BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F, ____encoder_4)); }
	inline Encoder_t5095F24D3B1D0F70D08762B980731B9F1ADEE56A * get__encoder_4() const { return ____encoder_4; }
	inline Encoder_t5095F24D3B1D0F70D08762B980731B9F1ADEE56A ** get_address_of__encoder_4() { return &____encoder_4; }
	inline void set__encoder_4(Encoder_t5095F24D3B1D0F70D08762B980731B9F1ADEE56A * value)
	{
		____encoder_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____encoder_4), (void*)value);
	}

	inline static int32_t get_offset_of__leaveOpen_5() { return static_cast<int32_t>(offsetof(BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F, ____leaveOpen_5)); }
	inline bool get__leaveOpen_5() const { return ____leaveOpen_5; }
	inline bool* get_address_of__leaveOpen_5() { return &____leaveOpen_5; }
	inline void set__leaveOpen_5(bool value)
	{
		____leaveOpen_5 = value;
	}

	inline static int32_t get_offset_of__largeByteBuffer_6() { return static_cast<int32_t>(offsetof(BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F, ____largeByteBuffer_6)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__largeByteBuffer_6() const { return ____largeByteBuffer_6; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__largeByteBuffer_6() { return &____largeByteBuffer_6; }
	inline void set__largeByteBuffer_6(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____largeByteBuffer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____largeByteBuffer_6), (void*)value);
	}

	inline static int32_t get_offset_of__maxChars_7() { return static_cast<int32_t>(offsetof(BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F, ____maxChars_7)); }
	inline int32_t get__maxChars_7() const { return ____maxChars_7; }
	inline int32_t* get_address_of__maxChars_7() { return &____maxChars_7; }
	inline void set__maxChars_7(int32_t value)
	{
		____maxChars_7 = value;
	}
};

struct BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F_StaticFields
{
public:
	// System.IO.BinaryWriter System.IO.BinaryWriter::Null
	BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F_StaticFields, ___Null_0)); }
	inline BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F * get_Null_0() const { return ___Null_0; }
	inline BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_0), (void*)value);
	}
};


// FMETP.SharpZipLib.Checksum.Crc32
struct Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3  : public RuntimeObject
{
public:
	// System.UInt32 FMETP.SharpZipLib.Checksum.Crc32::checkValue
	uint32_t ___checkValue_3;

public:
	inline static int32_t get_offset_of_checkValue_3() { return static_cast<int32_t>(offsetof(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3, ___checkValue_3)); }
	inline uint32_t get_checkValue_3() const { return ___checkValue_3; }
	inline uint32_t* get_address_of_checkValue_3() { return &___checkValue_3; }
	inline void set_checkValue_3(uint32_t value)
	{
		___checkValue_3 = value;
	}
};

struct Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_StaticFields
{
public:
	// System.UInt32 FMETP.SharpZipLib.Checksum.Crc32::crcInit
	uint32_t ___crcInit_0;
	// System.UInt32 FMETP.SharpZipLib.Checksum.Crc32::crcXor
	uint32_t ___crcXor_1;
	// System.UInt32[] FMETP.SharpZipLib.Checksum.Crc32::crcTable
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___crcTable_2;

public:
	inline static int32_t get_offset_of_crcInit_0() { return static_cast<int32_t>(offsetof(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_StaticFields, ___crcInit_0)); }
	inline uint32_t get_crcInit_0() const { return ___crcInit_0; }
	inline uint32_t* get_address_of_crcInit_0() { return &___crcInit_0; }
	inline void set_crcInit_0(uint32_t value)
	{
		___crcInit_0 = value;
	}

	inline static int32_t get_offset_of_crcXor_1() { return static_cast<int32_t>(offsetof(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_StaticFields, ___crcXor_1)); }
	inline uint32_t get_crcXor_1() const { return ___crcXor_1; }
	inline uint32_t* get_address_of_crcXor_1() { return &___crcXor_1; }
	inline void set_crcXor_1(uint32_t value)
	{
		___crcXor_1 = value;
	}

	inline static int32_t get_offset_of_crcTable_2() { return static_cast<int32_t>(offsetof(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_StaticFields, ___crcTable_2)); }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* get_crcTable_2() const { return ___crcTable_2; }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF** get_address_of_crcTable_2() { return &___crcTable_2; }
	inline void set_crcTable_2(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* value)
	{
		___crcTable_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___crcTable_2), (void*)value);
	}
};


// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct CriticalFinalizerObject_tA3367C832FFE7434EB3C15C7136AF25524150997  : public RuntimeObject
{
public:

public:
};


// FMETP.SharpZipLib.Zip.Compression.Deflater
struct Deflater_tD13935AC79599366FDFE44A979912695AFA19A34  : public RuntimeObject
{
public:
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Deflater::level
	int32_t ___level_0;
	// System.Boolean FMETP.SharpZipLib.Zip.Compression.Deflater::noZlibHeaderOrFooter
	bool ___noZlibHeaderOrFooter_1;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Deflater::state
	int32_t ___state_2;
	// System.Int64 FMETP.SharpZipLib.Zip.Compression.Deflater::totalOut
	int64_t ___totalOut_3;
	// FMETP.SharpZipLib.Zip.Compression.DeflaterPending FMETP.SharpZipLib.Zip.Compression.Deflater::pending
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * ___pending_4;
	// FMETP.SharpZipLib.Zip.Compression.DeflaterEngine FMETP.SharpZipLib.Zip.Compression.Deflater::engine
	DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * ___engine_5;

public:
	inline static int32_t get_offset_of_level_0() { return static_cast<int32_t>(offsetof(Deflater_tD13935AC79599366FDFE44A979912695AFA19A34, ___level_0)); }
	inline int32_t get_level_0() const { return ___level_0; }
	inline int32_t* get_address_of_level_0() { return &___level_0; }
	inline void set_level_0(int32_t value)
	{
		___level_0 = value;
	}

	inline static int32_t get_offset_of_noZlibHeaderOrFooter_1() { return static_cast<int32_t>(offsetof(Deflater_tD13935AC79599366FDFE44A979912695AFA19A34, ___noZlibHeaderOrFooter_1)); }
	inline bool get_noZlibHeaderOrFooter_1() const { return ___noZlibHeaderOrFooter_1; }
	inline bool* get_address_of_noZlibHeaderOrFooter_1() { return &___noZlibHeaderOrFooter_1; }
	inline void set_noZlibHeaderOrFooter_1(bool value)
	{
		___noZlibHeaderOrFooter_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(Deflater_tD13935AC79599366FDFE44A979912695AFA19A34, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_totalOut_3() { return static_cast<int32_t>(offsetof(Deflater_tD13935AC79599366FDFE44A979912695AFA19A34, ___totalOut_3)); }
	inline int64_t get_totalOut_3() const { return ___totalOut_3; }
	inline int64_t* get_address_of_totalOut_3() { return &___totalOut_3; }
	inline void set_totalOut_3(int64_t value)
	{
		___totalOut_3 = value;
	}

	inline static int32_t get_offset_of_pending_4() { return static_cast<int32_t>(offsetof(Deflater_tD13935AC79599366FDFE44A979912695AFA19A34, ___pending_4)); }
	inline DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * get_pending_4() const { return ___pending_4; }
	inline DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B ** get_address_of_pending_4() { return &___pending_4; }
	inline void set_pending_4(DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * value)
	{
		___pending_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pending_4), (void*)value);
	}

	inline static int32_t get_offset_of_engine_5() { return static_cast<int32_t>(offsetof(Deflater_tD13935AC79599366FDFE44A979912695AFA19A34, ___engine_5)); }
	inline DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * get_engine_5() const { return ___engine_5; }
	inline DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 ** get_address_of_engine_5() { return &___engine_5; }
	inline void set_engine_5(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * value)
	{
		___engine_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___engine_5), (void*)value);
	}
};


// FMETP.SharpZipLib.Zip.Compression.DeflaterConstants
struct DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578  : public RuntimeObject
{
public:

public:
};

struct DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields
{
public:
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterConstants::MAX_BLOCK_SIZE
	int32_t ___MAX_BLOCK_SIZE_0;
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.DeflaterConstants::GOOD_LENGTH
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___GOOD_LENGTH_1;
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.DeflaterConstants::MAX_LAZY
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___MAX_LAZY_2;
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.DeflaterConstants::NICE_LENGTH
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___NICE_LENGTH_3;
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.DeflaterConstants::MAX_CHAIN
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___MAX_CHAIN_4;
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.DeflaterConstants::COMPR_FUNC
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___COMPR_FUNC_5;

public:
	inline static int32_t get_offset_of_MAX_BLOCK_SIZE_0() { return static_cast<int32_t>(offsetof(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields, ___MAX_BLOCK_SIZE_0)); }
	inline int32_t get_MAX_BLOCK_SIZE_0() const { return ___MAX_BLOCK_SIZE_0; }
	inline int32_t* get_address_of_MAX_BLOCK_SIZE_0() { return &___MAX_BLOCK_SIZE_0; }
	inline void set_MAX_BLOCK_SIZE_0(int32_t value)
	{
		___MAX_BLOCK_SIZE_0 = value;
	}

	inline static int32_t get_offset_of_GOOD_LENGTH_1() { return static_cast<int32_t>(offsetof(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields, ___GOOD_LENGTH_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_GOOD_LENGTH_1() const { return ___GOOD_LENGTH_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_GOOD_LENGTH_1() { return &___GOOD_LENGTH_1; }
	inline void set_GOOD_LENGTH_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___GOOD_LENGTH_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GOOD_LENGTH_1), (void*)value);
	}

	inline static int32_t get_offset_of_MAX_LAZY_2() { return static_cast<int32_t>(offsetof(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields, ___MAX_LAZY_2)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_MAX_LAZY_2() const { return ___MAX_LAZY_2; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_MAX_LAZY_2() { return &___MAX_LAZY_2; }
	inline void set_MAX_LAZY_2(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___MAX_LAZY_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MAX_LAZY_2), (void*)value);
	}

	inline static int32_t get_offset_of_NICE_LENGTH_3() { return static_cast<int32_t>(offsetof(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields, ___NICE_LENGTH_3)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_NICE_LENGTH_3() const { return ___NICE_LENGTH_3; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_NICE_LENGTH_3() { return &___NICE_LENGTH_3; }
	inline void set_NICE_LENGTH_3(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___NICE_LENGTH_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___NICE_LENGTH_3), (void*)value);
	}

	inline static int32_t get_offset_of_MAX_CHAIN_4() { return static_cast<int32_t>(offsetof(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields, ___MAX_CHAIN_4)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_MAX_CHAIN_4() const { return ___MAX_CHAIN_4; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_MAX_CHAIN_4() { return &___MAX_CHAIN_4; }
	inline void set_MAX_CHAIN_4(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___MAX_CHAIN_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MAX_CHAIN_4), (void*)value);
	}

	inline static int32_t get_offset_of_COMPR_FUNC_5() { return static_cast<int32_t>(offsetof(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields, ___COMPR_FUNC_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_COMPR_FUNC_5() const { return ___COMPR_FUNC_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_COMPR_FUNC_5() { return &___COMPR_FUNC_5; }
	inline void set_COMPR_FUNC_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___COMPR_FUNC_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___COMPR_FUNC_5), (void*)value);
	}
};


// FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman
struct DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5  : public RuntimeObject
{
public:
	// FMETP.SharpZipLib.Zip.Compression.DeflaterPending FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::pending
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * ___pending_6;
	// FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::literalTree
	Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * ___literalTree_7;
	// FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::distTree
	Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * ___distTree_8;
	// FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::blTree
	Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * ___blTree_9;
	// System.Int16[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::d_buf
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* ___d_buf_10;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::l_buf
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___l_buf_11;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::last_lit
	int32_t ___last_lit_12;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::extra_bits
	int32_t ___extra_bits_13;

public:
	inline static int32_t get_offset_of_pending_6() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5, ___pending_6)); }
	inline DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * get_pending_6() const { return ___pending_6; }
	inline DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B ** get_address_of_pending_6() { return &___pending_6; }
	inline void set_pending_6(DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * value)
	{
		___pending_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pending_6), (void*)value);
	}

	inline static int32_t get_offset_of_literalTree_7() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5, ___literalTree_7)); }
	inline Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * get_literalTree_7() const { return ___literalTree_7; }
	inline Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 ** get_address_of_literalTree_7() { return &___literalTree_7; }
	inline void set_literalTree_7(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * value)
	{
		___literalTree_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___literalTree_7), (void*)value);
	}

	inline static int32_t get_offset_of_distTree_8() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5, ___distTree_8)); }
	inline Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * get_distTree_8() const { return ___distTree_8; }
	inline Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 ** get_address_of_distTree_8() { return &___distTree_8; }
	inline void set_distTree_8(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * value)
	{
		___distTree_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___distTree_8), (void*)value);
	}

	inline static int32_t get_offset_of_blTree_9() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5, ___blTree_9)); }
	inline Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * get_blTree_9() const { return ___blTree_9; }
	inline Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 ** get_address_of_blTree_9() { return &___blTree_9; }
	inline void set_blTree_9(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * value)
	{
		___blTree_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blTree_9), (void*)value);
	}

	inline static int32_t get_offset_of_d_buf_10() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5, ___d_buf_10)); }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* get_d_buf_10() const { return ___d_buf_10; }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD** get_address_of_d_buf_10() { return &___d_buf_10; }
	inline void set_d_buf_10(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* value)
	{
		___d_buf_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___d_buf_10), (void*)value);
	}

	inline static int32_t get_offset_of_l_buf_11() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5, ___l_buf_11)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_l_buf_11() const { return ___l_buf_11; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_l_buf_11() { return &___l_buf_11; }
	inline void set_l_buf_11(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___l_buf_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___l_buf_11), (void*)value);
	}

	inline static int32_t get_offset_of_last_lit_12() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5, ___last_lit_12)); }
	inline int32_t get_last_lit_12() const { return ___last_lit_12; }
	inline int32_t* get_address_of_last_lit_12() { return &___last_lit_12; }
	inline void set_last_lit_12(int32_t value)
	{
		___last_lit_12 = value;
	}

	inline static int32_t get_offset_of_extra_bits_13() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5, ___extra_bits_13)); }
	inline int32_t get_extra_bits_13() const { return ___extra_bits_13; }
	inline int32_t* get_address_of_extra_bits_13() { return &___extra_bits_13; }
	inline void set_extra_bits_13(int32_t value)
	{
		___extra_bits_13 = value;
	}
};

struct DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields
{
public:
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::BL_ORDER
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___BL_ORDER_0;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::bit4Reverse
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___bit4Reverse_1;
	// System.Int16[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::staticLCodes
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* ___staticLCodes_2;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::staticLLength
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___staticLLength_3;
	// System.Int16[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::staticDCodes
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* ___staticDCodes_4;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::staticDLength
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___staticDLength_5;

public:
	inline static int32_t get_offset_of_BL_ORDER_0() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields, ___BL_ORDER_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_BL_ORDER_0() const { return ___BL_ORDER_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_BL_ORDER_0() { return &___BL_ORDER_0; }
	inline void set_BL_ORDER_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___BL_ORDER_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BL_ORDER_0), (void*)value);
	}

	inline static int32_t get_offset_of_bit4Reverse_1() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields, ___bit4Reverse_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_bit4Reverse_1() const { return ___bit4Reverse_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_bit4Reverse_1() { return &___bit4Reverse_1; }
	inline void set_bit4Reverse_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___bit4Reverse_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bit4Reverse_1), (void*)value);
	}

	inline static int32_t get_offset_of_staticLCodes_2() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields, ___staticLCodes_2)); }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* get_staticLCodes_2() const { return ___staticLCodes_2; }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD** get_address_of_staticLCodes_2() { return &___staticLCodes_2; }
	inline void set_staticLCodes_2(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* value)
	{
		___staticLCodes_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___staticLCodes_2), (void*)value);
	}

	inline static int32_t get_offset_of_staticLLength_3() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields, ___staticLLength_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_staticLLength_3() const { return ___staticLLength_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_staticLLength_3() { return &___staticLLength_3; }
	inline void set_staticLLength_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___staticLLength_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___staticLLength_3), (void*)value);
	}

	inline static int32_t get_offset_of_staticDCodes_4() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields, ___staticDCodes_4)); }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* get_staticDCodes_4() const { return ___staticDCodes_4; }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD** get_address_of_staticDCodes_4() { return &___staticDCodes_4; }
	inline void set_staticDCodes_4(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* value)
	{
		___staticDCodes_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___staticDCodes_4), (void*)value);
	}

	inline static int32_t get_offset_of_staticDLength_5() { return static_cast<int32_t>(offsetof(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields, ___staticDLength_5)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_staticDLength_5() const { return ___staticDLength_5; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_staticDLength_5() { return &___staticDLength_5; }
	inline void set_staticDLength_5(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___staticDLength_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___staticDLength_5), (void*)value);
	}
};


// FMZipHelper
struct FMZipHelper_t8FC297F74FB92FF3BF484032BEE2EF3C7CF98F56  : public RuntimeObject
{
public:

public:
};


// FMETP.SharpZipLib.Zip.Compression.Inflater
struct Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7  : public RuntimeObject
{
public:
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::mode
	int32_t ___mode_4;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::readAdler
	int32_t ___readAdler_5;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::neededBits
	int32_t ___neededBits_6;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::repLength
	int32_t ___repLength_7;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::repDist
	int32_t ___repDist_8;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::uncomprLen
	int32_t ___uncomprLen_9;
	// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::isLastBlock
	bool ___isLastBlock_10;
	// System.Int64 FMETP.SharpZipLib.Zip.Compression.Inflater::totalOut
	int64_t ___totalOut_11;
	// System.Int64 FMETP.SharpZipLib.Zip.Compression.Inflater::totalIn
	int64_t ___totalIn_12;
	// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::noHeader
	bool ___noHeader_13;
	// FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator FMETP.SharpZipLib.Zip.Compression.Inflater::input
	StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * ___input_14;
	// FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow FMETP.SharpZipLib.Zip.Compression.Inflater::outputWindow
	OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * ___outputWindow_15;
	// FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader FMETP.SharpZipLib.Zip.Compression.Inflater::dynHeader
	InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * ___dynHeader_16;
	// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.Inflater::litlenTree
	InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * ___litlenTree_17;
	// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.Inflater::distTree
	InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * ___distTree_18;
	// FMETP.SharpZipLib.Checksum.Adler32 FMETP.SharpZipLib.Zip.Compression.Inflater::adler
	Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * ___adler_19;

public:
	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_readAdler_5() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___readAdler_5)); }
	inline int32_t get_readAdler_5() const { return ___readAdler_5; }
	inline int32_t* get_address_of_readAdler_5() { return &___readAdler_5; }
	inline void set_readAdler_5(int32_t value)
	{
		___readAdler_5 = value;
	}

	inline static int32_t get_offset_of_neededBits_6() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___neededBits_6)); }
	inline int32_t get_neededBits_6() const { return ___neededBits_6; }
	inline int32_t* get_address_of_neededBits_6() { return &___neededBits_6; }
	inline void set_neededBits_6(int32_t value)
	{
		___neededBits_6 = value;
	}

	inline static int32_t get_offset_of_repLength_7() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___repLength_7)); }
	inline int32_t get_repLength_7() const { return ___repLength_7; }
	inline int32_t* get_address_of_repLength_7() { return &___repLength_7; }
	inline void set_repLength_7(int32_t value)
	{
		___repLength_7 = value;
	}

	inline static int32_t get_offset_of_repDist_8() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___repDist_8)); }
	inline int32_t get_repDist_8() const { return ___repDist_8; }
	inline int32_t* get_address_of_repDist_8() { return &___repDist_8; }
	inline void set_repDist_8(int32_t value)
	{
		___repDist_8 = value;
	}

	inline static int32_t get_offset_of_uncomprLen_9() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___uncomprLen_9)); }
	inline int32_t get_uncomprLen_9() const { return ___uncomprLen_9; }
	inline int32_t* get_address_of_uncomprLen_9() { return &___uncomprLen_9; }
	inline void set_uncomprLen_9(int32_t value)
	{
		___uncomprLen_9 = value;
	}

	inline static int32_t get_offset_of_isLastBlock_10() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___isLastBlock_10)); }
	inline bool get_isLastBlock_10() const { return ___isLastBlock_10; }
	inline bool* get_address_of_isLastBlock_10() { return &___isLastBlock_10; }
	inline void set_isLastBlock_10(bool value)
	{
		___isLastBlock_10 = value;
	}

	inline static int32_t get_offset_of_totalOut_11() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___totalOut_11)); }
	inline int64_t get_totalOut_11() const { return ___totalOut_11; }
	inline int64_t* get_address_of_totalOut_11() { return &___totalOut_11; }
	inline void set_totalOut_11(int64_t value)
	{
		___totalOut_11 = value;
	}

	inline static int32_t get_offset_of_totalIn_12() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___totalIn_12)); }
	inline int64_t get_totalIn_12() const { return ___totalIn_12; }
	inline int64_t* get_address_of_totalIn_12() { return &___totalIn_12; }
	inline void set_totalIn_12(int64_t value)
	{
		___totalIn_12 = value;
	}

	inline static int32_t get_offset_of_noHeader_13() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___noHeader_13)); }
	inline bool get_noHeader_13() const { return ___noHeader_13; }
	inline bool* get_address_of_noHeader_13() { return &___noHeader_13; }
	inline void set_noHeader_13(bool value)
	{
		___noHeader_13 = value;
	}

	inline static int32_t get_offset_of_input_14() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___input_14)); }
	inline StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * get_input_14() const { return ___input_14; }
	inline StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 ** get_address_of_input_14() { return &___input_14; }
	inline void set_input_14(StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * value)
	{
		___input_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___input_14), (void*)value);
	}

	inline static int32_t get_offset_of_outputWindow_15() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___outputWindow_15)); }
	inline OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * get_outputWindow_15() const { return ___outputWindow_15; }
	inline OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 ** get_address_of_outputWindow_15() { return &___outputWindow_15; }
	inline void set_outputWindow_15(OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * value)
	{
		___outputWindow_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___outputWindow_15), (void*)value);
	}

	inline static int32_t get_offset_of_dynHeader_16() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___dynHeader_16)); }
	inline InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * get_dynHeader_16() const { return ___dynHeader_16; }
	inline InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA ** get_address_of_dynHeader_16() { return &___dynHeader_16; }
	inline void set_dynHeader_16(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * value)
	{
		___dynHeader_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dynHeader_16), (void*)value);
	}

	inline static int32_t get_offset_of_litlenTree_17() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___litlenTree_17)); }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * get_litlenTree_17() const { return ___litlenTree_17; }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE ** get_address_of_litlenTree_17() { return &___litlenTree_17; }
	inline void set_litlenTree_17(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * value)
	{
		___litlenTree_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___litlenTree_17), (void*)value);
	}

	inline static int32_t get_offset_of_distTree_18() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___distTree_18)); }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * get_distTree_18() const { return ___distTree_18; }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE ** get_address_of_distTree_18() { return &___distTree_18; }
	inline void set_distTree_18(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * value)
	{
		___distTree_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___distTree_18), (void*)value);
	}

	inline static int32_t get_offset_of_adler_19() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7, ___adler_19)); }
	inline Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * get_adler_19() const { return ___adler_19; }
	inline Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB ** get_address_of_adler_19() { return &___adler_19; }
	inline void set_adler_19(Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * value)
	{
		___adler_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adler_19), (void*)value);
	}
};

struct Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields
{
public:
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.Inflater::CPLENS
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___CPLENS_0;
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.Inflater::CPLEXT
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___CPLEXT_1;
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.Inflater::CPDIST
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___CPDIST_2;
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.Inflater::CPDEXT
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___CPDEXT_3;

public:
	inline static int32_t get_offset_of_CPLENS_0() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields, ___CPLENS_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_CPLENS_0() const { return ___CPLENS_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_CPLENS_0() { return &___CPLENS_0; }
	inline void set_CPLENS_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___CPLENS_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CPLENS_0), (void*)value);
	}

	inline static int32_t get_offset_of_CPLEXT_1() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields, ___CPLEXT_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_CPLEXT_1() const { return ___CPLEXT_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_CPLEXT_1() { return &___CPLEXT_1; }
	inline void set_CPLEXT_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___CPLEXT_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CPLEXT_1), (void*)value);
	}

	inline static int32_t get_offset_of_CPDIST_2() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields, ___CPDIST_2)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_CPDIST_2() const { return ___CPDIST_2; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_CPDIST_2() { return &___CPDIST_2; }
	inline void set_CPDIST_2(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___CPDIST_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CPDIST_2), (void*)value);
	}

	inline static int32_t get_offset_of_CPDEXT_3() { return static_cast<int32_t>(offsetof(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields, ___CPDEXT_3)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_CPDEXT_3() const { return ___CPDEXT_3; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_CPDEXT_3() { return &___CPDEXT_3; }
	inline void set_CPDEXT_3(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___CPDEXT_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CPDEXT_3), (void*)value);
	}
};


// FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader
struct InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA  : public RuntimeObject
{
public:
	// FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::input
	StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * ___input_1;
	// System.Collections.Generic.IEnumerator`1<System.Boolean> FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::state
	RuntimeObject* ___state_2;
	// System.Collections.Generic.IEnumerable`1<System.Boolean> FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::stateMachine
	RuntimeObject* ___stateMachine_3;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::codeLengths
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___codeLengths_4;
	// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::litLenTree
	InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * ___litLenTree_5;
	// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::distTree
	InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * ___distTree_6;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::litLenCodeCount
	int32_t ___litLenCodeCount_7;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::distanceCodeCount
	int32_t ___distanceCodeCount_8;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::metaCodeCount
	int32_t ___metaCodeCount_9;

public:
	inline static int32_t get_offset_of_input_1() { return static_cast<int32_t>(offsetof(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA, ___input_1)); }
	inline StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * get_input_1() const { return ___input_1; }
	inline StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 ** get_address_of_input_1() { return &___input_1; }
	inline void set_input_1(StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * value)
	{
		___input_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___input_1), (void*)value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA, ___state_2)); }
	inline RuntimeObject* get_state_2() const { return ___state_2; }
	inline RuntimeObject** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(RuntimeObject* value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___state_2), (void*)value);
	}

	inline static int32_t get_offset_of_stateMachine_3() { return static_cast<int32_t>(offsetof(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA, ___stateMachine_3)); }
	inline RuntimeObject* get_stateMachine_3() const { return ___stateMachine_3; }
	inline RuntimeObject** get_address_of_stateMachine_3() { return &___stateMachine_3; }
	inline void set_stateMachine_3(RuntimeObject* value)
	{
		___stateMachine_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateMachine_3), (void*)value);
	}

	inline static int32_t get_offset_of_codeLengths_4() { return static_cast<int32_t>(offsetof(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA, ___codeLengths_4)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_codeLengths_4() const { return ___codeLengths_4; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_codeLengths_4() { return &___codeLengths_4; }
	inline void set_codeLengths_4(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___codeLengths_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___codeLengths_4), (void*)value);
	}

	inline static int32_t get_offset_of_litLenTree_5() { return static_cast<int32_t>(offsetof(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA, ___litLenTree_5)); }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * get_litLenTree_5() const { return ___litLenTree_5; }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE ** get_address_of_litLenTree_5() { return &___litLenTree_5; }
	inline void set_litLenTree_5(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * value)
	{
		___litLenTree_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___litLenTree_5), (void*)value);
	}

	inline static int32_t get_offset_of_distTree_6() { return static_cast<int32_t>(offsetof(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA, ___distTree_6)); }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * get_distTree_6() const { return ___distTree_6; }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE ** get_address_of_distTree_6() { return &___distTree_6; }
	inline void set_distTree_6(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * value)
	{
		___distTree_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___distTree_6), (void*)value);
	}

	inline static int32_t get_offset_of_litLenCodeCount_7() { return static_cast<int32_t>(offsetof(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA, ___litLenCodeCount_7)); }
	inline int32_t get_litLenCodeCount_7() const { return ___litLenCodeCount_7; }
	inline int32_t* get_address_of_litLenCodeCount_7() { return &___litLenCodeCount_7; }
	inline void set_litLenCodeCount_7(int32_t value)
	{
		___litLenCodeCount_7 = value;
	}

	inline static int32_t get_offset_of_distanceCodeCount_8() { return static_cast<int32_t>(offsetof(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA, ___distanceCodeCount_8)); }
	inline int32_t get_distanceCodeCount_8() const { return ___distanceCodeCount_8; }
	inline int32_t* get_address_of_distanceCodeCount_8() { return &___distanceCodeCount_8; }
	inline void set_distanceCodeCount_8(int32_t value)
	{
		___distanceCodeCount_8 = value;
	}

	inline static int32_t get_offset_of_metaCodeCount_9() { return static_cast<int32_t>(offsetof(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA, ___metaCodeCount_9)); }
	inline int32_t get_metaCodeCount_9() const { return ___metaCodeCount_9; }
	inline int32_t* get_address_of_metaCodeCount_9() { return &___metaCodeCount_9; }
	inline void set_metaCodeCount_9(int32_t value)
	{
		___metaCodeCount_9 = value;
	}
};

struct InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_StaticFields
{
public:
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::MetaCodeLengthIndex
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___MetaCodeLengthIndex_0;

public:
	inline static int32_t get_offset_of_MetaCodeLengthIndex_0() { return static_cast<int32_t>(offsetof(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_StaticFields, ___MetaCodeLengthIndex_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_MetaCodeLengthIndex_0() const { return ___MetaCodeLengthIndex_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_MetaCodeLengthIndex_0() { return &___MetaCodeLengthIndex_0; }
	inline void set_MetaCodeLengthIndex_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___MetaCodeLengthIndex_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MetaCodeLengthIndex_0), (void*)value);
	}
};


// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE  : public RuntimeObject
{
public:
	// System.Int16[] FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::tree
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* ___tree_0;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE, ___tree_0)); }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* get_tree_0() const { return ___tree_0; }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tree_0), (void*)value);
	}
};

struct InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_StaticFields
{
public:
	// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::defLitLenTree
	InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * ___defLitLenTree_1;
	// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::defDistTree
	InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * ___defDistTree_2;

public:
	inline static int32_t get_offset_of_defLitLenTree_1() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_StaticFields, ___defLitLenTree_1)); }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * get_defLitLenTree_1() const { return ___defLitLenTree_1; }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE ** get_address_of_defLitLenTree_1() { return &___defLitLenTree_1; }
	inline void set_defLitLenTree_1(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * value)
	{
		___defLitLenTree_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defLitLenTree_1), (void*)value);
	}

	inline static int32_t get_offset_of_defDistTree_2() { return static_cast<int32_t>(offsetof(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_StaticFields, ___defDistTree_2)); }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * get_defDistTree_2() const { return ___defDistTree_2; }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE ** get_address_of_defDistTree_2() { return &___defDistTree_2; }
	inline void set_defDistTree_2(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * value)
	{
		___defDistTree_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defDistTree_2), (void*)value);
	}
};


// FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17  : public RuntimeObject
{
public:
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::rawLength
	int32_t ___rawLength_0;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::rawData
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___rawData_1;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::clearTextLength
	int32_t ___clearTextLength_2;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::clearText
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___clearText_3;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::available
	int32_t ___available_4;
	// System.Security.Cryptography.ICryptoTransform FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::cryptoTransform
	RuntimeObject* ___cryptoTransform_5;
	// System.IO.Stream FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::inputStream
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___inputStream_6;

public:
	inline static int32_t get_offset_of_rawLength_0() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17, ___rawLength_0)); }
	inline int32_t get_rawLength_0() const { return ___rawLength_0; }
	inline int32_t* get_address_of_rawLength_0() { return &___rawLength_0; }
	inline void set_rawLength_0(int32_t value)
	{
		___rawLength_0 = value;
	}

	inline static int32_t get_offset_of_rawData_1() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17, ___rawData_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_rawData_1() const { return ___rawData_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_rawData_1() { return &___rawData_1; }
	inline void set_rawData_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___rawData_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rawData_1), (void*)value);
	}

	inline static int32_t get_offset_of_clearTextLength_2() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17, ___clearTextLength_2)); }
	inline int32_t get_clearTextLength_2() const { return ___clearTextLength_2; }
	inline int32_t* get_address_of_clearTextLength_2() { return &___clearTextLength_2; }
	inline void set_clearTextLength_2(int32_t value)
	{
		___clearTextLength_2 = value;
	}

	inline static int32_t get_offset_of_clearText_3() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17, ___clearText_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_clearText_3() const { return ___clearText_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_clearText_3() { return &___clearText_3; }
	inline void set_clearText_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___clearText_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clearText_3), (void*)value);
	}

	inline static int32_t get_offset_of_available_4() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17, ___available_4)); }
	inline int32_t get_available_4() const { return ___available_4; }
	inline int32_t* get_address_of_available_4() { return &___available_4; }
	inline void set_available_4(int32_t value)
	{
		___available_4 = value;
	}

	inline static int32_t get_offset_of_cryptoTransform_5() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17, ___cryptoTransform_5)); }
	inline RuntimeObject* get_cryptoTransform_5() const { return ___cryptoTransform_5; }
	inline RuntimeObject** get_address_of_cryptoTransform_5() { return &___cryptoTransform_5; }
	inline void set_cryptoTransform_5(RuntimeObject* value)
	{
		___cryptoTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cryptoTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_inputStream_6() { return static_cast<int32_t>(offsetof(InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17, ___inputStream_6)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_inputStream_6() const { return ___inputStream_6; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_inputStream_6() { return &___inputStream_6; }
	inline void set_inputStream_6(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___inputStream_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputStream_6), (void*)value);
	}
};


// System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identity_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};

// FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718  : public RuntimeObject
{
public:
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::window
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___window_0;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::windowEnd
	int32_t ___windowEnd_1;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::windowFilled
	int32_t ___windowFilled_2;

public:
	inline static int32_t get_offset_of_window_0() { return static_cast<int32_t>(offsetof(OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718, ___window_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_window_0() const { return ___window_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_window_0() { return &___window_0; }
	inline void set_window_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___window_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___window_0), (void*)value);
	}

	inline static int32_t get_offset_of_windowEnd_1() { return static_cast<int32_t>(offsetof(OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718, ___windowEnd_1)); }
	inline int32_t get_windowEnd_1() const { return ___windowEnd_1; }
	inline int32_t* get_address_of_windowEnd_1() { return &___windowEnd_1; }
	inline void set_windowEnd_1(int32_t value)
	{
		___windowEnd_1 = value;
	}

	inline static int32_t get_offset_of_windowFilled_2() { return static_cast<int32_t>(offsetof(OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718, ___windowFilled_2)); }
	inline int32_t get_windowFilled_2() const { return ___windowFilled_2; }
	inline int32_t* get_address_of_windowFilled_2() { return &___windowFilled_2; }
	inline void set_windowFilled_2(int32_t value)
	{
		___windowFilled_2 = value;
	}
};


// FMETP.SharpZipLib.Zip.Compression.PendingBuffer
struct PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05  : public RuntimeObject
{
public:
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.PendingBuffer::buffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer_0;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::start
	int32_t ___start_1;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::end
	int32_t ___end_2;
	// System.UInt32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::bits
	uint32_t ___bits_3;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::bitCount
	int32_t ___bitCount_4;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05, ___buffer_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_buffer_0() const { return ___buffer_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buffer_0), (void*)value);
	}

	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05, ___start_1)); }
	inline int32_t get_start_1() const { return ___start_1; }
	inline int32_t* get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(int32_t value)
	{
		___start_1 = value;
	}

	inline static int32_t get_offset_of_end_2() { return static_cast<int32_t>(offsetof(PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05, ___end_2)); }
	inline int32_t get_end_2() const { return ___end_2; }
	inline int32_t* get_address_of_end_2() { return &___end_2; }
	inline void set_end_2(int32_t value)
	{
		___end_2 = value;
	}

	inline static int32_t get_offset_of_bits_3() { return static_cast<int32_t>(offsetof(PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05, ___bits_3)); }
	inline uint32_t get_bits_3() const { return ___bits_3; }
	inline uint32_t* get_address_of_bits_3() { return &___bits_3; }
	inline void set_bits_3(uint32_t value)
	{
		___bits_3 = value;
	}

	inline static int32_t get_offset_of_bitCount_4() { return static_cast<int32_t>(offsetof(PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05, ___bitCount_4)); }
	inline int32_t get_bitCount_4() const { return ___bitCount_4; }
	inline int32_t* get_address_of_bitCount_4() { return &___bitCount_4; }
	inline void set_bitCount_4(int32_t value)
	{
		___bitCount_4 = value;
	}
};


// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50  : public RuntimeObject
{
public:

public:
};


// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1  : public RuntimeObject
{
public:
	// System.String[] System.Runtime.Serialization.SerializationInfo::m_members
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___m_members_3;
	// System.Object[] System.Runtime.Serialization.SerializationInfo::m_data
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_data_4;
	// System.Type[] System.Runtime.Serialization.SerializationInfo::m_types
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___m_types_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Serialization.SerializationInfo::m_nameToIndex
	Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * ___m_nameToIndex_6;
	// System.Int32 System.Runtime.Serialization.SerializationInfo::m_currMember
	int32_t ___m_currMember_7;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::m_converter
	RuntimeObject* ___m_converter_8;
	// System.String System.Runtime.Serialization.SerializationInfo::m_fullTypeName
	String_t* ___m_fullTypeName_9;
	// System.String System.Runtime.Serialization.SerializationInfo::m_assemName
	String_t* ___m_assemName_10;
	// System.Type System.Runtime.Serialization.SerializationInfo::objectType
	Type_t * ___objectType_11;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::isFullTypeNameSetExplicit
	bool ___isFullTypeNameSetExplicit_12;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::isAssemblyNameSetExplicit
	bool ___isAssemblyNameSetExplicit_13;
	// System.Boolean System.Runtime.Serialization.SerializationInfo::requireSameTokenInPartialTrust
	bool ___requireSameTokenInPartialTrust_14;

public:
	inline static int32_t get_offset_of_m_members_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_members_3)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_m_members_3() const { return ___m_members_3; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_m_members_3() { return &___m_members_3; }
	inline void set_m_members_3(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___m_members_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_members_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_data_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_data_4)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_data_4() const { return ___m_data_4; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_data_4() { return &___m_data_4; }
	inline void set_m_data_4(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_data_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_data_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_types_5() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_types_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_m_types_5() const { return ___m_types_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_m_types_5() { return &___m_types_5; }
	inline void set_m_types_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___m_types_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_types_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_nameToIndex_6() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_nameToIndex_6)); }
	inline Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * get_m_nameToIndex_6() const { return ___m_nameToIndex_6; }
	inline Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 ** get_address_of_m_nameToIndex_6() { return &___m_nameToIndex_6; }
	inline void set_m_nameToIndex_6(Dictionary_2_tC94E9875910491F8130C2DC8B11E4D1548A55162 * value)
	{
		___m_nameToIndex_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_nameToIndex_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_currMember_7() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_currMember_7)); }
	inline int32_t get_m_currMember_7() const { return ___m_currMember_7; }
	inline int32_t* get_address_of_m_currMember_7() { return &___m_currMember_7; }
	inline void set_m_currMember_7(int32_t value)
	{
		___m_currMember_7 = value;
	}

	inline static int32_t get_offset_of_m_converter_8() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_converter_8)); }
	inline RuntimeObject* get_m_converter_8() const { return ___m_converter_8; }
	inline RuntimeObject** get_address_of_m_converter_8() { return &___m_converter_8; }
	inline void set_m_converter_8(RuntimeObject* value)
	{
		___m_converter_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_converter_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_fullTypeName_9() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_fullTypeName_9)); }
	inline String_t* get_m_fullTypeName_9() const { return ___m_fullTypeName_9; }
	inline String_t** get_address_of_m_fullTypeName_9() { return &___m_fullTypeName_9; }
	inline void set_m_fullTypeName_9(String_t* value)
	{
		___m_fullTypeName_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_fullTypeName_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_assemName_10() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___m_assemName_10)); }
	inline String_t* get_m_assemName_10() const { return ___m_assemName_10; }
	inline String_t** get_address_of_m_assemName_10() { return &___m_assemName_10; }
	inline void set_m_assemName_10(String_t* value)
	{
		___m_assemName_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_assemName_10), (void*)value);
	}

	inline static int32_t get_offset_of_objectType_11() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___objectType_11)); }
	inline Type_t * get_objectType_11() const { return ___objectType_11; }
	inline Type_t ** get_address_of_objectType_11() { return &___objectType_11; }
	inline void set_objectType_11(Type_t * value)
	{
		___objectType_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectType_11), (void*)value);
	}

	inline static int32_t get_offset_of_isFullTypeNameSetExplicit_12() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___isFullTypeNameSetExplicit_12)); }
	inline bool get_isFullTypeNameSetExplicit_12() const { return ___isFullTypeNameSetExplicit_12; }
	inline bool* get_address_of_isFullTypeNameSetExplicit_12() { return &___isFullTypeNameSetExplicit_12; }
	inline void set_isFullTypeNameSetExplicit_12(bool value)
	{
		___isFullTypeNameSetExplicit_12 = value;
	}

	inline static int32_t get_offset_of_isAssemblyNameSetExplicit_13() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___isAssemblyNameSetExplicit_13)); }
	inline bool get_isAssemblyNameSetExplicit_13() const { return ___isAssemblyNameSetExplicit_13; }
	inline bool* get_address_of_isAssemblyNameSetExplicit_13() { return &___isAssemblyNameSetExplicit_13; }
	inline void set_isAssemblyNameSetExplicit_13(bool value)
	{
		___isAssemblyNameSetExplicit_13 = value;
	}

	inline static int32_t get_offset_of_requireSameTokenInPartialTrust_14() { return static_cast<int32_t>(offsetof(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1, ___requireSameTokenInPartialTrust_14)); }
	inline bool get_requireSameTokenInPartialTrust_14() const { return ___requireSameTokenInPartialTrust_14; }
	inline bool* get_address_of_requireSameTokenInPartialTrust_14() { return &___requireSameTokenInPartialTrust_14; }
	inline void set_requireSameTokenInPartialTrust_14(bool value)
	{
		___requireSameTokenInPartialTrust_14 = value;
	}
};


// FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0  : public RuntimeObject
{
public:
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::window_
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___window__0;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::windowStart_
	int32_t ___windowStart__1;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::windowEnd_
	int32_t ___windowEnd__2;
	// System.UInt32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::buffer_
	uint32_t ___buffer__3;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::bitsInBuffer_
	int32_t ___bitsInBuffer__4;

public:
	inline static int32_t get_offset_of_window__0() { return static_cast<int32_t>(offsetof(StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0, ___window__0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_window__0() const { return ___window__0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_window__0() { return &___window__0; }
	inline void set_window__0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___window__0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___window__0), (void*)value);
	}

	inline static int32_t get_offset_of_windowStart__1() { return static_cast<int32_t>(offsetof(StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0, ___windowStart__1)); }
	inline int32_t get_windowStart__1() const { return ___windowStart__1; }
	inline int32_t* get_address_of_windowStart__1() { return &___windowStart__1; }
	inline void set_windowStart__1(int32_t value)
	{
		___windowStart__1 = value;
	}

	inline static int32_t get_offset_of_windowEnd__2() { return static_cast<int32_t>(offsetof(StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0, ___windowEnd__2)); }
	inline int32_t get_windowEnd__2() const { return ___windowEnd__2; }
	inline int32_t* get_address_of_windowEnd__2() { return &___windowEnd__2; }
	inline void set_windowEnd__2(int32_t value)
	{
		___windowEnd__2 = value;
	}

	inline static int32_t get_offset_of_buffer__3() { return static_cast<int32_t>(offsetof(StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0, ___buffer__3)); }
	inline uint32_t get_buffer__3() const { return ___buffer__3; }
	inline uint32_t* get_address_of_buffer__3() { return &___buffer__3; }
	inline void set_buffer__3(uint32_t value)
	{
		___buffer__3 = value;
	}

	inline static int32_t get_offset_of_bitsInBuffer__4() { return static_cast<int32_t>(offsetof(StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0, ___bitsInBuffer__4)); }
	inline int32_t get_bitsInBuffer__4() const { return ___bitsInBuffer__4; }
	inline int32_t* get_address_of_bitsInBuffer__4() { return &___bitsInBuffer__4; }
	inline void set_bitsInBuffer__4(int32_t value)
	{
		___bitsInBuffer__4 = value;
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree
struct Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04  : public RuntimeObject
{
public:
	// System.Int16[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::freqs
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* ___freqs_0;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::length
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___length_1;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::minNumCodes
	int32_t ___minNumCodes_2;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::numCodes
	int32_t ___numCodes_3;
	// System.Int16[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::codes
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* ___codes_4;
	// System.Int32[] FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::bl_counts
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___bl_counts_5;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::maxLength
	int32_t ___maxLength_6;
	// FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::dh
	DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * ___dh_7;

public:
	inline static int32_t get_offset_of_freqs_0() { return static_cast<int32_t>(offsetof(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04, ___freqs_0)); }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* get_freqs_0() const { return ___freqs_0; }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD** get_address_of_freqs_0() { return &___freqs_0; }
	inline void set_freqs_0(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* value)
	{
		___freqs_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freqs_0), (void*)value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04, ___length_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_length_1() const { return ___length_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___length_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___length_1), (void*)value);
	}

	inline static int32_t get_offset_of_minNumCodes_2() { return static_cast<int32_t>(offsetof(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04, ___minNumCodes_2)); }
	inline int32_t get_minNumCodes_2() const { return ___minNumCodes_2; }
	inline int32_t* get_address_of_minNumCodes_2() { return &___minNumCodes_2; }
	inline void set_minNumCodes_2(int32_t value)
	{
		___minNumCodes_2 = value;
	}

	inline static int32_t get_offset_of_numCodes_3() { return static_cast<int32_t>(offsetof(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04, ___numCodes_3)); }
	inline int32_t get_numCodes_3() const { return ___numCodes_3; }
	inline int32_t* get_address_of_numCodes_3() { return &___numCodes_3; }
	inline void set_numCodes_3(int32_t value)
	{
		___numCodes_3 = value;
	}

	inline static int32_t get_offset_of_codes_4() { return static_cast<int32_t>(offsetof(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04, ___codes_4)); }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* get_codes_4() const { return ___codes_4; }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD** get_address_of_codes_4() { return &___codes_4; }
	inline void set_codes_4(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* value)
	{
		___codes_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___codes_4), (void*)value);
	}

	inline static int32_t get_offset_of_bl_counts_5() { return static_cast<int32_t>(offsetof(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04, ___bl_counts_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_bl_counts_5() const { return ___bl_counts_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_bl_counts_5() { return &___bl_counts_5; }
	inline void set_bl_counts_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___bl_counts_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bl_counts_5), (void*)value);
	}

	inline static int32_t get_offset_of_maxLength_6() { return static_cast<int32_t>(offsetof(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04, ___maxLength_6)); }
	inline int32_t get_maxLength_6() const { return ___maxLength_6; }
	inline int32_t* get_address_of_maxLength_6() { return &___maxLength_6; }
	inline void set_maxLength_6(int32_t value)
	{
		___maxLength_6 = value;
	}

	inline static int32_t get_offset_of_dh_7() { return static_cast<int32_t>(offsetof(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04, ___dh_7)); }
	inline DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * get_dh_7() const { return ___dh_7; }
	inline DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 ** get_address_of_dh_7() { return &___dh_7; }
	inline void set_dh_7(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * value)
	{
		___dh_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dh_7), (void*)value);
	}
};


// FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7
struct U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3  : public RuntimeObject
{
public:
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::<>2__current
	bool ___U3CU3E2__current_1;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::<>4__this
	InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * ___U3CU3E4__this_3;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::<dataCodeCount>5__2
	int32_t ___U3CdataCodeCountU3E5__2_4;
	// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::<metaCodeTree>5__3
	InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * ___U3CmetaCodeTreeU3E5__3_5;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::<index>5__4
	int32_t ___U3CindexU3E5__4_6;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::<i>5__5
	int32_t ___U3CiU3E5__5_7;
	// System.Byte FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::<codeLength>5__6
	uint8_t ___U3CcodeLengthU3E5__6_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3, ___U3CU3E2__current_1)); }
	inline bool get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline bool* get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(bool value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3, ___U3CU3E4__this_3)); }
	inline InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdataCodeCountU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3, ___U3CdataCodeCountU3E5__2_4)); }
	inline int32_t get_U3CdataCodeCountU3E5__2_4() const { return ___U3CdataCodeCountU3E5__2_4; }
	inline int32_t* get_address_of_U3CdataCodeCountU3E5__2_4() { return &___U3CdataCodeCountU3E5__2_4; }
	inline void set_U3CdataCodeCountU3E5__2_4(int32_t value)
	{
		___U3CdataCodeCountU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CmetaCodeTreeU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3, ___U3CmetaCodeTreeU3E5__3_5)); }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * get_U3CmetaCodeTreeU3E5__3_5() const { return ___U3CmetaCodeTreeU3E5__3_5; }
	inline InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE ** get_address_of_U3CmetaCodeTreeU3E5__3_5() { return &___U3CmetaCodeTreeU3E5__3_5; }
	inline void set_U3CmetaCodeTreeU3E5__3_5(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * value)
	{
		___U3CmetaCodeTreeU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmetaCodeTreeU3E5__3_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CindexU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3, ___U3CindexU3E5__4_6)); }
	inline int32_t get_U3CindexU3E5__4_6() const { return ___U3CindexU3E5__4_6; }
	inline int32_t* get_address_of_U3CindexU3E5__4_6() { return &___U3CindexU3E5__4_6; }
	inline void set_U3CindexU3E5__4_6(int32_t value)
	{
		___U3CindexU3E5__4_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__5_7() { return static_cast<int32_t>(offsetof(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3, ___U3CiU3E5__5_7)); }
	inline int32_t get_U3CiU3E5__5_7() const { return ___U3CiU3E5__5_7; }
	inline int32_t* get_address_of_U3CiU3E5__5_7() { return &___U3CiU3E5__5_7; }
	inline void set_U3CiU3E5__5_7(int32_t value)
	{
		___U3CiU3E5__5_7 = value;
	}

	inline static int32_t get_offset_of_U3CcodeLengthU3E5__6_8() { return static_cast<int32_t>(offsetof(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3, ___U3CcodeLengthU3E5__6_8)); }
	inline uint8_t get_U3CcodeLengthU3E5__6_8() const { return ___U3CcodeLengthU3E5__6_8; }
	inline uint8_t* get_address_of_U3CcodeLengthU3E5__6_8() { return &___U3CcodeLengthU3E5__6_8; }
	inline void set_U3CcodeLengthU3E5__6_8(uint8_t value)
	{
		___U3CcodeLengthU3E5__6_8 = value;
	}
};


// System.ArraySegment`1<System.Byte>
struct ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE 
{
public:
	// T[] System.ArraySegment`1::_array
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____array_0;
	// System.Int32 System.ArraySegment`1::_offset
	int32_t ____offset_1;
	// System.Int32 System.ArraySegment`1::_count
	int32_t ____count_2;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE, ____array_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__array_0() const { return ____array_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.DateTime
struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MinValue_31)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MaxValue_32)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MaxValue_32 = value;
	}
};


// FMETP.SharpZipLib.Zip.Compression.DeflaterPending
struct DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B  : public PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int16
struct Int16_tD0F031114106263BB459DA1F099FF9F42691295A 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_tD0F031114106263BB459DA1F099FF9F42691295A, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.IO.Stream
struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB  : public MarshalByRefObject_tD4DF91B488B284F899417EC468D8E50E933306A8
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_t32CD2C230786712954C1DB518DBE420A1F4C7974 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____activeReadWriteTask_3), (void*)value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t3EF85FC980AE57957BEBB6B78E81DE2E3233D385 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____asyncActiveSemaphore_4), (void*)value);
	}
};

struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_StaticFields, ___Null_1)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_Null_1() const { return ___Null_1; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_1), (void*)value);
	}
};


// System.Threading.Thread
struct Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414  : public CriticalFinalizerObject_tA3367C832FFE7434EB3C15C7136AF25524150997
{
public:
	// System.Threading.InternalThread System.Threading.Thread::internal_thread
	InternalThread_t12B78B27503AE19E9122E212419A66843BF746EB * ___internal_thread_6;
	// System.Object System.Threading.Thread::m_ThreadStartArg
	RuntimeObject * ___m_ThreadStartArg_7;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject * ___pending_exception_8;
	// System.Security.Principal.IPrincipal System.Threading.Thread::principal
	RuntimeObject* ___principal_9;
	// System.Int32 System.Threading.Thread::principal_version
	int32_t ___principal_version_10;
	// System.MulticastDelegate System.Threading.Thread::m_Delegate
	MulticastDelegate_t * ___m_Delegate_12;
	// System.Threading.ExecutionContext System.Threading.Thread::m_ExecutionContext
	ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * ___m_ExecutionContext_13;
	// System.Boolean System.Threading.Thread::m_ExecutionContextBelongsToOuterScope
	bool ___m_ExecutionContextBelongsToOuterScope_14;

public:
	inline static int32_t get_offset_of_internal_thread_6() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___internal_thread_6)); }
	inline InternalThread_t12B78B27503AE19E9122E212419A66843BF746EB * get_internal_thread_6() const { return ___internal_thread_6; }
	inline InternalThread_t12B78B27503AE19E9122E212419A66843BF746EB ** get_address_of_internal_thread_6() { return &___internal_thread_6; }
	inline void set_internal_thread_6(InternalThread_t12B78B27503AE19E9122E212419A66843BF746EB * value)
	{
		___internal_thread_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___internal_thread_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_ThreadStartArg_7() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___m_ThreadStartArg_7)); }
	inline RuntimeObject * get_m_ThreadStartArg_7() const { return ___m_ThreadStartArg_7; }
	inline RuntimeObject ** get_address_of_m_ThreadStartArg_7() { return &___m_ThreadStartArg_7; }
	inline void set_m_ThreadStartArg_7(RuntimeObject * value)
	{
		___m_ThreadStartArg_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ThreadStartArg_7), (void*)value);
	}

	inline static int32_t get_offset_of_pending_exception_8() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___pending_exception_8)); }
	inline RuntimeObject * get_pending_exception_8() const { return ___pending_exception_8; }
	inline RuntimeObject ** get_address_of_pending_exception_8() { return &___pending_exception_8; }
	inline void set_pending_exception_8(RuntimeObject * value)
	{
		___pending_exception_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pending_exception_8), (void*)value);
	}

	inline static int32_t get_offset_of_principal_9() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___principal_9)); }
	inline RuntimeObject* get_principal_9() const { return ___principal_9; }
	inline RuntimeObject** get_address_of_principal_9() { return &___principal_9; }
	inline void set_principal_9(RuntimeObject* value)
	{
		___principal_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___principal_9), (void*)value);
	}

	inline static int32_t get_offset_of_principal_version_10() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___principal_version_10)); }
	inline int32_t get_principal_version_10() const { return ___principal_version_10; }
	inline int32_t* get_address_of_principal_version_10() { return &___principal_version_10; }
	inline void set_principal_version_10(int32_t value)
	{
		___principal_version_10 = value;
	}

	inline static int32_t get_offset_of_m_Delegate_12() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___m_Delegate_12)); }
	inline MulticastDelegate_t * get_m_Delegate_12() const { return ___m_Delegate_12; }
	inline MulticastDelegate_t ** get_address_of_m_Delegate_12() { return &___m_Delegate_12; }
	inline void set_m_Delegate_12(MulticastDelegate_t * value)
	{
		___m_Delegate_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Delegate_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_ExecutionContext_13() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___m_ExecutionContext_13)); }
	inline ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * get_m_ExecutionContext_13() const { return ___m_ExecutionContext_13; }
	inline ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 ** get_address_of_m_ExecutionContext_13() { return &___m_ExecutionContext_13; }
	inline void set_m_ExecutionContext_13(ExecutionContext_t16AC73BB21FEEEAD34A017877AC18DD8BB836414 * value)
	{
		___m_ExecutionContext_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExecutionContext_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_ExecutionContextBelongsToOuterScope_14() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414, ___m_ExecutionContextBelongsToOuterScope_14)); }
	inline bool get_m_ExecutionContextBelongsToOuterScope_14() const { return ___m_ExecutionContextBelongsToOuterScope_14; }
	inline bool* get_address_of_m_ExecutionContextBelongsToOuterScope_14() { return &___m_ExecutionContextBelongsToOuterScope_14; }
	inline void set_m_ExecutionContextBelongsToOuterScope_14(bool value)
	{
		___m_ExecutionContextBelongsToOuterScope_14 = value;
	}
};

struct Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_StaticFields
{
public:
	// System.LocalDataStoreMgr System.Threading.Thread::s_LocalDataStoreMgr
	LocalDataStoreMgr_t6CC44D0584911B6A6C6823115F858FC34AB4A80A * ___s_LocalDataStoreMgr_0;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentCulture
	AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * ___s_asyncLocalCurrentCulture_4;
	// System.Threading.AsyncLocal`1<System.Globalization.CultureInfo> System.Threading.Thread::s_asyncLocalCurrentUICulture
	AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * ___s_asyncLocalCurrentUICulture_5;

public:
	inline static int32_t get_offset_of_s_LocalDataStoreMgr_0() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_StaticFields, ___s_LocalDataStoreMgr_0)); }
	inline LocalDataStoreMgr_t6CC44D0584911B6A6C6823115F858FC34AB4A80A * get_s_LocalDataStoreMgr_0() const { return ___s_LocalDataStoreMgr_0; }
	inline LocalDataStoreMgr_t6CC44D0584911B6A6C6823115F858FC34AB4A80A ** get_address_of_s_LocalDataStoreMgr_0() { return &___s_LocalDataStoreMgr_0; }
	inline void set_s_LocalDataStoreMgr_0(LocalDataStoreMgr_t6CC44D0584911B6A6C6823115F858FC34AB4A80A * value)
	{
		___s_LocalDataStoreMgr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_LocalDataStoreMgr_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentCulture_4() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_StaticFields, ___s_asyncLocalCurrentCulture_4)); }
	inline AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * get_s_asyncLocalCurrentCulture_4() const { return ___s_asyncLocalCurrentCulture_4; }
	inline AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 ** get_address_of_s_asyncLocalCurrentCulture_4() { return &___s_asyncLocalCurrentCulture_4; }
	inline void set_s_asyncLocalCurrentCulture_4(AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * value)
	{
		___s_asyncLocalCurrentCulture_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_asyncLocalCurrentCulture_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncLocalCurrentUICulture_5() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_StaticFields, ___s_asyncLocalCurrentUICulture_5)); }
	inline AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * get_s_asyncLocalCurrentUICulture_5() const { return ___s_asyncLocalCurrentUICulture_5; }
	inline AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 ** get_address_of_s_asyncLocalCurrentUICulture_5() { return &___s_asyncLocalCurrentUICulture_5; }
	inline void set_s_asyncLocalCurrentUICulture_5(AsyncLocal_1_t480A201BA0D1C62C2C6FA6598EEDF7BB35D85349 * value)
	{
		___s_asyncLocalCurrentUICulture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_asyncLocalCurrentUICulture_5), (void*)value);
	}
};

struct Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_ThreadStaticFields
{
public:
	// System.LocalDataStoreHolder System.Threading.Thread::s_LocalDataStore
	LocalDataStoreHolder_tF51C9DD735A89132114AE47E3EB51C11D0FED146 * ___s_LocalDataStore_1;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentCulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___m_CurrentCulture_2;
	// System.Globalization.CultureInfo System.Threading.Thread::m_CurrentUICulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___m_CurrentUICulture_3;
	// System.Threading.Thread System.Threading.Thread::current_thread
	Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * ___current_thread_11;

public:
	inline static int32_t get_offset_of_s_LocalDataStore_1() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_ThreadStaticFields, ___s_LocalDataStore_1)); }
	inline LocalDataStoreHolder_tF51C9DD735A89132114AE47E3EB51C11D0FED146 * get_s_LocalDataStore_1() const { return ___s_LocalDataStore_1; }
	inline LocalDataStoreHolder_tF51C9DD735A89132114AE47E3EB51C11D0FED146 ** get_address_of_s_LocalDataStore_1() { return &___s_LocalDataStore_1; }
	inline void set_s_LocalDataStore_1(LocalDataStoreHolder_tF51C9DD735A89132114AE47E3EB51C11D0FED146 * value)
	{
		___s_LocalDataStore_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_LocalDataStore_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentCulture_2() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_ThreadStaticFields, ___m_CurrentCulture_2)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_m_CurrentCulture_2() const { return ___m_CurrentCulture_2; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_m_CurrentCulture_2() { return &___m_CurrentCulture_2; }
	inline void set_m_CurrentCulture_2(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___m_CurrentCulture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentCulture_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentUICulture_3() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_ThreadStaticFields, ___m_CurrentUICulture_3)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_m_CurrentUICulture_3() const { return ___m_CurrentUICulture_3; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_m_CurrentUICulture_3() { return &___m_CurrentUICulture_3; }
	inline void set_m_CurrentUICulture_3(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___m_CurrentUICulture_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentUICulture_3), (void*)value);
	}

	inline static int32_t get_offset_of_current_thread_11() { return static_cast<int32_t>(offsetof(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414_ThreadStaticFields, ___current_thread_11)); }
	inline Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * get_current_thread_11() const { return ___current_thread_11; }
	inline Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 ** get_address_of_current_thread_11() { return &___current_thread_11; }
	inline void set_current_thread_11(Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * value)
	{
		___current_thread_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_thread_11), (void*)value);
	}
};


// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10
struct __StaticArrayInitTypeSizeU3D10_t365E850E6FAB121EEEBD1F52E8FA8935F328F49A 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_t365E850E6FAB121EEEBD1F52E8FA8935F328F49A__padding[10];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1024
struct __StaticArrayInitTypeSizeU3D1024_t72E3F24CDA9A31DD0F8F4EBD019E9D3CFD859C17 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D1024_t72E3F24CDA9A31DD0F8F4EBD019E9D3CFD859C17__padding[1024];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=116
struct __StaticArrayInitTypeSizeU3D116_tF60CBD82F5C6185F250ADA08F8B8F5148D09D098 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D116_tF60CBD82F5C6185F250ADA08F8B8F5148D09D098__padding[116];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120
struct __StaticArrayInitTypeSizeU3D120_t7130D2CCFFEAA95F47F81906AFEB92C3CB93BE99 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t7130D2CCFFEAA95F47F81906AFEB92C3CB93BE99__padding[120];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16
struct __StaticArrayInitTypeSizeU3D16_t95BAA4FD93F7DBE19019E543E7FE19E15FC4A44F 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D16_t95BAA4FD93F7DBE19019E543E7FE19E15FC4A44F__padding[16];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40
struct __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6__padding[40];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=76
struct __StaticArrayInitTypeSizeU3D76_t7E4BCEAEB22B2ACFC3AFE39AF4473F82E4D52B7F 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D76_t7E4BCEAEB22B2ACFC3AFE39AF4473F82E4D52B7F__padding[76];
	};

public:
};


// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::00C40B3F013EDA60390F2E849C4581815A9419E4
	__StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  ___00C40B3F013EDA60390F2E849C4581815A9419E4_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::03B569C38E3CD6B720388919D43735A904012C52
	__StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  ___03B569C38E3CD6B720388919D43735A904012C52_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=76 <PrivateImplementationDetails>::1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38
	__StaticArrayInitTypeSizeU3D76_t7E4BCEAEB22B2ACFC3AFE39AF4473F82E4D52B7F  ___1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=1024 <PrivateImplementationDetails>::373B494F210C656134C5728D551D4C97B013EB33
	__StaticArrayInitTypeSizeU3D1024_t72E3F24CDA9A31DD0F8F4EBD019E9D3CFD859C17  ___373B494F210C656134C5728D551D4C97B013EB33_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=116 <PrivateImplementationDetails>::67C0E784F3654B008A81E2988588CF4956CCF3DA
	__StaticArrayInitTypeSizeU3D116_tF60CBD82F5C6185F250ADA08F8B8F5148D09D098  ___67C0E784F3654B008A81E2988588CF4956CCF3DA_4;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::6BC4EAB0D604C8D4599021AD611C5DBA7FF7E306
	__StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  ___6BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::79D521E6E3E55103005E9CC3FA43B3174FAF090F
	__StaticArrayInitTypeSizeU3D120_t7130D2CCFFEAA95F47F81906AFEB92C3CB93BE99  ___79D521E6E3E55103005E9CC3FA43B3174FAF090F_6;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=16 <PrivateImplementationDetails>::89CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD
	__StaticArrayInitTypeSizeU3D16_t95BAA4FD93F7DBE19019E543E7FE19E15FC4A44F  ___89CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120 <PrivateImplementationDetails>::D068832E6B13A623916709C1E0E25ADCBE7B455F
	__StaticArrayInitTypeSizeU3D120_t7130D2CCFFEAA95F47F81906AFEB92C3CB93BE99  ___D068832E6B13A623916709C1E0E25ADCBE7B455F_8;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=116 <PrivateImplementationDetails>::D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF
	__StaticArrayInitTypeSizeU3D116_tF60CBD82F5C6185F250ADA08F8B8F5148D09D098  ___D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::DB7C763C9670DD0F6ED34B75B3410A39D835F964
	__StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  ___DB7C763C9670DD0F6ED34B75B3410A39D835F964_10;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::E727EF4792A349C485D893E60874475A54F24B97
	__StaticArrayInitTypeSizeU3D10_t365E850E6FAB121EEEBD1F52E8FA8935F328F49A  ___E727EF4792A349C485D893E60874475A54F24B97_11;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=40 <PrivateImplementationDetails>::ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79
	__StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  ___ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12;

public:
	inline static int32_t get_offset_of_U300C40B3F013EDA60390F2E849C4581815A9419E4_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___00C40B3F013EDA60390F2E849C4581815A9419E4_0)); }
	inline __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  get_U300C40B3F013EDA60390F2E849C4581815A9419E4_0() const { return ___00C40B3F013EDA60390F2E849C4581815A9419E4_0; }
	inline __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6 * get_address_of_U300C40B3F013EDA60390F2E849C4581815A9419E4_0() { return &___00C40B3F013EDA60390F2E849C4581815A9419E4_0; }
	inline void set_U300C40B3F013EDA60390F2E849C4581815A9419E4_0(__StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  value)
	{
		___00C40B3F013EDA60390F2E849C4581815A9419E4_0 = value;
	}

	inline static int32_t get_offset_of_U303B569C38E3CD6B720388919D43735A904012C52_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___03B569C38E3CD6B720388919D43735A904012C52_1)); }
	inline __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  get_U303B569C38E3CD6B720388919D43735A904012C52_1() const { return ___03B569C38E3CD6B720388919D43735A904012C52_1; }
	inline __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6 * get_address_of_U303B569C38E3CD6B720388919D43735A904012C52_1() { return &___03B569C38E3CD6B720388919D43735A904012C52_1; }
	inline void set_U303B569C38E3CD6B720388919D43735A904012C52_1(__StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  value)
	{
		___03B569C38E3CD6B720388919D43735A904012C52_1 = value;
	}

	inline static int32_t get_offset_of_U31FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2)); }
	inline __StaticArrayInitTypeSizeU3D76_t7E4BCEAEB22B2ACFC3AFE39AF4473F82E4D52B7F  get_U31FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2() const { return ___1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2; }
	inline __StaticArrayInitTypeSizeU3D76_t7E4BCEAEB22B2ACFC3AFE39AF4473F82E4D52B7F * get_address_of_U31FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2() { return &___1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2; }
	inline void set_U31FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2(__StaticArrayInitTypeSizeU3D76_t7E4BCEAEB22B2ACFC3AFE39AF4473F82E4D52B7F  value)
	{
		___1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2 = value;
	}

	inline static int32_t get_offset_of_U3373B494F210C656134C5728D551D4C97B013EB33_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___373B494F210C656134C5728D551D4C97B013EB33_3)); }
	inline __StaticArrayInitTypeSizeU3D1024_t72E3F24CDA9A31DD0F8F4EBD019E9D3CFD859C17  get_U3373B494F210C656134C5728D551D4C97B013EB33_3() const { return ___373B494F210C656134C5728D551D4C97B013EB33_3; }
	inline __StaticArrayInitTypeSizeU3D1024_t72E3F24CDA9A31DD0F8F4EBD019E9D3CFD859C17 * get_address_of_U3373B494F210C656134C5728D551D4C97B013EB33_3() { return &___373B494F210C656134C5728D551D4C97B013EB33_3; }
	inline void set_U3373B494F210C656134C5728D551D4C97B013EB33_3(__StaticArrayInitTypeSizeU3D1024_t72E3F24CDA9A31DD0F8F4EBD019E9D3CFD859C17  value)
	{
		___373B494F210C656134C5728D551D4C97B013EB33_3 = value;
	}

	inline static int32_t get_offset_of_U367C0E784F3654B008A81E2988588CF4956CCF3DA_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___67C0E784F3654B008A81E2988588CF4956CCF3DA_4)); }
	inline __StaticArrayInitTypeSizeU3D116_tF60CBD82F5C6185F250ADA08F8B8F5148D09D098  get_U367C0E784F3654B008A81E2988588CF4956CCF3DA_4() const { return ___67C0E784F3654B008A81E2988588CF4956CCF3DA_4; }
	inline __StaticArrayInitTypeSizeU3D116_tF60CBD82F5C6185F250ADA08F8B8F5148D09D098 * get_address_of_U367C0E784F3654B008A81E2988588CF4956CCF3DA_4() { return &___67C0E784F3654B008A81E2988588CF4956CCF3DA_4; }
	inline void set_U367C0E784F3654B008A81E2988588CF4956CCF3DA_4(__StaticArrayInitTypeSizeU3D116_tF60CBD82F5C6185F250ADA08F8B8F5148D09D098  value)
	{
		___67C0E784F3654B008A81E2988588CF4956CCF3DA_4 = value;
	}

	inline static int32_t get_offset_of_U36BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___6BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5)); }
	inline __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  get_U36BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5() const { return ___6BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5; }
	inline __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6 * get_address_of_U36BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5() { return &___6BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5; }
	inline void set_U36BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5(__StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  value)
	{
		___6BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5 = value;
	}

	inline static int32_t get_offset_of_U379D521E6E3E55103005E9CC3FA43B3174FAF090F_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___79D521E6E3E55103005E9CC3FA43B3174FAF090F_6)); }
	inline __StaticArrayInitTypeSizeU3D120_t7130D2CCFFEAA95F47F81906AFEB92C3CB93BE99  get_U379D521E6E3E55103005E9CC3FA43B3174FAF090F_6() const { return ___79D521E6E3E55103005E9CC3FA43B3174FAF090F_6; }
	inline __StaticArrayInitTypeSizeU3D120_t7130D2CCFFEAA95F47F81906AFEB92C3CB93BE99 * get_address_of_U379D521E6E3E55103005E9CC3FA43B3174FAF090F_6() { return &___79D521E6E3E55103005E9CC3FA43B3174FAF090F_6; }
	inline void set_U379D521E6E3E55103005E9CC3FA43B3174FAF090F_6(__StaticArrayInitTypeSizeU3D120_t7130D2CCFFEAA95F47F81906AFEB92C3CB93BE99  value)
	{
		___79D521E6E3E55103005E9CC3FA43B3174FAF090F_6 = value;
	}

	inline static int32_t get_offset_of_U389CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___89CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7)); }
	inline __StaticArrayInitTypeSizeU3D16_t95BAA4FD93F7DBE19019E543E7FE19E15FC4A44F  get_U389CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7() const { return ___89CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7; }
	inline __StaticArrayInitTypeSizeU3D16_t95BAA4FD93F7DBE19019E543E7FE19E15FC4A44F * get_address_of_U389CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7() { return &___89CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7; }
	inline void set_U389CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7(__StaticArrayInitTypeSizeU3D16_t95BAA4FD93F7DBE19019E543E7FE19E15FC4A44F  value)
	{
		___89CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7 = value;
	}

	inline static int32_t get_offset_of_D068832E6B13A623916709C1E0E25ADCBE7B455F_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___D068832E6B13A623916709C1E0E25ADCBE7B455F_8)); }
	inline __StaticArrayInitTypeSizeU3D120_t7130D2CCFFEAA95F47F81906AFEB92C3CB93BE99  get_D068832E6B13A623916709C1E0E25ADCBE7B455F_8() const { return ___D068832E6B13A623916709C1E0E25ADCBE7B455F_8; }
	inline __StaticArrayInitTypeSizeU3D120_t7130D2CCFFEAA95F47F81906AFEB92C3CB93BE99 * get_address_of_D068832E6B13A623916709C1E0E25ADCBE7B455F_8() { return &___D068832E6B13A623916709C1E0E25ADCBE7B455F_8; }
	inline void set_D068832E6B13A623916709C1E0E25ADCBE7B455F_8(__StaticArrayInitTypeSizeU3D120_t7130D2CCFFEAA95F47F81906AFEB92C3CB93BE99  value)
	{
		___D068832E6B13A623916709C1E0E25ADCBE7B455F_8 = value;
	}

	inline static int32_t get_offset_of_D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9)); }
	inline __StaticArrayInitTypeSizeU3D116_tF60CBD82F5C6185F250ADA08F8B8F5148D09D098  get_D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9() const { return ___D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9; }
	inline __StaticArrayInitTypeSizeU3D116_tF60CBD82F5C6185F250ADA08F8B8F5148D09D098 * get_address_of_D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9() { return &___D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9; }
	inline void set_D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9(__StaticArrayInitTypeSizeU3D116_tF60CBD82F5C6185F250ADA08F8B8F5148D09D098  value)
	{
		___D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9 = value;
	}

	inline static int32_t get_offset_of_DB7C763C9670DD0F6ED34B75B3410A39D835F964_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___DB7C763C9670DD0F6ED34B75B3410A39D835F964_10)); }
	inline __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  get_DB7C763C9670DD0F6ED34B75B3410A39D835F964_10() const { return ___DB7C763C9670DD0F6ED34B75B3410A39D835F964_10; }
	inline __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6 * get_address_of_DB7C763C9670DD0F6ED34B75B3410A39D835F964_10() { return &___DB7C763C9670DD0F6ED34B75B3410A39D835F964_10; }
	inline void set_DB7C763C9670DD0F6ED34B75B3410A39D835F964_10(__StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  value)
	{
		___DB7C763C9670DD0F6ED34B75B3410A39D835F964_10 = value;
	}

	inline static int32_t get_offset_of_E727EF4792A349C485D893E60874475A54F24B97_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___E727EF4792A349C485D893E60874475A54F24B97_11)); }
	inline __StaticArrayInitTypeSizeU3D10_t365E850E6FAB121EEEBD1F52E8FA8935F328F49A  get_E727EF4792A349C485D893E60874475A54F24B97_11() const { return ___E727EF4792A349C485D893E60874475A54F24B97_11; }
	inline __StaticArrayInitTypeSizeU3D10_t365E850E6FAB121EEEBD1F52E8FA8935F328F49A * get_address_of_E727EF4792A349C485D893E60874475A54F24B97_11() { return &___E727EF4792A349C485D893E60874475A54F24B97_11; }
	inline void set_E727EF4792A349C485D893E60874475A54F24B97_11(__StaticArrayInitTypeSizeU3D10_t365E850E6FAB121EEEBD1F52E8FA8935F328F49A  value)
	{
		___E727EF4792A349C485D893E60874475A54F24B97_11 = value;
	}

	inline static int32_t get_offset_of_ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B_StaticFields, ___ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12)); }
	inline __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  get_ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12() const { return ___ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12; }
	inline __StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6 * get_address_of_ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12() { return &___ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12; }
	inline void set_ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12(__StaticArrayInitTypeSizeU3D40_tEC5A6A5FFF54B74F8F679BDA4FF3DF233F7B1FD6  value)
	{
		___ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12 = value;
	}
};


// FMETP.SharpZipLib.Zip.Compression.DeflateStrategy
struct DeflateStrategy_t140F2DE7F214A85A5D5F9D2321FE10CB82994E51 
{
public:
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflateStrategy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeflateStrategy_t140F2DE7F214A85A5D5F9D2321FE10CB82994E51, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream
struct DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E  : public Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB
{
public:
	// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::_IsStreamOwner
	bool ____IsStreamOwner_5;
	// System.Security.Cryptography.ICryptoTransform FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::cryptoTransform_
	RuntimeObject* ___cryptoTransform__6;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::buffer_
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer__7;
	// FMETP.SharpZipLib.Zip.Compression.Deflater FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::deflater_
	Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * ___deflater__8;
	// System.IO.Stream FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::baseOutputStream_
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseOutputStream__9;
	// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::isClosed_
	bool ___isClosed__10;

public:
	inline static int32_t get_offset_of__IsStreamOwner_5() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E, ____IsStreamOwner_5)); }
	inline bool get__IsStreamOwner_5() const { return ____IsStreamOwner_5; }
	inline bool* get_address_of__IsStreamOwner_5() { return &____IsStreamOwner_5; }
	inline void set__IsStreamOwner_5(bool value)
	{
		____IsStreamOwner_5 = value;
	}

	inline static int32_t get_offset_of_cryptoTransform__6() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E, ___cryptoTransform__6)); }
	inline RuntimeObject* get_cryptoTransform__6() const { return ___cryptoTransform__6; }
	inline RuntimeObject** get_address_of_cryptoTransform__6() { return &___cryptoTransform__6; }
	inline void set_cryptoTransform__6(RuntimeObject* value)
	{
		___cryptoTransform__6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cryptoTransform__6), (void*)value);
	}

	inline static int32_t get_offset_of_buffer__7() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E, ___buffer__7)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_buffer__7() const { return ___buffer__7; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_buffer__7() { return &___buffer__7; }
	inline void set_buffer__7(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___buffer__7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buffer__7), (void*)value);
	}

	inline static int32_t get_offset_of_deflater__8() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E, ___deflater__8)); }
	inline Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * get_deflater__8() const { return ___deflater__8; }
	inline Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 ** get_address_of_deflater__8() { return &___deflater__8; }
	inline void set_deflater__8(Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * value)
	{
		___deflater__8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deflater__8), (void*)value);
	}

	inline static int32_t get_offset_of_baseOutputStream__9() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E, ___baseOutputStream__9)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_baseOutputStream__9() const { return ___baseOutputStream__9; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_baseOutputStream__9() { return &___baseOutputStream__9; }
	inline void set_baseOutputStream__9(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___baseOutputStream__9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___baseOutputStream__9), (void*)value);
	}

	inline static int32_t get_offset_of_isClosed__10() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E, ___isClosed__10)); }
	inline bool get_isClosed__10() const { return ___isClosed__10; }
	inline bool* get_address_of_isClosed__10() { return &___isClosed__10; }
	inline void set_isClosed__10(bool value)
	{
		___isClosed__10 = value;
	}
};

struct DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::_aesRnd
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____aesRnd_11;

public:
	inline static int32_t get_offset_of__aesRnd_11() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E_StaticFields, ____aesRnd_11)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__aesRnd_11() const { return ____aesRnd_11; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__aesRnd_11() { return &____aesRnd_11; }
	inline void set__aesRnd_11(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____aesRnd_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____aesRnd_11), (void*)value);
	}
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream
struct InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E  : public Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB
{
public:
	// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::_IsStreamOwner
	bool ____IsStreamOwner_5;
	// FMETP.SharpZipLib.Zip.Compression.Inflater FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::inf
	Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * ___inf_6;
	// FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::inputBuffer
	InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * ___inputBuffer_7;
	// System.IO.Stream FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::baseInputStream
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseInputStream_8;
	// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::isClosed
	bool ___isClosed_9;

public:
	inline static int32_t get_offset_of__IsStreamOwner_5() { return static_cast<int32_t>(offsetof(InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E, ____IsStreamOwner_5)); }
	inline bool get__IsStreamOwner_5() const { return ____IsStreamOwner_5; }
	inline bool* get_address_of__IsStreamOwner_5() { return &____IsStreamOwner_5; }
	inline void set__IsStreamOwner_5(bool value)
	{
		____IsStreamOwner_5 = value;
	}

	inline static int32_t get_offset_of_inf_6() { return static_cast<int32_t>(offsetof(InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E, ___inf_6)); }
	inline Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * get_inf_6() const { return ___inf_6; }
	inline Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 ** get_address_of_inf_6() { return &___inf_6; }
	inline void set_inf_6(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * value)
	{
		___inf_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inf_6), (void*)value);
	}

	inline static int32_t get_offset_of_inputBuffer_7() { return static_cast<int32_t>(offsetof(InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E, ___inputBuffer_7)); }
	inline InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * get_inputBuffer_7() const { return ___inputBuffer_7; }
	inline InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 ** get_address_of_inputBuffer_7() { return &___inputBuffer_7; }
	inline void set_inputBuffer_7(InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * value)
	{
		___inputBuffer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputBuffer_7), (void*)value);
	}

	inline static int32_t get_offset_of_baseInputStream_8() { return static_cast<int32_t>(offsetof(InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E, ___baseInputStream_8)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_baseInputStream_8() const { return ___baseInputStream_8; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_baseInputStream_8() { return &___baseInputStream_8; }
	inline void set_baseInputStream_8(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___baseInputStream_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___baseInputStream_8), (void*)value);
	}

	inline static int32_t get_offset_of_isClosed_9() { return static_cast<int32_t>(offsetof(InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E, ___isClosed_9)); }
	inline bool get_isClosed_9() const { return ___isClosed_9; }
	inline bool* get_address_of_isClosed_9() { return &___isClosed_9; }
	inline void set_isClosed_9(bool value)
	{
		___isClosed_9 = value;
	}
};


// System.IO.MemoryStream
struct MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C  : public Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____buffer_5;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_6;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_7;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_8;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_9;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_10;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_11;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_12;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_13;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_tEF253D967DB628A9F8A389A9F2E4516871FD3725 * ____lastReadTask_14;

public:
	inline static int32_t get_offset_of__buffer_5() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____buffer_5)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__buffer_5() const { return ____buffer_5; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__buffer_5() { return &____buffer_5; }
	inline void set__buffer_5(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____buffer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buffer_5), (void*)value);
	}

	inline static int32_t get_offset_of__origin_6() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____origin_6)); }
	inline int32_t get__origin_6() const { return ____origin_6; }
	inline int32_t* get_address_of__origin_6() { return &____origin_6; }
	inline void set__origin_6(int32_t value)
	{
		____origin_6 = value;
	}

	inline static int32_t get_offset_of__position_7() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____position_7)); }
	inline int32_t get__position_7() const { return ____position_7; }
	inline int32_t* get_address_of__position_7() { return &____position_7; }
	inline void set__position_7(int32_t value)
	{
		____position_7 = value;
	}

	inline static int32_t get_offset_of__length_8() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____length_8)); }
	inline int32_t get__length_8() const { return ____length_8; }
	inline int32_t* get_address_of__length_8() { return &____length_8; }
	inline void set__length_8(int32_t value)
	{
		____length_8 = value;
	}

	inline static int32_t get_offset_of__capacity_9() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____capacity_9)); }
	inline int32_t get__capacity_9() const { return ____capacity_9; }
	inline int32_t* get_address_of__capacity_9() { return &____capacity_9; }
	inline void set__capacity_9(int32_t value)
	{
		____capacity_9 = value;
	}

	inline static int32_t get_offset_of__expandable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____expandable_10)); }
	inline bool get__expandable_10() const { return ____expandable_10; }
	inline bool* get_address_of__expandable_10() { return &____expandable_10; }
	inline void set__expandable_10(bool value)
	{
		____expandable_10 = value;
	}

	inline static int32_t get_offset_of__writable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____writable_11)); }
	inline bool get__writable_11() const { return ____writable_11; }
	inline bool* get_address_of__writable_11() { return &____writable_11; }
	inline void set__writable_11(bool value)
	{
		____writable_11 = value;
	}

	inline static int32_t get_offset_of__exposable_12() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____exposable_12)); }
	inline bool get__exposable_12() const { return ____exposable_12; }
	inline bool* get_address_of__exposable_12() { return &____exposable_12; }
	inline void set__exposable_12(bool value)
	{
		____exposable_12 = value;
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_14() { return static_cast<int32_t>(offsetof(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C, ____lastReadTask_14)); }
	inline Task_1_tEF253D967DB628A9F8A389A9F2E4516871FD3725 * get__lastReadTask_14() const { return ____lastReadTask_14; }
	inline Task_1_tEF253D967DB628A9F8A389A9F2E4516871FD3725 ** get_address_of__lastReadTask_14() { return &____lastReadTask_14; }
	inline void set__lastReadTask_14(Task_1_tEF253D967DB628A9F8A389A9F2E4516871FD3725 * value)
	{
		____lastReadTask_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lastReadTask_14), (void*)value);
	}
};


// System.RuntimeFieldHandle
struct RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.IO.SeekOrigin
struct SeekOrigin_t4A91B37D046CD7A6578066059AE9F6269A888D4F 
{
public:
	// System.Int32 System.IO.SeekOrigin::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SeekOrigin_t4A91B37D046CD7A6578066059AE9F6269A888D4F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.Serialization.StreamingContextStates
struct StreamingContextStates_tF4C7FE6D6121BD4C67699869C8269A60B36B42C3 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_tF4C7FE6D6121BD4C67699869C8269A60B36B42C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMETP.SharpZipLib.GZip.GZipOutputStream/OutputState
struct OutputState_t324A83C08F89C45CCD395F73CA9919CA335E8F83 
{
public:
	// System.Int32 FMETP.SharpZipLib.GZip.GZipOutputStream/OutputState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OutputState_t324A83C08F89C45CCD395F73CA9919CA335E8F83, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMETP.SharpZipLib.Zip.Compression.DeflaterEngine
struct DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386  : public RuntimeObject
{
public:
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::ins_h
	int32_t ___ins_h_0;
	// System.Int16[] FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::head
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* ___head_1;
	// System.Int16[] FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::prev
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* ___prev_2;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::matchStart
	int32_t ___matchStart_3;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::matchLen
	int32_t ___matchLen_4;
	// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::prevAvailable
	bool ___prevAvailable_5;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::blockStart
	int32_t ___blockStart_6;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::strstart
	int32_t ___strstart_7;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::lookahead
	int32_t ___lookahead_8;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::window
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___window_9;
	// FMETP.SharpZipLib.Zip.Compression.DeflateStrategy FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::strategy
	int32_t ___strategy_10;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::max_chain
	int32_t ___max_chain_11;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::max_lazy
	int32_t ___max_lazy_12;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::niceLength
	int32_t ___niceLength_13;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::goodLength
	int32_t ___goodLength_14;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::compressionFunction
	int32_t ___compressionFunction_15;
	// System.Byte[] FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::inputBuf
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___inputBuf_16;
	// System.Int64 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::totalIn
	int64_t ___totalIn_17;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::inputOff
	int32_t ___inputOff_18;
	// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::inputEnd
	int32_t ___inputEnd_19;
	// FMETP.SharpZipLib.Zip.Compression.DeflaterPending FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::pending
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * ___pending_20;
	// FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::huffman
	DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * ___huffman_21;
	// FMETP.SharpZipLib.Checksum.Adler32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::adler
	Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * ___adler_22;

public:
	inline static int32_t get_offset_of_ins_h_0() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___ins_h_0)); }
	inline int32_t get_ins_h_0() const { return ___ins_h_0; }
	inline int32_t* get_address_of_ins_h_0() { return &___ins_h_0; }
	inline void set_ins_h_0(int32_t value)
	{
		___ins_h_0 = value;
	}

	inline static int32_t get_offset_of_head_1() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___head_1)); }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* get_head_1() const { return ___head_1; }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD** get_address_of_head_1() { return &___head_1; }
	inline void set_head_1(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* value)
	{
		___head_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___head_1), (void*)value);
	}

	inline static int32_t get_offset_of_prev_2() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___prev_2)); }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* get_prev_2() const { return ___prev_2; }
	inline Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD** get_address_of_prev_2() { return &___prev_2; }
	inline void set_prev_2(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* value)
	{
		___prev_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prev_2), (void*)value);
	}

	inline static int32_t get_offset_of_matchStart_3() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___matchStart_3)); }
	inline int32_t get_matchStart_3() const { return ___matchStart_3; }
	inline int32_t* get_address_of_matchStart_3() { return &___matchStart_3; }
	inline void set_matchStart_3(int32_t value)
	{
		___matchStart_3 = value;
	}

	inline static int32_t get_offset_of_matchLen_4() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___matchLen_4)); }
	inline int32_t get_matchLen_4() const { return ___matchLen_4; }
	inline int32_t* get_address_of_matchLen_4() { return &___matchLen_4; }
	inline void set_matchLen_4(int32_t value)
	{
		___matchLen_4 = value;
	}

	inline static int32_t get_offset_of_prevAvailable_5() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___prevAvailable_5)); }
	inline bool get_prevAvailable_5() const { return ___prevAvailable_5; }
	inline bool* get_address_of_prevAvailable_5() { return &___prevAvailable_5; }
	inline void set_prevAvailable_5(bool value)
	{
		___prevAvailable_5 = value;
	}

	inline static int32_t get_offset_of_blockStart_6() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___blockStart_6)); }
	inline int32_t get_blockStart_6() const { return ___blockStart_6; }
	inline int32_t* get_address_of_blockStart_6() { return &___blockStart_6; }
	inline void set_blockStart_6(int32_t value)
	{
		___blockStart_6 = value;
	}

	inline static int32_t get_offset_of_strstart_7() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___strstart_7)); }
	inline int32_t get_strstart_7() const { return ___strstart_7; }
	inline int32_t* get_address_of_strstart_7() { return &___strstart_7; }
	inline void set_strstart_7(int32_t value)
	{
		___strstart_7 = value;
	}

	inline static int32_t get_offset_of_lookahead_8() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___lookahead_8)); }
	inline int32_t get_lookahead_8() const { return ___lookahead_8; }
	inline int32_t* get_address_of_lookahead_8() { return &___lookahead_8; }
	inline void set_lookahead_8(int32_t value)
	{
		___lookahead_8 = value;
	}

	inline static int32_t get_offset_of_window_9() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___window_9)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_window_9() const { return ___window_9; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_window_9() { return &___window_9; }
	inline void set_window_9(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___window_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___window_9), (void*)value);
	}

	inline static int32_t get_offset_of_strategy_10() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___strategy_10)); }
	inline int32_t get_strategy_10() const { return ___strategy_10; }
	inline int32_t* get_address_of_strategy_10() { return &___strategy_10; }
	inline void set_strategy_10(int32_t value)
	{
		___strategy_10 = value;
	}

	inline static int32_t get_offset_of_max_chain_11() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___max_chain_11)); }
	inline int32_t get_max_chain_11() const { return ___max_chain_11; }
	inline int32_t* get_address_of_max_chain_11() { return &___max_chain_11; }
	inline void set_max_chain_11(int32_t value)
	{
		___max_chain_11 = value;
	}

	inline static int32_t get_offset_of_max_lazy_12() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___max_lazy_12)); }
	inline int32_t get_max_lazy_12() const { return ___max_lazy_12; }
	inline int32_t* get_address_of_max_lazy_12() { return &___max_lazy_12; }
	inline void set_max_lazy_12(int32_t value)
	{
		___max_lazy_12 = value;
	}

	inline static int32_t get_offset_of_niceLength_13() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___niceLength_13)); }
	inline int32_t get_niceLength_13() const { return ___niceLength_13; }
	inline int32_t* get_address_of_niceLength_13() { return &___niceLength_13; }
	inline void set_niceLength_13(int32_t value)
	{
		___niceLength_13 = value;
	}

	inline static int32_t get_offset_of_goodLength_14() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___goodLength_14)); }
	inline int32_t get_goodLength_14() const { return ___goodLength_14; }
	inline int32_t* get_address_of_goodLength_14() { return &___goodLength_14; }
	inline void set_goodLength_14(int32_t value)
	{
		___goodLength_14 = value;
	}

	inline static int32_t get_offset_of_compressionFunction_15() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___compressionFunction_15)); }
	inline int32_t get_compressionFunction_15() const { return ___compressionFunction_15; }
	inline int32_t* get_address_of_compressionFunction_15() { return &___compressionFunction_15; }
	inline void set_compressionFunction_15(int32_t value)
	{
		___compressionFunction_15 = value;
	}

	inline static int32_t get_offset_of_inputBuf_16() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___inputBuf_16)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_inputBuf_16() const { return ___inputBuf_16; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_inputBuf_16() { return &___inputBuf_16; }
	inline void set_inputBuf_16(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___inputBuf_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputBuf_16), (void*)value);
	}

	inline static int32_t get_offset_of_totalIn_17() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___totalIn_17)); }
	inline int64_t get_totalIn_17() const { return ___totalIn_17; }
	inline int64_t* get_address_of_totalIn_17() { return &___totalIn_17; }
	inline void set_totalIn_17(int64_t value)
	{
		___totalIn_17 = value;
	}

	inline static int32_t get_offset_of_inputOff_18() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___inputOff_18)); }
	inline int32_t get_inputOff_18() const { return ___inputOff_18; }
	inline int32_t* get_address_of_inputOff_18() { return &___inputOff_18; }
	inline void set_inputOff_18(int32_t value)
	{
		___inputOff_18 = value;
	}

	inline static int32_t get_offset_of_inputEnd_19() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___inputEnd_19)); }
	inline int32_t get_inputEnd_19() const { return ___inputEnd_19; }
	inline int32_t* get_address_of_inputEnd_19() { return &___inputEnd_19; }
	inline void set_inputEnd_19(int32_t value)
	{
		___inputEnd_19 = value;
	}

	inline static int32_t get_offset_of_pending_20() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___pending_20)); }
	inline DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * get_pending_20() const { return ___pending_20; }
	inline DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B ** get_address_of_pending_20() { return &___pending_20; }
	inline void set_pending_20(DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * value)
	{
		___pending_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pending_20), (void*)value);
	}

	inline static int32_t get_offset_of_huffman_21() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___huffman_21)); }
	inline DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * get_huffman_21() const { return ___huffman_21; }
	inline DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 ** get_address_of_huffman_21() { return &___huffman_21; }
	inline void set_huffman_21(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * value)
	{
		___huffman_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___huffman_21), (void*)value);
	}

	inline static int32_t get_offset_of_adler_22() { return static_cast<int32_t>(offsetof(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386, ___adler_22)); }
	inline Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * get_adler_22() const { return ___adler_22; }
	inline Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB ** get_address_of_adler_22() { return &___adler_22; }
	inline void set_adler_22(Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * value)
	{
		___adler_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adler_22), (void*)value);
	}
};


// FMETP.SharpZipLib.GZip.GZipInputStream
struct GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9  : public InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E
{
public:
	// FMETP.SharpZipLib.Checksum.Crc32 FMETP.SharpZipLib.GZip.GZipInputStream::crc
	Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * ___crc_10;
	// System.Boolean FMETP.SharpZipLib.GZip.GZipInputStream::readGZIPHeader
	bool ___readGZIPHeader_11;
	// System.Boolean FMETP.SharpZipLib.GZip.GZipInputStream::completedLastBlock
	bool ___completedLastBlock_12;

public:
	inline static int32_t get_offset_of_crc_10() { return static_cast<int32_t>(offsetof(GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9, ___crc_10)); }
	inline Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * get_crc_10() const { return ___crc_10; }
	inline Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 ** get_address_of_crc_10() { return &___crc_10; }
	inline void set_crc_10(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * value)
	{
		___crc_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___crc_10), (void*)value);
	}

	inline static int32_t get_offset_of_readGZIPHeader_11() { return static_cast<int32_t>(offsetof(GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9, ___readGZIPHeader_11)); }
	inline bool get_readGZIPHeader_11() const { return ___readGZIPHeader_11; }
	inline bool* get_address_of_readGZIPHeader_11() { return &___readGZIPHeader_11; }
	inline void set_readGZIPHeader_11(bool value)
	{
		___readGZIPHeader_11 = value;
	}

	inline static int32_t get_offset_of_completedLastBlock_12() { return static_cast<int32_t>(offsetof(GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9, ___completedLastBlock_12)); }
	inline bool get_completedLastBlock_12() const { return ___completedLastBlock_12; }
	inline bool* get_address_of_completedLastBlock_12() { return &___completedLastBlock_12; }
	inline void set_completedLastBlock_12(bool value)
	{
		___completedLastBlock_12 = value;
	}
};


// FMETP.SharpZipLib.GZip.GZipOutputStream
struct GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292  : public DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E
{
public:
	// FMETP.SharpZipLib.Checksum.Crc32 FMETP.SharpZipLib.GZip.GZipOutputStream::crc
	Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * ___crc_12;
	// FMETP.SharpZipLib.GZip.GZipOutputStream/OutputState FMETP.SharpZipLib.GZip.GZipOutputStream::state_
	int32_t ___state__13;

public:
	inline static int32_t get_offset_of_crc_12() { return static_cast<int32_t>(offsetof(GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292, ___crc_12)); }
	inline Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * get_crc_12() const { return ___crc_12; }
	inline Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 ** get_address_of_crc_12() { return &___crc_12; }
	inline void set_crc_12(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * value)
	{
		___crc_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___crc_12), (void*)value);
	}

	inline static int32_t get_offset_of_state__13() { return static_cast<int32_t>(offsetof(GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292, ___state__13)); }
	inline int32_t get_state__13() const { return ___state__13; }
	inline int32_t* get_address_of_state__13() { return &___state__13; }
	inline void set_state__13(int32_t value)
	{
		___state__13 = value;
	}
};


// FMETP.SharpZipLib.SharpZipBaseException
struct SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B  : public Exception_t
{
public:

public:
};


// System.Runtime.Serialization.StreamingContext
struct StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_additionalContext_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// FMETP.SharpZipLib.GZip.GZipException
struct GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4  : public SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B
{
public:

public:
};


// System.IO.IOException
struct IOException_t09E5C01DA4748C36D703728C4668C5CDF3882EBA  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_17;

public:
	inline static int32_t get_offset_of__maybeFullPath_17() { return static_cast<int32_t>(offsetof(IOException_t09E5C01DA4748C36D703728C4668C5CDF3882EBA, ____maybeFullPath_17)); }
	inline String_t* get__maybeFullPath_17() const { return ____maybeFullPath_17; }
	inline String_t** get_address_of__maybeFullPath_17() { return &____maybeFullPath_17; }
	inline void set__maybeFullPath_17(String_t* value)
	{
		____maybeFullPath_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____maybeFullPath_17), (void*)value);
	}
};


// System.InvalidOperationException
struct InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// FMETP.SharpZipLib.StreamDecodingException
struct StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C  : public SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B
{
public:

public:
};


// System.ArgumentNullException
struct ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:

public:
};


// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_actualValue_19), (void*)value);
	}
};

struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rangeMessage_18), (void*)value);
	}
};


// System.IO.EndOfStreamException
struct EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059  : public IOException_t09E5C01DA4748C36D703728C4668C5CDF3882EBA
{
public:

public:
};


// FMETP.SharpZipLib.ValueOutOfRangeException
struct ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07  : public StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int16[]
struct Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int16_t m_Items[1];

public:
	inline int16_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int16_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int16_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int16_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int16_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int16_t value)
	{
		m_Items[index] = value;
	}
};


// System.Int32 System.ArraySegment`1<System.Byte>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<System.Byte>::get_Offset()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method);
// !0[] System.ArraySegment`1<System.Byte>::get_Array()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method);
// System.Void System.ArraySegment`1<System.Byte>::.ctor(!0[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_gshared (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___array0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Checksum.Adler32::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Adler32_Reset_mA0BDC81A3B31E939D321A24040D47BBCEACECC22 (Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * __this, const RuntimeMethod* method);
// System.Int32 System.ArraySegment`1<System.Byte>::get_Count()
inline int32_t ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *, const RuntimeMethod*))ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_gshared_inline)(__this, method);
}
// System.Int32 System.ArraySegment`1<System.Byte>::get_Offset()
inline int32_t ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *, const RuntimeMethod*))ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_gshared_inline)(__this, method);
}
// !0[] System.ArraySegment`1<System.Byte>::get_Array()
inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	return ((  ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* (*) (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *, const RuntimeMethod*))ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_gshared_inline)(__this, method);
}
// System.Void FMETP.SharpZipLib.Checksum.Crc32::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Crc32_Reset_m6CB0530F210EBBEE3AA26D0EB48753B858A9AA55 (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Checksum.Crc32::Update(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * __this, int32_t ___bval0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F (RuntimeArray * ___array0, RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  ___fldHandle1, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterPending::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterPending__ctor_m5F119C4BFC420BEB34433D4F5422C464B159B6A7 (DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterPending,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine__ctor_m1A08402452F08A53C08973444EBAB7435D5CC37A (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * ___pending0, bool ___noAdlerCalculation1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetStrategy(FMETP.SharpZipLib.Zip.Compression.DeflateStrategy)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_SetStrategy_mAC8771AF03A22E52E55CC5353F334E831F51B2D0 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, int32_t ___strategy0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_SetLevel_m05D7A3C79B57A8AF9C72613CD730EC90FAB4CE4D (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, int32_t ___level0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_Reset_m84EBDB6798E67F0321AEF102C98B90355A38D74F (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_Reset_mC6B583113FB3D8C9D12FA178435E7CAA2E1327BB (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_Reset_mC49C3D98C62110B5453E75554DF402BE6C776CCE (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method);
// System.Int64 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::get_TotalIn()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int64_t DeflaterEngine_get_TotalIn_mC27F3CA9F01B1C1974B8DCFF91D7DE9B816F26FD_inline (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.PendingBuffer::get_IsFlushed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PendingBuffer_get_IsFlushed_m64096BA3F7B56951266958E6233A77C74844D569 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::NeedsInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_NeedsInput_mDDBEA2E842B74A55BB42B0DD6790EE4D5A8ED5A0 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SetInput(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SetLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_SetLevel_m2EB416322DD10370B059672DB7C652B8ED1A54DC (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, int32_t ___level0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::set_Strategy(FMETP.SharpZipLib.Zip.Compression.DeflateStrategy)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DeflaterEngine_set_Strategy_m49FD868FE9D1242660EA59FD2C03881C34FF68AB_inline (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteShortMSB(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_WriteShortMSB_m7597C12105E7CEE947A8CFDE511DD1CB805B3CD9 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, int32_t ___s0, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::get_Adler()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DeflaterEngine_get_Adler_m778B43EAEE4DE749764D60C3A970BFFCB36814CB (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::ResetAdler()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_ResetAdler_mFFE9B0AFB523F170C8523221606BFF85713DA012 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::Flush(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PendingBuffer_Flush_m828B34DC184E3CFC8595E381C70B0068CBD5CDFA (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___output0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::Deflate(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_Deflate_m9DD2CC475173BB34AE655813C1EDE6999B261099 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, bool ___flush0, bool ___finish1, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::get_BitCount()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PendingBuffer_get_BitCount_mB4A917AF510467CF020B8E173785361118CF9F41_inline (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteBits(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, int32_t ___b0, int32_t ___count1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::AlignToByte()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_AlignToByte_m17AE6E041B765E2F686F17560EE7D22E763271D4 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, const RuntimeMethod* method);
// System.Int32 System.Math::Min(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterPending)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman__ctor_m2B19A99445EFEB9B74D2153F80E7F4DC5792F4E1 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * ___pending0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Checksum.Adler32::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Adler32__ctor_m83B2F2CA75BAC4A379FC88436032D5E4CD15EADF (Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::FillWindow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_FillWindow_m6F85F39A3609EFAA52BEA5CDBE91FFCC81363544 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateStored(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_DeflateStored_mCA925305F6F3CC7515FE55DD3ABBF515DC038F41 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, bool ___flush0, bool ___finish1, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateFast(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_DeflateFast_mBF7F0E5FE39F8D6E4008A05387BE7D37CC1BD0EE (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, bool ___flush0, bool ___finish1, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateSlow(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_DeflateSlow_m7954800AA1BE94A4BEF695D92CFEC124209018A3 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, bool ___flush0, bool ___finish1, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97 (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman_Reset_m695094C3E44BCC5197F58D2CF7F0CCAA51AB8599 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, const RuntimeMethod* method);
// System.Int64 FMETP.SharpZipLib.Checksum.Adler32::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t Adler32_get_Value_mB072B09485C331979F68992167A098E33F9C26EB (Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushStoredBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman_FlushStoredBlock_m9E4ABC1CFA682400BB19AEA57450D550B7F450D8 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___stored0, int32_t ___storedOffset1, int32_t ___storedLength2, bool ___lastBlock3, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::UpdateHash()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_UpdateHash_m5D5500B13BFF64919559AE7022C08A9F9A5C8D09 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman_FlushBlock_m3480AE4CB9E565601FE9618FAD10F25972C19AAA (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___stored0, int32_t ___storedOffset1, int32_t ___storedLength2, bool ___lastBlock3, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyLit(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterHuffman_TallyLit_m054CCF260A35DB550E760EC54483DBCAE9DC699A (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, int32_t ___literal0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SlideWindow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_SlideWindow_mDBA845C625026D2D96B768ED9754DD79D5954F11 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method);
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877 (RuntimeArray * ___sourceArray0, int32_t ___sourceIndex1, RuntimeArray * ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const RuntimeMethod* method);
// System.Void System.ArraySegment`1<System.Byte>::.ctor(!0[],System.Int32,System.Int32)
inline void ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___array0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	((  void (*) (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t, const RuntimeMethod*))ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_gshared)(__this, ___array0, ___offset1, ___count2, method);
}
// System.Void FMETP.SharpZipLib.Checksum.Adler32::Update(System.ArraySegment`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Adler32_Update_m97D8416919DC739C90BEE5CE6F50C85521246A58 (Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * __this, ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  ___segment0, const RuntimeMethod* method);
// System.Int32 System.Math::Max(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Max_mD8AA27386BF012C65303FCDEA041B0CC65056E7B (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::InsertString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DeflaterEngine_InsertString_m5D0EB65FCC71B2A30AD3699AFB65378EAFDF1B2A (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::FindLongestMatch(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_FindLongestMatch_m1835826D27ABAB7A5A5D59A6E774233045C8245B (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, int32_t ___curMatch0, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyDist(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterHuffman_TallyDist_mAE5F24A91BE5C005AB7F8669A06D274B815099D6 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, int32_t ___distance0, int32_t ___length1, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::IsFull()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterHuffman_IsFull_mAC09A062E0A78CCD17FAE0260C22B7F79374BA42 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, const RuntimeMethod* method);
// System.Int16 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::BitReverse(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B (int32_t ___toReverse0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree__ctor_mC175FAE853657848BE82C5A0C72FC6418B9C81B3 (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * ___dh0, int32_t ___elems1, int32_t ___minCodes2, int32_t ___maxLength3, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_Reset_m88D2AC4D14EE34A72036C8440EE3ADFCDAB75698 (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildCodes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_BuildCodes_mA2357168306F9D5F06255D9ED89B1C646039FBDC (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteTree(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_WriteTree_mAE4872807EDE476D00318D47C89ED01F9E95882A (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * ___blTree0, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Lcode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DeflaterHuffman_Lcode_m366E178D7EAF0A531BC9970CA28376AD2186D528 (int32_t ___length0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteSymbol(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, int32_t ___code0, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Dcode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DeflaterHuffman_Dcode_m606D9F263A5BB3ABAC73BD88317213EA3728CD1A (int32_t ___distance0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteShort(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_WriteShort_mFD2AE37F8EC3E199F8E16E360B785246C287552B (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteBlock(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_WriteBlock_m139A76BB21F2AA07C21B3B701F651A7B24903840 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___block0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildTree()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_BuildTree_m5FF35C4C61B273025E9B99F2B6C17AFDF1C917E4 (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::CalcBLFreq(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_CalcBLFreq_m81CFB0D0DE3A835DF438203762616C9ECBA4B7AC (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * ___blTree0, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::GetEncodedLength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Tree_GetEncodedLength_m54044E0BBD9F68EFA7B3B2EE99BB131B45993997 (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::SetStaticCodes(System.Int16[],System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_SetStaticCodes_m0527B8E8E0520CA78EF84CB97DA604434F6C63EA (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* ___staticCodes0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___staticLengths1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::CompressBlock()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman_CompressBlock_m59A757C733FEAA3CE8288F098417E05A31E35D78 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::SendAllTrees(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman_SendAllTrees_m012E9E03627932FA1908F0B9A93872973802A748 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, int32_t ___blTreeCodes0, const RuntimeMethod* method);
// System.Void System.IO.Stream::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stream__ctor_m5EB0B4BCC014E7D1F18FE0E72B2D6D0C5C13D5C4 (Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * __this, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m71044C2110E357B71A1C30D2561C3F861AF1DC0D (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Finish()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_Finish_mBB8D02EF15BCCC6A0DCD0560C80BC7F707824F60 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Deflater::Deflate(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Deflater_Deflate_m4B828E32CABE4976218147C102256B124DBB3A9C (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___output0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::EncryptBlock(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_EncryptBlock_m6C13E0059BA831A5AE1863991316BDE10F96ACF9 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Deflater::get_IsFinished()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Deflater_get_IsFinished_m84E8F8D8DE052B1016C5E57958E60F271146D446 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914 (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_Deflate_mFD09C1AC5EC2990148643BDA9FDED4A88E515B0B (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, bool ___flushing0, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Deflater::get_IsNeedingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Deflater_get_IsNeedingInput_mB8C863FBF533F2C8BD42C9728907E092EDAEA899 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90 (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Flush()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_Flush_mCC6C370B48AC60F627C8A37E0363D76C951E4B07 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::GetAuthCodeIfAES()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_GetAuthCodeIfAES_mF293FA59B514BCD46C50C97764D534CB1191EB54 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_IsStreamOwner()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool DeflaterOutputStream_get_IsStreamOwner_m3023CE9D69403AC66C712F5CC82EB88D8B395E61_inline (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method);
// System.Void System.IO.Stream::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stream_Dispose_m117324084DDAD414761AD29FB17A419840BA6EA0 (Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetInput(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_SetInput_mD20D223304B17FE2D9637620A767625C0A3619A8 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___input0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_Deflate_m8300F455B747EED583C35BE29A3D84A71FF42B7B (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method);
// System.Security.Cryptography.RandomNumberGenerator System.Security.Cryptography.RandomNumberGenerator::Create()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * RandomNumberGenerator_Create_m04A5418F8572F0498EE0659633B4C0620CB55721 (const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer__ctor_mD43BCBDDD48F118DE0DA5A277D973B569C0D6041 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, int32_t ___bufferSize0, const RuntimeMethod* method);
// System.Void System.IO.MemoryStream::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MemoryStream__ctor_mD27B3DF2400D46A4A81EE78B0BD2C29EFCFAA44F (MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipOutputStream__ctor_m6E95590355B8E138591499632751259F020161A6 (GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseOutputStream0, const RuntimeMethod* method);
// System.Void System.IO.BinaryWriter::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryWriter__ctor_mC6F89939E04734FBEEA375D7E0FF9C042E4AB71A (BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___output0, const RuntimeMethod* method);
// System.Void System.IO.MemoryStream::.ctor(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MemoryStream__ctor_m3E041ADD3DB7EA42E7DB56FE862097819C02B9C2 (MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipInputStream__ctor_mC075983580E627945130BC69DE408678F68542FA (GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseInputStream0, const RuntimeMethod* method);
// System.Void System.IO.BinaryReader::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BinaryReader__ctor_m8D2F966D44EF5BD30D54D94653A831EFDB9C6A60 (BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___input0, const RuntimeMethod* method);
// System.Byte[] FMZipHelper::ReadAllBytes(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* FMZipHelper_ReadAllBytes_m6AC058706643D287F7BB6AD9A4E793F0E58D8277 (BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * ___reader0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SharpZipBaseException__ctor_mEE9B481134AA334BBF8777D11D8001825E16A988 (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SharpZipBaseException__ctor_m8A2F854EAC4E1806C22166372CE8BD8A7DA0502D (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipInputStream__ctor_m826EE21BB68D245D97F7BC8BD658370543038BD0 (GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseInputStream0, int32_t ___size1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Inflater__ctor_m203485CA9DDAD7143A3459BD6BA7D479FA7B1C69 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, bool ___noHeader0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,FMETP.SharpZipLib.Zip.Compression.Inflater,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputStream__ctor_m152F154E3EB66C5B8E82F049C1ED5B37E52A19F8 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseInputStream0, Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * ___inflater1, int32_t ___bufferSize2, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.GZip.GZipInputStream::ReadHeader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F (GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Read(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InflaterInputStream_Read_mC36B703875162B5FAB4DC4F47A05AAD596331F50 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Checksum.Crc32::Update(System.ArraySegment`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Crc32_Update_m6521C75BD473D1869DA1C3F3D00C936F07D3957D (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * __this, ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  ___segment0, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsFinished()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_get_IsFinished_m90C7D1B230EEEB2150FA15482E9F6A97AF168812 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.GZip.GZipInputStream::ReadFooter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipInputStream_ReadFooter_m5B658299CE668EC4346D6EA9571AD17532B9AC45 (GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Checksum.Crc32::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Crc32__ctor_m69D0E66F79D9E4A6937BA772C21E6D3EDA9B6F01 (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t InflaterInputBuffer_get_Available_m7BDBC3F8048C19BF8C9D3E253BBFDA8601A107DE_inline (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputBuffer_Fill_mD6D37972021807E5F4615E4D33276A13D2D64925 (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894 (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, const RuntimeMethod* method);
// System.Void System.IO.EndOfStreamException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800 (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.GZip.GZipException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Int64 FMETP.SharpZipLib.Checksum.Crc32::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t Crc32_get_Value_m624503DF9F1044753BE1CB1AD489D7DA5035587B (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * __this, const RuntimeMethod* method);
// System.Int64 FMETP.SharpZipLib.Zip.Compression.Inflater::get_TotalOut()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int64_t Inflater_get_TotalOut_m24899EBBA3839BC2A41C59359686D6CB758576F0_inline (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::get_RemainingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Inflater_get_RemainingInput_m1D9B9068C5E2A64081E9CA6F3AA11FDE926B6102 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InflaterInputBuffer_set_Available_mB9B0F342C4B3FB90CCFC0F34B4C5CEDDB7104981_inline (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Inflater_Reset_mCBF0E4780E8532B200538A1D7601C6EEEE9DA97C (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InflaterInputBuffer_ReadClearTextBuffer_m91A4E800CE258802DA172CC80C03A08E919411A5 (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___outBuffer0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipOutputStream__ctor_mCD36645CB0B363E3E72E92182F18C6ED80873E28 (GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseOutputStream0, int32_t ___size1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::.ctor(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater__ctor_m698267A3FB8383CEDAA02AA133076DF05411CDA8 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, int32_t ___level0, bool ___noZlibHeaderOrFooter1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,FMETP.SharpZipLib.Zip.Compression.Deflater,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream__ctor_mB41165AE84240A90382E4B53F11B299A4CDD58A1 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseOutputStream0, Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * ___deflater1, int32_t ___bufferSize2, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::WriteHeader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipOutputStream_WriteHeader_mADDFD2A440ACC6DE89CD4CA229C496D790BE0C55 (GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Write(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_Write_m3B8E53BB694B9FEFBDAA9FF03BE50F4A41BDF12A (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_Finish_m0B586B7D62DFF8763BA25CB632AA7DD4F2EC2499 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method);
// System.Int64 FMETP.SharpZipLib.Zip.Compression.Deflater::get_TotalIn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t Deflater_get_TotalIn_m361FC4798E6EDA1D0A1E980CFDD628B211815A62 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method);
// System.DateTime System.DateTime::get_Now()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C (const RuntimeMethod* method);
// System.Int64 System.DateTime::get_Ticks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t DateTime_get_Ticks_m175EDB41A50DB06872CC48CAB603FEBD1DFF46A9 (DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * __this, const RuntimeMethod* method);
// System.Void System.DateTime::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DateTime__ctor_m1AD9E79A671864DFB1AABDB75D207C688B868D88 (DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * __this, int32_t ___year0, int32_t ___month1, int32_t ___day2, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamManipulator__ctor_mC16675A2CEAB3EE9F281DC7F5B59D4DF5DC77069 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputWindow__ctor_mA9477B707FFBACD81303FAF7FE2F7C658990FF30 (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamManipulator_Reset_m7379C771C7C78B44A155BAD54952808BD4F0B5EA (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputWindow_Reset_mB97240B42A56D72E549B1967E7646C715C8899E4 (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::PeekBits(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, int32_t ___bitCount0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::DropBits(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, int32_t ___bitCount0, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetFreeSpace()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OutputWindow_GetFreeSpace_m8FDF473C07DD2A7E667C0D4F9A52657DDD06B99B (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Write(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputWindow_Write_m8E87F3E22FAA5D8B122FB61BD4DF14D07F2E42C6 (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::GetSymbol(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InflaterHuffmanTree_GetSymbol_m0F2DAE53C30D2A81AC3ED21E476F2D3451728442 (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * __this, StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * ___input0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Repeat(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputWindow_Repeat_mD7467DB942F68DACD17A6A466A2F7974AD2969A5 (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, int32_t ___length0, int32_t ___distance1, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeHeader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_DecodeHeader_mB7913D25A9125D416A703F502EFAB4707D10C92C (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeDict()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_DecodeDict_m653948520B90CBEFF564A8C76DF83D897026373E (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeChksum()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_DecodeChksum_m82FFF42FBBBDFD86E8EBE560EEB21E7EA9B49DCC (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SkipToByteBoundary()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamManipulator_SkipToByteBoundary_m7C36C130332D5995F8938FFC6F8A45478B77A47C (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::.ctor(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterDynHeader__ctor_mE2911B9C7C908A0691E08CBCDCFB9573644BCBD9 (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * __this, StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * ___input0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyStored(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OutputWindow_CopyStored_m7676CBB69C573A09CED8B5EB799DC3C8759E6BDF (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * ___input0, int32_t ___length1, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_IsNeedingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StreamManipulator_get_IsNeedingInput_m962D2BBB56BD8BAC834E2A12897C13A7D4DCE456 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::AttemptRead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InflaterDynHeader_AttemptRead_m4593F6F078B40FEC075F5760125F157BD564AC53 (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * __this, const RuntimeMethod* method);
// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::get_LiteralLengthTree()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * InflaterDynHeader_get_LiteralLengthTree_m79581D6BBB306802E0B9BCD492B4D4285509F9F2 (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * __this, const RuntimeMethod* method);
// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::get_DistanceTree()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * InflaterDynHeader_get_DistanceTree_m4F6820D7E6C0EB2A9A37807FC2241A66361E05F6 (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeHuffman()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_DecodeHuffman_mAB4BC68E10CE0BC0F737716E1B52EA85096BC740 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SetInput(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005 (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * __this, String_t* ___paramName0, String_t* ___message1, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::Decode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_Decode_m58AF8D0040DD4AA68E0E477F77674C329E2D01CC (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyOutput(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OutputWindow_CopyOutput_m256CF0DA13981FB04C8D0DB6538E622E11FBEA34 (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___output0, int32_t ___offset1, int32_t ___len2, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetAvailable()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OutputWindow_GetAvailable_m00EBEEB3AAAEB4C919C63615591FAF21BB60BBEC_inline (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBytes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StreamManipulator_get_AvailableBytes_m4993F192D46EF3A516A6E364BDE5E952D8281457 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<System.Boolean> FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::CreateStateMachine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InflaterDynHeader_CreateStateMachine_mDFFFFAC859E480605B8D903AF0985F656B0FDE1C (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateStateMachineU3Ed__7__ctor_mDA5BCC7D91C9FE2D8B24060145C553D67CBC0A2B (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamDecodingException__ctor_mE34C08B6F04E109D9A4E0B2DA3DB7C67555C5CE1 (StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.ctor(System.Collections.Generic.IList`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterHuffmanTree__ctor_m8979A23B855128F705F710C36B32AB863EADF1D5 (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * __this, RuntimeObject* ___codeLengths0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::BuildTree(System.Collections.Generic.IList`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterHuffmanTree_BuildTree_mB50BCB289AD6E0A6F2D346F25AF26AFAE424EAD9 (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * __this, RuntimeObject* ___codeLengths0, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBits()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t StreamManipulator_get_AvailableBits_m7FA2836285F2C2BA6B74A8AA135D770DB9030A7B_inline (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Inflater_SetInput_mEF7B47FADA151D32D6020DC409B36CF51A22F849 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___index1, int32_t ___count2, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11 (Exception_t * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputBuffer__ctor_m497E472D25D2E3D02803F5561C2A1C80477EBE26 (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, int32_t ___bufferSize1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(FMETP.SharpZipLib.Zip.Compression.Inflater)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputBuffer_SetInflaterInput_m84191E11B0A3E51A5F01CC1CA537E8915FB9243F (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * ___inflater0, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_IsStreamOwner()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool InflaterInputStream_get_IsStreamOwner_m3C551CED06177186C1D848E92B2E4B8F2C879814_inline (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingDictionary()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_get_IsNeedingDictionary_mC165F0D2C014EECB04B5CD95E1875610942D4155 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Inflater_Inflate_m21F470F4D5A79E52EDA46572AEB52DD878A3C424 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_get_IsNeedingInput_mD762AD35E335CCC57F14DC3304BF50A845676F16 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Fill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputStream_Fill_m1DE4E5EC14785EFD25C866FBD31A46A1144445F2 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::SlowRepeat(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputWindow_SlowRepeat_m12DFD2153B9406457D3D8DDCF4C8CB5C8B78E9FD (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, int32_t ___repStart0, int32_t ___length1, int32_t ___distance2, const RuntimeMethod* method);
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::CopyBytes(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StreamManipulator_CopyBytes_m6FA30492A0A8534309A1538CF230500658CA3C79 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___output0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m26BD2B620B5FBFA4376C16011C60E18A2EDC8E96 (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * __this, const RuntimeMethod* method);
// System.Void System.Exception::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B (Exception_t * __this, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m0CD24092BF55B8EDE25AED989ACADB80298EF917 (Exception_t * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamDecodingException__ctor_m4F10EB31C40C7AC98DF71DCA43F9D59F7ABDBFED (StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C * __this, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamDecodingException__ctor_m402438841B691D097F1B428835488DB9CF0D4826 (StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildLength(System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_BuildLength_m85D4045DCC50CC1D520AD0FC0C9486F28BC9F0CD (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___childs0, const RuntimeMethod* method);
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * Thread_get_CurrentThread_m80236D2457FBCC1F76A08711E059A0B738DA71EC (const RuntimeMethod* method);
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Thread_get_ManagedThreadId_m7818C94F78A2DE2C7C278F6EA24B31F2BB758FD0 (Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * __this, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::TryGetBits(System.Int32,System.Int32&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StreamManipulator_TryGetBits_mC4C8072E3A5991B94049F9C3F9382E2D5B0B9D96 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, int32_t ___bitCount0, int32_t* ___output1, int32_t ___outputOffset2, const RuntimeMethod* method);
// System.Void FMETP.SharpZipLib.ValueOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueOutOfRangeException__ctor_mEA093740F1E941798A6138627AEF47268840D542 (ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07 * __this, String_t* ___nameOfValue0, const RuntimeMethod* method);
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::TryGetBits(System.Int32,System.Byte[]&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StreamManipulator_TryGetBits_mEB43579CCD99015132323748F193722297150632 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, int32_t ___bitCount0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** ___array1, int32_t ___index2, const RuntimeMethod* method);
// System.Void System.Buffer::BlockCopy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725 (RuntimeArray * ___src0, int32_t ___srcOffset1, RuntimeArray * ___dst2, int32_t ___dstOffset3, int32_t ___count4, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<System.Boolean> FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.Generic.IEnumerable<System.Boolean>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumerableU3CSystem_BooleanU3E_GetEnumerator_m167460EFE831503FB0E0A7B18C3612A68229266F (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Checksum.Adler32::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Adler32__ctor_m83B2F2CA75BAC4A379FC88436032D5E4CD15EADF (Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		Adler32_Reset_mA0BDC81A3B31E939D321A24040D47BBCEACECC22(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Checksum.Adler32::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Adler32_Reset_mA0BDC81A3B31E939D321A24040D47BBCEACECC22 (Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * __this, const RuntimeMethod* method)
{
	{
		__this->set_checkValue_1(1);
		return;
	}
}
// System.Int64 FMETP.SharpZipLib.Checksum.Adler32::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t Adler32_get_Value_mB072B09485C331979F68992167A098E33F9C26EB (Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * __this, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = __this->get_checkValue_1();
		return ((int64_t)((uint64_t)L_0));
	}
}
// System.Void FMETP.SharpZipLib.Checksum.Adler32::Update(System.ArraySegment`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Adler32_Update_m97D8416919DC739C90BEE5CE6F50C85521246A58 (Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * __this, ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  ___segment0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		uint32_t L_0 = __this->get_checkValue_1();
		V_0 = ((int32_t)((int32_t)L_0&(int32_t)((int32_t)65535)));
		uint32_t L_1 = __this->get_checkValue_1();
		V_1 = ((int32_t)((uint32_t)L_1>>((int32_t)16)));
		int32_t L_2;
		L_2 = ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_inline((ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *)(&___segment0), /*hidden argument*/ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_RuntimeMethod_var);
		V_2 = L_2;
		int32_t L_3;
		L_3 = ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_inline((ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *)(&___segment0), /*hidden argument*/ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_RuntimeMethod_var);
		V_3 = L_3;
		goto IL_0073;
	}

IL_0029:
	{
		V_4 = ((int32_t)3800);
		int32_t L_4 = V_4;
		int32_t L_5 = V_2;
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_6 = V_2;
		V_4 = L_6;
	}

IL_0038:
	{
		int32_t L_7 = V_2;
		int32_t L_8 = V_4;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)L_8));
		goto IL_0059;
	}

IL_003f:
	{
		uint32_t L_9 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10;
		L_10 = ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_inline((ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *)(&___segment0), /*hidden argument*/ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_RuntimeMethod_var);
		int32_t L_11 = V_3;
		int32_t L_12 = L_11;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
		NullCheck(L_10);
		int32_t L_13 = L_12;
		uint8_t L_14 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)((int32_t)((int32_t)L_14&(int32_t)((int32_t)255)))));
		uint32_t L_15 = V_1;
		uint32_t L_16 = V_0;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)L_16));
	}

IL_0059:
	{
		int32_t L_17 = V_4;
		int32_t L_18 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)1));
		V_4 = L_18;
		if ((((int32_t)L_18) >= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		uint32_t L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var);
		uint32_t L_20 = ((Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_StaticFields*)il2cpp_codegen_static_fields_for(Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var))->get_BASE_0();
		V_0 = ((int32_t)((uint32_t)(int32_t)L_19%(uint32_t)(int32_t)L_20));
		uint32_t L_21 = V_1;
		uint32_t L_22 = ((Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_StaticFields*)il2cpp_codegen_static_fields_for(Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var))->get_BASE_0();
		V_1 = ((int32_t)((uint32_t)(int32_t)L_21%(uint32_t)(int32_t)L_22));
	}

IL_0073:
	{
		int32_t L_23 = V_2;
		if ((((int32_t)L_23) > ((int32_t)0)))
		{
			goto IL_0029;
		}
	}
	{
		uint32_t L_24 = V_1;
		uint32_t L_25 = V_0;
		__this->set_checkValue_1(((int32_t)((int32_t)((int32_t)((int32_t)L_24<<(int32_t)((int32_t)16)))|(int32_t)L_25)));
		return;
	}
}
// System.Void FMETP.SharpZipLib.Checksum.Adler32::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Adler32__cctor_m2C0D47239F5F537E57B30315489B79E9895614C8 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_StaticFields*)il2cpp_codegen_static_fields_for(Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var))->set_BASE_0(((int32_t)65521));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Checksum.Crc32::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Crc32__ctor_m69D0E66F79D9E4A6937BA772C21E6D3EDA9B6F01 (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		Crc32_Reset_m6CB0530F210EBBEE3AA26D0EB48753B858A9AA55(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Checksum.Crc32::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Crc32_Reset_m6CB0530F210EBBEE3AA26D0EB48753B858A9AA55 (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		uint32_t L_0 = ((Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_StaticFields*)il2cpp_codegen_static_fields_for(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var))->get_crcInit_0();
		__this->set_checkValue_3(L_0);
		return;
	}
}
// System.Int64 FMETP.SharpZipLib.Checksum.Crc32::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t Crc32_get_Value_m624503DF9F1044753BE1CB1AD489D7DA5035587B (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		uint32_t L_0 = __this->get_checkValue_3();
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		uint32_t L_1 = ((Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_StaticFields*)il2cpp_codegen_static_fields_for(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var))->get_crcXor_1();
		return ((int64_t)((uint64_t)((uint32_t)((uint32_t)((int32_t)((int32_t)L_0^(int32_t)L_1))))));
	}
}
// System.Void FMETP.SharpZipLib.Checksum.Crc32::Update(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * __this, int32_t ___bval0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_0 = ((Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_StaticFields*)il2cpp_codegen_static_fields_for(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var))->get_crcTable_2();
		uint32_t L_1 = __this->get_checkValue_3();
		int32_t L_2 = ___bval0;
		if ((int64_t)(((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)((uint64_t)L_1))^(int64_t)((int64_t)((int64_t)L_2))))&(int64_t)((int64_t)((int64_t)((int32_t)255)))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A_RuntimeMethod_var);
		NullCheck(L_0);
		intptr_t L_3 = ((intptr_t)((int64_t)((int64_t)((int64_t)((int64_t)((int64_t)((uint64_t)L_1))^(int64_t)((int64_t)((int64_t)L_2))))&(int64_t)((int64_t)((int64_t)((int32_t)255))))));
		uint32_t L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		uint32_t L_5 = __this->get_checkValue_3();
		__this->set_checkValue_3(((int32_t)((int32_t)L_4^(int32_t)((int32_t)((uint32_t)L_5>>8)))));
		return;
	}
}
// System.Void FMETP.SharpZipLib.Checksum.Crc32::Update(System.ArraySegment`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Crc32_Update_m6521C75BD473D1869DA1C3F3D00C936F07D3957D (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * __this, ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  ___segment0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0;
		L_0 = ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_inline((ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *)(&___segment0), /*hidden argument*/ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_RuntimeMethod_var);
		V_0 = L_0;
		int32_t L_1;
		L_1 = ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_inline((ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *)(&___segment0), /*hidden argument*/ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_RuntimeMethod_var);
		V_1 = L_1;
		goto IL_0025;
	}

IL_0012:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2;
		L_2 = ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_inline((ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE *)(&___segment0), /*hidden argument*/ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_RuntimeMethod_var);
		int32_t L_3 = V_1;
		int32_t L_4 = L_3;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
		NullCheck(L_2);
		int32_t L_5 = L_4;
		uint8_t L_6 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(__this, L_6, /*hidden argument*/NULL);
	}

IL_0025:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)1));
		V_0 = L_8;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Checksum.Crc32::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Crc32__cctor_m28D994FACF6F0B95FD78D6FC2F9284222B0CEF69 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____373B494F210C656134C5728D551D4C97B013EB33_3_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_StaticFields*)il2cpp_codegen_static_fields_for(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var))->set_crcInit_0((-1));
		((Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_StaticFields*)il2cpp_codegen_static_fields_for(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var))->set_crcXor_1((-1));
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_0 = (UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF*)SZArrayNew(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256));
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_1 = L_0;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____373B494F210C656134C5728D551D4C97B013EB33_3_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1, L_2, /*hidden argument*/NULL);
		((Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_StaticFields*)il2cpp_codegen_static_fields_for(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var))->set_crcTable_2(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::.ctor(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater__ctor_m698267A3FB8383CEDAA02AA133076DF05411CDA8 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, int32_t ___level0, bool ___noZlibHeaderOrFooter1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___level0;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_000f;
		}
	}
	{
		___level0 = 6;
		goto IL_0025;
	}

IL_000f:
	{
		int32_t L_1 = ___level0;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = ___level0;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)9))))
		{
			goto IL_0025;
		}
	}

IL_0018:
	{
		String_t* L_3;
		L_3 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___level0), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_4 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Deflater__ctor_m698267A3FB8383CEDAA02AA133076DF05411CDA8_RuntimeMethod_var)));
	}

IL_0025:
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_5 = (DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B *)il2cpp_codegen_object_new(DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B_il2cpp_TypeInfo_var);
		DeflaterPending__ctor_m5F119C4BFC420BEB34433D4F5422C464B159B6A7(L_5, /*hidden argument*/NULL);
		__this->set_pending_4(L_5);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_6 = __this->get_pending_4();
		bool L_7 = ___noZlibHeaderOrFooter1;
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_8 = (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 *)il2cpp_codegen_object_new(DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386_il2cpp_TypeInfo_var);
		DeflaterEngine__ctor_m1A08402452F08A53C08973444EBAB7435D5CC37A(L_8, L_6, L_7, /*hidden argument*/NULL);
		__this->set_engine_5(L_8);
		bool L_9 = ___noZlibHeaderOrFooter1;
		__this->set_noZlibHeaderOrFooter_1(L_9);
		Deflater_SetStrategy_mAC8771AF03A22E52E55CC5353F334E831F51B2D0(__this, 0, /*hidden argument*/NULL);
		int32_t L_10 = ___level0;
		Deflater_SetLevel_m05D7A3C79B57A8AF9C72613CD730EC90FAB4CE4D(__this, L_10, /*hidden argument*/NULL);
		Deflater_Reset_m84EBDB6798E67F0321AEF102C98B90355A38D74F(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_Reset_m84EBDB6798E67F0321AEF102C98B90355A38D74F (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method)
{
	Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * G_B2_0 = NULL;
	Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * G_B3_1 = NULL;
	{
		bool L_0 = __this->get_noZlibHeaderOrFooter_1();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000c:
	{
		G_B3_0 = ((int32_t)16);
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_state_2(G_B3_0);
		__this->set_totalOut_3(((int64_t)((int64_t)0)));
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_1 = __this->get_pending_4();
		NullCheck(L_1);
		PendingBuffer_Reset_mC6B583113FB3D8C9D12FA178435E7CAA2E1327BB(L_1, /*hidden argument*/NULL);
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_2 = __this->get_engine_5();
		NullCheck(L_2);
		DeflaterEngine_Reset_mC49C3D98C62110B5453E75554DF402BE6C776CCE(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int64 FMETP.SharpZipLib.Zip.Compression.Deflater::get_TotalIn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t Deflater_get_TotalIn_m361FC4798E6EDA1D0A1E980CFDD628B211815A62 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method)
{
	{
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_0 = __this->get_engine_5();
		NullCheck(L_0);
		int64_t L_1;
		L_1 = DeflaterEngine_get_TotalIn_mC27F3CA9F01B1C1974B8DCFF91D7DE9B816F26FD_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Flush()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_Flush_mCC6C370B48AC60F627C8A37E0363D76C951E4B07 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_state_2();
		__this->set_state_2(((int32_t)((int32_t)L_0|(int32_t)4)));
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Finish()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_Finish_mBB8D02EF15BCCC6A0DCD0560C80BC7F707824F60 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_state_2();
		__this->set_state_2(((int32_t)((int32_t)L_0|(int32_t)((int32_t)12))));
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Deflater::get_IsFinished()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Deflater_get_IsFinished_m84E8F8D8DE052B1016C5E57958E60F271146D446 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_state_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_0016;
		}
	}
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_1 = __this->get_pending_4();
		NullCheck(L_1);
		bool L_2;
		L_2 = PendingBuffer_get_IsFlushed_m64096BA3F7B56951266958E6233A77C74844D569(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		return (bool)0;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Deflater::get_IsNeedingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Deflater_get_IsNeedingInput_mB8C863FBF533F2C8BD42C9728907E092EDAEA899 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, const RuntimeMethod* method)
{
	{
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_0 = __this->get_engine_5();
		NullCheck(L_0);
		bool L_1;
		L_1 = DeflaterEngine_NeedsInput_mDDBEA2E842B74A55BB42B0DD6790EE4D5A8ED5A0(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetInput(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_SetInput_mD20D223304B17FE2D9637620A767625C0A3619A8 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___input0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_state_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)8)))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_1 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_1, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralD35E5EE2D5AC8610DB60F486ABC47D10280EB70E)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Deflater_SetInput_mD20D223304B17FE2D9637620A767625C0A3619A8_RuntimeMethod_var)));
	}

IL_0015:
	{
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_2 = __this->get_engine_5();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = ___input0;
		int32_t L_4 = ___offset1;
		int32_t L_5 = ___count2;
		NullCheck(L_2);
		DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014(L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_SetLevel_m05D7A3C79B57A8AF9C72613CD730EC90FAB4CE4D (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, int32_t ___level0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___level0;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0009;
		}
	}
	{
		___level0 = 6;
		goto IL_001f;
	}

IL_0009:
	{
		int32_t L_1 = ___level0;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = ___level0;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)9))))
		{
			goto IL_001f;
		}
	}

IL_0012:
	{
		String_t* L_3;
		L_3 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___level0), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_4 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_4, L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Deflater_SetLevel_m05D7A3C79B57A8AF9C72613CD730EC90FAB4CE4D_RuntimeMethod_var)));
	}

IL_001f:
	{
		int32_t L_5 = __this->get_level_0();
		int32_t L_6 = ___level0;
		if ((((int32_t)L_5) == ((int32_t)L_6)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_7 = ___level0;
		__this->set_level_0(L_7);
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_8 = __this->get_engine_5();
		int32_t L_9 = ___level0;
		NullCheck(L_8);
		DeflaterEngine_SetLevel_m2EB416322DD10370B059672DB7C652B8ED1A54DC(L_8, L_9, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetStrategy(FMETP.SharpZipLib.Zip.Compression.DeflateStrategy)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Deflater_SetStrategy_mAC8771AF03A22E52E55CC5353F334E831F51B2D0 (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, int32_t ___strategy0, const RuntimeMethod* method)
{
	{
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_0 = __this->get_engine_5();
		int32_t L_1 = ___strategy0;
		NullCheck(L_0);
		DeflaterEngine_set_Strategy_m49FD868FE9D1242660EA59FD2C03881C34FF68AB_inline(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Deflater::Deflate(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Deflater_Deflate_m4B828E32CABE4976218147C102256B124DBB3A9C (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___output0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		int32_t L_0 = ___length2;
		V_0 = L_0;
		int32_t L_1 = __this->get_state_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)127)))))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_2 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral52487EB2942AE06ED9A634DF79C35C26D9ADAEA2)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Deflater_Deflate_m4B828E32CABE4976218147C102256B124DBB3A9C_RuntimeMethod_var)));
	}

IL_0017:
	{
		int32_t L_3 = __this->get_state_2();
		if ((((int32_t)L_3) >= ((int32_t)((int32_t)16))))
		{
			goto IL_00be;
		}
	}
	{
		V_1 = ((int32_t)30720);
		int32_t L_4 = __this->get_level_0();
		V_2 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1))>>(int32_t)1));
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) <= ((int32_t)3)))
		{
			goto IL_003f;
		}
	}

IL_003d:
	{
		V_2 = 3;
	}

IL_003f:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_2;
		V_1 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)((int32_t)L_8<<(int32_t)6))));
		int32_t L_9 = __this->get_state_2();
		if (!((int32_t)((int32_t)L_9&(int32_t)1)))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10|(int32_t)((int32_t)32)));
	}

IL_0054:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)31), (int32_t)((int32_t)((int32_t)L_12%(int32_t)((int32_t)31)))))));
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_13 = __this->get_pending_4();
		int32_t L_14 = V_1;
		NullCheck(L_13);
		PendingBuffer_WriteShortMSB_m7597C12105E7CEE947A8CFDE511DD1CB805B3CD9(L_13, L_14, /*hidden argument*/NULL);
		int32_t L_15 = __this->get_state_2();
		if (!((int32_t)((int32_t)L_15&(int32_t)1)))
		{
			goto IL_00ac;
		}
	}
	{
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_16 = __this->get_engine_5();
		NullCheck(L_16);
		int32_t L_17;
		L_17 = DeflaterEngine_get_Adler_m778B43EAEE4DE749764D60C3A970BFFCB36814CB(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_18 = __this->get_engine_5();
		NullCheck(L_18);
		DeflaterEngine_ResetAdler_mFFE9B0AFB523F170C8523221606BFF85713DA012(L_18, /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_19 = __this->get_pending_4();
		int32_t L_20 = V_3;
		NullCheck(L_19);
		PendingBuffer_WriteShortMSB_m7597C12105E7CEE947A8CFDE511DD1CB805B3CD9(L_19, ((int32_t)((int32_t)L_20>>(int32_t)((int32_t)16))), /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_21 = __this->get_pending_4();
		int32_t L_22 = V_3;
		NullCheck(L_21);
		PendingBuffer_WriteShortMSB_m7597C12105E7CEE947A8CFDE511DD1CB805B3CD9(L_21, ((int32_t)((int32_t)L_22&(int32_t)((int32_t)65535))), /*hidden argument*/NULL);
	}

IL_00ac:
	{
		int32_t L_23 = __this->get_state_2();
		__this->set_state_2(((int32_t)((int32_t)((int32_t)16)|(int32_t)((int32_t)((int32_t)L_23&(int32_t)((int32_t)12))))));
	}

IL_00be:
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_24 = __this->get_pending_4();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_25 = ___output0;
		int32_t L_26 = ___offset1;
		int32_t L_27 = ___length2;
		NullCheck(L_24);
		int32_t L_28;
		L_28 = PendingBuffer_Flush_m828B34DC184E3CFC8595E381C70B0068CBD5CDFA(L_24, L_25, L_26, L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		int32_t L_29 = ___offset1;
		int32_t L_30 = V_4;
		___offset1 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)L_30));
		int64_t L_31 = __this->get_totalOut_3();
		int32_t L_32 = V_4;
		__this->set_totalOut_3(((int64_t)il2cpp_codegen_add((int64_t)L_31, (int64_t)((int64_t)((int64_t)L_32)))));
		int32_t L_33 = ___length2;
		int32_t L_34 = V_4;
		___length2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_33, (int32_t)L_34));
		int32_t L_35 = ___length2;
		if (!L_35)
		{
			goto IL_01d3;
		}
	}
	{
		int32_t L_36 = __this->get_state_2();
		if ((((int32_t)L_36) == ((int32_t)((int32_t)30))))
		{
			goto IL_01d3;
		}
	}
	{
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_37 = __this->get_engine_5();
		int32_t L_38 = __this->get_state_2();
		int32_t L_39 = __this->get_state_2();
		NullCheck(L_37);
		bool L_40;
		L_40 = DeflaterEngine_Deflate_m9DD2CC475173BB34AE655813C1EDE6999B261099(L_37, (bool)((!(((uint32_t)((int32_t)((int32_t)L_38&(int32_t)4))) <= ((uint32_t)0)))? 1 : 0), (bool)((!(((uint32_t)((int32_t)((int32_t)L_39&(int32_t)8))) <= ((uint32_t)0)))? 1 : 0), /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_00be;
		}
	}
	{
		int32_t L_41 = __this->get_state_2();
		V_5 = L_41;
		int32_t L_42 = V_5;
		if ((((int32_t)L_42) == ((int32_t)((int32_t)16))))
		{
			goto IL_013c;
		}
	}
	{
		int32_t L_43 = V_5;
		if ((((int32_t)L_43) == ((int32_t)((int32_t)20))))
		{
			goto IL_0140;
		}
	}
	{
		int32_t L_44 = V_5;
		if ((((int32_t)L_44) == ((int32_t)((int32_t)28))))
		{
			goto IL_0183;
		}
	}
	{
		goto IL_00be;
	}

IL_013c:
	{
		int32_t L_45 = V_0;
		int32_t L_46 = ___length2;
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_45, (int32_t)L_46));
	}

IL_0140:
	{
		int32_t L_47 = __this->get_level_0();
		if (!L_47)
		{
			goto IL_0176;
		}
	}
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_48 = __this->get_pending_4();
		NullCheck(L_48);
		int32_t L_49;
		L_49 = PendingBuffer_get_BitCount_mB4A917AF510467CF020B8E173785361118CF9F41_inline(L_48, /*hidden argument*/NULL);
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)8, (int32_t)((int32_t)((int32_t)((-L_49))&(int32_t)7))));
		goto IL_0171;
	}

IL_015c:
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_50 = __this->get_pending_4();
		NullCheck(L_50);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_50, 2, ((int32_t)10), /*hidden argument*/NULL);
		int32_t L_51 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_51, (int32_t)((int32_t)10)));
	}

IL_0171:
	{
		int32_t L_52 = V_6;
		if ((((int32_t)L_52) > ((int32_t)0)))
		{
			goto IL_015c;
		}
	}

IL_0176:
	{
		__this->set_state_2(((int32_t)16));
		goto IL_00be;
	}

IL_0183:
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_53 = __this->get_pending_4();
		NullCheck(L_53);
		PendingBuffer_AlignToByte_m17AE6E041B765E2F686F17560EE7D22E763271D4(L_53, /*hidden argument*/NULL);
		bool L_54 = __this->get_noZlibHeaderOrFooter_1();
		if (L_54)
		{
			goto IL_01c6;
		}
	}
	{
		DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * L_55 = __this->get_engine_5();
		NullCheck(L_55);
		int32_t L_56;
		L_56 = DeflaterEngine_get_Adler_m778B43EAEE4DE749764D60C3A970BFFCB36814CB(L_55, /*hidden argument*/NULL);
		V_7 = L_56;
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_57 = __this->get_pending_4();
		int32_t L_58 = V_7;
		NullCheck(L_57);
		PendingBuffer_WriteShortMSB_m7597C12105E7CEE947A8CFDE511DD1CB805B3CD9(L_57, ((int32_t)((int32_t)L_58>>(int32_t)((int32_t)16))), /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_59 = __this->get_pending_4();
		int32_t L_60 = V_7;
		NullCheck(L_59);
		PendingBuffer_WriteShortMSB_m7597C12105E7CEE947A8CFDE511DD1CB805B3CD9(L_59, ((int32_t)((int32_t)L_60&(int32_t)((int32_t)65535))), /*hidden argument*/NULL);
	}

IL_01c6:
	{
		__this->set_state_2(((int32_t)30));
		goto IL_00be;
	}

IL_01d3:
	{
		int32_t L_61 = V_0;
		int32_t L_62 = ___length2;
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_61, (int32_t)L_62));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterConstants::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterConstants__cctor_m3168234B4911EEA27420B684ECB3824C838F8DE4 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____00C40B3F013EDA60390F2E849C4581815A9419E4_0_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____03B569C38E3CD6B720388919D43735A904012C52_1_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____6BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____DB7C763C9670DD0F6ED34B75B3410A39D835F964_10_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12_FieldInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_0;
		L_0 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(((int32_t)65535), ((int32_t)65531), /*hidden argument*/NULL);
		((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->set_MAX_BLOCK_SIZE_0(L_0);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = L_1;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_3 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____00C40B3F013EDA60390F2E849C4581815A9419E4_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_2, L_3, /*hidden argument*/NULL);
		((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->set_GOOD_LENGTH_1(L_2);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_4 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_5 = L_4;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_6 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____6BC4EAB0D604C8D4599021AD611C5DBA7FF7E306_5_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_5, L_6, /*hidden argument*/NULL);
		((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->set_MAX_LAZY_2(L_5);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_7 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_8 = L_7;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_9 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____03B569C38E3CD6B720388919D43735A904012C52_1_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_8, L_9, /*hidden argument*/NULL);
		((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->set_NICE_LENGTH_3(L_8);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_10 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_11 = L_10;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_12 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____ED032026472FF77A8A17BA4AFF3FC57AF4B4BF79_12_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_11, L_12, /*hidden argument*/NULL);
		((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->set_MAX_CHAIN_4(L_11);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_13 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_14 = L_13;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_15 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____DB7C763C9670DD0F6ED34B75B3410A39D835F964_10_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_14, L_15, /*hidden argument*/NULL);
		((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->set_COMPR_FUNC_5(L_14);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterPending,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine__ctor_m1A08402452F08A53C08973444EBAB7435D5CC37A (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * ___pending0, bool ___noAdlerCalculation1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_0 = ___pending0;
		__this->set_pending_20(L_0);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_1 = ___pending0;
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_2 = (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 *)il2cpp_codegen_object_new(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		DeflaterHuffman__ctor_m2B19A99445EFEB9B74D2153F80E7F4DC5792F4E1(L_2, L_1, /*hidden argument*/NULL);
		__this->set_huffman_21(L_2);
		bool L_3 = ___noAdlerCalculation1;
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_4 = (Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB *)il2cpp_codegen_object_new(Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var);
		Adler32__ctor_m83B2F2CA75BAC4A379FC88436032D5E4CD15EADF(L_4, /*hidden argument*/NULL);
		__this->set_adler_22(L_4);
	}

IL_0027:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)65536));
		__this->set_window_9(L_5);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_6 = (Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)SZArrayNew(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32768));
		__this->set_head_1(L_6);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_7 = (Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)SZArrayNew(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32768));
		__this->set_prev_2(L_7);
		int32_t L_8 = 1;
		V_0 = L_8;
		__this->set_strstart_7(L_8);
		int32_t L_9 = V_0;
		__this->set_blockStart_6(L_9);
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::Deflate(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_Deflate_m9DD2CC475173BB34AE655813C1EDE6999B261099 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, bool ___flush0, bool ___finish1, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;

IL_0000:
	{
		DeflaterEngine_FillWindow_m6F85F39A3609EFAA52BEA5CDBE91FFCC81363544(__this, /*hidden argument*/NULL);
		bool L_0 = ___flush0;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = __this->get_inputOff_18();
		int32_t L_2 = __this->get_inputEnd_19();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		V_1 = (bool)G_B3_0;
		int32_t L_3 = __this->get_compressionFunction_15();
		V_2 = L_3;
		int32_t L_4 = V_2;
		switch (L_4)
		{
			case 0:
			{
				goto IL_0036;
			}
			case 1:
			{
				goto IL_0041;
			}
			case 2:
			{
				goto IL_004c;
			}
		}
	}
	{
		goto IL_0057;
	}

IL_0036:
	{
		bool L_5 = V_1;
		bool L_6 = ___finish1;
		bool L_7;
		L_7 = DeflaterEngine_DeflateStored_mCA925305F6F3CC7515FE55DD3ABBF515DC038F41(__this, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0062;
	}

IL_0041:
	{
		bool L_8 = V_1;
		bool L_9 = ___finish1;
		bool L_10;
		L_10 = DeflaterEngine_DeflateFast_mBF7F0E5FE39F8D6E4008A05387BE7D37CC1BD0EE(__this, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_0062;
	}

IL_004c:
	{
		bool L_11 = V_1;
		bool L_12 = ___finish1;
		bool L_13;
		L_13 = DeflaterEngine_DeflateSlow_m7954800AA1BE94A4BEF695D92CFEC124209018A3(__this, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		goto IL_0062;
	}

IL_0057:
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_14 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_14, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral0EF0E9609C4859EEAC4F853683B3C2A224D9AA13)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterEngine_Deflate_m9DD2CC475173BB34AE655813C1EDE6999B261099_RuntimeMethod_var)));
	}

IL_0062:
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_15 = __this->get_pending_20();
		NullCheck(L_15);
		bool L_16;
		L_16 = PendingBuffer_get_IsFlushed_m64096BA3F7B56951266958E6233A77C74844D569(L_15, /*hidden argument*/NULL);
		bool L_17 = V_0;
		if (((int32_t)((int32_t)L_16&(int32_t)L_17)))
		{
			goto IL_0000;
		}
	}
	{
		bool L_18 = V_0;
		return L_18;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SetInput(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___buffer0;
		NullCheck((RuntimeObject *)(RuntimeObject *)L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(RuntimeObject *)L_1);
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_3 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014_RuntimeMethod_var)));
	}

IL_000f:
	{
		int32_t L_4 = ___offset1;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_5;
		L_5 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___offset1), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_6 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_6, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014_RuntimeMethod_var)));
	}

IL_0020:
	{
		int32_t L_7 = ___count2;
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_8;
		L_8 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___count2), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_9 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_9, L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014_RuntimeMethod_var)));
	}

IL_0031:
	{
		int32_t L_10 = __this->get_inputOff_18();
		int32_t L_11 = __this->get_inputEnd_19();
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_004a;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_12 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_12, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral30D44D3EB7B534CAF51AD514F18FE7DD626129FE)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014_RuntimeMethod_var)));
	}

IL_004a:
	{
		int32_t L_13 = ___offset1;
		int32_t L_14 = ___count2;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_14));
		int32_t L_15 = ___offset1;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) > ((int32_t)L_16)))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_17 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18 = ___buffer0;
		NullCheck(L_18);
		if ((((int32_t)L_17) <= ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))))))
		{
			goto IL_0065;
		}
	}

IL_0058:
	{
		String_t* L_19;
		L_19 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___count2), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_20 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_20, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterEngine_SetInput_m33B8E1262536DF022CF57CCE8D4A9B215EEDC014_RuntimeMethod_var)));
	}

IL_0065:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_21 = ___buffer0;
		__this->set_inputBuf_16(L_21);
		int32_t L_22 = ___offset1;
		__this->set_inputOff_18(L_22);
		int32_t L_23 = V_0;
		__this->set_inputEnd_19(L_23);
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::NeedsInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_NeedsInput_mDDBEA2E842B74A55BB42B0DD6790EE4D5A8ED5A0 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_inputEnd_19();
		int32_t L_1 = __this->get_inputOff_18();
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_Reset_mC49C3D98C62110B5453E75554DF402BE6C776CCE (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_0 = __this->get_huffman_21();
		NullCheck(L_0);
		DeflaterHuffman_Reset_m695094C3E44BCC5197F58D2CF7F0CCAA51AB8599(L_0, /*hidden argument*/NULL);
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_1 = __this->get_adler_22();
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_2 = __this->get_adler_22();
		NullCheck(L_2);
		Adler32_Reset_mA0BDC81A3B31E939D321A24040D47BBCEACECC22(L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		int32_t L_3 = 1;
		V_0 = L_3;
		__this->set_strstart_7(L_3);
		int32_t L_4 = V_0;
		__this->set_blockStart_6(L_4);
		__this->set_lookahead_8(0);
		__this->set_totalIn_17(((int64_t)((int64_t)0)));
		__this->set_prevAvailable_5((bool)0);
		__this->set_matchLen_4(2);
		V_1 = 0;
		goto IL_005c;
	}

IL_004f:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_5 = __this->get_head_1();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (int16_t)0);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_005c:
	{
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) < ((int32_t)((int32_t)32768))))
		{
			goto IL_004f;
		}
	}
	{
		V_2 = 0;
		goto IL_0075;
	}

IL_0068:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_9 = __this->get_prev_2();
		int32_t L_10 = V_2;
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (int16_t)0);
		int32_t L_11 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0075:
	{
		int32_t L_12 = V_2;
		if ((((int32_t)L_12) < ((int32_t)((int32_t)32768))))
		{
			goto IL_0068;
		}
	}
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::ResetAdler()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_ResetAdler_mFFE9B0AFB523F170C8523221606BFF85713DA012 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method)
{
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_0 = __this->get_adler_22();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_1 = __this->get_adler_22();
		NullCheck(L_1);
		Adler32_Reset_mA0BDC81A3B31E939D321A24040D47BBCEACECC22(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::get_Adler()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DeflaterEngine_get_Adler_m778B43EAEE4DE749764D60C3A970BFFCB36814CB (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method)
{
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_0 = __this->get_adler_22();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_1 = __this->get_adler_22();
		NullCheck(L_1);
		int64_t L_2;
		L_2 = Adler32_get_Value_mB072B09485C331979F68992167A098E33F9C26EB(L_1, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_2));
	}
}
// System.Int64 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::get_TotalIn()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t DeflaterEngine_get_TotalIn_mC27F3CA9F01B1C1974B8DCFF91D7DE9B816F26FD (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = __this->get_totalIn_17();
		return L_0;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::set_Strategy(FMETP.SharpZipLib.Zip.Compression.DeflateStrategy)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_set_Strategy_m49FD868FE9D1242660EA59FD2C03881C34FF68AB (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_strategy_10(L_0);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SetLevel(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_SetLevel_m2EB416322DD10370B059672DB7C652B8ED1A54DC (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, int32_t ___level0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___level0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___level0;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)9))))
		{
			goto IL_0016;
		}
	}

IL_0009:
	{
		String_t* L_2;
		L_2 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___level0), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_3 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterEngine_SetLevel_m2EB416322DD10370B059672DB7C652B8ED1A54DC_RuntimeMethod_var)));
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_4 = ((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->get_GOOD_LENGTH_1();
		int32_t L_5 = ___level0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_goodLength_14(L_7);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_8 = ((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->get_MAX_LAZY_2();
		int32_t L_9 = ___level0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		int32_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		__this->set_max_lazy_12(L_11);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_12 = ((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->get_NICE_LENGTH_3();
		int32_t L_13 = ___level0;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		__this->set_niceLength_13(L_15);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_16 = ((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->get_MAX_CHAIN_4();
		int32_t L_17 = ___level0;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		int32_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		__this->set_max_chain_11(L_19);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_20 = ((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->get_COMPR_FUNC_5();
		int32_t L_21 = ___level0;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		int32_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		int32_t L_24 = __this->get_compressionFunction_15();
		if ((((int32_t)L_23) == ((int32_t)L_24)))
		{
			goto IL_018b;
		}
	}
	{
		int32_t L_25 = __this->get_compressionFunction_15();
		V_0 = L_25;
		int32_t L_26 = V_0;
		switch (L_26)
		{
			case 0:
			{
				goto IL_007a;
			}
			case 1:
			{
				goto IL_00c4;
			}
			case 2:
			{
				goto IL_0108;
			}
		}
	}
	{
		goto IL_017e;
	}

IL_007a:
	{
		int32_t L_27 = __this->get_strstart_7();
		int32_t L_28 = __this->get_blockStart_6();
		if ((((int32_t)L_27) <= ((int32_t)L_28)))
		{
			goto IL_00b9;
		}
	}
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_29 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_30 = __this->get_window_9();
		int32_t L_31 = __this->get_blockStart_6();
		int32_t L_32 = __this->get_strstart_7();
		int32_t L_33 = __this->get_blockStart_6();
		NullCheck(L_29);
		DeflaterHuffman_FlushStoredBlock_m9E4ABC1CFA682400BB19AEA57450D550B7F450D8(L_29, L_30, L_31, ((int32_t)il2cpp_codegen_subtract((int32_t)L_32, (int32_t)L_33)), (bool)0, /*hidden argument*/NULL);
		int32_t L_34 = __this->get_strstart_7();
		__this->set_blockStart_6(L_34);
	}

IL_00b9:
	{
		DeflaterEngine_UpdateHash_m5D5500B13BFF64919559AE7022C08A9F9A5C8D09(__this, /*hidden argument*/NULL);
		goto IL_017e;
	}

IL_00c4:
	{
		int32_t L_35 = __this->get_strstart_7();
		int32_t L_36 = __this->get_blockStart_6();
		if ((((int32_t)L_35) <= ((int32_t)L_36)))
		{
			goto IL_017e;
		}
	}
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_37 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_38 = __this->get_window_9();
		int32_t L_39 = __this->get_blockStart_6();
		int32_t L_40 = __this->get_strstart_7();
		int32_t L_41 = __this->get_blockStart_6();
		NullCheck(L_37);
		DeflaterHuffman_FlushBlock_m3480AE4CB9E565601FE9618FAD10F25972C19AAA(L_37, L_38, L_39, ((int32_t)il2cpp_codegen_subtract((int32_t)L_40, (int32_t)L_41)), (bool)0, /*hidden argument*/NULL);
		int32_t L_42 = __this->get_strstart_7();
		__this->set_blockStart_6(L_42);
		goto IL_017e;
	}

IL_0108:
	{
		bool L_43 = __this->get_prevAvailable_5();
		if (!L_43)
		{
			goto IL_0131;
		}
	}
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_44 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_45 = __this->get_window_9();
		int32_t L_46 = __this->get_strstart_7();
		NullCheck(L_45);
		int32_t L_47 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_46, (int32_t)1));
		uint8_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		NullCheck(L_44);
		bool L_49;
		L_49 = DeflaterHuffman_TallyLit_m054CCF260A35DB550E760EC54483DBCAE9DC699A(L_44, ((int32_t)((int32_t)L_48&(int32_t)((int32_t)255))), /*hidden argument*/NULL);
	}

IL_0131:
	{
		int32_t L_50 = __this->get_strstart_7();
		int32_t L_51 = __this->get_blockStart_6();
		if ((((int32_t)L_50) <= ((int32_t)L_51)))
		{
			goto IL_0170;
		}
	}
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_52 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_53 = __this->get_window_9();
		int32_t L_54 = __this->get_blockStart_6();
		int32_t L_55 = __this->get_strstart_7();
		int32_t L_56 = __this->get_blockStart_6();
		NullCheck(L_52);
		DeflaterHuffman_FlushBlock_m3480AE4CB9E565601FE9618FAD10F25972C19AAA(L_52, L_53, L_54, ((int32_t)il2cpp_codegen_subtract((int32_t)L_55, (int32_t)L_56)), (bool)0, /*hidden argument*/NULL);
		int32_t L_57 = __this->get_strstart_7();
		__this->set_blockStart_6(L_57);
	}

IL_0170:
	{
		__this->set_prevAvailable_5((bool)0);
		__this->set_matchLen_4(2);
	}

IL_017e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_58 = ((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->get_COMPR_FUNC_5();
		int32_t L_59 = ___level0;
		NullCheck(L_58);
		int32_t L_60 = L_59;
		int32_t L_61 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		__this->set_compressionFunction_15(L_61);
	}

IL_018b:
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::FillWindow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_FillWindow_m6F85F39A3609EFAA52BEA5CDBE91FFCC81363544 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_strstart_7();
		if ((((int32_t)L_0) < ((int32_t)((int32_t)65274))))
		{
			goto IL_0013;
		}
	}
	{
		DeflaterEngine_SlideWindow_mDBA845C625026D2D96B768ED9754DD79D5954F11(__this, /*hidden argument*/NULL);
	}

IL_0013:
	{
		int32_t L_1 = __this->get_lookahead_8();
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)262))))
		{
			goto IL_00db;
		}
	}
	{
		int32_t L_2 = __this->get_inputOff_18();
		int32_t L_3 = __this->get_inputEnd_19();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_00db;
		}
	}
	{
		int32_t L_4 = __this->get_lookahead_8();
		int32_t L_5 = __this->get_strstart_7();
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)65536), (int32_t)L_4)), (int32_t)L_5));
		int32_t L_6 = V_0;
		int32_t L_7 = __this->get_inputEnd_19();
		int32_t L_8 = __this->get_inputOff_18();
		if ((((int32_t)L_6) <= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)L_8)))))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_9 = __this->get_inputEnd_19();
		int32_t L_10 = __this->get_inputOff_18();
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)L_10));
	}

IL_0066:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = __this->get_inputBuf_16();
		int32_t L_12 = __this->get_inputOff_18();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_13 = __this->get_window_9();
		int32_t L_14 = __this->get_strstart_7();
		int32_t L_15 = __this->get_lookahead_8();
		int32_t L_16 = V_0;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_11, L_12, (RuntimeArray *)(RuntimeArray *)L_13, ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)L_15)), L_16, /*hidden argument*/NULL);
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_17 = __this->get_adler_22();
		if (!L_17)
		{
			goto IL_00b0;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_18 = __this->get_adler_22();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_19 = __this->get_inputBuf_16();
		int32_t L_20 = __this->get_inputOff_18();
		int32_t L_21 = V_0;
		ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  L_22;
		memset((&L_22), 0, sizeof(L_22));
		ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D((&L_22), L_19, L_20, L_21, /*hidden argument*/ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_RuntimeMethod_var);
		NullCheck(L_18);
		Adler32_Update_m97D8416919DC739C90BEE5CE6F50C85521246A58(L_18, L_22, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		int32_t L_23 = __this->get_inputOff_18();
		int32_t L_24 = V_0;
		__this->set_inputOff_18(((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)L_24)));
		int64_t L_25 = __this->get_totalIn_17();
		int32_t L_26 = V_0;
		__this->set_totalIn_17(((int64_t)il2cpp_codegen_add((int64_t)L_25, (int64_t)((int64_t)((int64_t)L_26)))));
		int32_t L_27 = __this->get_lookahead_8();
		int32_t L_28 = V_0;
		__this->set_lookahead_8(((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)L_28)));
	}

IL_00db:
	{
		int32_t L_29 = __this->get_lookahead_8();
		if ((((int32_t)L_29) < ((int32_t)3)))
		{
			goto IL_00ea;
		}
	}
	{
		DeflaterEngine_UpdateHash_m5D5500B13BFF64919559AE7022C08A9F9A5C8D09(__this, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::UpdateHash()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_UpdateHash_m5D5500B13BFF64919559AE7022C08A9F9A5C8D09 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_window_9();
		int32_t L_1 = __this->get_strstart_7();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = __this->get_window_9();
		int32_t L_5 = __this->get_strstart_7();
		NullCheck(L_4);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_ins_h_0(((int32_t)((int32_t)((int32_t)((int32_t)L_3<<(int32_t)5))^(int32_t)L_7)));
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::InsertString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DeflaterEngine_InsertString_m5D0EB65FCC71B2A30AD3699AFB65378EAFDF1B2A (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method)
{
	int16_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_ins_h_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = __this->get_window_9();
		int32_t L_2 = __this->get_strstart_7();
		NullCheck(L_1);
		int32_t L_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)2));
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0<<(int32_t)5))^(int32_t)L_4))&(int32_t)((int32_t)32767)));
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_5 = __this->get_prev_2();
		int32_t L_6 = __this->get_strstart_7();
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_7 = __this->get_head_1();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		int16_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		int16_t L_11 = L_10;
		V_0 = L_11;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_6&(int32_t)((int32_t)32767)))), (int16_t)L_11);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_12 = __this->get_head_1();
		int32_t L_13 = V_1;
		int32_t L_14 = __this->get_strstart_7();
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (int16_t)((int16_t)((int16_t)L_14)));
		int32_t L_15 = V_1;
		__this->set_ins_h_0(L_15);
		int16_t L_16 = V_0;
		return ((int32_t)((int32_t)L_16&(int32_t)((int32_t)65535)));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SlideWindow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterEngine_SlideWindow_mDBA845C625026D2D96B768ED9754DD79D5954F11 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t G_B3_0 = 0;
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* G_B3_1 = NULL;
	int32_t G_B2_0 = 0;
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B4_1 = 0;
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* G_B4_2 = NULL;
	int32_t G_B9_0 = 0;
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* G_B9_1 = NULL;
	int32_t G_B8_0 = 0;
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* G_B8_1 = NULL;
	int32_t G_B10_0 = 0;
	int32_t G_B10_1 = 0;
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* G_B10_2 = NULL;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_window_9();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = __this->get_window_9();
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_0, ((int32_t)32768), (RuntimeArray *)(RuntimeArray *)L_1, 0, ((int32_t)32768), /*hidden argument*/NULL);
		int32_t L_2 = __this->get_matchStart_3();
		__this->set_matchStart_3(((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)((int32_t)32768))));
		int32_t L_3 = __this->get_strstart_7();
		__this->set_strstart_7(((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)((int32_t)32768))));
		int32_t L_4 = __this->get_blockStart_6();
		__this->set_blockStart_6(((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)((int32_t)32768))));
		V_0 = 0;
		goto IL_0084;
	}

IL_0056:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_5 = __this->get_head_1();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		int16_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_1 = ((int32_t)((int32_t)L_8&(int32_t)((int32_t)65535)));
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_9 = __this->get_head_1();
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		G_B2_0 = L_10;
		G_B2_1 = L_9;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)32768))))
		{
			G_B3_0 = L_10;
			G_B3_1 = L_9;
			goto IL_0077;
		}
	}
	{
		G_B4_0 = 0;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_007e;
	}

IL_0077:
	{
		int32_t L_12 = V_1;
		G_B4_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)((int32_t)32768)));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_007e:
	{
		NullCheck(G_B4_2);
		(G_B4_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B4_1), (int16_t)((int16_t)((int16_t)G_B4_0)));
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0084:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) < ((int32_t)((int32_t)32768))))
		{
			goto IL_0056;
		}
	}
	{
		V_2 = 0;
		goto IL_00be;
	}

IL_0090:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_15 = __this->get_prev_2();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		int32_t L_17 = L_16;
		int16_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_3 = ((int32_t)((int32_t)L_18&(int32_t)((int32_t)65535)));
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_19 = __this->get_prev_2();
		int32_t L_20 = V_2;
		int32_t L_21 = V_3;
		G_B8_0 = L_20;
		G_B8_1 = L_19;
		if ((((int32_t)L_21) >= ((int32_t)((int32_t)32768))))
		{
			G_B9_0 = L_20;
			G_B9_1 = L_19;
			goto IL_00b1;
		}
	}
	{
		G_B10_0 = 0;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_00b8;
	}

IL_00b1:
	{
		int32_t L_22 = V_3;
		G_B10_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_22, (int32_t)((int32_t)32768)));
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_00b8:
	{
		NullCheck(G_B10_2);
		(G_B10_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B10_1), (int16_t)((int16_t)((int16_t)G_B10_0)));
		int32_t L_23 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_00be:
	{
		int32_t L_24 = V_2;
		if ((((int32_t)L_24) < ((int32_t)((int32_t)32768))))
		{
			goto IL_0090;
		}
	}
	{
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::FindLongestMatch(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_FindLongestMatch_m1835826D27ABAB7A5A5D59A6E774233045C8245B (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, int32_t ___curMatch0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_4 = NULL;
	Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	uint8_t V_8 = 0x0;
	uint8_t V_9 = 0x0;
	int32_t V_10 = 0;
	{
		int32_t L_0 = __this->get_strstart_7();
		V_1 = L_0;
		int32_t L_1 = V_1;
		int32_t L_2 = __this->get_lookahead_8();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_3;
		L_3 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(((int32_t)258), L_2, /*hidden argument*/NULL);
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), (int32_t)1));
		int32_t L_4 = V_1;
		int32_t L_5;
		L_5 = Math_Max_mD8AA27386BF012C65303FCDEA041B0CC65056E7B(((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)((int32_t)32506))), 0, /*hidden argument*/NULL);
		V_3 = L_5;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = __this->get_window_9();
		V_4 = L_6;
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_7 = __this->get_prev_2();
		V_5 = L_7;
		int32_t L_8 = __this->get_max_chain_11();
		V_6 = L_8;
		int32_t L_9 = __this->get_niceLength_13();
		int32_t L_10 = __this->get_lookahead_8();
		int32_t L_11;
		L_11 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(L_9, L_10, /*hidden argument*/NULL);
		V_7 = L_11;
		int32_t L_12 = __this->get_matchLen_4();
		int32_t L_13;
		L_13 = Math_Max_mD8AA27386BF012C65303FCDEA041B0CC65056E7B(L_12, 2, /*hidden argument*/NULL);
		__this->set_matchLen_4(L_13);
		int32_t L_14 = V_1;
		int32_t L_15 = __this->get_matchLen_4();
		int32_t L_16 = V_2;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)L_15))) <= ((int32_t)L_16)))
		{
			goto IL_0074;
		}
	}
	{
		return (bool)0;
	}

IL_0074:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_17 = V_4;
		int32_t L_18 = V_1;
		int32_t L_19 = __this->get_matchLen_4();
		NullCheck(L_17);
		int32_t L_20 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)L_19)), (int32_t)1));
		uint8_t L_21 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_8 = L_21;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_22 = V_4;
		int32_t L_23 = V_1;
		int32_t L_24 = __this->get_matchLen_4();
		NullCheck(L_22);
		int32_t L_25 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)L_24));
		uint8_t L_26 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		V_9 = L_26;
		int32_t L_27 = __this->get_matchLen_4();
		int32_t L_28 = __this->get_goodLength_14();
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_29 = V_6;
		V_6 = ((int32_t)((int32_t)L_29>>(int32_t)2));
	}

IL_00a4:
	{
		int32_t L_30 = ___curMatch0;
		V_0 = L_30;
		int32_t L_31 = __this->get_strstart_7();
		V_1 = L_31;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_32 = V_4;
		int32_t L_33 = V_0;
		int32_t L_34 = __this->get_matchLen_4();
		NullCheck(L_32);
		int32_t L_35 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)L_34));
		uint8_t L_36 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		uint8_t L_37 = V_9;
		if ((!(((uint32_t)L_36) == ((uint32_t)L_37))))
		{
			goto IL_0469;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_38 = V_4;
		int32_t L_39 = V_0;
		int32_t L_40 = __this->get_matchLen_4();
		NullCheck(L_38);
		int32_t L_41 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)L_40)), (int32_t)1));
		uint8_t L_42 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		uint8_t L_43 = V_8;
		if ((!(((uint32_t)L_42) == ((uint32_t)L_43))))
		{
			goto IL_0469;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_44 = V_4;
		int32_t L_45 = V_0;
		NullCheck(L_44);
		int32_t L_46 = L_45;
		uint8_t L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_48 = V_4;
		int32_t L_49 = V_1;
		NullCheck(L_48);
		int32_t L_50 = L_49;
		uint8_t L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		if ((!(((uint32_t)L_47) == ((uint32_t)L_51))))
		{
			goto IL_0469;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_52 = V_4;
		int32_t L_53 = V_0;
		int32_t L_54 = ((int32_t)il2cpp_codegen_add((int32_t)L_53, (int32_t)1));
		V_0 = L_54;
		NullCheck(L_52);
		int32_t L_55 = L_54;
		uint8_t L_56 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_57 = V_4;
		int32_t L_58 = V_1;
		int32_t L_59 = ((int32_t)il2cpp_codegen_add((int32_t)L_58, (int32_t)1));
		V_1 = L_59;
		NullCheck(L_57);
		int32_t L_60 = L_59;
		uint8_t L_61 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		if ((!(((uint32_t)L_56) == ((uint32_t)L_61))))
		{
			goto IL_0469;
		}
	}
	{
		int32_t L_62 = V_2;
		int32_t L_63 = V_1;
		V_10 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_62, (int32_t)L_63))%(int32_t)8));
		int32_t L_64 = V_10;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_64, (int32_t)1)))
		{
			case 0:
			{
				goto IL_0126;
			}
			case 1:
			{
				goto IL_0140;
			}
			case 2:
			{
				goto IL_016f;
			}
			case 3:
			{
				goto IL_01b3;
			}
			case 4:
			{
				goto IL_020c;
			}
			case 5:
			{
				goto IL_027a;
			}
			case 6:
			{
				goto IL_02fa;
			}
		}
	}
	{
		goto IL_0378;
	}

IL_0126:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_65 = V_4;
		int32_t L_66 = V_1;
		int32_t L_67 = ((int32_t)il2cpp_codegen_add((int32_t)L_66, (int32_t)1));
		V_1 = L_67;
		NullCheck(L_65);
		int32_t L_68 = L_67;
		uint8_t L_69 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_70 = V_4;
		int32_t L_71 = V_0;
		int32_t L_72 = ((int32_t)il2cpp_codegen_add((int32_t)L_71, (int32_t)1));
		V_0 = L_72;
		NullCheck(L_70);
		int32_t L_73 = L_72;
		uint8_t L_74 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		if ((!(((uint32_t)L_69) == ((uint32_t)L_74))))
		{
			goto IL_0378;
		}
	}
	{
		goto IL_0378;
	}

IL_0140:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_75 = V_4;
		int32_t L_76 = V_1;
		int32_t L_77 = ((int32_t)il2cpp_codegen_add((int32_t)L_76, (int32_t)1));
		V_1 = L_77;
		NullCheck(L_75);
		int32_t L_78 = L_77;
		uint8_t L_79 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_78));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_80 = V_4;
		int32_t L_81 = V_0;
		int32_t L_82 = ((int32_t)il2cpp_codegen_add((int32_t)L_81, (int32_t)1));
		V_0 = L_82;
		NullCheck(L_80);
		int32_t L_83 = L_82;
		uint8_t L_84 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		if ((!(((uint32_t)L_79) == ((uint32_t)L_84))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_85 = V_4;
		int32_t L_86 = V_1;
		int32_t L_87 = ((int32_t)il2cpp_codegen_add((int32_t)L_86, (int32_t)1));
		V_1 = L_87;
		NullCheck(L_85);
		int32_t L_88 = L_87;
		uint8_t L_89 = (L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_88));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_90 = V_4;
		int32_t L_91 = V_0;
		int32_t L_92 = ((int32_t)il2cpp_codegen_add((int32_t)L_91, (int32_t)1));
		V_0 = L_92;
		NullCheck(L_90);
		int32_t L_93 = L_92;
		uint8_t L_94 = (L_90)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		if ((!(((uint32_t)L_89) == ((uint32_t)L_94))))
		{
			goto IL_0378;
		}
	}
	{
		goto IL_0378;
	}

IL_016f:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_95 = V_4;
		int32_t L_96 = V_1;
		int32_t L_97 = ((int32_t)il2cpp_codegen_add((int32_t)L_96, (int32_t)1));
		V_1 = L_97;
		NullCheck(L_95);
		int32_t L_98 = L_97;
		uint8_t L_99 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_98));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_100 = V_4;
		int32_t L_101 = V_0;
		int32_t L_102 = ((int32_t)il2cpp_codegen_add((int32_t)L_101, (int32_t)1));
		V_0 = L_102;
		NullCheck(L_100);
		int32_t L_103 = L_102;
		uint8_t L_104 = (L_100)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		if ((!(((uint32_t)L_99) == ((uint32_t)L_104))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_105 = V_4;
		int32_t L_106 = V_1;
		int32_t L_107 = ((int32_t)il2cpp_codegen_add((int32_t)L_106, (int32_t)1));
		V_1 = L_107;
		NullCheck(L_105);
		int32_t L_108 = L_107;
		uint8_t L_109 = (L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_110 = V_4;
		int32_t L_111 = V_0;
		int32_t L_112 = ((int32_t)il2cpp_codegen_add((int32_t)L_111, (int32_t)1));
		V_0 = L_112;
		NullCheck(L_110);
		int32_t L_113 = L_112;
		uint8_t L_114 = (L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		if ((!(((uint32_t)L_109) == ((uint32_t)L_114))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_115 = V_4;
		int32_t L_116 = V_1;
		int32_t L_117 = ((int32_t)il2cpp_codegen_add((int32_t)L_116, (int32_t)1));
		V_1 = L_117;
		NullCheck(L_115);
		int32_t L_118 = L_117;
		uint8_t L_119 = (L_115)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_120 = V_4;
		int32_t L_121 = V_0;
		int32_t L_122 = ((int32_t)il2cpp_codegen_add((int32_t)L_121, (int32_t)1));
		V_0 = L_122;
		NullCheck(L_120);
		int32_t L_123 = L_122;
		uint8_t L_124 = (L_120)->GetAt(static_cast<il2cpp_array_size_t>(L_123));
		if ((!(((uint32_t)L_119) == ((uint32_t)L_124))))
		{
			goto IL_0378;
		}
	}
	{
		goto IL_0378;
	}

IL_01b3:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_125 = V_4;
		int32_t L_126 = V_1;
		int32_t L_127 = ((int32_t)il2cpp_codegen_add((int32_t)L_126, (int32_t)1));
		V_1 = L_127;
		NullCheck(L_125);
		int32_t L_128 = L_127;
		uint8_t L_129 = (L_125)->GetAt(static_cast<il2cpp_array_size_t>(L_128));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_130 = V_4;
		int32_t L_131 = V_0;
		int32_t L_132 = ((int32_t)il2cpp_codegen_add((int32_t)L_131, (int32_t)1));
		V_0 = L_132;
		NullCheck(L_130);
		int32_t L_133 = L_132;
		uint8_t L_134 = (L_130)->GetAt(static_cast<il2cpp_array_size_t>(L_133));
		if ((!(((uint32_t)L_129) == ((uint32_t)L_134))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_135 = V_4;
		int32_t L_136 = V_1;
		int32_t L_137 = ((int32_t)il2cpp_codegen_add((int32_t)L_136, (int32_t)1));
		V_1 = L_137;
		NullCheck(L_135);
		int32_t L_138 = L_137;
		uint8_t L_139 = (L_135)->GetAt(static_cast<il2cpp_array_size_t>(L_138));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_140 = V_4;
		int32_t L_141 = V_0;
		int32_t L_142 = ((int32_t)il2cpp_codegen_add((int32_t)L_141, (int32_t)1));
		V_0 = L_142;
		NullCheck(L_140);
		int32_t L_143 = L_142;
		uint8_t L_144 = (L_140)->GetAt(static_cast<il2cpp_array_size_t>(L_143));
		if ((!(((uint32_t)L_139) == ((uint32_t)L_144))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_145 = V_4;
		int32_t L_146 = V_1;
		int32_t L_147 = ((int32_t)il2cpp_codegen_add((int32_t)L_146, (int32_t)1));
		V_1 = L_147;
		NullCheck(L_145);
		int32_t L_148 = L_147;
		uint8_t L_149 = (L_145)->GetAt(static_cast<il2cpp_array_size_t>(L_148));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_150 = V_4;
		int32_t L_151 = V_0;
		int32_t L_152 = ((int32_t)il2cpp_codegen_add((int32_t)L_151, (int32_t)1));
		V_0 = L_152;
		NullCheck(L_150);
		int32_t L_153 = L_152;
		uint8_t L_154 = (L_150)->GetAt(static_cast<il2cpp_array_size_t>(L_153));
		if ((!(((uint32_t)L_149) == ((uint32_t)L_154))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_155 = V_4;
		int32_t L_156 = V_1;
		int32_t L_157 = ((int32_t)il2cpp_codegen_add((int32_t)L_156, (int32_t)1));
		V_1 = L_157;
		NullCheck(L_155);
		int32_t L_158 = L_157;
		uint8_t L_159 = (L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_158));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_160 = V_4;
		int32_t L_161 = V_0;
		int32_t L_162 = ((int32_t)il2cpp_codegen_add((int32_t)L_161, (int32_t)1));
		V_0 = L_162;
		NullCheck(L_160);
		int32_t L_163 = L_162;
		uint8_t L_164 = (L_160)->GetAt(static_cast<il2cpp_array_size_t>(L_163));
		if ((!(((uint32_t)L_159) == ((uint32_t)L_164))))
		{
			goto IL_0378;
		}
	}
	{
		goto IL_0378;
	}

IL_020c:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_165 = V_4;
		int32_t L_166 = V_1;
		int32_t L_167 = ((int32_t)il2cpp_codegen_add((int32_t)L_166, (int32_t)1));
		V_1 = L_167;
		NullCheck(L_165);
		int32_t L_168 = L_167;
		uint8_t L_169 = (L_165)->GetAt(static_cast<il2cpp_array_size_t>(L_168));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_170 = V_4;
		int32_t L_171 = V_0;
		int32_t L_172 = ((int32_t)il2cpp_codegen_add((int32_t)L_171, (int32_t)1));
		V_0 = L_172;
		NullCheck(L_170);
		int32_t L_173 = L_172;
		uint8_t L_174 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_173));
		if ((!(((uint32_t)L_169) == ((uint32_t)L_174))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_175 = V_4;
		int32_t L_176 = V_1;
		int32_t L_177 = ((int32_t)il2cpp_codegen_add((int32_t)L_176, (int32_t)1));
		V_1 = L_177;
		NullCheck(L_175);
		int32_t L_178 = L_177;
		uint8_t L_179 = (L_175)->GetAt(static_cast<il2cpp_array_size_t>(L_178));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_180 = V_4;
		int32_t L_181 = V_0;
		int32_t L_182 = ((int32_t)il2cpp_codegen_add((int32_t)L_181, (int32_t)1));
		V_0 = L_182;
		NullCheck(L_180);
		int32_t L_183 = L_182;
		uint8_t L_184 = (L_180)->GetAt(static_cast<il2cpp_array_size_t>(L_183));
		if ((!(((uint32_t)L_179) == ((uint32_t)L_184))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_185 = V_4;
		int32_t L_186 = V_1;
		int32_t L_187 = ((int32_t)il2cpp_codegen_add((int32_t)L_186, (int32_t)1));
		V_1 = L_187;
		NullCheck(L_185);
		int32_t L_188 = L_187;
		uint8_t L_189 = (L_185)->GetAt(static_cast<il2cpp_array_size_t>(L_188));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_190 = V_4;
		int32_t L_191 = V_0;
		int32_t L_192 = ((int32_t)il2cpp_codegen_add((int32_t)L_191, (int32_t)1));
		V_0 = L_192;
		NullCheck(L_190);
		int32_t L_193 = L_192;
		uint8_t L_194 = (L_190)->GetAt(static_cast<il2cpp_array_size_t>(L_193));
		if ((!(((uint32_t)L_189) == ((uint32_t)L_194))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_195 = V_4;
		int32_t L_196 = V_1;
		int32_t L_197 = ((int32_t)il2cpp_codegen_add((int32_t)L_196, (int32_t)1));
		V_1 = L_197;
		NullCheck(L_195);
		int32_t L_198 = L_197;
		uint8_t L_199 = (L_195)->GetAt(static_cast<il2cpp_array_size_t>(L_198));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_200 = V_4;
		int32_t L_201 = V_0;
		int32_t L_202 = ((int32_t)il2cpp_codegen_add((int32_t)L_201, (int32_t)1));
		V_0 = L_202;
		NullCheck(L_200);
		int32_t L_203 = L_202;
		uint8_t L_204 = (L_200)->GetAt(static_cast<il2cpp_array_size_t>(L_203));
		if ((!(((uint32_t)L_199) == ((uint32_t)L_204))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_205 = V_4;
		int32_t L_206 = V_1;
		int32_t L_207 = ((int32_t)il2cpp_codegen_add((int32_t)L_206, (int32_t)1));
		V_1 = L_207;
		NullCheck(L_205);
		int32_t L_208 = L_207;
		uint8_t L_209 = (L_205)->GetAt(static_cast<il2cpp_array_size_t>(L_208));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_210 = V_4;
		int32_t L_211 = V_0;
		int32_t L_212 = ((int32_t)il2cpp_codegen_add((int32_t)L_211, (int32_t)1));
		V_0 = L_212;
		NullCheck(L_210);
		int32_t L_213 = L_212;
		uint8_t L_214 = (L_210)->GetAt(static_cast<il2cpp_array_size_t>(L_213));
		if ((!(((uint32_t)L_209) == ((uint32_t)L_214))))
		{
			goto IL_0378;
		}
	}
	{
		goto IL_0378;
	}

IL_027a:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_215 = V_4;
		int32_t L_216 = V_1;
		int32_t L_217 = ((int32_t)il2cpp_codegen_add((int32_t)L_216, (int32_t)1));
		V_1 = L_217;
		NullCheck(L_215);
		int32_t L_218 = L_217;
		uint8_t L_219 = (L_215)->GetAt(static_cast<il2cpp_array_size_t>(L_218));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_220 = V_4;
		int32_t L_221 = V_0;
		int32_t L_222 = ((int32_t)il2cpp_codegen_add((int32_t)L_221, (int32_t)1));
		V_0 = L_222;
		NullCheck(L_220);
		int32_t L_223 = L_222;
		uint8_t L_224 = (L_220)->GetAt(static_cast<il2cpp_array_size_t>(L_223));
		if ((!(((uint32_t)L_219) == ((uint32_t)L_224))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_225 = V_4;
		int32_t L_226 = V_1;
		int32_t L_227 = ((int32_t)il2cpp_codegen_add((int32_t)L_226, (int32_t)1));
		V_1 = L_227;
		NullCheck(L_225);
		int32_t L_228 = L_227;
		uint8_t L_229 = (L_225)->GetAt(static_cast<il2cpp_array_size_t>(L_228));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_230 = V_4;
		int32_t L_231 = V_0;
		int32_t L_232 = ((int32_t)il2cpp_codegen_add((int32_t)L_231, (int32_t)1));
		V_0 = L_232;
		NullCheck(L_230);
		int32_t L_233 = L_232;
		uint8_t L_234 = (L_230)->GetAt(static_cast<il2cpp_array_size_t>(L_233));
		if ((!(((uint32_t)L_229) == ((uint32_t)L_234))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_235 = V_4;
		int32_t L_236 = V_1;
		int32_t L_237 = ((int32_t)il2cpp_codegen_add((int32_t)L_236, (int32_t)1));
		V_1 = L_237;
		NullCheck(L_235);
		int32_t L_238 = L_237;
		uint8_t L_239 = (L_235)->GetAt(static_cast<il2cpp_array_size_t>(L_238));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_240 = V_4;
		int32_t L_241 = V_0;
		int32_t L_242 = ((int32_t)il2cpp_codegen_add((int32_t)L_241, (int32_t)1));
		V_0 = L_242;
		NullCheck(L_240);
		int32_t L_243 = L_242;
		uint8_t L_244 = (L_240)->GetAt(static_cast<il2cpp_array_size_t>(L_243));
		if ((!(((uint32_t)L_239) == ((uint32_t)L_244))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_245 = V_4;
		int32_t L_246 = V_1;
		int32_t L_247 = ((int32_t)il2cpp_codegen_add((int32_t)L_246, (int32_t)1));
		V_1 = L_247;
		NullCheck(L_245);
		int32_t L_248 = L_247;
		uint8_t L_249 = (L_245)->GetAt(static_cast<il2cpp_array_size_t>(L_248));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_250 = V_4;
		int32_t L_251 = V_0;
		int32_t L_252 = ((int32_t)il2cpp_codegen_add((int32_t)L_251, (int32_t)1));
		V_0 = L_252;
		NullCheck(L_250);
		int32_t L_253 = L_252;
		uint8_t L_254 = (L_250)->GetAt(static_cast<il2cpp_array_size_t>(L_253));
		if ((!(((uint32_t)L_249) == ((uint32_t)L_254))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_255 = V_4;
		int32_t L_256 = V_1;
		int32_t L_257 = ((int32_t)il2cpp_codegen_add((int32_t)L_256, (int32_t)1));
		V_1 = L_257;
		NullCheck(L_255);
		int32_t L_258 = L_257;
		uint8_t L_259 = (L_255)->GetAt(static_cast<il2cpp_array_size_t>(L_258));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_260 = V_4;
		int32_t L_261 = V_0;
		int32_t L_262 = ((int32_t)il2cpp_codegen_add((int32_t)L_261, (int32_t)1));
		V_0 = L_262;
		NullCheck(L_260);
		int32_t L_263 = L_262;
		uint8_t L_264 = (L_260)->GetAt(static_cast<il2cpp_array_size_t>(L_263));
		if ((!(((uint32_t)L_259) == ((uint32_t)L_264))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_265 = V_4;
		int32_t L_266 = V_1;
		int32_t L_267 = ((int32_t)il2cpp_codegen_add((int32_t)L_266, (int32_t)1));
		V_1 = L_267;
		NullCheck(L_265);
		int32_t L_268 = L_267;
		uint8_t L_269 = (L_265)->GetAt(static_cast<il2cpp_array_size_t>(L_268));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_270 = V_4;
		int32_t L_271 = V_0;
		int32_t L_272 = ((int32_t)il2cpp_codegen_add((int32_t)L_271, (int32_t)1));
		V_0 = L_272;
		NullCheck(L_270);
		int32_t L_273 = L_272;
		uint8_t L_274 = (L_270)->GetAt(static_cast<il2cpp_array_size_t>(L_273));
		if ((!(((uint32_t)L_269) == ((uint32_t)L_274))))
		{
			goto IL_0378;
		}
	}
	{
		goto IL_0378;
	}

IL_02fa:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_275 = V_4;
		int32_t L_276 = V_1;
		int32_t L_277 = ((int32_t)il2cpp_codegen_add((int32_t)L_276, (int32_t)1));
		V_1 = L_277;
		NullCheck(L_275);
		int32_t L_278 = L_277;
		uint8_t L_279 = (L_275)->GetAt(static_cast<il2cpp_array_size_t>(L_278));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_280 = V_4;
		int32_t L_281 = V_0;
		int32_t L_282 = ((int32_t)il2cpp_codegen_add((int32_t)L_281, (int32_t)1));
		V_0 = L_282;
		NullCheck(L_280);
		int32_t L_283 = L_282;
		uint8_t L_284 = (L_280)->GetAt(static_cast<il2cpp_array_size_t>(L_283));
		if ((!(((uint32_t)L_279) == ((uint32_t)L_284))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_285 = V_4;
		int32_t L_286 = V_1;
		int32_t L_287 = ((int32_t)il2cpp_codegen_add((int32_t)L_286, (int32_t)1));
		V_1 = L_287;
		NullCheck(L_285);
		int32_t L_288 = L_287;
		uint8_t L_289 = (L_285)->GetAt(static_cast<il2cpp_array_size_t>(L_288));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_290 = V_4;
		int32_t L_291 = V_0;
		int32_t L_292 = ((int32_t)il2cpp_codegen_add((int32_t)L_291, (int32_t)1));
		V_0 = L_292;
		NullCheck(L_290);
		int32_t L_293 = L_292;
		uint8_t L_294 = (L_290)->GetAt(static_cast<il2cpp_array_size_t>(L_293));
		if ((!(((uint32_t)L_289) == ((uint32_t)L_294))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_295 = V_4;
		int32_t L_296 = V_1;
		int32_t L_297 = ((int32_t)il2cpp_codegen_add((int32_t)L_296, (int32_t)1));
		V_1 = L_297;
		NullCheck(L_295);
		int32_t L_298 = L_297;
		uint8_t L_299 = (L_295)->GetAt(static_cast<il2cpp_array_size_t>(L_298));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_300 = V_4;
		int32_t L_301 = V_0;
		int32_t L_302 = ((int32_t)il2cpp_codegen_add((int32_t)L_301, (int32_t)1));
		V_0 = L_302;
		NullCheck(L_300);
		int32_t L_303 = L_302;
		uint8_t L_304 = (L_300)->GetAt(static_cast<il2cpp_array_size_t>(L_303));
		if ((!(((uint32_t)L_299) == ((uint32_t)L_304))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_305 = V_4;
		int32_t L_306 = V_1;
		int32_t L_307 = ((int32_t)il2cpp_codegen_add((int32_t)L_306, (int32_t)1));
		V_1 = L_307;
		NullCheck(L_305);
		int32_t L_308 = L_307;
		uint8_t L_309 = (L_305)->GetAt(static_cast<il2cpp_array_size_t>(L_308));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_310 = V_4;
		int32_t L_311 = V_0;
		int32_t L_312 = ((int32_t)il2cpp_codegen_add((int32_t)L_311, (int32_t)1));
		V_0 = L_312;
		NullCheck(L_310);
		int32_t L_313 = L_312;
		uint8_t L_314 = (L_310)->GetAt(static_cast<il2cpp_array_size_t>(L_313));
		if ((!(((uint32_t)L_309) == ((uint32_t)L_314))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_315 = V_4;
		int32_t L_316 = V_1;
		int32_t L_317 = ((int32_t)il2cpp_codegen_add((int32_t)L_316, (int32_t)1));
		V_1 = L_317;
		NullCheck(L_315);
		int32_t L_318 = L_317;
		uint8_t L_319 = (L_315)->GetAt(static_cast<il2cpp_array_size_t>(L_318));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_320 = V_4;
		int32_t L_321 = V_0;
		int32_t L_322 = ((int32_t)il2cpp_codegen_add((int32_t)L_321, (int32_t)1));
		V_0 = L_322;
		NullCheck(L_320);
		int32_t L_323 = L_322;
		uint8_t L_324 = (L_320)->GetAt(static_cast<il2cpp_array_size_t>(L_323));
		if ((!(((uint32_t)L_319) == ((uint32_t)L_324))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_325 = V_4;
		int32_t L_326 = V_1;
		int32_t L_327 = ((int32_t)il2cpp_codegen_add((int32_t)L_326, (int32_t)1));
		V_1 = L_327;
		NullCheck(L_325);
		int32_t L_328 = L_327;
		uint8_t L_329 = (L_325)->GetAt(static_cast<il2cpp_array_size_t>(L_328));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_330 = V_4;
		int32_t L_331 = V_0;
		int32_t L_332 = ((int32_t)il2cpp_codegen_add((int32_t)L_331, (int32_t)1));
		V_0 = L_332;
		NullCheck(L_330);
		int32_t L_333 = L_332;
		uint8_t L_334 = (L_330)->GetAt(static_cast<il2cpp_array_size_t>(L_333));
		if ((!(((uint32_t)L_329) == ((uint32_t)L_334))))
		{
			goto IL_0378;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_335 = V_4;
		int32_t L_336 = V_1;
		int32_t L_337 = ((int32_t)il2cpp_codegen_add((int32_t)L_336, (int32_t)1));
		V_1 = L_337;
		NullCheck(L_335);
		int32_t L_338 = L_337;
		uint8_t L_339 = (L_335)->GetAt(static_cast<il2cpp_array_size_t>(L_338));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_340 = V_4;
		int32_t L_341 = V_0;
		int32_t L_342 = ((int32_t)il2cpp_codegen_add((int32_t)L_341, (int32_t)1));
		V_0 = L_342;
		NullCheck(L_340);
		int32_t L_343 = L_342;
		uint8_t L_344 = (L_340)->GetAt(static_cast<il2cpp_array_size_t>(L_343));
	}

IL_0378:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_345 = V_4;
		int32_t L_346 = V_1;
		NullCheck(L_345);
		int32_t L_347 = L_346;
		uint8_t L_348 = (L_345)->GetAt(static_cast<il2cpp_array_size_t>(L_347));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_349 = V_4;
		int32_t L_350 = V_0;
		NullCheck(L_349);
		int32_t L_351 = L_350;
		uint8_t L_352 = (L_349)->GetAt(static_cast<il2cpp_array_size_t>(L_351));
		if ((!(((uint32_t)L_348) == ((uint32_t)L_352))))
		{
			goto IL_042c;
		}
	}

IL_0385:
	{
		int32_t L_353 = V_1;
		int32_t L_354 = V_2;
		if ((!(((uint32_t)L_353) == ((uint32_t)L_354))))
		{
			goto IL_0396;
		}
	}
	{
		int32_t L_355 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_355, (int32_t)1));
		int32_t L_356 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_356, (int32_t)1));
		goto IL_042c;
	}

IL_0396:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_357 = V_4;
		int32_t L_358 = V_1;
		int32_t L_359 = ((int32_t)il2cpp_codegen_add((int32_t)L_358, (int32_t)1));
		V_1 = L_359;
		NullCheck(L_357);
		int32_t L_360 = L_359;
		uint8_t L_361 = (L_357)->GetAt(static_cast<il2cpp_array_size_t>(L_360));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_362 = V_4;
		int32_t L_363 = V_0;
		int32_t L_364 = ((int32_t)il2cpp_codegen_add((int32_t)L_363, (int32_t)1));
		V_0 = L_364;
		NullCheck(L_362);
		int32_t L_365 = L_364;
		uint8_t L_366 = (L_362)->GetAt(static_cast<il2cpp_array_size_t>(L_365));
		if ((!(((uint32_t)L_361) == ((uint32_t)L_366))))
		{
			goto IL_042c;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_367 = V_4;
		int32_t L_368 = V_1;
		int32_t L_369 = ((int32_t)il2cpp_codegen_add((int32_t)L_368, (int32_t)1));
		V_1 = L_369;
		NullCheck(L_367);
		int32_t L_370 = L_369;
		uint8_t L_371 = (L_367)->GetAt(static_cast<il2cpp_array_size_t>(L_370));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_372 = V_4;
		int32_t L_373 = V_0;
		int32_t L_374 = ((int32_t)il2cpp_codegen_add((int32_t)L_373, (int32_t)1));
		V_0 = L_374;
		NullCheck(L_372);
		int32_t L_375 = L_374;
		uint8_t L_376 = (L_372)->GetAt(static_cast<il2cpp_array_size_t>(L_375));
		if ((!(((uint32_t)L_371) == ((uint32_t)L_376))))
		{
			goto IL_042c;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_377 = V_4;
		int32_t L_378 = V_1;
		int32_t L_379 = ((int32_t)il2cpp_codegen_add((int32_t)L_378, (int32_t)1));
		V_1 = L_379;
		NullCheck(L_377);
		int32_t L_380 = L_379;
		uint8_t L_381 = (L_377)->GetAt(static_cast<il2cpp_array_size_t>(L_380));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_382 = V_4;
		int32_t L_383 = V_0;
		int32_t L_384 = ((int32_t)il2cpp_codegen_add((int32_t)L_383, (int32_t)1));
		V_0 = L_384;
		NullCheck(L_382);
		int32_t L_385 = L_384;
		uint8_t L_386 = (L_382)->GetAt(static_cast<il2cpp_array_size_t>(L_385));
		if ((!(((uint32_t)L_381) == ((uint32_t)L_386))))
		{
			goto IL_042c;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_387 = V_4;
		int32_t L_388 = V_1;
		int32_t L_389 = ((int32_t)il2cpp_codegen_add((int32_t)L_388, (int32_t)1));
		V_1 = L_389;
		NullCheck(L_387);
		int32_t L_390 = L_389;
		uint8_t L_391 = (L_387)->GetAt(static_cast<il2cpp_array_size_t>(L_390));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_392 = V_4;
		int32_t L_393 = V_0;
		int32_t L_394 = ((int32_t)il2cpp_codegen_add((int32_t)L_393, (int32_t)1));
		V_0 = L_394;
		NullCheck(L_392);
		int32_t L_395 = L_394;
		uint8_t L_396 = (L_392)->GetAt(static_cast<il2cpp_array_size_t>(L_395));
		if ((!(((uint32_t)L_391) == ((uint32_t)L_396))))
		{
			goto IL_042c;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_397 = V_4;
		int32_t L_398 = V_1;
		int32_t L_399 = ((int32_t)il2cpp_codegen_add((int32_t)L_398, (int32_t)1));
		V_1 = L_399;
		NullCheck(L_397);
		int32_t L_400 = L_399;
		uint8_t L_401 = (L_397)->GetAt(static_cast<il2cpp_array_size_t>(L_400));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_402 = V_4;
		int32_t L_403 = V_0;
		int32_t L_404 = ((int32_t)il2cpp_codegen_add((int32_t)L_403, (int32_t)1));
		V_0 = L_404;
		NullCheck(L_402);
		int32_t L_405 = L_404;
		uint8_t L_406 = (L_402)->GetAt(static_cast<il2cpp_array_size_t>(L_405));
		if ((!(((uint32_t)L_401) == ((uint32_t)L_406))))
		{
			goto IL_042c;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_407 = V_4;
		int32_t L_408 = V_1;
		int32_t L_409 = ((int32_t)il2cpp_codegen_add((int32_t)L_408, (int32_t)1));
		V_1 = L_409;
		NullCheck(L_407);
		int32_t L_410 = L_409;
		uint8_t L_411 = (L_407)->GetAt(static_cast<il2cpp_array_size_t>(L_410));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_412 = V_4;
		int32_t L_413 = V_0;
		int32_t L_414 = ((int32_t)il2cpp_codegen_add((int32_t)L_413, (int32_t)1));
		V_0 = L_414;
		NullCheck(L_412);
		int32_t L_415 = L_414;
		uint8_t L_416 = (L_412)->GetAt(static_cast<il2cpp_array_size_t>(L_415));
		if ((!(((uint32_t)L_411) == ((uint32_t)L_416))))
		{
			goto IL_042c;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_417 = V_4;
		int32_t L_418 = V_1;
		int32_t L_419 = ((int32_t)il2cpp_codegen_add((int32_t)L_418, (int32_t)1));
		V_1 = L_419;
		NullCheck(L_417);
		int32_t L_420 = L_419;
		uint8_t L_421 = (L_417)->GetAt(static_cast<il2cpp_array_size_t>(L_420));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_422 = V_4;
		int32_t L_423 = V_0;
		int32_t L_424 = ((int32_t)il2cpp_codegen_add((int32_t)L_423, (int32_t)1));
		V_0 = L_424;
		NullCheck(L_422);
		int32_t L_425 = L_424;
		uint8_t L_426 = (L_422)->GetAt(static_cast<il2cpp_array_size_t>(L_425));
		if ((!(((uint32_t)L_421) == ((uint32_t)L_426))))
		{
			goto IL_042c;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_427 = V_4;
		int32_t L_428 = V_1;
		int32_t L_429 = ((int32_t)il2cpp_codegen_add((int32_t)L_428, (int32_t)1));
		V_1 = L_429;
		NullCheck(L_427);
		int32_t L_430 = L_429;
		uint8_t L_431 = (L_427)->GetAt(static_cast<il2cpp_array_size_t>(L_430));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_432 = V_4;
		int32_t L_433 = V_0;
		int32_t L_434 = ((int32_t)il2cpp_codegen_add((int32_t)L_433, (int32_t)1));
		V_0 = L_434;
		NullCheck(L_432);
		int32_t L_435 = L_434;
		uint8_t L_436 = (L_432)->GetAt(static_cast<il2cpp_array_size_t>(L_435));
		if ((((int32_t)L_431) == ((int32_t)L_436)))
		{
			goto IL_0385;
		}
	}

IL_042c:
	{
		int32_t L_437 = V_1;
		int32_t L_438 = __this->get_strstart_7();
		int32_t L_439 = __this->get_matchLen_4();
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_437, (int32_t)L_438))) <= ((int32_t)L_439)))
		{
			goto IL_0469;
		}
	}
	{
		int32_t L_440 = ___curMatch0;
		__this->set_matchStart_3(L_440);
		int32_t L_441 = V_1;
		int32_t L_442 = __this->get_strstart_7();
		__this->set_matchLen_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_441, (int32_t)L_442)));
		int32_t L_443 = __this->get_matchLen_4();
		int32_t L_444 = V_7;
		if ((((int32_t)L_443) >= ((int32_t)L_444)))
		{
			goto IL_048b;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_445 = V_4;
		int32_t L_446 = V_1;
		NullCheck(L_445);
		int32_t L_447 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_446, (int32_t)1));
		uint8_t L_448 = (L_445)->GetAt(static_cast<il2cpp_array_size_t>(L_447));
		V_8 = L_448;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_449 = V_4;
		int32_t L_450 = V_1;
		NullCheck(L_449);
		int32_t L_451 = L_450;
		uint8_t L_452 = (L_449)->GetAt(static_cast<il2cpp_array_size_t>(L_451));
		V_9 = L_452;
	}

IL_0469:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_453 = V_5;
		int32_t L_454 = ___curMatch0;
		NullCheck(L_453);
		int32_t L_455 = ((int32_t)((int32_t)L_454&(int32_t)((int32_t)32767)));
		int16_t L_456 = (L_453)->GetAt(static_cast<il2cpp_array_size_t>(L_455));
		int32_t L_457 = ((int32_t)((int32_t)L_456&(int32_t)((int32_t)65535)));
		___curMatch0 = L_457;
		int32_t L_458 = V_3;
		if ((((int32_t)L_457) <= ((int32_t)L_458)))
		{
			goto IL_048b;
		}
	}
	{
		int32_t L_459 = V_6;
		int32_t L_460 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_459, (int32_t)1));
		V_6 = L_460;
		if (L_460)
		{
			goto IL_00a4;
		}
	}

IL_048b:
	{
		int32_t L_461 = __this->get_matchLen_4();
		return (bool)((((int32_t)((((int32_t)L_461) < ((int32_t)3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateStored(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_DeflateStored_mCA925305F6F3CC7515FE55DD3ABBF515DC038F41 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, bool ___flush0, bool ___finish1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B8_0 = 0;
	{
		bool L_0 = ___flush0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = __this->get_lookahead_8();
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		int32_t L_2 = __this->get_strstart_7();
		int32_t L_3 = __this->get_lookahead_8();
		__this->set_strstart_7(((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)L_3)));
		__this->set_lookahead_8(0);
		int32_t L_4 = __this->get_strstart_7();
		int32_t L_5 = __this->get_blockStart_6();
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)L_5));
		int32_t L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var);
		int32_t L_7 = ((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->get_MAX_BLOCK_SIZE_0();
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_8 = __this->get_blockStart_6();
		if ((((int32_t)L_8) >= ((int32_t)((int32_t)32768))))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_9 = V_0;
		G_B8_0 = ((((int32_t)((((int32_t)L_9) < ((int32_t)((int32_t)32506)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_005b;
	}

IL_0057:
	{
		G_B8_0 = 0;
		goto IL_005b;
	}

IL_005a:
	{
		G_B8_0 = 1;
	}

IL_005b:
	{
		bool L_10 = ___flush0;
		if (!((int32_t)((int32_t)G_B8_0|(int32_t)L_10)))
		{
			goto IL_00a2;
		}
	}
	{
		bool L_11 = ___finish1;
		V_1 = L_11;
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var);
		int32_t L_13 = ((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->get_MAX_BLOCK_SIZE_0();
		if ((((int32_t)L_12) <= ((int32_t)L_13)))
		{
			goto IL_0071;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var);
		int32_t L_14 = ((DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterConstants_tA6E97754DFF5DF17B6328541CB8F0D5132C4D578_il2cpp_TypeInfo_var))->get_MAX_BLOCK_SIZE_0();
		V_0 = L_14;
		V_1 = (bool)0;
	}

IL_0071:
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_15 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_16 = __this->get_window_9();
		int32_t L_17 = __this->get_blockStart_6();
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		NullCheck(L_15);
		DeflaterHuffman_FlushStoredBlock_m9E4ABC1CFA682400BB19AEA57450D550B7F450D8(L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		int32_t L_20 = __this->get_blockStart_6();
		int32_t L_21 = V_0;
		__this->set_blockStart_6(((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)L_21)));
		bool L_22 = V_1;
		if (L_22)
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_23 = V_0;
		return (bool)((!(((uint32_t)L_23) <= ((uint32_t)0)))? 1 : 0);
	}

IL_00a0:
	{
		return (bool)0;
	}

IL_00a2:
	{
		return (bool)1;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateFast(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_DeflateFast_mBF7F0E5FE39F8D6E4008A05387BE7D37CC1BD0EE (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, bool ___flush0, bool ___finish1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t G_B27_0 = 0;
	{
		int32_t L_0 = __this->get_lookahead_8();
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)262))))
		{
			goto IL_01ec;
		}
	}
	{
		bool L_1 = ___flush0;
		if (L_1)
		{
			goto IL_01ec;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		int32_t L_2 = __this->get_lookahead_8();
		if (L_2)
		{
			goto IL_0053;
		}
	}
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_3 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = __this->get_window_9();
		int32_t L_5 = __this->get_blockStart_6();
		int32_t L_6 = __this->get_strstart_7();
		int32_t L_7 = __this->get_blockStart_6();
		bool L_8 = ___finish1;
		NullCheck(L_3);
		DeflaterHuffman_FlushBlock_m3480AE4CB9E565601FE9618FAD10F25972C19AAA(L_3, L_4, L_5, ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)L_7)), L_8, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_strstart_7();
		__this->set_blockStart_6(L_9);
		return (bool)0;
	}

IL_0053:
	{
		int32_t L_10 = __this->get_strstart_7();
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)65274))))
		{
			goto IL_0066;
		}
	}
	{
		DeflaterEngine_SlideWindow_mDBA845C625026D2D96B768ED9754DD79D5954F11(__this, /*hidden argument*/NULL);
	}

IL_0066:
	{
		int32_t L_11 = __this->get_lookahead_8();
		if ((((int32_t)L_11) < ((int32_t)3)))
		{
			goto IL_015e;
		}
	}
	{
		int32_t L_12;
		L_12 = DeflaterEngine_InsertString_m5D0EB65FCC71B2A30AD3699AFB65378EAFDF1B2A(__this, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		V_0 = L_13;
		if (!L_13)
		{
			goto IL_015e;
		}
	}
	{
		int32_t L_14 = __this->get_strategy_10();
		if ((((int32_t)L_14) == ((int32_t)2)))
		{
			goto IL_015e;
		}
	}
	{
		int32_t L_15 = __this->get_strstart_7();
		int32_t L_16 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)L_16))) > ((int32_t)((int32_t)32506))))
		{
			goto IL_015e;
		}
	}
	{
		int32_t L_17 = V_0;
		bool L_18;
		L_18 = DeflaterEngine_FindLongestMatch_m1835826D27ABAB7A5A5D59A6E774233045C8245B(__this, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_015e;
		}
	}
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_19 = __this->get_huffman_21();
		int32_t L_20 = __this->get_strstart_7();
		int32_t L_21 = __this->get_matchStart_3();
		int32_t L_22 = __this->get_matchLen_4();
		NullCheck(L_19);
		bool L_23;
		L_23 = DeflaterHuffman_TallyDist_mAE5F24A91BE5C005AB7F8669A06D274B815099D6(L_19, ((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)L_21)), L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = __this->get_lookahead_8();
		int32_t L_25 = __this->get_matchLen_4();
		__this->set_lookahead_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_24, (int32_t)L_25)));
		int32_t L_26 = __this->get_matchLen_4();
		int32_t L_27 = __this->get_max_lazy_12();
		if ((((int32_t)L_26) > ((int32_t)L_27)))
		{
			goto IL_012d;
		}
	}
	{
		int32_t L_28 = __this->get_lookahead_8();
		if ((((int32_t)L_28) < ((int32_t)3)))
		{
			goto IL_012d;
		}
	}
	{
		goto IL_0109;
	}

IL_00f4:
	{
		int32_t L_29 = __this->get_strstart_7();
		__this->set_strstart_7(((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1)));
		int32_t L_30;
		L_30 = DeflaterEngine_InsertString_m5D0EB65FCC71B2A30AD3699AFB65378EAFDF1B2A(__this, /*hidden argument*/NULL);
	}

IL_0109:
	{
		int32_t L_31 = __this->get_matchLen_4();
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_31, (int32_t)1));
		int32_t L_32 = V_2;
		__this->set_matchLen_4(L_32);
		int32_t L_33 = V_2;
		if ((((int32_t)L_33) > ((int32_t)0)))
		{
			goto IL_00f4;
		}
	}
	{
		int32_t L_34 = __this->get_strstart_7();
		__this->set_strstart_7(((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1)));
		goto IL_014f;
	}

IL_012d:
	{
		int32_t L_35 = __this->get_strstart_7();
		int32_t L_36 = __this->get_matchLen_4();
		__this->set_strstart_7(((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)L_36)));
		int32_t L_37 = __this->get_lookahead_8();
		if ((((int32_t)L_37) < ((int32_t)2)))
		{
			goto IL_014f;
		}
	}
	{
		DeflaterEngine_UpdateHash_m5D5500B13BFF64919559AE7022C08A9F9A5C8D09(__this, /*hidden argument*/NULL);
	}

IL_014f:
	{
		__this->set_matchLen_4(2);
		bool L_38 = V_1;
		if (L_38)
		{
			goto IL_0199;
		}
	}
	{
		goto IL_01ec;
	}

IL_015e:
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_39 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_40 = __this->get_window_9();
		int32_t L_41 = __this->get_strstart_7();
		NullCheck(L_40);
		int32_t L_42 = L_41;
		uint8_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		NullCheck(L_39);
		bool L_44;
		L_44 = DeflaterHuffman_TallyLit_m054CCF260A35DB550E760EC54483DBCAE9DC699A(L_39, ((int32_t)((int32_t)L_43&(int32_t)((int32_t)255))), /*hidden argument*/NULL);
		int32_t L_45 = __this->get_strstart_7();
		__this->set_strstart_7(((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1)));
		int32_t L_46 = __this->get_lookahead_8();
		__this->set_lookahead_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_46, (int32_t)1)));
	}

IL_0199:
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_47 = __this->get_huffman_21();
		NullCheck(L_47);
		bool L_48;
		L_48 = DeflaterHuffman_IsFull_mAC09A062E0A78CCD17FAE0260C22B7F79374BA42(L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_01ec;
		}
	}
	{
		bool L_49 = ___finish1;
		if (!L_49)
		{
			goto IL_01b4;
		}
	}
	{
		int32_t L_50 = __this->get_lookahead_8();
		G_B27_0 = ((((int32_t)L_50) == ((int32_t)0))? 1 : 0);
		goto IL_01b5;
	}

IL_01b4:
	{
		G_B27_0 = 0;
	}

IL_01b5:
	{
		V_3 = (bool)G_B27_0;
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_51 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_52 = __this->get_window_9();
		int32_t L_53 = __this->get_blockStart_6();
		int32_t L_54 = __this->get_strstart_7();
		int32_t L_55 = __this->get_blockStart_6();
		bool L_56 = V_3;
		NullCheck(L_51);
		DeflaterHuffman_FlushBlock_m3480AE4CB9E565601FE9618FAD10F25972C19AAA(L_51, L_52, L_53, ((int32_t)il2cpp_codegen_subtract((int32_t)L_54, (int32_t)L_55)), L_56, /*hidden argument*/NULL);
		int32_t L_57 = __this->get_strstart_7();
		__this->set_blockStart_6(L_57);
		bool L_58 = V_3;
		return (bool)((((int32_t)L_58) == ((int32_t)0))? 1 : 0);
	}

IL_01ec:
	{
		int32_t L_59 = __this->get_lookahead_8();
		bool L_60 = ___flush0;
		if (((int32_t)((int32_t)((((int32_t)((((int32_t)L_59) < ((int32_t)((int32_t)262)))? 1 : 0)) == ((int32_t)0))? 1 : 0)|(int32_t)L_60)))
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateSlow(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterEngine_DeflateSlow_m7954800AA1BE94A4BEF695D92CFEC124209018A3 (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, bool ___flush0, bool ___finish1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	bool V_4 = false;
	int32_t G_B36_0 = 0;
	{
		int32_t L_0 = __this->get_lookahead_8();
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)262))))
		{
			goto IL_0253;
		}
	}
	{
		bool L_1 = ___flush0;
		if (L_1)
		{
			goto IL_0253;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		int32_t L_2 = __this->get_lookahead_8();
		if (L_2)
		{
			goto IL_0083;
		}
	}
	{
		bool L_3 = __this->get_prevAvailable_5();
		if (!L_3)
		{
			goto IL_0049;
		}
	}
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_4 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = __this->get_window_9();
		int32_t L_6 = __this->get_strstart_7();
		NullCheck(L_5);
		int32_t L_7 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1));
		uint8_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_4);
		bool L_9;
		L_9 = DeflaterHuffman_TallyLit_m054CCF260A35DB550E760EC54483DBCAE9DC699A(L_4, ((int32_t)((int32_t)L_8&(int32_t)((int32_t)255))), /*hidden argument*/NULL);
	}

IL_0049:
	{
		__this->set_prevAvailable_5((bool)0);
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_10 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = __this->get_window_9();
		int32_t L_12 = __this->get_blockStart_6();
		int32_t L_13 = __this->get_strstart_7();
		int32_t L_14 = __this->get_blockStart_6();
		bool L_15 = ___finish1;
		NullCheck(L_10);
		DeflaterHuffman_FlushBlock_m3480AE4CB9E565601FE9618FAD10F25972C19AAA(L_10, L_11, L_12, ((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)L_14)), L_15, /*hidden argument*/NULL);
		int32_t L_16 = __this->get_strstart_7();
		__this->set_blockStart_6(L_16);
		return (bool)0;
	}

IL_0083:
	{
		int32_t L_17 = __this->get_strstart_7();
		if ((((int32_t)L_17) < ((int32_t)((int32_t)65274))))
		{
			goto IL_0096;
		}
	}
	{
		DeflaterEngine_SlideWindow_mDBA845C625026D2D96B768ED9754DD79D5954F11(__this, /*hidden argument*/NULL);
	}

IL_0096:
	{
		int32_t L_18 = __this->get_matchStart_3();
		V_0 = L_18;
		int32_t L_19 = __this->get_matchLen_4();
		V_1 = L_19;
		int32_t L_20 = __this->get_lookahead_8();
		if ((((int32_t)L_20) < ((int32_t)3)))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_21;
		L_21 = DeflaterEngine_InsertString_m5D0EB65FCC71B2A30AD3699AFB65378EAFDF1B2A(__this, /*hidden argument*/NULL);
		V_2 = L_21;
		int32_t L_22 = __this->get_strategy_10();
		if ((((int32_t)L_22) == ((int32_t)2)))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_23 = V_2;
		if (!L_23)
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_24 = __this->get_strstart_7();
		int32_t L_25 = V_2;
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_24, (int32_t)L_25))) > ((int32_t)((int32_t)32506))))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_26 = V_2;
		bool L_27;
		L_27 = DeflaterEngine_FindLongestMatch_m1835826D27ABAB7A5A5D59A6E774233045C8245B(__this, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_28 = __this->get_matchLen_4();
		if ((((int32_t)L_28) > ((int32_t)5)))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_29 = __this->get_strategy_10();
		if ((((int32_t)L_29) == ((int32_t)1)))
		{
			goto IL_0107;
		}
	}
	{
		int32_t L_30 = __this->get_matchLen_4();
		if ((!(((uint32_t)L_30) == ((uint32_t)3))))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_31 = __this->get_strstart_7();
		int32_t L_32 = __this->get_matchStart_3();
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_31, (int32_t)L_32))) <= ((int32_t)((int32_t)4096))))
		{
			goto IL_010e;
		}
	}

IL_0107:
	{
		__this->set_matchLen_4(2);
	}

IL_010e:
	{
		int32_t L_33 = V_1;
		if ((((int32_t)L_33) < ((int32_t)3)))
		{
			goto IL_0199;
		}
	}
	{
		int32_t L_34 = __this->get_matchLen_4();
		int32_t L_35 = V_1;
		if ((((int32_t)L_34) > ((int32_t)L_35)))
		{
			goto IL_0199;
		}
	}
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_36 = __this->get_huffman_21();
		int32_t L_37 = __this->get_strstart_7();
		int32_t L_38 = V_0;
		int32_t L_39 = V_1;
		NullCheck(L_36);
		bool L_40;
		L_40 = DeflaterHuffman_TallyDist_mAE5F24A91BE5C005AB7F8669A06D274B815099D6(L_36, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_37, (int32_t)1)), (int32_t)L_38)), L_39, /*hidden argument*/NULL);
		int32_t L_41 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_41, (int32_t)2));
	}

IL_0139:
	{
		int32_t L_42 = __this->get_strstart_7();
		__this->set_strstart_7(((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1)));
		int32_t L_43 = __this->get_lookahead_8();
		__this->set_lookahead_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_43, (int32_t)1)));
		int32_t L_44 = __this->get_lookahead_8();
		if ((((int32_t)L_44) < ((int32_t)3)))
		{
			goto IL_0165;
		}
	}
	{
		int32_t L_45;
		L_45 = DeflaterEngine_InsertString_m5D0EB65FCC71B2A30AD3699AFB65378EAFDF1B2A(__this, /*hidden argument*/NULL);
	}

IL_0165:
	{
		int32_t L_46 = V_1;
		int32_t L_47 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_46, (int32_t)1));
		V_1 = L_47;
		if ((((int32_t)L_47) > ((int32_t)0)))
		{
			goto IL_0139;
		}
	}
	{
		int32_t L_48 = __this->get_strstart_7();
		__this->set_strstart_7(((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)1)));
		int32_t L_49 = __this->get_lookahead_8();
		__this->set_lookahead_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_49, (int32_t)1)));
		__this->set_prevAvailable_5((bool)0);
		__this->set_matchLen_4(2);
		goto IL_01e5;
	}

IL_0199:
	{
		bool L_50 = __this->get_prevAvailable_5();
		if (!L_50)
		{
			goto IL_01c2;
		}
	}
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_51 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_52 = __this->get_window_9();
		int32_t L_53 = __this->get_strstart_7();
		NullCheck(L_52);
		int32_t L_54 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_53, (int32_t)1));
		uint8_t L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		NullCheck(L_51);
		bool L_56;
		L_56 = DeflaterHuffman_TallyLit_m054CCF260A35DB550E760EC54483DBCAE9DC699A(L_51, ((int32_t)((int32_t)L_55&(int32_t)((int32_t)255))), /*hidden argument*/NULL);
	}

IL_01c2:
	{
		__this->set_prevAvailable_5((bool)1);
		int32_t L_57 = __this->get_strstart_7();
		__this->set_strstart_7(((int32_t)il2cpp_codegen_add((int32_t)L_57, (int32_t)1)));
		int32_t L_58 = __this->get_lookahead_8();
		__this->set_lookahead_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_58, (int32_t)1)));
	}

IL_01e5:
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_59 = __this->get_huffman_21();
		NullCheck(L_59);
		bool L_60;
		L_60 = DeflaterHuffman_IsFull_mAC09A062E0A78CCD17FAE0260C22B7F79374BA42(L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0253;
		}
	}
	{
		int32_t L_61 = __this->get_strstart_7();
		int32_t L_62 = __this->get_blockStart_6();
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_61, (int32_t)L_62));
		bool L_63 = __this->get_prevAvailable_5();
		if (!L_63)
		{
			goto IL_020c;
		}
	}
	{
		int32_t L_64 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_64, (int32_t)1));
	}

IL_020c:
	{
		bool L_65 = ___finish1;
		if (!L_65)
		{
			goto IL_0222;
		}
	}
	{
		int32_t L_66 = __this->get_lookahead_8();
		if (L_66)
		{
			goto IL_0222;
		}
	}
	{
		bool L_67 = __this->get_prevAvailable_5();
		G_B36_0 = ((((int32_t)L_67) == ((int32_t)0))? 1 : 0);
		goto IL_0223;
	}

IL_0222:
	{
		G_B36_0 = 0;
	}

IL_0223:
	{
		V_4 = (bool)G_B36_0;
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_68 = __this->get_huffman_21();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_69 = __this->get_window_9();
		int32_t L_70 = __this->get_blockStart_6();
		int32_t L_71 = V_3;
		bool L_72 = V_4;
		NullCheck(L_68);
		DeflaterHuffman_FlushBlock_m3480AE4CB9E565601FE9618FAD10F25972C19AAA(L_68, L_69, L_70, L_71, L_72, /*hidden argument*/NULL);
		int32_t L_73 = __this->get_blockStart_6();
		int32_t L_74 = V_3;
		__this->set_blockStart_6(((int32_t)il2cpp_codegen_add((int32_t)L_73, (int32_t)L_74)));
		bool L_75 = V_4;
		return (bool)((((int32_t)L_75) == ((int32_t)0))? 1 : 0);
	}

IL_0253:
	{
		int32_t L_76 = __this->get_lookahead_8();
		bool L_77 = ___flush0;
		if (((int32_t)((int32_t)((((int32_t)((((int32_t)L_76) < ((int32_t)((int32_t)262)))? 1 : 0)) == ((int32_t)0))? 1 : 0)|(int32_t)L_77)))
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman__cctor_m723E4AA3A5F7F92EBFF90A4CF9A14AD20052410E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____89CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7_FieldInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)19));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = L_0;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1, L_2, /*hidden argument*/NULL);
		((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->set_BL_ORDER_0(L_1);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = L_3;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_5 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____89CE0E8EA590FD37283D6BEFD9E6805C8C47ADBD_7_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_4, L_5, /*hidden argument*/NULL);
		((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->set_bit4Reverse_1(L_4);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_6 = (Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)SZArrayNew(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var, (uint32_t)((int32_t)286));
		((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->set_staticLCodes_2(L_6);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)286));
		((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->set_staticLLength_3(L_7);
		V_0 = 0;
		goto IL_006e;
	}

IL_0050:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_8 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLCodes_2();
		int32_t L_9 = V_0;
		int32_t L_10 = V_0;
		int16_t L_11;
		L_11 = DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B(((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)48), (int32_t)L_10))<<(int32_t)8)), /*hidden argument*/NULL);
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (int16_t)L_11);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLLength_3();
		int32_t L_13 = V_0;
		int32_t L_14 = L_13;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (uint8_t)8);
	}

IL_006e:
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) < ((int32_t)((int32_t)144))))
		{
			goto IL_0050;
		}
	}
	{
		goto IL_009a;
	}

IL_0078:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_16 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLCodes_2();
		int32_t L_17 = V_0;
		int32_t L_18 = V_0;
		int16_t L_19;
		L_19 = DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B(((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)256), (int32_t)L_18))<<(int32_t)7)), /*hidden argument*/NULL);
		NullCheck(L_16);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (int16_t)L_19);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_20 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLLength_3();
		int32_t L_21 = V_0;
		int32_t L_22 = L_21;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
		NullCheck(L_20);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (uint8_t)((int32_t)9));
	}

IL_009a:
	{
		int32_t L_23 = V_0;
		if ((((int32_t)L_23) < ((int32_t)((int32_t)256))))
		{
			goto IL_0078;
		}
	}
	{
		goto IL_00c6;
	}

IL_00a4:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_24 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLCodes_2();
		int32_t L_25 = V_0;
		int32_t L_26 = V_0;
		int16_t L_27;
		L_27 = DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B(((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)-256), (int32_t)L_26))<<(int32_t)((int32_t)9))), /*hidden argument*/NULL);
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_25), (int16_t)L_27);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_28 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLLength_3();
		int32_t L_29 = V_0;
		int32_t L_30 = L_29;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
		NullCheck(L_28);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(L_30), (uint8_t)7);
	}

IL_00c6:
	{
		int32_t L_31 = V_0;
		if ((((int32_t)L_31) < ((int32_t)((int32_t)280))))
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_00ee;
	}

IL_00d0:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_32 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLCodes_2();
		int32_t L_33 = V_0;
		int32_t L_34 = V_0;
		int16_t L_35;
		L_35 = DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B(((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)-88), (int32_t)L_34))<<(int32_t)8)), /*hidden argument*/NULL);
		NullCheck(L_32);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(L_33), (int16_t)L_35);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_36 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLLength_3();
		int32_t L_37 = V_0;
		int32_t L_38 = L_37;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1));
		NullCheck(L_36);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(L_38), (uint8_t)8);
	}

IL_00ee:
	{
		int32_t L_39 = V_0;
		if ((((int32_t)L_39) < ((int32_t)((int32_t)286))))
		{
			goto IL_00d0;
		}
	}
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_40 = (Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)SZArrayNew(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30));
		((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->set_staticDCodes_4(L_40);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_41 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30));
		((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->set_staticDLength_5(L_41);
		V_0 = 0;
		goto IL_012e;
	}

IL_0112:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_42 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticDCodes_4();
		int32_t L_43 = V_0;
		int32_t L_44 = V_0;
		int16_t L_45;
		L_45 = DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B(((int32_t)((int32_t)L_44<<(int32_t)((int32_t)11))), /*hidden argument*/NULL);
		NullCheck(L_42);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(L_43), (int16_t)L_45);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_46 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticDLength_5();
		int32_t L_47 = V_0;
		NullCheck(L_46);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(L_47), (uint8_t)5);
		int32_t L_48 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)1));
	}

IL_012e:
	{
		int32_t L_49 = V_0;
		if ((((int32_t)L_49) < ((int32_t)((int32_t)30))))
		{
			goto IL_0112;
		}
	}
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterPending)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman__ctor_m2B19A99445EFEB9B74D2153F80E7F4DC5792F4E1 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * ___pending0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_0 = ___pending0;
		__this->set_pending_6(L_0);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_1 = (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 *)il2cpp_codegen_object_new(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04_il2cpp_TypeInfo_var);
		Tree__ctor_mC175FAE853657848BE82C5A0C72FC6418B9C81B3(L_1, __this, ((int32_t)286), ((int32_t)257), ((int32_t)15), /*hidden argument*/NULL);
		__this->set_literalTree_7(L_1);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_2 = (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 *)il2cpp_codegen_object_new(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04_il2cpp_TypeInfo_var);
		Tree__ctor_mC175FAE853657848BE82C5A0C72FC6418B9C81B3(L_2, __this, ((int32_t)30), 1, ((int32_t)15), /*hidden argument*/NULL);
		__this->set_distTree_8(L_2);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_3 = (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 *)il2cpp_codegen_object_new(Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04_il2cpp_TypeInfo_var);
		Tree__ctor_mC175FAE853657848BE82C5A0C72FC6418B9C81B3(L_3, __this, ((int32_t)19), 4, 7, /*hidden argument*/NULL);
		__this->set_blTree_9(L_3);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_4 = (Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)SZArrayNew(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16384));
		__this->set_d_buf_10(L_4);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16384));
		__this->set_l_buf_11(L_5);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman_Reset_m695094C3E44BCC5197F58D2CF7F0CCAA51AB8599 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, const RuntimeMethod* method)
{
	{
		__this->set_last_lit_12(0);
		__this->set_extra_bits_13(0);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_0 = __this->get_literalTree_7();
		NullCheck(L_0);
		Tree_Reset_m88D2AC4D14EE34A72036C8440EE3ADFCDAB75698(L_0, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_1 = __this->get_distTree_8();
		NullCheck(L_1);
		Tree_Reset_m88D2AC4D14EE34A72036C8440EE3ADFCDAB75698(L_1, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_2 = __this->get_blTree_9();
		NullCheck(L_2);
		Tree_Reset_m88D2AC4D14EE34A72036C8440EE3ADFCDAB75698(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::SendAllTrees(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman_SendAllTrees_m012E9E03627932FA1908F0B9A93872973802A748 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, int32_t ___blTreeCodes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_0 = __this->get_blTree_9();
		NullCheck(L_0);
		Tree_BuildCodes_mA2357168306F9D5F06255D9ED89B1C646039FBDC(L_0, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_1 = __this->get_literalTree_7();
		NullCheck(L_1);
		Tree_BuildCodes_mA2357168306F9D5F06255D9ED89B1C646039FBDC(L_1, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_2 = __this->get_distTree_8();
		NullCheck(L_2);
		Tree_BuildCodes_mA2357168306F9D5F06255D9ED89B1C646039FBDC(L_2, /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_3 = __this->get_pending_6();
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_4 = __this->get_literalTree_7();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_numCodes_3();
		NullCheck(L_3);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_3, ((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)((int32_t)257))), 5, /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_6 = __this->get_pending_6();
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_7 = __this->get_distTree_8();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_numCodes_3();
		NullCheck(L_6);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_6, ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)), 5, /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_9 = __this->get_pending_6();
		int32_t L_10 = ___blTreeCodes0;
		NullCheck(L_9);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_9, ((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)4)), 4, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_008d;
	}

IL_006a:
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_11 = __this->get_pending_6();
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_12 = __this->get_blTree_9();
		NullCheck(L_12);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_13 = L_12->get_length_1();
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_14 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_BL_ORDER_0();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_13);
		int32_t L_18 = L_17;
		uint8_t L_19 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_11);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_11, L_19, 3, /*hidden argument*/NULL);
		int32_t L_20 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_008d:
	{
		int32_t L_21 = V_0;
		int32_t L_22 = ___blTreeCodes0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_006a;
		}
	}
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_23 = __this->get_literalTree_7();
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_24 = __this->get_blTree_9();
		NullCheck(L_23);
		Tree_WriteTree_mAE4872807EDE476D00318D47C89ED01F9E95882A(L_23, L_24, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_25 = __this->get_distTree_8();
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_26 = __this->get_blTree_9();
		NullCheck(L_25);
		Tree_WriteTree_mAE4872807EDE476D00318D47C89ED01F9E95882A(L_25, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::CompressBlock()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman_CompressBlock_m59A757C733FEAA3CE8288F098417E05A31E35D78 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_0 = 0;
		goto IL_00b2;
	}

IL_0007:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_l_buf_11();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = ((int32_t)((int32_t)L_3&(int32_t)((int32_t)255)));
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_4 = __this->get_d_buf_10();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		int32_t L_8 = V_2;
		int32_t L_9 = L_8;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)1));
		if (!L_9)
		{
			goto IL_00a2;
		}
	}
	{
		int32_t L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		int32_t L_11;
		L_11 = DeflaterHuffman_Lcode_m366E178D7EAF0A531BC9970CA28376AD2186D528(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_12 = __this->get_literalTree_7();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA(L_12, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_3;
		V_4 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)((int32_t)261)))/(int32_t)4));
		int32_t L_15 = V_4;
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_16 = V_4;
		if ((((int32_t)L_16) > ((int32_t)5)))
		{
			goto IL_0066;
		}
	}
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_17 = __this->get_pending_6();
		int32_t L_18 = V_1;
		int32_t L_19 = V_4;
		int32_t L_20 = V_4;
		NullCheck(L_17);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_17, ((int32_t)((int32_t)L_18&(int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_19&(int32_t)((int32_t)31))))), (int32_t)1)))), L_20, /*hidden argument*/NULL);
	}

IL_0066:
	{
		int32_t L_21 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		int32_t L_22;
		L_22 = DeflaterHuffman_Dcode_m606D9F263A5BB3ABAC73BD88317213EA3728CD1A(L_21, /*hidden argument*/NULL);
		V_5 = L_22;
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_23 = __this->get_distTree_8();
		int32_t L_24 = V_5;
		NullCheck(L_23);
		Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA(L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_5;
		V_4 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)L_25/(int32_t)2)), (int32_t)1));
		int32_t L_26 = V_4;
		if ((((int32_t)L_26) <= ((int32_t)0)))
		{
			goto IL_00ae;
		}
	}
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_27 = __this->get_pending_6();
		int32_t L_28 = V_2;
		int32_t L_29 = V_4;
		int32_t L_30 = V_4;
		NullCheck(L_27);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_27, ((int32_t)((int32_t)L_28&(int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_29&(int32_t)((int32_t)31))))), (int32_t)1)))), L_30, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_00a2:
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_31 = __this->get_literalTree_7();
		int32_t L_32 = V_1;
		NullCheck(L_31);
		Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA(L_31, L_32, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		int32_t L_33 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_00b2:
	{
		int32_t L_34 = V_0;
		int32_t L_35 = __this->get_last_lit_12();
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_0007;
		}
	}
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_36 = __this->get_literalTree_7();
		NullCheck(L_36);
		Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA(L_36, ((int32_t)256), /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushStoredBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman_FlushStoredBlock_m9E4ABC1CFA682400BB19AEA57450D550B7F450D8 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___stored0, int32_t ___storedOffset1, int32_t ___storedLength2, bool ___lastBlock3, const RuntimeMethod* method)
{
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * G_B2_0 = NULL;
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * G_B3_1 = NULL;
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_0 = __this->get_pending_6();
		bool L_1 = ___lastBlock3;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		NullCheck(G_B3_1);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(G_B3_1, G_B3_0, 3, /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_2 = __this->get_pending_6();
		NullCheck(L_2);
		PendingBuffer_AlignToByte_m17AE6E041B765E2F686F17560EE7D22E763271D4(L_2, /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_3 = __this->get_pending_6();
		int32_t L_4 = ___storedLength2;
		NullCheck(L_3);
		PendingBuffer_WriteShort_mFD2AE37F8EC3E199F8E16E360B785246C287552B(L_3, L_4, /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_5 = __this->get_pending_6();
		int32_t L_6 = ___storedLength2;
		NullCheck(L_5);
		PendingBuffer_WriteShort_mFD2AE37F8EC3E199F8E16E360B785246C287552B(L_5, ((~L_6)), /*hidden argument*/NULL);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_7 = __this->get_pending_6();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8 = ___stored0;
		int32_t L_9 = ___storedOffset1;
		int32_t L_10 = ___storedLength2;
		NullCheck(L_7);
		PendingBuffer_WriteBlock_m139A76BB21F2AA07C21B3B701F651A7B24903840(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		DeflaterHuffman_Reset_m695094C3E44BCC5197F58D2CF7F0CCAA51AB8599(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterHuffman_FlushBlock_m3480AE4CB9E565601FE9618FAD10F25972C19AAA (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___stored0, int32_t ___storedOffset1, int32_t ___storedLength2, bool ___lastBlock3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t G_B19_0 = 0;
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * G_B19_1 = NULL;
	int32_t G_B18_0 = 0;
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * G_B18_1 = NULL;
	int32_t G_B20_0 = 0;
	int32_t G_B20_1 = 0;
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * G_B20_2 = NULL;
	int32_t G_B23_0 = 0;
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * G_B23_1 = NULL;
	int32_t G_B22_0 = 0;
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * G_B22_1 = NULL;
	int32_t G_B24_0 = 0;
	int32_t G_B24_1 = 0;
	DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * G_B24_2 = NULL;
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_0 = __this->get_literalTree_7();
		NullCheck(L_0);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_1 = L_0->get_freqs_0();
		NullCheck(L_1);
		int16_t* L_2 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)256))));
		int32_t L_3 = *((int16_t*)L_2);
		*((int16_t*)L_2) = (int16_t)((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1))));
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_4 = __this->get_literalTree_7();
		NullCheck(L_4);
		Tree_BuildTree_m5FF35C4C61B273025E9B99F2B6C17AFDF1C917E4(L_4, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_5 = __this->get_distTree_8();
		NullCheck(L_5);
		Tree_BuildTree_m5FF35C4C61B273025E9B99F2B6C17AFDF1C917E4(L_5, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_6 = __this->get_literalTree_7();
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_7 = __this->get_blTree_9();
		NullCheck(L_6);
		Tree_CalcBLFreq_m81CFB0D0DE3A835DF438203762616C9ECBA4B7AC(L_6, L_7, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_8 = __this->get_distTree_8();
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_9 = __this->get_blTree_9();
		NullCheck(L_8);
		Tree_CalcBLFreq_m81CFB0D0DE3A835DF438203762616C9ECBA4B7AC(L_8, L_9, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_10 = __this->get_blTree_9();
		NullCheck(L_10);
		Tree_BuildTree_m5FF35C4C61B273025E9B99F2B6C17AFDF1C917E4(L_10, /*hidden argument*/NULL);
		V_0 = 4;
		V_3 = ((int32_t)18);
		goto IL_0083;
	}

IL_0065:
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_11 = __this->get_blTree_9();
		NullCheck(L_11);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12 = L_11->get_length_1();
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_13 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_BL_ORDER_0();
		int32_t L_14 = V_3;
		NullCheck(L_13);
		int32_t L_15 = L_14;
		int32_t L_16 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_12);
		int32_t L_17 = L_16;
		uint8_t L_18 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_19 = V_3;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_007f:
	{
		int32_t L_20 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)1));
	}

IL_0083:
	{
		int32_t L_21 = V_3;
		int32_t L_22 = V_0;
		if ((((int32_t)L_21) > ((int32_t)L_22)))
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_23 = V_0;
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_24 = __this->get_blTree_9();
		NullCheck(L_24);
		int32_t L_25;
		L_25 = Tree_GetEncodedLength_m54044E0BBD9F68EFA7B3B2EE99BB131B45993997(L_24, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_26 = __this->get_literalTree_7();
		NullCheck(L_26);
		int32_t L_27;
		L_27 = Tree_GetEncodedLength_m54044E0BBD9F68EFA7B3B2EE99BB131B45993997(L_26, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_28 = __this->get_distTree_8();
		NullCheck(L_28);
		int32_t L_29;
		L_29 = Tree_GetEncodedLength_m54044E0BBD9F68EFA7B3B2EE99BB131B45993997(L_28, /*hidden argument*/NULL);
		int32_t L_30 = __this->get_extra_bits_13();
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)14), (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_23, (int32_t)3)))), (int32_t)L_25)), (int32_t)L_27)), (int32_t)L_29)), (int32_t)L_30));
		int32_t L_31 = __this->get_extra_bits_13();
		V_2 = L_31;
		V_4 = 0;
		goto IL_00e5;
	}

IL_00c5:
	{
		int32_t L_32 = V_2;
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_33 = __this->get_literalTree_7();
		NullCheck(L_33);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_34 = L_33->get_freqs_0();
		int32_t L_35 = V_4;
		NullCheck(L_34);
		int32_t L_36 = L_35;
		int16_t L_37 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_36));
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_38 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLLength_3();
		int32_t L_39 = V_4;
		NullCheck(L_38);
		int32_t L_40 = L_39;
		uint8_t L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_37, (int32_t)L_41))));
		int32_t L_42 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1));
	}

IL_00e5:
	{
		int32_t L_43 = V_4;
		if ((((int32_t)L_43) < ((int32_t)((int32_t)286))))
		{
			goto IL_00c5;
		}
	}
	{
		V_5 = 0;
		goto IL_0113;
	}

IL_00f3:
	{
		int32_t L_44 = V_2;
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_45 = __this->get_distTree_8();
		NullCheck(L_45);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_46 = L_45->get_freqs_0();
		int32_t L_47 = V_5;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		int16_t L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_50 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticDLength_5();
		int32_t L_51 = V_5;
		NullCheck(L_50);
		int32_t L_52 = L_51;
		uint8_t L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_49, (int32_t)L_53))));
		int32_t L_54 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_54, (int32_t)1));
	}

IL_0113:
	{
		int32_t L_55 = V_5;
		if ((((int32_t)L_55) < ((int32_t)((int32_t)30))))
		{
			goto IL_00f3;
		}
	}
	{
		int32_t L_56 = V_1;
		int32_t L_57 = V_2;
		if ((((int32_t)L_56) < ((int32_t)L_57)))
		{
			goto IL_011f;
		}
	}
	{
		int32_t L_58 = V_2;
		V_1 = L_58;
	}

IL_011f:
	{
		int32_t L_59 = ___storedOffset1;
		if ((((int32_t)L_59) < ((int32_t)0)))
		{
			goto IL_0137;
		}
	}
	{
		int32_t L_60 = ___storedLength2;
		int32_t L_61 = V_1;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_60, (int32_t)4))) >= ((int32_t)((int32_t)((int32_t)L_61>>(int32_t)3)))))
		{
			goto IL_0137;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_62 = ___stored0;
		int32_t L_63 = ___storedOffset1;
		int32_t L_64 = ___storedLength2;
		bool L_65 = ___lastBlock3;
		DeflaterHuffman_FlushStoredBlock_m9E4ABC1CFA682400BB19AEA57450D550B7F450D8(__this, L_62, L_63, L_64, L_65, /*hidden argument*/NULL);
		return;
	}

IL_0137:
	{
		int32_t L_66 = V_1;
		int32_t L_67 = V_2;
		if ((!(((uint32_t)L_66) == ((uint32_t)L_67))))
		{
			goto IL_0188;
		}
	}
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_68 = __this->get_pending_6();
		bool L_69 = ___lastBlock3;
		G_B18_0 = 2;
		G_B18_1 = L_68;
		if (L_69)
		{
			G_B19_0 = 2;
			G_B19_1 = L_68;
			goto IL_0149;
		}
	}
	{
		G_B20_0 = 0;
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		goto IL_014a;
	}

IL_0149:
	{
		G_B20_0 = 1;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
	}

IL_014a:
	{
		NullCheck(G_B20_2);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(G_B20_2, ((int32_t)il2cpp_codegen_add((int32_t)G_B20_1, (int32_t)G_B20_0)), 3, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_70 = __this->get_literalTree_7();
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_71 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLCodes_2();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_72 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticLLength_3();
		NullCheck(L_70);
		Tree_SetStaticCodes_m0527B8E8E0520CA78EF84CB97DA604434F6C63EA(L_70, L_71, L_72, /*hidden argument*/NULL);
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_73 = __this->get_distTree_8();
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_74 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticDCodes_4();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_75 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_staticDLength_5();
		NullCheck(L_73);
		Tree_SetStaticCodes_m0527B8E8E0520CA78EF84CB97DA604434F6C63EA(L_73, L_74, L_75, /*hidden argument*/NULL);
		DeflaterHuffman_CompressBlock_m59A757C733FEAA3CE8288F098417E05A31E35D78(__this, /*hidden argument*/NULL);
		DeflaterHuffman_Reset_m695094C3E44BCC5197F58D2CF7F0CCAA51AB8599(__this, /*hidden argument*/NULL);
		return;
	}

IL_0188:
	{
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_76 = __this->get_pending_6();
		bool L_77 = ___lastBlock3;
		G_B22_0 = 4;
		G_B22_1 = L_76;
		if (L_77)
		{
			G_B23_0 = 4;
			G_B23_1 = L_76;
			goto IL_0196;
		}
	}
	{
		G_B24_0 = 0;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		goto IL_0197;
	}

IL_0196:
	{
		G_B24_0 = 1;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
	}

IL_0197:
	{
		NullCheck(G_B24_2);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(G_B24_2, ((int32_t)il2cpp_codegen_add((int32_t)G_B24_1, (int32_t)G_B24_0)), 3, /*hidden argument*/NULL);
		int32_t L_78 = V_0;
		DeflaterHuffman_SendAllTrees_m012E9E03627932FA1908F0B9A93872973802A748(__this, L_78, /*hidden argument*/NULL);
		DeflaterHuffman_CompressBlock_m59A757C733FEAA3CE8288F098417E05A31E35D78(__this, /*hidden argument*/NULL);
		DeflaterHuffman_Reset_m695094C3E44BCC5197F58D2CF7F0CCAA51AB8599(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::IsFull()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterHuffman_IsFull_mAC09A062E0A78CCD17FAE0260C22B7F79374BA42 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_last_lit_12();
		return (bool)((((int32_t)((((int32_t)L_0) < ((int32_t)((int32_t)16384)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyLit(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterHuffman_TallyLit_m054CCF260A35DB550E760EC54483DBCAE9DC699A (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, int32_t ___literal0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_0 = __this->get_d_buf_10();
		int32_t L_1 = __this->get_last_lit_12();
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int16_t)0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = __this->get_l_buf_11();
		int32_t L_3 = __this->get_last_lit_12();
		V_0 = L_3;
		int32_t L_4 = V_0;
		__this->set_last_lit_12(((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)));
		int32_t L_5 = V_0;
		int32_t L_6 = ___literal0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (uint8_t)((int32_t)((uint8_t)L_6)));
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_7 = __this->get_literalTree_7();
		NullCheck(L_7);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_8 = L_7->get_freqs_0();
		int32_t L_9 = ___literal0;
		NullCheck(L_8);
		int16_t* L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)));
		int32_t L_11 = *((int16_t*)L_10);
		*((int16_t*)L_10) = (int16_t)((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1))));
		bool L_12;
		L_12 = DeflaterHuffman_IsFull_mAC09A062E0A78CCD17FAE0260C22B7F79374BA42(__this, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyDist(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterHuffman_TallyDist_mAE5F24A91BE5C005AB7F8669A06D274B815099D6 (DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * __this, int32_t ___distance0, int32_t ___length1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_0 = __this->get_d_buf_10();
		int32_t L_1 = __this->get_last_lit_12();
		int32_t L_2 = ___distance0;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int16_t)((int16_t)((int16_t)L_2)));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = __this->get_l_buf_11();
		int32_t L_4 = __this->get_last_lit_12();
		V_2 = L_4;
		int32_t L_5 = V_2;
		__this->set_last_lit_12(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)));
		int32_t L_6 = V_2;
		int32_t L_7 = ___length1;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (uint8_t)((int32_t)((uint8_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)3)))));
		int32_t L_8 = ___length1;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		int32_t L_9;
		L_9 = DeflaterHuffman_Lcode_m366E178D7EAF0A531BC9970CA28376AD2186D528(((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)3)), /*hidden argument*/NULL);
		V_0 = L_9;
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_10 = __this->get_literalTree_7();
		NullCheck(L_10);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_11 = L_10->get_freqs_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int16_t* L_13 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)));
		int32_t L_14 = *((int16_t*)L_13);
		*((int16_t*)L_13) = (int16_t)((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1))));
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) < ((int32_t)((int32_t)265))))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_16 = V_0;
		if ((((int32_t)L_16) >= ((int32_t)((int32_t)285))))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_17 = __this->get_extra_bits_13();
		int32_t L_18 = V_0;
		__this->set_extra_bits_13(((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)((int32_t)261)))/(int32_t)4)))));
	}

IL_0071:
	{
		int32_t L_19 = ___distance0;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		int32_t L_20;
		L_20 = DeflaterHuffman_Dcode_m606D9F263A5BB3ABAC73BD88317213EA3728CD1A(((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_20;
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_21 = __this->get_distTree_8();
		NullCheck(L_21);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_22 = L_21->get_freqs_0();
		int32_t L_23 = V_1;
		NullCheck(L_22);
		int16_t* L_24 = ((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)));
		int32_t L_25 = *((int16_t*)L_24);
		*((int16_t*)L_24) = (int16_t)((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1))));
		int32_t L_26 = V_1;
		if ((((int32_t)L_26) < ((int32_t)4)))
		{
			goto IL_00a7;
		}
	}
	{
		int32_t L_27 = __this->get_extra_bits_13();
		int32_t L_28 = V_1;
		__this->set_extra_bits_13(((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)L_28/(int32_t)2)), (int32_t)1)))));
	}

IL_00a7:
	{
		bool L_29;
		L_29 = DeflaterHuffman_IsFull_mAC09A062E0A78CCD17FAE0260C22B7F79374BA42(__this, /*hidden argument*/NULL);
		return L_29;
	}
}
// System.Int16 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::BitReverse(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B (int32_t ___toReverse0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_bit4Reverse_1();
		int32_t L_1 = ___toReverse0;
		NullCheck(L_0);
		int32_t L_2 = ((int32_t)((int32_t)L_1&(int32_t)((int32_t)15)));
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_bit4Reverse_1();
		int32_t L_5 = ___toReverse0;
		NullCheck(L_4);
		int32_t L_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5>>(int32_t)4))&(int32_t)((int32_t)15)));
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_bit4Reverse_1();
		int32_t L_9 = ___toReverse0;
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9>>(int32_t)8))&(int32_t)((int32_t)15)));
		uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12 = ((DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var))->get_bit4Reverse_1();
		int32_t L_13 = ___toReverse0;
		NullCheck(L_12);
		int32_t L_14 = ((int32_t)((int32_t)L_13>>(int32_t)((int32_t)12)));
		uint8_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		return ((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3<<(int32_t)((int32_t)12)))|(int32_t)((int32_t)((int32_t)L_7<<(int32_t)8))))|(int32_t)((int32_t)((int32_t)L_11<<(int32_t)4))))|(int32_t)L_15))));
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Lcode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DeflaterHuffman_Lcode_m366E178D7EAF0A531BC9970CA28376AD2186D528 (int32_t ___length0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___length0;
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)255)))))
		{
			goto IL_000e;
		}
	}
	{
		return ((int32_t)285);
	}

IL_000e:
	{
		V_0 = ((int32_t)257);
		goto IL_001f;
	}

IL_0016:
	{
		int32_t L_1 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)4));
		int32_t L_2 = ___length0;
		___length0 = ((int32_t)((int32_t)L_2>>(int32_t)1));
	}

IL_001f:
	{
		int32_t L_3 = ___length0;
		if ((((int32_t)L_3) >= ((int32_t)8)))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = ___length0;
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Dcode(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DeflaterHuffman_Dcode_m606D9F263A5BB3ABAC73BD88317213EA3728CD1A (int32_t ___distance0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_000d;
	}

IL_0004:
	{
		int32_t L_0 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)2));
		int32_t L_1 = ___distance0;
		___distance0 = ((int32_t)((int32_t)L_1>>(int32_t)1));
	}

IL_000d:
	{
		int32_t L_2 = ___distance0;
		if ((((int32_t)L_2) >= ((int32_t)4)))
		{
			goto IL_0004;
		}
	}
	{
		int32_t L_3 = V_0;
		int32_t L_4 = ___distance0;
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)L_4));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,FMETP.SharpZipLib.Zip.Compression.Deflater,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream__ctor_mB41165AE84240A90382E4B53F11B299A4CDD58A1 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseOutputStream0, Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * ___deflater1, int32_t ___bufferSize2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__IsStreamOwner_5((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_il2cpp_TypeInfo_var);
		Stream__ctor_m5EB0B4BCC014E7D1F18FE0E72B2D6D0C5C13D5C4(__this, /*hidden argument*/NULL);
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = ___baseOutputStream0;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_1 = ___baseOutputStream0;
		NullCheck(L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_3 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterOutputStream__ctor_mB41165AE84240A90382E4B53F11B299A4CDD58A1_RuntimeMethod_var)));
	}

IL_001c:
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_4 = ___baseOutputStream0;
		NullCheck(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.IO.Stream::get_CanWrite() */, L_4);
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_6 = ___baseOutputStream0;
		NullCheck(L_6);
		String_t* L_7;
		L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_8 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m71044C2110E357B71A1C30D2561C3F861AF1DC0D(L_8, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral906BBE5F7CD672B7E71DFD5F848785C3BC25F023)), L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterOutputStream__ctor_mB41165AE84240A90382E4B53F11B299A4CDD58A1_RuntimeMethod_var)));
	}

IL_0035:
	{
		int32_t L_9 = ___bufferSize2;
		if ((((int32_t)L_9) >= ((int32_t)((int32_t)512))))
		{
			goto IL_004a;
		}
	}
	{
		String_t* L_10;
		L_10 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___bufferSize2), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_11 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_11, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterOutputStream__ctor_mB41165AE84240A90382E4B53F11B299A4CDD58A1_RuntimeMethod_var)));
	}

IL_004a:
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_12 = ___baseOutputStream0;
		__this->set_baseOutputStream__9(L_12);
		int32_t L_13 = ___bufferSize2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_14 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_13);
		__this->set_buffer__7(L_14);
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_15 = ___deflater1;
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_16 = ___deflater1;
		__this->set_deflater__8(L_16);
		return;
	}

IL_0068:
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_17 = ___deflater1;
		NullCheck(L_17);
		String_t* L_18;
		L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_19 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_19, L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterOutputStream__ctor_mB41165AE84240A90382E4B53F11B299A4CDD58A1_RuntimeMethod_var)));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_Finish_m0B586B7D62DFF8763BA25CB632AA7DD4F2EC2499 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_0 = __this->get_deflater__8();
		NullCheck(L_0);
		Deflater_Finish_mBB8D02EF15BCCC6A0DCD0560C80BC7F707824F60(L_0, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_000d:
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_1 = __this->get_deflater__8();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = __this->get_buffer__7();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = __this->get_buffer__7();
		NullCheck(L_3);
		NullCheck(L_1);
		int32_t L_4;
		L_4 = Deflater_Deflate_m4B828E32CABE4976218147C102256B124DBB3A9C(L_1, L_2, 0, ((int32_t)((int32_t)(((RuntimeArray*)L_3)->max_length))), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0062;
		}
	}
	{
		RuntimeObject* L_6 = __this->get_cryptoTransform__6();
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = __this->get_buffer__7();
		int32_t L_8 = V_0;
		DeflaterOutputStream_EncryptBlock_m6C13E0059BA831A5AE1863991316BDE10F96ACF9(__this, L_7, 0, L_8, /*hidden argument*/NULL);
	}

IL_0042:
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_9 = __this->get_baseOutputStream__9();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = __this->get_buffer__7();
		int32_t L_11 = V_0;
		NullCheck(L_9);
		VirtActionInvoker3< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t >::Invoke(27 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_9, L_10, 0, L_11);
	}

IL_0055:
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_12 = __this->get_deflater__8();
		NullCheck(L_12);
		bool L_13;
		L_13 = Deflater_get_IsFinished_m84E8F8D8DE052B1016C5E57958E60F271146D446(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_000d;
		}
	}

IL_0062:
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_14 = __this->get_deflater__8();
		NullCheck(L_14);
		bool L_15;
		L_15 = Deflater_get_IsFinished_m84E8F8D8DE052B1016C5E57958E60F271146D446(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_007a;
		}
	}
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_16 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_16, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral245B50E2C13208F7C0CD450C27207D86AF461D75)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterOutputStream_Finish_m0B586B7D62DFF8763BA25CB632AA7DD4F2EC2499_RuntimeMethod_var)));
	}

IL_007a:
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_17 = __this->get_baseOutputStream__9();
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(17 /* System.Void System.IO.Stream::Flush() */, L_17);
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_IsStreamOwner()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterOutputStream_get_IsStreamOwner_m3023CE9D69403AC66C712F5CC82EB88D8B395E61 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get__IsStreamOwner_5();
		return L_0;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::EncryptBlock(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_EncryptBlock_m6C13E0059BA831A5AE1863991316BDE10F96ACF9 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICryptoTransform_t001D97000AA0178942D19FC52942472140472E5E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_cryptoTransform__6();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___buffer0;
		int32_t L_2 = ___length2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = ___buffer0;
		NullCheck(L_0);
		int32_t L_4;
		L_4 = InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t001D97000AA0178942D19FC52942472140472E5E_il2cpp_TypeInfo_var, L_0, L_1, 0, L_2, L_3, 0);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_Deflate_m8300F455B747EED583C35BE29A3D84A71FF42B7B (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		DeflaterOutputStream_Deflate_mFD09C1AC5EC2990148643BDA9FDED4A88E515B0B(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_Deflate_mFD09C1AC5EC2990148643BDA9FDED4A88E515B0B (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, bool ___flushing0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		goto IL_004a;
	}

IL_0002:
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_0 = __this->get_deflater__8();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = __this->get_buffer__7();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = __this->get_buffer__7();
		NullCheck(L_2);
		NullCheck(L_0);
		int32_t L_3;
		L_3 = Deflater_Deflate_m4B828E32CABE4976218147C102256B124DBB3A9C(L_0, L_1, 0, ((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		RuntimeObject* L_5 = __this->get_cryptoTransform__6();
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = __this->get_buffer__7();
		int32_t L_7 = V_0;
		DeflaterOutputStream_EncryptBlock_m6C13E0059BA831A5AE1863991316BDE10F96ACF9(__this, L_6, 0, L_7, /*hidden argument*/NULL);
	}

IL_0037:
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_8 = __this->get_baseOutputStream__9();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9 = __this->get_buffer__7();
		int32_t L_10 = V_0;
		NullCheck(L_8);
		VirtActionInvoker3< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t >::Invoke(27 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_8, L_9, 0, L_10);
	}

IL_004a:
	{
		bool L_11 = ___flushing0;
		if (L_11)
		{
			goto IL_0002;
		}
	}
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_12 = __this->get_deflater__8();
		NullCheck(L_12);
		bool L_13;
		L_13 = Deflater_get_IsNeedingInput_mB8C863FBF533F2C8BD42C9728907E092EDAEA899(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0002;
		}
	}

IL_005a:
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_14 = __this->get_deflater__8();
		NullCheck(L_14);
		bool L_15;
		L_15 = Deflater_get_IsNeedingInput_mB8C863FBF533F2C8BD42C9728907E092EDAEA899(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0072;
		}
	}
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_16 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_16, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralABA8F64A9F94A564D9AC678557D76653D0CD2628)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterOutputStream_Deflate_mFD09C1AC5EC2990148643BDA9FDED4A88E515B0B_RuntimeMethod_var)));
	}

IL_0072:
	{
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanRead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterOutputStream_get_CanRead_m036DB4EA3070204EC64FECAA891CA131BCF57DCF (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanSeek()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterOutputStream_get_CanSeek_m64D0AD4B961EF54B7029ED70BB36B159740BC1DB (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanWrite()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool DeflaterOutputStream_get_CanWrite_m2069B75439F0BCC32E24DB5EFC3FED753D5DC97A (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = __this->get_baseOutputStream__9();
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean System.IO.Stream::get_CanWrite() */, L_0);
		return L_1;
	}
}
// System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t DeflaterOutputStream_get_Length_mCA97C224C22168134C2DDABD2B4B3405CA44E126 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = __this->get_baseOutputStream__9();
		NullCheck(L_0);
		int64_t L_1;
		L_1 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, L_0);
		return L_1;
	}
}
// System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t DeflaterOutputStream_get_Position_mD75B65440EEDED8F83F2E071C04FADA083436F11 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = __this->get_baseOutputStream__9();
		NullCheck(L_0);
		int64_t L_1;
		L_1 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		return L_1;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::set_Position(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_set_Position_mDF47ADFCA9BF73AD629D520EA8563A6C2795E95F (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_0, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral9413892BFE723FDF603E2A7F4DCECB3703459A95)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterOutputStream_set_Position_mDF47ADFCA9BF73AD629D520EA8563A6C2795E95F_RuntimeMethod_var)));
	}
}
// System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Seek(System.Int64,System.IO.SeekOrigin)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t DeflaterOutputStream_Seek_mD6DE8991D472B16453F824CDC50160028B5DDD68 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, int64_t ___offset0, int32_t ___origin1, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_0, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralCBDDDDE911E2F08DD3A4D52E8CFF5D244531824C)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterOutputStream_Seek_mD6DE8991D472B16453F824CDC50160028B5DDD68_RuntimeMethod_var)));
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::ReadByte()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DeflaterOutputStream_ReadByte_mACE3AF54BD8F42F3FD2290D30A854F9CC0FAC88A (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_0, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralCCF12B74DC9FC35FD79F50B6BD9E5803766CD12B)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterOutputStream_ReadByte_mACE3AF54BD8F42F3FD2290D30A854F9CC0FAC88A_RuntimeMethod_var)));
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Read(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DeflaterOutputStream_Read_m8480D2238C5FE7F4D0B13A6752ACFEC62FBE86FE (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_0, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral30CAD18346B4AC106F4C27BDD5FF7775D7F71A38)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DeflaterOutputStream_Read_m8480D2238C5FE7F4D0B13A6752ACFEC62FBE86FE_RuntimeMethod_var)));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Flush()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_Flush_m141FAC28B8AAC682115B212F4A82756D216A6BE9 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_0 = __this->get_deflater__8();
		NullCheck(L_0);
		Deflater_Flush_mCC6C370B48AC60F627C8A37E0363D76C951E4B07(L_0, /*hidden argument*/NULL);
		DeflaterOutputStream_Deflate_mFD09C1AC5EC2990148643BDA9FDED4A88E515B0B(__this, (bool)1, /*hidden argument*/NULL);
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_1 = __this->get_baseOutputStream__9();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(17 /* System.Void System.IO.Stream::Flush() */, L_1);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_Dispose_m0859A0D7B542948EA2174A3934D9D8A0E41348CE (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		bool L_0 = __this->get_isClosed__10();
		if (L_0)
		{
			goto IL_004b;
		}
	}
	{
		__this->set_isClosed__10((bool)1);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker0::Invoke(29 /* System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish() */, __this);
			RuntimeObject* L_1 = __this->get_cryptoTransform__6();
			if (!L_1)
			{
				goto IL_0035;
			}
		}

IL_001d:
		{
			DeflaterOutputStream_GetAuthCodeIfAES_mF293FA59B514BCD46C50C97764D534CB1191EB54(__this, /*hidden argument*/NULL);
			RuntimeObject* L_2 = __this->get_cryptoTransform__6();
			NullCheck(L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_2);
			__this->set_cryptoTransform__6((RuntimeObject*)NULL);
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x4B, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			bool L_3;
			L_3 = DeflaterOutputStream_get_IsStreamOwner_m3023CE9D69403AC66C712F5CC82EB88D8B395E61_inline(__this, /*hidden argument*/NULL);
			if (!L_3)
			{
				goto IL_004a;
			}
		}

IL_003f:
		{
			Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_4 = __this->get_baseOutputStream__9();
			NullCheck(L_4);
			Stream_Dispose_m117324084DDAD414761AD29FB17A419840BA6EA0(L_4, /*hidden argument*/NULL);
		}

IL_004a:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
	}

IL_004b:
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::GetAuthCodeIfAES()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_GetAuthCodeIfAES_mF293FA59B514BCD46C50C97764D534CB1191EB54 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::WriteByte(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_WriteByte_m6A1E6EC2B811A4FA74D2BA8C932D7F27CCF7A457 (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, uint8_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)1);
		V_0 = L_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = V_0;
		uint8_t L_2 = ___value0;
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_2);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = V_0;
		VirtActionInvoker3< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t >::Invoke(27 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, __this, L_3, 0, 1);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Write(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream_Write_m3B8E53BB694B9FEFBDAA9FF03BE50F4A41BDF12A (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	{
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_0 = __this->get_deflater__8();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___buffer0;
		int32_t L_2 = ___offset1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		Deflater_SetInput_mD20D223304B17FE2D9637620A767625C0A3619A8(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		DeflaterOutputStream_Deflate_m8300F455B747EED583C35BE29A3D84A71FF42B7B(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterOutputStream__cctor_mC050D00F80DCFFC143B164FC42E88A868EAA222C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * L_0;
		L_0 = RandomNumberGenerator_Create_m04A5418F8572F0498EE0659633B4C0620CB55721(/*hidden argument*/NULL);
		((DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E_StaticFields*)il2cpp_codegen_static_fields_for(DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E_il2cpp_TypeInfo_var))->set__aesRnd_11(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterPending::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeflaterPending__ctor_m5F119C4BFC420BEB34433D4F5422C464B159B6A7 (DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * __this, const RuntimeMethod* method)
{
	{
		PendingBuffer__ctor_mD43BCBDDD48F118DE0DA5A277D973B569C0D6041(__this, ((int32_t)65536), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] FMZipHelper::FMZipBytes(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* FMZipHelper_FMZipBytes_mFD4D5CDF5908FCBC97214EFBF45BD25AE810C26E (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___RawBytes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FMZipHelper_FMZipBytes_mFD4D5CDF5908FCBC97214EFBF45BD25AE810C26E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * V_0 = NULL;
	GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * V_1 = NULL;
	BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F * V_2 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_3 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___RawBytes0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL;
	}

IL_0005:
	{
		MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_1 = (MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C *)il2cpp_codegen_object_new(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C_il2cpp_TypeInfo_var);
		MemoryStream__ctor_mD27B3DF2400D46A4A81EE78B0BD2C29EFCFAA44F(L_1, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_2 = V_0;
			GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * L_3 = (GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 *)il2cpp_codegen_object_new(GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292_il2cpp_TypeInfo_var);
			GZipOutputStream__ctor_m6E95590355B8E138591499632751259F020161A6(L_3, L_2, /*hidden argument*/NULL);
			V_1 = L_3;
		}

IL_0012:
		try
		{ // begin try (depth: 2)
			{
				GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * L_4 = V_1;
				BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F * L_5 = (BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F *)il2cpp_codegen_object_new(BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F_il2cpp_TypeInfo_var);
				BinaryWriter__ctor_mC6F89939E04734FBEEA375D7E0FF9C042E4AB71A(L_5, L_4, /*hidden argument*/NULL);
				V_2 = L_5;
			}

IL_0019:
			try
			{ // begin try (depth: 3)
				BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F * L_6 = V_2;
				ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = ___RawBytes0;
				NullCheck(L_6);
				VirtActionInvoker1< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(9 /* System.Void System.IO.BinaryWriter::Write(System.Byte[]) */, L_6, L_7);
				BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F * L_8 = V_2;
				NullCheck(L_8);
				VirtActionInvoker0::Invoke(6 /* System.Void System.IO.BinaryWriter::Flush() */, L_8);
				GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * L_9 = V_1;
				NullCheck(L_9);
				VirtActionInvoker0::Invoke(29 /* System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish() */, L_9);
				Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_10 = V_0;
				NullCheck(L_10);
				int64_t L_11;
				L_11 = VirtFuncInvoker0< int64_t >::Invoke(10 /* System.Int64 System.IO.Stream::get_Length() */, L_10);
				if ((int64_t)(L_11) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), FMZipHelper_FMZipBytes_mFD4D5CDF5908FCBC97214EFBF45BD25AE810C26E_RuntimeMethod_var);
				ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((intptr_t)L_11));
				V_3 = L_12;
				Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_13 = V_0;
				NullCheck(L_13);
				int64_t L_14;
				L_14 = VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(24 /* System.Int64 System.IO.Stream::Seek(System.Int64,System.IO.SeekOrigin) */, L_13, ((int64_t)((int64_t)0)), 0);
				Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_15 = V_0;
				ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_16 = V_3;
				ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_17 = V_3;
				NullCheck(L_17);
				NullCheck(L_15);
				int32_t L_18;
				L_18 = VirtFuncInvoker3< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t >::Invoke(25 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_15, L_16, 0, ((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length))));
				ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_19 = V_3;
				V_4 = L_19;
				IL2CPP_LEAVE(0x72, FINALLY_0054);
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t *)e.ex;
				goto FINALLY_0054;
			}

FINALLY_0054:
			{ // begin finally (depth: 3)
				{
					BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F * L_20 = V_2;
					if (!L_20)
					{
						goto IL_005d;
					}
				}

IL_0057:
				{
					BinaryWriter_t70074014C7FE27CD9F7500C3F02C4AB61D35554F * L_21 = V_2;
					NullCheck(L_21);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_21);
				}

IL_005d:
				{
					IL2CPP_END_FINALLY(84)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(84)
			{
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
				IL2CPP_END_CLEANUP(0x72, FINALLY_005e);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_005e;
		}

FINALLY_005e:
		{ // begin finally (depth: 2)
			{
				GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * L_22 = V_1;
				if (!L_22)
				{
					goto IL_0067;
				}
			}

IL_0061:
			{
				GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * L_23 = V_1;
				NullCheck(L_23);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_23);
			}

IL_0067:
			{
				IL2CPP_END_FINALLY(94)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(94)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0x72, FINALLY_0068);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0068;
	}

FINALLY_0068:
	{ // begin finally (depth: 1)
		{
			Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_24 = V_0;
			if (!L_24)
			{
				goto IL_0071;
			}
		}

IL_006b:
		{
			Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_25 = V_0;
			NullCheck(L_25);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_25);
		}

IL_0071:
		{
			IL2CPP_END_FINALLY(104)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(104)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x72, IL_0072)
	}

IL_0072:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_26 = V_4;
		return L_26;
	}
}
// System.Byte[] FMZipHelper::FMUnzipBytes(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* FMZipHelper_FMUnzipBytes_m2625C076A010F8B2D86DA45E95E45231C11313FA (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___ZipBytes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * V_0 = NULL;
	GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * V_1 = NULL;
	BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * V_2 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___ZipBytes0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL;
	}

IL_0005:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___ZipBytes0;
		MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_2 = (MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C *)il2cpp_codegen_object_new(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m3E041ADD3DB7EA42E7DB56FE862097819C02B9C2(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_3 = V_0;
			GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * L_4 = (GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 *)il2cpp_codegen_object_new(GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9_il2cpp_TypeInfo_var);
			GZipInputStream__ctor_mC075983580E627945130BC69DE408678F68542FA(L_4, L_3, /*hidden argument*/NULL);
			V_1 = L_4;
		}

IL_0013:
		try
		{ // begin try (depth: 2)
			{
				GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * L_5 = V_1;
				BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * L_6 = (BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 *)il2cpp_codegen_object_new(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128_il2cpp_TypeInfo_var);
				BinaryReader__ctor_m8D2F966D44EF5BD30D54D94653A831EFDB9C6A60(L_6, L_5, /*hidden argument*/NULL);
				V_2 = L_6;
			}

IL_001a:
			try
			{ // begin try (depth: 3)
				BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * L_7 = V_2;
				ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8;
				L_8 = FMZipHelper_ReadAllBytes_m6AC058706643D287F7BB6AD9A4E793F0E58D8277(L_7, /*hidden argument*/NULL);
				V_3 = L_8;
				IL2CPP_LEAVE(0x41, FINALLY_0023);
			} // end try (depth: 3)
			catch(Il2CppExceptionWrapper& e)
			{
				__last_unhandled_exception = (Exception_t *)e.ex;
				goto FINALLY_0023;
			}

FINALLY_0023:
			{ // begin finally (depth: 3)
				{
					BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * L_9 = V_2;
					if (!L_9)
					{
						goto IL_002c;
					}
				}

IL_0026:
				{
					BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * L_10 = V_2;
					NullCheck(L_10);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_10);
				}

IL_002c:
				{
					IL2CPP_END_FINALLY(35)
				}
			} // end finally (depth: 3)
			IL2CPP_CLEANUP(35)
			{
				IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
				IL2CPP_END_CLEANUP(0x41, FINALLY_002d);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_002d;
		}

FINALLY_002d:
		{ // begin finally (depth: 2)
			{
				GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * L_11 = V_1;
				if (!L_11)
				{
					goto IL_0036;
				}
			}

IL_0030:
			{
				GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * L_12 = V_1;
				NullCheck(L_12);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_12);
			}

IL_0036:
			{
				IL2CPP_END_FINALLY(45)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(45)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0x41, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_13 = V_0;
			if (!L_13)
			{
				goto IL_0040;
			}
		}

IL_003a:
		{
			Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_14 = V_0;
			NullCheck(L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_14);
		}

IL_0040:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x41, IL_0041)
	}

IL_0041:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_15 = V_3;
		return L_15;
	}
}
// System.Byte[] FMZipHelper::ReadAllBytes(System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* FMZipHelper_ReadAllBytes_m6AC058706643D287F7BB6AD9A4E793F0E58D8277 (BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * ___reader0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * V_0 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_1 = NULL;
	int32_t V_2 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_0 = (MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C *)il2cpp_codegen_object_new(MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C_il2cpp_TypeInfo_var);
		MemoryStream__ctor_mD27B3DF2400D46A4A81EE78B0BD2C29EFCFAA44F(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_0006:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)4096));
			V_1 = L_1;
			goto IL_001c;
		}

IL_0013:
		{
			MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_2 = V_0;
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = V_1;
			int32_t L_4 = V_2;
			NullCheck(L_2);
			VirtActionInvoker3< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t >::Invoke(27 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_2, L_3, 0, L_4);
		}

IL_001c:
		{
			BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * L_5 = ___reader0;
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = V_1;
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = V_1;
			NullCheck(L_7);
			NullCheck(L_5);
			int32_t L_8;
			L_8 = VirtFuncInvoker3< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t >::Invoke(24 /* System.Int32 System.IO.BinaryReader::Read(System.Byte[],System.Int32,System.Int32) */, L_5, L_6, 0, ((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length))));
			int32_t L_9 = L_8;
			V_2 = L_9;
			if (L_9)
			{
				goto IL_0013;
			}
		}

IL_002b:
		{
			MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_10 = V_0;
			NullCheck(L_10);
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11;
			L_11 = VirtFuncInvoker0< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* >::Invoke(32 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_10);
			V_3 = L_11;
			IL2CPP_LEAVE(0x3E, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_12 = V_0;
			if (!L_12)
			{
				goto IL_003d;
			}
		}

IL_0037:
		{
			MemoryStream_t0B450399DD6D0175074FED99DD321D65771C9E1C * L_13 = V_0;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_13);
		}

IL_003d:
		{
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
	}

IL_003e:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_14 = V_3;
		return L_14;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.GZip.GZipException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipException__ctor_m91D8DC139F8C46AD9006D1E34323B5280AA6D684 (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * __this, const RuntimeMethod* method)
{
	{
		SharpZipBaseException__ctor_mEE9B481134AA334BBF8777D11D8001825E16A988(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.GZip.GZipException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.GZip.GZipException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipException__ctor_mBFACA4B8DAEDC713836A45D82EA0FE360B5FD818 (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method)
{
	{
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_0 = ___info0;
		StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  L_1 = ___context1;
		SharpZipBaseException__ctor_m8A2F854EAC4E1806C22166372CE8BD8A7DA0502D(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipInputStream__ctor_mC075983580E627945130BC69DE408678F68542FA (GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseInputStream0, const RuntimeMethod* method)
{
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = ___baseInputStream0;
		GZipInputStream__ctor_m826EE21BB68D245D97F7BC8BD658370543038BD0(__this, L_0, ((int32_t)4096), /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipInputStream__ctor_m826EE21BB68D245D97F7BC8BD658370543038BD0 (GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseInputStream0, int32_t ___size1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = ___baseInputStream0;
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_1 = (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 *)il2cpp_codegen_object_new(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var);
		Inflater__ctor_m203485CA9DDAD7143A3459BD6BA7D479FA7B1C69(L_1, (bool)1, /*hidden argument*/NULL);
		int32_t L_2 = ___size1;
		InflaterInputStream__ctor_m152F154E3EB66C5B8E82F049C1ED5B37E52A19F8(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.GZip.GZipInputStream::Read(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GZipInputStream_Read_mC933C5AF086B6DE9150D47B384F08DFA7FBB56A1 (GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Exception_t * V_2 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 4> __leave_targets;

IL_0000:
	{
		bool L_0 = __this->get_readGZIPHeader_11();
		if (L_0)
		{
			goto IL_0035;
		}
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			bool L_1;
			L_1 = GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F(__this, /*hidden argument*/NULL);
			if (L_1)
			{
				goto IL_0014;
			}
		}

IL_0010:
		{
			V_1 = 0;
			goto IL_0072;
		}

IL_0014:
		{
			goto IL_0035;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0016;
		}
		throw e;
	}

CATCH_0016:
	{ // begin catch(System.Exception)
		{
			V_2 = ((Exception_t *)IL2CPP_GET_ACTIVE_EXCEPTION(Exception_t *));
			bool L_2 = __this->get_completedLastBlock_12();
			if (!L_2)
			{
				goto IL_0033;
			}
		}

IL_001f:
		{
			Exception_t * L_3 = V_2;
			if (((GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 *)IsInstClass((RuntimeObject*)L_3, ((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4_il2cpp_TypeInfo_var)))))
			{
				goto IL_002f;
			}
		}

IL_0027:
		{
			Exception_t * L_4 = V_2;
			if (!((EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)IsInstClass((RuntimeObject*)L_4, ((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)))))
			{
				goto IL_0033;
			}
		}

IL_002f:
		{
			V_1 = 0;
			IL2CPP_POP_ACTIVE_EXCEPTION();
			goto IL_0072;
		}

IL_0033:
		{
			IL2CPP_POP_ACTIVE_EXCEPTION();
			goto IL_0035;
		}
	} // end catch (depth: 1)

IL_0035:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = ___buffer0;
		int32_t L_6 = ___offset1;
		int32_t L_7 = ___count2;
		int32_t L_8;
		L_8 = InflaterInputStream_Read_mC36B703875162B5FAB4DC4F47A05AAD596331F50(__this, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_10 = __this->get_crc_10();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = ___buffer0;
		int32_t L_12 = ___offset1;
		int32_t L_13 = V_0;
		ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  L_14;
		memset((&L_14), 0, sizeof(L_14));
		ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D((&L_14), L_11, L_12, L_13, /*hidden argument*/ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_RuntimeMethod_var);
		NullCheck(L_10);
		Crc32_Update_m6521C75BD473D1869DA1C3F3D00C936F07D3957D(L_10, L_14, /*hidden argument*/NULL);
	}

IL_0056:
	{
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_15 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inf_6();
		NullCheck(L_15);
		bool L_16;
		L_16 = Inflater_get_IsFinished_m90C7D1B230EEEB2150FA15482E9F6A97AF168812(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0069;
		}
	}
	{
		GZipInputStream_ReadFooter_m5B658299CE668EC4346D6EA9571AD17532B9AC45(__this, /*hidden argument*/NULL);
	}

IL_0069:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_18 = ___count2;
		if (L_18)
		{
			goto IL_0000;
		}
	}

IL_0070:
	{
		int32_t L_19 = V_0;
		return L_19;
	}

IL_0072:
	{
		int32_t L_20 = V_1;
		return L_20;
	}
}
// System.Boolean FMETP.SharpZipLib.GZip.GZipInputStream::ReadHeader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F (GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t G_B50_0 = 0;
	int32_t G_B49_0 = 0;
	int32_t G_B52_0 = 0;
	int32_t G_B51_0 = 0;
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_0 = (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 *)il2cpp_codegen_object_new(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		Crc32__ctor_m69D0E66F79D9E4A6937BA772C21E6D3EDA9B6F01(L_0, /*hidden argument*/NULL);
		__this->set_crc_10(L_0);
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_1 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = InflaterInputBuffer_get_Available_m7BDBC3F8048C19BF8C9D3E253BBFDA8601A107DE_inline(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_3 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_3);
		InflaterInputBuffer_Fill_mD6D37972021807E5F4615E4D33276A13D2D64925(L_3, /*hidden argument*/NULL);
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_4 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_4);
		int32_t L_5;
		L_5 = InflaterInputBuffer_get_Available_m7BDBC3F8048C19BF8C9D3E253BBFDA8601A107DE_inline(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_6 = (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 *)il2cpp_codegen_object_new(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		Crc32__ctor_m69D0E66F79D9E4A6937BA772C21E6D3EDA9B6F01(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_7 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_7);
		int32_t L_8;
		L_8 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) >= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_10 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_10, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_0055:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_11 = V_0;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)31))))
		{
			goto IL_006c;
		}
	}
	{
		GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * L_14 = (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4_il2cpp_TypeInfo_var)));
		GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB(L_14, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral2338B88AF89C2719503E11D89F92D58C2B27109F)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_006c:
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_15 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_15);
		int32_t L_16;
		L_16 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		int32_t L_17 = V_1;
		if ((((int32_t)L_17) >= ((int32_t)0)))
		{
			goto IL_0087;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_18 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_18, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_0087:
	{
		int32_t L_19 = V_1;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)139))))
		{
			goto IL_009a;
		}
	}
	{
		GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * L_20 = (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4_il2cpp_TypeInfo_var)));
		GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB(L_20, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC885E9B07C81BFE24C1773C611D9F1399145BDFB)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_009a:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_21 = V_0;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_21, L_22, /*hidden argument*/NULL);
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_23 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_23);
		int32_t L_24;
		L_24 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		int32_t L_25 = V_2;
		if ((((int32_t)L_25) >= ((int32_t)0)))
		{
			goto IL_00bc;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_26 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_26, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_00bc:
	{
		int32_t L_27 = V_2;
		if ((((int32_t)L_27) == ((int32_t)8)))
		{
			goto IL_00cb;
		}
	}
	{
		GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * L_28 = (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4_il2cpp_TypeInfo_var)));
		GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB(L_28, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral656A09E37E7997B12AB93C019EA4CC35E11E437D)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_00cb:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_29 = V_0;
		int32_t L_30 = V_2;
		NullCheck(L_29);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_29, L_30, /*hidden argument*/NULL);
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_31 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_31);
		int32_t L_32;
		L_32 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_31, /*hidden argument*/NULL);
		V_3 = L_32;
		int32_t L_33 = V_3;
		if ((((int32_t)L_33) >= ((int32_t)0)))
		{
			goto IL_00ed;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_34 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_34, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_00ed:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_35 = V_0;
		int32_t L_36 = V_3;
		NullCheck(L_35);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_35, L_36, /*hidden argument*/NULL);
		int32_t L_37 = V_3;
		if (!((int32_t)((int32_t)L_37&(int32_t)((int32_t)224))))
		{
			goto IL_0108;
		}
	}
	{
		GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * L_38 = (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4_il2cpp_TypeInfo_var)));
		GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB(L_38, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral5CA4B7D02E9487E750582AC773BCF704AFBABE6F)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_38, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_0108:
	{
		V_4 = 0;
		goto IL_0138;
	}

IL_010d:
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_39 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_39);
		int32_t L_40;
		L_40 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_39, /*hidden argument*/NULL);
		V_5 = L_40;
		int32_t L_41 = V_5;
		if ((((int32_t)L_41) >= ((int32_t)0)))
		{
			goto IL_012a;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_42 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_42, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_42, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_012a:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_43 = V_0;
		int32_t L_44 = V_5;
		NullCheck(L_43);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
	}

IL_0138:
	{
		int32_t L_46 = V_4;
		if ((((int32_t)L_46) < ((int32_t)6)))
		{
			goto IL_010d;
		}
	}
	{
		int32_t L_47 = V_3;
		if (!((int32_t)((int32_t)L_47&(int32_t)4)))
		{
			goto IL_01c0;
		}
	}
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_48 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_48);
		int32_t L_49;
		L_49 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_48, /*hidden argument*/NULL);
		V_6 = L_49;
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_50 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_50);
		int32_t L_51;
		L_51 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_50, /*hidden argument*/NULL);
		V_7 = L_51;
		int32_t L_52 = V_6;
		if ((((int32_t)L_52) < ((int32_t)0)))
		{
			goto IL_0166;
		}
	}
	{
		int32_t L_53 = V_7;
		if ((((int32_t)L_53) >= ((int32_t)0)))
		{
			goto IL_0171;
		}
	}

IL_0166:
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_54 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_54, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_54, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_0171:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_55 = V_0;
		int32_t L_56 = V_6;
		NullCheck(L_55);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_55, L_56, /*hidden argument*/NULL);
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_57 = V_0;
		int32_t L_58 = V_7;
		NullCheck(L_57);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_57, L_58, /*hidden argument*/NULL);
		int32_t L_59 = V_7;
		int32_t L_60 = V_6;
		V_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_59<<(int32_t)8))|(int32_t)L_60));
		V_9 = 0;
		goto IL_01ba;
	}

IL_018f:
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_61 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_61);
		int32_t L_62;
		L_62 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_61, /*hidden argument*/NULL);
		V_10 = L_62;
		int32_t L_63 = V_10;
		if ((((int32_t)L_63) >= ((int32_t)0)))
		{
			goto IL_01ac;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_64 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_64, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_64, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_01ac:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_65 = V_0;
		int32_t L_66 = V_10;
		NullCheck(L_65);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_65, L_66, /*hidden argument*/NULL);
		int32_t L_67 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_67, (int32_t)1));
	}

IL_01ba:
	{
		int32_t L_68 = V_9;
		int32_t L_69 = V_8;
		if ((((int32_t)L_68) < ((int32_t)L_69)))
		{
			goto IL_018f;
		}
	}

IL_01c0:
	{
		int32_t L_70 = V_3;
		if (!((int32_t)((int32_t)L_70&(int32_t)8)))
		{
			goto IL_01f8;
		}
	}
	{
		goto IL_01cf;
	}

IL_01c7:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_71 = V_0;
		int32_t L_72 = V_11;
		NullCheck(L_71);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_71, L_72, /*hidden argument*/NULL);
	}

IL_01cf:
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_73 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_73);
		int32_t L_74;
		L_74 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_73, /*hidden argument*/NULL);
		int32_t L_75 = L_74;
		V_11 = L_75;
		if ((((int32_t)L_75) > ((int32_t)0)))
		{
			goto IL_01c7;
		}
	}
	{
		int32_t L_76 = V_11;
		if ((((int32_t)L_76) >= ((int32_t)0)))
		{
			goto IL_01f0;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_77 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_77, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_77, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_01f0:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_78 = V_0;
		int32_t L_79 = V_11;
		NullCheck(L_78);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_78, L_79, /*hidden argument*/NULL);
	}

IL_01f8:
	{
		int32_t L_80 = V_3;
		if (!((int32_t)((int32_t)L_80&(int32_t)((int32_t)16))))
		{
			goto IL_0231;
		}
	}
	{
		goto IL_0208;
	}

IL_0200:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_81 = V_0;
		int32_t L_82 = V_12;
		NullCheck(L_81);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_81, L_82, /*hidden argument*/NULL);
	}

IL_0208:
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_83 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_83);
		int32_t L_84;
		L_84 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_83, /*hidden argument*/NULL);
		int32_t L_85 = L_84;
		V_12 = L_85;
		if ((((int32_t)L_85) > ((int32_t)0)))
		{
			goto IL_0200;
		}
	}
	{
		int32_t L_86 = V_12;
		if ((((int32_t)L_86) >= ((int32_t)0)))
		{
			goto IL_0229;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_87 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_87, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_87, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_0229:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_88 = V_0;
		int32_t L_89 = V_12;
		NullCheck(L_88);
		Crc32_Update_m3476EB0AD0427E601D2BCE0216ED8AF9E766190A(L_88, L_89, /*hidden argument*/NULL);
	}

IL_0231:
	{
		int32_t L_90 = V_3;
		if (!((int32_t)((int32_t)L_90&(int32_t)2)))
		{
			goto IL_028c;
		}
	}
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_91 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_91);
		int32_t L_92;
		L_92 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_91, /*hidden argument*/NULL);
		int32_t L_93 = L_92;
		G_B49_0 = L_93;
		if ((((int32_t)L_93) >= ((int32_t)0)))
		{
			G_B50_0 = L_93;
			goto IL_0250;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_94 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_94, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_94, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_0250:
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_95 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		NullCheck(L_95);
		int32_t L_96;
		L_96 = InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894(L_95, /*hidden argument*/NULL);
		V_13 = L_96;
		int32_t L_97 = V_13;
		G_B51_0 = G_B50_0;
		if ((((int32_t)L_97) >= ((int32_t)0)))
		{
			G_B52_0 = G_B50_0;
			goto IL_026d;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_98 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_98, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralC4DC0349FD455ED8B1D92DF3704860E8A1D53B65)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_98, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_026d:
	{
		int32_t L_99 = V_13;
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_100 = V_0;
		NullCheck(L_100);
		int64_t L_101;
		L_101 = Crc32_get_Value_m624503DF9F1044753BE1CB1AD489D7DA5035587B(L_100, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)G_B52_0<<(int32_t)8))|(int32_t)L_99))) == ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_101))&(int32_t)((int32_t)65535))))))
		{
			goto IL_028c;
		}
	}
	{
		GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * L_102 = (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4_il2cpp_TypeInfo_var)));
		GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB(L_102, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralF286F33D4D4346AB086D0F76D7130EC74F9C4F29)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_102, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadHeader_mF3847F2D2357E0F5B8EEF583DDD1461058A87D9F_RuntimeMethod_var)));
	}

IL_028c:
	{
		__this->set_readGZIPHeader_11((bool)1);
		return (bool)1;
	}
}
// System.Void FMETP.SharpZipLib.GZip.GZipInputStream::ReadFooter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipInputStream_ReadFooter_m5B658299CE668EC4346D6EA9571AD17532B9AC45 (GZipInputStream_t5E06331B7A5B774838AEFDA08723DC4C24DB36E9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	int64_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	uint32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)8);
		V_0 = L_0;
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_1 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inf_6();
		NullCheck(L_1);
		int64_t L_2;
		L_2 = Inflater_get_TotalOut_m24899EBBA3839BC2A41C59359686D6CB758576F0_inline(L_1, /*hidden argument*/NULL);
		V_1 = ((int64_t)((int64_t)L_2&(int64_t)((int64_t)((uint64_t)((uint32_t)((uint32_t)(-1)))))));
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_3 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_4 = L_3;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = InflaterInputBuffer_get_Available_m7BDBC3F8048C19BF8C9D3E253BBFDA8601A107DE_inline(L_4, /*hidden argument*/NULL);
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_6 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inf_6();
		NullCheck(L_6);
		int32_t L_7;
		L_7 = Inflater_get_RemainingInput_m1D9B9068C5E2A64081E9CA6F3AA11FDE926B6102(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		InflaterInputBuffer_set_Available_mB9B0F342C4B3FB90CCFC0F34B4C5CEDDB7104981_inline(L_4, ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), /*hidden argument*/NULL);
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_8 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inf_6();
		NullCheck(L_8);
		Inflater_Reset_mCBF0E4780E8532B200538A1D7601C6EEEE9DA97C(L_8, /*hidden argument*/NULL);
		V_2 = 8;
		goto IL_0069;
	}

IL_0042:
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_9 = ((InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E *)__this)->get_inputBuffer_7();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = V_0;
		int32_t L_11 = V_2;
		int32_t L_12 = V_2;
		NullCheck(L_9);
		int32_t L_13;
		L_13 = InflaterInputBuffer_ReadClearTextBuffer_m91A4E800CE258802DA172CC80C03A08E919411A5(L_9, L_10, ((int32_t)il2cpp_codegen_subtract((int32_t)8, (int32_t)L_11)), L_12, /*hidden argument*/NULL);
		V_5 = L_13;
		int32_t L_14 = V_5;
		if ((((int32_t)L_14) > ((int32_t)0)))
		{
			goto IL_0064;
		}
	}
	{
		EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 * L_15 = (EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&EndOfStreamException_tDA8337E29A941EFB3E26721033B1826C1ACB0059_il2cpp_TypeInfo_var)));
		EndOfStreamException__ctor_m62AD97E22305B690B74C4EA6E3EAC36D10CE3800(L_15, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralD65228AEF331C4DDC9C591DAE27D7197C62B893F)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadFooter_m5B658299CE668EC4346D6EA9571AD17532B9AC45_RuntimeMethod_var)));
	}

IL_0064:
	{
		int32_t L_16 = V_2;
		int32_t L_17 = V_5;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)L_17));
	}

IL_0069:
	{
		int32_t L_18 = V_2;
		if ((((int32_t)L_18) > ((int32_t)0)))
		{
			goto IL_0042;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = 0;
		uint8_t L_21 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_22 = V_0;
		NullCheck(L_22);
		int32_t L_23 = 1;
		uint8_t L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_25 = V_0;
		NullCheck(L_25);
		int32_t L_26 = 2;
		uint8_t L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_28 = V_0;
		NullCheck(L_28);
		int32_t L_29 = 3;
		uint8_t L_30 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_21&(int32_t)((int32_t)255)))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_24&(int32_t)((int32_t)255)))<<(int32_t)8))))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27&(int32_t)((int32_t)255)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_30<<(int32_t)((int32_t)24)))));
		int32_t L_31 = V_3;
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_32 = __this->get_crc_10();
		NullCheck(L_32);
		int64_t L_33;
		L_33 = Crc32_get_Value_m624503DF9F1044753BE1CB1AD489D7DA5035587B(L_32, /*hidden argument*/NULL);
		if ((((int32_t)L_31) == ((int32_t)((int32_t)((int32_t)L_33)))))
		{
			goto IL_00d7;
		}
	}
	{
		String_t* L_34;
		L_34 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_3), /*hidden argument*/NULL);
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_35 = __this->get_crc_10();
		NullCheck(L_35);
		int64_t L_36;
		L_36 = Crc32_get_Value_m624503DF9F1044753BE1CB1AD489D7DA5035587B(L_35, /*hidden argument*/NULL);
		V_6 = ((int32_t)((int32_t)L_36));
		String_t* L_37;
		L_37 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_6), /*hidden argument*/NULL);
		String_t* L_38;
		L_38 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral71C99B122416C97FB9B0D52972951F4694129599)), L_34, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral9807D224CD9C86357FB82B5C8337A0C24ED00CB5)), L_37, /*hidden argument*/NULL);
		GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * L_39 = (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4_il2cpp_TypeInfo_var)));
		GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB(L_39, L_38, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_39, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadFooter_m5B658299CE668EC4346D6EA9571AD17532B9AC45_RuntimeMethod_var)));
	}

IL_00d7:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_40 = V_0;
		NullCheck(L_40);
		int32_t L_41 = 4;
		uint8_t L_42 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_43 = V_0;
		NullCheck(L_43);
		int32_t L_44 = 5;
		uint8_t L_45 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_46 = V_0;
		NullCheck(L_46);
		int32_t L_47 = 6;
		uint8_t L_48 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_49 = V_0;
		NullCheck(L_49);
		int32_t L_50 = 7;
		uint8_t L_51 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_42&(int32_t)((int32_t)255)))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_45&(int32_t)((int32_t)255)))<<(int32_t)8))))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_48&(int32_t)((int32_t)255)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_51<<(int32_t)((int32_t)24)))));
		int64_t L_52 = V_1;
		uint32_t L_53 = V_4;
		if ((((int64_t)L_52) == ((int64_t)((int64_t)((uint64_t)L_53)))))
		{
			goto IL_0113;
		}
	}
	{
		GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 * L_54 = (GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipException_t617B92F955D5B8B9B6AEF7EA7C73AD02787BD9A4_il2cpp_TypeInfo_var)));
		GZipException__ctor_mE8276333A2B33DE5EA53CB9E5B175AB8D613DFEB(L_54, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral7C8891184659AF0C1E4D415CD6AEC9C7D572EB95)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_54, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipInputStream_ReadFooter_m5B658299CE668EC4346D6EA9571AD17532B9AC45_RuntimeMethod_var)));
	}

IL_0113:
	{
		__this->set_readGZIPHeader_11((bool)0);
		__this->set_completedLastBlock_12((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipOutputStream__ctor_m6E95590355B8E138591499632751259F020161A6 (GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseOutputStream0, const RuntimeMethod* method)
{
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = ___baseOutputStream0;
		GZipOutputStream__ctor_mCD36645CB0B363E3E72E92182F18C6ED80873E28(__this, L_0, ((int32_t)4096), /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipOutputStream__ctor_mCD36645CB0B363E3E72E92182F18C6ED80873E28 (GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseOutputStream0, int32_t ___size1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Deflater_tD13935AC79599366FDFE44A979912695AFA19A34_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_0 = (Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 *)il2cpp_codegen_object_new(Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3_il2cpp_TypeInfo_var);
		Crc32__ctor_m69D0E66F79D9E4A6937BA772C21E6D3EDA9B6F01(L_0, /*hidden argument*/NULL);
		__this->set_crc_12(L_0);
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_1 = ___baseOutputStream0;
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_2 = (Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 *)il2cpp_codegen_object_new(Deflater_tD13935AC79599366FDFE44A979912695AFA19A34_il2cpp_TypeInfo_var);
		Deflater__ctor_m698267A3FB8383CEDAA02AA133076DF05411CDA8(L_2, (-1), (bool)1, /*hidden argument*/NULL);
		int32_t L_3 = ___size1;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E_il2cpp_TypeInfo_var);
		DeflaterOutputStream__ctor_mB41165AE84240A90382E4B53F11B299A4CDD58A1(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::Write(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipOutputStream_Write_mEDC538022944DBACC0349CAADF293F28D5714CC1 (GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_state__13();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		GZipOutputStream_WriteHeader_mADDFD2A440ACC6DE89CD4CA229C496D790BE0C55(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		int32_t L_1 = __this->get_state__13();
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0022;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_2 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralDC7B5EAEC47EFC435ADE020F2A569BDBA22F89CC)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GZipOutputStream_Write_mEDC538022944DBACC0349CAADF293F28D5714CC1_RuntimeMethod_var)));
	}

IL_0022:
	{
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_3 = __this->get_crc_12();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = ___buffer0;
		int32_t L_5 = ___offset1;
		int32_t L_6 = ___count2;
		ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  L_7;
		memset((&L_7), 0, sizeof(L_7));
		ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D((&L_7), L_4, L_5, L_6, /*hidden argument*/ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_RuntimeMethod_var);
		NullCheck(L_3);
		Crc32_Update_m6521C75BD473D1869DA1C3F3D00C936F07D3957D(L_3, L_7, /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8 = ___buffer0;
		int32_t L_9 = ___offset1;
		int32_t L_10 = ___count2;
		DeflaterOutputStream_Write_m3B8E53BB694B9FEFBDAA9FF03BE50F4A41BDF12A(__this, L_8, L_9, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipOutputStream_Dispose_m765DA62FBF2FC4CCC406731A3EAC26AD1061406D (GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker0::Invoke(29 /* System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish() */, __this);
		IL2CPP_LEAVE(0x2C, FINALLY_0008);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0008;
	}

FINALLY_0008:
	{ // begin finally (depth: 1)
		{
			int32_t L_0 = __this->get_state__13();
			if ((((int32_t)L_0) == ((int32_t)3)))
			{
				goto IL_002b;
			}
		}

IL_0011:
		{
			__this->set_state__13(3);
			bool L_1;
			L_1 = DeflaterOutputStream_get_IsStreamOwner_m3023CE9D69403AC66C712F5CC82EB88D8B395E61_inline(__this, /*hidden argument*/NULL);
			if (!L_1)
			{
				goto IL_002b;
			}
		}

IL_0020:
		{
			Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_2 = ((DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E *)__this)->get_baseOutputStream__9();
			NullCheck(L_2);
			Stream_Dispose_m117324084DDAD414761AD29FB17A419840BA6EA0(L_2, /*hidden argument*/NULL);
		}

IL_002b:
		{
			IL2CPP_END_FINALLY(8)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(8)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
	}

IL_002c:
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::Finish()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipOutputStream_Finish_mF46A4E0A1E918AE80042EA7CCA80044F0D65CA25 (GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	{
		int32_t L_0 = __this->get_state__13();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		GZipOutputStream_WriteHeader_mADDFD2A440ACC6DE89CD4CA229C496D790BE0C55(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		int32_t L_1 = __this->get_state__13();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0093;
		}
	}
	{
		__this->set_state__13(2);
		DeflaterOutputStream_Finish_m0B586B7D62DFF8763BA25CB632AA7DD4F2EC2499(__this, /*hidden argument*/NULL);
		Deflater_tD13935AC79599366FDFE44A979912695AFA19A34 * L_2 = ((DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E *)__this)->get_deflater__8();
		NullCheck(L_2);
		int64_t L_3;
		L_3 = Deflater_get_TotalIn_m361FC4798E6EDA1D0A1E980CFDD628B211815A62(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((uint32_t)((int64_t)((int64_t)L_3&(int64_t)((int64_t)((uint64_t)((uint32_t)((uint32_t)(-1)))))))));
		Crc32_t71C09578F88D96B91B91FCC0D60ECF9E21266FA3 * L_4 = __this->get_crc_12();
		NullCheck(L_4);
		int64_t L_5;
		L_5 = Crc32_get_Value_m624503DF9F1044753BE1CB1AD489D7DA5035587B(L_4, /*hidden argument*/NULL);
		V_1 = ((int32_t)((uint32_t)((int64_t)((int64_t)L_5&(int64_t)((int64_t)((uint64_t)((uint32_t)((uint32_t)(-1)))))))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)8);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = L_6;
		uint32_t L_8 = V_1;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)((int32_t)((uint8_t)L_8)));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9 = L_7;
		uint32_t L_10 = V_1;
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)((int32_t)((uint8_t)((int32_t)((uint32_t)L_10>>8)))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = L_9;
		uint32_t L_12 = V_1;
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)((int32_t)((uint8_t)((int32_t)((uint32_t)L_12>>((int32_t)16))))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_13 = L_11;
		uint32_t L_14 = V_1;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)((int32_t)((uint8_t)((int32_t)((uint32_t)L_14>>((int32_t)24))))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_15 = L_13;
		uint32_t L_16 = V_0;
		NullCheck(L_15);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)((int32_t)((uint8_t)L_16)));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_17 = L_15;
		uint32_t L_18 = V_0;
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)((int32_t)((uint8_t)((int32_t)((uint32_t)L_18>>8)))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_19 = L_17;
		uint32_t L_20 = V_0;
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)((int32_t)((uint8_t)((int32_t)((uint32_t)L_20>>((int32_t)16))))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_21 = L_19;
		uint32_t L_22 = V_0;
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)((int32_t)((uint8_t)((int32_t)((uint32_t)L_22>>((int32_t)24))))));
		V_2 = L_21;
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_23 = ((DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E *)__this)->get_baseOutputStream__9();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_24 = V_2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_25 = V_2;
		NullCheck(L_25);
		NullCheck(L_23);
		VirtActionInvoker3< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t >::Invoke(27 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_23, L_24, 0, ((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length))));
	}

IL_0093:
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::WriteHeader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GZipOutputStream_WriteHeader_mADDFD2A440ACC6DE89CD4CA229C496D790BE0C55 (GZipOutputStream_tE5D8D66C813E13FB3898086F640E8DE3C266A292 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____E727EF4792A349C485D893E60874475A54F24B97_11_FieldInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_1 = NULL;
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_state__13();
		if (L_0)
		{
			goto IL_0079;
		}
	}
	{
		__this->set_state__13(1);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_il2cpp_TypeInfo_var);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_1;
		L_1 = DateTime_get_Now_mCAC695993D6E2C57B900C83BEF3F8B18BC4EBC2C(/*hidden argument*/NULL);
		V_2 = L_1;
		int64_t L_2;
		L_2 = DateTime_get_Ticks_m175EDB41A50DB06872CC48CAB603FEBD1DFF46A9((DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 *)(&V_2), /*hidden argument*/NULL);
		DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  L_3;
		memset((&L_3), 0, sizeof(L_3));
		DateTime__ctor_m1AD9E79A671864DFB1AABDB75D207C688B868D88((&L_3), ((int32_t)1970), 1, 1, /*hidden argument*/NULL);
		V_2 = L_3;
		int64_t L_4;
		L_4 = DateTime_get_Ticks_m175EDB41A50DB06872CC48CAB603FEBD1DFF46A9((DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 *)(&V_2), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int64_t)((int64_t)((int64_t)il2cpp_codegen_subtract((int64_t)L_2, (int64_t)L_4))/(int64_t)((int64_t)((int64_t)((int32_t)10000000)))))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = L_5;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_7 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____E727EF4792A349C485D893E60874475A54F24B97_11_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_6, L_7, /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8 = L_6;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)((int32_t)((uint8_t)L_9)));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = L_8;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)((int32_t)((uint8_t)((int32_t)((int32_t)L_11>>(int32_t)8)))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12 = L_10;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)((int32_t)((uint8_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)16))))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_14 = L_12;
		int32_t L_15 = V_0;
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)((int32_t)((uint8_t)((int32_t)((int32_t)L_15>>(int32_t)((int32_t)24))))));
		V_1 = L_14;
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_16 = ((DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E *)__this)->get_baseOutputStream__9();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_17 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18 = V_1;
		NullCheck(L_18);
		NullCheck(L_16);
		VirtActionInvoker3< ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t >::Invoke(27 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_16, L_17, 0, ((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))));
	}

IL_0079:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Inflater__ctor_m203485CA9DDAD7143A3459BD6BA7D479FA7B1C69 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, bool ___noHeader0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * G_B4_0 = NULL;
	Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * G_B5_1 = NULL;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		bool L_0 = ___noHeader0;
		__this->set_noHeader_13(L_0);
		bool L_1 = ___noHeader0;
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_2 = (Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB *)il2cpp_codegen_object_new(Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB_il2cpp_TypeInfo_var);
		Adler32__ctor_m83B2F2CA75BAC4A379FC88436032D5E4CD15EADF(L_2, /*hidden argument*/NULL);
		__this->set_adler_19(L_2);
	}

IL_001b:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_3 = (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 *)il2cpp_codegen_object_new(StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0_il2cpp_TypeInfo_var);
		StreamManipulator__ctor_mC16675A2CEAB3EE9F281DC7F5B59D4DF5DC77069(L_3, /*hidden argument*/NULL);
		__this->set_input_14(L_3);
		OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * L_4 = (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 *)il2cpp_codegen_object_new(OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718_il2cpp_TypeInfo_var);
		OutputWindow__ctor_mA9477B707FFBACD81303FAF7FE2F7C658990FF30(L_4, /*hidden argument*/NULL);
		__this->set_outputWindow_15(L_4);
		bool L_5 = ___noHeader0;
		G_B3_0 = __this;
		if (L_5)
		{
			G_B4_0 = __this;
			goto IL_0038;
		}
	}
	{
		G_B5_0 = 0;
		G_B5_1 = G_B3_0;
		goto IL_0039;
	}

IL_0038:
	{
		G_B5_0 = 2;
		G_B5_1 = G_B4_0;
	}

IL_0039:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_mode_4(G_B5_0);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Inflater_Reset_mCBF0E4780E8532B200538A1D7601C6EEEE9DA97C (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * G_B2_0 = NULL;
	Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * G_B3_1 = NULL;
	{
		bool L_0 = __this->get_noHeader_13();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = 2;
		G_B3_1 = G_B2_0;
	}

IL_000d:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_mode_4(G_B3_0);
		__this->set_totalIn_12(((int64_t)((int64_t)0)));
		__this->set_totalOut_11(((int64_t)((int64_t)0)));
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_1 = __this->get_input_14();
		NullCheck(L_1);
		StreamManipulator_Reset_m7379C771C7C78B44A155BAD54952808BD4F0B5EA(L_1, /*hidden argument*/NULL);
		OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * L_2 = __this->get_outputWindow_15();
		NullCheck(L_2);
		OutputWindow_Reset_mB97240B42A56D72E549B1967E7646C715C8899E4(L_2, /*hidden argument*/NULL);
		__this->set_dynHeader_16((InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA *)NULL);
		__this->set_litlenTree_17((InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE *)NULL);
		__this->set_distTree_18((InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE *)NULL);
		__this->set_isLastBlock_10((bool)0);
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_3 = __this->get_adler_19();
		if (!L_3)
		{
			goto IL_0067;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_4 = __this->get_adler_19();
		NullCheck(L_4);
		Adler32_Reset_mA0BDC81A3B31E939D321A24040D47BBCEACECC22(L_4, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeHeader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_DecodeHeader_mB7913D25A9125D416A703F502EFAB4707D10C92C (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_0, ((int32_t)16), /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_3 = __this->get_input_14();
		NullCheck(L_3);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_3, ((int32_t)16), /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)8))|(int32_t)((int32_t)((int32_t)L_5>>(int32_t)8))))&(int32_t)((int32_t)65535)));
		int32_t L_6 = V_0;
		if (!((int32_t)((int32_t)L_6%(int32_t)((int32_t)31))))
		{
			goto IL_0040;
		}
	}
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_7 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_7, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralD2C4AFE33DBE351005555CACEE117BC851996B97)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_DecodeHeader_mB7913D25A9125D416A703F502EFAB4707D10C92C_RuntimeMethod_var)));
	}

IL_0040:
	{
		int32_t L_8 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)3840)))) == ((int32_t)((int32_t)2048))))
		{
			goto IL_0059;
		}
	}
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_9 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_9, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral1956A49F1096CFA1A4F0007238FFD9DAF226E029)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_DecodeHeader_mB7913D25A9125D416A703F502EFAB4707D10C92C_RuntimeMethod_var)));
	}

IL_0059:
	{
		int32_t L_10 = V_0;
		if (((int32_t)((int32_t)L_10&(int32_t)((int32_t)32))))
		{
			goto IL_0068;
		}
	}
	{
		__this->set_mode_4(2);
		goto IL_0077;
	}

IL_0068:
	{
		__this->set_mode_4(1);
		__this->set_neededBits_6(((int32_t)32));
	}

IL_0077:
	{
		return (bool)1;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeDict()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_DecodeDict_m653948520B90CBEFF564A8C76DF83D897026373E (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		goto IL_003f;
	}

IL_0002:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_0, 8, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		return (bool)0;
	}

IL_0015:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_3 = __this->get_input_14();
		NullCheck(L_3);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_3, 8, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_readAdler_5();
		int32_t L_5 = V_0;
		__this->set_readAdler_5(((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)8))|(int32_t)L_5)));
		int32_t L_6 = __this->get_neededBits_6();
		__this->set_neededBits_6(((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)8)));
	}

IL_003f:
	{
		int32_t L_7 = __this->get_neededBits_6();
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_0002;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeHuffman()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_DecodeHuffman_mAB4BC68E10CE0BC0F737716E1B52EA85096BC740 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * L_0 = __this->get_outputWindow_15();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = OutputWindow_GetFreeSpace_m8FDF473C07DD2A7E667C0D4F9A52657DDD06B99B(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_01d3;
	}

IL_0011:
	{
		int32_t L_2 = __this->get_mode_4();
		V_2 = L_2;
		int32_t L_3 = V_2;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)7)))
		{
			case 0:
			{
				goto IL_004f;
			}
			case 1:
			{
				goto IL_00c4;
			}
			case 2:
			{
				goto IL_0113;
			}
			case 3:
			{
				goto IL_0154;
			}
		}
	}
	{
		goto IL_01c8;
	}

IL_0035:
	{
		OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * L_4 = __this->get_outputWindow_15();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		OutputWindow_Write_m8E87F3E22FAA5D8B122FB61BD4DF14D07F2E42C6(L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1));
		V_0 = L_7;
		if ((((int32_t)L_7) >= ((int32_t)((int32_t)258))))
		{
			goto IL_004f;
		}
	}
	{
		return (bool)1;
	}

IL_004f:
	{
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_8 = __this->get_litlenTree_17();
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_9 = __this->get_input_14();
		NullCheck(L_8);
		int32_t L_10;
		L_10 = InflaterHuffmanTree_GetSymbol_m0F2DAE53C30D2A81AC3ED21E476F2D3451728442(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		V_1 = L_11;
		if (!((int32_t)((int32_t)L_11&(int32_t)((int32_t)-256))))
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) >= ((int32_t)((int32_t)257))))
		{
			goto IL_008f;
		}
	}
	{
		int32_t L_13 = V_1;
		if ((((int32_t)L_13) >= ((int32_t)0)))
		{
			goto IL_0078;
		}
	}
	{
		return (bool)0;
	}

IL_0078:
	{
		__this->set_distTree_18((InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE *)NULL);
		__this->set_litlenTree_17((InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE *)NULL);
		__this->set_mode_4(2);
		return (bool)1;
	}

IL_008f:
	{
	}

IL_0090:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_14 = ((Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields*)il2cpp_codegen_static_fields_for(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var))->get_CPLENS_0();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		int32_t L_16 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)((int32_t)257)));
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		__this->set_repLength_7(L_17);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_18 = ((Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields*)il2cpp_codegen_static_fields_for(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var))->get_CPLEXT_1();
		int32_t L_19 = V_1;
		NullCheck(L_18);
		int32_t L_20 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)((int32_t)257)));
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		__this->set_neededBits_6(L_21);
		goto IL_00c4;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_00b8;
		}
		throw e;
	}

CATCH_00b8:
	{ // begin catch(System.Exception)
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_22 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_22, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral60165C068AEA0DE0A862E2324EB9140E987AEF22)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_DecodeHuffman_mAB4BC68E10CE0BC0F737716E1B52EA85096BC740_RuntimeMethod_var)));
	} // end catch (depth: 1)

IL_00c4:
	{
		int32_t L_23 = __this->get_neededBits_6();
		if ((((int32_t)L_23) <= ((int32_t)0)))
		{
			goto IL_010b;
		}
	}
	{
		__this->set_mode_4(8);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_24 = __this->get_input_14();
		int32_t L_25 = __this->get_neededBits_6();
		NullCheck(L_24);
		int32_t L_26;
		L_26 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_24, L_25, /*hidden argument*/NULL);
		V_3 = L_26;
		int32_t L_27 = V_3;
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_00ec;
		}
	}
	{
		return (bool)0;
	}

IL_00ec:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_28 = __this->get_input_14();
		int32_t L_29 = __this->get_neededBits_6();
		NullCheck(L_28);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_28, L_29, /*hidden argument*/NULL);
		int32_t L_30 = __this->get_repLength_7();
		int32_t L_31 = V_3;
		__this->set_repLength_7(((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)L_31)));
	}

IL_010b:
	{
		__this->set_mode_4(((int32_t)9));
	}

IL_0113:
	{
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_32 = __this->get_distTree_18();
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_33 = __this->get_input_14();
		NullCheck(L_32);
		int32_t L_34;
		L_34 = InflaterHuffmanTree_GetSymbol_m0F2DAE53C30D2A81AC3ED21E476F2D3451728442(L_32, L_33, /*hidden argument*/NULL);
		V_1 = L_34;
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) >= ((int32_t)0)))
		{
			goto IL_012b;
		}
	}
	{
		return (bool)0;
	}

IL_012b:
	{
	}

IL_012c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_36 = ((Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields*)il2cpp_codegen_static_fields_for(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var))->get_CPDIST_2();
		int32_t L_37 = V_1;
		NullCheck(L_36);
		int32_t L_38 = L_37;
		int32_t L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		__this->set_repDist_8(L_39);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_40 = ((Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields*)il2cpp_codegen_static_fields_for(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var))->get_CPDEXT_3();
		int32_t L_41 = V_1;
		NullCheck(L_40);
		int32_t L_42 = L_41;
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		__this->set_neededBits_6(L_43);
		goto IL_0154;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0148;
		}
		throw e;
	}

CATCH_0148:
	{ // begin catch(System.Exception)
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_44 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_44, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral2938DFAC96402A5DC952E0ADEA558EB6FFAC2C8C)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_DecodeHuffman_mAB4BC68E10CE0BC0F737716E1B52EA85096BC740_RuntimeMethod_var)));
	} // end catch (depth: 1)

IL_0154:
	{
		int32_t L_45 = __this->get_neededBits_6();
		if ((((int32_t)L_45) <= ((int32_t)0)))
		{
			goto IL_019f;
		}
	}
	{
		__this->set_mode_4(((int32_t)10));
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_46 = __this->get_input_14();
		int32_t L_47 = __this->get_neededBits_6();
		NullCheck(L_46);
		int32_t L_48;
		L_48 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_46, L_47, /*hidden argument*/NULL);
		V_4 = L_48;
		int32_t L_49 = V_4;
		if ((((int32_t)L_49) >= ((int32_t)0)))
		{
			goto IL_017f;
		}
	}
	{
		return (bool)0;
	}

IL_017f:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_50 = __this->get_input_14();
		int32_t L_51 = __this->get_neededBits_6();
		NullCheck(L_50);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_50, L_51, /*hidden argument*/NULL);
		int32_t L_52 = __this->get_repDist_8();
		int32_t L_53 = V_4;
		__this->set_repDist_8(((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)L_53)));
	}

IL_019f:
	{
		OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * L_54 = __this->get_outputWindow_15();
		int32_t L_55 = __this->get_repLength_7();
		int32_t L_56 = __this->get_repDist_8();
		NullCheck(L_54);
		OutputWindow_Repeat_mD7467DB942F68DACD17A6A466A2F7974AD2969A5(L_54, L_55, L_56, /*hidden argument*/NULL);
		int32_t L_57 = V_0;
		int32_t L_58 = __this->get_repLength_7();
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_57, (int32_t)L_58));
		__this->set_mode_4(7);
		goto IL_01d3;
	}

IL_01c8:
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_59 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_59, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralBDA3F81E88BE1700D497DE0A5571831377B1C88F)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_59, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_DecodeHuffman_mAB4BC68E10CE0BC0F737716E1B52EA85096BC740_RuntimeMethod_var)));
	}

IL_01d3:
	{
		int32_t L_60 = V_0;
		if ((((int32_t)L_60) >= ((int32_t)((int32_t)258))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeChksum()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_DecodeChksum_m82FFF42FBBBDFD86E8EBE560EEB21E7EA9B49DCC (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		goto IL_003f;
	}

IL_0002:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_0, 8, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		return (bool)0;
	}

IL_0015:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_3 = __this->get_input_14();
		NullCheck(L_3);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_3, 8, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_readAdler_5();
		int32_t L_5 = V_0;
		__this->set_readAdler_5(((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)8))|(int32_t)L_5)));
		int32_t L_6 = __this->get_neededBits_6();
		__this->set_neededBits_6(((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)8)));
	}

IL_003f:
	{
		int32_t L_7 = __this->get_neededBits_6();
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_0002;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_8 = __this->get_adler_19();
		if (!L_8)
		{
			goto IL_0098;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_9 = __this->get_adler_19();
		NullCheck(L_9);
		int64_t L_10;
		L_10 = Adler32_get_Value_mB072B09485C331979F68992167A098E33F9C26EB(L_9, /*hidden argument*/NULL);
		int32_t L_11 = __this->get_readAdler_5();
		if ((((int32_t)((int32_t)((int32_t)L_10))) == ((int32_t)L_11)))
		{
			goto IL_0098;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_12 = __this->get_adler_19();
		NullCheck(L_12);
		int64_t L_13;
		L_13 = Adler32_get_Value_mB072B09485C331979F68992167A098E33F9C26EB(L_12, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_13));
		String_t* L_14;
		L_14 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		int32_t* L_15 = __this->get_address_of_readAdler_5();
		String_t* L_16;
		L_16 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_15, /*hidden argument*/NULL);
		String_t* L_17;
		L_17 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral1DBDFB2A21ED7BDE42E71C3049B7639D2C2DEC93)), L_14, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral64FB23415B98CBBAAE0C2B5B5CAD4E64AC7C92C3)), L_16, /*hidden argument*/NULL);
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_18 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_18, L_17, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_DecodeChksum_m82FFF42FBBBDFD86E8EBE560EEB21E7EA9B49DCC_RuntimeMethod_var)));
	}

IL_0098:
	{
		__this->set_mode_4(((int32_t)12));
		return (bool)0;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::Decode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_Decode_m58AF8D0040DD4AA68E0E477F77674C329E2D01CC (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		int32_t L_0 = __this->get_mode_4();
		V_1 = L_0;
		int32_t L_1 = V_1;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0046;
			}
			case 1:
			{
				goto IL_004d;
			}
			case 2:
			{
				goto IL_005b;
			}
			case 3:
			{
				goto IL_0142;
			}
			case 4:
			{
				goto IL_0171;
			}
			case 5:
			{
				goto IL_01b3;
			}
			case 6:
			{
				goto IL_01fb;
			}
			case 7:
			{
				goto IL_0233;
			}
			case 8:
			{
				goto IL_0233;
			}
			case 9:
			{
				goto IL_0233;
			}
			case 10:
			{
				goto IL_0233;
			}
			case 11:
			{
				goto IL_0054;
			}
			case 12:
			{
				goto IL_023a;
			}
		}
	}
	{
		goto IL_023c;
	}

IL_0046:
	{
		bool L_2;
		L_2 = Inflater_DecodeHeader_mB7913D25A9125D416A703F502EFAB4707D10C92C(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_004d:
	{
		bool L_3;
		L_3 = Inflater_DecodeDict_m653948520B90CBEFF564A8C76DF83D897026373E(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0054:
	{
		bool L_4;
		L_4 = Inflater_DecodeChksum_m82FFF42FBBBDFD86E8EBE560EEB21E7EA9B49DCC(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_005b:
	{
		bool L_5 = __this->get_isLastBlock_10();
		if (!L_5)
		{
			goto IL_0092;
		}
	}
	{
		bool L_6 = __this->get_noHeader_13();
		if (!L_6)
		{
			goto IL_0075;
		}
	}
	{
		__this->set_mode_4(((int32_t)12));
		return (bool)0;
	}

IL_0075:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_7 = __this->get_input_14();
		NullCheck(L_7);
		StreamManipulator_SkipToByteBoundary_m7C36C130332D5995F8938FFC6F8A45478B77A47C(L_7, /*hidden argument*/NULL);
		__this->set_neededBits_6(((int32_t)32));
		__this->set_mode_4(((int32_t)11));
		return (bool)1;
	}

IL_0092:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_8 = __this->get_input_14();
		NullCheck(L_8);
		int32_t L_9;
		L_9 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_8, 3, /*hidden argument*/NULL);
		V_0 = L_9;
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_00a5;
		}
	}
	{
		return (bool)0;
	}

IL_00a5:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_11 = __this->get_input_14();
		NullCheck(L_11);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_11, 3, /*hidden argument*/NULL);
		bool L_12 = __this->get_isLastBlock_10();
		int32_t L_13 = V_0;
		__this->set_isLastBlock_10((bool)((int32_t)((int32_t)L_12|(int32_t)((!(((uint32_t)((int32_t)((int32_t)L_13&(int32_t)1))) <= ((uint32_t)0)))? 1 : 0))));
		int32_t L_14 = V_0;
		V_2 = ((int32_t)((int32_t)L_14>>(int32_t)1));
		int32_t L_15 = V_2;
		switch (L_15)
		{
			case 0:
			{
				goto IL_00dc;
			}
			case 1:
			{
				goto IL_00f0;
			}
			case 2:
			{
				goto IL_010f;
			}
		}
	}
	{
		goto IL_0129;
	}

IL_00dc:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_16 = __this->get_input_14();
		NullCheck(L_16);
		StreamManipulator_SkipToByteBoundary_m7C36C130332D5995F8938FFC6F8A45478B77A47C(L_16, /*hidden argument*/NULL);
		__this->set_mode_4(3);
		goto IL_0140;
	}

IL_00f0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var);
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_17 = ((InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_StaticFields*)il2cpp_codegen_static_fields_for(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var))->get_defLitLenTree_1();
		__this->set_litlenTree_17(L_17);
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_18 = ((InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_StaticFields*)il2cpp_codegen_static_fields_for(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var))->get_defDistTree_2();
		__this->set_distTree_18(L_18);
		__this->set_mode_4(7);
		goto IL_0140;
	}

IL_010f:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_19 = __this->get_input_14();
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_20 = (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA *)il2cpp_codegen_object_new(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_il2cpp_TypeInfo_var);
		InflaterDynHeader__ctor_mE2911B9C7C908A0691E08CBCDCFB9573644BCBD9(L_20, L_19, /*hidden argument*/NULL);
		__this->set_dynHeader_16(L_20);
		__this->set_mode_4(6);
		goto IL_0140;
	}

IL_0129:
	{
		String_t* L_21;
		L_21 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_0), /*hidden argument*/NULL);
		String_t* L_22;
		L_22 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral7F602873ED0E988A95A87335349E7292B60E4F24)), L_21, /*hidden argument*/NULL);
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_23 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_23, L_22, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_Decode_m58AF8D0040DD4AA68E0E477F77674C329E2D01CC_RuntimeMethod_var)));
	}

IL_0140:
	{
		return (bool)1;
	}

IL_0142:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_24 = __this->get_input_14();
		NullCheck(L_24);
		int32_t L_25;
		L_25 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_24, ((int32_t)16), /*hidden argument*/NULL);
		int32_t L_26 = L_25;
		V_2 = L_26;
		__this->set_uncomprLen_9(L_26);
		int32_t L_27 = V_2;
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_015d;
		}
	}
	{
		return (bool)0;
	}

IL_015d:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_28 = __this->get_input_14();
		NullCheck(L_28);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_28, ((int32_t)16), /*hidden argument*/NULL);
		__this->set_mode_4(4);
	}

IL_0171:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_29 = __this->get_input_14();
		NullCheck(L_29);
		int32_t L_30;
		L_30 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_29, ((int32_t)16), /*hidden argument*/NULL);
		V_3 = L_30;
		int32_t L_31 = V_3;
		if ((((int32_t)L_31) >= ((int32_t)0)))
		{
			goto IL_0185;
		}
	}
	{
		return (bool)0;
	}

IL_0185:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_32 = __this->get_input_14();
		NullCheck(L_32);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_32, ((int32_t)16), /*hidden argument*/NULL);
		int32_t L_33 = V_3;
		int32_t L_34 = __this->get_uncomprLen_9();
		if ((((int32_t)L_33) == ((int32_t)((int32_t)((int32_t)L_34^(int32_t)((int32_t)65535))))))
		{
			goto IL_01ac;
		}
	}
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_35 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_35, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralF5140C8A872A6E28C108AF6DAFC168839A3721CD)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_Decode_m58AF8D0040DD4AA68E0E477F77674C329E2D01CC_RuntimeMethod_var)));
	}

IL_01ac:
	{
		__this->set_mode_4(5);
	}

IL_01b3:
	{
		OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * L_36 = __this->get_outputWindow_15();
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_37 = __this->get_input_14();
		int32_t L_38 = __this->get_uncomprLen_9();
		NullCheck(L_36);
		int32_t L_39;
		L_39 = OutputWindow_CopyStored_m7676CBB69C573A09CED8B5EB799DC3C8759E6BDF(L_36, L_37, L_38, /*hidden argument*/NULL);
		V_4 = L_39;
		int32_t L_40 = __this->get_uncomprLen_9();
		int32_t L_41 = V_4;
		__this->set_uncomprLen_9(((int32_t)il2cpp_codegen_subtract((int32_t)L_40, (int32_t)L_41)));
		int32_t L_42 = __this->get_uncomprLen_9();
		if (L_42)
		{
			goto IL_01ec;
		}
	}
	{
		__this->set_mode_4(2);
		return (bool)1;
	}

IL_01ec:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_43 = __this->get_input_14();
		NullCheck(L_43);
		bool L_44;
		L_44 = StreamManipulator_get_IsNeedingInput_m962D2BBB56BD8BAC834E2A12897C13A7D4DCE456(L_43, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_44) == ((int32_t)0))? 1 : 0);
	}

IL_01fb:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_45 = __this->get_dynHeader_16();
		NullCheck(L_45);
		bool L_46;
		L_46 = InflaterDynHeader_AttemptRead_m4593F6F078B40FEC075F5760125F157BD564AC53(L_45, /*hidden argument*/NULL);
		if (L_46)
		{
			goto IL_020a;
		}
	}
	{
		return (bool)0;
	}

IL_020a:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_47 = __this->get_dynHeader_16();
		NullCheck(L_47);
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_48;
		L_48 = InflaterDynHeader_get_LiteralLengthTree_m79581D6BBB306802E0B9BCD492B4D4285509F9F2(L_47, /*hidden argument*/NULL);
		__this->set_litlenTree_17(L_48);
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_49 = __this->get_dynHeader_16();
		NullCheck(L_49);
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_50;
		L_50 = InflaterDynHeader_get_DistanceTree_m4F6820D7E6C0EB2A9A37807FC2241A66361E05F6(L_49, /*hidden argument*/NULL);
		__this->set_distTree_18(L_50);
		__this->set_mode_4(7);
	}

IL_0233:
	{
		bool L_51;
		L_51 = Inflater_DecodeHuffman_mAB4BC68E10CE0BC0F737716E1B52EA85096BC740(__this, /*hidden argument*/NULL);
		return L_51;
	}

IL_023a:
	{
		return (bool)0;
	}

IL_023c:
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_52 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_52, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralE2342616D08840279278C2B09E3EEBDC0A0FEAE6)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_52, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_Decode_m58AF8D0040DD4AA68E0E477F77674C329E2D01CC_RuntimeMethod_var)));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Inflater_SetInput_mEF7B47FADA151D32D6020DC409B36CF51A22F849 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___index1, int32_t ___count2, const RuntimeMethod* method)
{
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_0 = __this->get_input_14();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___buffer0;
		int32_t L_2 = ___index1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		int64_t L_4 = __this->get_totalIn_12();
		int32_t L_5 = ___count2;
		__this->set_totalIn_12(((int64_t)il2cpp_codegen_add((int64_t)L_4, (int64_t)((int64_t)((int64_t)L_5)))));
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Inflater_Inflate_m21F470F4D5A79E52EDA46572AEB52DD878A3C424 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___buffer0;
		NullCheck((RuntimeObject *)(RuntimeObject *)L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(RuntimeObject *)L_1);
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_3 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_Inflate_m21F470F4D5A79E52EDA46572AEB52DD878A3C424_RuntimeMethod_var)));
	}

IL_000f:
	{
		int32_t L_4 = ___count2;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_5;
		L_5 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___count2), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_6 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005(L_6, L_5, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral93A2E40752B3D03B73B603835B057B861A530EE9)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_Inflate_m21F470F4D5A79E52EDA46572AEB52DD878A3C424_RuntimeMethod_var)));
	}

IL_0025:
	{
		int32_t L_7 = ___offset1;
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_8;
		L_8 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___offset1), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_9 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005(L_9, L_8, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral553CED52ED1AB1E6F030EF57734B26F12CA2DE3A)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_Inflate_m21F470F4D5A79E52EDA46572AEB52DD878A3C424_RuntimeMethod_var)));
	}

IL_003b:
	{
		int32_t L_10 = ___offset1;
		int32_t L_11 = ___count2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12 = ___buffer0;
		NullCheck(L_12);
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)L_11))) <= ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))))))
		{
			goto IL_004e;
		}
	}
	{
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_13 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_13, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralE2892E4FE328B349200CDDAF1DED7E765A3843B4)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_13, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Inflater_Inflate_m21F470F4D5A79E52EDA46572AEB52DD878A3C424_RuntimeMethod_var)));
	}

IL_004e:
	{
		int32_t L_14 = ___count2;
		if (L_14)
		{
			goto IL_0062;
		}
	}
	{
		bool L_15;
		L_15 = Inflater_get_IsFinished_m90C7D1B230EEEB2150FA15482E9F6A97AF168812(__this, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0060;
		}
	}
	{
		bool L_16;
		L_16 = Inflater_Decode_m58AF8D0040DD4AA68E0E477F77674C329E2D01CC(__this, /*hidden argument*/NULL);
	}

IL_0060:
	{
		return 0;
	}

IL_0062:
	{
		V_0 = 0;
	}

IL_0064:
	{
		int32_t L_17 = __this->get_mode_4();
		if ((((int32_t)L_17) == ((int32_t)((int32_t)11))))
		{
			goto IL_00be;
		}
	}
	{
		OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * L_18 = __this->get_outputWindow_15();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_19 = ___buffer0;
		int32_t L_20 = ___offset1;
		int32_t L_21 = ___count2;
		NullCheck(L_18);
		int32_t L_22;
		L_22 = OutputWindow_CopyOutput_m256CF0DA13981FB04C8D0DB6538E622E11FBEA34(L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		int32_t L_23 = V_1;
		if ((((int32_t)L_23) <= ((int32_t)0)))
		{
			goto IL_00be;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_24 = __this->get_adler_19();
		if (!L_24)
		{
			goto IL_009c;
		}
	}
	{
		Adler32_t2CB4C85E7AFAEAAE844DB83A402C35C4BF6982EB * L_25 = __this->get_adler_19();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_26 = ___buffer0;
		int32_t L_27 = ___offset1;
		int32_t L_28 = V_1;
		ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE  L_29;
		memset((&L_29), 0, sizeof(L_29));
		ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D((&L_29), L_26, L_27, L_28, /*hidden argument*/ArraySegment_1__ctor_mAA780E22BB5AE07078510EDCE524DD1EA1E98E0D_RuntimeMethod_var);
		NullCheck(L_25);
		Adler32_Update_m97D8416919DC739C90BEE5CE6F50C85521246A58(L_25, L_29, /*hidden argument*/NULL);
	}

IL_009c:
	{
		int32_t L_30 = ___offset1;
		int32_t L_31 = V_1;
		___offset1 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)L_31));
		int32_t L_32 = V_0;
		int32_t L_33 = V_1;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)L_33));
		int64_t L_34 = __this->get_totalOut_11();
		int32_t L_35 = V_1;
		__this->set_totalOut_11(((int64_t)il2cpp_codegen_add((int64_t)L_34, (int64_t)((int64_t)((int64_t)L_35)))));
		int32_t L_36 = ___count2;
		int32_t L_37 = V_1;
		___count2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_36, (int32_t)L_37));
		int32_t L_38 = ___count2;
		if (L_38)
		{
			goto IL_00be;
		}
	}
	{
		int32_t L_39 = V_0;
		return L_39;
	}

IL_00be:
	{
		bool L_40;
		L_40 = Inflater_Decode_m58AF8D0040DD4AA68E0E477F77674C329E2D01CC(__this, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_0064;
		}
	}
	{
		OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * L_41 = __this->get_outputWindow_15();
		NullCheck(L_41);
		int32_t L_42;
		L_42 = OutputWindow_GetAvailable_m00EBEEB3AAAEB4C919C63615591FAF21BB60BBEC_inline(L_41, /*hidden argument*/NULL);
		if ((((int32_t)L_42) <= ((int32_t)0)))
		{
			goto IL_00de;
		}
	}
	{
		int32_t L_43 = __this->get_mode_4();
		if ((!(((uint32_t)L_43) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0064;
		}
	}

IL_00de:
	{
		int32_t L_44 = V_0;
		return L_44;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_get_IsNeedingInput_mD762AD35E335CCC57F14DC3304BF50A845676F16 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		bool L_1;
		L_1 = StreamManipulator_get_IsNeedingInput_m962D2BBB56BD8BAC834E2A12897C13A7D4DCE456(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingDictionary()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_get_IsNeedingDictionary_mC165F0D2C014EECB04B5CD95E1875610942D4155 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_mode_4();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = __this->get_neededBits_6();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0013:
	{
		return (bool)0;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsFinished()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Inflater_get_IsFinished_m90C7D1B230EEEB2150FA15482E9F6A97AF168812 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_mode_4();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0019;
		}
	}
	{
		OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * L_1 = __this->get_outputWindow_15();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = OutputWindow_GetAvailable_m00EBEEB3AAAEB4C919C63615591FAF21BB60BBEC_inline(L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_0019:
	{
		return (bool)0;
	}
}
// System.Int64 FMETP.SharpZipLib.Zip.Compression.Inflater::get_TotalOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t Inflater_get_TotalOut_m24899EBBA3839BC2A41C59359686D6CB758576F0 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = __this->get_totalOut_11();
		return L_0;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::get_RemainingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Inflater_get_RemainingInput_m1D9B9068C5E2A64081E9CA6F3AA11FDE926B6102 (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = StreamManipulator_get_AvailableBytes_m4993F192D46EF3A516A6E364BDE5E952D8281457(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Inflater__cctor_m3C243F5B53527B60D563D5CF4B499CC1D825A65E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____67C0E784F3654B008A81E2988588CF4956CCF3DA_4_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____79D521E6E3E55103005E9CC3FA43B3174FAF090F_6_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____D068832E6B13A623916709C1E0E25ADCBE7B455F_8_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9_FieldInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)29));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = L_0;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____D8E4ACBC2D957C3344A3CAD69FCF9A60C8034DBF_9_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1, L_2, /*hidden argument*/NULL);
		((Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields*)il2cpp_codegen_static_fields_for(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var))->set_CPLENS_0(L_1);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_3 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)29));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_4 = L_3;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_5 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____67C0E784F3654B008A81E2988588CF4956CCF3DA_4_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_4, L_5, /*hidden argument*/NULL);
		((Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields*)il2cpp_codegen_static_fields_for(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var))->set_CPLEXT_1(L_4);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_6 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_7 = L_6;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_8 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____D068832E6B13A623916709C1E0E25ADCBE7B455F_8_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_7, L_8, /*hidden argument*/NULL);
		((Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields*)il2cpp_codegen_static_fields_for(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var))->set_CPDIST_2(L_7);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_9 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_10 = L_9;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_11 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____79D521E6E3E55103005E9CC3FA43B3174FAF090F_6_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_10, L_11, /*hidden argument*/NULL);
		((Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_StaticFields*)il2cpp_codegen_static_fields_for(Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7_il2cpp_TypeInfo_var))->set_CPDEXT_3(L_10);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::AttemptRead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InflaterDynHeader_AttemptRead_m4593F6F078B40FEC075F5760125F157BD564AC53 (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_tD64DA1873BBF65E545905171348E0241A3B706C0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = __this->get_state_2();
		NullCheck(L_0);
		bool L_1;
		L_1 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_0);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		RuntimeObject* L_2 = __this->get_state_2();
		NullCheck(L_2);
		bool L_3;
		L_3 = InterfaceFuncInvoker0< bool >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Boolean>::get_Current() */, IEnumerator_1_tD64DA1873BBF65E545905171348E0241A3B706C0_il2cpp_TypeInfo_var, L_2);
		return L_3;
	}

IL_0019:
	{
		return (bool)1;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::.ctor(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterDynHeader__ctor_mE2911B9C7C908A0691E08CBCDCFB9573644BCBD9 (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * __this, StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_t90AD96F2C518D6F04343C83B67B02220C715C642_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)316));
		__this->set_codeLengths_4(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_1 = ___input0;
		__this->set_input_1(L_1);
		RuntimeObject* L_2;
		L_2 = InflaterDynHeader_CreateStateMachine_mDFFFFAC859E480605B8D903AF0985F656B0FDE1C(__this, /*hidden argument*/NULL);
		__this->set_stateMachine_3(L_2);
		RuntimeObject* L_3 = __this->get_stateMachine_3();
		NullCheck(L_3);
		RuntimeObject* L_4;
		L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Boolean>::GetEnumerator() */, IEnumerable_1_t90AD96F2C518D6F04343C83B67B02220C715C642_il2cpp_TypeInfo_var, L_3);
		__this->set_state_2(L_4);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Boolean> FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::CreateStateMachine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InflaterDynHeader_CreateStateMachine_mDFFFFAC859E480605B8D903AF0985F656B0FDE1C (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * L_0 = (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 *)il2cpp_codegen_object_new(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3_il2cpp_TypeInfo_var);
		U3CCreateStateMachineU3Ed__7__ctor_mDA5BCC7D91C9FE2D8B24060145C553D67CBC0A2B(L_0, ((int32_t)-2), /*hidden argument*/NULL);
		U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_3(__this);
		return L_1;
	}
}
// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::get_LiteralLengthTree()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * InflaterDynHeader_get_LiteralLengthTree_m79581D6BBB306802E0B9BCD492B4D4285509F9F2 (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * __this, const RuntimeMethod* method)
{
	{
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_0 = __this->get_litLenTree_5();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_1 = __this->get_litLenTree_5();
		return L_1;
	}

IL_000f:
	{
		StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C * L_2 = (StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C_il2cpp_TypeInfo_var)));
		StreamDecodingException__ctor_mE34C08B6F04E109D9A4E0B2DA3DB7C67555C5CE1(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral693A080DEBFE98FE11083064E8A1C20EFF085780)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterDynHeader_get_LiteralLengthTree_m79581D6BBB306802E0B9BCD492B4D4285509F9F2_RuntimeMethod_var)));
	}
}
// FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::get_DistanceTree()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * InflaterDynHeader_get_DistanceTree_m4F6820D7E6C0EB2A9A37807FC2241A66361E05F6 (InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * __this, const RuntimeMethod* method)
{
	{
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_0 = __this->get_distTree_6();
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_1 = __this->get_distTree_6();
		return L_1;
	}

IL_000f:
	{
		StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C * L_2 = (StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C_il2cpp_TypeInfo_var)));
		StreamDecodingException__ctor_mE34C08B6F04E109D9A4E0B2DA3DB7C67555C5CE1(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral693A080DEBFE98FE11083064E8A1C20EFF085780)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterDynHeader_get_DistanceTree_m4F6820D7E6C0EB2A9A37807FC2241A66361E05F6_RuntimeMethod_var)));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterDynHeader__cctor_m5EBE9FC83B3C055E1F094B6DE01A772F6977CB32 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2_FieldInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)19));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = L_0;
		RuntimeFieldHandle_t7BE65FC857501059EBAC9772C93B02CD413D9C96  L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t25A1A5C706AD459FF2BFF192DF067D8150488D1B____1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_2_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_mE27238308FED781F2D6A719F0903F2E1311B058F((RuntimeArray *)(RuntimeArray *)L_1, L_2, /*hidden argument*/NULL);
		((InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_StaticFields*)il2cpp_codegen_static_fields_for(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_il2cpp_TypeInfo_var))->set_MetaCodeLengthIndex_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterHuffmanTree__cctor_mC7178D83B2CC2CD1FFAFC67BD5C575E6913F1E69 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	int32_t V_1 = 0;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)288));
			V_0 = L_0;
			V_1 = 0;
			goto IL_0017;
		}

IL_000f:
		{
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = V_0;
			int32_t L_2 = V_1;
			int32_t L_3 = L_2;
			V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1));
			NullCheck(L_1);
			(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)8);
		}

IL_0017:
		{
			int32_t L_4 = V_1;
			if ((((int32_t)L_4) < ((int32_t)((int32_t)144))))
			{
				goto IL_000f;
			}
		}

IL_001f:
		{
			goto IL_002a;
		}

IL_0021:
		{
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = V_0;
			int32_t L_6 = V_1;
			int32_t L_7 = L_6;
			V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
			NullCheck(L_5);
			(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (uint8_t)((int32_t)9));
		}

IL_002a:
		{
			int32_t L_8 = V_1;
			if ((((int32_t)L_8) < ((int32_t)((int32_t)256))))
			{
				goto IL_0021;
			}
		}

IL_0032:
		{
			goto IL_003c;
		}

IL_0034:
		{
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9 = V_0;
			int32_t L_10 = V_1;
			int32_t L_11 = L_10;
			V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
			NullCheck(L_9);
			(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (uint8_t)7);
		}

IL_003c:
		{
			int32_t L_12 = V_1;
			if ((((int32_t)L_12) < ((int32_t)((int32_t)280))))
			{
				goto IL_0034;
			}
		}

IL_0044:
		{
			goto IL_004e;
		}

IL_0046:
		{
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_13 = V_0;
			int32_t L_14 = V_1;
			int32_t L_15 = L_14;
			V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
			NullCheck(L_13);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (uint8_t)8);
		}

IL_004e:
		{
			int32_t L_16 = V_1;
			if ((((int32_t)L_16) < ((int32_t)((int32_t)288))))
			{
				goto IL_0046;
			}
		}

IL_0056:
		{
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_17 = V_0;
			InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_18 = (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE *)il2cpp_codegen_object_new(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var);
			InflaterHuffmanTree__ctor_m8979A23B855128F705F710C36B32AB863EADF1D5(L_18, (RuntimeObject*)(RuntimeObject*)L_17, /*hidden argument*/NULL);
			((InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_StaticFields*)il2cpp_codegen_static_fields_for(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var))->set_defLitLenTree_1(L_18);
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_19 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32));
			V_0 = L_19;
			V_1 = 0;
			goto IL_0075;
		}

IL_006d:
		{
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_20 = V_0;
			int32_t L_21 = V_1;
			int32_t L_22 = L_21;
			V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
			NullCheck(L_20);
			(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (uint8_t)5);
		}

IL_0075:
		{
			int32_t L_23 = V_1;
			if ((((int32_t)L_23) < ((int32_t)((int32_t)32))))
			{
				goto IL_006d;
			}
		}

IL_007a:
		{
			ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_24 = V_0;
			InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_25 = (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE *)il2cpp_codegen_object_new(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var);
			InflaterHuffmanTree__ctor_m8979A23B855128F705F710C36B32AB863EADF1D5(L_25, (RuntimeObject*)(RuntimeObject*)L_24, /*hidden argument*/NULL);
			((InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_StaticFields*)il2cpp_codegen_static_fields_for(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var))->set_defDistTree_2(L_25);
			goto IL_0093;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0087;
		}
		throw e;
	}

CATCH_0087:
	{ // begin catch(System.Exception)
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_26 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_26, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralB81349898E91060D24CC2168BA4D1A2744026AF9)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterHuffmanTree__cctor_mC7178D83B2CC2CD1FFAFC67BD5C575E6913F1E69_RuntimeMethod_var)));
	} // end catch (depth: 1)

IL_0093:
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.ctor(System.Collections.Generic.IList`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterHuffmanTree__ctor_m8979A23B855128F705F710C36B32AB863EADF1D5 (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * __this, RuntimeObject* ___codeLengths0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___codeLengths0;
		InflaterHuffmanTree_BuildTree_mB50BCB289AD6E0A6F2D346F25AF26AFAE424EAD9(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::BuildTree(System.Collections.Generic.IList`1<System.Byte>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterHuffmanTree_BuildTree_mB50BCB289AD6E0A6F2D346F25AF26AFAE424EAD9 (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * __this, RuntimeObject* ___codeLengths0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_1_t5AB6E9D20BDB8A993042228A58C871DF8C3BCE87_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_1_t15467A9C40BD12CE17BE6FF409B2EF0FDD26B9D6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* V_0 = NULL;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_0 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		V_0 = L_0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		V_1 = L_1;
		V_5 = 0;
		goto IL_0037;
	}

IL_0015:
	{
		RuntimeObject* L_2 = ___codeLengths0;
		int32_t L_3 = V_5;
		NullCheck(L_2);
		uint8_t L_4;
		L_4 = InterfaceFuncInvoker1< uint8_t, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Byte>::get_Item(System.Int32) */, IList_1_t15467A9C40BD12CE17BE6FF409B2EF0FDD26B9D6_il2cpp_TypeInfo_var, L_2, L_3);
		V_6 = L_4;
		int32_t L_5 = V_6;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_6 = V_0;
		int32_t L_7 = V_6;
		NullCheck(L_6);
		int32_t* L_8 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)));
		int32_t L_9 = *((int32_t*)L_8);
		*((int32_t*)L_8) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0031:
	{
		int32_t L_10 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0037:
	{
		int32_t L_11 = V_5;
		RuntimeObject* L_12 = ___codeLengths0;
		NullCheck(L_12);
		int32_t L_13;
		L_13 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Byte>::get_Count() */, ICollection_1_t5AB6E9D20BDB8A993042228A58C871DF8C3BCE87_il2cpp_TypeInfo_var, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0015;
		}
	}
	{
		V_2 = 0;
		V_3 = ((int32_t)512);
		V_7 = 1;
		goto IL_0095;
	}

IL_004e:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_14 = V_1;
		int32_t L_15 = V_7;
		int32_t L_16 = V_2;
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (int32_t)L_16);
		int32_t L_17 = V_2;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_18 = V_0;
		int32_t L_19 = V_7;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		int32_t L_22 = V_7;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)((int32_t)((int32_t)L_21<<(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)16), (int32_t)L_22))&(int32_t)((int32_t)31)))))));
		int32_t L_23 = V_7;
		if ((((int32_t)L_23) < ((int32_t)((int32_t)10))))
		{
			goto IL_008f;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_24 = V_1;
		int32_t L_25 = V_7;
		NullCheck(L_24);
		int32_t L_26 = L_25;
		int32_t L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		V_8 = ((int32_t)((int32_t)L_27&(int32_t)((int32_t)130944)));
		int32_t L_28 = V_2;
		V_9 = ((int32_t)((int32_t)L_28&(int32_t)((int32_t)130944)));
		int32_t L_29 = V_3;
		int32_t L_30 = V_9;
		int32_t L_31 = V_8;
		int32_t L_32 = V_7;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_30, (int32_t)L_31))>>(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)16), (int32_t)L_32))&(int32_t)((int32_t)31)))))));
	}

IL_008f:
	{
		int32_t L_33 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_0095:
	{
		int32_t L_34 = V_7;
		if ((((int32_t)L_34) <= ((int32_t)((int32_t)15))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_35 = V_3;
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_36 = (Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)SZArrayNew(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var, (uint32_t)L_35);
		__this->set_tree_0(L_36);
		V_4 = ((int32_t)512);
		V_10 = ((int32_t)15);
		goto IL_0114;
	}

IL_00b4:
	{
		int32_t L_37 = V_2;
		V_11 = ((int32_t)((int32_t)L_37&(int32_t)((int32_t)130944)));
		int32_t L_38 = V_2;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_39 = V_0;
		int32_t L_40 = V_10;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		int32_t L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		int32_t L_43 = V_10;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_38, (int32_t)((int32_t)((int32_t)L_42<<(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)16), (int32_t)L_43))&(int32_t)((int32_t)31)))))));
		int32_t L_44 = V_2;
		V_12 = ((int32_t)((int32_t)L_44&(int32_t)((int32_t)130944)));
		goto IL_0108;
	}

IL_00d8:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_45 = __this->get_tree_0();
		int32_t L_46 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		int16_t L_47;
		L_47 = DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B(L_46, /*hidden argument*/NULL);
		int32_t L_48 = V_4;
		int32_t L_49 = V_10;
		NullCheck(L_45);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(L_47), (int16_t)((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)((-L_48))<<(int32_t)4))|(int32_t)L_49)))));
		int32_t L_50 = V_4;
		int32_t L_51 = V_10;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_50, (int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_51, (int32_t)((int32_t)9)))&(int32_t)((int32_t)31)))))));
		int32_t L_52 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)((int32_t)128)));
	}

IL_0108:
	{
		int32_t L_53 = V_12;
		int32_t L_54 = V_11;
		if ((((int32_t)L_53) < ((int32_t)L_54)))
		{
			goto IL_00d8;
		}
	}
	{
		int32_t L_55 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_55, (int32_t)1));
	}

IL_0114:
	{
		int32_t L_56 = V_10;
		if ((((int32_t)L_56) >= ((int32_t)((int32_t)10))))
		{
			goto IL_00b4;
		}
	}
	{
		V_13 = 0;
		goto IL_01d1;
	}

IL_0122:
	{
		RuntimeObject* L_57 = ___codeLengths0;
		int32_t L_58 = V_13;
		NullCheck(L_57);
		uint8_t L_59;
		L_59 = InterfaceFuncInvoker1< uint8_t, int32_t >::Invoke(0 /* !0 System.Collections.Generic.IList`1<System.Byte>::get_Item(System.Int32) */, IList_1_t15467A9C40BD12CE17BE6FF409B2EF0FDD26B9D6_il2cpp_TypeInfo_var, L_57, L_58);
		V_14 = L_59;
		int32_t L_60 = V_14;
		if (!L_60)
		{
			goto IL_01cb;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_61 = V_1;
		int32_t L_62 = V_14;
		NullCheck(L_61);
		int32_t L_63 = L_62;
		int32_t L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		V_2 = L_64;
		int32_t L_65 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		int16_t L_66;
		L_66 = DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B(L_65, /*hidden argument*/NULL);
		V_15 = L_66;
		int32_t L_67 = V_14;
		if ((((int32_t)L_67) > ((int32_t)((int32_t)9))))
		{
			goto IL_016e;
		}
	}

IL_0146:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_68 = __this->get_tree_0();
		int32_t L_69 = V_15;
		int32_t L_70 = V_13;
		int32_t L_71 = V_14;
		NullCheck(L_68);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(L_69), (int16_t)((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_70<<(int32_t)4))|(int32_t)L_71)))));
		int32_t L_72 = V_15;
		int32_t L_73 = V_14;
		V_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_72, (int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_73&(int32_t)((int32_t)31)))))));
		int32_t L_74 = V_15;
		if ((((int32_t)L_74) < ((int32_t)((int32_t)512))))
		{
			goto IL_0146;
		}
	}
	{
		goto IL_01bb;
	}

IL_016e:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_75 = __this->get_tree_0();
		int32_t L_76 = V_15;
		NullCheck(L_75);
		int32_t L_77 = ((int32_t)((int32_t)L_76&(int32_t)((int32_t)511)));
		int16_t L_78 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		V_16 = L_78;
		int32_t L_79 = V_16;
		V_17 = ((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_79&(int32_t)((int32_t)15)))&(int32_t)((int32_t)31)))));
		int32_t L_80 = V_16;
		V_16 = ((-((int32_t)((int32_t)L_80>>(int32_t)4))));
	}

IL_0192:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_81 = __this->get_tree_0();
		int32_t L_82 = V_16;
		int32_t L_83 = V_15;
		int32_t L_84 = V_13;
		int32_t L_85 = V_14;
		NullCheck(L_81);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_82|(int32_t)((int32_t)((int32_t)L_83>>(int32_t)((int32_t)9)))))), (int16_t)((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_84<<(int32_t)4))|(int32_t)L_85)))));
		int32_t L_86 = V_15;
		int32_t L_87 = V_14;
		V_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_86, (int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_87&(int32_t)((int32_t)31)))))));
		int32_t L_88 = V_15;
		int32_t L_89 = V_17;
		if ((((int32_t)L_88) < ((int32_t)L_89)))
		{
			goto IL_0192;
		}
	}

IL_01bb:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_90 = V_1;
		int32_t L_91 = V_14;
		int32_t L_92 = V_2;
		int32_t L_93 = V_14;
		NullCheck(L_90);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(L_91), (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_92, (int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)16), (int32_t)L_93))&(int32_t)((int32_t)31))))))));
	}

IL_01cb:
	{
		int32_t L_94 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_94, (int32_t)1));
	}

IL_01d1:
	{
		int32_t L_95 = V_13;
		RuntimeObject* L_96 = ___codeLengths0;
		NullCheck(L_96);
		int32_t L_97;
		L_97 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Byte>::get_Count() */, ICollection_1_t5AB6E9D20BDB8A993042228A58C871DF8C3BCE87_il2cpp_TypeInfo_var, L_96);
		if ((((int32_t)L_95) < ((int32_t)L_97)))
		{
			goto IL_0122;
		}
	}
	{
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::GetSymbol(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InflaterHuffmanTree_GetSymbol_m0F2DAE53C30D2A81AC3ED21E476F2D3451728442 (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * __this, StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * ___input0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_0 = ___input0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_0, ((int32_t)9), /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		V_0 = L_2;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_009f;
		}
	}
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_3 = __this->get_tree_0();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int16_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		int32_t L_7 = V_1;
		V_2 = ((int32_t)((int32_t)L_7&(int32_t)((int32_t)15)));
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) < ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_9 = V_2;
		if (L_9)
		{
			goto IL_0030;
		}
	}
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_10 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_10, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral99EF3F46FC3EBDEC21B57E38C06DA94FE17B8895)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterHuffmanTree_GetSymbol_m0F2DAE53C30D2A81AC3ED21E476F2D3451728442_RuntimeMethod_var)));
	}

IL_0030:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_11 = ___input0;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		return ((int32_t)((int32_t)L_13>>(int32_t)4));
	}

IL_003b:
	{
		int32_t L_14 = V_1;
		V_3 = ((-((int32_t)((int32_t)L_14>>(int32_t)4))));
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_15 = ___input0;
		int32_t L_16 = V_2;
		NullCheck(L_15);
		int32_t L_17;
		L_17 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_15, L_16, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		V_0 = L_18;
		if ((((int32_t)L_18) < ((int32_t)0)))
		{
			goto IL_0068;
		}
	}
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_19 = __this->get_tree_0();
		int32_t L_20 = V_3;
		int32_t L_21 = V_0;
		NullCheck(L_19);
		int32_t L_22 = ((int32_t)((int32_t)L_20|(int32_t)((int32_t)((int32_t)L_21>>(int32_t)((int32_t)9)))));
		int16_t L_23 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		V_1 = L_23;
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_24 = ___input0;
		int32_t L_25 = V_1;
		NullCheck(L_24);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_24, ((int32_t)((int32_t)L_25&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_26 = V_1;
		return ((int32_t)((int32_t)L_26>>(int32_t)4));
	}

IL_0068:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_27 = ___input0;
		NullCheck(L_27);
		int32_t L_28;
		L_28 = StreamManipulator_get_AvailableBits_m7FA2836285F2C2BA6B74A8AA135D770DB9030A7B_inline(L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_29 = ___input0;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		int32_t L_31;
		L_31 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_29, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_32 = __this->get_tree_0();
		int32_t L_33 = V_3;
		int32_t L_34 = V_0;
		NullCheck(L_32);
		int32_t L_35 = ((int32_t)((int32_t)L_33|(int32_t)((int32_t)((int32_t)L_34>>(int32_t)((int32_t)9)))));
		int16_t L_36 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		V_1 = L_36;
		int32_t L_37 = V_1;
		int32_t L_38 = V_4;
		if ((((int32_t)((int32_t)((int32_t)L_37&(int32_t)((int32_t)15)))) > ((int32_t)L_38)))
		{
			goto IL_009d;
		}
	}
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_39 = ___input0;
		int32_t L_40 = V_1;
		NullCheck(L_39);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_39, ((int32_t)((int32_t)L_40&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_41 = V_1;
		return ((int32_t)((int32_t)L_41>>(int32_t)4));
	}

IL_009d:
	{
		return (-1);
	}

IL_009f:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_42 = ___input0;
		NullCheck(L_42);
		int32_t L_43;
		L_43 = StreamManipulator_get_AvailableBits_m7FA2836285F2C2BA6B74A8AA135D770DB9030A7B_inline(L_42, /*hidden argument*/NULL);
		V_5 = L_43;
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_44 = ___input0;
		int32_t L_45 = V_5;
		NullCheck(L_44);
		int32_t L_46;
		L_46 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(L_44, L_45, /*hidden argument*/NULL);
		V_0 = L_46;
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_47 = __this->get_tree_0();
		int32_t L_48 = V_0;
		NullCheck(L_47);
		int32_t L_49 = L_48;
		int16_t L_50 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		V_1 = L_50;
		int32_t L_51 = V_1;
		if ((((int32_t)L_51) < ((int32_t)0)))
		{
			goto IL_00d3;
		}
	}
	{
		int32_t L_52 = V_1;
		int32_t L_53 = V_5;
		if ((((int32_t)((int32_t)((int32_t)L_52&(int32_t)((int32_t)15)))) > ((int32_t)L_53)))
		{
			goto IL_00d3;
		}
	}
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_54 = ___input0;
		int32_t L_55 = V_1;
		NullCheck(L_54);
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(L_54, ((int32_t)((int32_t)L_55&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_56 = V_1;
		return ((int32_t)((int32_t)L_56>>(int32_t)4));
	}

IL_00d3:
	{
		return (-1);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputBuffer__ctor_m497E472D25D2E3D02803F5561C2A1C80477EBE26 (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___stream0, int32_t ___bufferSize1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = ___stream0;
		__this->set_inputStream_6(L_0);
		int32_t L_1 = ___bufferSize1;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)1024))))
		{
			goto IL_001c;
		}
	}
	{
		___bufferSize1 = ((int32_t)1024);
	}

IL_001c:
	{
		int32_t L_2 = ___bufferSize1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_2);
		__this->set_rawData_1(L_3);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = __this->get_rawData_1();
		__this->set_clearText_3(L_4);
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InflaterInputBuffer_get_Available_m7BDBC3F8048C19BF8C9D3E253BBFDA8601A107DE (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_available_4();
		return L_0;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputBuffer_set_Available_mB9B0F342C4B3FB90CCFC0F34B4C5CEDDB7104981 (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_available_4(L_0);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(FMETP.SharpZipLib.Zip.Compression.Inflater)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputBuffer_SetInflaterInput_m84191E11B0A3E51A5F01CC1CA537E8915FB9243F (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * ___inflater0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_available_4();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_1 = ___inflater0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = __this->get_clearText_3();
		int32_t L_3 = __this->get_clearTextLength_2();
		int32_t L_4 = __this->get_available_4();
		int32_t L_5 = __this->get_available_4();
		NullCheck(L_1);
		Inflater_SetInput_mEF7B47FADA151D32D6020DC409B36CF51A22F849(L_1, L_2, ((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)L_4)), L_5, /*hidden argument*/NULL);
		__this->set_available_4(0);
	}

IL_002f:
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputBuffer_Fill_mD6D37972021807E5F4615E4D33276A13D2D64925 (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICryptoTransform_t001D97000AA0178942D19FC52942472140472E5E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		__this->set_rawLength_0(0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_rawData_1();
		NullCheck(L_0);
		V_0 = ((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length)));
		goto IL_0041;
	}

IL_0012:
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_1 = __this->get_inputStream_6();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = __this->get_rawData_1();
		int32_t L_3 = __this->get_rawLength_0();
		int32_t L_4 = V_0;
		NullCheck(L_1);
		int32_t L_5;
		L_5 = VirtFuncInvoker3< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t >::Invoke(25 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_1, L_2, L_3, L_4);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_7 = __this->get_rawLength_0();
		int32_t L_8 = V_1;
		__this->set_rawLength_0(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8)));
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)L_10));
	}

IL_0041:
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_12 = __this->get_inputStream_6();
		NullCheck(L_12);
		bool L_13;
		L_13 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Stream::get_CanRead() */, L_12);
		if (L_13)
		{
			goto IL_0012;
		}
	}

IL_0052:
	{
		RuntimeObject* L_14 = __this->get_cryptoTransform_5();
		if (!L_14)
		{
			goto IL_0081;
		}
	}
	{
		RuntimeObject* L_15 = __this->get_cryptoTransform_5();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_16 = __this->get_rawData_1();
		int32_t L_17 = __this->get_rawLength_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18 = __this->get_clearText_3();
		NullCheck(L_15);
		int32_t L_19;
		L_19 = InterfaceFuncInvoker5< int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t, int32_t, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*, int32_t >::Invoke(3 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t001D97000AA0178942D19FC52942472140472E5E_il2cpp_TypeInfo_var, L_15, L_16, 0, L_17, L_18, 0);
		__this->set_clearTextLength_2(L_19);
		goto IL_008d;
	}

IL_0081:
	{
		int32_t L_20 = __this->get_rawLength_0();
		__this->set_clearTextLength_2(L_20);
	}

IL_008d:
	{
		int32_t L_21 = __this->get_clearTextLength_2();
		__this->set_available_4(L_21);
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InflaterInputBuffer_ReadClearTextBuffer_m91A4E800CE258802DA172CC80C03A08E919411A5 (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___outBuffer0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___length2;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1;
		L_1 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___length2), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_2 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputBuffer_ReadClearTextBuffer_m91A4E800CE258802DA172CC80C03A08E919411A5_RuntimeMethod_var)));
	}

IL_0011:
	{
		int32_t L_3 = ___offset1;
		V_0 = L_3;
		int32_t L_4 = ___length2;
		V_1 = L_4;
		goto IL_006f;
	}

IL_0017:
	{
		int32_t L_5 = __this->get_available_4();
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		InflaterInputBuffer_Fill_mD6D37972021807E5F4615E4D33276A13D2D64925(__this, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_available_4();
		if ((((int32_t)L_6) > ((int32_t)0)))
		{
			goto IL_0031;
		}
	}
	{
		return 0;
	}

IL_0031:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = __this->get_available_4();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_9;
		L_9 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = __this->get_clearText_3();
		int32_t L_11 = __this->get_clearTextLength_2();
		int32_t L_12 = __this->get_available_4();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_13 = ___outBuffer0;
		int32_t L_14 = V_0;
		int32_t L_15 = V_2;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_10, ((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)L_12)), (RuntimeArray *)(RuntimeArray *)L_13, L_14, L_15, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		int32_t L_17 = V_2;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_17));
		int32_t L_18 = V_1;
		int32_t L_19 = V_2;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)L_19));
		int32_t L_20 = __this->get_available_4();
		int32_t L_21 = V_2;
		__this->set_available_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)L_21)));
	}

IL_006f:
	{
		int32_t L_22 = V_1;
		if ((((int32_t)L_22) > ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_23 = ___length2;
		return L_23;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894 (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_available_4();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		InflaterInputBuffer_Fill_mD6D37972021807E5F4615E4D33276A13D2D64925(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_available_4();
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		Exception_t * L_2 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral146305E5A9EF2C098E8782ED66BAFB9A4F45E076)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputBuffer_ReadLeByte_m9CE7B5D13768CED6E3F964F1E14C41AE06725894_RuntimeMethod_var)));
	}

IL_0023:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = __this->get_rawData_1();
		int32_t L_4 = __this->get_rawLength_0();
		int32_t L_5 = __this->get_available_4();
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)L_5));
		uint8_t L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		int32_t L_8 = __this->get_available_4();
		__this->set_available_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)));
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,FMETP.SharpZipLib.Zip.Compression.Inflater,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputStream__ctor_m152F154E3EB66C5B8E82F049C1ED5B37E52A19F8 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___baseInputStream0, Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * ___inflater1, int32_t ___bufferSize2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__IsStreamOwner_5((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB_il2cpp_TypeInfo_var);
		Stream__ctor_m5EB0B4BCC014E7D1F18FE0E72B2D6D0C5C13D5C4(__this, /*hidden argument*/NULL);
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = ___baseInputStream0;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_1 = ___baseInputStream0;
		NullCheck(L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_3 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream__ctor_m152F154E3EB66C5B8E82F049C1ED5B37E52A19F8_RuntimeMethod_var)));
	}

IL_001c:
	{
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_4 = ___inflater1;
		if (L_4)
		{
			goto IL_002b;
		}
	}
	{
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_5 = ___inflater1;
		NullCheck(L_5);
		String_t* L_6;
		L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_7 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream__ctor_m152F154E3EB66C5B8E82F049C1ED5B37E52A19F8_RuntimeMethod_var)));
	}

IL_002b:
	{
		int32_t L_8 = ___bufferSize2;
		if ((((int32_t)L_8) > ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_9;
		L_9 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___bufferSize2), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_10 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_10, L_9, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream__ctor_m152F154E3EB66C5B8E82F049C1ED5B37E52A19F8_RuntimeMethod_var)));
	}

IL_003c:
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_11 = ___baseInputStream0;
		__this->set_baseInputStream_8(L_11);
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_12 = ___inflater1;
		__this->set_inf_6(L_12);
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_13 = ___baseInputStream0;
		int32_t L_14 = ___bufferSize2;
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_15 = (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 *)il2cpp_codegen_object_new(InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17_il2cpp_TypeInfo_var);
		InflaterInputBuffer__ctor_m497E472D25D2E3D02803F5561C2A1C80477EBE26(L_15, L_13, L_14, /*hidden argument*/NULL);
		__this->set_inputBuffer_7(L_15);
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_IsStreamOwner()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InflaterInputStream_get_IsStreamOwner_m3C551CED06177186C1D848E92B2E4B8F2C879814 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get__IsStreamOwner_5();
		return L_0;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Fill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputStream_Fill_m1DE4E5EC14785EFD25C866FBD31A46A1144445F2 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method)
{
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_0 = __this->get_inputBuffer_7();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = InflaterInputBuffer_get_Available_m7BDBC3F8048C19BF8C9D3E253BBFDA8601A107DE_inline(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_2 = __this->get_inputBuffer_7();
		NullCheck(L_2);
		InflaterInputBuffer_Fill_mD6D37972021807E5F4615E4D33276A13D2D64925(L_2, /*hidden argument*/NULL);
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_3 = __this->get_inputBuffer_7();
		NullCheck(L_3);
		int32_t L_4;
		L_4 = InflaterInputBuffer_get_Available_m7BDBC3F8048C19BF8C9D3E253BBFDA8601A107DE_inline(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_5 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_5, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral4EB4BE55A4E968B39CF168F95F155F0CE9E4A4BC)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream_Fill_m1DE4E5EC14785EFD25C866FBD31A46A1144445F2_RuntimeMethod_var)));
	}

IL_0032:
	{
		InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * L_6 = __this->get_inputBuffer_7();
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_7 = __this->get_inf_6();
		NullCheck(L_6);
		InflaterInputBuffer_SetInflaterInput_m84191E11B0A3E51A5F01CC1CA537E8915FB9243F(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanRead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InflaterInputStream_get_CanRead_m901AA598D0CC2468E7E5B3CE585CEEFA939105A4 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method)
{
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = __this->get_baseInputStream_8();
		NullCheck(L_0);
		bool L_1;
		L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Stream::get_CanRead() */, L_0);
		return L_1;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanSeek()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InflaterInputStream_get_CanSeek_mCB0CB60D7E7E2158874E2C7628079B18B3AA2759 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanWrite()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InflaterInputStream_get_CanWrite_m61C79F95230F6844B064AAB2D218466D42D9AB35 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t InflaterInputStream_get_Length_mE1ED6BA8DC45ECE8669A532BDADF82426AA14EEF (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_0, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral51F1DBAC2A477B647C00F35F5067C191B5F39517)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream_get_Length_mE1ED6BA8DC45ECE8669A532BDADF82426AA14EEF_RuntimeMethod_var)));
	}
}
// System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t InflaterInputStream_get_Position_m1E7B0E273E2BC810F840C5CC8B70FF60A68D19E4 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method)
{
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = __this->get_baseInputStream_8();
		NullCheck(L_0);
		int64_t L_1;
		L_1 = VirtFuncInvoker0< int64_t >::Invoke(11 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		return L_1;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::set_Position(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputStream_set_Position_mBC3541E547A6D58CB4C318F765D9D5584C850071 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_0, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral0C5D82EC0DDFC2A751DB2B8E28F3256ABDE01270)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream_set_Position_mBC3541E547A6D58CB4C318F765D9D5584C850071_RuntimeMethod_var)));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Flush()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputStream_Flush_m2B3D20377D1F08B4B69BC0ED964F488E107DA881 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method)
{
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_0 = __this->get_baseInputStream_8();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(17 /* System.Void System.IO.Stream::Flush() */, L_0);
		return;
	}
}
// System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Seek(System.Int64,System.IO.SeekOrigin)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t InflaterInputStream_Seek_m44A365F774ED907FB953EE6964F02C492556933A (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, int64_t ___offset0, int32_t ___origin1, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_0, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralCBC3EE4B235C41540E696A527B41C2EB3D741A62)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream_Seek_m44A365F774ED907FB953EE6964F02C492556933A_RuntimeMethod_var)));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Write(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputStream_Write_m9B9B4D2C82868E7F074E04AA712DE2F617CAC2BF (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_0, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral1B51DC7F7A299FDF20D4111AA6D04EFD1D4FDD6B)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream_Write_m9B9B4D2C82868E7F074E04AA712DE2F617CAC2BF_RuntimeMethod_var)));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::WriteByte(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputStream_WriteByte_m0D8A47F5A21C9369611A324A154A2B8AAB8A9134 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, uint8_t ___value0, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_0, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral19735335CA05EBD60664E496D17C7EF387AC7C8B)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream_WriteByte_m0D8A47F5A21C9369611A324A154A2B8AAB8A9134_RuntimeMethod_var)));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InflaterInputStream_Dispose_mE2742F1199772B431DE0600042D5611EB77432E1 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, bool ___disposing0, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_isClosed_9();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		__this->set_isClosed_9((bool)1);
		bool L_1;
		L_1 = InflaterInputStream_get_IsStreamOwner_m3C551CED06177186C1D848E92B2E4B8F2C879814_inline(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * L_2 = __this->get_baseInputStream_8();
		NullCheck(L_2);
		Stream_Dispose_m117324084DDAD414761AD29FB17A419840BA6EA0(L_2, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Read(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InflaterInputStream_Read_mC36B703875162B5FAB4DC4F47A05AAD596331F50 (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_0 = __this->get_inf_6();
		NullCheck(L_0);
		bool L_1;
		L_1 = Inflater_get_IsNeedingDictionary_mC165F0D2C014EECB04B5CD95E1875610942D4155(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_2 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral8FF23D129BAAFF88D5BFD6399DCAA2557F8F2030)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream_Read_mC36B703875162B5FAB4DC4F47A05AAD596331F50_RuntimeMethod_var)));
	}

IL_0018:
	{
		int32_t L_3 = ___count2;
		V_0 = L_3;
	}

IL_001a:
	{
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_4 = __this->get_inf_6();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = ___buffer0;
		int32_t L_6 = ___offset1;
		int32_t L_7 = V_0;
		NullCheck(L_4);
		int32_t L_8;
		L_8 = Inflater_Inflate_m21F470F4D5A79E52EDA46572AEB52DD878A3C424(L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = ___offset1;
		int32_t L_10 = V_1;
		___offset1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_10));
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)L_12));
		int32_t L_13 = V_0;
		if (!L_13)
		{
			goto IL_0065;
		}
	}
	{
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_14 = __this->get_inf_6();
		NullCheck(L_14);
		bool L_15;
		L_15 = Inflater_get_IsFinished_m90C7D1B230EEEB2150FA15482E9F6A97AF168812(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0065;
		}
	}
	{
		Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * L_16 = __this->get_inf_6();
		NullCheck(L_16);
		bool L_17;
		L_17 = Inflater_get_IsNeedingInput_mD762AD35E335CCC57F14DC3304BF50A845676F16(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0057;
		}
	}
	{
		InflaterInputStream_Fill_m1DE4E5EC14785EFD25C866FBD31A46A1144445F2(__this, /*hidden argument*/NULL);
		goto IL_001a;
	}

IL_0057:
	{
		int32_t L_18 = V_1;
		if (L_18)
		{
			goto IL_001a;
		}
	}
	{
		Exception_t * L_19 = (Exception_t *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var)));
		Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11(L_19, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral77B8FF6E0BFB09EB9D8A7585234E9561468125FD)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InflaterInputStream_Read_mC36B703875162B5FAB4DC4F47A05AAD596331F50_RuntimeMethod_var)));
	}

IL_0065:
	{
		int32_t L_20 = ___count2;
		int32_t L_21 = V_0;
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)L_21));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Write(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputWindow_Write_m8E87F3E22FAA5D8B122FB61BD4DF14D07F2E42C6 (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_windowFilled_2();
		V_0 = L_0;
		int32_t L_1 = V_0;
		__this->set_windowFilled_2(((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)1)));
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)32768)))))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_3 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral20B43C997D46773915E000B0A5DBB64316FDD2E8)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&OutputWindow_Write_m8E87F3E22FAA5D8B122FB61BD4DF14D07F2E42C6_RuntimeMethod_var)));
	}

IL_0023:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = __this->get_window_0();
		int32_t L_5 = __this->get_windowEnd_1();
		V_0 = L_5;
		int32_t L_6 = V_0;
		__this->set_windowEnd_1(((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1)));
		int32_t L_7 = V_0;
		int32_t L_8 = ___value0;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (uint8_t)((int32_t)((uint8_t)L_8)));
		int32_t L_9 = __this->get_windowEnd_1();
		__this->set_windowEnd_1(((int32_t)((int32_t)L_9&(int32_t)((int32_t)32767))));
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::SlowRepeat(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputWindow_SlowRepeat_m12DFD2153B9406457D3D8DDCF4C8CB5C8B78E9FD (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, int32_t ___repStart0, int32_t ___length1, int32_t ___distance2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0042;
	}

IL_0002:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_window_0();
		int32_t L_1 = __this->get_windowEnd_1();
		V_0 = L_1;
		int32_t L_2 = V_0;
		__this->set_windowEnd_1(((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1)));
		int32_t L_3 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = __this->get_window_0();
		int32_t L_5 = ___repStart0;
		int32_t L_6 = L_5;
		___repStart0 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
		NullCheck(L_4);
		int32_t L_7 = L_6;
		uint8_t L_8 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)L_8);
		int32_t L_9 = __this->get_windowEnd_1();
		__this->set_windowEnd_1(((int32_t)((int32_t)L_9&(int32_t)((int32_t)32767))));
		int32_t L_10 = ___repStart0;
		___repStart0 = ((int32_t)((int32_t)L_10&(int32_t)((int32_t)32767)));
	}

IL_0042:
	{
		int32_t L_11 = ___length1;
		int32_t L_12 = L_11;
		___length1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1));
		if ((((int32_t)L_12) > ((int32_t)0)))
		{
			goto IL_0002;
		}
	}
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Repeat(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputWindow_Repeat_mD7467DB942F68DACD17A6A466A2F7974AD2969A5 (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, int32_t ___length0, int32_t ___distance1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_windowFilled_2();
		int32_t L_1 = ___length0;
		int32_t L_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1));
		V_2 = L_2;
		__this->set_windowFilled_2(L_2);
		int32_t L_3 = V_2;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)32768))))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_4 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_4, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral20B43C997D46773915E000B0A5DBB64316FDD2E8)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&OutputWindow_Repeat_mD7467DB942F68DACD17A6A466A2F7974AD2969A5_RuntimeMethod_var)));
	}

IL_0023:
	{
		int32_t L_5 = __this->get_windowEnd_1();
		int32_t L_6 = ___distance1;
		V_0 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)L_6))&(int32_t)((int32_t)32767)));
		int32_t L_7 = ___length0;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)32768), (int32_t)L_7));
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		if ((((int32_t)L_8) > ((int32_t)L_9)))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_10 = __this->get_windowEnd_1();
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_12 = ___length0;
		int32_t L_13 = ___distance1;
		if ((((int32_t)L_12) > ((int32_t)L_13)))
		{
			goto IL_0097;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_14 = __this->get_window_0();
		int32_t L_15 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_16 = __this->get_window_0();
		int32_t L_17 = __this->get_windowEnd_1();
		int32_t L_18 = ___length0;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_14, L_15, (RuntimeArray *)(RuntimeArray *)L_16, L_17, L_18, /*hidden argument*/NULL);
		int32_t L_19 = __this->get_windowEnd_1();
		int32_t L_20 = ___length0;
		__this->set_windowEnd_1(((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)L_20)));
		return;
	}

IL_0073:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_21 = __this->get_window_0();
		int32_t L_22 = __this->get_windowEnd_1();
		V_2 = L_22;
		int32_t L_23 = V_2;
		__this->set_windowEnd_1(((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1)));
		int32_t L_24 = V_2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_25 = __this->get_window_0();
		int32_t L_26 = V_0;
		int32_t L_27 = L_26;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
		NullCheck(L_25);
		int32_t L_28 = L_27;
		uint8_t L_29 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (uint8_t)L_29);
	}

IL_0097:
	{
		int32_t L_30 = ___length0;
		int32_t L_31 = L_30;
		___length0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_31, (int32_t)1));
		if ((((int32_t)L_31) > ((int32_t)0)))
		{
			goto IL_0073;
		}
	}
	{
		return;
	}

IL_00a1:
	{
		int32_t L_32 = V_0;
		int32_t L_33 = ___length0;
		int32_t L_34 = ___distance1;
		OutputWindow_SlowRepeat_m12DFD2153B9406457D3D8DDCF4C8CB5C8B78E9FD(__this, L_32, L_33, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyStored(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OutputWindow_CopyStored_m7676CBB69C573A09CED8B5EB799DC3C8759E6BDF (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * ___input0, int32_t ___length1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___length1;
		int32_t L_1 = __this->get_windowFilled_2();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_2;
		L_2 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(L_0, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)32768), (int32_t)L_1)), /*hidden argument*/NULL);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_3 = ___input0;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = StreamManipulator_get_AvailableBytes_m4993F192D46EF3A516A6E364BDE5E952D8281457(L_3, /*hidden argument*/NULL);
		int32_t L_5;
		L_5 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(L_2, L_4, /*hidden argument*/NULL);
		___length1 = L_5;
		int32_t L_6 = __this->get_windowEnd_1();
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)32768), (int32_t)L_6));
		int32_t L_7 = ___length1;
		int32_t L_8 = V_1;
		if ((((int32_t)L_7) <= ((int32_t)L_8)))
		{
			goto IL_005d;
		}
	}
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_9 = ___input0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_10 = __this->get_window_0();
		int32_t L_11 = __this->get_windowEnd_1();
		int32_t L_12 = V_1;
		NullCheck(L_9);
		int32_t L_13;
		L_13 = StreamManipulator_CopyBytes_m6FA30492A0A8534309A1538CF230500658CA3C79(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_16 = V_0;
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_17 = ___input0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18 = __this->get_window_0();
		int32_t L_19 = ___length1;
		int32_t L_20 = V_1;
		NullCheck(L_17);
		int32_t L_21;
		L_21 = StreamManipulator_CopyBytes_m6FA30492A0A8534309A1538CF230500658CA3C79(L_17, L_18, 0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)L_20)), /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_21));
		goto IL_0071;
	}

IL_005d:
	{
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_22 = ___input0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_23 = __this->get_window_0();
		int32_t L_24 = __this->get_windowEnd_1();
		int32_t L_25 = ___length1;
		NullCheck(L_22);
		int32_t L_26;
		L_26 = StreamManipulator_CopyBytes_m6FA30492A0A8534309A1538CF230500658CA3C79(L_22, L_23, L_24, L_25, /*hidden argument*/NULL);
		V_0 = L_26;
	}

IL_0071:
	{
		int32_t L_27 = __this->get_windowEnd_1();
		int32_t L_28 = V_0;
		__this->set_windowEnd_1(((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)L_28))&(int32_t)((int32_t)32767))));
		int32_t L_29 = __this->get_windowFilled_2();
		int32_t L_30 = V_0;
		__this->set_windowFilled_2(((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)L_30)));
		int32_t L_31 = V_0;
		return L_31;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetFreeSpace()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OutputWindow_GetFreeSpace_m8FDF473C07DD2A7E667C0D4F9A52657DDD06B99B (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_windowFilled_2();
		return ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)32768), (int32_t)L_0));
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetAvailable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OutputWindow_GetAvailable_m00EBEEB3AAAEB4C919C63615591FAF21BB60BBEC (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_windowFilled_2();
		return L_0;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyOutput(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t OutputWindow_CopyOutput_m256CF0DA13981FB04C8D0DB6538E622E11FBEA34 (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___output0, int32_t ___offset1, int32_t ___len2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_windowEnd_1();
		V_0 = L_0;
		int32_t L_1 = ___len2;
		int32_t L_2 = __this->get_windowFilled_2();
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = __this->get_windowFilled_2();
		___len2 = L_3;
		goto IL_0030;
	}

IL_001a:
	{
		int32_t L_4 = __this->get_windowEnd_1();
		int32_t L_5 = __this->get_windowFilled_2();
		int32_t L_6 = ___len2;
		V_0 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)L_5)), (int32_t)L_6))&(int32_t)((int32_t)32767)));
	}

IL_0030:
	{
		int32_t L_7 = ___len2;
		V_1 = L_7;
		int32_t L_8 = ___len2;
		int32_t L_9 = V_0;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)L_9));
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0057;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = __this->get_window_0();
		int32_t L_12 = V_2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_13 = ___output0;
		int32_t L_14 = ___offset1;
		int32_t L_15 = V_2;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_11, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)32768), (int32_t)L_12)), (RuntimeArray *)(RuntimeArray *)L_13, L_14, L_15, /*hidden argument*/NULL);
		int32_t L_16 = ___offset1;
		int32_t L_17 = V_2;
		___offset1 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_17));
		int32_t L_18 = V_0;
		___len2 = L_18;
	}

IL_0057:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_19 = __this->get_window_0();
		int32_t L_20 = V_0;
		int32_t L_21 = ___len2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_22 = ___output0;
		int32_t L_23 = ___offset1;
		int32_t L_24 = ___len2;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_19, ((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)L_21)), (RuntimeArray *)(RuntimeArray *)L_22, L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = __this->get_windowFilled_2();
		int32_t L_26 = V_1;
		__this->set_windowFilled_2(((int32_t)il2cpp_codegen_subtract((int32_t)L_25, (int32_t)L_26)));
		int32_t L_27 = __this->get_windowFilled_2();
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_0085;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_28 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_m26BD2B620B5FBFA4376C16011C60E18A2EDC8E96(L_28, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&OutputWindow_CopyOutput_m256CF0DA13981FB04C8D0DB6538E622E11FBEA34_RuntimeMethod_var)));
	}

IL_0085:
	{
		int32_t L_29 = V_1;
		return L_29;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputWindow_Reset_mB97240B42A56D72E549B1967E7646C715C8899E4 (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = 0;
		V_0 = L_0;
		__this->set_windowEnd_1(L_0);
		int32_t L_1 = V_0;
		__this->set_windowFilled_2(L_1);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputWindow__ctor_mA9477B707FFBACD81303FAF7FE2F7C658990FF30 (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32768));
		__this->set_window_0(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer__ctor_mD43BCBDDD48F118DE0DA5A277D973B569C0D6041 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, int32_t ___bufferSize0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___bufferSize0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_0);
		__this->set_buffer_0(L_1);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_Reset_mC6B583113FB3D8C9D12FA178435E7CAA2E1327BB (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = 0;
		V_0 = L_0;
		__this->set_bitCount_4(L_0);
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->set_end_2(L_2);
		int32_t L_3 = V_0;
		__this->set_start_1(L_3);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteShort(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_WriteShort_mFD2AE37F8EC3E199F8E16E360B785246C287552B (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_buffer_0();
		int32_t L_1 = __this->get_end_2();
		V_0 = L_1;
		int32_t L_2 = V_0;
		__this->set_end_2(((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1)));
		int32_t L_3 = V_0;
		int32_t L_4 = ___value0;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)((int32_t)((uint8_t)L_4)));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = __this->get_buffer_0();
		int32_t L_6 = __this->get_end_2();
		V_0 = L_6;
		int32_t L_7 = V_0;
		__this->set_end_2(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1)));
		int32_t L_8 = V_0;
		int32_t L_9 = ___value0;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (uint8_t)((int32_t)((uint8_t)((int32_t)((int32_t)L_9>>(int32_t)8)))));
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteBlock(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_WriteBlock_m139A76BB21F2AA07C21B3B701F651A7B24903840 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___block0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___block0;
		int32_t L_1 = ___offset1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = __this->get_buffer_0();
		int32_t L_3 = __this->get_end_2();
		int32_t L_4 = ___length2;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_0, L_1, (RuntimeArray *)(RuntimeArray *)L_2, L_3, L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_end_2();
		int32_t L_6 = ___length2;
		__this->set_end_2(((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_6)));
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::get_BitCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PendingBuffer_get_BitCount_mB4A917AF510467CF020B8E173785361118CF9F41 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_bitCount_4();
		return L_0;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::AlignToByte()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_AlignToByte_m17AE6E041B765E2F686F17560EE7D22E763271D4 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_bitCount_4();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = __this->get_buffer_0();
		int32_t L_2 = __this->get_end_2();
		V_0 = L_2;
		int32_t L_3 = V_0;
		__this->set_end_2(((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
		int32_t L_4 = V_0;
		uint32_t L_5 = __this->get_bits_3();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (uint8_t)((int32_t)((uint8_t)L_5)));
		int32_t L_6 = __this->get_bitCount_4();
		if ((((int32_t)L_6) <= ((int32_t)8)))
		{
			goto IL_0052;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = __this->get_buffer_0();
		int32_t L_8 = __this->get_end_2();
		V_0 = L_8;
		int32_t L_9 = V_0;
		__this->set_end_2(((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)));
		int32_t L_10 = V_0;
		uint32_t L_11 = __this->get_bits_3();
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (uint8_t)((int32_t)((uint8_t)((int32_t)((uint32_t)L_11>>8)))));
	}

IL_0052:
	{
		__this->set_bits_3(0);
		__this->set_bitCount_4(0);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteBits(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, int32_t ___b0, int32_t ___count1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		uint32_t L_0 = __this->get_bits_3();
		int32_t L_1 = ___b0;
		int32_t L_2 = __this->get_bitCount_4();
		__this->set_bits_3(((int32_t)((int32_t)L_0|(int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)31))))))));
		int32_t L_3 = __this->get_bitCount_4();
		int32_t L_4 = ___count1;
		__this->set_bitCount_4(((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)L_4)));
		int32_t L_5 = __this->get_bitCount_4();
		if ((((int32_t)L_5) < ((int32_t)((int32_t)16))))
		{
			goto IL_008e;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = __this->get_buffer_0();
		int32_t L_7 = __this->get_end_2();
		V_0 = L_7;
		int32_t L_8 = V_0;
		__this->set_end_2(((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1)));
		int32_t L_9 = V_0;
		uint32_t L_10 = __this->get_bits_3();
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (uint8_t)((int32_t)((uint8_t)L_10)));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = __this->get_buffer_0();
		int32_t L_12 = __this->get_end_2();
		V_0 = L_12;
		int32_t L_13 = V_0;
		__this->set_end_2(((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1)));
		int32_t L_14 = V_0;
		uint32_t L_15 = __this->get_bits_3();
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (uint8_t)((int32_t)((uint8_t)((int32_t)((uint32_t)L_15>>8)))));
		uint32_t L_16 = __this->get_bits_3();
		__this->set_bits_3(((int32_t)((uint32_t)L_16>>((int32_t)16))));
		int32_t L_17 = __this->get_bitCount_4();
		__this->set_bitCount_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)((int32_t)16))));
	}

IL_008e:
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteShortMSB(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PendingBuffer_WriteShortMSB_m7597C12105E7CEE947A8CFDE511DD1CB805B3CD9 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, int32_t ___s0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_buffer_0();
		int32_t L_1 = __this->get_end_2();
		V_0 = L_1;
		int32_t L_2 = V_0;
		__this->set_end_2(((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1)));
		int32_t L_3 = V_0;
		int32_t L_4 = ___s0;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)((int32_t)((uint8_t)((int32_t)((int32_t)L_4>>(int32_t)8)))));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = __this->get_buffer_0();
		int32_t L_6 = __this->get_end_2();
		V_0 = L_6;
		int32_t L_7 = V_0;
		__this->set_end_2(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1)));
		int32_t L_8 = V_0;
		int32_t L_9 = ___s0;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (uint8_t)((int32_t)((uint8_t)L_9)));
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.PendingBuffer::get_IsFlushed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PendingBuffer_get_IsFlushed_m64096BA3F7B56951266958E6233A77C74844D569 (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_end_2();
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::Flush(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PendingBuffer_Flush_m828B34DC184E3CFC8595E381C70B0068CBD5CDFA (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___output0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_bitCount_4();
		if ((((int32_t)L_0) < ((int32_t)8)))
		{
			goto IL_0044;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = __this->get_buffer_0();
		int32_t L_2 = __this->get_end_2();
		V_0 = L_2;
		int32_t L_3 = V_0;
		__this->set_end_2(((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
		int32_t L_4 = V_0;
		uint32_t L_5 = __this->get_bits_3();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (uint8_t)((int32_t)((uint8_t)L_5)));
		uint32_t L_6 = __this->get_bits_3();
		__this->set_bits_3(((int32_t)((uint32_t)L_6>>8)));
		int32_t L_7 = __this->get_bitCount_4();
		__this->set_bitCount_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)8)));
	}

IL_0044:
	{
		int32_t L_8 = ___length2;
		int32_t L_9 = __this->get_end_2();
		int32_t L_10 = __this->get_start_1();
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)L_10)))))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_11 = __this->get_end_2();
		int32_t L_12 = __this->get_start_1();
		___length2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)L_12));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_13 = __this->get_buffer_0();
		int32_t L_14 = __this->get_start_1();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_15 = ___output0;
		int32_t L_16 = ___offset1;
		int32_t L_17 = ___length2;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_13, L_14, (RuntimeArray *)(RuntimeArray *)L_15, L_16, L_17, /*hidden argument*/NULL);
		__this->set_start_1(0);
		__this->set_end_2(0);
		goto IL_00a9;
	}

IL_0087:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18 = __this->get_buffer_0();
		int32_t L_19 = __this->get_start_1();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_20 = ___output0;
		int32_t L_21 = ___offset1;
		int32_t L_22 = ___length2;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_18, L_19, (RuntimeArray *)(RuntimeArray *)L_20, L_21, L_22, /*hidden argument*/NULL);
		int32_t L_23 = __this->get_start_1();
		int32_t L_24 = ___length2;
		__this->set_start_1(((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)L_24)));
	}

IL_00a9:
	{
		int32_t L_25 = ___length2;
		return L_25;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SharpZipBaseException__ctor_mEE9B481134AA334BBF8777D11D8001825E16A988 (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m0E9BEC861F6DBED197960E5BA23149543B1D7F5B(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914 (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SharpZipBaseException__ctor_m8A2F854EAC4E1806C22166372CE8BD8A7DA0502D (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Exception_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_0 = ___info0;
		StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  L_1 = ___context1;
		IL2CPP_RUNTIME_CLASS_INIT(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m0CD24092BF55B8EDE25AED989ACADB80298EF917(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamDecodingException__ctor_m4F10EB31C40C7AC98DF71DCA43F9D59F7ABDBFED (StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDEAAC5E3A77D04DD8512744622F3D88ACFA2767D);
		s_Il2CppMethodInitialized = true;
	}
	{
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(__this, _stringLiteralDEAAC5E3A77D04DD8512744622F3D88ACFA2767D, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamDecodingException__ctor_mE34C08B6F04E109D9A4E0B2DA3DB7C67555C5CE1 (StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamDecodingException__ctor_m402438841B691D097F1B428835488DB9CF0D4826 (StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method)
{
	{
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_0 = ___info0;
		StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  L_1 = ___context1;
		SharpZipBaseException__ctor_m8A2F854EAC4E1806C22166372CE8BD8A7DA0502D(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::PeekBits(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, int32_t ___bitCount0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_bitsInBuffer__4();
		int32_t L_1 = ___bitCount0;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_2 = __this->get_windowStart__1();
		int32_t L_3 = __this->get_windowEnd__2();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0019;
		}
	}
	{
		return (-1);
	}

IL_0019:
	{
		uint32_t L_4 = __this->get_buffer__3();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = __this->get_window__0();
		int32_t L_6 = __this->get_windowStart__1();
		V_0 = L_6;
		int32_t L_7 = V_0;
		__this->set_windowStart__1(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		uint8_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = __this->get_window__0();
		int32_t L_12 = __this->get_windowStart__1();
		V_0 = L_12;
		int32_t L_13 = V_0;
		__this->set_windowStart__1(((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1)));
		int32_t L_14 = V_0;
		NullCheck(L_11);
		int32_t L_15 = L_14;
		uint8_t L_16 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		int32_t L_17 = __this->get_bitsInBuffer__4();
		__this->set_buffer__3(((int32_t)((int32_t)L_4|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)255)))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)255)))<<(int32_t)8))))<<(int32_t)((int32_t)((int32_t)L_17&(int32_t)((int32_t)31))))))));
		int32_t L_18 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)((int32_t)16))));
	}

IL_007e:
	{
		uint32_t L_19 = __this->get_buffer__3();
		int32_t L_20 = ___bitCount0;
		return ((int32_t)((int32_t)((int64_t)((int64_t)((int64_t)((uint64_t)L_19))&(int64_t)((int64_t)((int64_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_20&(int32_t)((int32_t)31))))), (int32_t)1))))))));
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::TryGetBits(System.Int32,System.Int32&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StreamManipulator_TryGetBits_mC4C8072E3A5991B94049F9C3F9382E2D5B0B9D96 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, int32_t ___bitCount0, int32_t* ___output1, int32_t ___outputOffset2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___bitCount0;
		int32_t L_1;
		L_1 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		int32_t* L_3 = ___output1;
		int32_t L_4 = V_0;
		int32_t L_5 = ___outputOffset2;
		*((int32_t*)L_3) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		int32_t L_6 = ___bitCount0;
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(__this, L_6, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::TryGetBits(System.Int32,System.Byte[]&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StreamManipulator_TryGetBits_mEB43579CCD99015132323748F193722297150632 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, int32_t ___bitCount0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** ___array1, int32_t ___index2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___bitCount0;
		int32_t L_1;
		L_1 = StreamManipulator_PeekBits_m63B6C79F072DAC8D1908BE7979A30140591122E6(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** L_3 = ___array1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = *((ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726**)L_3);
		int32_t L_5 = ___index2;
		int32_t L_6 = V_0;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (uint8_t)((int32_t)((uint8_t)L_6)));
		int32_t L_7 = ___bitCount0;
		StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1(__this, L_7, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::DropBits(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamManipulator_DropBits_m9E47BB767695955BAB75027BE0D5158E0EA978A1 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, int32_t ___bitCount0, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = __this->get_buffer__3();
		int32_t L_1 = ___bitCount0;
		__this->set_buffer__3(((int32_t)((uint32_t)L_0>>((int32_t)((int32_t)L_1&(int32_t)((int32_t)31))))));
		int32_t L_2 = __this->get_bitsInBuffer__4();
		int32_t L_3 = ___bitCount0;
		__this->set_bitsInBuffer__4(((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)L_3)));
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBits()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StreamManipulator_get_AvailableBits_m7FA2836285F2C2BA6B74A8AA135D770DB9030A7B (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_bitsInBuffer__4();
		return L_0;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBytes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StreamManipulator_get_AvailableBytes_m4993F192D46EF3A516A6E364BDE5E952D8281457 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_windowEnd__2();
		int32_t L_1 = __this->get_windowStart__1();
		int32_t L_2 = __this->get_bitsInBuffer__4();
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1)), (int32_t)((int32_t)((int32_t)L_2>>(int32_t)3))));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SkipToByteBoundary()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamManipulator_SkipToByteBoundary_m7C36C130332D5995F8938FFC6F8A45478B77A47C (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = __this->get_buffer__3();
		int32_t L_1 = __this->get_bitsInBuffer__4();
		__this->set_buffer__3(((int32_t)((uint32_t)L_0>>((int32_t)((int32_t)((int32_t)((int32_t)L_1&(int32_t)7))&(int32_t)((int32_t)31))))));
		int32_t L_2 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_2&(int32_t)((int32_t)-8))));
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_IsNeedingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool StreamManipulator_get_IsNeedingInput_m962D2BBB56BD8BAC834E2A12897C13A7D4DCE456 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_windowStart__1();
		int32_t L_1 = __this->get_windowEnd__2();
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::CopyBytes(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StreamManipulator_CopyBytes_m6FA30492A0A8534309A1538CF230500658CA3C79 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___output0, int32_t ___offset1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___length2;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1;
		L_1 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___length2), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_2 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamManipulator_CopyBytes_m6FA30492A0A8534309A1538CF230500658CA3C79_RuntimeMethod_var)));
	}

IL_0011:
	{
		int32_t L_3 = __this->get_bitsInBuffer__4();
		if (!((int32_t)((int32_t)L_3&(int32_t)7)))
		{
			goto IL_0026;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_4 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_4, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral36919DB41559814DAA1900952B7D08BD1CA0C036)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamManipulator_CopyBytes_m6FA30492A0A8534309A1538CF230500658CA3C79_RuntimeMethod_var)));
	}

IL_0026:
	{
		V_0 = 0;
		goto IL_005e;
	}

IL_002a:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = ___output0;
		int32_t L_6 = ___offset1;
		int32_t L_7 = L_6;
		___offset1 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
		uint32_t L_8 = __this->get_buffer__3();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (uint8_t)((int32_t)((uint8_t)L_8)));
		uint32_t L_9 = __this->get_buffer__3();
		__this->set_buffer__3(((int32_t)((uint32_t)L_9>>8)));
		int32_t L_10 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)il2cpp_codegen_subtract((int32_t)L_10, (int32_t)8)));
		int32_t L_11 = ___length2;
		___length2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)1));
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_005e:
	{
		int32_t L_13 = __this->get_bitsInBuffer__4();
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_14 = ___length2;
		if ((((int32_t)L_14) > ((int32_t)0)))
		{
			goto IL_002a;
		}
	}

IL_006b:
	{
		int32_t L_15 = ___length2;
		if (L_15)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_16 = V_0;
		return L_16;
	}

IL_0070:
	{
		int32_t L_17 = __this->get_windowEnd__2();
		int32_t L_18 = __this->get_windowStart__1();
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)L_18));
		int32_t L_19 = ___length2;
		int32_t L_20 = V_1;
		if ((((int32_t)L_19) <= ((int32_t)L_20)))
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_21 = V_1;
		___length2 = L_21;
	}

IL_0085:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_22 = __this->get_window__0();
		int32_t L_23 = __this->get_windowStart__1();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_24 = ___output0;
		int32_t L_25 = ___offset1;
		int32_t L_26 = ___length2;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_22, L_23, (RuntimeArray *)(RuntimeArray *)L_24, L_25, L_26, /*hidden argument*/NULL);
		int32_t L_27 = __this->get_windowStart__1();
		int32_t L_28 = ___length2;
		__this->set_windowStart__1(((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)L_28)));
		int32_t L_29 = __this->get_windowStart__1();
		int32_t L_30 = __this->get_windowEnd__2();
		if (!((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_29, (int32_t)L_30))&(int32_t)1)))
		{
			goto IL_00e3;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_31 = __this->get_window__0();
		int32_t L_32 = __this->get_windowStart__1();
		V_2 = L_32;
		int32_t L_33 = V_2;
		__this->set_windowStart__1(((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1)));
		int32_t L_34 = V_2;
		NullCheck(L_31);
		int32_t L_35 = L_34;
		uint8_t L_36 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		__this->set_buffer__3(((int32_t)((int32_t)L_36&(int32_t)((int32_t)255))));
		__this->set_bitsInBuffer__4(8);
	}

IL_00e3:
	{
		int32_t L_37 = V_0;
		int32_t L_38 = ___length2;
		return ((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)L_38));
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamManipulator_Reset_m7379C771C7C78B44A155BAD54952808BD4F0B5EA (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		__this->set_buffer__3(0);
		int32_t L_0 = 0;
		V_0 = L_0;
		__this->set_bitsInBuffer__4(L_0);
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->set_windowEnd__2(L_2);
		int32_t L_3 = V_0;
		__this->set_windowStart__1(L_3);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SetInput(System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___buffer0, int32_t ___offset1, int32_t ___count2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___buffer0;
		NullCheck((RuntimeObject *)(RuntimeObject *)L_1);
		String_t* L_2;
		L_2 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (RuntimeObject *)(RuntimeObject *)L_1);
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_3 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var)));
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_3, L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A_RuntimeMethod_var)));
	}

IL_000f:
	{
		int32_t L_4 = ___offset1;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_5;
		L_5 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___offset1), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_6 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005(L_6, L_5, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralF0A45CCAC3B8CC663DC29BD756A86295833579C1)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A_RuntimeMethod_var)));
	}

IL_0025:
	{
		int32_t L_7 = ___count2;
		if ((((int32_t)L_7) >= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_8;
		L_8 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___count2), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_9 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005(L_9, L_8, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralF0A45CCAC3B8CC663DC29BD756A86295833579C1)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A_RuntimeMethod_var)));
	}

IL_003b:
	{
		int32_t L_10 = __this->get_windowStart__1();
		int32_t L_11 = __this->get_windowEnd__2();
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_0054;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_12 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var)));
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_12, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral30D44D3EB7B534CAF51AD514F18FE7DD626129FE)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A_RuntimeMethod_var)));
	}

IL_0054:
	{
		int32_t L_13 = ___offset1;
		int32_t L_14 = ___count2;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_14));
		int32_t L_15 = ___offset1;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) > ((int32_t)L_16)))
		{
			goto IL_0062;
		}
	}
	{
		int32_t L_17 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18 = ___buffer0;
		NullCheck(L_18);
		if ((((int32_t)L_17) <= ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))))))
		{
			goto IL_006f;
		}
	}

IL_0062:
	{
		String_t* L_19;
		L_19 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___count2), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_20 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_m329C2882A4CB69F185E98D0DD7E853AA9220960A(L_20, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamManipulator_SetInput_m5E6C92E2A564017A46D870D175677FA39D8F655A_RuntimeMethod_var)));
	}

IL_006f:
	{
		int32_t L_21 = ___count2;
		if (!((int32_t)((int32_t)L_21&(int32_t)1)))
		{
			goto IL_00a7;
		}
	}
	{
		uint32_t L_22 = __this->get_buffer__3();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_23 = ___buffer0;
		int32_t L_24 = ___offset1;
		int32_t L_25 = L_24;
		___offset1 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
		NullCheck(L_23);
		int32_t L_26 = L_25;
		uint8_t L_27 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		int32_t L_28 = __this->get_bitsInBuffer__4();
		__this->set_buffer__3(((int32_t)((int32_t)L_22|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27&(int32_t)((int32_t)255)))<<(int32_t)((int32_t)((int32_t)L_28&(int32_t)((int32_t)31))))))));
		int32_t L_29 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)8)));
	}

IL_00a7:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_30 = ___buffer0;
		__this->set_window__0(L_30);
		int32_t L_31 = ___offset1;
		__this->set_windowStart__1(L_31);
		int32_t L_32 = V_0;
		__this->set_windowEnd__2(L_32);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StreamManipulator__ctor_mC16675A2CEAB3EE9F281DC7F5B59D4DF5DC77069 (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.ValueOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueOutOfRangeException__ctor_mEA093740F1E941798A6138627AEF47268840D542 (ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07 * __this, String_t* ___nameOfValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4B80EC9460A3D933B23C9CCFF5E4011E47D0FF85);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___nameOfValue0;
		String_t* L_1;
		L_1 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_0, _stringLiteral4B80EC9460A3D933B23C9CCFF5E4011E47D0FF85, /*hidden argument*/NULL);
		StreamDecodingException__ctor_mE34C08B6F04E109D9A4E0B2DA3DB7C67555C5CE1(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.ValueOutOfRangeException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueOutOfRangeException__ctor_m92C0CD2C12020AC04BEDA8C8CDEAFAF675B0F5D2 (ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07 * __this, const RuntimeMethod* method)
{
	{
		StreamDecodingException__ctor_m4F10EB31C40C7AC98DF71DCA43F9D59F7ABDBFED(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.ValueOutOfRangeException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValueOutOfRangeException__ctor_m8B9E2FE21F5C7D6E51F5FD3C15DA0747FA527C96 (ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07 * __this, SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ___info0, StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  ___context1, const RuntimeMethod* method)
{
	{
		SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * L_0 = ___info0;
		StreamingContext_t5888E7E8C81AB6EF3B14FDDA6674F458076A8505  L_1 = ___context1;
		StreamDecodingException__ctor_m402438841B691D097F1B428835488DB9CF0D4826(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree__ctor_mC175FAE853657848BE82C5A0C72FC6418B9C81B3 (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * ___dh0, int32_t ___elems1, int32_t ___minCodes2, int32_t ___maxLength3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_0 = ___dh0;
		__this->set_dh_7(L_0);
		int32_t L_1 = ___minCodes2;
		__this->set_minNumCodes_2(L_1);
		int32_t L_2 = ___maxLength3;
		__this->set_maxLength_6(L_2);
		int32_t L_3 = ___elems1;
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_4 = (Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)SZArrayNew(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var, (uint32_t)L_3);
		__this->set_freqs_0(L_4);
		int32_t L_5 = ___maxLength3;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_6 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)L_5);
		__this->set_bl_counts_5(L_6);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_Reset_m88D2AC4D14EE34A72036C8440EE3ADFCDAB75698 (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0011;
	}

IL_0004:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_0 = __this->get_freqs_0();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int16_t)0);
		int32_t L_2 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1));
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_4 = __this->get_freqs_0();
		NullCheck(L_4);
		if ((((int32_t)L_3) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		__this->set_codes_4((Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)NULL);
		__this->set_length_1((ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteSymbol(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, int32_t ___code0, const RuntimeMethod* method)
{
	{
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_0 = __this->get_dh_7();
		NullCheck(L_0);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_1 = L_0->get_pending_6();
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_2 = __this->get_codes_4();
		int32_t L_3 = ___code0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int16_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = __this->get_length_1();
		int32_t L_7 = ___code0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		uint8_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_1);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_1, ((int32_t)((int32_t)L_5&(int32_t)((int32_t)65535))), L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::SetStaticCodes(System.Int16[],System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_SetStaticCodes_m0527B8E8E0520CA78EF84CB97DA604434F6C63EA (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* ___staticCodes0, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___staticLengths1, const RuntimeMethod* method)
{
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_0 = ___staticCodes0;
		__this->set_codes_4(L_0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = ___staticLengths1;
		__this->set_length_1(L_1);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildCodes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_BuildCodes_mA2357168306F9D5F06255D9ED89B1C646039FBDC (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_0 = __this->get_freqs_0();
		NullCheck(L_0);
		int32_t L_1 = __this->get_maxLength_6();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)L_1);
		V_0 = L_2;
		V_1 = 0;
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_3 = __this->get_freqs_0();
		NullCheck(L_3);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_4 = (Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD*)SZArrayNew(Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_3)->max_length))));
		__this->set_codes_4(L_4);
		V_2 = 0;
		goto IL_0049;
	}

IL_002e:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_5 = V_0;
		int32_t L_6 = V_2;
		int32_t L_7 = V_1;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (int32_t)L_7);
		int32_t L_8 = V_1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_9 = __this->get_bl_counts_5();
		int32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		int32_t L_13 = V_2;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)((int32_t)((int32_t)L_12<<(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)15), (int32_t)L_13))&(int32_t)((int32_t)31)))))));
		int32_t L_14 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0049:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = __this->get_maxLength_6();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_002e;
		}
	}
	{
		V_3 = 0;
		goto IL_0094;
	}

IL_0056:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_17 = __this->get_length_1();
		int32_t L_18 = V_3;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		uint8_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		V_4 = L_20;
		int32_t L_21 = V_4;
		if ((((int32_t)L_21) <= ((int32_t)0)))
		{
			goto IL_0090;
		}
	}
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_22 = __this->get_codes_4();
		int32_t L_23 = V_3;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_24 = V_0;
		int32_t L_25 = V_4;
		NullCheck(L_24);
		int32_t L_26 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_25, (int32_t)1));
		int32_t L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5_il2cpp_TypeInfo_var);
		int16_t L_28;
		L_28 = DeflaterHuffman_BitReverse_mDCB3571E59334E12433EB62A58989067C0E23E9B(L_27, /*hidden argument*/NULL);
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(L_23), (int16_t)L_28);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_29 = V_0;
		int32_t L_30 = V_4;
		NullCheck(L_29);
		int32_t* L_31 = ((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_30, (int32_t)1)))));
		int32_t L_32 = *((int32_t*)L_31);
		int32_t L_33 = V_4;
		*((int32_t*)L_31) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)16), (int32_t)L_33))&(int32_t)((int32_t)31)))))));
	}

IL_0090:
	{
		int32_t L_34 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
	}

IL_0094:
	{
		int32_t L_35 = V_3;
		int32_t L_36 = __this->get_numCodes_3();
		if ((((int32_t)L_35) < ((int32_t)L_36)))
		{
			goto IL_0056;
		}
	}
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildTree()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_BuildTree_m5FF35C4C61B273025E9B99F2B6C17AFDF1C917E4 (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* V_4 = NULL;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* V_5 = NULL;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t G_B13_0 = 0;
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_0 = __this->get_freqs_0();
		NullCheck(L_0);
		V_0 = ((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length)));
		int32_t L_1 = V_0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)L_1);
		V_1 = L_2;
		V_2 = 0;
		V_3 = 0;
		V_7 = 0;
		goto IL_0067;
	}

IL_0019:
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_3 = __this->get_freqs_0();
		int32_t L_4 = V_7;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		int16_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_8 = L_6;
		int32_t L_7 = V_8;
		if (!L_7)
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_8 = V_2;
		int32_t L_9 = L_8;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
		V_9 = L_9;
		goto IL_003d;
	}

IL_0031:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_10 = V_1;
		int32_t L_11 = V_9;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_12 = V_1;
		int32_t L_13 = V_10;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (int32_t)L_15);
		int32_t L_16 = V_10;
		V_9 = L_16;
	}

IL_003d:
	{
		int32_t L_17 = V_9;
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_0058;
		}
	}
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_18 = __this->get_freqs_0();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_19 = V_1;
		int32_t L_20 = V_9;
		int32_t L_21 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_20, (int32_t)1))/(int32_t)2));
		V_10 = L_21;
		NullCheck(L_19);
		int32_t L_22 = L_21;
		int32_t L_23 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_18);
		int32_t L_24 = L_23;
		int16_t L_25 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		int32_t L_26 = V_8;
		if ((((int32_t)L_25) > ((int32_t)L_26)))
		{
			goto IL_0031;
		}
	}

IL_0058:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_27 = V_1;
		int32_t L_28 = V_9;
		int32_t L_29 = V_7;
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (int32_t)L_29);
		int32_t L_30 = V_7;
		V_3 = L_30;
	}

IL_0061:
	{
		int32_t L_31 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
	}

IL_0067:
	{
		int32_t L_32 = V_7;
		int32_t L_33 = V_0;
		if ((((int32_t)L_32) < ((int32_t)L_33)))
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0085;
	}

IL_006e:
	{
		int32_t L_34 = V_3;
		if ((((int32_t)L_34) < ((int32_t)2)))
		{
			goto IL_0075;
		}
	}
	{
		G_B13_0 = 0;
		goto IL_007a;
	}

IL_0075:
	{
		int32_t L_35 = V_3;
		int32_t L_36 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
		V_3 = L_36;
		G_B13_0 = L_36;
	}

IL_007a:
	{
		V_11 = G_B13_0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_37 = V_1;
		int32_t L_38 = V_2;
		int32_t L_39 = L_38;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)1));
		int32_t L_40 = V_11;
		NullCheck(L_37);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(L_39), (int32_t)L_40);
	}

IL_0085:
	{
		int32_t L_41 = V_2;
		if ((((int32_t)L_41) < ((int32_t)2)))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_42 = V_3;
		int32_t L_43 = __this->get_minNumCodes_2();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_44;
		L_44 = Math_Max_mD8AA27386BF012C65303FCDEA041B0CC65056E7B(((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1)), L_43, /*hidden argument*/NULL);
		__this->set_numCodes_3(L_44);
		int32_t L_45 = V_2;
		int32_t L_46 = V_2;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_47 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)4, (int32_t)L_46)), (int32_t)2)));
		V_4 = L_47;
		int32_t L_48 = V_2;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_49 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_48)), (int32_t)1)));
		V_5 = L_49;
		V_6 = L_45;
		V_12 = 0;
		goto IL_00f2;
	}

IL_00bd:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_50 = V_1;
		int32_t L_51 = V_12;
		NullCheck(L_50);
		int32_t L_52 = L_51;
		int32_t L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		V_13 = L_53;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_54 = V_4;
		int32_t L_55 = V_12;
		int32_t L_56 = V_13;
		NullCheck(L_54);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_55))), (int32_t)L_56);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_57 = V_4;
		int32_t L_58 = V_12;
		NullCheck(L_57);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_58)), (int32_t)1))), (int32_t)(-1));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_59 = V_5;
		int32_t L_60 = V_12;
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_61 = __this->get_freqs_0();
		int32_t L_62 = V_13;
		NullCheck(L_61);
		int32_t L_63 = L_62;
		int16_t L_64 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		NullCheck(L_59);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(L_60), (int32_t)((int32_t)((int32_t)L_64<<(int32_t)8)));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_65 = V_1;
		int32_t L_66 = V_12;
		int32_t L_67 = V_12;
		NullCheck(L_65);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(L_66), (int32_t)L_67);
		int32_t L_68 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_68, (int32_t)1));
	}

IL_00f2:
	{
		int32_t L_69 = V_12;
		int32_t L_70 = V_2;
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_00bd;
		}
	}

IL_00f7:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_71 = V_1;
		NullCheck(L_71);
		int32_t L_72 = 0;
		int32_t L_73 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		V_14 = L_73;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_74 = V_1;
		int32_t L_75 = V_2;
		int32_t L_76 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_75, (int32_t)1));
		V_2 = L_76;
		NullCheck(L_74);
		int32_t L_77 = L_76;
		int32_t L_78 = (L_74)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		V_15 = L_78;
		V_16 = 0;
		V_17 = 1;
		goto IL_0140;
	}

IL_010d:
	{
		int32_t L_79 = V_17;
		int32_t L_80 = V_2;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_79, (int32_t)1))) >= ((int32_t)L_80)))
		{
			goto IL_012c;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_81 = V_5;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_82 = V_1;
		int32_t L_83 = V_17;
		NullCheck(L_82);
		int32_t L_84 = L_83;
		int32_t L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		NullCheck(L_81);
		int32_t L_86 = L_85;
		int32_t L_87 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_88 = V_5;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_89 = V_1;
		int32_t L_90 = V_17;
		NullCheck(L_89);
		int32_t L_91 = ((int32_t)il2cpp_codegen_add((int32_t)L_90, (int32_t)1));
		int32_t L_92 = (L_89)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		NullCheck(L_88);
		int32_t L_93 = L_92;
		int32_t L_94 = (L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		if ((((int32_t)L_87) <= ((int32_t)L_94)))
		{
			goto IL_012c;
		}
	}
	{
		int32_t L_95 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add((int32_t)L_95, (int32_t)1));
	}

IL_012c:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_96 = V_1;
		int32_t L_97 = V_16;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_98 = V_1;
		int32_t L_99 = V_17;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		int32_t L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		NullCheck(L_96);
		(L_96)->SetAt(static_cast<il2cpp_array_size_t>(L_97), (int32_t)L_101);
		int32_t L_102 = V_17;
		V_16 = L_102;
		int32_t L_103 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_103, (int32_t)2)), (int32_t)1));
	}

IL_0140:
	{
		int32_t L_104 = V_17;
		int32_t L_105 = V_2;
		if ((((int32_t)L_104) < ((int32_t)L_105)))
		{
			goto IL_010d;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_106 = V_5;
		int32_t L_107 = V_15;
		NullCheck(L_106);
		int32_t L_108 = L_107;
		int32_t L_109 = (L_106)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		V_18 = L_109;
		goto IL_0156;
	}

IL_014e:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_110 = V_1;
		int32_t L_111 = V_17;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_112 = V_1;
		int32_t L_113 = V_16;
		NullCheck(L_112);
		int32_t L_114 = L_113;
		int32_t L_115 = (L_112)->GetAt(static_cast<il2cpp_array_size_t>(L_114));
		NullCheck(L_110);
		(L_110)->SetAt(static_cast<il2cpp_array_size_t>(L_111), (int32_t)L_115);
	}

IL_0156:
	{
		int32_t L_116 = V_16;
		int32_t L_117 = L_116;
		V_17 = L_117;
		if ((((int32_t)L_117) <= ((int32_t)0)))
		{
			goto IL_0170;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_118 = V_5;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_119 = V_1;
		int32_t L_120 = V_17;
		int32_t L_121 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_120, (int32_t)1))/(int32_t)2));
		V_16 = L_121;
		NullCheck(L_119);
		int32_t L_122 = L_121;
		int32_t L_123 = (L_119)->GetAt(static_cast<il2cpp_array_size_t>(L_122));
		NullCheck(L_118);
		int32_t L_124 = L_123;
		int32_t L_125 = (L_118)->GetAt(static_cast<il2cpp_array_size_t>(L_124));
		int32_t L_126 = V_18;
		if ((((int32_t)L_125) > ((int32_t)L_126)))
		{
			goto IL_014e;
		}
	}

IL_0170:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_127 = V_1;
		int32_t L_128 = V_17;
		int32_t L_129 = V_15;
		NullCheck(L_127);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(L_128), (int32_t)L_129);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_130 = V_1;
		NullCheck(L_130);
		int32_t L_131 = 0;
		int32_t L_132 = (L_130)->GetAt(static_cast<il2cpp_array_size_t>(L_131));
		V_19 = L_132;
		int32_t L_133 = V_6;
		int32_t L_134 = L_133;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_134, (int32_t)1));
		V_15 = L_134;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_135 = V_4;
		int32_t L_136 = V_15;
		int32_t L_137 = V_14;
		NullCheck(L_135);
		(L_135)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_136))), (int32_t)L_137);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_138 = V_4;
		int32_t L_139 = V_15;
		int32_t L_140 = V_19;
		NullCheck(L_138);
		(L_138)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_139)), (int32_t)1))), (int32_t)L_140);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_141 = V_5;
		int32_t L_142 = V_14;
		NullCheck(L_141);
		int32_t L_143 = L_142;
		int32_t L_144 = (L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_143));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_145 = V_5;
		int32_t L_146 = V_19;
		NullCheck(L_145);
		int32_t L_147 = L_146;
		int32_t L_148 = (L_145)->GetAt(static_cast<il2cpp_array_size_t>(L_147));
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_149;
		L_149 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(((int32_t)((int32_t)L_144&(int32_t)((int32_t)255))), ((int32_t)((int32_t)L_148&(int32_t)((int32_t)255))), /*hidden argument*/NULL);
		V_20 = L_149;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_150 = V_5;
		int32_t L_151 = V_15;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_152 = V_5;
		int32_t L_153 = V_14;
		NullCheck(L_152);
		int32_t L_154 = L_153;
		int32_t L_155 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_156 = V_5;
		int32_t L_157 = V_19;
		NullCheck(L_156);
		int32_t L_158 = L_157;
		int32_t L_159 = (L_156)->GetAt(static_cast<il2cpp_array_size_t>(L_158));
		int32_t L_160 = V_20;
		int32_t L_161 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_155, (int32_t)L_159)), (int32_t)L_160)), (int32_t)1));
		V_18 = L_161;
		NullCheck(L_150);
		(L_150)->SetAt(static_cast<il2cpp_array_size_t>(L_151), (int32_t)L_161);
		V_16 = 0;
		V_17 = 1;
		goto IL_0208;
	}

IL_01d5:
	{
		int32_t L_162 = V_17;
		int32_t L_163 = V_2;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_162, (int32_t)1))) >= ((int32_t)L_163)))
		{
			goto IL_01f4;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_164 = V_5;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_165 = V_1;
		int32_t L_166 = V_17;
		NullCheck(L_165);
		int32_t L_167 = L_166;
		int32_t L_168 = (L_165)->GetAt(static_cast<il2cpp_array_size_t>(L_167));
		NullCheck(L_164);
		int32_t L_169 = L_168;
		int32_t L_170 = (L_164)->GetAt(static_cast<il2cpp_array_size_t>(L_169));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_171 = V_5;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_172 = V_1;
		int32_t L_173 = V_17;
		NullCheck(L_172);
		int32_t L_174 = ((int32_t)il2cpp_codegen_add((int32_t)L_173, (int32_t)1));
		int32_t L_175 = (L_172)->GetAt(static_cast<il2cpp_array_size_t>(L_174));
		NullCheck(L_171);
		int32_t L_176 = L_175;
		int32_t L_177 = (L_171)->GetAt(static_cast<il2cpp_array_size_t>(L_176));
		if ((((int32_t)L_170) <= ((int32_t)L_177)))
		{
			goto IL_01f4;
		}
	}
	{
		int32_t L_178 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add((int32_t)L_178, (int32_t)1));
	}

IL_01f4:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_179 = V_1;
		int32_t L_180 = V_16;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_181 = V_1;
		int32_t L_182 = V_17;
		NullCheck(L_181);
		int32_t L_183 = L_182;
		int32_t L_184 = (L_181)->GetAt(static_cast<il2cpp_array_size_t>(L_183));
		NullCheck(L_179);
		(L_179)->SetAt(static_cast<il2cpp_array_size_t>(L_180), (int32_t)L_184);
		int32_t L_185 = V_17;
		V_16 = L_185;
		int32_t L_186 = V_16;
		V_17 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_186, (int32_t)2)), (int32_t)1));
	}

IL_0208:
	{
		int32_t L_187 = V_17;
		int32_t L_188 = V_2;
		if ((((int32_t)L_187) < ((int32_t)L_188)))
		{
			goto IL_01d5;
		}
	}
	{
		goto IL_0217;
	}

IL_020f:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_189 = V_1;
		int32_t L_190 = V_17;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_191 = V_1;
		int32_t L_192 = V_16;
		NullCheck(L_191);
		int32_t L_193 = L_192;
		int32_t L_194 = (L_191)->GetAt(static_cast<il2cpp_array_size_t>(L_193));
		NullCheck(L_189);
		(L_189)->SetAt(static_cast<il2cpp_array_size_t>(L_190), (int32_t)L_194);
	}

IL_0217:
	{
		int32_t L_195 = V_16;
		int32_t L_196 = L_195;
		V_17 = L_196;
		if ((((int32_t)L_196) <= ((int32_t)0)))
		{
			goto IL_0231;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_197 = V_5;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_198 = V_1;
		int32_t L_199 = V_17;
		int32_t L_200 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_199, (int32_t)1))/(int32_t)2));
		V_16 = L_200;
		NullCheck(L_198);
		int32_t L_201 = L_200;
		int32_t L_202 = (L_198)->GetAt(static_cast<il2cpp_array_size_t>(L_201));
		NullCheck(L_197);
		int32_t L_203 = L_202;
		int32_t L_204 = (L_197)->GetAt(static_cast<il2cpp_array_size_t>(L_203));
		int32_t L_205 = V_18;
		if ((((int32_t)L_204) > ((int32_t)L_205)))
		{
			goto IL_020f;
		}
	}

IL_0231:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_206 = V_1;
		int32_t L_207 = V_17;
		int32_t L_208 = V_15;
		NullCheck(L_206);
		(L_206)->SetAt(static_cast<il2cpp_array_size_t>(L_207), (int32_t)L_208);
		int32_t L_209 = V_2;
		if ((((int32_t)L_209) > ((int32_t)1)))
		{
			goto IL_00f7;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_210 = V_1;
		NullCheck(L_210);
		int32_t L_211 = 0;
		int32_t L_212 = (L_210)->GetAt(static_cast<il2cpp_array_size_t>(L_211));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_213 = V_4;
		NullCheck(L_213);
		if ((((int32_t)L_212) == ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_213)->max_length)))/(int32_t)2)), (int32_t)1)))))
		{
			goto IL_0256;
		}
	}
	{
		SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B * L_214 = (SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&SharpZipBaseException_tE660382205A40E1EEB17977DAF23CB76E0B36F1B_il2cpp_TypeInfo_var)));
		SharpZipBaseException__ctor_mC93C2277982A0BB93D84D08F66F94AA490A38914(L_214, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral664F1CF5298604232E38438FFBAECBE325640454)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_214, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&Tree_BuildTree_m5FF35C4C61B273025E9B99F2B6C17AFDF1C917E4_RuntimeMethod_var)));
	}

IL_0256:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_215 = V_4;
		Tree_BuildLength_m85D4045DCC50CC1D520AD0FC0C9486F28BC9F0CD(__this, L_215, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::GetEncodedLength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Tree_GetEncodedLength_m54044E0BBD9F68EFA7B3B2EE99BB131B45993997 (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_001e;
	}

IL_0006:
	{
		int32_t L_0 = V_0;
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_1 = __this->get_freqs_0();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		int32_t L_3 = L_2;
		int16_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = __this->get_length_1();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		uint8_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_4, (int32_t)L_8))));
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_001e:
	{
		int32_t L_10 = V_1;
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_11 = __this->get_freqs_0();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))))))
		{
			goto IL_0006;
		}
	}
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::CalcBLFreq(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_CalcBLFreq_m81CFB0D0DE3A835DF438203762616C9ECBA4B7AC (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * ___blTree0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_3 = (-1);
		V_4 = 0;
		goto IL_00d0;
	}

IL_000a:
	{
		V_2 = 1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_length_1();
		int32_t L_1 = V_4;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_5 = L_3;
		int32_t L_4 = V_5;
		if (L_4)
		{
			goto IL_0025;
		}
	}
	{
		V_0 = ((int32_t)138);
		V_1 = 3;
		goto IL_0043;
	}

IL_0025:
	{
		V_0 = 6;
		V_1 = 3;
		int32_t L_5 = V_3;
		int32_t L_6 = V_5;
		if ((((int32_t)L_5) == ((int32_t)L_6)))
		{
			goto IL_0043;
		}
	}
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_7 = ___blTree0;
		NullCheck(L_7);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_8 = L_7->get_freqs_0();
		int32_t L_9 = V_5;
		NullCheck(L_8);
		int16_t* L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)));
		int32_t L_11 = *((int16_t*)L_10);
		*((int16_t*)L_10) = (int16_t)((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1))));
		V_2 = 0;
	}

IL_0043:
	{
		int32_t L_12 = V_5;
		V_3 = L_12;
		int32_t L_13 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
		goto IL_005c;
	}

IL_004e:
	{
		int32_t L_14 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
		int32_t L_15 = V_2;
		int32_t L_16 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
		V_2 = L_16;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) >= ((int32_t)L_17)))
		{
			goto IL_0072;
		}
	}

IL_005c:
	{
		int32_t L_18 = V_4;
		int32_t L_19 = __this->get_numCodes_3();
		if ((((int32_t)L_18) >= ((int32_t)L_19)))
		{
			goto IL_0072;
		}
	}
	{
		int32_t L_20 = V_3;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_21 = __this->get_length_1();
		int32_t L_22 = V_4;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		uint8_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		if ((((int32_t)L_20) == ((int32_t)L_24)))
		{
			goto IL_004e;
		}
	}

IL_0072:
	{
		int32_t L_25 = V_2;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) >= ((int32_t)L_26)))
		{
			goto IL_008b;
		}
	}
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_27 = ___blTree0;
		NullCheck(L_27);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_28 = L_27->get_freqs_0();
		int32_t L_29 = V_3;
		NullCheck(L_28);
		int16_t* L_30 = ((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_29)));
		int32_t L_31 = *((int16_t*)L_30);
		int32_t L_32 = V_2;
		*((int16_t*)L_30) = (int16_t)((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)((int16_t)((int16_t)L_32))))));
		goto IL_00d0;
	}

IL_008b:
	{
		int32_t L_33 = V_3;
		if (!L_33)
		{
			goto IL_00a3;
		}
	}
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_34 = ___blTree0;
		NullCheck(L_34);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_35 = L_34->get_freqs_0();
		NullCheck(L_35);
		int16_t* L_36 = ((L_35)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)16))));
		int32_t L_37 = *((int16_t*)L_36);
		*((int16_t*)L_36) = (int16_t)((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)1))));
		goto IL_00d0;
	}

IL_00a3:
	{
		int32_t L_38 = V_2;
		if ((((int32_t)L_38) > ((int32_t)((int32_t)10))))
		{
			goto IL_00bd;
		}
	}
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_39 = ___blTree0;
		NullCheck(L_39);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_40 = L_39->get_freqs_0();
		NullCheck(L_40);
		int16_t* L_41 = ((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)17))));
		int32_t L_42 = *((int16_t*)L_41);
		*((int16_t*)L_41) = (int16_t)((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1))));
		goto IL_00d0;
	}

IL_00bd:
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_43 = ___blTree0;
		NullCheck(L_43);
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_44 = L_43->get_freqs_0();
		NullCheck(L_44);
		int16_t* L_45 = ((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)18))));
		int32_t L_46 = *((int16_t*)L_45);
		*((int16_t*)L_45) = (int16_t)((int16_t)((int16_t)((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1))));
	}

IL_00d0:
	{
		int32_t L_47 = V_4;
		int32_t L_48 = __this->get_numCodes_3();
		if ((((int32_t)L_47) < ((int32_t)L_48)))
		{
			goto IL_000a;
		}
	}
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteTree(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_WriteTree_mAE4872807EDE476D00318D47C89ED01F9E95882A (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * ___blTree0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_3 = (-1);
		V_4 = 0;
		goto IL_00df;
	}

IL_000a:
	{
		V_2 = 1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = __this->get_length_1();
		int32_t L_1 = V_4;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_5 = L_3;
		int32_t L_4 = V_5;
		if (L_4)
		{
			goto IL_0025;
		}
	}
	{
		V_0 = ((int32_t)138);
		V_1 = 3;
		goto IL_0038;
	}

IL_0025:
	{
		V_0 = 6;
		V_1 = 3;
		int32_t L_5 = V_3;
		int32_t L_6 = V_5;
		if ((((int32_t)L_5) == ((int32_t)L_6)))
		{
			goto IL_0038;
		}
	}
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_7 = ___blTree0;
		int32_t L_8 = V_5;
		NullCheck(L_7);
		Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA(L_7, L_8, /*hidden argument*/NULL);
		V_2 = 0;
	}

IL_0038:
	{
		int32_t L_9 = V_5;
		V_3 = L_9;
		int32_t L_10 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
		goto IL_0051;
	}

IL_0043:
	{
		int32_t L_11 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
		int32_t L_12 = V_2;
		int32_t L_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
		V_2 = L_13;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) >= ((int32_t)L_14)))
		{
			goto IL_0067;
		}
	}

IL_0051:
	{
		int32_t L_15 = V_4;
		int32_t L_16 = __this->get_numCodes_3();
		if ((((int32_t)L_15) >= ((int32_t)L_16)))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_17 = V_3;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_18 = __this->get_length_1();
		int32_t L_19 = V_4;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		uint8_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		if ((((int32_t)L_17) == ((int32_t)L_21)))
		{
			goto IL_0043;
		}
	}

IL_0067:
	{
		int32_t L_22 = V_2;
		int32_t L_23 = V_1;
		if ((((int32_t)L_22) >= ((int32_t)L_23)))
		{
			goto IL_007e;
		}
	}
	{
		goto IL_0074;
	}

IL_006d:
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_24 = ___blTree0;
		int32_t L_25 = V_3;
		NullCheck(L_24);
		Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA(L_24, L_25, /*hidden argument*/NULL);
	}

IL_0074:
	{
		int32_t L_26 = V_2;
		int32_t L_27 = L_26;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_27, (int32_t)1));
		if ((((int32_t)L_27) > ((int32_t)0)))
		{
			goto IL_006d;
		}
	}
	{
		goto IL_00df;
	}

IL_007e:
	{
		int32_t L_28 = V_3;
		if (!L_28)
		{
			goto IL_009f;
		}
	}
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_29 = ___blTree0;
		NullCheck(L_29);
		Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA(L_29, ((int32_t)16), /*hidden argument*/NULL);
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_30 = __this->get_dh_7();
		NullCheck(L_30);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_31 = L_30->get_pending_6();
		int32_t L_32 = V_2;
		NullCheck(L_31);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_31, ((int32_t)il2cpp_codegen_subtract((int32_t)L_32, (int32_t)3)), 2, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_009f:
	{
		int32_t L_33 = V_2;
		if ((((int32_t)L_33) > ((int32_t)((int32_t)10))))
		{
			goto IL_00c2;
		}
	}
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_34 = ___blTree0;
		NullCheck(L_34);
		Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA(L_34, ((int32_t)17), /*hidden argument*/NULL);
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_35 = __this->get_dh_7();
		NullCheck(L_35);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_36 = L_35->get_pending_6();
		int32_t L_37 = V_2;
		NullCheck(L_36);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_36, ((int32_t)il2cpp_codegen_subtract((int32_t)L_37, (int32_t)3)), 3, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00c2:
	{
		Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * L_38 = ___blTree0;
		NullCheck(L_38);
		Tree_WriteSymbol_m5A94C147F9601F8A0EE437DE0AFD003F80A842DA(L_38, ((int32_t)18), /*hidden argument*/NULL);
		DeflaterHuffman_t13BA47703D71031D76B8E41F9F01AB155E9E74B5 * L_39 = __this->get_dh_7();
		NullCheck(L_39);
		DeflaterPending_t385564C35CFD3DB0806B9FCCC04C0769914DD24B * L_40 = L_39->get_pending_6();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		PendingBuffer_WriteBits_m72022B62AF0AA36F4FC463148826888140F08EAD(L_40, ((int32_t)il2cpp_codegen_subtract((int32_t)L_41, (int32_t)((int32_t)11))), 7, /*hidden argument*/NULL);
	}

IL_00df:
	{
		int32_t L_42 = V_4;
		int32_t L_43 = __this->get_numCodes_3();
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_000a;
		}
	}
	{
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildLength(System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tree_BuildLength_m85D4045DCC50CC1D520AD0FC0C9486F28BC9F0CD (Tree_t2337C4C5B988BEC52CF2309D785C38EE3DA27B04 * __this, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___childs0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	{
		Int16U5BU5D_tD134F1E6F746D4C09C987436805256C210C2FFCD* L_0 = __this->get_freqs_0();
		NullCheck(L_0);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_1 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length))));
		__this->set_length_1(L_1);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = ___childs0;
		NullCheck(L_2);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length)))/(int32_t)2));
		int32_t L_3 = V_0;
		V_1 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1))/(int32_t)2));
		V_2 = 0;
		V_6 = 0;
		goto IL_0036;
	}

IL_0026:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_4 = __this->get_bl_counts_5();
		int32_t L_5 = V_6;
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)0);
		int32_t L_6 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0036:
	{
		int32_t L_7 = V_6;
		int32_t L_8 = __this->get_maxLength_6();
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_9 = V_0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_10 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)L_9);
		V_3 = L_10;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_11 = V_3;
		int32_t L_12 = V_0;
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1))), (int32_t)0);
		int32_t L_13 = V_0;
		V_7 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)1));
		goto IL_00ca;
	}

IL_0054:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_14 = ___childs0;
		int32_t L_15 = V_7;
		NullCheck(L_14);
		int32_t L_16 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_15)), (int32_t)1));
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		if ((((int32_t)L_17) == ((int32_t)(-1))))
		{
			goto IL_0098;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_18 = V_3;
		int32_t L_19 = V_7;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
		int32_t L_22 = V_8;
		int32_t L_23 = __this->get_maxLength_6();
		if ((((int32_t)L_22) <= ((int32_t)L_23)))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_24 = __this->get_maxLength_6();
		V_8 = L_24;
		int32_t L_25 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)1));
	}

IL_007d:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_26 = V_3;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_27 = ___childs0;
		int32_t L_28 = V_7;
		NullCheck(L_27);
		int32_t L_29 = ((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_28));
		int32_t L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_31 = V_3;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_32 = ___childs0;
		int32_t L_33 = V_7;
		NullCheck(L_32);
		int32_t L_34 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_33)), (int32_t)1));
		int32_t L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		int32_t L_36 = V_8;
		int32_t L_37 = L_36;
		V_9 = L_37;
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(L_35), (int32_t)L_37);
		int32_t L_38 = V_9;
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_30), (int32_t)L_38);
		goto IL_00c4;
	}

IL_0098:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_39 = V_3;
		int32_t L_40 = V_7;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		int32_t L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		V_10 = L_42;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_43 = __this->get_bl_counts_5();
		int32_t L_44 = V_10;
		NullCheck(L_43);
		int32_t* L_45 = ((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_44, (int32_t)1)))));
		int32_t L_46 = *((int32_t*)L_45);
		*((int32_t*)L_45) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1));
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_47 = __this->get_length_1();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_48 = ___childs0;
		int32_t L_49 = V_7;
		NullCheck(L_48);
		int32_t L_50 = ((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_49));
		int32_t L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_52 = V_3;
		int32_t L_53 = V_7;
		NullCheck(L_52);
		int32_t L_54 = L_53;
		int32_t L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		NullCheck(L_47);
		(L_47)->SetAt(static_cast<il2cpp_array_size_t>(L_51), (uint8_t)((int32_t)((uint8_t)L_55)));
	}

IL_00c4:
	{
		int32_t L_56 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_56, (int32_t)1));
	}

IL_00ca:
	{
		int32_t L_57 = V_7;
		if ((((int32_t)L_57) >= ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_58 = V_2;
		if (L_58)
		{
			goto IL_00d3;
		}
	}
	{
		return;
	}

IL_00d3:
	{
		int32_t L_59 = __this->get_maxLength_6();
		V_4 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_59, (int32_t)1));
	}

IL_00dd:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_60 = __this->get_bl_counts_5();
		int32_t L_61 = V_4;
		int32_t L_62 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_61, (int32_t)1));
		V_4 = L_62;
		NullCheck(L_60);
		int32_t L_63 = L_62;
		int32_t L_64 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		if (!L_64)
		{
			goto IL_00dd;
		}
	}

IL_00ed:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_65 = __this->get_bl_counts_5();
		int32_t L_66 = V_4;
		NullCheck(L_65);
		int32_t* L_67 = ((L_65)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_66)));
		int32_t L_68 = *((int32_t*)L_67);
		*((int32_t*)L_67) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_68, (int32_t)1));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_69 = __this->get_bl_counts_5();
		int32_t L_70 = V_4;
		int32_t L_71 = ((int32_t)il2cpp_codegen_add((int32_t)L_70, (int32_t)1));
		V_4 = L_71;
		NullCheck(L_69);
		int32_t* L_72 = ((L_69)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_71)));
		int32_t L_73 = *((int32_t*)L_72);
		*((int32_t*)L_72) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_73, (int32_t)1));
		int32_t L_74 = V_2;
		int32_t L_75 = __this->get_maxLength_6();
		int32_t L_76 = V_4;
		V_2 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_74, (int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_75, (int32_t)1)), (int32_t)L_76))&(int32_t)((int32_t)31)))))));
		int32_t L_77 = V_2;
		if ((((int32_t)L_77) <= ((int32_t)0)))
		{
			goto IL_0139;
		}
	}
	{
		int32_t L_78 = V_4;
		int32_t L_79 = __this->get_maxLength_6();
		if ((((int32_t)L_78) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_79, (int32_t)1)))))
		{
			goto IL_00ed;
		}
	}

IL_0139:
	{
		int32_t L_80 = V_2;
		if ((((int32_t)L_80) > ((int32_t)0)))
		{
			goto IL_00dd;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_81 = __this->get_bl_counts_5();
		int32_t L_82 = __this->get_maxLength_6();
		NullCheck(L_81);
		int32_t* L_83 = ((L_81)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_82, (int32_t)1)))));
		int32_t L_84 = *((int32_t*)L_83);
		int32_t L_85 = V_2;
		*((int32_t*)L_83) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_84, (int32_t)L_85));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_86 = __this->get_bl_counts_5();
		int32_t L_87 = __this->get_maxLength_6();
		NullCheck(L_86);
		int32_t* L_88 = ((L_86)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_87, (int32_t)2)))));
		int32_t L_89 = *((int32_t*)L_88);
		int32_t L_90 = V_2;
		*((int32_t*)L_88) = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_89, (int32_t)L_90));
		int32_t L_91 = V_1;
		V_5 = ((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_91));
		int32_t L_92 = __this->get_maxLength_6();
		V_11 = L_92;
		goto IL_01c0;
	}

IL_017c:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_93 = __this->get_bl_counts_5();
		int32_t L_94 = V_11;
		NullCheck(L_93);
		int32_t L_95 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_94, (int32_t)1));
		int32_t L_96 = (L_93)->GetAt(static_cast<il2cpp_array_size_t>(L_95));
		V_12 = L_96;
		goto IL_01b5;
	}

IL_018b:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_97 = ___childs0;
		int32_t L_98 = V_5;
		int32_t L_99 = L_98;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_99, (int32_t)1));
		NullCheck(L_97);
		int32_t L_100 = L_99;
		int32_t L_101 = (L_97)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		V_13 = ((int32_t)il2cpp_codegen_multiply((int32_t)2, (int32_t)L_101));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_102 = ___childs0;
		int32_t L_103 = V_13;
		NullCheck(L_102);
		int32_t L_104 = ((int32_t)il2cpp_codegen_add((int32_t)L_103, (int32_t)1));
		int32_t L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		if ((!(((uint32_t)L_105) == ((uint32_t)(-1)))))
		{
			goto IL_01b5;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_106 = __this->get_length_1();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_107 = ___childs0;
		int32_t L_108 = V_13;
		NullCheck(L_107);
		int32_t L_109 = L_108;
		int32_t L_110 = (L_107)->GetAt(static_cast<il2cpp_array_size_t>(L_109));
		int32_t L_111 = V_11;
		NullCheck(L_106);
		(L_106)->SetAt(static_cast<il2cpp_array_size_t>(L_110), (uint8_t)((int32_t)((uint8_t)L_111)));
		int32_t L_112 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_112, (int32_t)1));
	}

IL_01b5:
	{
		int32_t L_113 = V_12;
		if ((((int32_t)L_113) > ((int32_t)0)))
		{
			goto IL_018b;
		}
	}
	{
		int32_t L_114 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_114, (int32_t)1));
	}

IL_01c0:
	{
		int32_t L_115 = V_11;
		if (L_115)
		{
			goto IL_017c;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateStateMachineU3Ed__7__ctor_mDA5BCC7D91C9FE2D8B24060145C553D67CBC0A2B (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * L_1;
		L_1 = Thread_get_CurrentThread_m80236D2457FBCC1F76A08711E059A0B738DA71EC(/*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2;
		L_2 = Thread_get_ManagedThreadId_m7818C94F78A2DE2C7C278F6EA24B31F2BB758FD0(L_1, /*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_2);
		return;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateStateMachineU3Ed__7_System_IDisposable_Dispose_mF395E22DD873E589246058B5A071F32C85D1708B (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCreateStateMachineU3Ed__7_MoveNext_mC8FF14CD284CC4D2E34F84F2379C483DCB0FC93B (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * V_1 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_1 = __this->get_U3CU3E4__this_3();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_003e;
			}
			case 1:
			{
				goto IL_0057;
			}
			case 2:
			{
				goto IL_0089;
			}
			case 3:
			{
				goto IL_00b7;
			}
			case 4:
			{
				goto IL_0153;
			}
			case 5:
			{
				goto IL_01c7;
			}
			case 6:
			{
				goto IL_0255;
			}
			case 7:
			{
				goto IL_0292;
			}
			case 8:
			{
				goto IL_02c9;
			}
			case 9:
			{
				goto IL_03bd;
			}
		}
	}
	{
		return (bool)0;
	}

IL_003e:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_005e;
	}

IL_0047:
	{
		__this->set_U3CU3E2__current_1((bool)0);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0057:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_005e:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_3 = V_1;
		NullCheck(L_3);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_4 = L_3->get_input_1();
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_5 = V_1;
		NullCheck(L_5);
		int32_t* L_6 = L_5->get_address_of_litLenCodeCount_7();
		NullCheck(L_4);
		bool L_7;
		L_7 = StreamManipulator_TryGetBits_mC4C8072E3A5991B94049F9C3F9382E2D5B0B9D96(L_4, 5, (int32_t*)L_6, ((int32_t)257), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0090;
	}

IL_0079:
	{
		__this->set_U3CU3E2__current_1((bool)0);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0089:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0090:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_8 = V_1;
		NullCheck(L_8);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_9 = L_8->get_input_1();
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_10 = V_1;
		NullCheck(L_10);
		int32_t* L_11 = L_10->get_address_of_distanceCodeCount_8();
		NullCheck(L_9);
		bool L_12;
		L_12 = StreamManipulator_TryGetBits_mC4C8072E3A5991B94049F9C3F9382E2D5B0B9D96(L_9, 5, (int32_t*)L_11, 1, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0079;
		}
	}
	{
		goto IL_00be;
	}

IL_00a7:
	{
		__this->set_U3CU3E2__current_1((bool)0);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_00b7:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00be:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_13 = V_1;
		NullCheck(L_13);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_14 = L_13->get_input_1();
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_15 = V_1;
		NullCheck(L_15);
		int32_t* L_16 = L_15->get_address_of_metaCodeCount_9();
		NullCheck(L_14);
		bool L_17;
		L_17 = StreamManipulator_TryGetBits_mC4C8072E3A5991B94049F9C3F9382E2D5B0B9D96(L_14, 4, (int32_t*)L_16, 4, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a7;
		}
	}
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_18 = V_1;
		NullCheck(L_18);
		int32_t L_19 = L_18->get_litLenCodeCount_7();
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_20 = V_1;
		NullCheck(L_20);
		int32_t L_21 = L_20->get_distanceCodeCount_8();
		__this->set_U3CdataCodeCountU3E5__2_4(((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)L_21)));
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_22 = V_1;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_litLenCodeCount_7();
		if ((((int32_t)L_23) <= ((int32_t)((int32_t)286))))
		{
			goto IL_0104;
		}
	}
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_24 = V_1;
		NullCheck(L_24);
		int32_t* L_25 = L_24->get_address_of_litLenCodeCount_7();
		String_t* L_26;
		L_26 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_25, /*hidden argument*/NULL);
		ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07 * L_27 = (ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07_il2cpp_TypeInfo_var)));
		ValueOutOfRangeException__ctor_mEA093740F1E941798A6138627AEF47268840D542(L_27, L_26, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_27, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateStateMachineU3Ed__7_MoveNext_mC8FF14CD284CC4D2E34F84F2379C483DCB0FC93B_RuntimeMethod_var)));
	}

IL_0104:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_28 = V_1;
		NullCheck(L_28);
		int32_t L_29 = L_28->get_distanceCodeCount_8();
		if ((((int32_t)L_29) <= ((int32_t)((int32_t)30))))
		{
			goto IL_011f;
		}
	}
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_30 = V_1;
		NullCheck(L_30);
		int32_t* L_31 = L_30->get_address_of_distanceCodeCount_8();
		String_t* L_32;
		L_32 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_31, /*hidden argument*/NULL);
		ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07 * L_33 = (ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07_il2cpp_TypeInfo_var)));
		ValueOutOfRangeException__ctor_mEA093740F1E941798A6138627AEF47268840D542(L_33, L_32, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_33, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateStateMachineU3Ed__7_MoveNext_mC8FF14CD284CC4D2E34F84F2379C483DCB0FC93B_RuntimeMethod_var)));
	}

IL_011f:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_34 = V_1;
		NullCheck(L_34);
		int32_t L_35 = L_34->get_metaCodeCount_9();
		if ((((int32_t)L_35) <= ((int32_t)((int32_t)19))))
		{
			goto IL_013a;
		}
	}
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_36 = V_1;
		NullCheck(L_36);
		int32_t* L_37 = L_36->get_address_of_metaCodeCount_9();
		String_t* L_38;
		L_38 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_37, /*hidden argument*/NULL);
		ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07 * L_39 = (ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ValueOutOfRangeException_t89C789EC8FF5806608FD9FED55F1B47047AC8D07_il2cpp_TypeInfo_var)));
		ValueOutOfRangeException__ctor_mEA093740F1E941798A6138627AEF47268840D542(L_39, L_38, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_39, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateStateMachineU3Ed__7_MoveNext_mC8FF14CD284CC4D2E34F84F2379C483DCB0FC93B_RuntimeMethod_var)));
	}

IL_013a:
	{
		__this->set_U3CiU3E5__5_7(0);
		goto IL_018c;
	}

IL_0143:
	{
		__this->set_U3CU3E2__current_1((bool)0);
		__this->set_U3CU3E1__state_0(4);
		return (bool)1;
	}

IL_0153:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_015a:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_40 = V_1;
		NullCheck(L_40);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_41 = L_40->get_input_1();
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_42 = V_1;
		NullCheck(L_42);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** L_43 = L_42->get_address_of_codeLengths_4();
		IL2CPP_RUNTIME_CLASS_INIT(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_il2cpp_TypeInfo_var);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_44 = ((InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_StaticFields*)il2cpp_codegen_static_fields_for(InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA_il2cpp_TypeInfo_var))->get_MetaCodeLengthIndex_0();
		int32_t L_45 = __this->get_U3CiU3E5__5_7();
		NullCheck(L_44);
		int32_t L_46 = L_45;
		int32_t L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_41);
		bool L_48;
		L_48 = StreamManipulator_TryGetBits_mEB43579CCD99015132323748F193722297150632(L_41, 3, (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726**)L_43, L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0143;
		}
	}
	{
		int32_t L_49 = __this->get_U3CiU3E5__5_7();
		V_4 = L_49;
		int32_t L_50 = V_4;
		__this->set_U3CiU3E5__5_7(((int32_t)il2cpp_codegen_add((int32_t)L_50, (int32_t)1)));
	}

IL_018c:
	{
		int32_t L_51 = __this->get_U3CiU3E5__5_7();
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_52 = V_1;
		NullCheck(L_52);
		int32_t L_53 = L_52->get_metaCodeCount_9();
		if ((((int32_t)L_51) < ((int32_t)L_53)))
		{
			goto IL_015a;
		}
	}
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_54 = V_1;
		NullCheck(L_54);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_55 = L_54->get_codeLengths_4();
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_56 = (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE *)il2cpp_codegen_object_new(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var);
		InflaterHuffmanTree__ctor_m8979A23B855128F705F710C36B32AB863EADF1D5(L_56, (RuntimeObject*)(RuntimeObject*)L_55, /*hidden argument*/NULL);
		__this->set_U3CmetaCodeTreeU3E5__3_5(L_56);
		__this->set_U3CindexU3E5__4_6(0);
		goto IL_033e;
	}

IL_01b7:
	{
		__this->set_U3CU3E2__current_1((bool)0);
		__this->set_U3CU3E1__state_0(5);
		return (bool)1;
	}

IL_01c7:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_01ce:
	{
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_57 = __this->get_U3CmetaCodeTreeU3E5__3_5();
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_58 = V_1;
		NullCheck(L_58);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_59 = L_58->get_input_1();
		NullCheck(L_57);
		int32_t L_60;
		L_60 = InflaterHuffmanTree_GetSymbol_m0F2DAE53C30D2A81AC3ED21E476F2D3451728442(L_57, L_59, /*hidden argument*/NULL);
		int32_t L_61 = L_60;
		V_5 = L_61;
		if ((((int32_t)L_61) < ((int32_t)0)))
		{
			goto IL_01b7;
		}
	}
	{
		int32_t L_62 = V_5;
		if ((((int32_t)L_62) >= ((int32_t)((int32_t)16))))
		{
			goto IL_020e;
		}
	}
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_63 = V_1;
		NullCheck(L_63);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_64 = L_63->get_codeLengths_4();
		int32_t L_65 = __this->get_U3CindexU3E5__4_6();
		V_4 = L_65;
		int32_t L_66 = V_4;
		__this->set_U3CindexU3E5__4_6(((int32_t)il2cpp_codegen_add((int32_t)L_66, (int32_t)1)));
		int32_t L_67 = V_4;
		int32_t L_68 = V_5;
		NullCheck(L_64);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(L_67), (uint8_t)((int32_t)((uint8_t)L_68)));
		goto IL_033e;
	}

IL_020e:
	{
		__this->set_U3CiU3E5__5_7(0);
		int32_t L_69 = V_5;
		if ((!(((uint32_t)L_69) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0273;
		}
	}
	{
		int32_t L_70 = __this->get_U3CindexU3E5__4_6();
		if (L_70)
		{
			goto IL_022e;
		}
	}
	{
		StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C * L_71 = (StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C_il2cpp_TypeInfo_var)));
		StreamDecodingException__ctor_mE34C08B6F04E109D9A4E0B2DA3DB7C67555C5CE1(L_71, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralF387CA61FFEFEF87BC52030A46267A254BF3224C)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_71, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateStateMachineU3Ed__7_MoveNext_mC8FF14CD284CC4D2E34F84F2379C483DCB0FC93B_RuntimeMethod_var)));
	}

IL_022e:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_72 = V_1;
		NullCheck(L_72);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_73 = L_72->get_codeLengths_4();
		int32_t L_74 = __this->get_U3CindexU3E5__4_6();
		NullCheck(L_73);
		int32_t L_75 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_74, (int32_t)1));
		uint8_t L_76 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		__this->set_U3CcodeLengthU3E5__6_8(L_76);
		goto IL_025c;
	}

IL_0245:
	{
		__this->set_U3CU3E2__current_1((bool)0);
		__this->set_U3CU3E1__state_0(6);
		return (bool)1;
	}

IL_0255:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_025c:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_77 = V_1;
		NullCheck(L_77);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_78 = L_77->get_input_1();
		int32_t* L_79 = __this->get_address_of_U3CiU3E5__5_7();
		NullCheck(L_78);
		bool L_80;
		L_80 = StreamManipulator_TryGetBits_mC4C8072E3A5991B94049F9C3F9382E2D5B0B9D96(L_78, 2, (int32_t*)L_79, 3, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_0245;
		}
	}
	{
		goto IL_02e6;
	}

IL_0273:
	{
		int32_t L_81 = V_5;
		if ((!(((uint32_t)L_81) == ((uint32_t)((int32_t)17)))))
		{
			goto IL_02b0;
		}
	}
	{
		__this->set_U3CcodeLengthU3E5__6_8((uint8_t)0);
		goto IL_0299;
	}

IL_0282:
	{
		__this->set_U3CU3E2__current_1((bool)0);
		__this->set_U3CU3E1__state_0(7);
		return (bool)1;
	}

IL_0292:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0299:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_82 = V_1;
		NullCheck(L_82);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_83 = L_82->get_input_1();
		int32_t* L_84 = __this->get_address_of_U3CiU3E5__5_7();
		NullCheck(L_83);
		bool L_85;
		L_85 = StreamManipulator_TryGetBits_mC4C8072E3A5991B94049F9C3F9382E2D5B0B9D96(L_83, 3, (int32_t*)L_84, 3, /*hidden argument*/NULL);
		if (!L_85)
		{
			goto IL_0282;
		}
	}
	{
		goto IL_02e6;
	}

IL_02b0:
	{
		__this->set_U3CcodeLengthU3E5__6_8((uint8_t)0);
		goto IL_02d0;
	}

IL_02b9:
	{
		__this->set_U3CU3E2__current_1((bool)0);
		__this->set_U3CU3E1__state_0(8);
		return (bool)1;
	}

IL_02c9:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_02d0:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_86 = V_1;
		NullCheck(L_86);
		StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * L_87 = L_86->get_input_1();
		int32_t* L_88 = __this->get_address_of_U3CiU3E5__5_7();
		NullCheck(L_87);
		bool L_89;
		L_89 = StreamManipulator_TryGetBits_mC4C8072E3A5991B94049F9C3F9382E2D5B0B9D96(L_87, 7, (int32_t*)L_88, ((int32_t)11), /*hidden argument*/NULL);
		if (!L_89)
		{
			goto IL_02b9;
		}
	}

IL_02e6:
	{
		int32_t L_90 = __this->get_U3CindexU3E5__4_6();
		int32_t L_91 = __this->get_U3CiU3E5__5_7();
		int32_t L_92 = __this->get_U3CdataCodeCountU3E5__2_4();
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_90, (int32_t)L_91))) <= ((int32_t)L_92)))
		{
			goto IL_0327;
		}
	}
	{
		StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C * L_93 = (StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&StreamDecodingException_tDD1F05E176C1D380B30466986ABE8C366816659C_il2cpp_TypeInfo_var)));
		StreamDecodingException__ctor_mE34C08B6F04E109D9A4E0B2DA3DB7C67555C5CE1(L_93, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral42AEC79DC2BD0CC7051BC6692BBFF6CFDFB5F434)), /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_93, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateStateMachineU3Ed__7_MoveNext_mC8FF14CD284CC4D2E34F84F2379C483DCB0FC93B_RuntimeMethod_var)));
	}

IL_0306:
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_94 = V_1;
		NullCheck(L_94);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_95 = L_94->get_codeLengths_4();
		int32_t L_96 = __this->get_U3CindexU3E5__4_6();
		V_4 = L_96;
		int32_t L_97 = V_4;
		__this->set_U3CindexU3E5__4_6(((int32_t)il2cpp_codegen_add((int32_t)L_97, (int32_t)1)));
		int32_t L_98 = V_4;
		uint8_t L_99 = __this->get_U3CcodeLengthU3E5__6_8();
		NullCheck(L_95);
		(L_95)->SetAt(static_cast<il2cpp_array_size_t>(L_98), (uint8_t)L_99);
	}

IL_0327:
	{
		int32_t L_100 = __this->get_U3CiU3E5__5_7();
		V_4 = L_100;
		int32_t L_101 = V_4;
		__this->set_U3CiU3E5__5_7(((int32_t)il2cpp_codegen_subtract((int32_t)L_101, (int32_t)1)));
		int32_t L_102 = V_4;
		if ((((int32_t)L_102) > ((int32_t)0)))
		{
			goto IL_0306;
		}
	}

IL_033e:
	{
		int32_t L_103 = __this->get_U3CindexU3E5__4_6();
		int32_t L_104 = __this->get_U3CdataCodeCountU3E5__2_4();
		if ((((int32_t)L_103) < ((int32_t)L_104)))
		{
			goto IL_01ce;
		}
	}
	{
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_105 = V_1;
		NullCheck(L_105);
		int32_t L_106 = L_105->get_litLenCodeCount_7();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_107 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_106);
		V_2 = L_107;
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_108 = V_1;
		NullCheck(L_108);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_109 = L_108->get_codeLengths_4();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_110 = V_2;
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_111 = V_1;
		NullCheck(L_111);
		int32_t L_112 = L_111->get_litLenCodeCount_7();
		Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725((RuntimeArray *)(RuntimeArray *)L_109, 0, (RuntimeArray *)(RuntimeArray *)L_110, 0, L_112, /*hidden argument*/NULL);
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_113 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_114 = V_2;
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_115 = (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE *)il2cpp_codegen_object_new(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var);
		InflaterHuffmanTree__ctor_m8979A23B855128F705F710C36B32AB863EADF1D5(L_115, (RuntimeObject*)(RuntimeObject*)L_114, /*hidden argument*/NULL);
		NullCheck(L_113);
		L_113->set_litLenTree_5(L_115);
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_116 = V_1;
		NullCheck(L_116);
		int32_t L_117 = L_116->get_distanceCodeCount_8();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_118 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_117);
		V_3 = L_118;
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_119 = V_1;
		NullCheck(L_119);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_120 = L_119->get_codeLengths_4();
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_121 = V_1;
		NullCheck(L_121);
		int32_t L_122 = L_121->get_litLenCodeCount_7();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_123 = V_3;
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_124 = V_1;
		NullCheck(L_124);
		int32_t L_125 = L_124->get_distanceCodeCount_8();
		Buffer_BlockCopy_mD01FC13D87078586714AA235261A9E786C351725((RuntimeArray *)(RuntimeArray *)L_120, L_122, (RuntimeArray *)(RuntimeArray *)L_123, 0, L_125, /*hidden argument*/NULL);
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_126 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_127 = V_3;
		InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE * L_128 = (InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE *)il2cpp_codegen_object_new(InflaterHuffmanTree_t21DC3BCC4E3A76DCECB7BBB4DDC9F83B184D28AE_il2cpp_TypeInfo_var);
		InflaterHuffmanTree__ctor_m8979A23B855128F705F710C36B32AB863EADF1D5(L_128, (RuntimeObject*)(RuntimeObject*)L_127, /*hidden argument*/NULL);
		NullCheck(L_126);
		L_126->set_distTree_6(L_128);
		__this->set_U3CU3E2__current_1((bool)1);
		__this->set_U3CU3E1__state_0(((int32_t)9));
		return (bool)1;
	}

IL_03bd:
	{
		__this->set_U3CU3E1__state_0((-1));
		return (bool)0;
	}
}
// System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.Generic.IEnumerator<System.Boolean>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_BooleanU3E_get_Current_m6DA70CEC026BE9529FE9D72164DE32F2A3173C5D (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_Reset_m0C8764730A6F04009E62C81D5B02CFE577620BA6 (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_Reset_m0C8764730A6F04009E62C81D5B02CFE577620BA6_RuntimeMethod_var)));
	}
}
// System.Object FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_get_Current_m0B8E932CEA10979633EBB79CDBB0D348140C0E51 (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_U3CU3E2__current_1();
		bool L_1 = L_0;
		RuntimeObject * L_2 = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Boolean> FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.Generic.IEnumerable<System.Boolean>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumerableU3CSystem_BooleanU3E_GetEnumerator_m167460EFE831503FB0E0A7B18C3612A68229266F (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		Thread_tB9EB71664220EE16451AF3276D78DE6614D2A414 * L_2;
		L_2 = Thread_get_CurrentThread_m80236D2457FBCC1F76A08711E059A0B738DA71EC(/*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3;
		L_3 = Thread_get_ManagedThreadId_m7818C94F78A2DE2C7C278F6EA24B31F2BB758FD0(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_3))))
		{
			goto IL_0027;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_003a;
	}

IL_0027:
	{
		U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * L_4 = (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 *)il2cpp_codegen_object_new(U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3_il2cpp_TypeInfo_var);
		U3CCreateStateMachineU3Ed__7__ctor_mDA5BCC7D91C9FE2D8B24060145C553D67CBC0A2B(L_4, 0, /*hidden argument*/NULL);
		V_0 = L_4;
		U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * L_5 = V_0;
		InflaterDynHeader_tE810F12BA4812C4B8D25847DE4400CDE5B0F3FFA * L_6 = __this->get_U3CU3E4__this_3();
		NullCheck(L_5);
		L_5->set_U3CU3E4__this_3(L_6);
	}

IL_003a:
	{
		U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * L_7 = V_0;
		return L_7;
	}
}
// System.Collections.IEnumerator FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerable_GetEnumerator_m3950AE6E42EC05FF91763BAB01DC13EE64EED335 (U3CCreateStateMachineU3Ed__7_t48D88D328BB8A17D63C52C7C88A106F65C2B5FC3 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumerableU3CSystem_BooleanU3E_GetEnumerator_m167460EFE831503FB0E0A7B18C3612A68229266F(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int64_t DeflaterEngine_get_TotalIn_mC27F3CA9F01B1C1974B8DCFF91D7DE9B816F26FD_inline (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = __this->get_totalIn_17();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void DeflaterEngine_set_Strategy_m49FD868FE9D1242660EA59FD2C03881C34FF68AB_inline (DeflaterEngine_t1DEEC1E3D873829763F2E06E91C5795A9D1B0386 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_strategy_10(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t PendingBuffer_get_BitCount_mB4A917AF510467CF020B8E173785361118CF9F41_inline (PendingBuffer_t7BFDAC9AA079DF549B9016C23145683DDC3B5C05 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_bitCount_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool DeflaterOutputStream_get_IsStreamOwner_m3023CE9D69403AC66C712F5CC82EB88D8B395E61_inline (DeflaterOutputStream_tE60AE5AC0D31B5AE0FB100E6A6AF519C7498122E * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get__IsStreamOwner_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t InflaterInputBuffer_get_Available_m7BDBC3F8048C19BF8C9D3E253BBFDA8601A107DE_inline (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_available_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int64_t Inflater_get_TotalOut_m24899EBBA3839BC2A41C59359686D6CB758576F0_inline (Inflater_tE795C12D46C01707342DE90A67141C8F4C4ACAA7 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = __this->get_totalOut_11();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InflaterInputBuffer_set_Available_mB9B0F342C4B3FB90CCFC0F34B4C5CEDDB7104981_inline (InflaterInputBuffer_t7C037C93A8A067C6BF67767FAFB8FFA7A26B7D17 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_available_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t OutputWindow_GetAvailable_m00EBEEB3AAAEB4C919C63615591FAF21BB60BBEC_inline (OutputWindow_t86D3DDD496223417D7F0D3B8981ED1D2A6E49718 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_windowFilled_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t StreamManipulator_get_AvailableBits_m7FA2836285F2C2BA6B74A8AA135D770DB9030A7B_inline (StreamManipulator_t6AC4446221F74BA8E15EE935D935E66D3C238EB0 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_bitsInBuffer__4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool InflaterInputStream_get_IsStreamOwner_m3C551CED06177186C1D848E92B2E4B8F2C879814_inline (InflaterInputStream_t288E021E1031C5C138869820FC233CDFD638224E * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get__IsStreamOwner_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Count_mC13AC26CCFD0EACBCC08F24F2A7BB22841B44C32_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__count_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ArraySegment_1_get_Offset_m13F255A2A7A730982F330A448FCB32239782C505_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__offset_1();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ArraySegment_1_get_Array_m3D83A2CFF4D51F8ED83C89538616FF0A700F463C_gshared_inline (ArraySegment_1_t89782CFC3178DB9FD8FFCCC398B4575AE8D740AE * __this, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_0 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)__this->get__array_0();
		return (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)L_0;
	}
}
