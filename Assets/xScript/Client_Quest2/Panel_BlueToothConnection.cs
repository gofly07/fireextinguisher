﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Panel_BlueToothConnection : MonoBehaviour
{
    [SerializeField] private Text txtRemind;
    [SerializeField] private Slider sldProgressBar;

    private bool isConnect = false;
    private float time;
    private int maxtime = 15;

    public event EventHandler EHOpenClientMenu;

    private void OnEnable()
    {
        Init();
        StopAllCoroutines();
        StartCoroutine(BlueToothConectionProgress());
    }

    private void Init()
    {
        isConnect = false;
        time = 0;
        sldProgressBar.maxValue = maxtime;
        sldProgressBar.gameObject.SetActive(true);
        txtRemind.text = string.Format("請在{0}秒內按下實體滅火器藍芽偵測按鈕", maxtime);
        txtRemind.color = Color.black;
    }

    private void Update()
    {
        time += Time.deltaTime;
        if (time < maxtime)
            sldProgressBar.value = time;

        if (OVRInput.GetDown(OVRInput.RawButton.Y) || Input.GetKeyDown(KeyCode.S))
        {
            ClientManager.instance.SwitchFireExtinguisher(DKP.VR.HandsVisualization.DisplayMode.Powder);
            Destroy(GameObject.Find("LaserRay_l"));
            if (!isConnect)
                isConnect = true;
        }
    }

    private IEnumerator BlueToothConectionProgress()
    {
        yield return null;

        int waitTime = maxtime;
        while (true)
        {
            yield return new WaitForSeconds(1);

            waitTime--;
            //Sucess
            if (isConnect)
            {
                sldProgressBar.gameObject.SetActive(false);

                for (int i = 3; i >= 0; i--)
                {
                    txtRemind.text = string.Format("實體滅火器藍芽訊號確認成功...({0})", i);
                    yield return new WaitForSeconds(1);
                }
                EHOpenClientMenu(this, EventArgs.Empty);
                break;
            }
            //Failed
            else if (waitTime <= 0)
            {
                sldProgressBar.gameObject.SetActive(false);
                txtRemind.color = Color.red;

                for (int i = 3; i >= 0; i--)
                {
                    txtRemind.text = string.Format("實體滅火器訊號確認失敗...({0})", i);
                    yield return new WaitForSeconds(1);
                }
                OnEnable();
                break;
            }
        }
    }
}
