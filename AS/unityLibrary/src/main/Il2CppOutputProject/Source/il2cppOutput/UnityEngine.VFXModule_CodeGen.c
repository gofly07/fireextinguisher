﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.VFX.VFXEventAttribute::.ctor(System.IntPtr,System.Boolean,UnityEngine.VFX.VisualEffectAsset)
extern void VFXEventAttribute__ctor_mDE4833E90493250BDBF27A8A5AFAADE4E5A90964 (void);
// 0x00000002 System.IntPtr UnityEngine.VFX.VFXEventAttribute::Internal_Create()
extern void VFXEventAttribute_Internal_Create_mE5768E6D7DDCF1F8B6E917B5BAA7FE61820FE5DE (void);
// 0x00000003 UnityEngine.VFX.VFXEventAttribute UnityEngine.VFX.VFXEventAttribute::Internal_InstanciateVFXEventAttribute(UnityEngine.VFX.VisualEffectAsset)
extern void VFXEventAttribute_Internal_InstanciateVFXEventAttribute_m17249769F68DD56C7532726912EA0E4690E1A5D2 (void);
// 0x00000004 System.Void UnityEngine.VFX.VFXEventAttribute::Internal_InitFromAsset(UnityEngine.VFX.VisualEffectAsset)
extern void VFXEventAttribute_Internal_InitFromAsset_m5DC121D03DAA7A8E6FF348D98CDAAE209F1F8AE1 (void);
// 0x00000005 UnityEngine.VFX.VisualEffectAsset UnityEngine.VFX.VFXEventAttribute::get_vfxAsset()
extern void VFXEventAttribute_get_vfxAsset_mB7617193C2FB94DC54DA5FB7AD20306074841E9C (void);
// 0x00000006 System.Void UnityEngine.VFX.VFXEventAttribute::Release()
extern void VFXEventAttribute_Release_m9AEDE0F9E70188F9737D68638733D70DB4814983 (void);
// 0x00000007 System.Void UnityEngine.VFX.VFXEventAttribute::Finalize()
extern void VFXEventAttribute_Finalize_mC4B4672CB30A8925BD10CF82CFB59395A2C0A83A (void);
// 0x00000008 System.Void UnityEngine.VFX.VFXEventAttribute::Dispose()
extern void VFXEventAttribute_Dispose_m56663028C3FD054C89339050249052E8C350D0AF (void);
// 0x00000009 System.Void UnityEngine.VFX.VFXEventAttribute::Internal_Destroy(System.IntPtr)
extern void VFXEventAttribute_Internal_Destroy_m63125F41EFFA7AC712556EAA1D7BA8F4F2D860F0 (void);
// 0x0000000A System.Void UnityEngine.VFX.VFXExpressionValues::.ctor()
extern void VFXExpressionValues__ctor_m4B8043CAE5D33D062707D851DD4E62D9F7BB1320 (void);
// 0x0000000B UnityEngine.VFX.VFXExpressionValues UnityEngine.VFX.VFXExpressionValues::CreateExpressionValuesWrapper(System.IntPtr)
extern void VFXExpressionValues_CreateExpressionValuesWrapper_mA085560E2E04D2CD8C76664FB169F227AA0A6537 (void);
// 0x0000000C System.Void UnityEngine.VFX.VFXSpawnerCallbacks::OnPlay(UnityEngine.VFX.VFXSpawnerState,UnityEngine.VFX.VFXExpressionValues,UnityEngine.VFX.VisualEffect)
// 0x0000000D System.Void UnityEngine.VFX.VFXSpawnerCallbacks::OnUpdate(UnityEngine.VFX.VFXSpawnerState,UnityEngine.VFX.VFXExpressionValues,UnityEngine.VFX.VisualEffect)
// 0x0000000E System.Void UnityEngine.VFX.VFXSpawnerCallbacks::OnStop(UnityEngine.VFX.VFXSpawnerState,UnityEngine.VFX.VFXExpressionValues,UnityEngine.VFX.VisualEffect)
// 0x0000000F System.Void UnityEngine.VFX.VFXSpawnerCallbacks::.ctor()
extern void VFXSpawnerCallbacks__ctor_m4F7269BAF4E8085E8628FF6C0EF0BD42BF43B072 (void);
// 0x00000010 System.Void UnityEngine.VFX.VFXSpawnerState::.ctor(System.IntPtr,System.Boolean)
extern void VFXSpawnerState__ctor_m2A95971A7A6D3701F39FD7A656451CD619C3010E (void);
// 0x00000011 UnityEngine.VFX.VFXSpawnerState UnityEngine.VFX.VFXSpawnerState::CreateSpawnerStateWrapper()
extern void VFXSpawnerState_CreateSpawnerStateWrapper_m079EC8731BA4C6A57A5138A16BB9DA147BCE6E17 (void);
// 0x00000012 System.Void UnityEngine.VFX.VFXSpawnerState::SetWrapValue(System.IntPtr)
extern void VFXSpawnerState_SetWrapValue_mA673646567D2B54072EEC275FB79BEBF82C7EE31 (void);
// 0x00000013 System.Void UnityEngine.VFX.VFXSpawnerState::Release()
extern void VFXSpawnerState_Release_m9D420CAC0353FCBCBF0F2D59AA286ED3D0BC4649 (void);
// 0x00000014 System.Void UnityEngine.VFX.VFXSpawnerState::Finalize()
extern void VFXSpawnerState_Finalize_mDD2AA40919A974E7CE616FE003162A762B806CA5 (void);
// 0x00000015 System.Void UnityEngine.VFX.VFXSpawnerState::Dispose()
extern void VFXSpawnerState_Dispose_mE45940AB83108F8F02D81BE4CEDE8C738F1C47F5 (void);
// 0x00000016 System.Void UnityEngine.VFX.VFXSpawnerState::Internal_Destroy(System.IntPtr)
extern void VFXSpawnerState_Internal_Destroy_mED5E41CC22939E5F21A53FC44CA6FA7022F87273 (void);
// 0x00000017 System.Void UnityEngine.VFX.VisualEffectObject::.ctor()
extern void VisualEffectObject__ctor_mC6F115A9348A51326C366ACB2724884050A00B2D (void);
// 0x00000018 System.Void UnityEngine.VFX.VisualEffectAsset::.ctor()
extern void VisualEffectAsset__ctor_m61A7A279415031194C654C892DFB4A1AD19CCC7C (void);
// 0x00000019 System.Void UnityEngine.VFX.VisualEffectAsset::.cctor()
extern void VisualEffectAsset__cctor_mD1DB26F94C7046E351AB856DE9666BEB465E759E (void);
// 0x0000001A System.Void UnityEngine.VFX.VFXOutputEventArgs::.ctor(System.Int32,UnityEngine.VFX.VFXEventAttribute)
extern void VFXOutputEventArgs__ctor_m7C49FC1B34F3582FE08D7A1CAA5024C09A86F7A2 (void);
// 0x0000001B System.Void UnityEngine.VFX.VisualEffect::set_pause(System.Boolean)
extern void VisualEffect_set_pause_mAC5D0B2D037A4869FE22A237924530C267E79FA4 (void);
// 0x0000001C UnityEngine.VFX.VisualEffectAsset UnityEngine.VFX.VisualEffect::get_visualEffectAsset()
extern void VisualEffect_get_visualEffectAsset_m019D107588568B2C5F1FFA77C626DC986A63AD96 (void);
// 0x0000001D UnityEngine.VFX.VFXEventAttribute UnityEngine.VFX.VisualEffect::CreateVFXEventAttribute()
extern void VisualEffect_CreateVFXEventAttribute_mE99FC6DA23D465D06A8547CC5B2502FB0B30D8D6 (void);
// 0x0000001E System.Void UnityEngine.VFX.VisualEffect::CheckValidVFXEventAttribute(UnityEngine.VFX.VFXEventAttribute)
extern void VisualEffect_CheckValidVFXEventAttribute_m798412C5B7ACAD1DC17366FD1A8F35B7A289BC58 (void);
// 0x0000001F System.Void UnityEngine.VFX.VisualEffect::SendEventFromScript(System.Int32,UnityEngine.VFX.VFXEventAttribute)
extern void VisualEffect_SendEventFromScript_m6FDCD210185FA5B18F9951736974653E66F1A6F4 (void);
// 0x00000020 System.Void UnityEngine.VFX.VisualEffect::SendEvent(System.Int32,UnityEngine.VFX.VFXEventAttribute)
extern void VisualEffect_SendEvent_mB81EDFCDACF7DF4FF59DB085BA061C3F5282E139 (void);
// 0x00000021 System.Void UnityEngine.VFX.VisualEffect::SendEvent(System.String)
extern void VisualEffect_SendEvent_mB86244AAF394CA825E843E692CDD34F174B71BCB (void);
// 0x00000022 System.Boolean UnityEngine.VFX.VisualEffect::HasFloat(System.Int32)
extern void VisualEffect_HasFloat_mF05173BE6C2286D2CCCACDD96E57960BD4740F68 (void);
// 0x00000023 System.Boolean UnityEngine.VFX.VisualEffect::HasVector3(System.Int32)
extern void VisualEffect_HasVector3_m290498C54F35B5793B7A748555FBF17DEE23D157 (void);
// 0x00000024 System.Void UnityEngine.VFX.VisualEffect::SetFloat(System.Int32,System.Single)
extern void VisualEffect_SetFloat_mF115AB6D0D748CF247A71ED283EC8877498762EA (void);
// 0x00000025 System.Void UnityEngine.VFX.VisualEffect::SetVector2(System.Int32,UnityEngine.Vector2)
extern void VisualEffect_SetVector2_m0EEDC422CC1F08984D7EDF7AC59F17756E2DEDE9 (void);
// 0x00000026 System.Void UnityEngine.VFX.VisualEffect::SetVector3(System.Int32,UnityEngine.Vector3)
extern void VisualEffect_SetVector3_m9B318DA2BD87BC77E2E06B6821D2D0751C3CE6CB (void);
// 0x00000027 System.Void UnityEngine.VFX.VisualEffect::SetVector4(System.Int32,UnityEngine.Vector4)
extern void VisualEffect_SetVector4_m18B5E3734A0A6BBECCAA926DEAA527DF40D8E431 (void);
// 0x00000028 UnityEngine.Vector3 UnityEngine.VFX.VisualEffect::GetVector3(System.Int32)
extern void VisualEffect_GetVector3_m7CB65F483251FB276F7F62040FE4757B234D4E38 (void);
// 0x00000029 System.Boolean UnityEngine.VFX.VisualEffect::HasFloat(System.String)
extern void VisualEffect_HasFloat_m4CD2B4D03449F285E31ACD55B0A261E91DAF5877 (void);
// 0x0000002A System.Boolean UnityEngine.VFX.VisualEffect::HasVector3(System.String)
extern void VisualEffect_HasVector3_mA74BFA10A311D183CE71A0039E9D3D6CD5B8E257 (void);
// 0x0000002B System.Void UnityEngine.VFX.VisualEffect::SetFloat(System.String,System.Single)
extern void VisualEffect_SetFloat_m0D9164EE5F6C6643E1C0449BCB6D5E8FD05CBE13 (void);
// 0x0000002C System.Void UnityEngine.VFX.VisualEffect::SetVector2(System.String,UnityEngine.Vector2)
extern void VisualEffect_SetVector2_m6949CCFB08C62861FEF319208D3E3289AB87DE8C (void);
// 0x0000002D System.Void UnityEngine.VFX.VisualEffect::SetVector3(System.String,UnityEngine.Vector3)
extern void VisualEffect_SetVector3_mB54AD688AADF2AABF7A053D4188EC7076673D7F1 (void);
// 0x0000002E System.Void UnityEngine.VFX.VisualEffect::SetVector4(System.String,UnityEngine.Vector4)
extern void VisualEffect_SetVector4_mC329BCD56199CCC86D1D7EC2A898F17ACE413792 (void);
// 0x0000002F UnityEngine.Vector3 UnityEngine.VFX.VisualEffect::GetVector3(System.String)
extern void VisualEffect_GetVector3_mB50AF62B083482685CF41A493E35691638D28970 (void);
// 0x00000030 UnityEngine.VFX.VFXEventAttribute UnityEngine.VFX.VisualEffect::InvokeGetCachedEventAttributeForOutputEvent_Internal(UnityEngine.VFX.VisualEffect)
extern void VisualEffect_InvokeGetCachedEventAttributeForOutputEvent_Internal_m1A4D8B1736777CB18C2893313309880E61798C6B (void);
// 0x00000031 System.Void UnityEngine.VFX.VisualEffect::InvokeOutputEventReceived_Internal(UnityEngine.VFX.VisualEffect,System.Int32)
extern void VisualEffect_InvokeOutputEventReceived_Internal_mFC477F53118047CFF858A82FE87DF9356706421A (void);
// 0x00000032 System.Void UnityEngine.VFX.VisualEffect::SetVector2_Injected(System.Int32,UnityEngine.Vector2&)
extern void VisualEffect_SetVector2_Injected_mA8DDFCFCC959AB0E5D82B2A55A7035F4003DDA9B (void);
// 0x00000033 System.Void UnityEngine.VFX.VisualEffect::SetVector3_Injected(System.Int32,UnityEngine.Vector3&)
extern void VisualEffect_SetVector3_Injected_mA73BA46A825BE9994BA950DB314C64A9BEE8A89C (void);
// 0x00000034 System.Void UnityEngine.VFX.VisualEffect::SetVector4_Injected(System.Int32,UnityEngine.Vector4&)
extern void VisualEffect_SetVector4_Injected_mBFF4141512B0D2117A13DDDDF926D6572871A1D9 (void);
// 0x00000035 System.Void UnityEngine.VFX.VisualEffect::GetVector3_Injected(System.Int32,UnityEngine.Vector3&)
extern void VisualEffect_GetVector3_Injected_mAC9972D56C569104905B4CBDD18071B452182E32 (void);
static Il2CppMethodPointer s_methodPointers[53] = 
{
	VFXEventAttribute__ctor_mDE4833E90493250BDBF27A8A5AFAADE4E5A90964,
	VFXEventAttribute_Internal_Create_mE5768E6D7DDCF1F8B6E917B5BAA7FE61820FE5DE,
	VFXEventAttribute_Internal_InstanciateVFXEventAttribute_m17249769F68DD56C7532726912EA0E4690E1A5D2,
	VFXEventAttribute_Internal_InitFromAsset_m5DC121D03DAA7A8E6FF348D98CDAAE209F1F8AE1,
	VFXEventAttribute_get_vfxAsset_mB7617193C2FB94DC54DA5FB7AD20306074841E9C,
	VFXEventAttribute_Release_m9AEDE0F9E70188F9737D68638733D70DB4814983,
	VFXEventAttribute_Finalize_mC4B4672CB30A8925BD10CF82CFB59395A2C0A83A,
	VFXEventAttribute_Dispose_m56663028C3FD054C89339050249052E8C350D0AF,
	VFXEventAttribute_Internal_Destroy_m63125F41EFFA7AC712556EAA1D7BA8F4F2D860F0,
	VFXExpressionValues__ctor_m4B8043CAE5D33D062707D851DD4E62D9F7BB1320,
	VFXExpressionValues_CreateExpressionValuesWrapper_mA085560E2E04D2CD8C76664FB169F227AA0A6537,
	NULL,
	NULL,
	NULL,
	VFXSpawnerCallbacks__ctor_m4F7269BAF4E8085E8628FF6C0EF0BD42BF43B072,
	VFXSpawnerState__ctor_m2A95971A7A6D3701F39FD7A656451CD619C3010E,
	VFXSpawnerState_CreateSpawnerStateWrapper_m079EC8731BA4C6A57A5138A16BB9DA147BCE6E17,
	VFXSpawnerState_SetWrapValue_mA673646567D2B54072EEC275FB79BEBF82C7EE31,
	VFXSpawnerState_Release_m9D420CAC0353FCBCBF0F2D59AA286ED3D0BC4649,
	VFXSpawnerState_Finalize_mDD2AA40919A974E7CE616FE003162A762B806CA5,
	VFXSpawnerState_Dispose_mE45940AB83108F8F02D81BE4CEDE8C738F1C47F5,
	VFXSpawnerState_Internal_Destroy_mED5E41CC22939E5F21A53FC44CA6FA7022F87273,
	VisualEffectObject__ctor_mC6F115A9348A51326C366ACB2724884050A00B2D,
	VisualEffectAsset__ctor_m61A7A279415031194C654C892DFB4A1AD19CCC7C,
	VisualEffectAsset__cctor_mD1DB26F94C7046E351AB856DE9666BEB465E759E,
	VFXOutputEventArgs__ctor_m7C49FC1B34F3582FE08D7A1CAA5024C09A86F7A2,
	VisualEffect_set_pause_mAC5D0B2D037A4869FE22A237924530C267E79FA4,
	VisualEffect_get_visualEffectAsset_m019D107588568B2C5F1FFA77C626DC986A63AD96,
	VisualEffect_CreateVFXEventAttribute_mE99FC6DA23D465D06A8547CC5B2502FB0B30D8D6,
	VisualEffect_CheckValidVFXEventAttribute_m798412C5B7ACAD1DC17366FD1A8F35B7A289BC58,
	VisualEffect_SendEventFromScript_m6FDCD210185FA5B18F9951736974653E66F1A6F4,
	VisualEffect_SendEvent_mB81EDFCDACF7DF4FF59DB085BA061C3F5282E139,
	VisualEffect_SendEvent_mB86244AAF394CA825E843E692CDD34F174B71BCB,
	VisualEffect_HasFloat_mF05173BE6C2286D2CCCACDD96E57960BD4740F68,
	VisualEffect_HasVector3_m290498C54F35B5793B7A748555FBF17DEE23D157,
	VisualEffect_SetFloat_mF115AB6D0D748CF247A71ED283EC8877498762EA,
	VisualEffect_SetVector2_m0EEDC422CC1F08984D7EDF7AC59F17756E2DEDE9,
	VisualEffect_SetVector3_m9B318DA2BD87BC77E2E06B6821D2D0751C3CE6CB,
	VisualEffect_SetVector4_m18B5E3734A0A6BBECCAA926DEAA527DF40D8E431,
	VisualEffect_GetVector3_m7CB65F483251FB276F7F62040FE4757B234D4E38,
	VisualEffect_HasFloat_m4CD2B4D03449F285E31ACD55B0A261E91DAF5877,
	VisualEffect_HasVector3_mA74BFA10A311D183CE71A0039E9D3D6CD5B8E257,
	VisualEffect_SetFloat_m0D9164EE5F6C6643E1C0449BCB6D5E8FD05CBE13,
	VisualEffect_SetVector2_m6949CCFB08C62861FEF319208D3E3289AB87DE8C,
	VisualEffect_SetVector3_mB54AD688AADF2AABF7A053D4188EC7076673D7F1,
	VisualEffect_SetVector4_mC329BCD56199CCC86D1D7EC2A898F17ACE413792,
	VisualEffect_GetVector3_mB50AF62B083482685CF41A493E35691638D28970,
	VisualEffect_InvokeGetCachedEventAttributeForOutputEvent_Internal_m1A4D8B1736777CB18C2893313309880E61798C6B,
	VisualEffect_InvokeOutputEventReceived_Internal_mFC477F53118047CFF858A82FE87DF9356706421A,
	VisualEffect_SetVector2_Injected_mA8DDFCFCC959AB0E5D82B2A55A7035F4003DDA9B,
	VisualEffect_SetVector3_Injected_mA73BA46A825BE9994BA950DB314C64A9BEE8A89C,
	VisualEffect_SetVector4_Injected_mBFF4141512B0D2117A13DDDDF926D6572871A1D9,
	VisualEffect_GetVector3_Injected_mAC9972D56C569104905B4CBDD18071B452182E32,
};
extern void VFXOutputEventArgs__ctor_m7C49FC1B34F3582FE08D7A1CAA5024C09A86F7A2_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x0600001A, VFXOutputEventArgs__ctor_m7C49FC1B34F3582FE08D7A1CAA5024C09A86F7A2_AdjustorThunk },
};
static const int32_t s_InvokerIndices[53] = 
{
	1218,
	6178,
	6004,
	3287,
	3983,
	4055,
	4055,
	4055,
	6102,
	4055,
	6003,
	1245,
	1245,
	1245,
	4055,
	1955,
	6183,
	3273,
	4055,
	4055,
	4055,
	6102,
	4055,
	4055,
	6207,
	1816,
	3320,
	3983,
	3983,
	3287,
	1816,
	1816,
	3287,
	2869,
	2869,
	1840,
	1865,
	1867,
	1869,
	3096,
	2887,
	2887,
	1981,
	1983,
	1984,
	1985,
	3097,
	6004,
	5767,
	1758,
	1758,
	1758,
	1758,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_VFXModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_VFXModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_VFXModule_CodeGenModule = 
{
	"UnityEngine.VFXModule.dll",
	53,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_VFXModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
