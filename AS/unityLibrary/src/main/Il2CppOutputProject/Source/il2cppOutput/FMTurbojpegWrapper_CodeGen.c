﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Byte[] FMExtensionMethods::FMEncodeToJPG(UnityEngine.Texture2D,System.Int32,FMChromaSubsamplingOption)
extern void FMExtensionMethods_FMEncodeToJPG_mF34C6EA42568D795E3907398B0805642671FC990 (void);
// 0x00000002 System.Void FMExtensionMethods::FMLoadJPG(UnityEngine.Texture2D,UnityEngine.Texture2D&,System.Byte[])
extern void FMExtensionMethods_FMLoadJPG_m6F80A4605FECD0D7A6FD7C9088363FD490A286BE (void);
// 0x00000003 System.Void FMExtensionMethods::FMMatchResolution(UnityEngine.Texture2D,UnityEngine.Texture2D&,System.Int32,System.Int32)
extern void FMExtensionMethods_FMMatchResolution_m5CA46D3AD35FE10810AE24C762DACAD0CB68C23A (void);
// 0x00000004 System.Byte[] FMExtensionMethods::FMRawTextureDataToJPG(System.Byte[],System.Int32,System.Int32,System.Int32,FMChromaSubsamplingOption)
extern void FMExtensionMethods_FMRawTextureDataToJPG_m85A2EA12E76A38D087B912D0CED0F084C3D9EE8A (void);
// 0x00000005 System.Void FMExtensionMethods::FMJPGToRawTextureData(System.Byte[],System.Byte[]&,System.Int32&,System.Int32&,UnityEngine.TextureFormat)
extern void FMExtensionMethods_FMJPGToRawTextureData_mA8B43E8A0D5B9712F4A85D74F72BE51D132CDBAD (void);
// 0x00000006 FMTurboJpegWrapper.TJSubsamplingOption FMExtensionMethods::GetTJSubsampligOption(FMChromaSubsamplingOption)
extern void FMExtensionMethods_GetTJSubsampligOption_m74FD4BA6471F91419E3E139DFD8815E7C2CC0D1F (void);
// 0x00000007 System.Void FMTurboJpegWrapper.TJDecompressor::.ctor()
extern void TJDecompressor__ctor_m9DDCFB672B44DA49F610E0E5232F288EA50AAA34 (void);
// 0x00000008 System.Void FMTurboJpegWrapper.TJDecompressor::Finalize()
extern void TJDecompressor_Finalize_mC383BAD6492DC0B469BEBC3E92DF1D849F8D68FE (void);
// 0x00000009 System.Byte[] FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
extern void TJDecompressor_Decompress_m3A19D8ABEFBC56FEF69012A80F78D4480D070A2E (void);
// 0x0000000A System.Void FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,System.IntPtr,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
extern void TJDecompressor_Decompress_m39C66057ABFB7B90C028F1374BEF01D5452B3EC2 (void);
// 0x0000000B FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
extern void TJDecompressor_Decompress_m3DEC0F0E4A220B42DFD7D38D38B0389F44445C75 (void);
// 0x0000000C FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.Byte[],FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
extern void TJDecompressor_Decompress_m80B39EB731429D27BACDFB17B8B977ADA76651CC (void);
// 0x0000000D FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::DecodeFMJPG(System.Byte[],UnityEngine.TextureFormat)
extern void TJDecompressor_DecodeFMJPG_m6CC0B007AA65465F562ACE401F2D6129022341E1 (void);
// 0x0000000E System.Void FMTurboJpegWrapper.TJDecompressor::GetImageInfo(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern void TJDecompressor_GetImageInfo_m1BCAA6AC6AB869AACC1EAD4C29D9E01E6B27E5C3 (void);
// 0x0000000F System.Void FMTurboJpegWrapper.TJDecompressor::Dispose()
extern void TJDecompressor_Dispose_m918F5429F1097880B5945F8587E9B0017A37F2E1 (void);
// 0x00000010 System.Void FMTurboJpegWrapper.TJDecompressor::Dispose(System.Boolean)
extern void TJDecompressor_Dispose_m916E34446FE0C5DD2101267A42BC54C027153B1B (void);
// 0x00000011 System.Void FMTurboJpegWrapper.DecompressedImage::.ctor(System.Int32,System.Int32,System.Int32,System.Byte[],FMTurboJpegWrapper.TJPixelFormat)
extern void DecompressedImage__ctor_mEA72381F39D50F44CB114784B95B02B9B10007B4 (void);
// 0x00000012 System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Width()
extern void DecompressedImage_get_Width_mD99C0D51A23DFE6043BFA0124864922153892CAF (void);
// 0x00000013 System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Height()
extern void DecompressedImage_get_Height_m9BBC5D9D1CFF42D53D088026465248DFD6400D83 (void);
// 0x00000014 System.Byte[] FMTurboJpegWrapper.DecompressedImage::get_Data()
extern void DecompressedImage_get_Data_m5845759BA644DDCB0A686751B401A22149211BF9 (void);
// 0x00000015 System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitCompress()
extern void TurboJpegImport_TjInitCompress_m0E11173E0A9EC8190333E8810615BAA1260B7261 (void);
// 0x00000016 System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjCompress2(System.IntPtr,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.IntPtr&,System.UInt64&,System.Int32,System.Int32,System.Int32)
extern void TurboJpegImport_TjCompress2_m153EB5525C48045D5CF86EC1A74B66714C6DD65E (void);
// 0x00000017 System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitDecompress()
extern void TurboJpegImport_TjInitDecompress_m249156F548208F11CE0EB14918ED99F1820AA97C (void);
// 0x00000018 System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern void TurboJpegImport_TjDecompressHeader_m1C70E63777A15B304A78A5E262481DA984B152A5 (void);
// 0x00000019 System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void TurboJpegImport_TjDecompress_m591B0598027516C484448BE035AB9E75D5B60471 (void);
// 0x0000001A System.Void FMTurboJpegWrapper.TurboJpegImport::TjFree(System.IntPtr)
extern void TurboJpegImport_TjFree_mAEF709FF49044B6A1021411427D48CA7841F35FC (void);
// 0x0000001B System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDestroy(System.IntPtr)
extern void TurboJpegImport_TjDestroy_m133702A671B7275E7B1C1F096A4DC2983AFC6184 (void);
// 0x0000001C System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x86(System.IntPtr,System.IntPtr,System.UInt32,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern void TurboJpegImport_TjDecompressHeader3_x86_m1CC33DD0404AF9F552013DA9CBE510B6BD9457F7 (void);
// 0x0000001D System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x64(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern void TurboJpegImport_TjDecompressHeader3_x64_mD6E45CE0657F44D84480954738D2FE6A833935B9 (void);
// 0x0000001E System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x86(System.IntPtr,System.IntPtr,System.UInt32,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void TurboJpegImport_TjDecompress2_x86_mA296A566DD1B56EF26610DC67299E2D58ADC8842 (void);
// 0x0000001F System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x64(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void TurboJpegImport_TjDecompress2_x64_m7D71BE1DAD8A4013D134CD0683E3300A3515BBFC (void);
// 0x00000020 System.Void FMTurboJpegWrapper.TurboJpegImport::.cctor()
extern void TurboJpegImport__cctor_m6FFFAE012B029556B93B50A4476BDDE71F9D7850 (void);
// 0x00000021 System.Void FMTurboJpegWrapper.TJCompressor::.ctor()
extern void TJCompressor__ctor_m2BE57DF352ADAB1D70B3CBB7C48615E46D102780 (void);
// 0x00000022 System.Void FMTurboJpegWrapper.TJCompressor::Finalize()
extern void TJCompressor_Finalize_m4F353B1CBF857C1F7248466A268B31FE72FE220D (void);
// 0x00000023 System.Byte[] FMTurboJpegWrapper.TJCompressor::Compress(System.Byte[],System.Int32,System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJSubsamplingOption,System.Int32,FMTurboJpegWrapper.TJFlags)
extern void TJCompressor_Compress_m928E468B43ECEB3E6083AC8684F39347BCB90849 (void);
// 0x00000024 System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(UnityEngine.Texture2D,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
extern void TJCompressor_EncodeFMJPG_m04A1BB9FBAE993BE6DABC415BBB01574C24716A1 (void);
// 0x00000025 System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(System.Byte[],System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
extern void TJCompressor_EncodeFMJPG_mB6286123E7EC12E8C34EFF1B0D772F97ACC9D561 (void);
// 0x00000026 System.Void FMTurboJpegWrapper.TJCompressor::Dispose()
extern void TJCompressor_Dispose_mEF9EFCA1BCC301303AECF2137C940FF92E152B85 (void);
// 0x00000027 System.Void FMTurboJpegWrapper.TJCompressor::Dispose(System.Boolean)
extern void TJCompressor_Dispose_m0128E1435D98AB29E9BAEAB76F283D6AD4E1E3B0 (void);
// 0x00000028 System.Void FMTurboJpegWrapper.TJCompressor::CheckOptionsCompatibilityAndThrow(FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TJPixelFormat)
extern void TJCompressor_CheckOptionsCompatibilityAndThrow_m2B09B024F4D0D0C1F9B4800CED86E353E453032B (void);
// 0x00000029 System.Void FMTurboJpegWrapper.TjSize::.ctor(System.Int32,System.Int32)
extern void TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6 (void);
// 0x0000002A System.Void FMTurboJpegWrapper.TjSize::set_Width(System.Int32)
extern void TjSize_set_Width_m40DB05B6594E8FB81CD072C94EA99F619F12B648 (void);
// 0x0000002B System.Void FMTurboJpegWrapper.TjSize::set_Height(System.Int32)
extern void TjSize_set_Height_mA6BFA2A673A5796D6E5FD2623EBA70D2EB90AD98 (void);
// 0x0000002C System.Void FMTurboJpegWrapper.TJUtils::GetErrorAndThrow()
extern void TJUtils_GetErrorAndThrow_m116A7140103D97D986F589FC565B881F9ED3E046 (void);
static Il2CppMethodPointer s_methodPointers[44] = 
{
	FMExtensionMethods_FMEncodeToJPG_mF34C6EA42568D795E3907398B0805642671FC990,
	FMExtensionMethods_FMLoadJPG_m6F80A4605FECD0D7A6FD7C9088363FD490A286BE,
	FMExtensionMethods_FMMatchResolution_m5CA46D3AD35FE10810AE24C762DACAD0CB68C23A,
	FMExtensionMethods_FMRawTextureDataToJPG_m85A2EA12E76A38D087B912D0CED0F084C3D9EE8A,
	FMExtensionMethods_FMJPGToRawTextureData_mA8B43E8A0D5B9712F4A85D74F72BE51D132CDBAD,
	FMExtensionMethods_GetTJSubsampligOption_m74FD4BA6471F91419E3E139DFD8815E7C2CC0D1F,
	TJDecompressor__ctor_m9DDCFB672B44DA49F610E0E5232F288EA50AAA34,
	TJDecompressor_Finalize_mC383BAD6492DC0B469BEBC3E92DF1D849F8D68FE,
	TJDecompressor_Decompress_m3A19D8ABEFBC56FEF69012A80F78D4480D070A2E,
	TJDecompressor_Decompress_m39C66057ABFB7B90C028F1374BEF01D5452B3EC2,
	TJDecompressor_Decompress_m3DEC0F0E4A220B42DFD7D38D38B0389F44445C75,
	TJDecompressor_Decompress_m80B39EB731429D27BACDFB17B8B977ADA76651CC,
	TJDecompressor_DecodeFMJPG_m6CC0B007AA65465F562ACE401F2D6129022341E1,
	TJDecompressor_GetImageInfo_m1BCAA6AC6AB869AACC1EAD4C29D9E01E6B27E5C3,
	TJDecompressor_Dispose_m918F5429F1097880B5945F8587E9B0017A37F2E1,
	TJDecompressor_Dispose_m916E34446FE0C5DD2101267A42BC54C027153B1B,
	DecompressedImage__ctor_mEA72381F39D50F44CB114784B95B02B9B10007B4,
	DecompressedImage_get_Width_mD99C0D51A23DFE6043BFA0124864922153892CAF,
	DecompressedImage_get_Height_m9BBC5D9D1CFF42D53D088026465248DFD6400D83,
	DecompressedImage_get_Data_m5845759BA644DDCB0A686751B401A22149211BF9,
	TurboJpegImport_TjInitCompress_m0E11173E0A9EC8190333E8810615BAA1260B7261,
	TurboJpegImport_TjCompress2_m153EB5525C48045D5CF86EC1A74B66714C6DD65E,
	TurboJpegImport_TjInitDecompress_m249156F548208F11CE0EB14918ED99F1820AA97C,
	TurboJpegImport_TjDecompressHeader_m1C70E63777A15B304A78A5E262481DA984B152A5,
	TurboJpegImport_TjDecompress_m591B0598027516C484448BE035AB9E75D5B60471,
	TurboJpegImport_TjFree_mAEF709FF49044B6A1021411427D48CA7841F35FC,
	TurboJpegImport_TjDestroy_m133702A671B7275E7B1C1F096A4DC2983AFC6184,
	TurboJpegImport_TjDecompressHeader3_x86_m1CC33DD0404AF9F552013DA9CBE510B6BD9457F7,
	TurboJpegImport_TjDecompressHeader3_x64_mD6E45CE0657F44D84480954738D2FE6A833935B9,
	TurboJpegImport_TjDecompress2_x86_mA296A566DD1B56EF26610DC67299E2D58ADC8842,
	TurboJpegImport_TjDecompress2_x64_m7D71BE1DAD8A4013D134CD0683E3300A3515BBFC,
	TurboJpegImport__cctor_m6FFFAE012B029556B93B50A4476BDDE71F9D7850,
	TJCompressor__ctor_m2BE57DF352ADAB1D70B3CBB7C48615E46D102780,
	TJCompressor_Finalize_m4F353B1CBF857C1F7248466A268B31FE72FE220D,
	TJCompressor_Compress_m928E468B43ECEB3E6083AC8684F39347BCB90849,
	TJCompressor_EncodeFMJPG_m04A1BB9FBAE993BE6DABC415BBB01574C24716A1,
	TJCompressor_EncodeFMJPG_mB6286123E7EC12E8C34EFF1B0D772F97ACC9D561,
	TJCompressor_Dispose_mEF9EFCA1BCC301303AECF2137C940FF92E152B85,
	TJCompressor_Dispose_m0128E1435D98AB29E9BAEAB76F283D6AD4E1E3B0,
	TJCompressor_CheckOptionsCompatibilityAndThrow_m2B09B024F4D0D0C1F9B4800CED86E353E453032B,
	TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6,
	TjSize_set_Width_m40DB05B6594E8FB81CD072C94EA99F619F12B648,
	TjSize_set_Height_mA6BFA2A673A5796D6E5FD2623EBA70D2EB90AD98,
	TJUtils_GetErrorAndThrow_m116A7140103D97D986F589FC565B881F9ED3E046,
};
extern void TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6_AdjustorThunk (void);
extern void TjSize_set_Width_m40DB05B6594E8FB81CD072C94EA99F619F12B648_AdjustorThunk (void);
extern void TjSize_set_Height_mA6BFA2A673A5796D6E5FD2623EBA70D2EB90AD98_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x06000029, TjSize__ctor_mF5D73F9C0F28F6E700292E226BE6D1D607A063E6_AdjustorThunk },
	{ 0x0600002A, TjSize_set_Width_m40DB05B6594E8FB81CD072C94EA99F619F12B648_AdjustorThunk },
	{ 0x0600002B, TjSize_set_Height_mA6BFA2A673A5796D6E5FD2623EBA70D2EB90AD98_AdjustorThunk },
};
static const int32_t s_InvokerIndices[44] = 
{
	5119,
	5345,
	4899,
	4484,
	4586,
	5932,
	4055,
	4055,
	116,
	41,
	623,
	1017,
	1449,
	138,
	4055,
	3320,
	412,
	3965,
	3965,
	3983,
	6178,
	4171,
	6178,
	4250,
	4185,
	6102,
	5934,
	4248,
	4250,
	4184,
	4185,
	6207,
	4055,
	4055,
	61,
	1017,
	205,
	4055,
	3320,
	5739,
	1802,
	3271,
	3271,
	6207,
};
extern const CustomAttributesCacheGenerator g_FMTurbojpegWrapper_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_FMTurbojpegWrapper_CodeGenModule;
const Il2CppCodeGenModule g_FMTurbojpegWrapper_CodeGenModule = 
{
	"FMTurbojpegWrapper.dll",
	44,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_FMTurbojpegWrapper_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
