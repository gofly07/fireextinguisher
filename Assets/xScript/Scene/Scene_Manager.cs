﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Scene_Manager : MonoBehaviour
{
    public static Scene_Manager instance = new Scene_Manager();

    [Header("Streaming Paramenters")]
    [SerializeField] private Transform target;

    [Header("Fire Extinguisher Changer")]
    public DKP.VR.HandsVisualization handsVisualizationL;
    public DKP.VR.HandsVisualization handsVisualizationR;

    public string type = "乾粉滅火器模式";
    [SerializeField] private Text text;

    private void Start()
    {
        instance = this;
        Scene_Manager.instance.SwitchFireExtinguisher(DKP.VR.HandsVisualization.DisplayMode.Powder);

        text.text = "乾粉滅火器模式"; 
        type = "乾粉滅火器模式";
    }

    public void SwitchFireExtinguisher(DKP.VR.HandsVisualization.DisplayMode displayMode)
    {
        handsVisualizationL.ChangeState(displayMode);
        handsVisualizationR.ChangeState(displayMode);
    }

    private void Update()
    {
        if (OVRInput.GetDown(OVRInput.RawButton.B) && type == "乾粉滅火器模式")
        {
            type = "二氧化碳滅火器模式";
            Scene_Manager.instance.SwitchFireExtinguisher(DKP.VR.HandsVisualization.DisplayMode.CO2);
            text.text = "二氧化碳滅火器模式";
        }
        else if (OVRInput.GetDown(OVRInput.RawButton.B) && type == "二氧化碳滅火器模式")
        {
            type = "泡沫滅火器模式";
            Scene_Manager.instance.SwitchFireExtinguisher(DKP.VR.HandsVisualization.DisplayMode.Foam);
            text.text = "泡沫滅火器模式";
        }
        else if (OVRInput.GetDown(OVRInput.RawButton.B) && type == "泡沫滅火器模式")
        {
            type = "乾粉滅火器模式";
            Scene_Manager.instance.SwitchFireExtinguisher(DKP.VR.HandsVisualization.DisplayMode.Powder);
            text.text = "乾粉滅火器模式";
        }

#if !UNITY_EDITOR
        if (FMNetworkManager.instance.isActiveAndEnabled)
        {
            FMNetworkManager.instance.gameObject.transform.SetPositionAndRotation(target.position, target.rotation);
            FMNetworkManager.instance.gameObject.transform.GetChild(1).GetComponent<Canvas>().worldCamera = target.GetComponent<Camera>();
        }
#endif

    }
}
