﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using static GPTT.Data.API;
public class Panel_FireTypeIntroduction : MonoBehaviour
{
    [SerializeField] private Board board;
   
    [SerializeField] private List<FireType> fireTypes = new List<FireType>();
    [SerializeField] private Button btBack;

    public event EventHandler EHCloseFireType;

    [System.Serializable]
    private class FireType
    {
        public Toggle tgType;
        public string strTitle;
        [TextArea]
        public string strDescription;
        public GameObject displayObjects;
        public bool isClick = false;
    }

    [System.Serializable]
    private class Board
    {
        public RectTransform rectBoard;
        public Text txtBoardTitle;
        public Text txtBoardBody;
    }

    //**
    private FireExtinguisher fireExtinguisher = new FireExtinguisher();
    private DateTime _startTime;
    private DateTime _endTime;
    //**

    private void OnEnable()
    { 
        //**
        fireExtinguisher = new FireExtinguisher();
        _startTime = DateTime.Now;
        fireExtinguisher.startTime = _startTime.ToString("yyyy-MM-dd HH:mm:ss");
        //**

        Init();
        foreach (var i in fireTypes)
        {
            i.isClick = false;
            i.displayObjects.gameObject.SetActive(false);
            i.tgType.onValueChanged.AddListener(delegate { FireTypeChange(i.tgType); });
        }

        btBack.onClick.AddListener(delegate { StartCoroutine(BackToMenu()); });
        click = false;
    }

    private void Init()
    {
        board.rectBoard.gameObject.SetActive(false);
    }

    private void FireTypeChange(Toggle t)
    {
        foreach (var i in fireTypes)
        {
            if (i.tgType == t)
            {
                if (t.isOn)
                {
                    board.rectBoard.gameObject.SetActive(true);
                    i.displayObjects.SetActive(true);

                    board.txtBoardTitle.text = i.strTitle;
                    board.txtBoardBody.text = i.strDescription;
                    t.gameObject.GetComponent<AudioSource>().Play();
                    i.isClick = true;
                }
                else if (!t.isOn)
                {
                    board.rectBoard.gameObject.SetActive(false);
                    i.displayObjects.SetActive(false);

                    t.gameObject.GetComponent<AudioSource>().Stop();
                }
            }
        }
    }

    bool click = false;
    private IEnumerator BackToMenu()
    {
        if (!click)
        {
            click = true;
#if !UNITY_EDITOR
            //**
            _endTime = DateTime.Now;
            fireExtinguisher.endTime = _endTime.ToString("yyyy-MM-dd HH:mm:ss");

            var diff = _endTime.Subtract(_startTime);
            var res = String.Format("{0}h {1}m {2}s", diff.Hours, diff.Minutes, diff.Seconds);
            fireExtinguisher.totalTime = res;

            fireExtinguisher.project = "火災類型與滅火器-火災類型";

            List<bool> p = new List<bool>();
            foreach (var i in fireTypes)
                p.Add(i.isClick);

            if (p.Contains(false))
                fireExtinguisher.status = "未完成";
            else
                fireExtinguisher.status = "已完成";

            fireExtinguisher.userId = UserIDCheck.userID;

            yield return (GPTT.Data.APIHelper.FireExtinguisher_Insert(fireExtinguisher));
            //**
#endif
            yield return null;
            EHCloseFireType(this, EventArgs.Empty);
        }
    }
}
