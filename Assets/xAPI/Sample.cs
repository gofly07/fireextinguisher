﻿using GPTT.Data;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using static GPTT.Data.API;

namespace GPTT.Data
{
    public class Sample : MonoBehaviour
    {
        public Text UserInfo;
        private void Start()
        {
            Debug.Log(Util.GetSerialNumber());
            APIHelper.Init();
        }

        public void LoginOnclick()
        {
            //登入
            Login user = new Login
            {
                account = "test_firefighting@gptt.com.tw",
                pwd = Util.ToMD5("!QAZ2wsx")
            };
            StartCoroutine(APIHelper.Login(user, delegate (User user)
             {
                 UserInfo.text = user.name;
             }));
        }

        public void LogoutOnclick()
        {
            //登出
            APIHelper.Logout();
        }

        public void FireExtinguisherOnclick()
        {
            //新增滅火器訓練紀錄
            FireExtinguisher fireExtinguisher = new FireExtinguisher
            {
                startTime = "2021-12-06 13:00:00",
                endTime = "2021-12-06 13:35:24",
                totalTime = "0h 34m 24s",
                project = "滅火測試-大樓",
                status = "已完成",
                userId = "1"
            };
            StartCoroutine(APIHelper.FireExtinguisher_Insert(fireExtinguisher));
        }

        public void FireExtinguisherSearchOnclick()
        {
            //搜尋滅火器訓練紀錄列表
            Search search = new Search
            {
                limit = "10",
                page = "1",
                sort = "desc",
                sort_col = "id",
                userId = "1"
            };
            StartCoroutine(APIHelper.FireExtinguisherList_Search(search));
        }

        public void FiremenOnclick()
        {
            //新增消防員訓練紀錄
            Firemen firemen = new Firemen();
            firemen.trainingDate = "2021-12-06";
            firemen.startTime = "14:10:20";
            firemen.endTime = "15:12:30";
            firemen.totalTime = "01:02:10";
            firemen.modCategory = "模組5";
            firemen.place = "住宅";
            firemen.videoName = "2022-03-14 10:29:58";
            firemen.userId = "1";

            FiremenDetailed firemenDetailed = new FiremenDetailed();
            firemenDetailed.trainingTime = "14:11:20";
            firemenDetailed.personnel = "01水線指揮官";
            firemenDetailed.posture = "低姿勢";
            firemenDetailed.behavior = "銜接1-1/2水帶";
            firemenDetailed.location = "1樓";
            firemenDetailed.userId = "2";
            firemen.detailedList.Add(firemenDetailed);

            firemenDetailed = new FiremenDetailed();
            firemenDetailed.trainingTime = "14:12:20";
            firemenDetailed.personnel = "02副瞄子手";
            firemenDetailed.posture = "低姿勢";
            firemenDetailed.behavior = "使用SCBA";
            firemenDetailed.location = "1樓";
            firemenDetailed.userId = "2";
            firemen.detailedList.Add(firemenDetailed);

            firemenDetailed = new FiremenDetailed();
            firemenDetailed.trainingTime = "14:12:20";
            firemenDetailed.personnel = "03正瞄子手";
            firemenDetailed.posture = "低姿勢";
            firemenDetailed.behavior = "使用SCBA";
            firemenDetailed.location = "1樓";
            firemenDetailed.userId = "2";
            firemen.detailedList.Add(firemenDetailed);

            StartCoroutine(APIHelper.Firemen_Insert(firemen));
        }

        public void FiremenSearchOnclick()
        {
            //搜尋消防員訓練紀錄列表
            Search search = new Search
            {
                limit = "10",
                page = "1",
                sort = "asc",
                sort_col = "id",
                userId = "2"
            };
            StartCoroutine(APIHelper.FiremenList_Search(search));
        }

        public void FiremenDetailSearchOnclick()
        {
            //搜尋消防員訓練歷程列表
            Search search = new Search
            {
                limit = "10",
                page = "1",
                sort = "asc",
                sort_col = "id",
                userId = "2",
                firemenId = "20"
            };
            StartCoroutine(APIHelper.FiremenDetailList_Search(search));
        }

        //IEnumerator Login(Login login)
        //{
        //    yield return API.Login_API(login);

        //    if (API.ResponseMessage["responseCode"] == "200")
        //    {
        //        Debug.Log(UserData.id + " " + UserData.name + " " + UserData.email + " " + UserData.unit);
        //    }
        //    else
        //    {
        //        Debug.Log(API.ResponseMessage["message"]);
        //    }
        //}

        //IEnumerator FireExtinguisher_Insert(FireExtinguisher fireExtinguisher)
        //{
        //    yield return API.FireExtinguisher_API(fireExtinguisher);
        //    Debug.Log(API.ResponseMessage["responseCode"]);
        //    Debug.Log(API.ResponseMessage["message"]);
        //}

        //IEnumerator FireExtinguisherList_Search(Search search)
        //{
        //    yield return API.FireExtinguisherList_API(search);

        //    if (API.ResponseMessage["responseCode"] == "200")
        //    {
        //        Debug.Log(API.fireExtinguisherListData.total);
        //        Debug.Log(API.fireExtinguisherListData.last_page);

        //        foreach (FireExtinguisher data in API.fireExtinguisherListData.Data)
        //        {
        //            Debug.Log(data.id + " " + data.startTime + " " + data.endTime + " " + data.totalTime + " " + data.project + " " + data.status + " " + data.userName);
        //        }
        //    }
        //    else
        //    {
        //        Debug.Log(API.ResponseMessage["message"]);
        //    }
        //}

        //IEnumerator Firemen_Insert(Firemen firemen)
        //{
        //    yield return API.Firemen_API(firemen);

        //    if (API.ResponseMessage["responseCode"] != "200")
        //        Debug.Log(API.ResponseMessage["message"]);
        //}

        //IEnumerator FiremenList_Search(Search search)
        //{
        //    yield return API.FiremenList_API(search);

        //    if (API.ResponseMessage["responseCode"] == "200")
        //    {
        //        Debug.Log(API.firemenListData.total);
        //        Debug.Log(API.firemenListData.last_page);

        //        foreach (Firemen data in API.firemenListData.Data)
        //        {
        //            Debug.Log(data.id + " " + data.trainingDate + " " + data.startTime + " " + data.endTime + " " + data.totalTime + " " + data.modCategory + " " + data.place + " " + data.userName);
        //        }
        //    }
        //    else
        //    {
        //        Debug.Log(API.ResponseMessage["message"]);
        //    }
        //}

        //IEnumerator FiremenDetailList_Search(Search search)
        //{
        //    yield return API.FiremenDetailList_API(search);

        //    if (API.ResponseMessage["responseCode"] == "200")
        //    {
        //        Debug.Log(API.firemenDetailedData.total);
        //        Debug.Log(API.firemenDetailedData.last_page);

        //        foreach (FiremenDetailed data in API.firemenDetailedData.Data)
        //        {
        //            Debug.Log(data.id + " " + data.trainingTime + " " + data.personnel + " " + data.posture + " " + data.behavior + " " + data.location + " " + data.userName);
        //        }
        //    }
        //    else
        //    {
        //        Debug.Log(API.ResponseMessage["message"]);
        //    }
        //}
    }
}